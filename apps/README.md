# 业务组小部件开发指南 &middot; ![node version](https://img.shields.io/badge/node-8.10.0-brightgreen.svg) ![npm version](https://img.shields.io/badge/npm-5.6.0-blue.svg)  ![webpack version](https://img.shields.io/badge/webpack-4.1.1-blue.svg)

## 目录结构

```
|-- 开发目录结构
    |-- package.json
    |-- README.md
    |-- webpack.common.js
    |-- webpack.dev.js
    |-- webpack.prod.js
    |-- src
        |-- platform-widgets //小部件开发代码文件夹
            |-- widget //开发者自定义，根据业务定义命名
            |   |-- index.html //*必须
            |   |-- index.js //*必须
            |   |-- style.css
            |-- widget1
            |   |-- index.html
            |   |-- index.js
            |   |-- style.css
            |-- widget2
                |-- index.html
                |-- index.js
                |-- Sansation_Light.ttf
                |-- style.less
                |-- tmp.jpg
                |-- tmp2.png
===============================    
以下为生产所得文件目录结构
===============================
|-- 生产文件目录结构
    |-- platform-widgets
        |-- widget
        |   |-- index.html
        |   |-- index.js
        |-- widget1
        |   |-- index.html
        |   |-- index.js
        |-- widget2
            |-- index.html
            |-- index.js
            |-- Sansation_Light.7fd474bd.ttf
            |-- tmp.033e7da5.jpg
            |-- tmp2.b69e1401.png
```

## package.json的结构和依赖版本号

项目基于nodejs v8.10.0，npm v5.6.0，webpack v4.1.1
```json
{
  "name": "choose_the_seat",
  "version": "1.0.0",
  "babel": {
    "presets": [
      "env",
      "react",
      "stage-2"
    ],
    "plugins": []
  },
  "description": "choose the seat demo",
  "main": "index.js",
  "scripts": {
    "devapp": "webpack-dev-server --open --config webpack.dev.js --mode development",//平台小部件开发模式命令
    "buildapp": "webpack --config webpack.prod.js --mode production"//平台小部件生产模式命令
  },
  "keywords": [
    "js"
  ],
  "author": "glud",
  "license": "MIT",
  "devDependencies": {
    "autoprefixer": "^8.1.0",
    "babel-core": "^6.26.0",
    "babel-loader": "^7.1.4",
    "babel-preset-env": "^1.6.1",
    "babel-preset-react": "^6.24.1",
    "babel-preset-stage-2": "^6.24.1",
    "clean-webpack-plugin": "^0.1.19",
    "copy-webpack-plugin": "^4.5.1",
    "css-loader": "^0.28.11",
    "cssnano": "^3.10.0",
    "file-loader": "^1.1.11",
    "glob": "^7.1.2",
    "html-webpack-plugin": "^3.0.6",
    "less": "^3.0.1",
    "less-loader": "^4.1.0",
    "postcss-loader": "^2.1.3",
    "style-loader": "^0.20.3",
    "uglifyjs-webpack-plugin": "^1.2.4",
    "url-loader": "^1.0.1",
    "webpack": "^4.1.1",
    "webpack-cli": "^2.0.12",
    "webpack-dev-server": "^3.1.1",
    "webpack-merge": "^4.1.2"
  },
  "dependencies": {
    "echarts": "^4.0.4",
    "prop-types": "^15.6.1",
    "react": "^16.2.0",
    "react-dom": "^16.2.0"
  }
}
```

## 业务组开发使用步骤

*直接下载本项目，在本项目的基础上进行修改*
1. apps文件夹中放置的是业务组开发小部件的环境
2. 在apps文件夹下面，运行`npm install`，自动安装依赖环境
3. 外部（ncpub-multipage-demo根目录下）`config.json`中的属性值，

```
 "appProdOutputPath": "../dist", //表示输出路径为app的父级目录结构中的dist文件夹
	"appDevPort": 9000, //表示开发模式下的端口号为9000
	"appDevProxy": "http://127.0.0.1:80/", //表示开发模式下的代理服务器地址和端口，
```

4. 在项目中运行`npm run devapp`，即可打开 `http://localhost:9000`，想看某一个小部件运行效果，则需要切换到该文件夹下，如路径:`http://localhost:9000/platform-widgets/widget/` 。
5. 在项目中运行`npm run buildapp`，即在父级文件夹中dist文件夹下生成结果，目录结构上文已提到。

## 代码开发步骤示例
1. 业务组在*platform-widgets*下创建新的小部件文件夹，名称根据业务需要自定义
2. 将提供的widget文件夹中的index.html复制一份，放入新文件夹中，并创建index.js，
3. 将所需的静态资源文件直接放入新文件夹中，
4. 挂载点ID的命名规则为`模块英文名_组件英文名_小部件英文名`，
5. 修改index.html中的挂载点的ID
6. index.js中的`ReactDOM.render`方法中写清楚`document.querySelector('index.html对应挂载点ID')`
7. 运行`npm run devapp`，可查看效果； 
8. 值得注意的是，挂载点dom的宽和高已经根据UE规范给定了N个不同的尺寸。
    

## 常见的问题

1. 运行`npm run nc-dev`，端口被占用：打开文件config.json，修改`appDevPort`对应的数字；
2. 为了优化请求，html中已经引入了公共的依赖类库，react、react-dom、polyfill、axios、echarts，请勿重复引用；
3. 查看生产环境下的页面，需要在页面运行在服务器中，才能查看运行效果；
4. 命令打包过程中，出现打包文件体积大的黄色提示，只是警告，可以忽略。

