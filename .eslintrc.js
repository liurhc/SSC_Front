module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: 'airbnb',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  parser: "babel-eslint",
  plugins: [
    'react',
  ],
  settings: {
    "import/resolver": "webpack" 
  },
  rules: {
    'react/require-default-props': ['off'],
    'react/default-props-match-prop-types': ['off'],
    'linebreak-style': 0,
    'react/sort-comp': 0,
    'react/prop-types': 0,
    'react/jsx-filename-extension': 0,
    'func-names': 0,
    'react/jsx-no-bind': 0,
    'new-cap': 0,
    'react/destructuring-assignment': 0,
    'no-unused-expressions': 0,
    'max-len': 0,
    'eqeqeq': 0,
    'no-param-reassign': 0, //不能给函数的参数赋值
    "comma-dangle": ["error", "never"],
    'indent': [2, 4],
    'react/jsx-indent': [2, 4],
    'react/jsx-indent-props': [2, 4]
  },
};
