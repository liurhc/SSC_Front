import { high } from 'nc-lightapp-front';

const { Refer } = high;
const formId = 'head';

export default function (props = {}) {
	var conf = {
		multiLang:{
			domainName:'fct',
			currentLocale: 'simpchn', 
			moduleId: 'FCTREF'
		},
		refType: 'grid',
		refName: 'FCTREF-000000',
		placeholder: 'FCTREF-000000',
		queryGridUrl: '/nccloud/fct/ap/ApFctGridRef.do',
		columnConfig:[
			{
				name: ['FCTREF-000001','FCTREF-000002','FCTREF-000003','FCTREF-000004','FCTREF-000005','FCTREF-000006','FCTREF-000007','FCTREF-000008'],
				code: ['vbillcode','refname','valdate','invallidate','cvendorid','ntotalorigmny','norigpshamount','norigcopamount']
			}
		]
	};
	return <Refer {...Object.assign(conf, props)} />
}
