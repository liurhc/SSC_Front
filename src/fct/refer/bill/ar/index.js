import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang:{
			domainName:'fct',
			currentLocale: 'simpchn', 
			moduleId: 'FCTREF'
		},
		refType: 'grid',
		refName: 'FCTREF-000009',
        placeholder: 'FCTREF-000009',
		queryGridUrl: '/nccloud/fct/ar/ArFctGridRef.do',
		columnConfig:[
			{
				name: ['FCTREF-000001','FCTREF-000002','FCTREF-000003','FCTREF-000004','FCTREF-000010','FCTREF-000006','FCTREF-000011','FCTREF-000008'],
				code: ['vbillcode','refname','valdate','invallidate','pk_customer','ntotalorigmny','norigpshamount','norigcopamount']
			}
		]
	};
	return <Refer {...Object.assign(conf, props)} />
}
