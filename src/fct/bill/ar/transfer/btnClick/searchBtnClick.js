/*
 * @PageInfo: 查询按钮点击  
 */
import { ajax, base, spaCache } from 'nc-lightapp-front';
let { setDefData, getDefData } = spaCache;
const { NCMessage } = base;
import { ar_searchId, ar_headId, ar_bbodyId, ar_planbodyId, ar_pageId, ar_oid, ar_billType, fctdatasource, ar_pkname } from '../constants';
//点击查询，获取查询区数据
export default function () {
	let searchVal = this.props.search.getAllSearchData(ar_searchId);
	if (searchVal) {
		let bodyId = null;
		let bodypk = null;
		if (this.props.getUrlParam('dest_billtype') == 'F2' || this.props.getUrlParam('dest_billtype') == 'F3') {//F0-应收单 F1-应付单 F2-收款单 F3-付款单
			bodyId = ar_planbodyId;
			bodypk = 'pk_fct_ar_plan';
		} else if (this.props.getUrlParam('dest_billtype') == 'F0' || this.props.getUrlParam('dest_billtype') == 'F1') {
			bodyId = ar_bbodyId;
			bodypk = 'pk_fct_ar_b';
		}
		let busitypedata = [];
		let arrays = getDefData(ar_billType + this.props.getUrlParam('src_tradetype'), 'transfer.dataSource')==null?null: getDefData(ar_billType + this.props.getUrlParam('src_tradetype'), 'transfer.dataSource');
		if(!arrays instanceof Array){
			busitypedata.push(arrays);
		}else{
			busitypedata = arrays;
		}
		let data = {
			queryInfo: this.props.search.getQueryInfo(ar_searchId),
			appcode: this.props.getUrlParam('src_appcode'),
			pageId: ar_pageId,
			src_billtype: ar_billType,
			dest_billtype: this.props.getUrlParam('dest_billtype'),			
			dest_tradetype: this.props.getUrlParam('dest_tradetype'),
			busitype: busitypedata
		};
		//得到数据渲染到页面
		ajax({
			url: '/nccloud/fct/bill/transferquery.do',
			data: data,
			success: (res) => {
				if (res.data) {
					this.props.transferTable.setTransferTableValue(ar_headId, bodyId, res.data, ar_pkname, bodypk);
					setDefData(ar_pkname, fctdatasource, res.data);
				} else {
					NCMessage.create({ content: this.state.json['200400ARM-000052'], color: 'success', position: 'bottom' });/* 国际化处理： 查询结果为空*/
					this.props.transferTable.setTransferTableValue(ar_headId, bodyId, [], ar_pkname, bodypk);
				}
			}
		});
	}


};
