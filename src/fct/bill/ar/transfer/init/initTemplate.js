/*
 * @PageInfo: 应收单转单列表  
 */
import { base } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
export default function (props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: this.pageId,//页面code
			appcode: props.getUrlParam('src_appcode')
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(that, props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	);
}

/**
 * 自定义元数据样式
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(that, props, meta) {
	meta[that.searchId].items = meta[that.searchId].items.map((item, key) => {
		item.col = '3';
		if ( item.attrcode == 'pk_org_v') {
			item.isMultiSelectedEnabled = true
		}
		/**
		 * @author Don
		 * 加入财务权限
		 */
		if (item.attrcode == 'pk_org') {
			item.isMultiSelectedEnabled = true
			item.queryCondition = () => {
				item.isShowUnit = false;
			    return {
		                 DataPowerOperationCode: 'fi',//使用权组
		                 AppCode: props.getUrlParam('src_appcode'),
		                 TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
		        		};
		    }
		}
		//合同类型
        if (item.attrcode == 'ctrantypeid') {
	            item.queryCondition = () => {
	                return { parentbilltype: 'FCT2' }; // 根据单据类型过滤
	            }
	     } 
		return item;
	})
	// 设置固定宽度 撑开子表
	meta[that.headId].items.map((item) => {
		item.width = 120;
		return item;
	});
	meta[that.bodyId].items.map((item) => {
		item.width = 120;
		return item;
	});
	return meta;
}
