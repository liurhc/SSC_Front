//收款
/**
 * 查询区域
 */
export const ar_searchId = '200400ARM';

/**
 * 表头区域
 */
export const ar_headId = 'fct_ar';

/**
 * 表体区域
 */
export const ar_bbodyId = 'fct_ar_b';
export const ar_planbodyId = 'fct_ar_plan';
/**
 * 页面编码
 */
export const ar_pageId = '200400ARM_transfer';

/**
 * 查询模板主键
 */
export const ar_oid = '1002Z3100000000091N4';

/**
 * 小应用ID
 */
export const ar_appcode = '200400ARM';

export const ar_billType = 'FCT2';
/**
 * 单页应用缓存主键名字
 */
export const ar_pkname  = 'pk_fct_ar';

//付款
/**
 * 查询区域
 */
export const ap_searchId = '200401APM';

/**
 * 表头区域
 */
export const ap_headId = 'fct_ap';

/**
 * 表体区域
 */
export const ap_bbodyId = 'fct_ap_b';
export const ap_planbodyId = 'fct_ap_plan';
/**
 * 页面编码
 */
export const ap_pageId = '200401APM_transfer';

/**
 * 查询模板主键
 */
export const ap_oid = '1002Z3100000000091MF';

/**
 * 小应用ID
 */
export const ap_appcode = '200401APM';

export const ap_billType = 'FCT1';

/**
* 单页应用缓存
*/
export const fctdatasource= 'ncc.transferDataSource';//命名规范为："领域名.模块名.节点名.自定义名"。
/**
 * 单页应用缓存主键名字
 */
export const ap_pkname  = 'pk_fct_ap';
/**
 * 多语文件编码
 */
export const multiLangCode = '200400ARM';