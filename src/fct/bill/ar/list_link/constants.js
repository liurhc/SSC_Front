/**
 * 小应用主键
 */
export const appcode = '200400ARM';
/**
 * 页面编码
 */
export const pagecode = '200400ARM_FCT2-01_list';
/**
 * 表体区域
 */
export const tableId = 'fct_ar';
/**
 * 单据类型
 */
export const billtype = 'FCT2';
/**
 * 功能节点编码
 */
export const funcode = '200400ARM';
/**
 * 节点编码
 */
export const nodekey = 'FCT2-01';
/**
 * 多语文件编码
 */
export const multiLangCode = '200400ARM';
/**
 * 页面表格类型
 */
export const tableTypeObj = 'simpleTable';