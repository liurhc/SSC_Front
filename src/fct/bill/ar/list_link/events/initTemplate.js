import {searchId,pagecode,tableId} from '../constants';

export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pagecode, //页面id
			appcode: props.getSearchParam('c') //小应用编码
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(that,props, meta);
					props.meta.setMeta(meta);
				}
			}
		}
	);
}
function seperateDate(date) {
	if (typeof date !== 'string') return;
	let result = date.split(' ') && date.split(' ')[0];
	return result;
}
function modifierMeta(that,props, meta) {
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		item.renderStatus = 'browse';
		if (item.attrcode == 'vbillcode') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							props.openTo('/fct/bill/ar/card_link/index.html', {
								status: 'browse',
								id: record.pk_fct_ar.value,
								blatest:record.blatest.value.toString(),
								appcode:props.getSearchParam('c')
							});
						}}
					>
						{record.vbillcode.value}
					</a>
				);
			};
		}
		return item;
	}); 
	return meta;
}
