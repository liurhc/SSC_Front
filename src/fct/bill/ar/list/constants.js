/**
 * 小应用主键
 */
export const appcode = '200400ARM';

/**
 * 页面编码
 */
export const pagecodeCard = '200400ARM_FCT2-01_card';
/**
 * 页面编码
 */
export const pagecode = '200400ARM_FCT2-01_list';
/**
 * 查询模板主键
 */
export const oid = '0001Z31000000001LX2R';
/**
 * 查询区域
 */
export const searchId = '200400ARM';
/**
 * 表体区域
 */
export const tableId = 'fct_ar';
/**
 * 单据类型
 */
export const billtype = 'FCT2';
/**
 * 功能节点编码
 */
export const funcode = '200400ARM';
/**
 * 节点编码
 */
export const nodekey = 'FCT2-01';

/**
 * 单页应用缓存
 */
export const dataSource  = 'fct.bill.ar.200400ARM';
/**
 * 单页变更历史应用缓存
 */
export const dataSourceHistory  = 'fct.bill.ar.200400ARM.history';
/**
 * 单页应用缓存主键名字
 */
export const pkname  = 'pk_fct_ar';
/**
 * 多语文件编码
 */
export const multiLangCode = '200400ARM';
/**
 * 页面表格类型
 */
export const tableTypeObj = 'simpleTable';