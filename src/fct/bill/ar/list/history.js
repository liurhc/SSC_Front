import React, { Component } from 'react';
import { createPage,ajax,base,cardCache,getMultiLang} from 'nc-lightapp-front';
import {initTemplate,onSelectedFn,doubleClick,pageInfoClick} from './events';
const {NCAffix} = base;
import {multiLangCode,pagecode,appcode,tableId,dataSourceHistory,pkname} from './constants';
import './index.less'

//引入缓存---start
let {setDefData } = cardCache;
//引入缓存---end
/**
 * author songjqk
 *
 */
class HistoryTable extends Component {
    constructor(props){
        super(props);
        this.state={
            billtype:null,
            json:{}
        }
        this.pks;
    }
    componentDidMount() {
        //卡片页面变更历史跳入
        if (this.props.getUrlParam('status') == 'modifyhistory') {
            //设置返回按钮状态
            this.props.BillHeadInfo.setBillHeadInfoVisible({
                showBackBtn: true //控制显示返回按钮: true为显示,false为隐藏 ---非必传
            });
            let _this = this;
            let pk = _this.props.getUrlParam('id');
            let pageInfo = _this.props.table.getTablePageInfo(tableId);
            let data = {
                pageid: pagecode,
                pk:pk,
                pageInfo: pageInfo
            };
            ajax({
                url: '/nccloud/fct/ar/modifyhistorylist.do',
                data: data,
                success: function(res) {
                    let { success, data } = res;
                    if(data){
                        _this.props.table.setAllTableData(tableId,res.data[tableId]);
                            }else{
        
                                let nulldata = {
                                        rows:[]
                                    }
                                    _this.props.table.setAllTableData(tableId,nulldata);
                            }
                            //设置缓存
                            setDefData(tableId, dataSourceHistory, data);
                }
            });  
        }
    }
    componentWillMount(){
        let  callback= (json) =>{
            this.setState({json:json},()=>{
                initTemplate.call(this, this.props);
           })
       }
       getMultiLang({moduleId: multiLangCode, currentLocale: 'simpchn',domainName: 'fct',callback})
    }
    handleClick= () => {
        //来源于列表
        this.props.pushTo(
            '/list', 
            {appcode:this.props.getSearchParam('c')
            })
        
    }
    render() {
        const {table} = this.props;
        const { createSimpleTable } = table;
        const { createBillHeadInfo } = this.props.BillHeadInfo;

        /* 按钮适配 第二步: 从 props.button中取到 createButtonApp 方法用来创建按钮组*/
        return (
            <div className="nc-bill-list">
                <NCAffix>
                    <div className="nc-bill-header-area">
                        <div>
                        {
                            createBillHeadInfo(
                                {
                                    backBtnClick:()=>{              
                                        this.handleClick(); //返回按钮的点击事件
                                    }
                                }
                        )}				
                        </div>
                        <div className="header-title-search-area">
                             <h2 className="title-search-detail">{this.state.json['200400ARM-000063']}</h2>{/* 国际化处理： 变更历史*/}
                        </div>
                    </div>
                </NCAffix>
                {/* 列表区 */}
                <div className="nc-bill-table-area ar-table">
                    {createSimpleTable(tableId, {
                        dataSource: dataSourceHistory,
                        pkname: pkname,
                        handlePageInfoChange: pageInfoClick.bind(this),
                        onRowDoubleClick: doubleClick.bind(this),
                        onSelected:onSelectedFn.bind(this),
                        onSelectedAll:onSelectedFn.bind(this),
                        showCheck:true,
                        showIndex:true
                    })}
                </div>
            </div>
        )
    }
}
HistoryTable = createPage({
    billinfo:{
        billtype: 'grid',
        pagecode: pagecode,
        bodycode: tableId
    },
})(HistoryTable);


export default HistoryTable;
