import { createPage, ajax, base, toast,high} from 'nc-lightapp-front';
import toggleShow from './toggleShow';
import loadPageValue from './loadPageValue';
import { signinfo,pkname,formId, tableId1, tableId2, tableId3, tableId4, tableId5, tableId6, pageId, appcode,dataSource } from '../constants';
export default function(props) {
    let that = this;
    if (props.getUrlParam('status') != 'add') {
        let data = { pk: props.getUrlParam('id'), pageid: pageId,status:props.getUrlParam('status') };
        if(props.getUrlParam('id') == undefined)
        {
            toggleShow(that, props);
            return;
        }
        var url = '/nccloud/fct/ar/querycard.do';
        let cardData;
        var status = 'browse';
        if (props.getUrlParam('status') == 'copy') {
            url = '/nccloud/fct/ar/copycard.do';
            status = 'edit';
        } else if (props.getUrlParam('status') == 'edit') {
            status = 'edit';
        } else if (props.getUrlParam('status') == 'modify') {
            //变更进入 跳转页面
            url = '/nccloud/fct/ar/modify.do';
            status = 'edit';
        } else if (props.getUrlParam('blatest') == 'false') {
            //变更历史进入 跳转页面
            url = '/nccloud/fct/ar/modifyhistorycard.do';
        } 
        ajax({
            url: url,
            data: data,
            async:false,//同步
            success: (res) => {
                    if (res.formulamsg && res.formulamsg instanceof Array && res.formulamsg.length > 0) {
                        props.dealFormulamsg(
                            res.formulamsg,  //参数一：返回的公式对象
                            {                //参数二：界面使用的表格类型
                                "table1":tableTypeObj
                            }
                        );
                    }
                loadPageValue(res.data,props);
                var pk_fct_ar = props.form.getFormItemsValue(formId,"pk_fct_ar").value;
                props.setUrlParam({status:status});
                toggleShow(that, props);
            },
	    error: (res) => {
	           toast({ content: res.message, color: 'warning' });
		       props.setUrlParam({status:status});
		       toggleShow(that, props);
	    }
        });
        
    } else {
        toggleShow(that, props);
    }
}