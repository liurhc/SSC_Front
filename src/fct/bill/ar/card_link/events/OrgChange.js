import { formId, tableId1, tableId2, tableId3, tableId4, tableId5, tableId6, pageId, oid, appcode } from '../constants';
import {ajax } from 'nc-lightapp-front';
import loadPageValue from './loadPageValue';
import toggleShow from './toggleShow';
export default function(props,pk_org) {
	let that = this;
	if (pk_org == undefined || pk_org == '' || pk_org == null) {
		props.initMetaByPkorg();
		props.form.EmptyAllFormValue(formId);
        props.cardTable.setMulTablesData({[tableId1]: { rows: [] },[tableId2]: { rows: [] },[tableId3]: { rows: [] },[tableId4]: { rows: [] },[tableId5]: { rows: [] },[tableId6]: { rows: [] }});
		toggleShow(that, props);
	} else {
		props.resMetaAfterPkorgEdit();
		let data = {
			pageid: pageId,
			pk_org: pk_org//财务组织
		};
		props.form.EmptyAllFormValue(formId);
        props.cardTable.setMulTablesData({[tableId1]: { rows: [] },[tableId2]: { rows: [] },[tableId3]: { rows: [] },[tableId4]: { rows: [] },[tableId5]: { rows: [] },[tableId6]: { rows: [] }});
		ajax({
			url: '/nccloud/fct/ar/mainorgchg.do',
			data: data,
			success: (res) => {
				loadPageValue(res.data, props);
				//设置表单、表格字段编辑状态
				props.form.setFormItemsDisabled(formId, { earlysign: 'N' });
				let corigcurrencyid = props.form.getFormItemsValue(formId, 'corigcurrencyid').display;
				if (corigcurrencyid === that.state.json['200400ARM-000000']) {/* 国际化处理： 人民币*/
					//币种为人民币时，折本汇率不可修改
					props.form.setFormItemsDisabled(formId, { nexchangerate: true });
				} else {
					//币种为人民币时，折本汇率不可修改
					props.form.setFormItemsDisabled(formId, { nexchangerate: false });
				}
				toggleShow(that, props);
			}
		});
	}
}
