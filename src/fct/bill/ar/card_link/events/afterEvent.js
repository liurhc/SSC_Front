import { ajax, toast } from 'nc-lightapp-front';
import toggleShow from './toggleShow';
import { formId, tableId1, tableId2, tableId3, tableId4, tableId5, tableId6, pageId, oid, appcode } from '../constants';
import loadPageValue from './loadPageValue';
import OrgChange from './OrgChange';
import { calculateBodyMoney} from '../../../../public/components/pubUtils/billPubUtil.js';
export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
	let that = this;
	if (key === 'pk_org') {
		//变更财务组织调用事件 
		var pk_org = value == undefined || value == null ? '':value.value;
		OrgChange.call(that,props,pk_org); 
	} else if (moduleId == formId) {
		//表头编辑后事件
		let data = {
			pageId: pageId,
			event: props.createHeadAfterEventData(
				pageId,
				formId,
				[ tableId1, tableId2, tableId3, tableId4, tableId5, tableId6 ],
				moduleId,
				key,
				value
			),
			uiState: that.props.getUrlParam('status')
		};
		ajax({
			url: '/nccloud/fct/ar/cardheadafteredit.do',
			data: data,
			success: (res) => {
				loadPageValue(res.data, props);
				let corigcurrencyid = props.form.getFormItemsValue(moduleId, 'corigcurrencyid').display;
				if(corigcurrencyid === that.state.json['200400ARM-000000'])/* 国际化处理： 人民币*/
				{
					//币种为人民币时，折本汇率不可修改
					props.form.setFormItemsDisabled(formId, {'nexchangerate': true });
		
				}else
				{
					//币种为人民币时，折本汇率不可修改
					props.form.setFormItemsDisabled(formId, {'nexchangerate': false });
				}
				that.setState({
					convmode:res.data.userjson	
				})
				successCallback(props, key, res.data.userjson, i,value,moduleId);
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
				errorCallback(props, key);
			}
		});
		toggleShow(that, props);
	}
	else {
		let strUsePlanRate = props.form.getFormItemsValue(formId,'mountcalculation');
		if(key == 'planmoney')
		{
			if(strUsePlanRate.value == '0')
			{
			  //此处不好用
			  props.cardTable.setValByKeyAndIndex(moduleId,i,'planmoney',{value: changedrows[0]['oldvalue'].value});
			}
		}
		else if(key == 'norigtaxmny' || key == 'ntaxmny' || key == 'planrate')
		{
			if(strUsePlanRate.value == '0')
			{
				calculateBodyMoney(props,moduleId,key,that);
			}
		// }
		// else
		// {
			//表体编辑后事件
			let data = {
				rowindex: i,
				pageId: pageId,
				event: props.createBodyAfterEventData(
					pageId,
					formId,
					[ tableId1, tableId2, tableId3, tableId4, tableId5, tableId6 ],
					moduleId,
					key,
					changedrows
				),
				uiState: that.props.getUrlParam('status')
			};
			ajax({
				url: '/nccloud/fct/ar/cardbodyafteredit.do',
				data: data,
				success: (res) => {
					loadPageValue(res.data, props);
					successCallback(props,key,res.data.userjson,i,value,moduleId);
				},
				error: (res) => {
					toast({ color: 'danger', content: res.message });
					errorCallback(props,key);
				}
			});
			toggleShow(that, props);
		}
	}
}
/**
 * 后台处理失败，返回默认值
 */
export const errorCallback = (props, key) => {
	//合同签订日期
	if (key === 'subscribedate') {
		//后台处理失败，签字盖章日期置空
		props.form.setFormItemsValue(formId, { subscribedate: { value: null, display: null } });
	}
	//计划生效日期
	if (key === 'valdate') {
		//后台处理失败，签字盖章日期置空
		props.form.setFormItemsValue(formId, { valdate: { value: null, display: null } });
	}
	//计划终止日期
	if (key === 'invallidate') {
		//后台处理失败，签字盖章日期置空
		props.form.setFormItemsValue(formId, { invallidate: { value: null, display: null } });
	}
	//账期天数
	if (key === 'accountdate') {
		//后台处理失败，计划到期日置空
		props.form.setFormItemsValue(formId, { invallidate: { value: null, display: null } });
	}
};
/**
 * 后台处理成功处理
 */
export const successCallback = (props, key, userjson, i,value,moduleId) => {
	//合同类型
	if (key === 'ctrantypeid') {
		if (userjson == '1') {
			props.cardTable.setColEditableByKey(tableId1, 'pk_material', true);
			props.cardTable.setColEditableByKey(tableId1, 'castunitid', true);
		} else if (userjson == '2') {
			props.cardTable.setColEditableByKey(tableId1, 'pk_material', true);
			props.cardTable.setColEditableByKey(tableId1, 'materialname', true);
			props.cardTable.setColEditableByKey(tableId1, 'pk_marbasclass', true);
			props.cardTable.setColEditableByKey(tableId1, 'marbasclassname', true);
			props.cardTable.setColEditableByKey(tableId1, 'nastnum', true);
			props.cardTable.setColEditableByKey(tableId1, 'castunitid', true);
			props.cardTable.setColEditableByKey(tableId1, 'nnum', true);
			props.cardTable.setColEditableByKey(tableId1, 'nqtunitnum', true);
			props.cardTable.setColEditableByKey(tableId1, 'cunitid', true);
			props.cardTable.setColEditableByKey(tableId1, 'cqtunitid', true);
		}
	}
	if (key === 'personnelid' && i.values != undefined) {
		props.form.setFormItemsValue(formId, {
			depid_v: { value: i.values.pk_dept.value, display: i.values.deptname.value }
		});
	}
};
