import { InsertLine, DelLine, Open, CopyLine, BatchCopy,push } from '../../../../public/components/pubUtils/billPubUtil.js';
export default  function tableButtonClick (props, key, text, record, index,tableId){    
    let that = this;
    switch (key) {
          //行 展开
        case 'openline':
          Open(props, tableId, index, record, 'edit');
          break;
        //行 删除
        case 'deleteline':
            DelLine(props, tableId, index,key,that);
        break;
        case 'insertline':
            InsertLine(props, tableId, index);
            break;
         //行 复制
        case 'copyline':
            CopyLine(props, tableId, index,key,that);
            break;    
        case 'switchview':
            props.cardTable.toggleRowView(tableId, record);
            break;
        case 'push':
            push(props,record,'FCT2');
            break;
             //行 粘贴至此
        case 'copyatline':
            BatchCopy(props, tableId, '1',key,that,index);
            if (tableId == 'pk_fct_ar_b') {
                that.state.copyflag1=true 
                toggleShow(that,props);
              } else if (tableId == 'pk_fct_ar_term') {
                that.state.copyflag2=true 
                toggleShow(that,props);
              }else if (tableId == 'pk_fct_ar_plan') {
                that.state.copyflag3=true 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ar_memora') {
                that.state.copyflag4=true 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ar_change') {
                that.state.copyflag5=true 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ar_exec') {
                that.state.copyflag6=true 
                toggleShow(that,props);              
              }
        break;
        default:
            console.log(key);
            break;
    }
};
