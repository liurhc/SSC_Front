import { AddLine,BatchDelLine,BatchCopy} from '../../../../public/components/pubUtils/billPubUtil.js';
import toggleShow from './toggleShow';
import { ajax, toast ,cacheTools} from 'nc-lightapp-front';
export default  function tableButtonClick1 (props,key,tableId){
    let that = this;
    switch (key) {
        // 肩部删除
        case 'deletelines1': case 'deletelines2': case 'deletelines3':
        case 'deletelines4': case 'deletelines5': case 'deletelines6':
            BatchDelLine(props,tableId,key,that);
            break;
        //肩部添加    
        case 'addlines1':case 'addlines2':case 'addlines3':case 'addlines4':
        case 'addlines5':case 'addlines6':
            let tableNum = props.cardTable.getNumberOfRows(tableId);
            AddLine(props,tableId,tableNum,key,that);
            break;
        //肩部 复制
        case 'copylines1':case 'copylines2':case 'copylines3':case 'copylines4':
        case 'copylines5':case 'copylines6':
            let selectRows = props.cardTable.getCheckedRows(tableId);
            if (selectRows == null || selectRows.length == 0) {
                toast({
                'color': 'warning',
                'content': that.state.json['200400ARM-000023']/* 国际化处理： 未选中要复制的行*/
                });
                return false;
            }
            if (tableId == 'pk_fct_ar_b') {
                that.state.copyflag1=true 
                toggleShow(that,props);
              } else if (tableId == 'pk_fct_ar_term') {
                that.state.copyflag2=true 
                toggleShow(that,props);
              }else if (tableId == 'pk_fct_ar_plan') {
                that.state.copyflag3=true 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ar_memora') {
                that.state.copyflag4=true 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ar_change') {
                that.state.copyflag5=true 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ar_exec') {
                that.state.copyflag6=true 
                toggleShow(that,props);              
              }
              cacheTools.set('copyData', selectRows);
            break;
    //肩部 粘贴至末行
    case 'pastetoendlines1':case 'pastetoendlines2':case 'pastetoendlines3':
    case 'pastetoendlines4':case 'pastetoendlines5':case 'pastetoendlines6':
        BatchCopy(props,tableId,'1',undefined,key,that);
        if (tableId == 'pk_fct_ar_b') {
            that.state.copyflag1=false;
            toggleShow(that,props);
        } else if (tableId == 'pk_fct_ar_term') {
            that.state.copyflag2=false; 
            toggleShow(that,props);
        }else if (tableId == 'pk_fct_ar_plan') {
            that.state.copyflag3=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ar_memora') {
            that.state.copyflag4=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ar_change') {
            that.state.copyflag5=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ar_exec') {
            that.state.copyflag6=false; 
            toggleShow(that,props);              
        }
      break;
    //肩部 取消
    case 'cancellines1':case 'cancellines2':case 'cancellines3':case 'cancellines4':
    case 'cancellines5':case 'cancellines6':
        if (tableId == 'pk_fct_ar_b') {
            that.state.copyflag1=false;
            toggleShow(that,props);
        } else if (tableId == 'pk_fct_ar_term') {
            that.state.copyflag2=false; 
            toggleShow(that,props);
        }else if (tableId == 'pk_fct_ar_plan') {
            that.state.copyflag3=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ar_memora') {
            that.state.copyflag4=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ar_change') {
            that.state.copyflag5=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ar_exec') {
            that.state.copyflag6=false; 
            toggleShow(that,props);              
        }
      break;    
        default:
            console.log(key);
            break;
    }
};
