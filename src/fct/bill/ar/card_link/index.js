//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,high,getMultiLang} from 'nc-lightapp-front';
import {buttonClick,queryCard,initTemplate,toggleShow,afterEvent,beforeEvent,pageInfoClick,tableButtonClick,tableButtonClick1,loadPageValue} from './events';
import { tableTypeObj,multiLangCode,signinfo,mngainfo,auditinfo,pkname,formId, tableId1, tableId2, tableId3, tableId4, tableId5, tableId6, pageId, appcode,dataSource } from './constants';
const { BillTrack, NCUploader,ApproveDetail } = high;
let { NCFormControl,NCBackBtn,NCAnchor, NCScrollLink, NCScrollElement, NCAffix,NCInput } = base;
import './index.less'

import { getTemp } from '../../../public/components/pubUtils/billPubUtil.js';
class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			copyflag1: false,
			copyflag2: false,
			copyflag3: false,
			copyflag4: false,
			copyflag5: false,
			copyflag6: false,
			index:null,//控制当前行展开收起
			searchValue: '',
			checkValue: null,
			checkId: null,
			queryData: [],
			trackshow: false,
			showUploader: false,
			approveshow: false,
			billtype: null,
			showApproveDetail: false,//审批详情的控制
			json:{},
			convmode:'0',//折算模式
			pk_org:null,
			onDoActionValue:'',//执行原因
			id:'',
			url:'',
			temp:''
		};
		this.pks;
	}
	componentDidMount() {
		//查询单据详情
		queryCard.call(this,this.props);
	}
	componentWillMount(){
        let  callback= (json) =>{
            this.setState({json:json},()=>{
                initTemplate.call(this, this.props);
           })
       }
       getMultiLang({moduleId: multiLangCode, currentLocale: 'simpchn',domainName: 'fct',callback})
	}
	//打开审批详情
	openApprove = (props)=>{
		let record = props.form.getAllFormValue(formId);
		let pk = record.rows[0].values.pk_fct_ar;
		let billtype = record.rows[0].values.vtrantypecode;
		this.setState({
			showApproveDetail: true,
			billid:pk.value,
			billtype:billtype.value
		})
	}
	//审批详情模态框控制
	closeApprove = () => {
		this.setState({
			showApproveDetail: false
		})
	}
	beforeUpload(billId, fullPath, file, fileList) {
		const isLt20M = file.size / 1024 / 1024 < 20;
		if (!isLt20M) {
			toast({ content: `${this.state.json['200400ARM-000025']}20M！`, color: 'warning' });/* 国际化处理： 上传大小小于*/
		}
		return isLt20M;
		// 备注： return false 不执行上传  return true 执行上传
	}
	//关闭
	onHide = () => {
		this.setState({
			showUploader: false
		})
	}
	//附件
	openfile = (props) => {
		let record = props.form.getAllFormValue(formId);
		let pk = record.rows[0].values.pk_fct_ar;
		let vbillcode = record.rows[0].values.vbillcode;

		this.setState({
			checkId: pk.value,
			checkValue: vbillcode.value
		}, () => {
			if (this.state.showUploader == false) {
				this.setState({ showUploader: true });
			} else {
				this.setState({ showUploader: false });
			}
		});
	};
	//保存单据
	saveBill = () => {
		let url = '/nccloud/fct/ar/insertcard.do'; //新增保存
		if (this.props.getUrlParam('status') === 'edit') {
			url = '/nccloud/fct/ar/updatecard.do'; //修改保存
		}
		if (this.props.getUrlParam('status') === 'modify') {
			url = '/nccloud/fct/ar/modifysave.do'; //变更保存
		}

		let CardData = this.props.createExtCardData(pageId, formId, [
			tableId1,
			tableId2,
			tableId3,
			tableId4,
			tableId5,
			tableId6
		]);

		let callback = ()=>{
		ajax({
			url: url,
			data: CardData,
			async:false, 
			success: (res) => {
				let pk_fct_ar = null;
				if (res.success) {
					toast({ color: 'success', content: this.state.json['200400ARM-000024'] });/* 国际化处理： 保存成功*/
					loadPageValue(res.data,this.props);
					pk_fct_ar = this.props.form.getFormItemsValue(formId, 'pk_fct_ar').value; 
					this.props.setUrlParam({status:'browse',id:pk_fct_ar});
					toggleShow(this, this.props);
					}
				}
			});
		}
		//保存前数据校验
		this.props.validateToSave(CardData, callback , tableTypeObj , 'extcard');
	};

	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button';
		} else {
			return 'secondary - button';
		}
	};

	//获取列表肩部信息
	getTableHead = (tableId) => {
		let { createButton } = this.props.button;
		let that = this;
		let area;
		if(tableId == tableId5 || tableId == tableId6)
		{}
		else{
			if (tableId == tableId1) {
				area = 'card_body_outer1';
			} else if (tableId == tableId2) {
				area = 'card_body_outer2';
			} else if (tableId == tableId3) {
				area = 'card_body_outer3';
			} else if (tableId == tableId4) {
				area = 'card_body_outer4';
			}
			return (
				<div className="shoulder-definition-area">
				{
					this.props.getUrlParam('status') != 'browse' ?
					this.props.button.createButtonApp({ area: area, onButtonClick: (props, key) => tableButtonClick1.call(that, props, key, tableId) })
					:''
				}
				</div>
			);
		}
	};

	/**
 	* 执行原因
 	*/
	 onDoAction = () => {
		let {onDoActionValue,temp}=this.state;
		return(
			<div className="line-item">
				<div className="item-title">{this.state.json['200400ARM-000072']/* 国际化处理： 原因*/}</div>
				<div className="item-content">
					<NCInput
						defaultValue={onDoActionValue}
						placeholder={this.state.json['200400ARM-000070']/* 国际化处理： 请输入*/+temp+this.state.json['200400ARM-000072']/* 国际化处理：原因*/+this.state.json['200400ARM-000074']/* 国际化处理： 上限120字*/}
						maxlength="120"
						onChange={(value)=>{
							this.setState({
								onDoActionValue:value
							})
						}}
					/>
				</div>
			</div>
		)
	}
	onDoTrueAction=(flage)=>{
		let that=this;
		let {onDoActionValue,id,url,temp}=that.state;
		let printData = that.props.createExtCardData(pageId, formId, [tableId1, tableId2, tableId3, tableId4, tableId5, tableId6])
		let pk_data = printData.head[formId].rows[0].values.pk_fct_ar.value; 
		let index1 = [];
		index1.push(pk_data);
		let pageids = [];
		pageids.push(pageId);
		if(!flage){
			onDoActionValue=null;
		}
		let data = {
			"pk":index1,
			"pageid":pageids,
			'onDoActionValue':onDoActionValue
		}
		this.setState({
			onDoActionValue:''
		})
		ajax({
			url: url,
			data: data,
			success: (res) => {
				if (res) {
					toast({ color: 'success', content: temp+that.state.json['200400ARM-000013'] });/* 国际化处理： 成功*/
					loadPageValue(res.data,that.props);
					toggleShow(that,that.props);
					updateCache(pkname,pk_data,res.data,formId,dataSource);
				}
			}
		});
	}
	render() {
		let { cardTable, form, button, modal, cardPagination } = this.props;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createButton, createButtonApp, createOprationButton } = button;
		let { createModal } = modal;
		let { showUploader, target } = this.state;
		return (
			<div className="nc-bill-extCard">
				<NCAnchor>
					<NCScrollLink to= 'forminfo' spy={true} smooth={true} duration={300} offset={-100}>
						<p>{this.state.json['200400ARM-000026']}</p>{/* 国际化处理： 收款合同*/}
					</NCScrollLink>
					<NCScrollLink to='basicContract' spy={true} smooth={true} duration={300} offset={-100}>
						<p>{this.state.json['200400ARM-000027']}</p>{/* 国际化处理： 合同基本*/}
					</NCScrollLink>
					<NCScrollLink to='contractTerms' spy={true} smooth={true} duration={300} offset={-100}>
						<p>{this.state.json['200400ARM-000028']}</p>{/* 国际化处理： 合同条款*/}
					</NCScrollLink>
					<NCScrollLink to='recPlan' spy={true} smooth={true} duration={300} offset={-100}>
						<p>{this.state.json['200400ARM-000029']}</p>{/* 国际化处理： 收款计划*/}
					</NCScrollLink>
					<NCScrollLink to='bigContractIncident' spy={true} smooth={true} duration={300} offset={-100}>
						<p>{this.state.json['200400ARM-000030']}</p>{/* 国际化处理： 合同大事件*/}
					</NCScrollLink>
					<NCScrollLink to='changeHistory' spy={true} smooth={true} duration={300} offset={-100}>
						<p>{this.state.json['200400ARM-000031']}</p>{/* 国际化处理： 变更历史*/}
					</NCScrollLink>
					<NCScrollLink to='executionProcess' spy={true} smooth={true} duration={300} offset={-100}>
						<p>{this.state.json['200400ARM-000032']}</p>{/* 国际化处理： 执行过程*/}
					</NCScrollLink>
				</NCAnchor>
				<div className="nc-bill-top-area">
					{/*新增div*/}
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
							{
							this.props.getUrlParam('blatest') == 'false' ?
								<h2 className="title-search-detail">{this.state.json['200400ARM-000063']}</h2>/* 国际化处理： 变更历史*/
								:<h2 className="title-search-detail">{this.state.json['200400ARM-000026']}</h2>/* 国际化处理： 收款合同*/
							}
							</div>
							<div className="header-button-area">
								{/* 按钮适配 第三步:在页面的 dom 结构中创建按钮组，传入显示的区域，绑定按钮事件*/}
								{createButtonApp({
									area: 'page_header',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
								{/* 单据追溯 */}
								<BillTrack
									show={this.state.trackshow}
									close={() => {
										this.setState({ trackshow: false });
									}}
									pk={this.state.checkId} //单据id
									type="FCT2" //单据类型
								/>
								{/* 附件*/}
								{/* 这里是附件上传组件的使用，需要传入三个参数 */}
								{showUploader && (
									<NCUploader
										billId={this.state.checkId}
										billNo={this.state.checkValue}
										beforeUpload={this.beforeUpload}
										onHide={this.onHide}
									/>
								)}
								{/* 审批详情*/}
								<ApproveDetail
								show={this.state.showApproveDetail}
                                close={this.closeApprove}
                                billtype={this.state.billtype}
                                billid={this.state.billid}
								/>
							</div>
						</div>
					</NCAffix>
					<NCScrollElement name="forminfo">
						<div className="nc-bill-form-area">
							{createForm(formId, {
								expandArr: [formId,signinfo,mngainfo,auditinfo],
								onAfterEvent: afterEvent.bind(this),
								onBeforeEvent:beforeEvent.bind(this)
							})}
						</div>
					</NCScrollElement>
				</div>
				<div className="nc-bill-bottom-area">
					{/*新增div*/}
					<NCScrollElement name="basicContract" className='extCard-table'>
						<div className="nc-bill-table-area">
							{createCardTable(tableId1, {
								tableHead: this.getTableHead.bind(this, tableId1),
								modelSave: this.saveBill,
								onAfterEvent: afterEvent.bind(this),
								showCheck: true,
								showIndex: true
							})}
						</div>
					</NCScrollElement>
					<NCScrollElement name="contractTerms" className='ar-table'>
						<div className="nc-bill-table-area">
							{createCardTable(tableId2, {
								tableHead: this.getTableHead.bind(this, tableId2),
								modelSave: this.saveBill,
								onAfterEvent: afterEvent.bind(this),
								showCheck: true,
								showIndex: true
							})}
						</div>
					</NCScrollElement>
					<NCScrollElement name="recPlan" className='ar-table'>
						<div className="nc-bill-table-area">
							{createCardTable(tableId3, {
								tableHead: this.getTableHead.bind(this, tableId3),
								modelSave: this.saveBill,
								onAfterEvent: afterEvent.bind(this),
								showCheck: true,
								showIndex: true
							})}
						</div>
					</NCScrollElement>
					<NCScrollElement name="bigContractIncident" className='ar-table'>
						<div className="nc-bill-table-area">
							{createCardTable(tableId4, {
								tableHead: this.getTableHead.bind(this, tableId4),
								modelSave: this.saveBill,
								onAfterEvent: afterEvent.bind(this),
								showCheck: true,
								showIndex: true
							})}
						</div>
					</NCScrollElement>
					<NCScrollElement name="changeHistory" className='ar-table'>
						<div className="nc-bill-table-area">
							{createCardTable(tableId5, {
								tableHead: this.getTableHead.bind(this, tableId5),
								modelSave: this.saveBill,
								onAfterEvent: afterEvent.bind(this),
								showCheck: true,
								showIndex: true
							})}
						</div>
					</NCScrollElement>
					<NCScrollElement name="executionProcess" className='ar-table'>
						<div className="nc-bill-table-area">
							{createCardTable(tableId6, {
								tableHead: this.getTableHead.bind(this, tableId6),
								modelSave: this.saveBill,
								onAfterEvent: afterEvent.bind(this),
								showCheck: true,
								showIndex: true
							})}
						</div>
					</NCScrollElement>
				</div>
				<div>
				{createModal('onDoAction', {
					title: this.state.temp, // 弹框表头信息
					content: this.onDoAction(this), //弹框内容，可以是字符串或dom
					beSureBtnClick: this.onDoTrueAction.bind(this,true), //点击确定按钮事件
					cancelBtnClick: this.onDoTrueAction.bind(this,false),
					userControl: false, // 点确定按钮后，是否自动取消弹出框.true:手动关。false:自动关
					className: 'senior', //  模态框大小 sm/lg/xlg
					rightBtnName: this.state.json['200400ARM-000060']/* 国际化处理： 取消*/, //左侧按钮名称,默认取消
					leftBtnName: this.state.json['200400ARM-000073']/* 国际化处理： 确定*/ //右侧按钮名称， 默认确定
				})}
				</div>
			</div>
		);
	}
}
Card = createPage({
	//initTemplate: initTemplate,
	billinfo:{
        billtype: 'extcard', 
        pagecode: pageId, 
        headcode: formId,
        bodycode: [tableId1, tableId2, tableId3, tableId4, tableId5, tableId6]
    },
})(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
