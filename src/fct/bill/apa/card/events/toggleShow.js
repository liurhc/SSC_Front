import { base, ajax } from 'nc-lightapp-front';
import { formId, tableId1,tableId2,tableId3,tableId4,tableId5,tableId6 } from '../constants';
import {  OpencontracAfterEvent} from '../../../../public/components/pubUtils/OpencontracAfterEvent.js';

export default (that,props,executeAutoFocus) => {
    let FieldValue = props.form.getFormItemsValue(formId,'fstatusflag');
    let pkOrg = props.form.getFormItemsValue(formId,'pk_org');
    let fstatusflag;
    if(FieldValue != null && FieldValue !== "undefined" && FieldValue != undefined)
    {
        //新增情况无值，不进入
        fstatusflag = FieldValue.value;
    }
    let buttonHidden = [];//表头隐藏按钮
    let buttonNoEdit = [];//表头不可编辑按钮
    let status = props.getUrlParam('status');
    let flag = status === 'browse' ? false : true;
    let flag1 = flag;
    let status1 = status === 'browse' ? 'browse' : "edit";
    //清空所有按钮不可显示的状态
    clearButtonState(props);
    let id = props.getUrlParam('id');
    if(id === "undefined" || id == undefined)
    {
        flag1 = true;//地址栏不存在id,取消翻页
    }
    else
    {
        let idObj={id:props.getUrlParam('id'),status:1}//status均传1
        props.cardPagination.setCardPaginationId(idObj);

    }
    //表体按钮显示隐藏控制
    let copyflag1 = that.state.copyflag1 || false;
    let copyflag2 = that.state.copyflag2 || false;
    let copyflag3 = that.state.copyflag3 || false;
    let copyflag4 = that.state.copyflag4 || false;
    let copyflag5 = that.state.copyflag5 || false;
    let copyflag6 = that.state.copyflag6 || false;
    //编辑态
    props.button.setButtonVisible(['addlines1', 'deletelines1', 'copylines1'], flag && !copyflag1);
    props.button.setButtonVisible(['pastetoendlines1','cancellines1'], flag && copyflag1);
    props.button.setButtonVisible(['addlines2', 'deletelines2', 'copylines2'], flag && !copyflag2);
    props.button.setButtonVisible(['pastetoendlines2','cancellines2'], flag && copyflag2);
    props.button.setButtonVisible(['addlines3', 'deletelines3', 'copylines3'], flag && !copyflag3);
    props.button.setButtonVisible(['pastetoendlines3','cancellines3'], flag && copyflag3);
    props.button.setButtonVisible(['addlines4', 'deletelines4', 'copylines4'], flag && !copyflag4);
    props.button.setButtonVisible(['pastetoendlines4','cancellines4'], flag && copyflag4);
    props.button.setButtonVisible(['addlines5', 'deletelines5', 'copylines5'], flag && !copyflag5);
    props.button.setButtonVisible(['pastetoendlines5','cancellines5'], flag && copyflag5);
    props.button.setButtonVisible(['addlines6', 'deletelines6', 'copylines6'], flag && !copyflag6);
    props.button.setButtonVisible(['pastetoendlines6','cancellines6'], flag && copyflag6);
   
    //计划页签收款按钮，编辑态不显示
    props.button.setButtonVisible(['pushs'],false);


    if(props.getUrlParam('status') == 'modify')
    {//变更状态
        //平台提供设置完字段编辑性，在定位
        that.props.controlAutoFocus(true);
        // props.form.setFormStatus(formId, "browse");
        //props.form.disabled = true;   signorg
        let CanDisField = ['ctname','valdate','ivallidate','custunit','depid_v','personnelid','deliaddr','pk_payterm'];
        let CanotDisField = ['pk_org','vbillcode','ctrantypeid','subscribedate','pk_customer','corigcurrencyid','nexchangerate','signorg','bankaccount','overrate','organizer','earlysign','openct','mountcalculation'];

        // props.form.setFormItemsDisabled(formId,{[CanDisField,false});
        // props.form.setFormItemsDisabled(formId,{[CanotDisField,false]});
        pkOrgTableButton(pkOrg,props);
        props.form.setFormItemsDisabled(formId,{'ctname':false});
        props.form.setFormItemsDisabled(formId,{'valdate':false});
        props.form.setFormItemsDisabled(formId,{'ivallidate':false});
        props.form.setFormItemsDisabled(formId,{'custunit':false});
        props.form.setFormItemsDisabled(formId,{'depid_v':false});
        props.form.setFormItemsDisabled(formId,{'personnelid':false});
        props.form.setFormItemsDisabled(formId,{'deliaddr':false});
        props.form.setFormItemsDisabled(formId,{'pk_payterm':false});
        props.form.setFormItemsDisabled(formId,{'noriprepaylimitmny':false});
        props.form.setFormItemsDisabled(formId,{'nprepaylimitmny':false});
        props.form.setFormItemsDisabled(formId,{'pk_org':true});
        props.form.setFormItemsDisabled(formId,{'vbillcode':true});
        props.form.setFormItemsDisabled(formId,{'ctrantypeid':true});
        props.form.setFormItemsDisabled(formId,{'subscribedate':true});
        props.form.setFormItemsDisabled(formId,{'pk_customer':true});
        props.form.setFormItemsDisabled(formId,{'corigcurrencyid':true});
        props.form.setFormItemsDisabled(formId,{'nexchangerate':true});
        props.form.setFormItemsDisabled(formId,{'signorg':true});
        props.form.setFormItemsDisabled(formId,{'bankaccount':true});
        props.form.setFormItemsDisabled(formId,{'overrate':true});
        props.form.setFormItemsDisabled(formId,{'organizer':true});
        props.form.setFormItemsDisabled(formId,{'earlysign':true});
        // props.form.setFormItemsDisabled(formId,{'openct':true});
        props.form.setFormItemsDisabled(formId,{'mountcalculation':true});

        props.form.setFormItemsDisabled(formId,{'unconfpaymentori':true});
        props.form.setFormItemsDisabled(formId,{'unconfpaymentorg':true});
        props.form.setFormItemsDisabled(formId,{'unconfpayableori':true});
        props.form.setFormItemsDisabled(formId,{'unconfpayableorg':true});
        props.form.setFormItemsDisabled(formId,{'norikpmny':true})
        if(executeAutoFocus)
        {
            that.props.executeAutoFocus();
        }

    }else{
        //打开因为合同变更不可编辑的字段
        if(pkOrg==null||pkOrg.value == null ||pkOrg.value == '#mainorg#'){
            props.form.setFormItemsDisabled(formId,{'pk_org':false});
        }else{
            props.form.setFormItemsDisabled(formId,{'pk_org':false});
            props.form.setFormItemsDisabled(formId,{'subscribedate':false});
            props.form.setFormItemsDisabled(formId,{'pk_customer':false});
            props.form.setFormItemsDisabled(formId,{'corigcurrencyid':false});
            props.form.setFormItemsDisabled(formId,{'signorg':false});
            props.form.setFormItemsDisabled(formId,{'bankaccount':false});
            props.form.setFormItemsDisabled(formId,{'overrate':false});
            props.form.setFormItemsDisabled(formId,{'organizer':false});
            // props.form.setFormItemsDisabled(formId,{'openct':false});
            props.form.setFormItemsDisabled(formId,{'mountcalculation':false});
        }
    }
    //在掉一遍敞口合同
	OpencontracAfterEvent(that,props);

    props.cardPagination.setCardPaginationVisible('cardPaginationBtn', !flag1);//设置看片翻页的显隐性
    props.form.setFormStatus(formId, status1);
    props.cardTable.setStatus(tableId1, status1);
    props.cardTable.setStatus(tableId2, status1);
    props.cardTable.setStatus(tableId3, status1);
    props.cardTable.setStatus(tableId4, status1);
    props.cardTable.setStatus(tableId5, status1);
    props.cardTable.setStatus(tableId6, status1);
    if(props.getUrlParam('blatest') == 'false' || props.getUrlParam('blatest') == false)
    {
        props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);//设置看片翻页的显隐性
                //若非最新版本，代表为变更历史查询进入，隐藏页面按钮，及设置页面不可编辑
        let buttonAry = [ 'save','savecommit','cancel','add', 'edit','delete', 'drop_commit','drop_approve','drop_doaction','drop_fzgn','drop_yxgl','drop_linkquery','drop_print','drop_importandexport','back','commit','uncommit','copy','drop_more','close','refresh'];					  
        props.button.setButtonsVisible(buttonAry,false);
        return;
    }
    if(props.getUrlParam('status') == 'copy'){
        let corigcurrencyid = props.form.getFormItemsValue(formId, 'corigcurrencyid').display;
        if(corigcurrencyid === that.state.json['200401APM-000000'])/* 国际化处理： 人民币*/
        {
            //币种为人民币时，折本汇率不可修改
            props.form.setFormItemsDisabled(formId, {'nexchangerate': true });

        }else
        {
            //币种为人民币时，折本汇率不可修改
            props.form.setFormItemsDisabled(formId, {'nexchangerate': false });
        }
    }
    //浏览态    
    if(!flag)
    {
        switch (fstatusflag) {
            //自由
            case '0':
                buttonHidden = ['save','savecommit','uncommit','cancel','drop_approve','drop_doaction','modifyhistory','drop_linkquery','close'];
                props.button.setButtonVisible(buttonHidden, false);//隐藏按钮
               break;
            //生效
            case '1':
            buttonHidden = ['save','savecommit','cancel','edit','delete','commit','uncommit','drop_approve','validate','close','modifyhistory'];
            props.button.setButtonVisible(buttonHidden, false);///隐藏按钮
            //计划页签收款按钮，浏览态生效时显示
            props.button.setButtonVisible(['pushs'],true);
             break;
            //审批中
            case '2':
                buttonHidden = ['save','savecommit','cancel','delete','edit','commit','drop_doaction','modifyhistory','billlinkquery','close'];
                props.button.setButtonVisible(buttonHidden, false);///隐藏按钮
            break;
            //审批通过
            case '3':
                buttonHidden = ['save','savecommit','cancel','edit','delete','commit','approve','cancelvalidate','freeze','unfreeze','terminate','unterminate','modify','modifyhistory','billlinkquery','close'];
                props.button.setButtonVisible(buttonHidden, false);//隐藏按钮
            break;
            //审批未通过
            case '4':
                buttonHidden = ['save','savecommit','cancel','edit','delete','commit','drop_doaction','modifyhistory','billlinkquery','close'];
                props.button.setButtonVisible(buttonHidden, false);//显示按钮
            break;
            //冻结
            case '5':
                buttonHidden = ['save','savecommit','cancel','edit','delete','commit','uncommit','drop_approve','validate','cancelvalidate','freeze','terminate','unterminate','modify','close','modifyhistory'];
                props.button.setButtonVisible(buttonHidden, false);//显示按钮
            break;
            //终止
            case '6':
                buttonHidden = ['save','savecommit','cancel','edit','delete','commit','uncommit','drop_approve','validate','cancelvalidate','freeze','unfreeze','terminate','modify','close','modifyhistory'];
                props.button.setButtonVisible(buttonHidden, false);//显示按钮
            break;
            default:
            let buttonAll = ['save','savecommit','cancel','edit','delete','copy','drop_commit'
            ,'commit','uncommit','drop_approve','approve','unapprove','approveinfo','drop_doaction',
            'validate','cancelvalidate','freeze','unfreeze','terminate','unterminate','modify',
            'drop_fzgn','file','modifyhistory', 'drop_yxgl','yxck','yxsm','yxpj','drop_linkquery'
            ,'billlinkquery','drop_print','print','preview','output','close','drop_more'];
            props.button.setButtonVisible(buttonAll,false);//肩部全部隐藏
             break;
         } 
    }
    else
    {
        if(props.getUrlParam('status') == 'modify')
        {
            buttonHidden = ['savecommit','add','edit','delete','copy','commit','uncommit','drop_approve','drop_doaction','modifyhistory','yxpj','drop_linkquery','drop_print','close','refresh','drop_more'];
        }
        else
        {
            pkOrgTableButton(pkOrg,props);
            buttonHidden = ['add','edit','delete','copy','commit','uncommit','drop_approve','drop_doaction','modifyhistory','yxpj','drop_linkquery','drop_print','close','refresh','drop_more'];
        }
        props.button.setButtonVisible(buttonHidden, false);//隐藏按钮
    }
}
/**
 * 每次切换按钮时，去掉上次按钮状态
 */

export const clearButtonState = (props) => {
    let buttonAll = ['save','savecommit','cancel','add','edit','copy','delete','drop_commit'
    ,'commit','uncommit','drop_approve','approve','unapprove','approveinfo','drop_doaction',
    'validate','cancelvalidate','freeze','unfreeze','terminate','unterminate','modify',
    'drop_fzgn','file','modifyhistory', 'drop_yxgl','yxck','yxsm','yxpj','drop_linkquery'
    ,'billlinkquery','drop_print','print','preview','output','close','refresh','drop_more'];
    props.button.setButtonVisible(buttonAll,true);//肩部全部显示

    //表体肩部按钮
    let buttonOprAll =  ['addlines1','deletelines1','copylines1','pastetoendlines1','cancellines1',
                        'addlines2','deletelines2','copylines2','pastetoendlines2','cancellines2',
                        'addlines3','deletelines3','copylines3','pastetoendlines3','cancellines3',
                        'addlines4','deletelines4','copylines4','pastetoendlines4','cancellines4',
                        'addlines5','deletelines5','copylines5','pastetoendlines5','cancellines5',
                        'addlines6','deletelines6','copylines6','pastetoendlines6','cancellines6'];
    props.button.setButtonVisible(buttonOprAll, true);//全部显示
}
/**
 * @author Don
 */
function pkOrgTableButton(pkOrg,props) {
    if(pkOrg==null||pkOrg.value == null ||pkOrg.value == '#mainorg#'){
        return;
    }
    let buttonOprAll =  ['addlines1',
                        'addlines2',
                        'addlines3',
                        'addlines4',
                        'addlines5',,
                        'addlines6'];
        props.button.setButtonDisabled(buttonOprAll,false);
}

function setTableButtonDisable(props){
    let buttonOprAll =  ['addlines1','deletelines1','copylines1',
                        'addlines2','deletelines2','copylines2',
                        'addlines3','deletelines3','copylines3',
                        'addlines4','deletelines4','copylines4',
                        'addlines5','deletelines5','copylines5',
                        'addlines6','deletelines6','copylines6','pushs'];
    props.button.setButtonDisabled(buttonOprAll,true);
}
