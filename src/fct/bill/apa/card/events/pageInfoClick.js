import { ajax,cardCache } from 'nc-lightapp-front';
import loadPageValue from './loadPageValue';
import {pkname,formId,dataSource,pageId} from '../constants';
import toggleShow from './toggleShow';
export default function(props, pks,that) {
	if(that == undefined || that == null || that === "null")
	{
		that = this;
	}
	props.setUrlParam({id:pks});
	if (pks == null || pks === "null" ||  pks == undefined) {
		return;
	}
	let pkArr = [];
	pkArr.push(pks); //后台使用数组解析数据，此处传数组
	let data = {
		allpks: pkArr,
		pageid: pageId
	};
	ajax({
		url: '/nccloud/fct/ap/querycardpage.do',
		data: data,
		success: (res) => {
			if (res.data) {
				loadPageValue(res.data,props);
				toggleShow(that,props);
			}
		}
	});
}
