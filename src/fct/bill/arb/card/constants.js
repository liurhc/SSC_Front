/**
 * 小应用主键
 */
export const appcode = '200400ARMB';


/**
 * 页面编码
 */
export const pageId = '200400ARMB_card';

/**
 * 查询模板主键
 */
export const oid = '1002Z3100000000091N4';
/**
 * 表单区域
 */
export const formId = 'fct_ar';
/**
 * 表单签约信息区域
 */
export const signinfo = 'fct_ar_signinfo';

export const mngainfo =  'fct_ar_mngainfo';

export const auditinfo = 'fct_ar_auditinfo';
/**
 * 单页应用缓存
 */
export const dataSource  = 'fct.bill.ar.200400ARM';
/**
 * 单页应用缓存主键名字
 */
export const pkname  = 'pk_fct_ar';
/**
 * 表体区域
 */
export const tableId1 = 'pk_fct_ar_b';
export const tableId2 = 'pk_fct_ar_term';
export const tableId3 = 'pk_fct_ar_plan';
export const tableId4 = 'pk_fct_ar_memora';
export const tableId5 = 'pk_fct_ar_change';
export const tableId6 = 'pk_fct_ar_exec';
/**
 * 测拉区域
 */
export const tableId1childform2 = 'pk_fct_ar_b&childform2';
export const tableId2childform2 = 'pk_fct_ar_term&childform2';
export const tableId3childform2 = 'pk_fct_ar_plan&childform2';
export const tableId4childform2 = 'pk_fct_ar_memora&childform2';
export const tableId5childform2 = 'pk_fct_ar_change&childform2';
export const tableId6childform2 = 'pk_fct_ar_exec&childform2';
/**
 * 单据类型
 */
export const billtype = 'FCT2';
/**
 * 功能节点编码
 */
export const funcode = '200400ARM';
/**
 * 节点编码
 */
export const nodekey = 'FCT2-01';
/**
 * 多语文件编码
 */
export const multiLangCode = '200400ARM';
/**
 * 页面表格类型
 */
export const tableTypeObj = 'cardTable';

