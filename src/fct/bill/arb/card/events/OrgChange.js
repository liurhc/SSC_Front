import { formId, tableId1, tableId2, tableId3, tableId4, tableId5, tableId6, pageId, oid, appcode } from '../constants';
import {ajax} from 'nc-lightapp-front';
import loadPageValue from './loadPageValue';
import toggleShow from './toggleShow';
export default function(props,pk_org) {
	let that = this;
	if (pk_org == undefined || pk_org == '' || pk_org == null) {
		props.form.EmptyAllFormValue(formId);
		props.cardTable.setMulTablesData({[tableId1]: { rows: [] },[tableId2]: { rows: [] },[tableId3]: { rows: [] },[tableId4]: { rows: [] },[tableId5]: { rows: [] },[tableId6]: { rows: [] }});
		props.form.setFormItemsValue(formId, { pk_org: { value: null, display: null } });
		toggleShow(that, props);	
		props.initMetaByPkorg();		
	} else {
		props.resMetaAfterPkorgEdit();
		let data = {
			pageid: pageId,
			pk_org: pk_org,//财务组织
			ctrantypeid:props.getUrlParam('tradetype')
		};
		props.form.EmptyAllFormValue(formId);
        props.cardTable.setMulTablesData({[tableId1]: { rows: [] },[tableId2]: { rows: [] },[tableId3]: { rows: [] },[tableId4]: { rows: [] },[tableId5]: { rows: [] },[tableId6]: { rows: [] }});
		ajax({
			url: '/nccloud/fct/ar/mainorgchg.do',
			data: data,
			success: (res) => {
				loadPageValue(res.data, props);
				//设置表单、表格字段编辑状态
				props.form.setFormItemsDisabled(formId, { earlysign: 'N' });
				let corigcurrencyid = props.form.getFormItemsValue(formId, 'corigcurrencyid').display;
				if (corigcurrencyid === that.state.json['200400ARM-000000']) {/* 国际化处理： 人民币*/
					//币种为人民币时，折本汇率不可修改
					props.form.setFormItemsDisabled(formId, { nexchangerate: true });
				} else {
					//币种为人民币时，折本汇率不可修改
					props.form.setFormItemsDisabled(formId, { nexchangerate: false });
				}
				if(that.state.pk_transtype && that.state.transtype_name)
				{//设置合同类型值，及不可编辑
					props.form.setFormItemsValue(formId, { ctrantypeid: { value: that.state.pk_transtype, display: that.state.transtype_name } });
					props.form.setFormItemsValue(formId, { vtrantypecode: { value: that.state.transtype, display: that.state.transtype } });
					props.form.setFormItemsDisabled(formId, { ctrantypeid: true });
				}
				var vbillcodeEdit = res.data.userjson;
				//根据编码规则设置合同编码是否可编辑
				if("false" == vbillcodeEdit || false == vbillcodeEdit)
				{
					//不可编辑
					props.form.setFormItemsDisabled(formId, { vbillcode: true });
				}
				else
				{
					//可编辑
					props.form.setFormItemsDisabled(formId, { vbillcode: false });
				}
				if(props.getUrlParam('tradetype') != null)
				{
					//设置合同类型不可编辑
					props.form.setFormItemsDisabled(formId, { ctrantypeid: true });
				}
				toggleShow(that, props);
			}
		});
	}
}
