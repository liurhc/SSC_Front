import { ajax, base, toast,promptBox,print,output,cardCache,getBusinessInfo } from 'nc-lightapp-front';
import { imageScan, imageView } from 'sscrp/rppub/components/image';
import toggleShow from './toggleShow';
import loadPageValue from './loadPageValue';
import pageInfoClick from './pageInfoClick';
import queryCard from './queryCard';
import OrgChange from './OrgChange';
import {dataSource,pkname,formId,tableId1,tableId2,tableId3,tableId4,tableId5,tableId6,pageId,oid,appcode,billtype,funcode,nodekey} from '../constants';
export default function (props, id) {
  let that = this;
  let newid = id;
  let pageids = [];
  pageids.push(pageId);
  switch (newid) {
    case 'add':
        props.form.EmptyAllFormValue(formId);
        props.cardTable.setMulTablesData({[tableId1]: { rows: [] },[tableId2]: { rows: [] },[tableId3]: { rows: [] },[tableId4]: { rows: [] },[tableId5]: { rows: [] },[tableId6]: { rows: [] }});
        props.linkTo('/fct/bill/arb/card/index.html', {
            status: 'add',
            appcode: props.getSearchParam('c')
        });
        let pk_org = that.state.pk_org;
        props.setUrlParam({status:'add'});
        //变更财务组织调用事件 
        OrgChange.call(that,props,pk_org); 
        break;
    case 'save':
        that.saveBill();
        break  
    case 'savecommit':
        that.saveBill(newid);
        //保存后页面状态变更为浏览态
        if (props.getUrlParam('status') == 'browse') {
            onCommitOrUnCommit(that,props,id);
        }
        break;                       
    case 'edit':
        var tId = props.getUrlParam('id');
        props.linkTo('/fct/bill/ara/card/index.html', {
            status: 'edit',
            id: tId,
            appcode: props.getSearchParam('c')
        }); 
        queryCard.call(that,props);
      break;
    case 'refresh':
        var tId = props.getUrlParam('id');
        props.linkTo('/fct/bill/ara/card/index.html', {
            status: 'browse',
            id:tId,
            appcode: props.getSearchParam('c')
        });
        queryCard.call(that,props);
        toast({ color: 'success', title: that.state.json['200400ARM-000033'] });/* 国际化处理： 刷新成功*/
        break;  
    case 'copy':
        var tId = props.getUrlParam('id');
        props.linkTo('/fct/bill/ara/card/index.html', {
            status: 'copy',
            id:tId,
            appcode: props.getSearchParam('c')
        });
        queryCard.call(that,props);
        break;  
    //删除按钮
    case 'delete':
        let delObjs = [];
        delObjs.push({
			pk_bill: props.form.getFormItemsValue(formId,"pk_fct_ar").value,
			ts: props.form.getFormItemsValue(formId,"ts").value,
			index: 0,
			pageId: pageId
		});
        if (delObjs.length == 0) {
            toast({ content: `${that.state.json['200401APM-000037']}`, color: 'warning' });/* 国际化处理： 请先选中需要删除的数据*/
            return;
        }
        promptBox({
                color:"warning",
                title: that.state.json['200400ARM-000067'],
                content:that.state.json['200400ARM-000002'],/* 国际化处理： 是否确定删除要选中的合同?*/
                beSureBtnClick:function(){
            ajax({
                url: '/nccloud/fct/ar/deletecard.do',
                data: delObjs,
                success: (res) => {
                    if (res) {
                        toast({ color: 'success', content: that.state.json['200400ARM-000003'] });/* 国际化处理： 删除成功*/
                        props.form.EmptyAllFormValue(formId);
                        props.cardTable.setMulTablesData({[tableId1]: { rows: [] },[tableId2]: { rows: [] },[tableId3]: { rows: [] },[tableId4]: { rows: [] },[tableId5]: { rows: [] },[tableId6]: { rows: [] }});
                        props.setUrlParam({status:'browse',id:undefined});
                        toggleShow(that,props);
                    }
                }
            });}})
        break      
    //取消按钮
    case 'cancel':
    promptBox({
        color:"warning",title: that.state.json['200400ARM-000060'],content:that.state.json['200400ARM-000059'], beSureBtnClick:function(){
                var id = props.getUrlParam('id');
                props.resMetaAfterPkorgEdit();//取消设置状态为可编辑
                if(id == undefined)
                {
                    props.form.EmptyAllFormValue(formId);
                    props.cardTable.setMulTablesData({[tableId1]: { rows: [] },[tableId2]: { rows: [] },[tableId3]: { rows: [] },[tableId4]: { rows: [] },[tableId5]: { rows: [] },[tableId6]: { rows: [] }});
                props.linkTo('/fct/bill/ara/card/index.html', {
                    status: 'browse',
                    id: id,
                    appcode: props.getSearchParam('c')
                }); 
                toggleShow(that,props);
                }
                else
                {
                    props.form.cancel(formId);
                    props.cardTable.resetTableData(tableId1);
                    props.cardTable.resetTableData(tableId2);
                    props.cardTable.resetTableData(tableId3);
                    props.cardTable.resetTableData(tableId4);
                    props.cardTable.resetTableData(tableId5);
                    props.cardTable.resetTableData(tableId6);
                    props.linkTo('/fct/bill/arb/card/index.html', {
                        status: 'browse',
                        id: id,
                        appcode: props.getSearchParam('c')
                    }); 
                    toggleShow(that,props);
                }
            }});
        break
    case 'commit': case 'drop_commit': case 'uncommit':
        onCommitOrUnCommit(that,props,id);
        break; 
    case 'print':
        onPrint(props);
        break
    case 'output':
        onOutput(props);
        break;   
    case 'validate': case 'cancelvalidate': case 'freeze': 
        case 'unfreeze':case 'terminate':case 'unterminate':
            onDoAction(that,props,id);
            break 
    case 'modify':
            onModify(that,props);
            break    
    case 'modifyhistory':
            onModifyHistory(props);
            break
    //单据追溯            
    case 'billlinkquery':
        let printData1 = props.createExtCardData(pageId, formId, [tableId1, tableId2, tableId3, tableId4, tableId5, tableId6])
        let pk_data1 = printData1.head[formId].rows[0].values.pk_fct_ar.value;
        that.setState({
          trackshow: true,
          checkId:pk_data1
        })
        break;
    //附件上传
    case 'file':
        that.openfile(props);
        break;
    //审批详情
	case 'approveinfo':
        that.openApprove(props);
        break;    
    case 'yxsm'://影像扫描
        var billInfoMap = {};
        //基础字段 单据pk,单据类型，交易类型，单据的组织
        billInfoMap.pk_billid = props.form.getFormItemsValue(formId, 'pk_fct_ar').value;;
        billInfoMap.pk_billtype = props.form.getFormItemsValue(formId, 'cbilltypecode').value;
        billInfoMap.pk_tradetype = props.form.getFormItemsValue(formId, 'vtrantypecode').value;
        billInfoMap.pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;

        //影像所需 FieldMap
        billInfoMap.BillType = props.form.getFormItemsValue(formId, 'vtrantypecode').value;
        billInfoMap.BillDate = props.form.getFormItemsValue(formId, 'dbilldate').value;
        billInfoMap.Busi_Serial_No = props.form.getFormItemsValue(formId, 'pk_fct_ar').value;
        billInfoMap.pk_billtype = props.form.getFormItemsValue(formId, 'cbilltypecode').value;
        billInfoMap.OrgNo = props.form.getFormItemsValue(formId, 'pk_org').value;
        billInfoMap.BillCode = props.form.getFormItemsValue(formId, 'vbillcode').value;
        billInfoMap.OrgName = props.form.getFormItemsValue(formId, 'pk_org').display;
        billInfoMap.Cash = props.form.getFormItemsValue(formId, 'ntotalorigmny').value;
        imageScan(billInfoMap, 'iweb');
        break;
    case 'yxck': //影像查看
        var billInfoMap = {};
            
        //基础字段 单据pk,单据类型，交易类型，单据的组织
        billInfoMap.pk_billid = props.form.getFormItemsValue(formId, 'pk_fct_ar').value;;
        billInfoMap.pk_billtype = props.form.getFormItemsValue(formId, 'cbilltypecode').value;
        billInfoMap.pk_tradetype = props.form.getFormItemsValue(formId, 'vtrantypecode').value;
        billInfoMap.pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
        imageView(billInfoMap,  'iweb');
        break;                          
    default:
      break
  }
}
/**
 * 打印
 */
export const onPrint = (props) => {
  let printData = props.createExtCardData(pageId, formId, [tableId1, tableId2, tableId3, tableId4, tableId5, tableId6])
  let pk_data = printData.head[formId].rows[0].values.pk_fct_ar.value;
  var pk = [];
  pk.push(pk_data);
  print(
      'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
      '/nccloud/fct/ar/print.do', //后台服务url
      {
         billtype:billtype,  //单据类型
         funcode:funcode,      //功能节点编码，即模板编码
         nodekey:nodekey,     //模板节点标识
         oids:pk,    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
         userjson: billtype
      }
    )
}
/**
  * 打印输出
  */
export const onOutput = (props) => {
    let printData = props.createExtCardData(pageId, formId, [tableId1, tableId2, tableId3, tableId4, tableId5, tableId6])
    let pk_data = printData.head[formId].rows[0].values.pk_fct_ar.value;
    var pk = [];
    pk.push(pk_data);
    // 在你想要触发的事件里面调用output方法，必须传的参数有data， 选择传的参数有url（要打印调用的后台接口），callback（打印后的回调函数）
	output({
		url: '/nccloud/fct/ar/printoutput.do',
		data: {
			outputType: 'output', //输出类型
			funcode: funcode, //功能节点编码，即模板编码
			nodekey: nodekey, //模板节点标识
			oids: pk // 功能节点的数据主键 oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
        }
	});
};
/**
* 执行下的按钮功能
*/
export const onDoAction = (that,props,id) => {
    let printData = that.props.createExtCardData(pageId, formId, [tableId1, tableId2, tableId3, tableId4, tableId5, tableId6])
      let pk_data = printData.head[formId].rows[0].values.pk_fct_ar.value; 
      let valdate = printData.head[formId].rows[0].values.valdate.value.substring(0,10);
      let businessDates = getBusinessInfo().businessDate.substring(0,10);
      let index1 = [];
      let url;
      let temp;
      index1.push(pk_data);
      let pageids = [];
      pageids.push(pageId);
  
    if(id ===  'validate')
    {
        url = '/nccloud/fct/ar/validate.do';
        temp = that.state.json['200400ARM-000006'];/* 国际化处理： 生效*/
    }
    else if(id ===  'cancelvalidate')
    {
        url = '/nccloud/fct/ar/unvalidate.do';
        temp = that.state.json['200400ARM-000007'];/* 国际化处理： 取消生效*/
    }
    if(id ===  'freeze')
    {
        url = '/nccloud/fct/ar/freeze.do';
        temp = that.state.json['200400ARM-000008'];/* 国际化处理： 冻结*/
    }
    if(id ===  'unfreeze')
    {
        url = '/nccloud/fct/ar/unfreeze.do';
        temp = that.state.json['200400ARM-000009'];/* 国际化处理： 解冻*/
    }
    if(id ===  'terminate')
    {
        url = '/nccloud/fct/ar/terminate.do';
        temp = that.state.json['200400ARM-000010'];/* 国际化处理： 终止*/
    }
    if(id ===  'unterminate')
    {
        url = '/nccloud/fct/ar/unterminate.do';
        temp = that.state.json['200400ARM-000011'];/* 国际化处理： 取消终止*/
    }
    promptBox({
      color:"warning",content:that.state.json['200400ARM-000012']+temp+"?",/* 国际化处理： 是否确定要使该合同*/
      title:temp,
      beSureBtnClick:function(){
              if(id ===  'validate' || id ===  'cancelvalidate' || id ===  'freeze' || id ===  'unfreeze'  || id ===  'unterminate' ){
                  if(id ==='validate'){
                      if(businessDates<valdate){
                          that.setState({
                              id:id,
                              url:url,
                              temp:temp
                          },()=>{
                              props.modal.show('onDoAction');
                          })
                      }else{
                          let data = {
                              "pk":index1,
                              "pageid":pageids
                          }
                          ajax({
                              url: url,
                              data: data,
                              success: (res) => {
                                  if (res) {
                                      toast({ color: 'success', content: temp+that.state.json['200400ARM-000013'] });/* 国际化处理： 成功*/
                                      loadPageValue(res.data,props);
                                      toggleShow(that,props);
                                  }
                              }
                          });
                      }
                  }else{
                      that.setState({
                          id:id,
                          url:url,
                          temp:temp
                      },()=>{
                          props.modal.show('onDoAction');
                      })
                  }
                  
              }else{
                  let data = {
                      "pk":index1,
                      "pageid":pageids
                  }
                  ajax({
                      url: url,
                      data: data,
                      success: (res) => {
                          if (res) {
                              toast({ color: 'success', content: temp+that.state.json['200400ARM-000013'] });/* 国际化处理： 成功*/
                              loadPageValue(res.data,props);
                              toggleShow(that,props);
                          }
                      }
                  });
              }
  }})
}
/**
* 变更
*/
export const onModify = (that,props) => {
  let printData = props.createExtCardData(pageId, formId, [tableId1, tableId2, tableId3, tableId4, tableId5, tableId6])
  let pk_data = printData.head[formId].rows[0].values.pk_fct_ar.value; 
  promptBox({
    color:"warning",content:that.state.json['200400ARM-000014'],/* 国际化处理： 是否确认要变更该合同？*/
    beSureBtnClick:function(){
        props.linkTo('/fct/bill/ara/card/index.html', {
            status: 'modify',
            id: pk_data,
            appcode: props.getSearchParam('c')
        }); 
      toggleShow(that,props);
  }})

}
/**
* 变更历史
*/
export const onModifyHistory = (props) => {
  let printData = props.createExtCardData(pageId, formId, [tableId1, tableId2, tableId3, tableId4, tableId5, tableId6])
  let pk_data = printData.head[formId].rows[0].values.pk_fct_ar.value; 
  props.linkTo('/fct/bill/ar/list/index.html', { status:'modifyhistory',
  id: pk_data,
  appcode:props.getSearchParam('c')
})
}
export const onCommitOrUnCommit = (that,props,id) =>{
    let tempCommit;
    let indexCommit = [];
    let urlCommit;
    let printData = props.createExtCardData(pageId, formId, [tableId1, tableId2, tableId3, tableId4, tableId5, tableId6])
    let pk_data = printData.head[formId].rows[0].values.pk_fct_ar.value;
    indexCommit.push(pk_data);
    let pageids = [];
    pageids.push(pageId);
    if(id == 'uncommit')
    {
        urlCommit = '/nccloud/fct/ar/uncommit.do';
        tempCommit = that.state.json['200400ARM-000015'];/* 国际化处理： 收回*/
    }
    else
    {
        urlCommit = '/nccloud/fct/ar/commit.do';
        tempCommit = that.state.json['200400ARM-000016'];/* 国际化处理： 提交*/
    }
    if(indexCommit.length == 0)
    {
        toast({ content: `${that.state.json['200400ARM-000020']}，${that.state.json['200400ARM-000021']}`+tempCommit+`!`, color: 'warning' });/* 国际化处理： 未选中任何行,不允许操作*/
        return;
    }
promptBox({
    color:"warning",content:that.state.json['200400ARM-000017']+tempCommit+that.state.json['200400ARM-000018'],/* 国际化处理： 是否确定要,该合同?*/
    title:tempCommit,
    beSureBtnClick:function(){
    let data = {
        "pk":indexCommit,
        "pageid":pageids
    }
    ajax({
        url: urlCommit,
        data: data,
        success: (res) => {
            if (res) {
                toast({ color: 'success', content: tempCommit+that.state.json['200400ARM-000019'] });/* 国际化处理： 成功!*/
                loadPageValue(res.data,props);
                toggleShow(that,props);
            }
        }
});}})
}
