import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick'
import tableButtonClick from './tableButtonClick'
import tableButtonClick1 from './tableButtonClick1'
import toggleShow from './toggleShow';
import loadPageValue from './loadPageValue';
import queryCard from './queryCard';
export { buttonClick,queryCard,loadPageValue,toggleShow, afterEvent, initTemplate, pageInfoClick,tableButtonClick,tableButtonClick1 };
