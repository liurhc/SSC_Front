import {ajax,toast,getBusinessInfo } from 'nc-lightapp-front';
import refreshFun from './refreshFun';
import {appcode,tableId,pagecode,billtype,actionType,pagecodeCard} from '../constants';
const tableButtonClick = (that,props, key, text, record, index) => {
    let index1 = [];
    let pks = [];
    let data;
    switch (key) {
        // 修改
        case 'editline':
        let data = { 
            pk: record.pk_fct_ar.value,
            status:'edit',
            pageid:pagecodeCard
        };
        ajax({
            url: '/nccloud/fct/ar/querycard.do',
            data: data,
            success: (res) => {
                props.pushTo('/card',{
                    status:'edit',
                    id: record.pk_fct_ar.value,
                    appcode:props.getSearchParam('c')
                })
            }
        });
        break;
        //复制
        case 'copyline':
            props.pushTo('/card',{
                status:'copy',
                id: record.pk_fct_ar.value,
                appcode:props.getSearchParam('c')
            })
            break;
        //删除
        case 'deleteline':
           pks.push(record.pk_fct_ar.value);
           data = {
                "pk":pks,
                 pageid:pageids
            }
            ajax({
                url: '/nccloud/fct/ar/deletecard.do',
                data: data,
                success: (res) => {
                    if (res) {
                        toast({ color: 'success', content: that.state.json['200400ARM-000003'] });/* 国际化处理： 删除成功*/
                        props.table.deleteCacheId(tableId,pks);
                        props.table.deleteTableRowsByIndex(tableId, index);
                    }
                }
            });
            break;
        //单据追溯
        case 'billlinkqueryline':
            that.openBillTrack(props,record);
            break;
        //变更
        case 'modifyline':
            props.pushTo('/card',{
                status:'modify',
                id: record.pk_fct_ar.value,
                appcode:props.getSearchParam('c')
            })
            break; 
        //执行动作
        case 'validateline': case 'unfreezeline': case 'unterminateline': 
        case 'commitline': case 'uncommitline':
            oneOperation(that,key,billtype,pagecode,key,record,index);
            break
        //审批详情
		case 'approveinfoline':
            that.openApprove(props,record);
            break;       
        default:
            console.log(key);
            break;
    }
};
export default tableButtonClick;
/**
 * 执行下的按钮功能
 */
/**
     * 
     * @param {*} props 
     * @param {*} tableId   表格id
     * @param {*} billType  单据类型
     * @param {*} pageId     页面编码
     * @param {*} actionType 动作类型
     * @param {*} record    行记录
     */
export const oneOperation = function(that,id,billType,pageId,actionType,record,index) {
    let temp;
    if(id ===  'validateline')
    {
        temp = that.state.json['200400ARM-000006'];/* 国际化处理： 生效*/
    }
    if(id ===  'unfreezeline')
    {
        temp = that.state.json['200400ARM-000009'];/* 国际化处理： 解冻*/
    }
    if(id ===  'unterminateline')
    {
        temp = that.state.json['200400ARM-000011'];/* 国际化处理： 取消终止*/
    }
    if(id ===  'commitline')
    {
        temp = that.state.json['200400ARM-000016'];/* 国际化处理： 提交*/
    }
    if(id ===  'uncommitline')
    {
        temp = that.state.json['200400ARM-000015'];/* 国际化处理： 收回*/
    }
    let ajaxUrl = '/nccloud/fct/bill/oneAction.do';
    let valdate = record.valdate.value.substring(0,10);
    let businessDates = getBusinessInfo().businessDate.substring(0,10);
    let flage  = false;
    if(actionType === 'validateline'){
            if(businessDates<valdate){
                flage=true;
            }
    }else if ( actionType ===  'unfreezeline' || actionType ===  'unterminateline' ){
        flage=true;
    }
    let batchParams = [];
    batchParams.push({
            pk_bill: record.pk_fct_ar.value,
            ts: record.ts.value,
            billType: billType,
            pageId: pageId,
            actionCode:actionType,
            index:index
    });
    if(flage){
        that.setState({
            id:id,
            url:ajaxUrl,
            doTableId:tableId, 
            doBillType:billType,
            doPageId:pageId,
            doActionType:actionType,
            doTemp:temp,
            batchParams:batchParams
        },()=>{
            that.props.modal.show('onDoAction');
        })
    }
    else {
        ajax({
            url: ajaxUrl,
            data: batchParams,
            success: (res) => {
                let { success, data } = res;
                if(res.data.workflow && (res.data.workflow == 'approveflow'|| res.data.workflow == 'workflow'))
                {
                    that.setState({
                        url:ajaxUrl, 
                        commitdata:batchParams,
                        compositedata:res.data,
                        compositedisplay:true,
                        doBillType:billType
                    }) ;
                }else{
                    if (success) {
                        toast({ color: 'success', content: temp + that.state.json['200400ARM-000019'] });/* 国际化处理： 成功!*/
                        if (data) {
                            let updateValue = [];
                            for (let key in data) {
                                updateValue.push({ index: key, data: { values: data[key].values } });
                            }
                            that.props.table.updateDataByIndexs(tableId, updateValue);
                        }
                    }
                }
            }
        });
    }
}