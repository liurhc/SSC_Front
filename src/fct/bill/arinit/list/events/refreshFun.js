import { ajax, cacheTools, cardCache } from 'nc-lightapp-front';
import { searchId,oid, pagecode, tableId,dataSource } from '../constants';
import onSelectedFn from './onSelectedFn';
export default function(props) {

	//引入缓存---start
	let {setDefData, getDefData } = cardCache;
	//引入缓存---end
	let queryInfo = getDefData(searchId, dataSource);//获取缓存
	if (!queryInfo) {
		return;
	}
	let pageInfo = props.table.getTablePageInfo(tableId);
	let data = {
		pagecode: pagecode,
		queryInfo:queryInfo
	};
	ajax({
		url: '/nccloud/fct/arinit/querylist.do',
		data: data,
		success: function(res) {
			let { success, data } = res;
			if (data) {
				props.table.setAllTableData(tableId, res.data[tableId]);
			} else {
				let nulldata = {
					rows: []
				};
				props.table.setAllTableData(tableId, nulldata);
			}
			onSelectedFn(props, tableId);
			setDefData(tableId, dataSource, data);
		}
	});
}