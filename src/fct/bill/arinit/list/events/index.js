import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import refreshFun from './refreshFun';
import tableButtonClick from './tableButtonClick';
import doubleClick from './doubleClick';
import pageInfoClick from './pageInfoClick';
import onSelectedFn from './onSelectedFn';
export {initTemplate,onSelectedFn,buttonClick,pageInfoClick,tableButtonClick,refreshFun,doubleClick};
