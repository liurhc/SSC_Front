import { ajax, toast } from 'nc-lightapp-front';
import {appcode} from '../constants';
export default function doubleClick(record, index, e) {
    let status = 'browse';
    this.props.pushTo('/card', {
        status: status,
        id: record.pk_fct_ar.value,
        blatest:record.blatest.value,
        appcode:this.props.getSearchParam('c')
    });
}