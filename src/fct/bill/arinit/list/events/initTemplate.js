import { ajax, base, toast, Message,excelImportconfig } from 'nc-lightapp-front';
import tableButtonClick from './tableButtonClick';
import {searchId,pagecode,tableId,appcode,pagecodeCard} from '../constants';
import {getFirstOrgValue} from '../../../../public/components/pubUtils/billPubUtil.js';
let { NCPopconfirm, NCIcon } = base;
export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pagecode, //页面id
			appcode: props.getSearchParam('c') //小应用编码
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(that,props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button && that.props.getUrlParam('status') != 'modifyhistory') {
					/* 按钮适配  第一步：将请求回来的按钮组数据设置到页面的 属性上 */
					let button = data.button;
					// 临时模拟数据
					props.button.setButtons(button);
					props.button.setPopContent('deleteline',that.state.json['200400ARM-000041']);/* 国际化处理： 确定要该合同删除吗？*/
					props.button.setPopContent('commitline',that.state.json['200400ARM-000042']);/* 国际化处理： 确定要该合同提交吗？*/
					props.button.setPopContent('uncommitline',that.state.json['200400ARM-000043']); /* 国际化处理：确定要该合同收回吗？*/
					props.button.setPopContent('modifyline',that.state.json['200400ARM-000069']); /* 国际化处理： 确定要该合同变更吗？*/
					props.button.setPopContent('validateline',that.state.json['200400ARM-000044']);/* 国际化处理： 确定要该合同生效吗？*/
					props.button.setPopContent('unfreezeline',that.state.json['200400ARM-000045']); /* 国际化处理： 确定要该合同解冻吗？*/
					props.button.setPopContent('unterminateline',that.state.json['200400ARM-000046']); /* 国际化处理：确定要该合同取消终止吗？*/
					let toastArr = [];
					let excelimportconfig = excelImportconfig(props,"fct", "FCT2",true,"",{"appcode":props.getSearchParam('c'),"pagecode":pagecodeCard});
					props.button.setUploadConfig("import",excelimportconfig);
					props.button.setButtonDisabled(['delete','drop_commit','drop_doaction','drop_linkquery','drop_more','uncommit','export'],true);
				}
				if(data.context && that.props.getUrlParam('status') != 'modifyhistory')
				{
					let pk_org = data.context.pk_org;
					let org_Name = data.context.org_Name;
					//设置默认组织
					props.search.setSearchValByField(searchId, 'pk_org', { value: pk_org, display: org_Name }); 
					props.search.setSearchValByField(searchId, 'pk_org', { value: pk_org, display: org_Name },'normal'); 
					props.search.setSearchValByField(searchId, 'pk_org', { value: pk_org, display: org_Name },'super'); 

					if(data.context.paramMap)
					{
						let pk_transtype = data.context.paramMap.pk_transtype;
						let transtype_name = data.context.paramMap.transtype_name;
						//设置交易类型
						if(pk_transtype && transtype_name)
						{
							props.search.setSearchValByField(searchId, 'ctrantypeid', { value: pk_transtype, display: transtype_name }); 
							props.search.setDisabledByField(searchId, 'ctrantypeid', true);//设置不可编辑
						}
					}
				}
			}
		}
	);
}
function seperateDate(date) {
	if (typeof date !== 'string') return;
	let result = date.split(' ') && date.split(' ')[0];
	return result;
}
function modifierMeta(that,props, meta) {
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		item.col = '3';
		//财务组织
		if (item.attrcode == 'pk_org') {
		    //item.isShowDisabledData = false;
			// item.isShowUnit = false;
			item.queryCondition = () => {
				item.isShowUnit = false;
			    return {
		                 DataPowerOperationCode: 'fi',//使用权组
		                 AppCode: props.getSearchParam('c'),
		                 TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
		        		};
		    }
		}
		//合同类型
		if (item.attrcode == 'ctrantypeid') {
			item.queryCondition = () => {
				return { parentbilltype: 'FCT2' }; // 根据单据类型过滤
			}
		}
		//客户
		if (item.attrcode == 'pk_customer') {
			item.queryCondition = () => {
				let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_org') || {}).value.firstvalue);
				return { 
					DataPowerOperationCode: 'fi',
					isDataPowerEnable: 'Y',
					pk_org: pkOrgValue
			    };
			}
		}      
		//收支项目
		if (item.attrcode == 'pk_fct_ar_b.inoutcome') {
			item.isShowUnit = true
			item.unitCondition = () => {
				let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_org') || {}).value.firstvalue);
				return {
					pkOrgs: pkOrgValue,
					TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
				}
			}
			item.queryCondition = () => {
				let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_org') || {}).value.firstvalue);
				return { 
					pk_org: pkOrgValue // 根据组织过滤
				}; 
			}
			}
					if (item.attrcode ==  'depid')
					{//部门
						item.isShowUnit = true
						item.unitCondition = () => {
								let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_org') || {}).value.firstvalue);
								return {
										pkOrgs: pkOrgValue,
										TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
								}
						}
						item.queryCondition = () => {
								let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_org') || {}).value.firstvalue);
								return {
										DataPowerOperationCode: 'fi',//使用权组
										isDataPowerEnable: 'Y',
										pk_org: pkOrgValue,
								};
						}
				}
				if(item.attrcode ==  'pk_fct_ar_b.project')//项目
				{
					item.isShowUnit = true
					item.unitCondition = () => {
							let pkOrgValue = (props.search.getSearchValByField(searchId, 'pk_org') || {}).value.firstvalue;
							return {
									pkOrgs: pkOrgValue,
									TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
							}
					}
					item.queryCondition = () => {
							let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_org') || {}).value.firstvalue);
							return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									pk_org: pkOrgValue
							};
					}
				}
		return item;
	});
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		item.renderStatus = 'browse';
		if (item.attrcode == 'vbillcode') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							props.pushTo('/card', {
								status: 'browse',
								id: record.pk_fct_ar.value,
								blatest:record.blatest.value,
								appcode:props.getSearchParam('c')
							});
						}}
					>
						{record.vbillcode.value}
					</a>
				);
			};
		}
		return item;
	}); 
	//变更历史不创建操作列
	if(that.props.getUrlParam('status') != 'modifyhistory')
    {
		meta[tableId].items.push({
			label: that.state.json['200400ARM-000022'],/* 国际化处理： 操作*/
			itemtype: 'customer',
			attrcode: 'opr',
			width: '150px',
			visible: true,
			fixed: 'right',
			render: (text, record, index) => {
				let fstatusflag = record.fstatusflag.value;
				let buttonAry = swithButtonOprState(props,fstatusflag);
				return props.button.createOprationButton(buttonAry, {
					area: 'list_body_inner',
					buttonLimit: 3,
					onButtonClick: (props, key) => tableButtonClick(that,props, key, text, record, index)
				});
			}
		});
	}
	return meta;
}
/**
 * 每次切换按钮时，切换按钮状态
 */

export const swithButtonOprState = (props,fstatusflag) => {
	 let buttonAry = [];
	 switch (fstatusflag) {
        //自由
        case '0':
           buttonAry =  ['commitline','editline','copyline'];
           break;
        //生效
        case '1':
             buttonAry =  ['billlinkqueryline','modifyline','copyline'];
         break;
        //审批中
        case '2':
		     buttonAry =  ['uncommitline','approveinfoline','copyline'];
        break;
        //审批通过
        case '3':
		     buttonAry =  ['validateline','approveinfoline','copyline'];
        break;
        //审批未通过
        case '4':
		     buttonAry =  ['uncommitline','approveinfoline','copyline'];
        break;
        //冻结
        case '5':
		     buttonAry =  ['unfreezeline','billlinkqueryline','copyline'];
        break;
        //终止
        case '6':
		     buttonAry =  ['unterminateline','billlinkqueryline','copyline'];
        break;
        default:
         console.log(fstatusflag);
		 break;
	 } 
	 return buttonAry;
}
