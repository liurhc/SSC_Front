/**
 * 小应用主键
 */
export const appcode = '200401APMI';


/**
 * 页面编码
 */
export const pageId = '200401APMI_FCT1-01_card';

/**
 * 查询模板主键
 */
export const oid = '0001Z31000000001NWPK';
/**
 * 表单区域
 */
export const formId = 'fct_ap';
/**
 * 签约信息区域
 */
export const signinfo = 'fct_ap_signinfo';

export const mngainfo =  'fct_ap_mgnainfo';

export const auditinfo = 'fct_ap_auditinfo';
/**
 * 单页应用缓存
 */
export const dataSource  = 'fct.bill.ap.200401APMI';
/**
 * 单页应用缓存主键名字
 */
export const pkname  = 'pk_fct_ap';
/**
 * 表体区域
 */
export const tableId1 = 'pk_fct_ap_b';
export const tableId2 = 'pk_fct_ap_term';
export const tableId3 = 'pk_fct_ap_plan';
export const tableId4 = 'pk_fct_ap_memora';
export const tableId5 = 'pk_fct_ap_change';
export const tableId6 = 'pk_fct_ap_exec';
/**
 * 测拉区域
 */
export const tableId1childform2 = 'pk_fct_ap_b&childform2';
export const tableId2childform2 = 'pk_fct_ap_term&childform2';
export const tableId3childform2 = 'pk_fct_ap_plan&childform2';
export const tableId4childform2 = 'pk_fct_ap_memora&childform2';
export const tableId5childform2 = 'pk_fct_ap_change&childform2';
export const tableId6childform2 = 'pk_fct_ap_exec&childform2';
/**
 * 单据类型
 */
export const billtype = 'FCT1';
/**
 * 功能节点编码
 */
export const funcode = '200401APMI';
/**
 * 节点编码
 */
export const nodekey = 'FCT1-01';
/**
 * 多语文件编码
 */
export const multiLangCode = '200401APM';
/**
 * 页面表格类型
 */
export const tableTypeObj = 'cardTable';
