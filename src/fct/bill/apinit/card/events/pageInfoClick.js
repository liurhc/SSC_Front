import { ajax,cardCache } from 'nc-lightapp-front';
import loadPageValue from './loadPageValue';
import {pkname,formId,dataSource,pageId,tableId1,tableId2,tableId3,tableId4,tableId5,tableId6} from '../constants';
import toggleShow from './toggleShow';
export default function(props, pks,that) {
	if(that == undefined || that == null || that === "null")
	{
		that = this;
	}
	let { getCacheById, updateCache } = cardCache;
	props.setUrlParam({id:pks});
	if (pks == null || pks === "null" ||  pks == undefined) {
		props.form.EmptyAllFormValue(formId);
        props.cardTable.setMulTablesData({[tableId1]: { rows: [] },[tableId2]: { rows: [] },[tableId3]: { rows: [] },[tableId4]: { rows: [] },[tableId5]: { rows: [] },[tableId6]: { rows: [] }});
		return;
	}
	let cardData = getCacheById(pks, dataSource);

    if(cardData){
		loadPageValue(cardData,props);
		toggleShow(that,props);
	}
	else
	{
		let pkArr = [];
		pkArr.push(pks); //后台使用数组解析数据，此处传数组
		let data = {
			allpks: pkArr,
			pageid: pageId
		};
		ajax({
			url: '/nccloud/fct/ap/querycardpage.do',
			data: data,
			success: (res) => {
				if (res.data) {
					loadPageValue(res.data,props);
					toggleShow(that,props);
					updateCache(pkname,pks,res.data,formId,dataSource);
				}
			}
		});
	}
}
