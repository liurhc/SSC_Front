import { base, ajax,cacheTools,excelImportconfig } from 'nc-lightapp-front';
import tableButtonClick from './tableButtonClick'
import toggleShow from './toggleShow';
import OrgChange from './OrgChange';
let { NCPopconfirm, NCIcon } = base;

import {formId,tableId1,tableId2,tableId3,tableId4,tableId5,tableId6,pageId,oid,appcode} from '../constants';
export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: props.getSearchParam('c') //小应用编码
		}, 
		function (data){
			if(data){
				if(data.button){
					let button = data.button;
					props.button.setButtons(button, () => {	
						toggleShow(that,props);
					});
					let excelimportconfig = excelImportconfig(props,"fct", "FCT2");
					props.button.setUploadConfig("import",excelimportconfig);
				}
				if(data.template){
					let meta = data.template;
					modifierMeta(that,props, meta)
					props.meta.setMeta(meta,() => {
						let pk_org;
						let pk_transtype;
						let transtype_name;
						let transtype;
						if(data.context)
						{
							pk_org = data.context.pk_org;
							if(data.context.paramMap)
							{
								pk_transtype = data.context.paramMap.pk_transtype;
								transtype_name = data.context.paramMap.transtype_name;
								transtype = data.context.paramMap.transtype;
							}
							that.setState({
								pk_org:pk_org,
								pk_transtype:pk_transtype,
								transtype_name:transtype_name,
								transtype:transtype
							})
						}
						// 采购组织为空时不允许录入其他数据,不为空时要释放
						if (props.getUrlParam('status') === 'add' || cacheTools.get('status') == 'add') {
							//获取当前个性化设置的默认业务单元
							//变更财务组织调用事件 
							OrgChange.call(that,props,pk_org); 
						}
						if (props.getUrlParam('status') == 'modify')
						{
							var index = props.cardTable.getNumberOfRows(tableId5, false)-1;
							props.cardTable.setEditableByIndex(tableId5, index, 'vchgreason', true);//变更后最后一行的变更原因可编辑
						}  
					});
				}
			}   
		}
	)
}

function modifierMeta(that,props, meta) {
	let multiLang = props.MutiInit.getIntl('3630');
	//合同类型参照过滤
	meta[formId].items.map((item) => {
	})
	let porCol = {
		attrcode: 'opr',
		// label: multiLang && multiLang.get('36300TP-0005'),
		label: that.state.json['200401APM-000022'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		width:'200px',
		render: (text, record, index) => {
			let status = props.cardTable.getStatus(tableId1);
			let buttonAry = [];
			if(status === 'browse')
			{
				if(index == that.state.index) {
					buttonAry = that.state.switchflag1 ? ['closeline']:['switchview'];
				}else{
					buttonAry = ['switchview'];
				}
			}
			else
			{
				buttonAry = that.state.copyflag1 ? ['copyatline']:['openline','copyline', 'insertline', 'deleteline'];
			}
			return props.button.createOprationButton(buttonAry, {
                area: "card_body_inner1",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick.call(that,props, key, text, record, index,tableId1)
            });
        }
	};
	meta[tableId1].items.push(porCol);

	let porCol2 = {
		attrcode: 'opr',
		// label: multiLang && multiLang.get('36300TP-0005'),
		label: that.state.json['200401APM-000022'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		width:'200px',
		render: (text, record, index) => {
			let status = props.cardTable.getStatus(tableId2);
			let buttonAry = [];
			if(status === 'browse')
			{
				if(index == that.state.index) {
					buttonAry = that.state.switchflag2 ? ['closeline']:['switchview'];
				}else{
					buttonAry = ['switchview'];
				}
			}
			else
			{
				buttonAry = that.state.copyflag2 ? ['copyatline']:['openline','copyline', 'insertline', 'deleteline'];
			}
            return props.button.createOprationButton(buttonAry, {
                area: "card_body_inner2",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick.call(that,props, key, text, record, index,tableId2)
            });
        }
	};
	meta[tableId2].items.push(porCol2);

	let porCol3 = {
		attrcode: 'opr',
		// label: multiLang && multiLang.get('36300TP-0005'),
		label: that.state.json['200401APM-000022'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		width:'200px',
		render: (text, record, index) => {
			let status = props.cardTable.getStatus(tableId3);
			let buttonAry = [];
			if(status === 'browse')
			{
				if(index == that.state.index) {
					buttonAry = that.state.switchflag3 ? ['closeline']:['switchview'];
				}else{
					buttonAry = ['switchview'];
				}
				var fstatusflag = props.form.getFormItemsValue(formId,'fstatusflag').value;
				var blatest = props.form.getFormItemsValue(formId,'blatest').value;
				if(fstatusflag == '1' && blatest == true)
				buttonAry.push('push');
			}
			else
			{
				buttonAry = that.state.copyflag3 ? ['copyatline']:['openline','copyline', 'insertline', 'deleteline'];
			}
            return props.button.createOprationButton(buttonAry, {
                area: "card_body_inner3",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick.call(that,props, key, text, record, index,tableId3)
            });
        }
	};
	meta[tableId3].items.push(porCol3);

	let porCol4 = {
		attrcode: 'opr',
		// label: multiLang && multiLang.get('36300TP-0005'),
		label: that.state.json['200401APM-000022'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		width:'200px',
		render: (text, record, index) => {
			let status = props.cardTable.getStatus(tableId4);
			let buttonAry = [];
			if(status === 'browse')
			{
				if(index == that.state.index) {
					buttonAry = that.state.switchflag4 ? ['closeline']:['switchview'];
				}else{
					buttonAry = ['switchview'];
				}
			}
			else
			{
				buttonAry = that.state.copyflag4 ? ['copyatline']:['openline','copyline', 'insertline', 'deleteline'];
			}
            return props.button.createOprationButton(buttonAry, {
                area: "card_body_inner4",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick.call(that,props, key, text, record, index,tableId4)
            });
        }
	};
	meta[tableId4].items.push(porCol4);

	let porCol5 = {
		attrcode: 'opr',
		// label: multiLang && multiLang.get('36300TP-0005'),
		label: that.state.json['200401APM-000022'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		width:'200px',
		render: (text, record, index) => {
			let status = props.cardTable.getStatus(tableId5);
			let buttonAry = [];
			if(status === 'browse')
			{
				if(index == that.state.index) {
					buttonAry = that.state.switchflag5 ? ['closeline']:['switchview'];
				}else{
					buttonAry = ['switchview'];
				}
			}
			else
			{
				buttonAry = [''];//that.state.copyflag5 ? ['copyatline']:['openline','copyline', 'insertline', 'deleteline'];
			}
            return props.button.createOprationButton(buttonAry, {
                area: "card_body_inner5",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick.call(that,props, key, text, record, index,tableId5)
            });
        }
	};
	meta[tableId5].items.push(porCol5); 

	let porCol6 = {
		attrcode: 'opr',
		// label: multiLang && multiLang.get('36300TP-0006'),
		label: that.state.json['200401APM-000022'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		width:'200px',
		render: (text, record, index) => {
			let status = props.cardTable.getStatus(tableId6);
			let buttonAry = [];
			if(status === 'browse')
			{
				if(index == that.state.index) {
					buttonAry = that.state.switchflag6 ? ['closeline']:['switchview'];
				}else{
					buttonAry = ['switchview'];
				}
			}
			else
			{
				buttonAry = [''];//that.state.copyflag6 ? ['copyatline']:['openline','copyline', 'insertline', 'deleteline'];
			}
            return props.button.createOprationButton(buttonAry, {
                area: "card_body_inner6",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick.call(that,props, key, text, record, index,tableId6)
            });
        }
	};
	meta[tableId6].items.push(porCol6);

	return meta;
}