import {asyncComponent} from 'nc-lightapp-front';
import JobEditTable from '../list/index.js';


const card = asyncComponent(() => import(/* webpackChunkName: "fct/bill/apinit/card" *//* webpackMode: "eager" */'../card/index.js'));
const HistoryTable = asyncComponent(() => import(/* webpackChunkName: "fct/bill/apinit/list" */ /* webpackMode: "eager" */'../list/history.js'));

const routes = [
  {
    path: '/list',
    component: JobEditTable,
    exact: true,
  },
  {
    path: '/card',//定义路由
    component: card,
  },
  {
    path: '/history',
    component: HistoryTable,
    exact: true,
  }
];

export default routes;
