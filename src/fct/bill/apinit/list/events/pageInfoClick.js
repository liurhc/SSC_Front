import {ajax} from 'nc-lightapp-front';
import {pagecode,tableId} from '../constants';
export default function (props, config, pks) {
    let data = {
        "allpks": pks,
        "pageid": pagecode
    };
    ajax({
        url:'/nccloud/fct/apinit/querypage.do',
        data: data,
        success: function (res) {
            let {success,data} = res;
            if(success)
            {
                if(data)
                {
                    props.table.setAllTableData(tableId, res.data[tableId]);
                }
                else
                {
                    props.table.setAllTableData(tableId, {rows:[]});
                }
            }
        }
    });
}