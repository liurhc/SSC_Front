import {formId,tableId1,tableId2,tableId3,tableId4,tableId5,tableId6,pageId,oid,appcode} from '../constants';
export default function(data, props) {
	if (data) {
		if (data.head) {
			props.form.setAllFormValue({ [formId]: data.head[formId] });
		}
		if (data.bodys) {
			if (data.bodys[tableId1]) {
				props.cardTable.updateDataByRowId(tableId1, data.bodys[tableId1]);
			} else {
				props.cardTable.setTableData(tableId1, { rows: [] });
			}
			if (data.bodys[tableId2]) {
				props.cardTable.updateDataByRowId(tableId2, data.bodys[tableId2]);
			} else {
				props.cardTable.setTableData(tableId2, { rows: [] });
			}
			if (data.bodys[tableId3]) {
				props.cardTable.updateDataByRowId(tableId3, data.bodys[tableId3]);
			} else {
				props.cardTable.setTableData(tableId3, { rows: [] });
			}
			if (data.bodys[tableId4]) {
				props.cardTable.updateDataByRowId(tableId4, data.bodys[tableId4]);
			} else {
				props.cardTable.setTableData(tableId4, { rows: [] });
			}
			if (data.bodys[tableId5]) {
				props.cardTable.updateDataByRowId(tableId5, data.bodys[tableId5]);
			} else {
				props.cardTable.setTableData(tableId5, { rows: [] });
			}
			if (data.bodys[tableId6]) {
				props.cardTable.updateDataByRowId(tableId6, data.bodys[tableId6]);
			} else {
				props.cardTable.setTableData(tableId6, { rows: [] });
			}
		}
    }
    else {
        props.form.EmptyAllFormValue(formId);
        props.cardTable.setTableData(tableId1, { rows: [] });
        props.cardTable.setTableData(tableId2, { rows: [] });
        props.cardTable.setTableData(tableId3, { rows: [] });
        props.cardTable.setTableData(tableId4, { rows: [] });
        props.cardTable.setTableData(tableId5, { rows: [] });
        props.cardTable.setTableData(tableId6, { rows: [] });
    }
}
