import { createPage, ajax, base, toast, cardCache,high} from 'nc-lightapp-front';
import toggleShow from './toggleShow';
import loadPageValue from './loadPageValue';
import { signinfo,tableTypeObj,pkname,formId, tableId1, tableId2, tableId3, tableId4, tableId5, tableId6, pageId, appcode,dataSource } from '../constants';
let { getCacheById, updateCache,addCache } = cardCache;
export default function(props) {
    let that = this;
    let flag =false;
    if (props.getUrlParam('status') != 'add') {
        let data = { pk: props.getUrlParam('id'), pageid: pageId,status:props.getUrlParam('status') };
        var url = '/nccloud/fct/ap/querycard.do';
        let cardData;
        if (props.getUrlParam('status') == 'copy') {
            url = '/nccloud/fct/ap/copycard.do';
        } else if (props.getUrlParam('status') == 'modify') {
            //变更进入 跳转页面
            url = '/nccloud/fct/ap/modify.do';
            flag = true;
        } else if (props.getUrlParam('blatest') == 'false' || props.getUrlParam('blatest') == false) {
            //变更历史进入 跳转页面
            url = '/nccloud/fct/ap/modifyhistorycard.do';
        } else if(props.getUrlParam('status') == 'edit') {
	    //页面修改不从缓存中查数据
	    } else if (props.getUrlParam('status') != 'refresh') {
            //页面刷新不从缓存中查数据
            cardData = getCacheById(props.getUrlParam('id'), dataSource);        
        } else {
            //刷新更新状态为浏览态
            props.setUrlParam({status:'browse'});
        }
        if(url == '/nccloud/fct/ap/querycard.do' && cardData){
            loadPageValue(cardData,props);
            toggleShow(that, props,flag);
        } else {
            ajax({
                url: url,
                data: data,
                async:false,//同步
                success: (res) => {
                    if (res.formulamsg && res.formulamsg instanceof Array && res.formulamsg.length > 0) {
                        props.dealFormulamsg(
                            res.formulamsg,  //参数一：返回的公式对象
                            {                //参数二：界面使用的表格类型
                                "table1":tableTypeObj
                            }
                        );
                    }
                    loadPageValue(res.data,props);
                    toggleShow(that, props,flag);  
                },
                error: (res) => {
                    toast({ content: res.message, color: 'warning' });
                    props.setUrlParam({status:'browse'});
                    toggleShow(that, props,flag);
                }
            });
        }
    } else {
        toggleShow(that, props,true);
    }
}