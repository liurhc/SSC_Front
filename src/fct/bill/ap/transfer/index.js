import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { base, createPage, ajax,spaCache,getMultiLang } from 'nc-lightapp-front';
let {setDefData, getDefData } = spaCache;
let { NCBackBtn } = base;
import { initTemplate } from './init';
import { searchBtnClick } from './btnClick';
import {  multiLangCode,ap_searchId, ap_headId, ap_bbodyId,ap_planbodyId, ap_pageId, ap_oid, ap_billType,fctdatasource,ap_pkname } from './constants';
class TransferTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json:{}
		};
		this.searchId = ap_searchId;	
		this.headId = ap_headId;
		if(props.getUrlParam('dest_billtype')=='F2'||props.getUrlParam('dest_billtype')=='F3'){//F0-应收单 F1-应付单 F2-收款单 F3-付款单
			this.bodyId = ap_planbodyId;
			this.bodypk = 'pk_fct_ap_plan';
		}else if (props.getUrlParam('dest_billtype')=='F0'||props.getUrlParam('dest_billtype')=='F1'){
			this.bodyId = ap_bbodyId;
			this.bodypk = 'pk_fct_ap_b';
		}
		this.pageId = ap_pageId;
		this.oid = ap_oid;
	}
	componentDidMount() {
		if(!this.props.transferTable.hasCache(fctdatasource)){
			this.props.transferTable.setTransferTableValue(this.headId, this.bodyId, [], ap_pkname, this.bodypk);
		};
	}
	componentWillMount(){
        let  callback= (json) =>{
            this.setState({json:json},()=>{
                initTemplate.call(this, this.props);
           })
       }
       getMultiLang({moduleId: multiLangCode, currentLocale: 'simpchn',domainName: 'fct',callback})
    }
	//转单跳转目的小应用
	transfer = () => {
		setDefData('src_appcode', fctdatasource, this.props.getUrlParam("src_appcode"));
        setDefData('src_pagecode', fctdatasource, ap_pageId);
		this.props.pushTo('/card', {
			status: 'add',
			type: 'transfer',
			srcBilltype: ap_billType,
			dataSource: fctdatasource,
			pagecode: this.props.getUrlParam('dest_tradetype')
		});
	}
	//返回列表
	backList = () => {
		let dest_billtype = this.props.getUrlParam('dest_billtype');
		let pagecode = null;
		if (dest_billtype) {
			if (dest_billtype == 'F0') {
				pagecode = '20060RBM_LIST'; //应收单列表
			} else if (dest_billtype == 'F1') {
				pagecode = '20080PBM_LIST'; //应付单列表
			} else if (dest_billtype == 'F2') {
				pagecode = '20060GBM_LIST'; //收款单列表
			} else if (dest_billtype == 'F3') {
				pagecode = '20080EBM_LIST'; //付款单列表
			}
		}
		this.props.pushTo('/list', {
			pagecode: pagecode
		});
	};
	// react：界面渲染函数
	render() {
		const { transferTable, button, search } = this.props;
		const { NCCreateSearch } = search;
		const { createButton } = button;
		const { createTransferTable, createTransferList, getTransformFormDisplay } = transferTable;
		return (
			<div id="transferList" className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div>
						<NCBackBtn onClick={this.backList} />
					</div>
					<div className="header-title-search-area">
						<h2 className="title-search-detail">{this.state.json['200401APM-000055']}</h2>{/* 国际化处理： 选择合同*/}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(
						this.searchId,
						{
							clickSearchBtn: searchBtnClick.bind(this)
						}
						//模块id
					)}
				</div>
				<div className="nc-bill-transferTable-area">
					{createTransferTable({
						dataSource: fctdatasource,
						pkname:ap_pkname,
						headTableId: this.headId, //表格组件id
						bodyTableId: this.bodyId, //子表模板id
						fullTableId: this.bodyId,
						//点击加号展开，设置表格数据
						transferBtnText: this.state.json['200401APM-000054'], //转单按钮显示文字/* 国际化处理： 生成单据*/
						containerSelector: '#transferList',
						onTransferBtnClick: this.transfer,
						showChangeViewBtn:false
					})}
				</div>
			</div>
		);
	}
}

TransferTable = createPage({
})(TransferTable);
export default TransferTable;

