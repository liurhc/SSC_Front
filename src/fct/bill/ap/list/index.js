import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage,ajax,base,toast,cacheTools,high,cardCache,getMultiLang,promptBox,createPageIcon} from 'nc-lightapp-front';
import {buttonClick,initTemplate,onSelectedFn,doubleClick,pageInfoClick} from './events';
const { BillTrack,NCUploader,ApproveDetail   } = high;
const {NCFormControl,NCCheckbox,NCAffix,NCButton,NCInput} = base;
import {ExcelImport } from '../../../../uap/public/excelImport';
import {tableTypeObj,multiLangCode,searchId,pagecode,tableId,appcode,oid,dataSource,pkname} from './constants';
import Utils from '../../../../uap/public/utils';
import { getAllCheckedData,getTemp } from '../../../public/components/pubUtils/billPubUtil.js';
import ApprovalTrans from '../../../../uap/public/excomponents/approvalTrans';
import './index.less'
import AuditLoading from '../../../public/components/AuditLoading';
let ajaxurl = {
    query:'/nccloud/fct/ap/querylist.do',
}
//引入缓存---start
let {setDefData, getDefData } = cardCache;
//引入缓存---end
/**
 * author songjqk
 *
 */
class JobEditTable extends Component {
    constructor(props){
        super(props);
        this.state={
            searchValue:'',
            checkValue:null,
            checkId:null,
            queryData:[],
            trackshow: false,
            showUploader: false,
            approveshow:false,
            billtype:null,
            showApproveDetail: false,//审批详情的控制
            json:{},
            onDoActionValue:'',//执行原因
            url:'',
            doTableId:'', 
            doBillType:'',
            doPageId:'',
            doActionType:'',
            doTemp:'',
            compositedata:null,
            compositedisplay:false,
            commitdata:null,
            showloading: false,
            msg: ''
        }
        this.pks;
    }
    componentDidMount() {
            if(!this.props.table.hasCacheData(dataSource)){};
        
    }
    componentWillMount(){
        let  callback= (json) =>{
            this.setState({json:json},()=>{
                initTemplate.call(this, this.props);
           })
       }
       getMultiLang({moduleId: multiLangCode, currentLocale: 'simpchn',domainName: 'fct',callback})
    }
    //打开审批详情
	openApprove = (props,record)=>{
        let pk;
        let billtype
        if(record == undefined)
        {
            let checkData = props.table.getCheckedRows(tableId);
            if(checkData.length <= 0)
            {
                //未选中数据
                return;
            }
            pk = checkData[0].data.values.pk_fct_ap; 
            billtype = checkData[0].data.values.vtrantypecode;
        }
        else{
            pk = record.pk_fct_ap;
            billtype = record.vtrantypecode;
        }
        this.setState({
			showApproveDetail: true,
			billid:pk.value,
			billtype:billtype.value
        })
    }
    //审批详情模态框控制
	closeApprove = () => {
		this.setState({
			showApproveDetail: false
		})
	}
//单据追溯
    openBillTrack = (props,record)=>{
    let rows;
    let index1 = [];
    let pk;
    if(record === "undefined" || record == undefined)
    {
        rows = props.table.getCheckedRows(tableId);
        rows.forEach((ele, index) => {
            pk = ele.data.values.pk_fct_ap;
            index1.push(pk.value);
            });
            if(index1.length == 0){
                toast({ content: `${this.state.json['200401APM-000050']}！`, color: 'warning' });/* 国际化处理： 请选择合同*/
                return; 
            }
            if(index1.length>1){
                toast({ content: `${this.state.json['200401APM-000051']}！`, color: 'warning' });/* 国际化处理： 合同不能多选*/
                return; 
            }
    }
    else
    {
        pk = record.pk_fct_ap;
    }

    this.setState({
        trackshow: true,
        checkId:pk.value
    })
        
    }
    beforeUpload(billId, fullPath, file, fileList) {
        const isLt20M = file.size / 1024 / 1024 < 20;
        if (!isLt20M) {
            toast({ content: `${this.state.json['200401APM-000025']}20M！`, color: 'warning' });/* 国际化处理： 上传大小小于*/
        }
        return isLt20M;
        // 备注： return false 不执行上传  return true 执行上传
    }
    //关闭
    onHide = () => {
        this.setState({
            showUploader: false
        })
    }
    //附件
    openfile  = (props)=>{
        let rows = props.table.getCheckedRows(tableId);
        let index1 = [];
        let pk;
        let vbillcode;
        rows.forEach((ele, index) => {
        pk = ele.data.values.pk_fct_ap;
        vbillcode = ele.data.values.vbillcode;
        index1.push(pk.value);
        });
        if(index1.length == 0){
            toast({ content: `${this.state.json['200401APM-000050']}！`, color: 'warning' });/* 国际化处理： 请选择合同*/
            return; 
        }
        if(index1.length>1){
            toast({ content: `${this.state.json['200401APM-000051']}！`, color: 'warning' });/* 国际化处理： 合同不能多选*/
            return; 
        }  
        this.setState({
            checkId:pk.value,
            checkValue:vbillcode.value
        }, () => {
			if (this.state.showUploader == false) {
				this.setState({ showUploader: true });
			} else {
				this.setState({ showUploader: false });
			}
		});
    }

    //查询区按钮点击事件
    onClickSearchBtn(props,searchVal){
        let _this = this;
        let pageInfo = _this.props.table.getTablePageInfo(tableId);
        let queryInfo = props.search.getQueryInfo(searchId);
        if(!queryInfo)
        {
            return;
        }
        queryInfo.pageInfo = pageInfo;
        let earlySign = {
            "datatype":"32",
            "display": "",
            "field":"earlysign",
            "isIncludeSub":false,
            "oprtype":"=",
            "refurl":"",
            "value":{
                "firstvalue":false,
                "secondvalue":""
            }
        }
        queryInfo.querycondition.conditions.push(earlySign);
        setDefData(searchId, dataSource, queryInfo);//放入缓存
        let data = {
            queryInfo: queryInfo,
            pageCode: pagecode
            // conditions: searchVal.conditions,
            // oid: oid,
            // queryAreaCode: searchId//查询区编码
        };
        ajax({
            url: ajaxurl.query,
            data:data,
            success: function (res) {
                let { success, data } = res;
                if (success) {
                    if (res.formulamsg && res.formulamsg instanceof Array && res.formulamsg.length > 0) {
                        props.dealFormulamsg(
                            res.formulamsg,  //参数一：返回的公式对象
                            {                //参数二：界面使用的表格类型
                                "table1":tableTypeObj
                            }
                        );
                    }
                    if(data){
                        _this.props.table.setAllTableData(tableId,data[tableId]);
                        toast({ content: _this.state.json['200401APM-000062'].replace("{n}",res.data.fct_ap.allpks.length), color: 'success' });
                    }else{

                        let nulldata = {
                                rows:[]
                            }
                        _this.props.table.setAllTableData(tableId,nulldata);
                        toast({ content: _this.state.json['200401APM-000061'], color: 'warning' });
                    }
                    onSelectedFn( _this.props, tableId);
                    //设置缓存
                    setDefData(tableId, dataSource, data);
                }
            }
        })
     }

    /**
 	* 执行原因
 	*/
	onDoAction = () => {
		let {onDoActionValue,doTemp}=this.state;
		return(
			<div className="line-item">
				<div className="item-title">{this.state.json['200401APM-000072']/* 国际化处理： 原因*/}</div>
				<div className="item-content">
					<NCInput
						defaultValue={onDoActionValue}
            placeholder={this.state.json['200401APM-000070']/* 国际化处理： 请输入*/+doTemp+this.state.json['200401APM-000072']/* 国际化处理：原因*/+this.state.json['200401APM-000074']/* 国际化处理： 上限120字*/}
						maxlength="120"
						onChange={(value)=>{
							this.setState({
								onDoActionValue:value
							})
						}}
					/>
				</div>
			</div>
		)
	}
	onDoTrueAction=(flage)=>{
        let that=this;
        let {onDoActionValue,url,doTableId,doBillType,doPageId,doActionType,doTemp,batchParams}=that.state;
        if(batchParams!=null&&batchParams!=undefined&&batchParams.length>0){
            batchParams[0].onDoActionValue = flage?onDoActionValue:null;
        }else{
            batchParams  = getAllCheckedData(that.props, doTableId, doBillType,doPageId,doActionType,flage?onDoActionValue:null);
        }
		that.setState({
            onDoActionValue:'',
            showloading: true,
            title:getTemp(doBillType,doActionType,that)+that.state.json['2004common-000004']/* 国际化处理： "中，请耐心等待...*/
        })
		ajax({
            url: url,
            data: batchParams,
            loading:false,
            success: (res) => {
                let { success, data } = res;
                that.setState({
                    batchParams:null,
                    showloading: false
                })
                if (success) {
                    if (data.grid || data.message) {
                        //列表头部按钮操作，存在grid 或者 message
                        let grid = data.grid;
                        let updateValue = [];
                        for (let key in grid) {
                            updateValue.push({ index: key, data: { values: grid[key].values } });
                        }
                        that.props.table.updateDataByIndexs(tableId, updateValue);
                        if (data.flag == "1") {
                            toast({
                                color: 'success',
                                title: getTemp(doBillType,doActionType,that)+getTemp(doBillType,'success',that),
                                content: data.message
                            });
                        }
                        else if (data.flag == "2") {
                            toast({
                                duration:  'infinity',
                                color: 'danger',
                                content:data.message
                            });
                        }
                    }else {
                        if (data) {
                            let updateValue = [];
                            for (let key in data) {
                                updateValue.push({ index: key, data: { values: data[key].values } });
                            }
                            that.props.table.updateDataByIndexs(tableId, updateValue);
                            toast({ color: 'success', content: doTemp + getTemp(doBillType,'success',that) });/* 国际化处理： 成功!*/
                        }
                    } 
                }
            },
            error: function (res) {
                toast({
                    duration:  'infinity',
                    color: 'danger',
                    content:res.message
                });
                that.setState({
                    batchParams:null,
                    showloading: false
                })
            }

        });
    }
    closeAssgin=()=>{
		if (this.state.compositedisplay) {
			this.setState({ compositedisplay: false });
		}
	}
	getAssginUsedr=(value)=>{
            this.state.commitdata.forEach((val) => {
                val.assignObj = value;
            })
			let that=this;
			ajax({
				url: that.state.url,
				data: that.state.commitdata,
				success: (res) => {
                    debugger
                    let { success, data } = res;
					if(success){
                        if (data.grid) {
                            if (data.grid) {
                                let grid = data.grid;
                                let updateValue = [];
                                for (let key in grid) {
                                    updateValue.push({ index: key, data: { values: grid[key].values } });
                                }
                                that.props.table.updateDataByIndexs(tableId, updateValue);
                            }
                            toast({ color: 'success', content: getTemp(that.state.doBillType,'drop_commit',that)+getTemp(that.state.doBillType,'success',that) });/* 国际化处理： 成功*/
                            that.closeAssgin();
                        }else {
                            if (data) {
                                let updateValue = [];
                                for (let key in data) {
                                    updateValue.push({ index: key, data: { values: data[key].values } });
                                }
                                that.props.table.updateDataByIndexs(tableId, updateValue);
                                toast({ color: 'success', content: getTemp(that.state.doBillType,'drop_commit',that)+getTemp(that.state.doBillType,'success',that) });/* 国际化处理： 成功*/
                                that.closeAssgin();
                            }
                        }
					}
				}
			});
    }
    /**
	 * 获取Loading视图
	 */
	getLoading = () => {
		/* 如果要全屏遮罩，container 就不需要传了，上面也不需要写 id="table" style={{ position: 'relative' }} */
		return (
			<AuditLoading
				title={this.state.title} 
				container={'nc-ia-audit'}
				showloading={this.state.showloading}
			/>
		);
    };
    	// 查询区渲染完成回调函数
	renderCompleteEvent = () => {
		let cachesearch = getDefData(searchId, dataSource);
		if (cachesearch && cachesearch.querycondition && cachesearch.querycondition.conditions) {
			for (let item of cachesearch.querycondition.conditions) {
				if (item.field == 'dmakedate') {
					// 时间类型特殊处理
					let time = [];
					time.push(item.value.firstvalue);
					time.push(item.value.secondvalue);
					this.props.search.setSearchValByField(searchId, item.field, {
						display: item.display,
						value: time
					});
				} else {
					this.props.search.setSearchValByField(searchId, item.field, {
						display: item.display,
						value: item.value.firstvalue
					});
				}
			}
		}
	};
    render() {
        const {table,button ,search,modal} = this.props;
        let buttons = this.props.button.getButtons();
        const { createButton,createButtonApp } = button;
        const { NCCreateSearch } = search;
        const { createModal } = modal;
        const { createSimpleTable } = table;
        let { showUploader, target } = this.state;
        /* 按钮适配 第二步: 从 props.button中取到 createButtonApp 方法用来创建按钮组*/
        return (
            <div className="nc-bill-list">
                <NCAffix>
                    <div className="nc-bill-header-area">
                        <div className="header-title-search-area">
                            {/*页面大图标*/}
                            {createPageIcon()}
                            <h2 className="title-search-detail">{this.state.json['200401APM-000026']/* 国际化处理： 付款合同管理*/}</h2>
                        </div>
                        <div className="header-button-area">
                            {/* 按钮适配 第三步:在页面的 dom 结构中创建按钮组，传入显示的区域，绑定按钮事件*/}
                            {
                                createButtonApp({
                                area:searchId,
                                buttonLimit: 3,
                                onButtonClick: buttonClick.bind(this),
                                popContainer: document.querySelector('.header-button-area')
                            })}
                            {/* 单据追溯 */}
                            <BillTrack
                            show={this.state.trackshow}
                            close={()=>{
                                this.setState({trackshow: false})
                            }}

                            pk={this.state.checkId} //单据id
                            type='FCT1'  //单据类型
                          />

                           {/* 附件*/}
                            {/* 这里是附件上传组件的使用，需要传入三个参数 */}
                            {showUploader && <NCUploader 
                                billId={this.state.checkId} 
                                billNo={this.state.checkValue}
                                beforeUpload={this.beforeUpload}
                                onHide={this.onHide}
                                 />
                            }
                            {/* 审批详情*/}
                            <ApproveDetail
                                show={this.state.showApproveDetail}
                                close={this.closeApprove}
                                billtype={this.state.billtype}
                                billid={this.state.billid}
                            />
                        </div>
                    </div>
                </NCAffix>
                <div className="nc-bill-search-area">
                    {
                        NCCreateSearch(searchId, {
                        clickSearchBtn: this.onClickSearchBtn.bind(this),
                        showAdvBtn: true,                           //  显示高级按钮
                        // onAfterEvent: this.onAfterEvent.bind(this),  //编辑后事件
                        // searchBtnName :''                        //    查询按钮名称，默认查询
                        // showAdvSearchPlanBtn :false,    //高级面板中是否显示保存方案按钮 ;默认显示
                        // replaceAdvBtnEve:()=>{},        // 业务组替换高级面板 (fun)
                        // replaceAdvBody: this.replaceAdvBody,          // 业务组替换高级面板中的body (fun),return Dom 
                        addAdvTabs: this.addAdvTabs,              // 添加高级查询区自定义页签 (fun), return Dom 
                        // addAdvBody: ()=>{},              // 添加高级查询区自定义查询条件Dom (fun) , return Dom 
                        oid: oid,        //查询模板的oid，用于查询查询方案
                        renderCompleteEvent: this.renderCompleteEvent // 查询区渲染完成回调函数
                    })}
                </div>
                {/* 列表区 */}
                <div className="nc-bill-table-area ap-table">
                    {createSimpleTable(tableId, {
                        dataSource: dataSource,
                        pkname: pkname,
                        handlePageInfoChange: pageInfoClick.bind(this),
                        onRowDoubleClick: doubleClick.bind(this),
                        onSelected:onSelectedFn.bind(this),
                        onSelectedAll:onSelectedFn.bind(this),
                        showCheck:true,
                        showIndex:true,
                        componentInitFinished:()=>{
							onSelectedFn(this.props, tableId);
						}
                    })}
                </div>
                <div>
					{createModal('onDoAction', {
						title: this.state.doTemp/* 国际化处理： 原因*/, // 弹框表头信息
						content: this.onDoAction(this), //弹框内容，可以是字符串或dom
                        beSureBtnClick: this.onDoTrueAction.bind(this,true), //点击确定按钮事件
                        cancelBtnClick: this.onDoTrueAction.bind(this,false),
						userControl: false, // 点确定按钮后，是否自动取消弹出框.true:手动关。false:自动关
						className: 'senior', //  模态框大小 sm/lg/xlg
						rightBtnName: this.state.json['200401APM-000060']/* 国际化处理： 取消*/, //左侧按钮名称,默认取消
						leftBtnName: this.state.json['200401APM-000073']/* 国际化处理： 确定*/ //右侧按钮名称， 默认确定
					})}
				</div>
                {this.state.compositedisplay ? (
							<ApprovalTrans
								title={this.state.json['200401APM-000075']}/* 国际化处理： 指派*/
								data={this.state.compositedata}
								display={this.state.compositedisplay}
								getResult={this.getAssginUsedr}
								cancel={this.closeAssgin.bind(this)}
							/>
                        ) : null}
               <div id="nc-ia-audit" className="nc-bill-list">
                   {this.getLoading()}
               </div>
            </div>
        )
    }
}
JobEditTable = createPage({
    billinfo:{
        billtype: 'grid',
        pagecode: pagecode,
        bodycode: tableId
    },
})(JobEditTable);
export {JobEditTable}


//ReactDOM.render(<JobEditTable />, document.querySelector('#app'));
export default JobEditTable;