/**
 * 小应用主键
 */
export const appcode = '200401APM';

/**
 * 页面编码
 */
export const pagecodeCard = '200401APM_FCT1-01_card';
/**
 * 页面编码
 */
export const pagecode = '200401APM_FCT1-01_list';
/**
 * 查询模板主键
 */
export const oid = '0001Z31000000001NI6M';
/**
 * 查询区域
 */
export const searchId = '200401APM';
/**
 * 表体区域
 */
export const tableId = 'fct_ap';
/**
 * 单据类型
 */
export const billtype = 'FCT1';
/**
 * 功能节点编码
 */
export const funcode = '200401APM';
/**
 * 节点编码
 */
export const nodekey = 'FCT1-01';

/**
 * 单页应用缓存
 */
export const dataSource  = 'fct.bill.ap.200401APM';
/**
 * 单页变更历史应用缓存
 */
export const dataSourceHistory  = 'fct.bill.ap.200401APM.history';
/**
 * 单页应用缓存主键名字
 */
export const pkname  = 'pk_fct_ap';
/**
 * 多语文件编码
 */
export const multiLangCode = '200401APM';
/**
 * 页面表格类型
 */
export const tableTypeObj = 'simpleTable';