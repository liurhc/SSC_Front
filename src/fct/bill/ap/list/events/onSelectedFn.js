import { ajax, toast } from 'nc-lightapp-front';
export default function onSelectedFn(props, moduleId, record,index,status) {
    let checkData = props.table.getCheckedRows(moduleId);
    let buttonShow = [];
    let buttonNoEdit = [];
    let buttonOprShow = [];
    //清空所有按钮不可编辑的状态
    clearButtonState(props);
    if(checkData.length <= 0)
    {
        //为选中数据
        props.button.setButtonDisabled(['delete','drop_commit','drop_doaction','drop_linkquery','drop_more','uncommit','export'],true);
        return;
    }else if (checkData.length==1){
        let fstatusflag = checkData[0].data.values.fstatusflag.value; 
        switch (fstatusflag) {
            //自由
            case '0':
            buttonShow = [ 'add', 'delete','drop_commit','commit','drop_doaction','drop_fzgn','file','drop_yxgl','yxck','yxsm','yxpj','drop_linkquery','drop_print','print','preview','output'];
            buttonNoEdit = ['uncommit','drop_approve','approve','unapprove','approveinfo','validate','cancelvalidate','freeze','unfreeze','terminate','unterminate','modify','modifyhistory','billlinkquery'];
            props.button.setButtonVisible(buttonShow, true);//显示按钮
            props.button.setButtonDisabled(buttonNoEdit,true);//不可编辑按钮
            break;
            //生效
            case '1':
                buttonShow = ['add','drop_doaction','cancelvalidate','freeze','unfreeze','terminate','unterminate','modify','drop_fzgn','file','modifyhistory','drop_yxgl','yxck','yxsm','yxpj','drop_linkquery','billlinkquery','drop_print','print','preview','output','approveinfo']
                buttonNoEdit = ['delete','drop_commit','commit','uncommit','drop_approve','approve','unapprove','validate','import']
                props.button.setButtonVisible(buttonShow, true);//显示按钮
                props.button.setButtonDisabled(buttonNoEdit,true);//不可编辑按钮
                break;
            //审批中
            case '2':
                //buttonShow = ['add','uncommit','drop_approve','approve','unapprove','approveinfo','drop_doaction','drop_fzgn','file','drop_yxgl','yxck','yxsm','yxpj','drop_print','print','preview','output','drop_linkquery'];
                buttonNoEdit = ['delete','drop_commit','commit','validate','cancelvalidate','freeze','unfreeze','terminate','unterminate','modify','modifyhistory','billlinkquery','import'];
                //props.button.setButtonVisible(buttonShow, true);//显示按钮
                props.button.setButtonDisabled(buttonNoEdit,true);//不可编辑按钮
                break;
            //审批通过
            case '3':
                buttonShow = ['add','unapprove','approveinfo','uncommit','drop_doaction','validate','drop_fzgn','file','drop_yxgl','yxck','yxsm','yxpj','drop_print','print','preview','output','drop_linkquery'];
                //buttonNoEdit = ['delete','drop_commit','commit','uncommit','drop_approve','approve','cancelvalidate','freeze','unfreeze','terminate','unterminate','modify','modifyhistory','billlinkquery','import','export'];
                buttonNoEdit = ['delete','drop_commit','commit','drop_approve','approve','cancelvalidate','freeze','unfreeze','terminate','unterminate','modify','modifyhistory','billlinkquery','import'];
                props.button.setButtonVisible(buttonShow, true);//显示按钮
                props.button.setButtonDisabled(buttonNoEdit,true);//不可编辑按钮
                break;
            //审批未通过
            case '4':
                buttonShow = ['add','drop_approve','approve','unapprove','approveinfo','drop_doaction','drop_fzgn','file','drop_yxgl','yxck','yxsm','yxpj','drop_print','print','preview','output','drop_linkquery','uncommit'];
                buttonNoEdit = ['delete','drop_commit','commit','validate','cancelvalidate','freeze','unfreeze','terminate','unterminate','modify','modifyhistory','billlinkquery','import'];
                props.button.setButtonVisible(buttonShow, true);//显示按钮
                props.button.setButtonDisabled(buttonNoEdit,true);//不可编辑按钮
                break;
            //冻结
            case '5':
                buttonShow = ['add','drop_doaction','unfreeze','drop_fzgn','file','drop_yxgl','yxck','yxsm','yxpj','drop_print','print','preview','output','drop_linkquery','billlinkquery','approveinfo','modifyhistory'];
                buttonNoEdit = ['delete','drop_commit','commit','uncommit','drop_approve','approve','unapprove','validate','cancelvalidate','freeze','terminate','unterminate','modify','import'];
                props.button.setButtonVisible(buttonShow, true);//显示按钮
                props.button.setButtonDisabled(buttonNoEdit,true);//不可编辑按钮
                break;
            //终止
            case '6':
                buttonShow = ['add','drop_doaction','unterminate','drop_fzgn','file','drop_yxgl','yxck','yxsm','yxpj','drop_print','print','preview','output','drop_linkquery','billlinkquery','approveinfo','modifyhistory'];
                buttonNoEdit = ['delete','drop_commit','commit','uncommit','drop_approve','approve','unapprove','validate','cancelvalidate','freeze','unfreeze','terminate','modify','import'];
                props.button.setButtonVisible(buttonShow, true);//显示按钮
                props.button.setButtonDisabled(buttonNoEdit,true);//不可编辑按钮
                break;
                default:
                console.log(fstatusflag);
                break;
            }
    } 
}
/**
 * 每次切换按钮时，去掉上次按钮状态
 */

export const clearButtonState = (props) => {
    let buttonAll = ['add', 'delete','drop_commit','commit','drop_doaction','drop_fzgn','file','drop_yxgl','yxck','yxsm','yxpj','drop_linkquery','drop_print','print','preview','output','uncommit','drop_approve','approve','unapprove','approveinfo','validate','cancelvalidate','freeze','unfreeze','terminate','unterminate','modify','modifyhistory','billlinkquery','drop_importandexport','import','export','drop_more'];
    props.button.setButtonDisabled(buttonAll,false);//肩部全部可编辑按钮
    let buttonOprAll =  ['editline','deleteline','copyline','commitline','unapproveline','approveline','uncommitline','validateline','approveinfoline','unterminateline','modifyline','billlinkqueryline'];
    props.button.setButtonVisible(buttonOprAll, true);//操作列按钮全部隐藏按钮

}
