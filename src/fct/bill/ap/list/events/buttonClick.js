import { ajax, toast, print,output, promptBox,getBusinessInfo,cacheTools } from 'nc-lightapp-front';
import refreshFun from './refreshFun';
import { batchOperation ,setApListSessionStorage} from '../../../../public/components/pubUtils/billPubUtil.js';
import { imageScan, imageView } from 'sscrp/rppub/components/image';
import { tableId, billtype, funcode, nodekey, pagecode, searchId, appcode } from '../constants';
export default function buttonClick(props, id) {
	let that = this;
	let index1 = [];
	let rows;
	let data;
	let url;
	switch (id) {
		case 'add':
			props.pushTo('/card', {
				status: 'add',
				appcode: props.getSearchParam('c')
			});
			break;
		case 'delete':
			batchDelRow(that, props);
			break;
		case 'refresh':
			refreshFun(props);
			toast({ title: that.state.json['200401APM-000033'], color: 'success' });/* 国际化处理： 刷新成功*/
			break;
		case 'print':
			onPrint(that,props);
            break;
        case 'output':
			onOutput(that,props);
			break;   
		case 'validate':
		case 'cancelvalidate':
		case 'freeze':
		case 'unfreeze':
		case 'terminate':
		case 'unterminate':
		case 'commit':
		case 'drop_commit':
		case 'uncommit':
		    batchOperation(props, tableId, billtype,pagecode,id,that);
			break;
		case 'modify':
			onModify(that,props);
			break;
		case 'modifyhistory':
			onModifyHistory(that,props);
			break;
		//单据追溯
		case 'billlinkquery':
		    that.openBillTrack(props);
			break;
		//附件上传
		case 'file':
			that.openfile(props);
			break;
		//审批详情
		case 'approveinfo':
			that.openApprove(props);
			break;
		//执行情况
		case 'impletatusar':
		setApListSessionStorage(props,tableId);
			props.openTo('/fct/report/ap_detail/main/index.html',{
				status:'browse',
				appcode:'200401APSD'
				})
			break;
		case 'yxsm'://影像扫描
			let billdata = props.table.getCheckedRows(tableId)[0];
			// let openbillid = props.getUrlParam('id');
			console.log(billdata);
			var billInfoMap = {};
			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = billdata.data.values.pk_fct_ap.value;
			billInfoMap.pk_billtype = billdata.data.values.cbilltypecode.value;
			billInfoMap.pk_tradetype = billdata.data.values.vtrantypecode.value;
			billInfoMap.pk_org = billdata.data.values.pk_org.value;
	
			//影像所需 FieldMap
			billInfoMap.BillType = billdata.data.values.vtrantypecode.value;
			billInfoMap.BillDate = billdata.data.values.dbilldate.value;
			billInfoMap.Busi_Serial_No = billdata.data.values.pk_fct_ap.value;
			billInfoMap.pk_billtype = billdata.data.values.cbilltypecode.value;
			billInfoMap.OrgNo = billdata.data.values.pk_org.value;
			billInfoMap.BillCode = billdata.data.values.vbillcode.value;
			billInfoMap.OrgName = billdata.data.values.pk_org.value;
			billInfoMap.Cash = billdata.data.values.ntotalorigmny.value;
			imageScan(billInfoMap, 'iweb');
			break;
	  	case 'yxck': //影像查看
	  		let ckbilldata = props.table.getCheckedRows(tableId)[0];
			// let ckopenbillid = props.getUrlParam('id');
			console.log(ckbilldata);
			var billInfoMap = {};
			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = ckbilldata.data.values.pk_fct_ap.value;
			billInfoMap.pk_billtype = ckbilldata.data.values.cbilltypecode.value;
			billInfoMap.pk_tradetype = ckbilldata.data.values.vtrantypecode.value;
			billInfoMap.pk_org = ckbilldata.data.values.pk_org.value;
			imageView(billInfoMap,  'iweb');
		
			break;           
		default:
			console.log(id);
			break;
	}
}
/**
 * 打印
 */
export const onPrint = (that,props) => {
	let rows = props.table.getCheckedRows(tableId);
	let index1 = [];
	rows.forEach((ele, index) => {
                let pk = ele.data.values.pk_fct_ap;
		index1.push(pk.value);
	});
	if (index1.length == 0) {
		toast({ content: `${that.state.json['200401APM-000020']}，${that.state.json['200400ARM-000035']}！`, color: 'warning' });/* 国际化处理： 未选中任何行,不允许操作打印*/
		return;
	}
	print(
		'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
		'/nccloud/fct/ap/print.do', //后台服务url
		{
			billtype: billtype, //单据类型
			funcode: funcode, //功能节点编码，即模板编码
			nodekey: nodekey, //模板节点标识
			oids: index1, // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			userjson: billtype //单据类型,billtype不是必需字段，后台没有设置接收字段，以userjson代替
		}
	);
};
/**
  * 打印输出
  */
export const onOutput = (that,props) => {
	let rows = props.table.getCheckedRows(tableId);
	let index1 = [];
	rows.forEach((ele, index) => {
		let pk = ele.data.values.pk_fct_ap;
		index1.push(pk.value);
	});
	if (index1.length == 0) {
		toast({ content: `${that.state.json['200401APM-000020']}，${that.state.json['200400ARM-000036']}！`, color: 'warning' });/* 国际化处理： 未选中任何行,不允许操作打印输出*/
		return;
	} // 在你想要触发的事件里面调用output方法，必须传的参数有data， 选择传的参数有url（要打印调用的后台接口），callback（打印后的回调函数）
	output({
		url: '/nccloud/fct/ap/printoutput.do',
		data: {
			outputType: 'output', //输出类型
			funcode: funcode, //功能节点编码，即模板编码
			nodekey: nodekey, //模板节点标识
			oids: index1 // 功能节点的数据主键 oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
		},
	});
};
/**
 * 批量删除
 * @param {*} props 
 */
export const batchDelRow = (that,props) => {
	let delObjs = getAllCheckedData(props,tableId);
	if (delObjs.length == 0) {
		toast({ content: `${that.state.json['200401APM-000037']}`, color: 'warning' });/* 国际化处理： 请先选中需要删除的数据*/
		return;
	}
	promptBox({
		color: 'warning',
		title: that.state.json['200401APM-000067'],
		content: that.state.json['200401APM-000068'],/* 国际化处理： 确定要删除所选数据吗?*/
		beSureBtnClick: function() {
			ajax({
				url: '/nccloud/fct/ap/deletecard.do',
				data: delObjs,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data) {
							//1为全部成功 2//为部分成功
							if (data.flag == "1") {
                                toast({
                                    color: 'success',
									content: data.errMsg
								});
                            }
                            else if (data.flag == "2") {
                                toast({
									duration:  'infinity',
                                    color: 'danger',
                                    content:data.errMsg
                                });
                            }
							//删除当前行数据
							props.table.deleteTableRowsByIndex(tableId, data.successIndexs);
							//删除缓存数据
							props.table.deleteCacheId(tableId, data.successPKs);
						}
					}
				}
			});
		}
	});
};

//获取选中数据信息
export const getAllCheckedData = (props, tableId)=> {
	let checkedData = props.table.getCheckedRows(tableId);
	let checkedObj = [];
	checkedData.forEach((val) => {
		checkedObj.push({
			pk_bill: val.data.values.pk_fct_ap.value,
			ts: val.data.values.ts.value,
			index: val.index,
			pageId: pagecode
		});
	});
	return checkedObj;
};
/**
 * 变更
 */
export const onModify = (that,props) => {
	let rows = props.table.getCheckedRows(tableId);
	let index1 = [];
	let pk;
	rows.forEach((ele, index) => {
		pk = ele.data.values.pk_fct_ap;
		index1.push(pk.value);
	});
	if (index1.length != 1) {
		toast({ content: `${that.state.json['200401APM-000038']}，${that.state.json['200401APM-000039']}！`, color: 'warning' });/* 国际化处理： 未选中任何行或选中多行,不允许操作变更*/
		return;
	}
	promptBox({
		color: 'warning',
		content: that.state.json['200401APM-000014'],/* 国际化处理： 是否确认要变更该合同？*/
		beSureBtnClick: function() {
			props.pushTo('/card', {
				status: 'modify',
				id: pk.value,
				appcode: props.getSearchParam('c')
			});
		}
	});
};
/**
 * 变更历史
 */
export const onModifyHistory = (that,props) => {
	let rows = props.table.getCheckedRows(tableId);
	let index1 = [];
	let pk;
	rows.forEach((ele, index) => {
		pk = ele.data.values.pk_fct_ap.value;
		index1.push(pk);
	});
	if (index1.length != 1) {
		toast({ content: `${that.state.json['200401APM-000038']}，${that.state.json['200401APM-000040']}！`, color: 'warning' });/* 国际化处理： 未选中任何行或选中多行,不允许操作变更历史*/
		return;
	}
	let data = { pk: pk,from:'list'};
    let  url = '/nccloud/fct/ap/modifyhistorylist.do';
    ajax({
        url: url,
        data: data,
        success: (res) => {
              if(res.data)
              {
				props.pushTo(
					'/history', { 
					  status:'modifyhistory',
					  id: pk,
					  appcode:props.getSearchParam('c'),
					  from:'list'
					})
              }
              else
              {
                  toast({ color: 'danger', content: that.state.json['200401APM-000066'] });/* 国际化处理： 此合同还没有变更历史!*/
			  }
			}
		})
};