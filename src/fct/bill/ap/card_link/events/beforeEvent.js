import { ajax, toast } from 'nc-lightapp-front';
import toggleShow from './toggleShow';
import { formId,signinfo, tableId1, tableId2, tableId3, tableId4, tableId5, tableId6, pageId, oid, appcode } from '../constants';
export default function beforeEvent(props, moduleId, key, value, data) {
	let that = this;
	let meta = props.meta.getMeta();
	//财务组织
	let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value; 
	meta[formId].items.map((item) => {
		//财务组织
		if (item.attrcode == 'pk_org' && key == 'pk_org') {
		    //item.isShowDisabledData = false;
			item.isShowUnit = false;
		}
		//合同类型
		if (item.attrcode == 'ctrantypeid' && key == 'ctrantypeid') {
			item.queryCondition = () => {
				return { parentbilltype: 'FCT1' }; // 根据单据类型过滤
			}
		}
		//承办人员过滤
		if (item.attrcode == 'personnelid' && key == 'personnelid') {
			//承办组织
			let pk_dept = props.form.getFormItemsValue(formId, 'organizer').value; 
			item.queryCondition = () => {
				return { 
					pk_org: pk_org,
					pk_dept:pk_dept
				}; 
			}
		}
		//承办部门版本
		if (item.attrcode == 'depid_v' && key == 'depid_v') {
			item.isShowUnit = false;
			item.queryCondition = () => {
				return { 
					pk_org: pk_org
				}; 
			}
		}
		//项目过滤
		if (item.attrcode == 'cprojectid' && key == 'cprojectid') {
			//财务组织
			item.queryCondition = () => {
				return { 
					pk_org: pk_org
				}; 
			}
		}
		//付款协议
		if(item.attrcode == 'pk_payterm' && key == 'pk_payterm')
		{
			item.queryCondition = () => { 
                return{
					pk_org:pk_org,
				};
			}
		}
	})
	meta[signinfo].items.map((item) => {
		//供应商档案
		if (item.attrcode == 'cvendorid' && key == 'cvendorid') {
			item.isShowDisabledData = false;
		}
		//对方银行账户
		if (item.attrcode == 'bankaccount' && key == 'bankaccount') {
			debugger;
			item.queryCondition = () => {
				let pk_cust = props.form.getFormItemsValue(moduleId, 'cvendorid').value;
				let pk_currtype = props.form.getFormItemsValue(moduleId, 'corigcurrencyid').value; 
                return{
					accclass:"3",
					pk_cust:pk_cust,
					pk_currtype:pk_currtype
				};
			}
		}
		//本方银行账号
		if (item.attrcode == 'ourbankaccount' && key == 'ourbankaccount') {
			item.queryCondition = () => {
				return { 
					refnodename: that.state.json['200401APM-000001'],/* 国际化处理： 使用权参照*/
					pk_org: pk_org
			    }; // 根据单据类型过滤
			}
		}
	})
	props.meta.setMeta(meta);
	return true;
}
