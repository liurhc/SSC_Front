/**
 * 小应用主键
 */
export const appcode = '200401APM';

/**
 * 页面编码
 */
export const pagecode = '200401APM_FCT1-01_list';
/**
 * 表体区域
 */
export const tableId = 'fct_ap';
/**
 * 单据类型
 */
export const billtype = 'FCT1';
/**
 * 功能节点编码
 */
export const funcode = '200401APM';
/**
 * 节点编码
 */
export const nodekey = 'FCT1-01';

/**
 * 多语文件编码
 */
export const multiLangCode = '200401APM';
/**
 * 页面表格类型
 */
export const tableTypeObj = 'simpleTable';