import React, { Component } from 'react';
import { createPage,ajax,base,getMultiLang} from 'nc-lightapp-front';
import {initTemplate,doubleClick,pageInfoClick} from './events';
const {NCAffix} = base;
import {multiLangCode,pagecode,tableId} from './constants';
import './index.less'

/**
 * author songjqk
 *
 */
class HistoryTable extends Component {
    constructor(props){
        super(props);
        this.state={
            billtype:null,
            json:{}
        }
        this.pks;
    }
    componentDidMount() {
        let _this = this;
        let pk = _this.props.getUrlParam('id');
        let pageInfo = _this.props.table.getTablePageInfo(tableId);
        let data = {
            pageid: pagecode,
            pk:pk,
            pageInfo: pageInfo
        };
        ajax({
            url: '/nccloud/fct/ap/modifyhistorylist.do',
            data: data,
            success: function(res) {
                let { success, data } = res;
                if(data){
                    _this.props.table.setAllTableData(tableId,res.data[tableId]);
                        }else{
    
                            let nulldata = {
                                    rows:[]
                                }
                                _this.props.table.setAllTableData(tableId,nulldata);
                        }
            }
        });  
    }
    componentWillMount(){
        let  callback= (json) =>{
            this.setState({json:json},()=>{
                initTemplate.call(this, this.props);
           })
       }
       getMultiLang({moduleId: multiLangCode, currentLocale: 'simpchn',domainName: 'fct',callback})
    }
    handleClick= () => {
        //来源于列表
        this.props.pushTo(
            '/list', 
            {appcode:this.props.getSearchParam('c')
            })
        
    }
    render() {
        const {table} = this.props;
        const { createSimpleTable } = table;
        const { createBillHeadInfo } = this.props.BillHeadInfo;

        /* 按钮适配 第二步: 从 props.button中取到 createButtonApp 方法用来创建按钮组*/
        return (
            <div className="nc-bill-list">
                <NCAffix>
                    <div className="nc-bill-header-area">
                        <div className="header-title-search-area">
                             <h2 className="title-search-detail">{this.state.json['200401APM-000063']}</h2>{/* 国际化处理： 变更历史*/}
                        </div>
                    </div>
                </NCAffix>
                {/* 列表区 */}
                <div className="nc-bill-table-area ar-table">
                    {createSimpleTable(tableId, {
                        handlePageInfoChange: pageInfoClick.bind(this),
                        onRowDoubleClick: doubleClick.bind(this),
                        showCheck:true,
                        showIndex:true
                    })}
                </div>
            </div>
        )
    }
}
HistoryTable = createPage({
    billinfo:{
        billtype: 'grid',
        pagecode: pagecode,
        bodycode: tableId
    },
})(HistoryTable);

ReactDOM.render(<HistoryTable />, document.querySelector('#app'));
