import { ajax,deepClone} from 'nc-lightapp-front';
import toggleShow from './toggleShow';
import { InsertLine, DelLine, Open, CopyLine, BatchCopy,push,gerRowNo,resetAccountnum,deepCloneCleanData } from '../../../../public/components/pubUtils/billPubUtil.js';
import {dataSource,pkname,formId,tableId1,tableId2,tableId3,tableId4,tableId5,tableId6,pageId,oid,appcode,billtype,funcode,nodekey} from '../constants';
export default  function tableButtonClick (props, key, text, record, index,tableId){    
    let that = this;
    let parentPK=that.props.form.getFormItemsValue(formId, 'pk_fct_ap').value;
    switch (key) {
          //行 展开
        case 'openline':
          Open(props, tableId, index, record, 'edit');
          break;
        //行 删除
        case 'deleteline':
            DelLine(props, tableId, index, key,that);
            if(tableId== tableId3){
              resetAccountnum(props,tableId);
             }
        break;
        //行 增行
        case 'insertline':
              let CardData = props.createExtCardData(pageId, formId, [
                tableId1,
                tableId2,
                tableId3,
                tableId4,
                tableId5,
                tableId6
              ]);
              CardData.userjson = tableId;
              ajax({
                url: '/nccloud/fct/ap/addline.do',
                data: CardData,
                success: (res) => {
                    if (res.data && res.data.bodys) {
                        var rowData = res.data.bodys[tableId].rows;
                        if(tableId == tableId1)
                        {
                          //合同基本需要增加行号
                          var crowno = gerRowNo(props,tableId,index,1)
                          rowData.forEach((item) => {
                            item.values.crowno.value = crowno[0];
                          });
                        }
                       props.cardTable.addRow(tableId, index+1, rowData[0].values,false);
                       if(tableId== tableId3){
                        resetAccountnum(props,tableId);
                       }
                    }
                }
            });
            break;
         //行 复制
        case 'copyline':
            //子表主键
            let pk='';
            if (tableId == 'pk_fct_ap_b') {
              pk='pk_fct_ap_b';
            } else if (tableId == 'pk_fct_ap_term') {
              pk='pk_fct_ap_term';
            }else if (tableId == 'pk_fct_ap_plan') {
              pk='pk_fct_ap_plan';
            }else if (tableId == 'pk_fct_ap_memora') {
              pk='pk_fct_ap_memora';
            }else if (tableId == 'pk_fct_ap_change') {
              pk='pk_fct_ap_change';
            }else if (tableId == 'pk_fct_ap_exec') {
              pk='pk_fct_ap_exec';
            }
             //清空子表主键值、ts
             let selectRows  = [];
             selectRows.push(record);
             var deepCloneData =  deepCloneCleanData(selectRows,pk,props,tableId,index);
              CopyLine(props, tableId, index, key,that,deepCloneData);
             if(tableId== tableId3){
              resetAccountnum(props,tableId);
             }
            break;    
        case 'switchview': case 'closeline':
          if('switchview' == key)
            {
            that.state.index = index;
            }
            else{
            that.state.index = null;
            }
            if(tableId == tableId1)
            {
              that.state.switchflag1 = !that.state.switchflag1;
            }
            else if(tableId == tableId2)
            {
              that.state.switchflag2 = !that.state.switchflag2;
            }
            else if(tableId == tableId3)
            {
              that.state.switchflag3 = !that.state.switchflag3;
            }
            else if(tableId == tableId4)
            {
              that.state.switchflag4 = !that.state.switchflag4;
            }
            else if(tableId == tableId5)
            {
              that.state.switchflag5 = !that.state.switchflag5;
            }
            else if(tableId == tableId6)
            {
              that.state.switchflag6 = !that.state.switchflag6;
            }
            props.cardTable.toggleRowView(tableId, record);
            break;
        case 'push':
            push(props,record,'FCT1');
            break;
             //行 粘贴至此
        case 'copyatline':
            BatchCopy(props, tableId, '2', index ,key,that);
            if (tableId == 'pk_fct_ap_b') {
                that.state.copyflag1=false;
                toggleShow(that,props);
              } else if (tableId == 'pk_fct_ap_term') {
                that.state.copyflag2=false; 
                toggleShow(that,props);
              }else if (tableId == 'pk_fct_ap_plan') {
                that.state.copyflag3=false; 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ap_memora') {
                that.state.copyflag4=false; 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ap_change') {
                that.state.copyflag5=false; 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ap_exec') {
                that.state.copyflag6=false; 
                toggleShow(that,props);              
              }
              if(tableId== tableId3){
                resetAccountnum(props,tableId);
               }
        break;
        default:
            console.log(key);
            break;
    }
};
