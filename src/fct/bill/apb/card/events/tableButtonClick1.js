import { gerRowNo,BatchDelLine,BatchCopy,resetAccountnum,deepCloneCleanData,push} from '../../../../public/components/pubUtils/billPubUtil.js';
import toggleShow from './toggleShow';
import { ajax, toast ,cacheTools,deepClone} from 'nc-lightapp-front';
import {dataSource,pkname,formId,tableId1,tableId2,tableId3,tableId4,tableId5,tableId6,pageId,oid,appcode,billtype,funcode,nodekey} from '../constants';
export default  function tableButtonClick1 (props,key,tableId){
    let that = this;
    let parentPK=that.props.form.getFormItemsValue(formId, 'pk_fct_ap').value;
    switch (key) {
        // 肩部删除
        case 'deletelines1': case 'deletelines2': case 'deletelines3':
        case 'deletelines4': case 'deletelines5': case 'deletelines6':             
            BatchDelLine(props,tableId, key,that);
            setTableButton(props,tableId)
            if(tableId== tableId3){
                resetAccountnum(props,tableId);
            }
            break;
        //肩部添加    
        case 'addlines1':case 'addlines2':case 'addlines3':case 'addlines4':
        case 'addlines5':case 'addlines6':
            let tableNum = props.cardTable.getNumberOfRows(tableId);
            let CardData = props.createExtCardData(pageId, formId, [
                tableId1,
                tableId2,
                tableId3,
                tableId4,
                tableId5,
                tableId6
              ]);
              CardData.userjson = tableId;   
            ajax({
                url: '/nccloud/fct/ap/addline.do',
                data: CardData,
                success: (res) => {
                    if (res.data && res.data.bodys) {
                        var rowData = res.data.bodys[tableId].rows;
                        if(tableId == tableId1)
                        {
                          //合同基本需要增加行号
                          var crowno = gerRowNo(props,tableId,null,1)
                          rowData.forEach((item) => {
                            item.values.crowno.value = crowno[0];
                          });
                        }
                       props.cardTable.addRow(tableId, tableNum,rowData[0].values,false);
                       if(tableId== tableId3){
                        resetAccountnum(props,tableId);
                       }
                    }
                }
            });
            break;
        //肩部 复制
        case 'copylines1':case 'copylines2':case 'copylines3':case 'copylines4':
        case 'copylines5':case 'copylines6':
            let selectRows = props.cardTable.getCheckedRows(tableId);
            if (selectRows == null || selectRows.length == 0) {
                toast({
                'color': 'warning',
                'content': that.state.json['200401APM-000023']/* 国际化处理： 未选中要复制的行*/
                });
                return false;
            }
            //子表主键
            let pk='';
            if (tableId == 'pk_fct_ap_b') {
                pk='pk_fct_ap_b';
                that.state.copyflag1=true 
                toggleShow(that,props);
              } else if (tableId == 'pk_fct_ap_term') {
                pk='pk_fct_ap_term';
                that.state.copyflag2=true 
                toggleShow(that,props);
              }else if (tableId == 'pk_fct_ap_plan') {
                pk='pk_fct_ap_plan';
                that.state.copyflag3=true 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ap_memora') {
                pk='pk_fct_ap_memora';
                that.state.copyflag4=true 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ap_change') {
                pk='pk_fct_ap_change';
                that.state.copyflag5=true 
                toggleShow(that,props);              
              }else if (tableId == 'pk_fct_ap_exec') {
                pk='pk_fct_ap_exec';
                that.state.copyflag6=true 
                toggleShow(that,props);              
              }
            //清空子表主键值、ts
            var deepCloneData =  deepCloneCleanData(selectRows,pk,props,tableId,undefined);
            cacheTools.set('copyData', deepCloneData);
            if(tableId== tableId3){
                resetAccountnum(props,tableId);
            }
            break;
    //肩部 粘贴至末行
    case 'pastetoendlines1':case 'pastetoendlines2':case 'pastetoendlines3':
    case 'pastetoendlines4':case 'pastetoendlines5':case 'pastetoendlines6':
        BatchCopy(props,tableId,'2',undefined,key,that);
        if (tableId == 'pk_fct_ap_b') {
            that.state.copyflag1=false;
            toggleShow(that,props);
        } else if (tableId == 'pk_fct_ap_term') {
            that.state.copyflag2=false; 
            toggleShow(that,props);
        }else if (tableId == 'pk_fct_ap_plan') {
            that.state.copyflag3=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ap_memora') {
            that.state.copyflag4=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ap_change') {
            that.state.copyflag5=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ap_exec') {
            that.state.copyflag6=false; 
            toggleShow(that,props);              
        }
        if(tableId== tableId3){
            resetAccountnum(props,tableId);
           }
      break;
    //肩部 取消
    case 'cancellines1':case 'cancellines2':case 'cancellines3':case 'cancellines4':
    case 'cancellines5':case 'cancellines6':
        if (tableId == 'pk_fct_ap_b') {
            that.state.copyflag1=false;
            toggleShow(that,props);
        } else if (tableId == 'pk_fct_ap_term') {
            that.state.copyflag2=false; 
            toggleShow(that,props);
        }else if (tableId == 'pk_fct_ap_plan') {
            that.state.copyflag3=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ap_memora') {
            that.state.copyflag4=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ap_change') {
            that.state.copyflag5=false; 
            toggleShow(that,props);              
        }else if (tableId == 'pk_fct_ap_exec') {
            that.state.copyflag6=false; 
            toggleShow(that,props);              
        }
    //肩部 计划页签生效
    case 'pushs':
          let records = props.cardTable.getCheckedRows(tableId);
          push(props,records,'FCT1');
          break;    
        default:
            console.log(key);
            break;
    }
};


function setTableButton(props,moduleId){
    switch(moduleId){
		case 'pk_fct_ap_b':
			let tableButton1 = ['deletelines1','copylines1'];//,'pastetoendlines1','cancellines1'
			props.button.setButtonDisabled(tableButton1,checkTableDataLen(props, moduleId));
			break;
		case 'pk_fct_ap_term':
			let tableButton2 = ['deletelines2','copylines2'];//,'pastetoendlines2','cancellines2'
			props.button.setButtonDisabled(tableButton2,checkTableDataLen(props, moduleId));
			break;
		case 'pk_fct_ap_plan':
			let tableButton3 = ['deletelines3','copylines3'];//,'pastetoendlines3','cancellines3'
			props.button.setButtonDisabled(tableButton3,checkTableDataLen(props, moduleId));
			break;
		case 'pk_fct_ap_memora':
			let tableButton4 = ['deletelines4','copylines4'];//,'pastetoendlines4','cancellines4'
			props.button.setButtonDisabled(tableButton4,checkTableDataLen(props, moduleId));
			break;
	}
}

function checkTableDataLen(props, moduleId){
    let selectLen = props.cardTable.getCheckedRows(moduleId).length
    if(selectLen==undefined||selectLen==0) {
        return true
    }
    return false
}
