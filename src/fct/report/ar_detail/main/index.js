import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';
import {getFirstOrgValue} from '../../../public/components/pubUtils/billPubUtil.js';

export default class ArReport extends Component {
    constructor(props) {
        super(props);
    }

    disposeSearch(meta, props) {
        let orgFiltFields = [
			'pk_fct_ar_b.pk_srcmaterial',
			'pk_fct_ar_b.pk_srcmaterial.code',
			'pk_fct_ar_b.pk_srcmaterial.name',
			'cprojectid',
			'pk_fct_ar_b.inoutcome',
			'pk_fct_ar_b.project'
		];
		
		let items = meta['light_report'].items;
		items.forEach((item) => {
			if (item.attrcode == 'ctrantypeid') {
				// 合同类型过滤
				item.queryCondition = () => {
					return { parentbilltype: 'FCT2' };
				};
			}else if (item.attrcode == 'pk_customer') {
                //客户
                item.queryCondition = () => {
                    var pk_org = null;
                    if(props.search.getSearchValByField('light_report', 'pk_org'))
                    {
                        pk_org = getFirstOrgValue((props.search.getSearchValByField('light_report', 'pk_org') || {}).value.firstvalue);
                    }
                    return { 
                        DataPowerOperationCode: 'fi',
                        isDataPowerEnable: 'Y',
                        pk_org: pk_org
                    };
                }
            }   else if (orgFiltFields.includes(item.attrcode)) {
				//组织过滤
				item.queryCondition = () => {
                    var pk_org = null;
                    if(props.search.getSearchValByField('light_report', 'pk_org'))
                    {
                        pk_org = getFirstOrgValue((props.search.getSearchValByField('light_report', 'pk_org') || {}).value.firstvalue);
                    }
                    return { pk_org: pk_org };
				};
            }
            else if (item.attrcode == 'pk_org') {
				item.queryCondition = () => {
					item.isShowUnit = false;
					return {
						DataPowerOperationCode: 'fi', //使用权组
						AppCode: props.getSearchParam('c'),
						TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
					};
				};
			}
		});
		return meta; // 处理后的过滤参照返回给查询区模板
    }

    //查询字段的默认值设置
    setDefaultVal(searchId, props) {
        //查询区默认显示字段值
        debugger
        let  pk_org =null;
        let  pk_orgplay=null;
        // let  pk_fct_ar='';
        if(sessionStorage.getItem('LinkReport') && JSON.parse(sessionStorage.getItem('LinkReport'))){
            pk_org=JSON.parse(sessionStorage.getItem('LinkReport')).conditions["0"].value.firstvalue;
            pk_orgplay=JSON.parse(sessionStorage.getItem('LinkReport')).conditions["0"].display;
            // pk_fct_ar=JSON.parse(sessionStorage.getItem('LinkReport')).conditions["1"].value.firstvalue;
            props.search.setSearchValByField(searchId, 'pk_org', { value: pk_org,display:pk_orgplay}, '=');
        }
       
        // props.search.setSearchValByField(searchId, 'pk_fct_ar', { value: pk_fct_ar,display:pk_fct_ar}, '=');
    }



    /**
   * 
   * @param  items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
   */
    expandSearchVal(items) {
        // 变量赋值拓展
        console.log(items);
        if (items && items.length > 0) {
            items.forEach((item) => {
                if (item.field == 'user_name') {
                    // 需要进行拓展的变量
                    if (item.value.firstvalue == '11') {
                        let obj = {
                            //obj对象内oprtype为between时firstvalue,secondvalue都有值，其他情况只有firstvalue有值
                            field: 'user_other',
                            oprtype: 'like',
                            value: { firstvalue: '111', secondvalue: '222' }
                        };
                        items.push(obj);
                    }
                }
            });
        }
        return items;
    }

    render() {
        return (
            <div className="table">
                <SimpleReport
                    showAdvBtn={true}
                    disposeSearch={this.disposeSearch.bind(this)}
                    setDefaultVal={this.setDefaultVal.bind(this)}
                />
            </div>
        );
    }
}

ReactDOM.render(<ArReport />, document.getElementById('app'));