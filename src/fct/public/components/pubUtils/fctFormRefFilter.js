/**
 * 卡片态表头字段过滤 以及 代码控制可编辑性
 * @param {*} moduleId 区域id
 * @param {*} props 当前props
 * @param {*} key 操作的键
 * @param {*} data 当前表单所有值
 */
function formBeforeEvent(props, moduleId, key, value) {
    let meta = props.meta.getMeta();
    let pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
    let pk_dept = props.form.getFormItemsValue(this.formId, 'depid_v') ? props.form.getFormItemsValue(this.formId, 'depid_v').value : null;
    let pk_cust = props.form.getFormItemsValue(this.formId, 'cvendorid') ? props.form.getFormItemsValue(this.formId, 'cvendorid').value : null;
    let pk_currtype = props.form.getFormItemsValue(this.formId, 'corigcurrencyid') ? props.form.getFormItemsValue(this.formId, 'corigcurrencyid').value : null; 
    var config = {
        DataPowerOperationCode: '',//使用权
        isDataPowerEnable: 'Y',//使用权限
        pk_org: '',//财务组织
        parentbilltype:'',
        pk_dept:'',
        accclass:'',
        pk_cust:'',
        pk_currtype:'',
        refnodename:'',
        corigcurrencyid:'',
        GridRefActionExt:'',
        busifuncode:''
    }
    var vtrantypecode = '';
    if('fct_ar' == this.formId)
    {
        vtrantypecode = 'FCT2';
    }else if('fct_ap' == this.formId)
    {
        vtrantypecode = 'FCT1';
    }

    meta[this.formId].items.map((item) => {
        item.isShowUnit = false;
        item.isShowDisabledData = false;
        var attrcode = item.attrcode;
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = false;
        }
        if (attrcode == key) {
            attrcode = attrcode.indexOf('vdef') == -1 ? attrcode:'vdef';
            switch (attrcode) {
                case 'pk_org'://财务组织(根据集团过滤)
                    item.queryCondition = (p) => {
                        item.isShowUnit = false;
                        config.DataPowerOperationCode = 'fi';
                        config.AppCode = props.getSearchParam('c');
                        config.data = '';
                        config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                        return config;
                    }
                    break;
                case 'pk_payterm'://收款协议    
                    item.isShowUnit = false;
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
                case 'ctrantypeid'://合同类型
                    item.queryCondition = (p) => {
                        config.parentbilltype = vtrantypecode
                        return config;
                    }
                    break;    
                case 'personnelid'://承办人员
                    item.queryCondition = (p) => {
                        //config.busifuncode = 'all';
                        config.DataPowerOperationCode = 'fi';
                        config.pk_org = pk_org;
                        config.isDataPowerEnable = 'Y';
                        return config;
                    }
                    break;
                case 'depid_v'://承办部门版本
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
                case 'cprojectid'://项目过滤
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
                case 'vdef'://自定义项 加上组织过滤
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
            }
        }

    });
    //签约信息页签参照
    meta[this.signinfo].items.map((item) => {
        item.isShowUnit = false;
        item.isShowDisabledData = false;
        var attrcode = item.attrcode;
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = false;
        }
        if (attrcode == key) {
            attrcode = attrcode.indexOf('vdef') == -1 ? attrcode:'vdef';
            switch (attrcode) {
                case 'bankaccount':
                    if('FCT2' == vtrantypecode)
                    {
                        item.queryCondition = (p) => {
                            //本方银行账户
                            config.refnodename = this.state.json['200400ARM-000001'],/* 国际化处理： 使用权参照*/
                            config.pk_org = pk_org,
                            config.corigcurrencyid = pk_currtype,
                            config.GridRefActionExt = 'nccloud.web.fct.bill.action.BankAccountGridSqlBuilder'
                            return config;
                        }
                    }
                    else if('FCT1' == vtrantypecode)
                    {//对方银行账户
                        
                        item.refName= this.state.json['2004common-000003']/* 国际化处理： 供应商银行账户*/;
                        item.placeholder= this.state.json['2004common-000003']/* 国际化处理： 供应商银行账户*/;
                        item.queryCondition = (p) => {
                            config.accclass = "3",
                            config.pk_cust = pk_cust,
                            config.pk_currtype = pk_currtype
                            return config;
                        }
                    }
                    break;
                case 'ourbankaccount'://本方银行账户
                    item.queryCondition = (p) => {
                        config.refnodename = this.state.json['200401APM-000001'],/* 国际化处理： 使用权参照*/
                        config.pk_org = pk_org,
                        config.corigcurrencyid = pk_currtype,
                        config.GridRefActionExt = 'nccloud.web.fct.bill.action.BankAccountGridSqlBuilder'
                        return config;
                    }
                    break;
                case 'cvendorid'://供应商
                    item.queryCondition = (p) => {
                        config.DataPowerOperationCode = 'fi';
                        config.pk_org = pk_org;
                        //付款单和应付单
                        return config;
                    }
                    break;
                case 'pk_customer'://客户
                    item.queryCondition = (p) => {
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
                case 'vdef'://自定义项默认 加上组织过滤
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
            }
        }
    });
    props.meta.setMeta(meta);
    return true;
}


export { formBeforeEvent }