//根据字段判断是否调用后台

//收款表头
let arHeadBtn = ["openct","pk_customer","subscribedate","valdate",
                 "invallidate","ctrantypeid","nexchangerate",
                 "nglobalexchgrate","ngroupexchgrate","corigcurrencyid",
                 "csendcountryid","crececountryid","ctaxcountryid",
                 "pk_payterm","cprojectid","organizer_v","personnelid","mountcalculation"];
//收款表体
let arBodyBtn = ["delivdate","pk_material","pk_marbasclass","vqtunitrate",
                 "vchangerate","castunitid","cqtunitid",
                 "ftaxtypeflag","ctaxcodeid","moneytype",
                 "accountdate","enddate","norigtaxmny","noriplangpmny","nnum","norigtaxprice","ngtaxprice","ntaxrate"
                 ,"ntotalgpmny","ncopegpmny","noricopegpmny","noritotalgpmny","nastnum","cunitid","norigprice","ngprice","norigmny","ntax"
                 ,"nmny","ncaltaxmny","ntaxmny","nplangpmny","planrate","orgmoney","planmoney","balancemoney","unconfpayableori","norikpmny","unconfpaymentori"];
//付款表头
let apHeadBtn = ["openct","cvendorid","subscribedate","valdate","depid_v",
                 "invallidate","ctrantypeid","nexchangerate",
                 "nglobalexchgrate","ngroupexchgrate","corigcurrencyid",
                 "csendcountryid","crececountryid","ctaxcountryid",
                 "pk_payterm","cprojectid","organizer_v","personnelid","mountcalculation"];
//付款表体
let apBodyBtn = ["delivdate","pk_material","pk_marbasclass","vqtunitrate",
                 "vchangerate","castunitid","cqtunitid",
                 "ftaxtypeflag","ctaxcodeid","moneytype",
                 "accountdate","enddate","norigtaxmny","noriplangpmny","noritotalgpmny","nastnum","cunitid","norigprice"
                 ,"ngprice","norigmny","nmny","ntax","ncaltaxmny","nnosubtax","nplangpmny","ntotalgpmny"
                 ,"ncopegpmny","noricopegpmny","unconfpayableori","norikpmny","nnum","norigtaxprice","ngtaxprice"
                 ,"ntaxmny","nnosubtaxrate","ntotalplanamount","unconfpaymentorg","unconfpayableorg","planrate","planmoney","unconfpaymentori","balancemoney","orgmoney","unconfpaymentorg","balancemoneyorg"];                 

//收款表头过滤                
let fieldHeadAr = function (key) {
        if(arHeadBtn.indexOf(key)!=-1){
            return true;
        }else{
            return false;
        }
}
//收款表体过滤                
let fieldBodyAr = function (key) {
    if(arBodyBtn.indexOf(key)!=-1){
        return true;
    }else{
        return false;
    }
}
//付款表头过滤
let fieldHeadAp = function (key) {
    if(apHeadBtn.indexOf(key)!=-1){
        return true;
    }else{
        return false;
    }
}
//付款表头过滤
let fieldBodyAp = function (key) {
    if(apBodyBtn.indexOf(key)!=-1){
        return true;
    }else{
        return false;
    }
}

export { fieldHeadAr,fieldBodyAr,fieldHeadAp,fieldBodyAp }