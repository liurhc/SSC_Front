import { toast, ajax, cacheTools,deepClone,promptBox,getBusinessInfo } from 'nc-lightapp-front';
/**
 * FCT 前端按钮操作公共api
 * @author tangleic
 * @version 1.0
 */

/**
* 新增行
* @param {*} props 页面内置对象
* @param {*} areacode 区域编码
* @param {*} afterFunc 增行处理逻辑
*/
export const AddLine = function (props, tableId,rowNum) {
    if(tableId == "pk_fct_ar_b" || tableId == "pk_fct_ap_b")
    {
        var rownoArr = gerRowNo(props,tableId,null,1);
        props.cardTable.addRow(tableId, rowNum,{ "crowno": {value:rownoArr[0]} },false);
        
    }
    else
    {
        props.cardTable.addRow(tableId, rowNum,{ rows: [] },false);
    }
 
}

/**
 * 插入行
 * @param {*} props 页面内置对象
 * @param {*} areacode 区域编码
 * @param {*} index  行索引
 */
export const InsertLine = function (props, tableId, index) {
    if(tableId == "pk_fct_ar_b" || tableId == "pk_fct_ap_b")
    {
        var rownoArr = gerRowNo(props,tableId,index,1);
        props.cardTable.addRow(tableId, index + 1,{ "crowno": {value:rownoArr[0]} },false);
    }
    else
    {
        props.cardTable.addRow(tableId, index + 1,false);
    }
}
/**
 * 批量删除行
 * @param {*} props 平台页面内置对象
 * @param {*} areacode 区域编码
 * @returns 执行结果
 */
export const BatchDelLine = function (props, tableId,key ,that) {
    let selectRows = props.cardTable.getCheckedRows(tableId);
    if (selectRows == null || selectRows.length == 0) {
        toast({
            'color': 'warning',
            'content': that.state.json['2004common-000001']/* 国际化处理： 未选中要删除的行*/
        });
        return false;
    }
    let selectIndexs = [];
    for (let item of selectRows) {
        selectIndexs.push(item.index);
    }
    props.cardTable.delRowsByIndex(tableId, selectIndexs);
    calculateBodyMoney(props,tableId);
    return true;
}

/**
 * 删除行
 * @param {*} props 页面内置对象
 * @param {*} areacode 区域编码 
 * @param {*} index 行索引
 */
export const DelLine = function (props, tableId, index,key ,that) {
    props.cardTable.delRowsByIndex(tableId, index);
    calculateBodyMoney(props,tableId);
}

/**
 * 删除行2 删行后，不重新设置表体行的金额
 * @param {*} props 页面内置对象
 * @param {*} areacode 区域编码 
 * @param {*} index 行索引
 */
export const DelLine2 = function (props, tableId, index,key ,that) {
    props.cardTable.delRowsByIndex(tableId, index);
}

/**
 * 展开行
 * @param {*} props 页面内置对象
 * @param {*} areacode 区域编码 
 * @param {*} index  行索引
 * @param {*} record  行数据
 * @param {*} status 显示模式
 */
export const Open = function (props, tableId, index, record, status) {
    props.cardTable.openModel(tableId, status, record, index);
}

/**
 * 批量复制
 * @param {*} props 页面内置对象
 * @param {*} areacode 区域编码
 * @param {*} index 粘贴至此行的index,末行不需要传
 * @param {*} tableNum 表格号码1-6 代表
 *  @param {*} type 表格类型 1--收款  2--付款
 */
export const BatchCopy = function (props,tableId,type,index,key,that) {
    let deepCloneData =  cacheTools.get('copyData');
    if(index == undefined || index == 'undefined'){
        index = props.cardTable.getNumberOfRows(tableId, false)-1;
    }
    //合同基本子表处理
    if(tableId == "pk_fct_ar_b" || tableId == "pk_fct_ap_b")
    {
        var rownoArr = gerRowNo(props,tableId,index,deepCloneData.length);
        var i = 0;
        deepCloneData.forEach((item) => {
            item.values.crowno.value = rownoArr[i];//更新行号
            i++;
        });
    }
    props.cardTable.insertRowsAfterIndex(tableId, deepCloneData, index);

    calculateBodyMoney(props,tableId);
    return true;
}

/**
 * 复制行
 * @param {*} props 页面内置对象
 * @param {*} areacode 区域编码
 * @param {*} index  行索引
 */
export const CopyLine = function (props, tableId, index,key ,that,deepCloneData) {
    //合同基本子表处理
    if(tableId == "pk_fct_ar_b" || tableId == "pk_fct_ap_b")
    {
        var rownoArr = gerRowNo(props,tableId,index,deepCloneData.length);
        var i = 0;
        deepCloneData.forEach((item) => {
            item.values.crowno.value = rownoArr[i];//更新行号
            i++;
        });
    }
    props.cardTable.insertRowsAfterIndex(tableId, deepCloneData, index);
    calculateBodyMoney(props,tableId);
}

/**
 * 清理复制的行
 * @param {*} deepCloneData 复制的新行
 * @param {*} pk pk
 */
export const deepCloneCleanData = function (selectRows,pk,props,tableId,index) {
    let selectIndexs = [];
    var num = 0;

    for (let selectRow of selectRows) {
        if(selectRow.data!=undefined){
            selectRow.data.selected = false;
            selectIndexs.push(selectRow.data);
        }else{
            selectIndexs.push(selectRow);
        }
        num++;
    }
    if(index == undefined || index == 'undefined'){
        index = props.cardTable.getNumberOfRows(tableId, false)-1;
    }
    var deepCloneData = deepClone(selectIndexs);
    deepCloneData.forEach((item) => {
        for( let attr in item.values){
            item.values[pk] = {value:null,display:null};
        }
    });
    //合同基本子表处理
    if(pk == "pk_fct_ar_b" || pk == "pk_fct_ap_b")
    {
        // var rownoArr = gerRowNo(props,tableId,index,num);
        // var i = 0;
        deepCloneData.forEach((item) => {
            //保留的字段
            for( let attr in item.values){
                item.values.ncopegpmny = {value:null,display:null};
                item.values.noricopegpmny = {value:null,display:null};
                item.values.noriplangpmny = {value:null,display:null};
                item.values.noritotalgpmny = {value:null,display:null};
                item.values.nplangpmny = {value:null,display:null};
                item.values.ntotalgpmny = {value:null,display:null};
                item.values.unconfpayableori = {value:null,display:null};
                item.values.unconfpayableorg = {value:null,display:null};
                item.values.norikpmny = {value:null,display:null};
                item.values.unconfpaymentori = {value:null,display:null};
                item.values.unconfpaymentorg = {value:null,display:null};
            };
            // item.values.crowno.value = rownoArr[i];
            // i++;
        });
    }
    if(pk == "pk_fct_ar_plan" || pk == "pk_fct_ap_plan")
    {
        deepCloneData.forEach((item) => {
            //保留的字段
            for( let attr in item.values){
                item.values.balancemoney = {value:null,display:null};
                item.values.norikpmny = {value:null,display:null};
                item.values.balancemoneyorg = {value:null,display:null};
                item.values.unconfpaymentori = {value:null,display:null};
                item.values.unconfpaymentorg = {value:null,display:null};
            };
        });
    }
    return deepCloneData;
}
//计算单据表体金额
export const calculateBodyMoney = function (props,tableId){
        let formId = '';
        let tableId1 = '';
        let tableId3 = '';

        if (tableId == 'pk_fct_ar_b' || tableId == 'pk_fct_ar_plan') {
            formId = 'fct_ar';
            tableId1 = 'pk_fct_ar_b';
            tableId3 = 'pk_fct_ar_plan';
        }
        else if(tableId == 'pk_fct_ap_b' || tableId == 'pk_fct_ap_plan') 
        {
            formId = 'fct_ap';
            tableId1 = 'pk_fct_ap_b';
            tableId3 = 'pk_fct_ap_plan';
        }else{
            return;
        }

        let data1 = props.cardTable.getVisibleRows(tableId1);//合同基本
        let data3 = props.cardTable.getVisibleRows(tableId3);//合同计划

        //原币价税合计
        var norigtaxmny = getBodyAmountValue(formId, tableId1, data1, 'norigtaxmny');
        //本币价税合计
        var ntaxmny = getBodyAmountValue(formId, tableId1, data1, 'ntaxmny');
        //计划比例 
        var planrate = getBodyAmountValue(formId, tableId3, data3, 'planrate');
        
        var scale = 0;
        var planmoney = 0;
        var orgmoney = 0;
        var planmoneyRate = 0;
        props.beforeUpdatePage();//打开开关
        for (var j = 0; j < data3.length; j++) {
            planmoneyRate = data3[j].values.planrate.value;
            scale =data3[j].values.planrate.scale;
            planmoneyRate = parseFloat(planmoneyRate);
            if (!isNaN(planmoneyRate)) {
                planmoney = (planmoneyRate/100)*norigtaxmny;
                planmoney = planmoney.toFixed(scale);
                orgmoney = (planmoneyRate/100)*ntaxmny;
                orgmoney = orgmoney.toFixed(scale);
                //计划金额 
                props.cardTable.setValByKeyAndIndex(tableId3,j,'planmoney',{value: planmoney });
                //组织本币金额 
                props.cardTable.setValByKeyAndIndex(tableId3,j,'orgmoney',{value: orgmoney });
            }
        }
        props.form.setFormItemsValue(formId, { ntotalorigmny: { value: norigtaxmny} });
        props.form.setFormItemsValue(formId, { ntotaltaxmny : { value: ntaxmny} });  
        props.updatePage(formId, [tableId1,tableId3]);//统一的state更新
};
//获取单据表体行某个字段金额合计值
export const getBodyAmountValue = function (formId, tableId, data, fieldcode) {
	if (data) {
		var amount = 0;
        let scale = 0;
        var rowData =  data;
		for (let i = 0; i < rowData.length; i++) {
			let val = rowData[i].values[fieldcode].value;
			scale = rowData[i].values[fieldcode].scale;
			val = parseFloat(val);
			if (!isNaN(val)) {
				amount = amount + val;
			}
        }
		amount = amount.toFixed(scale);
		return amount;
	}
};

//推单
export const push = function (props,records,billtype) {
    let pkArr = [];
    let flag = records.length == undefined ? false:true;//true 代表传入数组，false代表传入为单条数据
    if('FCT1'==billtype){
        if(flag)
        {
            let pk_fct_ap = records[0].data.values['pk_fct_ap'].value;
            pkArr.push(pk_fct_ap);
            records.forEach((record) => {
                let pk_fct_ap_plan = record.data.values['pk_fct_ap_plan'].value;
                pkArr.push(pk_fct_ap_plan);
            });
        }
        else
        {
            let pk_fct_ap = records.values['pk_fct_ap'].value;
            let pk_fct_ap_plan = records.values['pk_fct_ap_plan'].value;
            pkArr.push(pk_fct_ap);
            pkArr.push(pk_fct_ap_plan);
        }
        cacheTools.set("fct1ToF3Pks", pkArr);
        let data = {
            "pkArr":pkArr,
        }
        ajax({
			url: '/nccloud/fct/ap/filterfct1toap.do',
			data: data,
			success: (res) => {
				if (res.success) {
					props.openTo('/nccloud/resources/arap/paybill/paybill/main/index.html#/card', {
                        status: 'add',
                        srcbilltype:'FCT1', 
                        appcode:'20080EBM',
                        pagecode:'20080EBM_CARD'
                    })
				}
				}
		});
        
    }
    if('FCT2'==billtype){
        if(flag)
        {
            let pk_fct_ar = records[0].data.values['pk_fct_ar'].value;
            pkArr.push(pk_fct_ar);
            records.forEach((record) => {
                let pk_fct_ar_plan = record.data.values['pk_fct_ar_plan'].value;
                pkArr.push(pk_fct_ar_plan);
            });
        }
        else
        {
            let pk_fct_ar = records.values['pk_fct_ar'].value;
            let pk_fct_ar_plan = records.values['pk_fct_ar_plan'].value;
            pkArr.push(pk_fct_ar);
            pkArr.push(pk_fct_ar_plan);
        }
        cacheTools.set("fct2ToF2Pks", pkArr);
        let data = {
            "pkArr":pkArr,
        }
        ajax({
			url: '/nccloud/fct/ap/filterfct2toar.do',
			data: data,
			success: (res) => {
				if (res.success) {
					props.openTo('/nccloud/resources/arap/gatheringbill/gatheringbill/main/index.html#/card', { 
                        status: 'add',
                        srcbilltype:'FCT2',
                        pagecode:'20060GBM_CARD',
                        appcode:'20060GBM'
                    })
				}
				}
		});
        
    }
};
/**
 * 
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} checknum  选中行号
 * @param {*} changenum 改变行数
 */
export const gerRowNo = function(props,tableId,checknum,changenum) {
    let finishno = [];
    let tableData = props.cardTable.getVisibleRows(tableId);
    let allno = [];
    let aftercheckno;
    if(tableData)
    {
        for (let item of tableData) {
            allno.push(item.values.crowno.value);
        }
    }
    if(allno.length == 0){
        finishno.push('10');
    }else{
        if(checknum==null){
            for (let i = 1; i <= changenum; i++) {
                finishno.push(String(parseFloat(allno[allno.length-1])+parseFloat(10*i)));
            }   
        }
        else{
            let checkno = allno[checknum];
            let num = 10;//默认以10增长
            if(parseFloat(allno.length-1)>parseFloat(checknum))
            {
                aftercheckno = allno[checknum+1];
                num = ((parseFloat(aftercheckno)-parseFloat(checkno))/(changenum+1));
            }
            for (let i = 1; i <= changenum; i++) {
                finishno.push(String(parseFloat(checkno)+parseFloat(num*i)));
            }
        }
    }
        return finishno;
}
    /**
     * 
     * @param {*} props 
     * @param {*} tableId   表格id
     * @param {*} billType  单据类型
     * @param {*} pageId     页面编码
     * @param {*} actionType 动作类型
     * @param {*} record    行记录
     */
export const batchOperation = function(props, tableId, billType,pageId,actionType,that) {
    //表头操作
    let batchParams  = getAllCheckedData(props, tableId, billType,pageId,actionType);
    let businessDates = getBusinessInfo().businessDate.substring(0,10);
    let flag  = false;
    let ajaxUrl = '/nccloud/fct/bill/batchAction.do';
    if (batchParams.length == 0) {
        toast({ color: 'warning', content: that.state.json['2004common-000002']/* 国际化处理： 请选中至少一行数据!*/ });
        return;
    }
    if(actionType === 'validate'){
        batchParams.forEach((item) => {
            if(businessDates<item.valdate.substring(0,10)){
                flag=true;
            }
        });
    }else if ( actionType ===  'cancelvalidate' || actionType ===  'freeze' || actionType ===  'unfreeze'  || actionType ===  'unterminate' ){
        flag=true;
    }
    promptBox({
        color:"warning",content:getTemp(billType,null,that)+getTemp(billType,actionType,that)+"?",/* 国际化处理： 是否确定要使该合同*/
        title:getTemp(billType,actionType,that),
        beSureBtnClick:function(){
            if(flag){
                that.setState({
                    url:ajaxUrl,
                    doTableId:tableId, 
                    doBillType:billType,
                    doPageId:pageId,
                    doActionType:actionType,
                    doTemp:getTemp(billType,actionType,that)
                },()=>{
                    props.modal.show('onDoAction');
                })
            }else{
                that.setState({
                    showloading: true,
                    title:getTemp(billType,actionType,that)+that.state.json['2004common-000004']/* 国际化处理： "中，请耐心等待...*/
                })
                ajax({
                    url: ajaxUrl,
                    data: batchParams,
                    loading:false,
                    success: (res) => {
                        let { success, data } = res;
                        that.setState({
                            showloading: false
                        })
                        if(res.data.workflow && (res.data.workflow == 'approveflow'|| res.data.workflow == 'workflow'))
                        {
                            that.setState({
                                url:ajaxUrl, 
                                commitdata:batchParams,
                                compositedata:res.data,
                                compositedisplay:true,
                                doBillType:billType
                            }) ;
                        }else{
                            if (success) {
                                if (data.grid) {
                                    let grid = data.grid;
                                    let updateValue = [];
                                    for (let key in grid) {
                                        updateValue.push({ index: key, data: { values: grid[key].values } });
                                    }
                                    props.table.updateDataByIndexs(tableId, updateValue);
                                }
                                //1为全部成功 2//为部分成功
                                if (data.flag == "1") {
                                    toast({
                                        color: 'success',
                                        title: getTemp(billType,actionType,that)+getTemp(billType,'success',that),
                                        content: data.message
                                    });
                                }
                                else if (data.flag == "2") {
                                    toast({
                                        duration:  'infinity',
                                        color: 'danger',
                                        content:data.message
                                    });
                                }
                            }
                        }
                    },
                    error: (res) => {
                        that.setState({
                            showloading: false
                        })
                        toast({ color: 'danger', content: res.message });
                    }
                });
            }         
        }})
};    
//获取选中数据的id和billType
export const getAllCheckedData = function(props, tableId, billType,pageId,actionType,onDoActionValue) {
    let checkedData = props.table.getCheckedRows(tableId);
    let checkedObj = [];
    let temp = '';
    if(billType == 'FCT2')
    {
        //收款
        checkedData.forEach((val) => {
            checkedObj.push({
                pk_bill: val.data.values.pk_fct_ar.value,
                ts: val.data.values.ts.value,
                billType: billType,
                index: val.index,
                pageId: pageId,
                actionCode:actionType,
                valdate:val.data.values.valdate.value,
                onDoActionValue:onDoActionValue,
                assignObj:null
            });
        });
    }
    else if(billType == 'FCT1')
    {
        //付款
        checkedData.forEach((val) => {
            checkedObj.push({
                pk_bill: val.data.values.pk_fct_ap.value,
                ts: val.data.values.ts.value,
                billType: billType,
                index: val.index,
                pageId: pageId,
                actionCode:actionType,
                valdate:val.data.values.valdate.value,
                onDoActionValue:onDoActionValue,
                assignObj:null
            });
        });
    }
    return checkedObj;
    };

//获取选中数据的temp
export const getTemp = function(billType,id,that) {
    let temp = '';
    if(billType == 'FCT2')
    {
        if(id ===  'drop_commit'){
            temp = that.state.json['200400ARM-000016'];/* 国际化处理： 提交*/
        }
        else if(id ===  'uncommit'){
            temp = that.state.json['200400ARM-000015'];/* 国际化处理： 收回*/
        }
        else if(id ===  'validate')
        {
            temp = that.state.json['200400ARM-000006'];/* 国际化处理： 生效*/
        }
        else if(id ===  'cancelvalidate')
        {
           temp = that.state.json['200400ARM-000007'];/* 国际化处理： 取消生效*/
        }
        else if(id ===  'freeze')
        {
            temp = that.state.json['200400ARM-000008'];/* 国际化处理： 冻结*/
        }
        else if(id ===  'unfreeze')
        {
            temp = that.state.json['200400ARM-000009'];/* 国际化处理： 解冻*/
        }
        else if(id ===  'terminate')
        {
            temp = that.state.json['200400ARM-000010'];/* 国际化处理： 终止*/
        }
        else if(id ===  'unterminate')
        {
            temp = that.state.json['200400ARM-000011'];/* 国际化处理： 取消终止*/
        }
        else if(id ===  'success'){
            temp = that.state.json['200400ARM-000019'];/* 国际化处理： 成功!*/
        }
        else
		{
			temp = that.state.json['200400ARM-000012'];/* 国际化处理： 是否确定要使该合同*/
		}
    }
    else if(billType == 'FCT1')
    {
        if(id ===  'drop_commit'){
            temp = that.state.json['200401APM-000016'];/* 国际化处理： 提交*/
        }
        else if(id ===  'uncommit'){
            temp = that.state.json['200401APM-000015'];/* 国际化处理： 收回*/
        }
        else if(id ===  'validate')
        {
            temp = that.state.json['200401APM-000006'];/* 国际化处理： 生效*/
        }
        else if(id ===  'cancelvalidate')
        {
           temp = that.state.json['200401APM-000007'];/* 国际化处理： 取消生效*/
        }
        else if(id ===  'freeze')
        {
            temp = that.state.json['200401APM-000008'];/* 国际化处理： 冻结*/
        }
        else if(id ===  'unfreeze')
        {
            temp = that.state.json['200401APM-000009'];/* 国际化处理： 解冻*/
        }
        else if(id ===  'terminate')
        {
            temp = that.state.json['200401APM-000010'];/* 国际化处理： 终止*/
        }
        else if(id ===  'unterminate')
        {
            temp = that.state.json['200401APM-000011'];/* 国际化处理： 取消终止*/
        }
        else if(id ===  'success'){
            temp = that.state.json['200401APM-000019'];/* 国际化处理： 成功!*/
        }
        else
		{
			temp = that.state.json['200401APM-000012'];/* 国际化处理： 是否确定要使该合同*/
		}
    }
    return temp;
};    

//获取选中的业务组织的第一个
export const  getFirstOrgValue = function(orgValues) {
    let pkOrgValue = '';
    if (orgValues != null) {
        let orgArray = orgValues.split(',');
        if (orgArray != null && orgArray.length > 0) {
            pkOrgValue = orgArray[0];
        }
    }
    return pkOrgValue;
}

//--------------------------------AR收款------------------------------------------------------------
//给报表查询条件缓存到SessionStorage--card
export const  setCardSessionStorage = function(props,formId) {
    let  pk_org1 = props.form.getFormItemsValue(formId, 'pk_org').value;
    let  pk_orgplay1 = props.form.getFormItemsValue(formId, 'pk_org').display;
    let  pk_fct_ar1=props.form.getFormItemsValue(formId, 'pk_fct_ar').value;
    let  field1={
        "field":"pk_org",
        "value":{
            "firstvalue":pk_org1,
            "secondvalue":null
            },
        "oprtype":"=",
        "display":pk_orgplay1
       }
    let  field2={
        "field":"pk_fct_ar",
        "value":{
            "firstvalue":pk_fct_ar1,
            "secondvalue":null
            },
        "oprtype":"=",
        "display":pk_fct_ar1
       }
    var  searchwhere={ logic: 'and', conditions: [field1,field2] }
    sessionStorage.setItem('LinkReport', JSON.stringify(searchwhere));
}
//给报表查询条件缓存到SessionStorage--list --- AR
export const  setListSessionStorage = function(props,tableId) {
        let  checkedrows = props.table.getCheckedRows(tableId);
        if(checkedrows.length==0){
            return;
        }
        let  pkarrs='';
        let  pkorgarrs='';
        let  pkorgnamearrs='';
        checkedrows.forEach((val) => {
            pkarrs+=(val.data.values.pk_fct_ar.value+',');
            pkorgarrs+=(val.data.values.pk_org.value+',');
            pkorgnamearrs+=(val.data.values.pk_org.display+',');
        });
        pkarrs=  pkarrs.substring(0,pkarrs.lastIndexOf(","));
        pkorgarrs=  pkorgarrs.substring(0,pkorgarrs.lastIndexOf(","));
        pkorgnamearrs=  pkorgnamearrs.substring(0,pkorgnamearrs.lastIndexOf(","));
        // let queryInfo = cacheTools.get('queryInfo');
        let  field1={
            "field":"pk_org",
            "value":{
                "firstvalue":pkorgarrs,
                "secondvalue":null
                },
            "oprtype":"=",
            "display":pkorgnamearrs
            }
        let  field2={
            "field":"pk_fct_ar",
            "value":{
                "firstvalue":pkarrs,
                "secondvalue":null
                },
            "oprtype":"=",
            "display":pkarrs
            }
        // var  searchwhere={ logic: 'and', conditions: [queryInfo.querycondition.conditions["0"],field2] };
        var  searchwhere={ logic: 'and', conditions: [field1,field2] };
        sessionStorage.setItem('LinkReport', JSON.stringify(searchwhere));
}
//--------------------------------AP付款------------------------------------------------------------

//给报表查询条件缓存到SessionStorage--AP----card
export const  setAPCardSessionStorage = function(props,formId) {
    let  pk_org1 = props.form.getFormItemsValue(formId, 'pk_org').value;
    let  pk_orgplay1 = props.form.getFormItemsValue(formId, 'pk_org').display;
    let  pk_fct_ap1=props.form.getFormItemsValue(formId, 'pk_fct_ap').value;
    let  field1={
        "field":"pk_org",
        "value":{
            "firstvalue":pk_org1,
            "secondvalue":null
            },
        "oprtype":"=",
        "display":pk_orgplay1
       }
    let  field2={
        "field":"pk_fct_ap",
        "value":{
            "firstvalue":pk_fct_ap1,
            "secondvalue":null
            },
        "oprtype":"=",
        "display":pk_fct_ap1
       }
    var  searchwhere={ logic: 'and', conditions: [field1,field2] }
    sessionStorage.setItem('LinkReport', JSON.stringify(searchwhere));
}
//给报表查询条件缓存到APSessionStorage--AP---list 123
export const  setApListSessionStorage = function(props,tableId) {
        let  checkedrows = props.table.getCheckedRows(tableId);
        if(checkedrows.length==0){
            return;
        }
        let  pkarrs='';
        let  pkorgarrs='';
        let  pkorgnamearrs='';
        checkedrows.forEach((val) => {
            pkarrs+=(val.data.values.pk_fct_ap.value+',');
            pkorgarrs+=(val.data.values.pk_org.value+',');
            pkorgnamearrs+=(val.data.values.pk_org.display+',');
        });
        pkarrs=  pkarrs.substring(0,pkarrs.lastIndexOf(","));
        pkorgarrs=  pkorgarrs.substring(0,pkorgarrs.lastIndexOf(","));
        pkorgnamearrs=  pkorgnamearrs.substring(0,pkorgnamearrs.lastIndexOf(","));
        // let queryInfo = cacheTools.get('queryInfo');
        let  field1={
            "field":"pk_org",
            "value":{
                "firstvalue":pkorgarrs,
                "secondvalue":null
                },
            "oprtype":"=",
            "display":pkorgnamearrs
            }
        let  field2={
            "field":"pk_fct_ap",
            "value":{
                "firstvalue":pkarrs,
                "secondvalue":null
                },
            "oprtype":"=",
            "display":pkarrs
            }
        // var  searchwhere={ logic: 'and', conditions: [queryInfo.querycondition.conditions["0"],field2] };
        var  searchwhere={ logic: 'and', conditions: [field1,field2] };
        sessionStorage.setItem('LinkReport', JSON.stringify(searchwhere));
}

//期出号重排
export const resetAccountnum= function(props,tableId){
    let data3 = props.cardTable.getVisibleRows(tableId);//合同计划
    props.beforeUpdatePage();//打开开关
    for (var j = 0; j < data3.length; j++) {
            //组织本币金额 
            props.cardTable.setValByKeyAndIndex(tableId,j,'accountnum',{value: (j+1).toFixed(0) });

    }
    props.updatePage(null, tableId);//统一的state更新
};