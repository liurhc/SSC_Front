

/**
 * 卡片态表头字段过滤 以及 代码控制可编辑性
 * @param {*} moduleId 区域id
 * @param {*} props 当前props
 * @param {*} key 操作的键
 * @param {*} data 当前表单所有值
 */
function tableBeforeEvent(props, moduleId, key, value) {
    let meta = props.meta.getMeta();
    let pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
    let pk_dept = props.form.getFormItemsValue(this.formId, 'organizer') ? props.form.getFormItemsValue(this.formId, 'organizer').value : null;
    let pk_cust = props.form.getFormItemsValue(this.formId, 'cvendorid') ? props.form.getFormItemsValue(this.formId, 'cvendorid').value : null;
    let pk_currtype = props.form.getFormItemsValue(this.formId, 'corigcurrencyid') ? props.form.getFormItemsValue(this.formId, 'corigcurrencyid').value : null; 
    let vtrantypecode = props.form.getFormItemsValue(this.formId, 'vtrantypecode') ? props.form.getFormItemsValue(this.formId, 'vtrantypecode').value : null; 
    var config = {
        DataPowerOperationCode: '',//使用权
        isDataPowerEnable: 'Y',//使用权限
        pk_org: '',//财务组织
        parentbilltype:'',
        pk_dept:'',
        accclass:'',
        pk_cust:'',
        pk_currtype:'',
        refnodename:'',
        corigcurrencyid:'',
        GridRefActionExt:''
    }
    // var vtrantypecode = '';
    // if('fct_ar' == this.formId)
    // {
    //     vtrantypecode = 'FCT2';
    // }else if('fct_ap' == this.formId)
    // {
    //     vtrantypecode = 'FCT1';
    // }
    //合同基本
    meta[this.tableId1].items.map((item) => {
        item.isShowUnit = false;
        item.isShowDisabledData = false;
        var tableId1 =this.tableId1;
        var attrcode = item.attrcode;
        //add by wufl 收支项目添加单据控制规则 向后台添加参数
        var crossrule_datasout = props.createMasterChildData(this.pageId,this.formId,this.tableId1);
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = false;
        }
        if (attrcode == key) {
            attrcode = attrcode.indexOf('vbdef') != -1 || attrcode.indexOf('vfree') != -1 ? 'vbdef':attrcode;
            switch (attrcode) {
                case 'inoutcome'://收支项目
                    debugger;
                    item.queryCondition = (p) => {
                        config.crossrule_datas=JSON.stringify(crossrule_datasout),
                        config.TreeRefActionExt = 'nccloud.web.action.erm.ref.sqlbuilder.PublicDefaultRefSqlBuilder',
                        config.crossrule_itemkey = attrcode,
                        config.DataPowerOperationCode = 'fi',//使用权组
                        config.crossrule_datatypes='billcard'
                        config.crossrule_area=this.tableId1,
                        config.pk_org = pk_org,
                        config.crossrule_org=pk_org,
                        config.crossrule_tradetype=vtrantypecode;
                        return config;
                    }
                    break;
                case 'vbdef'://自定义项 加上组织过滤
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
            }
        }
    });
    //合同基本编辑态
    meta[this.tableId1childform2].items.map((item) => {
        item.isShowUnit = false;
        item.isShowDisabledData = false;
        var attrcode = item.attrcode;       
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = false;
        }
        if (attrcode == key) {
            attrcode = attrcode.indexOf('vbdef') != -1 || attrcode.indexOf('vfree') != -1 ? 'vbdef':attrcode;
            switch (attrcode) {
                case 'inoutcome'://收支项目
                    item.queryCondition = (p) => {
                        config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder',
                        config.itemKey = attrcode,
                        config.DataPowerOperationCode = 'fi',//使用权组
                        config.pk_org = pk_org
                        return config;
                    }
                    break;
                case 'vbdef'://自定义项 加上组织过滤
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
            }
        }
    });
    //合同条款
    meta[this.tableId2].items.map((item) => {
        item.isShowUnit = false;
        item.isShowDisabledData = false;
        var attrcode = item.attrcode;
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = false;
        }
        if (attrcode == key) {
            switch (attrcode) {
                case 'vtermcode'://合同条款
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org
                        return config;
                    }
                    break;
                default://默认 加上组织过滤
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
            }
        }
    });
    //合同条款编辑态
    meta[this.tableId2childform2].items.map((item) => {
        item.isShowUnit = false;
        item.isShowDisabledData = false;
        var attrcode = item.attrcode;
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = false;
        }
        if (attrcode == key) {
            switch (attrcode) {
                case 'vtermcode'://合同条款
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org
                        return config;
                    }
                    break;
                default://默认 加上组织过滤
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
            }
        }
    });
    //合同计划
    meta[this.tableId3].items.map((item) => {
        item.isShowUnit = false;
        item.isShowDisabledData = false;
        var attrcode = item.attrcode;
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = false;
        }
        if (attrcode == key) {
            switch (attrcode) {
                case 'pk_incomeperiod'://起效依据
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org
                        return config;
                    }
                    break;
                default://默认 加上组织过滤
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
            }
        }
    });
    //合同计划编辑状态
    meta[this.tableId3childform2].items.map((item) => {
        item.isShowUnit = false;
        item.isShowDisabledData = false;
        var attrcode = item.attrcode;
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = false;
        }
        if (attrcode == key) {
            switch (attrcode) {
                case 'pk_incomeperiod'://起效依据
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org
                        return config;
                    }
                    break;
                default://默认 加上组织过滤
                    item.queryCondition = (p) => {
                        config.pk_org = pk_org;
                        return config;
                    }
                    break;
            }
        }
    });
    props.meta.setMeta(meta);
    return true;
}


export { tableBeforeEvent }