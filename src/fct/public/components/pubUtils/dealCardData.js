


let dealCardData = (that,cardData) =>{
    let newCardData = JSON.parse(JSON.stringify(cardData));
    let head = newCardData.head[that.formId].rows[0];
    let bodys1 =  newCardData.bodys[that.tableId1].rows;
    let bodys2 =  newCardData.bodys[that.tableId1].rows;
    let bodys3 =  newCardData.bodys[that.tableId1].rows;
    let bodys4 =  newCardData.bodys[that.tableId1].rows;
    let bodys5 =  newCardData.bodys[that.tableId1].rows;
    let bodys6 =  newCardData.bodys[that.tableId1].rows;

    dealHead(head ,newCardData);
    dealbodys(bodys1 ,newCardData);
    dealbodys(bodys2 ,newCardData);
    dealbodys(bodys3 ,newCardData);
    dealbodys(bodys4 ,newCardData);
    dealbodys(bodys5 ,newCardData);
    dealbodys(bodys6 ,newCardData);

    return newCardData;

}

let dealHead = (head) =>{
    let values = head.values;
    Object.keys(values).map((key,index) => {
        //清空value和display
        let value = values[key].value;
        let display = values[key].display;
        let scale = values[key].scale;
        if((!value || value == '') && (!display || display =='') && scale == '-1'){
            delete values[key];
        }
    });
}

let dealbodys = (bodys) =>{
    let length = bodys.length;
    for(let i = 0 ;i < length ; i ++){
        let body = bodys[i];
        let values = body.values;
        Object.keys(values).map((key) => {
            //清空value和display
            let value = values[key].value;
            let display = values[key].display;
            let scale = values[key].scale;
            if((!value || value == '') && (!display || display =='') && scale == '-1'){
                delete values[key];
            }
        });
    }
}

export { dealCardData };