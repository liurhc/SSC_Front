/**
 * 敞口合同编辑后事件
 * @param {*} that 
 * @param {*} props 
 */
function OpencontracAfterEvent(that,props) {
    //敞口合同
    let openct = props.form.getFormItemsValue(that.formId, 'openct') ? props.form.getFormItemsValue(that.formId, 'openct').value : null; 
    let data1 = props.cardTable.getVisibleRows(that.tableId1);//合同基本
    let data3 = props.cardTable.getVisibleRows(that.tableId3);//合同计划
    let opencontract = false;
    if(null != openct) {
        opencontract = openct;
    }
    if(opencontract){
        let scale = props.form.getFormItemsValue(that.formId,"overrate").scale;
    	//超合同付款比例应该为空并无法编辑
        props.form.setFormItemsValue(that.formId, { overrate: { value: null,scale: scale} });
        props.form.setFormItemsDisabled(that.formId,{overrate:true})
        //合同总金额可以为空
        props.form.setFormItemsRequired(that.formId,{ntotalorigmny:false});
    	//合同计划截至日期可以为空
        props.form.setFormItemsRequired(that.formId,{invallidate:false});
    	//合同实际截至日期可以为空
        props.form.setFormItemsRequired(that.formId,{actualinvalidate:false});
    	//合同总数量可以为空
        props.form.setFormItemsRequired(that.formId,{ntotalastnum:false});
        data1.forEach((item) => {
            //合同表体数量可以为空
            item.values.nastnum.required = false;
            item.values.nnum.required = false;
            //合同表体单价可以为空
            item.values.norigtaxmny.required = false;
            item.values.nqtorigtaxprice.required = false;
            item.values.ngtaxprice.required = false;
        });
        data3.forEach((item) => {
            //合同表体计划金额可以为空
            item.values.planmoney.required = false;
        });
    }else{
        let overrate = props.form.getFormItemsValue(that.formId, 'overrate') ? props.form.getFormItemsValue(that.formId, 'overrate').value : null; 
        if(!overrate)
        {
            //超合同付款比例置为0
            props.form.setFormItemsValue(that.formId, { overrate: { value: 0,scale:0} });    
        }
    	//超合同付款比例可以编辑
        props.form.setFormItemsDisabled(that.formId,{overrate:false})
    	//默认计划截止日期不能为空
        props.form.setFormItemsRequired(that.formId,{invallidate:true});
    	//合同表体计划金额不能为空
        data3.forEach((item) => {
            //合同表体计划金额可以为空
            item.values.planmoney.required = false;
        });
    }
}

export { OpencontracAfterEvent }