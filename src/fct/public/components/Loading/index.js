import React, { Component } from 'react';
import ReactDOM from 'react-dom';
export default class Loading extends Component {
	render() {
		let elm = this.props.show
			? ReactDOM.createPortal(this.props.children, this.props.container || document.body)
			: null;
		return elm;
	}
}
