/*
 * 成本计算Loading公共样式组件
 * @Author: songjqk 
 * @Date: 2019-01-04 16:53:35 
 */
import React, { Component } from 'react';
import { base, ajax, high } from 'nc-lightapp-front';
import Loading from '../Loading';
import loadingImg from './img/loading.png';
import './index.less';
const { NCButton, NCModal } = base;
const { Header, Body, Footer } = NCModal;

export default class AuditLoading extends Component {
	constructor() {
		super();
		this.state = {
			flag: true
		};
	}
	render() {
		let { showloading,title} = this.props;
		return(
			<Loading show={showloading} container={document.getElementById(this.props.container)}>
				<div className="loading-content">
					<img src={loadingImg} alt="" />
					<div>
						<span>
							{title}
						</span>
					</div>
				</div>
			</Loading>
		);
	}
}
