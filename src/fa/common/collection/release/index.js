/**
 * FA公共方法、组件、常量 统一打包输出
 */
//components
import * as AccbookAndPeriod from '../../components/AccbookAndPeriod/AccbookAndPeriod';
import * as CardLinkToBillUtil from '../../components/CardLinkToBillUtil/CardLinkToBillUtil';
import * as DeptMidlevRefFilter from '../../components/DeptMidlevRefFilter/DeptMidlevRefFilter';
import * as ImageMng from '../../components/ImageMng/ImageMng';
import * as ReferFilter from '../../components/ReferFilter/ReferFilter';
import * as ReferLinkage from '../../components/ReferLinkage/ReferLinkage';
import * as VoucherUtils from '../../components/VoucherUtils/VoucherUtils';
import WorkloadModal from '../../components/WorkloadModal/WorkloadModal';

let components = {
    AccbookAndPeriod, // 查询账簿期间的工具类
    CardLinkToBillUtil, //固定资产卡片联查单据统一入口
    DeptMidlevRefFilter, //FA74, 编辑管理部门、使用部门时是否允许选择非末级
    ImageMng, //影像处理工具类
    ReferFilter,//参照过滤
    ReferLinkage,//参照联动处理
    VoucherUtils,//凭证联查
    WorkloadModal,//工作量法 模态框
};

//utils
import * as DefaultDate from '../../components/DefaultDate';
import metaUtil from '../../components/MetaUtil/MetaUtil';

let utils = {
    DefaultDate, //时间类型处理工具类
    metaUtil, //meta 相关的一些工具方法
};
let fa = {
    fa_components: components,
    fa_utils: utils
};

export default fa;