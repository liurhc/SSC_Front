export const config = {
	url: {
		queryAccbookAndPeriodUrl: '/nccloud/fa/fapub/queryAccbookAndPeriod.do', //查询账簿与会计期间url
		queryAllAccbooksAndPeriodsUrl: '/nccloud/fa/fapub/queryAllAccbooksAndPeriods.do', //查询组织下所有账簿及其最小未结账月
		queryAccbookAndPeriodsFxValuesUrl: '/nccloud/fa/report/queryfxvalues.do', //查询函数实际值
		queryOrgByAccbookUrl: '/nccloud/fa/report/queryorgbyaccbook.do', //通过账簿查询组织
		accbookTreeRefUrl: '/nccloud/uapbd/ref/AccountBookTreeRef.do' //资产账簿参照请求路径
	},
	defaultField: {
		pk_org: 'pk_org',
		pk_accbook: 'pk_accbook',
		query_period: 'query_period',
		start_period: 'start_period',
		end_period: 'end_period'
	}
};
