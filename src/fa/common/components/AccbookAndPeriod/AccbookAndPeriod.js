import { config } from './const';
//@zhangypm 切换到正确的导入
import ampub from 'ampub';
import {
	getAccbookAndPeriodBase,
	getAccbooksAndPeriodsBase,
	getAccbookAndPeriodsFxValuesBase,
	getOrgByAccbookBase
} from './AccbookAndPeriodBase';

const { components } = ampub;
const { getContext, loginContextKeys } = components.LoginContext;
export { config };

/**
 * 查询区 通过组织获取账簿，顺便返回会计期间，组织多选或者空选清空账簿
 * @param {*} props
 * @param {*} moduleId 查询区域ID 如果没有特殊情况，后面的所有都可以不传了
 * @param {*} defData 如果不是从查询区获取值，需要直接传值.如果要传回调方法的话，这里传null即可
 * @param {*} defConfig 字段id，如果要传回调方法并且还想用默认字段id的话，这里传null即可
 *    形式：defConfig: {
		pk_org: 'pk_org',
		pk_accbook: 'pk_book',
		query_period: 'query_period',
		start_period: 'start_period',
		end_period: 'end_period'
	}
 * @param {*} success 成功回调方法，可以不传
 * @param {*} error 失败回调方法，可以不传
 */
export async function getAccbookAndPeriod(props, moduleId, defConfig, defData, success, error) {
	let businessDate = getContext(loginContextKeys.businessDate);
	getAccbookAndPeriodBase(props, moduleId, defConfig, defData, businessDate, success, error);
}

/**
 * 获取该财务组织对应的所有账簿及其对应期间
 * @param {*} props
 * @param {*} pk_org 组织主键
 * @param {*} success(props, res) 必须传
 * @param {*} error(props, res) 可以不传
 * @returns 返回成功的格式：
 *    {
 *		data: [
 *			{
 *				accbookName: "用友北京分公司-基准账簿", 
 *				pk_accbook: "1001C2100000000005SC", 
 *				minUnClosebookPeriod: {
 *					accyear: "2018", 
 *					period: "09", 
 *					startdate: "2018-08-01 23:00:00", 
 *					enddate: "2018-09-01 22:59:59", 
 *					status: "0"
 *				}, 
 *				isMainbook: true
 *			},
 *			....
 *		], 
 *		error: null, 
 *		formulamsg: null, 
 *		success: true
 *	}
 */
export async function getAccbooksAndPeriods(pk_org, success, error) {
	let businessDate = getContext(loginContextKeys.businessDate);
	getAccbooksAndPeriodsBase(pk_org, businessDate, success, error);
}

/**
 * 获取查询区的账簿函数和期间函数实际值的方法
 * @param {*} pk_org
 * @param {*} success
 * @param {*} error
 */
export async function getAccbookAndPeriodsFxValues(pk_org, isloading = true, success, error) {
	let businessDate = getContext(loginContextKeys.businessDate);
	getAccbookAndPeriodsFxValuesBase(pk_org, businessDate, isloading, success, error);
}

/**
 * 通过账簿获取组织
 * @param {*} pk_accbook
 * @param {*} isAsync 是否要异步执行
 * @param {*} success
 * @param {*} error
 */
export function getOrgByAccbook(pk_accbook, pk_unit, isAsync = true, success, error) {
	let businessDate = getContext(loginContextKeys.businessDate);
	getOrgByAccbookBase(pk_accbook, pk_unit, businessDate, isAsync, success, error);
}
