/** 不引用任何领域内公共方法的基础方法 */
import { ajax, toast } from 'nc-lightapp-front';
import { config } from './const';

const { url, defaultField } = config;
let fieldConfig = defaultField;
/**
 * 查询区 通过组织获取账簿，顺便返回会计期间，组织多选或者空选清空账簿
 * @param {*} props
 * @param {*} moduleId 查询区域ID 如果没有特殊情况，后面的所有都可以不传了
 * @param {*} defData 如果不是从查询区获取值，需要直接传值.如果要传回调方法的话，这里传null即可
 * @param {*} defConfig 字段id，如果要传回调方法并且还想用默认字段id的话，这里传null即可
 *    形式：defConfig: {
		pk_org: 'pk_org',
		pk_accbook: 'pk_book',
		query_period: 'query_period',
		start_period: 'start_period',
		end_period: 'end_period'
	}
 * @param {*} success 成功回调方法，可以不传
 * @param {*} error 失败回调方法，可以不传
 */
export async function getAccbookAndPeriodBase(props, moduleId, defConfig, defData, businessDate, success, error) {
	if (!success) {
		success = defalutSucess;
	}
	if (!error) {
		error = defalutError;
	}
	fieldConfig = defConfig ? defConfig : defaultField; //获取数值的字段

	let data = {};

	if (defData) {
		data = defData;
	} else {
		let returnData = getData(props, moduleId);
		data = {
			businessDate: businessDate, //业务日期
			...returnData
		};
	}

	//判断是否是多选组织或者空组织，界面无期间，多选和空选清空账簿 界面有期间，多选和空选只清空账簿
	if (!data.pk_org) {
		props.search.setSearchValByField(moduleId, 'pk_accbook', {});
		return;
	} else {
		let pk_orgs = data.pk_org.split(',');
		if (pk_orgs.length > 1) {
			props.search.setSearchValByField(moduleId, 'pk_accbook', {});
			return;
		}
	}

	let meta = props.meta.getMeta();
	let queryCondition = {};
	meta &&
		meta[moduleId] &&
		meta[moduleId].items.map((item) => {
			if (item.attrcode == fieldConfig.pk_accbook) {
				queryCondition = item.queryCondition();
			}
		});

	let refData = {
		queryCondition: {
			treetype: 'main',
			isshow: true,
			disabledDataShow: false,
			isDataPowerEnable: true,
			isshowUnit: false,
			DataPowerOperationCode: 'FADefault',
			...queryCondition
		},
		pageInfo: { pageSize: 10, pageIndex: 1 },
		pid: '',
		keyword: ''
	};

	let dataWithout = await higherAjax(url.queryAccbookAndPeriodUrl, data, error); //没有数据权限的主账簿及其他数据
	let dataWith = await higherAjax(url.accbookTreeRefUrl, refData, error); //有数据权限的账簿

	if (dataWithout.success && dataWithout.data) {
		let pk_accbook = dataWithout.data.accbook_value;
		let dataWithStr = JSON.stringify(dataWith);
		if (dataWithStr && dataWithStr.includes(`"refpk":"${pk_accbook}"`)) {
			if ('function' === typeof success) {
				success(props, dataWithout, moduleId);
			}
		}
	}
}

/**
 * 使用promise封装的ajax
 * @param {*} _url
 * @param {*} data
 */
function higherAjax(_url, data, error, loading = true) {
	return new Promise((reslove, reject) => {
		ajax({
			url: _url,
			data: data,
			loading: loading,
			success: (res) => {
				reslove(res);
			},
			error: (res) => {
				if ('function' === typeof error) {
					error(res);
				}
			}
		});
	});
}

/**
 * 获取值
 * @param {*} props
 * @param {*} moduleId
 */
function getData(props, moduleId) {
	let returnData = {};
	returnData.pk_org = props.search.getSearchValByField(moduleId, fieldConfig.pk_org).value.firstvalue;
	returnData.pk_accbook = props.search.getSearchValByField(moduleId, fieldConfig.pk_accbook).value.firstvalue;
	return returnData;
}

/**
 * 默认的错误处理方法
 * @param {*} props
 * @param {*} res
 */
function defalutError(res) {
	if (res && res.message) {
		toast({ color: 'danger', content: res.message }); //提示错误信息
	}
}

/**
 * 默认的成功处理方法
 * @param {*} props
 * @param {*} res
 */
function defalutSucess(props, res, moduleId) {
	let { success, data } = res;
	if (success) {
		let accbook = {
			value: data.accbook_value,
			display: data.accbook_display
		};
		props.search.setSearchValByField(moduleId, fieldConfig.pk_accbook, accbook); //设置默认账簿
		let { accyear, period } = data.period;
		let date = `${accyear}-${period}`;
		props.search.setSearchValByField(moduleId, fieldConfig.start_period, { value: date, display: date }); //起始期间
		props.search.setSearchValByField(moduleId, fieldConfig.end_period, { value: date, display: date }); //终止期间
		props.search.setSearchValByField(moduleId, fieldConfig.query_period, { value: date, display: date }); //会计期间
	}
}

/**
 * 获取该财务组织对应的所有账簿及其对应期间
 * @param {*} props
 * @param {*} pk_org 组织主键
 * @param {*} success(props, res) 必须传
 * @param {*} error(props, res) 可以不传
 * @returns 返回成功的格式：
 *    {
 *		data: [
 *			{
 *				accbookName: "用友北京分公司-基准账簿", 
 *				pk_accbook: "1001C2100000000005SC", 
 *				minUnClosebookPeriod: {
 *					accyear: "2018", 
 *					period: "09", 
 *					startdate: "2018-08-01 23:00:00", 
 *					enddate: "2018-09-01 22:59:59", 
 *					status: "0"
 *				}, 
 *				isMainbook: true
 *			},
 *			....
 *		], 
 *		error: null, 
 *		formulamsg: null, 
 *		success: true
 *	}
 */
export async function getAccbooksAndPeriodsBase(pk_org, businessDate, success, error) {
	if (!success) {
		success = (res) => {};
	}
	if (!error) {
		error = defalutError;
	}

	let data = {
		pk_org: pk_org,
		businessDate: businessDate
	};

	let refData = {
		queryCondition: {
			treetype: 'type',
			isshow: true,
			disabledDataShow: false,
			isDataPowerEnable: true,
			isshowUnit: false,
			DataPowerOperationCode: 'FADefault',
			TreeRefActionExt: 'nccloud.web.fa.common.refCondition.AccbookOrgRefFilter',
			pk_org: pk_org
		},
		pageInfo: { pageSize: 10, pageIndex: 1 },
		pid: '',
		keyword: ''
	};

	let dataWithout = await higherAjax(url.queryAllAccbooksAndPeriodsUrl, data, error); //没有数据权限的主账簿及其他数据
	let dataWith = await higherAjax(url.accbookTreeRefUrl, refData, error); //有数据权限的账簿

	let realData = {
		data: [],
		error: dataWithout.error,
		formulamsg: dataWithout.formulamsg,
		success: dataWithout.success
	};
	let dataWithStr = JSON.stringify(dataWith);
	if (dataWithout.success && dataWithout.data) {
		dataWithout.data.map((perBook) => {
			let pk_accbook = perBook.pk_accbook;
			if (dataWithStr && dataWithStr.includes(`"refpk":"${pk_accbook}"`)) {
				realData.data.push(perBook);
			}
		});
	}
	if ('function' === typeof success) {
		success(realData);
	}
}

/**
 * 获取查询区的账簿函数和期间函数实际值的方法
 * @param {*} pk_org
 * @param {*} success
 * @param {*} error
 */
export async function getAccbookAndPeriodsFxValuesBase(pk_org, businessDate, isloading = true, success, error) {
	if (!success) {
		success = (res) => {};
	}
	if (!error) {
		error = defalutError;
	}
	let data = {
		pk_org: pk_org,
		businessDate: businessDate
	};

	let refData = {
		queryCondition: {
			treetype: 'main',
			isshow: true,
			disabledDataShow: false,
			isDataPowerEnable: true,
			isshowUnit: false,
			DataPowerOperationCode: 'FADefault',
			TreeRefActionExt: 'nccloud.web.fa.common.refCondition.AccbookOrgRefFilter',
			pk_org: pk_org
		},
		pageInfo: { pageSize: 10, pageIndex: 1 },
		pid: '',
		keyword: ''
	};

	let dataWithout = await higherAjax(url.queryAccbookAndPeriodsFxValuesUrl, data, error, isloading); //没有数据权限的主账簿及其他数据
	let dataWith = await higherAjax(url.accbookTreeRefUrl, refData, error, isloading); //有数据权限的账簿

	if (dataWithout.success && dataWithout.data) {
		let pk_orgs = pk_org.split(',');
		pk_orgs &&
			pk_orgs.map((perOrg) => {
				let pk_accbook = dataWithout.data[perOrg] && dataWithout.data[perOrg].mainaccbook.value;
				if (pk_accbook) {
					let dataWithStr = JSON.stringify(dataWith);
					if (!(dataWithStr && dataWithStr.includes(`"refpk":"${pk_accbook}"`))) {
						dataWithout.data[perOrg].mainaccbook.value = '';
						dataWithout.data[perOrg].mainaccbook.display = '';
					}
				}
			});
	}
	if ('function' === typeof success) {
		success(dataWithout);
	}
}

/**
 * 通过账簿获取组织
 * @param {*} pk_accbook
 * @param {*} isAsync 是否要异步执行
 * @param {*} success
 * @param {*} error
 */
export function getOrgByAccbookBase(pk_accbook, pk_unit, businessDate, isAsync = true, success, error) {
	if (!success) {
		success = (res) => {};
	}
	if (!error) {
		error = defalutError;
	}
	ajax({
		url: url.queryOrgByAccbookUrl,
		async: isAsync,
		data: {
			pk_accbook: pk_accbook,
			pk_unit: pk_unit,
			businessDate: businessDate
		},
		success: (res) => {
			if ('function' === typeof success) {
				success(res);
			}
		},
		error: (res) => {
			if ('function' === typeof error) {
				error(res);
			}
		}
	});
}
