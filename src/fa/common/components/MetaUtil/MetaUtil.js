/**
 * 本工具类提供与 meta 相关的一些工具方法
 */

let metaUtil = {};


/**
 * 获取指定区域所有字段code
 * @param props
 * @param area 区域名称
 */
metaUtil.getAreaItemFields = (props, area) =>{
    const meta = props.meta.getMeta();
    const items = meta[area].items;
    return items.map((item) => item.attrcode);
};

/**
 * 获取指定区域的必输项字段
 * @param props
 * @param area 区域名称
 */
metaUtil.getRequiredFieldByArea = (props, area) => {
    const meta = props.meta.getMeta();
    const items = meta[area].items;
    const itemsRequired = items.filter((item) => {
        return item.required;
    });

    return itemsRequired.map((item) => item.attrcode);
};

/**
 * 获取meta中字段的中文名
 * @param props
 * @param area 区域名称
 * @param fields:Array 字段列表
 */
metaUtil.getFieldLabels = (props, area, fields) => {
    const meta = props.meta.getMeta();
    const items = meta[area].items;
    const itemsFilterd = items.filter((item) => {
        if (fields.has(item.attrcode)) {
            return true;
        }
    });

    return itemsFilterd.map((item) => item.label);
};

/**
 * 获取指定字段的模板属性
 * @param props
 * @param area 区域名称
 * @param field:Array 字段列表
 */
metaUtil.getMetaItemByField = (props, area, field) => {
    const meta = props.meta.getMeta();
    const items = meta[area].items;
    const itemMeta = items.filter((item) => {
        return item.attrcode === field;
    });
    return itemMeta && (Array.isArray(itemMeta) ? itemMeta[0] : null);
};

/**
 * 根据模板生成孔行数据（cardTable）
 * @param props
 * @param area 区域名称
 * @param field:Array 字段列表
 */
metaUtil.genEmptyRowDataByMeta = (props, area) => {
    const meta = props.meta.getMeta();
    const items = meta[area].items;
    let rowData = {};
    items.map((item) => {
        rowData[item.attrcode] = {display:null, scale:-1,value:null};
    });
    return {values: rowData};
};

/**
 * 更新form字段值
 * @param props
 * @param moduleId 区域编码
 * @param field 字段
 * @param newValue 新值，例如："01"，11.2，{display:"显示内容", value:"xxx"}
 */
metaUtil.updateFormItemValue = (props, moduleId, field, newValue) => {
    let oldValueObj = props.form.getFormItemsValue(moduleId, field);
    let newValueObj = null;
    if(typeof newValue === "object") {
        newValueObj = Object.assign(oldValueObj, newValue);
    } else {
        newValueObj = Object.assign(oldValueObj, {value:newValue});
    }
    props.form.setFormItemsValue(moduleId, {[field]: newValueObj});
};

/**
 * 清空form字段值
 * @param props
 * @param moduleId 区域编码
 * @param field 字段
 */
metaUtil.clearFormItemValue = (props, moduleId, field) => {
    let oldValueObj = props.form.getFormItemsValue(moduleId, field);
    let newValueObj = Object.assign(oldValueObj, {value: null, display: null});
    props.form.setFormItemsValue(moduleId, {[field]: newValueObj});
};


export default metaUtil;