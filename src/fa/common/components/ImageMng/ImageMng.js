/*
  影像扫描和影像查看
*/
import { toast } from 'nc-lightapp-front';
import { imageScan, imageView } from 'sscrp/rppub/components/image';

//@zhangypm切换到正确的导入
import ampub from 'ampub';
// import ampub from '../../../../ampub/common/collection/release';

const {utils} = ampub;
const {getMultiLangByID} = utils.multiLangUtils;

//影像扫描
function faImageScan(props, imageData = {}) {
	let { pagecode, formId, tableId, pkField } = imageData;

	//空判断
	if (!props || !pagecode || !formId || !pkField) {
		toast({ color: 'error', content: getMultiLangByID('facommon-000000') /* 国际化处理： 参数不可为空！*/ });
		return;
	}

	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let billdata = props.createMasterChildData(pagecode, formId, tableId);
	let headData = billdata.head[formId].rows[0].values;

	let billInfoMap = {};

	//基础字段 单据pk，单据类型，交易类型，单据的组织
	billInfoMap.pk_billid = pk;
	billInfoMap.pk_billtype = headData.bill_type.value;
	billInfoMap.pk_tradetype = headData.transi_type.value;
	billInfoMap.pk_org = headData.pk_org.value;

	//影像所需 FieldMap
	billInfoMap.BillType = headData.transi_type.value;
	billInfoMap.BillDate = headData.billmaketime.value;
	billInfoMap.Busi_Serial_No = pk;
	billInfoMap.OrgNo = headData.pk_org.value;
	billInfoMap.BillCode = headData.bill_code.value;
	billInfoMap.OrgName = headData.pk_org.display;
	billInfoMap.Cash = '0.00';

	imageScan(billInfoMap, 'iweb');
}

//影像查看
function faImageView(props, imageData = {}) {
	let { pagecode, formId, tableId, pkField } = imageData;
	//空判断
	if (!props || !pagecode || !formId || !pkField) {
		toast({ color: 'error', content: getMultiLangByID('facommon-000000') /* 国际化处理： 参数不可为空！*/ });
		return;
	}

	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let billdata = props.createMasterChildData(pagecode, formId, tableId);
	let headData = billdata.head[formId].rows[0].values;

	let billInfoMap = {};

	//基础字段 单据pk，单据类型，交易类型，单据的组织
	billInfoMap.pk_billid = pk;
	billInfoMap.pk_billtype = headData.bill_type.value;
	billInfoMap.pk_tradetype = headData.transi_type.value;
	billInfoMap.pk_org = headData.pk_org.value;

	imageView(billInfoMap, 'iweb');
}

//影像扫描---卡片单独处理
function cardFaImageScan(props, imageData = {}) {
	let billInfoMap = {};

	//基础字段 单据pk，单据类型，交易类型，单据的组织
	billInfoMap.pk_billid = imageData.pk_billid;
	billInfoMap.pk_billtype = imageData.pk_billtype;
	billInfoMap.pk_tradetype = imageData.pk_tradetype;
	billInfoMap.pk_org = imageData.pk_org;

	//影像所需 FieldMap
	billInfoMap.BillType = imageData.BillType;
	billInfoMap.BillDate = imageData.BillDate;
	billInfoMap.Busi_Serial_No = imageData.Busi_Serial_No;
	billInfoMap.OrgNo = imageData.OrgNo;
	billInfoMap.BillCode = imageData.BillCode;
	billInfoMap.OrgName = imageData.OrgName;
	billInfoMap.Cash = '0.00';

	imageScan(billInfoMap, 'iweb');
}

//影像查看---卡片单独处理
function cardFaImageView(props, imageData = {}) {
	let billInfoMap = {};

	//基础字段 单据pk，单据类型，交易类型，单据的组织
	billInfoMap.pk_billid = imageData.pk_billid;
	billInfoMap.pk_billtype = imageData.pk_billtype;
	billInfoMap.pk_tradetype = imageData.pk_tradetype;
	billInfoMap.pk_org = imageData.pk_org;

	imageView(billInfoMap, 'iweb');
}

export { faImageScan, faImageView, cardFaImageScan, cardFaImageView };
