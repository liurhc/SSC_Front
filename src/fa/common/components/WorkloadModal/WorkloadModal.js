/**
 * 工作量法 模态框
 * myConfig={
 * 		modalId:'',    --String  模态框的ID，必传项
 * 		defaultValue:{ --Object  默认值，若不需要则可以不传
 * 			total:,    		--Number  工作总量
 * 			accumulation:,  --Number  累计工作量 
 * 			unit:''         --String  工作量单位   
 * 		},
 * 		onSureClick:(total, accumulation, unit) => {  --Function  确认按钮  事件 若不需要则可以不传 ， 三个参数分别对应工作总量，累计工作量，和工作量单位
 *				this.onsureclick(total, accumulation, unit);  //此方法需要自己定义。
 *			}
 * }
 * 
 * 用法：
 * 		1，先把WorkloadModal 用相对路径引入到自己的页面
 * 		2，<WorkloadModal {...this.props} config={myConfig} />  --myConfig 为自己定义的变量，格式如上
 * 		3，弹出模态框用 props.modal.show(myConfig.modalId)  --
 */
import React, {Component} from 'react';
import {base, toast} from 'nc-lightapp-front';
//@zhangypm切换到正确的导入
import ampub from 'ampub';
// import ampub from '../../../../ampub/common/collection/release';

const {utils} = ampub;
const {getMultiLangByID} = utils.multiLangUtils;

import './index.less';

const { NCFormControl } = base;

export default class WorkloadModal extends Component {
	constructor(props) {
		super(props);
		let { defaultValue } = props.config;
		this.state = {
			total: defaultValue ? defaultValue.total : '0.00',
			accumulation: defaultValue ? defaultValue.accumulation : '0.00',
			unit: defaultValue ? defaultValue.unit : ''
		};
	}

	render() {
		let { modal, config } = this.props;
		let { modalId, onSureClick } = config;
		let { createModal } = modal;

		return createModal(modalId, {
			title: getMultiLangByID('facommon-000002')/* 国际化处理： 请输入工作量法相关数据*/ ,
			content: contentJSX.call(this),
			beSureBtnClick: typeof onSureClick === 'function' && (() => onSureClick(this.state.total, this.state.accumulation, this.state.unit)), //点击确定按钮事件
			size: '100'
		});
	}
}

function contentJSX() {
	let { total, accumulation, unit } = this.state;
	return (
		<div className="workload-modal">
			<div>
				<p>{getMultiLangByID('facommon-000003')/* 国际化处理： 工作总量*/}：</p>
				<NCFormControl
					className="demo-input"
					type="number"
					min="0.00"
					size="sm"
					step="0.01"
					value={total}
					onChange={(e) => {
						if ((e.split('.')[1] && e.split('.')[1].length > 2) || e < 0) {
							return;
						}
						this.setState({ total: e });
					}}
				/>
			</div>
			<div>
				<p>{getMultiLangByID('facommon-000004')/* 国际化处理： 累计工作量*/}：</p>
				<NCFormControl
					className="demo-input"
					type="number"
					min="0.00"
					size="sm"
					step="0.01"
					value={accumulation}
					onChange={(e) => {
						if ((e.split('.')[1] && e.split('.')[1].length > 2) || e < 0) {
							return;
						}
						this.setState({ accumulation: e });
					}}
				/>
			</div>
			<div>
				<p>{getMultiLangByID('facommon-000005')/* 国际化处理： 工作量单位*/}：</p>
				<NCFormControl
					className="demo-input"
					type="text"
					size="sm"
					value={unit}
					onChange={(e) => {
						this.setState({ unit: e });
					}}
				/>
			</div>
		</div>
	);
}
