import { toast, ajax, cacheTools } from 'nc-lightapp-front';
//@zhangypm切换到正确的导入
import ampub from 'ampub';
// import ampub from '../../../../ampub/common/collection/release';

// import { pageConfig } from '../../../../ampub/common/const/VoucherConst';
const { pageConfig } = ampub.commonConst.VoucherConst;
const { appCode, pageCode, makeCacheWord, url } = pageConfig;

/**
 * 生成凭证
 * @param {*} props 
 * @param {*} tableId 列表表体编码
 * @param {*} pk 单据主键
 * @param {*} appcode 应用appcode
 */
export function index(appcode, props, pks, billtype, pk_group, pk_org) {
	let dataArray = [ billtype ];
	// 如果
	if (pk_org.multi) {
		for (var orgvalue in pk_org.pks) {
			for (let pk of pk_org.pks[orgvalue]) {
				dataArray.push(pk);
			}
		}
	} else {
		for (let pk of pks) {
			dataArray.push(pk);
		}
	}
	// 命名格式为 appid_LinkVouchar,其中appid为自己小应用的应用编码
	let appId_MakeVouchar = appcode + makeCacheWord;
	//cacheTools.set(appId_MakeVouchar, [ dataArray ]);

	// 会计平台的应用编码,页面编码。不可修改
	let urlData = {
		dataArray: [ dataArray ]
	};
	ajax({
		url: url.generateVoucherUrl,
		data: urlData,
		success: (res) => {
			if (res.success) {
				cacheTools.set(appId_MakeVouchar, res.data.pklist);
				props.openTo(res.data.url, {
					status: 'edit',
					appcode: res.data.appcode,
					pagecode: res.data.pagecode,
					scene: appId_MakeVouchar
				});
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'warning', content: res.message });
			}
		}
	});
}
