/**
 * 
 * 查询区参照修改后联动清空受其过滤字段
 * 
 */

const defaultConfig = {
	//查询区
	searchId: 'searchArea',
	formId: 'card_head',
	tableId: 'bodyvos',
	linkageData: {
		//联动链
		//参照字段:['被过滤字段1'，'被过滤字段2']
		pk_org: [
			'pk_accbook',
			'pk_equiporg',
			'pk_usedept',
			'pk_assetuser',
			'pk_ownerorg',
			'pk_mandept',
			'pk_card',
			'queryCard',
			'pk_costcenter'
		],
		pk_equiporg: [ 'pk_costcenter', 'pk_usedept', 'pk_assetuser' ],
		pk_ownerorg: [ 'pk_mandept' ]
	}
};

/**
 * 
 * @param {*} props 
 * @param {*} field 
 * @param {*} config 
 * @param {*} areaType search、form、cardTable、editTable
 * @param {*} moduleId searchId、formId、tableId
 * @param {*} index 表体行号
 */
function referLinkageClear(props, field, config, areaType = 'search', moduleId, index) {
	if (!field || !props) {
		return;
	}
	//根据传过来的配置修改默认配置
	config = addConfig(config, defaultConfig);
	//字段会出现xxx.xxxx.itemcode 所以使用最后一个点后的编码
	let fields = field.split('.');
	field = fields[fields.length - 1];
	areaType = areaType.toLowerCase();
	switch (areaType) {
		case 'search':
			linkageClearSearch.call(this, props, field, config, moduleId);
			break;
		case 'form':
			linkageClearForm.call(this, props, field, config, moduleId);
			break;
		case 'cardtable':
			linkageClearTable.call(this, props, field, config, moduleId, 'cardTable', index);
			break;
		case 'edittable':
			linkageClearTable.call(this, props, field, config, moduleId, 'editTable', index);
			break;
		default:
			break;
	}
}

function linkageClearSearch(props, field, config, moduleId) {
	let searchId = config['searchId'];
	if (moduleId) {
		searchId = moduleId;
	}
	let linkageData = config['linkageData'];
	for (let key in linkageData) {
		//字段被修改
		if (key === field) {
			//清空被其过滤的字段
			let clearFields = linkageData[key];
			clearFields.map((clearField) => {
				props.search.setSearchValByField(searchId, clearField, { value: '', display: '' });
			});
		}
	}
}

function linkageClearForm(props, field, config, moduleId) {
	let formId = config['formId'];
	if (moduleId) {
		formId = moduleId;
	}
	let linkageData = config['linkageData'];
	for (let key in linkageData) {
		//字段被修改
		if (key === field) {
			//清空被其过滤的字段
			let clearFields = linkageData[key];
			clearFields.map((clearField) => {
				props.form.setFormItemsValue(formId, { [clearField]: { value: '', display: '' } });
			});
		}
	}
}

function linkageClearTable(props, field, config, moduleId, tableType, index) {
	let tableId = config['tableId'];
	if (moduleId) {
		tableId = moduleId;
	}
	let linkageData = config['linkageData'];
	for (let key in linkageData) {
		//字段被修改
		if (key === field) {
			//清空被其过滤的字段
			let clearFields = linkageData[key];
			clearFields.map((clearField) => {
				props[tableType].setValByKeyAndIndex(tableId, index, clearField, { value: '', display: '' });
			});
		}
	}
}

function addConfig(config, defaultConfig) {
	let newConfig = defaultConfig;
	if (!config) {
		return newConfig;
	}
	//修改查询区id
	if (config['searchId']) {
		newConfig['searchId'] = config['searchId'];
	}
	//修改查询区id
	if (config['formId']) {
		newConfig['formId'] = config['formId'];
	}
	//修改查询区id
	if (config['tableId']) {
		newConfig['tableId'] = config['tableId'];
	}
	let linkageData = config['linkageData'];
	if (linkageData) {
		//增加/修改配置
		for (let key in linkageData) {
			newConfig['linkageData'][key] = linkageData[key];
		}
	}
	return newConfig;
}

/**
 * 批改清空联动字段
 * @param {*} props 
 * @param {*} formId 表单区域id
 * @param {*} tableId 表体区域id
 * @param {*} alterKey 批改字段
 * @param {*} changKey 联动字段数组
 * @param {*} index 当前批改下标
 * @param {*} alterValue 批改值
 */
function referAlterClear(props, formId, tableId, alterKey, changKeys, index, alterValue, display) {
	let rows = props.cardTable.getVisibleRows(tableId);
	//效率优化，开启开关
	props.beforeUpdatePage();
	let changeValues = {};
	for (let j = 0; j < changKeys.length; j++) {
		let changKey = changKeys[j];
		changeValues[changKey] = { value: null, display: null };
	}

	if (rows && rows.length > 0) {
		for (let i = 0; i < rows.length; i++) {
			let row = rows[i];
			let value = row.values[alterKey].value;
			if (value == alterValue) {
				continue;
			} else {
				props.cardTable.setValByKeysAndIndex(tableId, i, changeValues);
			}
		}
		//重新给批改的字段塞值，否则批改的平台方法记录的code将是最后一次调用setValueByKeysAndIndex的值
		props.cardTable.setValByKeyAndIndex(tableId, index, alterKey, { value: alterValue, display: display });
	}
	// 性能优化，表单和表格统一渲染
	props.updatePage(formId, tableId);
}

export { referLinkageClear, referAlterClear };
