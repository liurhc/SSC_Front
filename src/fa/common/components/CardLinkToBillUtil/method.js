//固定资产卡片联查单据统一入口
//实现通过卡片页面传入的单据类型和pk先获取交易类型 通过交易类型和pk获取app信息（走我们自己的action获取app信息调用的ampub）
//FaCardDrillToBillAction（入口action）
import {ajax, toast} from 'nc-lightapp-front';
import {drillConfig} from './const';

//@zhangypm切换到正确的导入
import ampub from 'ampub';
// import ampub from '../../../../ampub/common/collection/release';

const {commonConst, utils} = ampub;
const {linkQueryConst} = commonConst.CommonKeys;
const {getMultiLangByID} = utils.multiLangUtils;

export default function CardDrillToBill(props, pk, billtype) {
    //调用ajax获取交易类型
    ajax({
        //原来变动单查询headvo交互
        url: drillConfig.querytransi_typeurl,
        data: {
            billtype: billtype,
            pk
        },
        success: (res) => {
            let {success, data} = res;
            if (success) {
                if (data) {
                    data['status'] = 'browse';
                    props.openTo(data[linkQueryConst.URL], data);
                } else {
                    toast({
                        color: 'danger',
                        content: getMultiLangByID('noauthorization-000001') /*国际化处理：您没有操作该应用的权限，请联系管理员为您分配权限*/
                    });
                }
            }
        }
    });
}
