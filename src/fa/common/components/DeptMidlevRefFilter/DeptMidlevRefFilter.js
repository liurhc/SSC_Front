/*
	FA74, 编辑管理部门、使用部门时是否允许选择非末级
*/
import { ajax } from 'nc-lightapp-front';

/**
 * 通过编辑前查询后台参数  IS_ALLOW_DEPT_MIDLEV
 * @param {*} props 
 */
export function queryOnlyLeafCanSelect(props, assembleData = {}) {
	let { formId, tableId, pk_org, field } = assembleData;
	//空判断
	if (!props || !pk_org) {
		return;
	}
	if (!(field && field.length > 0)) {
		return;
	}
	let is_allow_dept_midlev_new = false;
	ajax({
		url: '/nccloud/fa/fapub/onlyleafcanselect.do', //查询部门是否支持勾选非末级
		data: {
			pk_org
		},
		async: false, //改为同步处理
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data == 'Y') {
					is_allow_dept_midlev_new = true;
				}
			}
		}
	});
	let is_allow_dept_midlev_old = this.is_allow_dept_midlev;
	if (is_allow_dept_midlev_new != is_allow_dept_midlev_old) {
		this.is_allow_dept_midlev = is_allow_dept_midlev_new;
		let meta = props.meta.getMeta();
		//表头处理
		if (meta && formId && meta[formId] && meta[formId].items && meta[formId].items.length > 0) {
			meta[formId].items.map((item) => {
				if (field.includes(item.attrcode)) {
					item.onlyLeafCanSelect = !is_allow_dept_midlev_new;
				}
			});
		}
		//表体处理
		if (meta && tableId && meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
			meta[tableId].items.map((item) => {
				if (field.includes(item.attrcode)) {
					item.onlyLeafCanSelect = !is_allow_dept_midlev_new;
				}
			});
		}
		props.meta.setMeta(meta);
	}
}

export function rewriteMeta(props, assembleData = {}) {
	let { meta, formId, tableId, field } = assembleData;
	//空判断
	if (!props || !meta) {
		return;
	}
	if (!(field && field.length > 0)) {
		return;
	}
	// 表头处理
	if (formId && meta[formId] && meta[formId].items && meta[formId].items.length > 0) {
		meta[formId].items.map((item) => {
			if (field.includes(item.attrcode)) {
				item.onlyLeafCanSelect = !this.is_allow_dept_midlev;
			}
		});
	}
	// 表体处理
	if (tableId && meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		meta[tableId].items.map((item) => {
			if (field.includes(item.attrcode)) {
				item.onlyLeafCanSelect = !this.is_allow_dept_midlev;
			}
		});
	}
}
