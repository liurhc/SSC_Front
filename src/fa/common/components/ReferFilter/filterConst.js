/**
 * 公共过滤配置
 */
//@zhangypm切换到正确的导入
import ampub from 'ampub';
const {IBusiRoleConst} = ampub.commonConst.CommonKeys;
//import { IBusiRoleConst } from '../../../../ampub/common/const/CommonKeys';

export const defaultTemplateData = {
	pk_org: {
		//财务组织
		//主组织过滤
		RefActionExtType: 'TreeRefActionExt', //树形
		class: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' //java过滤类
	},
	pk_org_v: {
		//财务组织版本
		RefActionExtType: 'TreeRefActionExt',
		class: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter'
	},
	pk_accbook: {
		//财务核算账簿
		RefActionExtType: 'TreeRefActionExt',
		class: 'nccloud.web.fa.common.refCondition.AccbookOrgRefFilter',

		data: [
			//字段过滤
			{
				fields: [ 'pk_org' ], //使用组织 组织版本过滤 顺序即优先级
				returnName: 'pk_org'
			},
			{
				returnConst: true,
				returnName: 'isDataPowerEnable'
			},
			{
				returnConst: 'FADefault',
				returnName: 'DataPowerOperationCode'
			}
		]
	},
	pk_mandept: {
		//管理部门
		isRunWithChildren: true, //是否执行时包含下级
		defaultRunWithChildren: true, //是否默认勾选执行时包含下级
		orgMulti: 'pk_org', //参照左上业务单元参照框 按data中returnName的过滤字段取 即管理部门优先货主管理组织过滤其次组织其次组织版本
		data: [
			{
				fields: [ 'pk_ownerorg', 'pk_org' ],
				returnName: 'pk_org'
			},
			{
				returnConst: IBusiRoleConst.ASSETORG,
				returnName: 'busifuncode'
			}
		]
	},
	pk_equip_usedept: {
		orgMulti: 'pk_org',
		isRunWithChildren: true, //是否执行时包含下级
		defaultRunWithChildren: true, //是否默认勾选执行时包含下级
		data: [
			{
				fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
				returnName: 'pk_org' //被过滤字段名后台需要参数
			},
			{
				returnConst: IBusiRoleConst.ASSETORG,
				returnName: 'busifuncode'
			}
		]
	},
	pk_mandept_v: {
		//管理部门版本
		orgMulti: 'pk_org',
		data: [
			{
				fields: [ 'pk_ownerorg', 'pk_org' ],
				returnName: 'pk_org'
			},
			{
				returnConst: IBusiRoleConst.ASSETORG,
				returnName: 'busifuncode'
			}
		]
	},
	pk_usedept: {
		//使用部门
		isRunWithChildren: true, //是否执行时包含下级
		defaultRunWithChildren: true, //是否默认勾选执行时包含下级
		orgMulti: 'pk_org',
		data: [
			{
				fields: [ 'pk_equiporg', 'pk_org' ],
				returnName: 'pk_org'
			},
			{
				returnConst: IBusiRoleConst.ASSETORG,
				returnName: 'busifuncode'
			}
		]
	},
	pk_usedept_v: {
		//使用部门
		orgMulti: 'pk_org',
		data: [
			{
				fields: [ 'pk_equiporg', 'pk_org' ],
				returnName: 'pk_org'
			},
			{
				returnConst: IBusiRoleConst.ASSETORG,
				returnName: 'busifuncode'
			}
		]
	},
	pk_assetuser: {
		//使用人
		orgMulti: 'pk_org',
		data: [
			{
				fields: [ 'pk_equiporg', 'pk_org' ],
				returnName: 'pk_org'
			},
			{
				returnConst: IBusiRoleConst.ASSETORG,
				returnName: 'busifuncode'
			}
		]
	},
	pk_recorder: {
		//经办人
		orgMulti: 'pk_org',
		data: [
			{
				fields: [ 'pk_org' ],
				returnName: 'pk_org'
			},
			{
				returnConst: IBusiRoleConst.ASSETORG,
				returnName: 'busifuncode'
			}
		]
	},
	pk_ownerorg: {
		addOrgRelation: 'pk_orgs',
		//货主管理组织
		data: [
			{
				fields: [ 'pk_org' ],
				returnName: 'pk_orgs'
			}
		]
	},
	pk_ownerorg_v: {
		addOrgRelation: 'pk_orgs',
		//货主管理组织
		data: [
			{
				fields: [ 'pk_org' ],
				returnName: 'pk_orgs'
			}
		]
	},
	pk_costcenter: {
		//成本中心
		// RefActionExtType: 'TreeRefActionExt',
		// class: 'nccloud.web.fa.common.refCondition.CostCenterRefFilter',
		//成本中心
		isCurrentOrgCreated: true, //当前组织创建的成本中心
		data: [
			{
				//使用使用权过滤，使用权为空取财务组织
				fields: [ 'pk_equiporg', 'pk_org' ],
				returnName: 'pk_org'
			}
		]
	},
	pk_card: {
		//资产卡片
		orgMulti: 'pk_org',
		data: [
			{
				fields: [ 'pk_org' ],
				returnName: 'pk_org'
			}
		]
	},
	queryCard: {
		//资产卡片
		orgMulti: 'pk_org',
		data: [
			{
				fields: [ 'pk_org' ],
				returnName: 'pk_org'
			}
		]
	},
	pk_depprogram: {
		//模拟折旧设置
		data: [
			{
				fields: [ 'pk_org' ],
				returnName: 'pk_org'
			}
		]
	},
	pk_category: {
		//资产类别
		orgMulti: 'pk_org',
		data: [
			{
				fields: [ 'pk_org' ],
				returnName: 'pk_org'
			}
		]
	}
};
