import { defaultTemplateData } from './filterConst';

/**
 *  给查询参照增加业务单元过滤
 *  初始化没有编辑组织，直接编辑需要业务单元过滤的字段，则显示所有有权限的业务单元
 * @param {*} props 
 * @param {*} searchId 查询区域id
 * @param {*} item 操作的字段
 */
export function FinanceOrgMultiRefFilter(props, searchId, item, key = [ 'pk_org' ]) {
	item.isShowUnit = true;
	item.unitProps = {
		multiLang: {
			domainName: 'uapbd',
			// currentLocale: 'simpchn',
			moduleId: 'refer_uapbd'
		},
		refType: 'tree',
		refName: 'refer-000201' /* 国际化处理： 业务单元*/,
		placeholder: 'refer-000201' /* 国际化处理： 业务单元*/,
		// type: 'dropDown',
		refcode: 'uapbd/refer/org/FinanceOrgTreeRef/index',
		queryTreeUrl: '/nccloud/uapbd/org/FinanceOrgTreeRef.do',
		isMultiSelectedEnabled: false,
		isShowDisabledData: true,
		onlyLeafCanSelect: false,
		key: 'pk_org'
	};
	item.unitCondition = () => {
		let retData = {};
		for (let i = 0; i < key.length; i++) {
			let pk_org_value = props.search.getSearchValByField(searchId, key[i]); // 调用相应组件的取值API
			if (pk_org_value && pk_org_value.value && pk_org_value.value.firstvalue) {
				retData['pk_org'] = pk_org_value.value.firstvalue;
				break;
			}
		}
		retData['TreeRefActionExt'] ='nccloud.web.ampub.common.refCodition.FinanceOrgSqlBuilder';
		return retData;
	};
}

/**
 * 根据组织字段是否多选控制其他参照是否有业务单元过滤的参照框
 * @param {*} props 
 * @param {*} value 
 * @param {*} searchId 查询区id
 * @param {*} config 使用参照过滤配置
 */
export function isMultiCorpRefHandler(props, key, value, searchId, config) {
	let fieldArry = [];
	fieldArry = fieldArry.concat(getConfig.call(this, key, config));
	if (fieldArry.length < 1) {
		return;
	}
	let meta = props.meta.getMeta();
	//遍历传入的字段，分别给字段添加业务单元过滤，如果组织多选，则有业务单元过滤，如果是
	//单选则没有，组织不选的情况下参照所有的业务单元
	if (typeof fieldArry == 'object' && fieldArry.constructor == Array) {
		if (!value || value.length == 0) {
			return;
		}
		if (value.length == 1) {
			fieldArry.forEach((field) => {
				meta[searchId].items.map((item) => {
					if (item.attrcode == field) {
						item.isShowUnit = false;
					}
				});
			});
		} else {
			fieldArry.forEach((field) => {
				meta[searchId].items.map((item) => {
					if (item.attrcode == field) {
						item.isShowUnit = true;
						//TODO 平台不能够支持设置默认值
						/*item.defaultUnitValue = {
							display: '11',
							value: '66'
						};*/
					}
				});
			});
		}
	}
	// props.meta.setMeta(meta);
}

/**
 * 使用字段在过滤配置中查询设置多组织数据
 */
function getConfig(field, config) {
	let resultArray = [];
	//修改配置
	let specialFields = undefined;
	if (config) {
		specialFields = config['specialFields'];
	}
	let templateData = defaultTemplateData;
	if (specialFields && Object.keys(specialFields).length != 0) {
		for (let key in specialFields) {
			templateData[key] = specialFields[key];
		}
	}
	//根据字段查找其过滤前置字段
	for (let key in templateData) {
		let returnName = templateData[key].orgMulti;
		if (returnName) {
			templateData[key].data.map((item) => {
				if (item.returnName === returnName) {
					if (item.fields.includes(field)) {
						resultArray.push(key);
					}
				}
			});
		}
	}
	return resultArray;
}
