import { FinanceOrgMultiRefFilter, isMultiCorpRefHandler } from './multiRefFilter';
import { defaultTemplateData } from './filterConst';
import { ajax, toast } from 'nc-lightapp-front';
import { constInfo } from './const';

//@zhangypm切换到正确的导入
import ampub from 'ampub';
// import ampub from '../../../../ampub/common/collection/release';

const {components} = ampub;
const {loginContext, getContext, loginContextKeys} = components.LoginContext;
const {defRefCondition, commonRefCondition} = components.refInit;

const { url } = constInfo;
//公共参照过滤

//Config 结构及参数 模板
let defaultConfig = {
	searchId: '', //查询区id
	formId: '', //表头id
	bodyIds: [], //表达id数组
	specialFields: {}, //特殊过滤
	orgField: 'pk_org', //自定义项多组织 组织字段编码
	defPrefix: {
		search: [ 'bodyvos.def', 'def' ],
		head: 'def',
		body: 'def'
	},
	onlyLeafCanSelect: {
		pk_org: false, // 财务组织
		pk_category: false, // 资产类别
		pk_addreducestyle: false, // 增加方式
		pk_reducestyle: false, // 减少方式
		pk_usingstatus: false, // 使用状况
		pk_raorg: false, // 利润中心
		pk_costcenter: false, // 成本中心
		pk_usedept: false, // 使用部门
		pk_mandept: false //管理部门
	}
	/*示例
	specialFields: {
		pk_org:{
			RefActionExtType: 'TreeRefActionExt',   //参照类型 tree/grid
		    class: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter'  //后台过滤类
		}
		pk_usedept：{  //被过滤字段名
			orgMulti: 'pk_org' //添加左上角主组织参照
			data: [
				{
					fields: [ 'pk_equiporg', 'pk_org' ], //可被多个字段过滤 取第一个有的值	
					returnName: 'pk_org' //被过滤字段名后台需要参数
				},
				...
			]
		}
	}
	*/
};

/**
 * 
 * @param {*} props 
 * @param {*} meta 
 * @param {*} config 
 * @param {*} areaType 查询区-search、表头-head、表体-body
 * @param {*} areaId 
 */
function baseAddReferFilter(props, meta, config, areaType, areaId, record, tableType) {
	//空判断
	if (!props || !meta || !config || !areaType || !areaId || !meta[areaId] || !meta[areaId].items) {
		return;
	}
	//合并默认配置与传入配置
	let templateData = getConfig(config);
	let onlyLeafCanSelectConfig = getOnlyLeafCanSelectConfig(config);//默认配置+自定义配置
	let onlyLeafCanSelect = config['onlyLeafCanSelect'];//自定义配置
	meta[areaId].items.map((item) => {
		//字段可能出现xxx.xxxx.itemcode 所以使用最后一个点后的编码
		let attrcode = item.attrcode;
		let itemCode = attrcode.match(/[^.]+$/);
		//设置是否叶子节点可选 查询区处理默认配置+自定义配置
		if (areaType === 'search' && onlyLeafCanSelectConfig[itemCode] != null) {
			item.onlyLeafCanSelect = onlyLeafCanSelectConfig[itemCode];
		}
		//设置是否叶子节点可选 非查询区处理自定义配置
		if (areaType != 'search' && onlyLeafCanSelect && onlyLeafCanSelect[itemCode] != null) {
			item.onlyLeafCanSelect = onlyLeafCanSelect[itemCode];
		}
		let refFilterData = templateData[attrcode] ? templateData[attrcode] : templateData[itemCode];
		if (areaType === 'search') {
			commonRefCondition.call(this, props, item); //公共的执行时包含下级
		}
        //卡片参照前台不缓存
        if (attrcode.includes('pk_card')) {
            item.isCacheable = false;
        }
		//该字段是否存在过滤配置
		if (refFilterData) {
			//查询区多选组织参照过滤
			if (areaType === 'search') {
				if (refFilterData.orgMulti) {
					let returnName = refFilterData.orgMulti;
					refFilterData.data.map((data) => {
						if (data.returnName === returnName) {
							FinanceOrgMultiRefFilter.call(this, props, areaId, item, data.fields);
						}
					});
				}
				//执行时包含下级
				if (refFilterData.isRunWithChildren) {
					item.isRunWithChildren = true;
					//是否默认勾选
					if (refFilterData.defaultRunWithChildren) {
						item.defaultRunWithChildren = true;
					} else {
						item.defaultRunWithChildren = false;
					}
				}
			}
			//设置item的queryCondition
			setQueryCondition.call(this, props, areaId, areaType, item, refFilterData, config, record, tableType);
		} else {
			addUserDefReferFilter.call(this, props, item, areaType, areaId, config);
		}
	});
}

function setSearchAreaOnlyLeafCanSelect() {}

function getConfig(config) {
	let templateData = defaultTemplateData;
	let specialFields = config['specialFields'];
	if (specialFields && Object.keys(specialFields).length != 0) {
		for (let key in specialFields) {
			templateData[key] = specialFields[key];
		}
	}
	return templateData;
}

function getOnlyLeafCanSelectConfig(config) {
	let onlyLeafCanSelectConfig = defaultConfig.onlyLeafCanSelect;
	let onlyLeafCanSelect = config['onlyLeafCanSelect'];	
	if (onlyLeafCanSelect && Object.keys(onlyLeafCanSelect).length != 0) {
		for (let key in onlyLeafCanSelect) {
			onlyLeafCanSelectConfig[key] = onlyLeafCanSelect[key];
		}
	}
	return onlyLeafCanSelectConfig;
}

function queryOrgRelation(pks) {
	//同步请求
	ajax({
		loading: true,
		url: url.queryOrgRelationUrl,
		data: {
			pk_org: pks
		},
		async: false,
		success: (res) => {
			let { data, success } = res;
			if (success && data) {
				pks = data.pk_org.join(',');
			}
		},
		error: (err) => {
			toast({ color: 'danger', content: err.message });
		}
	});
	return pks;
}

function setQueryCondition(props, areaId, areaType, item, refFilterData, config, record, tableType) {
	//设置过滤参数
	item.queryCondition = () => {
		let returnData = {};
		//java类过滤
		if (refFilterData.RefActionExtType) {
			returnData[refFilterData.RefActionExtType] = refFilterData.class;
		}
		//常量或界面取值过滤
		if (refFilterData.data) {
			for (let i = 0; i < refFilterData.data.length; i++) {
				let dataItem = refFilterData.data[i];
				let fields = dataItem.fields;
				let returnConst = dataItem.returnConst;
				let returnName = dataItem.returnName;
				if (returnConst) {
					//常量
					returnData[returnName] = returnConst;
				} else {
					//页面取值
					switch (areaType) {
						case 'search':
							//查询区
							//从查询区循环取值 ，取第一个存在的
							for (let i = 0; i < fields.length; i++) {
								let fieldValue = props.search.getSearchValByField(areaId, fields[i]);
								if (fieldValue && fieldValue.value && fieldValue.value.firstvalue) {
									if (
										item.isShowUnit &&
										areaType === 'search' &&
										returnName === refFilterData.orgMulti
									) {
										continue;
									}
									returnData[returnName] = fieldValue.value.firstvalue;
									break;
								}
							}
							break;
						case 'head':
							//表头
							//从表头循环取值 ，取第一个存在的
							for (let i = 0; i < fields.length; i++) {
								let data = props.form.getFormItemsValue(areaId, fields[i]);
								if (data && data.value) {
									returnData[returnName] = data.value;
									break;
								}
							}
							break;
						case 'body':
							//表体
							if (!record) {
								let bodyvosId = areaId.replace('_edit', '');
								let rowData = props[tableType].getClickRowIndex(bodyvosId);
								if (rowData && rowData.record) {
									record = rowData.record;
								}
							}
							//循环取值 取第一个存在的
							for (let i = 0; i < fields.length; i++) {
								let field = fields[i];
								let data = '';
								//从表体取
								if (record && record.values[field] && record.values[field].value) {
									data = record.values[field].value;
								}
								let data_form = '';
								let formId = config.formId;
								//从表头取
								if (!data && formId) {
									data_form = props.form.getFormItemsValue(formId, field);
									if (data_form) {
										data_form = data_form.value;
									}
								}
								if (data || data_form) {
									returnData[returnName] = data || data_form;
									break;
								}
							}
							break;
					}
				}
			}
		}
		//给组织增加核算委托关系组织
		if (refFilterData.addOrgRelation) {
			let field = refFilterData.addOrgRelation;
			returnData[field] = queryOrgRelation(returnData[field]);
		}
		return returnData;
	};
}

function addUserDefReferFilter(props, item, areaType, areaId, config) {
	let pk_group = getContext(loginContextKeys.groupId);
	//是否查询区
	let isQuery = false;
	if (areaType === 'search') {
		isQuery = true;
	}
	//主组织字段编码
	let orgField = defaultConfig.orgField;
	if (config.orgField) {
		orgField = config.orgField;
	}
	//自定义前缀、区域id 表体使用表头
	let defPrefix = '';
	switch (areaType) {
		case 'search':
			defPrefix = defaultConfig.defPrefix.search;
			if (config.defPrefix && config.defPrefix.search) {
				defPrefix = config.defPrefix.search;
			}
			break;
		case 'head':
			defPrefix = defaultConfig.defPrefix.head;
			if (config.defPrefix && config.defPrefix.head) {
				defPrefix = config.defPrefix.head;
			}
			break;
		case 'body':
			defPrefix = defaultConfig.defPrefix.body;
			if (config.defPrefix && config.defPrefix.body) {
				defPrefix = config.defPrefix.body;
			}
			areaId = config.formId;
			break;
		default:
			defPrefix = 'def';
	}
	//调用ampub自定义参照过滤方法
	//存在从其他元数据自定义项与本元数据自定义项code的前缀不相同的情况需要多次调用
	if (defPrefix.map) {
		defPrefix.map((defPrefixItem) => {
			defRefCondition.call(
				this,
				props,
				item,
				areaId,
				pk_group,
				isQuery,
				orgField,
				defPrefixItem,
				FinanceOrgMultiRefFilter
			);
		});
	} else {
		defRefCondition.call(
			this,
			props,
			item,
			areaId,
			pk_group,
			isQuery,
			orgField,
			defPrefix,
			FinanceOrgMultiRefFilter
		);
	}
}

//查询区的参照过滤
function addSearchAreaReferFilter(props, meta, config) {
	baseAddReferFilter(props, meta, config, 'search', config.searchId);
}

//表头的参照过滤
function addHeadAreaReferFilter(props, meta, config) {
	baseAddReferFilter(props, meta, config, 'head', config.formId);
}

/**
 * 表体行添加参照过滤，需要放在编辑前事件中
 * @param props
 * @param meta
 * @param config
 * @param record 当前行的记录
 */
function addBodyReferFilter(props, meta, config, record, tableType = 'cardTable') {
	const { bodyIds } = config;
	if (bodyIds && bodyIds.length > 0) {
		bodyIds.map((bodyId) => {
			baseAddReferFilter(props, meta, config, 'body', bodyId, record, tableType);
		});
	}
}

export { addSearchAreaReferFilter, addHeadAreaReferFilter, addBodyReferFilter, isMultiCorpRefHandler };
