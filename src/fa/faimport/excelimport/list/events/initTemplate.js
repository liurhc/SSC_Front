import { onAfterEvent } from './index';
import ampub from 'ampub';
import fa from 'fa';
const { components } = ampub;
const { LoginContext } = components;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: '201201004A_asset' //页面id
		},
		function(data) {
			if (data) {
				if (data.context) {
					//初始化上下文变量
					loginContext.call(_this, data.context);
				}
				if (data.template) {
					let meta = data.template;
					meta = filterMeta(props, meta);

					props.meta.setMeta(meta, () => {
						props.search.setDisabledByField('search', 'pk_accbook', true);
					});
					let pk_org = getContext(loginContextKeys.pk_org);
					pk_org && onAfterEvent.call(_this, 'pk_org', pk_org);
				}
			}
		}
	);
}
function filterMeta(props, meta) {
	let defaultConfig = {
		searchId: 'search'
	};
	// 查询条件过滤
	addSearchAreaReferFilter(props, meta, defaultConfig);
	return meta;
}
