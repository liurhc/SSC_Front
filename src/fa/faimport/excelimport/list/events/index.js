import initTemplate from './initTemplate';
import onAfterEvent from './onAfterEvent';

export { initTemplate, onAfterEvent };
