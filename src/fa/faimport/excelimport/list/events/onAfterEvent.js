/**
 * @description:固定资产期初数据导入  --卡片节点 参照编辑后事件
 */
import { ajax, toast } from 'nc-lightapp-front';
export default function onAfterEvent(key, value) {
	let { nowData = {} } = this.state;
	if (typeof value === 'string' || value === null) {
		value = { refpk: value };
	}
	let _this = this;
	nowData[key] = value.refpk;
	switch (key) {
		case 'pk_org':
			if (!value.refpk) {
				//清空组织时候
				this.props.search.setDisabledByField('search', 'pk_accbook', true);
				this.props.search.setSearchValByField('search', 'pk_accbook', {
					value: '',
					display: ''
				});
				this.props.search.setSearchValByField('search', 'import_period', { value: '' });
				return;
			}
			ajax({
				url: '/nccloud/fa/excelimport/assetimportafteredit.do',
				data: {
					pk_org: value.refpk,
					queryCondition: { key: 'pk_org' }
				},
				success: (res) => {
					if (res.data) {
						let { pk_accbook, accbook_name, importPeriod } = res.data;
						_this.props.search.setDisabledByField('search', 'pk_accbook', false);
						_this.props.search.setSearchValByField('search', 'pk_accbook', {
							value: pk_accbook,
							display: accbook_name
						});
						_this.props.search.setSearchValByField('search', 'import_period', { value: importPeriod });

						nowData['pk_accbook'] = pk_accbook;
						this.setState({
							nowData
						});
					}
				},
				error: (res) => {
					toast({ color: 'danger', content: res.message });
					// 若出错，
					_this.props.search.setSearchValByField('search', 'pk_org', { display: null, value: null });
					_this.props.search.setSearchValByField('search', 'pk_accbook', { display: null, value: null });
					_this.props.search.setSearchValByField('search', 'import_period', { value: null });
				}
			});
			break;
		case 'pk_accbook':
			if (!value.refpk) {
				//清空账簿时候
				this.props.search.setSearchValByField('search', 'import_period', { value: '' });
				return;
			}
			ajax({
				url: '/nccloud/fa/excelimport/assetimportafteredit.do',
				data: {
					pk_org: this.props.search.getSearchValByField('search', 'pk_org').value.firstvalue,
					queryCondition: { key: 'pk_accbook', pk_accbook: value.refpk }
				},
				success: (res) => {
					if (res.data) {
						let { importPeriod } = res.data;
						_this.props.search.setSearchValByField('search', 'import_period', { value: importPeriod });
					}
				},
				error: (res) => {
					toast({ color: 'danger', content: res.message });
					// 若出错，
					_this.props.search.setSearchValByField('search', 'pk_accbook', { display: null, value: null });
					_this.props.search.setSearchValByField('search', 'import_period', { value: null });
				}
			});
			break;
		default:
			break;
	}

	this.setState({
		nowData
	});
}
