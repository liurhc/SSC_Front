import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, createPageIcon } from 'nc-lightapp-front';
import { initTemplate, onAfterEvent } from './events';
import './index.less';
import ampub from 'ampub';
const { components, utils } = ampub;
const { ExportArea } = components;
const { multiLangUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;
/**
 * 页面入口文件
 */
export default class Export extends Component {
	constructor(props) {
		super(props);
		this.state = {
			nowData: {
				pk_org: '',
				pk_accbook: ''
			}
		};
		initTemplate.call(this, props);
		this.cardSearchArea = this.cardSearchArea.bind(this);
		this.onAfterEvent = onAfterEvent.bind(this);
	}
	//卡片节点的查询区
	cardSearchArea = () => {
		const { search } = this.props;
		const { NCCreateSearch } = search;
		return (
			<div className="card-search-area">
				{NCCreateSearch('search', {
					hideBtnArea: true,
					onAfterEvent: this.onAfterEvent
				})}
			</div>
		);
	};

	render() {
		const configData = [
			{
				pagecode: '201201004A_category',
				importUrl: '/nccloud/fa/excelimport/categoryimport.do',
				exportUrl: '/nccloud/fa/excelimport/categoryexport.do',
				importTitle: getMultiLangByID('201201004A-000000'), //'资产类别',
				exportTitle: getMultiLangByID('201201004A-000001'), //'下载模板',
				configTemplate: getMultiLangByID('201201004A-000002'), //'配置模板',
				linkApp: getMultiLangByID('201201004A-000003'), //'查看资产类别',
				linkGlbUrl: '/fa/fabase/category/global/index.html',
				linkGrpUrl: '/fa/fabase/category/group/index.html',
				linkOrgUrl: '/fa/fabase/category/org/index.html',
				linkGlbAppCode: '201200506A',
				linkGrpAppCode: '201200504A',
				linkOrgAppCode: '201200505A',
				linkPageCode: '201200504A_tree',
				imgUrl: 'category',
				fourAppCode: '20121004'
			},
			{
				pagecode: '201201004A_assetequip',
				importUrl: '/nccloud/fa/excelimport/assetequipimport.do',
				exportUrl: '/nccloud/fa/excelimport/assetequipexport.do',
				importTitle: getMultiLangByID('201201004A-000004'), // '附属设备',
				exportTitle: getMultiLangByID('201201004A-000001'), //'下载模板',
				configTemplate: getMultiLangByID('201201004A-000002'), //'配置模板',
				linkApp: getMultiLangByID('201201004A-000005'), //'请到附属设备明细表查看',
				imgUrl: 'assetequip',
				fourAppCode: '20121004'
			},
			{
				pagecode: '201201004A_asset',
				importUrl: '/nccloud/fa/excelimport/assetimport.do',
				exportUrl: '/nccloud/fa/excelimport/assetexport.do',
				importTitle: getMultiLangByID('201201004A-000006'), //'卡片',
				exportTitle: getMultiLangByID('201201004A-000001'), //'下载模板',
				configTemplate: getMultiLangByID('201201004A-000002'), //'配置模板',
				linkApp: getMultiLangByID('201201004A-000007'), //'请到卡片台账查看',
				imgUrl: 'asset',
				cardSearchArea: this.cardSearchArea(),
				fourAppCode: '20121004'
			}
		];
		return (
			<div className="nc-bill-list" id="excelimport-info">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{getMultiLangByID('201201004A-000008')}</h2>
					</div>
				</div>
				<div className="nc-bill-card-area import-area">
					{configData.map((item) => {
						return <ExportArea {...this.props} nowData={this.state.nowData} importConfig={item} />;
					})}
				</div>
			</div>
		);
	}
}

Export = createPage({})(Export);
initMultiLangByModule({ fa: [ '201201004A' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Export />, document.querySelector('#app'));
});
