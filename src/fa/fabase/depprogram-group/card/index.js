import React, { Component } from 'react';
import CardBase from '../../depprogram-base/card-base';
import { createPage } from 'nc-lightapp-front';
export const cardpageConfig = {
	nodecode: '2012004090',
	nodeType: 'group',
	nodeTypeCN: '201200540A-000001' /*国际化处理：模拟折旧设置-集团*/,
	pagecode: '201200540A_card',
	pk_org: 'null',
	appcode: '201200540A',
	pagecode_list: '201200540A_list',
	printFuncode: '2012004090',
	dataSource: 'fa.fabase.depprogram-group.main'
};
const CardGroup = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: cardpageConfig.pagecode,
		bodycode: 'head'
	}
})(CardBase);

export default class Card extends Component {
	render() {
		return <CardGroup cardpageConfig={cardpageConfig} />;
	}
}
