import React, { Component } from 'react';
import ListBase from '../../depprogram-base/list-base';
import { createPage } from 'nc-lightapp-front';
// 页面配置
const listpageConfig = {
	nodecode: '2012004090',
	nodeType: 'group',
	nodeTypeCN: '201200540A-000001' /*国际化处理：模拟折旧设置-集团*/,
	pagecode: '201200540A_list',
	pk_org: 'null',
	appcode: '201200540A',
	pagecode_card: '201200540A_card',
	printFuncode: '2012004090',
	dataSource: 'fa.fabase.depprogram-group.main'
};

const ListGroup = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: listpageConfig.pagecode,
		bodycode: 'head'
	}
})(ListBase);

export default class List extends Component {
	render() {
		return <ListGroup listpageConfig={listpageConfig} />;
	}
}
