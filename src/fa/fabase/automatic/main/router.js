import { asyncComponent } from 'nc-lightapp-front';

const AtuoMaticList = asyncComponent(() =>
	import(/* webpackChunkName: "fa/fabase/automatic/list/list" */ /* webpackMode: "eager" */ '../list')
);
const AtuoMaticCard = asyncComponent(() =>
	import(/* webpackChunkName: "fa/fabase/automatic/card/card" */ /* webpackMode:"eager" */ '../card')
);

const routes = [
	{
		path: '/',
		component: AtuoMaticList,
		exact: true
	},
	{
		path: '/list',
		component: AtuoMaticList
	},
	{
		path: '/card',
		component: AtuoMaticCard
	}
];

export default routes;
