import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/*
* 公共常量定义
* */
export const commonConfig = {
	moduleId: '2012',
	appCode: '201200548A',
	pkName: 'alertRegistryPk',
	tableId: 'list_head',
	pageName: getMultiLangByID('201200548A-000030')/* 国际化处理： 自动折旧与结账设置*/,
	dataSource: 'fa.fabase.automatic.main'
};

export const commonConstValue = {
	everyMonth: getMultiLangByID('201200548A-000031')/* 国际化处理： 每月*/,
	everyMonthLastDay: getMultiLangByID('201200548A-000032')/* 国际化处理： 每月最后一天*/,
	day: getMultiLangByID('201200548A-000013')/* 国际化处理： 日*/
};
