import { commonConfig, commonConstValue } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/*
* card常量定义
* */
export const pageConfig = {
	...commonConfig,
	pageCode: '201200548A_card',
	routeToList: '/list',
	url: {
		updateUrl: '/nccloud/fa/automatic/update.do',
		saveUrl: '/nccloud/fa/automatic/save.do',
		toBackgroundTaskUrl: '/nccloud/resources/uap/imp/bgTaskDevelop/main/index.html',
		queryByPk: '/nccloud/fa/automatic/querycardautomaticbypk.do'
	},
	refer: {
		financeOrg: {
			refcode: 'uapbd/refer/org/FinanceOrgTreeRef/index.js',
			TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter'
		},
		accountBook: {
			refcode: 'uapbd/refer/org/AccountBookTreeRef/index.js',
			TreeRefActionExt: 'nccloud.web.fa.common.refCondition.AccbookOrgRefFilter'
		}
	},
	defaultData: {
		selectedValue: 'all', //页面左侧单选
		selectedDay: 'day0', //页面右侧选择日期，恢复默认选择‘最后一日’
		maticDate: getMultiLangByID('201200548A-000032')/* 国际化处理： 每月最后一天*/+' 23:00:00', //页面左侧自动结账时间
		checkedDate: '0,23:00:00',
		checkedTime: '23:00:00' //页面右侧时间选择器
	}
};

export const constValue = {
	...commonConstValue,
	tips: {
		save: {
			financeRequired: getMultiLangByID('201200548A-000001')/* 国际化处理： 资产组织未输入！*/,
			accountingBooKRequired: getMultiLangByID('201200548A-000002')/* 国际化处理： 财务核算账簿未输入！*/,
			timeRequired: getMultiLangByID('201200548A-000003')/* 国际化处理： 时间未输入！*/,
			success: getMultiLangByID('msgUtils-000014')/* 国际化处理： 保存成功*/
		}
	}
};
