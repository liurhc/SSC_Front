import React, { Component } from 'react';
import { createPage, base,ajax, createPageIcon } from 'nc-lightapp-front';
import { onTimeChange, getListData, initTemplate, buttonClick } from './events';
import './index.less';
import { pageConfig } from './const';
import ampub from 'ampub';
const { utils,components } = ampub;
const { LoginContext,AMRefer } = components;
const { loginContextKeys, getContext,loginContext} = LoginContext;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { pageName, refer, defaultData, routeToList,pageCode } = pageConfig;
const { NCRadio, NCTimepicker, NCCol, NCBackBtn } = base;

/**
 * 文件主入口
 * @author:zhangxxu
 * @description:月末计提结账
 */
class AtuoMaticCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedValue: defaultData.selectedValue, //选择方式 all-全部财务组织和资产账簿/split-多选财务组织和资产账簿
			periodTypeCode: '1', // 1-执行日期所在的会计期间2-执行日期的上一个会计期间
			checkedTime: defaultData.checkedTime, //右侧时间选择器的已选值。
			financeOrgIsNull: true,
			activeDay: 0 //选中的日期的具体的某一天
		};
		this.status = ''; // add-新增   edit-修改
		this.alertRegistryPk = '';
		this.ts = '';
		this.financeOrg = [];
		this.accountBook = [];
		this.checkedDate = defaultData.checkedDate;
		this.editDefaultTime = {
			date: 0,
			checkedTime: ''
		};
		initTemplate.call(this, this.props);
	}

	componentDidMount() {
		getListData.call(this);
	}

	render() {
		let { selectedValue, checkedTime, periodTypeCode, activeDay } = this.state;
		let { button } = this.props;
		let { createButtonApp } = button;
		let dateArr = new Array();
		//生成长度为32的数据，后面生成单选天使用
		for (let i = 0; i < 32; i++) {
			dateArr.push(i);
		}
		let financeorgconfig = {
			onChange: (val) => {
				//如果有改变的节点 清空所选账簿
				this.accountBook = [];
				this.financeOrg = val;
				//如果只选择单个财务组织,默认加载主账簿				
				if (val.length == 1) {
					let pk_org = val[0].refpk;
					ajax({
						url: '/nccloud/fa/fapub/queryAccbookAndPeriod.do',
						data: {
							pk_org: pk_org&&pk_org.substr(pk_org.length-20,pk_org.length),
							pk_accbook: '',
							businessDate: getContext(loginContextKeys.businessDate) //业务日期
						},
						async: false,
						success: (res) => {
							if (res.data) {
								this.accountBook = [
									{
										refpk: res.data.accbook_value,
										refname: res.data.accbook_display
									}
								];
							}
						}
					});
				}
				if (this.financeOrg.length > 0) {
					this.setState({
						financeOrgIsNull: false
					});
				} else {
					this.setState({
						financeOrgIsNull: true
					});
				}
			},
			placeholder: '', //去除默认显示名称
			defaultValue: this.financeOrg,
			isMultiSelectedEnabled: true,
			disabled: selectedValue !== 'split' || this.status === 'edit',
			refcode: refer.financeOrg.refcode,
			queryCondition: () => {
				return { TreeRefActionExt: refer.financeOrg.TreeRefActionExt };
			}
		};
		let accountbookorgconfig = {
			onChange: (val) => {
				this.accountBook = val;
			},
			placeholder: '', //去除默认显示名称
			defaultValue: this.accountBook,
			isMultiSelectedEnabled: true,
			disabled: selectedValue !== 'split' || status === 'edit' || this.state.financeOrgIsNull,
			refcode: refer.accountBook.refcode,

			queryCondition: () => {
				let data = '';
				this.financeOrg.map((item, index) => {
					let pk = item.refpk;
					if(pk){
						if (index != 0) {
							data += ',';
						}
						pk = pk.substr(pk.length-20,pk.length);					
						data += pk;
					}
				});
				return {
					pk_org: data,
					TreeRefActionExt: refer.accountBook.TreeRefActionExt
				};
			}
		};

		return (
			<div className="" id="automatic" style={{ height: 'auto' }}>
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						<NCBackBtn
							onClick={() => {
								this.props.pushTo(routeToList, {pagecode:pageCode});
							}}
						/>
					</div>
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{pageName}</h2>
					</div>
					<div className="header-button-area">
						{createButtonApp({
							area: 'card_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-table-area content-area">
					<div className="org-accbook">
						<p className="text">{getMultiLangByID('201200548A-000005')/* 国际化处理： 财务组织和资产账簿*/}</p>
						<div className="radio-area">
							<div>
								<NCRadio.NCRadioGroup
									name="checkRefer"
									selectedValue={selectedValue}
									onChange={(value) => {
										this.financeOrg = [];
										this.accountBook = [];
										this.setState({ selectedValue: value,financeOrgIsNull: true });
									}}
								>
									<NCRadio value="all" disabled={this.status === 'edit'}>
										{getMultiLangByID('201200548A-000006')/* 国际化处理： 全部财务组织和资产账簿*/}
									</NCRadio>
									<br />
									<NCRadio value="split" disabled={this.status === 'edit'}>
									{getMultiLangByID('201200548A-000007')/* 国际化处理： 多选财务组织和资产账簿*/}
									</NCRadio>
								</NCRadio.NCRadioGroup>
							</div>
							<div>
								<div>
									<p className="p-red">*</p>
									<p>{getMultiLangByID('201200548A-000008')/* 国际化处理： 财务组织*/}：</p>
									<AMRefer className="ref title-search-detail" config={financeorgconfig} />
								</div>
								<div>
									<p className="p-red">*</p>
									<p>{getMultiLangByID('201200548A-000009')/* 国际化处理： 资产账簿*/}：</p>
									<AMRefer className="ref title-search-detail" config={accountbookorgconfig} />
								</div>
							</div>
						</div>
					</div>
					<div className="auto-matic">
						<p className="text">{getMultiLangByID('201200548A-000010')/* 国际化处理： 自动计提结账的时间*/}</p>
						<div className="monthly">
							<p>
								<span>*</span>{getMultiLangByID('201200548A-000011')/* 国际化处理： 计提关账频率每月的*/}：
							</p>
							<div>
								{dateArr.map((date) => {
									if (date === 0) {
										return (
											<NCCol md={3}>
												<p
													className={`day${date} ${activeDay == date ? 'active-day' : ''}`}
													onClick={() => {
														this.setState({ activeDay: date });
													}}
												>
													{getMultiLangByID('201200548A-000012')/* 国际化处理： 最后一天*/}
												</p>
											</NCCol>
										);
									}
									return (
										<NCCol md={3}>
											<p
												className={`day${date} ${activeDay == date ? 'active-day' : ''}`}
												onClick={() => {
													this.setState({ activeDay: date });
												}}
											>
												{date}{getMultiLangByID('201200548A-000013')/* 国际化处理： 日*/}
											</p>
										</NCCol>
									);
								})}
							</div>
						</div>
						<div className="start-date">
							<p>{getMultiLangByID('201200548A-000014')/* 国际化处理： 开始时间*/}：</p>
							<div>
								<NCTimepicker
									onChange={onTimeChange.bind(this)}
									value={checkedTime}
									format={'HH:mm:ss'}
								/>
							</div>
						</div>
						<div className="implement-date">
							<p>{getMultiLangByID('201200548A-000015')/* 国际化处理： 执行期间*/}：</p>
							<div>
								<NCRadio.NCRadioGroup
									name="implement-date"
									selectedValue={periodTypeCode}
									onChange={(val) => {
										this.setState({
											periodTypeCode: val
										});
									}}
								>
									<NCRadio value="1">{getMultiLangByID('201200548A-000016')/* 国际化处理： 执行日期所在的会计期间*/}</NCRadio>
									<NCRadio value="2">{getMultiLangByID('201200548A-000017')/* 国际化处理： 执行日期的上一个会计期间*/}</NCRadio>
								</NCRadio.NCRadioGroup>
							</div>
						</div>
					</div>
					<div className="alert-area">
						<p>{getMultiLangByID('201200548A-000018')/* 国际化处理： 注：设置后，系统按约定的时间自动计提折旧，折旧分摊、结账。*/}</p>
						<p>{getMultiLangByID('201200548A-000023')/* 国际化处理： 自动计提结账结束后会给消息接收人发送消息（请到列表进行【消息接收配置】）*/}</p>
					</div>
				</div>
			</div>
		);
	}
}

AtuoMaticCard = createPage({})(AtuoMaticCard);

export default AtuoMaticCard;
