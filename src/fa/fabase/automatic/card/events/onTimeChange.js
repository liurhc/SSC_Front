/**
 * @author:zhangxxu
 * @description:选择时间 的事件
 *              --当选择时间时，将时间对象拼接成HH:mm:ss 格式，返回给this.state.checkTime 
 *              --默认返回23：00：00
 */

export default function(time) {
	this.setState({ checkedTime: time });
}
