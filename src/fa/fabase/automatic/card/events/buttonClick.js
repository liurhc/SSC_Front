import { ajax, toast, cardCache } from 'nc-lightapp-front';
import { pageConfig, constValue } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { msgUtils } = utils;
const { showConfirm, showMessage,MsgConst} = msgUtils;
let { updateCache, addCache } = cardCache;
const { url, tableId,pageCode } = pageConfig;

function buttonClick(props, id) {
	switch (id) {
		case 'Sure':
			sure.call(this, props);
			break;
		case 'Cancel':
			cancel.call(this, props);
			break;
		default:
			break;
	}
}

function sure(props) {
	if (!checkData.call(this)) {
		return;
	}
	let ajaxUrl = '';
	let status = this.status;
	let reqData = {};
	let time = this.state.activeDay + ',' + this.state.checkedTime;
	if (status === 'edit') {
		ajaxUrl = url.updateUrl;
		reqData = {
			selectedValue: this.state.selectedValue,
			alertRegistryPk: this.alertRegistryPk,
			periodTypeCode: this.state.periodTypeCode,
			financeOrgPk: this.financeOrg[0].refpk,
			accountBookPk: this.accountBook[0].refpk,
			time: time,
			ts: this.ts
		};
	} else {
		ajaxUrl = url.saveUrl;
		reqData = {
			selectedValue: this.state.selectedValue,
			alertRegistryPk: this.alertRegistryPk,
			periodTypeCode: this.state.periodTypeCode,
			financeOrg: this.financeOrg,
			accountBook: this.accountBook,
			time: time
		};
	}

	ajax({
		url: ajaxUrl,
		data: reqData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				//缓存修改
				modifyCache.call(this, status, data, reqData);
				//重设ts
				if (status === 'edit') {
					this.ts = data[tableId].rows[0].values.ts.value;
				}
				//提示用户	
				showMessage.call(this,props,{type: MsgConst.Type.SaveSuccess});
			}
		}
	});
}

function modifyCache(status, data, reqData) {
	modifyTimeDisplay(data);
	switch (status) {
		case 'edit':
			updateCache(
				pageConfig.pkName,
				reqData.alertRegistryPk,
				{ head: data },
				pageConfig.tableId,
				pageConfig.dataSource
			);
			break;
		case 'add':
			data[tableId].rows.map((item) => {
				addCache(
					item.values.alertRegistryPk.value,
					{
						head: {
							list_head: {
								rows: [ item ]
							}
						}
					},
					pageConfig.tableId,
					pageConfig.dataSource
				);
			});
			break;
		default:
			break;
	}
}

function checkData() {
	if (this.state.selectedValue === 'split') {
		if (this.financeOrg.length < 1) {
			toast({ content: constValue.tips.save.financeRequired, color: 'warning' });
			return false;
		}
		if (this.accountBook.length < 1) {
			toast({ content: constValue.tips.save.accountingBooKRequired, color: 'warning' });
			return false;
		}
	}
	if (!this.state.checkedTime) {
		toast({ content: constValue.tips.save.timeRequired, color: 'warning' });
		return false;
	}
	return true;
}

function modifyTimeDisplay(data) {
	data[tableId].rows.map((item) => {
		if (item.values.time && item.values.time.value) {
			let value = item.values.time.value;
			if (/[\d|\d\d],\d\d:\d\d:\d\d/.test(value)) {
				let day = value.split(',')[0];
				let time = value.split(',')[1];
				if ('0' == day) {
					item.values.time.display = constValue.everyMonthLastDay + ' ' + time;
				} else {
					item.values.time.display = constValue.everyMonth + day + constValue.day + ' ' + time;
				}
			}
		}
	});
}

function cancel(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: realCancel });
}

function realCancel(props){
	//跳回list界面
	props.pushTo('/list', {pagecode: pageCode});
}

export { buttonClick };
