import { pageConfig } from '../const';

const url = pageConfig.url;

export default function(props) {
	props.openTo(url.toBackgroundTaskUrl, {
		appcode: '101805PATD',
		pagecode: '101805PATD_page'
	});
}
