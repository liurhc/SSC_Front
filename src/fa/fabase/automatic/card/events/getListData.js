import { ajax } from 'nc-lightapp-front';
import { pageConfig, constValue } from '../const';

const { url, pageCode } = pageConfig;

export default function() {
	this.status = this.props.getUrlParam('status');
	//修改设置默认值
	if (this.status === 'edit') {
		this.alertRegistryPk = this.props.getUrlParam('alertRegistryPk');
		queryCardData.call(this, this.alertRegistryPk);
	}
}

//设置默认时间
function setEditDefaultDateTime(datetime) {
	if (!/[\d|\d\d],\d\d:\d\d:\d\d/.test(datetime)) {
		this.editDefaultTime.date = pageConfig.defaultData.selectedDay;
		this.editDefaultTime.checkedTime = pageConfig.defaultData.checkedTime;
		return;
	}
	let day = datetime.split(',')[0];
	let time = datetime.split(',')[1];
	this.editDefaultTime.date = day;
	this.editDefaultTime.checkedTime = time;
}

function queryCardData(pk) {
	ajax({
		url: url.queryByPk,
		data: {
			pagecode: pageCode,
			pk: pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success && data && data[0]) {
				data = data[0];
				setCardValue.call(this, data);
			}
		}
	});
}

function setCardValue(data) {
	this.financeOrg = [
		{
			refcode: data.financeOrgCode,
			refname: data.financeOrgName,
			refpk: data.financeOrgPk
		}
	];
	this.accountBook = [
		{
			refcode: data.accountBookCode,
			refname: data.accountBookName,
			refpk: data.accountBookPk
		}
	];
	this.ts = data.ts;
	setEditDefaultDateTime.call(this, data.time);
	this.checkedDate = data.time;
	this.setState({
		selectedValue: 'split',
		checkedTime: this.editDefaultTime.checkedTime,
		activeDay: this.editDefaultTime.date,
		periodTypeCode: data.periodTypeCode
	});
}
