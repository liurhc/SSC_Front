import { pageConfig } from '../const';
import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { loginContextKeys, getContext,loginContext} = LoginContext;
const { pageCode } = pageConfig;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pageCode
		},
		(data) => {
			if (data) {
				if (data.context) {
					//获取登录上下文信息
					loginContext.call(this, data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					props.meta.setMeta(meta);
				}
			}
		}
	);
}
