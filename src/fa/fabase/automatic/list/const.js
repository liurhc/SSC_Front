import { commonConfig, commonConstValue } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/*
* list常量定义
* */
export const pageConfig = {
	...commonConfig,
	pageCode: '201200548A_list',
	searchId: 'searchArea',
	routeToCard: '/card',
	url: {
		deleteUrl: '/nccloud/fa/automatic/delete.do',
		queryUrl: '/nccloud/fa/automatic/query.do',
		queryList: '/nccloud/fa/automatic/querypagelistbypks.do',
		sendMsgConfig: '/nccloud/fa/automatic/sendmsgconfig.do'
	}
};

export const constValue = {
	...commonConstValue,
	tips: {
		tipLable: getMultiLangByID('201200548A-000033')/* 国际化处理： 提示*/,
		delete: {
			success: getMultiLangByID('msgUtils-000015')/* 国际化处理： 删除成功*/,
			sureDelete: getMultiLangByID('msgUtils-000001')/* 国际化处理： 确定要删除吗？*/,
			pleseCheck: getMultiLangByID('201200548A-000036')/* 国际化处理： 请先选中需要删除的数据*/
		}
	}
};
