import React, { Component } from 'react';
import { createPage, base, ajax } from 'nc-lightapp-front';
import { modalConfig, metaConfig } from './const';
import { buttonClick, tableButtonClick, tableBeforeEvent, tableAfterEvent, querySendMsgConfig } from './handle';
import './index.less';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { NCButton, NCTabs,NCHotKeys, NCTooltip } = base;
const NCTabPane = NCTabs.NCTabPane;

class PrealertDevelopModal extends Component {
	// react：构造函数
	constructor(props) {
		super(props);
		this.state = {
			showModal: false
		};
		let meta = props.meta.getMeta();
		let opCol = {
			label: getMultiLangByID('amcommon-000000')/* 国际化处理： 操作*/,
			attrcode: 'opr',
			itemtype: 'customer',
			visible: true,
			fixed: 'right',
			render(text, record, index) {
				let buttonAry = [ 'msgTableDel' ];
				return props.button.createOprationButton(buttonAry, {
					area: 'msg_table_body',
					buttonLimit: 3,
					onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index)
				});
			}
		};
		meta.messageTable = metaConfig.messageTable;
		let items = meta.messageTable.items;
		//解决回退进入页面追加多个操作列
		if(items && items[items.length - 1].attrcode != 'opr'){
			items.push(opCol);
		}else {
			items.splice(items.length - 1,1,opCol);
		}
		props.button.setButtons(metaConfig.buttons);
		props.meta.setMeta(meta);
	}

	componentWillReceiveProps(nextprops) {
		if (nextprops.showModal != this.state.showModal) {
			this.setState(
				{
					showModal: nextprops.showModal
				},
				() => {
					if (this.state.showModal) {
						querySendMsgConfig.call(this, this.props, this.props.alertregistrypk);
					}
				}
			);
		}
	}

	componentDidMount() {}

	// react：界面渲染函数
	render() {
		let { editTable, button,onSure } = this.props;
		let { createButtonApp } = button;
		let { createEditTable } = editTable;
		const { NCModal } = base;

		return (
			<div>
				<NCModal
					size="lg"
					dialogClassName="model-tab-table"
					show={this.state.showModal}
					onHide={() => {
						this.props.closeModal();
					}}
				>
					<NCModal.Header closeButton='true'>
						<NCModal.Title>{getMultiLangByID('201200548A-000025')/* 国际化处理： 消息接收配置*/}</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>
						<div className="BGRegister">							
							<div className="messageTable">
								<div className="message-config-buttons">
									{createButtonApp({
										area: 'msg_table_header',
										onButtonClick: buttonClick.bind(this),
										popContainer: document.querySelector('.btnBox')
									})}
								</div>
								{createEditTable(modalConfig.msgTableId, {
									onAfterEvent: tableAfterEvent.bind(this),
									onBeforeEvent: tableBeforeEvent.bind(this), //可以使用编辑前时间改参照了
									isAddRow: true
								})}
							</div>
							{/* </NCTabPane>
							</NCTabs> */}
						</div>
					</NCModal.Body>
					<NCModal.Footer>
						<NCTooltip
							placement="top"
							inverse
							overlay={`${getMultiLangByID('201200548A-000026')/* 国际化处理： 确定*/}  (${
								NCHotKeys.USUAL_KEYS.NC_MODAL_CONFIRM
							})`}
							trigger={["hover", "focus"]}
							className="model-helper-overlay"
						>
							<NCButton
								color="primary"
								className="button-primary"
								size="large"
								onClick={() => {
									let tableRows = editTable.getAllRows(modalConfig.msgTableId, false);
									onSure(tableRows);
								}}
							>
								{getMultiLangByID('201200548A-000026')/* 国际化处理： 确定*/}
							</NCButton>
						</NCTooltip>
						<NCTooltip
							placement="top"
							inverse
							overlay={`${getMultiLangByID('msgUtils-000002')/* 国际化处理： 取消*/}  (${
								NCHotKeys.USUAL_KEYS.NC_MODAL_CALCEL
							})`}
							trigger={["focus", "hover"]}
							className="model-helper-overlay"
                    	>
							<NCButton
								color="danger"
								size="large"
								onClick={() => {
									this.props.closeModal();
								}}
							>
								{getMultiLangByID('msgUtils-000002')/* 国际化处理： 取消*/}
							</NCButton>
						</NCTooltip>
						<NCHotKeys
							keyMap={{
								sureBtnHandler: NCHotKeys.USUAL_KEYS.NC_MODAL_CONFIRM,
                				cancelBtnHandler: NCHotKeys.USUAL_KEYS.NC_MODAL_CALCEL
							}}
							handlers={{
								sureBtnHandler: () => {									
									let tableRows = editTable.getAllRows(modalConfig.msgTableId, false);
									onSure(tableRows);
								},
								cancelBtnHandler: () => {
									this.props.closeModal();									
								}
							}}
							// 是否启用组件
							enabled={true}
							// 是否为聚焦触发
    						focused={true}
    						// 默认display 可以设置 inline-block 等dispaly属性
						    display="inline-block"							
						>
						</NCHotKeys>
					</NCModal.Footer>
				</NCModal>
			</div>
		);
	}
}

export default (PrealertDevelopModal = createPage({})(PrealertDevelopModal));
