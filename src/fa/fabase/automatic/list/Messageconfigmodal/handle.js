import { ajax } from 'nc-lightapp-front';
import { modalConfig } from './const';

function buttonClick(props, id) {
	switch (id) {
		case 'msgTableAdd':
			addLine.call(this, props);
			break;
		default:
			break;
	}
}

function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		case 'msgTableDel':
			tableDeleteLine.call(this, props, index);
			break;
		default:
			break;
	}
}

function tableBeforeEvent(props, moduleId, item, index, value, record) {
	switch (moduleId) {
		case 'messageTable':
			if (item.attrcode == 'refCode') {
				let receiverTypeID = record.values.receiverTypeID.value;
				let refcode = '';
				let queryCondition = {};
				let isShowUnit = false;
				let unitProps = {
					multiLang: {
						domainName: 'uapbd',
						// currentLocale: 'simpchn',
						moduleId: 'refer_uapbd'
					},
					placeholder: 'refer-000199' /*国际化处理：业务单元+集团*/,
					rootNode: { refname: 'refer-000199', refpk: 'root' } /*国际化处理：业务单元+集团*/,
					refType: 'tree',
					refName: 'refer-000200' /*国际化处理：业务单元+集团参照*/,
					refCode: 'uapbd.org.BusinessUnitAndGroupTreeRef',
					queryTreeUrl: '/nccloud/uapbd/ref/BusinessUnitAndGroupTreeRef.do',
					treeConfig: {
						name: [ 'refer-000002', 'refer-000003' ],
						code: [ 'refcode', 'refname' ]
					} /*国际化处理：编码，名称*/,
					isMultiSelectedEnabled: false
				};
				switch (receiverTypeID.toString()) {
					case '':
					case '1':
						//用户
						refcode = 'uap/refer/riart/userDefaultRefer/index.js';						
						isShowUnit = true;
						queryCondition = {};
						break;
					case '2':
						//角色
						refcode = 'uap/refer/riart/roleRefer/index.js';
						unitProps = {};
						isShowUnit = false;
						queryCondition = {};
						break;
					case '3':
						//岗位
						refcode = 'uapbd/refer/org/PostDefaultAllGridRef/index.js';
						unitProps = {
							multiLang: {
								domainName: 'uapbd',
								// currentLocale: 'simpchn',
								moduleId: 'refer_uapbd'
							},
							refType: 'tree',
							refName: 'refer-000201' /* 国际化处理： 业务单元*/,
							refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
							rootNode: { refname: 'refer-000201', refpk: 'root' } /* 国际化处理： 业务单元*/,
							placeholder: 'refer-000201' /* 国际化处理： 业务单元*/,
							queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
							treeConfig: {
								name: [ 'refer-000002', 'refer-000003' ],
								code: [ 'refcode', 'refname' ]
							} /*国际化处理：编码，名称*/,
							isMultiSelectedEnabled: false
						};
						isShowUnit = true;
						queryCondition = {
							isPost: 'Y',
							GridRefActionExt: 'nccloud.web.riacc.prealertdevelop.action.PostDefaultAllGridSqlBuilder'
						};
						break;
					case '6':
						//用户组
						refcode = 'uap/refer/riart/userGroupTreeRef/index.js';
						queryCondition = { isAuthFilter: false };
						isShowUnit = true;
						break;
					case '7':
						//角色组
						refcode = 'uap/refer/riart/roleGroupRefer/index.js';
						queryCondition = { isAuthFilter: false };
						isShowUnit = true;
						break;
					case '11':
						//职责
						refcode = 'uap/refer/riart/responsibilityTabelRef/index.js';
						unitProps = {};
						isShowUnit = false;
						queryCondition = {};
						break;
					case '12':
						//人员
						refcode = 'uapbd/refer/psninfo/PsndocRiartTreeGridRef/index';
						unitProps = {
							multiLang: {
								domainName: 'uapbd',
								// currentLocale: 'simpchn',
								moduleId: 'refer_uapbd'
							},
							refType: 'tree',
							refName: 'refer-000201' /* 国际化处理： 业务单元*/,
							refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
							rootNode: { refname: 'refer-000201', refpk: 'root' } /* 国际化处理： 业务单元*/,
							placeholder: 'refer-000201' /* 国际化处理： 业务单元*/,
							queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
							treeConfig: {
								name: [ 'refer-000002', 'refer-000003' ],
								code: [ 'refcode', 'refname' ]
							} /*国际化处理：编码，名称*/,
							isMultiSelectedEnabled: false
						};
						isShowUnit = true;
						queryCondition = {};
						break;
					default:
						break;
				}

				let meta = props.meta.getMeta();
				meta['messageTable'].items.map((table_item) => {
					//编码
					if (table_item.attrcode == 'refCode') {
						// table_item = refCodeDefaultConfig;
						table_item.refcode = refcode;
						table_item.unitProps = unitProps;
						table_item.isShowUnit = isShowUnit;
						table_item.queryCondition = queryCondition;
					}
				});
				props.meta.setMeta(meta);
				props.renderItem('table', 'messageTable', 'refCode', null);
				break;
			}
	}
	return true;
}

function tableAfterEvent(props, moduleId, key, value, changeValue, index, record) {
	
	switch (moduleId) {
		case 'messageTable':
			//参与者类型改变后,清空编码和名称
			if (key == 'receiverTypeID') {
				props.editTable.setValByKeyAndIndex(modalConfig.msgTableId, index, 'refCode', {
					value: null,
					display: null
				});
				props.editTable.setValByKeyAndIndex(modalConfig.msgTableId, index, 'refName', {
					value: null,
					display: null
				});
			}
			if (key == 'refCode') {
				//设置编码和名称
				if(value){
					props.editTable.setValByKeyAndIndex(modalConfig.msgTableId, index, 'refCode', {					
						display: value.refcode
					});
					if(value.refname){
						props.editTable.setValByKeyAndIndex(modalConfig.msgTableId, index, 'refName', {					
							display: value.refname
						});
					} else {
						props.editTable.setValByKeyAndIndex(modalConfig.msgTableId, index, 'refName', {
							value: null,
							display: null
						});
					}
				}				
			}
			break;
		default:
			break;
	}
}

function addLine(props) {
	props.editTable.addRow(modalConfig.msgTableId, undefined, true);
}

function tableDeleteLine(props, index) {
	props.editTable.deleteTableRowsByIndex(modalConfig.msgTableId, [ index ]);
}

function querySendMsgConfig(props, alertregistrypk) {
	let data = {
		alertregistrypk: alertregistrypk
	};
	ajax({
		url: modalConfig.url.querySendMsgConfigUrl,
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.editTable.setTableData(modalConfig.msgTableId, data.messageTable);
				} else {
					props.editTable.setTableData(modalConfig.msgTableId, { rows: [] });
				}
			}
		}
	});
}

export { buttonClick, tableButtonClick, tableBeforeEvent, tableAfterEvent, querySendMsgConfig };
