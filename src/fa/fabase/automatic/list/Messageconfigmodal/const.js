import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const modalConfig = {
	msgTableId: 'messageTable',
	url: {
		querySendMsgConfigUrl: '/nccloud/fa/automatic/querysendmsgconfig.do'
	}
};

const metaConfig = {
	buttons: [
		{
			id: '0001A41000000006J5B5',
			type: 'buttongroup',
			key: 'buttonmsg_table_opr',
			title: '',
			area: 'msg_table_header',
			children: [
				{
					id: '0001A41000000006J5B1',
					type: 'general_btn',
					btncolor: 'button_main',
					key: 'msgTableAdd',
					title: getMultiLangByID('201200548A-000039')/* 国际化处理： 新增*/,
					area: 'msg_table_header',
					parentCode: 'buttonmsg_table_opr',
					children: []
				}
			]
		},
		{
			id: '0001A41000000006J5B2',
			type: 'button_secondary',
			key: 'msgTableDel',
			title: getMultiLangByID('msgUtils-000000')/* 国际化处理： 删除*/,
			area: 'msg_table_body',
			parentCode: 'buttonmsg_row_opr',
			children: []
		}
	],
	[modalConfig.msgTableId]: {
		moduletype: 'table',
		pagination: false,
		// editType: 'inline', //or popover
		status: 'edit', //or edit
		// lineHeight: '40px',
		items: [
			{
				label: getMultiLangByID('201200548A-000041')/* 国际化处理： 参与者类型*/,
				attrcode: 'receiverTypeID',
				itemtype: 'select',
				required: true,
				visible: true,
				options: [
					{
						display: getMultiLangByID('201200548A-000042')/* 国际化处理： 用户*/,
						value: 1
					},
					{
						display: getMultiLangByID('201200548A-000043')/* 国际化处理： 角色*/,
						value: 2
					},
					{
						display: getMultiLangByID('201200548A-000044')/* 国际化处理： 岗位*/,
						value: 3
					},
					{
						display: getMultiLangByID('201200548A-000045')/* 国际化处理： 用户组*/,
						value: 6
					},
					{
						display: getMultiLangByID('201200548A-000046')/* 国际化处理： 角色组*/,
						value: 7
					},
					{
						display: getMultiLangByID('201200548A-000047')/* 国际化处理： 职责*/,
						value: 11
					},
					{
						display: getMultiLangByID('201200548A-000048')/* 国际化处理： 人员*/,
						value: 12
					}
				],
				initialvalue: {
					value: 1,
					display: getMultiLangByID('201200548A-000042')/* 国际化处理： 用户*/
				}
			},
			{
				label: getMultiLangByID('201200548A-000041')/* 国际化处理： 参与者类型*/,
				attrcode: 'receiverTypeName',
				itemtype: 'input',
				visible: false
			},
			{
				label: getMultiLangByID('201200548A-000049')/* 国际化处理： 参照主键*/,
				attrcode: 'refPK',
				visible: false,
				itemtype: 'input'
			},
			{
				label: getMultiLangByID('201200548A-000050')/* 国际化处理： 编码*/,
				attrcode: 'refCode',
				required: true,
				itemtype: 'refer',
				refcode: 'uap/refer/riart/userDefaultRefer/index.js',
				visible: true,
				isMultiSelectedEnabled: false
			},
			{
				label: getMultiLangByID('201200548A-000051')/* 国际化处理： 名称*/,
				attrcode: 'refName',
				visible: true,
				disabled: true,
				itemtype: 'input'
			},
			{
				label: getMultiLangByID('201200548A-000052')/* 国际化处理： 消息中心*/,
				attrcode: 'messageCenter',
				required: true,
				visible: true,
				itemtype: 'checkbox_switch',
				initialvalue: {
					value: true,
					display: ''
				}
			},
			{
				label: getMultiLangByID('201200548A-000053')/* 国际化处理： 电子邮件*/,
				required: true,
				attrcode: 'email',
				visible: true,
				itemtype: 'checkbox_switch'
			},
			{
				label: getMultiLangByID('201200548A-000054')/* 国际化处理： 手机短信*/,
				required: true,
				attrcode: 'phone',
				visible: true,
				itemtype: 'checkbox_switch'
			}
		]
	}
};

export { modalConfig, metaConfig };
