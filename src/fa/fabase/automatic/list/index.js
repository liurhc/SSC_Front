import React, { Component } from 'react';
import { createPage, base, createPageIcon} from 'nc-lightapp-front';
import { initTemplate, buttonClick, pageInfoClick, searchBtnClick, selectedHandle, saveMessageConfig } from './events';
import { pageConfig } from './const';
import Messageconfigmodal from './Messageconfigmodal';

const { NCAffix } = base;
const { pageCode, pageName, searchId, tableId, url, dataSource, pkName } = pageConfig;

/**
 * @author:lijun13
 * @description:自动计提结账设置
 */

class AtuoMaticList extends Component {
	constructor(props) {
		super(props);
		initTemplate.call(this, props);
		this.state = {
			showModal: false
		};
		this.selectedrecord = undefined;
	}

	render() {
		let { table, search, ncmodal } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createModal } = ncmodal;
		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{pageName}</h2>
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						showAdvBtn: false
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						handlePageInfoChange: pageInfoClick.bind(this),
						pkname: pkName,
						showIndex: true,
						showCheck: true,
						onSelectedAll: selectedHandle.bind(this),
						onSelected: selectedHandle.bind(this),
						dataSource: dataSource
					})}
				</div>
				<div>
					{/* 提示框 */}
					{createModal(`${pageCode}-confirm`, { color: 'warning' })}
				</div>
				<div>
					<Messageconfigmodal
						showModal={this.state.showModal}
						alertregistrypk={this.alertregistrypk}
						closeModal={() => {
							this.setState({
								showModal: false
							});
						}}
						onSure={(data) => {
							saveMessageConfig.call(this, this.props, data);
						}}
						{...this.props}
					/>
				</div>
			</div>
		);
	}
}

AtuoMaticList = createPage({})(AtuoMaticList);

export default AtuoMaticList;
