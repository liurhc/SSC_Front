import { ajax } from 'nc-lightapp-front';
import { pageConfig, constValue } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils} = utils;
const { setListValue, setQueryInfoCache, listConst } = listUtils;

const { searchId, tableId, url } = pageConfig;
//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, queryInfo, isRefresh) {
	if (searchVal) {
		let pageInfo = props.table.getTablePageInfo(tableId);
		let data = queryInfo;
		data.pageInfo = pageInfo;
		// 缓存查询条件
		setQueryInfoCache.call(this, queryInfo, isRefresh, type);
		ajax({
			url: url.queryUrl,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						modifyTimeDisplay.call(this, data);
					}
					setListValue.call(
						this,
						props,
						res,
						tableId,
						isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
					);
				}
			}
		});
	}
}

export function modifyTimeDisplay(data) {
	data[tableId].rows.map((item) => {
		if (item.values.time && item.values.time.value) {
			let value = item.values.time.value;
			if (/[\d|\d\d],\d\d:\d\d:\d\d/.test(value)) {
				let day = value.split(',')[0];
				let time = value.split(',')[1];
				if ('0' == day) {
					item.values.time.display = constValue.everyMonthLastDay + ' ' + time;
				} else {
					item.values.time.display = constValue.everyMonth + day + constValue.day + ' ' + time;
				}
			}
		}
	});
}
