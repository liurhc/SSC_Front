import initTemplate from './initTemplate';
import { buttonClick, saveMessageConfig } from './buttonClick';
import tableButtonClick from './tableButtonClick';
import pageInfoClick from './pageInfoClick';
import searchBtnClick from './searchBtnClick';
import selectedHandle from './selectedHandle';

export {
	initTemplate,
	buttonClick,
	tableButtonClick,
	pageInfoClick,
	searchBtnClick,
	selectedHandle,
	saveMessageConfig
};
