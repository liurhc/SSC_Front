import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { modifyTimeDisplay } from './searchBtnClick';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils} = utils;
const { setListValue } = listUtils;

const { pageCode, url } = pageConfig;

export default function(props, config, pks) {
	let data = {
		allpks: pks,
		pagecode: pageCode
	};
	//得到数据渲染到页面
	ajax({
		url: url.queryList,
		data: data,
		success: function(res) {
			let { success, data } = res;
			if (success) {
				if (data) {
					modifyTimeDisplay.call(this, data);
				}
				setListValue.call(this, props, res);
			}
		}
	});
}
