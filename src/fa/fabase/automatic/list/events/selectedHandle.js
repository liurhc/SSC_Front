import { pageConfig } from '../const';
const { tableId } = pageConfig;

export default function(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	let disabled = false;
	if (checkedRows.length == 0) {
		disabled = true;
	}
	props.button.setDisabled({ Delete: disabled });
}
