import { tableButtonClick } from './';
import { pageConfig } from '../const';
import ampub from 'ampub';
import fa from 'fa';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { pageCode, searchId, tableId } = pageConfig;
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

let defaultConfig = {
	searchId: searchId
};

export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pageCode
		},
		function(data) {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001')/* 国际化处理： 确定要删除吗？*/);
					props.button.setDisabled({ Delete: true });
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(that, props, meta);
					addSearchAreaReferFilter(props, meta, defaultConfig);
					props.meta.setMeta(meta);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	let material_event = {
		label: getMultiLangByID('amcommon-000000')/* 国际化处理： 操作*/,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		width: '250px',
		fixed: 'right',
		render: (text, record, index) => {
			let buttonAry = [ 'MessageConfig', 'Edit', 'Delete' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(this, props, key, text, record, index)
			});
		}
	};
	meta[tableId].items.push(material_event);

	return meta;
}
