//表体行操作列处理
import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig, constValue } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { msgUtils } = utils;
const { showConfirm, showMessage,MsgConst} = msgUtils;
const { tableId, url, routeToCard,pageCode } = pageConfig;

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		case 'Edit':
			toCard.call(this, props, record);
			break;
		case 'Delete':
			doDelete.call(this, props, record, index);
			break;
		case 'MessageConfig':
			openMessageConfigWindow.call(this, props, record, index);
			break;
	}
}

function toCard(props, record) {
	props.pushTo(routeToCard, {
		status: 'edit',
		alertRegistryPk: record.alertRegistryPk.value,
		periodTypeCode: record.periodTypeCode.value,
		financeOrgName: record.financeOrgName.value,
		financeOrgCode: record.financeOrgCode.value,
		financeOrgPk: record.financeOrgPk.value,
		accountBookName: record.accountBookName.value,
		accountBookCode: record.accountBookCode.value,
		accountBookPk: record.accountBookPk.value,
		time: record.time.value,
		ts: record.ts.value,
		pagecode: pageCode
	});
}

function doDelete(props, record, index) {
	let data = [
		{
			alertRegistryPk: record.alertRegistryPk.value,
			ts: record.ts.value
		}
	];
	ajax({
		url: url.deleteUrl,
		data: data,
		success: (res) => {
			let { success } = res;
			if (success) {
				//删掉当前行
				props.table.deleteTableRowsByIndex(tableId, index);
				//删除缓存信息
				props.table.deleteCacheId(tableId, record.alertRegistryPk.value);				
				//提示用户
				showMessage.call(this,props,{type: MsgConst.Type.DeleteSuccess});
			}
		}
	});
}

function openMessageConfigWindow(props, record, index) {
	this.setState({
		showModal: true
	});
	if (record && record.alertRegistryPk && record.alertRegistryPk.value) {
		this.alertregistrypk = record.alertRegistryPk.value;
		this.ts = record.ts.value;
		this.index = index;
	}
}
