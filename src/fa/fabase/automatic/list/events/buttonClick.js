import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig, constValue } from '../const';
import selectedHandle from './selectedHandle';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils,msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showConfirm, showMessage,MsgConst} = msgUtils;
const { pageCode, tableId, url, routeToCard } = pageConfig;

function buttonClick(props, id) {
	switch (id) {
		case 'Add':
			this.props.pushTo('/card', {
				status: 'add',
				pagecode: pageCode
			});
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
	}
}

/**
 * 批量删除提示框
 * @param {*} props 
 */
function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this,props,{type: MsgConst.Type.ChooseDelete});
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: doDelete });
}

/**
 * 删除
 * @param {} props 
 */
function doDelete(props) {
	let deleteData = props.table.getCheckedRows(tableId);
	let data = [];
	let index = [];
	let params = deleteData.map((item, i) => {
		let id = item.data.values.alertRegistryPk.value;
		let itemData = {
			alertRegistryPk: id,
			ts: item.data.values.ts.value
		};
		data.push(itemData);
		index.push(item.index);
		return {
			id
		};
	});
	ajax({
		url: url.deleteUrl,
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				//删掉行
				this.props.table.deleteTableRowsByIndex(tableId, index);
				//删除缓存信息
				params.map((param) => {
					props.table.deleteCacheId(tableId, param.id);
				})
				//处理删除按钮状态
				selectedHandle.call(this, props);
				//提示用户
				showMessage.call(this,props,{type: MsgConst.Type.DeleteSuccess});
			}
		}
	});
}

function saveMessageConfig(props, tableRows) {
	let data = {
		pk_alertregistry: this.alertregistrypk,
		sendConfig: [],
		ts: this.ts
	};
	if (tableRows && tableRows.length > 0) {
		tableRows.map((row) => {
			//只保存不是删除的 把多出来的空行过滤掉
			let rowValue = row.values;
			if (row.status != 3 && rowValue.refCode.value != '') {
				data.sendConfig.push({
					receiverTypeID: rowValue.receiverTypeID.value,
					receiverid: rowValue.refCode.value,
					receiverdisplay: rowValue.refCode.display,
					email: rowValue.email.value,
					messageCenter: rowValue.messageCenter.value,
					phone: rowValue.phone.value
				});
			}
		});
	}
	ajax({
		url: url.sendMsgConfig,
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success && data) {
				this.setState({
					showModal: false
				});
				//消息设置成功更新ts
				props.table.setValByKeyAndIndex(tableId,this.index,'ts',{value:data});
				//提示用户
				toast({ content: getMultiLangByID('201200548A-000037')/* 国际化处理： 设置消息成功*/, color: 'success' });
			}
		}
	});
}

export { buttonClick, saveMessageConfig };
