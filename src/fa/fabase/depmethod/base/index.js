import React, { Component } from 'react';
import { base, high, toast, createPageIcon } from 'nc-lightapp-front';
import {
	beforeEvent,
	getAllDepmethod,
	addBtnClick,
	editBtnClick,
	delBtnClick,
	doSave,
	doToggleSeal,
	formulaClos,
	closeFormulaEditor,
	getFormulaTabData,
	initTemplate,
	depmethodIsUsing
} from './events';
import DepmethodItem from './DepmethodItem';
import './depmethod.less';
import { pageConfig } from './const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { FormulaEditor } = high;
const { formArea, modal, pageName } = pageConfig;
const { addModalId, editModalId } = modal;

class DepMethodBaseList extends Component {
	constructor(props) {
		super(props);
		this.amountformulaLength = 200; //默认设置公式的maxLength为200
		this.props = props;
		this.state = {
			depmethods: [],
			/**公式配置 */
			show: false,
			editingValue: ''
		};
		this.saveType = '';
		this.editingField = '';
		this.selectedDepmethod = undefined;
		this.formulaTabData = undefined;
		this.isLeaveOrRefresh = false;
		//查询公式编辑器数据
		getFormulaTabData.call(this);
		//当离开当前窗口时触发
		window.onbeforeunload = () => {
			if (this.isLeaveOrRefresh) {
				return '';
			}
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {
		getAllDepmethod.call(this);
	}

	render() {
		const { button, form, modal } = this.props;
		const { NCAffix } = base;
		let { createButtonApp } = button;
		let { createForm } = form;
		let { createModal } = modal;
		return (
			<div>
				<div className="nc-bill-list" id="fa-fabase-depmethod">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								{createPageIcon()}
								<h2 className="title-search-detail">{getMultiLangByID(pageName) /* 国际化处理： 模块名称*/}</h2>
							</div>
							<div className="header-button-area">
								{createButtonApp({
									area: formArea,
									buttonLimit: 3,
									onButtonClick: addBtnClick.bind(this),
									popContainer: document.querySelector('.btn-group')
								})}
							</div>
						</div>
					</NCAffix>
					<div className="nc-bill-card-area">
						{this.state.depmethods.map((depmethod, index) => {
							return (
								<DepmethodItem
									depmethod={depmethod}
									{...this.props}
									index={index}
									delBtnClick={delBtnClick.bind(this)}
									editBtnClick={editBtnClick.bind(this)}
									sealBtnClick={doToggleSeal.bind(this)}
									key={depmethod.values.pk_depmethod.value + depmethod.values.ts.value}
									queryIsUsing={depmethodIsUsing.bind(this)}
								/>
							);
						})}
						<div>
							{createModal(addModalId, {
								title: getMultiLangByID('201200512A-000012') /* 国际化处理： 新增折旧方法*/,
								size: '200',
								// className: 'senior',
								content: (
									<div className="addModal">
										{createForm(formArea, {
											onBeforeEvent: beforeEvent.bind(this)
										})}
									</div>
								),
								beSureBtnClick: doSave.bind(this),
								userControl: true,
								cancelBtnClick: () => {
									this.props.form.EmptyAllFormValue(formArea); //清空 form
									this.props.modal.close(addModalId);
									this.isLeaveOrRefresh = false;
								}
							})}
						</div>
						<div>
							{createModal(editModalId, {
								title: getMultiLangByID('201200512A-000013') /* 国际化处理： 修改折旧方法*/,
								size: '200',
								content: (
									<div className="editModal">
										{createForm(formArea, {
											onBeforeEvent: beforeEvent.bind(this)
										})}
									</div>
								),
								beSureBtnClick: doSave.bind(this),
								userControl: true,
								cancelBtnClick: () => {
									this.selectedDepmethod = undefined;
									this.props.modal.close(editModalId);
									this.isLeaveOrRefresh = false;
								}
							})}
						</div>
					</div>
				</div>
				<div>
					<FormulaEditor
						zIndex={300}
						ref="formulaEditor"
						value={this.state.editingValue}
						show={this.state.show}
						attrConfig={[
							{
								tab: this.formulaTabData && this.formulaTabData.tabname,
								TabPaneContent: formulaClos.bind(this),
								params: { name: this.formulaTabData && this.formulaTabData.tabname }
							}
						]}
						noShowFormula={[ getMultiLangByID('201200512A-000014') /* 国际化处理： 其它类型*/ ]}
						noShowAttr={[
							getMultiLangByID('201200512A-000015') /* 国际化处理： 表和字段*/,
							getMultiLangByID('201200512A-000001') /* 国际化处理： 元数据属性*/
						]}
						isValidateOnOK={true}
						onOk={(res) => {
							if (this.editingField === 'amountformula' || this.editingField === 'rateformula') {
								//校验公式长度
								if (res != null && res != '' && getLength(res) > this.amountformulaLength) {
									toast({
										color: 'danger',
										content: getMultiLangByID('201200512A-000020') /* 国际化处理： 公式长度超过最大值，请重新输入！*/
									});
									return;
								}
								this.selectedDepmethod.rows[0].values[this.editingField] = { value: res };
							}
							closeFormulaEditor.call(this);
						}} //点击确定回调
						onCancel={() => {
							closeFormulaEditor.call(this);
						}} //点击取消回调
						onHide={() => {
							closeFormulaEditor.call(this);
						}}
					/>
				</div>
			</div>
		);
	}
}

function getLength(str) {
	//获取字符串长度，中文2，其他1
	let realLength = 0,
		len = str.length,
		charCode = -1;
	for (let i = 0; i < len; i++) {
		charCode = str.charCodeAt(i);
		if (charCode >= 0 && charCode <= 128) {
			realLength += 1;
		} else {
			realLength += 4;
		}
	}
	return realLength;
}

export default DepMethodBaseList;
