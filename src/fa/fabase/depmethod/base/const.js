/**
 * 折旧方法常量
 */
const pageConfig = {
	formArea: 'list_head',
	pageName: '201200512A-000000' /* 国际化处理： 折旧方法*/,
	modal: {
		addModalId: 'depmethod-add-modal',
		editModalId: 'depmethod-item-edit-modal'
	},
	saveType: {
		addSave: 'addSave', //新增保存
		updateSave: 'updateSave' //修改保存
	},
	button: {
		add: 'Add',
		edit: 'Edit',
		delete: 'Delete'
	},
	url: {
		queryUrl: '/nccloud/fa/depmethod/query.do',
		insertUrl: '/nccloud/fa/depmethod/insert.do',
		deleteUrl: '/nccloud/fa/depmethod/delete.do',
		updateUrl: '/nccloud/fa/depmethod/update.do',
		sealUrl: '/nccloud/fa/depmethod/seal.do',
		unsealUrl: '/nccloud/fa/depmethod/unseal.do',
		isUsingUrl: '/nccloud/fa/depmethod/isusing.do',
		queryFormulaTabsUrl: '/nccloud/fa/depmethod/queryformulatab.do'
	}
};

export { pageConfig };
