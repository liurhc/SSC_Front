import React, { Component } from 'react';
import { base } from 'nc-lightapp-front';
import { pageConfig } from './const';
import { getMultiLangName } from './events';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

let { NCButton } = base;

const { button } = pageConfig;

class DepMethodItem extends Component {
	static defaultProps = {
		depmethod: {}
	};

	constructor(props) {
		super(props);
		this.state = {
			content: getMultiLangByID('201200512A-000016') /* 国际化处理： 确认停用？*/
		};
		props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000004') /* 国际化处理： 确定要删除所选数据吗？*/);
	}

	//删除按钮处理事件，弹出是否删除对话框
	delBtnClickHandler() {
		if (this.props.delBtnClick) {
			this.props.delBtnClick(this.props.depmethod);
		}
	}

	//修改按钮处理事件，弹出编辑对话框
	editBtnClickHandler() {
		if (this.props.editBtnClick) {
			this.props.editBtnClick(this.props.depmethod);
		}
	}

	//停用启用按钮
	sealBtnClickHandler() {
		if (this.props.sealBtnClick) {
			this.props.sealBtnClick(this.props.depmethod);
		}
	}

	onButtonClick(props, id) {
		switch (id) {
			case button.edit:
				this.editBtnClickHandler();
				break;

			case button.delete:
				this.delBtnClickHandler();
				break;

			default:
				break;
		}
	}

	render() {
		const { NCPopconfirm, NCTableSwitch } = base;
		const { index, queryIsUsing } = this.props;

		const title =
			this.props.depmethod.values.depmethod_code.value +
			' ' +
			getMultiLangName('depmethod_name', this.props.depmethod);
		const status =
			this.props.depmethod.values.enablestate.value == 2
				? getMultiLangByID('201200512A-000002') /* 国际化处理： 已启用*/
				: getMultiLangByID('201200512A-000003') /* 国际化处理： 已停用*/;
		const checked = this.props.depmethod.values.enablestate.value == 2 ? true : false;
		const amountformula = this.props.depmethod.values.amountformula.value;
		const rateformula = this.props.depmethod.values.rateformula.value;
		const preflag = this.props.depmethod.values.pre_flag.value;
		const pk = this.props.depmethod.values.pk_depmethod.value;
		return (
			<div className={`depmethod-item item-${index + 1}`}>
				<div className="depmethod-title">
					<div className="title-name" title={title}>
						{title}
					</div>
					<div>
						<div className="title-status">{status}</div>
						<NCPopconfirm
							trigger="click"
							placement="top"
							content={this.state.content}
							onClose={this.sealBtnClickHandler.bind(this)}
						>
							<span>
								<NCTableSwitch
									checked={checked}
									value={checked}
									onChange={(value) => {
										if (!value) {
											let callback = (isUsing) => {
												let content = isUsing
													? getMultiLangByID('201200512A-000004') /* 国际化处理： 使用中，确认停用？*/
													: getMultiLangByID('201200512A-000016') /* 国际化处理： 确认停用？*/;
												if (content != this.state.content) {
													this.setState({
														content: content
													});
												}
											};
											queryIsUsing(pk, callback, false);
										} else {
											let content = getMultiLangByID('201200512A-000017') /* 国际化处理： 确认启用？*/;
											if (content != this.state.content) {
												this.setState({
													content: content
												});
											}
										}
									}}
								/>
							</span>
						</NCPopconfirm>
						<div className="depmethod-btn">
							{preflag != 1 ? ( //不是预置数据
								<div>
									<NCButton onClick={this.editBtnClickHandler.bind(this)}>
										{getMultiLangByID('201200512A-000005') /* 国际化处理： 修改*/}
									</NCButton>
									<NCPopconfirm
										trigger="click"
										placement="top"
										content={getMultiLangByID('msgUtils-000001') /* 国际化处理： 确认要删除吗？*/}
										onClose={this.delBtnClickHandler.bind(this)}
									>
										<NCButton>{getMultiLangByID('msgUtils-000000') /* 国际化处理： 删除*/}</NCButton>
									</NCPopconfirm>
								</div>
							) : (
								''
							)}
						</div>
					</div>
					{/* <div className="depmethod-btn">
						
					</div> */}
				</div>
				<div className="depmethod-info">
					<div>
						<p>
							{getMultiLangByID('201200512A-000006') /* 国际化处理： 月折旧额公式*/}
							<span>{amountformula}</span>
						</p>
					</div>
					<div>
						<p>
							{getMultiLangByID('201200512A-000007') /* 国际化处理： 月折旧率公式*/}
							<span>{rateformula}</span>
						</p>
					</div>
				</div>
			</div>
		);
	}
}

export default DepMethodItem;
