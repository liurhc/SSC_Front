import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { url } = pageConfig;

/**
 * 查询全部折旧方法
 */
function getAllDepmethod() {
	ajax({
		url: url.queryUrl,
		data: {
			isShowSeal: true,
			nodeType: this.props.nodeType
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				const depmethods = getDataAfter.call(this, data.list_head.rows);
				this.setState({
					depmethods
				});
			}
		}
	});
}

//请求列表数据 after
function getDataAfter(depmethodList) {
	//按照需求将"不计提"的排到最后面
	//除了“不计提”外的其他元素
	let arr1 = depmethodList.filter((depmethod) => {
		return depmethod.values.depmethod_code.value != '01';
	});

	//只有“不计提”元素
	let arr2 = depmethodList.filter((depmethod) => {
		return depmethod.values.depmethod_code.value == '01';
	});
	return arr1.concat(arr2);
}

export { getAllDepmethod };
