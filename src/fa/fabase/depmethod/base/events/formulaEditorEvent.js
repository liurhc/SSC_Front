import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { formulaData, saveType, modal, url, formArea } = pageConfig;
const { addSave, updateSave } = saveType;
const { addModalId, editModalId } = modal;
/**
 * 公式编辑器定义字段
 */
function formulaClos({ setName, setExplain, name }) {
	let formulaTabsItemsData = this.formulaTabData.tabitems;
	return (
		// <ul style={{ height: 200 }}>
		<ul>
			{formulaTabsItemsData.map(function(dataItem) {
				return (
					<li
						// onClick={() => {
						// 	setExplain(dataItem);
						// }}
						onDoubleClick={() => {
							setName(dataItem);
						}}
					>
						{dataItem}
					</li>
				);
			})}
		</ul>
	);
}

/**
 * 公式编辑器关闭
 */
function closeFormulaEditor() {
	//显示from
	if (this.saveType == updateSave) {
		this.props.modal.show(editModalId);
	} else if (this.saveType == addSave) {
		this.props.modal.show(addModalId);
	}
	//给form赋打开公式编辑器之前值
	let fromData = this.selectedDepmethod;
	if (
		fromData &&
		fromData.rows &&
		fromData.rows.length > 0 &&
		fromData.rows[0] &&
		fromData.rows[0].values &&
		Object.keys(fromData.rows[0].values).length > 0
	) {
		this.props.form.setAllFormValue({ [formArea]: fromData });
	}
	//隐藏公式编辑器
	this.setState({
		show: false
	});
}

/**
 * 公式编辑器页签数据查询
 */
function getFormulaTabData() {
	ajax({
		url: url.queryFormulaTabsUrl,
		data: null,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				this.formulaTabData = data;
			}
		}
	});
}

export { formulaClos, closeFormulaEditor, getFormulaTabData };
