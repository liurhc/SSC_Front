import { pageConfig } from '../const';
const { modal, saveType, formArea } = pageConfig;
const { addModalId, editModalId } = modal;

export default function beforeEvent(props, moduleId, key, value, changedrows, val) {
	this.editingField = key;
	//公式字段
	if (key === 'rateformula' || key === 'amountformula') {
		let fromData = props.form.getAllFormValue(formArea);
		if (
			fromData &&
			fromData.rows &&
			fromData.rows.length > 0 &&
			fromData.rows[0] &&
			fromData.rows[0].values &&
			Object.keys(fromData.rows[0].values).length > 0
		) {
			this.selectedDepmethod = fromData;
		}
		//关闭from
		if (this.saveType === saveType.updateSave) {
			this.props.modal.close(editModalId);
		} else if (this.saveType === saveType.addSave) {
			this.props.modal.close(addModalId);
		}
		//显示公式编辑器
		this.setState({
			show: true,
			editingValue: value != null ? value.value : ''
		});
	}
	return true;
}
