import { pageConfig } from '../const';
import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { loginContext } = LoginContext;
const { formArea } = pageConfig;

export default function(props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: props.pageCode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					//表单设置为编辑态
					meta[formArea].status = 'edit';
					props.meta.setMeta(meta);
					evaluateMaxLength.call(this, props, formArea, 'amountformula');
				}
			}
		}
	);
}

//为公式字段赋maxlength
function evaluateMaxLength(props, area, field) {
	let itemMeta = getMetaByField(props, area, field);
	if (itemMeta && itemMeta.maxlength && itemMeta.maxlength != '') {
		let num = parseInt(itemMeta.maxlength);
		this.amountformulaLength = num > 0 ? num : this.amountformulaLength;
	}
}
//获取指定字段的模板属性
function getMetaByField(props, area, field) {
	const meta = props.meta.getMeta();
	const items = meta[area].items;
	const itemMeta = items.filter((item) => {
		return item.attrcode === field;
	});
	return itemMeta && (Array.isArray(itemMeta) ? itemMeta[0] : null);
}
