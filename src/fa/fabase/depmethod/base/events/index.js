import initTemplate from './initTemplate';
import beforeEvent from './beforeEvent';
import { getAllDepmethod } from './getPageData';
import {
	addBtnClick,
	delBtnClick,
	editBtnClick,
	doSave,
	doDelete,
	doToggleSeal,
	depmethodIsUsing,
	getMultiLangName
} from './buttonClick';
import { closeFormulaEditor, getFormulaTabData, formulaClos } from './formulaEditorEvent';

export {
	initTemplate,
	beforeEvent,
	getAllDepmethod,
	addBtnClick,
	delBtnClick,
	editBtnClick,
	doSave,
	doDelete,
	doToggleSeal,
	getFormulaTabData,
	closeFormulaEditor,
	formulaClos,
	depmethodIsUsing,
	getMultiLangName
};
