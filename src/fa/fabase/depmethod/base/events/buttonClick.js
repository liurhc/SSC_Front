import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { MsgConst, showMessage } = msgUtils;
const { LoginContext } = components;
const { loginContextKeys, getContext } = LoginContext;
const { url, saveType, modal, formArea } = pageConfig;
const { addSave, updateSave } = saveType;
const { addModalId, editModalId } = modal;

/**
 * 新增按钮点击
 */
function addBtnClick() {
	//保存类型
	this.saveType = addSave;
	//展示新增模态框
	this.props.modal.show(addModalId);
	this.isLeaveOrRefresh = true;
	//每次新增时清空 form 内容
	this.props.form.EmptyAllFormValue(formArea);
	this.selectedDepmethod = undefined;
}

//展示删除modal框
function delBtnClick(depmethodDel) {
	this.selectedDepmethod = depmethodDel;
	let pk_depmethod = this.selectedDepmethod.values.pk_depmethod.value;
	depmethodIsUsing.call(this, pk_depmethod, showDelTips);
}

function showDelTips(isUsing) {
	if (!isUsing) {
		//没被使用-删除
		doDelete.call(this);
	} else {
		//被使用-提示被使用不能删除
		toast({ color: 'danger', content: getMultiLangByID('201200512A-000008') /* 国际化处理： 该折旧方法正在被使用，不能删除！*/ });
	}
}

//展示编辑modal框
function editBtnClick(depmethodEdit) {
	//展示修改模态框
	this.props.modal.show(editModalId);
	this.isLeaveOrRefresh = true;
	//设置保存类型
	this.saveType = updateSave;
	//这里将 depmethodEdit copy 一份赋给 form， 否则在form 清空的时候会清空 this.state.depmethods 对应的值
	//（因为引用的是同一个对象）
	const depmethodCopy = JSON.parse(JSON.stringify(depmethodEdit));
	this.selectedDepmethod = depmethodCopy;
	this.props.form.setAllFormValue({ [formArea]: { rows: [ depmethodCopy ] } });
}

/**
 * 新增/修改
 * @param {newVO} depmethodNew 
 */
function doSave() {
	//获取 modal 输入的值
	let formData = this.props.form.getAllFormValue(formArea);
	let depmethodNew = formData.rows[0];
	//前端校验
	let validateRes = validate.call(this, depmethodNew);
	if (!validateRes) {
		return;
	}
	let requestUrl = url.insertUrl; //新增保存
	if (this.saveType == updateSave) {
		requestUrl = url.updateUrl; //修改保存
	}

	const depmethodNewCopy = JSON.parse(JSON.stringify(depmethodNew));

	let data = {
		pageid: this.props.pageCode,
		model: {
			areaType: 'form',
			areacode: formArea,
			rows: [ depmethodNewCopy ]
		}
	};
	this.props.validateToSave(data, () => {
		ajax({
			url: requestUrl,
			data: data,
			success: (res) => {
				if (res.success && res.data) {
					//合并返回的数据到当前数据的尾部
					const newDepmethod = res.data.list_head.rows[0];

					if (this.saveType == addSave) {
						//新增后返回
						const allDepmethods = [ ...this.state.depmethods ];
						allDepmethods.push(newDepmethod);
						this.setState({
							depmethods: allDepmethods
						});
					} else if (this.saveType == updateSave) {
						//修改后返回

						//合并
						let allDepmethodsNew = this.state.depmethods.map((depmethodOld) => {
							if (depmethodOld.values.pk_depmethod.value == newDepmethod.values.pk_depmethod.value) {
								return newDepmethod;
							} else {
								return depmethodOld;
							}
						});
						this.setState({
							depmethods: allDepmethodsNew
						});
					}

					if (this.saveType === addSave) {
						showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess });
						this.props.modal.close(addModalId);
						this.isLeaveOrRefresh = false;
					} else {
						toast({ color: 'success', content: getMultiLangByID('201200512A-000009') /* 国际化处理： 修改成功*/ });
						this.props.modal.close(editModalId);
						this.isLeaveOrRefresh = false;
					}

					//保存成功后更新界面
					this.props.form.EmptyAllFormValue(formArea);
				}
			},
			error: (err) => {
				toast({ color: 'danger', content: err.message });
			}
		});
	});
}

//*************************    校验   *************************
//前端校验：检验成功返回 false, 校验失败返回 错误消息
function validate(depmethodNew) {
	//检验必输项
	const checkResult = this.props.form.isCheckNow(formArea);
	if (!checkResult) {
		return false;
	}
	let reg = /^[0-9a-zA-Z]+$/;
	//折旧方法编码非法字符校验
	if (depmethodNew.values.depmethod_code.value != '' && !reg.test(depmethodNew.values.depmethod_code.value)) {
		toast({
			content: getMultiLangByID('201200512A-000021') /* 国际化处理： 折旧方法编码输入不合法，编号只能输入字母和数字！*/,
			color: 'warning'
		});
		return false;
	}

	return true;
}

/**
 * 删除
 * @param {VO} depmethodDel 
 */
function doDelete() {
	let depmethodDel = this.selectedDepmethod;
	let requestUrl = url.deleteUrl;
	let data = {
		pageid: this.props.pageCode,
		model: {
			areaType: 'table',
			pageinfo: null,
			rows: [ depmethodDel ]
		}
	};
	ajax({
		url: requestUrl,
		data: data,
		success: (res) => {
			if (res.success && res.data) {
				//从当前列表中删除
				const allDepmethods = this.state.depmethods.filter((depmethod) => {
					return depmethod.values.pk_depmethod.value != depmethodDel.values.pk_depmethod.value;
				});
				this.setState({
					depmethods: allDepmethods
				});
				showMessage.call(this, this.props, { type: MsgConst.Type.DeleteSuccess });
			}
		},
		error: (err) => {
			toast({ color: 'danger', content: err.message });
		}
	});
}

/**
 * 停用/启用
 * @param {VO} depmethod 
 */
function doToggleSeal(depmethodSeal) {
	this.selectedDepmethod = depmethodSeal;
	let depmethod = this.selectedDepmethod;
	let requestUrl = url.sealUrl;
	let flag = false;
	if (depmethod.values.enablestate.value == 3) {
		requestUrl = url.unsealUrl;
		flag = true;
	}
	let dataHandle = {
		pageid: this.props.pageCode,
		model: {
			areaType: 'table',
			pageinfo: null,
			rows: [ depmethod ]
		}
	};
	handleSealOrUnseal.call(this, dataHandle, requestUrl, flag);
}

//停启用处理
function handleSealOrUnseal(dataHandle, requestUrl, flag) {
	ajax({
		url: requestUrl,
		data: dataHandle,
		success: (res) => {
			if (res.success && res.data) {
				//合并返回的数据
				const newDepmethod = res.data.list_head.rows[0];
				//合并
				let allDepmethodsNew = this.state.depmethods.map((depmethodOld) => {
					if (depmethodOld.values.pk_depmethod.value == newDepmethod.values.pk_depmethod.value) {
						return newDepmethod;
					} else {
						return depmethodOld;
					}
				});
				this.setState({
					depmethods: allDepmethodsNew
				});
				if (flag) {
					showMessage.call(this, this.props, { type: MsgConst.Type.EnableSuccess });
				} else {
					showMessage.call(this, this.props, { type: MsgConst.Type.DisableSuccess });
				}
			} else {
				toast({ color: 'danger', content: res.error.message });
			}
		},
		error: (err) => {
			toast({ color: 'danger', content: err.message });
		}
	});
}

/**
 * 是否使用中
 * @param {折旧方法主键} pk_depmethod 
 * @param {回调函数} callback function(isUsing)
 */
function depmethodIsUsing(pk_depmethod, callback, isCheck = true) {
	if (!pk_depmethod) {
		return;
	}
	let data = { pk_depmethod };
	if (isCheck) {
		ajax({
			url: url.isUsingUrl,
			data: data,
			//async: false, //改为同步处理
			success: (res) => {
				let { data, success } = res;
				if (success && data && callback) {
					callback.call(this, data.isUsing);
				}
			},
			error: (err) => {
				toast({ color: 'danger', content: err.message });
			}
		});
	} else {
		callback.call(this, true);
	}
}

/**
 * 获取多语字段名称 字段值 * 
 * @param {*} multiLangName 多语字段名称
 * @param {*} item 数据对象 (如果有值返回当前语种值)
 * @return item 为空时返回当前语种字段 , 有值取当前语种值
 */
function getMultiLangName(multiLangName, item) {
	let index = getContext(loginContextKeys.languageIndex);
	if (!index) {
		index = ''; //未加载到默认主语种
	}
	//如果items有值返回字段value
	if (item) {
		//取当前语种值如果没有默认取主语种值
		let value = item.values[multiLangName + index].value;
		if (!value) {
			value = item.values[multiLangName].value;
		}
		return value;
	} else {
		//返回当前语种多语字段
		return multiLangName + index;
	}
}

export { addBtnClick, delBtnClick, editBtnClick, doSave, doDelete, doToggleSeal, depmethodIsUsing, getMultiLangName };
