import ReactDOM from 'react-dom';
import DepMethodBaseList from '../base';
import { pageConfig } from '../base/const';
import { createPage } from 'nc-lightapp-front';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
//管控级别为全局级
const nodeType = 'GLOBE_NODE';
const pageCode = '201200513A_list';

const DepMethodList = createPage({
	billinfo: {
		billtype: 'form',
		pagecode: pageCode,
		headcode: pageConfig.formArea
	}
})(DepMethodBaseList);

let moduleIds = { fa: [ '201200512A' ], ampub: [ 'common' ] };
initMultiLangByModule(moduleIds, () => {
	// 显示页面
	ReactDOM.render(<DepMethodList nodeType={nodeType} pageCode={pageCode} />, document.querySelector('#app'));
});
