//把关键组件的ID作为常量，并导出，便于修改与维护
//树根节点PK
export const treeRootPk = '1001A110000000001UPS';
//树ID
export const treeId = 'usingstatus_tree';
//表单ID
export const formId = 'card_head';

//按钮ID
export const save = 'Save';
export const cancel = 'Cancel';
export const doUnSeal = 'unSeal';
export const doSeal = 'seal';
export const edit = 'Edit';
export const add = 'Add';
export const del = 'Delete';
export const PrintButton = 'Print';
export const RefreshButton = 'Refresh';

export const url = {
    print : '/nccloud/fa/usingstatus/print.do',
    getMaxChildUrl:'/nccloud/fa/usingstatus/getMaxChild.do',//加载最大子节点编码
    queryUrl:'/nccloud/fa/usingstatus/query.do'//数据加载url
}

export const RootDisableButton = [
	'Print','PrintGroup','Output'
]