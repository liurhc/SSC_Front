/**
 * Created by dongzheng9.
 */
import { ajax, toast, print, output } from 'nc-lightapp-front';
import { saveValidate } from '../validator';
import {
	treeRootPk,
	treeId,
	formId,
	save,
	cancel,
	doUnSeal,
	doSeal,
	edit,
	add,
	del,
	PrintButton,
	RefreshButton,
	url,
	RootDisableButton
} from '../constant';
import { deleteValidate } from '../validator';
import ampub from 'ampub';
const { utils,components,commonConst } = ampub;
const { StatusUtils} = commonConst;
const { UISTATE } = StatusUtils;
const { LoginContext } = components;
const { loginContextKeys, getContext} = LoginContext
const { multiLangUtils,msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showConfirm, showMessage,MsgConst} = msgUtils;

let treeNodeDisable = false; //是否禁用树的变量
let showSealStatus = false; //是否显示停用的checkbox的被选中状态,false是没有被选中，也就是不显示停用

export default function(props, id) {
	//id为被点击的buttonID
	let formValues = props.form.getAllFormValue(formId); //获取表单所有数据的值

	switch (id) {
		case save:
			//编辑态点保存才有用
			if (props.form.getFormStatus(formId) == 'edit') {
				//校验
				let valid = saveValidate(formValues.rows[0].values, props.syncTree);
				if (!valid.valid) {
					toast({ content: valid.msg, color: 'danger' });
					break;
				}
				doSave(props);
			}
			break;
		case cancel:
			cancelClick(props, formValues);
			break;
		case doSeal:
			//停用按钮
			if (props.form.getFormStatus(formId) == 'browse') {
				//需要更新树
				doDisable(props);
			}
			break;
		case doUnSeal:
			//启用按钮
			if (props.form.getFormStatus(formId) == 'browse') {
				//需要更新树
				UnSeal(props);
			}
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refreshAction.call(this, props);
		default:
			break;
	}
	
}

//取消
function cancelClick(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancelReal });
}
//确认取消
function cancelReal(props) {
	let formValues = props.form.getAllFormValue(formId);

	props.form.cancel(formId);
	//解决新增取消之后的界面残留数据问题
	let pk = formValues.rows[0].values.pk_usingstatus.value;
	if (pk == '') {
		//新增取消显示父节点
		let parent_id = formValues.rows[0].values.parent_id.value;
		let parentValues = props.syncTree.getSyncTreeValue(treeId, parent_id);
		setFormValue(parent_id, parentValues, true, props);
	} else {
		//如果是修改 则恢复之前数据
		let currentValues = props.syncTree.getSyncTreeValue(treeId, pk);
		setFormValue(pk, currentValues, true, props);
	}
	let enablestate = formValues.rows[0].values.enablestate.value;
	if (enablestate == '2') {
		setButtonVisibleByCode(props, 'select2');
	} else {
		setButtonVisibleByCode(props, 'select3');
	}
	//设置启用状态为可编辑
	props.form.setFormItemsDisabled(formId, { enablestate: false });
	props.syncTree.setNodeDisable(treeId, false); //设置是否启用树
	setTreeNodeDisable(false); //启用显示停用按钮
}

/**
 * 返回当前节点的最大子节点
 * 由于平台的树不支持排序，需要修改这个方法
 * @param {object} syncTreeValue 当前节点(父节点)
 */
export function getMaxChild(syncTreeValue, props) {
	if (getShowSealStatus() == true) {
		//如果显示停用，就不需要去后台查了
		let parentCode = syncTreeValue.refcode;
		let syncTreeValueStr = JSON.stringify(syncTreeValue);
		for (let index = 1; index < 100; index++) {
			let maxChild = '00';
			if (index < 10) {
				maxChild = '0' + index;
			} else {
				maxChild = index;
			}
			maxChild = parentCode + maxChild;
			let queryData = `"status_code":{"value":"${maxChild}"}`;
			if (!syncTreeValueStr.includes(queryData)) {
				return maxChild;
			}
		}
	} else {
		let url = '/nccloud/fa/usingstatus/getMaxChild.do';
		let backData;
		ajax({
			loading: true,
			url: url,
			data: {
				pk_usingstatus: syncTreeValue.values.pk_usingstatus.value,
				//nodeType,如果是全局返回global
				nodeType: props.nodeType
			},
			async: false, //因为要返回返回值,所以要设置为同步
			success: function(res) {
				if (res.success == true) {
					backData = res.data;
				}
			}
		});
		return backData;
	}
}

/**
 * 判断表单中的status_code在树里面有没有
 * 如果有返回false，如果没有，返回true
 * @param {object} formValues 表单数据
 * @param {object} props 
 */
export function isAddSave(formValues) {
	if (
		formValues.rows[0].values.hasOwnProperty('pk_usingstatus') &&
		formValues.rows[0].values['pk_usingstatus'].value &&
		formValues.rows[0].values['pk_usingstatus'].value != ''
	) {
		//如果不存在
		return false;
	} else {
		return true;
	}
}

//保存
function doSave(props) {
	if (!props.form.isCheckNow(formId)) {
		return;
	}

	let formData = props.form.getAllFormValue(formId);
	// //拷贝一个对象，删掉循环删除display属性
	let backFormData = JSON.parse(JSON.stringify(formData));
	// 添加之前被删除掉得dr属性
	backFormData.rows[0].values.dr = {
		value: '0'
	};
	//保存的时候修改启用状态为数值
	if (backFormData.rows[0].values.enablestate.value == true || backFormData.rows[0].values.enablestate.value == '2') {
		backFormData.rows[0].values.enablestate.value = '2';
	} else {
		backFormData.rows[0].values.enablestate.value = '3';
	}
	let formValues = props.form.getAllFormValue(formId);
	//获取表单status_code，如果在树里面存在，是修改保存，如果树里面不存在，是新增保存
	//分别用不同的ajax来访问后台，并处理返回数据

	// 保存前执行验证公式
	props.validateToSave(
		{
			pageid: props.pagecode,
			userjson: props.nodeType,
			model: {
				areacode: formId,
				rows: backFormData.rows
			}
		},
		() => {
			// 调用Ajax保存数据
			if (isAddSave(backFormData, props)) {
				//如果在树里没有，则是新增保存
				ajax({
					loading: true,
					url: '/nccloud/fa/usingstatus/save.do',
					data: {
						pageid: props.pagecode,
						userjson: props.nodeType,
						model: {
							areacode: formId,
							rows: backFormData.rows
						}
					},
					success: function(res) {
						if (res.success == true) {
							//获取返回的数据并删除dr属性
							let backData = res.data[formId].rows[0];
							delete backData.values.dr;
							//新增保存操作返回值方法
							let newNode = {
								isleaf: true,
								disabled: false,
								pid: backData.values.parent_id.value, //父节点id
								refcode: backData.values.status_code.value,
								refname: backData.values.status_code.value + ' ' + getMultiLangName('status_name',backData), //界面上显示的名字
								refpk: backData.values.pk_usingstatus.value,
								values: backData.values
							};
							//新增节点成功调用方法
							props.syncTree.addNodeSuccess(treeId, newNode);
							props.form.setFormStatus(formId, 'browse');
							//刷新表单值
							setFormValue(backData.values.pk_usingstatus.value, backData, true, props);							
							let enablestate = backData.values.enablestate.value;
							if (enablestate == '2') {
								setButtonVisibleByCode(props, 'select2');
							} else {
								setButtonVisibleByCode(props, 'select3');
							}
							//设置启用状态为可编辑
							props.form.setFormItemsDisabled(formId, { enablestate: false });
							props.syncTree.setNodeDisable(treeId, false); //设置是否启用树
							setTreeNodeDisable(false); //启用显示停用按钮

							//展开父节点
							props.syncTree.openNodeByPk(treeId, newNode.pid);
							//选中新增节点
							props.syncTree.setNodeSelected(treeId, newNode.refpk);
							//设置按钮可用性
							props.button.setButtonDisabled(RootDisableButton, false);
							showMessage.call(this,props,{type: MsgConst.Type.SaveSuccess});
						}
					}
				});
			} else {
				//如果在树里，则是修改保存
				ajax({
					loading: true,
					url: '/nccloud/fa/usingstatus/update.do',
					data: {
						pageid: props.pagecode,
						userjson: props.nodeType,
						model: {
							areacode: formId,
							rows: backFormData.rows
						}
					},
					success: function(res) {
						if (res.success == true) {
							//获取返回的数据并删除dr属性
							let backData = res.data[formId].rows[0];
							delete backData.values.dr;
							//修改保存操作返回值方法
							//获取修改树节点的值
							let currentNode = props.syncTree.getSyncTreeValue(
								treeId,
								formValues.rows[0].values.pk_usingstatus.value
							);
							currentNode.refname = currentNode.refcode + ' ' + getMultiLangName('status_name',backData); //更新树节点显示名称
							currentNode.key = currentNode.refname; //key值不知道什么时候有的，也不知道用不用赋值，就先赋上了
							currentNode.values = backData.values; //更新树节点values数据
							props.syncTree.editNodeSuccess(treeId, currentNode);
							props.form.setFormStatus(formId, 'browse');
							//刷新表单值
							setFormValue(backData.values.pk_usingstatus.value, backData, true, props);
							let enablestate = backData.values.enablestate.value;
							if (enablestate == '2') {
								setButtonVisibleByCode(props, 'select2');
							} else {
								setButtonVisibleByCode(props, 'select3');
							}
							//设置启用状态为可编辑
							props.form.setFormItemsDisabled(formId, { enablestate: false });
							props.syncTree.setNodeDisable(treeId, false); //设置是否启用树
							setTreeNodeDisable(false); //启用显示停用按钮
							showMessage.call(this,props,{type: MsgConst.Type.SaveSuccess});
						}
					}
				});
			}
		}
	);
}

//删除
export function doDelete(props) {
	let item = this.currentItem;
	this.onSelectEve(item.refpk, item, true);
	//校验
	let valid = deleteValidate(item);
	if (!valid.valid) {
		toast({ content: valid.msg, color: 'danger' });
		return;
	}

	let formData = props.form.getAllFormValue(formId);
	let backFormData = JSON.parse(JSON.stringify(formData));
	backFormData.rows[0].values.dr = {
		value: '0'
	};
	//保存的时候修改启用状态为数值
	if (backFormData.rows[0].values.enablestate.value == true) {
		backFormData.rows[0].values.enablestate.value = '2';
	} else {
		backFormData.rows[0].values.enablestate.value = '3';
	}

	ajax({
		loading: true,
		url: '/nccloud/fa/usingstatus/delete.do',
		data: {
			pageid: props.pagecode,
			userjson: props.nodeType,
			model: {
				areacode: formId,
				rows: backFormData.rows
			}
		},
		success: function(res) {
			if (res.success == true) {
				//修改树节点
				props.syncTree.delNodeSuceess(treeId, backFormData.rows[0].values.pk_usingstatus.value);
				props.form.EmptyAllFormValue(formId); //清空表单值
				props.form.setFormItemsDisabled(formId, { enablestate: true });
				props.form.setFormStatus(formId, 'browse'); //设置表单状态
				setButtonVisibleByCode(props, 'select2'); //设置按钮显隐性		
				showMessage.call(this,props,{type: MsgConst.Type.DeleteSuccess});
			}
		}
	});
}

//启用
function UnSeal(props) {
	let formData = props.form.getAllFormValue(formId);
	let backFormData = JSON.parse(JSON.stringify(formData));
	backFormData.rows[0].values.dr = {
		value: '0'
	};
	//保存的时候修改启用状态为数值
	if (backFormData.rows[0].values.enablestate.value == true) {
		backFormData.rows[0].values.enablestate.value = '2';
	} else {
		backFormData.rows[0].values.enablestate.value = '3';
	}
	let formValues = props.form.getAllFormValue(formId);
	ajax({
		loading: true,
		url: '/nccloud/fa/usingstatus/unSeal.do',
		data: {
			pageid: props.pagecode,
			userjson: props.nodeType,
			model: {
				areacode: formId,
				rows: backFormData.rows
			}
		},
		success: function(res) {
			if (res.success == true) {
				res.data[formId].rows.map((row) => {
					let backData = row;
					delete backData.values.dr;
					let currentNode = props.syncTree.getSyncTreeValue(treeId, backData.values.pk_usingstatus.value);
					currentNode.values = backData.values;
					//修改树节点
					props.syncTree.editNodeSuccess(treeId, currentNode);
					//刷新表单值
					setFormValue(backData.values.pk_usingstatus.value, backData, true, props);
				});
				setButtonVisibleByCode(props, 'unSeal'); //设置按钮显隐性
				showMessage.call(this,props,{type: MsgConst.Type.EnableSuccess});
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			backFormData.rows[0].values.enablestate.value = false;
			setFormValue(backFormData.rows[0].values.pk_usingstatus.value, backFormData.rows[0], true, props); //修改表单值
		}
	});
}

//停用操作
export function doDisable(props) {
	let formData = props.form.getAllFormValue(formId);
	let backFormData = JSON.parse(JSON.stringify(formData));
	backFormData.rows[0].values.dr = {
		value: '0'
	};
	//保存的时候修改启用状态为数值
	if (backFormData.rows[0].values.enablestate.value == true) {
		backFormData.rows[0].values.enablestate.value = '2';
	} else {
		backFormData.rows[0].values.enablestate.value = '3';
	}
	let formValues = props.form.getAllFormValue(formId);
	ajax({
		loading: true,
		url: '/nccloud/fa/usingstatus/seal.do',
		data: {
			pageid: props.pagecode,
			userjson: props.nodeType,
			model: {
				areacode: formId,
				rows: backFormData.rows
			}
		},
		success: function(res) {
			if (res.success == true) {
				res.data[formId].rows.map((row) => {
					let backData = row;
					delete backData.values.dr;
					let currentNode = props.syncTree.getSyncTreeValue(treeId, backData.values.pk_usingstatus.value);
					currentNode.values = backData.values;
					//修改树节点
					props.syncTree.editNodeSuccess(treeId, currentNode);
					//只刷新当前编辑节点
					if(backFormData.rows[0].values.pk_usingstatus.value 
						== backData.values.pk_usingstatus.value){
						//刷新表单值
						setFormValue(backData.values.pk_usingstatus.value, backData, true, props);
					}
				});
				setButtonVisibleByCode(props, 'seal'); //设置按钮显隐性				
				showMessage.call(this,props,{type: MsgConst.Type.DisableSuccess});
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			backFormData.rows[0].values.enablestate.value = false;
			setFormValue(backFormData.rows[0].values.pk_usingstatus.value, backFormData.rows[0], true, props); //修改表单值
		}
	});
}

/**
 * 改变停启用提示信息
 * @param {*} props 
 */
export function changeEnableTips(props) {
	let validateRes = {};
	validateRes.valid = true; //校验是否通过,false是不通过，true是通过
	validateRes.msg = ''; //校验信息
	let formData = props.form.getAllFormValue(formId);
	//停用校验
	let parent_id = formData.rows[0].values.pk_usingstatus.value;
	if (parent_id) {
		let parentValues = props.syncTree.getSyncTreeValue(treeId, parent_id == '' ? '~' : parent_id);
		checkChildrenStatus(parentValues,validateRes);		
	}
	if (validateRes.valid) {
		props.form.setFormPopConfirmSwitchTips(formId, 'enablestate', [
			getMultiLangByID('msgUtils-000012') /* 国际化处理： 是否确认启用？*/,
			getMultiLangByID('msgUtils-000013') /* 国际化处理： 是否确认停用？*/
		]);
	} else {
		props.form.setFormPopConfirmSwitchTips(formId, 'enablestate', [
			getMultiLangByID('msgUtils-000012') /* 国际化处理： 是否确认启用？*/,
			validateRes.msg
		]);
	}
}

/**
 * 事先定义一些按钮显隐性的规则
 * @param {string} code 按钮显隐性代号
 */
export function setButtonVisibleByCode(props, code) {
	let visiables = new Array(); //显示的按钮
	let invisiables = new Array(); //隐藏的按钮
	switch (code) {
		case 'begin':
			//开始全部隐藏
			invisiables.push(save);
			invisiables.push(doSeal);
			invisiables.push(doUnSeal);
			invisiables.push(cancel);
			break;
		case 'select2':
			//因为2代表启用，所以是select2
			invisiables.push(save);
			visiables.push(doSeal);
			invisiables.push(doUnSeal);
			invisiables.push(cancel);

			visiables.push(PrintButton);
			visiables.push(RefreshButton);
			break;
		case 'select3':
			//因为3代表停用，所以是select3
			invisiables.push(save);
			invisiables.push(doSeal);
			visiables.push(doUnSeal);
			invisiables.push(cancel);

			visiables.push(PrintButton);
			visiables.push(RefreshButton);
			break;
		case add:
			//点击新增按钮的时候，显示取消、保存
			visiables.push(save);
			invisiables.push(doSeal);
			invisiables.push(doUnSeal);
			visiables.push(cancel);

			invisiables.push(PrintButton);
			invisiables.push(RefreshButton);
			break;
		case save:
		//点击保存按钮的时候 相当于选中//或者啥都不显示
		case del:
		//点击删除按钮的时候 //啥都不显示
		case cancel:
			//点击取消按钮的时候 相当于选中//或者啥都不显示
			invisiables.push(save);
			invisiables.push(doSeal);
			invisiables.push(doUnSeal);
			invisiables.push(cancel);

			visiables.push(PrintButton);
			visiables.push(RefreshButton);
			break;
		case edit:
			//点击编辑按钮的时候 显示保存、取消
			visiables.push(save);
			invisiables.push(doSeal);
			invisiables.push(doUnSeal);
			visiables.push(cancel);

			invisiables.push(PrintButton);
			invisiables.push(RefreshButton);
			break;
		case doSeal:
			//点击停用按钮的时候 显示启用
			visiables.push(doUnSeal);
			invisiables.push(doSeal);

			visiables.push(PrintButton);
			visiables.push(RefreshButton);
			break;
		case doUnSeal:
			//点击启用按钮的时候 显示停用
			invisiables.push(doUnSeal);
			visiables.push(doSeal);

			visiables.push(PrintButton);
			visiables.push(RefreshButton);
			break;
		default:
			break;
	}
	props.button.setButtonVisible(visiables, true);
	props.button.setButtonVisible(invisiables, false);
	invisiables.splice(0, invisiables.length);
	visiables.splice(0, visiables.length);
}

/**
 * 修改元素为树组件需要的格式
 * @param {object} element 单个元素
 */
export function changeGrid(element) {
	//根据使用状况编码的长度来判断是否是叶子节点
	if (element.values.status_code.value.length > 2) {
		//如果是叶子节点
		element.isleaf = true;
		element.pid = element.values.parent_id.value; //父节点id
	} else {
		element.isleaf = false;
		element.pid = treeRootPk; //父节点id
	}
	element.disabled = false; //添加disabled属性
	element.refcode = element.values.status_code.value; //不知道什么属性，暂时设置为空，应该是status_code吧
	element.refname = element.values.status_code.value + ' ' + getMultiLangName('status_name',element); //界面上显示的名字
	element.refpk = element.values.pk_usingstatus.value; //主键
	//删除dr属性
	delete element.values.dr;
}

/**
 * 设置表单数据
 */
export function setFormValue(data, item, isChange, props) {
	//设置表单要修改的数据
	if (data != treeRootPk) {
		let rows = new Array();
		let values = JSON.parse(JSON.stringify(item.values));
		if (!values.hasOwnProperty('parent_id')) {
			values.parent_id = {
				display: '',
				value: ''
			};
		}
		let rowValues = {
			values
		};
		rows.push(rowValues);
		let formData = {
			[formId]: {
				rows
			}
		};
		//给界面赋值的时候修改启用状态为数值
		let status = props.form.getFormStatus(formId);
		if (status == UISTATE.edit) {
			//编辑态恢复之前逻辑值
			modifyForEdit(formData[formId].rows[0]);
		} else {
			modifyForBrowse(formData[formId].rows[0]);
		}

		props.form.setAllFormValue(formData);
	}
}

//修正由于选择数据后编辑界面的逻辑值改变
export function modifyForEdit(item) {
	let enable = 'enablestate';
	// display有值的时候switch会是开启状态
	delete item.values[enable].display;
	if (item.values[enable].value == true || item.values[enable].value == '2') {
		item.values[enable].value = '2';
	} else {
		item.values[enable].value = '3';
	}
}
//控制界面启用状态
function modifyForBrowse(item) {
	let enable = 'enablestate';
	// display有值的时候switch会是开启状态
	delete item.values[enable].display;
	if (item.values[enable].value == '2' || item.values[enable].value == true) {
		item.values[enable].value = true;
	} else {
		item.values[enable].value = false;
	}
}

/**
 * treeNodeDisable的set方法
 * @param {Boolean} disable 是否禁用
 */
export function setTreeNodeDisable(disable) {
	treeNodeDisable = disable;
}

/**
 * treeNodeDisable的get方法
 */
export function getTreeNodeDisable() {
	return treeNodeDisable;
}

/**
 * showSealStatus的set方法
 * @param {Boolean} disable 是否禁用
 */
export function setShowSealStatus(disable) {
	showSealStatus = disable;
}

/**
 * showSealStatus的get方法
 */
export function getShowSealStatus() {
	return showSealStatus;
}

//校验子节点启用状态
export function checkChildrenStatus(syncTreeNode, validateRes) {
	// 遍历所有节点，如果节点有子树，递归地对子树进行校验
	if (syncTreeNode.children != null) {
		syncTreeNode.children.map((item) => {
			if (item.hasOwnProperty('children')) {
				checkChildrenStatus(item, validateRes);
			}
			if (item.values.enablestate.value != 3) {
				if (item.values.enablestate.value || item.values.enablestate.value == 2) {
					validateRes.valid = false;
					validateRes.msg = getMultiLangByID('201200508A-000001')/* 国际化处理： 该档案存在已启用的下级 ,是否将所有下级同时停用?*/;
					return;
				}
			}
		});
	}
}

export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');

	if (!printData) {
		showMessage.call(this,props,{type: MsgConst.Type.ChoosePrint});
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.print, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this,props,{type: MsgConst.Type.ChooseOutput});
		return;
	}
	output({
		url: url.print,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let treeData = props.syncTree.getSyncTreeValue(treeId);
	let pks = [];
	getTreeDatePk(treeData, pks);

	let printData = {
		filename: props.nodeTypeCN, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType: outputType // 输出类型
	};
	return printData;
}

/**
 * 重新加载页面
 * @param {*} props 
 */
export function refreshAction(props, id) {
	let callback = () => {
		showMessage.call(this,props,{type: MsgConst.Type.RefreshSuccess});
	};
	this.getData(callback);
}

//递归获取各个节点Pk
export function getTreeDatePk(treeData, pks) {
	treeData.map((node) => {
		if (node.refpk != treeRootPk) {
			pks.push(node.refpk);
		}
		if (node.children) {
			getTreeDatePk(node.children, pks);
		}
	});
}
/**
 * 获取多语字段名称 字段值 * 
 * @param {*} multiLangName 多语字段名称
 * @param {*} item 数据对象 (如果有值返回当前语种值)
 * @return item 为空时返回当前语种字段 , 有值取当前语种值
 */
export function getMultiLangName(multiLangName,item){
	let index = getContext(loginContextKeys.languageIndex);
	if(!index){
		index = '';//未加载到默认主语种
	}
	//如果items有值返回字段value
	if(item){
		//取当前语种值如果没有默认取主语种值
		let value = item.values[multiLangName + index].value;
		if(!value){
			value = item.values[multiLangName].value;
		}
		return value;
	}else{
		//返回当前语种多语字段
		return multiLangName + index;
	}

}