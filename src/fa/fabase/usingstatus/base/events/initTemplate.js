import {  RootDisableButton,formId} from '../constant';
import ampub from 'ampub';
const { utils,components } = ampub;
const { LoginContext } = components;
const { loginContext} = LoginContext;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export default function(props) {
	
	props.createUIDom(
		{
			pagecode: props.pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);					
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);

					//设置按钮的可点击行
					props.button.setButtonDisabled(RootDisableButton, true);
				}
				if (data.template) {
					let meta = data.template;
					meta[formId].status = 'browse';
					modifiy(meta[formId]);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
				
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	this.getData();//加载数据
}

function modifiy(data) {
	data['items'].map((item) => {
		if (item.attrcode == 'enablestate') {
            item.onPopconfirmCont = getMultiLangByID('msgUtils-000012') /* 国际化处理： 是否确认启用？*/;
            item.offPopconfirmCont = getMultiLangByID('msgUtils-000013') /* 国际化处理： 是否确认停用？*/;
        }
	});
}
