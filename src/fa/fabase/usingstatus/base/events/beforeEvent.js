import { changeEnableTips } from './headerButtonClick';

export default function beforeEvent(props, moduleId, key, value, index) {
	if (key == 'enablestate') {
		//更改停启用提示
		changeEnableTips(props);
	}
	return true;
}