import headerButtonClick from './headerButtonClick';
export default function afterEvent(props, moduleId, key, value, index) {
	if (key == 'enablestate') {
		let data = props.form.getAllFormValue(moduleId).rows[0].values;
		let value = data.enablestate.value;
		if (value) {
			headerButtonClick(props, 'unSeal');
		} else {
			headerButtonClick(props, 'seal');
		}
	}
}
