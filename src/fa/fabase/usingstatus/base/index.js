import React, { Component } from 'react';
import { ajax, base, toast, createPageIcon } from 'nc-lightapp-front';
import { treeRootPk, treeId, formId, save, cancel, doUnSeal, doSeal, edit, add, del ,RootDisableButton,url} from './constant';
import {
	initTemplate,
	afterEvent,
	buttonClick,
	headerButtonClick,
	doDelete,
	setFormValue,
	changeGrid,
	setButtonVisibleByCode,
	getTreeNodeDisable,
	setTreeNodeDisable,
	setShowSealStatus,
	modifyForEdit,
	beforeEvent
} from './events';
import { addValidate } from './validator';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils,closeBrowserUtils,msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showConfirm ,MsgConst} = msgUtils;
const { NCCheckbox } = base;

class TreeForm extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		let { form, button, table, search, syncTree } = this.props;
		let { setSyncTreeData } = syncTree;
		this.setSyncTreeData = setSyncTreeData; //设置根节点数据方法
		this.state = {
			editTree: true,
			isShowOff: false
		};
		closeBrowserUtils.call(this,props,{form:[formId]});
		initTemplate.call(this, props);
	}

	//初始化树数据
	componentDidMount() {
		// this.getData();
		setButtonVisibleByCode(this.props, 'begin');
	}

	//设置树上图标的隐藏
	onMouseEnterEve(key) {
		if (key === treeRootPk) {
			let obj = {
				delIcon: false, //false:隐藏； true:显示; 默认都为true显示
				editIcon: false,
				addIcon: false
			};
			this.props.syncTree.hideIcon(treeId, key, obj);
		}
	}

	getData = (callback) => {
		let treeData = null;
		let data = {
			//是否显示停用
			isShowSeal: this.state.isShowOff ? 'Y' : 'N',
			nodeType: this.props.nodeType,
			areacode: formId,
			//pagecode翻译得时候需要
			pagecode: this.props.pagecode
		};
		ajax({
			url: url.queryUrl,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success && data) {
					let rows = res.data[formId].rows;
					for (let i in rows) {
						if (rows.hasOwnProperty(i)) {
							let element = rows[i];
							changeGrid(element);
						}
					}
					let root = new Object();
					root.disabled = false;
					root.isleaf = false;
					root.refcode = '0';
					root.refname = getMultiLangByID('201200508A-000009')/* 国际化处理： 使用状况*/;
					root.refpk = treeRootPk;
					rows.push(root);
					treeData = this.props.syncTree.createTreeData(rows);
					this.sortTree(treeData);
					this.props.syncTree.setSyncTreeData(treeId, treeData);
					this.props.syncTree.openNodeByPk(treeId, treeRootPk); //展开根节点

					//默认选中第一个节点
					let first = treeData[0].children[0].refpk;
					this.props.syncTree.setNodeSelected(treeId,first);
					let currentNode = this.props.syncTree.getSelectNode(treeId);
					this.onSelectEve(currentNode.refpk,currentNode);
				} else {
					let rows = new Array();

					let root = new Object();
					root.disabled = false;
					root.isleaf = false;
					root.refcode = '0';
					root.refname = getMultiLangByID('201200508A-000009')/* 国际化处理： 使用状况*/;
					root.refpk = treeRootPk;
					rows.push(root);				
					treeData = this.props.syncTree.createTreeData(rows);
					this.props.syncTree.setSyncTreeData(treeId, treeData);

					//默认选中根节点
					this.props.syncTree.setNodeSelected(treeId,treeRootPk);
					let currentNode = this.props.syncTree.getSelectNode(treeId);
					this.onSelectEve(currentNode.refpk,currentNode);
				}
				typeof callback === 'function' && callback.call(this);
			}
		});
	};

	//根据编码对树上的数据排序
	sortTree = (
		tree,
		sortfunc = (a, b) => {
			return parseInt(a.refcode) - parseInt(b.refcode);
		}
	) => {
		// 对上层树排序
		tree.sort(sortfunc);
		// 遍历所有节点，如果节点有子树，递归地对子树排序
		tree.map((item) => {
			if (item.hasOwnProperty('children') && item.children != null) {
				this.sortTree(item.children);
			}
		});
	};

	//选择前事件
	onSelectEve = (data, item, isChange) => {
		//设置表单状态
		// this.props.form.setFormStatus(formId, 'browse');
		//判断是否是根节点，如果是根节点，就清空表单数据，如果不是，就赋值表单数据
		if (data == treeRootPk) {
			this.props.form.EmptyAllFormValue(formId);
			this.props.form.setFormItemsDisabled(formId, { enablestate: true });

			//设置按钮的可点击行
			this.props.button.setButtonDisabled(RootDisableButton, true);
		} else {
			//根据节点停启用状态设置按钮显隐性
			
			//设置表单数据
			setFormValue(data, item, isChange, this.props);
			this.props.form.setFormItemsDisabled(formId, { enablestate: false });

			//设置按钮的可点击行
			this.props.button.setButtonDisabled(RootDisableButton, false);
		}
	};

	//编辑前事件
	clickEditIconEve = (item) => {
		if (item.refpk != treeRootPk) {
			this.props.form.setFormStatus(formId, 'edit'); //修改表单状态
			//editRender方法会清空表单数据，需要重新赋值，下同
			this.onSelectEve(item.refpk, item, true);
			this.props.syncTree.setNodeDisable(treeId, true); //禁用树
			setTreeNodeDisable(true); //设置控制显示停用按钮的可编辑性
			this.props.form.setFormItemsDisabled(formId, { enablestate: true });
			setButtonVisibleByCode(this.props, edit); //设置按钮可见性
		} else {
			toast({ content: getMultiLangByID('201200508A-000010')/* 国际化处理： 当前节点不可被编辑*/, color: 'danger' });
		}
	};

	//新增前事件
	clickAddIconEve = (item) => {
		//设置选中节点
		this.onSelectEve(item.refpk, item, true);
		//设置选中节点高亮
		this.props.syncTree.setNodeSelected(treeId,item.refpk);
		if (item.refpk != treeRootPk) {
			//校验
			let valid = addValidate(item);
			if (!valid.valid) {
				toast({ content: valid.msg, color: 'danger' });
				return;
			}
			ajax({
				loading: true,
				url: url.getMaxChildUrl,
				data: {
					pk_usingstatus: item.values.pk_usingstatus.value,
					//nodeType,如果是全局返回global
					nodeType: this.props.nodeType,
				},
				async: false, //因为要返回返回值,所以要设置为同步
				success: (res) => {
					if (res.success == true) {
						let backData = res.data;
						//校验
						let valid = addValidate(item,backData);
						if (!valid.valid) {
							toast({ content: valid.msg, color: 'danger' });
							return;
						}
						this.props.form.setFormStatus(formId, 'edit');
						//设置新增时启用状态为不可编辑
						this.props.form.setFormItemsDisabled(formId, { enablestate: true });
						this.props.syncTree.setNodeDisable(treeId, true); //设置树不可编辑
						setButtonVisibleByCode(this.props, add); //设置按钮显隐性
						let formData = this.setDefaultValue(item, valid.other.maxChild); //设置默认值
						this.props.form.setAllFormValue(formData); //将默认值赋值到表单中
						this.props.syncTree.setNodeDisable(treeId, true); //禁用树
						setTreeNodeDisable(true); //设置控制显示停用按钮的可编辑性
					}
				}
			});

			
		} else {
			toast({ content: getMultiLangByID('201200508A-000011')/* 国际化处理： 不可新增根节点*/, color: 'danger' });
		}
	};

	//新增节点的时候设置表单默认值
	setDefaultValue = (item, maxChild) => {
		let rows = new Array();
		//先把父节点的数据完全复制到子节点
		let values = new Object();
		//修正由于选择数据后编辑界面的逻辑值改变
		modifyForEdit(item);
		values = JSON.parse(JSON.stringify(item.values));
		//修改特定的值
		//设置编码次级
		values.statuslevel.value = parseInt(values.statuslevel.value) + 1 + '';
		values.statuslevel.display = values.statuslevel.value;
		//设置是否预置(默认是否).
		values.preset_flag.value = '0';
		values.preset_flag.display = getMultiLangByID('201200508A-000012')/* 国际化处理： 否*/;
		//values.enablestate.display = '已启用';
		//如果父节点没有父节点，则没有parent_id属性需要手动添加
		values.parent_id = {
			value: values.pk_usingstatus.value,
			display: values.pk_usingstatus.display
		};
		//由于后台校验需要获取parent_id查询条件是status_code=和pk_usingstatus!=,所以需要清空主键信息
		values.pk_usingstatus = {
			value: '',
			display: ''
		};
		//设置子节点编码，返回父节点的最大子节点编码加1，如果无子节点，返回该节点编码+‘01’
		let syncTreeValue = new Object();
		syncTreeValue = this.props.syncTree.getSyncTreeValue(treeId, item.values.pk_usingstatus.value);
		values.status_code.value = maxChild;
		values.status_code.display = values.status_code.value;
		let rowValues = {
			values
		};
		rows.push(rowValues);
		let formData = {
			[formId]: {
				rows
			}
		};
		return formData;
	};

	//删除前事件
	clickDelIconEve = (item) => {
		//设置选中节点
		this.onSelectEve(item.refpk, item, true);
		//设置选中节点高亮
		this.props.syncTree.setNodeSelected(treeId,item.refpk);
		if (item.refpk != treeRootPk) {
			this.currentItem = item;
			showConfirm.call(this, this.props, { type: MsgConst.Type.Delete, beSureBtnClick: doDelete });						
		} else {
			toast({ content: getMultiLangByID('201200508A-000013')/* 国际化处理： 当前节点不可被删除*/, color: 'danger' });
		}
	};	

	//修改是否显示停用
	showOffChange = () => {
		this.setState(
			{
				isShowOff: !this.state.isShowOff
			},
			this.getData.bind(this)
		);
		setShowSealStatus(this.state.isShowOff); //设置用于跨页面传递值得变量
	};

	getButtonNames = (codeId) => {
		switch (codeId) {
			case save:
			//case doSeal:
			//case doUnSeal:
			case cancel:
				return 'main-button';
			default:
				return 'secondary - button';
		}
	};

	render() {
		const { button, form, syncTree, ncmodal, DragWidthCom } = this.props;
		const { createForm } = form;
		const { createButton } = button;
		let { createSyncTree } = syncTree;
		let { createModal } = ncmodal;
		let buttons = this.props.button.getButtons();
		let status = this.props.form.getFormStatus(formId);
		return (
			<div id="fa-fabase-usingstatus" class="nc-bill-tree-card">
				{createModal('modal')}
				<div className="header">
					{createPageIcon()}
					<div className="title">{this.props.nodeTypeCN}</div>
					<span className="showOff">
						<NCCheckbox
							className="showHideBtn"
							checked={this.state.isShowOff}
							onChange={this.showOffChange}
							disabled={getTreeNodeDisable() == true}
							size="lg"
						>
							{getMultiLangByID('amcommon-000004')/* 国际化处理： 显示停用*/}
						</NCCheckbox>
					</span>
					<div className="btn-group">
						{/* {buttons.map((v) => {
							return createButton(v.btncode, {
								name: v.btnname,
								visiable: true,
								onButtonClick: headerButtonClick.bind(this),
								buttonColor: this.getButtonNames(v.btncode)
							});
						})} */}
						{this.props.button.createButtonApp({
							area: 'card_head',
							buttonLimit: 3,
							onButtonClick: headerButtonClick.bind(this),
							popContainer: document.querySelector('.btn-group')
						})}
					</div>
				</div>
				<div className="tree-card">
					<DragWidthCom
						leftDom={
							<div className="tree-area">
								{createSyncTree({
									treeId: treeId, // 组件id
									needSearch: true, //是否需要查询框，默认为true,显示。false: 不显示
									needEdit: this.state.editTree, //是否需要编辑节点功能，默认为true,可编辑；false：不可编辑
									onSelectEve: this.onSelectEve.bind(this), //选择节点回调方法
									onMouseEnterEve: this.onMouseEnterEve.bind(this), //鼠标移动事件
									clickEditIconEve: this.clickEditIconEve.bind(this), //编辑点击 回调
									clickAddIconEve: this.clickAddIconEve.bind(this), //新增点击 回调
									clickDelIconEve: this.clickDelIconEve.bind(this), // 删除点击 回调
									showLine: true,
									showModal: false,
									searchType: 'filtration',
									disabledSearch:status=='edit'? true : false
								})}
							</div>
						}
						rightDom={
							<div className="card-area">
								{createForm(formId, {
									onAfterEvent: afterEvent,
									onBeforeEvent: beforeEvent
								})}
							</div>
						}
						defLeftWid="21%"
					/>
				</div>
			</div>
		);
	}
}

export default TreeForm;
