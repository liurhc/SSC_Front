import addValidate from './addValidator';
import deleteValidate from './deleteValidator';
import saveValidate from './saveValidator';

export { addValidate, deleteValidate, saveValidate };
