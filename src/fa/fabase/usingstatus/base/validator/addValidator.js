import isUsedValidate from './isUsedValidator';
import ampub from 'ampub';
const { utils} = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;


/**
 * 新增校验
 * @param {object} item 节点数据
 * @returns {object} validateRes 内含两个属性，一个是是否验证通过valid，另一个是错误信息msg
 * @param {oject} props 
 */
export default function addValidate(item,backData) {
	let values = item.values;
	let validateRes = new Object();
	validateRes.valid = false; //校验是否通过,false是不通过，true是通过
	validateRes.msg = getMultiLangByID('201200508A-000015')/* 国际化处理： 无法新增子节点！*/; //校验信息
	validateRes.other = {};

	//编码次级大于五校验
	if (values.statuslevel.value >= 5) {
		validateRes.valid = false;
		validateRes.msg += getMultiLangByID('201200508A-000016')/* 国际化处理： 编码级次为5，不能再增加子节点！*/;
		//校验不通过直接返回，不进行后续校验，节省资源
		return validateRes;
	}

	//当前节点是否被引用校验，校验是否通过,false是不通过，true是通过
	if(backData){
		let isUsedRes = new Object();
		isUsedRes = isUsedValidate(values);
		if (!isUsedRes.valid) {
			validateRes.valid = false;
			validateRes.msg += isUsedRes.msg;
			return validateRes;
		}
	}

	//校验编码每级是否是两位
	if(backData){
		let maxChild = backData + '';
		validateRes.other.maxChild = maxChild;
		if (maxChild.length != (parseInt(values.statuslevel.value) + 1) * 2) {
			validateRes.valid = false;
			validateRes.msg += getMultiLangByID('201200508A-000017')/* 国际化处理： 新增子节点编码长度超过两位！*/;
			return validateRes;
		} else {
			validateRes.valid = true;
		}
	}
	
	validateRes.valid = true;
	return validateRes;
}
