import { ajax } from 'nc-lightapp-front';
import ampub from 'ampub';
const { utils} = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

/**
 * 当前节点是否被引用校验
 * @param {object} values 需要校验的表单的全部数据
 * @returns {object} validateRes 内含两个属性，一个是是否验证通过valid，另一个是错误信息msg
 */
export default function isUsedValidate(values) {
	let validateRes = new Object();
	validateRes.valid = false; //校验是否通过,false是不通过，true是通过
	validateRes.msg = ''; //校验信息

	//false是没有被引用，true是被引用了
	let isUsed = false;
	ajax({
		loading: true,
		url: '/nccloud/fa/usingstatus/queryIsUsed.do',
		data: {
			pk_usingstatus: values.pk_usingstatus.value
		},
		async: false,
		success: function(res) {
			if (res.success == true) {
				if (res.data == true) {
					validateRes.msg += getMultiLangByID('201200508A-000020')/* 国际化处理： 使用状况已经被引用！*/;
					isUsed = true;
				}
			} else {
				validateRes.msg += getMultiLangByID('201200508A-000021')/* 国际化处理： 引用查询失败！*/;
				isUsed = true;
			}
		}
	});
	if (isUsed) {
		validateRes.valid = false;
	} else {
		validateRes.valid = true;
	}
	return validateRes;
}
