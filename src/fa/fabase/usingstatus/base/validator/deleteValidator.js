import ampub from 'ampub';
const { utils} = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

/**
 * 删除校验
 * @param {object} item 需要校验的表单的全部数据
 * @returns {object} validateRes 内含两个属性，一个是是否验证通过valid，另一个是错误信息msg
 */
export default function deleteValidate(item) {
    let values = item.values;
    let validateRes = new Object();
    validateRes.valid = false;//校验是否通过,false是不通过，true是通过
    validateRes.msg = getMultiLangByID('201200508A-000018')/* 国际化处理： 无法删除！*/;//校验信息

    //删除的是否是根节点校验
    if (values.statuslevel.value == 1 && values.preset_flag.value == 1) {
        validateRes.valid = false;
        validateRes.msg += getMultiLangByID('201200508A-000019')/* 国际化处理： 不可删除预置的一级档案!*/;
        return validateRes;
    }

    //如果是父节点，要依次判断子节点是否被引用，如果被引用了，提示哪个子节点被引用
    //这个校验可以放弃，因为要多次访问后台
    
    validateRes.valid = true;
    return validateRes;
}