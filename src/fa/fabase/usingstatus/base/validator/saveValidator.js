import ampub from 'ampub';
const { utils} = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

/**
 * 保存校验
 * @param {object} values 需要校验的表单的全部数据
 * @param {object} syncTree 树
 * @returns {object} validateRes 内含两个属性，一个是是否验证通过valid，另一个是错误信息msg
 */
export default function saveValidate(values, syncTree) {
	let validateRes = new Object();
	validateRes.valid = true; //校验是否通过,false是不通过，true是通过
	validateRes.msg = getMultiLangByID('201200508A-000022')/* 国际化处理： 保存失败: */; //校验信息

	//编码次级大于五校验
	if (values.statuslevel.value > 5) {
		validateRes.msg += getMultiLangByID('201200508A-000023')/* 国际化处理： 编码级次大于5，不能再增加子节点！*/;
		validateRes.valid = false;
		return validateRes;
	}
	//使用状况名称长度校验
	if (values.status_name.value.length > 40) {
		validateRes.msg += getMultiLangByID('201200508A-000024')/* 国际化处理： 名称长度不能大于40！*/;
		validateRes.valid = false;
		return validateRes;
	}

	return validateRes;
}