import React from 'react';
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import TreeForm from '../base';
import { formId } from '../base/constant';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule, getMultiLangByID } = multiLangUtils;

const nodecode = '2012004030';
const nodeType = 'group';
const nodeTypeCN = '201200508A-000027'/* 国际化处理： 使用状况-集团*/;
const pagecode = '201200508A_tree';
const appcode = '201200508A';

let TreeFormDom = createPage({
	billinfo: {
		billtype: 'form',
		pagecode: pagecode,
		headcode: formId
	}
})(TreeForm);

let moduleIds = { fa: [ '201200508A' ], ampub: [ 'common' ] };
initMultiLangByModule(moduleIds, () => {
	ReactDOM.render(
		<TreeFormDom
			appcode={appcode}
			pagecode={pagecode}
			nodecode={nodecode}
			nodeType={nodeType}
			nodeTypeCN={getMultiLangByID(nodeTypeCN)/* 国际化处理： 使用状况-集团*/}
		/>,
		document.querySelector('#app')
	);
});
