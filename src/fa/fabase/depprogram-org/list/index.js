import React, { Component } from 'react';
import ListBase, { baseConfig } from '../../depprogram-base/list-base';
import { createPage } from 'nc-lightapp-front';

// 页面配置
const listpageConfig = {
	nodecode: '2012004095',
	nodeType: 'org',
	nodeTypeCN: '201200540A-000000' /*国际化处理：模拟折旧设置-组织*/,
	pagecode: '201200541A_list',
	pk_org: 'null',
	appcode: '201200541A',
	pagecode_card: '201200541A_card',
	printFuncode: '2012004095',
	dataSource: 'fa.fabase.depprogram-org.main'
};

const ListOrg = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: listpageConfig.pagecode,
		bodycode: 'head'
	}
})(ListBase);

export default class List extends Component {
	render() {
		return <ListOrg listpageConfig={listpageConfig} />;
	}
}
