import React, { Component } from 'react';
import CardBase from '../../depprogram-base/card-base';
import { createPage } from 'nc-lightapp-front';
export const cardpageConfig = {
	nodecode: '2012004095',
	nodeType: 'org',
	nodeTypeCN: '201200540A-000000' /*国际化处理：模拟折旧设置-组织*/,
	pagecode: '201200541A_card',
	pk_org: 'null',
	appcode: '201200541A',
	pagecode_list: '201200540A_list',
	dataSource: 'fa.fabase.depprogram-org.main'
};

const CardGroup = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: cardpageConfig.pagecode,
		headcode: 'head',
		bodycode: 'body'
	}
})(CardBase);

export default class Card extends Component {
	render() {
		return <CardGroup cardpageConfig={cardpageConfig} />;
	}
}
