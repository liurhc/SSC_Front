import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, createPageIcon } from 'nc-lightapp-front';
import {
	headButtonClick,
	initTemplate,
	headAfterEvent,
	bodyAfterEvent,
	rateAfterEvent,
	headRowClick,
	headRowSelected,
	bodyButtonClick,
	rateButtonClick
} from './events';
import { pageConfig } from './const.js';
import { toggleShow, getData } from './events/headButtonClicks';
import './index.less';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, msgUtils, closeBrowserUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;
const { MsgConst, showMessage } = msgUtils;

const { NCCheckbox } = base;
const {
	headtableId,
	bodytableId,
	ratetableId,
	pagecode,
	areaType,
	rateAreacode,
	rateAddModalId,
	headmodalId,
	bodymodalId,
	ratemodalId,
	url,
	headTitle,
	bodyTitle,
	rateTitle
} = pageConfig;

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isshowseal: false,
			isshowbody: 'none'
		};
		closeBrowserUtils.call(this, props, { editTable: [ headtableId, bodytableId, ratetableId ] });
		initTemplate.call(this, props);
	}

	onChange = () => {
		let data = {
			pagecode,
			isshowseal: !this.state.isshowseal
		};
		this.setState({ isshowseal: !this.state.isshowseal });
		getData.call(this, data);
	};

	// 孙表查询弹出框内容
	modalContent(props) {
		props = this.props;
		return (
			<div className="">
				<div className="header">
					{/* 按钮区 btn-group */}
					<div className="btn-group">
						<div className="header-button-area">
							{props.button.createButtonApp({
								//按钮区域（在数据库中注册的按钮区域）
								area: 'list_rate',
								//按钮数量限制，超出指定数量的按钮将放入更多显示
								buttonLimit: 3,
								onButtonClick: rateButtonClick.bind(this),
								//指定下拉弹窗渲染的位置，不设置的话，下拉弹窗随滚动条滚动
								popContainer: document.querySelector('.header-button-area')
							})}
							{props.modal.createModal(ratemodalId, { color: 'warning' })}
						</div>
					</div>
				</div>
				<div className="table-area">
					{props.editTable.createEditTable(ratetableId, {
						onAfterEvent: rateAfterEvent
					})}
				</div>
			</div>
		);
	}

	//孙表弹框确认按钮事件---保存、新增
	addModalBeSureBtnClick() {
		//过滤空行,如果没有可忽略的字段，则放置空数组
		this.props.editTable.filterEmptyRows(ratetableId, []);
		//获取所有行数据，falg：是否关闭必输项校验，默认为false，改为true不校验必输项
		let ratetableData = this.props.editTable.getAllRows(ratetableId, false);
		//先判断是否为空
		if (ratetableData.length == 0) {
			toast({ content: getMultiLangByID('201200536A-000016') /* 国际化处理： 数据不能为空，请重新添加！*/, color: 'danger' });
			return;
		}

		let visibleRows = this.props.editTable.getVisibleRows(ratetableId);
		let pass = this.props.editTable.checkRequired(ratetableId, visibleRows);
		// 孙表必输项校验没过
		if (!pass) {
			return;
		}
		/*
			将数组过滤掉状态为3的，即删行的数据 status: 0 不变  1  修改  2  新增  3  删除
			1.后台不传删行的数据，通过not in的方式进行删除
			2.在前台判断分配比率相加之和是否等于100，需要排除删行的数据
		*/
		let newRatetableData = ratetableData.filter((row) => {
			return row.status != 3;
		});
		//判断分配比率是不是100，不是则不符合要求
		if (newRatetableData.length > 0) {
			let num = 0;
			for (let i = 0; i < newRatetableData.length; i++) {
				num = num + parseFloat(newRatetableData[i].values.rate.value);

				//判断是否是新增的数据，如果是，则加上pk_assetgroup_b
				if (
					newRatetableData[i].values.pk_assetgroup_b.value == null ||
					newRatetableData[i].values.pk_assetgroup_b.value == ''
				) {
					newRatetableData[i].values.pk_assetgroup_b.value = pageConfig.pk_assetgroup_b;
				}
			}
			if (num != 100) {
				toast({ content: getMultiLangByID('201200536A-000017') /* 国际化处理： 表体分摊比例之和必须等于100！*/, color: 'danger' });
				return;
			}
		} else {
			//说明全部删掉没有新增，直接提示
			toast({ content: getMultiLangByID('201200536A-000017') /* 国际化处理： 表体分摊比例之和必须等于100！*/, color: 'danger' });
			return;
		}
		const data = {
			model: {
				areaType,
				areacode: rateAreacode,
				rows: []
			},
			pageid: pagecode
		};
		data.model.rows = newRatetableData;
		ajax({
			url: url.insertRateUrl,
			data,
			success: (res) => {
				if (res.data) {
					this.props.editTable.setTableData(ratetableId, res.data[ratetableId]);
					showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess });
					toggleShow(this.props, 'browse');
					this.props.modal.close(rateAddModalId);
				}
			}
		});
	}

	render() {
		const { editTable, button, ncmodal, modal } = this.props;
		const { createEditTable } = editTable;
		const { createButtonApp } = button;
		const { createModal } = ncmodal;

		let status = editTable.getStatus(headtableId);
		return (
			<div>
				{/* 主表 */}
				<div classname="header">
					<div className="nc-single-table">
						{/* 头部 header */}
						<div className="nc-singleTable-header-area">
							{/* 标题 title */}
							<div className="header-title-search-area">
								{createPageIcon()}
								<h2 className="title-search-detail">{getMultiLangByID(headTitle) /* 国际化处理： 资产组*/}</h2>

								{/* 显示停用 showOff */}
								<div className="title-search-detail">
									<span className="showOff">
										<NCCheckbox
											colors="info"
											onChange={this.onChange}
											checked={this.state.isshowseal}
											disabled={status == 'edit'}
										>
											{getMultiLangByID('amcommon-000004') /* 国际化处理： 显示停用*/}
										</NCCheckbox>
									</span>
								</div>
							</div>
							{/* 按钮区 */}
							<div className="header-button-area">
								{createButtonApp({
									//按钮区域（在数据库中注册的按钮区域）
									area: 'list_head',
									//按钮数量限制，超出指定数量的按钮将放入更多显示
									buttonLimit: 3,
									onButtonClick: headButtonClick.bind(this),
									//指定下拉弹窗渲染的位置，不设置的话，下拉弹窗随滚动条滚动
									popContainer: document.querySelector('.header-button-area')
								})}
								{createModal(headmodalId, {
									color: 'warning'
								})}
								{createModal('Cancel', {
									color: 'warning'
								})}
							</div>
						</div>

						{/* 列表区 table-area */}
						<div className="table-area">
							{createEditTable(headtableId, {
								showCheck: true,
								showIndex: true,
								onAfterEvent: headAfterEvent,
								onRowClick: headRowClick.bind(this),
								selectedChange: headRowSelected.bind(this)
							})}
						</div>
					</div>
				</div>
				<div style={{ height: '10px' }} />

				{/* 子表 */}
				<div id="bodytable" style={{ display: `${this.state.isshowbody}` }}>
					<div className="header">
						{/* 标题 title */}
						<div className="title"> {getMultiLangByID(bodyTitle) /* 国际化处理： 财务组织*/} </div>
						{/* 按钮区 btn-group */}
						<div className="btn-group">
							<div className="header-button-area">
								{createButtonApp({
									//按钮区域（在数据库中注册的按钮区域）
									area: 'list_body',
									//按钮数量限制，超出指定数量的按钮将放入更多显示
									buttonLimit: 3,
									onButtonClick: bodyButtonClick.bind(this),
									//指定下拉弹窗渲染的位置，不设置的话，下拉弹窗随滚动条滚动
									popContainer: document.querySelector('.header-button-area')
								})}
								{/*createModal(`${bodytableId}-del-confirm`, { color: 'warning' })*/}
								{createModal(bodymodalId, {
									color: 'warning'
								})}
							</div>
						</div>
					</div>

					<div className="table-area">
						{createEditTable(bodytableId, {
							onAfterEvent: bodyAfterEvent
						})}
					</div>
				</div>
				<div>
					{modal.createModal(rateAddModalId, {
						title: getMultiLangByID(rateTitle) /* 国际化处理： 总部资产分摊模板*/,
						content: this.modalContent.call(this),
						beSureBtnClick: this.addModalBeSureBtnClick.bind(this),
						userControl: true,
						cancelBtnClick: () => {
							toggleShow(this.props, 'browse');
							this.props.modal.close(rateAddModalId);
						},
						closeModalEve: () => {
							toggleShow(this.props, 'browse');
							this.props.modal.close(rateAddModalId);
						}
					})}
				</div>
			</div>
		);
	}
}

SingleTable = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode,
		headcode: headtableId,
		bodycode: headtableId
	}
})(SingleTable);

initMultiLangByModule({ fa: [ '201200536A' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<SingleTable />, document.querySelector('#app'));
});
