import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { headtableId, bodytableId } = pageConfig;

/**
 * 子表编辑后事件
 */

export default function(props, id, key, value, changedrows, index, data, type, eventType) {
	//先判断是不是修改的财务组织
	if (key == 'pk_org') {
		//过滤空行
		//props.editTable.filterEmptyRows(headtableId,['pkorg_belong','memo']);
		//先判断 -- 表体财务组织不能是总部资产所属财务组织
		let headAllRows = props.editTable.getAllRows(headtableId, false);
		for (let i = 0; i < headAllRows.length; i++) {
			//删除行不再交验
			if (headAllRows[i].status == 3) {
				continue;
			}
			if (
				data.values.pk_org.value != null &&
				data.values.pk_org.value == headAllRows[i].values.pkorg_belong.value
			) {
				toast({
					content: getMultiLangByID('201200536A-000004') /* 国际化处理： 表体财务组织不能是总部资产所属财务组织！*/,
					color: 'warning'
				});
				props.editTable.setValByKeyAndIndex(bodytableId, index, 'pk_org', { value: null, display: null });
				props.editTable.setValByKeyAndIndex(bodytableId, index, 'pk_org.name', { value: null, display: null });
				return;
			}
		}

		//再判断财务组织是否重复设置
		let bodyAllRows = props.editTable.getAllRows(bodytableId, false);
		let flag = true;
		for (let i = 0; i < bodyAllRows.length; i++) {
			//删除行不再交验
			if (bodyAllRows[i].status == 3) {
				continue;
			}
			//与自己不在校验
			if (index == i) {
				continue;
			}
			if (data.values.pk_org.value && data.values.pk_org.value == bodyAllRows[i].values.pk_org.value) {
				flag = false;
			}
		}
		if (flag) {
			let refpk = data.values.pk_org.value;
			if (refpk != null && refpk != '') {
				let data1 = {
					refpk
				};

				ajax({
					url: pageConfig.url.translateOrgUrl,
					data: data1,
					success: function(res) {
						if (res.data) {
							let code = res.data.code;
							let name = res.data.name;
							let pk_org = res.data.pk_org;
							props.editTable.setValByKeyAndIndex(bodytableId, index, 'pk_org', {
								value: pk_org,
								display: code
							});
							props.editTable.setValByKeyAndIndex(bodytableId, index, 'pk_org.name', {
								value: name,
								display: name
							});
						}
					}
				});
			}
		} else {
			toast({ content: getMultiLangByID('201200536A-000003') /* 国际化处理： 财务组织不允许重复设置,请重新选择！*/, color: 'danger' });
			props.editTable.setValByKeyAndIndex(bodytableId, index, 'pk_org', { value: null, display: null });
			props.editTable.setValByKeyAndIndex(bodytableId, index, 'pk_org.name', { value: null, display: null });
		}
	}
}
