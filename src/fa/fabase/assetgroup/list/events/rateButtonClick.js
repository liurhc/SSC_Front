import { pageConfig } from './../const.js';

/**
 * 孙表绑定按钮点击事件
 */
const { ratetableId } = pageConfig;

export default function(props) {
	props.editTable.addRow(ratetableId);
}
