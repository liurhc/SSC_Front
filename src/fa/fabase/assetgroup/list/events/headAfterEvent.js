import { headButtonClicks, toggleShow, addClick } from './headButtonClicks';
import { pageConfig } from './../const.js';
import { ajax, toast } from 'nc-lightapp-front';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/**
 * 主表编辑后事件
 */
const { headtableId, bodytableId, pagecode, editBodyBtns, url } = pageConfig;

export default function(props, id, key, value, changedrows, index, data, type, eventType) {
	// 停用、启用
	if (key === 'enablestate') {
		if (value) {
			//value表示编辑后的值（新值）
			headButtonClicks.enable(props, index);
		} else {
			headButtonClicks.disable(props, index);
		}
	}
	if (key == 'pkorg_belong') {
		//说明已经存在总部资产，需要校验
		if (data.values.group_flag.value == false) {
			toast({ content: getMultiLangByID('201200536A-000009') /* 国际化处理： 非总部资产不可选择！*/, color: 'warning' });
			props.editTable.setValByKeyAndIndex(headtableId, index, 'pkorg_belong', { value: null, display: null });
			return;
		}
		let bodyAllRows = props.editTable.getAllRows(bodytableId, false);
		bodyAllRows.map((row) => {
			if (data.values.pkorg_belong.value == row.values.pk_org.value) {
				toast({
					content: getMultiLangByID('201200536A-000004') /* 国际化处理： 表体财务组织不能是总部资产所属财务组织！*/,
					color: 'warning'
				});
				props.editTable.setValByKeyAndIndex(headtableId, index, 'pkorg_belong', {
					value: null,
					display: null
				});
				return;
			}
		});
		//判断 -- 表体财务组织不能是总部资产所属财务组织
		//let pk_assetgroup = data.values.pk_assetgroup.value;
		// let data1 = {
		// 	pagecode,
		// 	pk_assetgroup,
		// 	areacode: bodytableId
		// };
		// ajax({
		// 	url: url.queryBodyUrl,
		// 	data: data1,
		// 	success: (res) => {
		// 		if (res.data) {
		// 			let bodyDatas = res.data[bodytableId].rows;
		// 			for (let i = 0; i < bodyDatas.length; i++) {
		// 				if (data.values.pkorg_belong.display == bodyDatas[i].values['pk_org.name'].display) {
		// 					toast({
		// 						content: getMultiLangByID('201200536A-000004') /* 国际化处理： 表体财务组织不能是总部资产所属财务组织！*/,
		// 						color: 'warning'
		// 					});
		// 					props.editTable.setValByKeyAndIndex(headtableId, index, 'pkorg_belong', {
		// 						value: null,
		// 						display: null
		// 					});
		// 					return;
		// 				}
		// 			}
		// 		}
		// 	}
		// });
	}
	if (key == 'group_flag') {
		if (value) {
			let allRows = props.editTable.getAllRows(headtableId, true);
			let num = 0;
			for (let i = 0; i < allRows.length; i++) {
				//删除行不再交验
				if (allRows[i].status == 3) {
					continue;
				}
				//与自己不在校验
				if (index == i) {
					continue;
				}
				if (allRows[i].values.group_flag.value == true) {
					num = num + 1;
					break;
				}
			}
			if (num != 0) {
				toast({
					content: getMultiLangByID('201200536A-000010') /* 国际化处理： 总部资产已经存在，不能有多个总部资产！*/,
					color: 'warning'
				});
				props.editTable.setValByKeyAndIndex(headtableId, index, 'group_flag', { value: false, display: null });
			} else {
				props.button.setButtonVisible(editBodyBtns, true);
				props.editTable.setStatus(bodytableId, 'edit');
				toggleShow(props, 'edit');
				document.getElementById('bodytable').style.display = 'block';
			}
		} else {
			props.editTable.setValByKeyAndIndex(headtableId, index, 'pkorg_belong', { value: null, display: null });
			//隐藏
			document.getElementById('bodytable').style.display = 'none';
		}
	}
	if (key == 'group_code') {
		//资产组编号不能超过4位
		//编号只能输入字母和数字
		let reg = /^[0-9a-zA-Z]+$/;
		if (value != '' && !reg.test(value)) {
			toast({
				content: getMultiLangByID('201200536A-000019') /* 国际化处理： 资产组编号输入不合法，编号只能输入字母和数字！*/,
				color: 'warning'
			});
			props.editTable.setValByKeyAndIndex(headtableId, index, 'group_code', { value: null, display: null });
			return;
		}
		if (value.length > 4) {
			toast({
				content: getMultiLangByID('201200536A-000020') /* 国际化处理： 资产组编号不能超过4位！*/,
				color: 'warning'
			});
			props.editTable.setValByKeyAndIndex(headtableId, index, 'group_code', { value: null, display: null });
			return;
		}
		let allRows = props.editTable.getAllRows(headtableId, true);
		for (let i = 0; i < allRows.length; i++) {
			//删除行不再交验
			if (allRows[i].status == 3) {
				continue;
			}
			//与自己不在校验
			if (index == i) {
				continue;
			}
			if (
				data.values.group_code.value != null &&
				data.values.group_code.value != '' &&
				data.values.group_code.value == allRows[i].values.group_code.value
			) {
				toast({ content: getMultiLangByID('201200536A-000012') /* 国际化处理： 资产组编码不允许重复！*/, color: 'warning' });
				props.editTable.setValByKeyAndIndex(headtableId, index, 'group_code', { value: null, display: null });
				return;
			}
		}
	}
	if (key == 'group_name') {
		let allRows = props.editTable.getAllRows(headtableId, true);
		for (let i = 0; i < allRows.length; i++) {
			//删除行不再交验
			if (allRows[i].status == 3) {
				continue;
			}
			//与自己不在校验
			if (index == i) {
				continue;
			}
			if (
				data.values.group_name.value != null &&
				data.values.group_name.value != '' &&
				data.values.group_name.value == allRows[i].values.group_name.value
			) {
				toast({ content: getMultiLangByID('201200536A-000013') /* 国际化处理： 资产组名称不允许重复！*/, color: 'warning' });
				props.editTable.setValByKeyAndIndex(headtableId, index, 'group_name', { value: null, display: null });
				return;
			}
		}
	}
	if (eventType == 'blur' && key != 'enablestate') {
		// 平台的自动增行不会附默认值，这里调用自己的增行
		let num = props.editTable.getNumberOfRows(headtableId);
		if (num == index + 1) {
			addClick.call(this, props, true);
		}
	}
}
