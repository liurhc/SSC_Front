import { headButtonClicks } from './headButtonClicks';

/**
 * 主表绑定按钮点击事件
 * @param {*} props 
 * @param {*} id 
 */
export default function(props, id) {
	headButtonClicks[id].call(this, props);
}
