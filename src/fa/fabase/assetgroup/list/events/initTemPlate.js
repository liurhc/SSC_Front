import headTableButtonClick from './headTableButtonClick';
import bodyTableButtonClick from './bodyTableButtonClick';
import rateTableButtonClick from './rateTableButtonClick';
import { setStatus, enableRow, disableRow, getData } from './headButtonClicks';
import { pageConfig } from './../const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, tableUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createEnableSwitch } = tableUtils;
/**
 * 模板初始化
 */
const { headtableId, bodytableId, ratetableId, pagecode } = pageConfig;

export default function initTemplate(props) {
	props.createUIDom(
		{
			//页面id
			pagecode
		},
		(data) => {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status') || 'browse';
	setStatus.call(this, props, status);
	let data = {
		pagecode,
		isshowseal: this.state.isshowseal
	};
	getData.call(this, data);
}

function modifierMeta(props, meta) {
	meta[headtableId].items.map((item) => {
		// 财务组织过滤
		if (item.attrcode == 'pkorg_belong') {
			item.queryCondition = () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		}
		return item;
	});
	meta[bodytableId].items.map((item) => {
		// 财务组织过滤
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		}
		return item;
	});
	meta[ratetableId].items.map((item) => {
		// 资产组参照过滤--不显示总部资产
		if (item.attrcode == 'pk_assetgroup') {
			item.refName = getMultiLangByID('201200536A-000000'); //资产组
			item.refName_db = getMultiLangByID('201200536A-000000'); //资产组
			item.queryCondition = () => {
				let filter = { notShowGroupFlag: true };
				return filter;
			};
		}
		return item;
	});

	// 处理停启用开关
	createEnableSwitch.call(this, props, { meta, enableRow, disableRow });

	const { button } = props;
	const { createOprationButton } = button;
	meta[headtableId].status = 'browse';
	//添加主表操作列
	let headOprCol = {
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		attrcode: 'headopr',
		itemtype: 'customer',
		// 锁定在右边
		fixed: 'right',
		width: '110px',
		render: (text, record, index) => {
			let recordVal = record.values;
			let status = props.editTable.getStatus(headtableId);
			let enableShow = false;
			let disableShow = false;
			let buttonAry = [ 'Delete' ];
			if (status == 'browse') {
				if (recordVal.enablestate.value == '2') {
					enableShow = false;
					disableShow = true;
				} else {
					enableShow = true;
					disableShow = false;
				}
			}
			return createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => headTableButtonClick.call(this, props, key, text, record, index)
			});
		}
	};
	//添加子表操作列
	let bodyOprCol = {
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		attrcode: 'bodyopr',
		itemtype: 'customer',
		render: (text, record, index) => {
			let buttonAry = [ 'Allocation', 'DelLine' ];
			return createOprationButton(buttonAry, {
				area: 'list_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => bodyTableButtonClick.call(this, props, key, text, record, index)
			});
		}
	};
	//添加孙表操作列
	let rateOprCol = {
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		attrcode: 'rateopr',
		itemtype: 'customer',
		render: (text, record, index) => {
			let buttonAry = [ 'DelLine' ];
			return createOprationButton(buttonAry, {
				area: 'list_rate_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => rateTableButtonClick.call(this, props, key, text, record, index)
			});
		}
	};
	meta[headtableId].items.push(headOprCol);
	meta[bodytableId].items.push(bodyOprCol);
	meta[ratetableId].items.push(rateOprCol);
	return meta;
}
