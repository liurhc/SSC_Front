import { ajax, toast, print, output } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { MsgConst, showMessage } = msgUtils;
/**
 * 主表绑定按钮点击事件
 */
const {
	headtableId,
	bodytableId,
	pagecode,
	areaType,
	editHeadBtns,
	editBodyBtns,
	browseHeadBtns,
	headmodalId,
	url,
	printFilename,
	printNodekey
} = pageConfig;

export const headButtonClicks = {
	Add: addClick,
	Cancel: cancelClick,
	Save: saveClick,
	enable: enableRow,
	disable: disableRow,
	Delete: batchDelClick,
	Edit: editClick,
	delRow: delRow,
	singleDel: singleDel,
	Print: printTemp,
	Output: outputTemp,
	Refresh: refreshTemp
};

//刷新
function refreshTemp(props) {
	let data = {
		pagecode,
		isshowseal: this.state.isshowseal
	};
	getData.call(this, data, true);
}

//输出
function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

//打印
function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType) {
	let checkedRows = props.editTable.getCheckedRows(headtableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values.pk_assetgroup.value);
	});
	let printData = {
		filename: printFilename, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

//新增按钮点击事件-head
export function addClick(props, isBlur = false) {
	let defaultValue = {};
	defaultValue['enablestate'] = {
		value: true
	};
	props.editTable.addRow(headtableId, undefined, isBlur == false, defaultValue);
	props.editTable.setStatus(bodytableId, 'edit');
	setStatus.call(this, props, 'edit');
	toggleShow(props, 'edit');
}

// 批量删除数据
function batchDelClick(props) {
	let checkedRows = props.editTable.getCheckedRows(headtableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	let indexArr = [];
	checkedRows.map((row) => {
		indexArr.push(row.index);
	});
	let status = props.editTable.getStatus(headtableId);
	if (status == 'browse' || status == undefined) {
		props.ncmodal.show(headmodalId, {
			title: getMultiLangByID('msgUtils-000000') /* 国际化处理： 删除*/,
			content: getMultiLangByID('msgUtils-000004') /* 国际化处理： 确定要删除所选数据吗？*/,
			beSureBtnClick: () => {
				delClick(props, headtableId, indexArr);
			}
		});
	} else {
		delRow(props, indexArr);
	}
}

/**
 * 前台删除--主表
 * @param {*} props 
 * @param {*} index 
 */
export function delRow(props, index) {
	props.editTable.deleteTableRowsByIndex(headtableId, index);
}

/**
 * 批量删除
 * @param {*} props 
 */
function delClick(props, headtableId, index = []) {
	let allRows = props.editTable.getAllRows(headtableId, false);
	let changedRows = [];
	for (let i = 0; i < allRows.length; i++) {
		if (index.includes(i)) {
			changedRows.push(allRows[i]);
		}
	}
	if (changedRows && changedRows.length != 0) {
		changedRows.map((row) => {
			modifiedBeforeAjax(row);
			row['status'] = '3';
		});
		const data = {
			model: {
				areaType,
				areacode: headtableId,
				rows: []
			},
			pageid: pagecode
		};
		data.model.rows = changedRows;
		ajax({
			url: url.deleteUrl,
			data,
			success: (res) => {
				let allRows = props.editTable.getAllRows(headtableId, false);
				let newRows = [];
				for (let i = 0; i < allRows.length; i++) {
					if (!index.includes(i)) {
						newRows.push(allRows[i]);
					}
				}
				let allData = props.editTable.getAllData(headtableId);
				allData.rows = newRows;
				props.editTable.setTableData(headtableId, allData);
				showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
				nonGroup_flag(props);
			}
		});
	}
}

/**
 * 浏览态直接删除单个数据
 * @param {*} props 
 * @param {*} headtableId 
 * @param {*} index 
 */
export function singleDel(props, index) {
	delClick(props, headtableId, [ index ]);
}

/**
 * 修改
 * @param {*} props 
 */
function editClick(props) {
	setStatus.call(this, props, 'edit');
	props.editTable.setStatus(bodytableId, 'edit');
	toggleShow(props, 'edit');
}

// 切换子表按钮状态
export function toggleShow(props, status) {
	let browseBtns = [ 'Allocation' ];
	let editBtns = [ 'DelLine', 'AddLine' ];
	if (status === 'edit') {
		//编辑态
		props.button.setButtonVisible(browseBtns, false);
		props.button.setButtonVisible(editBtns, true);
	} else {
		//浏览态
		props.button.setButtonVisible(editBtns, false);
		props.button.setButtonVisible(browseBtns, true);
	}
}

//取消按钮点击事件
function cancelClick(props) {
	props.ncmodal.show('Cancel', {
		title: getMultiLangByID('msgUtils-000002') /* 国际化处理： 取消*/,
		content: getMultiLangByID('msgUtils-000003') /* 国际化处理： 确定要取消吗？*/,
		beSureBtnClick: () => {
			let data = {
				pagecode,
				isshowseal: this.state.isshowseal
			};
			getData.call(this, data);
			//过滤空行
			// props.editTable.filterEmptyRows(headtableId,[ 'memo','pkorg_belong','group_flag','enablestate']);
			// props.editTable.filterEmptyRows(bodytableId,[ 'pk_org','pk_org.name']);
			// props.editTable.cancelEdit(headtableId);
			props.editTable.cancelEdit(bodytableId);
			setBatchBtnsEnable.call(this, props, headtableId);
			setStatus.call(this, props, 'browse');
			toggleShow(props, 'browse');
		}
	});
}

//保存按钮点击事件
function saveClick(props) {
	//过滤空行
	props.editTable.filterEmptyRows(bodytableId, []);
	props.editTable.filterEmptyRows(headtableId, [ 'enablestate', 'group_flag' ]);

	let visibleRows = props.editTable.getVisibleRows(headtableId);
	let pass = props.editTable.checkRequired(headtableId, visibleRows);
	// 主表必输项校验没过
	if (!pass) {
		return;
	}
	let bodyVisibleRows = props.editTable.getVisibleRows(bodytableId);
	let bodyPass = props.editTable.checkRequired(bodytableId, bodyVisibleRows);
	// 子表必输项校验没过
	if (!bodyPass) {
		return;
	}
	//先判断是否有修改的数据
	let headExist = props.editTable.getChangedRows(headtableId);
	let bodyExist = props.editTable.getChangedRows(bodytableId);
	if (headExist.length == 0 && bodyExist.length == 0) {
		props.editTable.cancelEdit(headtableId, () => {
			setBatchBtnsEnable.call(this, props, headtableId);
		});
		props.editTable.cancelEdit(bodytableId);
		setStatus.call(this, props, 'browse');
		toggleShow(props, 'browse');
		showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
		return;
	}

	/*
		获取所有行数据，falg：是否关闭必输项校验，默认为false，改为true不校验必输项
		如果主表没修改，子表修改，则传回的HeadVO不好处理，所以将全部主表数据传回去
	*/
	let headTableData = props.editTable.getAllRows(headtableId, false);
	let bodyTableData = props.editTable.getAllRows(bodytableId, false);
	//判断选择总部资产之后，总部资产所属财务组织是否为空
	if (headTableData.length > 0) {
		for (let i = 0; i < headTableData.length; i++) {
			if (headTableData[i].values.group_flag.value == true) {
				if (
					headTableData[i].values.pkorg_belong.value == '' ||
					headTableData[i].values.pkorg_belong.value == null
				) {
					toast({
						content: getMultiLangByID('201200536A-000014') /* 国际化处理： 总部资产所属财务组织不能为空！*/,
						color: 'warning'
					});
					return;
				}
			}
		}
	}
	//先判断分配比率是不是100，不是则不符合要求
	if (bodyTableData.length > 0) {
		let num = 0;
		let flag = 0;
		for (let i = 0; i < bodyTableData.length; i++) {
			/* 
				status: 0 不变  1  修改  2  新增  3  删除
				删除行不再交验
			*/
			if (bodyTableData[i].status == 3) {
				flag++;
				continue;
			}
			num = num + parseFloat(bodyTableData[i].values.rate.value);
		}
		if (flag != bodyTableData.length) {
			if (num != 100) {
				toast({
					content: getMultiLangByID('201200536A-000015') /* 国际化处理： 表体分摊比例之和必须等于100！*/,
					color: 'warning'
				});
				return;
			}
		}
	}
	// 将enablestate的值由boolean转换为int类型，以适配后端
	headTableData.map((item) => {
		modifiedBeforeAjax(item);
	});

	//由于验证公式传的是grid，所以只传主表数据，不传子表数据
	const headdata = {
		pageid: pagecode,
		model: {
			areaType,
			areacode: headtableId,
			rows: headTableData
		}
	};
	const data = {
		head: {
			model: {
				areaType,
				areacode: headtableId,
				rows: []
			},
			pageid: pagecode
		},
		body: {
			model: {
				areaType,
				areacode: bodytableId,
				rows: []
			},
			pageid: pagecode
		},
		pageid: pagecode,
		isshowseal: this.state.isshowseal
	};
	data.head.model.rows = headTableData;
	data.body.model.rows = bodyTableData;
	// 保存前执行验证公式--主表data
	props.validateToSave(headdata, () => {
		ajax({
			url: url.saveUrl,
			data,
			success: (res) => {
				if (res.data) {
					// 将enablestate的值由int转换为bool类型，以适配switch
					res.data.head[headtableId].rows.map((item) => {
						modifiedAfterAjax(item);
					});
					props.editTable.setTableData(headtableId, res.data.head[headtableId]);
					if (res.data.body == null || res.data.body == undefined) {
						props.editTable.setTableData(bodytableId, { rows: [] });
					} else {
						props.editTable.setTableData(bodytableId, res.data.body[bodytableId]);
					}
					props.editTable.cancelEdit(headtableId, () => {
						setBatchBtnsEnable.call(this, props, headtableId);
					});
					props.editTable.cancelEdit(bodytableId);
					setStatus.call(this, props, 'browse');
					toggleShow(props, 'browse');
					showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
				}
			}
		});
	});
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status) {
	switch (status) {
		case 'edit':
			props.editTable.setStatus(headtableId, 'edit');
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseHeadBtns, false);
			props.button.setButtonVisible(editHeadBtns, true);
			// 如果有新增按钮，改为次要按钮
			editHeadBtns.map((item) => {
				if (item == 'Add') {
					props.button.setMainButton(item, false);
				}
			});
			// 行删除时不需要悬浮框提示
			props.button.setPopContent('Delete', undefined);
			break;
		default:
			props.editTable.setStatus(headtableId, 'browse');
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editHeadBtns, false);
			props.button.setButtonVisible(editBodyBtns, false);
			props.button.setButtonVisible(browseHeadBtns, true);
			// 如果有新增按钮，改为主要按钮
			browseHeadBtns.map((item) => {
				if (item == 'Add') {
					props.button.setMainButton(item, true);
				}
			});
			// 行删除时悬浮框提示
			props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/);
			break;
	}
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, headtableId) {
	let checkedRows = props.editTable.getCheckedRows(headtableId);
	let { pageConfig = {} } = props;
	let { batchBtns = [ 'Delete', 'Print', 'Output' ] } = pageConfig;
	props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0));
	//修改按钮是否可用
	let allRows = props.editTable.getAllRows(headtableId, true);
	props.button.setButtonDisabled([ 'Edit' ], !(allRows && allRows.length > 0));
}

//在修改之后看是否还有总部资产，若没有则清空子表
function nonGroup_flag(props) {
	let allRows = props.editTable.getAllRows(headtableId, true);
	let num = 0;
	for (let i = 0; i < allRows.length; i++) {
		if (allRows[i].values.group_flag.value == true) {
			num = num + 1;
		}
	}
	if (num == 0) {
		//清空子表
		props.editTable.setTableData(bodytableId, { rows: [] });
	}
}

//启用
export function enableRow(props, index) {
	let allRows = props.editTable.getAllRows(headtableId, false);
	modifiedBeforeAjax(allRows[index]);
	const data = {
		pageid: pagecode,
		model: {
			areaType,
			rows: [ allRows[index] ]
		}
	};
	ajax({
		url: url.unSealUrl,
		data,
		success: function(res) {
			if (res.data) {
				// 更新数据
				let retData = res.data[headtableId];
				allRows[index] = retData.rows[0];
				let allData = props.editTable.getAllData(headtableId);
				allData.rows = allRows;
				// 将enablestate的值由int转换为bool类型，以适配switch
				allData.rows.map((item) => {
					modifiedAfterAjax(item);
				});
				props.editTable.setTableData(headtableId, allData);
				showMessage.call(this, props, { type: MsgConst.Type.EnableSuccess });
			}
		}
	});
}

//停用
export function disableRow(props, index) {
	let allRows = props.editTable.getAllRows(headtableId, false);
	modifiedBeforeAjax(allRows[index]);
	const data = {
		pageid: pagecode,
		model: {
			areaType,
			rows: [ allRows[index] ]
		}
	};
	ajax({
		url: url.sealUrl,
		data,
		success: function(res) {
			if (res.data) {
				// 更新数据
				let retData = res.data[headtableId];
				allRows[index] = retData.rows[0];
				let allData = props.editTable.getAllData(headtableId);
				allData.rows = allRows;
				// 将enablestate的值由int转换为bool类型，以适配switch
				allData.rows.map((item) => {
					modifiedAfterAjax(item);
				});
				props.editTable.setTableData(headtableId, allData);
				showMessage.call(this, props, { type: MsgConst.Type.DisableSuccess });
			}
		}
	});
}

function modifiedBeforeAjax(item) {
	if (item.values['enablestate'].value == true) {
		item.values['enablestate'].value = '2';
	} else {
		item.values['enablestate'].value = '3';
	}
}

function modifiedAfterAjax(item) {
	if (item.values['enablestate'].value == 2) {
		item.values['enablestate'].value = true;
	} else if (item.values['enablestate'].value == 3) {
		item.values['enablestate'].value = false;
	}
}

/**
 * 查询数据
 */
export function getData(data, isRefresh = false) {
	ajax({
		url: url.queryHeadUrl,
		data: data,
		success: (res) => {
			if (res.data == null) {
				//清空
				this.props.editTable.setTableData(headtableId, { rows: [] });
				this.props.editTable.setTableData(bodytableId, { rows: [] });
				setBatchBtnsEnable.call(this, this.props, headtableId);
				if (isRefresh) {
					showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
				}
				return;
			}
			// 将enablestate的值由int转换为bool类型，以适配switch
			res.data[headtableId].rows.map((item) => {
				if (item.values['enablestate'].value == 2) {
					item.values['enablestate'].value = true;
				} else {
					item.values['enablestate'].value = false;
				}
			});
			this.props.editTable.setTableData(headtableId, res.data[headtableId]);
			setBatchBtnsEnable.call(this, this.props, headtableId);
			toggleShow.call(this, this.props, 'browse');
			if (isRefresh) {
				showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
			}
		}
	});
}
