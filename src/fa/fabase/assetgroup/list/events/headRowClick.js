import { ajax } from 'nc-lightapp-front';
import { toggleShow } from './headButtonClicks';
import { pageConfig } from './../const.js';

/**
 * 主表行点击事件
 */
const { headtableId, bodytableId, pagecode, url } = pageConfig;

export default function(props, id, record, index) {
	let status = props.editTable.getStatus(headtableId);
	let group_flag = record.values.group_flag.value;
	let pk_assetgroup = record.values.pk_assetgroup.value;
	pageConfig.pk_assetgroup = pk_assetgroup;
	let data = {
		pagecode,
		pk_assetgroup,
		areacode: bodytableId
	};
	if (group_flag == true) {
		let bodyExist = props.editTable.getChangedRows(bodytableId);
		if (bodyExist && bodyExist.length > 0) {
			this.setState({
				isshowbody: 'block'
			});
		} else {
			ajax({
				url: url.queryBodyUrl,
				data: data,
				success: (res) => {
					if (res.data) {
						props.editTable.setTableData(bodytableId, res.data[bodytableId]);
						if (status === 'edit') {
							props.editTable.setStatus(bodytableId, 'edit');
						} else {
							props.editTable.setStatus(bodytableId, 'browse');
						}
					}
					toggleShow(props, status);
					this.setState({
						isshowbody: 'block'
					});
				}
			});
		}
	} else if (group_flag == false) {
		//隐藏
		this.setState({
			isshowbody: 'none'
		});
	} else {
		return;
	}
}
