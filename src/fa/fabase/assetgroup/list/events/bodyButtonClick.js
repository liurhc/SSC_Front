import { toast } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/**
 * 子表绑定按钮点击事件
 */
const { bodytableId, headtableId } = pageConfig;

export default function(props) {
	let allRows = props.editTable.getAllRows(headtableId, true);
	let status = props.editTable.getStatus(bodytableId);
	if (status == undefined || status == 'browse') {
		toast({ content: getMultiLangByID('201200536A-000005') /* 国际化处理： 浏览态不可新增！*/, color: 'warning' });
		return;
	}
	let flag = true;
	for (let i = 0; i < allRows.length; i++) {
		if (allRows[i].values.group_flag.value == true) {
			flag = false;
		}
	}
	if (flag) {
		toast({ content: getMultiLangByID('201200536A-000006') /* 国际化处理： 无总部资产，不可新增！*/, color: 'warning' });
		return;
	}
	//let isBlur = false;
	//props.editTable.addRow(bodytableId, undefined, isBlur == false);
	props.editTable.addRow(bodytableId);
}
