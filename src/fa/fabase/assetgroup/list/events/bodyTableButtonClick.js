import { ajax, toast } from 'nc-lightapp-front';
import { toggleShow } from './headButtonClicks';
import { pageConfig } from './../const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ratetableId, bodytableId, rateAddModalId, pagecode, url } = pageConfig;

/**
 * 子表表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	switch (key) {
		case 'DelLine':
			// 删除行
			delLine.call(this, props, index);
			break;
		case 'Allocation':
			// 分配
			AllocationLine.call(this, props, record, index);
			break;
		default:
			break;
	}
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 */
function delLine(props, index) {
	let status = props.editTable.getStatus(bodytableId);
	if (status == undefined || status == 'browse') {
		toast({ content: getMultiLangByID('201200536A-000007') /* 国际化处理： 浏览态不可直接删除！*/, color: 'warning' });
	} else {
		delRow.call(this, props, index);
	}
}

function delRow(props, index) {
	props.editTable.deleteTableRowsByIndex(bodytableId, index);
}

/**
 * 分配
 * @param {*} props 
 * @param {*} index 
 */
function AllocationLine(props, record, index) {
	let pk_assetgroup_b = record.values.pk_assetgroup_b.value;
	if (pk_assetgroup_b == null || pk_assetgroup_b == '') {
		toast({ content: getMultiLangByID('201200536A-000008') /* 国际化处理： 该数据还未记录，请先进行保存，在进行分配！*/, color: 'warning' });
		return;
	}
	toggleShow(props, 'edit');
	props.modal.show(rateAddModalId);
	// 先将以前数据清空
	props.editTable.setTableData(ratetableId, { rows: [] });
	let data = {
		pk_assetgroup_b,
		pagecode,
		areacode: ratetableId
	};
	//记录子表主键id
	pageConfig.pk_assetgroup_b = pk_assetgroup_b;
	ajax({
		url: url.queryRateUrl,
		data: data,
		success: (res) => {
			if (res.data != null) {
				props.editTable.setTableData(ratetableId, res.data[ratetableId]);
				props.editTable.setStatus(ratetableId, 'edit');
			}
		}
	});
}
