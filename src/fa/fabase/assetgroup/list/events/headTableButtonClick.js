import { singleDel, delRow } from './headButtonClicks';
import { pageConfig } from './../const.js';

const headtableId = pageConfig.headtableId;

/**
 * 主表表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	switch (key) {
		case 'Delete':
			// 删除行
			delLine.call(this, props, index);
			break;
		default:
			break;
	}
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 */
function delLine(props, index) {
	let status = props.editTable.getStatus(headtableId);
	if (status == undefined || status == 'browse') {
		singleDel.call(this, props, index);
	} else {
		delRow.call(this, props, index);
	}
}
