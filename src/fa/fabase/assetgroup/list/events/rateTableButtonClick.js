import { pageConfig } from './../const.js';

const { ratetableId } = pageConfig;
/**
 * 孙表表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	switch (key) {
		case 'DelLine':
			// 删除行
			deleteLine.call(this, props, index);
			break;
		default:
			break;
	}
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 */
function deleteLine(props, index) {
	props.editTable.deleteTableRowsByIndex(ratetableId, index);
	props.editTable.setStatus(ratetableId, 'edit');
}
