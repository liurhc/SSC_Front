import { toast } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ratetableId } = pageConfig;
/**
 * 孙表编辑后事件
 */

export default function(props, id, key, value, changedrows, index, data, type, eventType) {
	//先判断是不是修改的资产组
	if (key == 'pk_assetgroup') {
		//过滤空行
		//props.editTable.filterEmptyRows(ratetableId, []);
		let rateAllRows = props.editTable.getAllRows(ratetableId, false);
		if (rateAllRows.length > 0) {
			for (let i = 0; i < rateAllRows.length; i++) {
				//删除行不再交验
				if (rateAllRows[i].status == 3) {
					continue;
				}
				//与自己不在校验
				if (index == i) {
					continue;
				}
				if (
					data.values.pk_assetgroup != null &&
					rateAllRows[i].values.pk_assetgroup.display != null &&
					data.values.pk_assetgroup.display == rateAllRows[i].values.pk_assetgroup.display
				) {
					toast({ content: getMultiLangByID('201200536A-000018') /* 资产组不允许重复设置,请重新选择！*/, color: 'warning' });
					props.editTable.setValByKeyAndIndex(ratetableId, index, 'pk_assetgroup', {
						value: null,
						display: null
					});
				}
			}
		}
	}
}
