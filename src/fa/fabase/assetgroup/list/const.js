// 页面配置
const pageConfig = {
	// 小应用编码
	appcode: '201200536A',
	// 主表格区域id
	headtableId: 'list_head',
	// 子表格区域id
	bodytableId: 'list_body',
	// 孙表格区域id
	ratetableId: 'list_rate',
	//列表区域类型
	areaType: 'table',
	//孙表区域编码
	rateAreacode: 'list_rate',
	// 表格节点编码
	pagecode: '201200536A_list',
	// 主表浏览态按钮
	browseHeadBtns: [ 'Add', 'Edit', 'Delete', 'Print', 'Refresh' ],
	// 主表编辑态按钮
	editHeadBtns: [ 'Add', 'Delete', 'Save', 'Cancel' ],
	// 子表编辑态按钮
	editBodyBtns: [ 'AddLine' ],
	// 路径
	url: {
		translateOrgUrl: '/nccloud/fa/assetgroup/translateOrg.do',
		queryRateUrl: '/nccloud/fa/assetgroup/queryRate.do',
		queryBodyUrl: '/nccloud/fa/assetgroup/queryBody.do',
		queryHeadUrl: '/nccloud/fa/assetgroup/queryHead.do',
		saveUrl: '/nccloud/fa/assetgroup/update.do',
		deleteUrl: '/nccloud/fa/assetgroup/delete.do',
		sealUrl: '/nccloud/fa/assetgroup/doSeal.do',
		unSealUrl: '/nccloud/fa/assetgroup/doUnSeal.do',
		printUrl: '/nccloud/fa/assetgroup/printList.do',
		insertRateUrl: '/nccloud/fa/assetgroup/insertRate.do'
	},
	//分配主表模态框ID
	headmodalId: 'assetgroup_head',
	//分配子表模态框ID
	bodymodalId: 'assetgroup_body',
	//分配孙表模态框ID
	ratemodalId: 'assetgroup_rate',
	//分配孙表弹出框ID--表格
	rateAddModalId: 'assetgroup-add-modal',

	formArea: 'head',
	//记录主表总部资产主键id
	pk_assetgroup: '',
	//记录子表主键id
	pk_assetgroup_b: '',
	//功能节点编码、模板编码
	printFuncode: '2012004070',
	//模板节点标识
	printNodekey: null,
	//文件名称
	printFilename: '201200536A-000000' /* 国际化处理： 资产组*/,
	//主表名称
	headTitle: '201200536A-000000' /* 国际化处理： 资产组*/,
	//子表名称
	bodyTitle: '201200536A-000001' /* 国际化处理： 财务组织*/,
	//孙表名称
	rateTitle: '201200536A-000002' /* 国际化处理： 总部资产分摊模板*/
};

export { pageConfig };
