import initTemplate from './initTemplate';
import buttonClick, { changeToBoolean, getData, toCard } from './buttonClick';
import doubleClick from './doubleClick';
import afterEvent from './afterEvent';
import tableButtonClick from './tableButtonClick';
export { toCard, initTemplate, buttonClick, doubleClick, changeToBoolean, afterEvent, tableButtonClick, getData };
