//表体行操作列处理
import { base, ajax, toast } from 'nc-lightapp-front';
import { baselistpageConfig } from '../const';
import { toCard } from '.';

// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

let { cardRouter, tableId, url } = baselistpageConfig;
export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		case 'Edit':
			toCard.call(this, props, record, 'edit');
			break;
		case 'Delete':
			DeleteAction(props, key, text, record, index);
			break;
	}
}
function DeleteAction(props, key, text, record, index) {
	//通过传往后台一个PK来进行删除
	ajax({
		url: url.deleteUrl,
		data: {
			pk: record.values.pk_depprogram.value,
			//组织
			pk_org: record.values.pk_org.value,
			//nodeType,如果是组织返回org
			nodeType: props.listpageConfig.nodeType,
			//nodeCode：功能节点编号，便于区分集团还是组织
			nodeCode: props.listpageConfig.nodeCode
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				//删掉当前行
				props.editTable.delRow(tableId, index);
				// 平台适配单页清缓存
				props.table.deleteCacheId(tableId, record.values.pk_depprogram.value);
				//提示用户
				toast({ content: getMultiLangByID('msgUtils-000015') /*删除成功*/, color: 'success' });
			}
		}
	});
}
