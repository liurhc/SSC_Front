import { toCard } from '.';
export default function doubleClick(record, index, props) {
	toCard.call(this, props, record, 'browse');
}
