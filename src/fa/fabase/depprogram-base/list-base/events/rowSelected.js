import { baselistpageConfig } from '../const';

/**
 * 行选中事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} record 
 * @param {*} index 
 * @param {*} status 
 */
export default function(props) {
	let checkedRows = props.editTable.getCheckedRows(baselistpageConfig.tableId);
	let disabled = false;
	if (checkedRows.length == 0) {
		disabled = true;
	}
	props.button.setButtonDisabled([ 'Print', 'BatchDelete', 'Output' ], disabled);
}
