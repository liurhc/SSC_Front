import { toast, print, ajax, output, cardCache } from 'nc-lightapp-front';
import { baselistpageConfig } from '../const';
import rowSelected from './rowSelected';

// import { modifyBeforeAjax, modifyAfterAjax } from '../../../../../ampub/common/utils/tableUtils';
// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
// import { showMessage, MsgConst } from '../../../../../ampub/common/utils/msgUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, tableUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { modifyBeforeAjax, modifyAfterAjax } = tableUtils;
const { showConfirm, showMessage, MsgConst } = msgUtils;

let { cardRouter, tableId, add, url } = baselistpageConfig;
// 判断财务组织是否选择的变量，不选择不允许添加
export default function buttonClick(props, id) {
	switch (id) {
		case add:
			addaction.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'BatchDelete':
			props.modal.show('BatchDelete', {
				title: getMultiLangByID('msgUtils-000000') /*删除*/,
				content: getMultiLangByID('msgUtils-000004') /*确定要删除所选数据吗？*/,
				beSureBtnClick: () => {
					batchDelete.call(this, props);
				}
			});

			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 将enablestate的值从int型转为boolean型
 * @param {Object} item 
 */
export function changeToBoolean(item) {
	if (item.values['enablestate'].value == 2) {
		item.values['enablestate'].value = true;
	} else {
		item.values['enablestate'].value = false;
	}
}

/**
 * 将enablestate的值从boolean型转为int型
 * @param {Object} item 
 */
export function changeToInt(item) {
	if (item.values['enablestate'].value == true) {
		item.values['enablestate'].value = '2';
	} else {
		item.values['enablestate'].value = '3';
	}
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		toast({ content: getMultiLangByID('msgUtils-000021') /*请选择需要打印的数据*/, color: 'warning' });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		toast({ content: getMultiLangByID('msgUtils-000022') /*请选择需要输出的数据*/, color: 'warning' });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

/* 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		toast({ content: getMultiLangByID('msgUtils-000021') /*请选择需要打印的数据*/, color: 'warning' });
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values['pk_depprogram'].value);
	});
	let printData = {
		filename: props.listpageConfig.pagecode, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

//批量删除
function batchDelete(props) {
	let that = this;
	//获取一下行号删除掉
	let rowsWillDel = props.editTable.getCheckedRows(tableId);
	let indexs = [];
	if (rowsWillDel && rowsWillDel.length) {
		rowsWillDel = rowsWillDel.map((row) => {
			indexs.push(row.index);
			return row.data;
		});

		doDelete.call(that, props, rowsWillDel, indexs);
	} else {
		toast({ content: getMultiLangByID('201200540A-000013') /*请选择需要删除的档案*/, color: 'warning' });
	}
}
/**
 * 执行删除
 *
 * @props props
 * @param rows 需要删除的行数组
 */
function doDelete(props, rows, indexs) {
	let that = this;
	if (rows) {
		let pks = [];
		rows.forEach((row) => {
			pks.push(row.values.pk_depprogram.value);
		});
		const data = {
			pks: pks.join(',')
		};
		ajax({
			url: url.batchDeleteUrl,
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//提示用户
					if (data) {
						let resData = data.split('</br>');
						if (resData) {
							if (resData.length == 1) {
								if (resData[0] == getMultiLangByID('201200540A-000014') /*该单据已经被他人删除！*/) {
									toast({ content: resData.shift(), color: 'danger', duration: 'infinity' });
								} else {
									toast({ content: resData.shift(), color: 'success', duration: 'infinity' });
								}
							} else {
								let color = 'danger';
								if (resData.length == 2) {
									color = 'success';
								}
								toast({
									duration: 'infinity',
									content: resData.shift(),
									color: color,
									groupOperation: true,
									groupOperationMsg: resData,
									TextArr: [
										getMultiLangByID('201200540A-000015') /*查看*/,
										getMultiLangByID('amcommon-000006') /*收起*/,
										getMultiLangByID('ScriptReturn-000000') /*我知道了*/
									]
								});
							}
						}
					}
				}
				getData.call(that, that.state.curOrg);
			},
			error: (err) => {
				toast({ content: err.message, color: 'danger' });
			}
		});
	}
}
function doDeleteBeforeCheck(props, row) {
	const pk_org = row.pk_org.value;
	const pk_group = row.pk_group.value;
	const isOrg = props.listpageConfig.nodeType;
	let error = '';
	//增加删除判断组织不能删除集团的数据
	if (isOrg === 'org') {
		if (pk_org === pk_group) {
			error =
				getMultiLangByID('201200540A-000016', {
					code: row.program_code.value
				}) /*方案编码【{code}】为集团数据组织节点不可以删除！*/ + '</br>';
		}
	}
	return error;
}
/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	getData.call(this, this.state.curOrg, () => {
		toast({ color: 'success', content: getMultiLangByID('msgUtils-000020') /*刷新成功*/ });
	});
}
//查询数据这里需要打开页面默认加载查询 需要由加载时index里的componentDidMount方法调用。
export function getData(value, callback) {
	let that = this;
	let data = {
		//是否显示停用
		isShowSeal: that.state.isShowOff + '',
		//组织
		pk_org: value.refpk,
		//nodeType,如果是组织返回org
		nodeType: that.props.listpageConfig.nodeType,
		//nodeCode：功能节点编号，便于区分集团还是组织
		nodeCode: that.props.listpageConfig.nodeCode,
		//pagecode翻译得时候需要
		pagecode: that.props.listpageConfig.pagecode
	};
	ajax({
		url: url.queryUrl,
		data: data,
		success: (res) => {
			let { success, data } = res;
			let pks = new Map();
			let index = 0;
			if (success) {
				if (data) {
					//启用状况的值由int->boolean 适配switch
					data[tableId].rows.map((item) => {
						changeToBoolean(item);
						item.values['enablestate']['isEdit'] = true;
						pks.set(item.values['pk_depprogram'].value, index++);
						if (item.values['pk_org_v'].value == '~' || !item.values['pk_org_v'].value) {
							item.values['pk_org_v'].display = item.values['pk_group'].display;
						}
					});
					let returndata = JSON.parse(JSON.stringify(data[tableId]));
					that.setState({ returndata, pks }, () => {
						that.searchOnChange();
					});
				} else {
					//增加如果没有任何一条启用的数据查询全部停用数据,
					getDatabyseal.call(that, that.state.curOrg);
				}
			}
			rowSelected(that.props);
			if (callback && 'function' == typeof callback) {
				callback();
			}
		}
	});
}

function addaction(props) {
	// //如果是组织节点且未选择财务组织，提示用户
	if (props.listpageConfig.nodeType == 'org') {
		if (!this.hasOrg) {
			toast({ content: getMultiLangByID('201200540A-000017') /*未选择财务组织，无法新增！*/, color: 'danger' });
			return;
		}
	}
	setAllPks.call(this, props, 'pk_depprogram');
	props.pushTo(cardRouter, {
		pagecode: props.listpageConfig.pagecode_card,
		status: 'add',
		pk_org: props.listpageConfig.pk_org.value,
		display: props.listpageConfig.pk_org.display
		// org:应该需要传财务组织，卡片态需要接受并处理这个参数(赋值到表单)
	});
}

export function toCard(props, record, status) {
	setAllPks.call(this, props, 'pk_depprogram', record.values.pk_depprogram.value);
	props.pushTo(cardRouter, {
		status: status,
		pagecode: props.listpageConfig.pagecode_card,
		pk: record.values.pk_depprogram.value,
		org: record.values.pk_org.value,
		display: record.values.pk_org.display,
		id: record.values.pk_depprogram.value
	});
}

function setAllPks(props, pkField, currentPk) {
	let allpks = [];
	let tableData = props.editTable.getAllRows(tableId);
	tableData.map((record, index) => {
		if (record && record.values && record.values[pkField]) {
			allpks.push(record.values[pkField].value);
		}
	});
	let editTable = {};
	editTable.simpleTable = {};
	editTable.simpleTable.allpks = allpks;
	props.ViewModel.setData(this.dataSource, editTable);
}

export function getDatabyseal(value) {
	let that = this;
	let data = {
		//是否显示停用
		isShowSeal: true + '',
		//组织
		pk_org: value.refpk,
		//nodeType,如果是组织返回org
		nodeType: that.props.listpageConfig.nodeType,
		//nodeCode：功能节点编号，便于区分集团还是组织
		nodeCode: that.props.listpageConfig.nodeCode,
		//pagecode翻译得时候需要
		pagecode: that.props.listpageConfig.pagecode
	};
	ajax({
		url: url.queryUrl,
		data: data,
		success: (res) => {
			let { success, data } = res;
			let pks = new Map();
			let index = 0;
			if (success) {
				if (data) {
					//启用状况的值由int->boolean 适配switch
					data[tableId].rows.map((item) => {
						changeToBoolean(item);
						item.values['enablestate']['isEdit'] = true;
						pks.set(item.values['pk_depprogram'].value, index++);
						if (item.values['pk_org_v'].value == '~' || !item.values['pk_org_v'].value) {
							item.values['pk_org_v'].display = item.values['pk_group'].display;
						}
					});
					let returndata = JSON.parse(JSON.stringify(data[tableId]));
					that.setState({ returndata, pks, isShowOff: true }, () => {
						that.searchOnChange();
					});
				} else {
					that.props.editTable.setTableData(tableId, { rows: [] });
				}
				rowSelected(that.props);
			}
		}
	});
}

/**
 * 启用
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
export function enableRow(props, index) {
	const { tableId, url } = baselistpageConfig;
	let allData = props.editTable.getAllData(tableId);
	const nodeType = props.listpageConfig.nodeType;
	const pagecode = props.listpageConfig.pagecode;
	let allRows = allData.rows;
	// 组织节点集团数据不可停启用
	let recordVal = allRows[index].values;
	let flag =
		nodeType == 'org' &&
		recordVal.pk_group &&
		recordVal.pk_org &&
		recordVal.pk_group.value == recordVal.pk_org.value;
	if (flag) {
		toast({ content: getMultiLangByID('201200540A-000018') /*业务单元节点不能停启用集团数据*/, color: 'warning' });
		return;
	}
	recordVal.enablestate.value = !recordVal.enablestate.value;
	modifyBeforeAjax(allRows[index]);
	const data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: [ allRows[index] ]
		}
	};
	ajax({
		url: url.unsealUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let resData = data[tableId];
				//将enablestate的int值转为boolean值，以适配switch
				changeToBoolean(resData.rows[0]);
				//通过成功返回的数据修改之前获取的全部列的数据
				allRows[index] = resData.rows[0];
				//获取表格的全部数据
				let allData = props.editTable.getAllData(tableId);
				//设置表格数据
				allData.rows = allRows;
				props.editTable.setTableData(tableId, allData);
				//提示用户消息
				showMessage.call(this, props, { type: MsgConst.Type.EnableSuccess });
			}
		},
		error: (res) => {
			modifyAfterAjax(allRows[index]);
			toast({ color: 'danger', content: res.message });
		}
	});
}

/**
 * 停用
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
export function disableRow(props, index) {
	const { tableId, url } = baselistpageConfig;
	let allData = props.editTable.getAllData(tableId);
	const nodeType = props.listpageConfig.nodeType;
	const pagecode = props.listpageConfig.pagecode;
	let allRows = allData.rows;
	// 组织节点集团数据不可停启用
	let recordVal = allRows[index].values;
	let flag =
		nodeType == 'org' &&
		recordVal.pk_group &&
		recordVal.pk_org &&
		recordVal.pk_group.value == recordVal.pk_org.value;
	if (flag) {
		toast({ content: getMultiLangByID('201200540A-000018') /*业务单元节点不能停启用集团数据*/, color: 'warning' });
		return;
	}
	modifyBeforeAjax(allRows[index]);
	const data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: [ allRows[index] ]
		}
	};
	ajax({
		url: url.sealUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let resData = data[tableId];
				//将enablestate的int值转为boolean值，以适配switch
				changeToBoolean(resData.rows[0]);
				//通过成功返回的数据修改之前获取的全部列的数据
				allRows[index] = resData.rows[0];
				//获取表格的全部数据
				let allData = props.editTable.getAllData(tableId);
				//设置表格数据
				allData.rows = allRows;
				props.editTable.setTableData(tableId, allData);
				//提示用户消息
				showMessage.call(this, props, { type: MsgConst.Type.DisableSuccess });
			}
		},
		error: (res) => {
			modifyAfterAjax(allRows[index]);
			toast({ color: 'danger', content: res.message });
		}
	});
}
