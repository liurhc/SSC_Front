import { cardCache } from 'nc-lightapp-front';
import { tableButtonClick } from '../events';
import { baselistpageConfig } from '../const';
import { toCard } from '.';
import { enableRow, disableRow, getData } from './buttonClick';

// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
// import {loginContext,getContext,loginContextKeys} from '../../../../../ampub/common/components/AMInitInfo/loginContext';
// import { createEnableSwitch } from '../../../../../ampub/common/utils/tableUtils';
import ampub from 'ampub';
const { utils, components } = ampub;
const { LoginContext } = components;
const { multiLangUtils, tableUtils } = utils;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { getMultiLangByID } = multiLangUtils;
const { createEnableSwitch } = tableUtils;

let { tableId } = baselistpageConfig;
let { getDefData } = cardCache;

export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: props.listpageConfig.pagecode,
			appcode: props.listpageConfig.appcode
		},
		function(data) {
			if (data) {
				if (data.context) {
					//获取登录上下文信息
					loginContext.call(that, data.context);
					let curOrg = {
						refpk: getContext(loginContextKeys.pk_org),
						refname: getContext(loginContextKeys.org_Name)
					};
					//组织节点设置默认组织
					if (that.props.listpageConfig.nodeType != 'group') {
						let value = getDefData('lastOrg', that.dataSource);
						if (value) {
							that.onOrgChange(value);
						} else {
							that.onOrgChange(curOrg);
						}
					} else {
						getData.call(that, curOrg);
					}
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000004') /*确定要删除所选数据吗？*/);
					props.button.setButtonDisabled([ 'Print', 'BatchDelete', 'Output' ], true);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(that, props, meta);
					props.meta.setMeta(meta);
				}
			}
		}
	);
}

/**
 * 添加操作列
 * @param {Object} props 
 * @param {Object} meta 
 */

function modifierMeta(props, meta) {
	let that = this;

	//在资产编码下添加超链接，链接到卡片页面
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'program_code') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div className="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								toCard.call(that, props, record, 'browse');
							}}
						>
							{record.values.program_code && record.values.program_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
	if (meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		// 处理停启用开关
		meta = createEnableSwitch.call(that, props, { tableId, meta, enableRow, disableRow });
	}

	let material_event = {
		label: getMultiLangByID('amcommon-000000') /*操作*/,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		width: '200px',
		//	fixed: 'right',
		render: (text, record, index) => {
			let recordVal = record.values;
			let status = props.editTable.getStatus(tableId);
			// 组织节点不允许操作集团数据
			let flag =
				props.listpageConfig.nodeType == 'org' &&
				recordVal.pk_group &&
				recordVal.pk_org &&
				recordVal.pk_group.value == recordVal.pk_org.value;
			let buttonAry = [];
			if (!flag) {
				buttonAry = [ 'Edit', 'Delete' ];
			}
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(that, props, key, text, record, index)
			});
		}
	};
	meta[tableId].items.push(material_event);

	return meta;
}
