import React, { Component } from 'react';
import { base, high, cardCache, createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, doubleClick, afterEvent, getData } from './events';
import './index.less';
import { baselistpageConfig } from './const';
import rowSelected from './events/rowSelected';

// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
// import AMRefer from '../../../../ampub/common/components/AMRefer';
import ampub from 'ampub';
const { components, utils } = ampub;
const { AMRefer } = components;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { NCCheckbox, NCAffix } = base;
const { PrintOutput } = high;
let { add, tableId, url } = baselistpageConfig;
let { setDefData } = cardCache;

class ListBase extends Component {
	constructor(props) {
		super(props);
		this.state = {
			returndata: {},
			printData: {},
			pks: new Map(),
			value: '',
			curOrg: {},
			isShowOff: false
		};
		this.hasOrg = false;
		this.dataSource = this.props.listpageConfig.dataSource;
		initTemplate.call(this, props);
	}
	componentDidMount() {}
	showOffChange = () => {
		this.setState(
			{
				isShowOff: !this.state.isShowOff
			},
			() => {
				getData.call(this, this.state.curOrg);
			}
		);
	};

	searchOnChange = () => {
		let value = this.state.value;
		if (value && value.length > 0) {
			let filterDatas = this.state.returndata.rows.filter((row) => {
				return (
					row.values.program_code.value.indexOf(value) != -1 ||
					row.values.program_name.value.indexOf(value) != -1
				);
			});
			this.props.editTable.setTableData(tableId, { rows: filterDatas });
		} else {
			this.props.editTable.setTableData(tableId, this.state.returndata);
		}
	};

	//财务组织选择
	onOrgChange = (value) => {
		this.setState(
			{
				curOrg: value
			},
			() => {
				setDefData('lastOrg', this.dataSource, value);
				getData.call(this, this.state.curOrg);
				//设置控制变量
				this.hasOrg = value.refpk ? true : false;
				//如果没有组织,将新增按钮置灰
				if (this.state.curOrg && this.state.curOrg.refpk) {
					this.props.button.setButtonDisabled(add, false);
				} else {
					this.props.button.setButtonDisabled(add, true);
				}
			}
		);
	};

	//新增前事件
	addbuttonClick(props) {
		if (this.props.listpageConfig.nodeType == 'org') {
			this.props.pk_org = {
				value: this.state.curOrg.refpk,
				display: this.state.curOrg.refname
			};
		}
		//这里我是直接改值之后做参照传递过去的，你直接获取props，得到的只是初始化的proos
		buttonClick(this.props, 'add');
	}

	showOrgPanel = () => {
		//org
		let orgConfig = {
			disabled: status && status == 'edit',
			onChange: this.onOrgChange.bind(this),
			isMultiSelectedEnabled: false,
			defaultValue: this.state.curOrg,
			queryCondition: () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			},
			refcode: 'uapbd/refer/org/FinanceOrgTreeRef/index.js'
		};
		if (this.props.listpageConfig.nodeType == 'group') {
			return;
		} else {
			return (
				<div className="search-box">
					<AMRefer className="title-search-detail" config={orgConfig} />
				</div>
			);
		}
	};
	render() {
		let { button, editTable, ncmodal } = this.props;
		let { createEditTable } = editTable;
		const { createButtonApp } = button;
		let { printData = {} } = this.state; // 需要输出的数据
		let { createModal } = ncmodal;
		return (
			<div className="nc-single-table">
				{/* 头部 header */}
				<NCAffix>
					<div className="nc-singleTable-header-area">
						{/* 标题 title */}
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">
								{getMultiLangByID(this.props.listpageConfig.nodeTypeCN)}
							</h2>
						</div>
						{this.showOrgPanel()}
						{/* 显示停用  showOff*/}
						<div className="title-search-detail">
							<span className="showOff">
								<NCCheckbox
									className="showHideBtn"
									checked={this.state.isShowOff}
									onChange={this.showOffChange}
									disabled={status == 'edit'}
									size="lg"
								>
									{getMultiLangByID('amcommon-000004') /*显示停用*/}
								</NCCheckbox>
							</span>
						</div>

						{/* 按钮区 */}
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.btn-group')
							})}
						</div>
					</div>
				</NCAffix>
				{/* 列表区 */}
				<div className="nc-singleTable-table-area">
					{createEditTable(tableId, {
						showIndex: true,
						showCheck: true,
						onSelected: rowSelected.bind(this), // 左侧选择列单个选择框回调
						onSelectedAll: rowSelected.bind(this), // 左侧选择列全选回调
						onAfterEvent: afterEvent,
						onRowDoubleClick: doubleClick.bind(this)
					})}
				</div>
				{createModal('BatchDelete', {})}
				{/* 输出区 printOutput */}
				<PrintOutput ref="printOutput" url={url.printUrl} data={printData} />
			</div>
		);
	}
}

export default ListBase;
