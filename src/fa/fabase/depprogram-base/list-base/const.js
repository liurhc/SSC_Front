// 页面配置
export const baselistpageConfig = {
	tableId: 'head',
	add: 'Add',
	cardRouter: '/card',
	listRouter: '/list',

	// 请求链接
	url: {
		queryUrl: '/nccloud/fa/depprogram/queryHeadVOs.do',
		printUrl: '/nccloud/fa/depprogram/printList.do',
		batchDeleteUrl:'/nccloud/fa/depprogram/batchdelete.do',
		unsealUrl:'/nccloud/fa/depprogram/unseal.do',
		sealUrl:'/nccloud/fa/depprogram/seal.do',
        deleteUrl:'/nccloud/fa/depprogram/delete.do',
	}
};
