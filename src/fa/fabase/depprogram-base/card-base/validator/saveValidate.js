import { basecardpageConfig } from '../const';
// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default function saveValidate(props) {
	let { formId1 } = basecardpageConfig;
	let validateRes = new Object();
	validateRes.valid = false; //校验是否通过,false是不通过，true是通过
	validateRes.msg = []; //校验信息
	/*
     设值冲突校验
     模拟减值准备和减值准备（本币原值%）不能同时设置，模拟净残值率和模拟净残值不能同时设置
    */
	// 获取模拟减值准备的值
	let sim_predevaluate = props.form.getFormItemsValue(formId1, 'sim_predevaluate').value;
	//获取减值准备（本币原值%）的值
	let sim_predevaluaterate = props.form.getFormItemsValue(formId1, 'sim_predevaluaterate').value;
	if (sim_predevaluate && sim_predevaluaterate) {
		validateRes.msg.push(getMultiLangByID('201200540A-000007')) /*模拟减值准备和减值准备（本币原值%）不能同时设置！*/;
	}

	//获取模拟净残值率
	let sim_salvagerate = props.form.getFormItemsValue(formId1, 'sim_salvagerate').value;
	//获取模拟净残值
	let sim_salvalue = props.form.getFormItemsValue(formId1, 'sim_salvalue').value;
	if (sim_salvagerate && sim_salvalue) {
		validateRes.msg.push(getMultiLangByID('201200540A-000008')) /*模拟净残值率和模拟净残值不能同时设置！*/;
	}

	/*
     减值准备（本币原值%）必须在-100到100之间，且位数限制为3位
    */
	let sim_predevaluaterateInt = parseFloat(sim_predevaluaterate);
	if (sim_predevaluaterateInt > 100 || sim_predevaluaterateInt < -100) {
		validateRes.msg.push(getMultiLangByID('201200540A-000009')) /*减值准备（本币原值%）必须在-100到100之间！*/;
	}

	/*
     表体选择资产类别时不能重复 
    */
	let category = props.cardTable.getColValue('body', 'pk_category');
	let pk_categoryStr = JSON.stringify(category);
	for (let index = 0; index < category.length; index++) {
		let element = category[index].value + '';
		if (!element == '') {
			if (pk_categoryStr.indexOf(element) != pk_categoryStr.lastIndexOf(element)) {
				validateRes.msg.push(getMultiLangByID('201200540A-000010')) /*表体选择资产类别时不能重复！*/;
				break;
			}
		}
	}
	//判断表体类别是录入 pk_category
	for (let categoryindex = 0; categoryindex < category.length; categoryindex++) {
		if (!category[categoryindex].value) {
			validateRes.msg.push(getMultiLangByID('201200540A-000011')) /*表体选择资产类别不能为空！*/;
			break;
		}
	}
	let new_value = props.cardTable.getColValue('body', 'new_value');

	//判断新增资产本币原值 是否录入 new_value
	for (let new_valueindex = 0; new_valueindex < new_value.length; new_valueindex++) {
		if (!new_value[new_valueindex].value) {
			validateRes.msg.push(getMultiLangByID('201200540A-000012')) /*表体选择新增资产本币原值不能为空！*/;
			break;
		}
	}
	if (validateRes.msg.length == 0) {
		validateRes.valid = true;
	}
	return validateRes;
}
