import { basecardpageConfig } from '../const';

// import { orgChangeEvent } from '../../../../../ampub/common/components/OrgChangeUtils/orgChangeEvent';
import ampub from 'ampub';
const { components } = ampub;
const { OrgChangeEvent } = components;
const { orgChangeEvent } = OrgChangeEvent;

const { formId, tableId } = basecardpageConfig;

export default function afterEvent(props, moduleId, key, value, changedrows, i) {
	//将第二个表单的值设置到第一个表单中
	let values = {
		[key]: {
			value: value.value,
			display: (() => {
				return value.display ? value.display : value.value;
			})()
		}
	};
	props.form.setFormItemsValue(formId, values);
}

export function headAfterEvent(props, moduleId, key, value, oldValue, record) {
	if (key == 'pk_org_v') {
		orgChangeEvent.call(
			this,
			props,
			this.props.cardpageConfig.pagecode,
			formId,
			tableId,
			key,
			value,
			oldValue,
			false,
			[ 'AddLine' ],
			() => {
				props.cardTable.setTableData(tableId, { rows: [] }, () => {
					if (value && value.value) {
						props.cardTable.addRow(tableId, undefined, {}, false);
					}
				});
			}
		);
	}
}
