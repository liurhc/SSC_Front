import buttonClick, { backToList, getDataByPk, getData, setStatus } from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent, { headAfterEvent } from './afterEvent';
import tableButtonClick from './tableButtonClick';
import pageInfoClick from './pageInfoClick';

export {
	setStatus,
	buttonClick,
	afterEvent,
	initTemplate,
	tableButtonClick,
	backToList,
	pageInfoClick,
	getDataByPk,
	getData,
	headAfterEvent
};
