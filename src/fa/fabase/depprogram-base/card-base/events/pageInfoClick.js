import { getDataByPk } from './buttonClick';

/**
 * 分页查询
 * @param {*} props 
 * @param {*} pk 
 */
export default function(props, pk) {
	// 更新参数
	props.setUrlParam({ id: pk });
	getDataByPk.call(this, props, pk);
}
