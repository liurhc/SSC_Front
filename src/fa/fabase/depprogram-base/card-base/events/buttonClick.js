import { ajax, toast, cardCache } from 'nc-lightapp-front';
//import { back, del, edit, save, cancel, addLine, add, tableId, formId, formId1 } from '../constant';
import saveValidate from '../validator/saveValidate';
import { basecardpageConfig } from '../const';
import { pageInfoClick } from '.';

// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
// import { setHeadAreaData } from '../../../../../ampub/common/utils/cardUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setHeadAreaData } = cardUtils;

let {
	edit,
	save,
	cancel,
	addLine,
	add,
	tableId,
	formId,
	formId1,
	listRouter,
	cardRouter,
	url,
	addButtons,
	browseButtons,
	editButtons
} = basecardpageConfig;
export default function(props, id) {
	switch (id) {
		case save:
			saveaction.call(this, props);
			break;
		case edit:
			editaction.call(this, props);
			break;
		case cancel:
			let currentStatus = props.getUrlParam('status');
			if (currentStatus == 'add') {
				let cacheData = props.ViewModel.getData(props.cardpageConfig.dataSource);
				let pk_depprogram = cacheData.simpleTable.allpks[cacheData.simpleTable.allpks.length - 1];
				props.setUrlParam({ id: pk_depprogram });
			}
			props.modal.show(cancel, {
				title: getMultiLangByID('msgUtils-000002') /*取消*/,
				content: getMultiLangByID('msgUtils-000003') /*确定要取消吗？*/,
				beSureBtnClick: () => {
					cancelaction.call(this, props);
				}
			});
			break;
		case addLine:
			//表体新增行按钮
			props.cardTable.addRow(tableId);
			break;
		case add:
			addaction.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this);
			break;
		default:
			break;
	}
}

/**
* 返回
* @param {*} props 
*/
export function backToList(props) {
	props.pushTo(listRouter, { pagecode: props.cardpageConfig.pagecode_list });
}
/**
* 保存后
* @param {*} props 
*/
export function afterSave(props, data, oldstatus, pk_depprogram) {
	// 保存成功后处理缓存
	if (oldstatus == 'add') {
		cardCache.addCache(pk_depprogram, data, formId, props.cardpageConfig.dataSource);
		props.ViewModel.getData(props.cardpageConfig.dataSource).simpleTable.allpks.push(pk_depprogram);
		props.setUrlParam({ id: pk_depprogram });
	} else if (oldstatus == 'edit') {
		cardCache.addCache(pk_depprogram, data, formId, props.cardpageConfig.dataSource);
		props.setUrlParam({ id: pk_depprogram });
	}
}
export function getData(callback) {
	let that = this;
	//查询档案详情
	if (that.props.getUrlParam('status') != 'add') {
		//如果不是新增态，从url获取id参数，加上pagid参数传入后台，返回AggVO
		//如果是pk的话
		let data = {
			pk: that.props.getUrlParam('id'),
			pagecode: that.props.cardpageConfig.pagecode
		};
		ajax({
			url: url.queryUrl,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (res.data.head) {
						if (
							res.data.head[formId].rows[0].values.pk_org_v.value == '~' ||
							!res.data.head[formId].rows[0].values.pk_org_v.value
						) {
							res.data.head[formId].rows[0].values.pk_org_v.display =
								res.data.head[formId].rows[0].values.pk_group.display;
						}
						//赋值表单
						that.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						//新增一个form 给以赋值
						that.props.form.setAllFormValue({ [formId1]: res.data.head[formId] });

						that.setState({
							billID: res.data.head.head.rows[0].values.pk_depprogram.value
						});
					}
					if (res.data.body) {
						//赋值表格
						that.props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}
					if (callback && 'function' == typeof callback) {
						callback();
					}
				}
			}
		});
	} else {
		//如果是新增态
		//需要设置默认值 只需要设置启用状态为是即可
		setbasevalue.call(this, props);
	}
	//无论是不是新增状态，取出URL中的org参数，添加到props里面
	if (that.props.cardpageConfig.nodeType == 'org') {
		that.props.cardpageConfig.pk_org = {
			value: that.props.getUrlParam('org')
		};
		//将org的值赋值到表单pk_org字段
		that.props.form.setFormItemsValue(formId, {
			pk_org: {
				value: that.props.getUrlParam('org'),
				display: null
			}
		});
	}
}
/**
* 刷新
* @param {*} props 
*/
export function refresh(isshow = true) {
	getData.call(this, () => {
		if (isshow) {
			toast({ color: 'success', content: getMultiLangByID('msgUtils-000020') /*刷新成功*/ });
		}
	});
}
//保存方法
export function saveaction(props) {
	let that = this;
	//过滤表格空行
	props.cardTable.filterEmptyRows(tableId, [ 'pk_category', 'new_value' ], 'include');
	//校验表单，主要是校验必输项
	let flag = props.form.isCheckNow(formId);
	let flag1 = props.form.isCheckNow(formId1);
	let allRows = props.cardTable.getAllRows(tableId);
	let oldstatus = props.form.getFormStatus(formId);
	// 必输项校验没过
	if (!allRows) {
		return;
	}
	if (!(flag && flag1)) {
		return;
	}
	//新增加一个表单 校验增加
	let validateRes = saveValidate(props);

	if (!validateRes.valid) {
		let toastMsg = '';
		validateRes.msg &&
			validateRes.msg.map((m) => {
				toastMsg += m + '\n';
			});
		toast({
			content: toastMsg,
			color: 'danger'
		});
		return;
	}

	//组装要返回的url和数据
	//创建主子表数据 参数(pageid,headcode,bodycode)
	let data = props.createMasterChildDataSimple(props.cardpageConfig.pagecode, formId, tableId);
	//临时解决平台BUG
	if (data.body.body.rows == false) {
		data.body.body.rows = props.cardTable.getAllRows(tableId);
	}
	//这里如果是number类型的数据的话，返回后台会变成float类型，如果平台不处理的话，这里需要自己手动处理一下
	for (let key in data.head.head.rows[0].values) {
		if (data.head.head.rows[0].values.hasOwnProperty(key)) {
			let element = data.head.head.rows[0].values[key];
			if (typeof element.value == 'number') {
				element.value += '';
			}
		}
	}
	//默认是新增操作
	let posturl = url.insertUrl;
	//如果是编辑态，修改url为更新操作
	if (props.getUrlParam('status') == 'edit') {
		posturl = url.updateUrl;
	}
	//后台操作
	ajax({
		url: posturl,
		data: data,
		success: (res) => {
			let { success, data } = res;
			let pk_depprogram = null;
			if (success) {
				if (res.data) {
					if (res.data.head && res.data.head.head) {
						//更新form数据
						props.form.setAllFormValue({ [formId]: res.data.head.head });
						//增加form 增加数据更新
						props.form.setAllFormValue({ [formId1]: res.data.head.head });
						//如果是传pk的话
						pk_depprogram = res.data.head.head.rows[0].values.pk_depprogram.value;
					}
					if (res.data.body && res.data.body.body) {
						//更新table数据
						props.cardTable.setTableData(tableId, res.data.body.body);
					}
					toast({ content: getMultiLangByID('msgUtils-000014') /*保存成功!*/, color: 'success' });
				}
				setStatus.call(that, props, 'browse', afterSave.call(that, props, data, oldstatus, pk_depprogram));
			}
		}
	});
}

//修改方法
function editaction(props) {
	//浏览态显示，修改按钮，重新加载当前页面，并设置界面状态为编辑态,这个方法也太简单粗暴了吧
	//复杂细腻的方法：修改form状态，修改table状态，修改Url但不跳转，添加操作列，同时取消的时候要删除操作列
	//如果是传pk的话
	let dataa = props.createMasterChildDataSimple(props.pagecode, formId, tableId);
	if (dataa) {
		const pk_org = dataa.head.head.rows[0].values.pk_org.value;
		const pk_group = dataa.head.head.rows[0].values.pk_group && dataa.head.head.rows[0].values.pk_group.value;
		const hasOrg = props.cardpageConfig.nodeType;
		//增加删除判断组织不能修改集团的数据
		if (hasOrg === 'org') {
			if (pk_org === pk_group) {
				let error = getMultiLangByID('201200540A-000004') /*该数据为集团数据组织节点不可以修改！*/;
				toast({ content: error, color: 'danger' });
			} else {
				setStatus.call(this, props, 'edit');
			}
		} else {
			setStatus.call(this, props, 'edit');
		}
	}
}
function cancelaction(props) {
	// 表单返回上一次的值
	props.form.cancel(formId);
	props.form.cancel(formId1);
	// 表格返回上一次的值
	props.cardTable.resetTableData(tableId, () => {
		//重新加载当前页面，并设置状态为浏览态
		setStatus.call(this, props, 'browse', () => {
			if (this.lastStatus == 'add') {
				props.cardTable.filterEmptyRows(tableId);
				getDataByPk.call(this, props, props.getUrlParam('id'));
			} else if (this.lastStatus == 'edit') {
				refresh.call(this, false);
			}
		});
	});
}
//新增方法体
function addaction(props) {
	props.form.EmptyAllFormValue(formId);
	props.form.EmptyAllFormValue(formId1);
	props.cardTable.setTableData(tableId, { rows: [] });
	setStatus.call(this, props, 'add');
	setbasevalue.call(this, props);
}
/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	props.form.EmptyAllFormValue(formId);
	let nulldata = {
		rows: []
	};
	props.cardTable.setTableData(tableId, nulldata);
	if (!data) {
		return;
	}
	if (data.head) {
		props.form.setAllFormValue({ [formId]: data.head[formId] });
		if (data.head[formId] && data.head[formId].rows && data.head[formId].rows[0].values) {
			let bill_code = data.head[formId].rows[0].values.bill_code;
			if (bill_code) {
				this.setState({ bill_code: bill_code.value });
			}
		}
	}
	if (data.body) {
		props.cardTable.setTableData(tableId, data.body[tableId]);
	}

	setBillFlowActionVisible.call(this, props);
}
/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk) {
	if (pk) {
		let data = {
			pk,
			pagecode: this.props.cardpageConfig.pagecode
		};
		ajax({
			url: url.queryUrl,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (res.data.head) {
						if (
							res.data.head[formId].rows[0].values.pk_org_v.value == '~' ||
							!res.data.head[formId].rows[0].values.pk_org_v.value
						) {
							res.data.head[formId].rows[0].values.pk_org_v.display =
								res.data.head[formId].rows[0].values.pk_group.display;
						}
						//赋值表单
						this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						//新增一个form 给以赋值
						this.props.form.setAllFormValue({ [formId1]: res.data.head[formId] });

						this.setState({
							billID: res.data.head.head.rows[0].values.pk_depprogram.value
						});
					}
					if (res.data.body) {
						//赋值表格
						this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
					} else {
						this.props.cardTable.setTableData(tableId, { rows: [] });
					}
				}
			}
		});
	}
}

/**
 * 设置卡片态状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status, callback = () => {}) {
	this.setState({ status }, () => {
		//修改URL参数
		let urlStatus = props.getUrlParam('status');
		this.lastStatus = urlStatus;
		if (urlStatus) {
			props.setUrlParam({ status: status });
		} else {
			props.addUrlParam({ status: status });
		}
		if (status == 'edit' || status == 'add') {
			props.cardTable.addRow(tableId, undefined, {}, false);
		}
		//修改页面状态
		props.form.setFormStatus(formId, status);
		props.form.setFormStatus(formId1, status);
		props.cardTable.setStatus(tableId, status);
		//修改按钮状态
		setButtonVisible(props, status);
		setHeadAreaData.call(this, props, { status });
		callback.call(this);
	});
}

/**
 * 设置按钮状态
 * @param {*} props 
 * @param {*} status 
 */
export function setButtonVisible(props, status) {
	let hidden = [];
	let show = [];
	switch (status) {
		case 'add':
			hidden = addButtons.hiddenButtons;
			show = addButtons.showButtons;
			break;
		case 'edit':
			hidden = editButtons.hiddenButtons;
			show = editButtons.showButtons;
			break;
		case 'browse':
			hidden = browseButtons.hiddenButtons;
			show = browseButtons.showButtons;
			break;
		default:
			break;
	}
	props.button.setButtonVisible(hidden, false);
	props.button.setButtonVisible(show, true);
}
export function setbasevalue(props) {
	let datas = {
		enablestate: {
			display: getMultiLangByID('201200540A-000005') /*是*/,
			value: '2'
		},
		dr: {
			display: '0',
			value: '0'
		},
		pk_org_v: {
			display: this.state.curOrg_v.refname,
			value: this.state.curOrg_v.refpk
		}
	};
	props.form.setFormItemsValue(formId, datas);
}
