import { basecardpageConfig, defaultConfig } from '../const';
import { tableButtonClick, getData, setStatus } from '../events';
import { setbasevalue } from './buttonClick';

// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
// import {loginContext,getContext,loginContextKeys} from '../../../../../ampub/common/components/AMInitInfo/loginContext';
// import { addHeadAreaReferFilter } from '../../../../common/components/ReferFilter';
import ampub from 'ampub';
const { utils, components } = ampub;
const { LoginContext } = components;
const { multiLangUtils } = utils;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { getMultiLangByID } = multiLangUtils;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addHeadAreaReferFilter } = ReferFilter;

let { tableId } = basecardpageConfig;
export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: props.cardpageConfig.pagecode,
			appcode: props.cardpageConfig.appcode
		},
		function(data) {
			if (data) {
				if (data.context) {
					//获取登录上下文信息
					loginContext.call(that, data.context);
					//设置交易类型
					let curOrg_v = {
						refpk: getContext(loginContextKeys.pk_org_v),
						refname: getContext(loginContextKeys.org_v_Name)
					};
					that.setState({ curOrg_v });
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					//主组织处理
					addHeadAreaReferFilter(props, meta, defaultConfig);
					meta = modifierMeta.call(that, props, meta);
					//过滤body
					modifiy(props, meta['body']);
					//过滤headinfo
					modifiyheadinfo(props, meta['headinfo']);
					props.meta.setMeta(meta, () => {});
				}
				if (data.button && data.template) {
					let status = props.getUrlParam('status');
					if (!status) {
						status = 'browse';
					}
					//查询数据
					if (that.props.getUrlParam('pk')) {
						getData.call(that, setStatus.bind(that, props, status));
					} else {
						setbasevalue.call(that, props);
						setStatus.call(that, props, status);
					}
				}
			}
		}
	);
}

function modifiy(props, data) {
	if (props.nodeType == 'org') {
		data['items'].map((item) => {
			if (item.itemtype === 'input') {
				item.initialvalue = {
					value: '',
					display: ''
				};
			}
			if (item.attrcode == 'pk_category') {
				let pk_org = props.getUrlParam('org');
				item.queryCondition = () => {
					return {
						pk_org: pk_org
					}; // 根据组织过滤
				};
			}
		});
	}
}

function modifiyheadinfo(props, data) {
	data['items'].map((item) => {
		//过滤折旧方法中的工作量法
		if (item.attrcode == 'pk_depmethod') {
			item.queryCondition = () => {
				return {
					GridRefActionExt: 'nccloud.web.fa.fabase.depprogram.refcondition.DepProgramDepmthodRefFilter',
					//过滤掉工作量法
					notdisplaybycode: '04'
				}; // 根据组织过滤
			};
		}
	});
}

function modifierMeta(props, meta) {
	let that = this;
	let material_event = {
		label: getMultiLangByID('amcommon-000000') /*操作*/,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		width: '120px',
		//fixed: 'right',
		render: (text, record, index) => {
			let status = props.cardTable.getStatus(tableId);
			let buttonAry;
			if (status === 'browse') {
				buttonAry = [];
			} else {
				buttonAry = [ 'DelLine' ];
			}
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(that, props, key, text, record, index),
				popContainer: document.querySelector('.header-button-area')
			});
		}
	};
	meta[tableId].items.push(material_event);

	return meta;
}
