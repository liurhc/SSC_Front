import React, { Component } from 'react';
import { ajax, toast, high, base, createPage } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, backToList, pageInfoClick, getData, headAfterEvent } from './events';
import './index.less';
import { basecardpageConfig } from './const';
const { NCBackBtn, NCAffix } = base;
const { PrintOutput } = high;
let { back, del, edit, save, cancel, addLine, add, tableId, formId, formId1, listRouter } = basecardpageConfig;

// import { createCardTitleArea } from '../../../../ampub/common/utils/cardUtils';
// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea } = cardUtils;

class CardBase extends Component {
	constructor(props) {
		super(props);
		this.state = {
			printData: {},
			billID: '',
			curOrg_v: {},
			status: ''
		};
		this.lastStatus = 'browse';
		window.onbeforeunload = () => {
			if (this.state.status == 'edit' || this.state.status == 'add') {
				return '';
			}
		};
		initTemplate.call(this, props);
		this.dataSource = this.props.cardpageConfig.dataSource;
	}

	componentDidMount() {
		//调用da
		//	 getData.call(this);
	}
	//删除单据，用于模态框点击确定的事件绑定
	delConfirm = () => {
		let data = {
			//取得表单ok值
			pk: this.props.form.getFormItemsValue(formId, 'pk_depprogram').value,
			//组织
			pk_org: this.props.form.getFormItemsValue(formId, 'pk_org_v').value,
			//nodeType,如果是组织返回org
			nodeType: this.props.cardpageConfig.nodeType,
			//nodeCode：功能节点编号，便于区分集团还是组织
			nodeCode: this.props.cardpageConfig.nodeCode
		};
		ajax({
			url: '/nccloud/fa/depprogram/delete.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//提示用户
					toast({ content: getMultiLangByID('msgUtils-000015') /*国际化处理：删除成功*/, color: 'success' });
					//返回列表态

					props.pushTo(listRouter, { pagecode: props.cardpageConfig.pagecode_list });
				}
			}
		});
	};

	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 1,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};
	render() {
		let { form, button, cardPagination, cardTable, ncmodal } = this.props;
		let { createForm } = form;
		const { createCardPagination } = cardPagination;
		let { createCardTable } = cardTable;
		let { createModal } = ncmodal;
		const { createButtonApp } = button;
		let { status } = this.state; // 需要输出的数据
		return (
			<div className="nc-bill-card" id="fa-fabase-depprogram">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								{createCardTitleArea.call(this, this.props, {
									title: getMultiLangByID(this.props.cardpageConfig.nodeTypeCN),
									formId,
									backBtnClick: backToList.bind(this)
								})}
							</div>
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.btn-group')
								})}
							</div>
							{status == 'browse' && (
								<div className="header-cardPagination-area">
									{createCardPagination({
										handlePageInfoChange: pageInfoClick.bind(this),
										dataSource: this.props.cardpageConfig.dataSource
									})}
								</div>
							)}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area depprogram-card">
						<p className="text-info">{getMultiLangByID('201200540A-000002') /*国际化处理：方案信息*/}</p>
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
					<div className="nc-bill-form-area depprogram-card">
						<p className="text-dep">{getMultiLangByID('201200540A-000003') /*国际化处理：模拟折旧计算项目*/}</p>
						{createForm(formId1, {
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</div>

				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							onAfterEvent: afterEvent.bind(this),
							tableHead: this.getTableHead.bind(this)
						})}
					</div>
					{createModal('DelLine', {})}
					{createModal(cancel, {})}
				</div>
			</div>
		);
	}
}
// const CardBase = createPage({})(Card);

export default CardBase;
