// 页面配置
export const basecardpageConfig = {
	tableId: 'body',
	formId: 'head',
	formId1: 'headinfo',
	save: 'Save',
	cancel: 'Cancel',
	addLine: 'AddLine',
	del: 'Del',
	edit: 'Edit',
	back: 'Back',
	add: 'Add',
	listRouter: '/list',
	cardRouter: '/card',
	//按钮
	addButtons: {
		hiddenButtons: [ 'Edit', 'Add', 'Refresh' ],
		showButtons: [ 'Save', 'Cancel', 'AddLine', 'DeleteLine' ]
	},
	browseButtons: {
		hiddenButtons: [ 'Save', 'AddLine', 'DeleteLine', 'Cancel' ],
		showButtons: [ 'Add', 'Edit', 'Refresh' ]
	},
	editButtons: {
		hiddenButtons: [ 'Add', 'Edit', 'Refresh' ],
		showButtons: [ 'Save', 'Cancel', 'AddLine', 'DeleteLine' ]
	},
	// 请求链接
	url: {
		queryUrl: '/nccloud/fa/depprogram/queryAggVO.do',
		insertUrl: '/nccloud/fa/depprogram/insert.do',
		updateUrl: '/nccloud/fa/depprogram/update.do'
	}
};

export const defaultConfig = {
	searchId: '',
	formId: 'head',
	bodyIds: '',
	fieldMap: {
		pk_org: 'pk_org', //财务组织
		pk_accbook: 'pk_accbook', //账簿
		pk_mandept: 'pk_mandept', //管理部门
		//或者 pk_usedept: 'pk_usedept'
		pk_usedept: [ 'pk_usedept', 'pk_dept' ], //使用部门
		pk_assetuser: 'pk_assetuser', //使用人
		pk_recorder: 'pk_recorder', //经办人
		pk_ownerorg: 'pk_ownerorg' //货主管理组织
	}
};
