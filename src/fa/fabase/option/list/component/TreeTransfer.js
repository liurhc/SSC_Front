import React, { Component } from 'react';
import { high } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { Transfer } = high;
//平台提供的穿梭组件
export default class TreeTransfer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dataSource: [],
			targetKeys: []
		};
	}
	componentWillReceiveProps(nextProps) {
		let disabled = nextProps.disabled == false ? false : true;
		nextProps.dataSource = nextProps.dataSource.map((data) => {
			return {
				...data,
				disabled: disabled
			};
		});
		this.setState({
			dataSource: nextProps.dataSource,
			targetKeys: nextProps.targetKeys
		});
	}
	render() {
		const { dataSource, targetKeys } = this.state;
		const transferProps = {
			dataSource,
			targetKeys,
			rowKey: 'key', // 和默认值相同时可以不指定
			rowTitle: 'title',
			type: 'tree',
			titles: [ getMultiLangByID('201200544A-000009') /*待选字段*/, getMultiLangByID('201200544A-000010') /*已选字段*/ ],
			onTargetKeysChange: this.props.onTargetKeysChange,
			className: 'my-transfer-demo',
			showMoveBtn: true,
			disable: true,

			listRender: ({ title }) => title
		};
		return <Transfer {...transferProps} />;
	}
}
