import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
import {
	initTemplate,
	afterEvent,
	changeOption,
	depOption,
	afterChangeStyle,
	afterChangeValue,
	onTargetKeysChange,
	cateEndChange,
	cateStartChange,
	usingEndChange,
	usingStartChange,
	addStartChange,
	addEndChange,
	getDepCheckboxs,
	getChangeRadiogroup,
	buttionClick
} from './events';
import { TreeTransfer } from './component';
import './index.less';
import { optionConfig } from './const';

import ampub from 'ampub';
const { components, utils } = ampub;
const { AMRefer } = components;
const { multiLangUtils } = utils;
const { initMultiLangByModule, getMultiLangByID } = multiLangUtils;

const { NCRadio, NCCheckbox, NCScrollElement, NCScrollLink, NCFormControl, NCAffix, NCPopover } = base;
const { pageConfig, scrollLinkName, initState } = optionConfig;
/**
 * 页面入口文件
 */
class App extends Component {
	constructor(props) {
		super(props);
		this.state = JSON.parse(JSON.stringify(initState));
		//参数
		this.parameter = {
			isDrill: false, //是否是联查跳转来的
			isBatch: false, //是否是批改
			isEdit: false, //页面是否是编辑态
			isFipMakeVoucher: false //单个组织修改时，是否已经折旧制单
		};

		this.orgAccbookDataCache = {}; //单个组织账簿修改的时候缓存数据 pk_orgpk_accbook:data的形式存储,如果是默认主账簿则存两份，另一份是：pk_org:data
		this.orgAccbookPkCache = ''; //单个组织账簿修改的时候缓存数据 主键缓存，快速判断是否有缓存，形式：@@pk1@@pk2@@,如果是默认账簿，只存pkorg，如果是组织加账簿存pk_orgpk_accbook
		//折旧设置相关方法
		this.depFun = {
			depOption: depOption.bind(this),
			getDepCheckboxs: getDepCheckboxs.bind(this)
		};
		//变动设置相关方法
		this.changeFun = {
			changeOption: changeOption.bind(this),
			getChangeRadiogroup: getChangeRadiogroup.bind(this)
		};
		//凭证生成口径相关方法
		this.voucherFun = {
			afterChangeStyle: afterChangeStyle.bind(this),
			afterChangeValue: afterChangeValue.bind(this)
		};
		//折旧分摊相关方法
		this.gatherFun = {
			onTargetKeysChange: onTargetKeysChange.bind(this),
			cateEndChange: cateEndChange.bind(this),
			cateStartChange: cateStartChange.bind(this),
			usingEndChange: usingEndChange.bind(this),
			usingStartChange: usingStartChange.bind(this),
			addStartChange: addStartChange.bind(this),
			addEndChange: addEndChange.bind(this)
		};

		window.onbeforeunload = () => {
			if (this.parameter.isEdit) {
				return '';
			}
		};

		//初始化页面
		initTemplate.call(this, props);
	}

	componentWillMount() {
		//加载多语
		this.multiTitle = {
			pageTitle: getMultiLangByID('201200544A-000043') /*固定资产参数*/,
			depTitle: getMultiLangByID('201200544A-000005') /*折旧设置*/,
			changeTitle: getMultiLangByID('201200544A-000006') /*变动设置*/,
			gatherTitle: getMultiLangByID('201200544A-000007') /*折旧分摊*/,
			cateLevelTitle: getMultiLangByID('201200544A-000044') /*资产类别级次：*/,
			usingstatusTitle: getMultiLangByID('201200544A-000046') /*使用状况级次：*/,
			addLevelTitle: getMultiLangByID('201200544A-000047') /*增减方式级次：*/,
			gatherTip: getMultiLangByID('201200544A-000048') /*注：整数，0或空代表明细级*/,
			popoverContent: getMultiLangByID('201200544A-000045') /*结束级次不能小于开始级次*/,
			voucherTitle: getMultiLangByID('201200544A-000049') /*凭证生成口径*/,
			defaultStyle: getMultiLangByID('201200544A-000050') /*折旧摊销只生成一张凭证（默认方式）*/,
			cateStyle: getMultiLangByID('201200544A-000051') /*按资产类别生成凭证：*/,
			cateholder: getMultiLangByID('201200544A-000054') /*资产类别*/,
			usedeptStyle: getMultiLangByID('201200544A-000052') /*按使用部门生成凭证：*/,
			usedeptholder: getMultiLangByID('201200544A-000055') /*使用部门*/,
			mandeptStyle: getMultiLangByID('201200544A-000053') /*按管理部门生成凭证：*/,
			mandeptholder: getMultiLangByID('201200544A-000056') /*管理部门*/
		};
	}

	//参照过滤方法
	queryCondition = () => {
		let pk_org = this.props.search.getSearchValByField(pageConfig.searchId, 'pk_org');
		let org = pk_org && pk_org.value && pk_org.value.firstvalue ? pk_org.value.firstvalue : ''; //用于参照过滤
		return { pk_org: org };
	};

	//页面加载方法
	render() {
		let { button, search, ncmodal } = this.props;
		let { createModal } = ncmodal;
		let { createButtonApp } = button;
		let { NCCreateSearch } = search;
		let { option, gather, voucher, scroll } = this.state;
		let {
			categoryLevelStart,
			categoryLevelEnd,
			usingstatusLevelStart,
			usingstatusLevelEnd,
			addreducestyleLevelStart,
			addreducestyleLevelEnd,
			targetKeys,
			dataSource,
			isPopoverShow
		} = gather;
		let { catValue, deptValue, manageValue } = voucher.voucherValue;
		let { isBatch, isEdit } = this.parameter;
		let { scrollLink } = scroll; //楼层组件参数
		let { gatherFun, voucherFun, changeFun, depFun } = this; //方法参数
		let checkboxs = depFun.getDepCheckboxs(option.optionValue);
		let settingChange = changeFun.getChangeRadiogroup(option.optionValue, isEdit);
		let {
			pageTitle,
			depTitle,
			changeTitle,
			gatherTitle,
			cateLevelTitle,
			usingstatusTitle,
			addLevelTitle,
			gatherTip,
			popoverContent,
			voucherTitle,
			defaultStyle,
			cateStyle,
			cateholder,
			usedeptStyle,
			usedeptholder,
			mandeptStyle,
			mandeptholder
		} = this.multiTitle;

		return (
			<div className="nc-bill-list option-box">
				{/* 按钮和查询去 */}
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{pageTitle}</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttionClick.bind(this)
							})}
						</div>
					</div>
					<div className="nc-bill-search-area option-search-area">
						{NCCreateSearch(pageConfig.searchId, {
							showAdvBtn: false,
							onAfterEvent: afterEvent.bind(this),
							hideBtnArea: true
						})}
					</div>
				</NCAffix>
				<div className="nc-bill-table-area option-info-area">
					{/* 左侧楼层组件 */}
					<div className="test-wrapper">
						{scrollLinkName.map((item, index) => {
							return (
								<NCScrollLink to={index} spy={true} smooth={true} duration={300} offset={-200}>
									<div
										className={`scrollitem ${scrollLink == index ? 'actived' : ''}`}
										onClick={() => {
											this.setState({ scroll: { scrollLink: index } });
										}}
									>
										<p>{getMultiLangByID(item)}</p>
									</div>
								</NCScrollLink>
							);
						})}
					</div>
					{/* 右侧内容区 */}
					<div className="scrollElement">
						{/* 折旧设置 */}
						<NCScrollElement name="0">
							<div className="dep-setting">
								<p className="setting-title">{depTitle}</p>
								<div className="demo-checkbox">
									{checkboxs.map((item) => {
										if (item.key != 'dep_flag') {
											return (
												<NCCheckbox
													disabled={!isEdit /*是否禁用，如果是编辑态启用*/}
													onChange={() => depFun.depOption(item.key)}
													checked={item.checked}
												>
													{item.title}
												</NCCheckbox>
											);
										}
									})}
								</div>
							</div>
						</NCScrollElement>
						{/* 变动设置 */}
						<NCScrollElement name="1">
							<div className="change-setting">
								<p className="setting-title">{changeTitle}</p>
								<div>
									{settingChange.map(({ title, name, selectedValue, children }) => {
										return (
											<div className="change-setting-list">
												<p>{title}</p>
												<NCRadio.NCRadioGroup
													name={name}
													selectedValue={selectedValue}
													onChange={() => changeFun.changeOption(selectedValue, name)}
												>
													{children.map((child) => {
														return (
															<NCRadio value={child.value} disabled={child.disabled}>
																{child.title}
															</NCRadio>
														);
													})}
												</NCRadio.NCRadioGroup>
											</div>
										);
									})}
								</div>
							</div>
						</NCScrollElement>
						{/* 折旧分摊 */}
						<NCScrollElement name="2">
							<div className="dep-apportion">
								<p className="setting-title">{gatherTitle}</p>
								<div>
									{/* 穿梭组件 */}
									<TreeTransfer
										dataSource={dataSource}
										targetKeys={targetKeys}
										disabled={!isEdit}
										onTargetKeysChange={(targetKeys) => gatherFun.onTargetKeysChange(targetKeys)}
									/>
									{/* 级次信息 */}
									<div className="level-area">
										<div>
											<span>{cateLevelTitle}</span>
											<NCFormControl
												className="level-input"
												value={categoryLevelStart}
												min="0"
												disabled={
													!targetKeys.some((item) => {
														return item === 'pk_category';
													}) || !isEdit
												}
												onChange={(e) => gatherFun.cateStartChange(e)}
											/>
											{/* -
											<NCPopover
												show={this.state.isPopoverShow.category}
												placement="top"
												content={popoverContent}
											{/*}>
												<NCFormControl
													className="level-input"
													value={categoryLevelEnd}
													min="0"
													disabled={
														!targetKeys.some((item) => {
															return item === 'pk_category';
														}) || !isEdit
													}
													onChange={(e) => gatherFun.cateEndChange(e)}
												/>
											</NCPopover> */}
										</div>
										<div>
											<span>{usingstatusTitle}</span>
											<NCFormControl
												className="level-input"
												value={usingstatusLevelStart}
												min="0"
												disabled={
													!targetKeys.some((item) => {
														return item === 'pk_usingstatus';
													}) || !isEdit
												}
												onChange={(e) => gatherFun.usingStartChange(e)}
											/>
											{/* -
											<NCPopover
												show={this.state.isPopoverShow.usingstatus}
												placement="top"
												content={popoverContent}
											{/*}>
												<NCFormControl
													className="level-input"
													value={usingstatusLevelEnd}
													min="0"
													disabled={
														!targetKeys.some((item) => {
															return item === 'pk_usingstatus';
														}) || !isEdit
													}
													onChange={(e) => gatherFun.usingEndChange(e)}
												/>
											</NCPopover> */}
										</div>
										<div>
											<span>{addLevelTitle}</span>
											<NCFormControl
												className="level-input"
												value={addreducestyleLevelStart}
												min="0"
												disabled={
													!targetKeys.some((item) => {
														return item === 'pk_addreducestyle';
													}) || !isEdit
												}
												onChange={(e) => gatherFun.addStartChange(e)}
											/>
											{/* -
											<NCPopover
												show={this.state.isPopoverShow.addreducestyle}
												placement="top"
												content={popoverContent}
											{/*}>
												<NCFormControl
													className="level-input"
													value={addreducestyleLevelEnd}
													min="0"
													disabled={
														!targetKeys.some((item) => {
															return item === 'pk_addreducestyle';
														}) || !isEdit
													}
													onChange={(e) => gatherFun.addEndChange(e)}
												/>
											</NCPopover> */}
										</div>
									</div>

									<p className="pColor">{gatherTip}</p>
								</div>
							</div>
						</NCScrollElement>
						{/* 凭证生成口径 */}
						<NCScrollElement name="3">
							<div className="caliber">
								<p className="setting-title">{voucherTitle}</p>
								<NCRadio.NCRadioGroup
									name="itemgroup"
									selectedValue={voucher.selectedVoucher /*选中方式*/}
									onChange={(value) => voucherFun.afterChangeStyle(value) /*更改选中之后调用的方法 */}
								>
									<div>
										<NCRadio value="default" disabled={!isEdit || isBatch}>
											{defaultStyle}
										</NCRadio>
									</div>
									<div className="proof-list">
										<div>
											<NCRadio value="catValue" disabled={!isEdit || isBatch}>
												{cateStyle}
											</NCRadio>
										</div>
										<div>
											<AMRefer
												className="title-search-detail"
												config={{
													placeholder: cateholder,
													disabled:
														!isEdit ||
														(voucher.selectedVoucher === 'catValue' ? false : true),
													onChange: (value) => {
														voucherFun.afterChangeValue(value);
													},
													isMultiSelectedEnabled: true,
													defaultValue: catValue,
													onlyLeafCanSelect: false,
													queryCondition: this.queryCondition,
													refcode: 'fa/refer/fabase/category/index.js'
												}}
											/>
										</div>
									</div>
									<div className="proof-list">
										<div>
											<NCRadio value="deptValue" disabled={!isEdit || isBatch}>
												{usedeptStyle}
											</NCRadio>
										</div>
										<div>
											<AMRefer
												className="title-search-detail"
												config={{
													placeholder: usedeptholder,
													disabled:
														!isEdit ||
														(voucher.selectedVoucher === 'deptValue' ? false : true),
													onChange: (value) => {
														voucherFun.afterChangeValue(value);
													},
													isMultiSelectedEnabled: true,
													defaultValue: deptValue,
													queryCondition: this.queryCondition,
													attrcode: 'pk_usedept',
													moduleId: 'pk_usedept',
													refcode: 'uapbd/refer/org/DeptTreeRef/index.js'
												}}
											/>
										</div>
									</div>
									<div className="proof-list">
										<div>
											<NCRadio value="manageValue" disabled={!isEdit || isBatch}>
												{mandeptStyle}
											</NCRadio>
										</div>
										<div>
											<AMRefer
												className="title-search-detail"
												config={{
													placeholder: mandeptholder,
													disabled:
														!isEdit ||
														(voucher.selectedVoucher === 'manageValue' ? false : true),
													onChange: (value) => {
														voucherFun.afterChangeValue(value);
													},
													isMultiSelectedEnabled: true,
													defaultValue: manageValue,
													queryCondition: this.queryCondition,
													attrcode: 'pk_mandept',
													moduleId: 'pk_mandept',
													refcode: 'uapbd/refer/org/DeptTreeRef/index.js'
												}}
											/>
										</div>
									</div>
								</NCRadio.NCRadioGroup>
							</div>
						</NCScrollElement>
					</div>
				</div>
				{createModal(`201200544A-confirm`, {})}
			</div>
		);
	}
}

App = createPage({})(App);
initMultiLangByModule({ fa: [ '201200544A' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<App />, document.querySelector('#app'));
});
