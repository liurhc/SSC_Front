import { gatheritemChange } from '.';
import { toast } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
//折旧分摊相关方法

/**
 * 穿梭组件从待选字段到已选字段的方法
 */
export function onTargetKeysChange(targetKeys) {
	//穿梭组件做多右边可以移动7个
	if (targetKeys.length > 6) {
		let delsum = targetKeys.length - 6;
		targetKeys.splice(6, delsum);
		let error = getMultiLangByID('201200544A-000026') /*已选字段数量最大为6个！*/;
		toast({ content: error, color: 'warning' });
		return;
	}
	gatheritemChange.call(this, targetKeys);
}
/**资产类别级次开始*/
export function cateStartChange(e) {
	levelChangeBase.call(this, 'category', true, e);
}
/**资产类别级次结束*/
export function cateEndChange(e) {
	levelChangeBase.call(this, 'category', false, e);
}
/**使用状况级次开始*/
export function usingStartChange(e) {
	levelChangeBase.call(this, 'usingstatus', true, e);
}
/**使用状况级次结束*/
export function usingEndChange(e, type) {
	levelChangeBase.call(this, 'usingstatus', false, e);
}
/**增减方式开始级次*/
export function addStartChange(e) {
	levelChangeBase.call(this, 'addreducestyle', true, e);
}
/**增减方式结束级次*/
export function addEndChange(e) {
	levelChangeBase.call(this, 'addreducestyle', false, e);
}
/**
 * 级次控制基础方法，只有当结束级次大于等于开始级次的时候不显示提示框,
 * update by dongzheng9 20181119 由于后台只支持一个级次，这里要做修改，留下开始级次，隐藏结束级次，提示框不显示，
 * 先将代码注释，如果以后支持两个级次，可将注释放开
*/
export function levelChangeBase(changeType, isStart, value) {
	//只有是正整数或者为空的时候执行
	let reg = /^\d+$/; //正则表达，正整数
	if ((reg.test(value) && value >= 0 && value <= 2147483647) || !value) {
		let startField = `${changeType}LevelStart`; //开始级次字段编码
		let endField = `${changeType}LevelEnd`; //结束级次字段编码
		let isPopoverShow = this.state.gather.isPopoverShow; //是否显示‘开始级次不能大于结束级次’提示框
		let startValue = ''; //界面上开始级次的值
		let endValue = ''; //界面上结束级次的值
		let setStart = ''; //设置到state中的开始值
		let setEnd = ''; //设置到state中的结束值
		if (isStart) {
			//如果是开始级次的话
			startValue = value; //开始值等于传入值
			endValue = this.state.gather[endField]; //结束值从state中取
		} else {
			//如果是结束级次的话
			startValue = this.state.gather[startField]; //开始值从state中取
			endValue = value; //结束值等于传入值
		}
		//初始化要设置到state中的开始值和结束值
		setStart = startValue;
		setEnd = endValue;
		//以下是判断开始级次的值，决定是否显示提示框
		// if (!endValue) {
		// 	//如果结束值不存在
		// 	if (isStart) {
		// 		//如果是开始级次修改事件，不用提示，设置结束级次与开始级次相等
		// 		isPopoverShow[changeType] = false;
		// 		setEnd = startValue;
		// 	} else {
		// 		//如果是结束级次修改事件
		// 		if (!startValue) {
		// 			//如果开始值也不存在，不需要提示
		// 			isPopoverShow[changeType] = false;
		// 		} else {
		// 			//如果开始值存在，需要提示
		// 			isPopoverShow[changeType] = true;
		// 		}
		// 	}
		// } else {
		// 	//如果开始值存在
		// 	if (startValue) {
		// 		//如果开始值也存在，判断大小
		// 		if (endValue - startValue < 0) {
		// 			//如果结束级次存在，且结束级次小于开始级次，提示
		// 			isPopoverShow[changeType] = true;
		// 		} else {
		// 			//如果结束级次存在，且结束级次大于开始级次，不提示
		// 			isPopoverShow[changeType] = false;
		// 		}
		// 	} else {
		// 		//如果开始值不存在，不需要提示
		// 		isPopoverShow[changeType] = false;
		// 	}
		// }
		//设置state的值，开始级次、结束级次、是否显示提示框
		//update by dongzheng9 20181119 由于只剩一个级次框，将结束级次赋值为开始级次，不修改提示框变量

		setStart = setStart ? parseInt(setStart).toString() : ''; //转为整数，去除类似01的0
		setEnd = setEnd ? parseInt(setEnd).toString() : '';
		this.setState({
			gather: {
				...this.state.gather,
				[startField]: setStart,
				[endField]: setStart
				// isPopoverShow
			}
		});
	}
}
