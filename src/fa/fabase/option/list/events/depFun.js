import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

/**
 * 折旧设置
 */
export default function depOption(key) {
	switch (key) {
		case 'servicemonth_flag':
			let servicemonth_flag = this.state.option.optionValue.servicemonth_flag;
			let data = Object.assign({}, this.state.option.optionValue, {
				servicemonth_flag: { value: !servicemonth_flag.value }
			});
			this.setState({ option: { optionValue: data } });
			break;
		case 'depmethod_flag':
			let depmethod_flag = this.state.option.optionValue.depmethod_flag;
			let depdata = Object.assign({}, this.state.option.optionValue, {
				depmethod_flag: { value: !depmethod_flag.value }
			});
			this.setState({ option: { optionValue: depdata } });
			break;
		case 'newusingstatus':
			let newusingstatus = this.state.option.optionValue.newusingstatus;
			let usedata = Object.assign({}, this.state.option.optionValue, {
				newusingstatus: { value: !newusingstatus.value }
			});
			this.setState({ option: { optionValue: usedata } });
			break;
		case 'workloan_flag':
			let workloan_flag = this.state.option.optionValue.workloan_flag;
			let workdata = Object.assign({}, this.state.option.optionValue, {
				workloan_flag: { value: !workloan_flag.value }
			});
			this.setState({ option: { optionValue: workdata } });
			break;
		case 'dep_flag':
			let dep_flag = this.state.option.optionValue.dep_flag;
			let dedata = Object.assign({}, this.state.option.optionValue, { dep_flag: { value: !dep_flag.value } });
			this.setState({ option: { optionValue: dedata } });

			break;
		case 'depall_flag':
			let depall_flag = this.state.option.optionValue.depall_flag;
			let dpldata = Object.assign({}, this.state.option.optionValue, {
				depall_flag: { value: !depall_flag.value }
			});
			this.setState({ option: { optionValue: dpldata } });
			break;
		default:
			break;
	}
}

/**
 * 获取折旧设置界面信息
 * @param {*} params 
 */
export function getDepCheckboxs(optionValue) {
	let { servicemonth_flag, depmethod_flag, newusingstatus, workloan_flag, dep_flag, depall_flag } = optionValue; //折旧设置参数
	let checkboxs = [
		//折旧设置
		{
			title: getMultiLangByID('201200544A-000027') /*使用月限调整当期有效*/,
			checked: servicemonth_flag.value,
			key: 'servicemonth_flag'
		},
		{
			title: getMultiLangByID('201200544A-000028') /*折旧方法调整当期有效*/,
			checked: depmethod_flag.value,
			key: 'depmethod_flag'
		},
		{
			title: getMultiLangByID('201200544A-000029') /*使用状况调整当期有效*/,
			checked: newusingstatus.value,
			key: 'newusingstatus'
		},
		{
			title: getMultiLangByID('201200544A-000030') /*工作量调整当期有效*/,
			checked: workloan_flag.value,
			key: 'workloan_flag'
		},
		{ title: getMultiLangByID('201200544A-000031') /*折旧方法当期有效*/, checked: dep_flag.value, key: 'dep_flag' },
		{
			title: getMultiLangByID('201200544A-000032') /*使用期限的最后一期折旧提足*/,
			checked: depall_flag.value,
			key: 'depall_flag'
		}
	];
	return checkboxs;
}
