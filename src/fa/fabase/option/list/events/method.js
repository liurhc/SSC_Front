import { ajax, toast } from 'nc-lightapp-front';
import { optionConfig } from '../const';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

/**
 * 与后台交互基础方法
 * @param {*} props 
 * @param {*} url 地址
 * @param {*} data 数据
 * @param {*} success 成功后回调事件
 * @param {*} error 失败后回调事件，默认弹出提示框
 */
const { pageConfig } = optionConfig;
const { buttonCode } = pageConfig;
export async function commonAjax(props, url, data, error) {
	let that = this;
	function defalutError(res) {
		if (res && res.message) {
			toast({ color: 'danger', content: res.message }); //提示错误信息
		}
	}
	return new Promise((reslove, reject) => {
		ajax({
			url: url,
			data: data,
			success: (res) => {
				reslove.call(that, res);
			},
			error: error || defalutError
		});
	});
}

/**
 * 批量toast
 * @param {*} content 显示内容 
 * @param {*} msg 展开显示内容，数组
 * @param {*} color 颜色 默认waring
 * @param {*} time 超时时间 默认不超时
 */
export function groupToast(content = '', msg = [], color = 'danger', time = 'infinity') {
	content = getMultiLangByID(content); //处理多语
	let toastMsg = '';
	msg &&
		msg.map((m) => {
			toastMsg += getMultiLangByID(m) + '\n';
		}); //处理多语
	toast({
		content: toastMsg,
		duration: time, //消失时间，默认是3秒; 值为 infinity 时不消失,非必输
		color: color
	});
}
/**
 * 设置查询区字段是否禁用
 * @param {*} props
 * @param {*} searchId 区域ID
 * @param {*} fields 字段编码数组
 * @param {*} isDisable 是否禁用 默认是true
 */
export function setSearchAreaFieldDisable(props, searchId, fields = [], isDisable = true) {
	if (props && searchId) {
		fields &&
			fields.map((field) => {
				props.search.setDisabledByField(searchId, field, isDisable);
			});
	}
}
/**
 * 根据预先定义好的方案设置按钮显隐性
 * @param {*} props 
 * @param {*} code 方案编码
 */
export function setButtonsVisibleByCode(props, code) {
	if (props && code) {
		let visibleFlags = {};
		switch (code) {
			case 'init':
			case 'cancel':
				visibleFlags[buttonCode.edit] = true;
				visibleFlags[buttonCode.batch] = true;
				visibleFlags[buttonCode.save] = false;
				visibleFlags[buttonCode.cancel] = false;
				break;
			case 'edit':
			case 'batch':
			case 'drill':
				visibleFlags[buttonCode.edit] = false;
				visibleFlags[buttonCode.batch] = false;
				visibleFlags[buttonCode.save] = true;
				visibleFlags[buttonCode.cancel] = true;
				break;
			default:
				break;
		}
		props.button.setButtonsVisible(visibleFlags);
	}
}

/**
 * 查询组织对应的数据
 * 缓存分为组织账簿缓存(以组织账簿主键为key)，组织缓存(以组织主键为key)，如果存在多账簿，需要判断组织缓存是否存在且账簿是否相同
 * @param {Object} props 
 * @param {String} pk_org 
 * @param {String} pk_accbook 
 * @param {boolean} forceSearch 是否强制查询
 */
export async function queryOrgData(props, pk_org, pk_accbook, forceSearch = true) {
	//Todo 参数设置与结账页面同时操作，缓存会有负面影响，导致数据更新不及时，暂时将强制查询置为true；
	if (pk_org) {
		let orgData = false;
		let orgAccbookPkCache = this.orgAccbookPkCache; //缓存key列表
		let orgAccbookDataCache = this.orgAccbookDataCache; //缓存数据集合
		let pk = pk_org;
		if (pk_accbook) {
			//有账簿条件传入，优先读取组织账簿缓存，如果没有读取组织缓存，判断账簿是否相同
			pk += pk_accbook;
			if (orgAccbookPkCache.includes(pk)) {
				orgData = orgAccbookDataCache[pk]; //如果有组织和账簿的缓存，读取缓存
			} else if (orgAccbookPkCache.includes(pk_org)) {
				//如果有组织缓存，读取组织缓存，并确认组织缓存中的账簿是否为传入账簿，如果不是需要重新查询
				let tempOrgData = orgAccbookDataCache[pk_org];
				//如果是多账簿查询，有可能只存在组织主账簿缓存，不存在组织缓存，所以需要判断缓存是否存在才能进行后续操作
				if (tempOrgData) {
					let backbook = tempOrgData.pk_accbook.value;
					if (pk_accbook === backbook) {
						orgData = tempOrgData;
						this.orgAccbookDataCache[pk] = orgData; //存入缓存
						this.orgAccbookPkCache += `@@${pk}`; //缓存key
					}
				}
			}
		} else {
			//没有账簿条件传入，读取组织缓存
			if (orgAccbookPkCache.includes(pk)) {
				orgData = orgAccbookDataCache[pk]; //如果有组织的缓存，读取缓存
			}
		}
		if (!orgData || forceSearch) {
			//如果没有缓存数据，进行后台查询
			let data = { pk_org, pk_accbook };
			let res = await commonAjax.call(this, props, pageConfig.url.querydateUrl, data);
			if (res && res.data) {
				orgData = res.data;
				//这里可以直接赋值pk，前面的判断如果有账簿，pk已经添加了账簿主键
				if (!this.orgAccbookPkCache.includes(`@@${pk}`)) {
					this.orgAccbookPkCache += `@@${pk}`;
				}
				this.orgAccbookDataCache[pk] = orgData;
			}
		}
		return orgData;
	}
}
//穿梭组件回调函数,组装要返回后台的折旧分摊项目
export function gatheritemChange(targetKeys) {
	let {
		categoryLevelStart,
		categoryLevelEnd,
		usingstatusLevelStart,
		usingstatusLevelEnd,
		addreducestyleLevelStart,
		addreducestyleLevelEnd
	} = this.state.gather;
	let gatheritems = [];
	gatheritems = targetKeys.map((item) => {
		//设置开始级次为level2，结束级次为level1
		if (item != undefined) {
			return {
				level2: {
					value: (() => {
						if (item === 'pk_category') {
							return categoryLevelStart;
						} else if (item === 'pk_usingstatus') {
							return usingstatusLevelStart;
						} else if (item === 'pk_addreducestyle') {
							return addreducestyleLevelStart;
						} else {
							return '0';
						}
					})()
				},
				level1: {
					value: (() => {
						if (item === 'pk_category') {
							return categoryLevelEnd;
						} else if (item === 'pk_usingstatus') {
							return usingstatusLevelEnd;
						} else if (item === 'pk_addreducestyle') {
							return addreducestyleLevelEnd;
						} else {
							return '0';
						}
					})()
				},
				pk_accbook: { value: this.state.option.optionValue.pk_accbook.value },
				item_code: { value: item },
				pk_org: { value: this.state.option.optionValue.pk_org.value },
				item_name: {
					value: (() => {
						let item_name = '';
						this.state.gather.dataSource.map((e) => {
							if (e.key == item) {
								item_name = e.title;
							}
						});
						return item_name;
					})()
				},
				system_flag: { value: '0' },
				period: { value: this.state.option.optionValue.period.value },
				accyear: { value: this.state.option.optionValue.accyear.value },
				dr: { value: '0' } //默认
			};
		}
	});
	this.setState({
		gather: {
			...this.state.gather,
			gatheritems,
			targetKeys
		}
	});
}
