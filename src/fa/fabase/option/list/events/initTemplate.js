import { optionConfig } from '../const';
import { setSearchAreaFieldDisable, getOrgDataAfter, setButtonsVisibleByCode, queryOrgData } from '.';

import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { loginContext, getContext, loginContextKeys } = LoginContext;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;
/**
 * 初始化模板
 * @param {*} props 
 */
const { defaultConfig, pageConfig } = optionConfig;
export default function(props) {
	let createUIDomCallBack = (data) => {
		if (data) {
			//加载按钮
			if (data.button) {
				let button = data.button;
				props.button.setButtons(button);
			}
			//加载模板
			if (data.template) {
				let meta = data.template;
				addSearchAreaReferFilter(props, meta, defaultConfig); //参照过滤
				props.meta.setMeta(meta);
				initpage.call(this, props); //联查处理
			}
			//加载上下文
			if (data.context) {
				//初始化上下文变量
				loginContext.call(this, data.context);
				if (!this.parameter.isDrill) {
					let pk_org = getContext(loginContextKeys.pk_org);
					let org_name = getContext(loginContextKeys.org_Name);
					if (pk_org) {
						// 如果设置默认财务组织，需要进行数据设置
						props.search.setSearchValByField('searchArea', 'pk_org', {
							value: pk_org,
							display: org_name
						});
						// 设置账簿可编辑
						setSearchAreaFieldDisable(props, pageConfig.searchId, [ 'accbook' ], false);
						queryOrgData
							.call(this, props, pk_org)
							.then((orgData) => getOrgDataAfter.call(this, orgData, props, pageConfig.searchId));
					} else {
						let none = { display: '', value: '' };
						props.search.setSearchValByField(pageConfig.searchId, 'accbook', none);
						props.search.setSearchValByField(pageConfig.searchId, 'year', none);
						props.search.setSearchValByField(pageConfig.searchId, 'period', none);
						setSearchAreaFieldDisable(props, pageConfig.searchId, [ 'accbook', 'year', 'period' ], true);
					}
				}
			}
		}
	};
	props.createUIDom(
		{
			pagecode: '201200544A_list'
		},
		createUIDomCallBack.bind(this)
	);
}

//获取折旧摊销传过来的数据加载页面获取数据
function initpage(props) {
	let pk_org = props.getUrlParam('pk_org');
	let pk_accbook = props.getUrlParam('pk_accbook');
	let year = props.getUrlParam('accyear');
	let period = props.getUrlParam('period');
	if (pk_org) {
		this.parameter.isDrill = true; //修改控制参数
		this.parameter.isEdit = true; //修改是否是编辑态参数
		queryOrgData.call(this, props, pk_org, pk_accbook).then((orgData) => {
			getOrgDataAfter.call(this, orgData, props, pageConfig.searchId, pk_accbook, year, period);
			//通过org查询穿梭组件数据并改变其可编辑
			setSearchAreaFieldDisable(props, pageConfig.searchId, [ 'pk_org', 'accbook', 'year', 'period' ], true);
			//设置按钮显隐性
			setButtonsVisibleByCode(props, 'drill');
		});
	} else {
		setButtonsVisibleByCode(props, 'init');
	}
}
