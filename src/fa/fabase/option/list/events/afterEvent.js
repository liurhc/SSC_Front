import { ajaxQuery } from '.';
import { toast } from 'nc-lightapp-front';
import { geteditdataSource } from './buttonClick';
import { optionConfig } from '../const';
import { setSearchAreaFieldDisable, queryOrgData } from './method';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/**
 * 查询区编辑后事件
 * @param {*} props 
 * @param {*} id 
 * @param {*} key 
 * @param {*} value 
 * @param {*} data 
 * @param {*} index 
 */
const { pageConfig } = optionConfig;
export default function afterEvent(field, value) {
	//这里需要重置一下是否联查，只要走过编辑后事件就说明不是联查过来的了
	this.parameter.isDrill = false;
	switch (field) {
		//组织
		case 'pk_org':
			orgAfter.call(this, this.props, pageConfig.searchId, value);
			break;
		//账簿
		case 'accbook':
			accbookAfter.call(this, this.props, pageConfig.searchId, value);
			break;
		//年度
		case 'year':
			accyearAfter.call(this, this.props, pageConfig.searchId, value);
			break;
		//会计期间
		case 'period':
			periodAfter.call(this, this.props, pageConfig.searchId, value);
			break;
		default:
			break;
	}
}

/**
 * 组织编辑后事件
 * @param {*} props 
 * @param {*} searchId 
 * @param {*} field 
 * @param {*} value 
 */
function orgAfter(props, searchId, value) {
	//先清空账簿年度和期间
	props.search.setSearchValByField(searchId, 'accbook', { value: '', display: '' });
	props.search.setSearchValByField(searchId, 'year', { value: '', display: '' });
	props.search.setSearchValByField(searchId, 'period', { value: '', display: '' });
	let { isBatch } = this.parameter;
	if (isBatch) {
		//批改编辑后
		//通过org获取预置和自定义项的穿梭组件默认值
		if (value && value.length > 0) {
			//通过org获取穿梭组件全部时区 包含自定义项
			geteditdataSource.call(this).then((dataSource) => {
				if (dataSource) {
					this.setState({
						gather: {
							...this.state.gather,
							dataSource: dataSource,
							targetKeys: []
						}
					});
				}
			});
		} else {
			this.props.search.clearSearchArea(searchId); //清空查询区内容
			//清空穿梭组件数据
			this.setState({
				gather: {
					...this.state.gather,
					dataSource: [],
					targetKeys: []
				}
			});
		}
	} else {
		//修改编辑后
		let pk_org = value && value.refpk; //组织主键
		if (pk_org) {
			//如果有值
			setSearchAreaFieldDisable(props, searchId, [ 'accbook' ], false); //设置账簿可编辑
			//获取相关值，并进行操作
			queryOrgData
				.call(this, props, pk_org)
				.then((orgData) => getOrgDataAfter.call(this, orgData, props, searchId));
		} else {
			//如果是空选
			this.props.search.clearSearchArea(searchId); //清空查询区内容
			setSearchAreaFieldDisable(props, searchId, [ 'accbook', 'year', 'period' ], true); //设置账簿、年度、期间不可编辑
			//清空穿梭组件数据
			this.setState({
				gather: {
					...this.state.gather,
					dataSource: [],
					targetKeys: []
				}
			});
		}
	}
}

/**
 * 账簿编辑后事件
 * @param {*} props 
 * @param {*} value 
 * @param {*} searchId 
 */
function accbookAfter(props, searchId, value) {
	let { isBatch } = this.parameter;
	if (!isBatch) {
		//修改编辑后
		let pk_accbook = value && value.refpk;
		if (pk_accbook) {
			// 如果账簿信息切换，需要重置账簿主键，否则无法切换成功。
			let pk_org = props.search.getSearchValByField(searchId, 'pk_org');
			if (pk_org && pk_org.value && pk_org.value.firstvalue) {
				pk_org = pk_org.value.firstvalue;
				queryOrgData
					.call(this, props, pk_org, pk_accbook)
					.then((orgData) => getOrgDataAfter.call(this, orgData, props, searchId, pk_accbook));
			}
		} else {
			let none = { display: '', value: '' }; //空值
			props.search.setSearchValByField(searchId, 'year', none);
			props.search.setSearchValByField(searchId, 'period', none);
			setSearchAreaFieldDisable(props, searchId, [ 'year', 'period' ], true);
		}
	}
}

/**
 * 年度编辑后事件
 * @param {*} props 
 * @param {*} searchId 
 * @param {*} value 
 */
function accyearAfter(props, searchId, value) {
	//年度编辑后时间不存在批改，但是还是要判断一下
	let { isBatch } = this.parameter;
	if (!isBatch) {
		let pk_org = this.props.search.getSearchValByField(searchId, 'pk_org');
		let pk_accbook = this.props.search.getSearchValByField(searchId, 'accbook');
		if (
			pk_org &&
			pk_org.value &&
			pk_org.value.firstvalue &&
			pk_accbook &&
			pk_accbook.value &&
			pk_accbook.value.firstvalue
		) {
			//如果账簿和组织同时存在，可以使用
			pk_org = pk_org.value.firstvalue;
			pk_accbook = pk_accbook.value.firstvalue;

			if (value) {
				queryOrgData
					.call(this, props, pk_org, pk_accbook)
					.then((orgData) => getOrgDataAfter.call(this, orgData, props, searchId, pk_accbook, value));
			} else {
				let none = { display: '', value: '' }; //空值
				//设置期间不可编辑,并清空备选项
				meta = props.meta.getMeta();
				meta.searchArea.items.map((item) => {
					if (item.attrcode === 'period') {
						return Object.assign({}, item, { options: [] });
					}
				});
				this.props.meta.setMeta(meta, () => {
					setSearchAreaFieldDisable(props, searchId, [ 'period' ], true);
					this.props.search.setSearchValByField(searchId, 'year', none);
					this.props.search.setSearchValByField(searchId, 'period', none);
				});
			}
		}
	}
}

/**
 * 期间编辑后事件
 * @param {*} props 
 * @param {*} searchId 
 * @param {*} value 
 */
function periodAfter(props, searchId, value) {
	//期间编辑后时间不存在批改，但是还是要判断一下
	let { isBatch } = this.parameter;
	if (!isBatch) {
		let pk_org = props.search.getSearchValByField(searchId, 'pk_org');
		let pk_accbook = props.search.getSearchValByField(searchId, 'accbook');
		let year = props.search.getSearchValByField(searchId, 'year');
		if (
			pk_org &&
			pk_org.value &&
			pk_org.value.firstvalue &&
			pk_accbook &&
			pk_accbook.value &&
			pk_accbook.value.firstvalue &&
			year &&
			year.value &&
			year.value.firstvalue
		) {
			//如果账簿和组织同时存在，可以使用
			pk_org = pk_org.value.firstvalue;
			pk_accbook = pk_accbook.value.firstvalue;
			year = year.value.firstvalue;
			if (value) {
				ajaxQuery.call(this);
			}
		}
	}
}

/**
 * 获取数据成功操作
 * @param {*} orgData 
 */
export function getOrgDataAfter(orgData, props, searchId, pk_accbook, year, period) {
	let that = this;
	if (orgData && orgData.pk_accbook) {
		props.search.setSearchValByField(searchId, 'pk_org', orgData.pk_org); //设置查询区账簿
		props.search.setSearchValByField(searchId, 'accbook', orgData.pk_accbook); //设置查询区账簿
		if (orgData.years) {
			//备选年度数据
			setSearchAreaFieldDisable(props, searchId, [ 'year', 'period' ], false);
			let yearData = orgData.years.map((year) => {
				return {
					display: year,
					value: year
				};
			});
			let periodData = [];
			let currentyear = ''; //最小未结账年
			let currentperiod = ''; //最小未结账月
			if ((orgData.mincloseperiod || year) && orgData.yearperiod) {
				currentyear = year ? year : orgData.mincloseperiod.accyear;
				//有期间放期间，有年度放年度第一个，都没有放最小未结账月
				currentperiod = period ? period : year ? orgData.yearperiod[year][0] : orgData.mincloseperiod.period;
				periodData = orgData.yearperiod[currentyear].map((period) => {
					return {
						display: period,
						value: period
					};
				});
			}
			let meta = props.meta.getMeta(); //模板数据
			meta.searchArea.items.forEach((item) => {
				if (item.attrcode === 'year') {
					item.options = yearData;
				}
				if (item.attrcode === 'period') {
					item.options = periodData;
				}
			});
			//更新模板年度和期间备选数据
			props.meta.setMeta(meta, () => {
				props.search.setSearchValByField(searchId, 'year', {
					value: currentyear,
					display: currentyear
				}); //设置年度
				props.search.setSearchValByField(searchId, 'period', {
					value: currentperiod,
					display: currentperiod
				}); //设置期间
				ajaxQuery.call(that); //查询数据
			});
		}
	} else {
		//没有数据，提示错误信息，会计期间和年度清空并禁用
		let none = { value: '', display: '' };
		let error = getMultiLangByID('201200544A-000011') /*该组织没有获取到相关账簿,年度,最小未结账月等信息,确认账簿是否被启用！*/;
		toast({ content: error, color: 'danger' });
		setSearchAreaFieldDisable(props, searchId, [ 'year', 'period' ], true);
		props.search.setSearchValByField(searchId, 'year', none);
		props.search.setSearchValByField(searchId, 'period', none);
	}
}
