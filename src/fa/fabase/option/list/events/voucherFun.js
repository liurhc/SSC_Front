import { optionConfig } from '../const';

/**
 * 凭证生成口径相关方法
 */
let { pageConfig } = optionConfig;
/**
 * 修改生成方式的方法
 * @param {*} value 
 */
export function afterChangeStyle(value) {
	if (value && value !== this.state.voucher.selectedVoucher) {
		//如果方式发生了变化，修改方式，清空数据
		this.setState({
			voucher: {
				selectedVoucher: value, //凭证生成口径方式
				voucherValue: {
					//凭证生成口径界面数据
					catValue: [], //资产类别
					deptValue: [], //使用部门
					manageValue: [] //管理部门
				},
				voucherstyle: [] //返回到后台的数据
			}
		});
	}
}
/**
 * 生成方式对应的参照内容修改
 * @param {*} value 
 */
export function afterChangeValue(value) {
	let selectedStyle = this.state.voucher.selectedVoucher; //当前选择的生成方式
	let voucherstyle = [];
	if (selectedStyle !== 'default') {
		//如果不是默认的
		if (value && value.length > 0) {
			value.map((item) => {
				let voucherstyle_name = '';
				if (selectedStyle === 'catValue') {
					voucherstyle_name = 'pk_category';
				} else if (selectedStyle === 'deptValue') {
					voucherstyle_name = 'pk_usedept';
				} else {
					voucherstyle_name = 'pk_mandept';
				}
				let pk_org = this.props.search.getSearchValByField(pageConfig.searchId, 'pk_org');
				let pk_accbook = this.props.search.getSearchValByField(pageConfig.searchId, 'accbook');
				voucherstyle.push({
					status: '0',
					values: {
						pk_accbook: { value: pk_accbook.value.firstvalue },
						voucherstyle_level: { value: item.level ? `${item.level}` : '0' }, //默认
						pk_org: { value: pk_org.value.firstvalue },
						voucherstyle_value: { value: item.refpk },
						voucherstyle_name: { value: voucherstyle_name },
						dr: { value: '0' } //默认
					}
				});
			});
		}
	}
	this.setState({
		voucher: {
			...this.state.voucher,
			voucherValue: {
				//凭证生成口径数据
				...this.state.voucher.voucherValue,
				[selectedStyle]: value //资产类别
			},
			voucherstyle
		}
	});
}
