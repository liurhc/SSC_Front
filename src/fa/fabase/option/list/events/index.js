import { geteditdataSource, buttionClick } from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent, { getOrgDataAfter } from './afterEvent';
import changeOption, { getChangeRadiogroup } from './changeFun';
import depOption, { getDepCheckboxs } from './depFun';
import ajaxQuery from './ajaxQuery';
import {
	setButtonsVisibleByCode,
	commonAjax,
	groupToast,
	setSearchAreaFieldDisable,
	queryOrgData,
	gatheritemChange
} from './method';
import { afterChangeStyle, afterChangeValue } from './voucherFun';
import {
	onTargetKeysChange,
	cateEndChange,
	cateStartChange,
	usingEndChange,
	usingStartChange,
	addStartChange,
	addEndChange
} from './gatherFun';

/**
 * 页面所有事件
 */
export {
	buttionClick,
	initTemplate,
	afterEvent,
	changeOption,
	depOption,
	ajaxQuery,
	geteditdataSource,
	setButtonsVisibleByCode,
	commonAjax,
	groupToast,
	setSearchAreaFieldDisable,
	afterChangeStyle,
	afterChangeValue,
	onTargetKeysChange,
	cateEndChange,
	cateStartChange,
	usingEndChange,
	usingStartChange,
	addStartChange,
	addEndChange,
	getChangeRadiogroup,
	getDepCheckboxs,
	queryOrgData,
	getOrgDataAfter,
	gatheritemChange
};
