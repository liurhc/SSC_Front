import { toast } from 'nc-lightapp-front';
import { optionConfig } from '../const';
import { commonAjax, gatheritemChange } from '.';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/**
 * 发起请求
 */
const { pageConfig, initState } = optionConfig;
export default async function ajaxQuery() {
	let allSearchData = this.props.search.getAllSearchData(pageConfig.searchId, false); //查询区数据
	let queryData = {}; //传到后台的数据
	let i = 0;
	allSearchData &&
		allSearchData.conditions &&
		allSearchData.conditions.map((condition) => {
			if (condition.field == 'accbook') {
				queryData.pk_accbook = condition.value && condition.value.firstvalue;
			} else if (condition.field == 'year') {
				queryData.accyear = condition.value && condition.value.firstvalue;
			} else {
				queryData[condition.field] = condition.value && condition.value.firstvalue;
			}
			i++;
		});
	if (i != 4) {
		return;
	}
	let res = await commonAjax.call(this, this.props, pageConfig.url.queryUrl, queryData); //查询数据
	if (res.data) {
		let { optionvo, gatheritemvo, voucherstylevo, left, isFipMakeVoucher } = res.data;
		let newState = JSON.parse(JSON.stringify(initState));
		if (!optionvo) {
			toast({ content: getMultiLangByID('201200544A-000013') /*未获取到对应的参数设置信息！*/, color: 'warning' });
		} else {
			if (optionvo) {
				//折旧设置信息与变动设置信息
				//判断数据是否为空不为空将数据赋值
				let values = optionvo.optionvo.rows[0].values;
				newState.option.optionValue = values;
			}
			//折旧分摊项目
			if (gatheritemvo) {
				let checkedArr = gatheritemvo.gatheritemvo.rows;
				newState.gather.gatheritems = checkedArr;
				//读取需要在界面上显示的级次，同时返回在穿梭主键左面的字段编码
				newState.gather.targetKeys = checkedArr.map(({ values }) => {
					//设置开始级次为level2，结束级次为level1 与NC相同，如果级次是0，显示为空
					if (values.item_code.value === 'pk_category') {
						newState.gather.categoryLevelStart = values.level2.value == '0' ? '' : values.level2.value;
						newState.gather.categoryLevelEnd = values.level1.value == '0' ? '' : values.level1.value;
					} else if (values.item_code.value === 'pk_usingstatus') {
						newState.gather.usingstatusLevelStart = values.level2.value == '0' ? '' : values.level2.value;
						newState.gather.usingstatusLevelEnd = values.level1.value == '0' ? '' : values.level1.value;
					} else if (values.item_code.value === 'pk_addreducestyle') {
						newState.gather.addreducestyleLevelStart =
							values.level2.value == '0' ? '' : values.level2.value;
						newState.gather.addreducestyleLevelEnd = values.level1.value == '0' ? '' : values.level1.value;
					}
					return values.item_code.value;
				});
			}
			//穿梭组件
			if (left) {
				newState.gather.dataSource = left.map(({ item_code, item_name }) => {
					return {
						key: item_code,
						title: item_name,
						disabled: true
					};
				});
			}
			//凭证生成口径
			if (voucherstylevo) {
				let voucherRows = voucherstylevo.voucherstylevo.rows;
				if (voucherRows && voucherRows.length > 0) {
					let voucherstyle_name = voucherRows[0].values.voucherstyle_name.value;
					let catValue = [];
					let deptValue = [];
					let manageValue = [];
					let selectedValue = 'default';
					voucherRows.map((voucherRow) => {
						let voucher = {
							refcode: voucherRow.values.code && voucherRow.values.code.value,
							refname: voucherRow.values.voucherstyle_value.display,
							refpk: voucherRow.values.voucherstyle_value.value,
							level: voucherRow.values.voucherstyle_level.value
						};
						if (voucherstyle_name === 'pk_category') {
							catValue.push(voucher);
							selectedValue = 'catValue';
						} else if (voucherstyle_name === 'pk_usedept') {
							deptValue.push(voucher);
							selectedValue = 'deptValue';
						} else {
							manageValue.push(voucher);
							selectedValue = 'manageValue';
						}
					});
					newState.voucher.selectedVoucher = selectedValue;
					newState.voucher.voucherValue = {
						catValue,
						deptValue,
						manageValue
					};
					newState.voucher.voucherstyle = voucherRows;
				}
			}
			//是否已经折旧制单
			this.parameter.isFipMakeVoucher = isFipMakeVoucher;
		}

		this.setState(newState, () => {
			gatheritemChange.call(this, newState.gather.targetKeys);
		});
	}
}
