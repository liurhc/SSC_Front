import { toast } from 'nc-lightapp-front';
import { optionConfig } from '../const';
import ajaxQuery from './ajaxQuery';
import { queryOrgData, setButtonsVisibleByCode, setSearchAreaFieldDisable, commonAjax, groupToast } from '.';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/**
 * 按钮点击事件
 * @param {*} props 
 * @param {*} id 
 */
const { pageConfig, initState } = optionConfig;
const { searchId, url, buttonCode } = pageConfig;
export function buttionClick(props, id) {
	switch (id) {
		case buttonCode.edit:
			edit.call(this, props); //修改按钮
			break;
		case buttonCode.batch:
			batch.call(this, props); //批改按钮
			break;
		case buttonCode.save:
			save.call(this, props); //保存按钮
			break;
		case buttonCode.cancel:
			cancel.call(this, props); //取消按钮
			break;
		default:
			break;
	}
}
/**
 * 修改方法 
 */
async function edit(props) {
	let pk_org = props.search.getSearchValByField(searchId, 'pk_org');
	let pk_accbook = props.search.getSearchValByField(searchId, 'accbook');
	let accyear = props.search.getSearchValByField(searchId, 'year');
	let period = props.search.getSearchValByField(searchId, 'period');

	//判断组织、账簿、年度、期间信息是否输入完全
	if (
		!pk_org.value.firstvalue ||
		!pk_accbook.value.firstvalue ||
		!accyear.value.firstvalue ||
		!period.value.firstvalue
	) {
		let error = getMultiLangByID('201200544A-000014') /*请先输入完整信息查询后再修改！*/;
		toast({ content: error, color: 'danger' });
		return;
	}
	//判断是否已经折旧制单
	if (this.parameter.isFipMakeVoucher) {
		let error = getMultiLangByID('201200544A-000057') /*已进行折旧制单，不能修改参数！*/;
		toast({ content: error, color: 'danger' });
		return;
	}
	let orgData = await queryOrgData.call(this, props, pk_org.value.firstvalue, pk_accbook.value.firstvalue); //查询组织相关数据
	if (orgData) {
		let minaccyear = orgData.mincloseperiod.accyear;
		let mainperiod = orgData.mincloseperiod.period;
		//校验当前页面的年库和期间是否是最小未结账月
		if (minaccyear != accyear.value.firstvalue || mainperiod != period.value.firstvalue) {
			let error = getMultiLangByID('201200544A-000015') /*该会计期间不是最小未结账月！*/;
			toast({ content: error, color: 'danger' });
			return;
		} else {
			//校验通过之后，修改页面为编辑态，修改按钮为保存和取消
			setButtonsVisibleByCode(props, 'edit');
			this.parameter.isEdit = true;
			//如果是单组织查询后修改控制查询区全部不可以更改。
			setSearchAreaFieldDisable(props, searchId, [ 'pk_org', 'accbook', 'year', 'period' ], true);
		}
	}
}
/**
 * 批改方法
 */
function batch(props) {
	let meta = props.meta.getMeta();
	//清空查询区信息
	props.search.clearSearchArea(searchId);
	//修改按钮为显示和保存
	setButtonsVisibleByCode(props, 'batch');
	//修改页面状态为编辑态，修改数据为初始化数据(折旧分摊左边有值)，修改批改标志位为true
	this.parameter.isBatch = true;
	this.parameter.isEdit = true;
	let newState = JSON.parse(JSON.stringify(initState));
	this.setState(newState, () => {
		setSearchAreaFieldDisable(props, searchId, [ 'year', 'period' ], true);
		setSearchAreaFieldDisable(props, searchId, [ 'pk_org', 'accbook' ], false);
		//将查询区组织和账簿参照设置为多选
		meta.searchArea.items[0].isMultiSelectedEnabled = true;
		meta.searchArea.items[1].isMultiSelectedEnabled = true;
		props.meta.setMeta(meta);
	});
}
/**
 * 保存方法
 * 对于联查过来的操作需要
 * 1校验是否已经折旧制单（联查跳转会跳过修改按钮，这里有必要加一个校验），同时后台也要加校验，防止同时操作
 * 2校验是否是最小未结账月
 * 3补充年度和期间的备选数据
 * 保存数据成功之后
 * 1.设置页面为浏览态
 * 2.修改按钮为批改和修改
 * 3.修改查询模板编辑性：四个项目均可编辑
 * 获取查询区账簿的数据
 */
async function save(props) {
	let fields = [];
	let meta = props.meta.getMeta(); //获取模板数据，进而获得名称
	let checkFun = (checkFields) => {
		let checkFieldsArr = checkFields.split('&&');
		checkFieldsArr.map((code) => {
			let value = props.search.getSearchValByField(searchId, code); //需要校验的字段的值
			if (!(value && value.value && value.value.firstvalue)) {
				meta &&
					meta[searchId] &&
					meta[searchId].items &&
					meta[searchId].items.map((metaItem) => {
						if (metaItem.attrcode == code) {
							fields.push(metaItem.label); //获取校验不通过的翻译名称
						}
					});
			}
		});
	};
	if (this.parameter.isBatch) {
		let checkFields = 'pk_org&&accbook'; //需要校验的字段
		checkFun(checkFields); //校验
	} else {
		let checkFields = 'pk_org&&accbook&&year&&period'; //需要校验是否有值的字段
		checkFun(checkFields); //校验
	}
	if (fields.length > 0) {
		let andName = getMultiLangByID('201200544A-000058'); /*国际化处理：和 */
		let content = ''; //将content处理为：A或者A和B或者A、B和C的形式
		for (let index = 0; index < fields.length; index++) {
			const fieldLabel = fields[index];
			if (fields.length == 1) {
				content = fieldLabel;
			} else if (index == fields.length - 2) {
				content += `${fieldLabel}${andName}`;
			} else if (index == fields.length - 1) {
				content += `${fieldLabel}`;
			} else {
				content += `${fieldLabel}、`;
			}
		}
		toast({
			content: getMultiLangByID('201200544A-000016', { content: content }) /*请输入{content}信息后再保存！*/,
			color: 'danger'
		});
		return;
	}

	//修改校验是否有最小未结账月，批改不用校验
	if (!this.parameter.isBatch) {
		let curPk_org = props.search.getSearchValByField(searchId, 'pk_org'); //组织字段
		let curPk_accbook = props.search.getSearchValByField(searchId, 'accbook'); //账簿字段
		let curAccyear = props.search.getSearchValByField(searchId, 'year');
		let curPeriod = props.search.getSearchValByField(searchId, 'period');
		let orgData = await queryOrgData.call(
			this,
			props,
			curPk_org.value.firstvalue,
			curPk_accbook.value.firstvalue,
			true
		); //强制查询查询组织相关数据
		if (orgData) {
			let minaccyear = orgData.mincloseperiod.accyear;
			let mainperiod = orgData.mincloseperiod.period;
			//校验当前页面的年库和期间是否是最小未结账月
			if (minaccyear != curAccyear.value.firstvalue || mainperiod != curPeriod.value.firstvalue) {
				let error = getMultiLangByID('201200544A-000015') /*该会计期间不是最小未结账月！*/;
				toast({ content: error, color: 'danger' });
				return;
			}
		} else {
			let error = getMultiLangByID('201200544A-000015') /*该会计期间不是最小未结账月！*/;
			toast({ content: error, color: 'danger' });
			return;
		}
	}

	let { option, gather, voucher, scroll } = this.state;
	let { isDrill, isBatch, isEdit, isFipMakeVoucher } = this.parameter;
	// let toastMsg = [];
	// let isPopoverShow = gather.isPopoverShow;
	//update by dongzheng9 由于暂时不需要显示提示框 注释掉，以后需要的话放开注释即可
	// for (const key in isPopoverShow) {
	// 	if (isPopoverShow.hasOwnProperty(key)) {
	// 		const element = isPopoverShow[key];
	// 		if (element) {
	// 			let fieldName = '';
	// 			switch (key) {
	// 				case 'category':
	// 					fieldName = getMultiLangByID('201200544A-000017') /*资产类别级次*/;
	// 					break;
	// 				case 'usingstatus':
	// 					fieldName = getMultiLangByID('201200544A-000018') /*使用状况级次*/;
	// 					break;
	// 				case 'addreducestyle':
	// 					fieldName = getMultiLangByID('201200544A-000019') /*增减方式级次*/;
	// 					break;
	// 				default:
	// 					break;
	// 			}
	// 			toastMsg.push(
	// 				getMultiLangByID('201200544A-000022', { fieldName: fieldName }) /*${fieldName}中结束级次不能小于开始级次*/
	// 			);
	// 		}
	// 	}
	// }
	// if (toastMsg.length > 0) {
	// 	toast({
	// 		duration: 'infinity',
	// 		color: 'danger',
	// 		groupOperation: true,
	// 		TextArr: [ '', '', '' ],
	// 		groupOperationMsg: toastMsg
	// 	});
	// 	return;
	// }
	gather.gatheritems.map((item) => {
		//设置开始级次为level2，结束级次为level1,与NC相同如果不存在设为0
		/*
		 *设置开始级次为level2，结束级次为level1的原因在于，NC之前设置级次的时候，只能设置一个级次，level1和level2相等，
		 *执行的时候取得是level1(GatherProcess.reGatherByGatherItemLevel)，NC显示取得是level2(GatherItemPane.doSetJc)，这里将
		 *开始和结束对调，执行得时候按照结束级次执行。为了显示相同，读取数据得时候同样进行了对调取数，开始级次取level2，结束级次level1。
		 */
		switch (item.item_code.value) {
			case 'pk_category':
				item.level2.value = gather.categoryLevelStart ? gather.categoryLevelStart : '0';
				item.level1.value = gather.categoryLevelEnd ? gather.categoryLevelEnd : '0';
				break;
			case 'pk_usingstatus':
				item.level2.value = gather.usingstatusLevelStart ? gather.usingstatusLevelStart : '0';
				item.level1.value = gather.usingstatusLevelEnd ? gather.usingstatusLevelEnd : '0';
				break;
			case 'pk_addreducestyle':
				item.level2.value = gather.addreducestyleLevelStart ? gather.addreducestyleLevelStart : '0';
				item.level1.value = gather.addreducestyleLevelEnd ? gather.addreducestyleLevelEnd : '0';
				break;
			default:
				break;
		}
	});
	//获取账簿
	let accbooks = props.search.getSearchValByField(searchId, 'accbook');
	let saveurl = '';
	let data = {};
	//批量修改时保存方法
	if (isBatch) {
		saveurl = url.batchSaveUrl;
		//批改经要求基础信息需要传入账簿的集合 这里的accbooks.value.firstvalue是由多个账簿的pk由“,”分割的字符串
		//到后端时通过账簿获取年度和最小未结账月循环修改
		option.optionValue.pk_accbook.value = accbooks.value.firstvalue;
		data = {
			option: {
				rows: {
					areaType: 'form',
					rows: [
						{
							areaType: 'form',
							values: option.optionValue
						}
					]
				}
			},
			gatheritem: {
				rows: {
					areaType: 'form',
					rows: gather.gatheritems.map((item) => {
						return { areaType: 'form', values: item };
					})
				}
			}
		};
	} else {
		//保存方法 需要将org accbook year porid全部齐全的传入 统一保存
		saveurl = url.saveUrl;
		data = {
			option: {
				rows: {
					areaType: 'form',
					rows: [
						{
							areaType: 'form',
							values: option.optionValue
						}
					]
				}
			},
			gatheritem: {
				rows: {
					areaType: 'form',
					rows: gather.gatheritems.map((item) => {
						return { areaType: 'form', values: item };
					})
				}
			},
			voucherstyle: {
				rows: {
					areaType: 'form',
					rows: voucher.voucherstyle
				}
			}
		};
	}
	let res = await commonAjax.call(this, props, saveurl, data);
	if (res) {
		let meta = this.props.meta.getMeta();
		this.parameter.isEdit = false;
		this.parameter.isBatch = false;
		setButtonsVisibleByCode(props, 'init');
		//查询区修改参照多选，保存完参照修改为单选
		meta.searchArea.items[0].isMultiSelectedEnabled = false;
		meta.searchArea.items[1].isMultiSelectedEnabled = false;
		//批改不需要刷新查询
		let that = this;
		props.meta.setMeta(meta, () => {
			if (isBatch) {
				let newState = JSON.parse(JSON.stringify(that.state));
				that.setState(newState, () => {
					props.search.clearSearchArea(searchId); //清空查询区
					setSearchAreaFieldDisable(props, searchId, [ 'accbook', 'year', 'period' ], true);
					setSearchAreaFieldDisable(props, searchId, [ 'pk_org' ], false);
					if (res.data.mesg) {
						let resData = res.data.mesg.split('</br>');
						if (resData) {
							if (resData.length == 1 || resData.length == 2) {
								toast({ content: resData.shift(), color: 'success', duration: 'infinity' });
							} else {
								groupToast(resData.shift(), resData);
							}
						}
					}
				});
			} else {
				setSearchAreaFieldDisable(props, searchId, [ 'pk_org', 'accbook', 'year', 'period' ], false);
				ajaxQuery.call(that);
				toast({
					content: getMultiLangByID('201200544A-000023') /*保存成功，需要重新计提折旧才会生效*/,
					color: 'success'
				});
			}
		});
	}
}
/**
 * 取消方法
 */
function cancel(props) {
	//确认取消方法
	let cancelConfirm = () => {
		let meta = props.meta.getMeta();
		// 修改按钮显隐性
		setButtonsVisibleByCode(props, 'cancel');
		//修改页面状态为浏览态
		this.parameter.isEdit = false;
		this.parameter.isBatch = false;
		let newState = JSON.parse(JSON.stringify(initState));
		this.setState(newState, () => {
			//修改组织控件可编辑
			setSearchAreaFieldDisable(props, searchId, [ 'accbook', 'year', 'period' ], true);
			setSearchAreaFieldDisable(props, searchId, [ 'pk_org' ], false);
			//清空查询区
			props.search.clearSearchArea(searchId);
			//取消时应该把批改时修改的组织和账簿多选修改成单选
			meta.searchArea.items[0].isMultiSelectedEnabled = false;
			meta.searchArea.items[1].isMultiSelectedEnabled = false;
		});
	};
	props.ncmodal.show(`201200544A-confirm`, {
		title: getMultiLangByID('msgUtils-000002'), //取消
		content: getMultiLangByID('msgUtils-000003'), //确定要取消吗？
		beSureBtnClick: cancelConfirm
	});
}

//获取穿梭组件数据的方法//组织单选的时候不会走此方法
export async function geteditdataSource() {
	let org = this.props.search.getSearchValByField(searchId, 'pk_org').value.firstvalue.split(',');
	let dataSource = false;
	if (org[0]) {
		let data = {
			pk_org: org[0]
		};
		let res = await commonAjax.call(this, this.props, url.queryLeftUrl, data);
		if (res && res.data) {
			if (res.data.left) {
				res.data.left &&
					(dataSource = res.data.left.map(({ item_code, item_name }) => {
						return {
							key: item_code,
							title: item_name,
							disabled: false
						};
					}));
			} else {
				toast({ content: getMultiLangByID('201200544A-000025') /*未获取到对应的参数设置信息！*/, color: 'warning' });
			}
		}
	}
	return dataSource;
}
