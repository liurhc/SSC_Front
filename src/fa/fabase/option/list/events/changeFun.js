import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

/**
 * 变动设置
 */
export default function changeOption(value, name) {
	switch (name) {
		case 'sortChange':
			let newcate_flag = this.state.option.optionValue.newcate_flag.value;
			let newdata = Object.assign({}, this.state.option.optionValue, {
				newcate_flag: newcate_flag == '0' ? { value: '1' } : { value: '0' }
			});
			this.setState({ option: { optionValue: newdata } });
			break;
		case 'deptChange':
			let newusedept_flag = this.state.option.optionValue.newusedept_flag.value;
			let deptdata = Object.assign({}, this.state.option.optionValue, {
				newusedept_flag: newusedept_flag == '0' ? { value: '1' } : { value: '0' }
			});
			this.setState({ option: { optionValue: deptdata } });
			break;
		case 'deptTransfer':
			let newmandept_flag = this.state.option.optionValue.newmandept_flag.value;
			let transdata = Object.assign({}, this.state.option.optionValue, {
				newmandept_flag: newmandept_flag == '0' ? { value: '1' } : { value: '0' }
			});
			this.setState({ option: { optionValue: transdata } });
			break;
		case 'fileChange':
			let jobmngfil_flag = this.state.option.optionValue.jobmngfil_flag.value;
			let filedata = Object.assign({}, this.state.option.optionValue, {
				jobmngfil_flag: jobmngfil_flag == '0' ? { value: '1' } : { value: '0' }
			});
			this.setState({ option: { optionValue: filedata } });
			break;
		default:
			break;
	}
}

/**
 * 获取变动设置界面
 * @param {*} optionValue 
 */
export function getChangeRadiogroup(optionValue, isEdit) {
	let { newcate_flag, newusedept_flag, newmandept_flag, jobmngfil_flag } = optionValue; //变动设置参数
	let settingChange = [
		//变动设置
		{
			title: getMultiLangByID('201200544A-000033') /*资产类别变化：*/,
			name: 'sortChange',
			selectedValue: newcate_flag.value,
			children: [
				{ value: '0', disabled: !isEdit, title: getMultiLangByID('201200544A-000034') /*当期折旧归属变动前类别*/ },
				{ value: '1', disabled: !isEdit, title: getMultiLangByID('201200544A-000035') /*当期折旧归属变动后类别*/ }
			]
		},
		{
			title: getMultiLangByID('201200544A-000036') /*使用部门变化：*/,
			name: 'deptChange',
			selectedValue: newusedept_flag.value,
			children: [
				{ value: '0', disabled: !isEdit, title: getMultiLangByID('201200544A-000037') /*当期折旧归属变动前部门*/ },
				{ value: '1', disabled: !isEdit, title: getMultiLangByID('201200544A-000038') /*当期折旧归属变动后部门*/ }
			]
		},
		{
			title: getMultiLangByID('201200544A-000039') /*管理部门转移：*/,
			name: 'deptTransfer',
			selectedValue: newmandept_flag.value,
			children: [
				{ value: '0', disabled: !isEdit, title: getMultiLangByID('201200544A-000037') /*当期折旧归属变动前部门*/ },
				{ value: '1', disabled: !isEdit, title: getMultiLangByID('201200544A-000038') /*当期折旧归属变动后部门*/ }
			]
		},
		{
			title: getMultiLangByID('201200544A-000040') /*项目档案变化：*/,
			name: 'fileChange',
			selectedValue: jobmngfil_flag.value,
			children: [
				{ value: '0', disabled: !isEdit, title: getMultiLangByID('201200544A-000041') /*当期折旧归属变动前项目*/ },
				{ value: '1', disabled: !isEdit, title: getMultiLangByID('201200544A-000042') /*当期折旧归属变动后项目*/ }
			]
		}
	];
	return settingChange;
}
