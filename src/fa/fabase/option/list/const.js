export const optionConfig = {
	pageConfig: {
		searchId: 'searchArea',
		url: {
			querydateUrl: '/nccloud/fa/depreciation/querydate.do',
			batchSaveUrl: '/nccloud/fa/option/batchoptionsave.do',
			saveUrl: '/nccloud/fa/option/optionsave.do',
			queryLeftUrl: '/nccloud/fa/option/queryleft.do',
			queryUrl: '/nccloud/fa/option/query.do'
		},
		buttonCode: {
			edit: 'Edit',
			batch: 'batchButton',
			save: 'Save',
			cancel: 'Cancel'
		}
	},

	//定义组织过滤常量
	defaultConfig: {
		searchId: 'searchArea',
		specialFields: {
			accbook: {
				//财务核算账簿
				RefActionExtType: 'TreeRefActionExt',
				class: 'nccloud.web.fa.common.refCondition.AccbookOrgRefFilter',
				data: [
					//字段过滤
					{
						fields: [ 'pk_org' ], //使用组织
						returnName: 'pk_org'
					}
				]
			}
		}
	},

	initState: {
		option: {
			//折旧设置和变动设置相关
			optionValue: {
				servicemonth_flag: { value: true }, //使用月限调整当期有效
				depmethod_flag: { value: true }, //折旧方法调整当期有效
				newusingstatus: { value: true }, //使用状况调整当期有效
				workloan_flag: { value: true }, //工作量调整当期有效
				dep_flag: { value: true }, //折旧方法当期有效
				depall_flag: { value: true }, //使用期限的最后一期折旧提足
				newcate_flag: { value: '0' }, //资产类别变化：
				newusedept_flag: { value: '0' }, //使用部门变化
				newmandept_flag: { value: '0' }, //管理部门转移
				jobmngfil_flag: { value: '0' }, //项目档案变化
				pk_accbook: { value: '' },
				ts: { value: '' },
				depperiod: { value: '1' },
				period: { value: '0' },
				accyear: { value: '' },
				pk_org: { value: '' },
				pk_option: { value: '' },
				dr: { value: '0' }
			}
		},
		gather: {
			//折旧分摊相关
			categoryLevelStart: '', //资产类别开始级次
			categoryLevelEnd: '', //资产类别结束级次
			usingstatusLevelStart: '', //使用状况开始级次
			usingstatusLevelEnd: '', //使用状况结束级次
			addreducestyleLevelStart: '', //增减方式开始级次
			addreducestyleLevelEnd: '', //增减方式结束级次
			isPopoverShow: {
				//是否显示级次提示框
				category: false,
				usingstatus: false,
				addreducestyle: false
			},
			dataSource: [],
			targetKeys: [],
			gatheritems: [] //折旧分摊数据
		},
		voucher: {
			//凭证生成口径相关
			selectedVoucher: 'default', //凭证生成口径方式 (初始化为默认方式)
			voucherValue: {
				//凭证生成口径数据
				catValue: [], //资产类别
				deptValue: [], //使用部门
				manageValue: [] //管理部门
			},
			voucherstyle: [] //返回到后台的数据
		},
		scroll: {
			//楼层组件相关
			scrollLink: 0 //被选中的楼层
		}
	},

	//定义楼层电梯常量
	scrollLinkName: [
		'201200544A-000005' /*折旧设置*/,
		'201200544A-000006' /*变动设置*/,
		'201200544A-000007' /*折旧分摊*/,
		'201200544A-000008' /*凭证生成口径*/
	]
};
