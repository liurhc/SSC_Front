export const categoryConst = {
	categoryFormId: 'card_head',
	categoryTreeId: 'category_tree'
};

const defaultButtons = {
	Save: false,
	Cancel: false,
	Print :true,
	Refresh :true
};

const openButtons = {
	Save: false,
	Cancel: false,
	Print :true,
	Refresh :true
};

const editButtons = {
	Save: true,
	Cancel: true,
	Print :false,
	Refresh :false
};

const enableButtons = {
	enable: true,
	disable: false,
	Print :true,
	Refresh :true
};

const disableButtons = {
	enable: false,
	disable: true,
	Print :true,
	Refresh :true
};

export const categoryUrl = {
	add: '/nccloud/fa/category/save.do',
	edit: '/nccloud/fa/category/update.do',
	del: '/nccloud/fa/category/delete.do',
	enable: '/nccloud/fa/category/enable.do',
	disable: '/nccloud/fa/category/disable.do',
	print : '/nccloud/fa/category/print.do'
};

export const buttonsLayout = {
	default: defaultButtons,
	start: openButtons,
	edit: editButtons,
	enable: enableButtons,
	disable: disableButtons,
};

export const browsButton = {
	Print : 'Print',
	Refresh : 'Refresh'
}

export const RootDisableButton = [
	'Print','PrintGroup','Output'
]