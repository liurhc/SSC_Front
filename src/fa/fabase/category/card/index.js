import React, { Component } from 'react';
import { createPage, base, ajax, toast, high, createPageIcon } from 'nc-lightapp-front';
import { categoryConst, buttonsLayout, categoryUrl, RootDisableButton } from './constant';
import { dispatcher, handleAfterEditEvent,beforeEvent, processData, initTemplate, modifiyMeta,getMultiLangName } from './events';
import ampub from 'ampub';
const { utils,components } = ampub;
const { AMRefer } = components;
const { multiLangUtils,closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { NCCheckbox } = base;
const treeRootPk = '~';
class Category extends Component {
	constructor(props) {
		super(props);
		let { form, button, syncTree } = this.props;
		let { setSyncTreeData, openNodeByPk, createTreeData } = syncTree;
		let { setButtonsVisible } = button;
		this.setTreeData = setSyncTreeData; // 设置树根节点数据方法
		this.openNodeByPk = openNodeByPk; // 设置某个节点展开
		this.createTreeData = createTreeData; // 将数据组合成树结构
		this.setButtonsVisible = setButtonsVisible;
		this.state = {			
			mainorg: {}, //当前组织
			editTree: true, //使用树上按钮
			isShowOff: false // 是否显示停用,
		};
		this.initData = {
			depmethod_init: {//不计提
				display: '',
				value: ''
			},
			depmethod_default_init: {//默认折旧
				display: '',
				value: ''
			},
			transtype_00_init: {//通用资产
				display: '',
				value: ''
			},
			transtype_05_init: {//递延资产
				display: '',
				value: ''
			}
		};
		closeBrowserUtils.call(this,props,{form:[categoryConst.categoryFormId]});
		initTemplate.call(this, props);
	}

	loadInitData() {
		ajax({
			url: '/nccloud/fa/category/init.do',			
			success: (res) => {
				if (res.data) {
					if(res.data.depmethod_01){
						this.initData.depmethod_init.value = res.data.depmethod_01[0];
						this.initData.depmethod_init.display = res.data.depmethod_01[1];
					}
					if(res.data.depmethod_default){
						this.initData.depmethod_default_init.value = res.data.depmethod_default[0];
						this.initData.depmethod_default_init.display = res.data.depmethod_default[1];
					}
					if(res.data.transtype_00){
						this.initData.transtype_00_init.value = res.data.transtype_00[0];
						this.initData.transtype_00_init.display = res.data.transtype_00[1];
					}
					if(res.data.transtype_05){
						this.initData.transtype_05_init.value = res.data.transtype_05[0];
						this.initData.transtype_05_init.display = res.data.transtype_05[1];
					}

					//全局管控模式		depattr 
					if(this.props.node_type == "global"){
						//交易类型不可以编辑
						this.props.form.setFormItemsDisabled(categoryConst.categoryFormId,{
							pk_transitype: true
						});
						//折旧方法不是全局级
						if(!this.initData.depmethod_init.value){
							//折旧方法不可以编辑
							this.props.form.setFormItemsDisabled(categoryConst.categoryFormId,{
								pk_depmethod: true
							});								
						}
						//递延资产不可编辑						
						this.props.form.setFormItemsDisabled(categoryConst.categoryFormId,{
							extensionasset_flag: true
						});
					}					
				}	
			},
			error: (res) => {
				if (res && res.message) {
					toast({ color: 'warning', content: res.message });
				}
			}
		});
	}

	componentDidMount() {		
		this.props.button.setButtonsVisible(buttonsLayout.start);

		this.loadInitData();
	}

	//查询数据
	getTreeData = (org,callback) => {
		let that = this;
	
		if ((org == null || org == '') && this.props.node_type == 'org') {
			return ;
		}
		ajax({
			url: '/nccloud/fa/category/query.do',
			data: {
				isShowSeal: this.state.isShowOff ? 'Y' : 'N',
				nodeType: this.props.node_type,
				pagecode: this.props.pagecode,
				areacode: categoryConst.categoryFormId,
				pk_org: org
			},
			success: (res) => {
				let { data, success, message } = res;
				if (success && data) {
					data[categoryConst.categoryFormId].rows.map((item) => {
						this.modifyAfterAjax(this.props, item);
					});

					let treeData;
					if (data == null) {
						treeData = this.buildRootData();
					} else {
						treeData = this.buildTreeData(data[categoryConst.categoryFormId].rows);
					}
					this.sortTree(treeData);
					this.props.syncTree.setSyncTreeData(categoryConst.categoryTreeId, treeData);
					this.props.syncTree.openNodeByPk(categoryConst.categoryTreeId, '~');
					this.setButtonsVisible(buttonsLayout.default);
					//默认选中第一个节点
					let first = treeData[0].children[0].refpk;
					this.props.syncTree.setNodeSelected(categoryConst.categoryTreeId,first);
					let currentNode = this.props.syncTree.getSelectNode(categoryConst.categoryTreeId);
					this.onSelect(currentNode.refpk,currentNode);
					//处理联查场景的节点定位
					this.dealLinkToById.call(this);

				} else {
					let vnode = { refname: getMultiLangByID('201200504A-000025')/* 国际化处理： 固定资产类别*/, refpk: '~', refcode: '~', values: new Object() };
					this.props.syncTree.setSyncTreeData(categoryConst.categoryTreeId, [ vnode ]);
					//默认选中根节点
					this.props.syncTree.setNodeSelected(categoryConst.categoryTreeId,'~');
					let currentNode = this.props.syncTree.getSelectNode(categoryConst.categoryTreeId);
					this.onSelect(currentNode.refpk,currentNode);
				}
				typeof callback === 'function' && callback.call(this);
			}
		});
	};

	//处理联查场景的节点定位
	dealLinkToById(){
		let link_id = this.link_id;
		if(link_id){
			//选择当前节点
			this.props.syncTree.setNodeSelected(categoryConst.categoryTreeId,link_id);
			let currentNode = this.props.syncTree.getSelectNode(categoryConst.categoryTreeId);
			if(currentNode){
				this.onSelect(currentNode.refpk,currentNode);
				//展开上级节点
				this.openParentNode.call(this,currentNode);
			}else {
				//默认选中根节点
				this.props.syncTree.setNodeSelected(categoryConst.categoryTreeId,'~');
				currentNode = this.props.syncTree.getSelectNode(categoryConst.categoryTreeId);
				this.onSelect(currentNode.refpk,currentNode);
			}
			//清空跳转用来定位的id , 保证只定位一次
			this.link_id = undefined;
		}
	}

	//递归展开上级节点
	openParentNode(currentNode) {
		let pid =  currentNode.pid;
		this.props.syncTree.openNodeByPk(categoryConst.categoryTreeId, pid);
		//如果上级根节点不是跟节点继续递归调用
		if(pid != '~'){
			let parentNode = this.props.syncTree.getSyncTreeValue(categoryConst.categoryTreeId,pid);
			this.openParentNode.call(this,parentNode);
		}
	}

	//在发送网络请求之后对数据的处理
	modifyAfterAjax(props, item) {
		let enable = 'enablestate';
		// display有值的时候switch会是开启状态
		delete item.values[enable].display;
		if (item.values[enable].value == '2' || item.values[enable].value == true) {
			item.values[enable].value = true;
		} else {
			item.values[enable].value = false;
		}
	}

	//加载树上面的数据
	buildTreeData(data) {
		data.forEach((item) => {
			let values = item.values;
			if (!values.hasOwnProperty('pk_parent')) {
				item.values['pk_parent'] = {
					value: '~'
				};
			}
		});
		let formatData = data.map((item, key) => {
			let values = item.values;
			if (values.dr) {
				delete values.dr;
			}
			let pid = values.pk_parent.value;
			//如果上层节点的pid为空创建树形结构时候报栈溢出增加默认值
			if (pid == '' || pid == null) {
				pid = treeRootPk;
			}
			let data = {
				pid: pid,
				refcode: values.cate_code.value,
				refname: values.cate_code.value + ' ' + getMultiLangName('cate_name',item),
				refpk: values.pk_category.value,
				values: JSON.parse(JSON.stringify(values))
			};
			return data;
		});
		let vnode = { refname: getMultiLangByID('201200504A-000025')/* 国际化处理： 固定资产类别*/, refpk: treeRootPk, refcode: '~', values: new Object() };
		formatData.push(vnode);
		let treeData = this.props.syncTree.createTreeData(formatData);
		return treeData;
	}

	//根据编码对树上的数据排序
	sortTree = (
		tree,
		sortfunc = (a, b) => {
			if (a.refcode > b.refcode) {
				return 1;
			} else if (a.refcode < b.refcode) {
				return -1;
			} else {
				return 0;
			}
		}
	) => {
		// 对上层树排序
		tree.sort(sortfunc);
		// 遍历所有节点，如果节点有子树，递归地对子树排序
		tree.map((item) => {
			if (item.hasOwnProperty('children') && item.children != null) {
				this.sortTree(item.children);
			}
		});
	};

	//当数据库没有数据数据时，初始化根节点信息
	buildRootData = () => {
		let vnode = { refname: getMultiLangByID('201200504A-000025')/* 国际化处理： 固定资产类别*/, refpk: '~', refcode: '~', values: new Object() };
		let formatData = new Array();
		formatData.push(vnode);
		let treeData = this.props.syncTree.createTreeData(formatData);
		return treeData;
	};

	//设置树上图标的隐藏
	onMouseEnterEve(key) {
		if (key === '~') {
			let obj = {
				delIcon: false, //false:隐藏； true:显示; 默认都为true显示
				editIcon: false,
				addIcon: true
			};
			this.props.syncTree.hideIcon(categoryConst.categoryTreeId, key, obj);
		}
	}

	//数据选择事件
	onSelect(pk, item, ischange) {
		this.props.form.EmptyAllFormValue(categoryConst.categoryFormId);
		if (item.refpk == '~') {
			this.props.form.setFormItemsDisabled(categoryConst.categoryFormId, { enablestate: true });

			//设置按钮的可点击行
			this.props.button.setButtonDisabled(RootDisableButton, true);

			//调整meta
			modifiyMeta(this.props, false);
			return;
		}

		this.props.button.setButtonDisabled(RootDisableButton, false);

		this.modifyAfterAjax('', item);

		if (item.values.hasOwnProperty('servicemonth')) {
			let num = parseInt(item.values['servicemonth'].value);
			item.values['serviceyear_display'] = {
				value: parseInt(num / 12) + ''
			};
			item.values['serviceperiod_display'] = {
				value: num % 12 + ''
			};
			if (item.values['serviceyear_display'].value == 0) {
				item.values['serviceyear_display'].value = '0';
			}
			if (item.values['serviceperiod_display'].value == 0) {
				item.values['serviceperiod_display'].value = '0';
			}
		}
		let formData = {
			[categoryConst.categoryFormId]: {
				rows: [
					{
						values: JSON.parse(JSON.stringify(item.values))
					}
				]
			}
		};
		
		this.props.form.setAllFormValue(formData);
		this.props.form.setFormItemsDisabled(categoryConst.categoryFormId, { extensionasset_flag: true });
		if (
			this.props.node_type == 'org' &&
			(item.values.pk_org && item.values.pk_group) &&
			item.values.pk_org.value == item.values.pk_group.value
		) {
			this.props.form.setFormItemsDisabled(categoryConst.categoryFormId, { enablestate: true });
		} else {
			this.props.form.setFormItemsDisabled(categoryConst.categoryFormId, { enablestate: false });
		}
		

		//调整meta
		modifiyMeta(this.props);
	}



	showOffChange = () => {
		this.setState(
			{
				isShowOff: !this.state.isShowOff
			},
			this.getTreeData.bind(this,this.state.mainorg.refpk)
		);
	};

	//删除事件
	delClick = (item) => {
		this.onSelect(item.refpk, item, true);
		//设置选中节点高亮
		this.props.syncTree.setNodeSelected(categoryConst.categoryTreeId,item.refpk);
		this.props.item = {
			value: item
		};
		dispatcher(this.props, 'del',this);
	};

	//新增事件
	addClick = (item) => {
		this.onSelect(item.refpk, item, true);
		//设置选中节点高亮
		this.props.syncTree.setNodeSelected(categoryConst.categoryTreeId,item.refpk);
		this.props.item = {
			value: item
		};
		this.addBeforeClick(this.props);
	};

	//新增前事件，用于校验是否被引用，生成固定资产类别编码
	addBeforeClick(props) {
		let formData = props.form.getAllFormValue(categoryConst.categoryFormId);
		formData = processData(formData,props);
		let formDataCopy = JSON.parse(JSON.stringify(formData));
		if (this.props.node_type == 'org') {			
			formDataCopy.rows[0].values.pk_org = {
				value: this.state.mainorg.refpk
			};
			this.getMaxChild(props,formDataCopy);
			return;
		}

		//查询是否被引用
		ajax({
			url: '/nccloud/fa/category/queryIsUsed.do',
			data: {
				pageid: props.pagecode,
				model: {
					areaType: 'form',
					areacode: categoryConst.categoryFormId,
					rows: [ formDataCopy.rows[0] ]
				}
			},
			async: false,
			success: (res) => {
				let { data, message, success } = res;
				if (success) {
					if (data == true) {
						toast({ color: 'danger', content: getMultiLangByID('201200504A-000026')/* 国际化处理： 该资产类别已经被使用，不能增加下级*/ });
						props.syncTree.setNodeDisable(categoryConst.categoryTreeId, false);//恢复树的可编辑性
						return;
					}
					
					this.getMaxChild(props,formDataCopy);
				}
			}
		});
	}

	getMaxChild(props,formDataCopy){
		//获取最大子节点
		ajax({
			url: '/nccloud/fa/category/getMaxChild.do',
			data: {
				pageid: props.pagecode,
				userjson: props.node_type,
				model: {
					areaType: 'form',
					areacode: categoryConst.categoryFormId,
					rows: [ formDataCopy.rows[0] ]
				}
			},
			async: false,
			success: (res) => {
				let { data, message, success } = res;
				if (success) {
					
					this.props.cate_code = {
						value: data
					};															

					if (this.props.node_type == 'org') {
						this.props.pk_org = {
							value: this.state.mainorg.refpk
						};
					}
					let _this = this;
					dispatcher(this.props, 'add', _this);
				}
			}
		});
	}


	//编辑事件
	editClick = (item) => {
		this.onSelect(item.refpk, item, true);
		this.props.item = {
			value: item
		};
		dispatcher(this.props, 'edit');
	};

	//选择财务组织
	onOrgChange(value) {
		let changeValue = value;
		if(!value.refpk){
			changeValue = value[0];
		}
		
		if(changeValue){
			this.setState({ mainorg: changeValue }, () => {
				this.getTreeData(changeValue.refpk);
			});
		}
		
	}

	//按钮名称
	getButtonNames = (codeId) => {
		switch (codeId) {
			case 'Save':
				return 'main-button';
			case 'Cancel':
			default:
				return 'secondary - button';
		}
	};

	render() {
		const { button, form, syncTree, ncmodal, DragWidthCom } = this.props;

		const { createForm } = form;

		const { createButtonApp } = button;
		let { createSyncTree } = syncTree;
		let { createModal } = ncmodal;

		let status = this.props.form.getFormStatus(categoryConst.categoryFormId);
		
		let _this = this;
		let { mainorg } = this.state;
		let mainorgobj = {
			disabled: status == 'edit',
			onChange: (value) => {
				this.onOrgChange(value);
			},
			defaultValue: mainorg,
			refcode: this.props.orgRefCode,
			queryCondition: () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			}
		};

		return (
			<div id="fa-fabase-category" class="nc-bill-tree-card">
				{/* 头部 header*/}
				{createModal('modal')}
				<div className="header">
					{createPageIcon()}
					{/* 标题 title*/}
					<div className="title">{this.props.title}</div>
					{this.props.node_type == 'org' ? (
						<div className="search-box">
							{this.props.node_type == 'org' && (
								<AMRefer className="ref title-search-detail" config={mainorgobj} />
							)}
						</div>
					) : (
						''	
					)}
					<span className="showOff">
						<NCCheckbox
							className="showHideBtn"
							checked={this.state.isShowOff}
							onChange={this.showOffChange}
							disabled={this.props.form.getFormStatus(categoryConst.categoryFormId) == 'edit'}
							size="lg"
						>
							{getMultiLangByID('amcommon-000004')/* 国际化处理： 显示停用*/}
						</NCCheckbox>
					</span>
					{/* 按钮组 btn-group*/}
					<div className=" btn-group">
						{/* {buttons.map((v) => {
							return createButton(v.btncode, {
								name: v.btnname,
								onButtonClick: dispatcher,
								visible: false,
								buttonColor: this.getButtonNames(v.btncode)
							});
						})} */}
						{createButtonApp({
							area: 'card_head',
							buttonLimit: 3,
							onButtonClick: dispatcher.bind(this), //TODO 
							popContainer: document.querySelector('.btn-group')
						})}
					</div>
				</div>				
				{/* 树卡区域 */}
				<div className="tree-card">
					<DragWidthCom
						// 左树区域
						leftDom={
							<div className="tree-area">
								{createSyncTree({
									treeId: categoryConst.categoryTreeId,
									needSearch: true,
									needEdit: this.state.editTree,
									onSelectEve: this.onSelect.bind(this),
									onMouseEnterEve: this.onMouseEnterEve.bind(this),
									clickEditIconEve: this.editClick.bind(this), //编辑点击 回调
									clickAddIconEve: this.addClick.bind(this), //新增点击 回调
									clickDelIconEve: this.delClick.bind(this), // 删除点击 回调
									showLine: true,									
									showModal: false,
									searchType: 'filtration',									
									disabledSearch: status == 'edit' ? true : false
								})}
							</div>
						}
						rightDom={
							<div className="card-area">
								{createForm(categoryConst.categoryFormId, {
									onAfterEvent: handleAfterEditEvent.bind(this),
									onBeforeEvent: beforeEvent.bind(this)
								})}
							</div>
						}
						defLeftWid="21%" // 默认左侧区域宽度，px/百分百
					/>
				</div>
			</div>
		);
	}
}

export default Category;
