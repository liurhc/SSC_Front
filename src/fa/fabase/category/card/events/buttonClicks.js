import { categoryConst ,buttonsLayout, categoryUrl,RootDisableButton } from '../constant';
import {modifiyMeta} from './afterEvent.js';
import { ajax, toast ,print,output} from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils,components } = ampub;
const { LoginContext } = components;
const { loginContextKeys, getContext} = LoginContext
const { multiLangUtils,msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showConfirm, showMessage,MsgConst} = msgUtils;

//新增事件
function addClick(props,id,_this) {
	//设置编辑状态
	props.form.setFormItemsDisabled(categoryConst.categoryFormId, { extensionasset_flag: false });
	props.form.setFormItemsDisabled(categoryConst.categoryFormId, { enablestate: true });
	props.syncTree.setNodeDisable(categoryConst.categoryTreeId, true);
	delete props.form.getAllFormValue(categoryConst.categoryFormId).rows[0].values.upload_flag;
	delete props.form.getAllFormValue(categoryConst.categoryFormId).rows[0].values.dr;
	delete props.form.getAllFormValue(categoryConst.categoryFormId).rows[0].values.transi_type;
	let node = props.form.getAllFormValue(categoryConst.categoryFormId).rows[0];
	let item = node.values;
	if (props.node_type == 'org') {
		item['pk_org'] = {
			value: props.pk_org.value
		};
	} 
	
	if (!item.hasOwnProperty('pk_category')) {
		item['pk_category'] = {
			value: '~'
		};
	}
	//设置默认值
	let formData = setDefaultValue(props,item,_this);

	modifiyMeta(props);//新增恢复默认模板
	props.form.EmptyAllFormValue(categoryConst.categoryFormId);
	props.form.setAllFormValue(formData);
	props.button.setButtonsVisible(buttonsLayout.edit);
	props.form.setFormStatus(categoryConst.categoryFormId, 'edit');

	//修正由于选择数据后编辑界面的逻辑值
	modifyForEdit(props);
	//编码可编辑
	props.form.setFormItemsDisabled(categoryConst.categoryFormId,{
		cate_code: false
	})
}

//编辑事件;
export function editClick(props) {
	if (!dataValidator(props, getMultiLangByID('201200504A-000004')/* 国际化处理： 修改*/)) {
		props.syncTree.setNodeDisable(categoryConst.categoryTreeId, false);//恢复树的可编辑性
		return;
	}
	props.form.setFormItemsDisabled(categoryConst.categoryFormId, { extensionasset_flag: false });
	props.form.setFormItemsDisabled(categoryConst.categoryFormId, { enablestate: true });
	props.button.setButtonsVisible(buttonsLayout.edit);
	props.form.setFormStatus(categoryConst.categoryFormId, 'edit');
	props.syncTree.setNodeDisable(categoryConst.categoryTreeId, true);
	//修正由于选择数据后编辑界面的逻辑值
	modifyForEdit(props);
	//编码不可编辑
	let selectNode = props.item.value;	
	if(selectNode.children || queryIsUsed.call(this,props)){
		props.form.setFormItemsDisabled(categoryConst.categoryFormId,{
			cate_code: true
		});
	}else {
		props.form.setFormItemsDisabled(categoryConst.categoryFormId,{
			cate_code: false
		});
	}
}

//查询是否被引用
function queryIsUsed(props){
	let formData = props.form.getAllFormValue(categoryConst.categoryFormId);
	formData = processData(formData,props);
	let formDataCopy = JSON.parse(JSON.stringify(formData));	
	if (props.node_type == 'org') {			
		formDataCopy.rows[0].values.pk_org = {
			value: this.state.mainorg.refpk
		};				
	}
	let isUsed = false ;	
	ajax({
		url: '/nccloud/fa/category/queryIsUsed.do',
		data: {
			pageid: props.pagecode,
			model: {
				areaType: 'form',
				areacode: categoryConst.categoryFormId,
				rows: [ formDataCopy.rows[0] ]
			}
		},
		async: false,
		success: (res) => {
			let { data, message, success } = res;
			if (success) {			
				isUsed = data;		
			}
		},
		error : () =>{
			isUsed = false;
		}
	});
	return isUsed;
}

//修正由于选择数据后编辑界面的逻辑值改变
function modifyForEdit(props){
	let item = props.form.getAllFormValue(categoryConst.categoryFormId).rows[0];
	let enable = 'enablestate';
	// display有值的时候switch会是开启状态
	delete item.values[enable].display;
	if (item.values[enable].value == true) {
		item.values[enable].value = '2';
	} else {
		item.values[enable].value = '3';
	}

	let formData = {
		[categoryConst.categoryFormId]: {
			rows: [
				{
					values: JSON.parse(JSON.stringify(item.values))
				}
			]
		}
	};
	props.form.setAllFormValue(formData);

	//全局管控模式 
	if(props.node_type == "global"){
		//递延资产不可编辑						
		props.form.setFormItemsDisabled(categoryConst.categoryFormId,{
			extensionasset_flag: true
		});
	}	
}
//获取当前节点层级
function getCurrentCateLevel(props,id,level = 0){

	//当前节点为根节点返回
	if(!id || id == '~' ){
		return level;
	}
	level = level + 1;
	let parentItem = props.syncTree.getSyncTreeValue(categoryConst.categoryTreeId,id);	
	//向上查找上一层级	
	return getCurrentCateLevel(props,parentItem.pid,level);
}
function setDefaultValue(props,item,_this){


	let formData = {
		[categoryConst.categoryFormId]: {
			rows: [
				{
					values: {
						pk_parent: {
							value: item.pk_category.value,
							display: item.pk_category.display
						},
						enablestate: {
							value: true
						},
						depattr: {
							value: '1',
							display: getMultiLangByID('201200504A-000005')/* 国际化处理： 正常计提*/
						},
						pk_transitype: {
							display: _this.initData.transtype_00_init.display,
							value: _this.initData.transtype_00_init.value
						},
						pk_org: {
							value: item.pk_org.value,
							display: item.pk_org.display
						},
						pk_group: {
							value: item.pk_group.value,
							display: item.pk_group.display
						},
						cate_code: {
							value: props.cate_code.value
						},
						pk_depmethod: {
							value: _this.initData.depmethod_default_init.value,
							display: _this.initData.depmethod_default_init.display
						}
					}
				}
			]
		}
	};

	if(item.pk_category.value != ""){
		//上级节点不是根节点
		formData = {
			[categoryConst.categoryFormId]: {
				rows: [
					{
						values: {
							pk_parent: {
								value: item.pk_category.value,
								display: item.pk_category.display
							},
							enablestate: {
								value: true
							},
							depattr: {
								value: item.depattr.value,
								display: item.depattr.display
							},
							unit: {
								value: item.unit.value,
								display: item.unit.display
							},
							salvagerate: {
								value: item.salvagerate.value,
								display: item.salvagerate.display
							},
							pk_depmethod: {
								value: item.pk_depmethod.value,
								display: item.pk_depmethod.display
							},
							serviceyear_display: {
								value: item.serviceyear_display.value,
								display: item.serviceyear_display.display
							},
							serviceperiod_display: {
								value: item.serviceperiod_display.value,
								display: item.serviceperiod_display.display
							},
							pk_transitype: {
								value: item.pk_transitype.value,
								display: item.pk_transitype.display
							},
							extensionasset_flag : {
								value: item.extensionasset_flag.value,
								display: item.extensionasset_flag.display
							},
							pk_org: {
								value: item.pk_org.value,
								display: item.pk_org.display
							},
							pk_group: {
								value: item.pk_group.value,
								display: item.pk_group.display
							},
							cate_code: {
								value: props.cate_code.value
							},
							cate_name:{
								value: item.cate_name.value,
								display: item.cate_name.display
							},
							cate_name2:{
								value: item.cate_name2.value,
								display: item.cate_name2.display
							},
							cate_name3:{
								value: item.cate_name3.value,
								display: item.cate_name3.display
							},
							cate_name4:{
								value: item.cate_name4.value,
								display: item.cate_name4.display
							},
							cate_name5:{
								value: item.cate_name5.value,
								display: item.cate_name5.display
							},							
							cate_name6:{
								value: item.cate_name6.value,
								display: item.cate_name6.display
							}
						}
					}
				]
			}
		};
	}

	return formData;
}

//删除事件
function delClick(props,item,_this) {
	if (!dataValidator(props, getMultiLangByID('msgUtils-000000')/* 国际化处理： 删除*/)) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delReal });
}
//确认删除
function delReal(props){
	let formData = props.form.getAllFormValue(categoryConst.categoryFormId);
	formData = processData(formData,props);
	ajax({
		url: categoryUrl.del,
		data: {
			pageid: props.pagecode,
			model: {
				areaType: 'form',
				areacode: categoryConst.categoryFormId,
				rows: [ formData.rows[0] ]
			}
		},
		success: (res) => {
			let { data, success, message } = res;
			delete props.form.getAllFormValue(categoryConst.categoryFormId).rows[0].values.upload_flag;
			delete props.form.getAllFormValue(categoryConst.categoryFormId).rows[0].values.transi_type;
			delete props.form.getAllFormValue(categoryConst.categoryFormId).rows[0].values.dr;
			if (success) {
				props.syncTree.delNodeSuceess(
					categoryConst.categoryTreeId,
					formData.rows[0].values.pk_category.value
				);
				props.form.EmptyAllFormValue(categoryConst.categoryFormId);
				props.form.setFormItemsDisabled(categoryConst.categoryFormId, { enablestate: true });				
				showMessage.call(this,props,{type: MsgConst.Type.DeleteSuccess});
			}
			
		}
	});
}

//保存
function saveClick(props) {

	//表单必输项校验
	if (!props.form.isCheckNow(categoryConst.categoryFormId)) {
		return;
	}
	let formData = props.form.getAllFormValue(categoryConst.categoryFormId);
	if (formData.rows[0].values['salvagerate'].value < 0 || formData.rows[0].values['salvagerate'].value > 100) {
		toast({ color: 'danger', content: getMultiLangByID('201200504A-000007')/* 国际化处理： 净残值率输入值必须介于[0,100]*/ });
		return;
	}
	if (
		formData.rows[0].values['serviceyear_display'].value == '0' &&
		(formData.rows[0].values['serviceperiod_display'].value == '0' 
		|| formData.rows[0].values['serviceperiod_display'].value == ''
		|| formData.rows[0].values['serviceperiod_display'].value == null)
	) {
		toast({ color: 'danger', content: getMultiLangByID('201200504A-000008')/* 国际化处理： 使用月限应该大于0! */ });
		return;
	}
	if (
		formData.rows[0].values['serviceperiod_display'].value < 0 ||
		formData.rows[0].values['serviceperiod_display'].value > 12
	) {
		toast({ color: 'danger', content: getMultiLangByID('201200504A-000009')/* 国际化处理： 使用月限（月）输入值必须0-12的整数*/ });
		return;
	}
	//校验非全局级的资产类别交易类型是否为空
	if(props.node_type != 'global' &&!formData.rows[0].values['pk_transitype'].value){
		toast({ color: 'danger', content: getMultiLangByID('201200504A-000010')/* 国际化处理： [交易类型]不能为空!*/ });
		return;
	}
	// 校验资产类别编码
	let checkCodeMsg = checkForCode.call(this,props);
	if(checkCodeMsg){
		toast({ color: 'danger', content: checkCodeMsg });
		return;
	}
	let actionType;
	let actionCallback;
	if (
		formData.rows[0].values.hasOwnProperty('pk_category') &&
		formData.rows[0].values['pk_category'].value &&
		formData.rows[0].values['pk_category'].value != ''
	) {
		actionType = 'edit';
		actionCallback = updateCallback;
	} else {
		actionType = 'add';
		actionCallback = insertCallback;
	}
	formData = processData(formData,props);

	// 保存前执行验证公式
    props.validateToSave({
		pageid: props.pagecode,
		model: {
			areaType: 'form',
			areacode: categoryConst.categoryFormId
			,
			rows: [ formData.rows[0] ]
		}
	}, () => {
        // 调用Ajax保存数据
		ajax({
			url: categoryUrl[actionType],
			data: {
				pageid: props.pagecode,
				model: {
					areaType: 'form',
					areacode: categoryConst.categoryFormId,
					rows: [ formData.rows[0] ]
				}
			},
			success: (res) => {
				actionCallback(props, res);
				props.form.setFormStatus(categoryConst.categoryFormId, 'browse');
				props.button.setButtonsVisible(buttonsLayout.default);
				props.form.setFormItemsDisabled(categoryConst.categoryFormId, { enablestate: false });
				showMessage.call(this,props,{type: MsgConst.Type.SaveSuccess});
			}
		});
    });
	
}
// 校验编码
function checkForCode(props){
	let formData = props.form.getAllFormValue(categoryConst.categoryFormId);	
	//获取当前节点编码
	let cate_code = formData.rows[0].values.cate_code.value;
	//父节点pk
	let pk_parent = formData.rows[0].values.pk_parent.value;
	//获取父节点code
	let parentNode = props.syncTree.getSyncTreeValue(categoryConst.categoryTreeId,pk_parent);
	//当前节点的上一级不为根节点
	if(pk_parent && parentNode){
		let parent_code = parentNode.refcode;
		if(parent_code){
			var startWithReg= new RegExp("^"+parent_code); 
			if(!startWithReg.test(cate_code)){
				return getMultiLangByID('201200504A-000018')/* 国际化处理： 该资产类别编码不正确，下级编码应该继承上级编码 \n*/;
			}
		}	
	}else if(!pk_parent) {
		//上级节点为根节点
		let rootNode = props.syncTree.getSyncTreeValue(categoryConst.categoryTreeId,'~');
		if(rootNode && rootNode.children && rootNode.children.length > 0){
			//获取同级编码的长度
			let codelength = rootNode.children[0] && rootNode.children[0].refcode.length;
			if(cate_code && cate_code.length != codelength){
				return getMultiLangByID('201200504A-000018')/* 国际化处理： 该资产类别编码不正确，下级编码应该继承上级编码 \n*/;
			}
		}
	}
	var reg = /^[0-9a-zA-Z]+$/;
	if(!reg.test(cate_code)){
		return getMultiLangByID('201200504A-000019')/* 国际化处理： 类别编码输入不合法，编码只能输入字母和数字  \n */;
	}
}
//取消
function cancelClick(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancelReal });
}
//确认取消
function cancelReal(props){
	let formValues = props.form.getAllFormValue(categoryConst.categoryFormId);

	props.form.cancel(categoryConst.categoryFormId);
	props.button.setButtonsVisible(buttonsLayout.default);
	let item = props.form.getAllFormValue(categoryConst.categoryFormId).rows[0].values;

	
	let pk = formValues.rows[0].values.pk_category.value;
	let checkForEdit = false;
	if (pk == '') {
		//新增取消默认显示上一级数据
		let parent_id = formValues.rows[0].values.pk_parent.value;
		checkForEdit = (parent_id == '');
		let parentValues = props.syncTree.getSyncTreeValue(categoryConst.categoryTreeId, parent_id == '' ? '~':parent_id);
		setFormValueForItem(parent_id == '' ? '~':parent_id, parentValues, true, props);
	} else {
		//如果是修改 则恢复之前数据
		let currentValues = props.syncTree.getSyncTreeValue(categoryConst.categoryTreeId, pk);
		setFormValueForItem(pk, currentValues, true, props);
	}

	if (item.enablestate.value == 2) {
		props.button.setButtonsVisible(buttonsLayout.disable);
	} else {
		props.button.setButtonsVisible(buttonsLayout.enable);
	}


	modifiyMeta(props);

	//取消树为编辑态
	props.syncTree.setNodeDisable(categoryConst.categoryTreeId, false);

	//设置启用状态为可编辑
	if(!checkForEdit){
		props.form.setFormItemsDisabled(categoryConst.categoryFormId, { enablestate: false });
	}else {
		//根节点不可编辑
		props.form.setFormItemsDisabled(categoryConst.categoryFormId, { enablestate: true });
	}
}

//启用
function enableClick(props) {
	let formData = props.form.getAllFormValue(categoryConst.categoryFormId);
	formData = processData(formData,props);
	
	//启用校验
	let parent_id = formData.rows[0].values.pk_parent.value;
	if(parent_id){
		let parentValues = props.syncTree.getSyncTreeValue(categoryConst.categoryTreeId, parent_id == '' ? '~':parent_id);
		if(parent_id != "" && (!parentValues.values.enablestate.value || parentValues.values.enablestate.value==3)){
			//上级节点时启用状态 ,当前节点不可启用
			toast({ color: 'danger', content: getMultiLangByID('201200504A-000011')/* 国际化处理： 上级已被停用 , 不能对该档案进行启用*/ });
			//恢复停用状态
	
			props.form.setFormItemsValue(categoryConst.categoryFormId,{'enablestate':{value:false,display:null}});
			return;
		}
	}
	
	
	ajax({
		url: categoryUrl.enable,
		data: {
			pageid: props.pagecode,
			userjson: props.node_type,
			model: {
				areaType: 'form',
				areacode: categoryConst.categoryFormId,
				rows: [ formData.rows[0] ]
			}
		},
		success: (res) => {
			let { data, message, success } = res;
			if (success) {
				let treeData = form2tree(data);
				props.syncTree.editNodeSuccess(categoryConst.categoryTreeId, treeData);
				props.button.setButtonsVisible(buttonsLayout.disable);
				setFormValue(data[categoryConst.categoryFormId].rows[0].values.pk_category.value, data, null, props);

				showMessage.call(this,props,{type: MsgConst.Type.EnableSuccess});
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: getMultiLangByID('201200504A-000012')/* 国际化处理： 数据已被修改，请刷新界面*/ });
		}
	});
}

/**
 * 改变停启用提示信息
 * @param {*} props 
 */
export function changeEnableTips(props) {
	let validateRes = {};
	validateRes.valid = true; //校验是否通过,false是不通过，true是通过
	validateRes.msg = ''; //校验信息
	let formData = props.form.getAllFormValue(categoryConst.categoryFormId);
	//停用校验
	let parent_id = formData.rows[0].values.pk_category.value;
	if (parent_id) {
		let parentValues = props.syncTree.getSyncTreeValue(categoryConst.categoryTreeId, parent_id == '' ? '~':parent_id);
		checkChildrenStatus(parentValues,validateRes);		
	}
	if (validateRes.valid) {
		props.form.setFormPopConfirmSwitchTips(categoryConst.categoryFormId, 'enablestate', [
			getMultiLangByID('msgUtils-000012') /* 国际化处理： 是否确认启用？*/,
			getMultiLangByID('msgUtils-000013') /* 国际化处理： 是否确认停用？*/
		]);
	} else {
		props.form.setFormPopConfirmSwitchTips(categoryConst.categoryFormId, 'enablestate', [
			getMultiLangByID('msgUtils-000012') /* 国际化处理： 是否确认启用？*/,
			validateRes.msg
		]);
	}
}


//停用操作
function disableClick(props){
	let formData = props.form.getAllFormValue(categoryConst.categoryFormId);
	formData = processData(formData,props);

	ajax({
		url: categoryUrl.disable,
		data: {
			pageid: props.pagecode,
			userjson: props.node_type,
			model: {
				areaType: 'form',
				areacode: categoryConst.categoryFormId,
				rows: [ formData.rows[0] ]
			}
		},
		success: (res) => {
			let { data, message, success } = res;
			if (success) {

				res.data[categoryConst.categoryFormId].rows.map((row) => {

					let backData = row;
					delete backData.values.dr;
					let currentNode = props.syncTree.getSyncTreeValue(categoryConst.categoryTreeId, backData.values.pk_category.value);
					currentNode.values = backData.values;
					//修改树节点
					props.syncTree.editNodeSuccess(categoryConst.categoryTreeId, currentNode);
					//只刷新当前编辑节点表格数据
					if(formData.rows[0].values.pk_category.value 
						== backData.values.pk_category.value){
						props.button.setButtonsVisible(buttonsLayout.enable);
						setFormValue(data[categoryConst.categoryFormId].rows[0].values.pk_category.value, data, null, props);
					}
				})
				showMessage.call(this,props,{type: MsgConst.Type.DisableSuccess});
			}
		}
	});
}

//选择树上的数据
function chooseTree(childrens, treeData) {
	if(childrens != null){
	childrens.forEach((children) => {
		// 遍历所有节点，如果节点有子树，递归修改
		if (children.hasOwnProperty('children')) {
			chooseTree(children.children, treeData);
		}
		if (children.refpk == treeData.refpk) {
			if (children.hasOwnProperty('children')) {
				let childrens = new Array();
				childrens = children.children;
				//递归调用修改数据
				traversalTree(childrens);
				return;
			}
		}
	});
}
}

//遍历修改树上的数据
function traversalTree(children) {
	if(children != null){
	children.forEach((children) => {
		if (children.hasOwnProperty('children')) {
			// 遍历所有节点，如果节点有子树，递归修改
			traversalTree(children.children);
		}
		// 修改单据状态为停用
		children.values.enablestate.value = false;
	});
}
}

//保存返回值处理
function insertCallback(props, res) {
	let { data, success, message } = res;
	let treeData = form2tree(data);
	props.form.setFormStatus(categoryConst.categoryFormId, 'browse');
	props.syncTree.addNodeSuccess(categoryConst.categoryTreeId, treeData);
	props.syncTree.setNodeDisable(categoryConst.categoryTreeId, false);
	//更新表单数据
	setFormValue(data[categoryConst.categoryFormId].rows[0].values.pk_category.value, data, null, props);
	//展开父节点
	props.syncTree.openNodeByPk(categoryConst.categoryTreeId, treeData.pid);
	//选中新增节点
	props.syncTree.setNodeSelected(categoryConst.categoryTreeId,treeData.refpk);
	//设置按钮可用性
	props.button.setButtonDisabled(RootDisableButton, false);
}

//更新返回值处理
function updateCallback(props, res) {
	let { data, success, message } = res;
	let treeData = form2tree(data);
	props.form.setFormStatus(categoryConst.categoryFormId, 'browse');
	props.syncTree.editNodeSuccess(categoryConst.categoryTreeId, treeData);
	setFormValue(data[categoryConst.categoryFormId].rows[0].values.pk_category.value, data, null, props);
	props.syncTree.setNodeDisable(categoryConst.categoryTreeId, false);
}

//数据校验
function dataValidator(props, message) {
	if (props.node_type == 'org') {
		let pk_org = props.item.value.values.pk_org.value;
		let pk_group = props.item.value.values.pk_group.value;
		if (pk_org == pk_group) {
			toast({ content: getMultiLangByID('201200504A-000016')/* 国际化处理： 不能*/ + message + getMultiLangByID('201200504A-000017')/* 国际化处理： 集团级数据*/, color: 'danger' });
			props.form.setFormItemsDisabled(categoryConst.categoryFormId, { enablestate: true });
			return false;
		}
		return true;
	}
	return true;
}

//数据转换
function form2tree(data) {
	let values = data[categoryConst.categoryFormId].rows[0].values;
	if (values.dr) {
		delete values.dr;
	}
	let pid = '~';
	if(values.pk_parent.value){
		pid = values.pk_parent.value;
	}
	let treeData = {
		pid: pid,
		refname: values.cate_code.value + ' ' + getMultiLangName('cate_name',data[categoryConst.categoryFormId].rows[0]),
		refcode: values.cate_code.value,
		refpk: values.pk_category.value,
		isLeaf: false, // 新增节点肯定是叶子
		values: JSON.parse(JSON.stringify(values))
	};
	return treeData;
}

//数据加工
export function processData(formData,props) {
	if (formData.rows[0].values['pk_parent'].value == '~') {
		formData.rows[0].values['pk_parent'].value = null;
	}
	for (let key of Object.keys(formData.rows[0].values)) {
		if (typeof formData.rows[0].values[key].value == 'number') {
			formData.rows[0].values[key].value += '';
		}
		if (formData.rows[0].values[key].value == '') {
			formData.rows[0].values[key].value = null;
		}
	}
	if (formData.rows[0].values['enablestate'].value === true || formData.rows[0].values['enablestate'].value == '2') {
		formData.rows[0].values['enablestate'].value = '2';
	} else {
		formData.rows[0].values['enablestate'].value = '3';
	}
	let servicemonth_year = 0;
	let servicemonth_period = 0;
	if (formData.rows[0].values['serviceyear_display'].value) {
		servicemonth_year = parseInt(formData.rows[0].values['serviceyear_display'].value * 12);
	}
	if (formData.rows[0].values['serviceperiod_display'].value) {
		servicemonth_period = parseInt(formData.rows[0].values['serviceperiod_display'].value);
	}
	formData.rows[0].values['servicemonth'] = {
		value: parseInt(parseInt(servicemonth_year) + parseInt(servicemonth_period))
	};
	if (typeof formData.rows[0].values['servicemonth'].value == 'number') {
		formData.rows[0].values['servicemonth'].value += '';
	}
	if (!formData.rows[0].values['catelevel'].value){
		let pk_parent = formData.rows[0].values['pk_parent'].value ;
		formData.rows[0].values['catelevel'].value = getCurrentCateLevel(props,pk_parent) + '';
	}

	return formData;
}

export const buttonClicks = {
	add: addClick,
	edit: editClick,
	del: delClick,
	Save: saveClick,
	Cancel: cancelClick,
	enable: enableClick,
	disable: disableClick,
	Print : printTemp,
	Output : outputTemp,
	Refresh : refreshAction
};

/**
 * 设置表单数据
 */
export function setFormValue(data, item, isChange, props) {
	//设置表单要修改的数据
	if (data != '~') {
		let rows = new Array();
		let values = JSON.parse(JSON.stringify(item[categoryConst.categoryFormId].rows[0].values));
		if (!values.hasOwnProperty('parent_id')) {
			values.parent_id = {
				display: '',
				value: ''
			};
		}
		if (values.enablestate.value == '2') {
			values.enablestate.value = true;
		} else {
			values.enablestate.value = false;
		}

		if (values.hasOwnProperty('servicemonth')) {
			let num = parseInt(values['servicemonth'].value);
			values['serviceyear_display'] = {
				value: parseInt(num / 12)
			};
			values['serviceperiod_display'] = {
				value: num % 12
			};
			if (values['serviceyear_display'].value == 0) {
				values['serviceyear_display'].value = '0';
			}
			if (values['serviceperiod_display'].value == 0) {
				values['serviceperiod_display'].value = '0';
			}
		}
		
		
		let formData = {
			[categoryConst.categoryFormId]: {
				rows: [
					{
						values: JSON.parse(JSON.stringify(values))
					}
				]
			}
		};
		props.form.setAllFormValue(formData);
	}
}

/**
 * 设置表单item数据
 */
export function setFormValueForItem(data, item, isChange, props) {
	
	if (data != '~') {
		//设置表单要修改的数据
		let rows = new Array();
		let values = JSON.parse(JSON.stringify(item.values));
		if (!values.hasOwnProperty('parent_id')) {
			values.parent_id = {
				display: '',
				value: ''
			};
		}
		let rowValues = {
			values
		};
		rows.push(rowValues);
		let formData = {
			[categoryConst.categoryFormId]: {
				rows
			}
		};
		//给界面赋值的时候修改启用状态为数值

		if (
			formData[categoryConst.categoryFormId].rows[0].values.enablestate.value == 2 ||
			formData[categoryConst.categoryFormId].rows[0].values.enablestate.value == true
		) {
			formData[categoryConst.categoryFormId].rows[0].values.enablestate.value = true;
		} else {
			formData[categoryConst.categoryFormId].rows[0].values.enablestate.value = false;
		}
		props.form.setAllFormValue(formData);
	}else {
		props.form.EmptyAllFormValue(categoryConst.categoryFormId);
	}
}

//校验子节点启用状态
export function checkChildrenStatus (syncTreeNode,validateRes) {

	// 遍历所有节点，如果节点有子树，递归地对子树进行校验
	if(syncTreeNode.children != null){
		syncTreeNode.children.map((item) => {

			if (item.hasOwnProperty('children')) {
				checkChildrenStatus(item,validateRes);
			}
			if(item.values.enablestate.value != 3){
				if(item.values.enablestate.value || item.values.enablestate.value == 2){
					validateRes.valid = false;
					validateRes.msg = getMultiLangByID('201200504A-000014')/* 国际化处理： 该档案存在已启用的下级, 是否将所有下级同时停用？*/;
					return ;
				}
			}
		});
	}
};

export function printTemp(props) {

	let printData = getPrintData.call(this, props, 'print');
    
    if (!printData) {
        showMessage.call(this,props,{type: MsgConst.Type.ChoosePrint});
        return;
    }
    print(
        'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
        categoryUrl.print, // 后台打印服务url
        printData
    );

}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props,id,_this) {
    let printData = getPrintData.call(this, props, 'output');
    if (!printData) {
        showMessage.call(this,props,{type: MsgConst.Type.ChooseOutput});
        return;
    }
	output({
		url: categoryUrl.print,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {

	let treeData = props.syncTree.getSyncTreeValue(categoryConst.categoryTreeId);
	let pks = [];
	getTreeDatePk(treeData,pks);

	let printData = {
		filename: props.title, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType : outputType // 输出类型

	};
	return printData;
}
	
/**
 * 重新加载页面
 * @param {*} props 
 */
export function refreshAction(props,id,_this) {

	let callback = () => {
		showMessage.call(this,props,{type: MsgConst.Type.RefreshSuccess});
	};
	_this.getTreeData(_this.state.mainorg.refpk,callback);
}

//递归获取各个节点Pk
export function getTreeDatePk(treeData,pks) {
	treeData.map((node) => {
		if (node.refpk != '~') {
			pks.push(node.refpk);
		}
		if (node.children) {
			getTreeDatePk(node.children, pks);
		}
	});
};
/**
 * 获取多语字段名称 字段值 * 
 * @param {*} multiLangName 多语字段名称
 * @param {*} item 数据对象 (如果有值返回当前语种值)
 * @return item 为空时返回当前语种字段 , 有值取当前语种值
 */
export function getMultiLangName(multiLangName,item){
	let index = getContext(loginContextKeys.languageIndex);
	if(!index){
		index = '';//未加载到默认主语种
	}
	//如果items有值返回字段value
	if(item){
		//取当前语种值如果没有默认取主语种值
		let value = item.values[multiLangName + index].value;
		if(!value){
			value = item.values[multiLangName].value;
		}
		return value;
	}else{
		//返回当前语种多语字段
		return multiLangName + index;
	}
}