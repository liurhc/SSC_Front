import { buttonClicks } from './buttonClicks';
import { categoryConst } from '../constant';

/**
 * 
 * @param {*} props 
 * @param {*} id 
 */
export default function(props, id,_this) {
	let formValues = props.form.getAllFormValue(categoryConst.categoryFormId);
	if(this){
		_this = this;
	}
	buttonClicks[id](props,formValues,_this);
}
