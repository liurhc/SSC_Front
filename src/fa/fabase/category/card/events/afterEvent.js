import { categoryConst } from '../constant';
import { buttonClicks, setFormValue } from './buttonClicks';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;


export default function handleAfterEditEvent(props, moduleId, key, value, index) {
	let data = props.form.getAllFormValue(categoryConst.categoryFormId).rows[0].values;
	let values = JSON.parse(JSON.stringify(data));
	let rows = new Array();
	if (key == 'enablestate') {
		let value = values.enablestate.value;
		if (value) {
			//value表示编辑后的值（新值）
			buttonClicks.enable(props);
		} else {
			buttonClicks.disable(props);
		}
	}
	//若选择计提属性
	if (key == 'depattr') {
		let value = values.depattr.value;
		//不计提折旧
		if (value == 2) {
			//同步修改
			values.pk_depmethod = {
				display: this.initData.depmethod_init.display,
				value: this.initData.depmethod_init.value
			}

			let rowValues = {
				values
			};
			rows.push(rowValues);
			let formData = {
				[categoryConst.categoryFormId]: {
					rows
				}
			};
			props.form.setAllFormValue(formData);
		} 
		
	}
	if (key == 'extensionasset_flag') {
		let value = values.extensionasset_flag.value;
		let meta = props.meta.getMeta();
		
		if (value) {
			//修改表单值
			values.pk_transitype = {
				display: this.initData.transtype_05_init.display,
				value: this.initData.transtype_05_init.value
			};
			modifiyMeta(props);


		} else {
			//修改表单值
			values.pk_transitype = {
				display: this.initData.transtype_00_init.display,
				value: this.initData.transtype_00_init.value
			};
			modifiyMeta(props);
		}
		let rowValues = {
			values
		};
		rows.push(rowValues);
		let formData = {
			[categoryConst.categoryFormId]: {
				rows
			}
		};
		props.form.setAllFormValue(formData);
	}
}

export function modifiyMeta(props,checkFlag=true) {

	let fromdata = props.form.getAllFormValue(categoryConst.categoryFormId).rows[0].values;
	let values = JSON.parse(JSON.stringify(fromdata));

	let value = values.extensionasset_flag.value;
	let meta = props.meta.getMeta();

	let data = meta[categoryConst.categoryFormId];

	data['items'].map((item) => {
		//是否需要校验
		if(checkFlag){
			if(value){
				if (item.attrcode === 'depattr') {
					item.label = getMultiLangByID('201200504A-000000')/* 国际化处理： 摊销属性*/;
				}
				if (item.attrcode === 'pk_depmethod') {
					item.label = getMultiLangByID('201200504A-000001')/* 国际化处理： 摊销方法*/;
				}
			}else{
				if (item.attrcode === 'depattr') {
					item.label = getMultiLangByID('201200504A-000002')/* 国际化处理： 计提属性*/;
				}
				if (item.attrcode === 'pk_depmethod') {
					item.label =  getMultiLangByID('201200504A-000003')/* 国际化处理： 折旧方法*/;
				}
			}
		}else {
			if (item.attrcode === 'depattr') {
				item.label =  getMultiLangByID('201200504A-000002')/* 国际化处理： 计提属性*/;
			}
			if (item.attrcode === 'pk_depmethod') {
				item.label = getMultiLangByID('201200504A-000003')/* 国际化处理： 折旧方法*/;
			}
		}
	});
}