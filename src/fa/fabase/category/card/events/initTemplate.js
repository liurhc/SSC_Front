import { categoryConst, RootDisableButton} from '../constant';
import ampub from 'ampub';
const { utils,components } = ampub;
const { LoginContext } = components;
const { loginContextKeys, getContext,loginContext} = LoginContext;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default function(props) {
	let meta = {};
	const { node_type } = props;
	props.createUIDom(
		{
			pagecode: props.pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);					
				}
				if (data.button) {
					let button = data.button;
					if (node_type == 'org') {
						// 默认主组织
						let pk_org = getContext(loginContextKeys.pk_org);
						let org_Name = getContext(loginContextKeys.org_Name);
						if (pk_org) {
							this.onOrgChange({ refpk: pk_org, refname: org_Name });
						}
					}

					props.button.setButtons(button);
					props.button.setButtonDisabled(RootDisableButton, true);
				}
				if (data.template) {
					meta = data.template;
					modifiy(meta[categoryConst.categoryFormId]);
					props.meta.setMeta(meta,()=>{
						if (props.node_type == 'org') {
							this.getTreeData(this.state.mainorg.refpk);
						} else {
							let link_id = props.getUrlParam('id');
							//如果link_id有值
							if(link_id){
								//增加跳转id用以数据加载完毕后进行树节点定位且只执行一次
								this.link_id = link_id;
							}
							this.getTreeData(null);
						}
					});
				}
				
			}
		}
	);
}
function modifiy(data) {
	data['status'] = 'browse';
	data['items'].map((item) => {
		if (item.itemtype === 'input') {
			if(!item.initialvalue){
				item.initialvalue = {
					value: '',
					display: ''
				};
			}
		}
		if (item.attrcode == 'pk_transitype') {
			item.queryCondition = () => {
				return {
					parentbilltype: 'H1'
				}; // 根据单据类型过滤
			};
		}
		if (item.attrcode == 'enablestate') {
            item.onPopconfirmCont = getMultiLangByID('msgUtils-000012') /* 国际化处理： 是否确认启用？*/;
            item.offPopconfirmCont = getMultiLangByID('msgUtils-000013') /* 国际化处理： 是否确认停用？*/;
        }
	});
}
