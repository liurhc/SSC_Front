import { processData, setFormValue,getMultiLangName } from './buttonClicks';
import dispatcher from './buttonClickDispatcher';
import handleAfterEditEvent,{modifiyMeta} from './afterEvent';
import beforeEvent from './beforeEvent';
import initTemplate from './initTemplate';

export { dispatcher, handleAfterEditEvent,beforeEvent, processData, initTemplate, setFormValue ,modifiyMeta,getMultiLangName};
