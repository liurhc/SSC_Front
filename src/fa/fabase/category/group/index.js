import React from 'react';
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import Category from '../card';
import { categoryConst } from '../card/constant';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule, getMultiLangByID } = multiLangUtils;

/**
 * 固定资产类别--集团
 * xiaodi3
 * 2018-5-8
 */
const nodeCode = '2012004020'; //功能编码
const pagecode = '201200504A_tree';
const appcode = '201200504A';
const node_type = 'group';
const title = '201200504A-000029'/* 国际化处理： 固定资产类别-集团*/;

const CategoryBase = createPage({
	billinfo: {
		billtype: 'form',
		pagecode: pagecode,
		headcode: categoryConst.categoryFormId
	}
})(Category);

let moduleIds = { fa: [ '201200504A' ], ampub: [ 'common' ] };
initMultiLangByModule(moduleIds, () => {
	ReactDOM.render(
		<CategoryBase title={getMultiLangByID(title)/* 国际化处理： 固定资产类别-集团*/} nodeCode={nodeCode} pagecode={pagecode} appcode={appcode} node_type={node_type} />,
		document.querySelector('#app')
	);
});
