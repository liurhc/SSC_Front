import React from 'react';
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import Category from '../card';
import { categoryConst } from '../card/constant';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule, getMultiLangByID } = multiLangUtils;

/**
 * 固定资产类别--组织
 * xiaodi3
 * 2018-5-8
 */
const nodeCode = '2012004022'; //功能编码
const pagecode = '201200505A_tree';
const appcode = '201200505A';
const node_type = 'org';
const title ='201200504A-000030'/* 国际化处理： 固定资产类别-组织*/;
const orgRefCode = 'uapbd/refer/org/FinanceOrgTreeRef/index.js';

const CategoryBase = createPage({
	billinfo: {
		billtype: 'form',
		pagecode: pagecode,
		headcode: categoryConst.categoryFormId
	}
})(Category);

let moduleIds = { fa: [ '201200504A' ], ampub: [ 'common' ] };
initMultiLangByModule(moduleIds, () => {
	ReactDOM.render(
		<CategoryBase
			orgRefCode={orgRefCode}
			title={getMultiLangByID(title)/* 国际化处理： 固定资产类别-组织*/}
			nodeCode={nodeCode}
			pagecode={pagecode}
			appcode={appcode}
			node_type={node_type}
		/>,
		document.querySelector('#app')
	);
});
