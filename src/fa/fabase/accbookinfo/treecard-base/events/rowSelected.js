import { tableId } from '../const';

/**
 * 行选中事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} record 
 * @param {*} index 
 * @param {*} status 
 */
export default function(props, moduleId, record, index, status) {
	//处理表体删行按钮
	let checkedRows = [];
	let numcheck = props.editTable.getNumberOfRows(tableId, false);
	if (numcheck > 0) {
		checkedRows = props.editTable.getCheckedRows(tableId);
	}
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}
