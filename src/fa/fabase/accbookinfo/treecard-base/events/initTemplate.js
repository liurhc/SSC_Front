import { tableId } from '../const';
import { high } from 'nc-lightapp-front';
import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { loginContextKeys, getContext,loginContext} = LoginContext;
const { FormulaEditor } = high;

export default function(props) {
	const { nodetype } = props;
	props.createUIDom(
		{
			pagecode: props.pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					if (nodetype == 'org') {
						// 默认主组织
						let pk_org = getContext(loginContextKeys.pk_org);
						let org_Name = getContext(loginContextKeys.org_Name);
						if (pk_org) {
							this.onOrgChange({ refpk: pk_org, refname: org_Name });
						}
					}
					props.button.setButtons(button);
				}
			}
		}
	);
}

//修改meta
const modifierMeta = (props, meta) => {
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_category') {
			item.onlyLeafCanSelect = false;
		}
	});
	return meta;
};
