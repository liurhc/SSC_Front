/**
 * 编辑后事件
 * @param {*} props 
 * @param {*} id 
 * @param {*} key 
 * @param {*} value 
 * @param {*} data 
 * @param {*} index 
 */
import { toast, ajax } from 'nc-lightapp-front';
import { tableId, formId, url } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { msgUtils } = utils;
const { showConfirm, showMessage,MsgConst} = msgUtils;
export default function(props, moduleId, key, value, changedrows, index, record, type, method, flag = false) {
	/**
	 * 操作行号时，不触发编辑后
	 */
	if (key == 'crowno') {
		return;
	}
	if (
		(typeof value == 'object' && (value.length || value.value || value.refpk)) ||
		(typeof value == 'string' && value)
	) {
		//卡片类别调用后台带出相关字段
		if (key == 'pk_category') {
			let pagecode = props.pagecode;
			//因为这里树卡表使用了edittable，不能直接使用平台封装的主子表方法，所以手动拼接json
			let cardData = {
				pageid: props.pagecode,
				userJson: props.nodetype,
				head: {},
				body: {}
			};
			let metaObj = props.meta.getMeta();
			if (metaObj[formId] && metaObj[formId].moduletype && metaObj[formId].moduletype === 'form') {
				cardData.head[formId] = props.form.getAllFormValue(formId);
				cardData.head[formId].areacode = formId;
			}
			if (metaObj[tableId] && metaObj[tableId].moduletype && metaObj[tableId].moduletype === 'table') {
				cardData.body[tableId] = props.editTable.getAllData(tableId);
				cardData.body[tableId].areacode = tableId;
			}

			let data = {
				attrcode: key,
				changedrows: changedrows,
				card: cardData,
				index: index
			};
			if (props.nodetype == 'org') {
				let userJsonMap = { pk_org: this.state.pkOrg };
				data.userJson = JSON.stringify(userJsonMap);
			}
			ajax({
				url: url.bodyAfterEditURL,
				data: data,
				success: (res) => {
					if (res.data && res.data.body && res.data.body[tableId] && res.data.body[tableId].rows) {
						let rows = res.data.body[tableId].rows;

						props.editTable.setTableData(moduleId, { rows: [] }, false);
						props.editTable.setTableData(moduleId, { rows: rows }, false);
						let userObj = JSON.parse(res.data.userjson);
						if (userObj && userObj.msg) {
							showMessage(props, { content: userObj.msg, color: 'warning' });
						}
					}
				},
				error: (res) => {
					props.editTable.deleteTableRowsByIndex(tableId, index);
					if (props.nodetype == 'org') {
						let allRows = props.editTable.getAllData(tableId).rows;
						for (let i = 0; i < allRows.length; i++) {
							let row = allRows[i];
							let rowId = row.rowid;
							let rowValues = row.values;
							if (rowValues.pk_group && rowValues.pk_org) {
								if (rowValues.pk_group.value == rowValues.pk_org.value) {
									props.editTable.setEditableByKey(tableId, rowId, 'pk_category', false);
								}
							}
						}
					}
					showMessage(props, { content: res.message, color: 'warning' });
				}
			});
		}
	}
}
