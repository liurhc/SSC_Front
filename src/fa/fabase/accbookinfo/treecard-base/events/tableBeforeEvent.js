import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export default function beforeEvent(props, moduleId, item, index, value, record) {
	if (
		this.props.nodetype == 'org' &&
		record.values &&
		record.values.pk_group &&
		record.values.pk_org &&
		record.values.pk_group.value === record.values.pk_org.value
	) {
		return false;
	}

	this.setState({
		editingField: moduleId,
		rowid: index
	});
	//公式字段
	if (item.attrcode === 'localoriginvalue') {
		let copyValue = JSON.parse(JSON.stringify(value));
		if (copyValue != null && copyValue.value != null) {
			if (copyValue.value.indexOf(getMultiLangByID('201200516A-000000')) != -1) {
				copyValue.value = copyValue.value.replace(getMultiLangByID('201200516A-000000'), 'originvalue');
			}
			if (copyValue.value.indexOf(getMultiLangByID('201200516A-000001')) != -1) {
				copyValue.value = copyValue.value.replace(getMultiLangByID('201200516A-000001'), 'revalued_amount');
			}
			if (copyValue.value.indexOf(getMultiLangByID('201200516A-000002')) != -1) {
				copyValue.value = copyValue.value.replace(getMultiLangByID('201200516A-000002'), 'tax_input');
			}
			if (copyValue.value.indexOf(getMultiLangByID('201200516A-000003')) != -1) {
				copyValue.value = copyValue.value.replace(getMultiLangByID('201200516A-000003'), 'other_cost');
			}
			if (copyValue.value.indexOf(getMultiLangByID('201200516A-000004')) != -1) {
				copyValue.value = copyValue.value.replace(getMultiLangByID('201200516A-000004'), 'tax_cost');
			}
			if (copyValue.value.indexOf(getMultiLangByID('201200516A-000005')) != -1) {
				copyValue.value = copyValue.value.replace(getMultiLangByID('201200516A-000005'), 'currmoney');
			}
			if (copyValue.value.indexOf(getMultiLangByID('201200516A-000006')) != -1) {
				copyValue.value = copyValue.value.replace(getMultiLangByID('201200516A-000006'), 'install_fee');
			}
			if (copyValue.value.indexOf(getMultiLangByID('201200516A-000007')) != -1) {
				copyValue.value = copyValue.value.replace(getMultiLangByID('201200516A-000007'), 'dismant_cost');
			}
		}
		//平台编辑公式界面关闭后现在不会触发编辑前(注释掉此处处理)
		//解决公式界面关闭后再次触发编辑前
		// if(this.isCloseFlag){
		// 	this.isCloseFlag = false;//重置标识符
		// 	return false;//不打开公式界面
		// }
		//显示公式编辑器
		this.setState({
			show: true,
			editingValue: copyValue == null ? '' : copyValue.value
		});
	}
	return true;
}
