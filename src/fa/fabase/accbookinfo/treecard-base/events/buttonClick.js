import { ajax, toast } from 'nc-lightapp-front';
import { formId, tableId, treePkAndCode, treeCode, url } from '../const';
import ampub from 'ampub';
const { utils,commonConst } = ampub;
const { StatusUtils} = commonConst;
const { UISTATE } = StatusUtils;
const { multiLangUtils,msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showConfirm, showMessage,MsgConst} = msgUtils;
export default function(props, id) {
	switch (id) {
		case 'Save':
			let _this = this;
			//过滤空行
			// 过滤空行
			_this.props.editTable.filterEmptyRows(tableId, [ 'pk_category' ], 'include');

			// 表体必输校验
			let allRows = props.editTable.getAllRows(tableId);

			let flag = props.editTable.checkRequired(tableId, allRows);
			if (!flag) {
				return;
			}

			//组装保存所需要的数据
			let data = {
				pageid: _this.props.pagecode,
				userjson: _this.state.mainorg && _this.state.mainorg.refpk,
				head: {},
				body: {}
			};
			let metaObj = _this.props.meta.getMeta();
			if (metaObj[formId] && metaObj[formId].moduletype && metaObj[formId].moduletype === 'form') {
				data.head[formId] = _this.props.form.getAllFormValue(formId);
				data.head[formId].areacode = formId;
			}
			if (metaObj[tableId] && metaObj[tableId].moduletype && metaObj[tableId].moduletype === 'table') {
				data.body[tableId] = _this.props.editTable.getAllData(tableId);
				data.body[tableId].areacode = tableId;
			}
			props.validateToSave(data, () => {
				//保存
				ajax({
					url: url.saveURL,
					data: data,
					success: function(res) {
						let successData = res.data;
						//更新表头表体以及树数据
						let headData = successData.head.card_head.rows;
						let body;
						if (successData.body) {
							body = successData.body.bodyvos;
						}
						if(!body){
							body = {rows: []};
						}
						//去掉表头dr字段
						headData.map((item, values) => {
							if (item.values.dr) {
								delete item.values.dr;
							}
						});

						//循环表体，去掉dr字段，保证模板与vo一致不报错
						if (body && body.rows) {
							body.rows.map((item, values) => {
								if (item.values.dr) {
									delete item.values.dr;
								}
							});
						}

						//构建新的树节点
						let child = {
							pid: treePkAndCode,
							refcode: headData[0].values.accbooktypecode.value,
							refname:
								headData[0].values.accbooktypecode.value +
								'  ' +
								headData[0].values.pk_setofbook.display,
							refpk: headData[0].values.pk_setofbook.value,
							values: successData
						};
						//更新表头form
						_this.props.form.setAllFormValue({ card_head: successData.head.card_head });
						//更新表体table
						_this.props.editTable.setTableData(tableId, body);
						//更新树的数据信息
						_this.props.syncTree.editNodeSuccess(treeCode, child);
						//设置表头表体为浏览态
						_this.props.form.setFormStatus(formId, UISTATE.browse);
						_this.props.editTable.setStatus(tableId, UISTATE.browse);
						//返回浏览态设置树可选中
						_this.props.syncTree.setNodeDisable(treeCode, false);
						_this.toggleShow(UISTATE.browse);
						//去掉表体checkbox
						_this.setState({
							checkboxVisible: false
						});
						showMessage(props, { type: MsgConst.Type.SaveSuccess });
					}
				});
			});
			break;
		case 'Edit':
			//修改表头和表体为编辑态
			this.props.form.setFormStatus(formId, UISTATE.edit);
			this.props.editTable.setStatus(tableId, UISTATE.edit);
			//编辑态设置树不可选中
			this.props.syncTree.setNodeDisable(treeCode, true);
			this.toggleShow(UISTATE.edit);

			//处理表体删行按钮
			let checkedRows = [];
			let numcheck = this.props.editTable.getNumberOfRows(tableId, false);
			if (numcheck > 0) {
				checkedRows = this.props.editTable.getCheckedRows(tableId);
			}
			props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
			break;
		case 'Cancel':
			showConfirm(props, { beSureBtnClick: beSureBtnClick, type: MsgConst.Type.Cancel });
			break;
		case 'AddLine':
			//表体增行操作

			let num = this.props.editTable.getNumberOfRows(tableId);
			this.props.editTable.addRow(tableId, num, true);
			//默认增行为最后一行，所以直接往最后一行塞值
			//进项税抵扣标志默认为勾选
			this.props.editTable.setValByKeyAndIndex(tableId, num, 'taxinput_flag', { value: true });
			//本币原值来源默认与表头相同
			let localoriginvalue = this.props.form.getFormItemsValue(formId, 'localoriginvalue');
			this.props.editTable.setValByKeyAndIndex(tableId, num, 'localoriginvalue', {
				value: localoriginvalue.value,
				display: localoriginvalue.display
			});
			if (props.nodetype == 'org') {
				this.props.editTable.setValByKeyAndIndex(tableId, num, 'pk_org', { value: this.state.pkOrg });
			}
			break;
		case 'DelLine': //表体删行操作
			let index = this.props.editTable.getCheckedRows(tableId);
			if (index.length == 0) {
				showMessage(props, { type: MsgConst.Type.ChooseDelete });
				break;
			}
			//当为组织级节点时，判断是否勾选的行中存在集团级数据，如果有直接抛错
			let valiCheckRow = false;
			if (props.nodetype == 'org') {
				let checkRows = this.props.editTable.getCheckedRows(tableId);
				for (let i = 0; i < checkRows.length; i++) {
					if (checkRows[i].data.values.pk_group && checkRows[i].data.values.pk_org) {
						if (
							checkRows[i].data.values.pk_group.value != null &&
							checkRows[i].data.values.pk_category.value != null &&
							checkRows[i].data.values.pk_group.value == checkRows[i].data.values.pk_org.value
						) {
							valiCheckRow = true;
						}
					}
				}
			}
			if (valiCheckRow) {
				showMessage(props, { content: getMultiLangByID('201200516A-000014'), color: 'warning' });
				break;
			}
			let delIndex = [];
			index.map((item, value) => {
				delIndex.push(item.index);
			});
			this.props.editTable.deleteTableRowsByIndex(tableId, delIndex);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'BatchAlter':
			batchAlter.call(this,props);
			break;
	}
}

export const delRow = (props, tableId, index) => {
	if (props.nodetype == 'org') {
		//待处理
	}
	props.editTable.delRow(tableId, index);
};

function beSureBtnClick(props) {
	//取消时，返回编辑前的状态
	props.form.cancel(formId);
	props.editTable.cancelEdit(tableId);
	//返回浏览态设置树可选中
	props.syncTree.setNodeDisable(treeCode, false);
	editButtion(props);
}

function editButtion(props, flag = true) {
	props.button.setButtonVisible([ 'Edit', 'Refresh' ], flag);
	props.button.setButtonVisible([ 'Save', 'Cancel', 'AddLine', 'DelLine','BatchAlter' ], !flag);
}

function refresh(props) {
	let node = props.syncTree.getSelectNode(treeCode);
	let selectedNodeId = node.refpk;
	let nodetype = props.nodetype;
	let pagecode = props.pagecode;
	let org = null;
	if (props.nodetype != 'group') {
		org = this.state.mainorg.refpk;
	}
	let treeData = null; //构造树所需数据
	let _this = this;
	//异步查询账簿信息
	ajax({
		url: url.queryTreeURL,
		data: {
			pk_org: org,
			nodetype: nodetype,
			pagecode: pagecode
		},
		success: function(res) {
			if (res.success) {
				//设置账簿信息表头
				treeData = [
					{
						refcode: treePkAndCode,
						refname: getMultiLangByID('201200516A-000010'),
						refpk: treePkAndCode,
						values: {}
					}
				];
				let successData = res.data;

				//循环后台返回数据，构建前台树所需数据
				for (let i = 0; i < successData.length; i++) {
					let rowData = successData[i];
					//循环表头，去掉dr字段，保证模板与vo一致不报错
					rowData.head[formId].rows.map((item, values) => {
						if (item.values.dr) {
							delete item.values.dr;
						}
					});
					//循环表体，去掉dr字段，保证模板与vo一致不报错
					if (rowData.body) {
						rowData.body.bodyvos.rows.map((item, values) => {
							if (item.values.dr) {
								delete item.values.dr;
							}
						});
					}
					let headData = successData[i].head[formId].rows[0].values;
					let row = {
						pid: treePkAndCode,
						refcode: headData.accbooktypecode.value,
						refname: headData.accbooktypecode.value + '  ' + headData.pk_setofbook.display,
						refpk: headData.pk_setofbook.value,
						values: rowData
					};
					treeData.push(row);
				}
				treeData = _this.createTreeData(treeData);
				_this.setSyncTreeData(treeCode, treeData);
				props.syncTree.setNodeSelected(treeCode, selectedNodeId);
				let refreshValue = null;
				if (treeData && treeData[0] && treeData[0].children) {
					let children = treeData[0].children;
					for (let i = 0; i < children.length; i++) {
						if (children[i].refpk === selectedNodeId) {
							refreshValue = children[i].values;
						}
					}
				}
				if (refreshValue) {
					props.form.setAllFormValue({ card_head: JSON.parse(JSON.stringify(refreshValue.head[formId])) });
					if (refreshValue.body) {
						props.editTable.setTableData(tableId, refreshValue.body.bodyvos);
						if (props.nodetype == 'org') {
							let allRows = props.editTable.getAllData(tableId).rows;
							for (let i = 0; i < allRows.length; i++) {
								let row = allRows[i];
								let rowId = row.rowid;
								let rowValues = row.values;
								if (rowValues.pk_group && rowValues.pk_org) {
									if (rowValues.pk_group.value == rowValues.pk_org.value) {
										props.editTable.setEditableByKey(tableId, rowId, 'pk_category', false);
									}
								}
							}
						}
					} else {
						props.editTable.setTableData(tableId, { rows: [] });
					}
				}
				showMessage(props, { type: MsgConst.Type.RefreshSuccess });
			}
		}
	});
}
function batchAlter(props) {
	let num = props.editTable.getNumberOfRows(tableId);
	if (num <= 1) {
		return;
	}

	// 获取原始数据
	let changeData = props.editTable.getTableItemData(tableId);
	//pageConfig.disableBatchFiled.indexOf(changeData.batchChangeKey) < 0
	if (true) {
		//因为这里树卡表使用了edittable，不能直接使用平台封装的主子表方法，所以手动拼接json
		let cardData = {
			pageid: props.pagecode,
			userJson: props.nodetype,
			head: {},
			body: {}
		};
		let metaObj = props.meta.getMeta();
		if (metaObj[formId] && metaObj[formId].moduletype && metaObj[formId].moduletype === 'form') {
			cardData.head[formId] = props.form.getAllFormValue(formId);
			cardData.head[formId].areacode = formId;
		}
		if (metaObj[tableId] && metaObj[tableId].moduletype && metaObj[tableId].moduletype === 'table') {
			cardData.body[tableId] = props.editTable.getAllData(tableId);
			cardData.body[tableId].areacode = tableId;
		}
		let data = {
			card: cardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		}
		if (props.nodetype == 'org') {
			let userJsonMap = { pk_org: this.state.pkOrg };
			data.userJson = JSON.stringify(userJsonMap);
		}
		ajax({
			url: url.batchAlterURL,
			data: data,
			success: (res) => {
				if (res.data && res.data.body && res.data.body[tableId] && res.data.body[tableId].rows) {
					let rows = res.data.body[tableId].rows;
					props.editTable.setTableData(tableId, { rows: rows},false);			
				}
			},
			error: (res) => {
				if (res && res.message) {
					toast({ color: 'danger', content: res.message });
				}
			}
		});
	}
}