import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { high, ajax, createPageIcon } from 'nc-lightapp-front';
import { initTemplate, buttonClick, afterEvent, beforeEvent, tableBeforeEvent, rowSelected } from './events';
import { formId, tableId, treeCode, treePkAndCode, url, bodyButtonId } from './const';
import ampub from 'ampub';
const { utils,components,commonConst } = ampub;
const { StatusUtils} = commonConst;
const { AMRefer } = components;
const { multiLangUtils,closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { UISTATE } = StatusUtils;

const { FormulaEditor } = high;

class AccbookInfo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pkOrg: '',
			mainorg: {},
			//公式编辑器涉及字段
			show: false,
			// name: '公式页面',
			editingField: '',
			editingValue: '',
			rowid: ''
		};
		let { syncTree } = this.props;
		let { setSyncTreeData, createTreeData } = syncTree;
		this.setSyncTreeData = setSyncTreeData; //设置树根节点数据方法
		this.createTreeData = createTreeData; //树组件生成所需要的数据结构方法
		closeBrowserUtils.call(this,props,{form:[formId]});
		initTemplate.call(this, props);
	}

	queryTreeData = (org) => {
		let _this = this;
		let treeData = null; //构造树所需数据
		if ((org == null || org == '') && this.props.nodetype == 'org') {
			return;
		}
		//异步查询账簿信息
		ajax({
			url: url.queryTreeURL,
			data: {
				pk_org: org,
				nodetype: this.props.nodetype,
				pagecode: _this.props.pagecode
			},
			success: function(res) {
				if (res.success) {
					//设置账簿信息表头
					treeData = [
						{
							refcode: treePkAndCode,
							refname: getMultiLangByID('201200516A-000010'),
							refpk: treePkAndCode,
							values: {}
						}
					];
					let successData = res.data;

					//循环后台返回数据，构建前台树所需数据
					for (let i = 0; i < successData.length; i++) {
						let rowData = successData[i];
						//循环表头，去掉dr字段，保证模板与vo一致不报错
						rowData.head[formId].rows.map((item, values) => {
							if (item.values.dr) {
								delete item.values.dr;
							}
						});
						//循环表体，去掉dr字段，保证模板与vo一致不报错
						if (rowData.body) {
							rowData.body.bodyvos.rows.map((item, values) => {
								if (item.values.dr) {
									delete item.values.dr;
								}
							});
						}
						let headData = successData[i].head[formId].rows[0].values;
						let row = {
							pid: treePkAndCode,
							refcode: headData.accbooktypecode.value,
							refname: headData.accbooktypecode.value + '  ' + headData.pk_setofbook.display,
							refpk: headData.pk_setofbook.value,
							values: rowData
						};
						treeData.push(row);
					}
					treeData = _this.createTreeData(treeData);
					_this.setSyncTreeData(treeCode, treeData);

					// 默认选中第一个节点
					let first = treeData[0].children[0].refpk;
					_this.props.syncTree.setNodeSelected(treeCode, first);
					_this.onSelectEve(treeData[0].children[0].refcode, treeData[0].children[0]);
				}
			}
		});
	};

	//公式处理参数
	FormulaClos = ({ setName, setExplain, name }) => {
		return (
			<ul class="tab-content">
				{
					<li
						class="tab-content-item"
						onClick={() => {
							setExplain(getMultiLangByID('201200516A-000000'));
						}}
						onDoubleClick={() => {
							setName('originvalue');
						}}
					>
						{getMultiLangByID('201200516A-000000')}
					</li>
				}
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201200516A-000001'));
					}}
					onDoubleClick={() => {
						setName('revalued_amount');
					}}
				>
					{getMultiLangByID('201200516A-000001')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201200516A-000002'));
					}}
					onDoubleClick={() => {
						setName('tax_input');
					}}
				>
					{getMultiLangByID('201200516A-000002')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201200516A-000003'));
					}}
					onDoubleClick={() => {
						setName('other_cost');
					}}
				>
					{getMultiLangByID('201200516A-000003')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201200516A-000004'));
					}}
					onDoubleClick={() => {
						setName('tax_cost');
					}}
				>
					{getMultiLangByID('201200516A-000004')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201200516A-000005'));
					}}
					onDoubleClick={() => {
						setName('currmoney');
					}}
				>
					{getMultiLangByID('201200516A-000005')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201200516A-000006'));
					}}
					onDoubleClick={() => {
						setName('install_fee');
					}}
				>
					{getMultiLangByID('201200516A-000006')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201200516A-000007'));
					}}
					onDoubleClick={() => {
						setName('dismant_cost');
					}}
				>
					{getMultiLangByID('201200516A-000007')}
				</li>
			</ul>
		);
	};

	componentDidMount() {
		//if(this.props.nodetype=='group'){
		if (this.props.nodetype == 'group') {
			this.queryTreeData(null);
		} else {
			this.queryTreeData(this.state.mainorg.refpk);
		}

		this.toggleShow(UISTATE.browse);
		//第一次进入界面时，隐藏修改按钮
		this.props.button.setButtonVisible([ 'Edit', 'Refresh' ], false);
	}

	//根据显示切换页面按钮状态
	toggleShow = (status) => {
		let flag = status === UISTATE.browse ? true : false;
		this.props.button.setButtonVisible([ 'Edit', 'Refresh' ], flag);
		this.props.button.setButtonVisible([ 'Save', 'Cancel', 'AddLine', 'DelLine','BatchAlter' ], !flag);
		//	this.props.editTable.setAllCheckboxAble(tableId, !flag);
	};

	onSelectEve(data, item, isChange) {
		let flag = true;
		//如果是根节点，则清空右侧数据,并控制修改按钮不可用
		if (data != treePkAndCode) {
			//界面塞值时，重新拷贝一份
			this.props.form.setAllFormValue({ card_head: JSON.parse(JSON.stringify(item.values.head[formId])) });
		} else {
			flag = false;
			this.props.form.EmptyAllFormValue(formId);
		}
		//点击树节点，设置修改按钮可见
		this.props.button.setButtonVisible([ 'Edit', 'Refresh' ], flag);

		if (item.values.body) {
			this.props.editTable.setTableData(tableId, item.values.body.bodyvos);
			// if (this.props.nodetype == 'org') {
			// 	let allRows = this.props.editTable.getAllData(tableId).rows;
			// 	for (let i = 0; i < allRows.length; i++) {
			// 		let row = allRows[i];
			// 		let rowId = row.rowid;
			// 		let rowValues = row.values;
			// 		if (rowValues.pk_group && rowValues.pk_org) {
			// 			if (rowValues.pk_group.value == rowValues.pk_org.value) {
			// 				this.props.editTable.setEditableRowByIndex(tableId, i, false);
			// 			}
			// 		}
			// 	}
			// }
		} else {
			this.props.editTable.setTableData(tableId, { rows: [] });
		}
	}

	//选择财务组织
	onOrgChange = (value) => {
		//选择财务组织后，加载树
		this.setState({ mainorg: value }, () => {
			this.queryTreeData(value.refpk);
			//选择财务组织后，清空界面当前的表头表体数据
			this.props.form.EmptyAllFormValue(formId);
			this.props.editTable.setTableData(tableId, { rows: [] });
			this.state.pkOrg = value.refpk;
			//切换财务组织时，隐藏修改按钮
			this.props.button.setButtonVisible([ 'Edit' ], false);
			//过滤表体参照

			this.props.meta.getMeta()[tableId].items.map((item) => {
				if (item.attrcode == 'pk_category') {
					item.queryCondition = () => {
						return {
							pk_org: value.refpk
						}; // 根据pk_group过滤
					};
				}
			});
		});
	};
	//获取列表肩部信息
	getTableHead = () => {
		return (
			<div className="shoulder-definition-area">
				<div className="header">
					<span className="definition-search-title">{getMultiLangByID('201200516A-000029')}</span>
					<div className="btn-group">
						{this.props.button.createButtonApp({
							area: bodyButtonId,
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this)
						})}
					</div>
				</div>
			</div>
		);
	};
	render() {
		const { editTable, form, ncmodal, syncTree, DragWidthCom, orgRefCode } = this.props;
		const { createForm } = form;
		let { createEditTable } = editTable;
		let { createModal } = ncmodal;
		let { createSyncTree } = syncTree;
		let { mainorg } = this.state;
		let pagecode = this.props.pagecode;
		let mainorgobj = {
			disabled: this.props.form.getFormStatus(formId) == UISTATE.edit,
			onChange: (value) => {
				this.onOrgChange(value);
			},
			defaultValue: mainorg,
			refcode: orgRefCode,
			queryCondition: () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			}
		};
		return (
			<div id="fa-fabase-accbookinfo" className="nc-single-table">
				{/* 头部 header*/}
				<div className="nc-singleTable-header-area">
					{/* 节点标题 */}
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">
							{this.props.nodetype === 'group' ? (
								getMultiLangByID('201200516A-000017')
							) : (
								getMultiLangByID('201200516A-000018')
							)}
						</h2>
						{this.props.nodetype === 'org' && (
							<AMRefer className="ref title-search-detail" config={mainorgobj} />
						)}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: formId,
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this)
							//popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="tree-card">
					<DragWidthCom
						// 左树区域
						leftDom={
							<div className="tree-area">
								{createSyncTree({
									treeId: treeCode, // 组件id
									needSearch: false, //是否需要查询框，默认为true,显示。false: 不显示
									needEdit: false, //是否可以修改
									onSelectEve: this.onSelectEve.bind(this), //选择节点回调方法
									defaultExpandAll: true //默认展开树全部节点
								})}
							</div>
						} //左侧区域dom
						// 右卡片区域
						rightDom={
							<div>
								<div className="card-area">
									{createForm(formId, {
										onBeforeEvent: beforeEvent.bind(this)
									})}
								</div>
								<div className="table-area">
									{this.getTableHead()}
									{createEditTable(tableId, {
										showCheck: true,
										showIndex: true,
										onAfterEvent: afterEvent.bind(this),
										onBeforeEvent: tableBeforeEvent.bind(this),
										selectedChange: rowSelected.bind(this)
									})}
								</div>
							</div>
						} //右侧区域dom
						defLeftWid="20%" // 默认左侧区域宽度，px/百分比
					/>
				</div>

				<FormulaEditor
					ref="formulaEditor"
					show={this.state.show}
					value={this.state.editingValue}
					formulaConfig={[
						{ tab: getMultiLangByID('201200516A-000019'), TabPaneContent: this.FormulaClos, params: {} }
					]}
					noShowFormula={[
						getMultiLangByID('201200516A-000020'),
						getMultiLangByID('201200516A-000021'),
						getMultiLangByID('201200516A-000022'),
						getMultiLangByID('201200516A-000023'),
						getMultiLangByID('201200516A-000024'),
						getMultiLangByID('201200516A-000025'),
						getMultiLangByID('201200516A-000026'),
						getMultiLangByID('201200516A-000027'),
						getMultiLangByID('201200516A-000028')
					]}
					noShowAttr={[ 'all' ]}
					onOk={(res) => {
						if (res.indexOf('originvalue') != -1) {
							res = res.replace('originvalue', getMultiLangByID('201200516A-000000'));
						}
						if (res.indexOf('revalued_amount') != -1) {
							res = res.replace('revalued_amount', getMultiLangByID('201200516A-000001'));
						}
						if (res.indexOf('tax_input') != -1) {
							res = res.replace('tax_input', getMultiLangByID('201200516A-000002'));
						}
						if (res.indexOf('other_cost') != -1) {
							res = res.replace('other_cost', getMultiLangByID('201200516A-000003'));
						}
						if (res.indexOf('tax_cost') != -1) {
							res = res.replace('tax_cost', getMultiLangByID('201200516A-000004'));
						}
						if (res.indexOf('currmoney') != -1) {
							res = res.replace('currmoney', getMultiLangByID('201200516A-000005'));
						}
						if (res.indexOf('install_fee') != -1) {
							res = res.replace('install_fee', getMultiLangByID('201200516A-000006'));
						}
						if (res.indexOf('dismant_cost') != -1) {
							res = res.replace('dismant_cost', getMultiLangByID('201200516A-000007'));
						}
						if (this.state.editingField === formId) {
							this.props.form.setFormItemsValue(formId, { localoriginvalue: { value: res } });
						} else if (this.state.editingField === tableId) {
							this.props.editTable.setValByKeyAndIndex(tableId, this.state.rowid, 'localoriginvalue', {
								value: res
							});
							//修改当前行为已修改状态
							//修复新增行修改的子表主键丢失问题
							let pk_accbookinfo_b = this.props.editTable.getValByKeyAndIndex(
								tableId,
								this.state.rowid,
								'pk_accbookinfo_b'
							);
							if (pk_accbookinfo_b && pk_accbookinfo_b.value) {
								this.props.editTable.setRowStatus(tableId, this.state.rowid, 1);
							}
						}

						this.setState({
							show: false
						});
					}} //点击确定回调
					onCancel={(a) => {
						this.setState({ show: false });
						this.isCloseFlag = true;//公式界面关闭标识符
					}} //点击确定回调
					onHide={(a) => {
						this.setState({ show: false });
						this.isCloseFlag = true;//公式界面关闭标识符
					}}
				/>
			</div>
		);
	}
}

export default AccbookInfo;
