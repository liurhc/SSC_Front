export const treeCode = 'accbookTree'; //树的页面区域编码
export const treePkAndCode = 'accbookinfo'; //自定义的账簿信息树的根节点编码和pk
export const tableId = 'bodyvos'; //表体编码
export const formId = 'card_head'; //表头编码
export const bodyButtonId = 'card_body'; //表体按钮编码
export const url = {
	bodyAfterEditURL: '/nccloud/fa/accbookinfo/bodyAfterEdit.do',
	saveURL: '/nccloud/fa/accbookinfo/save.do',
	queryTreeURL: '/nccloud/fa/accbookinfo/queryTree.do',
	batchAlterURL: '/nccloud/fa/accbookinfo/batchAlter.do'
};
