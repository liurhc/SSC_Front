/**
 * 组织级账簿信息入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import AccbookInfo from '../treecard-base';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule} = multiLangUtils;

//组织级
const nodetype = 'org';
const pagecode = '201200517A_card';
const appcode = '201200517A';
const orgRefCode = 'uapbd/refer/org/FinanceOrgTreeRef/index.js';

const BaseAccbookInfo = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: 'card_head',
		bodycode: 'bodyvos',
		tabletype: 'editTable'
	}
})(AccbookInfo);

initMultiLangByModule({ fa: [ '201200516A' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(
		<BaseAccbookInfo nodetype={nodetype} pagecode={pagecode} orgRefCode={orgRefCode} appcode={appcode} />,
		document.querySelector('#app')
	);
});
