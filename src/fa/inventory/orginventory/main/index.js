import { RenderRouter } from 'nc-lightapp-front';
import routes from './router';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

(function main(routers, htmlTagid) {
	initMultiLangByModule({ ampub: [ 'common' ], fa: [ '201203004A', 'facommon' ] }, () => {
		RenderRouter(routers, htmlTagid);
	});
})(routes, 'app');
