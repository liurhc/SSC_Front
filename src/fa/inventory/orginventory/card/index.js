/* 盘点单卡片界面 */
import React, { Component } from 'react';
import { createPage, base, high, createPageIcon } from 'nc-lightapp-front';
import {
	buttonClick,
	StepOne,
	StepTwo,
	StepThree,
	initTemplate,
	AnewAllot,
	InventoryResult,
	backToList,
	DetailList,
	pageInfoClick,
	commit
} from './events';
import { pageConfig } from './const';
import './index.less';
import constant from '../constants';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { cardUtils, multiLangUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardPaginationArea } = cardUtils;
const { CommonKeys } = commonConst;
const { linkQueryConst } = CommonKeys;
const { ApprovalTrans } = components;
//新版本更新删除原 tinper-bee
const { NCStep, NCAffix, NCBackBtn } = base;
const { title, pagecode, dataSource, area, formId } = pageConfig;
const { NCSteps } = NCStep;
const { ApproveDetail } = high;
const steps = [
	{ title: getMultiLangByID('201203004A-000046') /*确定盘点范围*/ },
	{ title: getMultiLangByID('201203004A-000047') /*分配盘点人*/ },
	{ title: getMultiLangByID('201203004A-000048') /*确认*/ }
];

/**
 * 资产盘点
 * @author : liuzjk 
 */
class Maininfo extends Component {
	constructor(props) {
		super(props);
		// 是否从列表跳转的
		this.isFromList = 'false';
		// 单据号
		this.bill_code = null;
		// 盘点结果组织
		this.result_pk_org = '';
		// 时间戳
		this.ts = '';
		// 盘点范围
		this.rangeAreaData = null;
		// 标识符
		this.signature = '';
		/* 盘点结果 */
		this.checkResult = '0';
		// 临时存储分配盘点人数据
		this.tempEditTableDatas = null;
		this.echartsData = {
			total_num: 0,
			start_date: 0,
			end_date: 0,
			pk_org: '',
			pk_category: '',
			/* 盘点范围 */
			check_range: '',
			/* 查询区范围 */
			search_range: {},
			/**
			 * 饼图数据
			 */
			pie_data: {
				category_total: [],
				values: []
			},
			/**
			 * 盘点结果柱状图数据
			 */
			bar_data: {
				x_axis: [],
				unfinished: [],
				finished: []
			},
			/**
			 * 确定后柱状图数据
			 */
			linedata: {
				data: [],
				name: []
			}
		};
		/**
		 * 保存成功后从服务返回的数据
		 */
		this.childformData = {
			head: [],
			body: [],
			way: []
		};
		this.state = {
			// bill_code: '',
			pk_inventory: '',
			bill_status: '', //单据状态
			current: 0, //步骤条对应页面的参数默认：0
			inventoryWay: 1, //步骤二中分配盘点方式的值
			inventoryStatus: 'default',
			pk_bill: '',
			showApprove: false,
			/* 提交指派 */
			compositedisplay: false,
			compositedata: {},
			transi_type: '',
			isLeaveOrRefresh: false
		};
		closeBrowserUtils.call(this, props, {
			editTable: [ constant.AREA.STEP.ALLOT_CHECKUSER ],
			cardTable: [ constant.AREA.STEP.DETAIL_LIST ]
		});
		initTemplate.call(this, props);
	}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commit.call(this, 'SAVE', this.props, value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};
	//步骤条详细说明
	Ondescription = (current, index) => {
		if (index == current) {
			return getMultiLangByID('201203004A-000049') /*进行中*/;
		} else if (index < current) {
			return getMultiLangByID('201203004A-000050') /*已完成*/;
		} else {
			return;
		}
	};

	/**
	 * 头部显示数据控制
	 * 		1.返回按钮
	 * 		2.标题
	 * 		3.盘点单号
	 */
	createCardTitleArea = (current) => {
		/**************************************** 
		 *flag:返回按钮是否显示标识
		 *条件：查看盘点结果 或 第三步保存后 或 盘点明细（盘点结果穿透列表）
		 *		并且 非审批小应用 并且 非盘点报告穿透
		 ****************************************/
		let flag =
			(current === 3 || current === 6 || current === 7) &&
			this.props.getUrlParam('scene') != linkQueryConst.SCENETYPE.approvesce; /* 非审批小应用 */
		/* 单据号 是否显示 */
		let bill_code = this.bill_code;
		let titleValue = getMultiLangByID(title);
		if (bill_code) {
			titleValue = getMultiLangByID(title) + ':' + bill_code;
		}
		return (
			<div className="header-title-search-area org-content">
				{flag && (
					<NCBackBtn
						// className="title-search-detail"
						onClick={() => {
							backToList.call(this, this.props, current);
						}}
					/>
				)}
				{createPageIcon()}
				<h2 className="title-search-detail">{titleValue}</h2>
			</div>
		);
	};

	render() {
		let { ncmodal, ncUploader, button } = this.props;
		let { current, inventoryStatus, showApprove, pk_inventory, transi_type } = this.state; // 需要输出的数据
		let { createModal } = ncmodal;
		let { createNCUploader } = ncUploader;
		let { createButtonApp } = button;

		return (
			<div className="nc-bill-card fa-inventory">
				{/* <div className="nc-bill-top-area"> */}
				<NCAffix>
					{/* 头部 header */}
					<div className="nc-bill-header-area nonprint">
						{this.createCardTitleArea.call(this, current)}
						{/* 按钮区 btn-area */}
						<div className="header-button-area">
							{createButtonApp({
								area: 'card_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>

						{current == 7 &&
							inventoryStatus == 'confirmed' &&
							createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
					</div>
				</NCAffix>
				{/* </div> */}
				<div className="nc-bill-table-area">
					<div className={`current-content-area ${this.state.current === 1 && 'stepTwo-content-area'}`}>
						{this.state.current < 4 && (
							<div className="step-area">
								<NCSteps current={current}>
									{steps.map((item, index) => (
										<NCStep
											key={item.title}
											title={item.title}
											description={this.Ondescription(current, index)}
										/>
									))}
								</NCSteps>
							</div>
						)}
						{this.state.current === 0 && <StepOne {...this.props} rangeAreaData={this.rangeAreaData} />}
						{this.state.current === 1 &&
							(inventoryStatus === 'confirmedAllot' || inventoryStatus === 'unAllot' ? (
								<AnewAllot {...this.props} />
							) : (
								<StepTwo {...this.props} echartsData={this.echartsData} that={this} />
							))}
						{(this.state.current === 2 || this.state.current === 3) && (
							<StepThree {...this.props} echartsData={this.echartsData} that={this} />
						)}
						{this.state.current === 6 && <DetailList {...this.props} that={this} />}
						{this.state.current === 7 &&
							(inventoryStatus === 'confirmedAllot' ? (
								<AnewAllot {...this.props} />
							) : (
								<InventoryResult {...this.props} echartsData={this.echartsData} that={this} />
							))}
					</div>
				</div>
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/* 审批详情 */}
				<ApproveDetail
					show={showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={transi_type}
					billid={pk_inventory}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002') /*指派*/}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{/* 附件管理框 */}
				{createNCUploader(`${pagecode}-uploader`, {})}
			</div>
		);
	}
}

const MasterChildCardBase = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: pagecode, // 页面编码
		bodycode: area.card.body_area, //表体编码
		tabletype: 'cardTable'
	}
})(Maininfo);

export default MasterChildCardBase;
