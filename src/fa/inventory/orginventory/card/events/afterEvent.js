//编辑后事件及表体联动
import constant from '../../constants';
import { ajax } from 'nc-lightapp-front';
import ampub from 'ampub';
import fa from 'fa';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { fa_components } = fa;
const { ReferLinkage } = fa_components;
const { referLinkageClear } = ReferLinkage;
import { setOrgInventoryAttribute } from './buttonClick';

export default function afterEvent(that, props, moduleId, key, value, changedrows, i) {
	let result = that.props.cardTable.getValByKeyAndIndex(constant.AREA.STEP.DETAIL_LIST, i, 'check_result').value;
	if (value == null && result == null) {
		return;
	}
	// 如果输入的值为空则不处理
	if (result == null && (JSON.stringify(value) == '{}' || value == '')) {
		return;
	}
	if (
		key === 'card_num_after' ||
		key === 'pk_usingstatus_after' ||
		key === 'pk_mandept_after_v' ||
		key === 'assetuser_after' ||
		key === 'pk_usedept_after_v' ||
		key === 'pk_ownerorg_after_v' ||
		key === 'position_after' ||
		key === 'pk_equiporg_after_v'
	) {
		// 盘点编辑后事件
		checkInventResult.call(this, that, props, moduleId, key, value, changedrows, i);
	}
	if (key === 'pk_dept' || key === 'pk_category') {
		// 部门类别编辑后事件
		chooseMulti(that, props, moduleId, key, value, changedrows, i);
	}
	if (key === 'pk_ownerorg_after_v' || key === 'pk_equiporg_after_v') {
		referLinkageClear(props, key, constant.DEFAULTRANGECONFIG, 'cardtable', moduleId, i);
	}
	if (key == 'pk_usedept_after_v' && (changedrows[0].newvalue.value == null || changedrows[0].newvalue.value == '')) {
		that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
			pk_usedept_after: { value: null, display: null },
			['pk_usedept_after_v.name']: { value: null, display: null }
		});
	}
}
/* 根据盘点后数据给盘点结果赋值 */
function checkInventResult(that, props, moduleId, key, value, changedrows, i) {
	// 卡片
	let pk_card = that.props.cardTable.getValByKeyAndIndex(constant.AREA.STEP.DETAIL_LIST, i, 'pk_card').value;
	// 数量(盘点后)
	let card_num_after = that.props.cardTable.getValByKeyAndIndex(constant.AREA.STEP.DETAIL_LIST, i, 'card_num_after')
		.value;

	// 一、盘盈：
	if (pk_card == undefined || pk_card == null || pk_card == '') {
		// 盘点结果赋值
		that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
			check_result: {
				display: getMultiLangByID(constant.CHECK_RESULT.PROFIT.display),
				value: constant.CHECK_RESULT.PROFIT.value
			}
		});
		return;
	}
	// 未处理
	if ((card_num_after == '' || card_num_after == null) && key == 'card_num_after') {
		clearItemValue(that, i);
		return;
	}
	// 二、盘亏：
	if (card_num_after === 0 || card_num_after === '0') {
		// 盘点结果赋值
		that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
			check_result: {
				display: getMultiLangByID(constant.CHECK_RESULT.REDUCE.display),
				value: constant.CHECK_RESULT.REDUCE.value
			}
		});
		// 所有盘点后信息清除
		that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
			pk_usedept_after: { value: null, display: null },
			pk_usedept_after_v: { value: null, display: null },
			['pk_usedept_after_v.name']: { value: null, display: null },
			pk_mandept_after: { value: null, display: null },
			pk_mandept_after_v: { value: null, display: null },
			pk_equiporg_after: { value: null, display: null },
			pk_equiporg_after_v: { value: null, display: null },
			['pk_equiporg_after_v.name']: { value: null, display: null },
			pk_ownerorg_after: { value: null, display: null },
			pk_ownerorg_after_v: { value: null, display: null },
			['pk_ownerorg_after_v.name']: { value: null, display: null },
			assetuser_after: { value: null, display: null },
			position_after: { value: null, display: null },
			pk_usingstatus_after: { value: null, display: null }
		});
		return;
	}

	// 先判断是否是从盘亏转换到不符，如果是，则需要将清空的盘点后的值赋上盘点前的值
	// if (key == 'card_num_after' && changedrows[0].oldvalue.value == '0') {
	// 	evaluateAttribute(that, pk_card, i);
	// 	return;
	// }

	let INVENTORY_ITEMS = getInventoryItems.call(this);
	// 三、不符：
	// 状况（盘点后）
	for (var key in INVENTORY_ITEMS.WITH_V) {
		//使用部门单独处理
		if (key == 'pk_usedept_after_v') {
			//使用部门特殊处理
			let pk_usedept_after_v = that.props.cardTable.getValByKeyAndIndex(
				constant.AREA.STEP.DETAIL_LIST,
				i,
				'pk_usedept_after_v'
			).display;
			let pk_usedept_before_code = that.props.cardTable.getValByKeyAndIndex(
				constant.AREA.STEP.DETAIL_LIST,
				i,
				'pk_usedept_before_code'
			).value;
			if (pk_usedept_before_code != pk_usedept_after_v) {
				that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
					check_result: {
						display: getMultiLangByID(constant.CHECK_RESULT.NOT_EQUAL.display),
						value: constant.CHECK_RESULT.NOT_EQUAL.value
					}
				});
				return;
			}
			continue;
		}
		let item_value_before = that.props.cardTable.getValByKeyAndIndex(
			constant.AREA.STEP.DETAIL_LIST,
			i,
			INVENTORY_ITEMS.WITH_V[key].slice(0, INVENTORY_ITEMS.WITH_V[key].length - 'after_v'.length) + 'before_v'
		).value;
		let item_value_after = that.props.cardTable.getValByKeyAndIndex(
			constant.AREA.STEP.DETAIL_LIST,
			i,
			INVENTORY_ITEMS.WITH_V[key].slice(0, INVENTORY_ITEMS.WITH_V[key].length)
		).value;
		if (
			(item_value_before == null && item_value_after != null && item_value_after != '') ||
			(item_value_before != null && item_value_before != item_value_after)
		) {
			that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
				check_result: {
					display: getMultiLangByID(constant.CHECK_RESULT.NOT_EQUAL.display),
					value: constant.CHECK_RESULT.NOT_EQUAL.value
				}
			});
			if ((card_num_after == '' || card_num_after == null) && key != 'card_num_after') {
				let num = that.props.cardTable.getValByKeyAndIndex(constant.AREA.STEP.DETAIL_LIST, i, 'card_num_before')
					.value;
				that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
					card_num_after: {
						value: num,
						display: num
					}
				});
			}
			return;
		}
	}
	for (var key in INVENTORY_ITEMS.UNWITH_V) {
		let item_value_before = that.props.cardTable.getValByKeyAndIndex(
			constant.AREA.STEP.DETAIL_LIST,
			i,
			INVENTORY_ITEMS.UNWITH_V[key].slice(0, INVENTORY_ITEMS.UNWITH_V[key].length - 'after'.length) + 'before'
		).value;
		let item_value_after = that.props.cardTable.getValByKeyAndIndex(
			constant.AREA.STEP.DETAIL_LIST,
			i,
			INVENTORY_ITEMS.UNWITH_V[key]
		).value;
		if (
			(item_value_before == null && item_value_after != null && item_value_after != '') ||
			(item_value_before != null && item_value_before != item_value_after)
		) {
			that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
				check_result: {
					display: getMultiLangByID(constant.CHECK_RESULT.NOT_EQUAL.display),
					value: constant.CHECK_RESULT.NOT_EQUAL.value
				}
			});
			return;
		}
	}

	// 四、相符
	// 盘点结果赋值
	that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
		check_result: {
			display: getMultiLangByID(constant.CHECK_RESULT.EQUAL.display),
			value: constant.CHECK_RESULT.EQUAL.value
		}
	});
	if ((card_num_after == '' || card_num_after == null) && key != 'card_num_after') {
		let num = that.props.cardTable.getValByKeyAndIndex(constant.AREA.STEP.DETAIL_LIST, i, 'card_num_before').value;
		that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
			card_num_after: {
				value: num,
				display: num
			}
		});
	}
}

function evaluateAttribute(that, pk_card, i) {
	let data = {
		allpks: [ pk_card ]
	};
	ajax({
		url: constant.URL.QUERY_CARDMESSAGE,
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					let deptScaleVO = data[pk_card];
					let pk_usedept_after_value = '';
					let pk_usedept_after_v_value = '';
					if (deptScaleVO) {
						if (deptScaleVO.pk_dept) {
							pk_usedept_after_value = deptScaleVO.pk_dept;
						}
						if (deptScaleVO.pk_dept_v) {
							pk_usedept_after_v_value = deptScaleVO.pk_dept_v;
						}
					}
					let rows = that.props.cardTable.getRowsByIndexs(constant.AREA.STEP.DETAIL_LIST, i);
					let hiddenItems = getInventoryHiddenItems(that.props);
					//获取隐藏的字段进行赋值操作
					for (let key in hiddenItems) {
						if (key == 'pk_usedept_after_v') {
							that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
								pk_usedept_after_v: {
									display: rows[0].values.pk_usedept_before_code.value,
									value: pk_usedept_after_v_value
								}
							});
							continue;
						}
						if (key == 'pk_usedept_after') {
							that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
								pk_usedept_after: {
									display: rows[0].values.pk_usedept_before_code.value,
									value: pk_usedept_after_value
								}
							});
							continue;
						}
						let after = hiddenItems[key];
						let before = after.replace('after', 'before');
						that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
							[after]: {
								display: rows[0].values[before].display,
								value: rows[0].values[before].value
							}
						});
					}
					that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, i, {
						check_result: {
							display: getMultiLangByID(constant.CHECK_RESULT.NOT_EQUAL.display),
							value: constant.CHECK_RESULT.NOT_EQUAL.value
						}
					});
				}
			}
		}
	});
}
function getInventoryItems() {
	let INVENTORY_ITEMS = constant.INVENTORY_ITEMS;
	let meta = this.props.meta.getMeta()[constant.AREA.STEP.DETAIL_LIST].items;
	meta.map((meta_item) => {
		for (var key in INVENTORY_ITEMS.WITH_V) {
			if (meta_item.attrcode == INVENTORY_ITEMS.WITH_V[key]) {
				if (meta_item.visible == false) {
					delete INVENTORY_ITEMS.WITH_V[key];
				}
			}
		}
		for (var key in INVENTORY_ITEMS.UNWITH_V) {
			if (meta_item.attrcode == INVENTORY_ITEMS.UNWITH_V[key]) {
				if (meta_item.visible == false) {
					delete INVENTORY_ITEMS.UNWITH_V[key];
				}
			}
		}
	});
	return INVENTORY_ITEMS;
}

export function getInventoryHiddenItems(props) {
	let INVENTORY_ISHIDDEN_ITEMS = constant.INVENTORY_ISHIDDEN_ITEMS;
	let meta = props.meta.getMeta()[constant.AREA.STEP.DETAIL_LIST].items;
	meta.map((meta_item) => {
		for (var key in INVENTORY_ISHIDDEN_ITEMS) {
			if (meta_item.attrcode == INVENTORY_ISHIDDEN_ITEMS[key]) {
				if (meta_item.visible == true) {
					delete INVENTORY_ISHIDDEN_ITEMS[key];
				}
			}
		}
	});
	return INVENTORY_ISHIDDEN_ITEMS;
}

/* 部门类别多选 */
function chooseMulti(that, props, moduleId, key, value, changedrows, i) {
	if (that.state.current == 1) {
		if (key === 'pk_category') {
			that.props.editTable.setValByKeyAndIndex(constant.AREA.STEP.ALLOT_CHECKUSER, i, 'pk_category', {
				value: true
			});
		}
		if (key === 'pk_dept') {
			if (value[0].runWithChildren || true == value[0].runWithChildren) {
				that.props.editTable.setValByKeyAndIndex(constant.AREA.STEP.ALLOT_CHECKUSER, i, 'pk_dept', {
					value: true
				});
			}
		}
	}
}

function clearItemValue(that, index) {
	let WITH_V = constant.INVENTORY_ITEMS.WITH_V;
	for (var invkey in WITH_V) {
		let obj = {};
		obj[invkey] = {
			value: null,
			display: null
		};
		that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, index, obj);
	}
	let UNWITH_V = constant.INVENTORY_ITEMS.UNWITH_V;
	delete UNWITH_V.card_num_after;
	for (var invkey in UNWITH_V) {
		let obj = {};
		obj[invkey] = {
			value: null,
			display: null
		};
		that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, index, obj);
	}
	that.props.cardTable.setValByKeysAndIndex(constant.AREA.STEP.DETAIL_LIST, index, {
		check_result: {
			value: null,
			display: null
		}
	});
}
