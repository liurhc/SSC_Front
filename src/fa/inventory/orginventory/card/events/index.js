import StepOne from '../components/StepOne';
import StepTwo from '../components/StepTwo';
import StepThree from '../components/StepThree';
import AnewAllot from '../components/AnewAllot';
import InventoryResult from '../components/InventoryResult';
import DetailList from '../components/DetailList';
import initTemplate from './initTemplate';
import buttonClick, {
	commit,
	toggleShow,
	backToList,
	loadingResultData,
	skipClick,
	queryInventoryWay,
	setOrgInventoryAttribute,
	changeTime
} from './buttonClick';
import handleChange, { changeItem } from './handleChange';
import pageInfoClick from './pageInfoClick';
import afterEvent, { getInventoryHiddenItems } from './afterEvent';

export {
	buttonClick,
	StepOne,
	StepTwo,
	StepThree,
	initTemplate,
	AnewAllot,
	handleChange,
	InventoryResult,
	DetailList,
	toggleShow,
	backToList,
	pageInfoClick,
	loadingResultData,
	skipClick,
	commit,
	afterEvent,
	changeItem,
	queryInventoryWay,
	setOrgInventoryAttribute,
	getInventoryHiddenItems,
	changeTime
};
