//按钮点击事件处理
import constant from '../../constants';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

//分配盘点人中   分配盘点方式
export default function handleChange(index, that) {
	this.props.button.setButtonDisabled({ DelAssignLine: true });
	if ('' + index == that.state.inventoryWay) {
		return;
	}
	// 保存之后切换盘点方式提示
	if (
		(that.state.inventoryStatus == 'confirmedAllot' || that.state.inventoryStatus == 'confirmed') &&
		'' + index != that.state.inventoryWay &&
		!this.isHint
	) {
		this.props.modal.show('empty', {
			title: getMultiLangByID('201203004A-000041') /*提示*/,
			content: getMultiLangByID('201203004A-000043') /*切换分配类型，将清空未盘资产的计划盘点人，确认切换？*/,
			beSureBtnClick: () => {
				changeSelectValue.call(this, index);
				this.isHint = true;
			}
		});
		return;
	}
	if (this.flag) {
		// 记录上一个盘点方式
		this.pre_index = index;
		// 第一次点击点击使用人盘点
		if (index == '1') {
			return;
			//第一次点击除使用人盘点方式之外的方式
		} else {
			changeSelectValue.call(this, index);
			this.flag = false;
		}
		return;
	}
	let arr = [];
	if (this.pre_index == 2) {
		arr = [ 'pk_equiporg', 'pk_dept', 'inventory_checkuser' ];
	} else if (this.pre_index == 3) {
		arr = [ 'pk_ownerorg', 'pk_dept', 'inventory_checkuser' ];
	} else if (this.pre_index == 4) {
		arr = [ 'pk_category', 'inventory_checkuser' ];
	} else if (this.pre_index == 5) {
		arr = [ 'pk_category', 'pk_ownerorg', 'pk_dept', 'inventory_checkuser' ];
	} else if (this.pre_index == 6) {
		arr = [ 'pk_category', 'pk_equiporg', 'pk_dept', 'inventory_checkuser' ];
	}
	// 必输项校验
	that.props.editTable.filterEmptyRows(constant.AREA.STEP.ALLOT_CHECKUSER, constant.ALLOT_FILED, 'include');
	let data = that.props.editTable.getAllRows(constant.AREA.STEP.ALLOT_CHECKUSER, false);
	if (data.length != 0) {
		this.props.modal.show('empty', {
			title: getMultiLangByID('201203004A-000041') /*提示*/,
			content: getMultiLangByID('201203004A-000043') /*切换分配类型，将清空未盘资产的计划盘点人，确认切换？*/,
			beSureBtnClick: () => {
				changeSelectValue.call(this, index);
				this.isHint = true;
			}
		});
	} else {
		changeSelectValue.call(this, index);
	}
}

function changeSelectValue(nowindex) {
	let { that } = this.props;
	let index = that.state.inventoryWay;
	this.pre_index = index;
	that.setState({ inventoryWay: nowindex });
	this.setState({ checkedValue: nowindex }, () => {
		this.props.editTable.setTableData(constant.AREA.STEP.ALLOT_CHECKUSER, { rows: [] });
		// 使盘点方式表中所有选择下级字段为false
		if (nowindex != 1) {
			this.props.editTable.addRow(constant.AREA.STEP.ALLOT_CHECKUSER, 0, false, {
				deptincludesub: { display: 'false', value: 'false' },
				cateincludesub: { display: 'false', value: 'false' },
				equiporgincsub: { display: 'false', value: 'false' },
				ownerorgincsub: { display: 'false', value: 'false' }
			});
		}
		changeItem.call(this, nowindex);
	});
}

export function changeItem(index) {
	let ALLOT_CHECKUSER = constant.AREA.STEP.ALLOT_CHECKUSER;
	if (index == '2') {
		this.props.editTable.hideColByKey(ALLOT_CHECKUSER, [ 'pk_category', 'pk_ownerorg' ]);
		this.props.editTable.showColByKey(ALLOT_CHECKUSER, [ 'pk_equiporg', 'pk_dept' ]);
	} else if (index == '3') {
		this.props.editTable.hideColByKey(ALLOT_CHECKUSER, [ 'pk_category', 'pk_equiporg' ]);
		this.props.editTable.showColByKey(ALLOT_CHECKUSER, [ 'pk_ownerorg', 'pk_dept' ]);
	} else if (index == '4') {
		this.props.editTable.hideColByKey(ALLOT_CHECKUSER, [ 'pk_category', 'pk_equiporg', 'pk_dept' ]);
		this.props.editTable.showColByKey(ALLOT_CHECKUSER, [ 'pk_category' ]);
	} else if (index == '5') {
		this.props.editTable.hideColByKey(ALLOT_CHECKUSER, [ 'pk_equiporg' ]);
		this.props.editTable.showColByKey(ALLOT_CHECKUSER, [ 'pk_category', 'pk_ownerorg', 'pk_dept' ]);
	} else if (index == '6') {
		this.props.editTable.hideColByKey(ALLOT_CHECKUSER, [ 'pk_ownerorg' ]);
		this.props.editTable.showColByKey(ALLOT_CHECKUSER, [ 'pk_category', 'pk_equiporg', 'pk_dept' ]);
	}
}
