// 页面配置
export const pageConfig = {
	// 应用主键
	appid: '0001Z910000000003CDD',
	// 应用编码
	appcode: '201203004A',
	// 应用名称
	title: '201203004A-000044' /*盘点单*/,
	// 页面编码
	pagecode: '201203004A_card',
	// 卡片页面编码
	pagecodecard: '201203004A_card',
	// 主键字段
	pkField: 'pk_inventory',
	// 单据类型
	bill_type: 'HP',
	// 交易类型
	transi_type: 'HP-01',
	// 列表界面
	listRouter: '/list',
	// 输出文件名称
	printFilename: '201203004A-000045' /*固定资产盘点*/,
	// 表头
	formId: 'card_head',
	// 请求链接
	url: {
		queryCardUrl: '/nccloud/fa/orginventory/querycard.do', //盘点卡片查询
		query_list: '/nccloud/fa/orginventory/query.do',
		query_assign: '/nccloud/fa/orginventory/queryAssign.do', // 第二步查询有无未分配
		query_assign_saved: '/nccloud/fa/orginventory/queryAssignSaved.do', // 保存后第二步查询有无未分配
		save: '/nccloud/fa/orginventory/save.do', // 保存
		update: '/nccloud/fa/orginventory/update.do', // 更新
		skip_update: '/nccloud/fa/orginventory/skipUpdate.do', // 穿透列表更新
		query_saved: '/nccloud/fa/orginventory/querySaved.do', // 保存后再次查询有无未分配盘点人
		query_check_result: '/nccloud/fa/orginventory/queryCheckResult.do', //穿透查询
		query_inventory_result: '/nccloud/fa/orginventory/queryInventoryResult.do', //查看盘点结果
		query_inventory_way: '/nccloud/fa/orginventory/queryInventoryWay.do', //查看盘点方式
		commit: '/nccloud/fa/orginventory/commit.do', // 提交
		print_list: '/nccloud/fa/orginventory/printList.do', // 列表态打印输出
		print_card: '/nccloud/fa/orginventory/printCard.do', // 卡片态打印输出
		query_cardmessage: '/nccloud/fa/orginventory/queryCardMessage.do' // 查询卡片信息，为盘点单赋值
	},
	// 区域码
	area: {
		list: {
			//列表态
			search_area: 'searchArea', // 查询区
			grid_area: 'list_head', //列表区
			querytype: 'tree'
		},
		step: {
			//步骤条

			inventory_range: 'childform1', // 盘点范围区域
			inventory_date: 'childform2', // 盘点时间区域
			allot_checkuser: 'tableArea', // 分配盘点人区域
			anewallot: 'anewAllot', // 未分配区域
			detail_list: 'bodyvos', // 盘点明细
			querytype: 'tree' // 浏览态查询区查询类型
		},
		card: {
			//卡片态
			body_head: 'card_head', //卡片态表头区域
			body_area: 'bodyvos', //卡片态表体区域
			body_extend_area: 'bodyvos_browse', //卡片态表体展开区域
			body_sideslip_area: 'bodyvos_edit', //卡片态c侧滑区域
			tail_area: 'tail' //卡片态表尾区域
		}
	},
	// 按钮区域
	btn_area: {
		list_head: 'list_head', //列表态肩部
		list_table_head: '',
		list_table_inner: 'list_inner', //列表态表体行
		card_head: 'card_head', //卡片态肩部
		card_table_head: 'card_body', //卡片态表格肩部
		card_table_inner: 'card_body_inner' //卡片态表体行
	},
	// 步骤条buttons
	step_btns: {
		//所有按钮
		all_buttons: [
			'Confirm',
			'Previous',
			'Next',
			'Save',
			'BatchAlter',
			'ReInventory',
			'ReInventoryPeople',
			'Cancel',
			'Commit',
			'QueryAboutBillFlow',
			'Back',
			'Attachment',
			'Delete',
			'Edit',
			'AddLine',
			'Print',
			'Refresh'
		],
		//步骤条_第一步 buttons
		step_one_btns: [ 'Next', 'Cancel' ],
		//步骤条_第二步 buttons
		step_two_btns: [ 'Next', 'Previous', 'Cancel', 'DelLine' ],
		//步骤条_第二步操作列 buttons
		step_two_inner_btns: [ 'DelLine' ],
		//步骤条_第二到第三步未分配 buttons
		step_two_half_btns: [ 'Confirm', 'BatchAlter', 'ReInventory', 'Cancel' ],
		//步骤条_第三步 buttons
		step_three_btns: [ 'Save', 'Cancel' ],
		//步骤条_保存后 buttons
		step_saved: [ 'ReInventoryPeople', 'Attachment', 'Delete' ],

		//盘点结果_自由态
		result_free: [ 'Commit', 'Attachment', 'Print', 'Refresh' ],
		//盘点结果_已提交
		result_commit: [ 'Back', 'Attachment', 'Print', 'Refresh' ],
		//盘点结果_审核中
		result_approving: [ 'QueryAboutBillFlow', 'Attachment', 'Print', 'Refresh' ],
		//盘点结果_审批通过
		result_approved: [ 'Back', 'Attachment', 'Print', 'Refresh' ],

		//穿透_浏览态
		skip_browse: [ 'Edit', 'Print' ],
		//穿透_编辑态
		skip_edit: [ 'Save', 'Cancel', 'AddLine', 'Print' ],
		//穿透_不可编辑态
		skip_unedit: [ 'Print' ],
		//穿透_浏览态_操作列
		skip_inner_browse: [ 'OpenCard', 'OffLoss', 'Delete' ],
		//穿透_浏览态_每个条目——操作列
		skip_inner_browse_open: [ 'OpenCard' ],
		skip_inner_browse_offloss: [ 'OffLoss' ],
		skip_inner_browse_profit: [ 'OpenCard', 'Delete' ],
		//穿透_编辑态_操作列
		skip_inner_edit: [ 'OpenCard', 'OffLoss', 'Delete' ],
		//穿透_不可编辑态_操作列
		skip_inner_unedit: [ 'Detail' ],
		//穿透列表_所有按钮
		skip_all_button: [ 'OpenCard', 'OffLoss', 'Delete', 'Detail' ]
	},
	referConfig: {
		searchId: 'searchArea',
		formId: 'card_head',
		bodyIds: [ 'bodyvos', 'bodyvos_edit' ],
		specialFields: {
			pk_ownerorg_v: {
				addOrgRelation: 'pk_orgs',
				RefActionExtType: 'TreeRefActionExt',
				class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_OWNERORG_VSqlBuilder',
				//货主管理组织
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			}
		},
		defPrefix: {
			search: [ 'def', 'bodyvos.def' ],
			head: 'def',
			body: 'def'
		}
	},
	// 打印模板节点标识
	printNodekey: null,
	cardRouter: '/card',
	// 权限资源编码
	resourceCode: '2012048020',
	dataSource: 'fa.inventory.orginventory.main',
	batchBtns: [ 'Delete', 'Commit', 'UnCommit', 'Attachment', 'Print', 'Output' ]
};
