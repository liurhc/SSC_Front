import React, { Component } from 'react';
import echarts from 'echarts';
import { toggleShow, changeTime } from '../events';
import constant from '../../constants';
import { ajax, toast } from 'nc-lightapp-front';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default class SurePage extends Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		let { echartsData, that } = this.props;
		let { total_num, linedata } = echartsData;
		let { name, data } = linedata;
		let formatterTemp = getMultiLangByID('201203004A-000007', { total_num }); /*'总共{total_num }个资产'*/
		let option = {
			series: [
				{
					name: getMultiLangByID('201203004A-000014') /*资产总数*/,
					type: 'pie',
					radius: [ '55%', '65%' ],
					center: [ '35%', '50%' ],
					avoidLabelOverlap: false, //是否允许提示文字重叠，false 提示语显示在中心
					label: {
						normal: {
							show: true,
							position: 'center',
							formatter: formatterTemp,
							color: '#ccc',
							fontSize: 18
						}
					},
					data: [ { value: 335 } ]
				}
			],
			color: [ '#20CBAF' ]
		};
		let optionLine = {
			title: {
				text: getMultiLangByID('201203004A-000015') /*如下人员去盘点*/,
				x: 'left',
				align: 'right',
				textStyle: {
					fontSize: 16,
					fontWeight: 'normal'
				}
			},
			//悬浮提示框
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					// 坐标轴指示器，坐标轴触发有效
					type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
				},
				formatter: function(params) {
					let xAxis = params[0]['name'];
					let nameArr = params.map(({ seriesName }) => {
						return seriesName;
					});
					let dataArr = params.map(({ data }) => {
						return data;
					});
					let totalNum = 0;
					for (let i = 0; i < dataArr.length; i++) {
						totalNum += dataArr[i] * 1;
					}
					let str = '';
					for (let i = 0; i < nameArr.length; i++) {
						str += '<p>' + nameArr[i] + '：<span>' + dataArr[i] + '个</span></p >';
					}
					let content = getMultiLangByID('201203004A-000001', { totalNum }); /*共分配{totalNum}个资产*/
					return `<div class="toolTip">
				                        <p>${xAxis} <span>${content}</span></p >
				                        ${str}
				                    </div>`;
				}
			},
			legend: {
				data: [ getMultiLangByID('201203004A-000004') /*未盘*/, getMultiLangByID('201203004A-000003') /*已盘*/ ]
			},
			//数据区域放缩
			dataZoom: [
				{
					show: true,
					realtime: true,
					filterMode: 'filter',
					start: 0,
					end: 50
				}
			],
			xAxis: [
				{
					type: 'category',
					data: name,
					//data: [ '张三', '李四', '王五', '张强', '李倩', '何红', '王丽' ],
					axisTick: {
						//是否显示坐标刻度线
						show: false
					},
					axisLine: {
						//坐标轴是否显示
						show: true
					}
				}
			],
			yAxis: [
				{
					type: 'value',
					splitLine: {
						//是否显示网格线
						show: false
					},
					axisTick: {
						//是否显示坐标刻度线
						show: false
					},
					axisLine: {
						//坐标轴是否显示
						show: true
					},
					minInterval: 1
				}
			],
			series: [
				{
					name: getMultiLangByID('201203004A-000004') /*未盘*/,
					type: 'bar',
					stack: getMultiLangByID('201203004A-000005') /*总量*/,
					barWidth: 15,
					itemStyle: {
						normal: {
							color: function(params) {
								var colorList = [ '#20CBAF', '#20CBAF', '#20CBAF', '#20CBAF' ];
								return colorList[params.dataIndex];
							}
						}
					},
					data: data
				},
				{
					name: getMultiLangByID('201203004A-000003') /*已盘*/,
					type: 'bar',
					stack: getMultiLangByID('201203004A-000005') /*总量*/,
					barWidth: 15,
					itemStyle: {
						normal: {
							color: function(params) {
								//控制柱状图的颜色
								var colorList = [ '#FF9A48', '#FF9A48', '#FF9A48', '#FF9A48' ];
								return colorList[params.dataIndex];
							}
						}
					},
					data: []
				}
			],
			color: [ '#20CBAF', '#FF9A48' ]
		};
		const myChart = echarts.init(document.getElementById('echartPieBox'));
		const myChartLine = echarts.init(document.getElementById('echartsLineBox'));
		myChart.setOption(option);
		myChartLine.setOption(optionLine);
		//当浏览器窗口发生变化时候，echarts大小随着变化
		window.addEventListener('resize', () => {
			myChart.resize();
			myChartLine.resize();
		});
		//当离开当前窗口时触发
		window.onbeforeunload = () => {
			if (that.state.isLeaveOrRefresh) {
				return '';
			}
		};
		// 点击事件 -- 点击柱状图的未分配人员
		myChartLine.on('click', (params) => {
			if (params.name === getMultiLangByID('201203004A-000006') /*未分配人员*/) {
				ajax({
					url: constant.URL.QUERY_NOINVENCHECKUSER,
					data: {
						pk: that.state.pk_inventory
					},
					success: (res) => {
						if (
							that.state.inventoryStatus == 'confirmed' ||
							that.state.inventoryStatus == 'confirmedAllot'
						) {
							that.setState({ current: 1, inventoryStatus: 'confirmedAllot' }, () => {
								this.props.cardTable.setTableData(
									constant.AREA.STEP.ANEWALLOT,
									res.data.body[constant.AREA.CARD.BODY_AREA]
								);
								toggleShow(that, that.props);
								this.props.cardTable.setStatus(constant.AREA.STEP.ANEWALLOT, 'edit');
							});
						} else {
							that.setState({ current: 1, inventoryStatus: 'unAllot' }, () => {
								toggleShow(that, that.props);
								this.props.cardTable.setStatus(constant.AREA.STEP.ANEWALLOT, 'edit');
							});
						}
					},
					error: (res) => {
						if (res && res.message) {
							toast({ content: res.message, color: 'danger' });
						}
					}
				});
			}
		});
	}
	render() {
		let { echartsData, that } = this.props;
		let { start_date, end_date, search_range, check_range } = echartsData;
		let { inventoryStatus } = that.state;
		let { bill_code } = that;
		let start_date_temp = '';
		let end_date_temp = '';
		start_date != null && (start_date_temp = changeTime(start_date));
		end_date != null ? (end_date_temp = changeTime(end_date)) : (end_date_temp = '');

		return (
			<div className="echarts-area">
				<div id="echartPieBox" />
				<div class="assetInfo">
					{inventoryStatus == 'confirmed' && (
						<p>
							<span className="title">{getMultiLangByID('201203004A-000008') /*单据号*/}：</span>
							<span className="msg">{bill_code}</span>
						</p>
					)}
					<p>
						<span className="title">{getMultiLangByID('201203004A-000010') /*盘点时间*/}：</span>{' '}
						<span className="msg">{start_date_temp}</span>---<span className="msg">{end_date_temp}</span>
					</p>
					<p>
						<span className="title">{getMultiLangByID('201203004A-000013') /*盘点范围*/}：</span>
					</p>
					{inventoryStatus == 'confirmed' || inventoryStatus == 'confirmedAllot' ? (
						<p>
							<div>
								<span className="msg">{check_range}</span>
							</div>
						</p>
					) : (
						<p className="scope-p">
							{search_range.conditions.map((conditionItem) => {
								let rangeDetail = that.props.meta.getMeta().childform1.items.map((nameItem) => {
									if (nameItem.attrcode == conditionItem.field) {
										return (
											<div>
												<span className="msg">{nameItem.label}</span>
												<span className="msg">{conditionItem.oprtype}</span>
												<span className="msg scope-text" title={conditionItem.display}>
													{conditionItem.display}
												</span>
											</div>
										);
									}
								});
								return rangeDetail;
							})}
						</p>
					)}
					<p />
				</div>
				<div className="divisionLine" />
				<div id="echartsLineBox" />
			</div>
		);
	}
}
