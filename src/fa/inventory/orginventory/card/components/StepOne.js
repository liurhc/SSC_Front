import React, { Component } from 'react';
import constant from '../../constants';
import { stepOneAreaAfterEvent } from './../components/event';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export default class StepOne extends Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		let { form, search } = this.props;
		// search.setDisabled(constant.AREA.STEP.INVENTORY_RANGE, false);
		form.setFormStatus(constant.AREA.STEP.INVENTORY_DATE, 'edit');
	}
	reSetRangeData = (rangeAreaData) => {
		if (rangeAreaData) {
			this.props.search.setSearchValue(constant.AREA.STEP.INVENTORY_RANGE, rangeAreaData.conditions);
		}
	};
	render() {
		let { form, search, rangeAreaData } = this.props;
		let { createForm } = form;
		let { NCCreateSearch } = search;
		return (
			<div className="container-area">
				<div>
					<p>{getMultiLangByID('201203004A-000013') /*盘点范围*/}</p>
					{NCCreateSearch(constant.AREA.STEP.INVENTORY_RANGE, {
						onAfterEvent: stepOneAreaAfterEvent.bind(this),
						showSearchBtn: false,
						renderCompleteEvent: this.reSetRangeData.bind(this, rangeAreaData)
					})}
				</div>
				<div>
					<p>{getMultiLangByID('201203004A-000010') /*盘点时间*/}</p>
					{createForm(constant.AREA.STEP.INVENTORY_DATE, {})}
				</div>
			</div>
		);
	}
}
