import constant from '../../../constants';
import ampub from 'ampub';
import fa from 'fa';
const { commonConst } = ampub;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addBodyReferFilter } = ReferFilter;

/**
 * 第二步区域编辑前事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} index 
 * @param {*} record 
 */
export default function(that, props, moduleId, attrMeta, index, value, record) {
	let meta = this.props.meta.getMeta();
	if ((moduleId = 'tableArea2')) {
		let assignAreaFilterConfig = {
			bodyIds: [ 'tableArea2' ], //表达id数组
			specialFields: {
				//资产类别
				pk_category: {
					data: [
						{
							returnConst: '',
							returnName: 'pk_org'
						}
					]
				},
				//货主管理组织
				pk_ownerorg: {
					addOrgRelation: 'pk_orgs',
					data: [
						{
							returnConst: '',
							returnName: 'pk_orgs'
						}
					]
				},
				pk_dept: {
					data: [
						{
							returnConst: '',
							returnName: 'pk_org'
						},
						{
							returnConst: IBusiRoleConst.ASSETORG,
							returnName: 'busifuncode'
						}
					]
				}
			}, //特殊过滤
			defPrefix: { body: 'def' }
		};
		setOrgFilter.call(this, assignAreaFilterConfig, 'pk_ownerorg', that);
		setOrgFilter.call(this, assignAreaFilterConfig, 'pk_category', that);
		setPk_deptFilter.call(this, assignAreaFilterConfig, moduleId, index, that);
		addBodyReferFilter(props, meta, assignAreaFilterConfig, record, 'editTable');
	}
	this.props.meta.setMeta(meta);
	return true;
}

function setOrgFilter(assignAreaFilterConfig, field, that) {
	if (that.result_pk_org != null) {
		assignAreaFilterConfig.specialFields[field].data[0].returnConst = that.result_pk_org;
	} else {
		let pk_org = this.props.search.getSearchValByField(constant.AREA.STEP.INVENTORY_RANGE, 'pk_org').value
			.firstvalue;
		assignAreaFilterConfig.specialFields[field].data[0].returnConst = pk_org;
	}
}

function setPk_deptFilter(assignAreaFilterConfig, moduleId, index, that) {
	if (that.result_pk_org != null) {
		assignAreaFilterConfig.specialFields['pk_dept'].data[0].returnConst = that.result_pk_org;
	} else {
		let pk_org = this.props.search.getSearchValByField(constant.AREA.STEP.INVENTORY_RANGE, 'pk_org').value
			.firstvalue;
		assignAreaFilterConfig.specialFields['pk_dept'].data[0].returnConst = pk_org;
	}
	if (this.state.checkedValue == 3 || this.state.checkedValue == 5) {
		let ownerorg = this.props.editTable.getValByKeyAndIndex(moduleId, index, 'pk_ownerorg');
		if (ownerorg) {
			let pk_ownerorg = ownerorg.value;
			if (pk_ownerorg != null && pk_ownerorg != '') {
				assignAreaFilterConfig.specialFields['pk_dept'].data[0].returnConst = pk_ownerorg;
			}
		}
	} else if (this.state.checkedValue == 2 || this.state.checkedValue == 6) {
		let equiporg = this.props.editTable.getValByKeyAndIndex(moduleId, index, 'pk_equiporg');
		if (equiporg) {
			let pk_equiporg = equiporg.value;
			if (pk_equiporg != null && pk_equiporg != '') {
				assignAreaFilterConfig.specialFields['pk_dept'].data[0].returnConst = pk_equiporg;
			}
		}
	}
}
