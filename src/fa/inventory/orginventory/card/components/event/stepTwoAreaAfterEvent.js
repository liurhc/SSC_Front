/**
 * 第二步区域编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} index 
 * @param {*} record 
 */
export default function(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (key == 'pk_dept') {
		/***********************************
		 * 同一单元格，是否包含下级是相同的
		 * 如果不选中执行时包含下级，平台默
		 * 认不传runWithChildren参数
		 ***********************************/
		if (value[0] && (value[0].runWithChildren || true == value[0].runWithChildren)) {
			this.props.editTable.setValByKeyAndIndex(moduleId, index, 'deptincludesub', {
				value: true
			});
		} else {
			this.props.editTable.setValByKeyAndIndex(moduleId, index, 'deptincludesub', {
				value: false
			});
		}
	}
	if (this.state.checkedValue == 3 || this.state.checkedValue == 5) {
		if (key == 'pk_ownerorg') {
			clearPk_dept.call(this, moduleId, index);
		}
	} else if (this.state.checkedValue == 2 || this.state.checkedValue == 6) {
		if (key == 'pk_equiporg') {
			//&& !((newvalue == null && oldvalue == '') || (newvalue == '' && oldvalue == null))
			clearPk_dept.call(this, moduleId, index);
		}
	}
	return true;
}

function clearPk_dept(moduleId, index) {
	let pk_dept = this.props.editTable.getValByKeyAndIndex(moduleId, index, 'pk_dept').value;
	if (pk_dept != null && pk_dept != '') {
		this.props.editTable.setValByKeyAndIndex(moduleId, index, 'pk_dept', { value: null, display: null });
	}
}
