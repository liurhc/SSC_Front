//表体行操作列处理
import constant from '../../../constants';
import fa from 'fa';
const { fa_components } = fa;
const { DeptMidlevRefFilter } = fa_components;
const { queryOnlyLeafCanSelect } = DeptMidlevRefFilter;

export default function(props, moudleid, key, value, index, record) {
	// 如果数量为空即为盘亏，设置显示字段盘点后状态不可编辑
	let card_num_after = this.props.cardTable.getValByKeyAndIndex(
		constant.AREA.STEP.DETAIL_LIST,
		index,
		'card_num_after'
	).value;
	if (card_num_after == '0') {
		let WITH_V = constant.INVENTORY_ITEMS.WITH_V;
		for (var invkey in WITH_V) {
			if (key == invkey) {
				return false;
			}
		}
		let UNWITH_V = constant.INVENTORY_ITEMS.UNWITH_V;
		delete UNWITH_V.card_num_after;
		for (var invkey in UNWITH_V) {
			if (key == invkey) {
				return false;
			}
		}
	}
	// 如果当前设备是非盘盈，则不可编辑
	if ((key == 'asset_name' || key == 'pk_category') && record.values.check_result.value != '1') {
		return false;
	}

	// 处理管理部门非末级
	if (
		key == 'pk_mandept_after_v' ||
		key == 'pk_mandept_after' ||
		key == 'pk_usedept_after_v' ||
		key == 'pk_usedept_after'
	) {
		let pk_org = this.props.search.getSearchValByField(constant.AREA.STEP.INVENTORY_RANGE, 'pk_org').value
			.firstvalue;
		if (pk_org == null || pk_org == '') {
			let { that } = this.props;
			pk_org = that.echartsData.pk_org;
		}
		//表体部门非末级处理
		let field = [ 'pk_mandept_after_v', 'pk_mandept_after', 'pk_usedept_after_v', 'pk_usedept_after' ]; //传管理部门、使用部门字段
		let tableId = constant.AREA.STEP.DETAIL_LIST;
		let assembleData = {
			tableId,
			pk_org,
			field
		};
		//该方法只能处理管理部门，使用部门在表体参照过滤中进行处理
		queryOnlyLeafCanSelect.call(this, props, assembleData);
	}

	return true;
}
