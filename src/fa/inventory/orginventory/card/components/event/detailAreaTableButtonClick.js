//表体行操作列处理
import constant from './../../../constants';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default function(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'OpenCard':
			openCard.call(this, props, record, index, constant.AREA.STEP.DETAIL_LIST);
			break;
		case 'DelLine':
			delLine.call(this, props, index);
			break;
		case 'OffLoss':
			offLoss.call(this, props, index, constant.AREA.STEP.DETAIL_LIST);
			break;
		case 'Detail':
			detail.call(this, props, record, index, constant.AREA.STEP.DETAIL_LIST);
			break;
		default:
			break;
	}
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 */
function delLine(props, index) {
	props.cardTable.delRowsByIndex(constant.AREA.STEP.DETAIL_LIST, index);
}

/**
 * 表格行展开
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function openCard(props, record, index, tableId) {
	// props.cardTable.setClickRowIndex(tableId, { record, index });
	props.cardTable.openModel(tableId, 'edit', record, index);
}

/**
 * 取消盘亏
 * @param {*} props 
 * @param {*} index 
 */
function offLoss(props, index, tableId) {
	props.cardTable.setValByKeysAndIndex(tableId, index, {
		check_result: {
			value: constant.CHECK_RESULT.NOT_EQUAL.value,
			display: getMultiLangByID(constant.CHECK_RESULT.NOT_EQUAL.display)
		},
		card_num_after: { value: '1', display: 1 }
	});
}

/**
 * 详情
 * @param {*} props 
 * @param {*} index 
 */
function detail(props, record, index, tableId) {
	props.cardTable.toggleRowView(tableId, record);
}
