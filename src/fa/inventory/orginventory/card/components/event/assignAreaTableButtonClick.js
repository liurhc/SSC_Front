//表体行操作列处理
import constant from './../../../constants';

export default function(props, key, text, record, index) {
	switch (key) {
		case 'DelLine':
			delLine.call(this, props, index);
			break;
		default:
			break;
	}
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 */
function delLine(props, index, tableId) {
	props.editTable.deleteTableRowsByIndex(constant.AREA.STEP.ALLOT_CHECKUSER, index);
}
