import React, { Component } from 'react';
import constant from '../../constants';

export default class extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let { createCardTable } = this.props.cardTable;
		// let { createEditTable } = editTable;
		return (
			<div className="nc-bill-table-area">
				{createCardTable(constant.AREA.STEP.ANEWALLOT, {
					showIndex: true
				})}
			</div>
		);
	}
}
