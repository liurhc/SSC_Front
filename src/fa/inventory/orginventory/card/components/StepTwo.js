import React, { Component } from 'react';
import constant from '../../constants';
import { handleChange, changeItem } from '../events';
import { stepTwoAreaBeforeEvent, stepTwoAreaAfterEvent } from './../components/event';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default class stepTwo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			checkedValue: '1', //步骤二中分配盘点方式的值
			delLineAble: 'true'
		};
		this.flag = true; //步骤二中，第一次点击 使用人盘点 选项时候的标识
		this.pre_index = 1; // 记录上一步选择的盘点方式
		// 第二步弹框提示标志，false:显示提示 true:不显示提示
		this.isHint = false;
	}
	buttonClick = (props, id) => {
		switch (id) {
			case 'AddAssignLine':
				props.editTable.addRow(
					constant.AREA.STEP.ALLOT_CHECKUSER,
					props.editTable.getNumberOfRows(constant.AREA.STEP.ALLOT_CHECKUSER),
					true,
					{
						deptincludesub: { display: 'false', value: 'false' },
						cateincludesub: { display: 'false', value: 'false' },
						equiporgincsub: { display: 'false', value: 'false' },
						ownerorgincsub: { display: 'false', value: 'false' }
					}
				);
				break;
			case 'DelAssignLine':
				let checkedRows = this.props.editTable.getCheckedRows(constant.AREA.STEP.ALLOT_CHECKUSER);
				let deleteIndexs = [];
				checkedRows.map((item) => {
					deleteIndexs.push(item.index);
				});
				props.editTable.deleteTableRowsByIndex(constant.AREA.STEP.ALLOT_CHECKUSER, deleteIndexs);
				this.rowSelected.call(this, this.props, constant.AREA.STEP.ALLOT_CHECKUSER);
				break;
			default:
				break;
		}
	};

	// 模态框确定点击事件
	emptyConfirm() {
		let { that } = this.props;
		let index = that.state.inventoryWay;
		this.pre_index = index;
		this.setState({ checkedValue: index }, () => {
			if (that.state.inventoryWay != 1) {
				// 使盘点方式表中所有选择下级字段为false
				this.props.editTable.addRow(constant.AREA.STEP.ALLOT_CHECKUSER, 0, true, {
					deptincludesub: { display: 'false', value: 'false' },
					cateincludesub: { display: 'false', value: 'false' },
					equiporgincsub: { display: 'false', value: 'false' },
					ownerorgincsub: { display: 'false', value: 'false' }
				});
			}
			setTimeout(() => {
				this.props.editTable.setTableData(constant.AREA.STEP.ALLOT_CHECKUSER, { rows: [] });
				this.props.editTable.addRow(constant.AREA.STEP.ALLOT_CHECKUSER, 0, true);
				changeItem.call(this, index);
			}, 0);
		});
	}
	// 模态框取消点击事件
	emptyCancel() {
		that.setState({ inventoryWay: this.pre_index });
		this.setState({ checkedValue: this.pre_index }, () => {
			setTimeout(() => {
				changeItem.call(this, this.pre_index);
			}, 0);
		});
	}
	componentDidMount() {
		let { that } = this.props;
		// 如果是从列表跳转的，查询之前的盘点方式
		if (that.isFromList == 'true') {
			changeItem.call(this, that.state.inventoryWay);
			that.isFromList == 'false';
		} else {
			if (that.tempEditTableDatas != null) {
				this.props.editTable.setTableData(constant.AREA.STEP.ALLOT_CHECKUSER, that.tempEditTableDatas);
				that.tempEditTableDatas = null;
			}
		}
	}

	rowSelected = (props, moduleId, record, index, status) => {
		let checkedRows = this.props.editTable.getCheckedRows(moduleId);
		checkedRows && checkedRows.length != 0
			? this.props.button.setButtonDisabled({ DelAssignLine: false })
			: this.props.button.setButtonDisabled({ DelAssignLine: true });
	};

	render() {
		let { that, ncmodal, echartsData, button } = this.props;
		let { createButtonApp } = button;
		let { createEditTable } = this.props.editTable;
		let { total_num } = echartsData;
		let { createModal } = ncmodal;
		let { inventoryWay } = that.state;
		const radioArr = [
			getMultiLangByID('201203004A-000016') /*使用人盘点*/,
			getMultiLangByID('201203004A-000017') /*使用部门盘点*/,
			getMultiLangByID('201203004A-000018') /*管理部门盘点*/,
			getMultiLangByID('201203004A-000019') /*资产类别盘点*/,
			getMultiLangByID('201203004A-000020') /*按类别 + 管理部门盘点*/,
			getMultiLangByID('201203004A-000021') /*按类别 + 使用部门盘点*/
		];
		return (
			<div>
				<div className="allocation-person">
					<div className="radioLists">
						<p>
							<span>
								{getMultiLangByID('201203004A-000022', { total_num }) /*共有{total_num}个资产卡片，请选择分配类型*/}
							</span>
						</p>
						{radioArr.map((item, index) => {
							return (
								<div
									className={inventoryWay - 1 == index ? 'active' : ''}
									onClick={() => handleChange.call(this, index + 1, that)}
								>
									{item}
								</div>
							);
						})}
					</div>
					<div className="takeStockInfo">
						{inventoryWay == '1' ? (
							<div className="alert-info">
								<div className="alert-img-area" />
								<p>{getMultiLangByID('201203004A-000023') /*使用人：每台设备的使用盘点自己的设备*/}</p>
							</div>
						) : (
							<div>
								<div className="editTable-btn-area ">
									{createButtonApp({
										//按钮区域（在数据库中注册的按钮区域）
										area: 'assign',
										onButtonClick: this.buttonClick.bind(this),
										//指定下拉弹窗渲染的位置，不设置的话，下拉弹窗随滚动条滚动
										popContainer: document.querySelector('.header-button-area')
									})}
								</div>
								<div>
									{createEditTable(
										constant.AREA.STEP.ALLOT_CHECKUSER, //表格id
										{
											showIndex: true,
											showCheck: true,
											onSelected: this.rowSelected.bind(this),
											onSelectedAll: this.rowSelected.bind(this),
											onBeforeEvent: stepTwoAreaBeforeEvent.bind(this, that),
											onAfterEvent: stepTwoAreaAfterEvent.bind(this)
										}
									)}
								</div>
							</div>
						)}
					</div>
					{createModal('empty', {
						title: getMultiLangByID('201203004A-000025') /*注意*/,
						content: getMultiLangByID('201203004A-000024') /*切换分配类型，将清空未盘资产的计划盘点人，确认切换？*/,
						beSureBtnClick: this.emptyConfirm.bind(this),
						cancelBtnClick: this.emptyCancel.bind(this)
					})}
				</div>
			</div>
		);
	}
}
