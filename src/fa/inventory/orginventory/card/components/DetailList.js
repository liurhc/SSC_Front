import React, { Component } from 'react';
import constant from '../../constants';
import { afterEvent } from '../events';
import { detailAreaBeforeEvent } from './event';
export default class extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let { createCardTable } = this.props.cardTable;
		let { that } = this.props;
		return (
			<div className="nc-bill-table-area">
				{createCardTable(constant.AREA.STEP.DETAIL_LIST, {
					onAfterEvent: afterEvent.bind(this, that),
					onBeforeEvent: detailAreaBeforeEvent.bind(this),
					showIndex: true,
					hideModelSave: true,
					hideAdd: true,
					hideDel: true
				})}
			</div>
		);
	}
}
