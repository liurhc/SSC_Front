import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;
const { tableId, pagecode, url } = pageConfig;

export default function(props, config, pks) {
	let data = {
		allpks: pks,
		pagecode
	};
	//得到数据渲染到页面
	//得到数据渲染到页面
	ajax({
		url: url.queryPageUrl,
		data,
		success: (res) => {
			setListValue.call(this, props, res);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
