import constant from '../../constants';
import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage } = fa_components;
const { referLinkageClear } = ReferLinkage;

/**
 * 查询区编辑后事件
 * @param {*} key 
 * @param {*} value 
 */
export default function afterEvent(key, value) {
	if (key == 'pk_org') {
		referLinkageClear.call(this, this.props, key, constant.DEFAULTSEARCHCONFIG);
	}
}
