//按钮点击事件处理
import { ajax, toast, print, output } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import searchBtnClick from './searchBtnClick';
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, listUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { showMessage, showConfirm, MsgConst } = msgUtils;
const { LoginContext, ScriptReturnUtils } = components;
const { setContext } = LoginContext;
const { getScriptListReturn } = ScriptReturnUtils;

const {
	printFilename,
	appcode,
	searchAreaId,
	tableId,
	pagecode,
	pkField,
	url,
	printNodekey,
	cardRouter,
	batchBtns
} = pageConfig;
export default function buttonClick(props, id) {
	//按钮事件处理
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Commit':
			CommitAction.call(this, 'SAVE', props);
			break;
		case 'UnCommit':
			CommitAction.call(this, 'UNSAVE', props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
		default:
			break;
	}
}
/**
 * 新增
 * @param {*} props 
 */
export function add(props) {
	linkToCard.call(this, props, undefined, 'add');
}
/**
 * @param {*} props 
 */
export function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	showConfirm.call(this, props, {
		type: MsgConst.Type.DelSelect,
		beSureBtnClick: batchDel
	});
}
/**
 * 删除
 * 
 * @param {*} props 
 */
function batchDel(props) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pkField].value;
		let ts = v.data.values['ts'].value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, pkField, tableId.replace('list', 'card'), tableId, true);
			// 平台还没有支持批量，暂时用单个循环
			params.map((item) => {
				props.table.deleteCacheId(tableId, item.id);
			});
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}
// 提交
export function CommitAction(OperatorType, props, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	// 提交或收回分别需过滤出不符合其操作的数据
	let params = data.map((v) => {
		let id = v.data.values.pk_inventory.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecode,
			commitType: commitType,
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				false,
				pageConfig.dataSource
			);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	batchRefresh.call(this, props, searchBtnClick);
}
/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printListUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.printListUrl,
		data: printData
	});
}
/**
* 获取打印数据
* @param {*} props 
* @param {*} outputType 
*/
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values['pk_inventory'].value);
	});
	let filenameValue = getMultiLangByID(printFilename);
	let printData = {
		filename: filenameValue, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}
/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
}
/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	props.pushTo(cardRouter, {
		status,
		id: record[pkField] ? record[pkField].value : '',
		pagecode: pagecode.replace('list', 'card')
	});
}
/**
 * 附件管理
 * @param {*} props 
 */
function attachment(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	if (!checkedrows || checkedrows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOne });
		return;
	}
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billId 是单据主键
	let billId = checkedrows[0].data.values[pkField].value;
	props.ncUploader.show(`${pagecode}-uploader`, { billId: 'fa/inventory/' + billId, billNo });
}
/* 查看盘点结果 */
export function viewResult(props, key, record, index) {
	setContext('ts', record.ts.value);
	setContext('bill_status', record.bill_status.value);

	props.pushTo(cardRouter, {
		status: 'edit',
		current: '7',
		bill_code: record.bill_code.value,
		pk_org: record.pk_org.value,
		id: record[pkField] ? record[pkField].value : '',
		ts: record.ts.value,
		pagecode: pagecode.replace('list', 'card')
	});
}
