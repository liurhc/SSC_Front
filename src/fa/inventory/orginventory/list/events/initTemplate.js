import tableButtonClick from './tableButtonClick';
import { pageConfig } from '../const';
import { setBatchBtnsEnable, viewResult } from './buttonClick';
import constant from '../../constants';
import ampub from 'ampub';
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { loginContext } = LoginContext;
const { addSearchAreaReferFilter } = ReferFilter;

const { searchAreaId, tableId, pagecode } = pageConfig;

export default function(props) {
	props.createUIDom(
		{
			//页面id
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /*确认要删除吗？*/);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	setBatchBtnsEnable.call(this, props, tableId);
}

// 修饰数据
function modifierMeta(props, meta) {
	addSearchAreaReferFilter(props, meta, constant.DEFAULTSEARCHCONFIG);
	//将卡片编号改为参照
	meta[searchAreaId].items.map((item) => {
		if (item.attrcode == 'list.card_code') {
			item.itemtype = 'refer';
			item.datatype = '204';
			item.refcode = 'fa/refer/newasset/facardTreeGridRef/index.js';
			item.maxlength = '20';
			item.queryCondition = () => {
				let data = getSearchValue.call(this, props, 'pk_org'); // 调用相应组件的取值API
				let filter = {
					isShowUnit: false,
					DataPowerOperationCode: 'default',
					isDataPowerEnable: true
				};
				if (data) {
					filter['pk_org'] = data;
				}
				return filter; // 根据pk_org过滤
			};
		}
	});
	//修改列渲染样式
	meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div
						class="simple-table-td"
						field="bill_code"
						fieldname={getMultiLangByID('201203004A-000052') /*盘点单号*/}
					>
						<span
							className="code-detail-link"
							onClick={() => {
								viewResult.call(this, props, key, record, index);
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableId].items.push({
		label: getMultiLangByID('amcommon-000000') /*操作*/,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		visible: true,
		// 锁定在右边
		fixed: 'right',
		width: '210px',
		render: (text, record, index) => {
			let buttonAry =
				record.bill_status && record.bill_status.value == 0
					? constant.LIST_BTNS.LIST_INNER_FREE_BTNS
					: record.bill_status.value == 1
						? constant.LIST_BTNS.LIST_INNER_COMMIT_BTNS
						: record.bill_status.value == 2
							? constant.LIST_BTNS.LIST_INNER_APPROVING_BTNS
							: record.bill_status.value == 3
								? constant.LIST_BTNS.LIST_INNER_APPROVED_BTNS
								: record.bill_status.value == 4 ? constant.LIST_BTNS.LIST_INNER_UNAPPROVED_BTNS : null;
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(this, props, key, text, record, index)
			});
		}
	});
	return meta;
}

//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchAreaId, field);
	let value = '';
	if (data && data.value) {
		value = data.value.firstvalue;
	}
	if (value && value.split(',').length == 1) {
		return value;
	} else {
		return '';
	}
}
