//表体行操作列处理
import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ScriptReturnUtils, queryAboutUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListApprove } = queryAboutUtils;
const { tableId, pagecode, pkField, url, cardRouter, pagecodecard } = pageConfig;

export default function tableButtonClick(props, key, text, record, index) {
	// 表格操作按钮
	switch (key) {
		// 删除
		case 'Delete':
			singleDel.call(this, props, record, index);
			break;
		// 提交
		case 'Commit':
			commitClick.call(this, 'SAVE', props, tableId, index, record);
			break;
		// 收回
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', props, tableId, index, record);
			break;
		// 重新分配盘点人
		case 'ReInventory':
			reInventory.call(this, props, key, record, index);
			break;
		// 审批详情
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, record);
			break;
		default:
			break;
	}
	/* 删除 */
	function singleDel(props, record, index) {
		let id = record[pkField].value;
		let ts = record['ts'].value;
		let params = [
			{
				id,
				index
			}
		];
		let paramInfoMap = {
			[id]: ts
		};

		ajax({
			url: url.commitUrl,
			data: {
				paramInfoMap: paramInfoMap,
				dataType: 'listData',
				OperatorType: 'DELETE',
				pageid: pagecode.replace('list', 'card'),
				commitType: 'commit' //提交
			},
			success: (res) => {
				getScriptListReturn.call(
					this,
					params,
					res,
					props,
					pkField,
					tableId.replace('list', 'card'),
					tableId,
					true
				);
				// 清除缓存
				props.table.deleteCacheId(tableId, id);
			},
			error: (res) => {
				if (res && res.message) {
					toast({ content: res.message, color: 'danger' });
				}
			}
		});
	}
	/* 审批详情 */
	function queryAboutBillFlow(record) {
		openListApprove(this, record, 'pk_inventory');
	}
	// 重新分配盘点人
	function reInventory(props, key, record, index) {
		let queryInfo = {};
		let tempUserdefObj = {
			pk_inventory: record.pk_inventory.value,
			signature: '7',
			inventoryWay: '1'
		};
		queryInfo.userdefObj = tempUserdefObj;
		ajax({
			url: url.queryAssignSavedUrl,
			data: queryInfo,
			success: (res) => {
				if (res.data) {
					if (res.data.total == 'null') {
						toast({
							content: getMultiLangByID('201203004A-000034') /*所有资产已盘点完毕，不支持重新分配！*/,
							color: 'warning'
						});
					} else {
						props.pushTo(cardRouter, {
							status: 'edit',
							current: '1',
							id: record[pkField] ? record[pkField].value : '',
							total_num: res.data.total,
							pagecode: pagecode.replace('list', 'card'),
							pk_org: record.pk_org.value
						});
					}
				}
			}
		});
	}
	/* 提交 收回 */
	function commitClick(OperatorType, props, tableId, index, record) {
		let paramInfoMap = {};
		paramInfoMap[record.pk_inventory.value] = record.ts.value;
		let params = [ { id: record.pk_inventory.value, index: index } ];
		let commitType = '';
		if (OperatorType === 'SAVE') {
			//提交传这个
			commitType = 'commit';
		}
		ajax({
			url: url.commitUrl,
			data: {
				paramInfoMap: paramInfoMap,
				dataType: 'listData',
				OperatorType: OperatorType,
				pageid: pagecodecard,
				commitType: commitType
			},
			success: (res) => {
				getScriptListReturn.call(
					this,
					params,
					res,
					props,
					pkField,
					tableId.replace('list', 'card'),
					tableId,
					false,
					pageConfig.dataSource
				);
			},
			error: (res) => {
				if (res && res.message) {
					toast({ color: 'danger', content: res.message });
				}
			}
		});
	}
}
