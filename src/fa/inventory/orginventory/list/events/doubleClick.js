import { viewResult } from './buttonClick';

/* 行双击事件 */
export default function doubleClick(record, index, props, key) {
	viewResult.call(this, props, key, record, index);
}
