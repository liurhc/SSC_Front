//固定资产盘点单列表
import React, { Component } from 'react';
import { createPage, ajax, base, high, createPageIcon } from 'nc-lightapp-front';
import {
	CommitAction,
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	doubleClick,
	rowSelected,
	setBatchBtnsEnable,
	afterEvent
} from './events';
import { pageConfig } from './const';
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans } = components;
const { ApproveDetail } = high;
const { url, title, searchAreaId, tableId, dataSource, pkField, pagecode } = pageConfig;
class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			/* 审批详情 */
			pk_evaluate: '',
			showApprove: false,
			bill_type: '',
			/* 打印 */
			printData: {},
			/* 提交指派 */
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}
	componentDidMount() {}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		CommitAction.call(this, 'SAVE', this.props, value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { table, button, ncmodal, search, ncUploader } = this.props;
		const { createButtonApp } = button;
		const { createModal } = ncmodal;
		const { createSimpleTable } = table;
		const { NCCreateSearch } = search;
		let { createNCUploader } = ncUploader;

		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					{/* 标题 title */}
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
					</div>
					{/* 按钮区 btn-area */}
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchAreaId, {
						onAfterEvent: afterEvent.bind(this),
						clickSearchBtn: searchBtnClick.bind(this),
						dataSource: dataSource,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						}
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						showCheck: true,
						showIndex: true,
						handlePageInfoChange: pageInfoClick.bind(this),
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						pkname: pkField,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/* 附件管理框 */}
				{createNCUploader(`${pagecode}-uploader`, {})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_inventory}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002') /*指派*/}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

// List = createPage({
// 	initTemplate: initTemplate
// })(List);

// ReactDOM.render(<List />, document.querySelector('#app'));
const MasterChildListBase = createPage({})(List);
export default MasterChildListBase;
