// 页面配置
export const pageConfig = {
	// 应用主键
	appid: '0001Z910000000003CDD',
	// 应用编码
	appcode: '201203004A',
	// 应用名称
	title: '201203004A-000044' /*盘点单*/,
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '201203004A_list',
	// 卡片页面编码
	pagecodecard: '201202504A_card',
	// 主键字段
	pkField: 'pk_inventory',
	// 单据类型
	bill_type: 'HP',
	// 交易类型
	transi_type: 'HP-01',
	// 输出文件名称
	printFilename: '201203004A-000045' /*固定资产盘点*/,
	// 请求链接
	url: {
		queryUrl: '/nccloud/fa/orginventory/query.do', //列表查询
		queryPageUrl: '/nccloud/fa/orginventory/queryPage.do', // 列表分页
		printUrl: '/nccloud/fa/orginventory/printCard.do', //打印输出
		printListUrl: '/nccloud/fa/orginventory/printList.do', //列表打印输出
		commitUrl: '/nccloud/fa/orginventory/commit.do', //提交收回
		queryAssignSavedUrl: '/nccloud/fa/orginventory/queryAssignSaved.do', //重新分配盘点人
		queryInventoryWayUrl: '/nccloud/fa/orginventory/queryInventoryWay.do' //重新分配盘点人
	},
	// 打印模板节点标识
	printNodekey: null,
	cardRouter: '/card',
	// 权限资源编码
	resourceCode: '2012048020',
	dataSource: 'fa.inventory.orginventory.main',
	batchBtns: [ 'Delete', 'Commit', 'UnCommit', 'Attachment', 'Print', 'Output' ]
};
