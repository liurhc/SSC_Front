import ampub from 'ampub';
const { commonConst } = ampub;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;

/**
 * 常量定义
 * @author liuzjk
 * @since 2018-7-9
 */

const IS_DEV = false; //是否为开发阶段， 真是发布版本后这里是 false
const CHECK_RANGE_LENGTH = 100; // 盘点范围最大长度
const BILL_TYPE = 'HP'; //固定资产盘点 单据类型

const FILE_NAME = '201203004A'; //打印文件名称
const APP_CODE = '201203004A'; //打印文件名称
//界面编码
const PAGE_CODE = {
	LIST: '201203004A_list', //列表态模板编码
	CARD: '201203004A_card', //卡片态模板编码
	STEP: '201203004A_card' //步骤条模板编码
};
// 参照组织过滤
const DEFAULTSEARCHCONFIG = {
	searchId: 'searchArea',
	formId: 'card_head',
	bodyIds: [ 'list_head' ],
	specialFields: {
		card_code: {
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		pk_usedept_after: {
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		pk_mandept_before: {
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		pk_mandept_after: {
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		assetuser_before: {
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		assetuser_after: {
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		}
	},
	linkageData: {
		//联动链
		//参照字段:['被过滤字段1'，'被过滤字段2']
		pk_org: [
			'list.pk_category', //资产类别
			'list.card_code', //卡片编号
			'list.pk_usedept_before', //使用部门(盘点前)
			'list.pk_usedept_after', //使用部门(盘点后)
			'list.assetuser_before', //资产的使用人(盘点前)
			'list.assetuser_after', //资产的使用人(盘点后)
			'list.pk_mandept_before', //管理部门(盘点前)
			'list.pk_mandept_after' //管理部门(盘点后)
		]
	},

	defPrefix: {
		search: [ 'list.def', 'def' ],
		head: 'def',
		body: 'def'
	},
	onlyLeafCanSelect: {
		pk_usingstatus_before: false,
		pk_usingstatus_after: false
	}
};
const DEFAULTRANGECONFIG = {
	searchId: 'childform1',
	formId: 'card_head',
	bodyIds: [ 'bodyvos', 'tableArea2' ],
	defPrefix: {
		search: [ 'list.def', 'def' ],
		head: 'def',
		body: 'def'
	},
	specialFields: {
		pk_usedept: {
			//被过滤字段名
			data: [
				{
					fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
					returnName: 'pk_org' //被过滤字段名后台需要参数
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		pk_mandept: {
			//管理部门
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		pk_category: {
			//资产类别
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		},
		pk_dept: {
			//部门
			data: [
				{
					fields: [ 'pk_equiporg', 'pk_ownerorg', 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		pk_ownerorg: {
			addOrgRelation: 'pk_orgs',
			RefActionExtType: 'TreeRefActionExt',
			class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_OWNERORG_VSqlBuilder',
			//货主管理组织
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		}
	},
	linkageData: {
		//联动链
		//参照字段:['被过滤字段1'，'被过滤字段2']
		pk_org: [
			'pk_mandept', //管理部门
			'pk_usedept', //使用部门
			'pk_category' //资产类别
		],
		pk_ownerorg_after_v: [
			//货主管理组织
			'pk_mandept_after_v', //管理部门
			'pk_mandept_after'
		],
		pk_equiporg_after_v: [
			//使用权
			'pk_usedept_after_v', //使用部门
			'pk_usedept_after',
			'pk_usedept_after_v.name',
			'assetuser_after' //使用人
		]
	}
};

// 界面状态
const UISTATE = {
	ADD: 'add', // 新增态
	EDIT: 'edit', // 编辑态
	BROWSE: 'browse' // 浏览态
};

const NODE_CODE = '201203004A'; //节点功能编码
const APPID = '0001Z910000000003CDD'; //小应用ID   1009z91000000000zcsp
const SEARCH_TEMPLATE_ID = '1002Z910000000002ZCU'; //查询模板id

//区域
const AREA = {
	LIST: {
		//列表态
		SEARCH_AREA: 'searchArea', // 查询区
		GRID_AREA: 'list_head', //列表区
		QUERYTYPE: 'tree'
	},
	STEP: {
		//步骤条

		INVENTORY_RANGE: 'childform1', // 盘点范围区域
		INVENTORY_DATE: 'childform2', // 盘点时间区域
		ALLOT_CHECKUSER: 'tableArea2', // 分配盘点人区域
		ANEWALLOT: 'anewAllot', // 未分配区域
		DETAIL_LIST: 'bodyvos', // 盘点明细
		QUERYTYPE: 'tree' // 浏览态查询区查询类型
	},
	CARD: {
		//卡片态
		BODY_HEAD: 'card_head', //卡片态表头区域
		BODY_AREA: 'bodyvos', //卡片态表体区域
		BODY_EXTEND_AREA: 'bodyvos_browse', //卡片态表体展开区域
		BODY_SIDESLIP_AREA: 'bodyvos_edit', //卡片态c侧滑区域
		TAIL_AREA: 'tail' //卡片态表尾区域
	}
};
const DATASOURCE = 'fa.inventory.orginventory.main';
const ALLOT_FILED = [ 'pk_equiporg', 'pk_ownerorg', 'pk_dept', 'pk_category', 'inventory_checkuser' ];
const BTN_AREA = {
	LIST_HEAD: 'list_head', //列表态肩部
	LIST_TABLE_HEAD: '',
	LIST_TABLE_INNER: 'list_inner', //列表态表体行
	CARD_HEAD: 'card_head', //卡片态肩部
	CARD_TABLE_HEAD: 'card_body', //卡片态表格肩部
	CARD_TABLE_INNER: 'card_body_inner' //卡片态表体行
};

//  路径
const URL = {
	QUERY_LIST: '/nccloud/fa/orginventory/query.do',
	QUERY_ASSIGN: '/nccloud/fa/orginventory/queryAssign.do', // 第二步查询有无未分配
	SAVE: '/nccloud/fa/orginventory/save.do', // 保存
	QUERY_SAVED: '/nccloud/fa/orginventory/querySaved.do', // 保存后再次查询有无未分配盘点人
	QUERY_RESULT: '/nccloud/fa/orginventory/queryResult.do', //穿透查询
	COMMIT: '/nccloud/fa/orginventory/commit.do', // 提交
	UNCOMMIT: '/nccloud/fa/orginventory/unCommit.do', // 提交
	DELETE: '/nccloud/fa/orginventory/delete.do', // 删除
	PRINT_LIST: '/nccloud/fa/orginventory/printList.do', // 列表态打印输出
	PRINT_CARD: '/nccloud/fa/orginventory/printCard.do', // 卡片态打印输出
	QUERY_NOINVENCHECKUSER: '/nccloud/fa/orginventory/queryNoInvenCheckUser.do', // 查询保存后未分配盘点人
	QUERY_CARDMESSAGE: '/nccloud/fa/orginventory/queryCardMessage.do' // 查询卡片信息，为盘点单赋值
};

// 列表态buttons
const LIST_BTNS = {
	// 头部
	LIST_HEAD_BTNS: [ 'EditGroup', 'Commit', 'Attachment', 'Print', 'Refresh' ],
	// 列表_自由态操作列buttons
	LIST_INNER_FREE_BTNS: [ 'Commit', 'Delete', 'ReInventory' ],
	// 列表_已提交操作列buttons
	LIST_INNER_COMMIT_BTNS: [ 'UnCommit', 'QueryAboutBillFlow' ],
	// 列表_审核中操作列buttons
	LIST_INNER_APPROVING_BTNS: [ 'QueryAboutBillFlow' ],
	// 列表_已审核操作列buttons
	LIST_INNER_APPROVED_BTNS: [ 'UnCommit', 'QueryAboutBillFlow' ],
	// 列表_审核未通过操作列
	LIST_INNER_UNAPPROVED_BTNS: [ 'QueryAboutBillFlow', 'Delete' ]
};
// 步骤条buttons
const STEP_BTNS = {
	//所有按钮
	ALL_BUTTONS: [
		'Confirm',
		'Previous',
		'Next',
		'Save',
		'BatchAlter',
		'ReInventory',
		'ReInventoryPeople',
		'Cancel',
		'Commit',
		'QueryAboutBillFlow',
		'UnCommit',
		'Attachment',
		'Delete',
		'Edit',
		'AddLine',
		'Print',
		'Output',
		'Refresh',
		'AllMatch'
	],
	//步骤条_第一步 buttons
	STEP_ONE_BTNS: [ 'Next', 'Cancel' ],
	//步骤条_第二步 buttons
	STEP_TWO_BTNS: [ 'Next', 'Previous', 'Cancel' ],
	//步骤条_第二步操作列 buttons
	STEP_TWO_INNER_BTNS: [ 'DelLine' ],
	//步骤条_第二到第三步未分配 buttons
	STEP_TWO_HALF_BTNS: [ 'Confirm', 'BatchAlter', 'ReInventory', 'Cancel' ],
	//步骤条_第三步 buttons
	STEP_THREE_BTNS: [ 'Save', 'Cancel' ],
	//步骤条_保存后 buttons
	STEP_SAVED: [ 'ReInventoryPeople', 'Attachment', 'Delete' ],

	//盘点结果_自由态
	RESULT_FREE: [ 'Commit', 'QueryAboutBillFlow', 'Attachment', 'Print', 'Output', 'Refresh' ],
	//盘点结果_已提交
	RESULT_COMMIT: [ 'UnCommit', 'QueryAboutBillFlow', 'Attachment', 'Print', 'Output', 'Refresh' ],
	//盘点结果_审核中
	RESULT_APPROVING: [ 'QueryAboutBillFlow', 'Attachment', 'Print', 'Output', 'Refresh' ],
	//盘点结果_审批通过
	RESULT_APPROVED: [ 'UnCommit', 'QueryAboutBillFlow', 'Attachment', 'Print', 'Output', 'Refresh' ],
	//盘点结果_审批未通过
	RESULT_UNAPPROVED: [ 'Attachment', 'QueryAboutBillFlow', 'Print', 'Output', 'Refresh' ],

	//穿透_浏览态
	SKIP_BROWSE: [ 'Edit', 'Print', 'Output' ],
	//穿透_编辑态
	SKIP_EDIT: [ 'Save', 'Cancel', 'AllMatch', 'AddLine' ],
	//穿透_不可编辑态
	SKIP_UNEDIT: [ 'Print', 'Output' ],
	//穿透_浏览态_操作列
	SKIP_INNER_BROWSE: [ 'OpenCard', 'OffLoss', 'DelLine' ],
	//穿透_编辑态_每个条目——操作列
	SKIP_INNER_BROWSE_OPEN: [ 'OpenCard' ],
	SKIP_INNER_BROWSE_OFFLOSS: [ 'OffLoss', 'OpenCard' ],
	SKIP_INNER_BROWSE_PROFIT: [ 'OpenCard', 'DelLine' ],
	//穿透_编辑态_操作列
	SKIP_INNER_EDIT: [ 'OpenCard', 'OffLoss', 'DelLine' ],
	//穿透_不可编辑态_操作列
	SKIP_INNER_UNEDIT: [ 'Detail' ],
	//穿透列表_所有按钮
	SKIP_ALL_BUTTON: [ 'OpenCard', 'OffLoss', 'DelLine', 'Detail' ],
	//盘点结果_未分配盘点人
	RESULT_UNALLOT_BUTTON: [ 'Confirm', 'BatchAlter' ]
};
const LEGEND_NAME = [
	'201203004A-000004' /*未盘*/,
	'201203004A-000036' /*相符*/,
	'201203004A-000037' /*不符*/,
	'201203004A-000038' /*盘盈*/,
	'201203004A-000039' /*盘亏*/,
	'201203004A-000040' /*全部*/,
	'201203004A-000051' /*不符+盘盈+盘亏*/
];
//自由态编码-这个与模板上的保持一致即可 不方便抽取多语，故废弃，在引用出定义
/*const BILL_STATUS = {
	UNCOMMIT: {
		display: '自由态',
		value: '0'
	},
	COMMIT: {
		display: '已提交',
		value: '1'
	},
	APPROVE_ING: {
		display: '审批中',
		value: '2'
	},
	APPROVE: {
		display: '审批通过',
		value: '3'
	},
	UNAPPROVE: {
		display: '审批未通过',
		value: '4'
	},
	CLOSE: {
		display: '关闭',
		value: '6'
	}
};*/
/* 盘点结果 */
const CHECK_RESULT = {
	EQUAL: {
		display: '201203004A-000036' /*相符*/,
		value: '0'
	},
	PROFIT: {
		display: '201203004A-000038' /*盘盈*/,
		value: '1'
	},
	REDUCE: {
		display: '201203004A-000039' /*盘亏*/,
		value: '2'
	},
	NOT_EQUAL: {
		display: '201203004A-000037' /*不符*/,
		value: '3'
	},
	NOT_INVENT: {
		display: '201203004A-000004' /*未盘*/,
		value: '0'
	}
};
//数据状态
const DATA_STATUS = {
	NOCHANGED: 0, //未改变
	UPDATED: 1, //更新
	NEW: 2, //新增
	DELETED: 3 //删除
};

//字段类型
const FIELDS = {
	HEAD_PK: 'pk_inventory', //表头主键字段
	BODY_PK: 'pk_inventory_b' //子表主键字段
};

// 盘点字段（盘点后(带版本)）
const INVENTORY_ITEMS = {
	WITH_V: {
		pk_usedept_after_v: 'pk_usedept_after_v',
		pk_mandept_after_v: 'pk_mandept_after_v',
		pk_equiporg_after_v: 'pk_equiporg_after_v',
		pk_ownerorg_after_v: 'pk_ownerorg_after_v'
	},
	UNWITH_V: {
		card_num_after: 'card_num_after',
		assetuser_after: 'assetuser_after',
		position_after: 'position_after',
		pk_usingstatus_after: 'pk_usingstatus_after'
	}
};

// 盘点需要处理前后赋值的字段
const INVENTORY_ISHIDDEN_ITEMS = {
	pk_usingstatus_after: 'pk_usingstatus_after',
	pk_mandept_after_v: 'pk_mandept_after_v',
	pk_mandept_after: 'pk_mandept_after',
	assetuser_after: 'assetuser_after',
	position_after: 'position_after',
	pk_ownerorg_after_v: 'pk_ownerorg_after_v',
	pk_ownerorg_after: 'pk_ownerorg_after',
	pk_equiporg_after: 'pk_equiporg_after',
	pk_equiporg_after_v: 'pk_equiporg_after_v',
	pk_usedept_after_v: 'pk_usedept_after_v',
	pk_usedept_after: 'pk_usedept_after'
};

const constant = {
	IS_DEV,
	FILE_NAME,
	APP_CODE,
	PAGE_CODE,
	UISTATE,
	NODE_CODE,
	APPID,
	SEARCH_TEMPLATE_ID,
	AREA,
	BTN_AREA,
	URL,
	LIST_BTNS,
	STEP_BTNS,
	FIELDS,
	DATA_STATUS,
	BILL_TYPE,
	DEFAULTSEARCHCONFIG,
	DEFAULTRANGECONFIG,
	LEGEND_NAME,
	CHECK_RESULT,
	ALLOT_FILED,
	DATASOURCE,
	INVENTORY_ITEMS,
	CHECK_RANGE_LENGTH,
	INVENTORY_ISHIDDEN_ITEMS
};

export default constant;
