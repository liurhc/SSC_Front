/**
 * 累计折旧入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import List from '../../base/list';

const AlterListBase = createPage({})(List);

// 页面配置
const pageConfig = {
	pagecode: '201202016A_list', //单据模板pageID
	appcode: '201202016A', //小应用编码
	transType: 'HG-06', //交易类型
	printNodekey: null,
	pagename: 'accudep',
	dataSource: 'fa.alter.accudep.main',
	title: '201202004A-000000' //表头标题
};

// ReactDOM.render(<AccudepList pageConfig={pageConfig} />, document.querySelector('#app'));
export default class AccudepList extends Component {
	render() {
		return <AlterListBase pageConfig={pageConfig} />;
	}
}
