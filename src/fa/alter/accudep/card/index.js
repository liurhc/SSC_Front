/**
 * 资产累计折旧入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import Card from '../../base/card';

const AlterCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: '201202016A_card',
		headcode: 'card_head',
		bodycode: 'bodyvos'
	}
})(Card);

const pageConfig = {
	pagecode: '201202016A_card', //页面编码
	appcode: '201202016A', //小应用编码
	printNodekey: null,
	transType: 'HG-06', //交易类型
	title: '201202004A-000000',
	pagename: 'accudep',
	dataSource: 'fa.alter.accudep.main'
};

// ReactDOM.render(<AccudepCard pageConfig={pageConfig} />, document.querySelector('#app'));
export default class AccudepCard extends Component {
	render() {
		return <AlterCardBase pageConfig={pageConfig} />;
	}
}
