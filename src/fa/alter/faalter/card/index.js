/**
 * 固定资产变动入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import Card from '../../base/card';

const AlterCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: '201202036A_card',
		headcode: 'card_head',
		bodycode: 'bodyvos'
	}
})(Card);

const pageConfig = {
	pagecode: '201202036A_card', //页面编码
	appcode: '201202036A', //小应用编码
	transType: 'HG-00', //交易类型
	title: '201202004A-000052',
	pagename: 'faalter',
	printNodekey: null,
	dataSource: 'fa.alter.faalter.main'
};

// ReactDOM.render(<FaAlterCard pageConfig={pageConfig} />, document.querySelector('#app'));
export default class FaAlterCard extends Component {
	render() {
		return <AlterCardBase pageConfig={pageConfig} />;
	}
}
