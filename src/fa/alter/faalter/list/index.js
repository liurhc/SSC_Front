/**
 * 其他变动调整入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import List from '../../base/list';

const AlterListBase = createPage({})(List);

// 页面配置
const pageConfig = {
	pagecode: '201202036A_list', //单据模板pageID
	appcode: '201202036A', //小应用编码
	title: '201202004A-000052', //表头标题
	pagename: 'faalter',
	printNodekey: null,
	transType: 'HG-00', //交易类型
	dataSource: 'fa.alter.faalter.main'
};

// ReactDOM.render(<FaalterList pageConfig={pageConfig} />, document.querySelector('#app'));
export default class FaalterList extends Component {
	render() {
		return <AlterListBase pageConfig={pageConfig} />;
	}
}
