/**
 * 价值调整入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax } from 'nc-lightapp-front';
import Card from '../../base/card';

const AlterCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: '201202032A_card',
		headcode: 'card_head',
		bodycode: 'bodyvos'
	}
})(Card);

const pageConfig = {
	pagecode: '201202032A_card', //页面编码
	appcode: '201202032A', //小应用编码
	transType: 'HG-05', //交易类型
	title: '201202004A-000058',
	printNodekey: null,
	pagename: 'value',
	dataSource: 'fa.alter.value.main'
};

// ReactDOM.render(<ValueCard pageConfig={pageConfig} />, document.querySelector('#app'));
export default class ValueCard extends Component {
	render() {
		return <AlterCardBase pageConfig={pageConfig} />;
	}
}
