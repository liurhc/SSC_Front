/**
 * 价值调整入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import List from '../../base/list';

const AlterListBase = createPage({})(List);

// 页面配置
const pageConfig = {
	pagecode: '201202032A_list', //单据模板pageID
	appcode: '201202032A', //小应用编码
	transType: 'HG-05', //交易类型
	printNodekey: null,
	pagename: 'value',
	dataSource: 'fa.alter.value.main',
	title: '201202004A-000058' //表头标题
};

// ReactDOM.render(<ValueList pageConfig={pageConfig} />, document.querySelector('#app'));
export default class ValueList extends Component {
	render() {
		return <AlterListBase pageConfig={pageConfig} />;
	}
}
