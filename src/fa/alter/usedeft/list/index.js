/**
 * 原币原值调整入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import List from '../../base/list';

const AlterListBase = createPage({})(List);

// 页面配置
const pageConfig = {
	pagecode: '201202024A_list', //单据模板pageID
	appcode: '201202024A', //小应用编码
	transType: 'HG-14', //交易类型
	printNodekey: null,
	pagename: 'usedeft',
	dataSource: 'fa.alter.usedeft.main',
	title: '201202004A-000057' //表头标题
};

// ReactDOM.render(<UseDeftList pageConfig={pageConfig} />, document.querySelector('#app'));
export default class UseDeftList extends Component {
	render() {
		return <AlterListBase pageConfig={pageConfig} />;
	}
}
