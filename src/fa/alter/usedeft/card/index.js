/**
 * 使用部门调整入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import Card from '../../base/card';

const AlterCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: '201202024A_card',
		headcode: 'card_head',
		bodycode: 'bodyvos'
	}
})(Card);

const pageConfig = {
	pagecode: '201202024A_card', //页面编码
	appcode: '201202024A', //小应用编码
	transType: 'HG-14', //交易类型
	title: '201202004A-000057',
	printNodekey: null,
	pagename: 'usedeft',
	dataSource: 'fa.alter.usedeft.main'
};

// ReactDOM.render(<UseDeftCard pageConfig={pageConfig} />, document.querySelector('#app'));
export default class UseDeftCard extends Component {
	render() {
		return <AlterCardBase pageConfig={pageConfig} />;
	}
}
