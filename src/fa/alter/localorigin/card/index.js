/**
 * 本币原值调整入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import Card from '../../base/card';

const AlterCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: '201202008A_card',
		headcode: 'card_head',
		bodycode: 'bodyvos'
	}
})(Card);

const pageConfig = {
	pagecode: '201202008A_card', //页面编码
	appcode: '201202008A', //小应用编码
	transType: 'HG-02', //交易类型
	title: '201202004A-000053',
	printNodekey: null,
	pagename: 'localorigin',
	dataSource: 'fa.alter.localorigin.main'
};

// ReactDOM.render(<LocalOriginCard pageConfig={pageConfig} />, document.querySelector('#app'));
export default class LocalOriginCard extends Component {
	render() {
		return <AlterCardBase pageConfig={pageConfig} />;
	}
}
