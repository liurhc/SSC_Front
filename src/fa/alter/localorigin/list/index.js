/**
 * 本币原值调整入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import List from '../../base/list';

const AlterListBase = createPage({})(List);

// 页面配置
const pageConfig = {
	pagecode: '201202008A_list', //单据模板pageID
	appcode: '201202008A', //小应用编码
	title: '201202004A-000053', //表头标题
	printNodekey: null,
	pagename: 'localorigin',
	dataSource: 'fa.alter.localorigin.main',
	transType: 'HG-02' //交易类型
};

// ReactDOM.render(<LocalOriginList pageConfig={pageConfig} />, document.querySelector('#app'));
export default class LocalOriginList extends Component {
	render() {
		return <AlterListBase pageConfig={pageConfig} />;
	}
}
