/**
 * 资产追溯入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import List from '../../base/list';

const AlterListBase = createPage({})(List);

// 页面配置
const pageConfig = {
	pagecode: '201202012A_list', //单据模板pageID
	appcode: '201202012A', //小应用编码
	transType: 'HG-04', //交易类型
	printNodekey: null,
	pagename: 'trace',
	dataSource: 'fa.alter.trace.main',
	title: '201202004A-000056' //表头标题
};

// ReactDOM.render(<TraceList pageConfig={pageConfig} />, document.querySelector('#app'));
export default class TraceList extends Component {
	render() {
		return <AlterListBase pageConfig={pageConfig} />;
	}
}
