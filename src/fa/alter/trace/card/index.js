/**
 * 资产追溯入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import Card from '../../base/card';

const AlterCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: '201202012A_card',
		headcode: 'card_head',
		bodycode: 'bodyvos'
	}
})(Card);

const pageConfig = {
	pagecode: '201202012A_card', //页面编码
	appcode: '201202012A', //小应用编码
	transType: 'HG-04', //交易类型
	title: '201202004A-000056',
	printNodekey: null,
	pagename: 'trace',
	dataSource: 'fa.alter.trace.main'
};

// ReactDOM.render(<TraceCard pageConfig={pageConfig} />, document.querySelector('#app'));
export default class TraceCard extends Component {
	render() {
		return <AlterCardBase pageConfig={pageConfig} />;
	}
}
