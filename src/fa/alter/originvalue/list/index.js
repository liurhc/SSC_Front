/**
 * 原币原值入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import List from '../../base/list';

const AlterListBase = createPage({})(List);

// 页面配置
const pageConfig = {
	pagecode: '201202004A_list', //单据模板pageID
	appcode: '201202004A', //小应用编码
	title: '201202004A-000055', //表头标题
	transType: 'HG-01', //交易类型
	pagename: 'originvalue',
	printNodekey: null,
	dataSource: 'fa.alter.originvalue.main'
};

// ReactDOM.render(<LocalOriginList pageConfig={pageConfig} />, document.querySelector('#app'));
export default class OriginValueList extends Component {
	render() {
		return <AlterListBase pageConfig={pageConfig} />;
	}
}
