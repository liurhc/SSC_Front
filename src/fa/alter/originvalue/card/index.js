/**
 * 原币原值入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import Card from '../../base/card';

const AlterCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: '201202004A_card',
		headcode: 'card_head',
		bodycode: 'bodyvos'
	}
})(Card);

const pageConfig = {
	pagecode: '201202004A_card', //页面编码
	appcode: '201202004A', //小应用编码
	printNodekey: null,
	transType: 'HG-01', //交易类型
	title: '201202004A-000055',
	pagename: 'originvalue',
	dataSource: 'fa.alter.originvalue.main'
};

// ReactDOM.render(<OriginValueCard pageConfig={pageConfig} />, document.querySelector('#app'));
export default class OriginValueCard extends Component {
	render() {
		return <AlterCardBase pageConfig={pageConfig} />;
	}
}
