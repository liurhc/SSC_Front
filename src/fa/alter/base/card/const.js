export const baseConfig = {
	tableId: 'bodyvos', //表体编码
	tableEditId: 'bodyvos_edit', //表体侧拉编辑区域编码
	formId: 'card_head', //表头编码
	cardBody: 'card_body', //表体区域
	bill_type: 'HG', //单据类型
	ds: 'dataSource',
	account_field: [
		'localoriginvalue',
		'tax_input',
		'taxinput_flag',
		'accudep',
		'curryeardep',
		'predevaluate',
		'depamount',
		'deprate',
		'salvage',
		'salvagerate',
		'depunit',
		'netvalue',
		'netrating',
		'pk_depmethod',
		'servicemonth',
		'servicemonth_display',
		'dep_start_date',
		'dep_end_date',
		'naturemonth',
		'naturemonth_display',
		'usedmonth',
		'localcurr_rate',
		'paydept_flag'
	],
	alterVO: {
		pk_alter: 'pk_alter', //变动单主键
		pk_org_v: 'pk_org_v', //组织key
		pk_card: 'pk_card', //卡片主键
		asset_code_after: 'asset_code_after', //变动后资产编码
		ts: 'ts',
		bill_status: 'bill_status', //单据状态
		bill_code: 'bill_code', //单据号
		pk_org: 'pk_org', //组织
		pk_accbook: 'pk_accbook',
		business_date: 'business_date', //业务日期
		pk_equiporg_v_after: 'pk_equiporg_v_after',
		pk_costcenter_after: 'pk_costcenter_after',
		pk_assetuser_after: 'pk_assetuser_after',
		pk_ownerorg_v_after: 'pk_ownerorg_v_after',
		pk_mandept_v_after: 'pk_mandept_v_after',
		usedept_before: 'usedept_before',
		pk_depmethod_after: 'pk_depmethod_after',
		pk_depmethod_before: 'pk_depmethod_before',
		usedept_after: 'usedept_after', //使用部门变动后
		pk_bill_src: 'pk_bill_src', //来源单据主键
		bill_type_src: 'bill_type_src', //来源单据类型
		transi_type_src: 'transi_type_src', //来源交易类型
		pk_reason: 'pk_reason', //变动原因
		pk_fa_alters: 'pk_fa_alters' //自定义的卡片变动项目
	},
	listRouter: '/list',
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'More',
		//影像
		'FAReceiptGroup',
		'FAReceiptScan',
		'FAReceiptShow',
		//联查
		'QueryAbout',
		'QueryAboutGroup',
		'QueryAboutBillSrc',
		'QueryAboutBillFlow',
		'FABillQueryAboutVoucher',

		//打印
		'PrintGroup',
		'Print',
		'Output',
		'Refresh',
		'OpenCard'
	], //浏览态展示按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'AddLine', 'DelLine', 'OpenCard', 'BatchAlter', 'BatchFormulaAction' ], //编辑态展示按钮
	addhideBtns: [ 'AddLine', 'DelLine', 'BatchAlter', 'BatchFormulaAction' ],
	url: {
		saveURL: '/nccloud/fa/alter/save.do',
		updateURL: '/nccloud/fa/alter/update.do',
		deleteURL: '/nccloud/fa/alter/delete.do',
		headAfterEditURL: '/nccloud/fa/alter/headAfterEdit.do',
		bodyAfterEditURL: '/nccloud/fa/alter/bodyAfterEdit.do',
		queryCardByPkURL: '/nccloud/fa/alter/queryCardByPk.do',
		queryCardByPkURL: '/nccloud/fa/alter/queryCardByPk.do',
		openAlterMsgURL: '/nccloud/fa/alter/openAlterMsg.do',
		editURL: '/nccloud/fa/alter/edit.do',
		commitURL: '/nccloud/fa/alter/commit.do',
		batchAlterURL: '/nccloud/fa/alter/batchAlter.do',
		printCardURL: '/nccloud/fa/alter/printCard.do',
		batchFormulaURL: '/nccloud/fa/alter/batchFormula.do',
		lockcard: '/nccloud/fa/facard/lockcard.do',
		querySrcUrl: '/nccloud/ampub/common/amLinkQuery.do'
	},
	//支持批量公式的字段
	canEditFields: [
		'localoriginvalue_alter',
		'originvalue_alter',
		'accudep_alter',
		'salvage_alter',
		'currmoney_alter',
		'localoriginvalue_after',
		'originvalue_after',
		'accudep_after',
		'salvage_after',
		'currmoney_after'
	]
};
