//主子表卡片
import React, { Component } from 'react';
import { toast, high, ajax } from 'nc-lightapp-front';
import {
	initTemplate,
	buttonClick,
	formAfterEvent,
	bodyAfterEvent,
	pageInfoClick,
	bodyBeforeEvent,
	setValue,
	backToList,
	rowSelected,
	save,
	commit,
	lockcards,
	lockrequest,
	saveCommit
} from './events';
import { baseConfig } from './const';
const { tableId, formId, url, cardBody } = baseConfig;
const { FormulaEditor } = high;
import ampub from 'ampub';
import fa from 'fa';

const { fa_components } = fa;
const { WorkloadModal } = fa_components;
const { components, utils, commonConst } = ampub;
const { ApprovalTrans,LoginContext } = components;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { loginContextKeys, getContext} = LoginContext;
const { showConfirm, showMessage,MsgConst} = msgUtils;

class Card extends Component {
	constructor(props) {
		super(props);
		this.is_allow_dept_midlev = false;
		this.state = {
			//表体是否显示
			checkboxVisible: true,
			//公式编辑器涉及字段
			show: false,
			//	name: '公式页面',
			editingField: '',
			editingRow: 0,
			tabBuild: [],
			//工作量法处理的值
			isworkloan: false,
			row_number: null,
			workloanvalues: {},
			//审批详情
			showApprove: false,
			pkAlter: '',
			//提交及指派
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {}

	componentWillMount() {
		window.onbeforeunload = () => {
			// 解锁卡片
			let status = this.props.cardTable.getStatus(tableId);
			if (status == 'edit') {
				lockcards.call(this, this.props, 'unlock', false);
			}
		};
		if (window.history && window.history.pushState) {
			window.onpopstate = () => {
				lockcards.call(this, this.props, 'unlock', false);
			};
		}
	}

	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	//提交及指派 回调
	getAssginUsedr = (value) => {		
		let status = this.props.form.getFormStatus(formId) || UISTATE.browse;
		if (status == UISTATE.browse) {
			commit.call(this, this.props, value);
		} else {
			saveCommit.call(this, this.props, value);
		}
	};

	// 移除事件
	componentWillUnmount() {
		window.onbeforeunload = null;
	}
	modelDelRowBefore = (props, moduleId, moduleIndex, row) => {
		let pk_bill_src = props.form.getFormItemsValue(formId, 'pk_bill_src');
		if (pk_bill_src != null && pk_bill_src != '' && pk_bill_src.value != null && pk_bill_src.value != '') {
			showMessage(props, { content: getMultiLangByID('201202004A-000026'), color: 'warning' });
			return false;
		}
		let { pageConfig } = props;
		let { dataSource } = pageConfig;
		let allpks = [ row.values.pk_card.value ];
		let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
		let usrid = getContext('userId', dataSource);
		let param = {
			allpks,
			msgMap: {
				usrid,
				lockflag: 'unlock',
				dSource
			}
		};
		lockrequest.call(this, param);
		return true;
	};

	//获取列表肩部信息
	getTableHead = () => {
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{this.props.button.createButtonApp({
						area: cardBody,
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this)
						//popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	//公式处理参数
	FormulaClos = ({ setName, setExplain, name }) => {
		let tabBuild = this.state.tabBuild;
		return (
			<ul class="tab-content">
				{tabBuild.indexOf('localoriginvalue_before') > -1 && (
					<li
						class="tab-content-item"
						onClick={() => {
							setExplain(getMultiLangByID('201202004A-000029'));
						}}
						onDoubleClick={() => {
							setName('localoriginvalue_before');
						}}
					>
						{getMultiLangByID('201202004A-000029')}
					</li>
				)}
				{tabBuild.indexOf('accudep_before') > -1 && (
					<li
						class="tab-content-item"
						onClick={() => {
							setExplain(getMultiLangByID('201202004A-000030'));
						}}
						onDoubleClick={() => {
							setName('accudep_before');
						}}
					>
						{getMultiLangByID('201202004A-000030')}
					</li>
				)}
				{tabBuild.indexOf('salvage_before') > -1 && (
					<li
						class="tab-content-item"
						onClick={() => {
							setExplain(getMultiLangByID('201202004A-000031'));
						}}
						onDoubleClick={() => {
							setName('salvage_before');
						}}
					>
						{getMultiLangByID('201202004A-000031')}
					</li>
				)}
				{tabBuild.indexOf('originvalue_before') > -1 && (
					<li
						class="tab-content-item"
						onClick={() => {
							setExplain(getMultiLangByID('201202004A-000032'));
						}}
						onDoubleClick={() => {
							setName('originvalue_before');
						}}
					>
						{getMultiLangByID('201202004A-000032')}
					</li>
				)}
				{tabBuild.indexOf('currmoney_before') > -1 && (
					<li
						class="tab-content-item"
						onClick={() => {
							setExplain(getMultiLangByID('201202004A-000033'));
						}}
						onDoubleClick={() => {
							setName('currmoney_before');
						}}
					>
						{getMultiLangByID('201202004A-000033')}
					</li>
				)}
			</ul>
		);
	};

	// 公式编辑后处理
	formulaAfter = (val) => {
		let { pageConfig } = this.props;
		let { pagecode } = pageConfig;
		let card_data = this.props.createMasterChildDataSimple(pagecode, formId, tableId);
		//获取当前界面表体展示字段
		let showListKey = [];
		let items = this.props.meta.getMeta().bodyvos.items;
		for (let i = 0; i < items.length; i++) {
			if (items[i].visible) {
				showListKey.push(items[i].attrcode);
			}
		}
		card_data.userjson = showListKey.join();
		//判断表体是否有数据
		if(card_data.body.bodyvos.rows && card_data.body.bodyvos.rows.length > 0){
			ajax({
				url: url.batchFormulaURL,
				data: {
					card: card_data,
					set_field: this.state.editingField,
					row_number: this.state.editingRow,
					input_formula: val
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data) {
							setValue.call(this, this.props, data);
						}
					}
				}
			});
		}		
	};

	//返回列表界面
	backToList = () => {
		let { pageConfig } = this.props;
		let { pagecode, pagename } = pageConfig;
		let url = '/fa/alter/' + pagename + '/list/index.html';
		this.props.linkTo(url, {
			pagecode
		});
	};
	render() {
		let { cardTable, form, ncmodal, cardPagination, pageConfig, ncUploader } = this.props;
		let { createForm } = form;
		const { createCardPagination } = cardPagination;
		let { createCardTable } = cardTable;
		let { createModal } = ncmodal;
		let { createNCUploader } = ncUploader;
		let { pagecode, title, dataSource, transType } = pageConfig;
		const { ApproveDetail } = high;

		let status = form.getFormStatus(formId);

		let isAdd = true;
		//如果是价值调整，把自动增行去掉
		if (transType === 'HG-05') {
			isAdd = false;
		}

		let config = {
			modalId: 'worked', //弹框的ID
			defaultValue: {
				//默认值
				total: 0.0, //总工作量
				accumulation: 0.0, //累计工作量
				unit: getMultiLangByID('201202004A-000034') //工作量单位
			},
			onSureClick: (total, accumulation, unit) => {
				let row = this.state.row_number;
				this.props.cardTable.setValByKeyAndIndex(tableId, row, 'allworkloan_after', { value: total });
				this.props.cardTable.setValByKeyAndIndex(tableId, row, 'accuworkloan_after', { value: accumulation });
				this.props.cardTable.setValByKeyAndIndex(tableId, row, 'workloanunit_after', { value: unit });
				let workloanvalues = {};
				workloanvalues.allworkloan = total;
				workloanvalues.accuworkloan = accumulation;
				workloanvalues.workloanunit = unit;
				this.setState({
					workloanvalues: workloanvalues
				});
			}
		};

		return (
			<div id={pagecode}>
				<div className="nc-bill-card">
					<div className="nc-bill-top-area">
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								{createCardTitleArea.call(this, this.props, {
									title: getMultiLangByID(title),
									formId,
									backBtnClick: backToList
								})}
							</div>
							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: formId,
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this)
									//popContainer: document.querySelector('.header-button-area')
								})}
								{createModal(`${tableId}-del-confirm`, { color: 'warning' })}
							</div>

							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>

						<div className="nc-bill-form-area">
							{createForm(formId, {
								onAfterEvent: formAfterEvent.bind(this)
							})}
						</div>
					</div>
					<div className="nc-bill-bottom-area">
						<div className="nc-bill-table-area">
							{/* {this.getTableHead()} */}
							{createCardTable(tableId, {
								tableHeadLeft: this.getTableHeadLeft.bind(this),
								tableHead: this.getTableHead.bind(this),
								showIndex: true,
								//showTotal: true,
								isAddRow: false,
								modelDelRowBefore: this.modelDelRowBefore.bind(this),
								modelSave: save.bind(this),
								onAfterEvent: bodyAfterEvent.bind(this),
								onBeforeEvent: bodyBeforeEvent.bind(this),
								showCheck: this.state.checkboxVisible,
								selectedChange: rowSelected.bind(this)
							})}
						</div>
					</div>
				</div>
				<WorkloadModal {...this.props} config={config} />
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{createNCUploader('alter-uploader', {disableModify:this.props.form.getFormItemsValue(formId, 'bill_status')&&this.props.form.getFormItemsValue(formId, 'bill_status').value!="0"})}
				<FormulaEditor
                    zIndex={300}
					ref="formulaEditor"
					show={this.state.show}
					value=""
					formulaConfig={[
						{
							tab: getMultiLangByID('201202004A-000035'),
							TabPaneContent: this.FormulaClos,
							key: 'formula',
							params: {}
						}
					]}
					noShowFormula={[
						getMultiLangByID('201202004A-000036'),
						getMultiLangByID('201202004A-000037'),
						getMultiLangByID('201202004A-000038'),
						getMultiLangByID('201202004A-000039'),
						getMultiLangByID('201202004A-000040'),
						getMultiLangByID('201202004A-000041'),
						getMultiLangByID('201202004A-000042'),
						getMultiLangByID('201202004A-000043'),
						getMultiLangByID('201202004A-000044')
					]}
					noShowAttr={[ 'all' ]}
					onOk={(res) => {
						this.setState({
							show: false
						});
						this.formulaAfter(res);
					}} //点击确定回调
					onCancel={(a) => {
						this.setState({ show: false });
					}} //点击确定回调
					onHide={(a) => {
						this.setState({ show: false });
					}}
				/>
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({
							showApprove: false
						});
					}}
					billtype={transType}
					billid={this.state.pkAlter}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('201202004A-000035')}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

export default Card;

export { initTemplate };
