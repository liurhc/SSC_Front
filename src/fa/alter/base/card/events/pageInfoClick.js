import { ajax } from 'nc-lightapp-front';
import { baseConfig } from '../const';
import { setBillFlowActionVisible, setValue, setStatus } from './buttonClick';
const { url, tableId, formId } = baseConfig;
/**
 * 分页查询
 * @param {*} props 
 * @param {*} pk 
 */
export default function(props, pk) {
	let { pageConfig } = props;
	let { pagecode } = pageConfig;
	let data = {
		pk: pk,
		pagecode: pagecode
	};
	ajax({
		url: url.queryCardByPkURL,
		data: data,
		success: (res) => {
			if (res.data) {
				setStatus.call(this, props);
				setValue.call(this, props, res.data);
				props.setUrlParam(pk); //动态修改地址栏中的id的值			
			}
		}
	});
}
