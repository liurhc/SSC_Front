/*
 * @Author: wangqsh
 * @PageInfo: 卡片态，table区编辑事件
 * @Date: 2018-06-05 09:20:22 
 */
import { toast, ajax } from 'nc-lightapp-front';
import { baseConfig } from '../const';
import { setValue,addLine } from './buttonClick';
import ampub from 'ampub';


const { utils } = ampub;
const { msgUtils } = utils;
const { showMessage} = msgUtils;
const { url, tableId, formId, alterVO } = baseConfig;

/**
 * 表格编辑后事件
 * @param {*} props 
 * @param {*} moduleId 区域id
 * @param {*} key 操作的键
 * @param {*} value 当前值
 * @param {*} changedrows 新旧值集合
 * @param {*} index 当前行index
 * @param {*} record 行数据
 * @param {*} type 表格内为line，弹窗为modal
 * @param {*} method 有blur有change
 * @param {*} flag 自动触发标志：true为自动触发（表头组套件选择之后自动执行），false为手动触发（页面手动选择）
 */
export default function(props, moduleId, key, value, changedRows, index, record, type, method, flag = false) {
	/**
	 * 操作行号时，不触发编辑后
	 */
	if (key == 'crowno' || key == alterVO.usedept_after || key == alterVO.pk_reason) {
		return;
	}

	//使用月限等字段为空时，不走编辑后事件
	if ((value == null || value == '') && key === 'naturemonth_after') {
		return;
	}

	let { pageConfig } = props;
	let { pagecode } = pageConfig;

	//编辑的是使用权，清空成本中心、使用部门、使用人
	if (key == alterVO.pk_equiporg_v_after && changedRows[0].newvalue.value != changedRows[0].oldvalue.value) {
		props.cardTable.setValByKeyAndIndex(tableId, index, alterVO.pk_costcenter_after, {
			value: null,
			display: null
		});
		props.cardTable.setValByKeyAndIndex(tableId, index, alterVO.usedept_after, { value: null, display: null });
		props.cardTable.setValByKeyAndIndex(tableId, index, alterVO.pk_assetuser_after, { value: null, display: null });
	}
	//编辑货主管理组织，清空管理部门
	if (key == alterVO.pk_ownerorg_v_after && changedRows[0].newvalue.value != changedRows[0].oldvalue.value) {
		props.cardTable.setValByKeyAndIndex(tableId, index, alterVO.pk_mandept_v_after, { value: null, display: null });
	}

	//获取当前界面表体展示字段
	let showListKey = [];
	let items = this.props.meta.getMeta().bodyvos.items;
	for (let i = 0; i < items.length; i++) {
		if (items[i].visible) {
			showListKey.push(items[i].attrcode);
		}
	}

	//如果编辑的是卡片，需要将所有数据传到后台，进行卡片加解锁以及重复校验；编辑的是其他字段，则只传当前行
	if (key === 'pk_card') {		
		//使用createBodyAfterEventData 初始化为后台平台可以使用的格式
		let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, index);

		//调用后台的时候，将变动项目去掉，因为只传value到后台，平台公共代码不会翻译变动项目
		if (data.card.head[formId] && data.card.head[formId].rows) {
			let bodyAfterRows = data.card.head[formId].rows;
			if (bodyAfterRows && bodyAfterRows[0] && bodyAfterRows[0].values && bodyAfterRows[0].values.pk_fa_alters) {
				delete bodyAfterRows[0].values.pk_fa_alters;
			}
		}

		data.userJson = JSON.stringify({
			showListKey: showListKey
		});
		ajax({
			url: url.bodyAfterEditURL,
			data: data,
			async: false,
			success: (res) => {
				let { success, data } = res;
				if (success) {					
					setValue.call(this, props, data);
					let num = props.cardTable.getNumberOfRows(tableId);
					//如果为最后一行 , 自动增行
					if (num == index + 1) {
						addLine.call(this, props);
					}
				}
			},
			error: (res) => {
				//卡片报错，清除当前行，再重新选择行
				showMessage(props, { color: 'danger', content: res.message });				
				props.cardTable.addRow(tableId, index,{}, false);
				props.cardTable.delRowsByIndex(tableId, index+1);
			}
		});
	} else {
		basebodyaftereditevent.call(
			this,
			props,
			moduleId,
			key,
			value,
			changedRows,
			index,
			record,
			showListKey,
			pagecode
		);
	}
}

function basebodyaftereditevent(props, moduleId, key, value, changedRows, index, record, showListKey, pagecode) {
	let cardData = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, index);

	//新增功能---差异化处理
	let newRecord = JSON.parse(JSON.stringify(record || {}));
	cardData.card.body[moduleId] = {
		...cardData.card.body[moduleId],
		rows: [ newRecord ]
	};
	cardData.index = 0; //修改编辑行为0
	cardData.userJson = JSON.stringify({
		showListKey: showListKey
	});

	ajax({
		url: url.bodyAfterEditURL,
		data: cardData,
		async: false,
		success: (res) => {
			if (res.data && res.data.body && res.data.body[tableId]) {
				if (res.data.userjson) {
					let userObj = JSON.parse(res.data.userjson);
					if (userObj && userObj['salvageError'] != null) {
						//修复编辑后事件优化后传单行导致提示信息行号不正确问题
						let msg = userObj['salvageError'];				
						showMessage(props, { color: 'warning', content: msg.replace(cardData.index+1,index+1)});
					}
				}
				//新增功能---差异化处理
				props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);

				if (key === alterVO.pk_depmethod_after) {
					let pk_depmethod_before = props.cardTable.getValByKeyAndIndex(
						tableId,
						index,
						alterVO.pk_depmethod_before
					);
					let pk_depmethod_after = props.cardTable.getValByKeyAndIndex(
						tableId,
						index,
						alterVO.pk_depmethod_after
					);
					if (
						pk_depmethod_before &&
						pk_depmethod_after &&
						pk_depmethod_before.value != pk_depmethod_after.value &&
						value &&
						value.refcode &&
						value.refcode == '04'
					) {
						//如果是工作量法，弹框
						this.setState({
							isworkloan: true,
							row_number: index
						});
						props.modal.show('worked');
					}
				}
			}
		},
		error: (res) => {
			//其他字段报错，清空当前字段
			props.cardTable.setValByKeyAndIndex(tableId, index, key, { display: null, value: null });
			//如果是追溯报错，清空追溯日期
			let { pageConfig } = props;
			let { transType } = pageConfig;
			if (transType === 'HG-04') {
				props.cardTable.setValByKeyAndIndex(tableId, index, 'tracedate', { display: null, value: null });
			}
			showMessage(props, { color: 'danger', content: res.message });
		}
	});
}
