import { setBodyBtnsEnable, lockrequest } from './buttonClick';
import { baseConfig } from '../const';
import ampub from 'ampub';


const { components, utils, commonConst } = ampub;
const { LoginContext } = components;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { loginContextKeys, getContext} = LoginContext;
const { showMessage} = msgUtils;
/**
 * 表格操作
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 * @param {*} tableId 
 */
export default function(props, key, text, record, index, tableId) {
	let { pageConfig } = props;
	let { dataSource } = pageConfig;
	switch (key) {
		// 展开
		case 'OpenCard':
			openCard.call(this, props, record, index, tableId);
			break;
		case 'DelLine':
			let pk_bill_src = props.form.getFormItemsValue(baseConfig.formId, 'pk_bill_src');
			if (pk_bill_src != null && pk_bill_src != '' && pk_bill_src.value != null && pk_bill_src.value != '') {
				showMessage(props, { content: getMultiLangByID('201202004A-000026'), color: 'warning' });
				return;
			}
			props.cardTable.delRowsByIndex(tableId, index);
			// 若行数据为新增态，则需解锁卡片
			if (record.status === '2') {
				if (record.values.pk_card && record.values.pk_card.value) {
					let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
					let usrid = getContext('userId', dataSource);
					let param = {
						allpks: [ record.values.pk_card.value ],
						msgMap: {
							usrid,
							dSource,
							lockflag: 'unlock'
						}
					};
					lockrequest.call(this, param);
				}
			}
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		default:
			break;
	}
}

/**
 * 表格行展开
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function openCard(props, record, index, tableId) {
	let status = props.cardTable.getStatus(tableId);
	if (status == UISTATE.edit) {
		props.cardTable.openModel(tableId, status, record, index);
	} else if (status == UISTATE.browse) {
		props.cardTable.toggleRowView(tableId, record);
	}
}
