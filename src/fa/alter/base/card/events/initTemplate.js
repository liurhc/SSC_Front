import buttonClick, { setStatus, getDataByPk, getAlterMsg, edit,add } from './buttonClick';
import tableButtonClick from './tableButtonClick';
import { baseConfig } from '../const';
import ampub from 'ampub';
import fa from 'fa';

const { fa_components } = fa;
const { ReferFilter, DeptMidlevRefFilter } = fa_components;
const { rewriteMeta } = DeptMidlevRefFilter;
const { addHeadAreaReferFilter,addBodyReferFilter } = ReferFilter;
const { components, utils, commonConst } = ampub;
const { faQueryAboutUtils, LoginContext } = components;
const { openAssetCardByPk } = faQueryAboutUtils;
const { loginContext, getContext } = LoginContext;
const { cardUtils, multiLangUtils } = utils;
const { afterModifyCardMeta } = cardUtils;
const { StatusUtils, CommonKeys} = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { UISTATE } = StatusUtils;
const { getMultiLangByID } = multiLangUtils;


const { tableId, formId, alterVO, tableEditId } = baseConfig;

export default function(props) {
	let { pageConfig } = props;
	let { dataSource, pagecode } = pageConfig;
	props.createUIDom(
		{
			pagecode: pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, dataSource);
					// 获取数据源
					// getContext(baseConfig.ds, dataSource);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status');
	if (status == UISTATE.add) {
		//处理消息打开
		let pkMsg = this.props.getUrlParam('pk_msg');
		if (pkMsg) {
			add.call(this, props,false);	
			getAlterMsg.call(this, this.props, pkMsg);
		} else {
			add.call(this, props);
		}		
	} else if (status == UISTATE.edit) {
		let pk = props.getUrlParam('id');
		props.button.setButtonVisible(baseConfig.browseBtns, false);
		if (pk && pk != 'undefined' && pk != 'null') {
			let callback = () => {
				edit.call(this, props, false);
			};
			getDataByPk.call(this, props, pk, callback);
		}
	} else {
		setStatus.call(this, props, status);
		let pk = this.props.getUrlParam('id');
		if (pk && pk != 'undefined') {
			getDataByPk.call(this, this.props, pk);
		}
	}
}

//修改meta
function modifierMeta(props, meta) {
	let { pageConfig } = props;
	let { transType } = pageConfig;
	let defaultConfig = {
		formId: formId,
		bodyIds: [ tableId, tableEditId ],
		specialFields: {
			//成本中心
			pk_costcenter_after: {
				RefActionExtType: 'TreeRefActionExt',
				class: 'nccloud.web.fa.common.refCondition.CostCenterRefFilter',
				data: [
					{
						fields: [ 'pk_equiporg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			},
			//货主管理组织
			pk_ownerorg_v_after: {
				addOrgRelation: 'pk_orgs',
				RefActionExtType: 'TreeRefActionExt',
				class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_OWNERORG_VSqlBuilder', //货主管理组织
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			},
			//使用部门
			usedept_after: {
				data: [
					{
						fields: [ 'pk_equiporg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			//管理部门
			pk_mandept_v_after: {
				//被过滤字段名
				orgMulti: 'pk_org', //添加左上角主组织参照
				data: [
					{
						fields: [ 'pk_ownerorg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			//使用人
			pk_assetuser_after: {
				//被过滤字段名
				orgMulti: 'pk_org', //添加左上角主组织参照
				data: [
					{
						fields: [ 'pk_equiporg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			pk_jobmngfil_after: {//变动后项目档案
				data: [
					//字段过滤
					{
						fields: [ 'pk_org' ], 
						returnName: 'pk_org'
					}
				]
			},
			provider_after : {//变动后供应商
				data: [
					//字段过滤
					{
						fields: [ 'pk_org' ], 
						returnName: 'pk_org'
					}
				]
			}

		},
		defPrefix: {
			body: [ 'def', 'pk_card.def' ]
		}
	};
	addHeadAreaReferFilter.call(this, props, meta, defaultConfig);
	//表体过滤
	addBodyReferFilter.call(this, props, meta, defaultConfig);
	//如果放出了账簿，卡片需要根据账簿来过滤
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_card') {
				item.queryCondition = () => {
					let accbookFlag = props.form.getFormItemsVisible(formId, 'pk_accbook');

					let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
					pk_org = pk_org && pk_org.value;
					let pk_accbook = props.form.getFormItemsValue(formId, 'pk_accbook');
					pk_accbook = pk_accbook && pk_accbook.value;
					//如果放出了账簿，卡片需要根据账簿来过滤
					if(accbookFlag){
						return {
							pk_org,
							pk_accbook
						};
					} else {
						return {
							pk_org							
						};
					}									
				};
			}
	});
	let field = [ 'pk_mandept_after', 'pk_mandept_v_after', 'usedept_after' ];
	let assembleData = {
		meta,
		formId,
		tableId,
		field
	};
	rewriteMeta.call(this, props, assembleData);
	//判断如果是固定资产变动，修改meta，增加变动项目参照
	if ('HG-00' == transType) {
		//获取表头所有item
		let items = meta[formId].items;
		let businessIndex = 0;
		//循环表头item，获取业务日期所在下标，变动项目默认+1，放在业务日期之后
		for (let i = 0; i < items.length; i++) {
			if (alterVO.business_date == items[i].attrcode) {
				businessIndex = i;
			}
			if (props.getUrlParam('pk_msg') && alterVO.pk_accbook == items[i].attrcode) {
				items[i].visible = false;
			}
		}
		let faAlterItem = {
			itemtype: 'refer',
			col: '4',
			attrcode: 'pk_fa_alters',
			visible: true,
			label: getMultiLangByID('201202004A-000022'),
			maxlength: '20',
			required: true,
			metapath: 'pk_fa_alters',
			refcode: 'fa/refer/alter/alteritem/index.js'
		};
		items.splice(businessIndex + 1, 0, faAlterItem);

		//隐藏变动项
		let bodyItems = meta[tableId].items;
		for (let i = 0; i < bodyItems.length; i++) {
			if (
				bodyItems[i].attrcode.indexOf('_before') != -1 ||
				bodyItems[i].attrcode.indexOf('_after') != -1 ||
				(bodyItems[i].attrcode.indexOf('_alter') != -1 && bodyItems[i].visible == true)
			) {
				bodyItems[i].visible = false;
			}
		}
	}

	//参照处理
	meta[tableId].items.map((item) => {
		//变动前项目设为不可编辑	影响前期折旧不需要设置为不可编辑
		if (item.attrcode != "dep_value_before" && item.attrcode.endsWith('_before')) {
			item.disabled = true;
		}
		if (item.attrcode === 'pk_card') {
			item.isMultiSelectedEnabled = () => {
				return true;
			};
		}

		if (item.attrcode == alterVO.pk_card) {
			//给资产编码列添加超链接
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<a
						className="code-detail-link"
						onClick={() => {
							openAssetCardByPk.call(this, props, record.values.pk_card.value);
						}}
					>
						{record.values.pk_card && record.values.pk_card.display}
					</a>
				);
			};
		}
	});

	// 添加表格操作列
	let oprCol = {
		label: getMultiLangByID('201202004A-000023'),
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		// 锁定在右边
		fixed: 'right',
		width: '130px',
		render: (text, record, index) => {
			let status = props.form.getFormStatus(formId);
			let buttonAry = [];
			//其他变动不用展开，额外处理下
			if (status == UISTATE.add || status == UISTATE.edit) {
				if (transType === 'HG-00') {
					buttonAry = [ 'DelLine' ];
				} else {
					buttonAry = [ 'OpenCard', 'DelLine' ];
				}
			} else if (status == UISTATE.browse) {
				if (transType != 'HG-00') {
					buttonAry = [ 'OpenCard' ];
				}
			}

			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index, tableId);
				}
			});
		}
	};
	meta[tableId].items.push(oprCol);

	meta = afterModifyCardMeta.call(this, props, meta);
	return meta;
}
