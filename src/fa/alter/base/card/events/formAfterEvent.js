/**
 * 编辑后事件
 * @param {*} props 
 * @param {*} id 
 * @param {*} key 
 * @param {*} value 
 * @param {*} data 
 * @param {*} index 
 */
import { toast, ajax } from 'nc-lightapp-front';
import { baseConfig } from '../const';
import { setValue, setBodyBtnsEnable, lockcards, lockrequest,updateAssetGroupItem } from './buttonClick';
const { url, tableId, formId, alterVO } = baseConfig;
import ampub from 'ampub';

const { components,utils } = ampub;
const { OrgChangeEvent,LoginContext } = components;
const { orgChangeEvent } = OrgChangeEvent;
const { getContext} = LoginContext;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default function(props, moduleId, key, value, changedrows, val) {
	let { pageConfig } = props;
	let { pagecode, dataSource } = pageConfig;

	if (key == alterVO.pk_org_v) {
		let data = this.props.cardTable.getChangedRows(tableId);
		// status : 0不变，1修改，2新增，3删除
		let allpks = [];
		for (let val of data) {
			// 判断如果是新增删除，在删除时已解锁，这里不解锁，若是修改删除，在这里需要解锁
			if (!val.values.pk_alter_b.value && val.status === '3') {
			} else {
				if (val.values.pk_card.value) {
					allpks.push(val.values.pk_card.value);
				}
			}
		}
		let callback = () => {
			let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v').value;
			if (!pk_org_v) {
				//更换组织之后需要清空表体行
				props.cardTable.setTableData(tableId, { rows: [] });
			} else {
				props.cardTable.addRow(tableId, undefined, undefined, false);
			}
			let afterData = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
			ajax({
				url: url.headAfterEditURL,
				data: afterData,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						setBodyBtnsEnable.call(this, props, tableId);
						setValue.call(this, props, data);

						if(res.data.userjson){
							let userObj = JSON.parse(res.data.userjson);
							if(userObj){
								let isHeadAssetFinanceorg = userObj.isHeadAssetGroup;
								//资产组 字段设置
								updateAssetGroupItem(props, !isHeadAssetFinanceorg);
							}
						}

						let dSource = getContext(baseConfig.ds, dataSource);
						let usrid = getContext('userId', dataSource);
						let param = {
							allpks,
							msgMap: {
								usrid,
								lockflag: 'unlock',
								dSource
							}
						};
						lockrequest.call(this, param);
					}
				}
			});
		};
		//表头切换组织，清空备注和自定义项字段
		orgChangeEvent.call(
			this,
			props,
			pagecode,
			formId,
			tableId,
			key,
			value,
			changedrows,
			[
				'remark',
				'def1',
				'def2',
				'def3',
				'def4',
				'def5',
				'def6',
				'def7',
				'def8',
				'def9',
				'def10',
				'def11',
				'def12',
				'def13',
				'def14',
				'def15',
				'def16',
				'def17',
				'def18',
				'def19',
				'def20'
			],
			false,
			callback
		);
	}
	//切换账簿，清空表体
	if (key == alterVO.pk_accbook) {
		//表体清空解锁
		lockcards.call(this, props, 'unlock');
		// 清空原表体数据，并新增一行
		props.cardTable.setTableData(tableId, { rows: [] });
		props.cardTable.addRow(tableId);
	}
	if (key == alterVO.pk_fa_alters) {
		if (changedrows.value != '' && changedrows.value != null && value.value != changedrows.value) {
			props.ncmodal.show(`${pagecode}-confirm`, {
				title: getMultiLangByID('201202004A-000020'),
				content: getMultiLangByID('201202004A-000021'),
				beSureBtnClick: () => {
					//清空时，解锁卡片
					lockcards.call(this, this.props, 'unlock');
					props.cardTable.setTableData(tableId, { rows: [] }, undefined, false);
					if (value.value != changedrows.value) {
						if (changedrows.value) {
							let oldkeys = changedrows.value.split(',');
							let olditems = [];
							oldkeys.map((item) => {
								olditems.push(`${item}_before`);
								olditems.push(`${item}_alter`);
								olditems.push(`${item}_after`);
							});
							props.cardTable.hideColByKey(tableId, olditems);
						}
						if (value.value) {
							let newkeys = value.value.split(',');
							let newitems = [];
							newkeys.map((item) => {
								newitems.push(`${item}_before`);
								newitems.push(`${item}_alter`);
								newitems.push(`${item}_after`);
							});
							props.cardTable.showColByKey(tableId, newitems);
						}
						//处理其他变动，账簿信息字段是否显示
						if (value.value) {
							let account_field = baseConfig.account_field;
							let newkeys = value.value.split(',');
							let flag = false;
							for (let i = 0; i < newkeys.length; i++) {
								for (let j = 0; j < account_field.length; j++) {
									if (newkeys[i] === account_field[j]) {
										flag = true;
										break;
									}
								}
								if (flag) {
									break;
								}
							}
							if (flag) {
								props.form.setFormItemsVisible(formId, { pk_accbook: true });
							} else {
								props.form.setFormItemsVisible(formId, { pk_accbook: false });
							}
						} else {
							props.form.setFormItemsVisible(formId, { pk_accbook: false });
						}
					}
				},
				//取消按钮事件回调
				cancelBtnClick: () => {
					props.form.setFormItemsValue(formId, { [key]: changedrows });
				}
			});
		} else {
			if (value.value != changedrows.value) {
				if (changedrows.value) {
					let oldkeys = changedrows.value.split(',');
					let olditems = [];
					oldkeys.map((item) => {
						olditems.push(`${item}_before`);
						olditems.push(`${item}_alter`);
						olditems.push(`${item}_after`);
					});
					props.cardTable.hideColByKey(tableId, olditems);
				}
				if (value.value) {
					let newkeys = value.value.split(',');
					let newitems = [];
					newkeys.map((item) => {
						newitems.push(`${item}_before`);
						newitems.push(`${item}_alter`);
						newitems.push(`${item}_after`);
					});
					props.cardTable.showColByKey(tableId, newitems);
				}
			}
			//处理其他变动，账簿信息字段是否显示
			if (value.value) {
				let account_field = baseConfig.account_field;
				let newkeys = value.value.split(',');
				let flag = false;
				for (let i = 0; i < newkeys.length; i++) {
					for (let j = 0; j < account_field.length; j++) {
						if (newkeys[i] === account_field[j]) {
							flag = true;
							break;
						}
					}
					if (flag) {
						break;
					}
				}
				if (flag) {
					props.form.setFormItemsVisible(formId, { pk_accbook: true });
				} else {
					props.form.setFormItemsVisible(formId, { pk_accbook: false });
				}
			} else {
				props.form.setFormItemsVisible(formId, { pk_accbook: false });
			}
		}
	}
	return;
}
