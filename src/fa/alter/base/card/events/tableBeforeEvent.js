import { baseConfig } from '../const';
const { formId, tableId, tableEditId } = baseConfig;
import ampub from 'ampub';
import fa from 'fa';

const { fa_components } = fa;
const { ReferFilter, DeptMidlevRefFilter } = fa_components;
const { addBodyReferFilter } = ReferFilter;
const { queryOnlyLeafCanSelect } = DeptMidlevRefFilter;
const { utils, commonConst } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { showConfirm, showMessage,MsgConst} = msgUtils;

/**
 * 变动单表体编辑前事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} index 
 * @param {*} record 
 */
export default function(props, moduleId, key, value, index, record) {
	let { pageConfig } = props;
	let { transType } = pageConfig;
	if (key == 'pk_mandept_after' || key == 'pk_mandept_v_after' || key == 'usedept_after') {
		let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
		//表体部门非末级处理
		let field = [ 'pk_mandept_after', 'pk_mandept_v_after' ]; //传管理部门、使用部门字段
		let assembleData = {
			formId,
			tableId,
			pk_org,
			field
		};
		queryOnlyLeafCanSelect.call(this, props, assembleData);
		//相关过滤合并到下面过滤config
		// let defaultConfig = {
		// 	formId: formId,
		// 	bodyIds: [ tableId, tableEditId ],
		// 	onlyLeafCanSelect: {
		// 		// 使用部门非末级处理，默认不可勾选
		// 		pk_usedept: !this.is_allow_dept_midlev
		// 	}
		// };
		// let meta = props.meta.getMeta();
		// addBodyReferFilter.call(this, props, meta, defaultConfig);
	}
	if (key === 'pk_card' && transType === 'HG-00') {
		let faAlters = props.form.getFormItemsValue(formId, 'pk_fa_alters').value;
		if (faAlters == null || faAlters === '') {
			showMessage(props, { content: getMultiLangByID('201202004A-000025'), color: 'warning' });
			return false;
		}
	}	

	this.state.editingField = key;
	this.state.editingRow = index;

	let defaultConfig = {
		formId: formId,
		bodyIds: [ tableId, tableEditId ],
		onlyLeafCanSelect: {
			// 使用部门非末级处理，默认不可勾选
			pk_usedept: !this.is_allow_dept_midlev
		},
		specialFields: {
			//成本中心
			pk_costcenter_after: {
				RefActionExtType: 'TreeRefActionExt',
				class: 'nccloud.web.fa.common.refCondition.CostCenterRefFilter',
				data: [
					{
						fields: [ 'pk_equiporg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			},
			//货主管理组织
			pk_ownerorg_v_after: {
				addOrgRelation: 'pk_orgs',
				RefActionExtType: 'TreeRefActionExt',
				class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_OWNERORG_VSqlBuilder', //货主管理组织
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			},
			//使用部门
			usedept_after: {
				data: [
					{
						fields: [ 'pk_equiporg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			//使用人
			pk_assetuser_after: {
				//被过滤字段名
				orgMulti: 'pk_org', //添加左上角主组织参照
				data: [
					{
						fields: [ 'pk_equiporg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			//管理部门
			pk_mandept_v_after: {
				//被过滤字段名
				orgMulti: 'pk_org', //添加左上角主组织参照
				data: [
					{
						fields: [ 'pk_ownerorg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			}
		},
		defPrefix: {
			body: [ 'def', 'pk_card.def' ]
		}
	};
	let filterMeta = props.meta.getMeta();
	addBodyReferFilter.call(this, props, filterMeta, defaultConfig, record);
	//重新处理一下pk_card过滤条件
	filterMeta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_card') {
				item.queryCondition = () => {
					let accbookFlag = props.form.getFormItemsVisible(formId, 'pk_accbook');

					let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
					pk_org = pk_org && pk_org.value;
					let pk_accbook = props.form.getFormItemsValue(formId, 'pk_accbook');
					pk_accbook = pk_accbook && pk_accbook.value;
					//如果放出了账簿，卡片需要根据账簿来过滤
					if(accbookFlag){
						return {
							pk_org,
							pk_accbook
						};
					} else {
						return {
							pk_org							
						};
					}									
				};
			}
	});
	return true;
}
