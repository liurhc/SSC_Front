import { ajax, cardCache, toast, print, output } from 'nc-lightapp-front';

import { baseConfig } from '../const';
import headAfterEvent from './formAfterEvent';
const { url, tableId, formId, alterVO, listRouter, browseBtns, editBtns, canEditFields, bill_type,tableEditId } = baseConfig;
import ampub from 'ampub';
import fa from 'fa';

const { fa_components } = fa;
const { ReferLinkage, ImageMng } = fa_components;
const { referAlterClear } = ReferLinkage;
const { faImageScan, faImageView } = ImageMng;
const { components, utils, commonConst } = ampub;
const { ScriptReturnUtils, LoginContext, queryVocherUtils } = components;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { getContext, loginContextKeys } = LoginContext;
const { cardUtils, msgUtils, multiLangUtils } = utils;
const { setCardValue, setHeadAreaData } = cardUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { UISTATE,BILLSTATUS } = StatusUtils;

export default function(props, id) {
	let { pageConfig } = props;
	let { appcode } = pageConfig;

	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Save':
			save.call(this, props);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Delete':
			del.call(this, props);
			break;
		case 'SaveCommit':
			saveCommit.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props);
			break;
		case 'UnCommit':
			uncommit.call(this, props);
			break;
		case 'AddLine':
			addLine.call(this, props,true);
			break;
		case 'DelLine':
			delLine.call(this, props);
			break;
		case 'BatchAlter':
			batchAlter.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'FAReceiptScan':
			faReceiptScan.call(this, props);
			break;
		case 'FABillQueryAboutVoucher':
			queryAboutVoucher(props, formId, alterVO.pk_alter, appcode);
			break;
		case 'FAReceiptShow':
			faReceiptShow.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		case 'BatchFormulaAction':
			batchFormula.call(this, props);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props);
			break;
		case 'QueryAboutBillSrc':
			queryAboutBillSrc.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 批改
 * @param {*} props 
 */
export function batchAlter(props) {
	let { pageConfig } = props;
	let { pagecode } = pageConfig;

	let allRows = props.cardTable.getAllRows(tableId, true);
	//解决增行之后，编辑，然后删行再点击批改报错问题
	if (allRows && allRows.length == 0) {
		return;
	}

	//获取变化的值
	let changeData = props.cardTable.getTableItemData(tableId);

	//卡片和资产变动后编码不支持批改
	if (changeData.batchChangeKey === alterVO.pk_card || changeData.batchChangeKey === alterVO.asset_code_after) {
		return;
	}

	let changeKey = changeData.batchChangeKey;
	let value = changeData.batchChangeValue;
	let index = changeData.batchChangeIndex;
	let display = changeData.batchChangeDisplay;
	//不需要走后台的批改字段
	// let afterArry = [
	// 	'usedept_after',
	// 	'pk_equip_usedept_after',
	// 	'assetsuit_code_after',
	// 	'bar_code_after',
	// 	'card_model_after',
	// 	'contrator_after',
	// 	'leave_date_after',
	// 	'license_after',
	// 	'measureunit_after',
	// 	'paydept_flag_after',
	// 	'pk_addreducestyle_after',
	// 	'pk_assetgroup_after',
	// 	'pk_assetuser_after',
	// 	'pk_equip_usedept_after',
	// 	'pk_jobmngfil_after',
	// 	'pk_usingstatus_after',
	// 	'producer_after',
	// 	'provider_after',
	// 	'soillevel_after',
	// 	'spec_after',
	// 	'workcenter_after',
	// 	'workloanunit_after',
	// 	'pk_category_after',
	// 	'pk_mandept_v_after',
	// 	'pk_costcenter_after',
	// 	'pk_equiporg_v_after',
	// 	'pk_ownerorg_v_after'
	// ];
	//获取当前界面表体展示字段
	let showListKey = [];
	let items = props.meta.getMeta().bodyvos.items;
	for (let i = 0; i < items.length; i++) {
		if (items[i].visible) {
			showListKey.push(items[i].attrcode);
		}
	}
	//指定的一些以及自定义项直接走前台
	//if (afterArry.indexOf(changeKey) != -1 || changeKey.indexOf('def') != -1) {
	let canAlter = true;
	//批改管理部门，需要判断依赖的货主管理组织是否一致
	if (
		changeKey === 'pk_mandept_v_after' ||
		changeKey === 'usedept_after' ||
		changeKey === 'pk_assetuser_after' ||
		changeKey === 'pk_costcenter_after'
	) {
		canAlter = isCanAlter.call(this, props, changeKey, allRows, index);
	}
	if (!canAlter) {
		showMessage(props, { content: getMultiLangByID('201202004A-000002'), color: 'warning' });
		return;
	}
	//批改货主管理组织，清空管理部门
	if (changeKey === 'pk_ownerorg_v_after') {
		referAlterClear(props, formId, tableId, 'pk_ownerorg_v_after', [ 'pk_mandept_v_after' ], index, value, display);
	} else if (changeKey === 'pk_equiporg_v_after') {
		//批改使用权，清空使用部门，使用人，成本中心
		referAlterClear(
			props,
			formId,
			tableId,
			'pk_equiporg_v_after',
			[ 'usedept_after', 'pk_assetuser_after', 'pk_costcenter_after' ],
			index,
			value,
			display
		);
	}

	//编辑的字段是资产类别或者管理部门，如果变动字段不包含卡片编码，则走前台处理
	// if (changeKey === 'pk_category_after' || changeKey === 'pk_mandept_v_after') {
	// 	if (showListKey.indexOf('asset_code_after') == -1) {
	// 		//处理不带v的管理部门
	// 		if (allRows[index] && allRows[index].values && allRows[index].values.pk_mandept_after) {
	// 			let pk_mandept = allRows[index].values.pk_mandept_after;
	// 			props.cardTable.setColValue(tableId, 'pk_mandept_after', pk_mandept);
	// 		}
	// 	} else {
	// 		canAlter = false;
	// 	}
	// }

	// if (canAlter) {
	// 	let returnData = props.cardTable.batchChangeTableData(tableId);
	// 	// return;
	// }
	//	}

	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let userjson = JSON.stringify({
		showListKey: showListKey
	});
	ajax({
		url: url.batchAlterURL,
		data: {
			card: cardData,
			batchChangeIndex: index, // 原始数据行号
			batchChangeKey: changeKey, // 原始数据key
			batchChangeValue: value, // 原始数据value
			userJson: userjson
		},
		success: (res) => {
			//净残值提示
			if (res.data && res.data.userjson) {
				let userObj = JSON.parse(res.data.userjson);
				if (userObj && userObj['salvageError'] != null) {
					showMessage(props, { color: 'warning', content: userObj['salvageError'] });
				}
			}
			if (res.data && res.data.body && res.data.body[tableId] && res.data.body[tableId].rows) {
				let rows = res.data.body[tableId].rows;
				props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
			}
		}
	});
}

/**
 * 批改依赖字段是否相同
 * @param {} props 
 * @param {*} changeKey 
 * @param {*} allRows 
 */
function isCanAlter(props, changeKey, allRows, index) {
	let alterKey = '';
	if (changeKey === 'pk_mandept_v_after') {
		alterKey = 'pk_ownerorg_v_after';
	} else if (changeKey === 'usedept_after') {
		alterKey = 'pk_equiporg_v_after';
	} else if (changeKey === 'pk_assetuser_after') {
		alterKey = 'pk_equiporg_v_after';
	} else if (changeKey === 'pk_costcenter_after') {
		alterKey = 'pk_equiporg_v_after';
	}

	//需要考虑模板没放出来，取值为undefined的情况
	if (allRows[index] && allRows[index].values && allRows[index].values[alterKey] !== undefined) {
		let alterValue = allRows[index].values[alterKey].value;
		for (let i = 0; i < allRows.length; i++) {
			if (i != index) {
				let alterRow = allRows[i];
				if (
					alterRow &&
					alterRow.values &&
					alterRow.values.pk_card &&
					alterRow.values.pk_card.value &&
					alterRow.values[alterKey] !== undefined &&
					alterRow.values[alterKey].value !== undefined
				) {
					if (alterRow.values[alterKey].value != alterValue) {
						return false;
					}
				}
			}
		}
	}

	return true;
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage(props, { content: getMultiLangByID('201202004A-000003'), color: 'warning' });
		return;
	}
	print('pdf', url.printCardURL, printData);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage(props, { content: getMultiLangByID('201202004A-000004'), color: 'warning' });
		return;
	}
	output({
		url: url.printCardURL,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 */
export function getPrintData(props, outputType = 'print') {
	let { pageConfig } = props;
	let { title, printNodekey } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, alterVO.pk_alter);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];

	let printData = {
		filename: title, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType: outputType
	};
	return printData;
}

/**
 * 新增设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	let { pageConfig } = props;
	let { dataSource } = pageConfig;
	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId, dataSource);
	let groupName = getContext(loginContextKeys.groupName, dataSource);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype, dataSource);
	let pk_transitype = getContext(loginContextKeys.pk_transtype, dataSource);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org, dataSource);
	let org_Name = getContext(loginContextKeys.org_Name, dataSource);
	let pk_org_v = getContext(loginContextKeys.pk_org_v, dataSource);
	let org_v_Name = getContext(loginContextKeys.org_v_Name, dataSource);
	let pk_accbook = getContext(loginContextKeys.defaultAccbookPk, dataSource);
	let aaccbookName = getContext(loginContextKeys.defaultAccbookName, dataSource);
	//业务日期
	let usrid = getContext(loginContextKeys.userId, dataSource);
	// 设置当前用户id
	this.setState({
		usrid
	});
	let business_date = getContext(loginContextKeys.businessDate, dataSource);
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype },
		business_date: { value: business_date },
		//自由态
		bill_status: { value: '0', display: getMultiLangByID('statusUtils-000000') },
		pk_accbook: { value: pk_accbook, display: aaccbookName }
	});
}

/**
* 新增
* @param {*} props 
* @param {*} isSetDefaultValue 是否设置默认值
*/
export function add(props,isSetDefaultValue = true) {
	let { pageConfig } = props;
	let { transType } = pageConfig;
	props.cardTable.closeExpandedRow(tableId);
	setStatus.call(this, props, UISTATE.add);
	// 清空数据
	props.form.EmptyAllFormValue(formId);
	//如果是HG-00其他变动，新增时，隐藏所有变动前后字段
	if (transType === 'HG-00') {
		let hideItem = [];
		let items = props.meta.getMeta().bodyvos.items;
		for (let i = 0; i < items.length; i++) {
			if (
				items[i].attrcode.indexOf('_before') != -1 ||
				items[i].attrcode.indexOf('_after') != -1 ||
				(items[i].attrcode.indexOf('_alter') != -1 && items[i].visible == true)
			) {
				hideItem.push(items[i].attrcode);
			}
		}
		props.cardTable.hideColByKey(tableId, hideItem);
		//新增时隐藏账簿
		props.form.setFormItemsVisible(formId, { pk_accbook: false });
	}

	setValue.call(this, props, undefined);
	if(isSetDefaultValue){
		setDefaultValue.call(this, props);
	}
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (pk_org_v && pk_org_v.value) {
		headAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
	}
}
/**
 * 提交
 * @param {*} props 
 */
export function commit(props, content) {
	let { pageConfig } = props;
	let { pagecode, dataSource } = pageConfig;
	let pk = props.getUrlParam('id');
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);

	let paramInfoMap = {};
	paramInfoMap[pk] = ts;

	//获取当前界面表体展示字段
	let showListKey = [];
	let items = props.meta.getMeta().bodyvos.items;
	for (let i = 0; i < items.length; i++) {
		if (items[i].visible) {
			showListKey.push(items[i].attrcode);
		}
	}

	let obj = {
		dataType: 'cardData',
		OperatorType: 'SAVE',
		commitType: 'commit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content,
		obj: showListKey
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitURL,
		data: CardData,
		success: (res) => {
			this.setState({
				compositedisplay: false
			});
			let { success, data } = res;
			if (success) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let setCommonValue = (data) => {
					setValue.call(this, props, data);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					alterVO.pk_alter,
					dataSource,
					setCommonValue,
					false,
					null,
					pagecode
				);
			}
		}
	});
}

/**
 * 收回
 * @param {*} props 
 */
export function uncommit(props, content) {
	let { pageConfig } = props;
	let { pagecode, dataSource } = pageConfig;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let pk = props.getUrlParam('id');
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);

	let paramInfoMap = {};
	paramInfoMap[pk] = ts;

	let showListKey = [];
	let items = props.meta.getMeta().bodyvos.items;
	for (let i = 0; i < items.length; i++) {
		if (items[i].visible) {
			showListKey.push(items[i].attrcode);
		}
	}

	let obj = {
		dataType: 'cardData',
		OperatorType: 'UNSAVE',
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		obj: showListKey,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitURL,
		data: CardData,
		success: (res) => {
			this.setState({
				compositedisplay: false
			});
			let { success, data } = res;
			if (success) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let setCommonValue = (data) => {
					setValue.call(this, props, data);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					alterVO.pk_alter,
					dataSource,
					setCommonValue,
					false,
					undefined,
					pagecode
				);
			}
		}
	});
}

/**
 * 修改
 * @param {*} props 
 */
export function edit(props) {
	let { pageConfig } = props;
	let { dataSource } = pageConfig;
	let transType = getContext(loginContextKeys.transtype, dataSource);
	let pk = props.form.getFormItemsValue(formId, alterVO.pk_alter).value;

	let srcPk = null;
	if (
		props.form.getFormItemsValue(formId, alterVO.pk_bill_src) &&
		props.form.getFormItemsValue(formId, alterVO.pk_bill_src).value
	) {
		props.form.getFormItemsValue(formId, alterVO.pk_bill_src).value;
	}
	if (!pk) {
		pk = props.getUrlParam('id');
	}
	if (!pk) {
		return;
	}
	//数据权限，如果匹配不到，默认走其他变动（暂时为项目档案调整）
	let node_code = '2012028120';
	if (transType === 'HG-01') {
		node_code = '2012028015';
	} else if (transType === 'HG-02') {
		node_code = '2012028020';
	} else if (transType === 'HG-04') {
		node_code = '2012028030';
	} else if (transType === 'HG-06') {
		node_code = '2012028040';
	} else if (transType === 'HG-13') {
		node_code = '2012028075';
	} else if (transType === 'HG-14') {
		node_code = '2012028080';
	} else if (transType === 'HG-16') {
		node_code = '2012028090';
	} else if (transType === 'HG-05') {
		node_code = '2012028035';
	}
	let transi_type = getContext(loginContextKeys.transtype, dataSource);
	if (transi_type === 'HG-05' || (transi_type === 'HG-00' && srcPk != null && srcPk != '')) {
		props.initMetaByPkorg();
		props.form.setFormItemsDisabled(formId, { business_date: false });
		props.cardTable.setColEditableByKey(tableId, 'pk_reason', false);
		//禁用增行删行以及批量公式按钮
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchFormulaAction' ], true);
	}

	ajax({
		url: url.editURL,
		data: {
			pk: pk,
			resourceCode: node_code
		},
		success: (res) => {
			if (res.data) {
				showMessage(props, { color: 'warning', content: res.data });
			} else {
				props.cardTable.closeExpandedRow(tableId);
				setStatus.call(this, props, UISTATE.edit);
				//编辑态不需要加锁
				//lockcards.call(this, props, 'lock');
			}
		},
		error: (res) => {
			if (res && res.message) {
				showMessage(props, { content: res.message, color: 'warning' });
			}
		}
	});
}

/**
 * 删除
 * @param {*} props 
 */
export function del(props) {
	showConfirm(props, { beSureBtnClick: beSureBtnClick, type: MsgConst.Type.Delete });
}

function beSureBtnClick(props) {
	let { pageConfig } = props;
	let { dataSource, pagecode } = pageConfig;
	let id = props.form.getFormItemsValue(formId, alterVO.pk_alter).value;

	let ts = props.form.getFormItemsValue(formId, alterVO.ts).value;
	//拼接删除参数
	var paramInfoMap = {};
	paramInfoMap[id] = ts;

	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);

	let obj = {
		dataType: 'cardData',
		OperatorType: 'DELETE',
		pageid: pagecode,
		paramInfoMap: paramInfoMap
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.deleteURL,
		data: CardData,
		success: (res) => {
			let { success } = res;
			if (success) {				
				let callback = (newpk) => {
					// 更新参数
					props.setUrlParam({ id: newpk });
					// 加载下一条数据
					loadDataByPk.call(this, props, newpk);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					alterVO.pk_alter,
					dataSource,
					undefined,
					true,
					callback
				);
			}
		}
	});
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk, callback) {
	if (!pk || pk == 'null') {
		//this.state.bill_code = '';
		props.form.EmptyAllFormValue(formId);
		setValue.call(this, props, undefined);
		typeof callback == 'function' && callback();
	} else {
		let { pageConfig } = props;
		let { dataSource } = pageConfig;
		let cachData = cardCache.getCacheById(pk, dataSource);
		if (cachData) {
			setValue.call(this, props, cachData);
			setBrowseBtnsVisible.call(this, props);
			typeof callback == 'function' && callback(cachData);
		} else {
			getDataByPk.call(this, props, pk, callback);
		}
	}
}

/**
* 保存
* @param {*} props 
*/
export function save(props) {
	let { pageConfig } = props;
	let { pagecode, dataSource } = pageConfig;

	props.cardTable.closeModel(tableId);

	// 过滤空行
	props.cardTable.filterEmptyRows(tableId, [ 'pk_card' ], 'include');

	// 表头必输校验
	let flag = props.form.isCheckNow(formId);
	if (!flag) {
		return;
	}

	// 表体必输校验
	// let allRows = props.cardTable.getAllRows(tableId, true);
	let allNum = props.cardTable.getNumberOfRows(tableId);
	if (allNum == 0) {
		showMessage(props, { content: getMultiLangByID('201202004A-000015'), color: 'warning' });
		return;
	}
	let check = props.cardTable.checkTableRequired(tableId);
	if (!check) {
		return;
	}

	let accbookFlag = props.form.getFormItemsVisible(formId, 'pk_accbook');
	if (!accbookFlag) {
		props.form.setFormItemsValue(formId, {
			pk_accbook: { value: null, display: null }
		});
	}

	let data = props.createMasterChildDataSimple(pagecode, formId, tableId);

	//获取当前界面表体展示字段
	let showListKey = [];
	let items = props.meta.getMeta().bodyvos.items;
	for (let i = 0; i < items.length; i++) {
		if (items[i].visible) {
			showListKey.push(items[i].attrcode);
		}
	}
	data.userjson = showListKey.join();

	//判断是修改保存还是新增保存
	let ajaxUrl = url.saveURL;
	let pk_alter = props.form.getFormItemsValue(formId, alterVO.pk_alter).value;
	let oldstatus = UISTATE.add;
	if (pk_alter && pk_alter != null) {
		ajaxUrl = url.updateURL;
		oldstatus = UISTATE.edit;
	}

	props.validateToSave(data, () => {
		ajax({
			url: ajaxUrl,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					// 关闭侧拉框
					props.beforeUpdatePage();
					setStatus.call(this, props);
					setValue.call(this, props, data);
					showMessage(props, { type: MsgConst.Type.SaveSuccess });
					
					let pk = props.form.getFormItemsValue(formId, alterVO.pk_alter).value;
					let cacheData = props.createMasterChildData(pagecode, formId, tableId);
					// 保存成功后处理缓存
					if (oldstatus == UISTATE.add) {
						cardCache.addCache(pk, cacheData, formId, dataSource);
					} else {
						cardCache.updateCache(alterVO.pk_alter, pk, cacheData, formId, dataSource);
					}
					// 设置按钮显隐性
					setBrowseBtnsVisible.call(this, props);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	});
}

/**
 * 保存提交
 * @param {*} props 
 */
export function saveCommit(props,content) {
	let { pageConfig } = props;
	let { pagecode, dataSource } = pageConfig;
	let pk = props.getUrlParam('id');

	// 过滤空行
	props.cardTable.filterEmptyRows(tableId, [ 'pk_card' ], 'include');
	// 关闭侧拉
	props.cardTable.closeModel(tableId);
	// 表头必输校验
	let flag = props.form.isCheckNow(formId);
	if (!flag) {
		return;
	}
	// 表体必输校验
	let allRows = props.cardTable.getAllRows(tableId, true);
	if (!allRows || allRows.length == 0) {
		showMessage(props, { content: getMultiLangByID('201202004A-000017'), color: 'warning' });
		return;
	}
	let check = props.cardTable.checkTableRequired(tableId);
	if (!check) {
		return;
	}

	let accbookFlag = props.form.getFormItemsVisible(formId, 'pk_accbook');
	if (!accbookFlag) {
		props.form.setFormItemsValue(formId, {
			pk_accbook: { value: null, display: null }
		});
	}

	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);

	let paramInfoMap = {};
	paramInfoMap[pk] = '';

	//获取当前界面表体展示字段
	let showListKey = [];
	let items = props.meta.getMeta().bodyvos.items;
	for (let i = 0; i < items.length; i++) {
		if (items[i].visible) {
			showListKey.push(items[i].attrcode);
		}
	}

	let obj = {
		dataType: 'cardData',
		OperatorType: 'SAVE',
		commitType: 'saveCommit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		obj: showListKey,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);

	//判断是修改保存还是新增保存
	let pk_alter = props.form.getFormItemsValue(formId, alterVO.pk_alter).value;
	let oldstatus = UISTATE.add;
	if (pk_alter && pk_alter != null) {
		oldstatus = UISTATE.edit;
	}

	let saveCommitAction = () => {
		ajax({
			url: url.commitURL,
			data: CardData,
			success: (res) => {				
				let { success, data } = res;
				if (success) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}					
					let setCommonValue = (data) => {
						setValue.call(this, props, data);
					};
					let callback = () => {
						setStatus.call(this, props, UISTATE.browse);
						// 设置按钮显隐性
						setBrowseBtnsVisible.call(this, props);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						alterVO.pk_alter,
						dataSource,
						setCommonValue,
						false,
						callback,
						pagecode
					);
				}				
			}
		});
	}
	if (content) {
		saveCommitAction();
	} else {
		// 保存提交前执行验证公式
		props.validateToSave(CardData, saveCommitAction);
	}
}

/**
* 新增行
* @param {*} props 
* @param {*} isFocus 是否聚焦
*/
export function addLine(props, isFocus = false) {
	let { pageConfig } = props;
	let { transType } = pageConfig;
	if (transType === 'HG-00') {
		let pk_fa_alters = props.form.getFormItemsValue(formId, alterVO.pk_fa_alters);
		if (pk_fa_alters.value == null) {
			showMessage(props, { content: getMultiLangByID('201202004A-000018'), color: 'warning' });
			return;
		}
	}

	props.cardTable.addRow(tableId,undefined, undefined,isFocus);
	setBodyBtnsEnable.call(this, props, tableId);
}

/**
 * 删行
 * @param {*} props 
 */
export function delLine(props) {
	let index = props.cardTable.getCheckedRows(tableId);
	if (index.length == 0) {
		showMessage(props, { content: getMultiLangByID('201202004A-000018'), color: 'warning' });
		return;
	}
	let delIndex = [];
	index.map((item, value) => {
		delIndex.push(item.index);
	});
	let allpks = [];
	index.map((item) => {
		// 若删除行为新增数据，则记录为需解锁卡片
		if (
			(item.data.status === '2' || item.data.status === '1') &&
			item.data.values.pk_card &&
			item.data.values.pk_card.value
		) {
			allpks.push(item.data.values.pk_card.value);
		}
	});
	let { pageConfig } = props;
	let { dataSource } = pageConfig;
	let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
	let usrid = getContext('userId', dataSource);
	// 若待解锁卡片数量大于0，则解锁
	if (allpks.length > 0) {
		let param = {
			allpks,
			msgMap: {
				usrid,
				lockflag: 'unlock',
				dSource
			}
		};
		lockrequest.call(this, param);
	}

	props.cardTable.delRowsByIndex(tableId, delIndex);
	setBodyBtnsEnable.call(this, props, tableId);
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	switch (status) {
		case UISTATE.add:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false,
				pk_accbook: false,
				pk_fa_alters:false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			break;
		case UISTATE.edit:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true,
				pk_accbook: true,
				pk_fa_alters: true
			});

			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 解决浏览器刷新后界面编辑按钮显示问题
			props.button.setButtonVisible(editBtns, false);
			setBrowseBtnsVisible.call(this, props);
			break;
		default:
			break;
	}
	setHeadAreaData.call(this, props, { status });
}

/**
* 取消
* @param {*} props 
*/
function cancelConfirm(props) {
	showConfirm(props, { beSureBtnClick: cancel, type: MsgConst.Type.Cancel });
}
/**
 * 取消按钮
 * @param {*} props 
 */
export function cancel(props) {
	let { pageConfig = {} } = props;
	let { dataSource } = pageConfig;
	// 解锁卡片
	lockcards.call(this, props, 'unlock');
	props.form.cancel(formId);
	props.cardTable.resetTableData(tableId);
	setStatus.call(this, props, UISTATE.browse);
	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, alterVO.pk_org_v);
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}

	let pk = props.form.getFormItemsValue(formId, alterVO.pk_alter).value;
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	loadDataByPk.call(this, props, pk);
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	setCardValue.call(this, props, data);

	if (data) {
		//处理其他变动变动项目字段
		if (
			data &&
			data.head &&
			data.head[formId] &&
			data.head[formId].rows &&
			data.head[formId].rows[0] &&
			data.head[formId].rows[0].values &&
			data.head[formId].rows[0].values.pk_fa_alters &&
			data.head[formId].rows[0].values.pk_fa_alters.value
		) {
			let hideItem = [];
			let olditems = props.meta.getMeta().bodyvos.items;
			for (let i = 0; i < olditems.length; i++) {
				if (
					olditems[i].attrcode.indexOf('_before') != -1 ||
					olditems[i].attrcode.indexOf('_after') != -1 ||
					(olditems[i].attrcode.indexOf('_alter') != -1 && olditems[i].visible == true)
				) {
					hideItem.push(olditems[i].attrcode);
				}
			}
			//处理隐藏项
			props.cardTable.hideColByKey(tableId, hideItem);

			let itemCodes = data.head[formId].rows[0].values.pk_fa_alters.value;
			let keys = itemCodes.split(',');
			let items = [];
			keys.map((key) => {
				items.push(`${key}_before`);
				items.push(`${key}_alter`);
				items.push(`${key}_after`);
			});
			props.cardTable.showColByKey(tableId, items);
			//处理账簿信息字段
			let account_field = baseConfig.account_field;
			let flag = false;
			for (let i = 0; i < keys.length; i++) {
				for (let j = 0; j < account_field.length; j++) {
					if (keys[i] === account_field[j]) {
						flag = true;
						break;
					}
				}
				if (flag) {
					break;
				}
			}
			if (flag) {
				props.form.setFormItemsVisible(formId, { pk_accbook: true });
			} else {
				props.form.setFormItemsVisible(formId, { pk_accbook: false });
			}
		}
	}

	setBrowseBtnsVisible.call(this, props);
}

/**
 * 浏览态设置按钮显示隐藏
 * @param {*} props 
 */
function setBrowseBtnsVisible(props) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	let pkVal = props.form.getFormItemsValue(formId, alterVO.pk_alter);
	let btnObj = {};
	if (status != UISTATE.browse) {
		browseBtns.map((item) => {
			btnObj[item] = false;
		});
		editBtns.map((item) => {
			btnObj[item] = true;
		});
	} else {
		editBtns.map((item) => {
			btnObj[item] = false;
		});
		if (!pkVal || !pkVal.value) {
			browseBtns.map((item) => {
				btnObj[item] = false;
			});
			btnObj['Add'] = true;
		} else {
			browseBtns.map((item) => {
				btnObj[item] = true;
			});
		}
	}
	props.button.setButtonVisible(btnObj);
	setBillFlowActionVisible.call(this, props);
}

/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter', 'BatchFormulaAction' ], true);
		return;
	}
	let pk_bill_src = props.form.getFormItemsValue(formId, 'pk_bill_src');

	if (pk_bill_src != null && pk_bill_src != '' && pk_bill_src.value != null && pk_bill_src.value != '') {
		//禁用增行删行以及批量公式按钮
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchFormulaAction' ], true);
	} else {
		props.button.setButtonDisabled([ 'AddLine', 'BatchAlter', 'BatchFormulaAction' ], false);
		let checkedRows = [];
		let num = props.cardTable.getNumberOfRows(moduleId, false);
		if (num > 0) {
			checkedRows = props.cardTable.getCheckedRows(moduleId);
		}
		props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
	}
}

/**
 * 浏览态设置审批流按钮显示隐藏
 * @param {*} props 
 */
export function setBillFlowActionVisible(props) {
	// 根据单据状态隐藏按钮
	let status = props.form.getFormStatus(formId);
	let pkVal = props.form.getFormItemsValue(formId, alterVO.pk_alter);
	if (status && status === UISTATE.browse && pkVal && pkVal.value) {
		let newHiddenBtns = [ 'Edit', 'Delete', 'Commit', 'UnCommit' ];
		props.button.setButtonVisible(newHiddenBtns, true);
		let bill_status = props.form.getFormItemsValue(formId, alterVO.bill_status);
		if (bill_status) {
			let bill_status_value = bill_status.value;
			if (bill_status_value === BILLSTATUS.free_check) {
				// 自由
				newHiddenBtns.splice(newHiddenBtns.indexOf('Edit'), 1);
				newHiddenBtns.splice(newHiddenBtns.indexOf('Delete'), 1);
				newHiddenBtns.splice(newHiddenBtns.indexOf('Commit'), 1);
			} else if (bill_status_value === BILLSTATUS.un_check) {
				// 已提交
				newHiddenBtns.splice(newHiddenBtns.indexOf('UnCommit'), 1);
			} else if (bill_status_value === BILLSTATUS.check_going) {
				// 审批中
			} else if (bill_status_value === BILLSTATUS.check_pass) {
				// 审批通过
				newHiddenBtns.splice(newHiddenBtns.indexOf('UnCommit'), 1);
			} else if (bill_status_value === BILLSTATUS.check_nopass) {
			}
			props.button.setButtonVisible(newHiddenBtns, false);
		}
	}
}

/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk, callback, isRefresh = false) {
	let { pageConfig } = props;
	let { dataSource, pagecode } = pageConfig;
	ajax({
		url: url.queryCardByPkURL,
		data: { pk, pagecode },
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					if (isRefresh) {
						showMessage(props, { type: MsgConst.Type.RefreshSuccess });
					}
					let transi_type = getContext(loginContextKeys.transtype, dataSource);
					if (transi_type === 'HG-05') {
						props.initMetaByPkorg();
						props.form.setFormItemsDisabled(formId, { business_date: false, pk_org: false });
						props.cardTable.setColEditableByKey(tableId, 'pk_reason', false);
						//禁用增行删行以及批量公式按钮
						props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchFormulaAction' ], true);
					}
					cardCache.updateCache(alterVO.pk_alter, pk, data, formId, dataSource);
				} else {
					setValue.call(this, props, undefined);
					cardCache.deleteCacheById(alterVO.pk_alter, pk, dataSource);
					showMessage(props, { content: getMultiLangByID('201202004A-000011'), color: 'warning' });
				}
				typeof callback == 'function' && callback(data);
			}
		}
	});
}

/**
 * 通过消息中心打开变动单
 * @param {*} props 
 * @param {*} pkMsg 
 */
export function getAlterMsg(props, pkMsg) {
	let { pageConfig } = props;
	let { pagecode } = pageConfig;
	ajax({
		url: url.openAlterMsgURL,
		data: {
			pkMsg: pkMsg,
			pagecode: pagecode
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if(data){
					setValue.call(this, props, data);
					//通过消息打开后，将除了业务日期和变动原因的字段全部禁用
					props.initMetaByPkorg();
					props.form.setFormItemsDisabled(formId, { business_date: false, pk_org: false, remark: false });
					props.cardTable.setColEditableByKey(tableId, 'pk_reason', false);
					//禁用增行删行以及批量公式按钮
					props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchFormulaAction' ], true);
				} else {
					showMessage(props, { content: getMultiLangByID('facommon-000018') /*国际化处理：'单据已经弃审!'*/, color: 'warning' });
					cancel.call(this,props);//提示后跳转到浏览态
				}				
			}
		},
		//异常手动抛出，同时切换到浏览态
		error: (res) => {
			let { message } = res;
			props.form.EmptyAllFormValue(formId);
			setStatus.call(this, props, UISTATE.browse);
			setValue.call(this, props, undefined);
			showMessage(props, { content: message, color: 'warning' });
		}
	});
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	let pk = props.form.getFormItemsValue(formId, alterVO.pk_alter).value;
	if (!pk) {
		return;
	}
	// 查询到最新单据数据
	getDataByPk.call(this, props, pk, undefined, true);
}

/**
 * 设置合计信息
 * @param {*} props 
 * @param {*} data 
 */
export function setTotalInfo(props, data) {
	let length = data.bodyvos.rows.length;
	this.setState({
		totalnum: length
	});
}

function attachment(props) {
	let billId = props.form.getFormItemsValue(formId, alterVO.pk_alter).value;
	//	let billNo = props.form.getFormItemsValue(formId, alterVO.bill_code).value;
	props.ncUploader.show('alter-uploader', {
		billId
	});
}

function batchFormula(props) {
	let editingField = this.state.editingField;

	if (editingField === '') {
		showMessage(props, { content: getMultiLangByID('201202004A-000012'), color: 'warning' });
		return;
	}
	if (canEditFields.indexOf(editingField) == -1) {
		showMessage(props, { content: getMultiLangByID('201202004A-000013'), color: 'warning' });
		return;
	}

	// 过滤空行
	props.cardTable.filterEmptyRows(tableId, [ 'pk_card' ], 'include');

	//判断当前显示的字段有哪些
	let items = props.meta.getMeta().bodyvos.items;
	let tabBuild = [];
	let allItemCodes = [];
	for (let i = 0; i < items.length; i++) {
		let itemCode = items[i].attrcode;
		if (items[i].visible) {
			allItemCodes.push(itemCode);
		}
	}

	if (
		//本币原值
		allItemCodes.indexOf('localoriginvalue_before') > -1 &&
		allItemCodes.indexOf('localoriginvalue_alter') > -1 &&
		allItemCodes.indexOf('localoriginvalue_after') > -1
	) {
		tabBuild.push('localoriginvalue_before');
	}
	if (
		//累计折旧变动
		allItemCodes.indexOf('accudep_before') > -1 &&
		allItemCodes.indexOf('accudep_alter') > -1 &&
		allItemCodes.indexOf('accudep_after') > -1
	) {
		tabBuild.push('accudep_before');
	}
	if (
		//净残值
		allItemCodes.indexOf('salvage_before') > -1 &&
		allItemCodes.indexOf('salvage_after') > -1
	) {
		tabBuild.push('salvage_before');
	}
	if (
		//原币原值
		allItemCodes.indexOf('originvalue_before') > -1 &&
		allItemCodes.indexOf('originvalue_alter') > -1 &&
		allItemCodes.indexOf('originvalue_after') > -1
	) {
		tabBuild.push('originvalue_before');
	}
	if (
		//购买价款
		allItemCodes.indexOf('currmoney_before') > -1 &&
		allItemCodes.indexOf('currmoney_alter') > -1 &&
		allItemCodes.indexOf('currmoney_after') > -1
	) {
		tabBuild.push('currmoney_before');
	}
	this.setState({
		show: true,
		tabBuild: tabBuild
	});
}

/**
* 返回
* @param {*} props 
*/
export function backToList(props) {
	let { pageConfig } = props;
	let { pagecode } = pageConfig;
	props.pushTo(listRouter, {pagecode:pagecode});
}

/**
 * 审批详情
 * @param {*} props 
 */
function queryAboutBillFlow(props) {
	let pkAlter = props.form.getFormItemsValue(formId, alterVO.pk_alter).value;
	this.setState({
		showApprove: true,
		pkAlter: pkAlter
	});
}

/**
 * 加锁，解锁卡片
 * @param {*} props 
 */
export function lockcards(props, lockflag, async = true) {
	let data = props.cardTable.getAllRows(tableId);
	// status : 0不变，1修改，2新增，3删除
	let allpks = [];
	for (let val of data) {
		// 判断如果是新增删除，在删除时已解锁，这里不解锁，若是修改删除，在这里需要解锁
		if (!val.values.pk_alter_b.value && val.status === '3') {
		} else {
			if (val.values.pk_card.value) {
				allpks.push(val.values.pk_card.value);
			}
		}
	}

	let { pageConfig } = props;
	let { dataSource } = pageConfig;
	let dSource = getContext(baseConfig.ds, dataSource);
	let usrid = getContext('userId', dataSource);
	let param = {
		allpks,
		msgMap: {
			usrid,
			lockflag,
			dSource
		}
	};
	if (allpks && allpks.length > 0 && lockflag && usrid && dSource) {
		lockrequest.call(this, param, async);
	}
}
/**
 * 加解锁请求
 * @param {*} param 
 */
export function lockrequest(param, async = true) {
	ajax({
		url: url.lockcard,
		data: param,
		async,
		success: (res) => {},
		error: (res) => {}
	});
}

//影像扫描
function faReceiptScan(props) {
	let { pageConfig } = props;
	let { pagecode } = pageConfig;
	let pkField = alterVO.pk_alter;
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageScan(props, imageData);
}

//影像查看
function faReceiptShow(props) {
	let { pageConfig } = props;
	let { pagecode } = pageConfig;
	let pkField = alterVO.pk_alter;
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageView(props, imageData);
}

function queryAboutBillSrc(props) {
	let pkBillSrc = props.form.getFormItemsValue(formId, alterVO.pk_bill_src).value;
	if (pkBillSrc == null) {
		showMessage(props, { content: getMultiLangByID('201202004A-000014'), color: 'warning' });
		return;
	}

	let transiTypeSrc = props.form.getFormItemsValue(formId, alterVO.transi_type_src).value;
	let param = {};
	if (transiTypeSrc) {
		param = {
			transtype: transiTypeSrc,
			id: pkBillSrc
		};
	} else {
		let billTypeSrc = props.form.getFormItemsValue(formId, alterVO.bill_type_src);
		param = {
			transtype: billTypeSrc,
			id: pkBillSrc
		};
	}

	ajax({
		url: url.querySrcUrl,
		data: param,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					data['status'] = 'browse';
					props.openTo(data['url'], data);
				}
			}
		},
		error: (res) => {}
	});
}

//资产组 字段设置
export function updateAssetGroupItem(props, isHeadAssetFinanceorg) {
    //参照过滤
    let meta = props.meta.getMeta();
    meta[tableId].items.map((item) => {
		// 资产组
		if (item.attrcode == 'pk_assetgroup_after') {
            item.isCacheable = false;
            item.queryCondition = () => {
				return {
                	notShowGroupFlag : isHeadAssetFinanceorg
                }
            };
        }
	});
	meta[tableEditId].items.map((item) => {
		// 资产组
        if (item.attrcode == 'pk_assetgroup_after') {
            item.isCacheable = false;
            item.queryCondition = () => {
                return {
                    notShowGroupFlag : isHeadAssetFinanceorg
                }
            };
        }
    });
    props.meta.setMeta(meta);
}
