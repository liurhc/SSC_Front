import initTemplate from './initTemplate';
import buttonClick, {
	getDataByPk,
	getAlterMsg,
	setValue,
	backToList,
	save,
	lockcards,
	commit,
	lockrequest,
	saveCommit
} from './buttonClick';
import formAfterEvent from './formAfterEvent';
import bodyAfterEvent from './tableAfterEvent';
import pageInfoClick from './pageInfoClick';
import tableButtonClick from './tableButtonClick';
import bodyBeforeEvent from './tableBeforeEvent';
import rowSelected from './rowSelected';

export {
	initTemplate,
	buttonClick,
	formAfterEvent,
	bodyAfterEvent,
	pageInfoClick,
	getDataByPk,
	getAlterMsg,
	tableButtonClick,
	bodyBeforeEvent,
	setValue,
	backToList,
	rowSelected,
	save,
	commit,
	lockcards,
	lockrequest,
	saveCommit
};
