export const baseConfig = {
	moduleId: '2012', //所属模块编码
	searchId: 'searchArea', //查询区域编码
	tableId: 'list_head', //列表表体编码
	listInner: 'list_inner', //表体行区域
	cardRouter: '/card', //路由跳转卡片
	ds: 'dataSource',
	alterVO: {
		pk_bill_src: 'pk_bill_src',
		transi_type_src: 'transi_type_src',
		bill_type_src: 'bill_type_src',
		pk_alter: 'pk_alter', //变动单主键
		pk_org: 'pk_org',
		bill_code: 'bill_code',
		ts: 'ts'
	},
	url: {
		printCardURL: '/nccloud/fa/alter/printCard.do',
		deleteURL: '/nccloud/fa/alter/delete.do',
		commitURL: '/nccloud/fa/alter/commit.do',
		queryPageURL: '/nccloud/fa/alter/queryPage.do',
		querySearchAreaURL: '/nccloud/fa/alter/querySearchArea.do',
		editURL: '/nccloud/fa/alter/edit.do',
		deleteURL: '/nccloud/fa/alter/delete.do',
		commitURL: '/nccloud/fa/alter/commit.do',
		querySrcUrl: '/nccloud/ampub/common/amLinkQuery.do'
	},
	batchBtns: [
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'FAReceipt',
		'FAReceiptShow',
		'FAReceiptScan',
		'QueryAbout',
		'FABillQueryAboutVoucher',
		'QueryAboutBillSrc',
		'QueryAboutVoucher',
		'QueryAboutBusiness',
		'Print',
		'Preview',
		'Output'
	]
};
