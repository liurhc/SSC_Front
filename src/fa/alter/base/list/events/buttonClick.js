import { ajax, toast, cacheTools, print, output } from 'nc-lightapp-front';
import { searchBtnClick } from '../events';
import { baseConfig } from '../const';
const { formId, tableId, cardRouter, alterVO, url } = baseConfig;
import ampub from 'ampub';

const { components, utils, commonConst } = ampub;
const { StatusUtils} = commonConst;
const { UISTATE } = StatusUtils;
const { ScriptReturnUtils,queryVocherUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { listUtils, msgUtils, multiLangUtils } = utils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;

export default function(props, id) {
	let { pageConfig } = props;
	let { appcode } = pageConfig;

	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props);
			break;
		case 'UnCommit':
			uncommit.call(this, props);
			break;
		case 'FABillQueryAboutVoucher':
			queryAboutVoucher(props, tableId, alterVO.pk_alter, appcode, true);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		case 'QueryAboutBillSrc':
			queryAboutBillSrc.call(this, props);
			break;
		default:
			break;
	}
}

function queryAboutBillSrc(props) {
	let data = props.table.getCheckedRows(tableId);
	if (!data || data.length == 0) {
		showMessage(props, { content: getMultiLangByID('201202004A-000048'), color: 'warning' });
		return;
	}
	let pkBillSrc = data[0].data.values[alterVO.pk_bill_src];
	if (pkBillSrc && pkBillSrc.value == null) {
		showMessage(props, { content: getMultiLangByID('201202004A-000014'), color: 'warning' });
		return;
	}
	let transiTypeSrc = null;
	if (data[0].data.values[alterVO.transi_type_src]) {
		transiTypeSrc = data[0].data.values[alterVO.transi_type_src].value;
	}
	let param = {};
	if (transiTypeSrc) {
		param = {
			transtype: transiTypeSrc,
			id: pkBillSrc.value
		};
	} else {
		let billTypeSrc = data[0].data.values[alterVO.bill_type_src].value;
		param = {
			transtype: billTypeSrc,
			id: pkBillSrc.vlaue
		};
	}

	ajax({
		url: url.querySrcUrl,
		data: param,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					data['status'] = 'browse';
					props.openTo(data['url'], data);
				}
			}
		},
		error: (res) => {}
	});
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage(props, { content: getMultiLangByID('201202004A-000003'), color: 'warning' });
		return;
	}
	print('pdf', url.printCardURL, printData);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage(props, { content: getMultiLangByID('201202004A-000004'), color: 'warning' });
		return;
	}
	output({
		url: url.printCardURL,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 */
export function getPrintData(props, outputType = 'print') {
	let { pageConfig } = props;
	let { title, printNodekey } = pageConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[alterVO.pk_alter].value);
	});

	let printData = {
		filename: title, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType: outputType
	};
	return printData;
}

/**
 * 新增
 * @param {*} props 
 */
export function add(props) {
	linkToCard.call(this, props, undefined, UISTATE.add);
}

/**
 * 删除
 * @param {*} props 
 * @param {*} status 
 */
export function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage(props, { content: getMultiLangByID('201202004A-000019'), color: 'warning' });
		return;
	}
	showConfirm(props, { beSureBtnClick: batchDel, type: MsgConst.Type.DelSelect });
}

/**
 * 后台删除
 * @param {*} props 
 */
export function batchDel(props) {
	let { pageConfig } = props;
	let { appcode } = pageConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return;
	}

	let paramInfoMap = {};
	let params = checkedRows.map((v) => {
		let id = v.data.values[alterVO.pk_alter].value;
		let ts = v.data.values[alterVO.ts].value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let code = appcode + '_card';
	ajax({
		url: url.deleteURL,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: code
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				alterVO.pk_alter,
				tableId.replace('list', 'card'),
				tableId,
				true
			);
		}
	});
}

/**
 * 提交
 * @param {*} props 
 */
export function commit(props, content) {
	let { pageConfig } = props;
	let { appcode, dataSource } = pageConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage(props, { content: getMultiLangByID('201202004A-000049'), color: 'warning' });
		return;
	}
	let paramInfoMap = {};
	let params = checkedRows.map((v) => {
		let id = v.data.values[alterVO.pk_alter].value;
		let ts = v.data.values['ts'].value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let pagecode = appcode + '_card';
	ajax({
		url: url.commitURL,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'SAVE',
			pageid: pagecode,
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				alterVO.pk_alter,
				tableId.replace('list', 'card'),
				tableId,
				false,
				dataSource
			);
		},
		error: (res) => {
			if (res && res.message) {
				showMessage(props, { content: res.message, color: 'warning' });
			}
		}
	});
}

/**
 * 取消提交
 */
export function uncommit(props) {
	let { pageConfig } = props;
	let { appcode, dataSource } = pageConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage(props, { content: getMultiLangByID('201202004A-000049'), color: 'warning' });
		return;
	}

	let paramInfoMap = {};
	let params = checkedRows.map((v) => {
		let id = v.data.values[alterVO.pk_alter].value;
		let ts = v.data.values['ts'].value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});

	let pagecode = appcode + '_card';
	ajax({
		url: url.commitURL,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'UNSAVE',
			pageid: pagecode
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				alterVO.pk_alter,
				tableId.replace('list', 'card'),
				tableId,
				false,
				dataSource
			);
		},
		error: (res) => {
			if (res && res.message) {
				showMessage(props, { content: res.message, color: 'warning' });
			}
		}
	});
}

/**
 * 设置按钮状态
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
	let { pageConfig } = props;
	let { transType } = pageConfig;
	if (transType != 'HG-00' && transType != 'HG-05') {
		props.button.setButtonsVisible({ QueryAboutBillSrc: false });
	}
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 附件
 * @param {*} props 
 */
function attachment(props) {
	let checkedrows = props.table.getCheckedRows(tableId);

	if (checkedrows.length == 0) {
		showMessage(props, { content: getMultiLangByID('201202004A-000049'), color: 'warning' });
		return;
	}
	// billNo 是单据主键
	let billId = checkedrows[0].data.values[alterVO.pk_alter].value;
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values[alterVO.bill_code].value;
	props.ncUploader.show('alter-uploader', {
		billId,
		billNo
	});
}

/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = UISTATE.browse) {
	let { pageConfig } = props;
	let { pagecode } = pageConfig;
	props.pushTo(cardRouter, {
		status,
		id: record[alterVO.pk_alter] ? record[alterVO.pk_alter].value : '',
		pagecode: pagecode
	});
}
