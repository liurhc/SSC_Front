import { ajax } from 'nc-lightapp-front';
import { baseConfig } from '../const';
const { tableId, url } = baseConfig;
import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';

const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils

export default function(props, config, pks) {
	let { pageConfig } = props;
	let { pagecode } = pageConfig;
	let data = {
		allpks: pks,
		pagecode: pagecode
	};
	ajax({
		url: url.queryPageURL,
		data: data,
		success: function(res) {
			setListValue.call(this, props, res);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
