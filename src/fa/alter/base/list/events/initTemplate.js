import { cacheTools } from 'nc-lightapp-front';
import { tableButtonClick, searchBtnClick } from '../events';
import { setBatchBtnsEnable, linkToCard } from './buttonClick';
import { baseConfig } from '../const';
import ampub from 'ampub';
import fa from 'fa';

const { components, utils,  commonConst} = ampub;
const { LoginContext } = components;
const { StatusUtils} = commonConst;
const { UISTATE, BILLSTATUS } = StatusUtils;
const { loginContext,getContext } = LoginContext;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;
const { searchId, tableId, listInner, alterVO } = baseConfig;

export default function(props) {
	let { pageConfig } = props;
	let { pagecode, dataSource } = pageConfig;
	props.createUIDom(
		{
			pagecode: pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, dataSource);
					// 获取数据源
					//getContext(baseConfig.ds, dataSource);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let { pageConfig } = props;
	let { appcode } = pageConfig;
	let searchVal = cacheTools.get(appcode + '_searchParams');

	if (searchVal) {
		props.search.setSearchValue(searchId, searchVal.conditions);
		searchBtnClick.call(this, props, searchVal);
	}
}

function modifierMeta(props, meta) {
	let defaultConfig = {
		searchId: searchId,
		bodyIds: [ tableId ],
		//特殊过滤
		specialFields: {
			//成本中心
			pk_costcenter_after: {
				RefActionExtType: 'TreeRefActionExt',
				class: 'nccloud.web.fa.common.refCondition.CostCenterRefFilter',
				data: [
					{
						fields: [ 'pk_equiporg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			},
			//货主管理组织
			pk_ownerorg_v_after: {
				addOrgRelation: 'pk_orgs',
				data: [
					{
						fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_orgs' //被过滤字段名后台需要参数
					}
				]
			},
			//使用部门
			usedept_after: {
				data: [
					{
						fields: [ 'pk_equiporg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			},
			//管理部门
			pk_mandept_v_after: {
				//被过滤字段名
				orgMulti: 'pk_org', //添加左上角主组织参照
				data: [
					{
						fields: [ 'pk_ownerorg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			},
			//使用人
			pk_assetuser_after: {
				//被过滤字段名
				orgMulti: 'pk_org', //添加左上角主组织参照
				data: [
					{
						fields: [ 'pk_equiporg_after', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			}
			//资产组
			// pk_assetgroup_after: {
			// 	//被过滤字段名
			// 	orgMulti: 'pk_org', //添加左上角主组织参照
			// 	data: [
			// 		{
			// 			fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
			// 			returnName: 'pk_org' //被过滤字段名后台需要参数
			// 		}
			// 	]
			// }
		},
		defPrefix: {
			search: [ 'bodyvos.def', 'bodyvos.pk_card.def', 'def' ],
			body: [ 'def', 'pk_card.def' ]
		}
	};
	addSearchAreaReferFilter.call(this, props, meta, defaultConfig);

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == alterVO.bill_code) {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('201202004A-000051')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, UISTATE.browse);
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}

		return item;
	});

	//添加操作列
	meta[tableId].items.push({
		label: getMultiLangByID('amcommon-000000'),
		itemtype: 'customer',
		attrcode: 'opr',
		width: '150px',
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			//表体行按钮全集
			let buttonAry = [];
			//自由态
			if (record.bill_status.value == BILLSTATUS.free_check) {
				buttonAry = [ 'Commit', 'Edit', 'Delete' ];
			} else if (record.bill_status.value == BILLSTATUS.un_check) {
				//已提交
				buttonAry = [ 'UnCommit' ];
			} else if (record.bill_status.value == BILLSTATUS.check_going) {
				//审批中
				buttonAry = [ 'QueryAboutBillFlow' ];
			} else if (record.bill_status.value == BILLSTATUS.check_pass) {
				//已审批
				buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
			} else if (bill_status_value === BILLSTATUS.check_nopass) {
				// 审批不通过
				buttonAry = [ 'Edit', 'Delete', 'QueryAboutBillFlow' ];
			}
			return props.button.createOprationButton(buttonAry, {
				area: listInner,
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(this, props, key, text, record, index)
			});
		}
	});
	return meta;
}
