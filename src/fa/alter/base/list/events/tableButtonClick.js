import { ajax, toast, cacheTools } from 'nc-lightapp-front';
import { linkToCard } from './buttonClick';
import { baseConfig } from '../const';
const { url, tableId, alterVO } = baseConfig;
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { ScriptReturnUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { msgUtils } = utils;
const { showMessage} = msgUtils;

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Commit':
			commit.call(this, props, record, index);
			break;
		case 'UnCommit':
			unCommit.call(this, props, record, index);
			break;
		case 'Edit':
			edit.call(this, props, record);
			break;
		case 'Delete':
			del.call(this, props, record, index);
			break;
		case 'QueryAboutBillFlow':
			this.setState({
				showApprove: true,
				pkAlter: record[alterVO.pk_alter].value
			});
			break;
		default:
			break;
	}
}

/**
 * 删除
 * @param {*} props 
 * @param {*} record 
 */
function del(props, record, index) {
	let { pageConfig } = props;
	let { pagecode } = pageConfig;
	let id = record.pk_alter.value;
	let ts = record.ts.value;
	var param = {};
	param[id] = ts;

	let params = [
		{
			id,
			index
		}
	];
	ajax({
		url: url.deleteURL,
		data: {
			paramInfoMap: param,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card')
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				alterVO.pk_alter,
				tableId.replace('list', 'card'),
				tableId,
				true
			);
			//清除缓存
			props.table.deleteCacheId(tableId, id);
		}
	});
}

/**
 * 修改
 * @param {*} props 
 * @param {*} record 
 */
function edit(props, record) {
	let { pageConfig } = props;
	let { transType } = pageConfig;
	//如果匹配不到，默认走其他变动（暂时为项目档案调整）
	let node_code = '2012028120';
	if (transType === 'HG-01') {
		node_code = '2012028015';
	} else if (transType === 'HG-02') {
		node_code = '2012028020';
	} else if (transType === 'HG-04') {
		node_code = '2012028030';
	} else if (transType === 'HG-06') {
		node_code = '2012028040';
	} else if (transType === 'HG-13') {
		node_code = '2012028075';
	} else if (transType === 'HG-14') {
		node_code = '2012028080';
	} else if (transType === 'HG-16') {
		node_code = '2012028090';
	} else if (transType === 'HG-05') {
		node_code = '2012028035';
	}

	ajax({
		url: url.editURL,
		data: {
			pk: record.pk_alter.value,
			resourceCode: node_code
		},
		success: (res) => {
			if (res.data) {
				showMessage(props, { content: res.data, color: 'warning' });
			} else {
				linkToCard.call(this, props, record, UISTATE.edit);
			}
		},
		error: (res) => {
			if (res && res.message) {
				showMessage(props, { content: res.message, color: 'warning' });
			}
		}
	});
}

/**
 * 提交
 * @param {*} props 
 */
function commit(props, record, index) {
	runScript.call(this, props, 'SAVE', record, index);
}

/**
 * 收回
 * @param {*} props 
 */
function unCommit(props, record, index) {
	runScript.call(this, props, 'UNSAVE', record, index);
}

/**
 * 提交收回处理
 * @param {*} props 
 */
function runScript(props, actionType, record, index) {
	let { pageConfig } = props;
	let { pagecode, dataSource } = pageConfig;

	let paramInfoMap = {};
	paramInfoMap[record.pk_alter.value] = record.ts.value;
	let id = record[alterVO.pk_alter].value;
	let params = [
		{
			id,
			index
		}
	];

	ajax({
		url: url.commitURL,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: actionType,
			pageid: pagecode.replace('list', 'card')
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				alterVO.pk_alter,
				tableId.replace('list', 'card'),
				tableId,
				false,
				dataSource
			);
		}
	});
}
