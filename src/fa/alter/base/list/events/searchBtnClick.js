import { ajax, toast } from 'nc-lightapp-front';
import { baseConfig } from '../const';
const { tableId, url } = baseConfig;
import { setBatchBtnsEnable } from './buttonClick';

import ampub from 'ampub';

const { components, utils } = ampub;
const { LoginContext } = components;
const { loginContextKeys, getContext } = LoginContext;
const { listUtils, msgUtils } = utils;
const { showMessage} = msgUtils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, queryInfo, isRefresh) {
	let { pageConfig } = props;
	let { pagecode, dataSource } = pageConfig;
	if (!searchVal) {
		return;
	}
	// 获取交易类型编码
	let transi_type = getContext(loginContextKeys.transtype, dataSource);
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(searchAreaId);
	}
	let pageInfo = props.table.getTablePageInfo(tableId);

	queryInfo.pagecode = pagecode;
	queryInfo.pageInfo = pageInfo;
	queryInfo.billtype = 'HG';
	queryInfo.transtype = transi_type;

	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	ajax({
		url: url.querySearchAreaURL,
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBatchBtnsEnable.call(this, props, tableId);
		},
		error: (res) => {
			showMessage(props, { content: res.message, color: 'warning' });
		}
	});
}
