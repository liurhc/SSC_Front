/**
 * 编辑后事件，用于关联查询区条件
 * props: props
 * searchId: 查询区需要的searchId参数
 * field: 编辑后的key
 * val: 编辑后的value
*/

import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage } = fa_components;
const { referLinkageClear } = ReferLinkage;

export default function(field, val, searchId) {
	let queryConfig = {
		//查询区
		searchId: searchId,
		linkageData: {
			//联动链
			//参照字段:['被过滤字段1'，'被过滤字段2']
			pk_org: [
				'pk_accbook',
				'bodyvos.pk_card.pk_usedept',
				'bodyvos.pk_card.pk_mandept',
				'bodyvos.pk_card.pk_category'
			]
		}
	};
	if (field === 'pk_org') {
		referLinkageClear.call(this, this.props, field, queryConfig);
	}
}
