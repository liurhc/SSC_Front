import React, { Component } from 'react';
import { high, base, createPageIcon } from 'nc-lightapp-front';
const { NCAffix } = base;
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	rowSelected,
	doubleClick,
	commit,
	setBatchBtnsEnable,
	afterEvent
} from './events';
import { baseConfig } from './const';
const { searchId, tableId, alterVO, url } = baseConfig;
import ampub from 'ampub';

const { components, utils } = ampub;
const { ApprovalTrans, queryVocherSrcUtils} = components;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { queryVoucherSrc } = queryVocherSrcUtils;

class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showApprove: false,
			pkAlter: '',
			compositedisplay: false,
			compositedata: {},
			//当前用户
			usrid: ''
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {
		let { pageConfig } = this.props;
		//凭证联查来源单据
		queryVoucherSrc(this.props, tableId, 'pk_alter', pageConfig.pagecode);
	}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commit.call(this, this.props, value);
	};

	//取消指派
	turnOff = () => {
		this.props.table.selectAllRows(tableId, false);
		rowSelected.call(this, this.props, tableId);
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { table, search, pageConfig, ncmodal, ncUploader } = this.props;
		let { createSimpleTable } = table;
		let { createModal } = ncmodal;
		let { NCCreateSearch } = search;
		let { createNCUploader } = ncUploader;
		let { title, dataSource, transType } = pageConfig;
		const { ApproveDetail } = high;
		return (
			<div id="fa-originvalue-list" className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: tableId,
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
							{/* {createModal(`${tableId}-del-confirm`, { color: 'warning' })} */}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: afterEvent.bind(this),
						dataSource: dataSource
					})}
				</div>
				<div className="table-area">
					{createSimpleTable(tableId, {
						handlePageInfoChange: pageInfoClick,
						showCheck: true,
						showIndex: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						pkname: alterVO.pk_alter,
						componentInitFinished: () => {
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				<div className="nc-bill-list">{createNCUploader('alter-uploader', {disableModify:(this.props.cardTable.getAllData(tableId).rows[0]==null ? '1': (this.props.table.getCheckedRows(tableId).length>0&&this.props.table.getCheckedRows(tableId)[0].data.values.bill_status.value))!='0'})}</div>
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({
							showApprove: false
						});
					}}
					billtype={transType}
					billid={this.state.pkAlter}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('201202004A-000045')}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

export default List;
