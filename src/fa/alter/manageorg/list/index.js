/**
 * 管理部门调整入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import List from '../../base/list';

const AlterListBase = createPage({})(List);

// 页面配置
const pageConfig = {
	pagecode: '201202020A_list', //单据模板pageID
	appcode: '201202020A', //小应用编码
	title: '201202004A-000054', //表头标题
	printNodekey: null,
	pagename: 'manageorg',
	dataSource: 'fa.alter.manageorg.main',
	transType: 'HG-13' //交易类型
};

// ReactDOM.render(<ManageOrgList pageConfig={pageConfig} />, document.querySelector('#app'));
export default class ManageOrgList extends Component {
	render() {
		return <AlterListBase pageConfig={pageConfig} />;
	}
}
