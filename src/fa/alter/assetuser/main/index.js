import { RenderRouter } from 'nc-lightapp-front';
import routes from './router';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

(function main(routers, htmlTagid) {
	let moduleIds = { fa: [ '201202004A', 'facommon' ], ampub: [ 'common' ] };
	initMultiLangByModule(moduleIds, () => {
		RenderRouter(routers, htmlTagid);
	});
})(routes, 'app');
