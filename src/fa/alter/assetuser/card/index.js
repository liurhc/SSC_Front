/**
 * 资产使用人调整入口
 * created by wangqsh 2018-05-05
 */

import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import Card from '../../base/card';

const AlterCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: '201202028A_card',
		headcode: 'card_head',
		bodycode: 'bodyvos'
	}
})(Card);

const pageConfig = {
	pagecode: '201202028A_card', //页面编码
	appcode: '201202028A', //小应用编码
	transType: 'HG-16', //交易类型
	title: '201202004A-000001',
	printNodekey: null,
	pagename: 'assetuser',
	dataSource: 'fa.alter.assetuser.main'
};

// ReactDOM.render(<AssetUserCard pageConfig={pageConfig} />, document.querySelector('#app'));
export default class AssetUserCard extends Component {
	render() {
		return <AlterCardBase pageConfig={pageConfig} />;
	}
}
