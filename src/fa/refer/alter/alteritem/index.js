import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function AlterItemGridRef(props = {}) {
	let cpconf = {
        multiLang: {
            domainName: 'fa',
            // currentLocale: 'simpchn',
            moduleId: 'refer_fa'
        },
		refType: 'grid',
		refName: 'refer_fa-000003' /*国际化处理：变动项目*/,
		refCode: 'fa.ref.AlterItemGridRef',
		queryGridUrl: '/nccloud/fa/ref/AlterItemGridRef.do',
		isMultiSelectedEnabled: true,
        /*国际化处理：编码，名称*/
		columnConfig: [ { name: [ 'refer_fa-000000', 'refer_fa-000001' ], code: [ 'refcode', 'refname' ] } ]
	};
	return <Refer {...Object.assign(cpconf, props)} />;
}
