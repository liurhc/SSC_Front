import {high} from 'nc-lightapp-front';
import {conf as unitConf} from  '../../../../uapbd/refer/org/FinanceOrgTreeRef'

const {Refer} = high;

export default function (props = {}) {
    let conf = {
        multiLang: {
            domainName: 'fa',
            // currentLocale: 'simpchn',
            moduleId: 'refer_fa'
        },
        refName: 'refer_fa-000009' /*国际化处理：固定资产卡片*/,
        placeholder: 'refer_fa-000009' /*国际化处理：固定资产卡片*/,
        refType: 'gridTree',
        rootNode: {refname: 'refer_fa-000005', refpk: 'root'},   /*国际化处理：固定资产类别*/
        refCode: 'fa.refer.newasset.facardTreeGridRef',
        queryTreeUrl: '/nccloud/fa/ref/facardTreeRef.do',
        queryGridUrl: '/nccloud/fa/ref/facardGridRef.do',
        columnConfig: [
            {
                /* ['资产编号', '资产名称', '规格', '型号', '管理部门', '使用部门', '使用人', '存放地点', '使用状况', '开始使用日期', '本币原值'], */
                name: ['refer_fa-000010',
                    'refer_fa-000011',
                    'refer_fa-000012',
                    'refer_fa-000013',
                    'refer_fa-000014',
                    'refer_fa-000015',
                    'refer_fa-000016',
                    'refer_fa-000017',
                    'refer_fa-000018',
                    'refer_fa-000019',
                    'refer_fa-000020'],
                code: [
                    'asset_code',
                    'asset_name',
                    'spec',
                    'card_model',
                    'mandeptname',
                    'pk_usedept',
                    'bd_psndocname',
                    'position',
                    'status_name',
                    'begin_date',
                    'localoriginvalue'
                ],
                fullTxtCode: {'asset_code': true, 'asset_name': true} //设置搜索列
            }
        ],
        treeConfig: {name: ['refer_fa-000000', 'refer_fa-000001'], code: ['refcode', 'refname']}/*国际化处理：编码，名称*/,
        isMultiSelectedEnabled: false,
        unitProps: unitConf
        /* 可以传递的参数如下
        queryCondition: function () {
            return {
                "pk_org": "",
                "pk_accbook": "",
                "haveNotReduceCondition": true,//是否考虑减少条件
                "oneReduceHave": false,//只要有一个账簿做减少后，是否可参照到卡片
                "returnCardCode": true,// 参照编码返回的是否为卡片编码，如果不是，那么返回的就是资产编码
                "userDefWherePart": true,//执行自定义SQL
            }
        }*/
    };

    return <Refer {...Object.assign(conf, props)} />;
}
