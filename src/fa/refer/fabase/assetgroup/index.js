import { high } from 'nc-lightapp-front';

const { Refer } = high;

//资产组参照
export default function(props = {}) {
	var conf = {
		multiLang: {
			domainName: 'fa',
			// currentLocale: 'simpchn',
			moduleId: 'refer_fa'
		},
		refType: 'grid',
		refName: 'refer_fa-000004' /*国际化处理：资产组*/,
		refCode: 'fa.ref.AssetGroupRefGridAction',
		queryGridUrl: '/nccloud/fa/ref/AssetGroupRefGridAction.do',
		/*国际化处理：编码，名称*/
		columnConfig: [ { name: [ 'refer_fa-000000', 'refer_fa-000001' ], code: [ 'refcode', 'refname' ] } ],
		isMultiSelectedEnable: false,
		isHasDisabledData: true //是否有停用属性
		/* 可以传递的参数如下
        queryCondition: function () {
            return {
                "notShowGroupFlag": true //是否不显示总部资产，默认不传：是false，显示总部资产
            }
        }*/
	};
	return <Refer {...Object.assign(conf, props)} />;
}
