import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function(props = {}) {
	var conf = {
        multiLang: {
            domainName: 'fa',
            // currentLocale: 'simpchn',
            moduleId: 'refer_fa'
		},
        refName: 'refer_fa-000005' /*国际化处理：固定资产类别*/,
		refType: 'tree',
		refCode: 'fa.ref.CategoryRefTreeAction',
		queryTreeUrl: '/nccloud/fa/ref/CategoryRefTreeAction.do',
		queryCondition: function () {
            return {
				"pk_org": "",
				"transi_type":"" //多个交易类型用',隔开
            }
		},
		rootNode : {
			refname : 'refer_fa-000021'/*国际化处理：资产类别*/,
			refpk : 'root'
		},
		onlyLeafCanSelect: true,
		isHasDisabledData: true,//是否有停用属性
		value: [], // 如果需要多选则必须有这个属性
        /*国际化处理：编码，名称*/
		treeConfig: { name: [ 'refer_fa-000000', 'refer_fa-000001' ], code: [ 'refcode', 'refname' ] }
	};
	return <Refer {...Object.assign(conf, props)} />;
}
