import { high } from 'nc-lightapp-front';
const { Refer } = high;

export default function(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'fa',
			// currentLocale: 'simpchn',
			moduleId: 'refer_fa'
		},
		refName: 'refer_fa-000002' /*国际化处理：模拟折旧设置*/,
		placeholder: 'refer_fa-000002' /*国际化处理：模拟折旧设置*/,
		refCode: 'fa.ref.depprogramGridRef',
		refType: 'grid',
		queryGridUrl: '/nccloud/fa/ref/depprogramGridRef.do',
		isMultiSelectedEnabled: false,
		columnConfig: [
			{ name: [ 'refer_fa-000000', 'refer_fa-000001' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：编码，名称*/
	};
	return <Refer {...Object.assign(conf, props)} />;
}
