import { high } from 'nc-lightapp-front';
const { Refer } = high;

export default function (props={}) {
    let conf ={
        multiLang: {
            domainName: 'fa',
            // currentLocale: 'simpchn',
            moduleId: 'refer_fa'
        },
        refName: 'refer_fa-000008' /*国际化处理：使用状况*/,
        placeholder: 'refer_fa-000008' /*国际化处理：使用状况*/,
        rootNode : {
			refname : 'refer_fa-000008'/*国际化处理：使用状况*/,
			refpk : 'root'
		},
        refCode :'fa.ref.usingstatusTreeRef',
        refType :'tree',
        queryTreeUrl:'/nccloud/fa/ref/usingstatusTreeRef.do',
        isMultiSelectedEnable: false,
        isHasDisabledData: true,//是否有停用属性
        onlyLeafCanSelect: true,
        value: [], // 如果需要多选则必须有这个属性
        /*国际化处理：编码，名称*/
        treeConfig: { name: [  'refer_fa-000000', 'refer_fa-000001' ], code: [ 'refcode', 'refname' ] }
    }
    return <Refer {...Object.assign(conf, props)} />;
}


