export default function afterEvent(props, moduleId, key, value, changedrows, index, record) {
	if (key === 'pk_dept') {
		// if (changedrows[0].newvalue.value === changedrows[0].oldvalue.value) {
		// 	return;
		// }
		let usescaleval = record.values.usescale.value;
		let display = record.values.pk_dept.display;
		let val = record.values.pk_dept.value;
		let displayarr = display.split(',');
		let valarr = val.split(',');
		props.editTable.deleteTableRowsByIndex(moduleId, index, true);
		for (let i = 0; i < displayarr.length; i++) {
			let data;
			if (i === 0) {
				data = { pk_dept: { value: valarr[i], display: displayarr[i] }, usescale: { value: usescaleval } };
			} else {
				data = { pk_dept: { value: valarr[i], display: displayarr[i] }, usescale: { value: '100' } };
			}
			props.editTable.addRow(moduleId, index, false, data);
			index++;
		}
	}
}
