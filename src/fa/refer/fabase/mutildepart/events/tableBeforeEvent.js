export default function beforeEvent(props, moduleId, item, index, value, record) {
	if (item.attrcode === 'pk_dept') {
		let meta = props.meta.getMeta();
		if (props.onlyLeafCanSelect) {
			meta['body'].items.map((item) => {
				if (item.attrcode === 'pk_dept') {
					item.onlyLeafCanSelect = true;
				}
			});
		} else {
			meta['body'].items.map((item) => {
				if (item.attrcode === 'pk_dept') {
					item.onlyLeafCanSelect = false;
				}
			});
		}
		props.meta.setMeta(meta);
	}
	return true;
}
