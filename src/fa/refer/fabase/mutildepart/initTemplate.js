import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';

let tableId = 'body';
let pageId = '201200599A_list';
export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: pageId, //页面id
			appcode: '201200599A' //
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_dept') {
			item.queryCondition = props.queryCondition;
			item.isMultiSelectedEnabled = true;
			item.onlyLeafCanSelect = props.onlyLeafCanSelect;
		}
	});
	meta[tableId].items.push({
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /*国际化处理：'操作'*/,
		itemtype: 'customer',
		width: 150,
		className: 'table-opr',
		visible: true,
		render: (text, record, index) => {
			return (
				<span className="delrefer">
					<a
						className="code-detail-link"
						onClick={() => {
							props.editTable.deleteTableRowsByIndex(tableId, index);
						}}
					>
						{getMultiLangByID('201200599A-000005' /*国际化处理：'删行'*/)}
					</a>
				</span>
			);
		}
	});
	return meta;
}
