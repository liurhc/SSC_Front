// 比较两个值(基本类型值或对象）是否一样
export function compare(a, b) {
	if (typeof a === 'object' && typeof b === 'object') {
		if (a && b) {
			// 去掉undefined
			a = JSON.parse(JSON.stringify(a))
			b = JSON.parse(JSON.stringify(b))
			var akeys = Object.keys(a),
				bkeys = Object.keys(b);
			if (akeys.length === bkeys.length) {
				for (var i = 0, l = akeys.length; i < l; i++) {
					var flag = compare(a[akeys[i]], b[akeys[i]]);
					if (!flag) {
						return false;
					}
				}
				return true;
			} else {
				return false;
			}
		} else {
			Object.is(a, b);
		}
	} else {
		return a == b;
	}
}

// 比较两个数组是否含有同样的值
export function compareArr(arr1, arr2) {
	if (arr1.length !== arr2.length) return false;
	arr1.sort();
	arr2.sort();
	let flag = true;
	for (let i = 0; i < arr1.length; i++) {
		if (arr1[i] !== arr2[i]) {
			flag = false;
			break;
		}
	}
	return flag;
}
