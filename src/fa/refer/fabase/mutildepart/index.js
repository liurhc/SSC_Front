import React from 'react';
import { createPage, ajax, base, high, toast } from 'nc-lightapp-front';
import initTemplate from './initTemplate';
import { compareArr } from './utils';
import { tableAfterEvent, tableBeforeEvent } from './events';
import { getMultiLangByID, initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';

const { PopRefer } = high.Refer, // 引入PopRefer类
	{ NCButton } = base;
const tableId = 'body';
class DepartRef extends PopRefer {
	// 继承PopRefer类
	constructor(props) {
		super(props);
		//initTemplate.call(this, props);
	}
	dropDownSearch = () => {};

	//loadTableData = () => {};
	//复写平台方法 --去掉请求
	show = () => {
		let { disabled } = this.props;
		let tableVal = this.props.value;
		if (!tableVal) {
			//tableVal = this.foolValueToValue(this.props.foolValue);
			tableVal = [];
			let valdata = this.props.foolValue;
			let namval = valdata.display;
			let pkVal = valdata.value;
			if (namval && pkVal) {
				let namearr = namval.split(',');
				let pkarr = pkVal.split(',');
				for (let i = 0; i < namearr.length; i++) {
					tableVal[i] = {};
					tableVal[i]['refname'] = namearr[i];
					tableVal[i]['refpk'] = pkarr[i];
				}
			}
		} else if (!tableVal.value) {
			tableVal = [];
		} else {
			let timtableVal = [];
			let namval = tableVal.display;
			let pkVal = tableVal.value;
			if (namval && pkVal) {
				let namearr = namval.split(',');
				let pkarr = pkVal.split(',');
				for (let i = 0; i < namearr.length; i++) {
					timtableVal[i] = {};
					timtableVal[i]['refname'] = namearr[i];
					timtableVal[i]['refpk'] = pkarr[i];
				}
			}
			tableVal = timtableVal;
		}

		let rows = [];
		if (tableVal) {
			tableVal.map((e) => {
				let nameStr = e.refname;
				let nameResult = nameStr.split(':');
				let pkStr = e.refpk;
				let result = pkStr.split('&');
				let departVal = {
					values: {
						pk_dept: { display: nameResult[0], value: result[0] },
						usescale: { display: result[1], value: result[1] },
						link_key: { display: result[2], value: result[2] },
						memo: { display: result[3], value: result[3] },
						pk_dept_v: { display: result[4], value: result[4] },
						pk_deptscale: { display: result[5], value: result[5] },
						pk_org: { display: result[6], value: result[6] },
						totalscale: { display: result[7], value: result[7] },
						ts: { display: result[8], value: result[8] },
						dr: { display: result[9], value: result[9] }
					}
				};
				rows.push(departVal);
			});
		}

		this.props.editTable.setTableData(tableId, { rows: rows });
		this.props.editTable.setStatus(tableId, 'edit');
		if (disabled) {
			return false;
		}
		this.setState(
			{
				isShow: true,
				isFirstShow: false,
				dropDownShow: false,
				isSelectedShow: false,
				searchVal: '',
				treeSearchVal: '',
				selectedShow: false,
				max: false
			},
			() => {
				this.handleClick();
			}
		);
	};
	multiSelect = () => {
		//使用部门比例精度
		let usescaleNum;
		let meta = this.props.meta.getMeta();
		meta[tableId].items.map((item) => {
			if (item.attrcode == 'usescale') {
				usescaleNum = item.scale;
			}
		});

		this.props.editTable.filterEmptyRows(tableId);
		let data;
		try {
			data = this.props.editTable.getAllRows(tableId, false);
		} catch (e) {
			data = [];
		}
		let dealData = [];
		data.map((e) => {
			if (e.status !== '3') {
				dealData.push(e);
			}
		});
		let valueStr = [
			'pk_dept',
			'usescale',
			'link_key',
			'memo',
			'pk_dept_v',
			'pk_deptscale',
			'pk_org',
			'totalscale',
			'ts',
			'dr'
		];
		let selections = [];
		for (let i = 0; i < dealData.length; i++) {
			let refStr;
			if (dealData.length === 1) {
				for (let j = 0; j < valueStr.length; j++) {
					let mVal = valueStr[j];
					if (dealData[i].values[mVal].value) {
						if (refStr !== undefined) {
							if (valueStr[j] === 'usescale') {
								refStr = refStr + '&' + '100';
							} else refStr = refStr + '&' + dealData[i].values[mVal].value;
						} else {
							if (valueStr[j] === 'usescale') {
								refStr = '100';
							} else refStr = dealData[i].values[mVal].value;
						}
					} else {
						if (mVal === 'pk_dept') {
							toast({
								content: getMultiLangByID('201200599A-000000') /*国际化处理：'部门不能为空'*/,
								color: 'warning'
							});
							return;
						}
						if (refStr !== undefined) {
							if (valueStr[j] === 'usescale') {
								refStr = refStr + '&' + '100';
							} else refStr = refStr + '&' + 'null';
						} else {
							if (valueStr[j] === 'usescale') {
								refStr = '100';
							} else refStr = 'null';
						}
					}
				}
				if (dealData[i].values.usescale.value <= 0 && dealData.length > 1) {
					toast({ content: getMultiLangByID('201200599A-000001') /*'比例必须大于零，请重新录入'*/, color: 'warning' });
					return;
				}
				let valS = {
					refname: dealData[i].values.pk_dept.display, //display
					refpk: refStr
				};
				selections.push(valS);
			} else {
				for (let k = 0; k < valueStr.length; k++) {
					let tVal = valueStr[k];
					if (dealData[i].values[tVal].value) {
						if (refStr !== undefined) {
							refStr = refStr + '&' + dealData[i].values[tVal].value;
						} else {
							refStr = dealData[i].values[tVal].value;
						}
					} else {
						if (tVal === 'pk_dept') {
							toast({ content: getMultiLangByID('201200599A-000000') /*''部门不能为空'*/, color: 'warning' });
							return;
						} else if (tVal === 'usescale') {
							refStr = refStr + '&' + '100';
							dealData[i].values.usescale.value = 100;
						} else {
							if (refStr !== undefined) {
								refStr = refStr + '&' + 'null';
							} else {
								refStr = 'null';
							}
						}
					}
				}
				if (dealData[i].values.usescale.value <= 0 && dealData.length > 1) {
					toast({ content: getMultiLangByID('201200599A-000001') /*'''比例必须大于零，请重新录入''*/, color: 'warning' });
					return;
				}

				let displayVal = dealData[i].values.usescale.value;
				//处理使用部门比例精度
				if (displayVal && usescaleNum) {
					displayVal = parseFloat(displayVal).toFixed(usescaleNum);
				}
				let ValSt = {
					refname: dealData[i].values.pk_dept.display + ':' + displayVal, //display
					refpk: refStr
				};
				selections.push(ValSt);
			}
		}

		let pkdepArry = [];
		for (let i = 0; i < selections.length; i++) {
			let pk_depStr = selections[i].refpk;
			let pk_dep = pk_depStr.split('&')[0];
			pkdepArry.push(pk_dep);
		}
		let nary = pkdepArry.sort();
		for (var i = 0; i < pkdepArry.length; i++) {
			if (nary[i] == nary[i + 1]) {
				toast({ content: getMultiLangByID('201200599A-000002') /*国际化处理：'部门不能重复'*/, color: 'warning' });
				return;
			}
		}
		let { onChange, onBlur, fieldDisplayed, fieldValued } = this.props;
		let isEqual = compareArr(selections.map((e) => e.refpk), [ ...this._value.keys() ]);
		isEqual || this.pushToLocal([ ...selections ]);
		let index = selections.length > 1 ? selections.findIndex((e) => /^#.*#$/.test(e.refpk)) : -1;
		if (index !== -1) {
			selections.splice(index, 1);
		}
		this.setState(
			{
				referVal: '',
				isShow: false,
				isMoreButtonShow: false
			},
			() => {
				let foolDisplay = selections
						.map((item) => {
							return Array.isArray(fieldDisplayed)
								? fieldDisplayed.map((e) => item[e]).join('/')
								: item[fieldDisplayed] || item.refname;
						})
						.join(','),
					foolValue = selections
						.map((item) => {
							return Array.isArray(fieldValued)
								? fieldValued.map((e) => item[e]).join('/')
								: item[fieldValued];
						})
						.join(',');
				!isEqual &&
					typeof onChange === 'function' &&
					onChange(selections, { display: foolDisplay, value: foolValue });
				typeof onBlur === 'function' && onBlur(selections, { display: foolDisplay, value: foolValue });
			}
		);
	};
	handleClick = () => {
		let rowData = this.props.editTable.getAllRows(tableId, false);
		let realData = [];
		for (let aVal of rowData) {
			if (aVal.status != '3') {
				realData.push(aVal);
			}
		}
		let length = realData.length;

		if (length < 1) {
			this.props.editTable.addRow(tableId, length);
			this.props.editTable.setValByKeyAndIndex(tableId, length, 'usescale', { value: '100', display: '100' });
		}
	};

	addhandleClick = () => {
		let rowData = this.props.editTable.getAllRows(tableId, false);
		let realData = [];
		for (let aVal of rowData) {
			if (aVal.status != '3') {
				realData.push(aVal);
			}
		}
		let length = realData.length;
		this.props.editTable.addRow(tableId, length);
		this.props.editTable.setValByKeyAndIndex(tableId, length, 'usescale', { value: '100', display: '100' });
	};

	//复写平台方法 --取消分页
	renderPopoverPageArea = () => {
		return;
	};

	//绘制表格区域
	renderPopoverRight = () => {
		return (
			<div className="nc-bill-table-area">
				<NCButton
					shape="border"
					className="addlinebtn"
					colors="primary"
					onClick={this.addhandleClick}
					style={{
						float: 'right',
						marginRight: '20px',
						marginTop: '5px',
						marginBottom: '5px',
						border: 'none'
					}}
				>
					{getMultiLangByID('201200599A-000003' /*国际化处理：'增行'*/)}
				</NCButton>
				{this.props.editTable.createEditTable(tableId, {
					onAfterEvent: tableAfterEvent.bind(this),
					onBeforeEvent: tableBeforeEvent.bind(this)
				})}
			</div>
		);
	};

	renderPopoverBottom = () => {
		return (
			<div
				className="buttons"
				key="3"
				style={{
					marginLeft: 'auto'
				}}
			>
				<NCButton
					style={{
						backgroundColor: '#E14C46',
						color: '#fff'
					}}
					onClick={this.multiSelect}
				>
					{getMultiLangByID('201200599A-000004' /*国际化处理：'确定'*/)}
				</NCButton>
				<NCButton
					style={{
						backgroundColor: '#eee',
						color: '#666',
						marginLeft: '9px'
					}}
					onClick={this.cancel}
				>
					{getMultiLangByID('msgUtils-000002' /*国际化处理：'取消'*/)}
				</NCButton>
			</div>
		);
	};
}
DepartRef = createPage({ initTemplate: initTemplate, appAutoFocus: false })(DepartRef);
initMultiLangByModule({ fa: [ '201200599A', 'facommon' ], ampub: [ 'common' ] }, () => {});

export default function(props = {}) {
	var conf = {
		refName: getMultiLangByID('facommon-000010') /*国际化处理：使用部门*/,
		placeholder: getMultiLangByID('facommon-000010') /*国际化处理：使用部门*/,
		refType: 'grid',
		queryCondition: function() {
			return {
				pk_org: '',
				busifuncode: ''
			};
		}
	};

	return <DepartRef {...Object.assign(conf, props)} />;
}
