import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function(props = {}) {
	var conf = {
		multiLang: {
			domainName: 'fa',
			// currentLocale: 'simpchn',
			moduleId: 'refer_fa'
		},
		refName: 'refer_fa-000006' /*国际化处理：折旧方法*/,
		placeholder: 'refer_fa-000006' /*国际化处理：折旧方法*/,
		refType: 'grid',
		refCode: 'fa.refer.fabase.depmethod',
		queryGridUrl: '/nccloud/fa/ref/depmethod.do',
		isMultiSelectedEnabled: false,
		isHasDisabledData: true, //是否有停用属性
		/*国际化处理：编码，名称*/
		columnConfig: [ { name: [ 'refer_fa-000000', 'refer_fa-000001' ], code: [ 'refcode', 'refname' ] } ]
	};

	return <Refer {...Object.assign(conf, props)} />;
}
