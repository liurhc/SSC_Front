import { ajax, toast } from 'nc-lightapp-front';
// 平台不支持小部件公共打包，改回原来的引用方式
import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
import { getContext, loginContextKeys } from '../../../../../ampub/common/components/AMInitInfo/loginContext';
import { getAccbooksAndPeriodsBase } from '../../../../common/components/AccbookAndPeriod/AccbookAndPeriodBase';

/**
 * 编辑后事件
 * @param {*} id
 * @param {*} value
 */
export default function(id, value) {
	switch (id) {
		case 'org':
			onOrgChange.call(this, value);
			break;
		case 'accbook':
			onAccbookChange.call(this, value);
			break;
		case 'year':
			onYearsChange.call(this, value);
			break;
		default:
			break;
	}
}

/**
 * 组织选择框切换事件,赋值并查询
 * @author：zhangxxu
 */
function onOrgChange(value) {
	let that = this;
	getAccbooksAndPeriodsBase(value, getContext(loginContextKeys.businessDate), (res) => {
		let stateData = {
			orgselect: value,
			orgdata: that.state.orgdata,
			accbookdata: {},
			yeardata: {},
			accbookselect: '',
			accyearselect: ''
		};
		if (res.success) {
			if (res.data && res.data.length > 0) {
				//组装账簿下拉范围
				let accbookData = []; //账簿下拉信息
				let mainAccbook = {}; //默认账簿
				accbookData = res.data.map((item) => {
					that.drillAccbook[item.pk_accbook] = item.accbookName; //用于联查数据形式为pk:name
					//如果是主账簿，放到第一个
					if (item.isMainbook) {
						mainAccbook = {
							display: item.accbookName,
							value: item.pk_accbook,
							years: item.years,
							accyear: item.minUnClosebookPeriod.accyear
						};
					}
					return {
						display: item.accbookName, //name
						value: item.pk_accbook, //pk
						accyear: item.minUnClosebookPeriod.accyear, //当前会计年
						years: item.years //会计年数组
					};
				});
				//查询区数据
				stateData = {
					orgselect: value,
					orgdata: that.state.orgdata,
					accbookdata: accbookData, //账簿下拉数据
					yeardata: mainAccbook.years, //年度下拉数据
					accbookselect: mainAccbook.value, //默认账簿
					accyearselect: mainAccbook.accyear //默认会计年
				};
			}
			// 渲染查询区
			that.state = stateData;
			getData.call(that);
		}
	});
}

/**
 * 账簿切换事件,设置账簿数据并查询
 * @param {*} value 账簿恻然值
 */
function onAccbookChange(value) {
	let that = this;
	that.state.accbookdata.map((item) => {
		if (item.value == value) {
			that.state.accbookselect = value;
			that.state.yeardata = item.years;
			that.state.accyearselect = item.accyear;
			getData.call(that);
		}
	});
}

/**
 * 年度切换事件,设置年份数据并查询
 * @param {*} value 年份的值
 */
function onYearsChange(value) {
	let that = this;
	that.state.accyearselect = value;
	getData.call(that);
}

/**
 * 执行查询
 */
export function getData() {
	let that = this;

	//查询条件
	let queryData = {
		pk_org: that.state.orgselect,
		pk_accbook: that.state.accbookselect,
		accyear: that.state.accyearselect
	};
	let stateData = that.state;
	if (queryData.pk_org) {
		ajax({
			url: '/nccloud/fa/tiles/querygeneral.do',
			data: queryData,
			success: (res) => {
				let searchData = {};
				if (res.success && res.data) {
					searchData = res.data;
				}
				//设置父组件state 重新渲染图表
				setParentState.call(that, searchData, stateData, () => {
					if (that.isRefresh) {
						toast({ content: getMultiLangByID('msgUtils-000020') /*刷新成功*/, color: 'success' });
						that.isRefresh = false;
					}
				});
			},
			error: (res) => {
				//设置父组件state 重新渲染图表
				setParentState.call(that, {}, stateData, () => {
					//提示错误信息
					toast({ content: res.message, color: 'warning' });
				});
			}
		});
	} else {
		//设置父组件state 重新渲染图表
		setParentState.call(that, {}, stateData, () => {
			that.isRefresh = false;
		});
	}
}

/**
 * 设置父组件state
 * @param {*} searchData 查询回来的数据
 * @param {*} searchAreaData 查询条件
 */
function setParentState(searchData, searchAreaData, callback = () => {}) {
	this.props.parentState.setState(
		{
			searchData: searchData,
			searchAreaData: searchAreaData,
			searchArea: this
		},
		callback()
	);
}
