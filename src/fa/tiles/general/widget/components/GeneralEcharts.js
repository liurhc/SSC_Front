import React, { Component } from 'react';
import echarts from 'echarts';
import { getData } from './onChange';

// 平台不支持小部件公共打包，改回原来的引用方式
import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
/**
 * 固定资产总账--echarts图表
 * @author:zhangxxu
 */
export default class GeneralEcharts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			accudeps: [],
			localoriginvalues: [],
			periods: []
		};
		this.nextProps;
	}

	componentDidMount() {}

	componentWillReceiveProps(nextProps) {
		this.nextProps = nextProps;
		this.setState(
			{
				accudeps: nextProps.searchData.accudeps,
				localoriginvalues: nextProps.searchData.localoriginvalues,
				periods: nextProps.searchData.periods
			},
			() => {
				let option = {
					//悬浮提示框
					tooltip: {
						trigger: 'axis',
						axisPointer: {
							// 坐标轴指示器，坐标轴触发有效
							type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
						}
					},
					//图例
					legend: {
						data: [
							getMultiLangByID('201204514-000000') /*原值*/,
							getMultiLangByID('201204514-000001') /*累计折旧*/
						]
					},
					xAxis: [
						{
							type: 'category',
							//TODO:X坐标的类目
							data: this.state.periods,
							axisTick: {
								alignWithLabel: true
							}
						}
					],
					//距离容器距离
					grid: {
						top: 30,
						left: 80,
						bottom: 30
					},
					yAxis: [
						{
							type: 'value'
						}
					],
					series: [
						{
							name: getMultiLangByID('201204514-000000') /*原值*/,
							type: 'bar',
							barWidth: 15,
							//TODO:原值的数据
							data: this.state.localoriginvalues
						},
						{
							name: getMultiLangByID('201204514-000001') /*累计折旧*/,
							type: 'bar',
							barWidth: 15,
							//TODO:累计折旧的数据
							data: this.state.accudeps
						}
					],
					color: [ '#20CBAF', '#FF9A48' ],
					backgroundColor: 'rgb(255, 255, 255)'
				};
				const generalEcharts = echarts.init(document.getElementById('generalEcharts'));
				generalEcharts.setOption(option);
			}
		);
	}
	render() {
		let nextProps = this.nextProps;
		return (
			<div className="generalEcharts-area">
				<div id="generalEcharts" />
				<span
					className="iconfont icon-shuaxin icon-refresh"
					onClick={() => {
						if (nextProps) {
							let pk_org = nextProps.searchArea.state.orgselect;
							if (pk_org) {
								nextProps.searchArea.isRefresh = true;
								getData.call(nextProps.searchArea);
							}
						}
					}}
				/>
			</div>
		);
	}
}
