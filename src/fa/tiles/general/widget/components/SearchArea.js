import React, { Component } from 'react';
import { ajax, toast, base } from 'nc-lightapp-front';
import onChange from './onChange';

import {
	loginContext,
	getContext,
	loginContextKeys
} from '../../../../../ampub/common/components/AMInitInfo/loginContext';
import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';

const { NCSelect } = base;
const NCOption = NCSelect.NCOption;

/**
 * 固定资产总账--查询区
 * @author:zhangxxu
 */
export default class SearchArea extends Component {
	constructor(props) {
		super(props);
		this.state = {
			orgdata: {},
			accbookdata: {},
			yeardata: {},
			orgselect: '',
			accbookselect: '',
			accyearselect: ''
		};
		this.loadIndex = 1;
		this.drillOrg = {};
		this.drillAccbook = {};
		this.onChange = onChange.bind(this);
		this.handleClick = this.handleClick.bind(this);
	}
	componentWillReceiveProps(nextProps) {
		this.setState({
			...nextProps.parentState.state.searchAreaData
		});
	}

	componentDidMount() {
		this.props.createUIDom({ pagecode: '201204512A_list', appcode: '201204512A' }, (data) => {
			loginContext(data.context);
			const { pageConfig = {} } = this.props;
			const { appcode, URL } = pageConfig;
			ajax({
				url: URL.queryOrg,
				data: {
					appcode
				},
				success: (res) => {
					if (res.success && res.data) {
						this.state.orgdata = res.data;
						this.state.orgdata.map((item) => {
							this.drillOrg[item.value] = item.display.split(' ')[1];
						});
						this.state.orgselect = getContext(loginContextKeys.pk_org);
						this.onChange('org', this.state.orgselect);
					}
				},
				error: (res) => {
					toast({ content: res.message, color: 'warning' });
				}
			});
		});
	}
	handleClick() {
		//拼写查询区
		let { orgselect, accbookselect, accyearselect } = this.state;
		if (orgselect && accbookselect && accyearselect) {
			let querycondition = {
				logic: 'and',
				conditions: [
					{
						display: this.drillOrg[this.state.orgselect],
						field: 'pk_org',
						oprtype: '=',
						isIncludeSub: false,
						value: {
							firstvalue: this.state.orgselect,
							secondvalue: ''
						}
					},
					{
						display: this.drillAccbook[this.state.accbookselect],
						field: 'pk_accbook',
						oprtype: '=',
						isIncludeSub: false,
						value: {
							firstvalue: this.state.accbookselect,
							secondvalue: ''
						}
					},
					{
						display: `${this.state.accyearselect}-01`,
						field: 'start_period',
						oprtype: '>=',
						isIncludeSub: false,
						value: {
							firstvalue: `${this.state.accyearselect}-01`,
							secondvalue: ''
						}
					},
					{
						display: `${this.state.accyearselect}-${this.props.parentState.state.searchData.periods
							.length}`,
						field: 'end_period',
						oprtype: '<=',
						isIncludeSub: false,
						value: {
							firstvalue: `${this.state.accyearselect}-${this.props.parentState.state.searchData.periods
								.length}`,
							secondvalue: ''
						}
					}
				]
			};
			// sessionStorage.setItem(`LinkReport`, JSON.stringify(querycondition));
			//由于session是按页面存在的，别的页面删除不了，与平台协商之后改为localstorage
			localStorage.setItem(`LinkReport`, JSON.stringify(querycondition));

			this.props.openTo('/fa/report/general/list/index.html', {
				appcode: '201204512A',
				pageCode: '201204512A_list'
			});
		}
	}

	render() {
		return (
			<div className="general-search">
				<p className="search-txt">{getMultiLangByID('201204514-000003') /*固定资产总账*/}</p>
				{/* 组织 */}
				{createSelect.call(this, this.state.orgselect, 'org')}
				{/* 账簿 */}
				{createSelect.call(this, this.state.accbookselect, 'accbook')}
				{/* 年度 */}
				{createSelect.call(this, this.state.accyearselect, 'year')}
				<p className={`search-link ${this.state.orgselect ? 'hasData' : 'noData'}`} onClick={this.handleClick}>
					{getMultiLangByID('201204514-000004') /*查询详情*/}
				</p>
			</div>
		);
	}
}

function createSelect(flag, type) {
	let loadIndex = this.loadIndex;
	if (flag) {
		flag = true;
	} else {
		if (loadIndex == 2 || loadIndex == 1) {
			flag = false;
		} else {
			flag = true;
		}
	}
	let { orgdata, accbookdata, yeardata } = this.state;
	let returnNode;
	switch (type) {
		case 'org':
			if (flag) {
				returnNode = (
					<NCSelect
						value={this.state.orgselect}
						onChange={(value) => {
							this.onChange('org', value);
						}}
					>
						{orgdata &&
							orgdata.map &&
							orgdata.map((item) => {
								return <NCOption value={item.value}>{item.display}</NCOption>;
							})}
					</NCSelect>
				);
			} else {
				returnNode = (
					<NCSelect
						placeholder={getMultiLangByID('201204514-000005') /*财务组织*/}
						onChange={(value) => {
							this.onChange('org', value);
						}}
					>
						{orgdata &&
							orgdata.map &&
							orgdata.map((item) => {
								return <NCOption value={item.value}>{item.display}</NCOption>;
							})}
					</NCSelect>
				);
			}
			break;
		case 'accbook':
			if (flag) {
				returnNode = (
					<NCSelect
						value={this.state.accbookselect}
						disabled={this.state.orgselect ? false : true}
						onChange={(value) => {
							this.onChange('accbook', value);
						}}
					>
						{accbookdata &&
							accbookdata.map &&
							accbookdata.map((item) => {
								return <NCOption value={item.value}>{item.display}</NCOption>;
							})}
					</NCSelect>
				);
			} else {
				returnNode = (
					<NCSelect
						placeholder={getMultiLangByID('201204514-000006') /*资产账簿*/}
						disabled={this.state.orgselect ? false : true}
						onChange={(value) => {
							this.onChange('accbook', value);
						}}
					>
						{accbookdata &&
							accbookdata.map &&
							accbookdata.map((item) => {
								return <NCOption value={item.value}>{item.display}</NCOption>;
							})}
					</NCSelect>
				);
			}
			break;
		case 'year':
			if (flag) {
				returnNode = (
					<NCSelect
						value={this.state.accyearselect}
						disabled={this.state.orgselect ? false : true}
						onChange={(value) => {
							this.onChange('year', value);
						}}
					>
						{yeardata &&
							yeardata.map &&
							yeardata.map((item) => {
								return <NCOption value={item}>{item}</NCOption>;
							})}
					</NCSelect>
				);
			} else {
				returnNode = (
					<NCSelect
						placeholder={getMultiLangByID('201204514-000007') /*年度*/}
						disabled={this.state.orgselect ? false : true}
						onChange={(value) => {
							this.onChange('year', value);
						}}
					>
						{yeardata &&
							yeardata.map &&
							yeardata.map((item) => {
								return <NCOption value={item}>{item}</NCOption>;
							})}
					</NCSelect>
				);
			}
			this.loadIndex++;
			break;

		default:
			break;
	}
	return returnNode;
}
