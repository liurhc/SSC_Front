import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';

import { SearchArea, GeneralEcharts } from './components';
import { pageConfig } from './const';
import './index.less';
// 平台不支持小部件公共打包，改回原来的引用方式
import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';

/**
 * 小部件--固定资产总账
 * @author:zhangxxu
 * 
 */
class General extends Component {
	constructor(props) {
		super(props);
		this.state = {
			searchData: {},
			searchAreaData: {},
			searchArea: {}
		};
	}

	render() {
		return (
			<div className="am-fa-tiles-general-area">
				<SearchArea {...this.props} pageConfig={pageConfig} parentState={this} />
				<GeneralEcharts pageConfig={pageConfig} {...this.state} />
			</div>
		);
	}
}
initMultiLangByModule({ fa: [ '201204514' ], ampub: [ 'common' ] });
General = createPage({})(General);
export default General;
