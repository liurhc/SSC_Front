/*
* 固定资产卡片维护-列表页
* 对应NC端【资产增加】节点
* */
import React, {Component} from 'react';
import ListBase from '../../facardbase/list';
import {PageConfig} from '../const';
import {createPage, cardCache} from 'nc-lightapp-front';

import constants from "../../facardbase/constants";
const templateDataSource = constants.templateDataSource;

class FacardList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            am_source: '',//原始卡片应用标志
        };
    }

    componentDidMount() {
        //是否来源于原始卡片标志:两种场景1、从原始卡片跳转过来时 url 地址， 2、从卡片态返回列表态时
        const am_source_url = this.props.getUrlParam('am_source');
        const am_source_cache = cardCache.getDefData('am_source', templateDataSource);
        const am_source = am_source_url || am_source_cache;
        //console.log('---------  list 资产增加 入口  componentDidMount ： am_source =' + (am_source));
        cardCache.setDefData('am_source', templateDataSource, am_source);

        this.setState({
            am_source
        });
    }

    render() {
        let pageConfig = this.state.am_source === 'orig' ? PageConfig.OldCard : PageConfig.CardNew;
        pageConfig.am_source = this.state.am_source;
        return (
            <ListBase pageConfig={pageConfig} {...this.props}/>
        );
    }
}
FacardList = createPage({})(FacardList);

export default FacardList;