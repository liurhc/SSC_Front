/*
* 固定资产卡片维护-打开消息
* */
import React, {Component} from 'react';
import {createPage, ajax} from 'nc-lightapp-front';
import {facard_msg_dataSource} from '../const';

//@zhangypm切换到正确的导入
import ampub from 'ampub';
// import ampub from '../../../../ampub/common/collection/release';

const {components, utils} = ampub;

const {setContext}  = components.LoginContext;
const {showMessage} = utils.msgUtils;
const { getMultiLangByID } = utils.multiLangUtils;


class FacardMsg extends Component {
    constructor(props) {
        super(props);

        this.openMsg.bind(this);

        this.initTemplate(props);
    }


    popTranstypeDialog() {
        let timer = setInterval(() => {
            var ss = document.getElementsByClassName('icon-refer');
            if (ss && ss.length > 0) {
                ss[0].click();
                clearInterval(timer);
            }
        }, 500);
    }


    initTemplate(props) {
        props.createUIDom(
            {
                //页面编码
                pagecode: '201201504A_msg'
            },
            (data) => {
                if (data) {
                    if (data.template) {
                        let meta = data.template;
                        meta['card_head'].items.map((item) => {
                            if (item.attrcode === 'transitype') {
                                item.queryCondition = () => {
                                    return {parentbilltype: 'H1'};
                                };
                            }
                        });

                        props.meta.setMeta(meta, () => {
                            props.form.setFormStatus('card_head', 'edit');
                            this.openMsg();
                        });
                    }
                }
            }
        );
    }

    afterEvent(props, moduleId, key, value, oldValue, record) {
        let {refpk, refcode} = value;
        let obj = {
            data: this.data,
            transi_type: refcode,
            pk_transitype: refpk,
        };

        //发送一个 ajax 到后台进行转换
        ajax({
            url: '/nccloud/fa/facard/messageTrans.do',
            data: obj,
            success: (res) => {
                let {success, data} = res;
                if (success && data) {
                    setContext('facard_msg', data, facard_msg_dataSource);
                    this.linkToCard(props, {
                        status: 'add',
                        transiType: refcode,
                        pk_transitype: refpk,
                        pagecode: refcode,
                    });
                }
            },
            error: (res) => {
                showMessage(props, {content: res.message, color: 'danger'});
            }
        });
    }

    linkToCard(props, param) {
        props.pushTo('/card', param);
    }

    //处理代办中心的消息打开
    openMsg() {
        const that = this;
        //处理消息打开
        let pkMsg = this.props.getUrlParam('pk_msg');
        if (pkMsg && pkMsg !== 'undefined' && pkMsg !== 'null') {
            ajax({
                url: '/nccloud/fa/facard/openmessage.do',
                data: {
                    pk_message: pkMsg
                },
                success: (res) => {
                    let {success, data} = res;
                    if (success && data) {
                        if (data.BillCard) {//类别字段有值
                            let {BillCard, FAParam} = data;
                            let assetAllData = [];
                            BillCard.heads.forEach((head) => {
                                if (head.assetAll && head.assetAll.rows && head.assetAll.rows.length > 0) {
                                    assetAllData = assetAllData.concat(head.assetAll.rows);
                                }
                            });
                            if (assetAllData.length > 0) {
                                //跳转到卡片新增界面
                                let transi_type = assetAllData[0].values.transi_type.value;
                                let pk_transitype = assetAllData[0].values.pk_transitype.value;
                                setContext('facard_msg', data, 'asset.facard.msg');
                                this.linkToCard(that.props, {
                                    status: 'add',
                                    transiType: transi_type,
                                    pk_transitype: pk_transitype,
                                    pagecode: transi_type,
                                });
                            }
                        } else {//弹出选择交易类型框
                            that.data = data;//此时data的类型为 vo 而不是 BillCard
                            this.popTranstypeDialog();
                        }
                    } else {
                        /*国际化处理：消息打开失败!*/
                        showMessage(that.props, {content: getMultiLangByID('facommon-000008'), color: 'danger'});
                    }
                },
                error: (res) => {
                    if (res && res.message) {
                        showMessage(that.props, {content: res.message, color: 'warning'});
                    }
                }
            });
        } else {
            /*国际化处理：消息打开错误!*/
            showMessage(that.props, {content: getMultiLangByID('facommon-000009'), color: 'danger'});
        }

    }

    render() {
        let {createForm} = this.props.form;
        return <div style={{display: 'none'}}>{
            createForm('card_head', {
                onAfterEvent: this.afterEvent.bind(this)
            })
        }
        </div>
    }
}

FacardMsg = createPage({})(FacardMsg);

export default FacardMsg;