
//@zhangypm切换到正确的导入
import ampub from 'ampub';
// import ampub from '../../../../ampub/common/collection/release';

const {utils} = ampub;

const {getMultiLangByID} = utils.multiLangUtils;

//公共配置
const PageConfig = {
    CardNew: {
        title: getMultiLangByID('201201504A-000000') /* 国际化处理： 资产增加*/,
        appid: '201201504A',//小应用编码
        nodecode: '2012016030',//功能节点号
        billType: 'H1',//单据类型
        newasset_flag: 1, //新增标志
    },
    OldCard: {
        title: getMultiLangByID('201201504A-000001') /* 国际化处理： 录入原始卡片*/,
        appid: '201201508A',//小应用编码 正确的 201201508A
        nodecode: '2012012010',//功能节点号
        billType: 'H0',//单据类型
        newasset_flag: 0, //新增标志
    }
};

//消息 数据缓存key
const facard_msg_dataSource = 'asset.facard.msg';

export {
    PageConfig,
    facard_msg_dataSource
}