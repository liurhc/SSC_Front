/*
* 固定资产卡片维护-卡片页
* */
import React, {Component} from 'react';
import CardBase from '../../facardbase/card';
import {PageConfig} from '../const';
import {createPage, cardCache} from 'nc-lightapp-front';
import constants from "../../facardbase/constants";

const templateDataSource = constants.templateDataSource;

class FacardCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            am_source: ''
        }
    }

    componentDidMount() {
        //console.log('--------  card 资产增加  入口  componentDidMount  ------');
        //是否来源于原始卡片标志:两种场景1、从原始卡片跳转过来时 url 地址， 2、从卡片态返回列表态时
        const am_source_url = this.props.getUrlParam('am_source');
        const am_source_cache = cardCache.getDefData('am_source', templateDataSource);
        const am_source = am_source_url || am_source_cache;
        cardCache.setDefData('am_source', templateDataSource, am_source);

        //console.log('--------- 来源标志 am_source = ' + am_source);
        this.setState({
            am_source
        });
    }


    render() {
        let pageConfig = this.state.am_source === 'orig' ? PageConfig.OldCard : PageConfig.CardNew;
        pageConfig.am_source = this.state.am_source;
        return (
            <CardBase pageConfig={pageConfig} {...this.props}/>
        );
    }
}

FacardCard = createPage({
    billinfo: {//元数据关联项处理参数
        billtype: 'extcard',
        pagecode: '',//由于这个参数是从 url地址中动态获取的， 这里取不到值，后续平台自动从url中获取，这里就可以写成 '' 了
        headcode: 'asset',
        bodycode: ['dept_tab'],
        tabletype: 'cardTable',
    },
})(FacardCard);

export default FacardCard;
