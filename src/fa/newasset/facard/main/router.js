import {asyncComponent} from 'nc-lightapp-front';

const List = asyncComponent(() => import(/* webpackChunkName: "fa/newasset/facard/list/list" */ /* webpackMode: "eager" */'../list'));
const Card = asyncComponent(() => import(/* webpackChunkName: "fa/newasset/facard/card/card" */ /* webpackMode:"eager" */ '../card'));
const Msg = asyncComponent(() => import(/* webpackChunkName: "fa/newasset/facard/msg/msg" */ /* webpackMode:"eager" */ '../msg'));

const routes = [
    {
        path: '/',
        component: List,
        exact: true
    },
    {
        path: '/list',
        component: List
    },
    {
        path: '/card',
        component: Card
    },
    {
        path: '/msg',
        component: Msg
    }
];

export default routes;
