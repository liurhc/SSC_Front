import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { msgUtils } = utils;
const { MsgConst, showMessage } = msgUtils;
const { pagecode, bill_type, transi_type, searchAreaId } = pageConfig;
/**
 * 点击查询，获取查询区数据
 * @param {*} props 
 * @param {*} searchVal 
 */
export default function(props, searchVal, type, queryInfo, flag) {
	if (!searchVal) {
		return;
	}
	queryInfo.pageid = pagecode;
	queryInfo.billtype = bill_type;
	queryInfo.transtype = transi_type;
	//初始化条件
	let queryconditions = [];
	if (queryInfo && queryInfo.querycondition && queryInfo.querycondition.conditions) {
		queryconditions = queryInfo.querycondition.conditions;
	}
	let conditions = dealConditions.call(this, props, queryconditions);
	this.setState({ conditions: conditions });
	// 条件查询
	ajax({
		url: pageConfig.url.queryUrl,
		data: queryInfo,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let pks = 0;
				if (data && data > 0) {
					pks = data;
					props.button.setButtonDisabled([ 'Print', 'Output', 'Preview' ], false);
				} else {
					showMessage.call(this, props, { type: MsgConst.Type.QueryNoData });
					props.button.setButtonDisabled([ 'Print', 'Output', 'Preview' ], true);
				}
				this.setState({ pks: pks });
			}
		}
	});
}

export function dealConditions(props, queryconditions) {
	//获取meta
	let meta = props.meta.getMeta();
	let searchAreaItems = meta[searchAreaId].items;
	let labelandfieldmap = {};
	searchAreaItems.map((item) => {
		labelandfieldmap[item.attrcode] = item.label;
	});
	let showconditions = [];
	queryconditions.map((item) => {
		let showconditionsitem = {};
		showconditionsitem.label = labelandfieldmap[item.field];
		switch (item.oprtype) {
			case '=':
				showconditionsitem.display = item.display;
				break;
			case 'between':
				if (item.display && /.+,.+/.test(item.display)) {
					let value = item.display.split(',');
					showconditionsitem.display = value[0] + '~' + value[1];
				} else if (item.value && item.value.firstvalue && item.value.secondvalue) {
					showconditionsitem.display = item.value.firstvalue + '~' + item.value.secondvalue;
				} else {
					showconditionsitem.display = item.display;
				}
				break;
			default:
				if (item.display) {
					showconditionsitem.display = item.display;
				} else {
					showconditionsitem.display = item.value.firstvalue;
				}
				break;
		}
		showconditions.push(showconditionsitem);
	});
	return showconditions;
}
