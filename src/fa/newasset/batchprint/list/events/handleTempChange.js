// 选择打印模板
export default function(value) {
	let { printTemp, selectedTemp } = this.state;
	printTemp.map((item) => {
		if (item.templatepk == value) {
			selectedTemp = item;
		}
	});
	this.setState({ selectedTemp });
}
