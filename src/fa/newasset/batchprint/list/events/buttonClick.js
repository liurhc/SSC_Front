import { toast, print, output, printOnClient, printPreview } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

/**
 * 按钮点击
 * @param {*} props 
 * @param {*} id 
 */
export default function(props, id) {
	switch (id) {
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Preview':
			preview.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		return;
	}
	//插件打印
	printData.type = '1';
	printOnClient(props, printData.printUrl, printData, false);
	// print(
	// 	'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
	// 	printData.printUrl, // 后台打印服务url
	// 	printData
	// );
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let { selectedTemp } = this.state;
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		return;
	}
	let printUrl = pageConfig.url.batchOutPutFacardUrl;
	if (/.*[list|List]$/.test(selectedTemp.nodekey)) {
		printUrl = pageConfig.url.batchOutPutListUrl;
	}
	output({
		url: printUrl,
		data: printData
	});
}

/**
 * 预览
 * @param {*} props 
 */
export function preview(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		return;
	}
	printData.type = '2';
	printData.download = 'preview'; //预览参数
	printData.realData = true; //是否预览真实数据
	delete printData.outputType;
	printPreview(
		props,
		printData.printUrl, // 后台打印服务url
		// 'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		printData,
		false
	);
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let { appcodeFacard, pagecode, bill_type, transi_type, searchAreaId } = pageConfig;
	let { pks, selectedTemp } = this.state;
	if (pks == 0 || pks == '0') {
		toast({
			content: getMultiLangByID('201201516A-000003', {
				/* 国际化处理： 请查询需要**的数据*/
				type:
					outputType == 'print'
						? getMultiLangByID('201201516A-000001') /* 国际化处理： 打印*/
						: getMultiLangByID('201201516A-000002') /* 国际化处理： 输出*/
			}),
			color: 'warning'
		});
		return false;
	}

	if (!selectedTemp || !selectedTemp.code) {
		toast({
			content: getMultiLangByID('201201516A-000008', {
				/* 国际化处理： 未选择需要**的模板*/
				type:
					outputType == 'print'
						? getMultiLangByID('201201516A-000001') /* 国际化处理： 打印*/
						: getMultiLangByID('201201516A-000002') /* 国际化处理： 输出*/
			}),
			color: 'warning'
		});
		return false;
	}

	let queryInfo = props.search.getQueryInfo(searchAreaId, true);
	queryInfo.pagecode = pagecode;
	queryInfo.billtype = bill_type;
	queryInfo.transtype = transi_type;

	let printUrl = pageConfig.url.batchPrintFacardUrl;
	if (/.*[list|List]$/.test(selectedTemp.nodekey)) {
		printUrl = pageConfig.url.batchPrintListUrl;
	}

	let printData = {
		printUrl: printUrl,
		filename: getMultiLangByID(pageConfig.title) /* 国际化处理： 文件名称*/,
		nodekey: selectedTemp.nodekey,
		printTemplateID: selectedTemp.code,
		appcode: appcodeFacard,
		funcode: appcodeFacard,
		//oids: pks, // 需要打印数据主键
		userjson: JSON.stringify(queryInfo), // 查询条件
		outputType: outputType // 输出类型
	};
	return printData;
}
