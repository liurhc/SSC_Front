import { pageConfig } from '../const';
import { dealConditions } from './searchBtnClick';
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter, ReferLinkage } = fa_components;
const { isMultiCorpRefHandler } = ReferFilter;
const { referLinkageClear } = ReferLinkage;
const { searchAreaId } = pageConfig;
/**
 * 查询区编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export default function afterEvent(props, moduleId, key, value, oldValue, val) {
	//联动清除
	referLinkageClear.call(this, props, key);
	let queryInfo = props.search.getQueryInfo(searchAreaId, false);
	let queryconditions = [];
	if (queryInfo && queryInfo.querycondition && queryInfo.querycondition.conditions) {
		queryconditions = queryInfo.querycondition.conditions;
	}
	//组织多选去掉业务单元框  不好使暂时不使用
	isMultiCorpRefHandler.call(this, props, key, value, searchAreaId);
	let conditions = dealConditions.call(this, props, queryconditions);
	props.button.setButtonDisabled([ 'Print', 'Output', 'Preview' ], true);
	this.setState({ conditions: conditions, pks: 0 });
}
