import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

/**
 * 获取全部打印模板
 */
export default function() {
	let { appcodeFacard } = pageConfig;
	let data = {
		appcode: appcodeFacard // 应用编码
	};
	// 查询所有打印模板
	ajax({
		url: pageConfig.url.queryAllPrintTemplateUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let printTemp = [];
				let selectedTemp = {};
				if (data && data.length > 0) {
					data.map((item) => {
						let temp = {};
						temp.code = item.templatecode;
						//模板名称多语处理
						let templatename = switchTemplateName(item.nodekey);
						if (templatename != undefined && templatename != '') {
							temp.name = getMultiLangByID(templatename) /* 国际化处理： 模板名称*/;
						} else {
							temp.name = item.templatename;
						}
						temp.nodekey = item.nodekey;
						temp.templatepk = item.templatepk;
						printTemp.push(temp);
					});
					selectedTemp = printTemp[0];
				}
				this.setState({ printTemp, selectedTemp });
			}
		}
	});
}

function switchTemplateName(nodekey) {
	switch (nodekey) {
		case '201201504A_child':
			return '201201516A-000016';
		case '201201504A_nochild':
			return '201201516A-000017';
		case '201201504A_list':
			return '201201516A-000018';
		default:
			return '';
	}
}
