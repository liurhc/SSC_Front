import { pageConfig } from '../const';
import afterEvent from './afterEvent';
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

/**
 * 模板初始化
 * @param {*} props 
 */
export default function(props) {
	let { pagecode } = pageConfig;

	let defaultConfig = {
		searchId: pageConfig.searchAreaId
	};

	props.createUIDom(
		{
			// 页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					addSearchAreaReferFilter(props, meta, defaultConfig);
					meta[pageConfig.searchAreaId].items.map((item) => {
						if (item.attrcode === 'pk_transitype') {
							item.queryCondition = () => {
								return { parentbilltype: 'H1' };
							};
						}
					});
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						props.button.setButtonDisabled([ 'Print', 'Output','Preview' ], true);
					});
				}
				afterEvent.call(this, props);
			}
		}
	);
}
