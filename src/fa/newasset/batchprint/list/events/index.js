import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import afterEvent from './afterEvent';
import getAllPrintTemplate from './getAllPrintTemplate';
import handleTempChange from './handleTempChange';

export { searchBtnClick, initTemplate, buttonClick, afterEvent, getAllPrintTemplate, handleTempChange };
