// 页面配置
export const pageConfig = {
	// 应用编码
	appcode: '201201516A',
	//固定资产卡片应用编码用于获取打印模板
	appcodeFacard: '201201504A',
	// 应用名称
	title: '201201516A-000000' /* 国际化处理： 批量打印固定资产卡片*/,
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '201201516A_list',
	// 主键字段
	pkField: 'pk_card',
	bill_type: null,
	transi_type: null,
	// 请求链接
	url: {
		queryUrl: '/nccloud/fa/batchprint/queryPrintFacadrdPks.do',
		queryAllPrintTemplateUrl: '/nccloud/fa/batchprint/queryAllPrintTemplate.do',
		batchPrintFacardUrl: '/nccloud/fa/batchprint/printFacard.do',
		batchPrintListUrl: '/nccloud/fa/batchprint/printList.do',
		batchOutPutFacardUrl: '/nccloud/fa/batchprint/outPutFacard.do',
		batchOutPutListUrl: '/nccloud/fa/batchprint/outPutList.do'
	},
	//输出限制
	outputlimit: {
		cardlimit: 5000,
		listlimit: 60000
	}
};
