import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
import { searchBtnClick, initTemplate, buttonClick, afterEvent, getAllPrintTemplate, handleTempChange } from './events';
import './index.less';
import { pageConfig } from './const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;
const { NCRadio } = base;
const { NCRadioGroup } = NCRadio;
const { pagecode } = pageConfig;

/**
 * 主子表列表页面入口文件
 */
class MasterChildList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pks: 0,
			printTemp: [],
			conditions: [],
			selectedTemp: {},
			moreRadio: true
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {
		getAllPrintTemplate.call(this);
	}

	render() {
		const { search, button, ncmodal } = this.props;
		const { createButtonApp } = button;
		const { NCCreateSearch } = search;
		let { title, searchAreaId } = pageConfig;
		const { createModal } = ncmodal;
		let { pks = 0, printTemp = [], conditions = [], selectedTemp = {}, moreRadio } = this.state; // 后台输出服务url，需要输出的数据和url
		let radioContent = (
			<div>
				{printTemp.length > 0 &&
				(printTemp.length <= 3 || (printTemp.length > 3 && !moreRadio)) && (
					<div>
						<NCRadioGroup
							name="printTemp"
							selectedValue={selectedTemp.templatepk}
							onChange={(value) => {
								handleTempChange.call(this, value);
							}}
						>
							{printTemp.map((item) => {
								return <NCRadio value={item.templatepk}>{item.name}</NCRadio>;
							})}
						</NCRadioGroup>
					</div>
				)}
				{printTemp.length > 3 &&
				moreRadio && (
					<div>
						<NCRadioGroup
							name="printTemp"
							selectedValue={selectedTemp.templatepk}
							onChange={(value) => {
								handleTempChange.call(this, value);
							}}
						>
							{printTemp.map((item, index) => {
								if (index < 3) {
									return <NCRadio value={item.templatepk}>{item.name}</NCRadio>;
								}
							})}
						</NCRadioGroup>
						<p
							className="more-bottom"
							onClick={() => {
								this.setState({ moreRadio: false });
							}}
						>
							{getMultiLangByID('201201516A-000012') /* 国际化处理： 更多*/}
						</p>
					</div>
				)}
			</div>
		);

		return (
			<div className="nc-bill-list" id="fa-batchprint">
				{/* 头部 header */}
				<div className="nc-bill-header-area">
					{/* 标题 title */}
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{getMultiLangByID(title) /* 国际化处理： 标题*/}</h2>
					</div>
				</div>
				{/* 查询区 search-area */}
				<div className="nc-bill-search-area card-scope-search">
					<p>{getMultiLangByID('201201516A-000010') /* 国际化处理： 卡片范围选择：*/}</p>
					{NCCreateSearch(searchAreaId, {
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: (key, value) => {
							afterEvent.call(this, this.props, searchAreaId, key, value);
						},
						advSearchClearEve: () => {
							this.props.button.setButtonDisabled([ 'Print', 'Output', 'Preview' ], true);
							this.setState({ conditions: [], pks: 0 });
						}
					})}
				</div>
				<div className="nc-bill-table-area card-style">
					<p>{getMultiLangByID('201201516A-000011') /* 国际化处理： 打印样式选择*/}</p>
					{radioContent}
					{/* <div className="">
						<NCRadioGroup
							name="printTemp"
							selectedValue={selectedTemp.templatepk}
							onChange={(value) => {
								handleTempChange.call(this, value);
							}}
						>
							{printTemp.map((item, index) => {
								if (index < 3) {
									return <NCRadio value={item.templatepk}>{item.name}</NCRadio>;
								}
							})}
							{printTemp.length > 3 &&
							moreRadio && (
								<p
									className="more-bottom"
									onClick={() => {
										this.setState({ moreRadio: false });
									}}
								>
									{getMultiLangByID('201201516A-000012') /* 国际化处理： 更多*/}
					{/*}	</p>
							)}
							{printTemp.length > 3 &&
								!moreRadio &&
								printTemp.map((item, index) => {
									if (index > 2) {
										return <NCRadio value={item.templatepk}>{item.name}</NCRadio>;
									}
								})}
						</NCRadioGroup> 
					</div>*/}
				</div>
				<div className="nc-bill-table-area response-area">
					<div>
						<div className="text">{getMultiLangByID('201201516A-000013') /* 国际化处理： 您选择的卡片范围：*/}</div>
						<div className="selected-area">
							{conditions.length > 0 &&
								conditions.map((item) => {
									return (
										<div className="scope_item">
											<p className="">{item.label}：</p>
											<p className="">{item.display && item.display.replace(/,/g, '，')}</p>
										</div>
									);
								})}
							{pks > 0 && (
								<div>
									<p>
										{getMultiLangByID('201201516A-000014', {
											num: pks
										}) /* 国际化处理： 共计{pks}张*/}
									</p>
								</div>
							)}
						</div>
						<div className="text">{getMultiLangByID('201201516A-000015') /* 国际化处理： 您选择的打印样式:*/}</div>
						<div className="selected-area">
							{selectedTemp.name && (
								<div className="scope_item">
									<p className="">{selectedTemp.name}</p>
								</div>
							)}
						</div>
						<div className="btn-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.bottom-button-area')
							})}
						</div>
					</div>
				</div>
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'info' })}
			</div>
		);
	}
}

const MasterChildListBase = createPage({})(MasterChildList);

initMultiLangByModule({ fa: [ '201201516A', 'facommon' ], ampub: [ 'common' ] }, () => {
	// 渲染页面
	ReactDOM.render(<MasterChildListBase />, document.querySelector('#app'));
});
