import { asyncComponent } from 'nc-lightapp-front';

const Source = asyncComponent(() =>
	import(/* webpackChunkName: "fa/newasset/assettransfer/source/source" */ /* webpackMode: "eager" */ '../source')
);

const Dest = asyncComponent(() =>
	import(/* webpackChunkName: "fa/newasset/assettransfer/dest/dest" */ /* webpackMode: "eager" */ '../dest')
);

const routes = [
	{
		path: '/',
		component: Source,
		exact: true
	},
	{
		path: '/source',
		component: Source,
		exact: true
	},
	{
		path: '/dest',
		component: Dest,
		exact: true
	}
];

export default routes;
