import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, ajax, createPageIcon } from 'nc-lightapp-front';
import translateVO from './utils/translateVO';
const { NCBackBtn, NCToggleViewBtn } = base;
import translateData from './utils/translateData';
import {
	initNewAssetTemplate,
	initEquipTemplate,
	initEquipMainTemplate,
	initPurTemplate,
	initPurMainTemplate,
	initProjectTemplate,
	initProMainTemplate,
	initProfitTemplate,
	initAllTemplate,
	initAlllMainTemplate,
	initNewAssetMainTemlate,
	initProfitMainTemplate,
	purAfterEvent,
	proAfterEvent,
	afterEvent
} from './events';
import {
	assetTransferSearch,
	assetTransferHead,
	assetTransferBody,
	assetMainTransferPageCode,
	equipTransferHead,
	equipTransferPageCode,
	equipTransferSearch,
	equipMainTransferPageCode,
	purTransferHead,
	purTransferBody,
	purTransferSearch,
	purMainTransferPageCode,
	proTransferHead,
	proTransferBody,
	proTransferSearch,
	profitTransferHead,
	profitTransferBody,
	profitMainTransferPageCode,
	proMainTransferPageCode,
	profitTransferSearch,
	allTransferHead,
	allTransferBody,
	allTransferSearch,
	allMainTransferPageCode,
	KeyMap,
	dataSource
} from './constants';

import ampub from 'ampub';

const {components, utils} = ampub;

const { getMultiLangByID } = utils.multiLangUtils;
const { setContext } = components.LoginContext;

import {
	purchase_btnClick,
	project_btnClick,
	selected_btnClick,
	all_btnClick,
	profit_btnClick,
	newasset_btnClick,
	equip_btnClick
} from './btnClick';
import  './index.less';

class AssetTransfer extends Component {
	constructor(props) {
		super(props);

		this.state = {
            pro_cardStyle:'0',
            pur_cardStyle:'0'
		}
	}
	componentDidMount() {}

	render() {
		const { transferTable, search, button } = this.props;
		const { NCCreateSearch } = search;
		const { createButtonApp } = button;
		const { createMultiTransferTable } = transferTable;

		let selectedShow = transferTable.getSelectedListDisplay('all');
		return (
			<div id="transferList" className="nc-bill-list">
				{!selectedShow ? (
					<div className="nc-bill-header-area">
						{!this.isRefAddLine && this.props.getUrlParam('from') ? (
							<NCBackBtn
								onClick={() => {
									//this.backClick(this.props);
								}}
							/>
						) : (
							''
						)}
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID('201201512A-000047')}</h2>
						</div>
						<div className="header-button-area">
							{/* 列设置 平台去掉了 */}
							{/* <NCSetColBtn onClick={this.handleClick} /> */}
							<NCToggleViewBtn
								expand={false}
								onClick={() => {
									if (!this.props.meta.getMeta()[purMainTransferPageCode]) {
										initPurMainTemplate(this.props); //加载主子拉平模板
									}
									if (!this.props.meta.getMeta()[proMainTransferPageCode]) {
										initProMainTemplate(this.props); //加载主子拉平模板
									}
									if (!this.props.meta.getMeta()[allMainTransferPageCode]) {
										initAlllMainTemplate(this.props); //加载主子拉平模板
									}
									if (!this.props.meta.getMeta()[assetMainTransferPageCode]) {
										initNewAssetMainTemlate(this.props); //加载主子拉平模板
									}
									if (!this.props.meta.getMeta()[equipMainTransferPageCode]) {
										initEquipMainTemplate(this.props); //加载主子拉平模板
									}
									if (!this.props.meta.getMeta()[profitMainTransferPageCode]) {
										initProfitMainTemplate(this.props); //加载主子拉平模板
									}
									this.props.transferTable.changeViewType();
								}}
							/>
						</div>
					</div>
				) : (
					''
				)}
				<div>
					{/* 创建多来源转单 */}
					{createMultiTransferTable(
						{
							dataSource: dataSource,
							onTabClick: (key) => {
								//点击页签的钩子函数

								//拿到当前页签的headTableId给转单页面使用
								switch (key) {
									case '1':
										this.curheadTableId = 'pk_newasset'; //记录主表id，供下游转单使用
										if (!this.props.meta.getMeta()[assetTransferHead]) {
											initNewAssetTemplate.call(this, this.props);
										}
										break;
									case '2':
										this.curheadTableId = 'pk_equip'; //记录主表id，供下游转单使用
										if (!this.props.meta.getMeta()[equipTransferHead]) {
											initEquipTemplate.call(this, this.props);
										}
										break;
									case '3':
										this.curheadTableId = 'pk_project'; //记录主表id，供下游转单使用
										if (!this.props.meta.getMeta()[purTransferHead]) {
											initPurTemplate.call(this, this.props);
										}
										break;
									case '4':
										this.curheadTableId = 'pk_project'; //记录主表id，供下游转单使用
										if (!this.props.meta.getMeta()[proTransferHead]) {
											initProjectTemplate.call(this, this.props);
										}
										break;
									case '5':
										this.curheadTableId = 'pk_profit'; //记录主表id，供下游转单使用
										if (!this.props.meta.getMeta()[profitTransferHead]) {
											initProfitTemplate.call(this, this.props);
										}
										break;
									default:
										break;
								}
							},
							showAll: true, //是否显示全部页签，不显示全部页签时不需要设置 默认为false
							//==========以下参数必须设置showAll为true时才生效==========
							allHeadId: 'all_head', //全部页签的主表id
							allBodyId: 'all_body', //全部页签的子表id
							allFullTableId: allMainTransferPageCode,
							headPkIds: [ 'pk_newasset', 'pk_equip', 'pk_transasset', 'pk_profit' ],
							bodyPkIds: [ 'pk_newasset_b', 'pk_equip', 'pk_transasset_b', 'pk_profit_b' ],
							transferBtnText: getMultiLangByID('201201512A-000044'), //转单按钮显示文字
							containerSelector: '#transferList', //容器的选择器 必须唯一,用于设置底部已选区域宽度

							onTransferBtnClick: () => {
								//点击转单按钮钩子函数
								//将已选数据转化为转单后台所需结构
								let selectdata = this.props.transferTable.getTransferTableSelectedValue();
								//全选
								let destAllDatas = selectdata[allTransferHead];
								let other = [];
								//新增资产审批单
								let newasset_billtype = {
									bill_type: { value: 'HN', display: getMultiLangByID('201201512A-000033') }
								};
								let destNewAssetDatas = translateData(
									selectdata[assetTransferHead],
									assetTransferHead,
									assetTransferBody,
									allTransferHead,
									allTransferBody,
									KeyMap.KeyMapNewToAll,
									newasset_billtype
								);
								other.push(...destNewAssetDatas);
								//设备卡片
								let equip_billtype = {
									bill_type: { value: '4A00', display: getMultiLangByID('201201512A-000032') }
								};
								let destEquipDatas = translateData(
									selectdata[equipTransferHead],
									equipTransferHead,
									null,
									allTransferHead,
									allTransferBody,
									KeyMap.KeyMapEquipToAll,
									equip_billtype
								);
								other.push(...destEquipDatas);
								//采购转固单
								let purject_billtype = {
									bill_type: { value: 'HJ-02', display: getMultiLangByID('201201512A-000034') }
								};
								let destPurjectDatas = translateData(
									selectdata[purTransferHead],
									purTransferHead,
									purTransferBody,
									allTransferHead,
									allTransferBody,
									KeyMap.KeyMapPurToAll,
									purject_billtype
								);
								other.push(...destPurjectDatas);
								//工程转固单
								let project_billtype = {
									bill_type: { value: 'HJ-01', display: getMultiLangByID('201201512A-000035') }
								};
								let destProjectDatas = translateData(
									selectdata[proTransferHead],
									proTransferHead,
									proTransferBody,
									allTransferHead,
									allTransferBody,
									KeyMap.KeyMapProToAll,
									project_billtype
								);
								other.push(...destProjectDatas);
								//盘盈
								let profit_billtype = {
									bill_type: { value: 'HS', display: getMultiLangByID('201201512A-000038') }
								};
								let destProfitDatas = translateData(
									selectdata[profitTransferHead],
									profitTransferHead,
									profitTransferBody,
									allTransferHead,
									allTransferBody,
									KeyMap.KeyMapProfitToAll,
									profit_billtype
								);
								other.push(...destProfitDatas);

								//过滤全部页签和其他页签重复数据
								let newDestAllDatas = destAllDatas.filter((item) => {
									let key = item.head[allTransferHead].rows[0].values.pk.value;
									let flag = true;
									other.map((otherItem) => {
										let otherKey = otherItem.head[allTransferHead].rows[0].values.pk.value;
										if (otherKey === key) {
											flag = false;
											let bodyRows = item.body[allTransferBody].rows;
											let newBodyRows = [];
											for (let i = 0; i < bodyRows.length; i++) {
												let bodyflag = true;
												for (let j = 0; j < otherItem.body[allTransferBody].rows.length; j++) {
													let otherbodykey =
														otherItem.body[allTransferBody].rows[i].values.pk_b.value;
													if (otherbodykey == bodyRows[i].values.pk_b.value) {
														bodyflag = false;
													}
												}
												if (bodyflag) {
													newBodyRows.push(bodyRows[i]);
												}
											}
											otherItem.body[allTransferBody].rows.push(...newBodyRows);
										}
									});
									return flag;
								});
								//将过滤后的全部页签数据重新和其他页签数据组装组装
								let finalData = [];
								if (newDestAllDatas) {
									finalData.push(...newDestAllDatas);
								}
								finalData.push(...other);
								//将前台数据转为后台所需数据
								let param = translateVO(
									finalData,
									allTransferHead,
									allTransferBody,
									this.state.pur_cardStyle,
									this.state.pro_cardStyle
								);
								//处理转固单建卡方式
								let proArray = [];
								let purArray = [];
								param.map((item) => {
									if (item.sourceBillType === 'HJ-01') {
										proArray.push(item.sourceHeadPk);
									}
									if (item.sourceBillType === 'HJ-02') {
										purArray.push(item.sourceHeadPk);
									}
								});

								let style = {};
								if (this.state.pro_cardStyle === this.state.pur_cardStyle) {
									proArray.push(...purArray);
									purArray = [];
								}
								if (proArray.length > 0) {
									style[this.state.pro_cardStyle] = proArray;
								}
								if (purArray.length > 0) {
									style[this.state.pur_cardStyle] = purArray;
								}

								if (Object.keys(style).length > 0) {
									let styleParam = {
										msgMap: style
									};
									ajax({
										url: '/nccloud/fa/transasset/createCardStyle.do',
										data: styleParam,
										success: (res) => {
											setContext.call(
												this,
												'multiSource_for_facard',
												JSON.stringify(param),
												dataSource
											);

											//点击转单按钮钩子函数
											this.props.pushTo('/dest', {
												pagecode: '201201512A_card'
											});
										}
									});
								} else {
									setContext.call(this, 'multiSource_for_facard', JSON.stringify(param), dataSource);

									//点击转单按钮钩子函数
									this.props.pushTo('/dest', {
										pagecode: '201201512A_card'
									});
								}
							},
							//查看已选
							onSelectedBtnClick: selected_btnClick.bind(this),
							onChangeViewClick: () => {
								//点击切换视图钩子函数
								if (!this.props.meta.getMeta()[allMainTransferPageCode]) {
									initAlllMainTemplate(this.props); //加载主子拉平模板
								}
								this.props.transferTable.changeViewType(allTransferHead);
							},
							selectArea: () => {
								return <span />;
							},

							onClearAll: () => {},
							onSelectedItemRemove: (record, bodys) => {}
						},
						[
							{
								tabName: getMultiLangByID('201201512A-000045'), //tab页签显示文字【全部】
								headTableId: allTransferHead, //表格组件id
								bodyTableId: allTransferBody,
								fullTableId: allMainTransferPageCode,
								searchArea: () => {
									//查询区域render
									return NCCreateSearch(allTransferSearch, {
										clickSearchBtn: all_btnClick.bind(this, this.isRefAddLine, this.billvo)
									});
								},
								onCheckedChange: (flag, record, index, bodys) => {},
								onChangeViewClick: () => {
									//点击切换视图钩子函数
									if (!this.props.meta.getMeta()[allMainTransferPageCode]) {
										initAlllMainTemplate(this.props); //加载主子拉平模板
									}
									this.props.transferTable.changeViewType(allTransferHead);
								}
							},
							{
								tabName: getMultiLangByID('201201512A-000033'), //tab页签显示文字【新增资产审批单】
								headTableId: assetTransferHead, //表格组件id
								bodyTableId: assetTransferBody, //子表模板id
								fullTableId: assetMainTransferPageCode,
								searchArea: () => {
									//查询区域render
									return NCCreateSearch(assetTransferSearch, {
										onAfterEvent: afterEvent.bind(this),
										clickSearchBtn: newasset_btnClick.bind(this, this.isRefAddLine, this.billvo)
									});
								},
								onCheckedChange: (flag, record, index, bodys) => {},
								onChangeViewClick: () => {
									//点击切换视图钩子函数
									if (!this.props.meta.getMeta()[assetMainTransferPageCode]) {
										initNewAssetMainTemlate(this.props); //加载主子拉平模板
									}
									this.props.transferTable.changeViewType(assetTransferHead);
								}
							},
							{
								tabName: getMultiLangByID('201201512A-000046'), //tab页签显示文字【资产卡片】
								headTableId: equipTransferHead, //表格组件id
								// bodyTableId: equipTransferBody, //表格组件id
								fullTableId: equipMainTransferPageCode,
								searchArea: () => {
									//查询区域render;
									return NCCreateSearch(equipTransferSearch, {
										clickSearchBtn: equip_btnClick.bind(this, this.isRefAddLine, this.billvo)
									});
								},
								onCheckedChange: (flag, record, index, bodys) => {},
								onChangeViewClick: () => {
									//点击切换视图钩子函数
									if (!this.props.meta.getMeta()[equipMainTransferPageCode]) {
										initEquipTemplate(this.props); //加载主子拉平模板
									}
									this.props.transferTable.changeViewType(equipTransferHead);
								}
							},
							{
								tabName: getMultiLangByID('201201512A-000034'), //tab页签显示文字【采购转固单】
								headTableId: purTransferHead, //表格组件id
								bodyTableId: purTransferBody,
								fullTableId: purMainTransferPageCode,
								searchArea: () => {
									//查询区域render
									return (
										<div>
											<div>
												{NCCreateSearch(purTransferSearch, {
													onAfterEvent: purAfterEvent.bind(this),
													clickSearchBtn: purchase_btnClick.bind(
														this,
														this.isRefAddLine,
														this.billvo
													)
												})}
											</div>
											<br />
											<div className='fa-transfer-createStyle'>
												{getMultiLangByID('201201512A-000048')}：
                                                <span>
													<input
                                                        type="radio"
                                                        name="pur"
                                                        checked={this.state.pur_cardStyle === '0'}
                                                        onClick={() => {
                                                            this.setState({
                                                                pur_cardStyle: '0'
                                                            });
                                                        }}
                                                    />
                                                    {getMultiLangByID('201201512A-000049')}
												</span>
                                                <span>
													<input
                                                        type="radio"
                                                        name="pur"
                                                        checked={this.state.pur_cardStyle === '2'}
                                                        onClick={() => {
                                                            this.setState({
                                                                pur_cardStyle: '2'
                                                            });
                                                        }}
                                                    />
                                                    {getMultiLangByID('201201512A-000050')}
												</span>
                                                <span>
													<input
                                                        type="radio"
                                                        name="pur"
                                                        checked={this.state.pur_cardStyle === '3'}
                                                        onClick={() => {
                                                            this.setState({
                                                                pur_cardStyle: '3'
                                                            });
                                                        }}
                                                    />
                                                    {getMultiLangByID('201201512A-000051')}
												</span>
											</div>
											<br />
										</div>
									);
								},
								onCheckedChange: (flag, record, index, bodys) => {},
								onChangeViewClick: () => {
									//点击切换视图钩子函数
									if (!this.props.meta.getMeta()[purMainTransferPageCode]) {
										initPurMainTemplate(this.props); //加载主子拉平模板
									}
									this.props.transferTable.changeViewType(purTransferHead);
								}
							},
							{
								tabName: getMultiLangByID('201201512A-000035'), //tab页签显示文字 【工程转固单】
								headTableId: proTransferHead, //表格组件id
								bodyTableId: proTransferBody,
								fullTableId: proMainTransferPageCode,
								searchArea: () => {
									//查询区域render
									return (
										<div>
											<div>
												{NCCreateSearch(proTransferSearch, {
													onAfterEvent: proAfterEvent.bind(this),
													clickSearchBtn: project_btnClick.bind(
														this,
														this.isRefAddLine,
														this.billvo
													)
												})}
											</div>
											<br />
											<div className='fa-transfer-createStyle'>
												{getMultiLangByID('201201512A-000048')}：
												<span>
													<input
														type="radio"
														name="pro"
														checked={this.state.pro_cardStyle === '0'}
														onClick={() => {
                                                            this.setState({
                                                                pro_cardStyle: '0'
                                                            });
														}}
													/>
													{getMultiLangByID('201201512A-000049')}
												</span>

												<span>
                                                    <input
                                                        type="radio"
                                                        name="pro"
                                                        checked={this.state.pro_cardStyle === '1'}
                                                        onClick={() => {
                                                            this.setState({
                                                                pro_cardStyle: '1'
                                                            });
                                                        }}
                                                    />
													{getMultiLangByID('201201512A-000052')}
												</span>

												<span>
                                                    <input
                                                        type="radio"
                                                        name="pro"
                                                        checked={this.state.pro_cardStyle === '2'}
                                                        onClick={() => {
                                                            this.setState({
                                                                pro_cardStyle: '2'
                                                            });
                                                        }}
                                                    />
                                                    {getMultiLangByID('201201512A-000050')}
												</span>

											</div>
											<br />
										</div>
									);
								},
								onCheckedChange: (flag, record, index, bodys) => {},
								onChangeViewClick: () => {
									//点击切换视图钩子函数
									if (!this.props.meta.getMeta()[proMainTransferPageCode]) {
										initProMainTemplate(this.props); //加载主子拉平模板
									}
									this.props.transferTable.changeViewType(proTransferHead);
								}
							},
							{
								tabName: getMultiLangByID('201201512A-000038'), //tab页签显示文字 【盘盈】
								headTableId: profitTransferHead, //表格组件id
								bodyTableId: profitTransferBody,
								fullTableId: profitMainTransferPageCode,
								searchArea: () => {
									//查询区域render
									return NCCreateSearch(profitTransferSearch, {
										onAfterEvent: afterEvent.bind(this),
										clickSearchBtn: profit_btnClick.bind(this, this.isRefAddLine, this.billvo)
									});
								},
								onCheckedChange: (flag, record, index, bodys) => {},
								onChangeViewClick: () => {
									//点击切换视图钩子函数
									if (!this.props.meta.getMeta()[profitMainTransferPageCode]) {
										initProfitMainTemplate(this.props); //加载主子拉平模板
									}
									this.props.transferTable.changeViewType(profitTransferHead);
								}
							}
						]
					)}
				</div>
			</div>
		);
	}
}

AssetTransfer = createPage({
	initTemplate: initAllTemplate
})(AssetTransfer);

export default AssetTransfer;
