export default function(selectData, headCode, bodyCode, pur_cardStyle, pro_cardStyle) {
	let transData = selectData.map((item) => {
		let bodys = item.body[bodyCode].rows;
		let bodyPks = bodys.map((item) => {
			return item.values.pk_b.value;
		});
		let cardStyle = null;
		if (item.head[headCode].rows[0].values.bill_type.value == 'HJ-01') {
			cardStyle = pro_cardStyle;
		} else if (item.head[headCode].rows[0].values.bill_type.value == 'HJ-02') {
			cardStyle = pur_cardStyle;
		}

		return {
			sourceHeadPk: item.head[headCode].rows[0].values.pk.value,
			sourceBodyPks: bodyPks,
			sourceBillType: item.head[headCode].rows[0].values.bill_type.value,
			createCardStyle: cardStyle
		};
	});
	return transData;
}
