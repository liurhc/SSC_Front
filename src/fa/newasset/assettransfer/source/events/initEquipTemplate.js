import { equipTransferPageCode, equipAppcode, equipTransferHead, equipTransferSearch } from '../constants';
import { linkCard } from './buttonClick';
import ampub from 'ampub';
import fa from 'fa';

const {utils} = ampub;
const {fa_components} = fa;

const { addSearchAreaReferFilter } = fa_components.ReferFilter;
const { getMultiLangByID } = utils.multiLangUtils;

export default function(props) {
	props.createUIDom(
		{
			pagecode: equipTransferPageCode,
			appcode: equipAppcode
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	let ListConfig = {
		searchId: equipTransferSearch,
		specialFields: {
			pk_org_fi: {
				RefActionExtType: 'TreeRefActionExt', //参照类型 tree/grid
				class: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' //后台过滤类
			}
		}
	};
	// 查询条件过滤
	addSearchAreaReferFilter.call(this, props, meta, ListConfig);

	meta[equipTransferSearch].items.map((item, key) => {
		if (item.attrcode == 'pk_ownerorg') {
			item.queryCondition = () => {
				let fieldValue = props.search.getSearchValByField(equipTransferSearch, 'pk_org_fi');
				let pk_org_fi = null;
				if (fieldValue && fieldValue.value && fieldValue.value.firstvalue) {
					pk_org_fi = fieldValue.value.firstvalue;
				}
				return {
					pk_org_fi: pk_org_fi,
					GridRefActionExt: 'nccloud.web.fa.newasset.assettransfer.refCondition.TransferOrgRefFilter'
				};
			};
		}
	});

	//增加超链接
	meta[equipTransferHead].items = meta[equipTransferHead].items.map((item, key) => {
		if (item.attrcode == 'equip_code') {
			item.render = (text, record, index) => {
				let transType = record.transi_type.value;
				return (
					<div class="simple-table-td" field="equip_code" fieldname={getMultiLangByID('201201512A-000040')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkCard.call(this, props, record, 'pk_equip', transType);
							}}
						>
							{record && record.equip_code && record.equip_code.value}
						</span>
					</div>
				);
			};
		}

		return item;
	});
	return meta;
}
