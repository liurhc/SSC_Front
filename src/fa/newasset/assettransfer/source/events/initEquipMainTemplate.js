import { equipMainTransferPageCode, equipAppcode } from '../constants';
import { linkCard } from './buttonClick';
import ampub from 'ampub';

const {utils} = ampub;

const { getMultiLangByID } = utils.multiLangUtils;

export default function(props) {
	props.createUIDom(
		{
			pagecode: equipMainTransferPageCode,
			appcode: equipAppcode
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	//增加超链接
	meta[equipMainTransferPageCode].items = meta[equipMainTransferPageCode].items.map((item, key) => {
		if (item.attrcode == 'equip_code') {
			item.render = (text, record, index) => {
				let transType = record.transi_type.value;
				return (
					<div class="simple-table-td" field="equip_code" fieldname={getMultiLangByID('201201512A-000040')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkCard.call(this, props, record, 'pk_equip', transType);
							}}
						>
							{record && record.equip_code && record.equip_code.value}
						</span>
					</div>
				);
			};
		}

		return item;
	});
	return meta;
}
