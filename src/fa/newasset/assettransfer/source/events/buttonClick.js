import { queryUrl, queryProfitUrl } from '../constants';
import { ajax, toast } from 'nc-lightapp-front';

export function linkCard(props, record, pkFiled, transtype) {
	let param = {
		id: record[pkFiled].value,
		transtype: transtype
	};
	let url = queryUrl;
	if (transtype === 'HS-01' || transtype === 'HS') {
		let code = record[pkFiled].value + '<:>' + record['bill_code'].value;
		param['id'] = code;
		param['transtype'] = 'HP-01';
		url = queryProfitUrl;
	}
	ajax({
		url: url,
		data: param,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					if (transtype === 'HS-01' || transtype === 'HS') {
						data['status'] = 'browse';
						data['current'] = '7';
					}
					props.openTo(data['url'], data);
				}
			}
		},
		error: (res) => {}
	});
}
