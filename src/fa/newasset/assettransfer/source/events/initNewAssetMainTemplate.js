import { assetAppcode, assetMainTransferPageCode } from '../constants';
import { linkCard } from './buttonClick';
import ampub from 'ampub';

const {utils} = ampub;

const { getMultiLangByID } = utils.multiLangUtils;

export default function(props) {
	props.createUIDom(
		{
			pagecode: assetMainTransferPageCode,
			appcode: assetAppcode
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}
function modifierMeta(props, meta) {
	//增加超链接
	meta[assetMainTransferPageCode].items = meta[assetMainTransferPageCode].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				let transType = record.transi_type.value;
				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('201201512A-000041')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkCard.call(this, props, record, 'pk_newasset', transType);
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}

		return item;
	});
	return meta;
}
