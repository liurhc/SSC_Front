import { profitMainTransferPageCode, profitAppcode } from '../constants';
import { viewResult } from '../../../../inventory/orginventory/list/events';

import ampub from 'ampub';

const {utils} = ampub;

const { getMultiLangByID } = utils.multiLangUtils;

export default function(props) {
	props.createUIDom(
		{
			pagecode: profitMainTransferPageCode,
			appcode: profitAppcode
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}
function modifierMeta(props, meta) {
	//增加超链接
	meta[profitMainTransferPageCode].items = meta[profitMainTransferPageCode].items.map((item, key) => {
		if (item.attrcode == 'pk_inventory') {
			item.render = (text, record, index) => {
				let transType = record.transi_type.value;
				return (
					<div class="simple-table-td" field="pk_inventory" fieldname={getMultiLangByID('201201512A-000042')}>
						<span
							className="code-detail-link"
							onClick={() => {
								viewResult.call(this, props, 'pk_profit', record);
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}

		return item;
	});
	return meta;
}
