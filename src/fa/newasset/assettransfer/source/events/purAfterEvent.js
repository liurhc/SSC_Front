import { purTransferSearch } from '../constants';

import fa from 'fa';

const {fa_components} = fa;

const { referLinkageClear } = fa_components.ReferLinkage;

const defaultConfig = {
	//查询区
	searchId: purTransferSearch,
	linkageData: {
		//联动链
		//参照字段:['被过滤字段1'，'被过滤字段2']
		pk_org: [
			'pk_recorder',
			'bodyvos.pk_usedept', //使用部门
			'bodyvos.pk_mandept', //管理部门
			'bodyvos.pk_asset_user' //使用人
		]
	}
};
export default function purAfterEvent(field, val) {
	if (field === 'pk_org') {
		referLinkageClear.call(this, this.props, field, defaultConfig);
	}
}
