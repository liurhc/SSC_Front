/**
 * 编辑后事件，用于关联查询区条件
 * props: props
 * searchId: 查询区需要的searchId参数
 * field: 编辑后的key
 * val: 编辑后的value
*/
import fa from 'fa';

const {fa_components} = fa;

const { referLinkageClear } = fa_components.ReferLinkage;

import { assetTransferSearch, profitTransferSearch } from '../constants';

export default function(field, val, searchId) {
	let queryConfig = {
		searchId: searchId
	};
	if (searchId === assetTransferSearch) {
		queryConfig['linkageData'] = {
			//联动链
			//参照字段:['被过滤字段1'，'被过滤字段2']
			pk_org: [
				'pk_applyorg', //申请单位
				'pk_proposer', //申请人
				'pk_transactor', //经办人
				'pk_applydapt', //申请部门
				'bodyvos.pk_usedept', //使用部门
				'bodyvos.pk_mandept', //管理部门
				'bodyvos.pk_category', //资产类别
				'bodyvos.pk_card', //资产卡片
				'bodyvos.pk_assetuser' //使用人
			],
			pk_applyorg: [ 'pk_applydapt', 'pk_proposer' ]
		};
	} else if (searchId === profitTransferSearch) {
		queryConfig['linkageData'] = {
			pk_org: [
				'bodyvos.pk_usedept', //使用部门
				'bodyvos.pk_mandept', //管理部门
				'bodyvos.pk_assetuser' //使用人
			]
		};
	}
	if (field === 'pk_org') {
		referLinkageClear.call(this, this.props, field, queryConfig);
	}
}
