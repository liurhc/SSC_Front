import { purTransferPageCode, purAppcode, purTransferSearch, purTransferHead } from '../constants';
import { linkCard } from './buttonClick';
import ampub from 'ampub';
import fa from 'fa';

const {utils, commonConst} = ampub;
const {fa_components} = fa;

const { addSearchAreaReferFilter } = fa_components.ReferFilter;
const { getMultiLangByID } = utils.multiLangUtils;
const { IBusiRoleConst } = commonConst.CommonKeys;

export default function(props) {
	props.createUIDom(
		{
			pagecode: purTransferPageCode,
			appcode: purAppcode
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}
function modifierMeta(props, meta) {
	let referConfig = {
		searchId: purTransferSearch,
		specialFields: {
			pk_asset_user: {
				//使用人
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_equiporg', 'pk_org' ],
						returnName: 'pk_org'
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			}
		}
	};
	// 查询条件过滤
	addSearchAreaReferFilter.call(this, props, meta, referConfig);

	//增加超链接
	meta[purTransferHead].items = meta[purTransferHead].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				let transType = record.transi_type.value;
				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('201201512A-000043')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkCard.call(this, props, record, 'pk_transasset', transType);
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}

		return item;
	});
	return meta;
}
