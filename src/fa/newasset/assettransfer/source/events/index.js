import initNewAssetTemplate from './initNewAssetTemplate';
import initEquipTemplate from './initEquipTemplate';
import initEquipMainTemplate from './initEquipMainTemplate';
import initPurTemplate from './initPurTemplate';
import initProjectTemplate from './initProjectTemplate';
import initProfitTemplate from './initProfitTemplate';
import initAllTemplate from './initAllTemplate';
import initAlllMainTemplate from './initAllMainTemplate';
import initPurMainTemplate from './initPurMainTemplate';
import initProMainTemplate from './initProjectMailTemplate';
import initNewAssetMainTemlate from './initNewAssetMainTemplate';
import initProfitMainTemplate from './initProfitMainTemplate';
import purAfterEvent from './purAfterEvent';
import proAfterEvent from './proAfterEvent';
import afterEvent from './afterEvent';

export {
	initNewAssetTemplate,
	initEquipTemplate,
	initEquipMainTemplate,
	initProfitTemplate,
	initPurTemplate,
	initProjectTemplate,
	initAllTemplate,
	initPurMainTemplate,
	initProMainTemplate,
	initAlllMainTemplate,
	initNewAssetMainTemlate,
	initProfitMainTemplate,
	purAfterEvent,
	proAfterEvent,
	afterEvent
};
