import { allTransferPageCode, allTransferSearch, allTransferHead } from '../constants';
import { linkCard } from './buttonClick';

import ampub from 'ampub';
import fa from 'fa';

const {utils} = ampub;
const {fa_components} = fa;

const { addSearchAreaReferFilter } = fa_components.ReferFilter;
const { getMultiLangByID } = utils.multiLangUtils;

export default function(props) {
	props.createUIDom(
		{
			pagecode: allTransferPageCode //卡片页面编码
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.addMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	let ListConfig = {
		searchId: allTransferSearch
	};
	// 查询条件过滤
	addSearchAreaReferFilter.call(this, props, meta, ListConfig);

	//增加超链接
	meta[allTransferHead].items = meta[allTransferHead].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				let transType = record.bill_type.value;
				if (transType === 'HN') {
					transType = 'HN-01';
				} else if (transType === '4A00') {
					transType = '4A00-01';
				}

				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('201201512A-000039')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkCard.call(this, props, record, 'pk', transType);
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}

		return item;
	});
}
