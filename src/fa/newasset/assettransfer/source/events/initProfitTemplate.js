import { profitTransferPageCode, profitAppcode, profitTransferSearch, profitTransferHead } from '../constants';
import { linkCard } from './buttonClick';

import ampub from 'ampub';
import fa from 'fa';

const {utils} = ampub;
const {fa_components} = fa;

const { addSearchAreaReferFilter } = fa_components.ReferFilter;
const { getMultiLangByID } = utils.multiLangUtils;

export default function(props) {
	props.createUIDom(
		{
			pagecode: profitTransferPageCode,
			appcode: profitAppcode
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}
function modifierMeta(props, meta) {
	let referConfig = {
		searchId: profitTransferSearch
	};
	// 查询条件过滤
	addSearchAreaReferFilter.call(this, props, meta, referConfig);

	meta[profitTransferSearch].items.map((item) => {
		if (item.attrcode == 'bodyvos.pk_category') {
			item.onlyLeafCanSelect = false;
		}
	});

	//增加超链接
	meta[profitTransferHead].items = meta[profitTransferHead].items.map((item, key) => {
		if (item.attrcode == 'pk_inventory') {
			item.render = (text, record, index) => {
				let transType = record.transi_type.value;
				return (
					<div
						className="simple-table-td"
						field="pk_inventory"
						fieldname={getMultiLangByID('201201512A-000042')}
					>
						<span
							className="code-detail-link"
							onClick={() => {
								linkCard.call(this, props, record, 'pk_profit', transType);
							}}
						>
							{record && record.pk_inventory && record.pk_inventory.value}
						</span>
					</div>
				);
			};
		}

		return item;
	});
	return meta;
}
