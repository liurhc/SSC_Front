import { allMainTransferPageCode } from '../constants';
import { linkCard } from './buttonClick';

import ampub from 'ampub';
const {utils} = ampub;

const {getMultiLangByID} = utils.multiLangUtils;

export default function(props) {
	props.createUIDom(
		{
			pagecode: allMainTransferPageCode //卡片页面编码
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	//增加超链接
	meta[allMainTransferPageCode].items = meta[allMainTransferPageCode].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				let transType = record.bill_type.value;
				if (transType === 'HN') {
					transType = 'HN-01';
				} else if (transType === '4A00') {
					transType = '4A00-01';
				}

				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('201201512A-000039')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkCard.call(this, props, record, 'pk', transType);
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}

		return item;
	});
	return meta;
}
