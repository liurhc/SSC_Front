import { assetAppcode, assetTransferPageCode, assetTransferSearch, assetTransferHead } from '../constants';
import { linkCard } from './buttonClick';

import ampub from 'ampub';
import fa from 'fa';

const {utils} = ampub;
const {fa_components} = fa;

const { addSearchAreaReferFilter } = fa_components.ReferFilter;
const { getMultiLangByID } = utils.multiLangUtils;

export default function(props) {
	props.createUIDom(
		{
			pagecode: assetTransferPageCode,
			appcode: assetAppcode
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}
function modifierMeta(props, meta) {
	let ListConfig = {
		searchId: assetTransferSearch,
		specialFields: {
			pk_applyorg: {
				// 申请单位
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			},
			pk_applydapt: {
				orgMulti: 'pk_org',
				// 申请部门
				data: [
					{
						fields: [ 'pk_applyorg', 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			},
			pk_proposer: {
				// 申请人
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					},
					{
						fields: [ 'pk_applydapt', 'pk_applydapt_v' ],
						returnName: 'pk_dept'
					}
				]
			},
			pk_transactor: {
				// 经办人
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			}
		}
	};
	// 查询条件过滤
	addSearchAreaReferFilter.call(this, props, meta, ListConfig);

	//增加超链接
	meta[assetTransferHead].items = meta[assetTransferHead].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				let transType = record.transi_type.value;
				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('201201512A-000041')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkCard.call(this, props, record, 'pk_newasset', transType);
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}

		return item;
	});
	return meta;
}
