export const dataSource = 'fa.newasset.assettransfer.main';
export const queryUrl = '/nccloud/ampub/common/amLinkQuery.do';
export const queryProfitUrl = '/nccloud/fa/assettransfer/LinkQuery.do';

//新增资产审批单常量
export const assetTransferPageCode = '201201520A_transfer';
export const assetTransferHead = '201201520A_head';
export const assetTransferBody = '201201520A_body';
export const assetTransferSearch = '201201520A_search';
export const assetAppcode = '201201520A';

export const assetMainTransferPageCode = '201201520A_transferMain';

//设备卡片
export const equipTransferPageCode = '451001008A_transfer';
export const equipTransferHead = '451001008A_head';
export const equipTransferBody = '451001008A_body';
export const equipTransferSearch = '451001008A_search';
export const equipAppcode = '451001008A';

export const equipMainTransferPageCode = '451001008A_transferMain';

//采购转固单
export const purTransferPageCode = '201201524A_transfer';
export const purTransferHead = '201201524A_head';
export const purTransferBody = '201201524A_body';
export const purTransferSearch = '201201524A_search';
export const purAppcode = '201201524A';

export const purMainTransferPageCode = '201201524A_transferMain';

//工程转固单
export const proTransferPageCode = '201201528A_transfer';
export const proTransferHead = '201201528A_head';
export const proTransferBody = '201201528A_body';
export const proTransferSearch = '201201528A_search';
export const proAppcode = '201201528A';

export const proMainTransferPageCode = '201201528A_transferMain';

//盘盈单
export const profitTransferPageCode = '2012048030A_transfer';
export const profitTransferHead = '2012048030A_head';
export const profitTransferBody = '2012048030A_body';
export const profitTransferSearch = '2012048030A_search';
export const profitAppcode = '201201512A';

export const profitMainTransferPageCode = '2012048030A_transferMain';

//全部
export const allTransferPageCode = 'all_transfer';
export const allTransferHead = 'all_head';
export const allTransferBody = 'all_body';
export const allTransferSearch = 'all_search';

export const allMainTransferPageCode = 'all_transferMain';

export const KeyMap = {
	KeyMapNewToAll: {
		all_head: {
			pk: 'pk_newasset',
			pk_org: 'pk_org_v',
			bill_code: 'bill_code',
			business_date: 'business_date',
			pk_recorder: 'pk_transactor'
		},
		all_body: {
			pk_b: 'pk_newasset_b',
			asset_name: 'asset_name',
			asset_spec: 'spec',
			asset_type: 'card_model',
			pk_category: 'pk_category',
			asset_value: 'sum_money'
		}
	},
	KeyMapPurToAll: {
		all_head: {
			pk: 'pk_transasset',
			pk_org: 'pk_org_v',
			bill_code: 'bill_code',
			business_date: 'business_date',
			pk_recorder: 'pk_recorder'
		},
		all_body: {
			pk_b: 'pk_transasset_b',
			asset_name: 'asset_name',
			asset_spec: 'asset_spec',
			asset_type: 'asset_type',
			pk_category: 'pk_category',
			asset_value: 'pricetaxsum_value'
		}
	},
	KeyMapProToAll: {
		all_head: {
			pk: 'pk_transasset',
			pk_org: 'pk_org_v',
			bill_code: 'bill_code',
			business_date: 'business_date',
			pk_recorder: 'pk_recorder'
		},
		all_body: {
			pk_b: 'pk_transasset_b',
			asset_name: 'asset_name',
			asset_spec: 'asset_spec',
			asset_type: 'asset_type',
			pk_category: 'pk_category',
			asset_value: 'asset_value'
		}
	},
	KeyMapProfitToAll: {
		all_head: {
			pk: 'pk_profit',
			pk_org: 'pk_org_v',
			bill_code: 'bill_code',
			business_date: 'business_date'
			//经办人暂时为空pk_recorder: 'pk_recorder'
		},
		all_body: {
			pk_b: 'pk_profit_b',
			asset_name: 'asset_name',
			asset_spec: 'spec',
			asset_type: 'model',
			pk_category: 'pk_category'
			//盘盈没有金额asset_value: 'asset_value'
		}
	},
	KeyMapEquipToAll: {
		all_head: {
			pk: 'pk_equip',
			pk_org: 'pk_org_v',
			bill_code: 'equip_code',
			business_date: 'start_used_date',
			asset_name: 'equip_name',
			asset_spec: 'spec',
			asset_type: 'model'
			//经办人暂时为空pk_recorder: 'pk_recorder'
		},
		all_body: {
			pk_b: 'pk_equip'
			// asset_name: 'equip_name',
			// asset_spec: 'spec',
			// asset_type: 'model'
			//pk_category: 'purc_price'  //暂时是无税价格，回头确认
		}
	}
};
