import translateData from '../utils/translateData';
import {
	proTransferHead,
	proTransferBody,
	purTransferHead,
	purTransferBody,
	assetTransferHead,
	assetTransferBody,
	equipTransferBody,
	equipTransferHead,
	profitTransferHead,
	profitTransferBody,
	KeyMap,
	allTransferHead,
	allTransferBody
} from '../constants';

import ampub from 'ampub';
const {utils} = ampub;

const {getMultiLangByID} = utils.multiLangUtils;

export default function select(data) {
	let selectdata = this.props.transferTable.getTransferTableSelectedValue();
	// 转换为全部模板需要的数据
	let destdatas = [];
	//全部
	destdatas.push(...selectdata[allTransferHead]);
	//新增资产审批单
	let newasset_billtype = { bill_type: { value: 'HN', display: getMultiLangByID('201201512A-000033') } };
	let destNewAssetDatas = translateData(
		selectdata[assetTransferHead],
		assetTransferHead,
		assetTransferBody,
		allTransferHead,
		allTransferBody,
		KeyMap.KeyMapNewToAll,
		newasset_billtype
	);
	destdatas.push(...destNewAssetDatas);
	//设备卡片
	let equip_billtype = { bill_type: { value: '4A00', display: getMultiLangByID('201201512A-000032') } };
	let destEquipDatas = translateData(
		selectdata[equipTransferHead],
		equipTransferHead,
		null,
		allTransferHead,
		allTransferBody,
		KeyMap.KeyMapEquipToAll,
		equip_billtype
	);
	destdatas.push(...destEquipDatas);
	//采购转固单
	let purject_billtype = { bill_type: { value: 'HJ-02', display: getMultiLangByID('201201512A-000034') } };
	let destPurjectDatas = translateData(
		selectdata[purTransferHead],
		purTransferHead,
		purTransferBody,
		allTransferHead,
		allTransferBody,
		KeyMap.KeyMapPurToAll,
		purject_billtype
	);
	destdatas.push(...destPurjectDatas);
	//工程转固单
	let project_billtype = { bill_type: { value: 'HJ-01', display: getMultiLangByID('201201512A-000035') } };
	let destProjectDatas = translateData(
		selectdata[proTransferHead],
		proTransferHead,
		proTransferBody,
		allTransferHead,
		allTransferBody,
		KeyMap.KeyMapProToAll,
		project_billtype
	);
	destdatas.push(...destProjectDatas);
	//盘盈
	let profit_billtype = { bill_type: { value: 'HS', display: getMultiLangByID('201201512A-000036') } };
	let destProfitDatas = translateData(
		selectdata[profitTransferHead],
		profitTransferHead,
		profitTransferBody,
		allTransferHead,
		allTransferBody,
		KeyMap.KeyMapProfitToAll,
		profit_billtype
	);
	destdatas.push(...destProfitDatas);
	//查看已选塞值
	this.props.transferTable.setMultiSelectedValue(allTransferHead, allTransferBody, destdatas, [ 'pk' ], [ 'pk_b' ]);
}
