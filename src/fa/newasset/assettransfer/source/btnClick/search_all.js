import {
	allTransferPageCode,
	allTransferHead,
	allTransferBody,
	allTransferSearch,
	proTransferPageCode,
	proTransferHead,
	proTransferBody,
	purTransferPageCode,
	purTransferHead,
	purTransferBody,
	equipTransferPageCode,
	equipTransferHead,
	equipTransferBody,
	assetTransferPageCode,
	assetTransferHead,
	assetTransferBody,
	profitTransferPageCode,
	profitTransferHead,
	profitTransferBody,
	KeyMap
} from '../constants';
import translateData from '../utils/translateData';
import { ajax, toast } from 'nc-lightapp-front';

import ampub from 'ampub';
const {utils} = ampub;

const {showMessage, MsgConst, showConfirm} = utils.msgUtils;
const {getMultiLangByID} = utils.multiLangUtils;

export default function clickSerachBtn(isRefAddLine, billvo) {
	let searchVal = this.props.search.getAllSearchData(allTransferSearch);
	if (searchVal) {
		let querydata = this.props.search.getQueryInfo('all_search', true);
		if (querydata) {
			querydata.pagecode = 'all_transfer';
			//querydata.pageInfo = pageInfo;
			//querydata.billtype = 'HN';
		}
		ajax({
			url: '/nccloud/fa/assettransfer/searchall.do',
			data: querydata,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let allData = [];
					if (data) {
						let keys = Object.keys(data);
						if(keys && keys.length) {
                            let vos = data[keys[0]];
                            let userjson = JSON.parse(vos[0].userjson);
                            userjson.sumCount = parseInt(userjson.sumCount );
                            userjson.currentCount = parseInt(userjson.currentCount);
                            if(userjson.sumCount > userjson.currentCount) {
                                // 本次查询出待生的固定资产卡片有 {sumCount }条，数据过大，请先批量处理完成 {currentCount } 张固定资产卡片，然后再继续处理。
                                toast({content: getMultiLangByID('201201512A-000054', {sumCount:userjson.sumCount, currentCount:userjson.currentCount}) , color: 'success'});
                            }
						}

                        //新增资产审批单
                        if (data[assetTransferPageCode]) {
                            let newAsset_billtype = {
                                bill_type: { value: 'HN', display: getMultiLangByID('201201512A-000033') }
                            };
                            let destNewAssetDatas = translateData(
                                data[assetTransferPageCode],
                                assetTransferHead,
                                assetTransferBody,
                                allTransferHead,
                                allTransferBody,
                                KeyMap.KeyMapNewToAll,
                                newAsset_billtype
                            );
                            allData.push(...destNewAssetDatas);
                        }

						//设备卡片
						if (data[equipTransferPageCode]) {
							let equip_billtype = {
								bill_type: { value: '4A00', display: getMultiLangByID('201201512A-000032') }
							};
							let destEquipDatas = translateData(
								data[equipTransferPageCode],
								equipTransferHead,
								null,
								allTransferHead,
								allTransferBody,
								KeyMap.KeyMapEquipToAll,
								equip_billtype
							);
							allData.push(...destEquipDatas);
						}

						//采购转固单
						if (data[purTransferPageCode]) {
							let pur_billtype = {
								bill_type: { value: 'HJ-02', display: getMultiLangByID('201201512A-000034') }
							};
							let destPurDatas = translateData(
								data[purTransferPageCode],
								purTransferHead,
								purTransferBody,
								allTransferHead,
								allTransferBody,
								KeyMap.KeyMapPurToAll,
								pur_billtype
							);
							allData.push(...destPurDatas);
						}
						//工程转固单
						if (data[proTransferPageCode]) {
							let project_billtype = {
								bill_type: { value: 'HJ-01', display: getMultiLangByID('201201512A-000035') }
							};
							let destProjectDatas = translateData(
								data[proTransferPageCode],
								proTransferHead,
								proTransferBody,
								allTransferHead,
								allTransferBody,
								KeyMap.KeyMapProToAll,
								project_billtype
							);
							allData.push(...destProjectDatas);
						}
						//盘盈单
						if (data[profitTransferPageCode]) {
							let profit_billtype = {
								bill_type: { value: 'HS', display: getMultiLangByID('201201512A-000036') }
							};
							let destProfitDatas = translateData(
								data[profitTransferPageCode],
								profitTransferHead,
								profitTransferBody,
								allTransferHead,
								allTransferBody,
								KeyMap.KeyMapProfitToAll,
								profit_billtype
							);
							allData.push(...destProfitDatas);
						}
						this.props.transferTable.setTransferTableValue(
							allTransferHead,
							allTransferBody,
							allData,
							'pk',
							'pk_b'
						);
						if (allData.length == 0) {
							showMessage(this.props, {
								color: 'warning',
								content: getMultiLangByID('201201512A-000037')
							});
						}
					} else {
						this.props.transferTable.setTransferTableValue(
							allTransferHead,
							allTransferBody,
							null,
							'pk',
							'pk_b'
						);
						showMessage(this.props, { color: 'warning', content: getMultiLangByID('201201512A-000037') });
					}
				}
			}
		});
	}
}
