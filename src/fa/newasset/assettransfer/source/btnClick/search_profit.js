import { profitTransferSearch, profitTransferHead, profitTransferBody, profitTransferPageCode } from '../constants';
import { ajax, toast } from 'nc-lightapp-front';
import ampub from 'ampub';
const {utils} = ampub;

const {showMessage, MsgConst, showConfirm} = utils.msgUtils;
const {getMultiLangByID} = utils.multiLangUtils;

export default function clickSerachBtn(isRefAddLine, billvo) {
	let searchVal = this.props.search.getAllSearchData(profitTransferSearch);
	if (searchVal) {
		let querydata = this.props.search.getQueryInfo(profitTransferSearch, true);
		if (querydata) {
			querydata.pagecode = profitTransferPageCode;
		}
		ajax({
			url: '/nccloud/fa/assettransfer/profit.do',
			data: querydata,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
                        if(data.length) {
                            let userjson = JSON.parse(data[0].userjson);
                            userjson.sumCount = parseInt(userjson.sumCount );
                            userjson.currentCount = parseInt(userjson.currentCount);
                            if(userjson.sumCount > userjson.currentCount) {
                                // 本次查询出待生的固定资产卡片有 {sumCount }条，数据过大，请先批量处理完成 {currentCount } 张固定资产卡片，然后再继续处理。
                                toast({content: getMultiLangByID('201201512A-000054', {sumCount:userjson.sumCount, currentCount:userjson.currentCount}) , color: 'success'});
                            }
                        }
						this.props.transferTable.setTransferTableValue(
							profitTransferHead,
							profitTransferBody,
							data,
							'pk_profit',
							'pk_profit_b'
						);
					} else {
						this.props.transferTable.setTransferTableValue(
							profitTransferHead,
							profitTransferBody,
							null,
							'pk_profit',
							'pk_profit_b'
						);
						showMessage(this.props, { color: 'warning', content: getMultiLangByID('201201512A-000037') });
					}
				}
			}
		});
	}
}
