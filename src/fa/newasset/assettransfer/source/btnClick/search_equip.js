import { equipTransferSearch, equipTransferHead, equipTransferBody, equipTransferPageCode } from '../constants';
import { ajax, toast } from 'nc-lightapp-front';
import ampub from 'ampub';
const {utils} = ampub;

const {showMessage, MsgConst, showConfirm} = utils.msgUtils;
const {getMultiLangByID} = utils.multiLangUtils;

export default function clickSerachBtn(isRefAddLine, billvo) {
	let searchVal = this.props.search.getAllSearchData(equipTransferSearch);
	if (searchVal) {
		// let data = {
		// 	querycondition: searchVal,
		// 	pageInfo: null,
		// 	pageid: equipTransferPageCode,
		// 	queryAreaCode: equipTransferSearch, //查询区编码
		// 	oid: '1001Z91000000003W2GC', //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
		// 	querytype: 'tree'
		// };
		let querydata = this.props.search.getQueryInfo(equipTransferSearch, true);
		if (querydata) {
			querydata.pagecode = equipTransferPageCode;
		}
		ajax({
			url: '/nccloud/fa/assettransfer/equip.do',
			data: querydata,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {//data is Array
						if(data.length) {
							let userjson = JSON.parse(data[0].userjson);
                            userjson.sumCount = parseInt(userjson.sumCount );
                            userjson.currentCount = parseInt(userjson.currentCount);
                            if(userjson.sumCount > userjson.currentCount) {
                                // 本次查询出待生的固定资产卡片有 {sumCount }条，数据过大，请先批量处理完成 {currentCount } 张固定资产卡片，然后再继续处理。
                                toast({content: getMultiLangByID('201201512A-000054', {sumCount:userjson.sumCount, currentCount:userjson.currentCount}) , color: 'success'});
                            }
						}
						this.props.transferTable.setTransferTableValue(
							equipTransferHead,
							equipTransferBody,
							data,
							'pk_equip',
							'pk_equip' //平台代码如果子表字段不输会报错
						);
					} else {
						this.props.transferTable.setTransferTableValue(equipTransferHead, null, [], 'pk_equip', null);
						showMessage(this.props, { color: 'warning', content: getMultiLangByID('201201512A-000037') });
					}
				}
			}
		});
	}
}
