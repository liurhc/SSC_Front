/**
 * 待生成固定资产卡片列表页
 * @author zhangypm
 * @since ncc1.0 2018年6月29日
 */

import React, { Component } from 'react';
import { createPage, ajax, base, toast } from 'nc-lightapp-front';
import constants from './constants';
import { dataSource } from '../source/constants';
import { buttonClick, initTemplate, afterEvent, beforeEvent, lockrequest } from './events';

import ampub from 'ampub';
const {components, utils} = ampub;

const {getContext} = components.LoginContext;
const {showMessage} = utils.msgUtils;
const {createCardTitleArea} = utils.cardUtils;
const {getMultiLangByID} = utils.multiLangUtils;

const tableId = constants.AREA.LIST.GRID_AREA; //表格区
const { FIELDS } = constants;

class List extends Component {
	constructor(props) {
		super(props);

		/*assetDataMap: {
                row_index: [assetvo, assetvo,...]
        },*/
		this.assetDataMap = {};

		//CardVO 的属性
		this.cardVoAttributes = '';
		//业务字段
        this.busiFields = [];

		//使用部门的缓存，缓存所有行数据, 这是为了便于处理 单使用部门，多使用部门的切换及重复编辑问题
		this.usedeptCache = {};

		this.templateid = '';

		//上游单据表头主键被加锁的集合
		this.sourceBillHeadPksLocked = new Set();

		//固定资产参数
		this.FAParamMap = {}; //以 pk_org 进行区分

		this.fa83param = false; //--支持多成本中心分摊	否（默认值）(集团级数据)


		this.doInit.bind(this);
		this.syncDataForCommit.bind(this);
		this.getDataBack.bind(this);
		this.setFAParamVOs.bind(this);
	}

	componentDidMount() {
		initTemplate.call(this, this.props);
		const that = this;

		this.doInit().then(() => {
			that.getData();
		});
	}

	componentWillMount() {
		// 监听浏览器关闭
		window.onbeforeunload = () => {
			// 解锁-执行同步请求
			lockrequest.call(this, [ ...this.sourceBillHeadPksLocked ], 'unlock', false);

			//浏览器回退键
            if (window.history && window.history.pushState) {
                window.onpopstate = () => {
                    lockrequest.call(this, [ ...this.sourceBillHeadPksLocked ], 'unlock', false);
                };
            }
		};
	}

	// 移除事件
	componentWillUnmount() {
		window.onbeforeunload = null;
	}

	//初始化
	doInit() {
		const that = this;
		const promise = new Promise(function(resolve, reject) {
			ajax({
				url: '/nccloud/fa/assettransfer/init.do',
				data: {},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						that.cardVoAttributes = data.cardVoAttributes;
						that.fa83param = data.FA83;

                        //业务字段
                        let busiFields = new Set(data.cardVoAttributes);
                        constants.FIELDS.BUSINESS_FIELD.map((f) => {
                            busiFields.add(f);
                        });
                        that.busiFields = [...busiFields];

						resolve(data.cardVoAttributes);
					} else {
						reject(data);
					}
				},
				error: (err) => {
					showMessage(that.props, { content: err.message, color: 'danger' });
					reject(err);
				}
			});
		});
		return promise;
	}

	getData() {
		const that = this;

		//从 缓存 中获取上游存储的数据
		let sourceBill = getContext.call(this, 'multiSource_for_facard', dataSource);
		if (sourceBill && sourceBill.length) {
			sourceBill = JSON.parse(sourceBill);
			
			sourceBill.map((sb) => {
				this.sourceBillHeadPksLocked.add(sb.sourceHeadPk);
			});

			this.sourceBill = sourceBill;

			ajax({
				url: constants.URL.transvo,
				data: {
					pageCode: constants.PAGE_CODE.LIST,
					templateid: this.templateid,
					transferInfos: sourceBill
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if(data && data.length) {
							this.getDataBack(data);
						}
					}
				},
				error: (err) => {
					showMessage(that.props, { content: err.message, color: 'danger' });
				}
			});
		}
	}

	getDataBack(data) {
		const tableData = [];
		// data is array
		for (let i = 0; i < data.length; i++) {
			const assetvos = data[i][tableId].rows;

			//得到主账簿数据
			const mainAccbookData = this.getMainAccbookData(assetvos);
			if (mainAccbookData) {
				tableData.push(mainAccbookData);
			} else {
				//没有主账簿数据的，说明没有资产类别
				tableData.push(assetvos[0]);
			}
			this.assetDataMap[i] = assetvos;
		}
		let paramVOs = JSON.parse(data[0].userjson);
		this.setFAParamVOs(paramVOs);

		//设置表格数据
		this.props.cardTable.setTableData(tableId, { rows: tableData }, () => {

		});
	}

	setFAParamVOs(paramVOs) {
		let FAParamMap = {};
		for(let paramVO of paramVOs) {
			FAParamMap[paramVO.pk_org] = paramVO;
		}
		this.FAParamMap = FAParamMap;
	}

	getMainAccbookData(assetvos) {
		let mainAccbookData = undefined;
		assetvos.some((assetvo) => {
			if (assetvo.values.business_flag.value) {
				mainAccbookData = assetvo;
				return true;
			}
		});
		return mainAccbookData;
	}

	getDataByAccbook(assetvos, accbook) {
		let accbookData = undefined;
		assetvos.some((assetvo) => {
			if (assetvo.values.pk_accbook.value == accbook) {
				accbookData = assetvo;
				return true;
			}
		});
		return accbookData;
	}

	//获取业务账簿
	getMainAccbook(assetvos) {
		let business_accbook = '';
		assetvos.some((assetvo) => {
			if (assetvo.values.business_flag.value) {
				business_accbook = assetvo.values.pk_accbook.value;
				return true;
			}
		});
		return business_accbook;
	}

	//提交数据前同步数据： 同步业务字段的数据（与账簿无关的数据）
	//单个卡片多个账簿之间数据的同步
	syncDataForCommit(assetvos) {
		let mainaccbookData = this.getMainAccbookData(assetvos);
		if (!mainaccbookData) {
			//如果不存在主账簿数据（还未设置资产类别时没有主账簿数据），直接返回
			return;
		}
		let otherAccbookDatas = assetvos.filter((assetvo) => {
			return assetvo.values.pk_accbook.value != mainaccbookData.values.pk_accbook.value;
		});

		if (otherAccbookDatas.length > 0) {
			for (let otherAccbookData of otherAccbookDatas) {
				//同步 BUSINESS_FIELD 数据到各个 asset
				for (let businessField of this.busiFields) {
					otherAccbookData.values[businessField] = mainaccbookData.values[businessField];
				}
			}
		}
	}

	//跳转到来源单据页
	backToSource() {
		buttonClick.call(this, this.props, constants.LIST_BTNS.HEAD.EXIT_TRANFER);
	}

	render() {
		let { cardTable, button, modal } = this.props;
		let { createCardTable } = cardTable;
		let { createModal } = modal;
		let { createButtonApp, createButton } = button;

		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{/*编辑卡片列表*/}
						{createCardTitleArea.call(this, this.props, {
							title: getMultiLangByID('201201512A-000031'),
							backBtnClick: this.backToSource
						})}
					</div>
					<div className="header-button-area">
						{createButtonApp({
							area: 'card_head',
							buttonLimit: 10,
							onButtonClick: buttonClick.bind(this)
						})}
					</div>
				</div>

				<div className="nc-bill-table-area">
					{createCardTable(tableId, {
						onBeforeEvent: beforeEvent.bind(this),
						onAfterEvent: afterEvent.bind(this),
						showCheck: true,
						showIndex: true
					})}
				</div>
				{/* 预警提示框 */}
				{createModal('budgetControl', { color: 'warning' })}
			</div>
		);
	}
}

List = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: constants.PAGE_CODE.LIST,
		bodycode: tableId,
		tabletype: 'cardTable'
	}
})(List);
export default List;
