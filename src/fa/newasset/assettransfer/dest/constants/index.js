//界面编码

const PAGE_CODE = {
	LIST: '201201512A_card' //列表态模板编码
};

// 界面状态
const UISTATE = {
	ADD: 'add', // 新增态
	EDIT: 'edit', // 编辑态
	BROWSE: 'browse' // 浏览态
};

const APPID = '201201512A'; //小应用编码

//区域
const AREA = {
	LIST: {
		//列表态
		GRID_AREA: 'assetAll' //列表区
	}
};

//  路径
const URL = {
	transvo: '/nccloud/fa/assettransfer/transvo.do', // 转换源vo到 assetvo
	headafteredit: '/nccloud/fa/facard/headafteredit.do', // 表头编辑后事件
	insert: '/nccloud/fa/assettransfer/insert.do', // 保存卡片
	delete_card: '/nccloud/fa/facard/delete.do', // 删除卡片
	batchalter: '/nccloud/fa/assettransfer/batchalter.do', // 批改
    lock: '/nccloud/fa/facard/lockcard.do',// 加锁/解锁
};

//列表按钮
const LIST_BTNS = {
	HEAD: {
		SAVE: 'Save', //保存
		BATCH_ALTER: 'BatchAlter', //批改
		EXIT_TRANFER: 'Back' //退出转单
	},
	TABLE_BODY: {
		SAVE: 'Save', //保存
		EXTEND: 'OpenCard', //展开
		DELETE: 'Delete', //删除
		CANCEL: 'Cancel' //取消
	}
};

//单据来源类型
const BILL_SOURCE_TYPES = {
	/** 1 手工录入 */
	handin_src: 'handin',
	/** 2 新增审批单据 */
	appBill_src: 'HN',
	/** 3 调入单据 */
	deploy_in_src: 'HD',
	/** 4 拆分单据 */
	split_src: 'HM',
	/** 5 合并单据 */
	conbin_src: 'HO',
	/** 6 盘盈资产 */
	inventory_src: 'HS',
	/** 7 设备卡片 */
	equip_src: '4A00',
	/** 8 转固单 */
	transasset_src: 'HJ',
	/** 8.1 采购转固单 */
	transasset_purchase_src: 'HJ-02',
	/** 8.2 工程转固单 */
	transasset_project_src: 'HJ-01'
};

const FIELDS = {
	HEAD_PK: 'pk_card', //表头主键字段
	BODY_PK: 'pk_cardhistory', //历史表主键
	PK_USEDEPT: 'pk_usedept', //使用部门字段
	PK_MANDEPT: 'pk_mandept', //管理部门字段
	USEDEP_FLAG: 'usedep_flag', //是否多使用部门
	/**
     * 卡片历史表中的财务字段（不同账簿其值会不同）:
     * 本币原值，进项税，是否抵扣进项税，累计折旧，本年折旧，减值准备，月折旧额，月折旧率，净残值，净残值率，单位折旧，净值，净额
     * 折旧方法、使用月限、使用月限显示、已使用月份、折本汇率、折旧承担部门
     * 注意：6.0把折旧承担部门修改为支持账簿级参数，那么它就在不同的账簿中值不同
     */
	ACCOUNT_FIELD: [
		'localoriginvalue',
		'tax_input',
		'taxinput_flag',
		'accudep',
		'curryeardep',
		'predevaluate',
		'depamount',
		'deprate',
		'salvage',
		'salvagerate',
		'depunit',
		'netvalue',
		'netrating',
		'pk_depmethod',
		'servicemonth',
		'servicemonth_display',
		'dep_start_date',
		'dep_end_date',
		'naturemonth',
		'naturemonth_display',
		'usedmonth',
		'localcurr_rate',
		'paydept_flag'
	],
	/**
     * 卡片历史表中的业务字段(在多账簿数据中其值都是一样的): 集团、组织、原币原值、使用部门、是否多使用部门、管理部门、数量、 资产类别、项目档案、使用状况、 卡片是否新增、是否最新状态,
     * 工作总量、累计工作量、月工作量、成本中心、货主管理组织
     */
	BUSINESS_FIELD: [
		'pk_group',
		'pk_org',
		'originvalue',
		'pk_usedept',
		'usedep_flag',
		'pk_mandept',
		'card_num',
		'pk_category',
		'pk_jobmngfil',
		'pk_usingstatus',
		'newasset_flag',
		'laststate_flag',
		'allworkloan',
		'accuworkloan',
		'monthworkloan',
		'pk_equiporg',
		'pk_costcenter',
		'pk_ownerorg'
	],
	/**
     * 编辑后不需要后台处理的字段，即没有联动关系的字段
     * 资产名称、条形码、规格、型号、管理部门、增加方式、使用状况、存放地点、[交易类型]
     */
	NORELATION_FIELD: [
		'asset_name',
		'bar_code',
		'card_model',
		'spec',
		'pk_mandept',
		'pk_addreducestyle',
		'pk_usingstatus',
		'position',
		'asset_code',
		'card_code',
		'pk_transitype'
	],
	/**
     * 自定义项标志
     */
	DEF_FIELD: 'def'
};

//参照过滤配置
const ReferFilterConfig = {
	bodyIds: [ AREA.LIST.GRID_AREA ],
	defPrefix: {
		body: 'def'
	},
	specialFields: {
		pk_dept: {
			//使用部门
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_equiporg', 'pk_org', 'pk_org_v' ],
					returnName: 'pk_org'
				}
			]
		},
		pk_equip_usedept: {
			//设备使用部门 PK_EQUIP_USEDEPT 参照 使用权
			data: [
				{
					fields: [ 'pk_equiporg' ],
					returnName: 'pk_org'
				}
			]
		},
		pk_equip_usedept_v: {
			//设备使用部门 PK_EQUIP_USEDEPT 参照 使用权
			data: [
				{
					fields: [ 'pk_equiporg' ],
					returnName: 'pk_org'
				}
			]
		},
		pk_jobmngfil: {
			//项目档案 参照集团+主组织下的项目
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		},
		asset_group: {
			//资产组参照
			RefActionExtType: 'GridRefActionExt',
			class: 'nccloud.web.fa.newasset.facard.refcondition.AssetGroupRefFilter',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		},
        pk_transitype: {
			data:[
                {
                    returnConst: 'H1',
                    returnName: 'parentbilltype'
                }
			]
		}
	}
};

const destDataSource = 'fa.newasset.assettransfer.dest';

const constant = {
	PAGE_CODE,
	UISTATE,
	APPID,
	AREA,
	URL,
	LIST_BTNS,
	BILL_SOURCE_TYPES,
	FIELDS,
	ReferFilterConfig,
    destDataSource,
};

export default constant;
