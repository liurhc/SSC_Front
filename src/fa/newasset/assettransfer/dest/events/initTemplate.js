import { base, toast } from 'nc-lightapp-front';
import constants from '../constants';
import { doSingleSave, doDeleteLineUnsaved, doDeleteLineSaved } from './buttonClick';

import ampub from 'ampub';

const {components, utils} = ampub;

const {openAssetCardByPk} = components.faQueryAboutUtils;
const {getContext, loginContext, loginContextKeys} = components.LoginContext;
const {getMultiLangByID} = utils.multiLangUtils;

let destDataSource = constants.destDataSource;

const tableId = constants.AREA.LIST.GRID_AREA;
const { SAVE, EXTEND, DELETE, CANCEL } = constants.LIST_BTNS.TABLE_BODY;

//加载模板
export default function(props) {
	const that = this;
	props.createUIDom(
		{
			pagecode: constants.PAGE_CODE.LIST //页面id
		},
		function(data) {

			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, destDataSource);

					//初始化时获取后台数据源信息，并存在缓存中，用于解锁
					getContext(loginContextKeys.dataSourceCode, destDataSource);
				}

				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
                    // 行删除时悬浮框提示
                    /* 国际化处理： 确定要删除吗？*/
                    props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));
                }
				if (data.template) {
					let meta = data.template;
					that.templateid = meta.pageid;
					modifierMeta.call(that, props, meta);
					props.meta.setMeta(meta);

					props.cardTable.setStatus(tableId, constants.UISTATE.EDIT);
				}
			}
		}
	);
}

//模板修改
function modifierMeta(props, meta) {
	const that = this;
	let status = props.getUrlParam('status');

	//表体添加操作列
	let oprCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		itemtype: 'customer', //操作列的制定类型
		className: 'table-opr',
		width: 150,
		fixed: 'right',
		render(text, record, index) {
			let pk_card = record.values.pk_card.value;
			let buttonAry = [];
			if (pk_card) {
				//表示已经保存了卡片， 展示的按钮： 展开、删除
				buttonAry = [ EXTEND, DELETE ];
			} else {
				//表示未保存卡片， 展示的按钮： 保存、取消
				buttonAry = [ SAVE, CANCEL ];
			}

			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) =>
					tableButtonClick.call(that, props, key, text, record, index, 'card_body_inner')
			});
		}
	};
    meta[tableId].items.map((item) => {
        //使用状况、增加方式只能选择末级
        if (item.attrcode === 'pk_addreducestyle' || item.attrcode === 'item.attrcode ') {
            item.onlyLeafCanSelect = true;
        }
    });
	meta[tableId].items.push(oprCol);
	return meta;
}

function tableButtonClick(props, key, text, record, index) {
	const that = this;

	switch (key) {
		//跳转到卡片编辑页
		case EXTEND:
			const pk_card = record.values.pk_card.value;

			if (pk_card) {
				//跳转到卡片界面进行编辑
				openAssetCardByPk(props, pk_card, { status: constants.UISTATE.EDIT });
			}
			break;

		//删除卡片
		case DELETE:
			doDeleteLineSaved(that, index);
			break;

		//保存卡片
		case SAVE:
			doSingleSave(that, index);
			break;

		//取消
		case CANCEL:
			doDeleteLineUnsaved(that, index);
			break;

		default:
			break;
	}
}
