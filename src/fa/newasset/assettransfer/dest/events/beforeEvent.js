//表格单元格的编辑前事件
import constants from "../constants";

import ampub from 'ampub';
import fa from 'fa';

const {commonConst} = ampub;
const {fa_components} = fa;

const {addBodyReferFilter} = fa_components.ReferFilter;
const {IBusiRoleConst} = commonConst.CommonKeys;

//与 资产类别 相关的字段
let categoryFiles = [
    'pk_depmethod',
    'naturemonth',
    'servicemonth',
    'salvagerate',
    'taxinput_flag',
    'naturemonth_display',
    'servicemonth_display'
];

/**
 * 工作量法相关的字段（不包括折旧方法）
 */
let workLoadFields = [
    /** 工作总量 */
    'allworkloan',
    /** 累计工作量 */
    'accuworkloan',
    /** 月工作量 */
    'monthworkloan',
    /** 单位折旧 */
    'depunit',
    /** 工作量单位 */
    'workloanunit'
];


const tableId = constants.AREA.LIST.GRID_AREA;

//props, moduleId(区域id), key(操作的键), value（当前值）,  index（当前index）, record（行数据）
export default function (props, moduleId, key, value, index, record) {
    const that = this;
    //已经保存的行不能再次进行编辑
    let pk_card = record.values.pk_card && record.values.pk_card.value;
    let pk_org = record.values.pk_org && record.values.pk_org.value;
    if (pk_card) {
        return false;
    }

    //参照过滤
    let meta = props.meta.getMeta();
    addBodyReferFilter.call(this, props, meta, constants.ReferFilterConfig, record);

    //自定义项 参照过滤(这里没有使用公共的自定义项参照过滤，因为没有 form 区域)
    if (key.startsWith(constants.ReferFilterConfig.defPrefix.body)) {
        meta[tableId].items.map((item) => {
            if (key === item.attrcode) {
                item.queryCondition = () => {
                    let pk_org = record.values.pk_org && record.values.pk_org.value;
                    let pk_group = record.values.pk_group && record.values.pk_group.value;
                    return { pk_org, pk_group, busifuncode: IBusiRoleConst.ASSETORG };
                };
            }
        });
    }


    //使用部门\管理部门
    meta[tableId].items.map((item, key) => {
        if (item.attrcode === 'pk_usedept' || item.attrcode === 'pk_mandept') {
            item.onlyLeafCanSelect = !that.FAParamMap[pk_org]['is_allow_dept_midlev'];
        }
    });

    //基于规则的编辑前判断
    //有一个规则不满足，就返回 false
    let funAry = [currRateBeforeEdit, categoryBeforeEdit, workloanFieldEdit, pkcostcenterBeforeEdit];
    let hasOne = funAry.some((f) => !f.call(that, props, moduleId, key, value, index, record));

    //console.log(" ------------   编辑前检验结果 = " + !hasOne);

    return !hasOne;
}


//============================================
//折本汇率编辑前  需要返回 true/false
function currRateBeforeEdit(props, moduleId, key, value, index, record) {
    // console.log('------------  currRateBeforeEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = ['localcurr_rate'];
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    let pk_currency = record.values.pk_currency && record.values.pk_currency.value;
    if (!pk_currency) {
        return false;
    }

    //获取当前账簿的币种
    let pk_accbook =  record.values.pk_accbook && record.values.pk_accbook.value;
    if(!pk_accbook) {
        return false;
    }
    let pk_org =  record.values.pk_org && record.values.pk_org.value;

    let pk_currency_book = this.FAParamMap[pk_org]['accbookCurrency'];
    pk_currency_book = pk_currency_book && pk_currency_book[pk_accbook];
    if (!pk_currency_book) {
        // console.log('------ 未获取到账簿的币种(可能是还未选择类别或发生错误) ------');
        return false;
    }

    return (pk_currency_book !== pk_currency);
}

//资产类别相关字段
function categoryBeforeEdit(props, moduleId, key, value, index, record) {
    // console.log('------------  categoryBeforeEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = categoryFiles;
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    //FA10 账簿信息设置中资产类别中的参数在卡片中是否可修改
    //说明: 在账簿信息中资产类别设置的参数带到卡片上时，是否允许手工修改；
    let pk_accbook =  record.values.pk_accbook && record.values.pk_accbook.value;
    if(!pk_accbook) {
        return false;
    }

    let pk_org =  record.values.pk_org && record.values.pk_org.value;
    let fa10 = this.FAParamMap[pk_org]['fa10param'];
    fa10 = fa10 && fa10[pk_accbook];
    return !!fa10;
}

//工作量法字段编辑前处理
function workloanFieldEdit(props, moduleId, key, value, index, record) {
    // console.log('------------  workloanFieldEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = workLoadFields;
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    let pk_depmethod =  record.values.pk_depmethod && record.values.pk_depmethod.value;
    if (!pk_depmethod) {
        return false;
    }
    let pk_org =  record.values.pk_org && record.values.pk_org.value;
    let methods = this.FAParamMap[pk_org].depmethods.filter((m) => {
        return m.pk_depmethod === pk_depmethod;
    });
    if (!methods.length) {
        // console.log('------ 理论上这是不会发生的，请检查折旧方法是否未启用，或者已经被删除------');
        return false;
    }
    let depmethod_code = methods[0].depmethod_code;
    // 取出折旧方法的编码进行比较：（因为在预置数据时，没有办法把主键预置到数据库）
    if (depmethod_code === '04') {//工作量法 预置数据的编码
        return true;
    } else {// 折旧方法中的公式中存在该字段，那么可以编辑

        let workloanFildes = this.FAParamMap[pk_org].depmethodFormulaFieldMap[pk_depmethod];
        if (!workloanFildes) {
            // console.log('------ 折旧方法不存在工作量字段不能编辑 ------');
            return false;
        }
        if (workloanFildes.includes(key)) {
            return true;
        }
    }

    return false;
}

//集团级业务参数控制的字段的可编辑性
function pkcostcenterBeforeEdit(props, moduleId, key, value, data) {
    // console.log('------------  pkcostcenterBeforeEdit -------------');

    //集团级参数FA83--支持多成本中心分摊
    // 否（默认值）：   成本中心为可编辑
    //是：	成本中心为不可编辑，自动取使用部门对应的成本中心，卡片页面上显示为空
    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = ['pk_costcenter'];
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    return !this.fa83param
}
