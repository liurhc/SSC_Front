import {ajax, toast} from 'nc-lightapp-front';
import constants from '../constants';

import ampub from 'ampub';
import fa from 'fa';

const {utils} = ampub;
const {fa_components} = fa;

const {referLinkageClear} = fa_components.ReferLinkage;
const {showMessage} = utils.msgUtils;

const pageCode = constants.PAGE_CODE.LIST;
const tableId = constants.AREA.LIST.GRID_AREA; //表格区

export default function (props, moduleId, field, newValue, changedrows, index, record, type, method) {

    //效率优化，开启开关
    props.beforeUpdatePage();

    //联动处理
    referLinkageClear(
        props,
        field,
        {
            tableId,
            linkageData: {
                pk_category: ['pk_depmethod', 'naturemonth', 'salvagerate']
            }
        },
        'cardtable',
        moduleId,
        index
    );

    //从 record 上取当前字段的值
    let currValue = record.values[field];

    //这里改为统一从界面上取数，并更新 assetDataMap
    //多使用部门标记
    let usedep_flag = false;

    //使用部门字段处理
    if (field === 'pk_usedept') {
        //判断是否是多使用部门，需要更新 usedep_flag 的值，
        //单使用部门需要走编辑后 获取资产套号， 多使用部门不需要
        if (newValue.length && newValue.length > 1) {
            //多使用部门
            usedep_flag = true;

            props.cardTable.setValByKeyAndIndex(tableId, index, 'usedep_flag', {value: usedep_flag});
            props.cardTable.setValByKeyAndIndex(tableId, index, 'assetsuit_code', {value: ''});
        } else {
            //单使用部门
            props.cardTable.setValByKeyAndIndex(tableId, index, 'usedep_flag', {value: usedep_flag});
        }
    }

    //资产类别需要同时更新交易类型 "pk_transitype", "transi_type"
    if (field === 'pk_category' && newValue && newValue.values) {
        props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_transitype', newValue.values.pk_transitype);
        props.cardTable.setValByKeyAndIndex(tableId, index, 'transi_type', newValue.values.transi_type);

    }

    //交易类型(pk_transitype)更新时同时更新 交易类型编码(transi_type)
    if (field === 'pk_transitype') {
        props.cardTable.setValByKeyAndIndex(tableId, index, 'transi_type', {value: newValue.refcode});
    }

    // 性能优化，表单和表格统一渲染
    props.updatePage(null, tableId);


    const that = this;
    //从 state 上获取缓存的数据
    let assetData = this.assetDataMap[index];
    const currentPkaccbook = this.getMainAccbook(assetData); //选择主账簿
    let mainAccbookData = this.getMainAccbookData(assetData); //选择主账簿数据
    if(!mainAccbookData) {//主账簿数据为空表示还未选择【资产类别】
        mainAccbookData = assetData[0];
    }
    //获取当前行的数据,并同步到主账簿中
    let rowData = props.cardTable.getRowsByIndexs(tableId, index)[0];
    for (let field in mainAccbookData.values) {//这里之所以循环赋值 是考虑到字段联动场景
        mainAccbookData.values[field] = rowData.values[field] || mainAccbookData.values[field];
    }

    //同步其他账簿的数据
    this.syncDataForCommit(assetData);

    //不需要关联处理的字段不需要发请求到后台， 包括自定义项
    if (constants.FIELDS.NORELATION_FIELD.includes(field) || field.startsWith(constants.FIELDS.DEF_FIELD)) {
        return;
    }

    if (usedep_flag) {
        //多使用部门不需要走编辑后事件
        return;
    }

    //类别字段为空 不走编辑后事件
    if (!record.values.pk_category || !record.values.pk_category.value) {
        return;
    }

    let param = {
        isCategoryChange: false,
        index,
        usedep_flag
    };

    const fromAccbookChange = false;

    let userJson = {
        newasset_flag: 1, //卡片新增标识：资产增加
        current_accbook: currentPkaccbook,
        fromAccbookChange,
        editStyle: 'table' //编辑方式：主要用于编辑后事件的多使用部门处理
    };


    //这里之所以不取 newValue 参数是由于 他是一个数值 而不是一个对象
    let data = {
        attrcode: field,
        newValue: currValue,
        customValue: userJson,
        grid: {
            pageid: pageCode,
            model: {
                areacode: tableId,
                areaType: 'table',
                rows: assetData
            }
        }
    };


    ajax({
        url: constants.URL.headafteredit,
        data,
        async: false,
        success: (res) => {
            let {success, data} = res;
            if (success) {
                Promise.resolve(true).then(() => {
                    afterEventBackHandler.call(that, data, param);
                });
            }
        },
        error: (err) => {
            //清空当前字段值
            let emptyVal = Object.assign(record.values[field], {display: '', value: ''});//保持scale为原值
            record.values[field] = emptyVal;
            props.cardTable.updateDataByIndexs(tableId, [{
                index,
                data: {status: record.status, values: record.values}
            }]);
            showMessage(props, {color: 'danger', content: err.message});
        }
    });
}

/**
 * 编辑后事件返回处理（除了 财务组织字段 ）
 */
function afterEventBackHandler(data, param) {
    //后台对当前账簿进行了处理，如果前台已经有选中的账簿，则后台就返回该账簿为当前账簿；
    //如果前台还未选中账簿，则后台设置业务账簿为当前选中账簿
    let {
        fieldsCantEdit,
        current_accbook,
        business_accbook,
        toUIObject: {heads, bodysMap},
        is_allow_dept_midlev
    } = data;

    this.setState({
        business_accbook,
        is_allow_dept_midlev
    });

    let assetAllData = [];
    if (heads) {
        heads.forEach((head) => {
            if (head[tableId] && head[tableId].rows && head[tableId].rows.length > 0) {
                assetAllData = assetAllData.concat(head[tableId].rows);
            }
        });
    }

    //解析返回的 assetvo 数据
    let rows = assetAllData;

    //更新 this.state.assetDataMap 中的值
    this.assetDataMap[param.index] = rows;

    let assetvoMap = new Map();
    for (let row of rows) {
        const pk_accbook = row['values']['pk_accbook'].value;
        assetvoMap.set(pk_accbook, row);
    }

    //更新表格数据为主账簿的数据
    const businessbookRow = assetvoMap.get(current_accbook);
    this.props.cardTable.updateDataByIndexs(tableId, [
        {
            index: param.index,
            data: {status: '2', values: businessbookRow.values}
        }
    ]);
}
