import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import Card, { initTemplate } from '../../transassetbase/card';
import { baseConfig } from '../../transassetbase/card/const';
/**
 * 采购转固入口
 * created by wagnwhf 2018-08-08
 */
const TransassetCard = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: '201201524A_card',
		headcode: 'card_head',
		bodycode: 'bodyvos'
	}
})(Card);
// 页面配置
const pageConfig = {
	...baseConfig,
	pagecode: '201201524A_card', //单据模板pageID
	appcode: '201201524A', // 小应用编码
	node_code: '2012016027',
	title: '201201524A-000004', // 采购转固
	dataSource: 'fa.newasset.purcahsetransasset.main',
	transi_type: 'HJ-02' //交易类型
};

export default class PurchaseTransassetCard extends Component {
	render() {
		return <TransassetCard pageConfig={pageConfig} />;
	}
}
