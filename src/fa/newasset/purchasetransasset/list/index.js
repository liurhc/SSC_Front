import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import List, { initTemplate } from '../../transassetbase/list';
import { baseConfig } from '../../transassetbase/list/const';
/**
 * 采购转固入口
 * created by wagnwhf 2018-08-08
 */
const TransassetList = createPage({})(List);
// 页面配置
const pageConfig = {
	...baseConfig,
	pagecode: '201201524A_list', //单据模板pageID
	appcode: '201201524A', // 小应用编码
	node_code: '2012016027',
	title: '201201524A-000004', // 采购转固
	dataSource: 'fa.newasset.purcahsetransasset.main',
	transi_type: 'HJ-02' //交易类型
};

export default class PurchaseTransassetList extends Component {
	render() {
		return <TransassetList pageConfig={pageConfig} />;
	}
}
