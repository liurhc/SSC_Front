import {ajax, cardCache} from 'nc-lightapp-front';
import constants from '../../constants';
import ampub from 'ampub';
const {utils} = ampub;

const {setListValue} = utils.listUtils;

import {listQueryBackHandler} from './commonHandler';
import {setBatchBtnsEnable} from "./rowSelected";

const tableId = constants.AREA.LIST.GRID_AREA;
const assetDataSource = constants.assetDataSource;

export default function (props, config, pks) {
    doQueryByPks(props, pks);
}

//根据pks进行查询
const doQueryByPks = (props, pks) => {

    //先把 pks(实际上是 pk_cardhistory)转为clientKey
    let pk2ClientKeyMap = cardCache.getDefData(constants.pk2ClientKeyMap, assetDataSource);

    let clientKeys = [];
    for (let pk of pks) {
        clientKeys.push(pk2ClientKeyMap[pk]);
    }

    let data = {
        allpks: clientKeys,
        pagecode: constants.PAGE_CODE.LIST,
    };
    ajax({
        url: constants.URL.QUERY_LIST_BYPKS,
        data: data,
        success: (res) => {
            setListValue.call(this, props, res, tableId);
            setBatchBtnsEnable.call(this, props, tableId);
        },
        error: (err) => {
            listQueryBackHandler.error(props, err);
        }
    });
};
