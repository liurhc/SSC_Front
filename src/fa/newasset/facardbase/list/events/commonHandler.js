/**
 * 公共方法
 */
import ampub from 'ampub';

const { utils} = ampub;

const { MsgConst, showMessage} = utils.msgUtils;
import constants from '../../constants';

const tableId = constants.AREA.LIST.GRID_AREA;

//列表态——列表查询回调处理
const listQueryBackHandler = {
    success: (props, res) => {
        let {success, data} = res;
        if (success) {
            if (data) {
                props.table.setAllTableData(tableId, data[tableId]);
            } else {//返回的数据为空
                props.table.setAllTableData(tableId, {rows: []});
                /*国际化处理：未查询出符合条件的数据*/
                showMessage(props, { type: MsgConst.Type.QueryNoData });

            }
        } else {
            props.table.setAllTableData(tableId, {rows: []});
            /*国际化处理：未查询出符合条件的数据*/
            showMessage(props, { type: MsgConst.Type.QueryNoData });
        }
    },
    error: (props, err) => {
        showMessage(props, {content: err.message, color: "danger"});
    }
};

export {
    listQueryBackHandler
}