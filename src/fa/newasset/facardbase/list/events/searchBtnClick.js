import {ajax, cardCache} from 'nc-lightapp-front';
import constants from '../../constants';

import ampub from 'ampub';
import fa from 'fa';

const {components, utils} = ampub;
const {fa_components} = fa;

const {setQueryInfoCache, setListValue, listConst} = utils.listUtils;
const {getContext, loginContextKeys} = components.LoginContext;
import {listQueryBackHandler} from './commonHandler';
import {setBatchBtnsEnable} from "./rowSelected";

const {getAccbookAndPeriod} = fa_components.AccbookAndPeriod;

const searchId = constants.AREA.LIST.SEARCH_AREA; //查询区
const tableId = constants.AREA.LIST.GRID_AREA;
const assetDataSource = constants.assetDataSource;

//点击查询，获取查询区数据
export default function (props, searchVal, type, queryInfo, isRefresh) {
    const {pageConfig} = this.props; //这里需要 使用 this.props 才能获取到自定义参数
    const pageInfo = props.table.getTablePageInfo(tableId);
    pageInfo.pageIndex = 0; //重置到第一页
    //获取查询参数
    if (!queryInfo) {
        queryInfo = props.search.getQueryInfo(searchId);
    }
    if (!queryInfo.querycondition) {//如果有必输项未填写
        return;
    }
    queryInfo.pageInfo = pageInfo;
    queryInfo.pagecode = constants.PAGE_CODE.LIST;
    queryInfo.billtype = pageConfig.billType;//单据类型
    queryInfo.userdefObj = {
        templateid: this.templateid
    };

    // 缓存查询条件
    setQueryInfoCache.call(this, queryInfo, isRefresh, type);

    ajax({
        url: constants.URL.QUERY_LIST,
        data: queryInfo,
        success: (res) => {
            setListValue.call(
                this,
                props,
                res,
                tableId,
                isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
            );
            setBatchBtnsEnable.call(this, props, tableId);

            //取出 pk2ClientKeyMap 并存到缓存中
            let {success, data} = res;
            if(success && data) {
                cardCache.setDefData(constants.pk2ClientKeyMap, assetDataSource, JSON.parse(data.userjson));
            }
        },
        error: (err) => {
            listQueryBackHandler.error(props, err);
        }
    });
}


/**
 * 高级面板点击前事件
 */
export function clickAdvBtnEve() {
    //只有第一次打开的时候设置默认值
    if (this.isFirstLoad) {
        this.isFirstLoad = false;
        setSearchDefaultValue.call(this, this.props, searchId);
    }
}

/**
 * 设置查询区域得默认值：根据组织带出账簿
 * @param props
 * @param searchId
 */
export function setSearchDefaultValue(props, searchId) {
    let pk_org = props.search.getSearchValByField(searchId, 'pk_org');//获取界面上的财务组织（如果有默认组织（#mainorg#））
    if (pk_org && pk_org.value && pk_org.value.firstvalue) {
        //页面模板得的默认值
        pk_org = pk_org.value.firstvalue;
    } else {
        //个性化中心的值
        pk_org = getContext(loginContextKeys.pk_org);
        let org_name = getContext(loginContextKeys.org_Name);
        if (pk_org && org_name) {
            props.search.setSearchValByField(searchId, 'pk_org', {value: pk_org, display: org_name});
        }
    }

    getAccbookAndPeriod.call(this, props, searchId, null, {
        pk_org: pk_org,
        businessDate: getContext(loginContextKeys.businessDate)
    });

}