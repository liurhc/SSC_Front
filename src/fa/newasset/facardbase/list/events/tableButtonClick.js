import constants from "../../constants";
import {linkToCard, deleteLine} from "./buttonClick";

import fa from 'fa';
const {fa_components} = fa;

const {CardLinkToBill} = fa_components.CardLinkToBillUtil;

const headPk = constants.FIELDS.HEAD_PK; //主实体 pk 字段
const bodyPk = constants.FIELDS.BODY_PK; //子实体 pk 字段

export default function (props, key, text, record, index) {
    switch (key) {
        case constants.LIST_BTNS.TABLE_BODY.EDIT:
            linkToCardOnRow(props, record, constants.UISTATE.EDIT);
            break;

        case  constants.LIST_BTNS.TABLE_BODY.COPY:
            doCopyOnTable(this, record);
            break;

        case  constants.LIST_BTNS.TABLE_BODY.DELETE:
            deleteLine(props, {values: record});
            break;

        case  constants.LIST_BTNS.TABLE_BODY.QueryAboutBusiness:
            billreview(props, record);
            break;

        default:
            break;
    }
}

//table 行点击的跳转到卡片态
function linkToCardOnRow(props, record, status) {
    const id = record[bodyPk].value;
    const param = {
        status,
        id,
        pk_transitype: record.pk_transitype.value,
        transiType: record.transi_type.value,
        pagecode: record.transi_type.value,
    };
    linkToCard(props, param);
}

//表格操作列 copy
function doCopyOnTable(that, record) {
    const pk_card = record.pk_card.value;
    that.facardCopy.showCopyCardDialog(that, pk_card);
}

//单据追溯
function billreview(props, record) {
    //获取卡片来源
    let bill_source = record.bill_source && record.bill_source.value;
    //获取来源主键
    let pk_bill_src = record.pk_bill_src && record.pk_bill_src.value;
    if (pk_bill_src) {
        if (!(bill_source === 'handin')) {
            CardLinkToBill(props, pk_bill_src, bill_source);
        }
    }
}

export {
    linkToCardOnRow
}



