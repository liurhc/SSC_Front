import constants from '../../constants';

import {linkToCard} from './buttonClick';

const bodyPk = constants.FIELDS.BODY_PK; //子实体 pk 字段

export default function doubleClick(record, index, props) {
    const param = {
        status: constants.UISTATE.BROWSE,
        id: record[bodyPk].value,
        pk_transitype: record.pk_transitype.value,
        transiType: record.transi_type.value,
        pagecode: record.transi_type.value,
    };
    linkToCard(this.props, param);
}
