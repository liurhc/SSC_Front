import initTemplate from './initTemplate';
import searchBtnClick, {clickAdvBtnEve} from './searchBtnClick';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import doubleClick from './doubleClick';
import afterEvent from './afterEvent';
import rowSelected, {setBatchBtnsEnable} from './rowSelected';


export {
    searchBtnClick,
    clickAdvBtnEve,
    pageInfoClick,
    initTemplate,
    buttonClick,
    doubleClick,
    afterEvent,
    rowSelected,
    setBatchBtnsEnable
};