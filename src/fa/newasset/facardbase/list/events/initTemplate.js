import {createPage, ajax, base, cardCache} from 'nc-lightapp-front';

import constants from '../../constants';

import {addTransitypeBtns} from '../../utils';

import ampub from 'ampub';
import fa from 'fa';

const {components, utils} = ampub;
const {fa_components} = fa;

const {loginContext, getContext, loginContextKeys} = components.LoginContext;
const {getMultiLangByID} = utils.multiLangUtils;

const {addSearchAreaReferFilter} = fa_components.ReferFilter;
import {setBatchBtnsEnable} from './rowSelected';
import tableButtonClick, {linkToCardOnRow} from './tableButtonClick';
import {setSearchDefaultValue} from './searchBtnClick';

const tableId = constants.AREA.LIST.GRID_AREA;
const searchArea = constants.AREA.LIST.SEARCH_AREA;
const assetDataSource = constants.assetDataSource;

export default function (props) {
    const that = this;
    props.createUIDom(
        {
            pagecode: constants.PAGE_CODE.LIST,//页面id
        },
        function (data) {
            if (data) {
                if (data.context) {
                    // 初始化上下文变量
                    loginContext(data.context);
                }
                if (data.button) {//小应用中注册的按钮
                    let button = data.button;
                    //获取当前语种
                    let currentLangSeq = getContext(loginContextKeys.languageIndex);
                    addTransitypeBtns.call(that, button, currentLangSeq);
                    props.button.setButtons(button, () => {
                        setBatchBtnsEnable(props, tableId);
                    });
                }
                if (data.template) {//返回模版信息
                    that.templateid = data.template.pageid;

                    let meta = data.template;
                    meta = modifierMeta.call(that, props, meta);
                    props.meta.setMeta(meta, () => {
                        doInit(that);
                    });
                }

                // 行删除时悬浮框提示
                /* 国际化处理： 确定要删除吗？*/
                props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));
            }
        }
    )
}


function modifierMeta(props, meta) {
    const that = this;

    //添加复制卡片对话框模板
    that.facardCopy.initCopyCardTemplate(meta);
    //分页控件显示标识
    meta[tableId].pagination = true;

    //添加参照过滤
    addSearchAreaReferFilter(props, meta, constants.ReferFilterConfig);

    meta[tableId].items = meta[tableId].items.map((item, key) => {
        item.width = 150;
        //单据号添加下划线超链接
        if (item.attrcode == 'asset_code') {
            item.render = (text, record, index) => {
                return (
                    <div className="simple-table-td">
                        <span className="code-detail-link"
                              onClick={() => {
                                  linkToCardOnRow(props, record, constants.UISTATE.BROWSE);
                              }}
                        >
                        		{record.asset_code && record.asset_code.value}
                        </span>
                    </div>

                );
            };
        }
        return item;
    });
    //添加操作列
    meta[tableId].items.push({
        attrcode: 'opr',
        label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
        width: 200,
        itemtype: 'customer',//操作列的制定类型
        fixed: 'right',
        className: 'table-opr',
        visible: true,
        render: (text, record, index) => {
            let {EDIT, COPY, DELETE, QueryAboutBusiness} = constants.LIST_BTNS.TABLE_BODY;
            let buttonAry = [EDIT, COPY, DELETE];
            if (record.pk_bill_src && record.pk_bill_src.value &&
                record.pk_bill_src.value !== 'handin') {
                buttonAry.push(QueryAboutBusiness);
            }
            return props.button.createOprationButton(buttonAry, {
                area: "list_inner",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick.call(that, props, key, text, record, index, 'card_body_inner')
            });
        }
    });


    meta[searchArea].items.map((item) => {
        if (item.attrcode === 'pk_transitype') {
            item.queryCondition = () => {
                return {parentbilltype: 'H1'};
            };
        }
        //增加方式 允许选择非末级
        if (item.attrcode === 'pk_addreducestyle') {
            item.onlyLeafCanSelect = false
        }
    });
    return meta;
}

//初始化
function doInit(that) {
    setBatchBtnsEnable(that.props, tableId);
    setSearchDefaultValue.call(that, that.props, searchArea);

    //初始化缓存
    let pk2ClientKeyMap = cardCache.getDefData(constants.pk2ClientKeyMap, assetDataSource);
    if (!pk2ClientKeyMap) {
        cardCache.setDefData(constants.pk2ClientKeyMap, assetDataSource, {});
    }
}
