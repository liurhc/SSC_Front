import constants from '../../constants';
import ampub from 'ampub';

const {components, utils} = ampub;

const {getContext} =components.LoginContext;
const {listConst} = utils.listUtils;

export default function rowSelected(props, moduleId, record, index, status) {
    setBatchBtnsEnable.call(this, props, moduleId);
}

/**
 *
 * 列表设置批量按钮的可用性
 *
 * @param {*} props
 * @param {*} moduleId
 */
function setBatchBtnsEnable(props, moduleId) {
    let checkedRows = props.table.getCheckedRows(moduleId);
    let batchBtns = Object.values(constants.LIST_BTNS.HEAD);
    props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0));

    //是否显示【刷新】按钮
    let isQueryed = getContext(listConst.QUERYINFOKEY.isQueryed, listConst.QUERYINFOKEY.dataSource);
    let queryInfo = getContext(listConst.QUERYINFOKEY.queryInfo, listConst.QUERYINFOKEY.dataSource);
    if (isQueryed && queryInfo) {
        props.button.setButtonDisabled('Refresh', false);
    } else {
        props.button.setButtonDisabled('Refresh', true);
    }
    //设置联查卡片按钮是否显示
    let flag = checkedRows.some((row) => {
        return !!(row.data.values.pk_equip && row.data.values.pk_equip.value);
    });
    props.button.setButtonDisabled(constants.LIST_BTNS.HEAD.QueryAboutCard, !flag);

}

export {setBatchBtnsEnable};
