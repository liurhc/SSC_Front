import {ajax} from 'nc-lightapp-front';
import constants from '../../constants';

import fa from 'fa';
const {fa_components} = fa;

const {referLinkageClear} = fa_components.ReferLinkage;
const {getAccbookAndPeriod} = fa_components.AccbookAndPeriod;


const searchId = constants.AREA.LIST.SEARCH_AREA;
const ReferLinkageClearConfig = constants.ReferLinkageClearConfig;

export default function afterEvent(field, value) {
    //联动处理
    referLinkageClear(this.props, field, ReferLinkageClearConfig);

    if (field === 'pk_org' && value && value.length === 1) {
        //获取账簿
        getAccbookAndPeriod(this.props, searchId);
    }

}

