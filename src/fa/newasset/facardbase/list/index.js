/**
 * 固定资产卡片 列表态
 * @author zhangypms
 * @since ncc1.0 2018年5月11日
 */
import React, {Component} from 'react';
import {createPage, ajax, base, high, cardCache, cacheTools, createPageIcon } from 'nc-lightapp-front';
import constants from '../constants';
import ampub from 'ampub';
const {components, commonConst, utils} = ampub;
const {showMessage} = utils.msgUtils;

const {queryVoucherSrc} = components.queryVocherSrcUtils;
const {getMultiLangByID} = utils.multiLangUtils;
const {setListValue} = utils.listUtils;
const {linkQueryConst} = commonConst.CommonKeys;

import {listQueryBackHandler} from "./events/commonHandler";


import {
    searchBtnClick,
    clickAdvBtnEve,
    pageInfoClick,
    initTemplate,
    buttonClick,
    doubleClick,
    afterEvent,
    rowSelected, setBatchBtnsEnable,
} from './events';

import FaCardCopy from '../components/CardCopy';
import '../components/CardCopy.less';

const searchId = constants.AREA.LIST.SEARCH_AREA; //查询区
const tableId = constants.AREA.LIST.GRID_AREA; //表格区
const assetDataSource = constants.assetDataSource;

class List extends Component {
    constructor(props) {
        super(props);

        this.facardCopy = new FaCardCopy('list');

        this.state = {};

        this.isFirstLoad = true;//高级按钮第一次点击

        this.templateid = "";//模板主键

        this.doInit.bind(this);
        this.linkedQueryHandler.bind(this);

        this.doInit().then(() => {
            initTemplate.call(this, this.props);
        });
    }

    componentDidMount() {
        //凭证联查来源单据
        queryVoucherSrc(this.props, tableId, "pk_card", constants.PAGE_CODE.LIST,constants.URL.QUERY_LIST_BYPKS);

        //其他单据联查卡片到列表态（如：工程转固单按数量拆分建卡生成多张卡片时，联查时需要到列表态）
        this.linkedQueryHandler(this.props);
    }

    //初始化
    doInit() {
        const that = this;
        const promise = new Promise(function (resolve, reject) {
            ajax({
                url: constants.URL.INIT,
                data: {},
                success: (res) => {
                    let {success, data} = res;
                    if (success) {
                        that.state.transiTypeAry = data.billTypeAry;

                        if (data.msgs && data.msgs.length) {
                            showMessage(null, {content: data.msgs.join(','), color: 'warning'});
                        }

                        resolve(data);

                    } else {
                        reject(data);
                    }
                },
                error: (err) => {
                    showMessage(null, {content: err.message, color: 'danger'});
                    reject(err);
                }
            });
        });
        return promise;
    }

    //删除对话框 确认按钮
    delConfirm = () => {
        buttonClick.call(this, this.props, "deleteConfirm");
    };

    //处理其他单据联查到卡片到列表态
    linkedQueryHandler(props) {
        //获取联查场景
        let scene = props.getUrlParam(linkQueryConst.SCENE);
        if (scene === linkQueryConst.SCENETYPE.linksce) {
            let linkParam = cacheTools.get('fa_facard_linkQuery_param');
            let pks =  linkParam['pk_cards'];
            //删除缓存
            cacheTools.remove('fa_facard_linkQuery_param');
            if(pks && pks.length) {
                let param = {
                    allpks: pks,
                    pagecode: constants.PAGE_CODE.LIST,
                };
                ajax({
                    url: constants.URL.QUERY_LIST_BYPKS_LINKED,
                    data: param,
                    success: (res) => {
                        setListValue.call(this, props, res, tableId);
                        setBatchBtnsEnable.call(this, props, tableId);

                        //取出 pk2ClientKeyMap 并存到缓存中
                        let {success, data} = res;
                        if(success && data) {
                            cardCache.setDefData(constants.pk2ClientKeyMap, assetDataSource, JSON.parse(data.userjson));
                        }
                    },
                    error: (err) => {
                        listQueryBackHandler.error(props, err);
                    }
                });
            }
        }
    }

    render() {
        let {table, search, button, modal, ncmodal, pageConfig, ncUploader} = this.props;
        let {createSimpleTable} = table;
        let {NCCreateSearch} = search;
        let {createNCUploader} = ncUploader;
        let {createModal} = ncmodal;
        let {createButtonApp} = button;

        return (
            <div className="nc-bill-list">
                <div className='nc-bill-header-area'>
                    <div className='header-title-search-area'>
                        {createPageIcon()}
                        <h2 className='title-search-detail'>{pageConfig.title}</h2></div>
                    <div className="header-button-area">
                        {createButtonApp({
                            area: 'list_head',
                            buttonLimit: 10,
                            onButtonClick: buttonClick.bind(this),
                            popContainer: document.querySelector('.btn-group')
                        })}
                    </div>
                </div>
                <div className="nc-bill-search-area">
                    {NCCreateSearch(searchId, {
                        clickAdvBtnEve: clickAdvBtnEve.bind(this), //高级按钮点击前事件
                        clickSearchBtn: searchBtnClick.bind(this),
                        onAfterEvent: afterEvent.bind(this),
                    })}
                </div>
                <div className="nc-bill-table-area">
                    {createSimpleTable(tableId, {
                        dataSource: assetDataSource,
                        pkname: 'pk_cardhistory',
                        handlePageInfoChange: pageInfoClick,
                        onSelected: rowSelected.bind(this),
                        onSelectedAll: rowSelected.bind(this),
                        showIndex: true,
                        showCheck: true,
                        onRowDoubleClick: doubleClick.bind(this),
                        componentInitFinished: () => {
                            //缓存数据赋值成功的钩子函数
                            //若初始化数据后需要对数据做修改，可以在这里处理
                            rowSelected.call(this, this.props, tableId);
                        }
                    })}
                </div>

                {modal.createModal(constants.MODAL_ID.CopyModal, {
                    title: getMultiLangByID('201201504A-000026')/* 国际化处理： 卡片复制 */,
                    content: this.facardCopy.copyCardDialog(this),
                    userControl: true,
                })}
                {/* 附件*/}
                {createNCUploader('uploader', {})}
            </div>
        );
    }
}

export default List;


