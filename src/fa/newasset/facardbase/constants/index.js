import ampub from 'ampub';
const {commonConst} = ampub;
const { IBusiRoleConst } = commonConst.CommonKeys;

/**
 * 常量定义
 * @author zhangypm
 * @since ncc1.0 2018年5月18日
 */

const IS_DEV = false; //是否为开发阶段， 真是发布版本后这里是 false

// 界面状态
const UISTATE = {
	ADD: 'add', // 新增态
	EDIT: 'edit', // 编辑态
	BROWSE: 'browse', // 浏览态
	BLANK: 'blank' //卡片空白页
};

const PAGE_CODE = {
	LIST: '201201504A_list' //界面编码 【原始卡片录入】 使用了【固定资产卡片维护】的页面
};

//区域
const AREA = {
	LIST: {
		//列表态
		SEARCH_AREA: 'searchArea', // 查询区
		GRID_AREA: 'list_head' //列表区
	},
	CARD: {
		//卡片态
		ASSET_ALL: 'assetAll', // 包括了 fa_card\fa_cardhistory 的全部数据
		BASIC_INFO_AREA: 'asset', // 表头基本信息区域
		FINANCE_AREA: 'finance', // 财务区
		AUDITINFO_AREA: 'card_tailinfo', //审计
		ALTER_TAB: 'alter_tab', // 变动页签
		DEP_TAB: 'dep_tab', // 折旧页签
		USEDEPT_TAB: 'dept_tab', // 多使用部门
		SUBEQUIP_TAB: 'subequip_tab', //附属设备
		DEPLOY_TAB: 'deploy_tab', // 调拨记录页签
		EVALUATE_TAB: 'evaluate_tab', // 评估记录页签
		DEVALUE_TAB: 'devalue_tab', //  减值记录页签
		REDUCE_TAB: 'reduce_tab', //  减少记录页签
		CopyCard: 'copyCard' //  复制卡片
	}
};

// button 所属的区域
const BTN_AREA = {
	LIST_HEAD: 'list_head', //列表态肩部
	LIST_TABLE_HEAD: '',
	LIST_TABLE_INNER: 'list_inner', //列表态表体行
	CARD_HEAD: 'card_head', //卡片态肩部
	CARD_TABLE_HEAD: 'card_body', //卡片态表格肩部
	CARD_TABLE_INNER: 'inner' //卡片态表体行
};

//---------------------------------------------------
//  路径
const URL = {
	INIT: '/nccloud/fa/facard/init.do', // 获取初始化参数
	QUERY_LIST: '/nccloud/fa/facard/querylist.do', // 列表态查询
	QUERY_LIST_BYPKS: '/nccloud/fa/facard/querylistbypks.do', // 列表态查询(根据pks)
	QUERY_LIST_BYPKS_LINKED: '/nccloud/fa/facard/querylistbypksonlinked.do', // 联查场景下的列表态查询(根据pks)
	QUERY_CARD: '/nccloud/fa/facard/querygridbypks.do', // 根据pk查询卡片
	QUERY_CARD_LINKED: '/nccloud/fa/facard/querylinked.do', // 联查卡片
	SAVE: '/nccloud/fa/facard/insert.do', // 保存
	UPDATE: '/nccloud/fa/facard/update.do', // 更新
	DELETE: '/nccloud/fa/facard/delete.do', // 删除
	gettransitype: '/nccloud/fa/facard/gettransitype.do', // 交易类型
	headafteredit: '/nccloud/fa/facard/headafteredit.do', // 表头编辑后事件
	querytabdata: '/nccloud/fa/facard/querytabdata.do', // 查询表体页签数据
	EDIT: '/nccloud/fa/facard/edit.do', // 卡片编辑
	simulateDep: '/nccloud/fa/facard/simulateDep.do', // 卡片模拟折旧
	simulateAccuDep: '/nccloud/fa/facard/simulateAccuDep.do', // 卡片模拟累计折旧
	COPY: '/nccloud/fa/facard/copy.do', // 卡片复制
	PRINT_LIST: '/nccloud/fa/facard/printList.do', // 列表打印
	PRINT_CARD: '/nccloud/fa/facard/printCard.do', // 卡片态打印
	LockOrUnlockCard: '/nccloud/fa/facard/lockcard.do', // 加锁/解锁卡片
	QueryResourceid: '/nccloud/fa/fapub/queryResourceid.do', //联查凭证时获取resourceid
	OpenMsg: '/nccloud/fa/facard/openmessage.do' //打开i代办中心消息
};

//列表按钮
const LIST_BTNS = {
	HEAD: {
		DELETE: 'Delete', //删除
		COPY: 'Copy', //复制、
		Attachment: 'Attachment', //附件
		QueryAbout: 'QueryAbout', //联查(下拉按钮)
		QueryAboutCard: 'QueryAboutCard', //  联查 设备卡片
		QueryAboutVoucher: 'QueryAboutVoucher', //  联查 凭证
		QueryAboutBusiness: 'QueryAboutBusiness', //  联查 单据追溯
		PRINT: 'Print', //打印按钮组
		PRINT_CHILD: 'print_child', //打印-带副卡
		PRINT_NOCHILD: 'print_nochild', //打印-不带副卡
		PRINT_LIST: 'print_list', //打印清单
		//Preview: 'Preview',//预览
		Output: 'Output', // 输出
		REFRESH: 'Refresh' // 输出
	},
	TABLE_BODY: {
		EDIT: 'Edit', //修改
		COPY: 'Copy', //复制
		DELETE: 'Delete', //删除
		QueryAboutBusiness: 'QueryAboutBusiness' //单据追溯
	}
};

//卡片浏览态 buttons
const CARD_BROWER_BTNS = [ 'EditGroup', 'Edit', 'Delete', 'Copy', 'Attachment', 'More', 'PTSwitch', 'Refresh' ];

//卡片编辑态 buttons 包括表体肩部及行内部的按钮
// LineGroup：附属设备表肩
// LineGroup1：多使用部门表肩
const CARD_EDIT_BTNS = [
	'SaveGroup',
	'Cancel',
	'SimulateDep',
	'SimulateAccudep',
	'LineGroup',
	'LineGroup1',
	'DelLine',
	'DelLine1'
];

//卡片空白页 buttons
const CARD_BLANK_BTNS = [ 'AddGroup' ];

//卡片态按钮
const CARD_BTNS = {
	HEAD: {
		//头部按钮
		ADD: 'Add', //新增
		EDIT: 'Edit', //修改
		DELETE: 'Delete', //删除
		SAVE: 'Save', //保存
		SaveAdd: 'SaveAdd', //保存新增
		CANCEL: 'Cancel', //取消
		REFRESH: 'Refresh', //刷新
		COPY: 'Copy', //复制
		SimulateDep: 'SimulateDep', //模拟折旧
		SimulateAccudep: 'SimulateAccudep', //模拟累计折旧
		PRINT_CHILD: 'print_child', //打印-带副卡
		PRINT_NOCHILD: 'print_nochild', //打印-不带副卡
		//Preview: 'Preview',//预览
		Output: 'Output', // 输出
		QueryAboutCard: 'QueryAboutCard', //  联查 设备卡片
		QueryAboutVoucher: 'QueryAboutVoucher', //  联查 凭证
		FABillQueryAboutVoucher: 'FABillQueryAboutVoucher', //  联查 业务凭证
		QueryAboutBusiness: 'QueryAboutBusiness', //  联查 单据追溯
		Attachment: 'Attachment', //  附件
		FAReceiptShow: 'FAReceiptShow', //  影像查看
		FAReceiptScan: 'FAReceiptScan' //  影像扫描
	},
	TABLE_HEAD: {
		//表体肩部按钮
		USEDEPT_TAB: {
			//对应小应用注册中的区域为 card_body1
			ADD_LINE: 'AddLine1', //增行
			DELETE_LINE: 'DelLine1', //删行
			BATCH_ALTER: 'BatchAlter1' //批改
		},
		SUBEQUIP_TAB: {
			// 对应小应用注册中的区域为 card_body
			ADD_LINE: 'AddLine', //增行
			DELETE_LINE: 'DelLine', //删行
			//Line', //插入行
			BATCH_ALTER: 'BatchAlter' //批改
		},
		DEP_TAB: {
			// 对应小应用注册中的区域为 card_body2
			PTSwitch: 'PTSwitch' //图表切换
		}
	},
	TABLE_BODY: {
		//表体操作列
		USEDEPT_TAB: {
			//对应小应用注册中的区域为 card_body_inner1
			DELETE_LINE: 'DelLine1' //删行
		},
		SUBEQUIP_TAB: {
			// 对应小应用注册中的区域为 card_body_inner
			DELETE_LINE: 'DelLine' //删行
		}
	}
};

//数据状态
const DATA_STATUS = {
	NOCHANGED: 0, //未改变
	UPDATED: 1, //更新
	NEW: 2, //新增
	DELETED: 3 //删除
};

//模态框的名称
const MODAL_ID = {
	CopyModal: 'copyCardModal', //复制卡片的模态框
	ConfirmModal: 'confirmModal', //弹出确认框
};

//字段类型
const FIELDS = {
	HEAD_PK: 'pk_card', //表头主键字段
	BODY_PK: 'pk_cardhistory', //历史表主键
	PK_USEDEPT: 'pk_usedept', //使用部门字段
	PK_MANDEPT: 'pk_mandept', //管理部门字段
	USEDEP_FLAG: 'usedep_flag', //是否多使用部门
	/**
         * 卡片历史表中的财务字段（不同账簿其值会不同）:
         * 本币原值，进项税，是否抵扣进项税，累计折旧，本年折旧，减值准备，月折旧额，月折旧率，净残值，净残值率，单位折旧，净值，净额
         * 折旧方法、使用月限、使用月限显示、已使用月份、折本汇率、折旧承担部门
         * 注意：6.0把折旧承担部门修改为支持账簿级参数，那么它就在不同的账簿中值不同
         */
	ACCOUNT_FIELD: [
		'localoriginvalue',
		'tax_input',
		'taxinput_flag',
		'accudep',
		'curryeardep',
		'predevaluate',
		'depamount',
		'deprate',
		'salvage',
		'salvagerate',
		'depunit',
		'netvalue',
		'netrating',
		'pk_depmethod',
		'servicemonth',
		'servicemonth_display',
		'dep_start_date',
		'dep_end_date',
		'naturemonth',
		'naturemonth_display',
		'usedmonth',
		'localcurr_rate',
		'paydept_flag'
	],
	/**
         * 卡片历史表中的业务字段(在多账簿数据中其值都是一样的): 集团、组织、原币原值、使用部门、是否多使用部门、管理部门、数量、资产类别、项目档案、使用状况、 卡片是否新增、是否最新状态,
         * 工作总量、累计工作量、月工作量、成本中心、货主管理组织, 工作量单位
         */
	BUSINESS_FIELD: [
		'pk_group',
		'pk_org',
		'pk_org_v',
		'originvalue',
		'pk_usedept',
		'pk_usedept_v',
		'usedep_flag',
		'pk_mandept',
		'pk_mandept_v',
		'card_num',
		'pk_category',
		'pk_jobmngfil',
		'pk_usingstatus',
		'newasset_flag',
		'laststate_flag',
		'allworkloan',
		'accuworkloan',
		'monthworkloan',
		'pk_equiporg',
		'pk_equiporg_v',
		'pk_costcenter',
		'pk_ownerorg',
		'pk_ownerorg_v',
		'workloanunit'
	],
	/**
         * 编辑后不需要后台处理的字段，即没有联动关系的字段
         * 资产名称、条形码、规格、型号、管理部门、增加方式、使用状况、存放地点
         */
	NORELATION_FIELD: [
		'asset_name',
		'bar_code',
		'card_model',
		'spec',
		'pk_mandept',
		'pk_mandept_v',
		'pk_addreducestyle',
		'pk_usingstatus',
		'position',
		'asset_code',
		'card_code'
	],
	/**
         * 在业务区，实际上属于财务字段（不同账簿其值会不同）
         */
	ACCOUNT_FIELD_IN_BASIC: [ 'originvalue', 'localcurr_rate', 'currmoney' ],
	/**
         * 卡片有后续操作后，不能修改的卡片业务字段。
         */
	unEditCardFileAfterUsed: [
		/** 集团 */
		'pk_group',
		/** 业务单元 */
		'pk_org',
		'pk_org_v',
		/** 利润中心 */
		'pk_raorg',
		'pk_raorg_v',
		/** 核算账簿 */
		'pk_accbook',
		/** 单据类型 */
		'bill_type',
		/** 卡片编码 */
		'card_code',
		/** 资产编码 */
		'asset_code',
		/** 开始使用日期 */
		'begin_date',
		/** 建卡日期 */
		'business_date',
		/** 保修截止日期 */
		'close_date',
		/** 交易类型 */
		'transi_type',
		/** 币种 */
		'pk_currency',
		/** 使用人 */
		'pk_assetuser',
		/** 增加方式 */
		'pk_addreducestyle',
		/** 递延资产标志 */
		'dy_flag',
		/** 资产套号 */
		'assetsuit_code',
		/** 供应商 */
		'provider',
		/** 工作中心 */
		'workcenter',
		/** 卡片来源 */
		'bill_source',
		/** 购买价款 */
		'currmoney',
		/** 资产组 */
		'pk_assetgroup',
		/** 评估值 */
		'revalued_amount',
		/** 相关税费 */
		'tax_cost',
		/** 弃置费用 */
		'dismant_cost',
		/** 其他费用 */
		'other_cost',
		/** 安装调试费 */
		'install_fee',
		/** 车辆牌照号 */
		'license',
		/** 净吨位 */
		'nettonnage',
		/** 资金来源 */
		'fundorigin',
		/** 土地级别 */
		'soillevel',
		/** 记录单位 */
		'measureunit',
		/** 土地面积 */
		'soilarea',
		/** 建筑面积 */
		'archarea',
		/** 建[座]数 */
		'quantity',
		/** 建筑承包商 */
		'contrator',
		/** 电机功率 */
		'machinepower',
		/** 电机数量 */
		'machinequan',
		/** 出厂日期 */
		'leave_date',
		/** 生产厂商 */
		'producer',
		/** 设备使用权 */
		'pk_equiporg',
		'pk_equiporg_v',
		/** 设备使用部门 */
		'pk_equip_usedept',
		'pk_equip_usedept_v',
		/** 卡片数量 */
		'card_num',
		/** 工作中心 */
		'pk_jobmngfil',
		/** 工作量单位 */
		'workloanunit'
	],
	/**
         * 自定义项标志
         */
	DEF_FIELD: 'def',
	/**
         * 日期类型的字段
         */
	DATE_TYPE_FIELDS: [ 'begin_date', 'close_date', 'business_date', 'dep_start_date', 'dep_end_date' ]
};

//单据来源类型
const BILL_SOURCE_TYPES = {
	/** 1 手工录入 */
	handin_src: 'handin',
	/** 2 新增审批单据 */
	appBill_src: 'HN',
	/** 3 调入单据 */
	deploy_in_src: 'HD',
	/** 4 拆分单据 */
	split_src: 'HM',
	/** 5 合并单据 */
	conbin_src: 'HO',
	/** 6 盘盈资产 */
	inventory_src: 'HS',
	/** 7 设备卡片 */
	equip_src: '4A00',
	/** 8 转固单 */
	transasset_src: 'HJ',
	/** 8.1 采购转固单 */
	transasset_purchase_src: 'HJ-02',
	/** 8.2 工程转固单 */
	transasset_project_src: 'HJ-01'
};

//资产的增加方式
const ASSET_ADDED_WAY = {
	/** 录入原始卡片 */
	isOld: 0,
	/** 新增资产 */
	isNew: 1,
	/** 不是这个月的新增资产 */
	notAddThisMonth: 2,
	/** 以前是原始卡片 */
	oldBefore: 10,
	/** 以前是新增资产 */
	newBefore: 11
};

//浏览态默认收起的页签
const TabsHideDefault = [
	AREA.CARD.ALTER_TAB,
	AREA.CARD.EVALUATE_TAB,
	AREA.CARD.REDUCE_TAB,
	AREA.CARD.DEVALUE_TAB,
	AREA.CARD.DEPLOY_TAB,
	AREA.CARD.DEP_TAB,
	AREA.CARD.SUBEQUIP_TAB
];

//页签数据和账簿有关系的页签
const ACCBOOK_TABS = [
	AREA.CARD.DEP_TAB,
	AREA.CARD.ALTER_TAB,
	AREA.CARD.EVALUATE_TAB,
	AREA.CARD.REDUCE_TAB,
	AREA.CARD.DEVALUE_TAB
];

const CARD_ALL_TABS = [
	AREA.CARD.ALTER_TAB,
	AREA.CARD.EVALUATE_TAB,
	AREA.CARD.REDUCE_TAB,
	AREA.CARD.DEVALUE_TAB,
	AREA.CARD.DEPLOY_TAB,
	AREA.CARD.DEP_TAB,
	AREA.CARD.SUBEQUIP_TAB,
	AREA.CARD.USEDEPT_TAB
];

//参照过滤配置
const ReferFilterConfig = {
	searchId: AREA.LIST.SEARCH_AREA,
	formId: AREA.CARD.BASIC_INFO_AREA,
	bodyIds: [ AREA.CARD.USEDEPT_TAB ],

	specialFields: {
		pk_dept: {
			//使用部门
			data: [
				{
					fields: [ 'pk_equiporg', 'pk_org', 'pk_org_v' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		pk_equip_usedept: {
			//设备使用部门 PK_EQUIP_USEDEPT 参照 使用权
			data: [
				{
					fields: [ 'pk_equiporg' ],
					returnName: 'pk_org'
				}
			]
		},
		pk_equip_usedept_v: {
			//设备使用部门 PK_EQUIP_USEDEPT 参照 使用权
			data: [
				{
					fields: [ 'pk_equiporg' ],
					returnName: 'pk_org'
				}
			]
		},
		pk_jobmngfil: {
			//项目档案 参照集团+主组织下的项目
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		},
		asset_group: {
			//资产组参照
			RefActionExtType: 'GridRefActionExt',
			class: 'nccloud.web.fa.newasset.facard.refcondition.AssetGroupRefFilter',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		},
		workcenter: {
			//工作中心
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		},
		provider: {
			// 供应商
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		}
	}
};

//参照联动配置
const ReferLinkageClearConfig = {
	//查询区
	searchId: AREA.LIST.SEARCH_AREA,
	formId: AREA.CARD.BASIC_INFO_AREA
};

//列表卡片缓存的key, 同于单页中卡片列表之间的数据同步
const assetDataSource = 'fa.newasset.facard.asset';

//自定义缓存的key, 用于缓存交易类型、模板等数据
const templateDataSource = 'fa.newasset.facard.template';

//pk_cardhistory->clintKey的映射关系在缓存中的key
const pk2ClientKeyMap = 'pk2ClientKeyMap';

const constant = {
	IS_DEV,
	UISTATE,
	AREA,
	BTN_AREA,
	URL,
	LIST_BTNS,
	CARD_BTNS,
	CARD_BROWER_BTNS,
	CARD_EDIT_BTNS,
	CARD_BLANK_BTNS,
	FIELDS,
	DATA_STATUS,
	ACCBOOK_TABS,
	BILL_SOURCE_TYPES,
	ASSET_ADDED_WAY,
	MODAL_ID,
	ReferFilterConfig,
	ReferLinkageClearConfig,
	assetDataSource,
	templateDataSource,
	PAGE_CODE,
	TabsHideDefault,
	pk2ClientKeyMap,
	CARD_ALL_TABS
};

export default constant;
