import constants from "../../constants";

const basicInfoForm = constants.AREA.CARD.BASIC_INFO_AREA;
const financeForm = constants.AREA.CARD.FINANCE_AREA;

//折旧方法关联的字段处理
export function updateDeptmethodItem(props, accbookData4Form) {
    let depmethod = accbookData4Form.rows[0].values.pk_depmethod;
    if (depmethod && depmethod.value) {
        let depmethod2 = this.depmethods.filter((m) => m.pk_depmethod === depmethod.value);
        if (depmethod2.length) {
            depmethod2 = {
                code: depmethod2[0]['depmethod_code']
            };
            depmethodItemModified(props, depmethod2);
        }
    }
}

export function depmethodItemModified(props, depmethod) {
    //@zhangypm 实际上这里最好的处理方式是在模板初始化时记录工作量字段的可编辑性、必输性
    if (depmethod.code === '04') {//这是工作量法
        props.form.setFormItemsRequired(financeForm,
            {'allworkloan': true, 'accuworkloan': true, 'workloanunit': true});

        props.form.setFormItemsDisabled(financeForm,
            {'allworkloan': false, 'accuworkloan': false, 'workloanunit': false});
    } else {
        props.form.setFormItemsRequired(financeForm,
            {'allworkloan': false, 'accuworkloan': false, 'workloanunit': false});
        props.form.setFormItemsDisabled(financeForm,
            {'allworkloan': true, 'accuworkloan': true, 'workloanunit': true});
    }
}

//资产组 字段设置
export function updateAssetGroupItem(props, isHeadAssetFinanceorg) {
    //参照过滤
    let meta = props.meta.getMeta();
    meta[basicInfoForm].items.map((item) => {
        // 资产组
        if (item.attrcode === 'pk_assetgroup') {
            item.isCacheable = false;
            item.queryCondition = () => {
                return {
                    notShowGroupFlag: isHeadAssetFinanceorg
                }
            };
        }
    });
    props.meta.setMeta(meta);
}
