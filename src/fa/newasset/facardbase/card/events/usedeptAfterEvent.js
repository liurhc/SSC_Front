/**
 * 多使用部门编辑后事件
 */
import {ajax, toast} from 'nc-lightapp-front';

import fa from 'fa';

const {fa_utils} = fa;

const {metaUtil} = fa_utils;

export default function (props, moduleId, field, newValue, changedrows, index, record, type, method) {
    if ('pk_dept' === field) {
        let currRows = props.cardTable.getVisibleRows(moduleId);
        let emptyRowData = metaUtil.genEmptyRowDataByMeta(props, moduleId);

        let newRows = [];
        let curr_depts = props.cardTable.getColValue(moduleId, "pk_dept");
        for (let usedept of newValue) {
            let isExisted = curr_depts.some((item) => {
                return item.value === usedept.refpk;
            });
            if (!isExisted) {
                let newRow = JSON.parse(JSON.stringify(emptyRowData));
                newRow.status = 2;
                newRow.values['pk_dept'] = {display: usedept.refcode, scale: -1, value: usedept.refpk, isEdit: false};
                newRow.values['pk_dept.name'] = {display: usedept.refname, scale: -1, value: usedept.refname};
                newRows.push(newRow);
            }
        }
        if (newValue.length > 1) {//如果是多选，需要把当前行移除
            currRows.splice(index, 1);
        }
        currRows = currRows.concat(newRows);
        props.cardTable.setTableData(moduleId, {rows: currRows}, null, false);//isCache设置为false，以便取消时回退数据
    }
}