/*
* 账簿数据的处理
* */
import {ajax} from 'nc-lightapp-front';

import ampub from 'ampub';

const {utils} = ampub;

const {showMessage} = utils.msgUtils;
const { getMultiLangByID } = utils.multiLangUtils;

import {updateDeptmethodItem} from './utils';
import constants from '../../constants';
import afterEvent from './afterEvent';

const basicInfoForm = constants.AREA.CARD.BASIC_INFO_AREA;
const financeForm = constants.AREA.CARD.FINANCE_AREA;

//财务区的显示/隐藏处理
function toggleFinanceArea() {
    let that = this;
    // 如果没有账簿 直接返回
    if (!this.state.accbooks.length) {
        /*国际化处理：没有可用的资产账簿*/
        showMessage( this.props, {content: getMultiLangByID('201201504A-000003'), color: 'info'});
        return;
    }

    //即将移除财务区
    if (that.state.financeShowFlag) {
        // 移除财务区，获取form的值并存下来，防止丢失了用户的输入数据
        if (!accbookSwitchBefore(that)) {//校验失败，不允许修改
            return;
        }
    }

    const status = that.props.getUrlParam('status');
    this.setState(
        {financeShowFlag: !this.state.financeShowFlag},
        () => {
            let currentFlag = that.state.financeShowFlag;
            if (currentFlag) { //显示财务区， 给财务区的 form 设值
                const accbookData = that.getAccbookDataByPkaccbook4Form(that.state.currentPkaccbook);
                that.props.form.setAllFormValue({
                    [financeForm]: accbookData,
                });
                if (status && Object.values(constants.UISTATE).includes(status)) {
                    that.props.form.setFormStatus(financeForm, status);
                }

                //更新工作量字段的编辑性
                updateDeptmethodItem.call(that, that.props, accbookData);

            }
        });
}

/**
 * 账簿切换前
 * @param that  为卡片入口页的 this 对象
 */
function accbookSwitchBefore(that) {
    const status = that.props.getUrlParam('status');
    if (!status) {
        /*国际化处理：访问地址错误，请重新打开节点*/
        showMessage(that.props, {content: getMultiLangByID('201201504A-000006'), color: 'danger'});
        return false;
    }
    //如果是编辑态，在切换按钮之前需要获取form的值并存下来，防止丢失了用户的输入数据
    if (status === constants.UISTATE.EDIT || status === constants.UISTATE.ADD) {

        //编辑态的时候 校验业务区、财务区的必输项，如果有未填写的，不允许切换账簿
        // 这里有一个隐藏的问题就是：如果模板上设置了必输项，但是visible=false, 将导致无法在界面上看到必输项的校验结果
        //这是平台校验方法隐藏的一个问题（截止到2018-6-20）
        let isCheck = that.props.form.isCheckNow([basicInfoForm, financeForm]);
        if (isCheck) {
            that.syncDataForCommit();
        }
        return isCheck;
    }

    return true;
}

//账簿按钮切换处理
function accbookSwitchHandler(accbook) {
    let that = this;
    let validate = accbookSwitchBefore(that);
    if (!validate) {//如果没有验证通过，不允许切换账簿
        return;
    }
    this.setState({
            currentPkaccbook: accbook.pk_accbook,
            depEchartsHide: true,
            dep_tab_style: 'table',
        },
        () => {
            //更新界面数据
            let {currentPkaccbook} = that.state;
            // 更新basicInfoForm数据
            that.basicInfoData = that.getAccbookDataByPkaccbook(currentPkaccbook);
            const basicInfoData4Form = that.getBasicInfoData4Form();

            // 更新财务区数据
            const accbookData = that.getAccbookDataByPkaccbook4Form(currentPkaccbook);
            that.props.form.setAllFormValue({
                [financeForm]: accbookData,
                [basicInfoForm]: basicInfoData4Form,
            });

            // 账簿变化，需要处理对应的折旧方法， 走一下编辑后事件
            // 参考：nc nc.ui.fa.asset.event.accbook.HeadDataChangeEventHandler
            const status = that.props.getUrlParam('status');
            if (status === constants.UISTATE.EDIT || status === constants.UISTATE.ADD) {
                let newValue = this.props.form.getFormItemsValue(financeForm, 'pk_depmethod');
                afterEvent.call(that, that.props, financeForm, "pk_depmethod", newValue, null, null, null, true);
            }

            //收起与账簿相关的页签，因为页签的数据是懒加载的
            const accbook_tabs = constants.ACCBOOK_TABS;
            that.props.cardTable.toggleCardTable(accbook_tabs, false);
        });
}

export {
    toggleFinanceArea,
    accbookSwitchHandler,
}
