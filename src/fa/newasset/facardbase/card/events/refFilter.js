/*
* 参照控制
* */

import constants from '../../constants';
const basicInfoForm = constants.AREA.CARD.BASIC_INFO_AREA;
const usedept_tab = constants.AREA.CARD.USEDEPT_TAB;

/*
* 更新部门是否能够选择非末级
* */
function updateDeptRefLevel(that, is_allow_dept_midlev_new) {
    if (is_allow_dept_midlev_new === undefined || is_allow_dept_midlev_new === null) {
        return;
    }
    let is_allow_dept_midlev_old = that.is_allow_dept_midlev;
    if (is_allow_dept_midlev_new != is_allow_dept_midlev_old) {
        that.is_allow_dept_midlev = is_allow_dept_midlev_new;
        let meta = that.props.meta.getMeta();
        meta[basicInfoForm].items.map((item) => {
            // 管理部门
            if (item.attrcode === 'pk_mandept' || item.attrcode === 'pk_mandept_v') {
                item.onlyLeafCanSelect = !is_allow_dept_midlev_new;
            }
            // 使用部门
            if (item.attrcode === 'pk_usedept' || item.attrcode === 'pk_usedept_v') {
                item.onlyLeafCanSelect = !is_allow_dept_midlev_new;
            }
        });

        //更新多使用部门页签
        meta[usedept_tab].items.map((item, key) => {
            if (item.attrcode == 'pk_dept' || item.attrcode == 'pk_dept_v') {
                item.onlyLeafCanSelect = !is_allow_dept_midlev_new;
            }
        });

        that.props.meta.setMeta(meta);
    }
}


export {
    updateDeptRefLevel,
}