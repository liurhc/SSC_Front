import constants from "../../constants";

const basicInfoForm = constants.AREA.CARD.BASIC_INFO_AREA;
const financeForm = constants.AREA.CARD.FINANCE_AREA;
/**
 * 业务区、财务区 的编辑前事件
 */

//与 资产类别 相关的字段
let categoryFiles = [
    'pk_depmethod',
    'naturemonth',
    'servicemonth',
    'salvagerate',
    'taxinput_flag',
    'naturemonth_display',
    'servicemonth_display'
];

/**
 * 工作量法相关的字段（不包括折旧方法）
 */
let workLoadFields = [
    /** 工作总量 */
    'allworkloan',
    /** 累计工作量 */
    'accuworkloan',
    /** 月工作量 */
    'monthworkloan',
    /** 单位折旧 */
    'depunit',
    /** 工作量单位 */
    'workloanunit'
];


//表单的编辑前事件
//props, moduleId(区域id), key(操作的键), value（当前值）,data(当前表单所有值)
export default function (props, moduleId, key, value, data) {

    let that = this;
    //有一个规则不满足，就返回 false
    let funAry = [orgEmptyBeforeRule, orgBeforeEdit,
        currRateBeforeEdit, categoryBeforeEdit,
        businessFieldBeforeEdit, accountFieldBeforeEdit, workloanFieldEdit,
        assetCodeBeforeEdit, depDateBeforeEdit, pkcostcenterBeforeEdit];
    let hasOne = funAry.some((f) => !f.call(that, props, moduleId, key, value, data));

    // console.log(" ------------   编辑前检验结果 = " + !hasOne);
    /*
    日期类型字段的特殊处理,由于平台在编辑前事件返回false的情况下无法禁止日期类型控件的
    编辑（截止2018年10月19日），所以我们做了这个处理，如果平台支持可以将其删除；
    需要注意在点击编辑按钮的时候，将这些字段得编辑性设为可编辑   */
    if (hasOne && constants.FIELDS.DATE_TYPE_FIELDS.includes(key)) {
        props.form.setFormItemsDisabled(moduleId, {[key]: hasOne});
    }

    return !hasOne;

}

//财务组织字段为空， 除自己外其他字段不能编辑
function orgEmptyBeforeRule(props, moduleId, key, value, data) {
    // console.log('------------  orgEmptyBeforeRule -------------');
    let org = props.form.getFormItemsValue(basicInfoForm, 'pk_org');
    if (!(org && org.value)) {//org is empty
        return (key === 'pk_org' || key === 'pk_org_v');
    }
    return true;
}

//组织编辑前， 需要返回 true/false
function orgBeforeEdit(props, moduleId, key, value, data) {
    // console.log('------------  orgBeforeEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = ['pk_org', 'pk_org_v'];
    if (!fieldToCheck.includes(key) || moduleId !== basicInfoForm) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    const status = props.getUrlParam('status');
    //编辑态，表头财务组织字段不可编辑
    if (status === constants.UISTATE.EDIT) {
        return false;
    }

    return true;
}

//折本汇率编辑前  需要返回 true/false
function currRateBeforeEdit(props, moduleId, key, value, data) {
    // console.log('------------  currRateBeforeEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = ['localcurr_rate'];
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    if (!this.state.accbooks || !this.state.accbooks.length) {
        return false;
    }

    let pk_currency = data.pk_currency && data.pk_currency.value;
    if (!pk_currency) {
        return false;
    }

    //获取当前账簿的币种
    let pk_currency_book = this.accbookCurrency[this.state.currentPkaccbook];
    if (!pk_currency_book) {
        // console.log('------ 这里发生了异常,未获取到账簿的币种 ------');
        return false;
    }

    return (pk_currency_book !== pk_currency);
}

//资产类别相关字段
function categoryBeforeEdit(props, moduleId, key, value, data) {
    // console.log('------------  categoryBeforeEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = categoryFiles;
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    //FA10 账簿信息设置中资产类别中的参数在卡片中是否可修改
    //说明: 在账簿信息中资产类别设置的参数带到卡片上时，是否允许手工修改；
    let fa10 = this.fa10param[this.state.currentPkaccbook];
    return !!fa10;
}

//业务字段编辑前判断 ： 卡片上的业务字段+加上历史表中的业务字段
function businessFieldBeforeEdit(props, moduleId, key, value, data) {
    // console.log('------------  businessFieldBeforeEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    let businessFieldSet = new Set(constants.FIELDS.unEditCardFileAfterUsed);
    constants.FIELDS.BUSINESS_FIELD.map((f) => {
        businessFieldSet.add(f);
    });
    let fieldToCheck = [...businessFieldSet];
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    // 新增状态（没有卡片主键），不需要控制
    const status = props.getUrlParam('status');
    if (status === constants.UISTATE.ADD) {
        return true;
    }

    /**
     * 设置业务字段的可编辑性：只要有一个账簿有后续操作，那么业务字段就不能再修改。
     */
    if (!this.editAbleBook || !this.editAbleBook.length) {
        return false;
    }

    let isAllow = this.state.accbooks.some((book) =>
        !this.editAbleBook.includes(book.pk_accbook)
    );

    return !isAllow.length;
}

//财务字段编辑前判断
function accountFieldBeforeEdit(props, moduleId, key, value, data) {
    // console.log('------------  accountFieldBeforeEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = constants.FIELDS.ACCOUNT_FIELD;
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    // 新增状态（没有卡片主键），不需要控制
    const status = props.getUrlParam('status');
    if (status === constants.UISTATE.ADD) {
        return true;
    }

    //没有可编辑的账簿
    if (!this.editAbleBook || !this.editAbleBook.length) {
        return false;
    }

    return this.editAbleBook.includes(this.state.currentPkaccbook);
}

//工作量法字段编辑前处理
function workloanFieldEdit(props, moduleId, key, value, data) {
    // console.log('------------  workloanFieldEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = workLoadFields;
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    let pk_depmethod = data.pk_depmethod && data.pk_depmethod.value;
    if (!pk_depmethod) {
        return false;
    }
    let methods = this.depmethods.filter((m) => {
        return m.pk_depmethod === pk_depmethod;
    });
    if (!methods.length) {
        // console.log('------ 理论上这是不会发生的，请检查折旧方法是否未启用，或者已经被删除------');
        return false;
    }
    let depmethod_code = methods[0].depmethod_code;//?????
    // 取出折旧方法的编码进行比较：（因为在预置数据时，没有办法把主键预置到数据库）
    if (depmethod_code === '04') {//工作量法 预置数据的编码
        return true;
    } else {// 折旧方法中的公式中存在该字段，那么可以编辑

        let workloanFildes = this.depmethodFormulaFieldMap[pk_depmethod];
        if (!workloanFildes) {
            // console.log('------ 折旧方法不存在工作量字段不能编辑 ------');
            return false;
        }
        if (workloanFildes.includes(key)) {
            return true;
        }
    }

    return false;
}

//资产编码、卡片编号
function assetCodeBeforeEdit(props, moduleId, key, value, data) {
    // console.log('------------  assetCodeBeforeEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    //let fieldToCheck = ['asset_code', 'card_code'];
    let fieldToCheck = ['card_code'];
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    return false;
}

//折旧开始日期编辑前处理
function depDateBeforeEdit(props, moduleId, key, value, data) {
    // console.log('------------  depDateBeforeEdit -------------');

    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = ['dep_start_date', 'dep_end_date'];
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    let pk_depmethod = data.pk_depmethod && data.pk_depmethod.value;
    if (!pk_depmethod) {
        return false;
    }
    let methods = this.depmethods.filter((m) => {
        return m.pk_depmethod === pk_depmethod;
    });
    if (!methods.length) {
        // console.log('------ 理论上这是不会发生的，请检查折旧方法是否未启用，或者已经被删除');
        return false;
    }
    let isDay = methods[0].isdaydep_flag;//???? 日折旧方法不可编辑
    return !isDay;

}


//集团级业务参数控制的字段的可编辑性
function pkcostcenterBeforeEdit(props, moduleId, key, value, data) {
    // console.log('------------  pkcostcenterBeforeEdit -------------');

    //集团级参数FA83--支持多成本中心分摊
    // 否（默认值）：	基本信息的成本中心为可编辑
    //是：	基本信息的成本中心为不可编辑，自动取使用部门对应的成本中心，卡片页面上显示为空
    //1、字段不在该规则中时，直接返回 true
    let fieldToCheck = ['pk_costcenter'];
    if (!fieldToCheck.includes(key)) {
        return true;
    }

    //2、字段在该规则中时，执行如下校验
    return !this.fa83param

}


