import {cardCache} from 'nc-lightapp-front';
import constants from "../../constants";
import initTemplate from "./initTemplate";
import setStatus from "./setStatus";

const {assetDataSource} = constants;

//翻页处理
export default function (props, pk) {
    const that = this;

    //清空表体数据
    this.clearBodies();

    if (!pk) {
        setStatus.call(that, props, constants.UISTATE.BLANK);
        return;
    }

    // 更新参数
    props.setUrlParam({
        id: pk,
        status: constants.UISTATE.BROWSE
    });

    let pk_card_new = this.getPkcardFromUrl();
    let pk_accbook_new = this.getPkaccbookByPkcardHis();

    if (pk_card_new === this.curr_pk_card) {
        //新的卡片与当前卡片相同，只是账簿不同，切换账簿即可
        this.setState({
            currentPkaccbook: pk_accbook_new
        });
        return;
    }
    let assetData = undefined;
    this.state.currentPkaccbook = pk_accbook_new;

    loadDataByPk(this, pk_card_new).then((data) => {
        assetData = data;
        if (data.heads && data.heads.length) {
            const transiType = that.getHeadAttributeValue(data, 'transi_type');
            const pk_transitype = that.getHeadAttributeValue(data, 'pk_transitype');
            props.setUrlParam({transiType, pk_transitype});
            return initTemplate.call(that, props);
        }
    }).then(() => {
        that.setValue(assetData);
        this.updateAssetCache(assetData, 'update');
    });
}

function loadDataByPk(that, pkcard) {
    return new Promise(function (resolve, reject) {
        let cacheData = cardCache.getDefData(pkcard, assetDataSource);
        if (cacheData) {
            resolve(cacheData);
        } else {
            resolve(that.getData(pkcard));
        }
    });

}
