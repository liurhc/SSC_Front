/**
 * 附属设备页签的按钮处理
 */
import constants from "../../constants/index";
import {setBodyBtnsEnable} from './setStatus';

import ampub from 'ampub';
const {utils} = ampub;
const {showMessage} = utils.msgUtils;
const { getMultiLangByID } = utils.multiLangUtils;

import fa from 'fa';
const {fa_utils} = fa;
const {metaUtil} = fa_utils;

const subequip_tab = constants.AREA.CARD.SUBEQUIP_TAB;

export default function buttonClick(props, id) {
    const that = this;
    switch (id) {
        //增行
        case constants.CARD_BTNS.TABLE_HEAD.SUBEQUIP_TAB.ADD_LINE:
            doAddLine.call(that);
            break;

        //删行
        case constants.CARD_BTNS.TABLE_HEAD.SUBEQUIP_TAB.DELETE_LINE:
            doDeleteLine.call(that);
            break;

        //批改
        case constants.CARD_BTNS.TABLE_HEAD.SUBEQUIP_TAB.BATCH_ALTER:
            doBatchAlter.call(that);
            break;

        default:
            break;
    }
}

function doAddLine() {
    this.props.cardTable.addRow(subequip_tab);

}

function doDeleteLine() {
    let rowsWillDel = this.props.cardTable.getCheckedRows(subequip_tab);
    let indexs = rowsWillDel.map((row) => row.index);
    this.props.cardTable.delRowsByIndex(subequip_tab, indexs);
    setBodyBtnsEnable(this.props, subequip_tab);
}

function doBatchAlter() {
    const colAllowedAlter = metaUtil.getAreaItemFields(this.props, subequip_tab);
    let index = colAllowedAlter.findIndex((e, i) => {
        return e === 'pk_subequip'
    });
    if (index > -1) {
        colAllowedAlter.splice(index, 1);
    }

    let changeData = this.props.cardTable.getTableItemData(subequip_tab);
    const field = changeData.batchChangeKey;
    if (colAllowedAlter.includes(field)) {
        //1、调用平台方法进行批改
        const lastEditField = this.props.cardTable.batchChangeTableData(subequip_tab);
    } else {
        /*国际化处理：该列不支持批改！*/
        showMessage(this.props, {content: getMultiLangByID('201201504A-000024') , color: 'info'});
    }
}
