//设置界面状态

import constants from "../../constants";

const basicInfoForm = constants.AREA.CARD.BASIC_INFO_AREA;
const financeForm = constants.AREA.CARD.FINANCE_AREA;

import ampub from 'ampub';
const {utils} = ampub;

const {setHeadAreaData} = utils.cardUtils;

//切换按钮状态
export default function setStatus(props, status) {

    let {ADD, EDIT, BROWSE, BLANK} = constants.UISTATE;
    if (status) {
        props.setUrlParam({status});
    }
    status = status || props.getUrlParam('status');

    const browseBtns = constants.CARD_BROWER_BTNS;
    const editBtns = constants.CARD_EDIT_BTNS;
    const blankBtns = constants.CARD_BLANK_BTNS;

    //按钮的显示状态
    if (status === BLANK) {
        this.resetOnAdd();

        props.button.setButtonVisible(browseBtns, false);
        props.button.setButtonVisible(editBtns, false);
        props.button.setButtonVisible(blankBtns, true);

    } else if (status === EDIT || status === ADD) {//编辑态
        props.button.setButtonVisible(browseBtns, false);
        props.button.setButtonVisible(editBtns, true);
        //props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);

        //多使用部门页签按钮及编辑性控制
        let usedep_flag = props.form.getFormItemsValue(basicInfoForm, 'usedep_flag').value;
        //表头使用部门字段控制
        props.form.setFormItemsDisabled(basicInfoForm, {pk_usedept: !!usedep_flag});
        // 表头使用部门默认是否可空
        props.form.setFormItemsRequired(basicInfoForm, {pk_usedept: !usedep_flag});
        // 多使用部门页签上的按钮及编辑性
        let btns = Object.values(constants.CARD_BTNS.TABLE_HEAD.USEDEPT_TAB);
        props.button.setButtonVisible(btns, !!usedep_flag);

        if(status === ADD) {//新增态 控制附属设备页签按钮可见，编辑态 在编辑按钮后控制
            let btns = Object.values(constants.CARD_BTNS.TABLE_HEAD.SUBEQUIP_TAB);
            props.button.setButtonVisible(btns, true);
        }

    } else {//浏览态
        props.button.setButtonVisible(browseBtns, true);
        props.button.setButtonVisible(editBtns, false);

        //联查 设备卡片按钮
        const pk_equip = props.form.getFormItemsValue(basicInfoForm, 'pk_equip').value;
        props.button.setButtonDisabled(constants.CARD_BTNS.HEAD.QueryAboutCard, !pk_equip);

    }
    props.form.setFormStatus(basicInfoForm, [ADD, EDIT].includes(status) ? EDIT : BROWSE);
    props.form.setFormStatus(financeForm, [ADD, EDIT].includes(status) ? EDIT : BROWSE);
    props.cardTable.setStatus(constants.AREA.CARD.SUBEQUIP_TAB, [ADD, EDIT].includes(status) ? EDIT : BROWSE);
    props.cardTable.setStatus(constants.AREA.CARD.USEDEPT_TAB, [ADD, EDIT].includes(status) ? EDIT : BROWSE);

    //设置标题区域状态
    setHeadAreaData.call(this, props, {status});

    //控制表体页签肩部按钮
    setBodyBtnsEnable(props, constants.AREA.CARD.SUBEQUIP_TAB);
    setBodyBtnsEnable(props, constants.AREA.CARD.USEDEPT_TAB);
}


/**
 * 设置表体肩部按钮是否可用
 * @param {*} props
 * @param {*} moduleId
 */
export function setBodyBtnsEnable(props, moduleId) {
    let pk_org = props.form.getFormItemsValue(basicInfoForm, 'pk_org');
    let btns = {
        addLine: 'AddLine',
        delLine: 'DelLine',
        batchAlter: 'BatchAlter'
    };
    if (moduleId === constants.AREA.CARD.USEDEPT_TAB) {//多使用部门
        btns.addLine = constants.CARD_BTNS.TABLE_HEAD.USEDEPT_TAB.ADD_LINE;
        btns.delLine = constants.CARD_BTNS.TABLE_HEAD.USEDEPT_TAB.DELETE_LINE;
        btns.batchAlter = constants.CARD_BTNS.TABLE_HEAD.USEDEPT_TAB.BATCH_ALTER;

    } else if (moduleId === constants.AREA.CARD.SUBEQUIP_TAB) {//附属设备
        btns.addLine = constants.CARD_BTNS.TABLE_HEAD.SUBEQUIP_TAB.ADD_LINE;
        btns.delLine = constants.CARD_BTNS.TABLE_HEAD.SUBEQUIP_TAB.DELETE_LINE;
        btns.batchAlter = constants.CARD_BTNS.TABLE_HEAD.SUBEQUIP_TAB.BATCH_ALTER;
    }

    if (!pk_org || !pk_org.value) {
        props.button.setButtonDisabled([btns.addLine, btns.delLine, btns.batchAlter], true);
    } else {
        props.button.setButtonDisabled([btns.addLine, btns.batchAlter], false);
        let checkedRows = [];
        let num = props.cardTable.getNumberOfRows(moduleId, false);
        if (num > 0) {
            checkedRows = props.cardTable.getCheckedRows(moduleId);
        }
        props.button.setButtonDisabled([btns.delLine], !(checkedRows && checkedRows.length > 0));
    }
}