/**
 * 表体页签显示处理
 * */
import constants from '../../constants';

const dep_tab = constants.AREA.CARD.DEP_TAB;

/**
 * 页签展开加载数据
 * @param props
 * @param moduleid
 * @param value 点击时模块是否展开
 */
export default function (props, moduleid, value) {
    if (!value) {//true 表示点击的时候模块正处于展开状态
        return;
    }
    const that = this;
    const pk_card = this.curr_pk_card;
    let pk_accbook = this.state.currentPkaccbook;
    let bodyTabDataManager = this.bodyTabDataManager;

    if (pk_card) {
        bodyTabDataManager.getTabVOs(pk_card, pk_accbook, moduleid).then((bodyvos) => {
            props.cardTable.setTableData(moduleid, bodyvos);

            //折旧页签数据处理
            if (moduleid === dep_tab) {
                depEchartsDatas.call(that, bodyvos);
            }
        });
    }
}

function depEchartsDatas(bodyvos) {
    if (bodyvos && bodyvos.rows && bodyvos.rows.length) {
        let accudeps = {
            xDatas: [],
            yDatas: []
        };
        let depamonts = {
            xDatas: [],
            yDatas: []
        };

        bodyvos.rows.map((row) => {
            let vo = row.values;
            depamonts.xDatas.push(vo['period_string'].value);
            depamonts.yDatas.push(vo['depamount'].value);

            accudeps.xDatas.push(vo['period_string'].value);
            accudeps.yDatas.push(vo['accudep'].value);
        });

        this.setState({
            depEchartsData: {
                accudeps,
                depamonts
            }
        });
    }

}