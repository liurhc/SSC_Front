/**
 * 多使用部门页签的按钮处理
 */
import {setBodyBtnsEnable} from './setStatus';
import constants from "../../constants/index";

import ampub from 'ampub';
const {utils} = ampub;

const {showMessage} = utils.msgUtils;
const {getMultiLangByID} = utils.multiLangUtils;

const usedept_tab = constants.AREA.CARD.USEDEPT_TAB;


export default function buttonClick(props, id) {
    const that = this;
    switch (id) {
        //增行
        case constants.CARD_BTNS.TABLE_HEAD.USEDEPT_TAB.ADD_LINE:
            doAddLine.call(that);
            break;

        //删行
        case constants.CARD_BTNS.TABLE_HEAD.USEDEPT_TAB.DELETE_LINE:
            doDeleteLine.call(that);
            break;

        //批改
        case constants.CARD_BTNS.TABLE_HEAD.USEDEPT_TAB.BATCH_ALTER:
            doBatchAlter.call(that);
            break;

        default:
            break;
    }
}

function doAddLine() {
    this.props.cardTable.addRow(usedept_tab);

}

function doDeleteLine() {
    let rowsWillDel = this.props.cardTable.getCheckedRows(usedept_tab);
    if (rowsWillDel && rowsWillDel.length) {
        let indexs = rowsWillDel.map((row) => row.index);
        this.props.cardTable.delRowsByIndex(usedept_tab, indexs);
    }

    setBodyBtnsEnable(this.props, usedept_tab);
}

function doBatchAlter() {
    const colAllowedAlter = ['usescale', 'memo'];
    let changeData = this.props.cardTable.getTableItemData(usedept_tab);
    const field = changeData.batchChangeKey;
    if (colAllowedAlter.includes(field)) {
        //1、调用平台方法进行批改
        const lastEditField = this.props.cardTable.batchChangeTableData(usedept_tab);
    } else {
        /*国际化处理：该列不支持批改！*/
        showMessage(this.props, {content: getMultiLangByID('201201504A-000024'), color: 'info'});
    }
}
