/**
 * 表体页签数据管理, 仅仅管理浏览态的页签数据,编辑态的数据不在这里进行管理
 * */
import {ajax} from 'nc-lightapp-front';
import constants from '../../constants';

import ampub from 'ampub';

const {utils} = ampub;

const {showMessage} = utils.msgUtils;

const assetAllArea = constants.AREA.CARD.ASSET_ALL;
const {ACCBOOK_TABS} = constants;

class BodyTabDataManager {

    /**
     *
     * @param main 传入主引用的 this 对象
     */
    constructor(main) {
        // 表体页签数据:
        // 页签数据分为与账簿相关的 和 与账簿无关的
        // 与账簿相关的数据格式为：  <卡片主键+账簿+页签编码, 表体数据>
        // 与账簿无关的数据格式为：  <卡片主键+页签编码, 表体数据>
        this.bodytabsData = new Map();
        this.main = main;
    }

    /**
     * 获取卡片页签数据
     *
     * @param pk_card
     * @param pk_accbook
     *            ：如果页签时非账簿页签，那么传入的账簿不起作用。
     * @param tabCode
     * @return
     */
    getTabVOs(pk_card, pk_accbook, tabCode) {
        let key = '';
        if (ACCBOOK_TABS.includes(tabCode)) {
            key = pk_card + pk_accbook + tabCode;
        } else {
            key = pk_card + tabCode;
        }
        const that = this;
        const promise = new Promise(function (resolve, reject) {
            if (that.bodytabsData.has(key)) {
                const datas = that.bodytabsData.get(key);
                resolve(datas);

            } else {
                // 去后台查询数据然后返回
                let promiseNC = that._getDataFromNC(pk_card, pk_accbook, tabCode);
                resolve(promiseNC); //这里调用 resolve 返回promise，而不是 return promise;

            }

        });
        return promise;
    }

    /**
     * 从NC端获取数据
     * @param pk_card
     * @param pk_accbook
     * @param tabCode
     * @private
     */
    _getDataFromNC(pk_card, pk_accbook, tabCode) {
        const main = this.main;
        const that = this;
        let assetvos = Object.values(main.assetDataMap);

        //组装提交数据：按照一主多子的格式
        let data = {
            pageid: main.state.pageCode,
            templetid: main.templateid,
            userjson: {
                tabCode,
                selected_accbook: pk_accbook,
            },
            heads: [{
                pageid: main.state.pageCode,
                model: {
                    areacode: assetAllArea,
                    areaType: 'table',
                    rows: assetvos,
                }
            }],
        };
        const promise = new Promise(function (resolve, reject) {
            ajax({
                url: constants.URL.querytabdata,
                data,
                success: (res) => {
                    let {success, data} = res;
                    if (success && data) {
                        let bodyvos = null;

                        if (data.bodysMap) {
                            let tabs = Object.keys(data.bodysMap);
                            for (let tab of tabs) {
                                if (tab === tabCode) {
                                    bodyvos = data.bodysMap[tab][tab];
                                    that.setTabVOs(pk_card, pk_accbook, tabCode, bodyvos);
                                    break;
                                }
                            }
                        }

                        resolve(bodyvos);
                    } else {
                        that.setTabVOs(pk_card, pk_accbook, tabCode, {rows: []});
                        resolve({rows: []});
                    }
                },
                error: (err) => {
                    showMessage(that.props, {content: err.message, color: 'danger'});
                    reject(err);
                }
            })
        });

        return promise;
    }

    /**
     * 存卡片页签数据
     *
     * @param pk_card
     * @param pk_accbook
     *            ：如果页签时非账簿页签，那么传入的账簿不起作用。
     * @param tabCode
     * @param datas
     * @return
     */
    setTabVOs(pk_card, pk_accbook, tabCode, datas) {
        let key = '';
        if (ACCBOOK_TABS.includes(tabCode)) {
            key = pk_card + pk_accbook + tabCode;
        } else {
            key = pk_card + tabCode;
        }

        this.bodytabsData.set(key, datas);
    }


}

export default BodyTabDataManager;