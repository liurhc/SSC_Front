/*
* 卡片复制
* */
import {ajax, cardCache} from 'nc-lightapp-front';

import constants from "../constants";
import ampub from 'ampub';

const {components, utils} = ampub;

const {loginContextKeys, getContext} = components.LoginContext;
const {showMessage, MsgConst} = utils.msgUtils;
const { getMultiLangByID } = utils.multiLangUtils;

const copyCardModal = constants.MODAL_ID.CopyModal;

const copyCardFormId = "copyCard";

const tableId = constants.AREA.LIST.GRID_AREA;

const {assetDataSource} = constants;

class FaCardCopy {

    constructor(type) {
        this.sourceVo = {};
        this.type = type; //在卡片态复制=“card”, 在列表态复制=“list”
        this.metaItems = {};
    }

    //初始化复制卡片的模板
    initCopyCardTemplate(meta) {
        meta[constants.AREA.CARD.CopyCard] = {
            "code": constants.AREA.CARD.CopyCard,
            "moduletype": "form",
            "name": getMultiLangByID('201201504A-000026')/* 国际化处理： 卡片复制 */,
            "items": [
                {
                    itemtype: "number",
                    scale: "0",
                    col: "12",
                    label: getMultiLangByID('201201504A-000027')/* 国际化处理： 请输入数量 */,
                    attrcode: "count",
                    isnextrow: true,
                    maxlength: "20",
                    required: true,
                    visible: true,
                    min: 1,
                },
                {
                    itemtype: "datepicker",
                    col: "12",
                    label: getMultiLangByID('201201504A-000028')/* 国际化处理： 请确认建卡日期 */,
                    attrcode: "business_date",
                    isnextrow: true,
                    maxlength: "19",
                    required: true,
                    visible: true,
                }
            ]
        };
        meta[constants.AREA.CARD.CopyCard].status = constants.UISTATE.EDIT;
        if (this.type === 'list') {//列表态
            meta[tableId].items.map((item) => {
                this.metaItems[item.attrcode] = item;
            });
        } else {//卡片态

        }
    }

    copyCardDialog(that) {
        return (
            <div>
                {that.props.form.createForm(copyCardFormId)}
            </div>
        );
    }

    //显示对话框
    showCopyCardDialog(that, pk_card, sourceVo) {
        this.sourceVo = sourceVo;
        //that.props.form.setFormStatus(copyCardFormId, constants.UISTATE.EDIT);
        let businessDate = getContext(loginContextKeys.businessDate);
        that.props.form.setFormItemsValue(copyCardFormId, {'business_date': {value: businessDate}});
        that.props.form.setFormItemsValue(copyCardFormId, {'count': {value: '1'}});
        that.props.modal.show(copyCardModal, {
            className: 'senior',
            beSureBtnClick: () => {
                this.doCopy(that, pk_card)
            },
            cancelBtnClick: () => {
                that.props.form.EmptyAllFormValue(copyCardFormId);//清空 form
                that.props.modal.close(copyCardModal);
            }
        });
    }

    //执行复制
    doCopy(that, pk_card) {
        //检验必输项
        const checkResult = that.props.form.isCheckNow(copyCardFormId);
        if (!checkResult) {
            return false;
        }

        let formData = that.props.form.getAllFormValue(copyCardFormId);
        formData = formData.rows[0];

        let data = {
            copyBusinessData: formData.values.business_date.value,
            copyNumber: formData.values.count.value,
        };

        let countReg = /^[1-9]\d*$/;
        if (!countReg.test(data.copyNumber)) {
            /*国际化处理：卡片数量必须为正整数*/
            showMessage(that.props, {content: getMultiLangByID('201201504A-000029'), color: 'danger'});
            return;
        }

        data.pk_card = pk_card;
        ajax({
            url: constants.URL.COPY,
            data,
            success: (res) => {
                let {success, data} = res;
                if (success && data) {
                    /*国际化处理：复制成功*/
                    showMessage(null, {type: MsgConst.Type.CopySuccess});
                    let {allpks, rows} = data[tableId];
                    //更新 pk_cardhistory -> clientKeys 缓存
                    let pk2ClientKeyMap = cardCache.getDefData(constants.pk2ClientKeyMap, assetDataSource);
                    Object.assign(pk2ClientKeyMap, JSON.parse(data.userjson));
                    cardCache.setDefData(constants.pk2ClientKeyMap, assetDataSource, pk2ClientKeyMap);

                    //更新表格数据
                    if (this.type === 'list') {
                        let tableData = that.props.table.getAllTableData(tableId);
                        tableData.rows = tableData.rows.concat(rows);
                        that.props.table.setAllTableData(tableId, tableData);
                    } else {
                        //将返回的数据放到列表的缓存中,列表态无需加缓存，平台会给加上
                        for (let i = 0; i < allpks.length; i++) {
                            cardCache.addCache(allpks[i], null, null, assetDataSource, rows[i].values);
                        }
                        if (this.successFunc) {
                            this.successFunc(allpks[allpks.length - 1]);
                        }
                    }

                    //关闭模态框
                    that.props.form.EmptyAllFormValue(copyCardFormId);//清空 form
                    that.props.modal.close(copyCardModal);
                }
            },
            error: (err) => {
                showMessage(null, {content: err.message, color: 'danger'});
            }
        });

    }

    /**
     * 设置复制成功之后需要执行的函数
     * @param successFunc
     */
    setCopySuccessAfter(successFunc) {
        if (typeof successFunc === "function") {
            this.successFunc = successFunc;
        }
    }
}

export default FaCardCopy;