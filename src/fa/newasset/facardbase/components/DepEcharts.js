import React, {Component} from 'react';
import {base} from 'nc-lightapp-front';
import echarts from 'echarts';
import ampub from 'ampub';

const {utils} = ampub;
const { getMultiLangByID } = utils.multiLangUtils;


const {NCTabs} = base;
const NCTabPane = NCTabs.NCTabPane;
/**
 * @author:zhangxxu
 * @description:计提记录 模块  tab页签切换 组件
 *
 */
export default class DepEcharts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            slidUp: false //是否收起图表 ， 默认不收起
        };
        this.onReturnTable = props.onReturnTable.bind(this);
        this.currItem = 'depamount'; //当前选择的项目
    }

    componentDidMount() {
        this.initCurrEchart();
    }

    componentWillReceiveProps(nextProps) {
        if (this.currItem === 'depamount') {
            this.depamountChart.setOption(this.getData(this.currItem));
        } else {
            this.accdepChart.setOption(this.getData(this.currItem));
        }
        this.setState({
            slidUp: nextProps.hide
        });
    }


    //初始化Echart 图
    initCurrEchart() {
        this.depamountChart = echarts.init(document.getElementById(this.currItem + '-box'));
        this.depamountChart.setOption(this.getData(this.currItem));
        // 当浏览器窗口发生变化时候，echarts大小随着变化
        window.addEventListener('resize', () => {
            this.depamountChart.resize();
        });
    }

    getData(key) {
        let {accudeps, depamonts} = this.props.seriesData;
        let depdata = {
            accudeps,
            depamonts
        };
        for (let key in depdata) {
            let item = depdata[key];
            if (!item) {
                depdata[key] = {
                    xDatas: [],
                    yDatas: [],
                }
            } else {
                item.xDatas = item.xDatas ? item.xDatas : [];
                item.yDatas = item.yDatas ? item.yDatas : [];
            }
        }
        let data = key === 'depamount' ? depdata.depamonts : depdata.accudeps;
        let {xDatas, yDatas} = data;
        //控制默认显示一年的折旧数据（最后12条）
        let start = 0;
        if(xDatas && xDatas.length > 12) {
            start = Math.floor((xDatas.length - 12) / xDatas.length * 100);
        }

        return {
            xAxis: {
                type: 'category',
                //data: [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' ]
                data: xDatas
            },
            yAxis: {
                type: 'value'
            },
            dataZoom: [
                {
                    show: true,
                    realtime: true,
                    start: start,
                    end: 100
                }
            ],
            series: [
                {
                    //data: [ 820, 932, 901, 934, 1290, 1330, 1320 ],
                    data: yDatas,
                    type: 'line',
                    label: {
                        normal: {
                            show: true,
                            position: 'top'
                        }
                    }
                }
            ]
        };
    }

    //切换页签时候触发的事件
    onChange = (key) => {
        let that = this;
        this.currItem = key;
        let accdepChart;
        let data = this.getData(key);
        if (key === 'accudep') {
            setTimeout(() => {
                accdepChart = echarts.init(document.getElementById('accdep-box'));
                accdepChart.setOption(data);
                // 当浏览器窗口发生变化时候，echarts大小随着变化
                window.addEventListener('resize', () => {
                    accdepChart.resize();
                });
                that.accdepChart = accdepChart;
            }, 500);
        }
    };

    render() {
        let slidUp = this.state.slidUp;
        return (
            <div className="echarts-area">
                <div className="echarts-head">
                    <span className={`toggle-icon iconfont ${slidUp ? 'icon-right' : 'icon-bottom'}`}
                          onClick={() => {
                              this.setState({
                                  slidUp: !slidUp
                              }, ()=>{
                                  this.currItem = 'depamount';
                                  if(!this.state.slidUp) {
                                      this.initCurrEchart();
                                  }
                              });

                          }}/>
                    <span className="echarts-name">折旧记录</span>
                    <span className={`return-icon iconfont icon-xingzhuang`} onClick={this.onReturnTable}/>
                </div>
                {this.state.slidUp === false && (
                    <div className="echarts-box">
                        <NCTabs
                            navtype="turn"
                            contenttype="moveleft"
                            defaultActiveKey="depamount"
                            tabBarPosition="top"
                            onChange={this.onChange.bind(this)}
                        >
                            <NCTabPane tab={getMultiLangByID('201201504A-000036')/* 国际化处理： 月折旧额*/} key="depamount">
                                <div id="depamount-box"/>
                            </NCTabPane>
                            <NCTabPane tab={getMultiLangByID('201201504A-000037')/* 国际化处理： 累计折旧*/}  key="accudep">
                                <div id="accdep-box"/>
                            </NCTabPane>
                        </NCTabs>
                    </div>
                )}
            </div>
        );
    }
}
