/*
* 卡片删除校验
* */
import {toast} from 'nc-lightapp-front';
import constants from '../constants';
import ampub from 'ampub';
const {utils} = ampub;

const { getMultiLangByID } = utils.multiLangUtils;

const {BILL_SOURCE_TYPES, ASSET_ADDED_WAY} = constants;

export default function doDeleteBeforeCheck(asset) {
    const source = asset.bill_source.value;
    let error = '';
    if (source === BILL_SOURCE_TYPES.conbin_src) {
        // 合并卡片,不能删除
        error = getMultiLangByID('201201504A-000032', {card_code: asset.card_code.value});
    } else if (source === BILL_SOURCE_TYPES.split_src) {
        // 拆分卡片,不能删除
        error = getMultiLangByID('201201504A-000033', {card_code: asset.card_code.value});
    } else if (source === BILL_SOURCE_TYPES.deploy_in_src) {
        // 调拨增加资产，不能删除
        error = getMultiLangByID('201201504A-000034', {card_code: asset.card_code.value});
    } else {
        const newasset_flag = asset.newasset_flag.value;
        if (newasset_flag > ASSET_ADDED_WAY.isNew) {
            //非当月增加，不能删除
            error = getMultiLangByID('201201504A-000035', {card_code: asset.card_code.value});
        }
    }
    return error;

}
