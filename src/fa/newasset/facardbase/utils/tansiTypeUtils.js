/**
 * 交易类型处理
 *
 */
import {ajax, toast} from 'nc-lightapp-front';

//根据 key 获取button
function getBtnByKey(buttons, key) {
    let targetBtn = '';
    for (let btn of buttons) {
        if(btn.key === key) {
            targetBtn = btn;
            return targetBtn;
        }
        if (btn.children && btn.children.length) {
            targetBtn = getBtnByKey(btn.children, key);
            if(targetBtn) {
                return targetBtn;
            }
        }
    }
}

function createBtns(transiTypeAry, currentLangSeq) {
    let btns = transiTypeAry.map((source, arr, index) => {
        let title = '-- multi language error ---';
        if(!currentLangSeq || currentLangSeq == 1) {
            title = source['billtypename'];
        } else {
            title = source['billtypename' + currentLangSeq];
        }
        return {
            id: source.pk_billtypeid,
            type: 'button_secondary',
            key: source.pk_billtypecode,
            title,
            area: 'card_head',
            order: index,
            parentCode: 'AddGroup1',
            children: [],
            visible: true,
        };
    });
    return btns;
}


function getPkTransitype(transitypeName) {
    let transiTypeAry = this.state.transiTypeAry;
    let pk_transitype = undefined;
    if (transiTypeAry) {
        for (let i = 0; i < transiTypeAry.length; i++) {
            let transitypeObj = transiTypeAry[i];
            if (transitypeObj['pk_billtypecode'] === transitypeName) {
                pk_transitype = transitypeObj['pk_billtypeid'];
                break;
            }
        }
    }

    return pk_transitype;
}

/**
 * 新增】按钮下增加交易类型button--交易类型已经在初始化时获取
 * @param buttons 后台返回的buttons
 * @param currentLangSeq  当前语种顺序
 */
function addTransitypeBtns(buttons, currentLangSeq = 1) {

    let transiTypeAry = this.state.transiTypeAry;
    //循环得到 AddGroupBtn
    let AddGroupBtn = getBtnByKey(buttons, 'AddGroup'); //新增
    let AddGroup1Btn = undefined; //新增 下拉按钮 的一个区域

    if (AddGroupBtn) {
        if (AddGroupBtn.children && AddGroupBtn.children.length > 0) {
            AddGroup1Btn = AddGroupBtn.children[0];
        }
        if (AddGroup1Btn) {
            if (transiTypeAry && transiTypeAry.length > 0) {
                let btns = createBtns(transiTypeAry, currentLangSeq);
                AddGroupBtn.visible = true;
                AddGroup1Btn.visible = true;
                AddGroup1Btn.children = AddGroup1Btn.children.concat(btns);
                //重新设置按钮，让他重绘界面
            } else {
                // console.log("------ 没有获取到交易类型");
            }
        } else {
            // console.log("------ 按钮解析错误，按钮注册中不存在 编码为‘AddGroup1’ 的按钮, 他的父按钮为 ‘AddGroup’");
        }
    } else {
        // console.log("------ 按钮解析错误，按钮注册中不存在 编码为‘AddGroup’ 的按钮");
    }

}

export {
    getPkTransitype,
    addTransitypeBtns
}
