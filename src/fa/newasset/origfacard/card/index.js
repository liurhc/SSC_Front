/*
* 原始卡片录入-卡片页
* */
import React, {Component} from 'react';
import {createPage, ajax} from 'nc-lightapp-front';
import ampub from 'ampub';

const {utils} = ampub;

const {showMessage} = utils.msgUtils;
const {getMultiLangByID} = utils.multiLangUtils;

class FacardCard extends Component {
    constructor(props) {
        super(props);

        this.getDestAppcode.bind(this);
    }

    //初始化
    getDestAppcode() {
        const that = this;
        const promise = new Promise(function (resolve, reject) {
            //获取当前 url 地址中的小应用编码
            const currentAppcode = that.props.getSearchParam('c');
            // console.log('------- 原始卡片的url地址   ---------');
            // console.log(window.location.href);
            // console.log('------- c -------  ' + currentAppcode);
            ajax({
                url: '/nccloud/fa/facard/init.do',
                data: {appcode: currentAppcode},
                success: (res) => {
                    let {success, data} = res;
                    // console.log("------ 获取 sourceAppcode ------   ");
                    // console.log(data.sourceAppcode);
                    if (success) {
                        if (data.sourceAppcode) {
                            resolve(data)
                        } else {
                            /*国际化处理：获取源小应用编码失败*/
                            showMessage(null, {content: getMultiLangByID('201201504A-000002'), color: 'danger'});
                            reject(data);
                        }
                    } else {
                        reject(data);
                    }
                },
                error: (err) => {
                    showMessage(null, {content: err.message, color: 'danger'});
                    reject(err);
                }
            });
        });
        return promise;
    }

    componentDidMount() {
        // console.log('---------  card 原始卡片 入口 componentDidMount --------');
        // console.log("------ 来源于 原始卡片，现在正在跳转 ");
        const props = this.props;
        this.getDestAppcode().then((data) => {
            let transiType = props.getUrlParam('transiType');
            let scene = props.getUrlParam('scene');
            let pk_transitype = props.getUrlParam('pk_transitype');
            let status = props.getUrlParam('status');
            let id = props.getUrlParam('id');
            props.linkTo('/fa/newasset/facard/main/index.html#/card', {
                    am_source: 'orig',
                    appcode: data.sourceAppcode,
                    pagecode: '201201504A_list',
                    nccloud_menu_name_self: getMultiLangByID('201201504A-000001')/*国际化处理：录入原始卡片*/,
                    transiType,
                    scene,
                    pk_transitype,
                    status,
                    id,
                }
            );
        });
    }

    render() {
        return "";
    }
}

FacardCard = createPage({})(FacardCard);
export default FacardCard;
