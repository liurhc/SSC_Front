import { RenderRouter } from 'nc-lightapp-front';
import routes from './router';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

(function main(routers, htmlTagid) {
	let moduleIds = { fa: [ '201201524A', '201201512A', 'facommon' ], ampub: [ 'common' ] };
	initMultiLangByModule(moduleIds, () => {
		RenderRouter(routers, htmlTagid);
	});
})(routes, 'app');
