//list常量定义  cuixuh
import { publicPageConfig } from '../const';
import ampub from 'ampub';
const { commonConst } = ampub;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;

export const pageConfig = {
	//引入公共常量
	...publicPageConfig,
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 列表页面编码
	pagecode: '201201520A_list',
	// 预算字段
	pkNtbparadimvo: 'pk_ntbparadimvo',
	btn_head: {
		ADD: 'Add', //新增
		EDIT: 'Edit', //修改
		DELETE: 'Delete', //删除
		COMMIT: 'Commit', //提交
		UNCOMMIT: 'UnCommit', //收回
		PRINT: 'Print', //打印
		OUTPUT: 'Output', //输出
		REFRESH: 'Refresh', // 刷新
		QUERYABOUTBUDGET: 'queryAboutBudget', // 联查预算
		COPY: 'Copy', //复制
		ATTACHMENT: 'Attachment' //附件
	},
	//列表表格行按钮
	btn_body: {
		EDIT: 'Edit', //修改
		DELETE: 'Delete', //删除
		COMMIT: 'Commit', //提交
		UNCOMMIT: 'UnCommit', //收回
		COPY: 'Copy', //复制
		QUERYABOUTBILLFLOW: 'QueryAboutBillFlow' //审批详情
	},
	ListConfig: {
		searchId: 'searchArea',
		formId: 'card_head',
		bodyIds: [ 'list_head' ],
		specialFields: {
			pk_applydapt: {
				orgMulti: 'pk_org',
				// 申请部门
				data: [
					{
						fields: [ 'pk_applyorg' ],
						returnName: 'pk_org'
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			pk_proposer: {
				// 申请人
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_applyorg' ],
						returnName: 'pk_org'
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
					// {
					// 	fields: [ 'pk_applydapt' ],
					// 	returnName: 'pk_dept'
					// }
				]
			},
			pk_transactor: {
				// 经办人
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			}
		},
		linkageData: {
			//联动链
			//参照字段:['被过滤字段1'，'被过滤字段2']
			pk_org: [
				'pk_proposer', //申请人
				'pk_transactor', //经办人
				'pk_applydapt', //申请部门
				'bodyvos.pk_usedept', //使用部门
				'bodyvos.pk_mandept', //管理部门
				'bodyvos.pk_category', //资产类别
				'bodyvos.pk_card', //资产卡片
				'bodyvos.pk_assetuser' //使用人
			],
			pk_applyorg: [ 'pk_applydapt', 'pk_proposer' ]
		}
	}
};
