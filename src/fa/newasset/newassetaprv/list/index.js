/**
 * 新增资产审批单 列表态
 * @author cuixuh
 */
import React, { Component } from 'react';
import { createPage, ajax, base, high, createPageIcon } from 'nc-lightapp-front';
import { pageConfig } from './const';
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	doubleClick,
	rowSelected,
	commitAction,
	afterEvent,
	setBatchBtnsEnable
} from './events';
import { getUrlParamByPrivateUse } from './events/buttonClick';
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans } = components;
const { MsgConst, showMessage } = msgUtils;
const { Inspection, ApproveDetail,ExcelImport } = high;
const { NCAffix } = base;
const { searchAreaId, tableId, pkNtbparadimvo, url, pagecode, pkField, title, dataSource } = pageConfig;

class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			sourceData: null,
			showApprove: false,
			transi_type: '',
			pk_newasset: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {
		let pk_ntbparadimvo = getUrlParamByPrivateUse.call(this, pkNtbparadimvo);
		//判断是否是通过预算联查跳转过来的页面
		if (pk_ntbparadimvo) {
			this.getData(pk_ntbparadimvo);
		}
	}

	getData = (pk_ntbparadimvo) => {
		var _this = this;
		ajax({
			url: url.QUERYABOUTBUDGETLIST,
			data: {
				pk_ntbparadimvo,
				pageCode: pagecode
			},
			success: (res) => {
				if (res.data) {
					_this.props.table.setAllTableData(tableId, res.data[tableId]);
				} else {
					//返回的数据为空
					_this.props.table.setAllTableData(tableId, { rows: [] });
					showMessage.call(this, props, { type: MsgConst.Type.QueryNoData });
				}
			}
		});
	};

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, this.props, 'SAVE', null, value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { table, search, button, ncUploader, ncmodal } = this.props;
		let { createModal } = ncmodal;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = button;
		let { createNCUploader } = ncUploader;

		return (
			<div className="nc-bill-list">
				<NCAffix>
					{/* 头部 header */}
					<div className="nc-bill-header-area">
						{/* 标题 title */}
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title) /* 国际化处理： 新增资产审批单*/}</h2>
						</div>
						{/* 按钮区 btn-area */}
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				{/* 查询区 search-area */}
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchAreaId, {
						onAfterEvent: afterEvent.bind(this),
						clickSearchBtn: searchBtnClick.bind(this),
						dataSource: dataSource,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						}
					})}
				</div>
				{/* 列表区 table-area */}
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						handlePageInfoChange: pageInfoClick.bind(this),
						dataSource: dataSource,
						pkname: pkField,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/*联查预算*/}
				<Inspection
					show={this.state.show}
					sourceData={this.state.sourceData}
					cancel={() => {
						this.setState({ show: false });
					}}
					affirm={() => {
						this.setState({ show: false });
					}}
				/>
				{/*审批详情*/}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_newasset}
				/>
				{/*附件*/}
				{createNCUploader(`${pagecode}-uploader`, {disableModify:(this.props.cardTable.getAllData('list_head').rows[0]==null ? '1': (this.props.table.getCheckedRows('list_head').length>0&&this.props.table.getCheckedRows('list_head')[0].data.values.bill_status.value))!='0'})}
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} /* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
                )}
                {/* 导入导出 - 2019-11-25 */}
				<ExcelImport
					{...Object.assign(this.props)}
					moduleName="fa" // 模块名
					billType= 'HN' // 单据类型
					pagecode = '201201520A_card' // 页面编码
					appcode = '201201520A' // 应用编码
					selectedPKS={[]}
				/>
			</div>
		);
	}
}

List = createPage({})(List);

export default List;
