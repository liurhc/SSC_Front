import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';
const { components, utils } = ampub;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;
const { LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;
const { searchAreaId, tableId, pagecode, url, bill_type } = pageConfig;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	//通过上下文获取交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(searchAreaId);
	}

	//分页信息
	let pageInfo = props.table.getTablePageInfo(tableId);

	queryInfo.pagecode = pagecode;
	queryInfo.pageInfo = pageInfo;
	queryInfo.billtype = bill_type;
	queryInfo.transtype = transi_type;

	//缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);

	//条件查询
	ajax({
		url: url.QUERY_LIST,
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
