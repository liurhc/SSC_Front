import { pageConfig } from '../const';
import ampub from 'ampub';
const { components } = ampub;
const { assetOrgMultiRefFilter } = components;
const { isMultiCorpRefHandler } = assetOrgMultiRefFilter;
import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage } = fa_components;
const { referLinkageClear } = ReferLinkage;
const { searchAreaId, ListConfig } = pageConfig;

/**
 * 查询区编辑后事件
 * @param {*} key 
 * @param {*} value 
 */
export default function afterEvent(key, value) {
	if (key == 'pk_org') {
		isMultiCorpRefHandler.call(this, this.props, value, searchAreaId, [ 'pk_applyorg', 'pk_transactor' ]);
		referLinkageClear.call(this, this.props, key, ListConfig);
	}
	if (key == 'pk_applyorg') {
		referLinkageClear.call(this, this.props, key, ListConfig);
	}
}
