import initTemplate from './initTemplate';
import searchBtnClick from './searchBtnClick';
import pageInfoClick from './pageInfoClick';
import buttonClick, { commitAction, setBatchBtnsEnable } from './buttonClick';
import doubleClick from './doubleClick';
import rowSelected from './rowSelected';
import afterEvent from './afterEvent';
export {
	searchBtnClick,
	afterEvent,
	pageInfoClick,
	setBatchBtnsEnable,
	initTemplate,
	buttonClick,
	doubleClick,
	rowSelected,
	commitAction
};
