import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;
const { pagecode, url, tableId } = pageConfig;

//分页处理
export default function(props, config, pks) {
	let data = {
		allpks: pks,
		pagecode
	};
	ajax({
		url: url.PAGEDATABYPKSURL,
		data,
		success: (res) => {
			setListValue.call(this, props, res);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
