import { pageConfig } from '../const';
import { linkToCard, setBatchBtnsEnable } from './buttonClick';
import tableButtonClick from './tableButtonClick';
import ampub from 'ampub';
import { excelImportconfig } from 'nc-lightapp-front';
const { components, commonConst, utils } = ampub;
const { CommonKeys, StatusUtils } = commonConst;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { IBusiRoleConst } = CommonKeys;
const { LoginContext } = components;
const { UISTATE, BILLSTATUS } = StatusUtils;
const { loginContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;
const { tableId, searchAreaId, pagecode, btn_body, ListConfig } = pageConfig;

/**
 * 模板初始化
 * @param {*} props 
 */
export default function(props) {
	props.createUIDom(
		{
			//页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext.call(this, data.context);
				}
				if (data.button) {
					//小应用中注册的按钮
                    let button = data.button;
                    
                    // 导入导出适配 - 20191125
					let excelimportconfig = excelImportconfig(props,'fa','HN',true,'',{'appcode':'201201520A', 'pagecode':'201201520A_card'});
					props.button.setUploadConfig("import",excelimportconfig);

					props.button.setButtons(button);
					// 行删除时悬浮框提示
					props.button.setPopContent(
						btn_body.DELETE,
						getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/
					);
				}
				if (data.template) {
					//返回模版信息
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	setBatchBtnsEnable.call(this, props, tableId);
}

//修改模板
function modifierMeta(props, meta) {
	//分页控件显示标识
	meta[tableId].pagination = true;
	//查询区参照过滤
	addSearchAreaReferFilter.call(this, props, meta, ListConfig);
	//经办人过滤单独处理
	meta[searchAreaId].items.map((item) => {
		// if (item.attrcode == 'pk_transactor') {
		// 	item.unitCondition = () => {
		// 		let retData = {};
		// 		let pk_org_value = props.search.getSearchValByField(searchAreaId, 'pk_org'); // 调用相应组件的取值API
		// 		if (pk_org_value && pk_org_value.value && pk_org_value.value.firstvalue) {
		// 			retData['pk_org'] = pk_org_value.value.firstvalue;
		// 		} else {
		// 			retData.wantnull = 'yes';
		// 		}
		// 		retData['TreeRefActionExt'] = 'nccloud.web.fa.fabase.ref.refCodition.UnitSqlBuilder';
		// 		return retData;
		// 	};
		// }
		//使用部门改为参照
		if (item.attrcode == 'bodyvos.pk_usedept') {
			item.itemtype = 'refer';
			item.datatype = '204';
			item.refcode = 'uapbd/refer/org/DeptNCTreeRef/index.js';
			item.maxlength = '20';
			item.queryCondition = () => {
				let data = getSearchValue.call(this, props, 'pk_org'); // 调用相应组件的取值API
				let filter = { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				if (data) {
					filter['pk_org'] = data;
				}
				return filter; // 根据pk_org过滤
			};
		}
	});
	meta[tableId].items.map((item, key) => {
		//单据号添加下划线超链接
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							linkToCard.call(this, props, record, UISTATE.browse);
						}}
					>
						{record && record.bill_code && record.bill_code.value}
					</span>
				);
			};
		}

		return item;
	});
	//添加操作列
	meta[tableId].items.push({
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		// 锁定在右边
		fixed: 'right',
		width: '200px',
		render: (text, record, index) => {
			let buttonAry = [];
			if (record) {
				let bill_status = record.bill_status && record.bill_status.value;
				if (bill_status == BILLSTATUS.free_check) {
					//自由态
					buttonAry = [ btn_body.COMMIT, btn_body.EDIT, btn_body.DELETE, btn_body.COPY ];
				} else if (bill_status == BILLSTATUS.un_check) {
					//已提交
					buttonAry = [ btn_body.UNCOMMIT, btn_body.COPY, btn_body.QUERYABOUTBILLFLOW ];
				} else if (bill_status == BILLSTATUS.check_going) {
					//审批中
					buttonAry = [ btn_body.COPY, btn_body.QUERYABOUTBILLFLOW ];
				} else if (bill_status == BILLSTATUS.check_pass) {
					//审批通过
					buttonAry = [ btn_body.UNCOMMIT, btn_body.COPY, btn_body.QUERYABOUTBILLFLOW ];
				} else if (bill_status == BILLSTATUS.check_nopass) {
					//审批不通过
					buttonAry = [ btn_body.EDIT, btn_body.DELETE, btn_body.QUERYABOUTBILLFLOW ];
				}
			}
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index);
				}
			});
		}
	});
	return meta;
}
//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchAreaId, field);
	let value = '';
	if (data && data.value) {
		value = data.value.firstvalue;
	}
	if (value && value.split(',').length == 1) {
		return value;
	} else {
		return '';
	}
}
