import { ajax } from 'nc-lightapp-front';
import { linkToCard, deleteLine, commitActionSignal, copyActionSignal, openApproveAction } from './buttonClick';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { msgUtils } = utils;
const { UISTATE } = StatusUtils;
const { showMessage } = msgUtils;
const { pkField, btn_body, url, resourceCode } = pageConfig;
/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	switch (key) {
		case btn_body.EDIT:
			edit.call(this, props, record, index);
			break;
		case btn_body.DELETE:
			singleDel.call(this, props, record, index);
			break;
		case btn_body.COMMIT:
			commit.call(this, props, record, index);
			break;
		case btn_body.UNCOMMIT:
			unCommit.call(this, props, record, index);
			break;
		case btn_body.COPY:
			copy.call(this, props, record, index);
			break;
		case btn_body.QUERYABOUTBILLFLOW:
			openApprove.call(this, props, record, index);
			break;
		default:
			break;
	}
}

/**
 * 表格行修改
 * @param {*} props 
 * @param {*} record 
 */
function edit(props, record, index) {
	let pk = record[pkField].value;
	let data = {
		pk,
		resourceCode
	};
	ajax({
		url: url.EDIT,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					showMessage.call(this, props, { color: 'warning', content: data });
				} else {
					linkToCard.call(this, props, record, UISTATE.edit);
				}
			}
		}
	});
}

/**
 * 删除
 * 
 * @param {*} props 
 */
function singleDel(props, record, index) {
	const rowData = { values: record, index };
	deleteLine(props, rowData);
}

/**
 * 提交
 * 
 * @param {*} props 
 */
function commit(props, record, index) {
	const rowData = { values: record, index };
	commitActionSignal.call(this, props, 'SAVE', rowData);
}

/**
 * 收回
 * 
 * @param {*} props 
 */
function unCommit(props, record, index) {
	const rowData = { values: record, index };
	commitActionSignal.call(this, props, 'UNSAVE', rowData);
}

/**
 * 复制
 * 
 * @param {*} props 
 */
function copy(props, record, index) {
	const rowData = { values: record, index };
	copyActionSignal.call(this, props, rowData);
}

/**
 * 审批详情
 * 
 * @param {*} props 
 */
function openApprove(props, record, index) {
	const rowData = { values: record, index };
	openApproveAction.call(this, props, rowData);
}
