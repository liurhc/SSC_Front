import { ajax, print, output } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import searchBtnClick from './searchBtnClick';
import ampub from 'ampub';
import { excelImportconfig } from 'nc-lightapp-front'; // 导入导出适配 -2019-11-25

const { components, commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { msgUtils, listUtils } = utils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { ScriptReturnUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { UISTATE } = StatusUtils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { tableId, pagecode, pkField, url, printNodekey, cardRouter, btn_head, printFilename, dataSource } = pageConfig;

/**
 * 按钮点击
 * @param {*} props 
 * @param {*} id 
 */
export default function buttonClick(props, id) {
	switch (id) {
		case btn_head.ADD:
			linkToCard.call(this, props, undefined, UISTATE.add);
			break;

		case btn_head.REFRESH:
			refreshAction.call(this, props);
			break;

		case btn_head.DELETE:
			batchDelete.call(this, props);
			break;

		case btn_head.COMMIT:
			commitAction.call(this, props, 'SAVE', null);
			break;

		case btn_head.UNCOMMIT:
			commitAction.call(this, props, 'UNSAVE', null);
			break;

		case btn_head.QUERYABOUTBUDGET:
			queryAboutBudgetAction.call(this, props);
			break;

		case btn_head.PRINT:
			printAction.call(this, props);
			break;

		case btn_head.OUTPUT:
			outputAction.call(this, props);
			break;

		case btn_head.COPY:
			copyAction.call(this, props, null);
			break;

		case btn_head.ATTACHMENT:
			attachmentAction.call(this, props);
            break;
            
        case 'import': // 导入
			break;

		case 'export': // 导出
			this.props.modal.show('exportFileModal');
			break;

		default:
			break;
	}
}

//审批详情
export function openApproveAction(props, row) {
	this.setState({
		showApprove: true,
		transi_type: row.values.transi_type.value,
		pk_newasset: row.values.pk_newasset.value
	});
}

//附件
function attachmentAction(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOne });
		return;
	}
	// 单据编码
	let billNo = checkedRows[0].data.values['bill_code'].value;
	// 单据主键
	let billId = checkedRows[0].data.values[pkField].value;
	props.ncUploader.show(`${pagecode}-uploader`, {
		billId: 'fa/newasset/' + billId,
		billNo
	});
}

//复制
function copyAction(props, row) {
	let id = '';
	if (row == null) {
		let data = props.table.getCheckedRows(tableId);
		// 选择多条数据时，默认复制第一条
		if (data.length == 0) {
			showMessage.call(this, props, { type: MsgConst.Type.ChooseCopy });
			return;
		}
		id = data[0].data.values.pk_newasset.value;
	} else {
		id = row.values.pk_newasset.value;
	}
	props.pushTo(cardRouter, {
		status: UISTATE.browse,
		id,
		copyflag: true,
		pagecode: pagecode.replace('list', 'card')
	});
}

//表体复制
export function copyActionSignal(props, row) {
	copyAction(props, row);
}

//输出
function outputAction(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.PRINTCARD,
		data: printData
	});
}

//打印
function printAction(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.PRINTCARD, // 后台打印服务url
		printData
	);
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values.pk_newasset.value);
	});
	let printData = {
		filename: printFilename, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

// 预算联查
function queryAboutBudgetAction(props) {
	let datas = props.table.getCheckedRows(tableId);
	if (datas.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOne });
		return;
	}
	let sourceData = null;
	let pk_newasset = datas[0].data.values.pk_newasset.value;
	ajax({
		url: url.QUERYABOUTBUDGET,
		data: {
			pk: pk_newasset
		},
		success: (res) => {
			if (res.data.hint != undefined) {
				showMessage.call(this, props, { color: 'warning', content: res.data.hint });
			} else {
				this.setState({
					show: true,
					sourceData: res.data
				});
			}
		}
	});
}

// 提交、收回
export function commitAction(props, OperatorType, rows, content) {
	let paramInfoMap = {};
	let params = '';
	//先判断是表肩批量提交还是表体按钮提交
	if (rows == null) {
		let data = props.table.getCheckedRows(tableId);
		if (data.length == 0) {
			showMessage.call(this, props, { type: MsgConst.Type.ChooseOne });
			return;
		}
		params = data.map((v) => {
			let id = v.data.values.pk_newasset.value;
			let ts = v.data.values.ts.value;
			let index = v.index;
			paramInfoMap[id] = ts;
			return {
				id,
				index
			};
		});
	} else {
		params = rows.map((row) => {
			let id = row.values[pkField].value;
			let ts = row.values.ts.value;
			let index = row.index;
			paramInfoMap[id] = ts;
			return {
				id,
				index
			};
		});
	}

	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: url.COMMIT,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecode,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(this, params, res, props, pkField, tableId, tableId, false, dataSource);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}

//表体提交、收回
export function commitActionSignal(props, OperatorType, row) {
	commitAction.call(this, props, OperatorType, [ row ]);
}

//肩部的批量删除
function batchDelete(props) {
	let data = props.table.getCheckedRows(tableId);
	if (data.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: deleteAction });
}

//表体行的单行删除
export function deleteLine(props, row) {
	deleteAction(props, [ row ]);
}

function deleteAction(props, rows) {
	let paramInfoMap = {};
	let params = '';
	//判断是表体还是表肩删除
	if (rows == null) {
		let data = props.table.getCheckedRows(tableId);
		params = data.map((v) => {
			let id = v.data.values.pk_newasset.value;
			let ts = v.data.values.ts.value;
			let index = v.index;
			paramInfoMap[id] = ts;
			return {
				id,
				index
			};
		});
	} else {
		params = rows.map((row) => {
			let id = row.values[pkField].value;
			let ts = row.values.ts.value;
			let index = row.index;
			paramInfoMap[id] = ts;
			return {
				id,
				index
			};
		});
	}
	ajax({
		url: url.DELETE,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			getScriptListReturn(params, res, props, 'pk_newasset', tableId, tableId, true);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}

//刷新
function refreshAction(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
}

//普通url取值  ---  带?的
export function getUrlParamByPrivateUse(name) {
	var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)'); //构造一个含有目标参数的正则表达式对象
	var params = null;
	if (window.location.search !== '') {
		params = window.location.search.substr(1).match(reg); //匹配目标参数
	} else {
		params = window.location.hash.substr(1).match(reg); //匹配目标参数
	}
	if (params != null) return params[2];
	return null; //返回参数值
}

/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status) {
	props.pushTo(cardRouter, {
		status,
		id: record[pkField] ? record[pkField].value : '',
		pagecode: pagecode.replace('list', 'card')
	});
}
