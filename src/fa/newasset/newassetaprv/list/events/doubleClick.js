import { linkToCard } from './buttonClick';
import ampub from 'ampub';
const { commonConst } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

/**
 * 行双击事件
 * @param {*} record 
 * @param {*} index 
 */
export default function doubleClick(record, index, props) {
	linkToCard.call(this, props, record, UISTATE.browse);
}
