/**
 * 公共常量定义
 * @author cuixuh
 * @since ncc1.0 
 */

export const publicPageConfig = {
	// 应用编码
	appcode: '201201520A',
	// 应用名称
	title: '201201520A-000001' /* 国际化处理： 新增资产审批单*/,
	// 主键字段
	pkField: 'pk_newasset',
	// 单据类型
	bill_type: 'HN',
	// 交易类型
	transi_type: 'HN-01',
	// 请求链接
	url: {
		EDIT: '/nccloud/fa/newassetaprv/edit.do', //修改权限校验
		COMMIT: '/nccloud/fa/newassetaprv/commit.do', // 提交
		UNCOMMIT: '/nccloud/fa/newassetaprv/uncommit.do', // 收回
		DELETE: '/nccloud/fa/newassetaprv/delete.do', // 删除
		QUERYABOUTBUDGET: '/nccloud/fa/newassetaprv/queryaboutbudget.do', // 联查预算
		QUERYABOUTBUDGETLIST: '/nccloud/fa/newassetaprv/queryaboutbudgetlist.do', //联查预算明细
		PRINTLIST: '/nccloud/fa/newassetaprv/printList.do', // 打印列表
		PRINTCARD: '/nccloud/fa/newassetaprv/printCard.do', // 打印卡片
		QUERY_CARD: '/nccloud/fa/newassetaprv/querycard.do', // 列表态查询
		QUERY_COPY: '/nccloud/fa/newassetaprv/copy.do', // 列表态复制
		SAVE: '/nccloud/fa/newassetaprv/insert.do', // 保存
		UPDATE: '/nccloud/fa/newassetaprv/update.do', // 更新
		QUERY_LIST: '/nccloud/fa/newassetaprv/querylist.do', // 列表态查询
		PAGEDATABYPKSURL: '/nccloud/fa/newassetaprv/querypagegridbypks.do', // 分页查询
		BODYAFTEREDIT: '/nccloud/fa/newassetaprv/bodyafteredit.do', //表体编辑后
		BATCHALTER: '/nccloud/fa/newassetaprv/batchAlter.do' // 批改编辑后
	},
	// 打印模板节点标识
	printNodekey: null,
	// 权限资源编码
	resourceCode: '2012016020',
	dataSource: 'fa.newasset.newassetaprv.main',
	// 路由
	cardRouter: '/card',
	listRouter: '/list',
	// 输出文件名称
	printFilename: '201201520A-000001' /* 国际化处理： 新增资产审批单*/
};
