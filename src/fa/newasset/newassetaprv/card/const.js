//card常量定义  cuixuh
import { publicPageConfig } from '../const';
import ampub from 'ampub';
const { commonConst } = ampub;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;

// 页面配置
export const pageConfig = {
	//引入公共常量
	...publicPageConfig,
	// 表单区域id
	formId: 'card_head',
	// 表格区域id
	tableId: 'bodyvos',
	// 查询区域id
	searchAreaId: 'searchArea',
	// 页面编码
	pagecode: '201201520A_card',
	// 编辑态按钮
	editBtns: [ 'SaveGroup', 'Cancel', 'BatchAlter', 'AddLine', 'DelLine', 'CopyLine' ],
	// 浏览态按钮
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Copy',
		'Commit',
		'UnCommit',
		'Refresh',
		'More',
		'Print',
		'Other',
		'QueryAbout'
	],
	CardBodyAfterEventConfig: {
		searchId: 'searchArea',
		formId: 'card_head',
		tableId: 'bodyvos',
		linkageData: {
			//联动链
			pk_ownerorg_v: [ 'pk_mandept_v', 'pk_mandept' ],
			pk_equiporg_v: [ 'pk_usedept', 'pk_assetuser' ]
		}
	},
	CardBodyConfig: {
		searchId: 'searchArea',
		formId: 'card_head',
		bodyIds: [ 'bodyvos', 'bodyvos_edit' ],
		onlyLeafCanSelect: {
			// 使用部门非末级处理，默认不可勾选
			pk_usedept: true
		},
		//特殊过滤
		specialFields: {
			pk_ownerorg_v: {
				addOrgRelation: 'pk_orgs',
				RefActionExtType: 'TreeRefActionExt',
				class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_OWNERORG_VSqlBuilder',
				//货主管理组织
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			},
			pk_usedept: {
				// 使用部门
				data: [
					{
						fields: [ 'pk_equiporg', 'pk_org', 'pk_equiporg_v' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			pk_mandept_v: {
				// 管理部门
				data: [
					{
						fields: [ 'pk_ownerorg', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			pk_assetuser: {
				//使用人
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_equiporg', 'pk_org' ],
						returnName: 'pk_org'
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			}
		}
	},

	CardHeadConfig: {
		searchId: 'searchArea',
		formId: 'card_head',
		bodyIds: [ 'bodyvos', 'bodyvos_edit' ],
		specialFields: {
			pk_applyorg_v: {
				// 申请单位
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			},
			pk_applydapt_v: {
				// 申请部门
				data: [
					{
						fields: [ 'pk_applyorg' ],
						returnName: 'pk_org'
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			pk_proposer: {
				// 申请人
				data: [
					{
						fields: [ 'pk_applyorg' ],
						returnName: 'pk_org'
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			pk_transactor: {
				// 经办人
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			}
		}
	},
	btn_head: {
		//头部按钮
		ADD: 'Add', //新增
		EDIT: 'Edit', //修改
		COMMIT: 'Commit', //提交
		UNCOMMIT: 'UnCommit', //收回
		DELETE: 'Delete', //删除
		SAVE: 'Save', //保存
		SAVECOMMIT: 'SaveCommit', //保存提交
		CANCEL: 'Cancel', //取消
		REFRESH: 'Refresh', //返回
		PRINT: 'Print', //打印
		OUTPUT: 'Output', //输出
		BATCHALTER: 'BatchAlter', //批改
		FARECEIPTSCAN: 'FAReceiptScan', //影像扫描
		FARECEIPTSHOW: 'FAReceiptShow', //影像查看
		ATTACHMENT: 'Attachment', //附件
		QUERYABOUTBUDGET: 'queryAboutBudget', //联查预算
		QUERYABOUTBILLFLOW: 'QueryAboutBillFlow', //审批详情
		QUERYABOUTASSET: 'QueryAboutAsset', //联查卡片
		COPY: 'Copy' //复制
	},
	btn_body: {
		//表体肩部按钮
		ADD_LINE: 'AddLine', //增行
		DELETE_LINE: 'DelLine', //删行
		BATCH_ALTER: 'BatchAlter' //批改
	},
	btn_body_inner: {
		//表体操作列
		EXTEND: 'Extend', //展开
		DELETE_LINE: 'DelLine', //删行
		COPY: 'CopyLine' //复制
	},
	LinkageClearConfig: {
		//查询区
		searchId: 'bodyvos',
		linkageData: {
			//联动链
			//参照字段:['被过滤字段1'，'被过滤字段2']
			pk_org_v: [
				'asset_name',
				'pk_category',
				'spec',
				'card_model',
				'card_num',
				'currmoney',
				'tax_cost',
				'other_cost',
				'tax_input',
				'sum_money',
				'pk_usedept'
			],
			pk_equiporg: [ 'pk_costcenter', 'pk_usedept', 'pk_assetuser' ],
			pk_ownerorg: [ 'pk_mandept' ]
		}
	},
	// 表肩隐藏的按钮
	hideBodyBtns: [ 'AddLine', 'BatchAlter' ]
};
