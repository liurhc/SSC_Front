/**
 * 新增资产审批单 卡片态
 * @author cuixuh
 */
import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	headAfterEvent,
	bodyAfterEvent,
	pageInfoClick,
	backToList,
	modelSave,
	rowSelected,
	beforeEvent,
	commitAction
} from './events';
import { pageConfig } from './const';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { multiLangUtils, closeBrowserUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { ApprovalTrans } = components;
const { UISTATE } = StatusUtils;
const { NCAffix } = base;
const { Inspection, ApproveDetail } = high;
const { title, formId, tableId, pagecode, dataSource } = pageConfig;

class Card extends Component {
	constructor(props) {
		super(props);
		this.is_allow_dept_midlev = false; //是否允许部门非末级(组织级数据)
		this.state = {
			show: false,
			sourceData: null,
			transi_type: '',
			pk_newasset: '',
			compositedisplay: false,
			compositedata: {}
		};
		closeBrowserUtils.call(this, props, { form: [ formId ], cardTable: [ tableId ] });
		initTemplate.call(this, props);
	}

	componentDidMount() {}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, this.props, 'SAVE', 'commit', value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	//获取列表肩部信息
	getTableHead = () => {
		let { createButtonApp } = this.props.button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	render() {
		let { cardTable, form, button, ncmodal, cardPagination, ncUploader } = this.props;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		let { createNCUploader } = ncUploader;
		let { createModal } = ncmodal;
		let status = form.getFormStatus(formId) || UISTATE.browse;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title) /* 国际化处理： 新增资产审批单*/,
								formId,
								backBtnClick: backToList
							})}
							{/* 按钮区 btn-area */}
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{/*浏览态增加 上一页/下一页 */}
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: modelSave.bind(this),
							showCheck: true,
							showIndex: true,
							selectedChange: rowSelected.bind(this),
							onBeforeEvent: beforeEvent.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this)
						})}
					</div>
				</div>
				{/* 提示框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/* 联查预算 */}
				<Inspection
					show={this.state.show}
					sourceData={this.state.sourceData}
					cancel={() => {
						this.setState({ show: false });
					}}
					affirm={() => {
						this.setState({ show: false });
					}}
				/>
				{/*审批详情*/}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_newasset}
				/>
				{/*附件*/}
				{createNCUploader(`${pagecode}-uploader`, {disableModify:this.props.form.getFormItemsValue("card_head", 'bill_status')&&this.props.form.getFormItemsValue("card_head", 'bill_status').value!="0"})}
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} /* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		billtype: 'card',
		pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(Card);

export default Card;
