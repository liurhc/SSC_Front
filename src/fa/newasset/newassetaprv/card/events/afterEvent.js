import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { addLine } from './buttonClick';
import ampub from 'ampub';
const { components } = ampub;
const { OrgChangeEvent } = components;
const { orgChangeEvent } = OrgChangeEvent;
import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage } = fa_components;
const { referLinkageClear } = ReferLinkage;
const { hideBodyBtns, formId, tableId, pagecode, url, CardBodyAfterEventConfig } = pageConfig;
//财务组织编辑后事件
export function headAfterEvent(props, moduleId, key, value, oldValue, record) {
	if (key == 'pk_org_v') {
		let callback = () => {
			//更换组织之后需要清空表体行
			props.cardTable.setTableData(tableId, { rows: [] });
			//如果财务组织不为空，需要默认增一行
			if (value.value != null) {
				defaultAddLine.call(this, props);
			}
		};
		orgChangeEvent.call(
			this,
			props,
			pagecode,
			formId,
			tableId,
			key,
			value,
			oldValue,
			false,
			hideBodyBtns,
			callback
		);
	}
	if (key == 'pk_applyorg_v') {
		//若修改申请单位，则清空申请部门和申请人
		if (value.value == '' || value.value == null || oldValue.value != value.value) {
			props.form.setFormItemsValue(formId, {
				pk_applydapt_v: { value: null, display: null },
				pk_proposer: { value: null, display: null }
			});
		}
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	// 初始化为后台平台可以使用的格式
	let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, index);

	//新增功能---差异化处理
	let newRecord = JSON.parse(JSON.stringify(record || {}));
	data.card.body[moduleId] = {
		...data.card.body[moduleId],
		rows: [ newRecord ]
	};
	data.index = 0; //修改编辑行为0

	if ([ 'currmoney', 'other_cost', 'tax_cost', 'tax_input', 'sum_money' ].includes(key)) {
		bodyAfterEditAjax.call(this, props, data);
	}
	if (key === 'pk_ownerorg_v' || key === 'pk_equiporg_v') {
		// 编辑货主管理组织，清空管理部门   编辑使用权，清空使用部门和使用人
		referLinkageClear(props, key, CardBodyAfterEventConfig, 'cardtable', moduleId, index);
	}
}

//编辑后事件后如果表体被清空默认增行
export function defaultAddLine(props) {
	let allRows = props.cardTable.getAllRows(tableId);
	let rowCount = 0;
	if (allRows) {
		rowCount = allRows.length;
	}
	if (rowCount == 0) {
		addLine.call(this, props);
	}
}

/**
 * 调后台处理编辑后事件的数据
 * @param {*} props 
 * @param {*} data 
 */
function bodyAfterEditAjax(props, data) {
	ajax({
		url: url.BODYAFTEREDIT,
		data,
		async: false,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				//新增功能---差异化处理
				props.cardTable.updateDataByRowId(tableId, data.body[tableId]);
			}
		}
	});
}
