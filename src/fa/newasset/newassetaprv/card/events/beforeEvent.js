import { pageConfig } from '../const';
const { CardBodyConfig, tableId, formId, url } = pageConfig;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter, DeptMidlevRefFilter } = fa_components;
const { addBodyReferFilter } = ReferFilter;
const { queryOnlyLeafCanSelect } = DeptMidlevRefFilter;

export default function beforeEvent(props, moduleId, key, value, index, record) {
	if (key == 'pk_mandept_v' || key == 'pk_usedept') {
		let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
		//表体部门非末级处理
		let field = [ 'pk_mandept', 'pk_mandept_v' ]; //传管理部门字段
		let assembleData = {
			formId,
			tableId,
			pk_org,
			field
		};
		//该方法只能处理管理部门，使用部门在表体参照过滤中进行处理
		queryOnlyLeafCanSelect.call(this, props, assembleData);
		//使用部门非末级处理
		CardBodyConfig.onlyLeafCanSelect.pk_usedept = !this.is_allow_dept_midlev;
	}
	let meta = props.meta.getMeta();
	//表体参照过滤
	addBodyReferFilter.call(this, props, meta, CardBodyConfig, record);
	return true;
}
