import { loadDataByPk } from './buttonClick';

//卡片分页查询
export default function(props, pk) {
	if (!pk || pk == 'null') {
		return;
	}
	// 更新参数
	props.setUrlParam({ id: pk });
	loadDataByPk.call(this, props, pk);
}
