import { pageConfig } from '../const';
import tableButtonClick from './tableButtonClick';
import { buttonClick, setStatus, copyDataByPk, loadDataByPk } from '../events';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { UISTATE } = StatusUtils;
const { afterModifyCardMeta } = cardUtils;
const { loginContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter, DeptMidlevRefFilter } = fa_components;
const { addHeadAreaReferFilter } = ReferFilter;
const { rewriteMeta } = DeptMidlevRefFilter;
const { tableId, pagecode, CardHeadConfig, formId } = pageConfig;

//加载模板
export default function(props) {
	props.createUIDom(
		{
			//页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext.call(this, data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status') || UISTATE.browse;
	if (status == UISTATE.add) {
		buttonClick.call(this, props, 'Add');
	} else if (status == UISTATE.edit) {
		let pk = this.props.getUrlParam('id');
		if (pk && pk != 'undefined') {
			loadDataByPk.call(this, this.props, pk);
			setStatus.call(this, props, UISTATE.edit);
			this.props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});
		}
	} else {
		let pk = this.props.getUrlParam('id');
		if (pk && pk != 'undefined') {
			//判断是否是复制跳转
			let copyflag = this.props.getUrlParam('copyflag');
			if (copyflag && copyflag != 'undefined') {
				//等发完ajax请求之后再设置按钮会有明显的按钮变换，所以在查询前先设置
				setStatus.call(this, props, UISTATE.add);
				copyDataByPk.call(this, this.props, pk);
			} else {
				loadDataByPk.call(this, this.props, pk);
				setStatus.call(this, props, status);
			}
		}
	}
}

//模板修改
function modifierMeta(props, meta) {
	//表头参照过滤
	addHeadAreaReferFilter.call(this, props, meta, CardHeadConfig);
	//表体部门非末级处理
	let field = [ 'pk_mandept', 'pk_mandept_v' ]; //传管理部门字段
	let assembleData = {
		meta,
		formId,
		tableId,
		field
	};
	rewriteMeta.call(this, props, assembleData);
	//表体添加操作列
	let porCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		itemtype: 'customer', //操作列的制定类型
		className: 'table-opr',
		width: 150,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			//新写法
			let buttonAry = status === UISTATE.browse ? [ 'Extend' ] : [ 'Extend', 'DelLine', 'CopyLine' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index, tableId);
				}
			});
		}
	};
	meta[tableId].items.push(porCol);
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);

	return meta;
}
