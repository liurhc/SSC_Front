import React, { Component } from 'react';
import { base, high, createPageIcon } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	doubleClick,
	rowSelected,
	commitAction,
	setBatchBtnsEnable,
	searchAfterEvent
} from './events';
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans } = components;
const { BillTrack, ApproveDetail } = high;
const { NCAffix } = base;
/**
 * 转固列表
 * wangwhf
 * 2018/08/08
 */
class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// 单据追溯
			trackshow: false,
			pk_bill: this.props.getUrlParam('id'),
			//审批详情
			showApprove: false,
			transi_type: 'HJ-01',
			compositedisplay: false,
			compositedata: {}
		};

		initTemplate.call(this, props);
	}
	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, 'SAVE', this.props, value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};
	render() {
		let { table, button, search, ncmodal, ncUploader, pageConfig } = this.props;
		let { createNCUploader } = ncUploader;
		let { createSimpleTable } = table;
		let { createButtonApp } = button;
		let { NCCreateSearch } = search;
		let { createModal } = ncmodal;
		let { searchId, tableId, dataSource, title, billtype, pkField } = pageConfig;
		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						dataSource: dataSource,
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: searchAfterEvent.bind(this)
					})}
				</div>

				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						handlePageInfoChange: pageInfoClick.bind(this),
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						showIndex: true,
						showCheck: true,
						dataSource: dataSource,
						pkname: pkField,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				{createModal('Delete', {})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 附件*/}
				{createNCUploader('uploader', {})}
				{/* 单据追溯 */}
				<BillTrack
					show={this.state.trackshow}
					close={() => {
						this.setState({ trackshow: false });
					}}
					pk={this.state.pk_bill}
					type={billtype}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay && (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} // 国际化：指派
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				)}
			</div>
		);
	}
}

export default List;
export { initTemplate };
