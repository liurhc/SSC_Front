import { ajax, toast } from 'nc-lightapp-front';
import { linkToCard } from './buttonClick';
import ampub from 'ampub';
const { components, commonConst } = ampub;
const { StatusUtils } = commonConst;
const { ScriptReturnUtils } = components;
const { UISTATE } = StatusUtils;
const { getScriptListReturn } = ScriptReturnUtils;

export default function tableButtonClick(props, key, text, record, index) {
	let { tableId } = props.pageConfig;
	switch (key) {
		// 表格操作按钮
		case 'Edit':
			edit.call(this, props, record, index);
			break;
		case 'Delete':
			delConfirm.call(this, props, record, index);
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', props, index, record);
			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', props, index, record);
			break;
		case 'QueryAboutBillFlow': //审批详情
			this.setState({
				showApprove: true,
				pk_bill: record.pk_transasset.value,
				transi_type: record.transi_type.value
			});
			break;
		default:
			break;
	}
}
/**
 * 修改
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function edit(props, record, index) {
	let { reqUrl, node_code } = props.pageConfig;
	ajax({
		url: reqUrl.edit,
		data: {
			pk: record.pk_transasset.value,
			resourceCode: node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'danger', content: res.data });
			} else {
				linkToCard.call(this, props, record, UISTATE.edit);
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
/**
 * 删除
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function delConfirm(props, record, index) {
	let { tableId, reqUrl, pagecode } = props.pageConfig;
	let id = record['pk_transasset'].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};
	ajax({
		url: reqUrl.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			if (res.data.errorMsg) {
				toast({ color: 'danger', content: res.data.errorMsg });
			} else {
				getScriptListReturn(params, res, props, 'pk_transasset', tableId, tableId, true);
				// 清除缓存
				props.table.deleteCacheId(tableId, id);
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
/**
 * 提交 收回 动作
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 * @param {*} record 
 */
function commitClick(OperatorType, props, index, record) {
	let { tableId, reqUrl, pagecode, dataSource } = props.pageConfig;
	let id = record['pk_transasset'].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: reqUrl.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecode,
			commitType
		},
		success: (res) => {
			if (res.success) {
				getScriptListReturn.call(
					this,
					params,
					res,
					props,
					'pk_transasset',
					tableId,
					tableId,
					false,
					dataSource
				);
			} else {
				toast({ content: res.data.errorMsg, color: 'danger' });
			}
		}
	});
}
