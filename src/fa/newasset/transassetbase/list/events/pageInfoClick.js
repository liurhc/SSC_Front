import { ajax } from 'nc-lightapp-front';
import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;

/**
 * 分页管理
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export default function(props, config, pks, isRefresh) {
	let { tableId, reqUrl, pagecode } = props.pageConfig;
	let data = {
		allpks: pks,
		pagecode
	};
	ajax({
		url: reqUrl.pageDataByPksUrl,
		data: data,
		success: function(res) {
			setListValue.call(this, props, res);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
