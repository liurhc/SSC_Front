import { ajax, toast } from 'nc-lightapp-front';
import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';
const { components, utils } = ampub;
const { listUtils } = utils;
const { LoginContext } = components;
const { setQueryInfoCache, setListValue, listConst } = listUtils;
const { getContext, loginContextKeys } = LoginContext;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, data, isRefresh) {
	let { pagecode, searchId, tableId, reqUrl, billtype } = props.pageConfig;
	if (!searchVal) {
		return;
	}
	// 获取交易类型编码
	let transi_type = getContext(loginContextKeys.transtype);
	let pageInfo = props.table.getTablePageInfo(tableId);
	if (!data) {
		data = props.search.getQueryInfo(searchId, true);
	}

	data.pageInfo = pageInfo;
	data.pagecode = pagecode;
	data.billtype = billtype;
	data.transtype = transi_type;

	// 缓存查询条件
	setQueryInfoCache.call(this, data, isRefresh, type);

	ajax({
		url: reqUrl.listQueryUrl,
		data: data,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBatchBtnsEnable.call(this, props, tableId);
		},
		error: (res) => {
			toast({ content: res.message, color: 'danger' });
		}
	});
}
