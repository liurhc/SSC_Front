import { tableButtonClick } from '../events';
import { setBatchBtnsEnable, linkToCard } from './buttonClick';
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { createOprationColumn } = listUtils;
const { loginContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

export default function(props) {
	let { pagecode } = props.pageConfig;
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					let { tableId } = props.pageConfig;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}
/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let { pageConfig = {} } = props;
	let { tableId } = pageConfig;
	setBatchBtnsEnable.call(this, props, tableId);
}
/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	let { tableId, referConfig, transi_type, searchId } = props.pageConfig;
	// 查询条件过滤
	addSearchAreaReferFilter(props, meta, referConfig);
	// 查询条件特殊处理
	meta[searchId].items.map((item) => {
		//按交易类型不同过滤不同的建卡方式
		if (item.attrcode === 'create_card_style') {
			let newoptions = [];
			if (transi_type === 'HJ-02') {
				item.options.map((option) => {
					if (option.value && option.value != '1') {
						newoptions.push(option);
					}
				});
			} else {
				item.options.map((option) => {
					if (option.value && option.value != '3') {
						newoptions.push(option);
					}
				});
			}
			item.options = newoptions;
		}
	});
	// 超链
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, 'browse');
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
	// 添加表格操作列
	let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
	meta[tableId].items.push(oprCol);
	return meta;
}
