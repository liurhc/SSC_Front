import { ajax, toast, print, output } from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
import ampub from 'ampub';
const { components, utils } = ampub;
const { msgUtils, listUtils } = utils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { ScriptReturnUtils } = components;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { getScriptListReturn } = ScriptReturnUtils;
export default function buttonClick(props, id) {
	switch (id) {
		case 'Add':
			linkToCard.call(this, props, undefined, 'add');
			break;
		case 'Delete':
			showConfirm.call(this, props, {
				type: MsgConst.Type.DelSelect,
				beSureBtnClick: DeleteAction
			});
			break;
		case 'Copy':
			CopyAction.call(this, props);
			break;
		case 'Commit':
			commitAction.call(this, 'SAVE', props);
			break;
		case 'UnCommit':
			commitAction.call(this, 'UNSAVE', props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refreshAction.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		case 'QueryAboutBusiness': // 单据追溯
			queryAboutBusiness.call(this, props);
			break;
		default:
			break;
	}
}
/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	let { cardRouter, pagecode } = props.pageConfig;
	props.pushTo(cardRouter, {
		status,
		id: record['pk_transasset'] ? record['pk_transasset'].value : '',
		pagecode: pagecode.replace('list', 'card')
	});
}
/**
 * delete
 * @param {*} props 
 */
function DeleteAction(props) {
	let { tableId, pagecode, reqUrl, formId, dataSource } = props.pageConfig;
	let data = props.table.getCheckedRows(tableId);
	if (data.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_transasset.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: reqUrl.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, 'pk_transasset', tableId, tableId, true, dataSource);
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}
/**
 * copy
 * @param {*} props 
 */
function CopyAction(props) {
	let { tableId, cardRouter, pagecode } = props.pageConfig;
	let data = props.table.getCheckedRows(tableId);
	// 选择多条数据时，默认复制第一条
	if (data.length == 0) {
		return;
	}
	let id = data[0].data.values.pk_transasset.value;
	props.pushTo(cardRouter, {
		status: 'browse',
		id,
		copyflag: true,
		pagecode: pagecode.replace('list', 'card')
	});
}
/**
 * 提交 、收回 
 */
export function commitAction(OperatorType, props, content) {
	let { tableId, reqUrl, pagecode, dataSource } = props.pageConfig;
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_transasset.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: reqUrl.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecode,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(this, params, res, props, 'pk_transasset', tableId, tableId, false, dataSource);
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}
/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, moduleId);
}
/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let { reqUrl } = props.pageConfig;
	let param = getPrintParam.call(this, props);
	if (!param) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}

	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printUrl, // 后台打印服务url
		param
	);
}
/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let { reqUrl } = props.pageConfig;
	let printData = getPrintParam.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: reqUrl.printUrl,
		data: printData
	});
}

export function getPrintParam(props, outputType = 'print') {
	let { tableId, pagecode, appcode } = props.pageConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values.pk_transasset.value);
	});
	let param = {
		filename: pagecode, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType
	};

	return param;
}
/**
 * 重新加载页面
 * @param {*} props 
 */
function refreshAction(props) {
	batchRefresh.call(this, props, searchBtnClick);
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	let { tableId } = props.pageConfig;
	let checkedrows = props.table.getCheckedRows(tableId);
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billNo 是单据主键
	let billId = checkedrows[0].data.values['pk_transasset'].value;
	props.ncUploader.show('uploader', {
		billId: 'fa/transasset/' + billId,
		billNo
	});
}
/**
 * 单据追溯
 * @param {*} props 
 */
function queryAboutBusiness(props) {
	let { tableId } = props.pageConfig;
	let checkedrows = props.table.getCheckedRows(tableId);
	// billNo 是单据主键
	let pk_bill = checkedrows[0].data.values['pk_transasset'].value;
	this.setState({
		trackshow: true,
		pk_bill
	});
}
