import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage } = fa_components;
const { referLinkageClear } = ReferLinkage;

export default function searchAfterEvent(field, value) {
	let { searchId, referConfig } = this.props.pageConfig;
	//联动处理
	referLinkageClear(this.props, field, referConfig);

	if (field === 'pk_org') {
		this.props.search.setSearchValByField(searchId, 'pk_recorder', {}); //清空经办人
	}
}
