import ampub from 'ampub';
const { commonConst } = ampub;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;

export const baseConfig = {
	moduleId: '2012', //所属模块编码
	searchId: 'searchArea', //查询区域编码
	tableId: 'list_head', //列表表体编码
	formId: 'card_head', //卡片表头编码
	cardRouter: '/card',
	billtype: 'HJ',
	pkField: 'pk_transasset',
	referConfig: {
		searchId: 'searchArea',
		formId: 'card_head',
		bodyIds: [ 'list_head' ],
		specialFields: {
			pk_asset_user: {
				//使用人
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_equiporg', 'pk_org' ],
						returnName: 'pk_org'
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			// 工程项目
			pk_project: {
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			},
			// 工程项目
			pk_sub_project: {
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			},
			// 供应商
			pk_supplier: {
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			}
		},

		linkageData: {
			//联动链
			//参照字段:['被过滤字段1'，'被过滤字段2']
			pk_org: [
				'pk_recorder', //经办人
				'bodyvos.pk_ownerorg',
				'bodyvos.pk_usedept', //使用部门
				'bodyvos.pk_mandept', //管理部门
				'bodyvos.pk_category', //资产类别
				'bodyvos.pk_asset_user' //使用人
			],
			pk_equiporg: [ 'bodyvos.pk_usedept', 'bodyvos.pk_asset_user' ],
			pk_ownerorg: [ 'bodyvos.pk_mandept' ]
		}
	},

	reqUrl: {
		insertUrl: '/nccloud/fa/transasset/insert.do',
		updateUrl: '/nccloud/fa/transasset/update.do',
		listQueryUrl: '/nccloud/fa/transasset/listquery.do',
		cardQueryUrl: '/nccloud/fa/transasset/cardquery.do',
		deleteUrl: '/nccloud/fa/transasset/delete.do',
		commitUrl: '/nccloud/fa/transasset/commit.do',
		pageDataByPksUrl: '/nccloud/fa/transasset/querypagegridbypks.do', // 分页查询
		printUrl: '/nccloud/fa/transasset/printCard.do',
		edit: '/nccloud/fa/transasset/edit.do'
	}
};
