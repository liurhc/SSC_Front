import ampub from 'ampub';
const { commonConst } = ampub;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;

export const baseConfig = {
	tableId: 'bodyvos', //列表表体编码
	formId: 'card_head', //列表表头编码
	moduleId: '2012', //所属模块编码
	listRouter: '/list',
	pkField: 'pk_transasset',
	bill_type: 'HJ',
	reqUrl: {
		insertUrl: '/nccloud/fa/transasset/insert.do',
		updateUrl: '/nccloud/fa/transasset/update.do',
		listQueryUrl: '/nccloud/fa/transasset/listquery.do',
		cardQueryUrl: '/nccloud/fa/transasset/cardquery.do',
		deleteUrl: '/nccloud/fa/transasset/delete.do',
		commitUrl: '/nccloud/fa/transasset/commit.do',
		pageDataByPksUrl: '/nccloud/fa/transasset/querypagegridbypks.do', // 分页查询
		bodyAfterEdit: '/nccloud/fa/transasset/bodyafteredit.do',
		headAfterEdit: '/nccloud/fa/transasset/headafteredit.do',
		printUrl: '/nccloud/fa/transasset/printCard.do',
		batchAlter: '/nccloud/fa/transasset/batchAltert.do',
		edit: '/nccloud/fa/transasset/edit.do',
		batchByFormula: '/nccloud/fa/transasset/batchFormula.do',
		openTransassetMsgURL: '/nccloud/fa/transasset/openTransassetMsg.do'
	},
	referConfig: {
		searchId: 'searchArea',
		formId: 'card_head',
		bodyIds: [ 'bodyvos', 'bodyvos_edit' ],
		onlyLeafCanSelect: {
			// 使用部门非末级处理，默认不可勾选
			pk_usedept_v: true
		},
		specialFields: {
			pk_ownerorg_v: {
				addOrgRelation: 'pk_orgs',
				RefActionExtType: 'TreeRefActionExt',
				class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_OWNERORG_VSqlBuilder',
				//货主管理组织
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			},
			pk_usedept_v: {
				// 使用部门
				data: [
					{
						fields: [ 'pk_equiporg', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			pk_mandept_v: {
				// 管理部门
				data: [
					{
						fields: [ 'pk_ownerorg', 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			pk_asset_user: {
				//使用人
				orgMulti: 'pk_org',
				data: [
					{
						fields: [ 'pk_equiporg', 'pk_org' ],
						returnName: 'pk_org'
					},
					{
						returnConst: IBusiRoleConst.ASSETORG,
						returnName: 'busifuncode'
					}
				]
			},
			pk_construct_unit: {
				// 建设单位
				data: [
					{
						fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			},
			pk_project: {
				// 工程项目
				data: [
					{
						fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			},
			pk_sub_project: {
				// 工程项目
				data: [
					{
						fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			},
			pk_material_v: {
				// 物料
				RefActionExtType: 'GridRefActionExt',
				class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_MATERIAL_VSqlBuilder',
				data: [
					{
						fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			}
		}
	},
	// 编辑态按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'AddLine', 'DelLine', 'BatchAlter', 'CopyLine' ],
	// 浏览态按钮
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Copy',
		'Commit',
		'UnCommit',
		'Attachment',
		'Refresh',
		'More',
		'QueryAboutBillFlow'
	],
	// 表肩隐藏的按钮
	hideBodyBtns: [ 'AddLine', 'DelLine', 'BatchAlter', 'CopyLine' ]
};
