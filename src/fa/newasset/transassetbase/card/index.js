import React, { Component } from 'react';
import { base, high } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	bodyAfterEvent,
	headAfterEvent,
	beforeEvent,
	pageInfoClick,
	saveAction,
	backToList,
	rowSelected,
	CommitAction,
	addRowAfter
} from './events';
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils, cardUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans } = components;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { NCAffix } = base;
const { BillTrack, ApproveDetail } = high;
/**
 * 转固
 * wangwhf
 * 2018/08/08
 */
class Card extends Component {
	constructor(props) {
		super(props);
		this.is_allow_dept_midlev = false; //是否允许部门非末级(组织级数据)
		this.state = {
			// 单据追溯
			trackshow: false,
			pk_bill: this.props.getUrlParam('id'),
			// 审批详情
			showApprove: false,
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		// 放置财务组织本位币种，用于增行赋值
		this.currency = {
			value: '',
			display: ''
		};
		closeBrowserUtils.call(this, props, {
			form: [ props.pageConfig.formId ],
			cardTable: [ props.pageConfig.tableId ]
		});
		initTemplate.call(this, props);
	}
	componentDidMount() {}
	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};
	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 4,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};
	//提交及指派 回调
	getAssginUsedr = (value) => {
		CommitAction.call(this, this.props, 'SAVE', 'commit', value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};
	render() {
		let { cardTable, form, button, ncmodal, ncUploader, pageConfig } = this.props;
		let { formId, pagecode, tableId, title, dataSource, bill_type } = pageConfig;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		return (
			<div id="nc-bill-card" className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title),
								formId,
								backBtnClick: backToList
							})}

							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: saveAction.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this),
							showIndex: true,
							showCheck: true,
							isAddRow: true,
							selectedChange: rowSelected.bind(this),
							addRowCallback: addRowAfter.bind(this, this.props)
						})}
					</div>
				</div>
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/* 单据追溯 */}
				<BillTrack
					show={this.state.trackshow}
					close={() => {
						this.setState({ trackshow: false });
					}}
					pk={this.state.pk_bill}
					type={bill_type}
				/>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 附件*/}
				{createNCUploader('uploader', {})}
				{/* 提交及指派 */}
				{this.state.compositedisplay && (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} // 国际化：指派
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				)}
			</div>
		);
	}
}

export default Card;
export { initTemplate };
