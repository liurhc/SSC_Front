import { ajax, toast } from 'nc-lightapp-front';
import { setValue } from './buttonClick';
import ampub from 'ampub';
const { components } = ampub;
const { OrgChangeEvent } = components;
const { orgChangeEvent } = OrgChangeEvent;

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key  
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	let { reqUrl, pagecode, formId, tableId, hideBodyBtns } = props.pageConfig;
	let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	if (key == 'pk_org_v') {
		let callback = () => {
			let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v').value;
			if (!pk_org_v) {
				//更换组织之后需要清空表体行
				props.cardTable.setTableData(tableId, { rows: [] });
			} else {
				ajax({
					url: reqUrl.headAfterEdit,
					data,
					success: (res) => {
						let { success, data } = res;
						if (success) {
							setValue.call(this, props, data);
						}
						// 取出财务组织币种暂存，用于增行赋给默认值
						let originvalue = data.body[tableId].rows[0].values.pk_originvalue_type;
						this.currency = {
							display: originvalue.display,
							value: originvalue.value
						};
					}
				});
			}
		};
		orgChangeEvent.call(
			this,
			props,
			pagecode,
			formId,
			tableId,
			key,
			value,
			oldValue,
			false,
			hideBodyBtns,
			callback
		);
	}
}
/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	// 类别判断，不能为空且必须为最末级
	if (key === 'pk_category') {
		if (!value.refpk) {
			return;
		}
		basebodyaftereditevent.call(this, props, moduleId, key, value, changedRows, index, record);
	} else if (key === 'pk_originvalue_type') {
		// 表体币种编辑后，更新金额的精度为币种精度:结算金额，审计调整，摊入金额，转固金额
		if (!value.refpk) {
			return;
		}
		basebodyaftereditevent.call(this, props, moduleId, key, value, changedRows, index, record);
	} else if (key === 'pk_ownerorg_v') {
		// 编辑货主管理组织，清空管理部门
		handlePkOwnerOrgVAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index);
	} else if (key === 'pk_equiporg_v') {
		// 编辑使用权，清空使用部门和使用人
		handlePkEquipOrgVAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index);
	}
}
/**
 * 货主管理组织
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
function handlePkOwnerOrgVAfterEditEvent(props, moduleId, key, value, changedrows, i) {
	// 清空管理部门
	props.cardTable.setValByKeyAndIndex(moduleId, i, 'pk_mandept_v', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(moduleId, i, 'pk_mandept', { value: null, display: null });
}
/**
 * 使用权
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i  
 */
function handlePkEquipOrgVAfterEditEvent(props, moduleId, key, value, changedrows, i) {
	// 清空使用部门，使用人
	props.cardTable.setValByKeyAndIndex(moduleId, i, 'pk_usedept_v', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(moduleId, i, 'pk_usedept', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(moduleId, i, 'pk_asset_user', { value: null, display: null });
}
/**
 * 表体公共ajax方法
 */

function basebodyaftereditevent(props, moduleId, key, value, changedrows, i, record) {
	let { reqUrl, pagecode, formId, tableId } = props.pageConfig;
	let cardData = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedrows);
	//新增功能---差异化处理
	let newRecord = JSON.parse(JSON.stringify(record || {}));
	cardData.card.body[moduleId] = {
		...cardData.card.body[moduleId],
		rows: [ newRecord ]
	};
	cardData.index = 0; //修改编辑行为0
	ajax({
		url: reqUrl.bodyAfterEdit,
		data: cardData,
		async: false,
		success: (res) => {
			let { success, data } = res;
			// if (success) {
			if (res.data && res.data.body && res.data.body[tableId]) {
				// setValue.call(this, props, data);
				//新增功能---差异化处理
				props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			//清空编辑字段值
			props.cardTable.setValByKeyAndIndex(moduleId, i, key, { value: null, display: null });
		}
	});
}
