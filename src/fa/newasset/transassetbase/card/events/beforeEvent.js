import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter, DeptMidlevRefFilter } = fa_components;
const { addBodyReferFilter } = ReferFilter;
const { queryOnlyLeafCanSelect } = DeptMidlevRefFilter;

/**
 * 编辑前处理
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} index 
 * @param {*} record 
 */
export default function beforeEvent(props, moduleId, key, value, index, record) {
	let { formId, tableId, referConfig } = props.pageConfig;
	if (key == 'pk_mandept_v' || key == 'pk_usedept_v') {
		let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
		//表体部门非末级处理
		let field = [ 'pk_mandept', 'pk_mandept_v' ]; //传管理部门字段
		let assembleData = {
			formId,
			tableId,
			pk_org,
			field
		};
		//该方法只能处理管理部门，使用部门在表体参照过滤中进行处理
		queryOnlyLeafCanSelect.call(this, props, assembleData);
		//使用部门非末级处理
		referConfig.onlyLeafCanSelect.pk_usedept = !this.is_allow_dept_midlev;
	}
	// 表体参照过滤
	bodyReferFilter.call(this, props, record);

	return true;
}
/**
 * 表体参照
 * @param {*} props 
 * @param {*} record 
 */
function bodyReferFilter(props, record) {
	let { referConfig } = props.pageConfig;
	let meta = props.meta.getMeta();
	// 表体参照过滤
	addBodyReferFilter.call(this, props, meta, referConfig, record);
}
