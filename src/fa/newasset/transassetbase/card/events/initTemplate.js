import { tableButtonClick, buttonClick, setStatus, copyDataByPk, loadDataByPk, getTransassetMsg } from '../events';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { UISTATE } = StatusUtils;
const { afterModifyCardMeta } = cardUtils;
const { loginContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter, DeptMidlevRefFilter } = fa_components;
const { addHeadAreaReferFilter } = ReferFilter;
const { rewriteMeta } = DeptMidlevRefFilter;

export default function(props) {
	let { pagecode } = props.pageConfig;
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}
/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status');
	if (status == 'add') {
		buttonClick.call(this, props, 'Add');
	} else if (status == 'edit') {
		let pk = props.getUrlParam('id');
		if (pk && pk != 'undefined') {
			loadDataByPk.call(this, props, pk);
			setStatus.call(this, props, 'edit');
			props.form.setFormItemsDisabled(props.pageConfig.formId, {
				pk_org_v: true,
				pk_org: true
			});
		}
	} else {
		let pk = this.props.getUrlParam('id');
		if (pk && pk != 'undefined') {
			let copyflag = props.getUrlParam('copyflag');
			if (copyflag && copyflag != 'undefined') {
				copyDataByPk.call(this, props, pk); // 复制数据
			} else {
				loadDataByPk.call(this, props, pk);
				setStatus.call(this, props, status);
			}
		} else {
			//处理消息打开
			let pkMsg = props.getUrlParam('pk_msg');
			if (pkMsg && pkMsg != 'undefined' && pkMsg != 'null') {
				setStatus.call(this, props, status);
				getTransassetMsg.call(this, props, pkMsg);
			}
		}
	}
}
function modifierMeta(props, meta) {
	let { formId, tableId, referConfig, transi_type } = props.pageConfig;
	// 表头参照过滤
	addHeadAreaReferFilter.call(this, props, meta, referConfig);
	// 表头按交易类型不同过滤不同的建卡方式
	meta[formId].items.map((item) => {
		if (item.attrcode === 'create_card_style') {
			let newoptions = [];
			if (transi_type === 'HJ-02') {
				item.options.map((option) => {
					if (option.value && option.value != '1') {
						newoptions.push(option);
					}
				});
			} else {
				item.options.map((option) => {
					if (option.value && option.value != '3') {
						newoptions.push(option);
					}
				});
			}
			item.options = newoptions;
		}
	});
	//表体部门非末级处理
	let field = [ 'pk_mandept', 'pk_mandept_v' ]; //传管理部门字段
	let assembleData = {
		meta,
		formId,
		tableId,
		field
	};
	rewriteMeta.call(this, props, assembleData);
	// 添加表格操作列
	//表体添加操作列
	let oprCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000'),
		visible: true,
		itemtype: 'customer', //操作列的制定类型
		className: 'table-opr',
		width: 150,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			//新写法
			let buttonAry = status === UISTATE.browse ? [ 'OpenCard' ] : [ 'OpenCard', 'DelLine', 'CopyLine' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index, tableId);
				}
			});
		}
	};
	meta[tableId].items.push(oprCol);
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);

	return meta;
}
