import { ajax, toast, print, output, cardCache } from 'nc-lightapp-front';
import { headAfterEvent } from './afterEvent';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { multiLangUtils, msgUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { saveValidatorsUtil, LoginContext, ScriptReturnUtils, faQueryAboutUtils } = components;
const { UISTATE } = StatusUtils;
const { beforeSaveValidator } = saveValidatorsUtil;
const { openAssetCardByChooseRow } = faQueryAboutUtils;
const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = cardUtils;
const { getContext, loginContextKeys } = LoginContext;
const { getScriptCardReturnData } = ScriptReturnUtils;
import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage, ImageMng } = fa_components;
const { referAlterClear } = ReferLinkage;
const { faImageScan, faImageView } = ImageMng;

/**
 * 卡片动作按钮
 * @param {*} props 
 * @param {*} id 
 */
export default function(props, id) {
	switch (id) {
		case 'Add':
			AddAction.call(this, props);
			break;
		case 'Copy':
			CopyAction.call(this, props);
			break;
		case 'Save':
			saveAction.call(this, props);
			break;
		case 'Edit':
			EditAction.call(this, props);
			break;
		case 'Delete':
			showConfirm.call(this, props, {
				type: MsgConst.Type.Delete,
				beSureBtnClick: DeleteAction
			});
			break;
		case 'Cancel':
			showConfirm.call(this, props, {
				type: MsgConst.Type.Cancel,
				beSureBtnClick: CancelAction
			});
			break;
		case 'Commit':
			CommitAction.call(this, props, 'SAVE', 'commit');
			break;
		case 'SaveCommit':
			CommitAction.call(this, props, 'SAVE', 'saveCommit');
			break;
		case 'UnCommit':
			CommitAction.call(this, props, 'UNSAVE', '');
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'QueryAboutBusiness': // 单据追溯
			queryAboutBusiness.call(this, props);
			break;
		case 'AddLine':
			addLine.call(this, props);
			break;
		case 'DelLine':
			delrowaction.call(this, props);
			break;
		case 'BatchAlter': // 批改
			BatchAltert.call(this, props);
			break;
		case 'FAReceiptScan': // 影像扫描
			FAReceiptScan.call(this, props);
			break;
		case 'FAReceiptShow': // 影像查看
			FAReceiptShow.call(this, props);
			break;
		case 'QueryAboutAsset': // 联查固定资产卡片
			queryAboutAsset.call(this, props);
			break;
		case 'Attachment': //附件
			attachment.call(this, props);
			break;
		case 'QueryAboutBillFlow': // 审批详情
			queryAboutBillFlow.call(this, props);
			break;
		default:
			break;
	}
}
// add
function AddAction(props) {
	let { formId, tableId } = props.pageConfig;
	props.cardTable.closeExpandedRow(tableId);
	// 设置新增态
	setStatus.call(this, props, UISTATE.add);
	// 清空数据
	setValue.call(this, props, undefined);
	// 设置默认值
	setDefaultValue.call(this, props);
	// 设置字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (pk_org_v && pk_org_v.value) {
		headAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		setBodyBtnsEnable.call(this, props, tableId);
	}
}
/**
 * 设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	let { formId, bill_type } = props.pageConfig;
	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId);
	let groupName = getContext(loginContextKeys.groupName);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	let pk_transitype = getContext(loginContextKeys.pk_transtype);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org);
	let org_Name = getContext(loginContextKeys.org_Name);
	let pk_org_v = getContext(loginContextKeys.pk_org_v);
	let org_v_Name = getContext(loginContextKeys.org_v_Name);
	let businessdate = getContext(loginContextKeys.businessDate);
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype },
		bill_status: { display: getMultiLangByID('statusUtils-000000'), value: '0' }, // 自由态
		create_card_style: { value: '0' },
		business_date: { value: businessdate }
	});
}
/**
 * 修改
 * @param {*} props 
 */
function EditAction(props) {
	let { reqUrl, tableId, formId, pkField } = props.pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField).value || props.getUrlParam('id');
	ajax({
		url: reqUrl.edit,
		data: {
			pk,
			resourceCode: '2012016025'
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'danger', content: res.data });
			} else {
				props.cardTable.closeExpandedRow(tableId);
				setStatus.call(this, props, UISTATE.edit);
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
/**
 * 删除
 * @param {*} props 
 */
function DeleteAction(props) {
	let { reqUrl, pagecode, formId, tableId, dataSource, pkField } = props.pageConfig;
	let paramInfoMap = {};
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	paramInfoMap[pk] = ts;
	ajax({
		url: reqUrl.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			let { success } = res;
			if (success) {
				let callback = (newpk) => {
					// 更新参数
					props.setUrlParam({ id: newpk });
					// 加载下一条数据
					loadDataByPk.call(this, props, newpk);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					undefined,
					true,
					callback,
					pagecode
				);
			}
		}
	});
}
// copy
function CopyAction(props) {
	let { formId, tableId } = props.pageConfig;
	// 清空表头部分字段
	props.form.setFormItemsValue(formId, { pk_transasset: null });
	props.form.setFormItemsValue(formId, { ts: null });
	props.form.setFormItemsValue(formId, { bill_code: null });
	props.form.setFormItemsValue(formId, { billmaker: null });
	props.form.setFormItemsValue(formId, { billmaketime: null });
	props.form.setFormItemsValue(formId, { auditor: null });
	props.form.setFormItemsValue(formId, { audittime: null });
	props.form.setFormItemsValue(formId, {
		bill_status: { display: getMultiLangByID('statusUtils-000000'), value: '0' }
	}); // 自由态
	// 清空表体相关字段信息
	let body = props.cardTable.getAllData(tableId);
	body.rows.map((ele) => {
		// 转固相关字段
		ele.values.pk_transasset = {};
		ele.values.pk_transasset_b = {};
		ele.values.ts = {};
		// 下游卡片相关字段
		ele.values.pk_card = {}; // 卡片编码
		ele.values.created_num = { value: '0' }; //已生成卡片数量
		// 上游来源相关字段
		ele.values.pk_bill_b_src = {}; // 来源表体主键
		ele.values.pk_bill_src = {}; //来源单据主键
		ele.values.pk_transitype_src = {}; // 来源交易类型
		ele.values.transi_type_src = {}; //来源交易类型编码
		ele.values.bill_type_src = {}; // 来源单据类型
		ele.values.bill_code_source = {}; // 单据号
	});
	props.cardTable.setTableData(tableId, body);

	setStatus.call(this, props, UISTATE.add);
}

/**
 * save
 * @param {*} props 
 */
export function saveAction(props) {
	let { reqUrl, pagecode, formId, tableId, pkField } = props.pageConfig;
	// 保存前校验
	let keys = [ 'amount', 'pk_originvalue_type', 'created_num' ];
	let saveValidator = beforeSaveValidator.call(this, props, formId, tableId, keys);
	// 如果校验不通过，则返回
	if (!saveValidator) {
		return;
	}
	let data = props.createMasterChildDataSimple(pagecode, formId, tableId);
	// 判断表体行数量字段是否为0
	let row = 1;
	let amountStr = '';
	data.body.bodyvos.rows.map((currow) => {
		if (currow.values.amount && currow.values.amount.value === '0') {
			if (!amountStr) {
				amountStr = row;
			} else {
				amountStr = amountStr + ',' + row;
			}
		}
		row = row + 1;
	});
	// 如果存在行中数量为0的列，则给出提示
	if (amountStr) {
		toast({ color: 'danger', content: getMultiLangByID('201201524A-000002', { rows: amountStr }) });
		return;
	}
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let url = reqUrl.insertUrl;
	let oldstatus = UISTATE.add;
	if (pk && pk != null) {
		url = reqUrl.updateUrl;
		oldstatus = UISTATE.edit;
		data.body.bodyvos.rows.map((currow) => {
			currow.values.pk_transasset = { value: props.form.getFormItemsValue(formId, pkField).value };
		});
	}
	// 保存前执行验证公式
	props.validateToSave(data, () => {
		ajax({
			url: url,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//关闭侧拉
					props.cardTable.closeModel(tableId);
					afterSave.call(this, props, data, oldstatus);
				}
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
			}
		});
	});
}
/**
* 保存后
* @param {*} props 
*/
export function afterSave(props, data, oldstatus) {
	let { pagecode, formId, dataSource, pkField, tableId } = props.pageConfig;
	// 性能优化，关闭表单和表格渲染
	props.beforeUpdatePage();
	setValue.call(this, props, data);
	setStatus.call(this, props, UISTATE.browse);
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let cacheData = props.createMasterChildData(pagecode, formId, tableId);
	// 保存成功后处理缓存
	if (oldstatus == UISTATE.add) {
		cardCache.addCache(pk, cacheData, formId, dataSource);
	} else {
		cardCache.updateCache(pkField, pk, cacheData, formId, dataSource);
	}
	showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
	setStatus.call(this, props, UISTATE.browse);
	// 性能优化，表单和表格统一渲染
	props.updatePage(formId, tableId);
}
/**
 * 取消
 * @param {*} props 
 */
function CancelAction(props) {
	let { formId, tableId, dataSource, pkField } = props.pageConfig;
	props.form.cancel(formId);
	props.cardTable.resetTableData(tableId);
	setStatus.call(this, props, UISTATE.browse);
	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	loadDataByPk.call(this, props, pk);
}
/**
 * 提交，收回
 * @param {*} props 
 * @param {*} OperatorType 
 * @param {*} commitType 
 */
export function CommitAction(props, OperatorType, commitType, content) {
	let { formId, tableId, pagecode, reqUrl, dataSource, pkField } = props.pageConfig;
	// 保存提交校验
	if (commitType === 'saveCommit') {
		let keys = [ 'amount', 'pk_originvalue_type', 'created_num' ];
		let savevalidator = beforeSaveValidator.call(this, props, formId, tableId, keys);
		// 校验失败，则返回
		if (!savevalidator) {
			return;
		}
	}
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	// 保存提交情况，需要判断是否为指派情况,决定是否验证公式
	if (content) {
		ajax({
			url: reqUrl.commitUrl,
			data: CardData,
			success: (res) => {
				if (res.success) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					let callback = () => {
						setStatus.call(this, props, UISTATE.browse);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkField,
						dataSource,
						null,
						false,
						callback,
						pagecode
					);
				}
			}
		});
	} else {
		// 保存前执行验证公式
		props.validateToSave(CardData, () => {
			ajax({
				url: reqUrl.commitUrl,
				data: CardData,
				success: (res) => {
					if (res.success) {
						if (content) {
							this.setState({
								compositedisplay: false
							});
						}
						let callback = () => {
							setStatus.call(this, props, UISTATE.browse);
						};
						getScriptCardReturnData.call(
							this,
							res,
							props,
							formId,
							tableId,
							pkField,
							dataSource,
							null,
							false,
							callback,
							pagecode
						);
					}
				}
			});
		});
	}
}

// card body 删行
function delrowaction(props) {
	let { tableId } = props.pageConfig;
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(tableId, checkedIndex);
	setBodyBtnsEnable.call(this, props, tableId);
}
/**
 * 增行
 * @param {*} props 
 */
function addLine(props) {
	let { tableId } = props.pageConfig;
	props.cardTable.addRow(tableId, undefined, {}, false);
	addRowAfter.call(this, props);
}
/**
 * 增行后处理事件
 * @param {*} props 
 */
export function addRowAfter(props) {
	let { tableId } = props.pageConfig;
	let visiblowRows = props.cardTable.getVisibleRows(tableId);
	let index = visiblowRows.length - 1;
	props.cardTable.setValByKeyAndIndex(tableId, index, 'amount', { value: '1', display: '1' });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_originvalue_type', {
		value: this.currency.value,
		display: this.currency.display
	});
	setBodyBtnsEnable.call(this, props, tableId);
}
/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let { reqUrl } = props.pageConfig;
	let printData = getPrintParam.call(this, props);
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}

	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printUrl, // 后台打印服务url
		printData
	);
}
/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let { reqUrl } = props.pageConfig;
	let printData = getPrintParam.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: reqUrl.printUrl,
		data: printData
	});
}
/**
 * 打印输出参数
 * @param {*} props 
 * @param {*} output 
 */
function getPrintParam(props, outputType = 'print') {
	let { formId, pagecode, pkField } = props.pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let param = {
		filename: pagecode, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType
	};

	return param;
}
/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	let { formId, tableId, editBtns, pkField } = props.pageConfig;
	// 更新参数
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	props.setUrlParam({ status, id: pkVal ? pkVal.value : '' });
	switch (status) {
		case UISTATE.add:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			setBrowseBtnsVisible.call(this, props);
			props.button.setButtonVisible(editBtns, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.edit:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			setBrowseBtnsVisible.call(this, props);
			props.button.setButtonVisible(editBtns, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			setBrowseBtnsVisible.call(this, props);
			break;
		default:
			break;
	}
	setHeadAreaData.call(this, props, { status });
}
/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let { formId } = props.pageConfig;
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
		return;
	}
	props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
	let checkedRows = [];
	let num = props.cardTable.getNumberOfRows(moduleId, false);
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}
/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	let { tableId, formId } = props.pageConfig;
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	setCardValue.call(this, props, data);
	setBrowseBtnsVisible.call(this, props);
	if (status != UISTATE.browse) {
		setBodyBtnsEnable.call(this, props, tableId);
	}
}
/**
 * 浏览态设置按钮显示隐藏
 * @param {*} props 
 */
function setBrowseBtnsVisible(props) {
	let { formId, pkField, browseBtns } = props.pageConfig;
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	if (status != UISTATE.browse) {
		props.button.setButtonVisible(browseBtns, false);
	} else {
		let pkVal = props.form.getFormItemsValue(formId, pkField);
		if (!pkVal || !pkVal.value) {
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible('Add', true);
			return;
		}
		props.button.setButtonVisible(browseBtns, true);
		setBillFlowBtnsVisible.call(this, props, formId, pkField);
	}
}
/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk, callback) {
	let { dataSource } = props.pageConfig;
	if (!pk || pk == 'null') {
		setValue.call(this, props, undefined);
		typeof callback == 'function' && callback();
	} else {
		let cachData = cardCache.getCacheById(pk, dataSource);
		if (cachData) {
			setValue.call(this, props, cachData);
			typeof callback == 'function' && callback(cachData);
		} else {
			getDataByPk.call(this, props, pk, callback);
		}
	}
}
/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk, callback) {
	let { reqUrl, pagecode, formId, dataSource, pkField } = props.pageConfig;
	ajax({
		url: reqUrl.cardQueryUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					cardCache.updateCache(pkField, pk, data, formId, dataSource);
				} else {
					setValue.call(this, props, undefined);
					cardCache.deleteCacheById(pkField, pk, dataSource);
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				}
				typeof callback == 'function' && callback(data);
			}
		}
	});
}
/**
 * 列表态复制数据
 * @param {*} props 
 * @param {*} pk 
 */
export function copyDataByPk(props, pk) {
	let { reqUrl, pagecode } = props.pageConfig;
	ajax({
		url: reqUrl.cardQueryUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				setValue.call(this, props, data);
				let copyflag = this.props.getUrlParam('copyflag');
				if (copyflag && copyflag == true) {
					CopyAction.call(this, props);
				}
			}
		}
	});
}
/**
 * 批改
 * @param {*} props 
 */
function BatchAltert(props) {
	let { reqUrl, formId, tableId, pagecode } = props.pageConfig;
	let num = props.cardTable.getNumberOfRows(tableId);
	if (num <= 1) {
		return;
	}
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);
	let changeKey = changeData.batchChangeKey;
	let value = changeData.batchChangeValue;
	let index = changeData.batchChangeIndex;
	let display = changeData.batchChangeDisplay;
	//0、判断是否允许批改
	if (changeKey === 'pk_usedept_v') {
		//使用部门
		//如果【使用权】这一列的值不相同，不允许批改
		let pk_equiporgs = props.cardTable.getColValue(tableId, 'pk_equiporg');
		pk_equiporgs = getArrayAttribute(pk_equiporgs, 'value');
		if (pk_equiporgs.length > 1) {
			// [使用权] 列中的值不一致，不允许批改！
			showMessage(props, { content: getMultiLangByID('201201512A-000011'), color: 'warning' });
			return;
		}
	} else if (changeKey === 'pk_mandept' || changeKey === 'pk_mandept_v') {
		//管理部门
		//如果【货主管理组织】这一列的值不相同，不允许批改
		let pk_ownerorgs = props.cardTable.getColValue(tableId, 'pk_ownerorg');
		pk_ownerorgs = getArrayAttribute(pk_ownerorgs, 'value');
		if (pk_ownerorgs.length > 1) {
			//[货主管理组织] 列中的值不一致，不允许批改！
			showMessage(props, { content: getMultiLangByID('201201512A-000012'), color: 'warning' });
			return;
		}
	} else if (changeKey === 'pk_asset_user') {
		//使用人
		//如果【使用权】这一列的值不相同，不允许批改
		let pk_equiporgs = props.cardTable.getColValue(tableId, 'pk_equiporg');
		pk_equiporgs = getArrayAttribute(pk_equiporgs, 'value');
		if (pk_equiporgs.length > 1) {
			showMessage(props, { content: getMultiLangByID('201201512A-000011'), color: 'warning' });
			return;
		}
	}
	if (changeKey === 'pk_ownerorg_v') {
		// 货主管理组织
		referAlterClear(props, formId, tableId, 'pk_ownerorg_v', [ 'pk_mandept_v' ], index, value, display);
	}
	if (changeKey === 'pk_equiporg_v') {
		// 使用权
		referAlterClear(
			props,
			formId,
			tableId,
			'pk_equiporg_v',
			[ 'pk_usedept_v', 'pk_asset_user' ],
			index,
			value,
			display
		);
	}
	// 数量为数值类型，若批改为空时，平台传过来是字符串，需手动处理为Null，否则会报错
	if (changeKey === 'amount' && value === '') {
		changeData.batchChangeValue = null;
	}
	props.cardTable.batchChangeTableData(tableId);
	// 如果需要批改的字段只是简单的赋值操作，则走前台的批改操作
	if (changeKey === 'asset_name') {
		return;
	} else {
		let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
		let data = {
			card: CardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			tableCode: ''
		};
		ajax({
			url: reqUrl.batchAlter,
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//把数据设置到界面上
					setValue.call(this, props, data);
				}
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
				props.cardTable.setColValue(tableId, changeData.batchChangeKey, { display: null, value: null });
			}
		});
	}
}
//得到不重复的 数组中元素的的某个属性值
function getArrayAttribute(ary, attr) {
	let set = new Set();
	ary.map((item) => {
		if (item[attr]) {
			set.add(item[attr]);
		} else {
			set.add('');
		}
	});
	return [ ...set ];
}
/**
 * 影像扫描
 * @param {*} props 
 */
function FAReceiptScan(props) {
	let { pagecode, formId, tableId, pkField } = props.pageConfig;
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageScan(props, imageData);
}
/**
 * 影像查看
 * @param {*} props 
 */
function FAReceiptShow(props) {
	let { pagecode, formId, tableId, pkField } = props.pageConfig;
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageView(props, imageData);
}
/**
 * refresh
 * @param {*} props 
 */
function refresh(props) {
	let { formId, pkField } = props.pageConfig;
	let id = props.form.getFormItemsValue(formId, pkField).value;
	if (!id) {
		return;
	}
	let callback = (data) => {
		if (data) {
			showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		}
	};
	// 查询到最新单据数据
	getDataByPk.call(this, props, id, callback);
}
/**
 * 单据追溯
 * @param {*} props 
 */
function queryAboutBusiness(props) {
	let { formId, pkField } = props.pageConfig;
	let pk_bill = props.form.getFormItemsValue(formId, pkField).value;
	this.setState({
		trackshow: true,
		pk_bill
	});
}
/**
 * 审批详情
 * @param {*} props 
 */
function queryAboutBillFlow(props) {
	let { formId, pkField } = props.pageConfig;
	let pk_bill = props.form.getFormItemsValue(formId, pkField).value;
	let transi_type = props.form.getFormItemsValue(formId, 'transi_type').value;
	this.setState({
		showApprove: true,
		pk_bill,
		transi_type
	});
}
/**
 * 卡片返回列表
 * @param {*} props 
 */
export function backToList(props) {
	let { listRouter, pagecode } = props.pageConfig;
	props.pushTo(listRouter, {
		pagecode: pagecode.replace('card', 'list')
	});
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	let { formId, pkField } = props.pageConfig;
	// 主键
	let billId = props.form.getFormItemsValue(formId, pkField).value;
	props.ncUploader.show('uploader', {
		billId: 'fa/transasset/' + billId
	});
}
/**
 * 联查卡片
 * @param {*} props 
 */
function queryAboutAsset(props) {
	let { tableId } = props.pageConfig;
	openAssetCardByChooseRow.call(this, props, tableId);
}
/**
 * 通过消息中心打开
 * @param {*} props 
 * @param {*} pkMsg 
 */
export function getTransassetMsg(props, pkMsg) {
	let { reqUrl, pagecode } = props.pageConfig;
	ajax({
		url: reqUrl.openTransassetMsgURL,
		data: {
			pkMsg: pkMsg,
			pagecode: pagecode
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					props.resMetaAfterPkorgEdit();
				}
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
				cancelAction.call(this, props); //减少单消息打开报错提示后跳转到浏览态
			}
		}
	});
}
