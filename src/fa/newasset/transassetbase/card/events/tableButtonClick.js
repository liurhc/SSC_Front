/**
 * 卡片操作列按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function tableButtonClick(props, key, text, record, index) {
	let { tableId } = props.pageConfig;
	switch (key) {
		// 表格操作按钮
		case 'OpenCard':
			let status = props.cardTable.getStatus(tableId);
			if (status === 'browse') {
				props.cardTable.toggleRowView(tableId, record);
			} else {
				props.cardTable.openModel(tableId, 'edit', record, index);
			}
			break;
		case 'DelLine':
			props.cardTable.delRowsByIndex(tableId, index);
			break;
		case 'CopyLine':
			props.cardTable.insertRowsAfterIndex(tableId, record, index);
			break;
		default:
			break;
	}
}
