import { commonConst as deploycommonConst } from '../const';
import ampub from 'ampub';
const { commonConst } = ampub;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;

// 节点固定变量数据值
export const staticVariable = {
	...deploycommonConst,
	pagecode: '201203504A_card', //单据模板pageID
	searchId: '2012052020', //查询区域编码
	tableId: 'bodyvos', //列表表体编码
	formId: 'card_head', //列表表头编码
	// 打印模板所在NC节点及标识
	printFuncode: '2012052020',
	printNodekey: null,
	listRouter: '/list',
	toDaployInData: {
		url: '/fa/deploy/deployin/main/index.html#/card',
		appcode: '201203508A',
		pagecode: '201203508A_card'
	}
};
// 过滤参数
export const defaultConfig = {
	searchId: 'searchArea',
	formId: 'card_head',
	bodyIds: [ 'bodyvos', 'bodyvos_edit' ],
	specialFields: {
		deployer: {
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		pk_org_in_v: {
			RefActionExtType: 'TreeRefActionExt', //树形
			class: 'nccloud.web.fa.deploy.deployout.refcondition.PK_ORG_IN_VSqlBuilder', //java权限过滤类
			data: [
				{
					fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
					returnName: 'pk_org' //被过滤字段名后台需要参数
				}
			]
		}
	}
};
// ajax请求链接
export const reqUrl = {
	insertUrl: '/nccloud/fa/deployout/insert.do',
	updateUrl: '/nccloud/fa/deployout/update.do',
	listQueryUrl: '/nccloud/fa/deployout/listquery.do',
	cardQueryUrl: '/nccloud/fa/deployout/cardquery.do',
	deleteUrl: '/nccloud/fa/deployout/delete.do',
	commitUrl: '/nccloud/fa/deployout/commit.do',
	unCommitUrl: '/nccloud/fa/deployout/uncommit.do',
	unApproveUrl: '/nccloud/fa/deployout/unapprove.do',
	pageDataByPksUrl: '/nccloud/fa/deployout/querypagegridbypks.do', // 分页查询
	billCodeGenUrl: '/nccloud/fa/deployout/billcodegenbackevent.do',
	headAfterEdit: '/nccloud/fa/deployout/headafteredit.do',
	bodyAfterEdit: '/nccloud/fa/deployout/bodyafteredit.do',
	printUrl: '/nccloud/fa/deployout/printCard.do',
	batchAlter: '/nccloud/fa/deployout/batchAltert.do',
	lockcard: '/nccloud/fa/facard/lockcard.do',
	edit: '/nccloud/fa/deployout/edit.do',
	editControl: '/nccloud/fa/deployout/editControl.do',
	linkQuerydepoyin: 'fa/deployout/linkQuerydepoyin.do'
};
export const buttonsStaus = {
	// 编辑态按钮
	editBtns: [ 'SaveGroup', 'Cancel', 'GetFinanceData', 'AddLine', 'DelLine', 'BatchAlter' ],
	// 浏览态按钮
	browseBtns: [
		'AddGroup',
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'FAReceipt',
		'Attachment',
		'Print',
		'Refresh',
		'More',
		'QueryAboutBillFlow'
	]
};

export const orgChangeClearFields = [ 'pk_org_in_v', 'deployer', 'confirm_flag', 'reason' ];
