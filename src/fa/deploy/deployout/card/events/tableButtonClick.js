import { staticVariable } from '../const';
import { doDelLine } from './';

const { tableId } = staticVariable;
/**
 * 操作列按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'OpenCard':
			openCard.call(this, props, record, index);
			break;
		case 'DelLine':
			tableDelLine.call(this, props, record, index);
			break;
		default:
			break;
	}
}

function openCard(props, record, index) {
	let status = props.cardTable.getStatus(tableId);
	if (status === 'browse') {
		props.cardTable.toggleRowView(tableId, record);
	} else if (status == 'edit') {
		props.cardTable.setClickRowIndex(tableId, { record, index });
		props.cardTable.openModel(tableId, 'edit', record, index);
	}
}

function tableDelLine(props, record, index) {
	let unlockPks = [];
	let checkedIndex = [ index ];
	// 若行数据为新增的，则需解锁卡片
	//0-不变 1-修改 2-新增 3-删除
	if (record.status == 2 || record.status == 1) {
		if (record.values.pk_card && record.values.pk_card.value) {
			unlockPks.push(record.values.pk_card.value);
		}
	}
	doDelLine.call(this, props, unlockPks, checkedIndex);
}
