import { ajax } from 'nc-lightapp-front';
import { staticVariable, reqUrl } from '../const';
const { tableId } = staticVariable;

export default function beforeEvent(props, moduleId, key, value, index, record) {
	// 调拨价格,编辑前根据集团及参数BD803控制调拨价格是否可编辑
	if (key === 'deploy_price') {
		ajax({
			url: reqUrl.editControl,
			success: (res) => {
				let { success, data } = res;
				// 如果BD803为手工输入，则可编辑，否则不可编辑
				if (success && data) {
					props.cardTable.setEditableByIndex(tableId, index, [ 'deploy_price' ], true);
				} else {
					props.cardTable.setEditableByIndex(tableId, index, [ 'deploy_price' ], false);
				}
			}
		});
	}

	return true;
}
