import { ajax, toast, print, output, cardCache } from 'nc-lightapp-front';
import { staticVariable, reqUrl, buttonsStaus } from '../const';
import { handlePkOrgAfterEditEvent } from './afterEvent';
const { pagecode, formId, tableId, dataSource, field, listRouter, billtype } = staticVariable;
const { editBtns, browseBtns } = buttonsStaus;
const { insertUrl, updateUrl, commitUrl, deleteUrl } = reqUrl;
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { CommonKeys, StatusUtils } = commonConst;
const { multiLangUtils, msgUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { linkQueryConst } = CommonKeys;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { LoginContext, ScriptReturnUtils, saveValidatorsUtil } = components;
const { UISTATE } = StatusUtils;
const { beforeSaveValidator } = saveValidatorsUtil;
const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = cardUtils;
const { getContext, loginContextKeys } = LoginContext;
const { getScriptCardReturnData } = ScriptReturnUtils;
import fa from 'fa';
const { fa_components } = fa;
const { ImageMng } = fa_components;
const { faImageScan, faImageView } = ImageMng;

/**
 * 按钮动作
 * @param {*} props  
 * @param {*} id 
 */
function buttonClick(props, id) {
	switch (id) {
		case 'Add':
			addAction.call(this, props);
			break;
		case 'Save':
			saveAction.call(this, props);
			break;
		case 'Edit':
			editAction.call(this, props);
			break;
		case 'Delete':
			showConfirm.call(this, props, {
				type: MsgConst.Type.Delete,
				beSureBtnClick: deleteAction
			});
			break;
		case 'Cancel':
			showConfirm.call(this, props, {
				type: MsgConst.Type.Cancel,
				beSureBtnClick: cancelAction
			});
			break;
		case 'Commit':
			commitAction.call(this, props, 'SAVE', 'commit');
			break;
		case 'SaveCommit':
			commitAction.call(this, props, 'SAVE', 'saveCommit');
			break;
		case 'UnCommit':
			commitAction.call(this, props, 'UNSAVE', '');
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'AddLine':
			props.cardTable.addRow(tableId, undefined, {}, false);
			break;
		case 'DelLine':
			delrowaction.call(this, props);
			break;
		case 'BatchAlter':
			batchAlter.call(this, props);
			break;
		case 'FAReceiptScan':
			FAReceiptScan.call(this, props);
			break;
		case 'FAReceiptShow':
			FAReceiptShow.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props);
			break;
		default:
			break;
	}
}
// add
function addAction(props) {
	props.form.EmptyAllFormValue(formId);
	props.cardTable.setTableData(tableId, { rows: [] });
	props.initMetaByPkorg('pk_org_v');
	delAllLine.call(this, props, tableId);
	// 设置按钮可见性，界面编辑性
	setStatus.call(this, props, 'add');
	setBatchBtnsEnable.call(this, props, tableId);
	// 设置默认值
	setDefaultValue.call(this, props);
}
/**
 * 设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	let defaultValue = {
		bill_type: { display: '', value: billtype },
		transi_type: { display: '', value: this.state.transi_type },
		bill_status: { display: getMultiLangByID('statusUtils-000000'), value: '0' }, // 自由态
		business_date: { display: '', value: getContext(loginContextKeys.businessDate, dataSource) }
	};
	let defaultOrgName_v = getContext(loginContextKeys.org_v_Name, dataSource);
	let defaultOrgPk_v = getContext(loginContextKeys.pk_org_v, dataSource);
	if (defaultOrgPk_v && defaultOrgName_v) {
		defaultValue['pk_org_v'] = {
			value: defaultOrgPk_v,
			display: defaultOrgName_v
		};
		props.form.setFormItemsValue(formId, defaultValue);
		handlePkOrgAfterEditEvent.call(this, props, formId, 'pk_org_v', defaultValue.pk_org_v, {
			value: defaultOrgPk_v,
			display: defaultOrgName_v
		});
	} else {
		props.form.setFormItemsValue(formId, defaultValue);
	}
}
// edit
function editAction(props) {
	let pk = props.form.getFormItemsValue(formId, field).value || props.getUrlParam('id');
	ajax({
		url: reqUrl.edit,
		data: {
			pk,
			resourceCode: '2012052020'
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'danger', content: res.data });
			} else {
				props.cardTable.closeExpandedRow(tableId);
				setStatus.call(this, props, 'edit');
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}

function lockBodyCard(props) {
	let rowsData = props.cardTable.getAllRows(tableId);
	if (!rowsData) {
		return;
	}
	let lockPks = [];
	rowsData.map((item) => {
		lockPks.push(item.values.pk_card.value);
	});
	if (lockPks.length < 1) {
		return;
	}
	lockrequest.call(this, 'lock', lockPks);
}

// delete
function deleteAction(props) {
	let paramInfoMap = {};
	let pk = props.form.getFormItemsValue(formId, field).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	paramInfoMap[pk] = ts;
	ajax({
		url: deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			let { success } = res;
			if (success) {
				let callback = (newpk) => {
					// 更新参数
					props.setUrlParam({ id: newpk });
					// 加载下一条数据
					loadDataByPk.call(this, props, newpk);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					field,
					dataSource,
					undefined,
					true,
					callback
				);
			}
		}
	});
}
/**
 * 保存
 * @param {*} props 
 */
function saveAction(props) {
	// 保存前校验
	if (!beforeSaveValidator.call(this, props, formId, tableId)) {
		return;
	}

	// 取得要保存的数据，这里加了处理，过滤掉因多选造成的多了一条错误垃圾数据，平台暂时没解决，自己处理了
	// let data = getVOData.call(this, props);
	let data = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let pk = props.form.getFormItemsValue(formId, field).value;
	let url = insertUrl;
	let oldstatus = UISTATE.add;
	if (pk && pk != null) {
		url = updateUrl;
		oldstatus = UISTATE.edit;
		data.body[tableId].rows.map((currow) => {
			currow.values.pk_deployout = { value: props.form.getFormItemsValue(formId, field).value };
		});
	} else {
		data.head[formId].rows[0].values.bill_status = { value: '0' };
		data.head[formId].rows[0].values.transi_type = { value: this.state.transi_type }; // 资产调出交易类型
		data.head[formId].rows[0].values.bill_type = { value: billtype };
	}
	props.validateToSave(data, () => {
		ajax({
			url: url,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//关闭侧拉
					props.cardTable.closeModel(tableId);
					afterSave.call(this, props, data, oldstatus);
				}
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
			}
		});
	});
}
/**
 * 取得要保存的数据，同时过滤表体卡片多选产生的脏数据
 * @param {*} props 
 */
function getVOData(props) {
	let data = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let body = props.cardTable.getAllData(tableId);
	let newbody = {};
	newbody.areacode = tableId;
	newbody.areaType = body.areaType;
	let rows = [];
	body.rows.map((ele) => {
		let pk_card = ele.values.pk_card.value;
		let pk_cards = pk_card.split(',');
		let length = pk_cards.length;
		if (length == 1) {
			rows.push(ele);
		}
	});
	newbody.rows = rows;
	data.body[tableId] = newbody;

	return data;
}
/**
* 保存后
* @param {*} props 
*/
function afterSave(props, data, oldstatus) {
	// 性能优化，关闭表单和表格渲染
	props.beforeUpdatePage();
	setValue.call(this, props, data);
	setStatus.call(this, props, UISTATE.browse);
	let pk = props.form.getFormItemsValue(formId, field).value;
	let cacheData = props.createMasterChildData(pagecode, formId, tableId);
	// 保存成功后处理缓存
	if (oldstatus == UISTATE.add) {
		cardCache.addCache(pk, cacheData, formId, dataSource);
	} else {
		cardCache.updateCache(field, pk, cacheData, formId, dataSource);
	}
	setStatus.call(this, props, UISTATE.browse);
	showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess }); // '保存成功
	// 性能优化，表单和表格统一渲染
	props.updatePage(formId, tableId);
}
/**
 * 取消
 * @param {*} props 
 */
function cancelAction(props) {
	// 解锁卡片
	lockcards.call(this, props, 'unlock');

	props.form.cancel(formId);
	delAllLine.call(this, props, tableId);
	props.cardTable.resetTableData(tableId);
	setStatus.call(this, props, 'browse');
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	let pk = props.form.getFormItemsValue(formId, field).value;
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	loadDataByPk.call(this, props, pk);
}

/**删出表体所有行 */
function delAllLine(props, moduleId) {
	let allrow = props.cardTable.getNumberOfRows(moduleId);
	let indexs = [];
	for (let i = 0; i < allrow; i++) {
		indexs.push(i);
	}
	props.cardTable.delRowsByIndex(moduleId, indexs);
}

/**
 * 提交
 * @param {*} props 
 * @param {*} OperatorType 
 * @param {*} commitType 
 */
function commitAction(props, OperatorType, commitType, content) {
	//提交校验
	if (commitType === 'saveCommit') {
		if (!beforeSaveValidator.call(this, props, formId, tableId)) {
			return;
		}
	}

	let pk = props.form.getFormItemsValue(formId, field).value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	//如果不是保存提交，需要过滤掉删行状态
	if (commitType === 'commit') {
		let filterRows = CardData.body[tableId].rows.filter((row) => {
			return row.status != 3;
		});
		CardData.body[tableId].rows = filterRows;
	}
	CardData.body[tableId].rows.map((currow) => {
		// 修改新增表体时，将表头主键赋值到表体中
		currow.values.pk_deployout = { value: props.form.getFormItemsValue(formId, field).value };
	});
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		billtype: billtype,
		transtype: this.state.transi_type,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	if (content) {
		ajax({
			url: commitUrl,
			data: CardData,
			success: (res) => {
				if (res.success) {
					this.setState({
						compositedisplay: false
					});
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					let callback = () => {
						setStatus.call(this, props, UISTATE.browse);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						field,
						dataSource,
						null,
						false,
						callback,
						pagecode
					);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	} else {
		props.validateToSave(CardData, () => {
			ajax({
				url: reqUrl.commitUrl,
				data: CardData,
				success: (res) => {
					if (res.success) {
						// 性能优化，关闭表单和表格渲染
						props.beforeUpdatePage();
						let callback = () => {
							setStatus.call(this, props, UISTATE.browse);
						};
						getScriptCardReturnData.call(
							this,
							res,
							props,
							formId,
							tableId,
							field,
							dataSource,
							null,
							false,
							callback,
							pagecode
						);
						// 性能优化，表单和表格统一渲染
						props.updatePage(formId, tableId);
					}
				}
			});
		});
	}
}

/**
 * 表间删行
 * @param {*} props 
 */
function delrowaction(props) {
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	let unlockPks = [];
	//选出要解锁的卡片主键
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
		// 若删除行为新增数据，则记录为需解锁卡片
		if (item.data.status == 2 || item.data.status == 1) {
			//0-不变 1-修改 2-新增 3-删除
			unlockPks.push(item.data.values.pk_card.value);
		}
	});
	doDelLine.call(this, props, unlockPks, checkedIndex);
	checkedRows = props.cardTable.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length < 1) {
		props.button.setDisabled({ DelLine: true });
	}
}

/**
 * 
 * @param {*} props 
 * @param {解锁pk数组} unlockPks 
 * @param {选中行index} checkedIndex 
 */
function doDelLine(props, unlockPks, checkedIndex) {
	//解锁
	if (unlockPks.length > 0) {
		lockrequest.call(this, 'unlock', unlockPks);
	}
	//删除行
	props.cardTable.delRowsByIndex(tableId, checkedIndex);
}

/**
 * 打印
 * @param {*} props 
 */
function printTemp(props) {
	let printData = getPrintParam.call(this, props);
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}

	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printUrl, // 后台打印服务url
		printData
	);
}
/**
 * 输出
 * @param {*} props 
 */
function outputTemp(props) {
	let printData = getPrintParam.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: reqUrl.printUrl,
		data: printData
	});
}
/**
 * 打印输出参数
 * @param {*} props 
 * @param {*} output 
 */
function getPrintParam(props, outputType) {
	let pk = props.form.getFormItemsValue(formId, field);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let param = {
		filename: pagecode, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks // 需要打印数据主键
	};
	if (outputType) {
		param.outputType = outputType;
	}
	return param;
}
/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk) {
	if (!pk || pk == 'null') {
		setValue.call(this, props, undefined);
		this.setState({ step: -1 });
		return;
	}
	// let cachData = cardCache.getCacheById(pk, dataSource);
	// if (cachData) {
	// 	this.setState({ pk_bill: pk });
	// 	setValue.call(this, props, cachData);
	// } else {
	// 由于平台采取局部刷新，而调出单有进度条处理，通过缓存刷新数据不会刷新进度条，所以这里改为直接去查一下数据
	getDataByPk.call(this, props, pk);
	// }
}
/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
function getDataByPk(props, pk, isRefresh) {
	ajax({
		url: reqUrl.cardQueryUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data.data);
					cardCache.updateCache(field, pk, data.data, formId, dataSource);
					this.setState({ step: data.step });
					if (isRefresh) {
						showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
					}
				} else {
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				}
			}
		}
	});
}
/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */

function setValue(props, data) {
	setCardValue.call(this, props, data);
	let status = props.form.getFormStatus(formId);
	setBrowseBtnsVisible.call(this, props);
	if (status != UISTATE.browse) {
		setBatchBtnsEnable.call(this, props, tableId);
	}
}
/**
 * 浏览态设置按钮显示隐藏
 * @param {*} props 
 */
function setBrowseBtnsVisible(props) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	if (status != UISTATE.browse) {
		props.button.setButtonVisible(browseBtns, false);
	} else {
		let pkVal = props.form.getFormItemsValue(formId, field);
		if (!pkVal || !pkVal.value) {
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible('Add', true);
			return;
		}
		props.button.setButtonVisible(browseBtns, true);
		setBillFlowBtnsVisible.call(this, props, formId, field);
	}
}
/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
function setStatus(props, status = 'browse') {
	switch (status) {
		case 'add':
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, 'edit');
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			setBrowseBtnsVisible.call(this, props);
			props.button.setButtonVisible(editBtns, true);
			setBatchBtnsEnable.call(this, props, tableId);
			break;
		case 'edit':
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			setBrowseBtnsVisible.call(this, props);
			props.button.setButtonVisible(editBtns, true);
			setBatchBtnsEnable.call(this, props, tableId);
			break;
		case 'browse':
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			setBrowseBtnsVisible.call(this, props);
			break;
		default:
			break;
	}
	setHeadAreaData.call(this, props, { status });
}
/**
 * 根据单据状态设置部分按钮可见性
 * @param {*} props 
 * @param {*} bill_status 
 */
function setBtnStatusByBillStatus(props) {
	// 根据单据状态隐藏按钮
	setBillFlowBtnsVisible(props, formId, field, 'bill_status');
}
/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
function setBatchBtnsEnable(props, moduleId) {
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.button.setButtonDisabled([ 'AddLine', 'OpenCard', 'DelLine', 'BatchAlter' ], true);
		return;
	}
	let num = props.cardTable.getNumberOfRows(moduleId);
	let checkedRows = [];
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	props.button.setButtonDisabled([ 'BatchAlter', 'AddLine' ], num <= 0);
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}

/**
 * 解锁、加锁
 * @param {*} props 
 *  @param {*} lockflag  lock-锁 unlock-解锁
 */
function lockcards(props, lockflag, async = true) {
	let data = props.cardTable.getChangedRows(tableId);
	// status : 0不变，1修改，2新增，3删除
	let allpks = [];
	for (let val of data) {
		// 若当前编辑态，且存在新增卡片则收集解锁
		if (val.status == 2 || val.status == 1) {
			allpks.push(val.values.pk_card.value);
		}
	}
	lockrequest.call(this, lockflag, allpks, async);
}

/**
 * 解锁、加锁请求
 * @param {*} lockflag lock/unlock
 * @param {*} pks []数组pks
 */
function lockrequest(lockflag, pks, async = true) {
	//检查pk为空
	if (!pks || pks.length < 1) {
		return;
	}
	//去空
	let allpks = [];
	pks.map((item) => {
		if (item) {
			//处理多选
			let itemArray = item.split(',');
			itemArray.map((pk) => {
				allpks.push(pk);
			});
		}
	});
	let userid = getContext.call(this, loginContextKeys.userId, dataSource);
	let dSource = getContext.call(this, loginContextKeys.dataSourceCode, dataSource);
	if (allpks && allpks.length > 0 && lockflag && userid && dSource) {
		ajax({
			url: reqUrl.lockcard,
			async,
			data: {
				allpks: allpks,
				msgMap: {
					lockflag: lockflag,
					usrid: userid,
					dSource: dSource
				}
			},
			success: (res) => {
				//console.log(lockflag + 'cards successfully');
			},
			error: (res) => {}
		});
	}
}
/**
 * 影像扫描
 * @param {*} props 
 */
function FAReceiptScan(props) {
	let imageData = {
		pagecode: pagecode,
		formId: formId,
		tableId: tableId,
		pkField: field
	};
	faImageScan.call(this, props, imageData);
}
/**
 * 影像查看
 * @param {*} props 
 */
function FAReceiptShow(props) {
	let imageData = {
		pagecode: pagecode,
		formId: formId,
		tableId: tableId,
		pkField: field
	};
	faImageView.call(this, props, imageData);
}
/**
 * refresh
 * @param {*} props 
 */
function refresh(props) {
	let id = props.form.getFormItemsValue(formId, field);
	if (!id || !id.value) {
		return;
	}
	if (!id.value) {
		toast({ content: getMultiLangByID('2012003504A-000007'), color: 'danger' }); // '刷新失败'
		return;
	}
	getDataByPk.call(this, props, id.value, true);
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	// 单据主键
	let billId = props.form.getFormItemsValue(formId, field).value;
	props.ncUploader.show('uploader', {
		billId: 'fa/deployout/' + billId
	});
}
/**
 * 审批详情
 * @param {*} props 
 */
function queryAboutBillFlow(props) {
	let id = props.form.getFormItemsValue(formId, field).value;
	let transi_type = props.form.getFormItemsValue(formId, 'transi_type').value;
	this.setState({
		showApprove: true,
		pk_bill: id,
		transi_type
	});
}
/**
 * 卡片返回列表
 * @param {*} props 
 */
function backToList(props) {
	props.pushTo(listRouter, {
		pagecode: pagecode.replace('card', 'list')
	});
}

/**
 * 批改按钮
 * @param {*} props 
 */
function batchAlter(props) {
	//卡片列表行数
	let num = props.cardTable.getNumberOfRows(tableId, false);
	//小于两行无需批改
	if (num < 2) {
		return;
	}

	//批改数据
	let changeData = props.cardTable.getTableItemData(tableId);
	// 不支持批改字段
	let unBatchCol = [ 'pk_card' ];
	if (unBatchCol.indexOf(changeData.batchChangeKey) === -1) {
		let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
		let data = {
			card: CardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			tableCode: ''
		};
		ajax({
			url: reqUrl.batchAlter,
			data,
			success: (res) => {
				if (res.data) {
					if (res.data.body && res.data.body[tableId]) {
						setCardValue.call(this, props, res.data);
					}
				}
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
				props.cardTable.setColValue(tableId, changeData.batchChangeKey, { display: null, value: null });
			}
		});
	}
}
/**
 * 侧拉删行解锁卡片
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} moduleIndex 
 * @param {*} row 
 */
function modelDelRow(props, moduleId, moduleIndex, row) {
	if (row.values.pk_card.value == null || row.values.pk_card.value == '') {
		return true;
	}
	let pks = [];
	pks.push(row.values.pk_card.value);
	lockrequest.call(this, 'unlock', pks);
	return true;
}

function toDeployIn(props) {
	let pk_deployin = props.form.getFormItemsValue(formId, 'pk_deployin');
	if (pk_deployin.value) {
		ajax({
			url: '/nccloud/fa/deployout/linkQuerydepoyin.do',
			data: { [linkQueryConst.ID]: pk_deployin.value },
			success: (res) => {
				if (res.data) {
					let linkData = res.data;
					linkData['status'] = 'browse';
					linkData[linkQueryConst.SCENE] = linkQueryConst.SCENETYPE.linksce; // 设置联查场景
					props.openTo(linkData[linkQueryConst.URL], linkData);
				} else {
					toast({ content: getMultiLangByID('QueryAbout-000002'), color: 'warning' }); /*国际化处理：'无权限'*/
				}
			},
			error: (err) => {
				toast({ content: err.message, color: 'danger' });
			}
		});
	}
}

export {
	buttonClick,
	saveAction,
	afterSave,
	printTemp,
	outputTemp,
	getDataByPk,
	setValue,
	setStatus,
	setBatchBtnsEnable,
	lockcards,
	lockrequest,
	backToList,
	doDelLine,
	commitAction,
	modelDelRow,
	setDefaultValue,
	toDeployIn
};
