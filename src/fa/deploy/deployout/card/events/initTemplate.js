import tableButtonClick from './tableButtonClick';
import { buttonClick, setStatus, toDeployIn } from './buttonClick';
import { staticVariable, defaultConfig } from '../const';
import ampub from 'ampub';
const { components, utils } = ampub;
const { cardUtils } = utils;
const { LoginContext, faQueryAboutUtils } = components;
const { openAssetCardByPk } = faQueryAboutUtils;
const { afterModifyCardMeta, createOprationColumn } = cardUtils;
const { getContext, loginContextKeys, loginContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addBodyReferFilter, addHeadAreaReferFilter } = ReferFilter;
const { pagecode, formId, tableId, dataSource } = staticVariable;

export default function(props) {
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					//获取登录上下文信息
					loginContext.call(this, data.context, dataSource);
					getContext.call(this, loginContextKeys.dataSourceCode, dataSource);
					//设置交易类型
					let transi_type = getContext(loginContextKeys.transtype, dataSource);
					this.setState({ transi_type: transi_type });
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta);
				}
				afterInit.call(this, props);
			}
		}
	);
}
/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status');
	if (status == 'add') {
		buttonClick.call(this, props, 'Add');
	} else if (status == 'edit') {
		buttonClick.call(this, props, 'Edit');
	} else {
		setStatus.call(this, props, status);
	}
}

function modifierMeta(props, meta) {
	// 参照过滤
	refFilter.call(this, props, meta);
	addLink.call(this, props, meta);
	// 添加表格操作列
	let oprCol = createOprationColumn.call(this, props, {
		formId: formId,
		tableId: tableId,
		tableButtonClick: tableButtonClick.bind(this)
	});
	meta[tableId].items.push(oprCol);
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);
	return meta;
}
/**
 * 过滤参照
 * @param {*} props 
 * @param {*} meta 
 */
function refFilter(props, meta) {
	// 表头参照过滤
	addHeadAreaReferFilter(props, meta, defaultConfig);
	// 表体参照过滤
	addBodyReferFilter(props, meta, defaultConfig);
}

function addLink(props, meta) {
	//单据号超链接
	meta[formId].items.map((item) => {
		if (item.attrcode === 'bill_code_in') {
			item.renderStatus = 'browse';
			item.render = (formItem) => {
				return (
					<div style={{ color: '#007ace', cursor: 'pointer' }}>
						<span
							onClick={() => {
								toDeployIn.call(this, props);
							}}
						>
							{formItem.value}
						</span>
					</div>
				);
			};
		}
	});
	// 卡片超链
	meta[tableId].items.map((item) => {
		if (item.attrcode === 'pk_card') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openAssetCardByPk.call(this, props, record.values.pk_card.value);
							}}
						>
							{record && record.values.pk_card && record.values.pk_card.display}
						</span>
					</div>
				);
			};
			item.isMultiSelectedEnabled = true;
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
				pk_org = pk_org && pk_org.value;
				return {
					pk_org,
					GridRefActionExt: 'nccloud.web.fa.common.refCondition.PK_CARDSqlBuilder'
				};
			};
		} else if (item.attrcode === 'pk_addreducestyle') {
			// 减少方式只能选择末级节点
			item.onlyLeafCanSelect = true;
		}
	});
}
