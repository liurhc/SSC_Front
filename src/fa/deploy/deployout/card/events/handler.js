import { staticVariable } from '../const';
const { tableId } = staticVariable;

function selectedHandle() {
	let checkedRows = this.props.cardTable.getCheckedRows(tableId);
	let disabled = false;
	if (checkedRows.length < 1) {
		disabled = true;
	}
	this.props.button.setDisabled({ DelLine: disabled });
}

export { selectedHandle };
