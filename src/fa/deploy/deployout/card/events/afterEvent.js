import { ajax, toast } from 'nc-lightapp-front';
import { staticVariable, reqUrl, orgChangeClearFields } from '../const';
import { lockrequest, setValue } from './buttonClick';
const { pagecode, formId, tableId } = staticVariable;
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { OrgChangeEvent } = components;
const { orgChangeEvent } = OrgChangeEvent;
const { setCardValue } = cardUtils;

/**
 * 表头编辑后
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	// 财务组织编辑后
	if (key === 'pk_org_v' || key === 'pk_org') {
		handlePkOrgVAfterEvent.call(this, props, moduleId, key, value, oldValue);
	}
	// 调出、调入组织不能一样
	if (key === 'pk_org_in' || key === 'pk_org_in_v') {
		handlePkOrgInAfterEditEvent.call(this, props, moduleId, key, value);
	}
	// 结算币种
	if (key === 'account_currency') {
		handleAccountCurrencyAfterEditEvent.call(this, props, moduleId, key, value);
	}
	// 调出日期
	if (key === 'business_date') {
		handleBusinessDateAfterEditEvent.call(this, props, moduleId, key, value);
	}
	// 结算汇率
	if (key === 'account_rate') {
		handleAccountRateAfterEditEvent.call(this, props, moduleId, key, value);
	}
}
/**
 * 表体编辑后
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	// 卡片编辑后
	if (key === 'pk_card') {
		handlePkCardAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index);
	}
}
/**
 * 财务组织编辑后
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 */
function handlePkOrgVAfterEvent(props, moduleId, key, value, oldValue) {
	let that = this;
	let rowsdata = props.cardTable.getAllRows(tableId);
	let callback = () => {
		let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v').value;
		if (pk_org_v) {
			//财务组织切换编辑后事件
			handlePkOrgAfterEditEvent.call(that, props, moduleId, key, value);
		}
		//表体清空解锁
		unlockCard.call(this, props, rowsdata);
	};
	orgChangeEvent.call(
		this,
		props,
		pagecode,
		formId,
		null, //不传tableId，在回调函数中在清空表体数据
		key,
		value,
		oldValue,
		orgChangeClearFields,
		[ 'confirm_flag' ],
		callback
	);
}

/**
 * 卡片解锁处理
 */
function unlockCard(props, rowsdata) {
	let allpks = [];
	if (rowsdata && rowsdata.length > 0) {
		rowsdata.map((val) => {
			// 若当前编辑态，且存在新增卡片则收集解锁
			if (val && val.values && val.values.pk_card && val.values.pk_card.value) {
				allpks.push(val.values.pk_card.value);
			}
		});
		lockrequest.call(this, 'unlock', allpks);
	}
	//更换组织之后需要清空表体行
	props.cardTable.setTableData(tableId, { rows: [] });
}

/**
 * 结算币种
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
function handleAccountCurrencyAfterEditEvent(props, moduleId, key, value) {
	handleHeadAfterEditEvent.call(this, props, moduleId, key, value);
}
/**
 * 结算汇率
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
function handleAccountRateAfterEditEvent(props, moduleId, key, value) {
	handleHeadAfterEditEvent.call(this, props, moduleId, key, value);
}
/**
 * 表头编辑后
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
function handleHeadAfterEditEvent(props, moduleId, key, value) {
	// 判空
	if (!value.value) {
		return;
	}
	let cardData = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	// 卡片联动处理
	ajax({
		url: reqUrl.headAfterEdit,
		data: cardData,
		success: (res) => {
			if (res.data) {
				setCardValue.call(this, props, res.data);
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			// 若出错，置空
			props.form.setFormItemsValue(formId, { [key]: { value: '', display: '' } });
		}
	});
}

/**
 * pk_org财务组织编辑后事件
 * 处理逻辑：
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
export function handlePkOrgAfterEditEvent(props, moduleId, key, value) {
	if (value && value.value) {
		// 恢复字段的可编辑性
		props.resMetaAfterPkorgEdit();
		props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
	} else {
		props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], true);
		return;
	}
	let cardData = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	ajax({
		url: reqUrl.headAfterEdit,
		data: cardData,
		success: (res) => {
			if (res.data && res.data.head && res.data.head[formId]) {
				props.form.setAllFormValue({ [formId]: res.data.head[formId] });
				setHeadDefaultValue.call(this, props);
			}
			if (res.data && res.data.body && res.data.body[tableId]) {
				setValue.call(this, props, res.data);
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			// 若出错，置空
			props.form.setFormItemsValue(formId, { [key]: { value: '', display: '' } });
		}
	});
}

/**
 * 调入组织编辑后：调出，调入财务组织不能相同
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
function handlePkOrgInAfterEditEvent(props, moduleId, key, value) {
	// 判空
	if (!value.value) {
		return;
	}
	let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v').value;
	if (key === 'pk_org_in_v' && value.value == pk_org_v) {
		props.form.setFormItemsValue(formId, { [key]: { value: '', display: '' } });
		toast({ color: 'danger', content: getMultiLangByID('2012003504A-000000') }); // '调出、调入业务单元不能相同！'
		return;
	} else if (key === 'pk_org_in' && value.value == pk_org) {
		props.form.setFormItemsValue(formId, { [key]: { value: '', display: '' } });
		toast({ color: 'danger', content: getMultiLangByID('2012003504A-000000') }); // '调出、调入业务单元不能相同！'
		return;
	}
	let cardData = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	// 卡片联动处理
	ajax({
		url: reqUrl.headAfterEdit,
		data: cardData,
		success: (res) => {
			if (res.data && res.data.head && res.data.head[formId]) {
				props.form.setAllFormValue({ [formId]: res.data.head[formId] });
				setHeadDefaultValue.call(this, props);
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			// 若出错，置空
			props.form.setFormItemsValue(formId, { [key]: { value: '', display: '' } });
		}
	});
}
/**
 * 设置表头默认值
 * @param {*} props 
 */
function setHeadDefaultValue(props) {
	props.form.setFormItemsValue(formId, { account_rate: { value: 1 } });
}
/**
 * 表头调出日期编辑后处理
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
function handleBusinessDateAfterEditEvent(props, moduleId, key, value) {
	// 日期未选中值，则返回
	if (value.value == null || value.value == '') {
		return;
	}
	let cardData = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);

	// 表体空则返回
	let filterRows = cardData.card.body[tableId].rows.filter((row) => {
		return row.values.pk_card.value != null && row.values.pk_card.value != '';
	});
	cardData.card.body[tableId].rows = filterRows;
	if (cardData.card.body[tableId].rows.length == 0) {
		return;
	}
	// 卡片联动处理
	ajax({
		url: reqUrl.headAfterEdit,
		data: cardData,
		success: (res) => {
			if (res.data && res.data.body && res.data.body[tableId]) {
				setCardValue.call(this, props, res.data);
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			// 若出错，置空
			props.form.setFormItemsValue(formId, { [key]: { value: '', display: '' } });
		}
	});
}
/**
 *  表体卡片编辑后处理
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
function handlePkCardAfterEditEvent(props, moduleId, key, value, changedRows, index) {
	let bussiness_date = props.form.getFormItemsValue(formId, 'business_date').value;
	// 调出日期为空时，提示用户信息。
	if (!bussiness_date) {
		// 则清空表头固定字段值，清空表体值，表体加空行并给出提示
		toast({ color: 'danger', content: getMultiLangByID('2012003504A-000001') }); // '请先选调出日期！'
		props.cardTable.setValByKeyAndIndex(tableId, index, key, { value: '', display: '' });
		return;
	}

	let cardData = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, index);
	// 卡片联动处理
	ajax({
		url: reqUrl.bodyAfterEdit,
		data: cardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				setValue.call(this, props, data, index);
			}
		},
		error: (res) => {
			clearBodyDataByIndex.call(this, props, index);
			toast({ color: 'danger', content: res.message });
		}
	});
}
/**
 * 根据索引清除当前行数据
 * @param {*} props 
 * @param {*} index 
 */
function clearBodyDataByIndex(props, index) {
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card', { display: null, value: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card.card_code', { display: null, value: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card.asset_name', { display: null, value: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card.spec', { display: null, value: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card.card_model', { display: null, value: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card.pk_category', { display: null, value: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card.pk_depmethod', { display: null, value: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card.localoriginvalue', { display: null, value: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card.originvalue', { display: null, value: null });
	props.cardTable.delRowsByIndex(tableId, index + 1);
}
