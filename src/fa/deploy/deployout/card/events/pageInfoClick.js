import { getDataByPk } from './';
// 卡片翻页查询
export default function(props, pk) {
	props.setUrlParam(pk);
	getDataByPk.call(this, props, pk);
}
