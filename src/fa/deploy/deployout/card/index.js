import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	bodyAfterEvent,
	headAfterEvent,
	pageInfoClick,
	getDataByPk,
	saveAction,
	lockcards,
	backToList,
	selectedHandle,
	commitAction,
	modelDelRow,
	beforeEvent
} from './events';
import { staticVariable } from './const';
const { NCAffix, NCStep } = base;
const { ApproveDetail } = high;
const { pagecode, formId, tableId, dataSource, title } = staticVariable;
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans } = components;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;

/**
 * 资产调出卡片
 * wangwhf
 * 2018/08/08
 */
class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			step: 0,
			// 审批详情
			pk_bill: this.props.getUrlParam('id'),
			showApprove: false,
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}
	componentWillMount() {
		// 解锁卡片
		window.onbeforeunload = () => {
			let status = this.props.cardTable.getStatus(tableId);
			if (status == 'edit') {
				lockcards.call(this, this.props, 'unlock', false);
			}
		};
		if (window.history && window.history.pushState) {
			window.onpopstate = () => {
				lockcards.call(this, this.props, 'unlock', false);
			};
		}
	}
	// 移除事件
	componentWillUnmount() {
		window.onbeforeunload = null;
	}
	componentDidMount() {
		let status = this.props.getUrlParam('status');
		if (status != 'add') {
			let pk = this.props.getUrlParam('id');
			if (pk && pk != 'undefined') {
				getDataByPk.call(this, this.props, pk);
			}
		}
	}
	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};
	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 4,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	// 进度条
	createSteps = () => {
		let status = this.props.form.getFormStatus(formId);
		// 非浏览态不显示步骤条
		if (!status || status != 'browse') {
			return;
		}
		let step = this.state.step;
		// 页面无数据不显示步骤条
		if (step === -1) {
			return;
		}
		let bill_status = this.props.form.getFormItemsValue(formId, 'bill_status');
		let bill_status_in = this.props.form.getFormItemsValue(formId, 'bill_status_in');
		if (bill_status_in && bill_status_in.value) {
			switch (bill_status_in.value) {
				case '0':
					step = 3;
					break;
				case '1':
				case '2':
					step = 4;
					break;
				case '3':
					step = 5;
					break;
				default:
					break;
			}
		} else if (bill_status && bill_status.value) {
			switch (bill_status.value) {
				case '0':
					step = 1;
					break;
				case '1':
				case '2':
					step = 2;
					break;
				case '3':
					step = 3;
					break;
				default:
					break;
			}
		}
		return (
			<div style={{ padding: 20 }}>
				<NCStep.NCSteps current={step}>
					<NCStep title={getMultiLangByID('2012003504A-000010')} />
					<NCStep title={getMultiLangByID('2012003504A-000011')} />
					<NCStep title={getMultiLangByID('2012003504A-000012')} />
					<NCStep title={getMultiLangByID('2012003504A-000013')} />
					<NCStep title={getMultiLangByID('2012003504A-000014')} />
				</NCStep.NCSteps>{' '}
			</div> //"调出录入""调出提交""调出完成""调入确认""调入完成"
		);
	};
	render() {
		let { cardTable, form, ncmodal, button, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createButtonApp } = button;
		let { createCardTable } = cardTable;
		let { createModal } = ncmodal;
		return (
			<div id="nc-bill-card" className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title),
								formId,
								backBtnClick: backToList
							})}
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>

					<div className="nc-bill-form-area">
						{this.createSteps()}
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: saveAction.bind(this),
							showCheck: true,
							showIndex: true,
							isAddRow: true,
							onSelectedAll: selectedHandle.bind(this),
							onSelected: selectedHandle.bind(this),
							modelDelRowBefore: modelDelRow.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				</div>
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/* 附件*/}
				{createNCUploader('uploader', {})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} // 国际化：指派
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={(content) => {
							commitAction.call(this, this.props, 'SAVE', 'commit', content); //原提交方法添加参数content
						}}
						cancel={() => {
							this.setState({
								compositedisplay: false
							});
						}}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}
Card = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(Card);

export default Card;
