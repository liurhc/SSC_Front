export const commonConst = {
	moduleId: '2012', //所属模块编码
	appid: '201203504A',
	billtype: 'HC',
	field: 'pk_deployout',
	title: '2012003504A-000018', // 资产调出单
	dataSource: 'fa.deploy.deployout.main',
	toDaployInData: {
		url: '/fa/deploy/deployin/main/index.html#/card',
		appcode: '201203508A',
		pagecode: '201203508A_card'
	}
};
