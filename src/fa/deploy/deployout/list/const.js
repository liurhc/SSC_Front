import { commonConst as deploycommonConst } from '../const';
import ampub from 'ampub';
const { commonConst } = ampub;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;

// 节点固定变量数据值
export const staticVariable = {
	...deploycommonConst,
	pagecode: '201203504A_list', //单据模板pageID
	searchId: 'searchArea', //查询区域编码
	tableId: 'list_head', //列表表体编码
	formId: 'card_head', //列表表头编码
	node_code: '2012052020',
	// 打印模板所在NC节点及标识
	printFuncode: '2012052020',
	printNodekey: null,
	cardRouter: '/card'
};
// 过滤参数
export const defaultConfig = {
	searchId: 'searchArea',
	formId: 'card_head',
	bodyIds: [ 'list_head' ],
	specialFields: {
		deployer: {
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		}
	},
	linkageData: {
		//联动链
		//参照字段:['被过滤字段1'，'被过滤字段2']
		pk_org: [
			'deployer', //经办人
			'bodyvos.pk_card', // 卡片编号
			'bodyvos.pk_card.pk_category', //资产类别
			'bodyvos.pk_card.pk_usedept', //使用部门
			'bodyvos.pk_card.pk_mandept' //管理部门
		]
	}
};
// ajax请求链接
export const reqUrl = {
	insertUrl: '/nccloud/fa/deployout/insert.do',
	updateUrl: '/nccloud/fa/deployout/update.do',
	listQueryUrl: '/nccloud/fa/deployout/listquery.do',
	cardQueryUrl: '/nccloud/fa/deployout/cardquery.do',
	deleteUrl: '/nccloud/fa/deployout/delete.do',
	commitUrl: '/nccloud/fa/deployout/commit.do',
	pageDataByPksUrl: '/nccloud/fa/deployout/querypagegridbypks.do', // 分页查询
	printUrl: '/nccloud/fa/deployout/printCard.do',
	edit: '/nccloud/fa/deployout/edit.do'
};
