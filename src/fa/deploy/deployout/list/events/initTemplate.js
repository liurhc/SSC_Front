import tableButtonClick from './tableButtonClick';
import { setBatchBtnsEnable, linkToCard, toDeployIn } from './buttonClick';
import { staticVariable, defaultConfig } from '../const';
const { pagecode, tableId, dataSource } = staticVariable;
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { createOprationColumn } = listUtils;
const { getContext, loginContextKeys, loginContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

export default function(props) {
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					//获取登录上下文信息
					loginContext.call(this, data.context, dataSource);
					getContext.call(this, loginContextKeys.dataSourceCode, dataSource);
					//设置交易类型
					let transi_type = getContext(loginContextKeys.transtype, dataSource);
					this.setState({ transi_type });
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	refFilter.call(this, props, meta);
	// 添加表格操作列
	let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
	meta[tableId].items.push(oprCol);
	return meta;
}
/**
 * 参照过滤
 * @param {*} props 
 * @param {*} meta 
 */
function refFilter(props, meta) {
	// 参照条件过滤
	addSearchAreaReferFilter(props, meta, defaultConfig);
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td">
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, 'browse');
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		if (item.attrcode == 'bill_code_in') {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td">
						<span
							className="code-detail-link"
							onClick={() => {
								toDeployIn.call(this, props, record);
							}}
						>
							{record && record.bill_code_in && record.bill_code_in.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
}
