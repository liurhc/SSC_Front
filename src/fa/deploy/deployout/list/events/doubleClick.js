import { linkToCard } from './buttonClick';
/**
 * 双击跳转
 * @param {*} record 
 * @param {*} index 
 * @param {*} e 
 */
export default function doubleClick(record, index, props) {
	linkToCard.call(this, props, record);
}
