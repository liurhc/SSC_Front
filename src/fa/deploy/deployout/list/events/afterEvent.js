import { defaultConfig } from '../const';
import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage } = fa_components;
const { referLinkageClear } = ReferLinkage;

export default function searchAfterEvent(field, value) {
	//联动处理
	referLinkageClear(this.props, field, defaultConfig);
}
