import { ajax } from 'nc-lightapp-front';
import { staticVariable, reqUrl } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;
const { pagecode } = staticVariable;

export default function(props, config, pks) {
	let data = {
		allpks: pks,
		pagecode
	};
	ajax({
		url: reqUrl.pageDataByPksUrl,
		data: data,
		success: function(res) {
			let { success } = res;
			if (success) {
				setListValue.call(this, props, res);
			}
		}
	});
}
