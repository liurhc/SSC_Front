import { ajax, toast, print, output } from 'nc-lightapp-front';
import { staticVariable, reqUrl } from '../const';
import searchBtnClick from './searchBtnClick';
const { pagecode, tableId, field, cardRouter, dataSource } = staticVariable;
const { commitUrl } = reqUrl;
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { CommonKeys } = commonConst;
const { multiLangUtils, msgUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { linkQueryConst } = CommonKeys;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { ScriptReturnUtils } = components;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { getScriptListReturn } = ScriptReturnUtils;

export default function buttonClick(props, id) {
	switch (id) {
		case 'Add':
			linkToCard.call(this, props, undefined, 'add');
			break;
		case 'Delete':
			showConfirm.call(this, props, {
				type: MsgConst.Type.DelSelect,
				beSureBtnClick: deleteAction
			});
			break;
		case 'Commit':
			commitAction.call(this, 'SAVE', props);
			break;
		case 'UnCommit':
			commitAction.call(this, 'UNSAVE', props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refreshAction.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		default:
			break;
	}
}
/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	props.pushTo(cardRouter, {
		status,
		id: record[field] ? record[field].value : '',
		pagecode: pagecode.replace('list', 'card')
	});
}
// delete
function deleteAction(props) {
	let data = props.table.getCheckedRows(tableId);
	if (data.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_deployout.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			getScriptListReturn(params, res, props, 'pk_deployout', tableId, tableId, true);
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}
/**
 * 提交 、收回 
 */
export function commitAction(OperatorType, props, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_deployout.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecode,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(this, params, res, props, 'pk_deployout', tableId, tableId, false, dataSource);
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
}
/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintParam.call(this, props);
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}

	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printUrl, // 后台打印服务url
		printData
	);
}
/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintParam.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: reqUrl.printUrl,
		data: printData
	});
}

export function getPrintParam(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values.pk_deployout.value);
	});
	let param = {
		filename: pagecode, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType
	};
	return param;
}
/**
 * 重新加载页面
 * @param {*} props 
 */
function refreshAction(props) {
	batchRefresh.call(this, props, searchBtnClick);
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billNo 是单据主键
	let billId = checkedrows[0].data.values['pk_deployout'].value;
	props.ncUploader.show('uploader', {
		billId: 'fa/deployout/' + billId,
		billNo
	});
}

export function toDeployIn(props, record) {
	if (record && record.pk_deployin && record.pk_deployin.value) {
		ajax({
			url: '/nccloud/fa/deployout/linkQuerydepoyin.do',
			data: { [linkQueryConst.ID]: record.pk_deployin.value },
			success: (res) => {
				if (res.data) {
					let linkData = res.data;
					linkData['status'] = 'browse';
					linkData[linkQueryConst.SCENE] = linkQueryConst.SCENETYPE.linksce; // 设置联查场景
					props.openTo(linkData[linkQueryConst.URL], linkData);
				} else {
					toast({ content: getMultiLangByID('QueryAbout-000002'), color: 'warning' }); /*国际化处理：'无权限'*/
				}
			},
			error: (err) => {
				toast({ content: err.message, color: 'danger' });
			}
		});
	}
}
