import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	beforeEvent,
	headAfterEvent,
	bodyAfterEvent,
	pageInfoClick,
	getDataByPk,
	saveAction,
	backToList,
	commitAction,
	getDeployInMsg
} from './events';
import { staticVariable } from './const';
const { NCAffix, NCStep } = base;
const { ApproveDetail } = high;
const { pagecode, formId, tableId, title, dataSource } = staticVariable;
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans } = components;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
import fa from 'fa';
const { fa_components } = fa;
const { WorkloadModal } = fa_components;

/**
 * 资产调入卡片
 * wangwhf
 * 2018/08/09
 */
class Card extends Component {
	constructor(props) {
		super(props);
		this.is_allow_dept_midlev = false; //是否允许部门非末级(组织级数据)
		this.state = {
			// 合计信息
			totalnum: '',
			//工作量法处理的值
			isworkloan: false,
			row_number: null,
			workloanvalues: {},
			// 审批详情
			pk_bill: this.props.getUrlParam('id'),
			showApprove: false,
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {
		let status = this.props.getUrlParam('status');
		if (status != 'add') {
			let pk = this.props.getUrlParam('id');
			if (pk && pk != 'undefined') {
				getDataByPk.call(this, this.props, pk);
			} else {
				//处理消息打开
				let pkMsg = this.props.getUrlParam('pk_msg');
				if (pkMsg && pkMsg != 'undefined' && pkMsg != 'null') {
					getDeployInMsg.call(this, this.props, pkMsg);
				}
			}
		}
	}

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};
	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 4,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	// 进度条
	createSteps = () => {
		let status = this.props.form.getFormStatus(formId);
		if (!status || status != 'browse') {
			return;
		}
		let step = 3;
		let bill_status = this.props.form.getFormItemsValue(formId, 'bill_status');
		if (bill_status && bill_status.value) {
			switch (bill_status.value) {
				case '0':
					step = 3;
					break;
				case '1':
				case '2':
					step = 4;
					break;
				case '3':
					step = 5;
					break;
				default:
					break;
			}
		} else {
			return;
		}
		return (
			<div style={{ padding: 20 }}>
				<NCStep.NCSteps current={step}>
					<NCStep title={getMultiLangByID('2012003508A-000006')} />
					<NCStep title={getMultiLangByID('2012003508A-000007')} />
					<NCStep title={getMultiLangByID('2012003508A-000008')} />
					<NCStep title={getMultiLangByID('2012003508A-000009')} />
					<NCStep title={getMultiLangByID('2012003508A-000010')} />
				</NCStep.NCSteps>{' '}
			</div> //"调出录入""调出提交""调出完成""调入确认""调入完成"
		);
	};

	render() {
		let { cardTable, form, button, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		let config = {
			modalId: 'worked', //弹框的ID
			defaultValue: {
				//默认值
				total: 0.0, //总工作量
				accumulation: 0.0, //累计工作量
				unit: getMultiLangByID('2012003508A-000011') //'天' //工作量单位
			},
			onSureClick: (total, accumulation, unit) => {
				let row = this.state.row_number;
				this.props.cardTable.setValByKeyAndIndex(tableId, row, 'allworkloan', { value: total });
				this.props.cardTable.setValByKeyAndIndex(tableId, row, 'accuworkloan', { value: accumulation });
				this.props.cardTable.setValByKeyAndIndex(tableId, row, 'workloanunit', { value: unit });
				let workloanvalues = {};
				workloanvalues.allworkloan = total;
				workloanvalues.accuworkloan = accumulation;
				workloanvalues.workloanunit = unit;
				this.setState({
					workloanvalues: workloanvalues
				});
			}
		};
		return (
			<div id="nc-bill-card" className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title),
								formId,
								backBtnClick: backToList
							})}
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>

					<div className="nc-bill-form-area">
						{this.createSteps()}
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: saveAction.bind(this),
							onBeforeEvent: beforeEvent.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							showIndex: true,
							showCheck: true,
							hideAdd: true,
							hideDel: true
						})}
					</div>
				</div>
				{createModal(`${pagecode}-confirm`, {})}
				<WorkloadModal {...this.props} config={config} />
				{/* 附件*/}
				{createNCUploader('uploader', {})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} // 国际化：指派
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={(content) => {
							commitAction.call(this, this.props, 'SAVE', 'commit', content); //原提交方法添加参数content
						}}
						cancel={() => {
							this.setState({
								compositedisplay: false
							});
						}}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(Card);

export default Card;
