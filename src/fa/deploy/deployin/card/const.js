import ampub from 'ampub';
const { commonConst } = ampub;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
import { conmmonConst } from '../const';
// 节点固定变量数据值
export const staticVariable = {
	...conmmonConst,
	pagecode: '201203508A_card', //单据模板pageID
	searchId: '201203508A', //查询区域编码
	tableId: 'bodyvos', //列表表体编码
	formId: 'card_head', //列表表头编码
	// 打印模板所在NC节点及标识
	printFuncode: '2012052030',
	printNodekey: null,
	listRouter: '/list'
};
// 过滤参数
export const defaultConfig = {
	searchId: 'searchArea',
	formId: 'card_head',
	bodyIds: [ 'bodyvos', 'bodyvos_edit' ],
	onlyLeafCanSelect: {
		// 使用部门非末级处理，默认不可勾选
		usedept: true
	},
	specialFields: {
		deployer: {
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		usedept: {
			data: [
				{
					fields: [ 'pk_equiporg', 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		pk_ownerorg_v: {
			addOrgRelation: 'pk_orgs',
			RefActionExtType: 'TreeRefActionExt',
			class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_OWNERORG_VSqlBuilder',
			//货主管理组织
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		}
	}
};
// ajax请求链接
export const reqUrl = {
	insertUrl: '/nccloud/fa/deployin/insert.do',
	updateUrl: '/nccloud/fa/deployin/update.do',
	listQueryUrl: '/nccloud/fa/deployin/listquery.do',
	cardQueryUrl: '/nccloud/fa/deployin/cardquery.do',
	deleteUrl: '/nccloud/fa/deployin/delete.do',
	commitUrl: '/nccloud/fa/deployin/commit.do',
	pageDataByPksUrl: '/nccloud/fa/deployin/querypagegridbypks.do', // 分页查询
	billCodeGenUrl: '/nccloud/fa/deployin/billcodegenbackevent.do',
	headAfterEdit: '/nccloud/fa/deployin/headafteredit.do',
	bodyAfterEdit: '/nccloud/fa/deployin/bodyafteredit.do',
	printUrl: '/nccloud/fa/deployin/printCard.do',
	batchAlter: '/nccloud/fa/deployin/batchAltert.do',
	edit: '/nccloud/fa/deployin/edit.do',
	assetCodeEditControl: '/nccloud/fa/deployin/AssetCodeEditControl.do',
	openDeployInMsgURL: '/nccloud/fa/deployin/openDeployInMsg.do'
};
export const buttonsStaus = {
	// 编辑态按钮
	editBtns: [ 'SaveGroup', 'Cancel', 'GetFinanceData', 'OpenCard', 'AddLine', 'DelLine', 'BatchAlter' ],
	// 浏览态按钮
	browseBtns: [
		'Edit',
		'Commit',
		'UnCommit',
		'Attachment',
		'OpenCard',
		'Refresh',
		'More',
		'Print',
		'ReceiptGroup',
		'QueryAboutBillFlow'
	]
};
