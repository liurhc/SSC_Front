import { ajax } from 'nc-lightapp-front';
import { staticVariable, defaultConfig, reqUrl } from '../const';
const { formId, tableId } = staticVariable;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter, DeptMidlevRefFilter } = fa_components;
const { addBodyReferFilter } = ReferFilter;
const { queryOnlyLeafCanSelect } = DeptMidlevRefFilter;

export default function beforeEvent(props, moduleId, key, value, index, record) {
	if (key === 'asset_code') {
		let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
		ajax({
			url: reqUrl.assetCodeEditControl,
			data: { pk_org },
			success: (res) => {
				let { success, data } = res;
				// 如果FA11为是，则可编辑，否则不可编辑
				if (success && data) {
					props.cardTable.setEditableByIndex(tableId, index, [ 'asset_code' ], true);
				} else {
					props.cardTable.setEditableByIndex(tableId, index, [ 'asset_code' ], false);
				}
			}
		});
	}
	if (key == 'pk_mandept_v' || key == 'usedept') {
		let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
		//表体部门非末级处理
		let field = [ 'pk_mandept', 'pk_mandept_v' ]; //传管理部门字段
		let assembleData = {
			formId,
			tableId,
			pk_org,
			field
		};
		//该方法只能处理管理部门，使用部门在表体参照过滤中进行处理
		queryOnlyLeafCanSelect.call(this, props, assembleData);
		//使用部门非末级处理
		defaultConfig.onlyLeafCanSelect.usedept = !this.is_allow_dept_midlev;
	}
	// 表体参照过滤
	let meta = props.meta.getMeta();
	addBodyReferFilter.call(this, props, meta, defaultConfig, record);
	return true;
}
