import { ajax, toast } from 'nc-lightapp-front';
import { staticVariable, reqUrl } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setCardValue } = cardUtils;
const { pagecode, formId, tableId } = staticVariable;
/**
 * 表头编辑后
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	// 调入日期
	if (key === 'business_date') {
		handleBusinessDateAfterEditEvent.call(this, props, moduleId, key, value);
	}
}
/**
 * 调入日期
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} i 
 */
function handleBusinessDateAfterEditEvent(props, moduleId, key, value) {
	if (!value.value) {
		return;
	}
	let cardData = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	//
	ajax({
		url: reqUrl.headAfterEdit,
		data: cardData,
		success: (res) => {
			if (res.data && res.data.body && res.data.body[tableId]) {
				setCardValue.call(this, props, res.data);
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			// 若出错，置空
			props.form.setFormItemsValue(formId, { [key]: { value: '', display: '' } });
		}
	});
}
/**
 * 表体编辑后
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	// 类别判断，不能为空且必须为最末级
	if (key === 'pk_category') {
		handlePkCategoryAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index, record);
	}
	// 开始使用日期
	if (key === 'begin_date') {
		handleBeginDateAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index, record);
	}
	// 使用月限
	if (key === 'naturemonth') {
		handleNatureMonthAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index, record);
	}
	// 净残值率反算
	if (key === 'salvage') {
		if (value == null || value == '') {
			return;
		}
		handleSalvageAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index, record);
	}
	// 当手工录入折旧截止日期时，重新计算使用月限和折旧期数
	if (key === 'dep_end_date') {
		depEndDateAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index, record);
	}
	// 净残值率
	if (key === 'salvagerate') {
		handleSalvageRateAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index, record);
	}
	// 折旧方法变动
	if (key === 'pk_depmethod') {
		handlePkDepmethodAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index, record);
	}
	if (key === 'pk_ownerorg_v' || key === 'pk_ownerorg') {
		// 编辑货主管理组织，清空管理部门
		handlePkOwnerOrgVAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index);
	}
	if (key === 'pk_equiporg_v' || key === 'pk_equiporg') {
		// 编辑使用权，清空使用部门和使用人
		handlePkEquipOrgVAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index);
	}
}
// ======================================= 表体 ====================================================================
/**
 * 货主管理组织
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} i 
 */
function handlePkOwnerOrgVAfterEditEvent(props, moduleId, key, value, changedRows, i) {
	// 清空管理部门
	props.cardTable.setValByKeyAndIndex(moduleId, i, 'pk_mandept_v', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(moduleId, i, 'pk_mandept', { value: null, display: null });
}
/**
 * 使用权
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} i 
 */
function handlePkEquipOrgVAfterEditEvent(props, moduleId, key, value, changedRows, i) {
	// 清空使用部门，使用人
	props.cardTable.setValByKeyAndIndex(moduleId, i, 'usedept', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(moduleId, i, 'pk_assetuser', { value: null, display: null });
}
/**
 * 类别判断
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} i 
 */
function handlePkCategoryAfterEditEvent(props, moduleId, key, value, changedRows, i, record) {
	baseHandleAfterEditEvent.call(this, props, moduleId, key, value, changedRows, i, record);
}
/**
 * 开始使用日期
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} i 
 */
function handleBeginDateAfterEditEvent(props, moduleId, key, value, changedRows, i, record) {
	let business_date = props.form.getFormItemsValue(formId, 'business_date');
	// 表体开始使用日期编辑后需要表头调入日期字段，故在次做校验
	if (!business_date.value) {
		toast({ color: 'danger', content: getMultiLangByID('2012003508A-000000') }); // '表头调入日期不能为空'
		return;
	}
	baseHandleAfterEditEvent.call(this, props, moduleId, key, value, changedRows, i, record);
}
/**
 * 使用月限
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} i 
 */
function handleNatureMonthAfterEditEvent(props, moduleId, key, value, changedRows, i, record) {
	baseHandleAfterEditEvent.call(this, props, moduleId, key, value, changedRows, i, record);
}
/**
 * 净残值率反算
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} i 
 */
function handleSalvageAfterEditEvent(props, moduleId, key, value, changedRows, i, record) {
	baseHandleAfterEditEvent.call(this, props, moduleId, key, value, changedRows, i, record);
}
/**
 * 当手工录入折旧截止日期时，重新计算使用月限和折旧期数
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} i 
 */
function depEndDateAfterEditEvent(props, moduleId, key, value, changedRows, i, record) {
	baseHandleAfterEditEvent.call(this, props, moduleId, key, value, changedRows, i, record);
}
/**
 * 净残值率
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} i 
 */
function handleSalvageRateAfterEditEvent(props, moduleId, key, value, changedRows, i, record) {
	baseHandleAfterEditEvent.call(this, props, moduleId, key, value, changedRows, i, record);
}
/**
 * 折旧方法变动
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} i 
 */
function handlePkDepmethodAfterEditEvent(props, moduleId, key, value, changedRows, i, record) {
	baseHandleAfterEditEvent.call(this, props, moduleId, key, value, changedRows, i, record);
	if (value.refcode === '04') {
		this.setState({
			isworkloan: true,
			row_number: i
		});
		props.modal.show('worked');
	}
}
/**
 * 编辑后请求类
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} i 
 */
function baseHandleAfterEditEvent(props, moduleId, key, value, changedRows, i, record) {
	let cardData = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, i);
	//新增功能---差异化处理
	let newRecord = JSON.parse(JSON.stringify(record || {}));
	cardData.card.body[moduleId] = {
		...cardData.card.body[moduleId],
		rows: [ newRecord ]
	};
	cardData.index = 0; //修改编辑行为0
	ajax({
		url: reqUrl.bodyAfterEdit,
		data: cardData,
		async: false,
		success: (res) => {
			if (res.data && res.data.body && res.data.body[tableId]) {
				// let rows = res.data.data.body[tableId].rows;
				// props.cardTable.updateDataByIndexs(tableId, [ { index: i, data: rows[i] } ]);
				//新增功能---差异化处理
				props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
			}
			if (res.data.userjson) {
				let userObj = JSON.parse(res.data.userjson);
				if (userObj && userObj['alterMsg'] != null) {
					toast({ color: 'warning', content: userObj['alterMsg'] });
				}
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			props.cardTable.setValByKeyAndIndex(moduleId, i, key, { value: null, display: '' });
		}
	});
}
