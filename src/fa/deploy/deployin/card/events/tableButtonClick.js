import { staticVariable } from '../const';
const { tableId } = staticVariable;
/**
 * 卡片表体操作列按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		case 'OpenCard':
			let status = props.cardTable.getStatus(tableId);
			if (status === 'browse') {
				props.cardTable.toggleRowView(tableId, record);
			} else if (status == 'edit') {
				props.cardTable.setClickRowIndex(tableId, { record, index });
				props.cardTable.openModel(tableId, 'edit', record, index);
			}
			break;
		case 'DelLine':
			props.cardTable.delRowsByIndex(tableId, index);
			break;
		default:
			break;
	}
}
