import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';
import { staticVariable, reqUrl, buttonsStaus } from '../const';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { CommonKeys, StatusUtils } = commonConst;
const { multiLangUtils, msgUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { linkQueryConst } = CommonKeys;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { LoginContext, ScriptReturnUtils, saveValidatorsUtil } = components;
const { UISTATE } = StatusUtils;
const { beforeSaveValidator } = saveValidatorsUtil;
const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = cardUtils;
const { getContext, loginContextKeys } = LoginContext;
const { getScriptCardReturnData } = ScriptReturnUtils;
import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage, ImageMng } = fa_components;
const { referAlterClear } = ReferLinkage;
const { faImageScan, faImageView } = ImageMng;
const { pagecode, formId, tableId, field, listRouter, dataSource } = staticVariable;
const { editBtns, browseBtns } = buttonsStaus;
const { updateUrl } = reqUrl;
/**
 * 卡片态按钮动作
 * @param {*} props 
 * @param {*} id 
 */
export default function(props, id) {
	switch (id) {
		case 'Save':
			saveAction.call(this, props);
			break;
		case 'Edit':
			editAction.call(this, props);
			break;
		case 'Cancel':
			showConfirm.call(this, props, {
				type: MsgConst.Type.Cancel,
				beSureBtnClick: cancelAction
			});
			break;
		case 'Commit':
			commitAction.call(this, props, 'SAVE', 'commit');
			break;
		case 'SaveCommit':
			commitAction.call(this, props, 'SAVE', 'saveCommit');
			break;
		case 'UnCommit':
			commitAction.call(this, props, 'UNSAVE', '');
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'AddLine':
			props.cardTable.addRow(tableId);
			break;
		case 'DelLine':
			delrowaction.call(this, props);
			break;
		case 'BatchAlter':
			BatchAltert.call(this, props);
			break;
		case 'FAReceiptScan':
			FAReceiptScan.call(this, props);
			break;
		case 'FAReceiptShow':
			FAReceiptShow.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props);
			break;
		default:
			break;
	}
}
/**
 * 调入修改数据权限控制
 * @param {*} props 
 */
function editAction(props) {
	let pk = props.form.getFormItemsValue(formId, field).value || props.getUrlParam('id');
	ajax({
		url: reqUrl.edit,
		data: {
			pk,
			resourceCode: '2012052030'
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'danger', content: res.data });
			} else {
				props.cardTable.closeExpandedRow(tableId);
				setStatus.call(this, props, 'edit');
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
// Save
export function saveAction(props) {
	// 保存前校验
	let validateRet = beforeSaveValidator.call(this, props, formId, tableId);
	if (!validateRet) {
		return;
	}

	let data = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let url = updateUrl;
	data.body.bodyvos.rows.map((currow) => {
		currow.values.pk_deployin = { value: props.form.getFormItemsValue(formId, field).value };
	});
	props.validateToSave(data, () => {
		ajax({
			url: url,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//关闭侧拉
					props.cardTable.closeModel(tableId);
					afterSave.call(this, props, data, UISTATE.edit);
				}
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
			}
		});
	});
}
/**
* 保存后
* @param {*} props 
*/
export function afterSave(props, data, oldstatus) {
	// 性能优化，关闭表单和表格渲染
	props.beforeUpdatePage();
	setValue.call(this, props, data);
	setStatus.call(this, props, UISTATE.browse);
	let pk = props.form.getFormItemsValue(formId, field).value;
	let cacheData = props.createMasterChildData(pagecode, formId, tableId);
	// 保存成功后处理缓存
	if (oldstatus == UISTATE.add) {
		cardCache.addCache(pk, cacheData, formId, dataSource);
	} else {
		cardCache.updateCache(field, pk, cacheData, formId, dataSource);
	}
	showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess }); // '保存成功
	// 性能优化，表单和表格统一渲染
	props.updatePage(formId, tableId);
}

// Cancel
function cancelAction(props) {
	props.form.cancel(formId);
	props.cardTable.resetTableData(tableId);

	setStatus.call(this, props, 'browse');

	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	let pk = props.form.getFormItemsValue(formId, field).value;
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	if (!pk) {
		pk = this.props.getUrlParam('id');
	}
	if (pk) {
		getDataByPk.call(this, props, pk);
	} else {
		props.form.EmptyAllFormValue(formId);
	}
}
//提交
export function commitAction(props, OperatorType, commitType, content) {
	// 保存提交校验
	if (commitType === 'saveCommit') {
		//提交调用保存校验，校验数据正确性(调出生成调入单未完善存在))
		let validateRet = beforeSaveValidator.call(this, props, formId, tableId);
		if (!validateRet) {
			return;
		}
	}
	let business_date = props.form.getFormItemsValue(formId, 'business_date').value;
	if (!business_date) {
		toast({ content: getMultiLangByID('2012003508A-000000'), color: 'danger' }); // '业务日期不能为空'
		return;
	}
	let pk = props.form.getFormItemsValue(formId, field).value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	if (content) {
		ajax({
			url: reqUrl.commitUrl,
			data: CardData,
			success: (res) => {
				if (res.success) {
					this.setState({
						compositedisplay: false
					});
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					let callback = () => {
						setStatus.call(this, props, UISTATE.browse);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						field,
						dataSource,
						null,
						false,
						callback,
						pagecode
					);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	} else {
		props.validateToSave(CardData, () => {
			ajax({
				url: reqUrl.commitUrl,
				data: CardData,
				success: (res) => {
					if (res.success) {
						// 性能优化，关闭表单和表格渲染
						props.beforeUpdatePage();
						let callback = () => {
							setStatus.call(this, props, UISTATE.browse);
							getDataByPk.call(this, props, pk);
						};
						getScriptCardReturnData.call(
							this,
							res,
							props,
							formId,
							tableId,
							field,
							dataSource,
							null,
							false,
							callback,
							pagecode
						);
						// 性能优化，表单和表格统一渲染
						props.updatePage(formId, tableId);
					}
				}
			});
		});
	}
}

// card body 删行
const delrowaction = (props) => {
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});

	props.cardTable.delRowsByIndex(tableId, checkedIndex);
	setBatchBtnsEnable.call(this, props, tableId);
};

/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk, isRefresh) {
	ajax({
		url: reqUrl.cardQueryUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					// 测试说不要，防止以后再要，先注释掉放着
					//修改业务日期为空，设置当前时间为业务日期
					// let status = props.form.getFormStatus(formId);
					// let business_date = props.form.getFormItemsValue(formId, 'business_date');
					// if (status && status == UISTATE.edit && (!business_date || !business_date.value)) {
					// 	let defaultValue = {
					// 		business_date: { display: '', value: getContext(loginContextKeys.businessDate) }
					// 	};
					// 	props.form.setFormItemsValue(formId, defaultValue);
					// }
					setValue.call(this, props, data);
					if (isRefresh) {
						showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
					}
				} else {
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				}
			}
		}
	});
}
/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	if (!data) {
		return;
	}
	setCardValue.call(this, props, data);
	let status = props.form.getFormStatus(formId);
	setStatus.call(this, props, status);
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = 'browse') {
	switch (status) {
		case 'add':
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, 'edit');
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			break;
		case 'edit':
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});
			props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
			setBatchBtnsEnable.call(this, props, tableId);
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			break;
		case 'browse':
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			props.button.setButtonVisible(browseBtns, true);
			// 根据单据状态重置部分按钮的可见性
			setBtnStatusByBillStatus.call(this, props);
			break;
		default:
			break;
	}
	setHeadAreaData.call(this, props, { status });
}

function setEditDefualtValue(props) {
	let defaultValue = {
		business_date: { display: '', value: getContext(loginContextKeys.businessDate) }
	};
	props.form.setFormItemsValue(formId, defaultValue);
}
/**
 * 根据单据状态设置部分按钮可见性
 * @param {*} props 
 * @param {*} bill_status 
 */
export function setBtnStatusByBillStatus(props) {
	setBillFlowBtnsVisible(props, formId, field, 'bill_status');
}
/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	let num = props.cardTable.getNumberOfRows(moduleId);
	let checkedRows = [];
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	props.button.setButtonDisabled([ 'BatchAlter' ], num <= 0);
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}
/**
 * 批改
 * @param {*} props 
 */
function BatchAltert(props) {
	let num = props.cardTable.getNumberOfRows(tableId, false);
	if (num < 2) {
		return;
	}
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);
	let changeKey = changeData.batchChangeKey;
	let value = changeData.batchChangeValue;
	let index = changeData.batchChangeIndex;
	let display = changeData.batchChangeDisplay;
	//0、判断是否允许批改
	if (changeKey === 'usedept') {
		//使用部门
		//如果【使用权】这一列的值不相同，不允许批改
		let pk_equiporgs = props.cardTable.getColValue(tableId, 'pk_equiporg');
		pk_equiporgs = getArrayAttribute(pk_equiporgs, 'value');
		if (pk_equiporgs.length > 1) {
			// [使用权] 列中的值不一致，不允许批改！
			showMessage(props, { content: getMultiLangByID('201201512A-000011'), color: 'warning' });
			return;
		}
	} else if (changeKey === 'pk_mandept' || changeKey === 'pk_mandept_v') {
		//管理部门
		//如果【货主管理组织】这一列的值不相同，不允许批改
		let pk_ownerorgs = props.cardTable.getColValue(tableId, 'pk_ownerorg');
		pk_ownerorgs = getArrayAttribute(pk_ownerorgs, 'value');
		if (pk_ownerorgs.length > 1) {
			//[货主管理组织] 列中的值不一致，不允许批改！
			showMessage(props, { content: getMultiLangByID('201201512A-000012'), color: 'warning' });
			return;
		}
	} else if (changeKey === 'pk_assetuser') {
		//使用人
		//如果【使用权】这一列的值不相同，不允许批改
		let pk_equiporgs = props.cardTable.getColValue(tableId, 'pk_equiporg');
		pk_equiporgs = getArrayAttribute(pk_equiporgs, 'value');
		if (pk_equiporgs.length > 1) {
			showMessage(props, { content: getMultiLangByID('201201512A-000011'), color: 'warning' });
			return;
		}
	}
	if (changeKey === 'pk_ownerorg_v') {
		// 货主管理组织
		referAlterClear(props, formId, tableId, 'pk_ownerorg_v', [ 'pk_mandept_v' ], index, value, display);
	}
	if (changeKey === 'pk_equiporg_v') {
		// 使用权
		referAlterClear(props, formId, tableId, 'pk_equiporg_v', [ 'usedept', 'pk_assetuser' ], index, value, display);
	}
	// 批改使用月限和折旧期数，由于其是数值类型，平台第一次设置空值时，回传过来''，这里手动设置为null
	if ((changeKey === 'naturemonth' || changeKey === 'servicemonth') && value === '') {
		changeData.batchChangeValue = null;
	}
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	ajax({
		url: reqUrl.batchAlter,
		data: {
			card: cardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		},
		success: (res) => {
			if (res.success) {
				if (res.data) {
					if (res.data.data.body && res.data.data.body[tableId]) {
						setCardValue.call(this, props, res.data.data);
					}
					if (res.data.alterMsg) {
						toast({ color: 'warning', content: res.data.alterMsg });
					}
				}
			}
		}
	});
	// 如果是选中工作量法批改，则须分别为每一行赋值总工作量，累计工作量，工作量单位
	let isworkloan = this.state.isworkloan;
	if (isworkloan) {
		props.cardTable.setColValue(tableId, 'allworkloan', { value: this.state.workloanvalues.allworkloan });
		props.cardTable.setColValue(tableId, 'accuworkloan', { value: this.state.workloanvalues.accuworkloan });
		props.cardTable.setColValue(tableId, 'workloanunit', { value: this.state.workloanvalues.workloanunit });
		this.setState({
			isworkloan: false,
			row_number: null
		});
	}
}
//得到不重复的 数组中元素的的某个属性值
function getArrayAttribute(ary, attr) {
	let set = new Set();
	ary.map((item) => {
		if (item[attr]) {
			set.add(item[attr]);
		} else {
			set.add('');
		}
	});
	return [ ...set ];
}
/**
 * 影像扫描
 * @param {*} props 
 */
function FAReceiptScan(props) {
	let imageData = {
		pagecode: pagecode,
		formId: formId,
		tableId: tableId,
		pkField: field
	};
	faImageScan.call(this, props, imageData);
}
/**
 * 影像查看
 * @param {*} props 
 */
function FAReceiptShow(props) {
	let imageData = {
		pagecode: pagecode,
		formId: formId,
		tableId: tableId,
		pkField: field
	};
	faImageView.call(this, props, imageData);
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintParam.call(this, props);
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}

	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printUrl, // 后台打印服务url
		printData
	);
}
/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintParam.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: reqUrl.printUrl,
		data: printData
	});
}
/**
 * 打印输出参数
 * @param {*} props 
 * @param {*} output 
 */
function getPrintParam(props, outputType) {
	let pk = props.form.getFormItemsValue(formId, field);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let param = {
		filename: pagecode, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks // 需要打印数据主键
	};
	if (outputType) {
		param.outputType = outputType;
	}

	return param;
}
/**
 * refresh
 * @param {*} props 
 */
function refresh(props) {
	let id = props.form.getFormItemsValue(formId, field);
	if (!id || !id.value) {
		return;
	}
	if (!id.value) {
		toast({ content: getMultiLangByID('2012003508A-000005'), color: 'danger' }); // '刷新失败'
		return;
	}
	getDataByPk.call(this, props, id.value, true);
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	// 单据主键
	let billId = props.form.getFormItemsValue(formId, field).value;
	props.ncUploader.show('uploader', {
		billId: 'fa/deployin/' + billId
	});
}
/**
 * 审批详情
 * @param {*} props 
 */
function queryAboutBillFlow(props) {
	let id = props.form.getFormItemsValue(formId, field).value;
	let transi_type = props.form.getFormItemsValue(formId, 'transi_type').value;
	this.setState({
		showApprove: true,
		pk_bill: id,
		transi_type: transi_type
	});
}
/**
 * 卡片返回列表
 * @param {*} props 
 */
export function backToList(props) {
	props.pushTo(listRouter, {
		pagecode: pagecode.replace('card', 'list')
	});
}

export function toDeployOut(props) {
	let pk_deployout = props.form.getFormItemsValue(formId, 'pk_bill_src');
	if (pk_deployout && pk_deployout.value) {
		ajax({
			url: '/nccloud/fa/deployin/linkQuerydepoyout.do',
			data: { [linkQueryConst.ID]: pk_deployout.value },
			success: (res) => {
				if (res.data) {
					let linkData = res.data;
					linkData['status'] = 'browse';
					linkData[linkQueryConst.SCENE] = linkQueryConst.SCENETYPE.linksce; // 设置联查场景
					props.openTo(linkData[linkQueryConst.URL], linkData);
				} else {
					toast({ content: getMultiLangByID('QueryAbout-000002'), color: 'warning' }); /*国际化处理：'无权限'*/
				}
			},
			error: (err) => {
				toast({ content: err.message, color: 'danger' });
			}
		});
	}
}
/**
 * 通过消息中心打开
 * @param {*} props 
 * @param {*} pkMsg 
 */
export function getDeployInMsg(props, pkMsg) {
	ajax({
		url: reqUrl.openDeployInMsgURL,
		data: {
			pkMsg: pkMsg,
			pagecode: pagecode
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					props.resMetaAfterPkorgEdit();
					//通过消息打开, 控制卡片不可编辑
					props.table.setTableValueDisabled(tableId, 'pk_card', true);
				}
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
				cancelAction.call(this, props); //减少单消息打开报错提示后跳转到浏览态
			}
		}
	});
}
