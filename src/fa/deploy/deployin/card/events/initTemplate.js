import { toast } from 'nc-lightapp-front';
import buttonClick, { setStatus, toDeployOut } from './buttonClick';
import tableButtonClick from './tableButtonClick';
import { staticVariable, defaultConfig } from '../const';
const { pagecode, formId, tableId } = staticVariable;
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext, faQueryAboutUtils } = components;
const { openAssetCardByPk } = faQueryAboutUtils;
const { afterModifyCardMeta, createOprationColumn } = cardUtils;
const { getContext, loginContextKeys, loginContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter, DeptMidlevRefFilter } = fa_components;
const { addBodyReferFilter, addHeadAreaReferFilter } = ReferFilter;
const { rewriteMeta } = DeptMidlevRefFilter;

export default function(props) {
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					//获取登录上下文信息
					loginContext.call(this, data.context);
					//设置交易类型
					let transi_type = getContext(loginContextKeys.transtype);
					this.setState({ transi_type });
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}
/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status');
	if (status == 'add') {
		buttonClick.call(this, props, 'Add');
	} else if (status == 'edit') {
		buttonClick.call(this, props, 'Edit');
	} else {
		setStatus.call(this, props, status);
	}
}
function modifierMeta(props, meta) {
	// 表头参照参数过滤
	addHeadAreaReferFilter(props, meta, defaultConfig);
	// 表头参照参数过滤
	addBodyReferFilter(props, meta, defaultConfig);
	// 表体参照参数过滤
	bodyReferFilter.call(this, props, meta);
	//表体部门非末级处理
	let field = [ 'pk_mandept', 'pk_mandept_v' ]; //传管理部门字段
	let assembleData = {
		meta,
		formId,
		tableId,
		field
	};
	rewriteMeta.call(this, props, assembleData);
	// 添加表格操作列
	let oprCol = createOprationColumn.call(this, props, {
		formId: formId,
		tableId: tableId,
		tableButtonClick: tableButtonClick
	});
	meta[tableId].items.push(oprCol);
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);
	return meta;
}
/**
 * 卡片超链，增加方式只能选择末级节点
 * @param {*} props 
 * @param {*} meta 
 */
function bodyReferFilter(props, meta) {
	meta[formId].items.map((item) => {
		// 调出超链
		if (item.attrcode == 'bill_code_out') {
			item.renderStatus = 'browse';
			item.render = (fromItem) => {
				return (
					<div style={{ color: '#007ace', cursor: 'pointer' }}>
						<span
							onClick={() => {
								toDeployOut.call(this, props);
							}}
						>
							{fromItem.value}
						</span>
					</div>
				);
			};
		}
	});
	meta[tableId].items.map((item) => {
		// 卡片超链
		if (item.attrcode == 'asset_code') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								if (record.values.pk_card_in.value) {
									openAssetCardByPk.call(this, props, record.values.pk_card_in.value);
								} else {
									toast({
										content: getMultiLangByID('2012003508A-000001'),
										color: 'warning'
									}); /*国际化处理：'未查到符合条件的数据，请确认要联查的信息'*/
								}
							}}
						>
							{record && record.values.asset_code && record.values.asset_code.value}
						</span>
					</div>
				);
			};
		}
		// 增加方式只能选择末级节点
		if (item.attrcode === 'pk_addreducestyle') {
			item.onlyLeafCanSelect = true;
		}
	});
}
