import buttonClick, {
	setStatus,
	saveAction,
	getDataByPk,
	setBtnStatusByBillStatus,
	backToList,
	commitAction,
	getDeployInMsg
} from './buttonClick';
import initTemplate from './initTemplate';
import { headAfterEvent, bodyAfterEvent } from './afterEvent';
import beforeEvent from './beforeEvent';
import pageInfoClick from './pageInfoClick';
import tableButtonClick from './tableButtonClick';
export {
	buttonClick,
	beforeEvent,
	headAfterEvent,
	bodyAfterEvent,
	initTemplate,
	pageInfoClick,
	tableButtonClick,
	saveAction,
	getDataByPk,
	setStatus,
	setBtnStatusByBillStatus,
	backToList,
	commitAction,
	getDeployInMsg
};
