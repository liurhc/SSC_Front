import React, { Component } from 'react';
import { createPage, base, high, createPageIcon } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	doubleClick,
	rowSelected,
	commitAction,
	setBatchBtnsEnable,
	searchAfterEvent
} from './events';
import { staticVariable } from './const';
const { ApproveDetail } = high;
const { NCAffix } = base;
const { searchId, tableId, dataSource, title, field } = staticVariable;
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans } = components;

/**
 * 资产调入
 * wangwhf
 * 2018/08/09
 */
class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// 审批详情
			pk_bill: this.props.getUrlParam('id'),
			showApprove: false,
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}

	render() {
		let { editTable, table, button, search, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						dataSource: dataSource,
						onAfterEvent: searchAfterEvent.bind(this)
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						handlePageInfoChange: pageInfoClick.bind(this),
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						showIndex: true,
						showCheck: true,
						dataSource: dataSource,
						pkname: field,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				{createModal('Delete', {})}
				{/* 附件*/}
				{createNCUploader('uploader', {})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} // 国际化：指派
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={(content) => {
							commitAction.call(this, 'SAVE', this.props, content); //原提交方法添加参数content
						}}
						cancel={() => {
							this.setState({
								compositedisplay: false
							});
						}}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

List = createPage({})(List);

export default List;
