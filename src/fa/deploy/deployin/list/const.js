import { conmmonConst } from '../const';
// 节点固定变量数据值
export const staticVariable = {
	...conmmonConst,
	pagecode: '201203508A_list', //单据模板pageID
	searchId: 'searchArea', //查询区域编码
	tableId: 'list_head', //列表表体编码
	node_code: '2012052030',
	// 打印模板所在NC节点及标识
	printFuncode: '2012052030',
	printNodekey: null,
	cardRouter: '/card'
};
// 过滤参数
export const defaultConfig = {
	searchId: staticVariable.searchId,
	specialFields: {
		deployer: {
			// 经办人
			data: [
				{
					fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
					returnName: 'pk_org' //被过滤字段名后台需要参数
				}
			]
		}
	},
	linkageData: {
		//联动链
		//参照字段:['被过滤字段1'，'被过滤字段2']
		pk_org: [
			'deployer', //经办人
			'bodyvos.pk_card_in', // 卡片编号
			'bodyvos.pk_category', //资产类别
			'bodyvos.usedept', //使用部门
			'bodyvos.pk_mandept' //管理部门
		]
	}
};
// ajax请求链接
export const reqUrl = {
	insertUrl: '/nccloud/fa/deployin/insert.do',
	updateUrl: '/nccloud/fa/deployin/update.do',
	listQueryUrl: '/nccloud/fa/deployin/listquery.do',
	cardQueryUrl: '/nccloud/fa/deployin/cardquery.do',
	deleteUrl: '/nccloud/fa/deployin/delete.do',
	commitUrl: '/nccloud/fa/deployin/commit.do',
	pageDataByPksUrl: '/nccloud/fa/deployin/querypagegridbypks.do', // 分页查询
	printUrl: '/nccloud/fa/deployin/printCard.do',
	edit: '/nccloud/fa/deployin/edit.do'
};
