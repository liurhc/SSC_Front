import { ajax } from 'nc-lightapp-front';
import { staticVariable, reqUrl } from '../const';
import { setBatchBtnsEnable } from './buttonClick';
const { pagecode, searchId, tableId, billtype } = staticVariable;
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(searchAreaId);
	}
	let pageInfo = props.table.getTablePageInfo(tableId);
	let data = queryInfo;
	if (data) {
		data.pageInfo = pageInfo;
		data.pagecode = pagecode;
		data.billtype = billtype;
		data.transtype = this.state.transi_type;
	}
	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	ajax({
		url: reqUrl.listQueryUrl,
		data: data,
		success: (res) => {
			let { success } = res;
			if (success) {
				setListValue.call(
					this,
					props,
					res,
					tableId,
					isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
				);
				setBatchBtnsEnable.call(this, props, tableId);
			}
		}
	});
}
