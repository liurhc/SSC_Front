import { tableButtonClick } from '../events';
import { setBatchBtnsEnable, linkToCard, toDeployOut } from './buttonClick';
import { staticVariable, defaultConfig } from '../const';
const { pagecode, tableId } = staticVariable;
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { createOprationColumn } = listUtils;
const { getContext, loginContextKeys, loginContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					//获取登录上下文信息
					loginContext.call(this, data.context);
					//设置交易类型
					let transi_type = getContext(loginContextKeys.transtype);
					this.setState({ transi_type });
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	// 参照条件过滤
	addSearchAreaReferFilter(props, meta, defaultConfig);
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td">
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, 'browse');
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		if (item.attrcode == 'bill_code_out') {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td">
						<span
							className="code-detail-link"
							onClick={() => {
								toDeployOut.call(this, props, record);
							}}
						>
							{record && record.bill_code_out && record.bill_code_out.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
	//添加操作列
	let oprCol = createOprationColumn.call(this, props, {
		tableId: tableId,
		tableButtonClick: tableButtonClick
	});
	meta[tableId].items.push(oprCol);
	return meta;
}
