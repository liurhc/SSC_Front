import { ajax, toast } from 'nc-lightapp-front';
import { staticVariable, reqUrl } from '../const';
import { linkToCard } from './buttonClick';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ScriptReturnUtils } = components;
const { UISTATE } = StatusUtils;
const { getScriptListReturn } = ScriptReturnUtils;
const { pagecode, tableId, field, dataSource } = staticVariable;
const { commitUrl } = reqUrl;

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Edit':
			edit.call(this, props, record, index);
			break;
		case 'Delete':
			delConfirm.call(this, props, record, index);
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', props, tableId, index, record);
			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', props, tableId, index, record);
			break;
		case 'QueryAboutBillFlow':
			this.setState({
				showApprove: true,
				pk_bill: record.pk_deployin.value,
				transi_type: record.transi_type.value
			});
			break;
		default:
			break;
	}
}
/**
 * 修改
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function edit(props, record, index) {
	ajax({
		url: reqUrl.edit,
		data: {
			pk: record.pk_deployin.value,
			resourceCode: staticVariable.node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'danger', content: res.data });
			} else {
				linkToCard.call(this, props, record, UISTATE.edit);
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
/**
 * 删除
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function delConfirm(props, record, index) {
	let id = record[field].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};
	ajax({
		url: commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			getScriptListReturn(params, res, props, field, tableId, tableId, true);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
/**
 * 提交 收回 动作
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 * @param {*} record 
 */
function commitClick(OperatorType, props, tableId, index, record) {
	let business_date = record['business_date'].value;
	if (!business_date) {
		toast({ content: getMultiLangByID('2012003508A-000000'), color: 'danger' }); // '业务日期不能为空'
		return;
	}

	let id = record[field].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecode,
			commitType
		},
		success: (res) => {
			if (res.success) {
				getScriptListReturn.call(this, params, res, props, field, tableId, tableId, false, dataSource);
			} else {
				toast({ content: res.data.errorMsg, color: 'danger' });
			}
		}
	});
}
