//资产评估单列表
import React, { Component } from 'react';
import { createPage, ajax, base, high, createPageIcon } from 'nc-lightapp-front';
import {
	CommitAction,
	rowSelected,
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	doubleClick,
	setBatchBtnsEnable,
	afterEvent
} from './events';
import { pagecode, searchId, moduleId, tableId, appid, dataSource, title } from './constants';
import { pkField } from '../card/constants';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { queryVocherSrcUtils, ApprovalTrans } = components;
const { queryVoucherSrc } = queryVocherSrcUtils;

const { NCAffix } = base;
const { BillTrack, ApproveDetail } = high;
class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showApprove: false,
			show: false,
			bill_type: '',
			pk_evaluate: '',
			/* 提交指派 */
			compositedisplay: false,
			compositedata: {},
			pk_bill: '',
			transi_type: ''
		};
		initTemplate.call(this, props);
	}
	componentDidMount() {
		//凭证联查来源单据
		queryVoucherSrc(this.props, tableId, pkField, pagecode);
	}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		CommitAction.call(this, 'SAVE', this.props, value);
	};
	//取消 指派
	turnOff = () => {
		this.props.table.selectAllRows(tableId, false);
		rowSelected.call(this, this.props, tableId);
		this.setState({
			compositedisplay: false
		});
	};

	//渲染界面
	render() {
		let { table, search, ncmodal, ncUploader } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createModal } = ncmodal;
		let { createNCUploader } = ncUploader;

		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{title}</h2>
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>

					<div className="nc-bill-search-area">
						{NCCreateSearch(searchId, {
							onAfterEvent: afterEvent.bind(this),
							clickSearchBtn: searchBtnClick.bind(this),
							dataSource: dataSource
						})}
					</div>
				</NCAffix>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						showIndex: true,
						showCheck: true,
						handlePageInfoChange: pageInfoClick.bind(this),
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						pkname: pkField,
						dataSource: dataSource,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 单据追溯 */}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_evaluate} //单据id
					type={this.state.bill_type} //单据类型
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('201202504A-000019') /* 国际化处理： 指派*/}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{/* 附件*/}
				{createNCUploader('uploader', {})}
				{createModal('Delete', {})}
			</div>
		);
	}
}

// List = createPage({
// 	// initTemplate: initTemplate,
// })(List);
//虚拟DOM渲染界面
// ReactDOM.render(<List />, document.querySelector('#app'));
const MasterChildListBase = createPage({})(List);
export default MasterChildListBase;
