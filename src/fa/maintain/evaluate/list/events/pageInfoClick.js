//表体页面分页处理
import { ajax } from 'nc-lightapp-front';
import { pagecode, tableId, url } from '../constants';
import { setBatchBtnsEnable } from './buttonClick';

import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;
/**
 * 分页处理
 * @param {*} props 
 */
export default (props, config, pks) => {
	let data = {
		allpks: pks,
		pagecode
	};
	//得到数据渲染到页面
	ajax({
		url: url.queryPageUrl,
		data,
		success: (res) => {
			setListValue.call(this, props, res);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
};
