//加载初始模板
import { pagecode, dataSource, tableId, searchId, ds, defaultConfig } from '../constants';
import { tableButtonClick } from '../events';
import { linkToCard, setBatchBtnsEnable } from './buttonClick';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createOprationColumn } = listUtils;
const { LoginContext } = components;
const { getContext, loginContext } = LoginContext;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter, AccbookAndPeriod } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;
const { getAccbookAndPeriod } = AccbookAndPeriod;
export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, dataSource);
					// 获取数据源
					getContext(ds, dataSource);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	setBatchBtnsEnable.call(this, props, tableId);
	getAccbookAndPeriod.call(this, props, searchId);
}

//修饰数据
function modifierMeta(props, meta) {
	addSearchAreaReferFilter(props, meta, defaultConfig);
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					// TODO
					<div
						class="simple-table-td"
						field="bill_code"
						fieldname={getMultiLangByID('201202504A-000038') /* 国际化处理： 评估单号*/}
					>
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, 'browse');
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});

	//添加操作列
	let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
	meta[tableId].items.push(oprCol);

	return meta;
}
