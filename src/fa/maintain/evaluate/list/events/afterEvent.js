import { defaultConfig } from '../constants';

import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage, AccbookAndPeriod } = fa_components;
const { referLinkageClear } = ReferLinkage;
const { getAccbookAndPeriod } = AccbookAndPeriod;

/**
 * 查询区编辑后事件
 * @param {*} key 
 * @param {*} value 
 */
export default function afterEvent(key, value) {
	if (key == 'pk_org') {
		referLinkageClear.call(this, this.props, key, defaultConfig);
		//根据财务组织加载默认账簿
		getAccbookAndPeriod.call(this, this.props, defaultConfig.searchId);
	}
}
