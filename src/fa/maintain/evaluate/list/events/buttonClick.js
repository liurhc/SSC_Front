//按钮点击事件处理
import { ajax, toast, print, output } from 'nc-lightapp-front';
import {
	printFilename,
	cardRouter,
	tableId,
	formId,
	pagecode,
	pkField,
	appcode,
	pagecodecard,
	url,
	uploaderPath,
	dataSource
} from '../constants';
import searchBtnClick from './searchBtnClick';

import ampub from 'ampub';
const { utils, components } = ampub;
const { msgUtils, listUtils } = utils;
const { batchRefresh, setBillFlowBtnsEnable } = listUtils;
const { ScriptReturnUtils, queryAboutUtils, queryVocherUtils } = components;
const { openListBillTrack } = queryAboutUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { getScriptListReturn } = ScriptReturnUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
export default function buttonClick(props, id) {
	//按钮事件处理
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Commit':
			CommitAction.call(this, 'SAVE', props);
			break;
		case 'UnCommit':
			CommitAction.call(this, 'UNSAVE', props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'QueryAbout':
			QueryAbout.call(this, props);
			break;
		case 'QueryAboutVoucher':
			queryAboutVoucher(props, tableId, 'pk_evaluate', appcode, true); // tableId为区域编码，第三个参数为主键编码，appcode为应用编码,新增参数，标志列表态联查
			break;
		case 'QueryAboutBillFlow':
			openListBillTrack(this, props, tableId, pkField);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		default:
			// console.log(props, id, '不包含此按钮处理');
			break;
	}
}
/**
 * 新增
 * @param {*} props 
 */
export function add(props) {
	linkToCard.call(this, props, undefined, 'add');
}

// 删除
export function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: delReal });
}
// 删除具体执行
function delReal(props) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_evaluate.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, pkField, formId, tableId, true, dataSource);
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}
// 提交
export function CommitAction(OperatorType, props, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	// 去掉此过滤	提交或收回分别需过滤出不符合其操作的数据
	let params = data.map((v) => {
		let id = v.data.values.pk_evaluate.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecodecard,
			commitType: commitType,
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				false,
				dataSource
			);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}
/**
* 获取打印数据
* @param {*} props 
* @param {*} outputType 
*/
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[pkField].value);
	});
	let printData = {
		filename: printFilename, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	if (!checkedrows || checkedrows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOne });
		return;
	}
	// billNo 是单据主键
	let billId = checkedrows[0].data.values[pkField].value;
	props.ncUploader.show('uploader', {
		billId: uploaderPath + billId
	});
}
/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	props.pushTo(cardRouter, {
		status,
		id: record[pkField] ? record[pkField].value : '',
		pagecode: pagecode
	});
}
/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
}
/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	batchRefresh.call(this, props, searchBtnClick);
}
