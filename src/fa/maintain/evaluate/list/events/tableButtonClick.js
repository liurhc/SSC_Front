//表体行操作列处理
import { ajax, toast } from 'nc-lightapp-front';
import { tableId, pkField, url, resourceCode, pagecodecard, dataSource } from '../constants';
import { linkToCard } from './buttonClick';

import ampub from 'ampub';
const { components, commonConst } = ampub;
const { ScriptReturnUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Edit':
			edit.call(this, props, record, index);
			break;
		case 'Delete':
			delConfirm.call(this, props, record, index);
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', props, tableId, index, record);

			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', props, tableId, index, record);

			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, record);
			break;
		default:
			// console.log(key, index, '不包含此按钮处理');
			break;
	}
}
/* 审批详情 */
function queryAboutBillFlow(record) {
	let pk = record.pk_evaluate.value;
	this.setState({
		pk_bill: pk,
		showApprove: true,
		transi_type: record.transi_type.value
	});
}
/**
 * 表格行修改
 * @param {*} props 
 * @param {*} record 
 */
function edit(props, record) {
	let pk = record[pkField].value;
	let data = {
		pk,
		resourceCode
	};
	ajax({
		url: url.editUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					toast({ color: 'warning', content: data });
				} else {
					linkToCard.call(this, props, record, UISTATE.edit);
				}
			}
		}
	});
}

/* 提交 收回 */
function commitClick(OperatorType, props, tableId, index, record) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_evaluate.value] = record.ts.value;
	let params = [ { id: record.pk_evaluate.value, index: index } ];
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecodecard,
			commitType: commitType
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				false,
				dataSource
			);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
//确认删除
function delConfirm(props, record, index) {
	let id = record[pkField].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};

	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecodecard,
			commitType: 'commit' //提交
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				true,
				dataSource
			);
			// 清除缓存
			props.table.deleteCacheId(tableId, id);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}
