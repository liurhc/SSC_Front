import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export const pagecode = '201202504A_list'; //单据模板pageID
export const pagecodecard = '201202504A_card'; //卡片单据模板pageID
export const filename = '201202504A'; //文件名称
export const moduleId = '2012'; //所属模块编码
export const searchId = 'searchArea'; //查询区域编码
export const tableId = 'list_head'; //列表表体编码
export const formId = 'card_head'; //卡片表头编码
export const appid = '0001Z91000000000177M'; //小应用id
export const appcode = '201202504A'; //小应用编码
export const oid = '1001Z91000000000C69T'; // 查询模板id
export const dataSource = 'fa.maintain.evaluate.main'; // 缓存
export const pkField = 'pk_evaluate'; // 评估单主键
export const batchBtns = [ 'Delete', 'Commit', 'UnCommit', 'Attachment', 'FAReceipt', 'QueryAbout', 'Print', 'Output' ]; // 进入单据隐藏的按钮
export const bill_type = 'HE'; // 单据类型
export const cardRouter = '/card'; // 卡片路由;
export const resourceCode = '2012032010'; // 资源编码;
export const printFilename = getMultiLangByID('201202504A-000000')/* 国际化处理： 资产评估*/; // 输出文件名称;
export const title = getMultiLangByID('201202504A-000000')/* 国际化处理： 资产评估*/; // 节点名称;
export const ds = 'dataSourceCode';
export const uploaderPath = 'fa/evaluate/';

export const url = {
	queryPageUrl: '/nccloud/fa/evaluate/querypagelistbypks.do', // 分页查询
	queryUrl: '/nccloud/fa/evaluate/query.do', // 列表查询
	editUrl: '/nccloud/fa/evaluate/edit.do', //编辑
	commitUrl: '/nccloud/fa/evaluate/commit.do', //提交、收回
	deleteUrl: '/nccloud/fa/evaluate/delete.do', //删除
	printUrl: '/nccloud/fa/evaluate/printCard.do' //打印
};

export const defaultConfig = {
	searchId: 'searchArea',
	formId: '',
	bodyIds: [],
	fieldMap: {
		pk_org: 'pk_org', //财务组织
		pk_accbook: 'pk_accbook', //账簿
		pk_mandept: 'pk_mandept', //管理部门
		//或者 pk_usedept: 'pk_usedept'
		pk_usedept: [ 'pk_usedept', 'pk_dept' ], //使用部门
		pk_assetuser: 'pk_assetuser', //使用人
		pk_recorder: 'pk_recorder', //经办人
		pk_ownerorg: 'pk_ownerorg' //货主管理组织
	},
	specialFields: {
		'bodyvos.pk_card' : {
			orgMulti: 'pk_org', //添加左上角主组织参照
			data: [
				{
					fields: [ 'pk_org', 'pk_org_v' ], //可被多个字段过滤 取第一个有的值
					returnName: 'pk_org' //被过滤字段名后台需要参数
				}
			]
		}
	},
	linkageData: {
		//联动链
		//参照字段:['被过滤字段1'，'被过滤字段2']
		pk_org: [
			'pk_accbook',
			'pk_card',
			'bodyvos.pk_card',
			'bodyvos.pk_card.pk_mandept',//管理部门
			'bodyvos.pk_card.pk_usedept',//使用部门
			'bodyvos.pk_card.pk_category'//资产类别
		]
	},
	defPrefix: {
		search: [ 'bodyvos.def', 'def', 'bodyvos.pk_card.def' ],
		head: 'def',
		body: 'def'
	}
};
