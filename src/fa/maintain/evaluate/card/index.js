//资产评估卡片态
import React, { Component } from 'react';
import { createPage, ajax, base, toast, high } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	pageInfoClick,
	getDataByPk,
	backToList,
	save,
	rowSelected,
	lockcards,
	commit,
	headAfterEvent,
	bodyAfterEvent,
	beforeEvent,
	lockrequest
} from './events';
import { url, formId, title, tableId, pagecode, dataSource, ds } from './constants';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;
const { NCAffix } = base;
const { FormulaEditor, ApproveDetail } = high;

class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			name: getMultiLangByID('201202504A-000011') /* 国际化处理： 公式页面*/,
			//公式处理的值
			set_field: '',
			row_number: '',
			signature: 'default',
			select_row: '',
			/* 提交指派 */
			compositedisplay: false,
			compositedata: {},
			showApprove: false,
			transi_type: '',
			pk_bill: props.getUrlParam('id'),
			// 当前用户
			usrid: ''
		};
		initTemplate.call(this, props);
	}

	componentWillMount() {
		window.onbeforeunload = () => {
			// 解锁卡片
			let status = this.props.cardTable.getStatus(tableId);
			if (status == 'edit') {
				lockcards.call(this, this.props, 'unlock', false);
			}
		};
		if (window.history && window.history.pushState) {
			window.onpopstate = () => {
				lockcards.call(this, this.props, 'unlock', false);
			};
		}
	}
	// 移除事件
	componentWillUnmount() {
		window.onbeforeunload = null;
	}
	//查询单据详情
	componentDidMount() {
		let status = this.props.getUrlParam('status') || UISTATE.browse;
		if (status != UISTATE.add) {
			let pk = this.props.getUrlParam('id');
			if (pk && pk != 'undefined') {
				getDataByPk.call(this, this.props, pk);
			}
		}
	}

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;

		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: (props, id) => {
							buttonClick.call(this, props, id, tableId);
						},
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	//提交及指派 回调
	getAssginUsedr = (value) => {
		let status = this.props.form.getFormStatus(formId) || UISTATE.browse;
		if (status == UISTATE.browse) {
			commit.call(this, this.props, 'SAVE', 'commit', value);
		} else {
			commit.call(this, this.props, 'SAVE', 'saveCommit', value);
		}
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};
	FormulaClos = ({ setName, setExplain, name }) => {
		return (
			<ul class="tab-content">
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202504A-000013') /* 国际化处理： 使用月限*/);
					}}
					onDoubleClick={() => {
						setName('old_servicemonth');
					}}
				>
					{getMultiLangByID('201202504A-000013') /* 国际化处理： 使用月限*/}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202504A-000014') /* 国际化处理： 净值*/);
					}}
					onDoubleClick={() => {
						setName('old_netvalue');
					}}
				>
					{getMultiLangByID('201202504A-000014') /* 国际化处理： 净值*/}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202504A-000015') /* 国际化处理： 净残值率*/);
					}}
					onDoubleClick={() => {
						setName('old_salvagerate');
					}}
				>
					{getMultiLangByID('201202504A-000015') /* 国际化处理： 净残值率*/}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202504A-000016') /* 国际化处理： 工作总量*/);
					}}
					onDoubleClick={() => {
						setName('old_allworkloan');
					}}
				>
					{getMultiLangByID('201202504A-000016') /* 国际化处理： 工作总量*/}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202504A-000017') /* 国际化处理： 本币原值*/);
					}}
					onDoubleClick={() => {
						setName('old_localoriginvalue');
					}}
				>
					{getMultiLangByID('201202504A-000017') /* 国际化处理： 本币原值*/}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202504A-000018') /* 国际化处理： 累计折旧*/);
					}}
					onDoubleClick={() => {
						setName('old_accudep');
					}}
				>
					{getMultiLangByID('201202504A-000018') /* 国际化处理： 累计折旧*/}
				</li>
			</ul>
		);
	};

	formulaAfter = (res) => {
		this.setState({ show: false });
	};

	// 侧拉删行前解锁
	modelDelRowBefore = (props, moduleId, moduleIndex, row) => {
		let allpks = [ row.values.pk_card.value ];
		let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
		let usrid = getContext('userId', dataSource);
		let param = {
			allpks,
			msgMap: {
				usrid,
				lockflag: 'unlock',
				dSource
			}
		};
		lockrequest.call(this, param);
		return true;
	};

	//渲染界面
	render() {
		let { button, cardTable, form, ncmodal, ncUploader } = this.props;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createModal } = ncmodal;
		let { createButtonApp } = button;
		let { createNCUploader } = ncUploader;

		return (
			<div id="nc-bill-card" className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						{/* 头部 header */}
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title,
								formId,
								backBtnClick: backToList
							})}
							{/* 按钮区 btn-area */}
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-table-area">
					<div className="nc-bill-tableTab-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: save.bind(this),
							onBeforeEvent: beforeEvent.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							showIndex: true,
							showCheck: true,
							selectedChange: rowSelected.bind(this),
							modelDelRowBefore: this.modelDelRowBefore.bind(this)
						})}
					</div>
				</div>
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}

				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('201202504A-000019') /* 国际化处理： 指派*/}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				<FormulaEditor
					zIndex={300}
					ref="formulaEditor"
					show={this.state.show}
					// show={false}
					// attrConfig={[
					// 	{ tab: '卡片项目', TabPaneContent: this.FormulaClos, params: { name: this.state.name } }
					// ]}
					formulaConfig={[
						{
							tab: getMultiLangByID('201202504A-000020') /* 国际化处理： 公式字段*/,
							TabPaneContent: this.FormulaClos,
							params: {}
						}
					]}
					noShowFormula={[
						getMultiLangByID('201202504A-000021') /* 国际化处理： 其它类型*/,
						getMultiLangByID('201202504A-000022') /* 国际化处理： 日期*/,
						getMultiLangByID('201202504A-000023') /* 国际化处理： 数字*/,
						getMultiLangByID('201202504A-000024') /* 国际化处理： 自定义*/,
						getMultiLangByID('201202504A-000025') /* 国际化处理： 数学*/,
						getMultiLangByID('201202504A-000026') /* 国际化处理： 字符串*/,
						getMultiLangByID('201202504A-000027') /* 国际化处理： 数据库*/,
						getMultiLangByID('201202504A-000028') /* 国际化处理： 财务*/,
						getMultiLangByID('201202504A-000029') /* 国际化处理： 常用*/
					]}
					noShowAttr={[ 'all' ]}
					onOk={(res) => {
						// 获取billcard
						let card_data = this.props.createMasterChildData(pagecode, formId, tableId);
						// 公式操作
						if (this.state.signature == '0') {
							card_data.body.bodyvos.rows = [];
							card_data.body.bodyvos.rows.push(this.state.select_row);
						}
						//判断表体是否有数据
						if (card_data.body.bodyvos.rows && card_data.body.bodyvos.rows.length > 0) {
							ajax({
								url: url.evaluateFormula,
								data: {
									card: card_data,
									set_field: this.state.set_field,
									row_number: this.state.row_number,
									input_formula: res
								},
								success: (res) => {
									if (res.success) {
										if (res.data) {
											// 公式
											if (this.state.signature == '0') {
												//行号
												let row_number = this.state.row_number;
												this.props.cardTable.updateDataByIndexs(tableId, [
													{
														index: row_number,
														data: res.data.body.bodyvos.rows[0]
													}
												]);
												this.setState({ signature: 'default' });
												// 批量公式
											} else if (this.state.signature == '1') {
												this.props.cardTable.setTableData(tableId, res.data.body.bodyvos);
												this.setState({ signature: 'default' });
											}
										}
									}
								}
							});
						}

						this.formulaAfter(res);
					}} //点击确定回调
					onCancel={(a) => {
						this.setState({ show: false, signature: 'default' });
					}} //点击确定回调
					onHide={(a) => {
						this.setState({ show: false, signature: 'default' });
					}}
				/>
				{/* 附件*/}
				{createNCUploader('uploader', {})}

				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
			</div>
		);
	}
}

const MasterChildCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode, // 页面编码
		headcode: formId, // 表头编码
		bodycode: tableId //表体编码
	}
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
export default MasterChildCardBase;
