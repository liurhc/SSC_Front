//编辑后事件及表体联动
import { ajax, toast } from 'nc-lightapp-front';
import { formId, tableId, pagecode, url } from '../constants';
import { setValue, addLine, delLine } from './buttonClick';

import ampub from 'ampub';
const { components } = ampub;
const { OrgChangeEvent } = components;
const { orgChangeEvent } = OrgChangeEvent;

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	// createHeadAfterEventData 初始化为后台平台可以使用的格式
	let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	if (key == 'pk_org_v') {
		// 组织切换确定后发送后台请求
		let callback = () => {
			headAfterEditAjax.call(this, props, data, value);
		};
		// 组织切换
		orgChangeEvent.call(this, props, pagecode, formId, tableId, key, value, oldValue, false, false, callback);
	}
	//账簿编辑后清空表体数据
	if (key == 'pk_accbook') {
		let rowNum = props.cardTable.getNumberOfRows(tableId, false);
		if (rowNum > 0) {
			props.cardTable.selectAllRows(tableId, true);
			delLine.call(this, props);
		}
		defaultAddLine.call(this, props);
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedrows, i, record) {
	//选择资产卡片后操作
	if (key === 'pk_card') {
		pkCardAfterEditEvent.call(this, props, moduleId, key, value, changedrows, i);
	}

	// 字段联动处理并解决精度问题
	if (
		key === 'new_localoriginvalue' ||
		key === 'new_accudep' ||
		key === 'new_servicemonth' ||
		key === 'new_salvage' ||
		key === 'new_salvagerate' ||
		key === 'new_allworkloan' ||
		key === 'new_naturemonth'
	) {
		afterEditEvent.call(this, props, moduleId, key, value, changedrows, i, record);
	}
}

// 卡片编辑后事件
function pkCardAfterEditEvent(props, moduleId, key, value, changedrows, i) {
	//所选数据为空不需要走编辑后事件
	if (
		changedrows &&
		changedrows.length == 1 &&
		changedrows[0].oldvalue.value == null &&
		changedrows[0].oldvalue.value == changedrows[0].newvalue.value
	) {
		return;
	}

	let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedrows);
	data.index = i;
	ajax({
		url: url.bodyEditUrl,
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let bodyrows = data.body[tableId].rows;
				for (let j = 0; j < bodyrows.length; j++) {
					if (!bodyrows[j].values.pk_card.value) {
						bodyrows[j].values.new_netvalue.value = null;
						bodyrows[j].values.old_netvalue.value = null;
					}
				}
				setValue.call(this, props, data);
				let num = props.cardTable.getNumberOfRows(tableId);
				//如果为最后一行 , 自动增行
				if (num == i + 1) {
					addLine.call(this, props);
				}
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			//清行数据
			props.cardTable.delRowsByIndex(tableId, i);
			props.cardTable.addRow(tableId, i, {}, false);
		}
	});
}
// 数值字段编辑后事件处理
function afterEditEvent(props, moduleId, key, value, changedrows, i, record) {
	if (changedrows[0].newvalue.value == changedrows[0].oldvalue.value) {
		this.state.set_field = key;
		this.state.row_number = i;
	} else {
		basebodyaftereditevent.call(this, props, moduleId, key, value, changedrows, i, record);
	}
}
// 表体编辑后事件处理
function basebodyaftereditevent(props, moduleId, key, value, changedrows, i, record) {
	let cardData = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedrows, i);

	//新增功能---差异化处理
	let newRecord = JSON.parse(JSON.stringify(record || {}));
	cardData.card.body[moduleId] = {
		...cardData.card.body[moduleId],
		rows: [ newRecord ]
	};
	cardData.index = 0; //修改编辑行为0

	ajax({
		url: url.bodyEditUrl,
		data: cardData,
		async: false,
		success: (res) => {
			if (res.data && res.data.body && res.data.body[tableId]) {
				//新增功能---差异化处理
				props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
			}
			let userObj = JSON.parse(res.data.userjson);
			if (userObj && userObj.alterMsg) {
				toast({ color: 'warning', content: userObj.alterMsg });
			}
			if (userObj && userObj.errMsg) {
				toast({ color: 'danger', content: userObj.errMsg });
			}
		},
		error: (res) => {
			//TODO 清空资产类别
			Promise.resolve(true).then(() => {
				props.cardTable.setValByKeyAndIndex(moduleId, i, key, { value: null, display: null });
			});
		}
	});
}
/**
 * 调后台处理编辑后事件的数据
 * @param {*} props 
 * @param {*} data 
 */
function headAfterEditAjax(props, data, value) {
	ajax({
		url: url.headEditUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				setValue.call(this, props, data);
				if (value.value) {
					defaultAddLine.call(this, props);
				}
			}
		}
	});
}
//编辑后事件后如果表体被清空默认增行
function defaultAddLine(props) {
	let allRows = props.cardTable.getNumberOfRows(tableId, false);
	let rowCount = 0;
	if (allRows) {
		rowCount = allRows;
	}
	if (rowCount == 0) {
		addLine.call(this, props);
	}
}
