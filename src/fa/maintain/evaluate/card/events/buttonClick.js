//按钮点击事件处理
import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';
import {
	pkField,
	dataSource,
	url,
	formId,
	tableId,
	pagecode,
	editButton,
	browseButton,
	listButton,
	appcode,
	printFilename,
	listRouter,
	resourceCode,
	bill_type,
	ds,
	uploaderPath,
	canEditFields
} from '../constants';

import { headAfterEvent } from './afterEvent';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = cardUtils;
const { LoginContext, ScriptReturnUtils, queryVocherUtils } = components;
const { loginContextKeys } = LoginContext;
const { queryAboutVoucher } = queryVocherUtils;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getContext } = LoginContext;
const { UISTATE, BILLSTATUS } = StatusUtils;
import fa from 'fa';
const { fa_components } = fa;
const { ImageMng } = fa_components;
const { faImageScan, faImageView } = ImageMng;

export default function(props, id) {
	switch (id) {
		// 新增
		case 'Add':
			add.call(this, props);
			break;
		// 保存
		case 'Save':
			save.call(this, props);
			break;
		// 编辑
		case 'Edit':
			edit.call(this, props);
			break;
		// 删除
		case 'Delete':
			delConfirm.call(this, props);
			break;
		// 取消
		case 'Cancel':
			cancel.call(this, props);
			break;
		// 提交
		case 'Commit':
			commit.call(this, props, 'SAVE', 'commit');
			break;
		// 保存提交
		case 'SaveCommit':
			commit.call(this, props, 'SAVE', 'saveCommit');
			break;
		// 收回
		case 'UnCommit':
			commit.call(this, props, 'UNSAVE', 'commit');
			break;
		//增行
		case 'AddLine':
			addLine.call(this, props, true);
			break;
		//表体删行
		case 'DelLine':
			delLine.call(this, props);
			break;
		//肩部公式
		case 'BatchFormula':
			batchFormula.call(this, props);
			break;
		// 打印
		case 'Print':
			printTemp.call(this, props);
			break;
		// 输出
		case 'Output':
			outputTemp.call(this, props);
			break;
		// 影像扫描
		case 'FAReceiptScan':
			FAReceiptScan.call(this, props);
			break;
		// 影像查看
		case 'FAReceiptShow':
			FAReceiptShow.call(this, props);
			break;
		// 附件
		case 'Attachment':
			attachment.call(this, props);
			break;
		// 联查凭证
		case 'QueryAboutVoucher':
			queryAboutVoucher(props, formId, 'pk_evaluate', appcode); // tableId为区域编码，第三个参数为主键编码，appcode为应用编码
			break;
		// 刷新
		case 'Refresh':
			refresh.call(this, props);
			break;
		// 批改
		case 'BatchAlter':
			BatchAltert.call(this, props);
			break;
		// 审批详情
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props);
			break;
		default:
			break;
	}
}
// 编辑
function edit(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		pk = props.getUrlParam('id');
	}
	if (!pk) {
		return;
	}
	props.cardTable.closeExpandedRow(tableId);

	let data = {
		pk,
		resourceCode
	};
	ajax({
		url: url.editURl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					toast({ color: 'warning', content: data });
				} else {
					props.cardTable.closeExpandedRow(tableId);
					setStatus.call(this, props, UISTATE.edit);
				}
			}
		}
	});
}
/**
* 删除
* @param {*} props 
*/
export function delConfirm(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delReal });
}
/**
 * 删除
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function delReal(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'DELETE',
		commitType: 'commit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: CardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data.success === 'allsuccess') {
					// 清除缓存
					cardCache.deleteCacheById(pkField, pk, dataSource);
					showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
					// 加载下一条数据
					let newpk = cardCache.getNextId(pk, dataSource);
					loadDataByPk.call(this, props, newpk);
				} else {
					toast({ content: data.errorMsg, color: 'danger' });
				}
			}
		}
	});
}
// 保存
export function save(props) {
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let reqUrl = url.saveUrl; //新增保存
	let oldstatus = props.form.getFormStatus(formId);
	if (oldstatus == UISTATE.edit) {
		reqUrl = url.updateUrl; //修改保存
	}
	// 保存前执行验证公式
	props.validateToSave(cardData, () => {
		ajax({
			url: reqUrl,
			data: cardData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					afterSave.call(this, props, data, oldstatus);
				}
			}
		});
	});
}
//提交
export function commit(props, OperatorType, commitType, content) {
	if (commitType === 'saveCommit') {
		let flag = validateBeforeSave.call(this, props);
		if (!flag) {
			return;
		}
	}
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);

	if (content) {
		// 调用Ajax保存数据
		ajax({
			url: url.commitUrl,
			data: CardData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					this.setState({
						compositedisplay: false
					});
					let callback = () => {
						setStatus.call(this, props, UISTATE.browse);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkField,
						dataSource,
						null,
						false,
						callback,
						pagecode
					);
				}
			}
		});
	} else {
		// 保存前执行验证公式
		props.validateToSave(CardData, () => {
			// 调用Ajax保存数据
			ajax({
				url: url.commitUrl,
				data: CardData,
				success: (res) => {
					let callback = () => {
						setStatus.call(this, props, UISTATE.browse);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkField,
						dataSource,
						null,
						false,
						callback,
						pagecode
					);
				}
			});
		});
	}
}
//切换按钮状态
function toggleShow(props) {
	let status = props.getUrlParam('status');
	let { form } = props;
	form.setFormStatus('head', 'edit');
	// form.setFormStatus('childform2', 'edit');

	//按钮的显示状态
	if (status === 'edit' || status === 'add') {
		//编辑态
		props.button.setButtonVisible(listButton, false);
		props.button.setButtonVisible(browseButton, false);
		props.button.setButtonVisible(editButton, true);
		props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
	} else {
		//浏览态
		props.button.setButtonVisible(browseButton, true);
		props.button.setButtonVisible(editButton, false);
		props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);
	}
	props.form.setFormStatus(formId, status);
	props.cardTable.setStatus(tableId, status);

	//updateTableShoulder(props);
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}
/**
* 获取打印数据
* @param {*} props 
* @param {*} outputType 
*/
function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(formId, pkField);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: printFilename, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}
/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	// 更新参数
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	props.setUrlParam({ status, id: pkVal ? pkVal.value : '' });
	switch (status) {
		case UISTATE.add:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			setBrowseBtnsVisible.call(this, props);
			props.button.setButtonVisible(editButton, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.edit:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			setBrowseBtnsVisible.call(this, props);
			props.button.setButtonVisible(editButton, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editButton, false);
			setBrowseBtnsVisible.call(this, props);
			break;
		default:
			break;
	}
	//获取bill_code
	let bill_code = props.form.getFormItemsValue(formId, 'bill_code').value;
	let data = {
		bill_code,
		status
	};
	setHeadAreaData.call(this, props, data);
}

/**
 * 影像扫描
 * @param {*} props 
 */
function FAReceiptScan(props) {
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageScan(props, imageData);
}
/**
 * 影像查看
 * @param {*} props 
 */
function FAReceiptShow(props) {
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageView(props, imageData);
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	let billId = props.form.getFormItemsValue(formId, 'pk_evaluate').value;
	props.ncUploader.show('uploader', {
		billId: uploaderPath + billId
	});
}
/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk) {
	if (!pk || pk == 'null') {
		setValue.call(this, props, undefined);
		return;
	}
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData) {
		setValue.call(this, props, cachData);
	} else {
		getDataByPk.call(this, props, pk);
	}
}
/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk, callback) {
	ajax({
		url: url.queryCardUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					cardCache.updateCache(pkField, pk, data, formId, dataSource);
				} else {
					setValue.call(this, props, undefined);
					cardCache.deleteCacheById(pkField, pk, dataSource);
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				}
				typeof callback == 'function' && callback(data);
			}
		}
	});
}
/**
* 新增
* @param {*} props 
*/
function add(props) {
	props.cardTable.closeExpandedRow(tableId);
	// 设置新增态
	setStatus.call(this, props, UISTATE.add);
	// 清空数据
	setValue.call(this, props, undefined);
	// 设置默认值
	setDefaultValue.call(this, props);
	// 设置字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (pk_org_v && pk_org_v.value) {
		headAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		setBodyBtnsEnable.call(this, props, tableId);
	}
}
/**
* 新增行
* @param {*} props 
* @param {*} isFocus 是否聚焦
*/
export function addLine(props, isFocus = false) {
	props.cardTable.addRow(tableId, undefined, {}, isFocus);
	setBodyBtnsEnable.call(this, props, tableId);
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	setCardValue.call(this, props, data);
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	if (status == UISTATE.browse) {
		setBrowseBtnsVisible.call(this, props);
	} else {
		setBodyBtnsEnable.call(this, props, tableId);
	}
}
/**
 * 新增设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	// 模板默认值处理

	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId, dataSource);
	let groupName = getContext(loginContextKeys.groupName, dataSource);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype, dataSource);
	let pk_transitype = getContext(loginContextKeys.pk_transtype, dataSource);

	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org, dataSource);
	let org_Name = getContext(loginContextKeys.org_Name, dataSource);
	let pk_org_v = getContext(loginContextKeys.pk_org_v, dataSource);
	let org_v_Name = getContext(loginContextKeys.org_v_Name, dataSource);
	// 账簿
	let AccbookName = getContext(loginContextKeys.defaultAccbookName, dataSource);
	let AccbookPk = getContext(loginContextKeys.defaultAccbookPk, dataSource);
	let businessdate = getContext(loginContextKeys.businessDate, dataSource);

	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype },
		business_date: { value: businessdate },
		pk_accbook: { display: AccbookName, value: AccbookPk },
		bill_status: { display: getMultiLangByID('statusUtils-000000') /* 国际化处理： 自由态*/, value: '0' }
	});
}
/**
* 删除行
* @param {*} props 
*/
export function delLine(props) {
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	let allpks = [];
	checkedRows &&
		checkedRows.map((item) => {
			checkedIndex.push(item.index);
			// 若删除行为新增数据，则记录为需解锁卡片
			if (item.data.values.pk_card && item.data.values.pk_card.value) {
				allpks.push(item.data.values.pk_card.value);
			}
		});
	// 若待解锁卡片数量大于0，则解锁
	let dSource = getContext(ds, dataSource);
	let usrid = getContext('userId', dataSource);
	if (allpks.length > 0) {
		let param = {
			id: 'unlock',
			allpks,
			msgMap: {
				usrid,
				lockflag: 'unlock',
				dSource
			}
		};
		lockrequest.call(this, param);
	}
	props.cardTable.delRowsByIndex(tableId, checkedIndex, () => {
		setBodyBtnsEnable.call(this, props, tableId);
	});
}
/**
* 返回
* @param {*} props 
*/
export function backToList(props) {
	props.pushTo(listRouter, { pagecode: pagecode });
}
/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	let num = props.cardTable.getNumberOfRows(moduleId);
	let checkedRows = [];
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	props.button.setButtonDisabled([ 'BatchAlter', 'BatchFormula' ], num <= 0);
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}
/**
* 保存前校验
* @param {*} props 
*/
function validateBeforeSave(props) {
	props.cardTable.closeModel(tableId);
	// 过滤空行
	props.cardTable.filterEmptyRows(tableId, [ 'pk_card', 'pk_evaluate_b' ], 'include');
	// 表头必输校验
	let flag = props.form.isCheckNow(formId);
	if (!flag) {
		return false;
	}
	// 表体必输校验
	let allRows = props.cardTable.getVisibleRows(tableId);
	if (!allRows || allRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.BodyNotNull });
		return false;
	}
	flag = props.cardTable.checkTableRequired(tableId);
	if (!flag) {
		return false;
	}

	//数据校验
	flag = dataValidityValidator(allRows);
	if (!flag) {
		return false;
	}

	return true;
}

//数据校验
function dataValidityValidator(allRows) {
	let allmsg = '';
	let row = 1;
	for (let rowVal of allRows) {
		let msg = '';
		let body = rowVal.values;
		//资产编码
		let card_code = body.pk_card && body.pk_card.display;
		//本币原值
		let localOriginvalue = body.new_localoriginvalue && body.new_localoriginvalue.value * 1;
		//累计折旧
		let accudep = body.new_accudep && body.new_accudep.value * 1;
		//减值准备
		let predevaluate = body.predevaluate && body.predevaluate.value * 1;
		let salvage = body.new_salvage && body.new_salvage.value * 1;
		if (localOriginvalue - accudep - predevaluate < salvage) {
			msg += getMultiLangByID('201202504A-000007', { card_code }) /* 国际化处理：资产编码：{card_code}的净额小于净残值。*/;
		}
		//工作总量
		let allworkloan = body.new_allworkloan && body.new_allworkloan.value * 1;
		//累计工作量
		let accuworkloan = body.accuworkloan && body.accuworkloan.value * 1;
		if (allworkloan < accuworkloan) {
			msg += getMultiLangByID('201202504A-000039') /* 国际化处理：评估后工作总量小于累计工作量。*/;
		}
		//本币原值
		if (localOriginvalue < 0) {
			msg += getMultiLangByID('201202504A-000040') /* 国际化处理：评估后本币原值必须大于0。*/;
		}
		//累计折旧
		if (accudep < 0) {
			msg += getMultiLangByID('201202504A-000041') /* 国际化处理：评估后累计折旧必须大于0。*/;
		}
		//折旧期数
		let serviceMonth = body.new_servicemonth && body.new_servicemonth.value * 1;
		if (serviceMonth < 0) {
			msg += getMultiLangByID('201202504A-000042') /* 国际化处理：评估后折旧期数必须大于0。*/;
		}
		//净残值率
		let salvageRate = body.new_salvagerate && body.new_salvagerate.value * 1;
		if (salvageRate < 0) {
			msg += getMultiLangByID('201202504A-000043') /* 国际化处理：评估后净残值率必须大于0。*/;
		}
		if (allworkloan < 0) {
			msg += getMultiLangByID('201202504A-000044') /* 国际化处理：评估后工作总量必须大于0。*/;
		}
		//使用月限
		let newNatuerMonth = body.new_naturemonth && body.new_naturemonth.value * 1;
		if (newNatuerMonth < 0) {
			msg += getMultiLangByID('201202504A-000045') /* 国际化处理：评估后使用月限必须大于0。*/;
		}
		//评估后折旧截止日期大于卡片折旧开始日期
		let newDep_end_date = body.new_dep_end_date && body.new_dep_end_date.value;
		let dep_start_date = body['pk_card.dep_start_date'] && body['pk_card.dep_start_date'].value;
		if (
			dep_start_date &&
			newDep_end_date &&
			new Date(newDep_end_date.replace(/-/g, '/')) < new Date(dep_start_date.replace(/-/g, '/'))
		) {
			msg += getMultiLangByID('201202504A-000046') /* 国际化处理：评估后折旧截止日期不能早于卡片的折旧开始日期。*/;
		}
		if (msg.length > 0) {
			allmsg +=
				getMultiLangByID('201202504A-000047', {
					msg: msg,
					row: row
				}) /* 国际化处理： 表体第[{row}]行数据：[{msg}] \n*/ + '';
		}
		row += 1;
	}
	if (allmsg.length > 0) {
		allmsg += getMultiLangByID('201202504A-000008') /* 国际化处理： 评估单不能保存！*/;
		toast({ content: allmsg, color: 'danger' });
		return false;
	}
	return true;
}

/**
 * 浏览态设置审批流按钮显示隐藏
 * @param {*} props 
 */
function setBillFlowActionVisible(props) {
	let status = props.form.getFormStatus(formId);
	if (status == UISTATE.browse) {
		// 根据单据状态隐藏按钮
		let newHiddenBtns = [ 'Edit', 'Delete', 'Commit', 'UnCommit', 'QueryAboutBillFlow' ];
		props.button.setButtonVisible(newHiddenBtns, true);
		let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
		if (bill_status) {
			let bill_status_value = bill_status.value;
			if (bill_status_value === BILLSTATUS.free_check) {
				// 自由
				// newHiddenBtns.remove('Edit');
				// newHiddenBtns.remove('Delete');
				// newHiddenBtns.remove('Commit');
			} else if (bill_status_value === BILLSTATUS.un_check) {
				// 已提交
				// newHiddenBtns.remove('UnCommit');
			} else if (bill_status_value === BILLSTATUS.check_going) {
				// 审批中
				// newHiddenBtns.remove('QueryAboutBillFlow');
			} else if (bill_status_value === BILLSTATUS.check_pass) {
				// 审批通过
				// newHiddenBtns.remove('UnCommit');
				// newHiddenBtns.remove('QueryAboutBillFlow');
			} else if (bill_status_value === BILLSTATUS.check_nopass) {
				// 审批不通过
				// newHiddenBtns.remove('QueryAboutBillFlow');
			}
		}
		props.button.setButtonVisible(newHiddenBtns, false);
	}
}
/**
* 保存后
* @param {*} props 
*/
export function afterSave(props, data, oldstatus) {
	setValue.call(this, props, data);
	setStatus.call(this, props, UISTATE.browse);
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let cachData = this.props.createMasterChildData(pagecode, formId, tableId);
	// 保存成功后处理缓存
	if (oldstatus == UISTATE.add) {
		cardCache.addCache(pk, cachData, formId, dataSource);
	} else {
		cardCache.updateCache(pkField, pk, cachData, formId, dataSource);
	}
	setBrowseBtnsVisible.call(this, props);
	showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
}
/**
* 取消
* @param {*} props 
*/
export function cancel(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancelReal });
}
/**
* 确认取消
* @param {*} props 
*/
export function cancelReal(props) {
	// 解锁卡片
	lockcards.call(this, props, 'unlock');
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	props.form.cancel(formId);
	props.cardTable.resetTableData(tableId);
	setStatus.call(this, props, UISTATE.browse);
	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	loadDataByPk.call(this, props, pk);
}
/* 批改 */
export function BatchAltert(props) {
	let num = props.cardTable.getNumberOfRows(tableId);
	if (num <= 1) {
		return;
	}
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);

	// 如果需要批改的字段需要特殊的处理，则调用后端的批改操作
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);

	if ('pk_card'.indexOf(changeData.batchChangeKey) < 0) {
		ajax({
			url: '/nccloud/fa/evaluate/evaluateBatchAlter.do',
			data: {
				card: cardData,
				batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
				batchChangeKey: changeData.batchChangeKey, // 原始数据key
				batchChangeValue: changeData.batchChangeValue, // 原始数据value
				// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
				tableCode: ''
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//把数据设置到界面上
					if (success) {
						//把数据设置到界面上
						setValue.call(this, props, data.data);
						if (data.alterMsg) {
							toast({ color: 'warning', content: data.alterMsg });
						}
					}
				}
			},
			error: (res) => {
				if (res && res.message) {
					toast({ color: 'danger', content: res.message });
				}
			}
		});
	}
}
/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	let callback = (data) => {
		if (data) {
			showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		}
	};
	// 查询到最新单据数据
	getDataByPk.call(this, props, pk, callback);
}
/**
 * 加锁，解锁卡片
 * @param {*} props 
 */
export function lockcards(props, lockflag, async = true) {
	let data = props.cardTable.getChangedRows(tableId);
	// status : 0不变，1修改，2新增，3删除
	let allpks = [];
	for (let val of data) {
		// 若当前编辑态，且存在新增卡片则收集解锁
		if (val.status == 2 && val.values.pk_card.value) {
			allpks.push(val.values.pk_card.value);
		}
	}
	let dSource = getContext(ds, dataSource);
	let usrid = getContext('userId', dataSource);
	let param = {
		allpks,
		msgMap: {
			usrid,
			lockflag,
			dSource
		}
	};
	lockrequest.call(this, param, async);
}
/**
 * 解锁请求
 * @param {*} param 
 */
export function lockrequest(param, async) {
	ajax({
		url: url.lockcard,
		data: param,
		async,
		success: (res) => {},
		error: (res) => {}
	});
}
/**
 * 浏览态设置按钮显示隐藏
 * @param {*} props 
 */
function setBrowseBtnsVisible(props) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	if (status != UISTATE.browse) {
		props.button.setButtonVisible(browseButton, false);
	} else {
		let pkVal = props.form.getFormItemsValue(formId, pkField);
		if (!pkVal || !pkVal.value) {
			props.button.setButtonVisible(browseButton, false);
			props.button.setButtonVisible('Add', true);
			return;
		}
		props.button.setButtonVisible(browseButton, true);
		setBillFlowBtnsVisible.call(this, props, formId, pkField);
	}
}
/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
	if (
		!pk_org_v ||
		!pk_org_v.value ||
		(bill_status && (bill_status.value == BILLSTATUS.un_check || bill_status.value == BILLSTATUS.check_going))
	) {
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
	} else {
		props.button.setButtonDisabled([ 'AddLine', 'BatchAlter', 'BatchFormula' ], false);
		let checkedRows = [];
		let num = props.cardTable.getNumberOfRows(moduleId, false);
		if (num > 0) {
			checkedRows = props.cardTable.getCheckedRows(moduleId);
		}
		props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
	}
}

/**
 * 审批详情
 * @param {*} props 
 */
export function queryAboutBillFlow(props) {
	this.setState({
		showApprove: true,
		transi_type: props.form.getFormItemsValue(formId, 'transi_type').value,
		pk_bill: props.form.getFormItemsValue(formId, pkField).value
	});
}
function batchFormula(props) {
	let editingField = this.state.editingField;

	if (editingField === '' || editingField == undefined) {
		showMessage(props, {
			content: getMultiLangByID('facommon-000011') /* 国际化处理： 请选择要进行批量公式的字段*/,
			color: 'warning'
		});
		return;
	}
	if (canEditFields.indexOf(editingField) == -1) {
		showMessage(props, { content: getMultiLangByID('facommon-000012') /* 国际化处理： 该项不支持公式*/, color: 'warning' });
		return;
	}

	this.setState({ show: true, signature: '1' });
}
