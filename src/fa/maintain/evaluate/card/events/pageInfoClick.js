//卡片态肩部上一页下一页处理
import { loadDataByPk } from './buttonClick';
export default function(props, pk) {
	// 更新参数
	props.setUrlParam({ id: pk });
	loadDataByPk.call(this, props, pk);
}
