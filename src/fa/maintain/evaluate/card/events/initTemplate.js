//初始化模板
import { tableButtonClick } from '../events';
import buttonClick, { setStatus } from './buttonClick';
import { pagecode, tableId, formId, editButton, browseButton, defaultConfig, dataSource, ds } from '../constants';
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { cardUtils } = utils;
const { StatusUtils } = commonConst;
const { createOprationColumn, afterModifyCardMeta } = cardUtils;
const { LoginContext, faQueryAboutUtils } = components;
const { openAssetCardByPk } = faQueryAboutUtils;
const { getContext, loginContext, loginContextKeys } = LoginContext;
const { UISTATE } = StatusUtils;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addHeadAreaReferFilter, addBodyReferFilter } = ReferFilter;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, dataSource);
					// 获取数据源
					getContext(ds, dataSource);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}
/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status') || UISTATE.browse;
	if (status == UISTATE.add) {
		buttonClick.call(this, props, 'Add');
	} else {
		setStatus.call(this, props, status);
	}
}

function modifierMeta(props, meta) {
	addHeadAreaReferFilter.call(this, props, meta, defaultConfig);
	//参照过滤
	addBodyReferFilter.call(this, props, meta, defaultConfig);
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	// 表头参照参数过滤
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_card') {
			item.isMultiSelectedEnabled = true;
			item.queryCondition = () => {
				let pk_org = '';
				if (getContext(loginContextKeys.pk_org)) {
					pk_org = getContext(loginContextKeys.pk_org);
				}
				if (props.form.getFormItemsValue(formId, 'pk_org').value) {
					pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				}
				let pk_accbook = props.form.getFormItemsValue(formId, 'pk_accbook');
				pk_accbook = pk_accbook && pk_accbook.value;
				return {
					pk_org,
					pk_accbook
				};
			};
		}
	});

	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_card') {
			//给资产编码列添加超链接
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openAssetCardByPk.call(this, props, record.values.pk_card.value);
							}}
						>
							{record.values.pk_card && record.values.pk_card.display}
						</span>
					</div>
				);
			};
		}
		// setQueryCondition(props, item);
	});
	// 添加表格操作列
	let width = '150px';
	let oprCol = createOprationColumn.call(this, props, { tableButtonClick, getInnerBtns, width });
	meta[tableId].items.push(oprCol);

	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);
	return meta;
}

//获取显示按钮组
function getInnerBtns(props) {
	let buttonAry;
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	if (status == UISTATE.add || status == UISTATE.edit) {
		buttonAry = [ 'OpenCard', 'DelLine', 'Formula' ];
	} else {
		buttonAry = [ 'OpenCard' ];
	}
	return buttonAry;
}

const getAllBtnsFlat = (buttons) => {
	let btns = [];
	btns = btns.concat(buttons);
	for (let x of buttons) {
		if (x.children && x.children instanceof Array) {
			return getAllBtnsFlat(x.children);
		}
	}
	return btns;
};

//根据当前界面的 status 决定需要渲染的按钮
const filterBtnsByStatus2 = (props, allBtns) => {
	const browseBtnsTemp = browseButton;
	const editBtnsTemp = editButton;

	let btnsForVisible = []; //需要显示的 button 数组
	let status = undefined;
	try {
		status = props.getUrlParam('status');
	} catch (e) {}
	if (!status) {
		status = 'browse';
	}
	switch (status) {
		case 'browse':
			//btnsForVisible = Object.values(constants.CARD_BROWER_BTNS);
			btnsForVisible = browseBtnsTemp;
			break;

		case 'add':
		case 'edit':
			btnsForVisible = editBtnsTemp;
			break;

		default:
			break;
	}

	allBtns.forEach((x) => {
		if (btnsForVisible.includes(x.key)) {
			x.visible = true;
		} else {
			x.visible = false;
		}
	});
};
