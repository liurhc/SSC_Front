import { tableId, dataSource, ds, canEditFields } from '../constants';
import { lockrequest } from '../events/buttonClick';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showMessage } = msgUtils;
const { LoginContext } = components;
const { getContext } = LoginContext;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		case 'OpenCard':
			openCard.call(this, props, record, index, tableId);
			break;
		case 'DelLine':
			delLine.call(this, props, index, tableId, record);
			break;
		case 'Formula':
			formula.call(this, props, key, index, record);
			break;
		default:
			break;
	}
}
/**
 * 表格行展开
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function openCard(props, record, index, tableId) {
	let status = props.cardTable.getStatus(tableId);
	if (status == UISTATE.edit) {
		props.cardTable.setClickRowIndex(tableId, { record, index });
		props.cardTable.openModel(tableId, status, record, index);
	} else if (status == UISTATE.browse) {
		props.cardTable.toggleRowView(tableId, record);
	}
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 */
function delLine(props, index, tableId, record) {
	props.cardTable.delRowsByIndex(tableId, index);
	// 若行数据为新增态，则需解锁卡片
	if (record.values.pk_card && record.values.pk_card.value) {
		let dSource = getContext(ds, dataSource);
		let usrid = getContext('userId', dataSource);
		let param = {
			allpks: [ record.values.pk_card.value ],
			msgMap: {
				usrid,
				lockflag: 'unlock',
				dSource
			}
		};
		lockrequest.call(this, param);
	}
}
/**
 * 操作列公式
 * @param {*} props 
 */
function formula(props, key, index, record) {
	let editingField = this.state.editingField;
	if (editingField === '' || editingField == undefined) {
		showMessage(props, { content: getMultiLangByID('facommon-000013') /* 国际化处理： 请选择要进行公式的字段*/, color: 'warning' });
		return;
	}
	if (canEditFields.indexOf(editingField) == -1) {
		showMessage(props, { content: getMultiLangByID('facommon-000012') /* 国际化处理： 该项不支持公式*/, color: 'warning' });
		return;
	}
	this.setState({ show: true, signature: '0', select_row: record, row_number: index });
}
