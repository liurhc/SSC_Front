/**
 * 减值编辑前事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} index 
 * @param {*} record 
 */
export default function beforeEvent(props, moduleId, key, value, index, record) {
	// 编辑前解决赋值问题
	if (
		key === 'new_localoriginvalue' ||
		key === 'new_accudep' ||
		key === 'new_servicemonth' ||
		key === 'new_salvage' ||
		key === 'new_allworkloan'
	) {
		this.setState({
			set_field: key,
			row_number: index,
			editingField: key
		});
	} else {
		this.setState({
			editingField: key
		});
	}
	return true;
}
