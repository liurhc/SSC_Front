import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export const pagecode = '201202504A_card'; //单据模板pageID
export const filename = '201202504A'; //文件名称
export const appcode = '201202504A'; //应用编码
export const moduleId = '2012'; //所属模块编码
export const searchId = 'searchArea'; //查询区域编码
export const tableId = 'bodyvos'; //列表表体编码
export const formId = 'card_head'; //列表表头编码
export const appid = '0001Z91000000000177M'; //注册按钮id
export const oid = '1001Z91000000000C69T'; // 查询模板id
export const printFilename = getMultiLangByID('201202504A-000000')/* 国际化处理： 资产评估*/; // 输出文件名称
export const dataSource = 'fa.maintain.evaluate.main'; // 缓存
export const ds = 'dataSource';
export const uploaderPath = 'fa/evaluate/';
// 编辑态显示按钮
export const editButton = [
	'Save',
	'SaveCommit',
	'Cancel',
	'BatchFormula',
	'BatchAlter',
	'OpenCard',
	'OpenCard',
	'AddLine',
	'DelLine',
	'Formula'
];
// 浏览态显示按钮
export const browseButton = [
	'Add',
	'Edit',
	'Delete',
	'Commit',
	'UnCommit',
	'Attachment',
	'FAReceipt',
	'Print',
	'Refresh',
	'OpenCard',
	'QueryAbout'
];
export const listButton = [ 'EditGroup', 'Commit', 'Attachment', 'FAReceipt', 'QueryAbout', 'Print', 'Refresh' ]; // 列表态显示按钮
export const pkField = 'pk_evaluate'; // 评估单主键
export const title = getMultiLangByID('201202504A-000000')/* 国际化处理： 资产评估*/; // 评估单名称
export const listRouter = '/list'; // 列表页面
export const resourceCode = '2012032010'; // 权限资源编码
export const bill_type = 'HE'; // 权限资源编码
export const url = {
	printUrl: '/nccloud/fa/evaluate/printCard.do', // 打印输出
	queryCardUrl: '/nccloud/fa/evaluate/querycard.do', // 卡片加载查询
	deleteUrl: '/nccloud/fa/evaluate/delete.do', //删除
	commitUrl: '/nccloud/fa/evaluate/commit.do', //提交、收回、删除
	editURl: '/nccloud/fa/evaluate/edit.do', // 编辑
	saveUrl: '/nccloud/fa/evaluate/save.do', //保存
	updateUrl: '/nccloud/fa/evaluate/update.do', //更新
	bodyEditUrl: '/nccloud/fa/evaluate/bodyAfterEditAction.do', //表体编辑后事件
	headEditUrl: '/nccloud/fa/evaluate/headAfterEditAction.do', //表头编辑后事件
	cardEditUrl: '/nccloud/fa/evaluate/queryCardByPkAction.do', //卡片编辑后事件
	lockcard: '/nccloud/fa/facard/lockcard.do', //加锁卡片
	evaluateFormula: '/nccloud/fa/evaluate/evaluateFormula.do' //公式、批量公式
};

export const defaultConfig = {
	searchId: '',
	formId: 'card_head',
	bodyIds: [ 'bodyvos', 'bodyvos_edit' ],
	fieldMap: {
		pk_org: 'pk_org', //财务组织
		pk_accbook: 'pk_accbook', //账簿
		pk_mandept: 'pk_mandept', //管理部门
		//或者 pk_usedept: 'pk_usedept'
		pk_usedept: [ 'pk_usedept', 'pk_dept' ], //使用部门
		pk_assetuser: 'pk_assetuser', //使用人
		pk_recorder: 'pk_recorder', //经办人
		pk_ownerorg: 'pk_ownerorg' //货主管理组织
	},
	defPrefix: {
		head: 'def',
		body: 'def'
	}
};
//支持批量公式的字段
export const canEditFields = [
	'new_localoriginvalue',
	'new_accudep',
	'new_servicemonth',
	'new_salvage',
	'new_allworkloan'
]