import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
// 页面配置
const pageConfig = {
	// 应用名称
	title: getMultiLangByID('201202520A-000000') /* 国际化处理：资产减少*/,
	// 小应用主键moudleId
	moduleId: '2012',
	//pageCode
	pagecode: '201202520A_card',
	// app编码
	appcode: '201202520A',
	// 卡片态表头
	formId: 'card_head',
	// 卡片态详细信息
	tableId: 'bodyvos',
	// 卡片态侧拉编辑详细信息
	tableIdEdit: 'bodyvos_edit',
	// 卡片向下展开详细信息
	tableIdBrowse: 'bodyvos_browse',
	// 卡片操作信息
	cardTailinfo: 'card_tailinfo',
	// 浏览态查询区查询类型
	querytype: 'tree',
	//小应用id
	appid: '0001Z9100000000025XM',
	//单据类型
	bill_type: 'HF',
	// 编辑态按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'OpenCard', 'AddLine', 'DelLine', 'BatchAlter' ],
	// 浏览态按钮
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'OpenCard',
		'Refresh',
		//联查按钮组
		'QueryAbout',
		'QueryAboutBusiness',
		'QueryAboutVoucher',
		'QueryAboutBillFlow',
		//影像
		'Receipt',
		'FAReceiptScan',
		'FAReceiptShow',
		'More',
		'Refresh'
	],
	// 主键字段
	pkField: 'pk_reduce',
	//请求链接
	url: {
		//新增报存
		saveUrl: '/nccloud/fa/reduce/save.do',
		//修改保存
		updateUrl: '/nccloud/fa/reduce/update.do',
		//打印请求
		printUrl: '/nccloud/fa/reduce/printCard.do',
		//数据查询请求
		cardQueryUrl: '/nccloud/fa/reduce/querycard.do',
		//deleteurl
		deleteUrl: '/nccloud/fa/reduce/delete.do',
		//卡片加解锁
		lockcard: '/nccloud/fa/facard/lockcard.do',
		//提交url
		commitUrl: '/nccloud/fa/reduce/commit.do',
		//联查来源单据
		querySrcBilUrl: '/nccloud/ampub/common/amLinkQuery.do',
		// 编辑url
		editUrl: '/nccloud/fa/reduce/edit.do',
		//表头编辑后事件
		headAfterEditUrl: '/nccloud/fa/reduce/headAfterEdit.do',
		//表体编辑后事件
		bodyAfterEditUrl: '/nccloud/fa/reduce/bodyAfterEdit.do',
		//消息处理
		openReduceMsgURL: '/nccloud/fa/reduce/openReduceMsg.do',
		//批改
		batchAlterUrl: '/nccloud/fa/reduce/reduceBatchAlter.do'
	},
	//禁用批改字段
	disableBatchFiled: [ 'pk_card' ],
	//表头组织为空置灰
	orgDisableButton: [ 'AddLine', 'BatchAlter' ],
	//过滤配置
	defaultConfig: {
		searchId: '',
		formId: 'card_head',
		bodyIds: [ 'bodyvos', 'bodyvos_edit' ],
		defPrefix: {
			search: [ 'bodyvos.def', 'bodyvos.pk_card.def', 'def' ],
			body: [ 'def', 'pk_card.def' ]
		},
		specialFields: {
			pk_cumandoc: {
				//客商档案
				data: [
					//字段过滤
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_org'
					}
				]
			}
		}
	},
	// 输出文件名称
	printFilename: getMultiLangByID('201202520A-000000') /* 国际化处理：资产减少*/,
	// 打印模板节点标识
	printNodekey: null,
	resourceCode: '2012056010',
	listRouter: '/list',
	dataSource: 'fa.maintain.reduce.main',
	ds: 'dataSource',
	uploaderPath: 'fa/reduce/'
};

export { pageConfig };
