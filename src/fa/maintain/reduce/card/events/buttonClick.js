import { ajax, toast, print, output, cardCache } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import { headAfterEvent } from './afterEvent';
import ampub from 'ampub';
import fa from 'fa';

const { fa_components } = fa;
const { ImageMng } = fa_components;
const { faImageScan, faImageView } = ImageMng;
const { components, utils, commonConst } = ampub;
const { ScriptReturnUtils, LoginContext, queryVocherUtils } = components;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { getContext, loginContextKeys } = LoginContext;
const { cardUtils, msgUtils, multiLangUtils } = utils;
const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = cardUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { UISTATE,BILLSTATUS } = StatusUtils;

const {
	appcode,
	formId,
	tableId,
	pagecode,
	editBtns,
	browseBtns,
	pkField,
	bill_type,
	url,
	printNodekey,
	listRouter,
	resourceCode,
	dataSource,
	printFilename,
	ds,
	uploaderPath
} = pageConfig;

export default function(props, id) {
	let _this = this;
	switch (id) {
		case 'Add':
			saveAction.call(this, props);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Save':
			save.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'AddLine':
			addLine.call(this, props,true);
			break;
		case 'DelLine':
			delLine.call(this, props);
			break;
		case 'BatchAlter':
			batchAlter.call(this, props);
			break;
		case 'UnCommit':
			unCommit.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props);
			break;
		case 'SaveCommit':
			saveCommit.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'FAReceiptScan':
			FAReceiptScan.call(this, props);
			break;
		case 'FAReceiptShow':
			FAReceiptShow.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props, _this);
			break;
		case 'QueryAboutVoucher':
			queryAboutVoucher(props, formId, pkField, appcode); // tableId为区域编码，第三个参数为主键编码，appcode为应用编码,新增参数，标志列表态联查
			break;
		case 'QueryAboutBusiness':
			queryAboutBusiness.call(this, props);
		default:
			break;
	}
}

/**
 * 提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function commit(props, content) {
	let oldstatus = props.form.getFormStatus(formId);

	if (oldstatus == UISTATE.browse) {
		runScript.call(this, props, 'SAVE', 'commit', content);
	} else {
		saveCommit.call(this, props, content);
	}
}

/**
 * 收回
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function unCommit(props) {
	runScript.call(this, props, 'UNSAVE', 'commit');
}

/**
 * 保存提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function saveCommit(props, content) {
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	let oldstatus = props.form.getFormStatus(formId);

	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'SAVE',
		commitType: 'saveCommit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);

	if (content) {
		// 调用Ajax保存数据
		ajax({
			url: url.commitUrl,
			data: CardData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					this.setState({
						compositedisplay: false
					});
					let callback = () => {
						setStatus.call(this, props, UISTATE.browse);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkField,
						dataSource,
						null,
						false,
						callback,
						pagecode
					);
				}
			},
			error: (res) => {
				if (res && res.message) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					toast({ content: res.message, color: 'danger' });
				}
			}
		});
	} else {
		// 保存前执行验证公式
		props.validateToSave(CardData, () => {
			// 调用Ajax保存数据
			ajax({
				url: url.commitUrl,
				data: CardData,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						let callback = () => {
							setStatus.call(this, props, UISTATE.browse);
						};
						getScriptCardReturnData.call(
							this,
							res,
							props,
							formId,
							tableId,
							pkField,
							dataSource,
							null,
							false,
							callback,
							pagecode
						);
					}
				},
				error: (res) => {
					if (res && res.message) {
						if (content) {
							this.setState({
								compositedisplay: false
							});
						}
						toast({ content: res.message, color: 'danger' });
					}
				}
			});
		});
	}
}

/**
 * 提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function runScript(props, operatorType, commitType, content) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: operatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: CardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let callback = () => {
					setStatus.call(this, props, UISTATE.browse);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					null,
					false,
					callback,
					pagecode
				);
			}
		},
		error: (res) => {
			if (res && res.message) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}

export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');

	if (!printData) {
		showMessage.call(this,props,{type: MsgConst.Type.ChoosePrint});
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this,props,{type: MsgConst.Type.ChooseOutput});
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {		
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[pkField].value);
	});
	let printData = {
		filename: printFilename, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType: outputType // 输出类型
	};

	return printData;
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(formId, 'pk_reduce');
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: pagecode, // 文件名称
		appcode: appcode, // 应用编码
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType: outputType // 输出类型
	};

	return printData;
}

/**
 * 重新加载页面
 * @param {*} props 
 */
export function refresh(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	let callback = (data) => {
		if (data) {
			showMessage.call(this,props,{type: MsgConst.Type.RefreshSuccess});
		}
	};
	// 查询到最新单据数据
	getDataByPk.call(this, props, pk, callback);
}

/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk, callback) {
	ajax({
		url: url.cardQueryUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					cardCache.updateCache(pkField, pk, data, formId, dataSource);
				} else {
					setValue.call(this, props, undefined);
					if (pk) {
						cardCache.deleteCacheById(pkField, pk, dataSource);					
						showMessage.call(this,props,{type: MsgConst.Type.DataDeleted});
					}
				}
				typeof callback == 'function' && callback(data);
			}
		}
	});
}

// Save
export function saveAction(props) {
	props.cardTable.closeExpandedRow(tableId);
	setStatus.call(this, props, UISTATE.add);
	// 清空数据
	setValue.call(this, props, undefined);
	setDefaultValue.call(this, props);
	// 设置字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (pk_org_v && pk_org_v.value) {
		headAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		setBodyBtnsEnable.call(this, props, tableId);
	}
}

/**
 * 新增设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	// 模板默认值处理

	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId, dataSource);
	let groupName = getContext(loginContextKeys.groupName, dataSource);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype, dataSource);
	let pk_transitype = getContext(loginContextKeys.pk_transtype, dataSource);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org, dataSource);
	let org_Name = getContext(loginContextKeys.org_Name, dataSource);
	let pk_org_v = getContext(loginContextKeys.pk_org_v, dataSource);
	let org_v_Name = getContext(loginContextKeys.org_v_Name, dataSource);
	let businessdate = getContext(loginContextKeys.businessDate, dataSource);
	// 当前用户
	let usrid = getContext(loginContextKeys.userId, dataSource);
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype },
		bill_status: { display: getMultiLangByID('statusUtils-000000')/* 国际化处理： 自由态*/, value: '0' },
		business_date: { value: businessdate }
	});
	// 设置当前用户id
	this.setState({
		usrid
	});
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	// 更新参数
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	props.setUrlParam({ status, id: pkVal ? pkVal.value : '' });
	switch (status) {
		case UISTATE.add:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false,
				pk_accbook: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			setBrowseBtnsVisible.call(this, props);
			props.button.setButtonVisible(editBtns, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.edit:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true,
				pk_accbook: true
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			setBrowseBtnsVisible.call(this, props);
			props.button.setButtonVisible(editBtns, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			setBrowseBtnsVisible.call(this, props);
			break;
		default:
			break;
	}
	let data = {
		status
	}
	//获取bill_code
	if(props.form.getFormItemsValue(formId, 'bill_code')){
		let bill_code = props.form.getFormItemsValue(formId, 'bill_code').value;
		data.bill_code = bill_code;		
	}
	setHeadAreaData.call(this, props, data);
}

/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
	if (
		!pk_org_v ||
		!pk_org_v.value ||
		(bill_status && (bill_status.value == BILLSTATUS.un_check || bill_status.value == BILLSTATUS.check_going))
	) {
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
	} else {
		props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
		let checkedRows = [];
		let num = props.cardTable.getNumberOfRows(moduleId, false);
		if (num > 0) {
			checkedRows = props.cardTable.getCheckedRows(moduleId);
		}
		props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
	}
}

/**
 * 影像扫描
 * @param {*} props 
 */
function FAReceiptScan(props) {
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageScan(props, imageData);
}
/**
 * 影像查看
 * @param {*} props 
 */
function FAReceiptShow(props) {
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageView(props, imageData);
}

/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	// 单据主键
	let billId = props.form.getFormItemsValue(formId, pkField).value;
	props.ncUploader.show('uploader', {
		billId : uploaderPath + billId
	});
}

/**
 * 加锁，解锁卡片 默认异步
 * @param {*} props 
 */
export function lockcards(props, lockflag,async = true) {
	let data = props.cardTable.getChangedRows(tableId);
	// status : 0不变，1修改，2新增，3删除
	let allpks = [];
	for (let val of data) {
		// 若当前编辑态，且存在新增卡片则收集解锁
		if (val.status == 2 && val.values.pk_card.value) {
			allpks.push(val.values.pk_card.value);
		}
	}
	let dSource = getContext(ds, dataSource);
	let usrid = getContext('userId', dataSource);
	let param = {
		allpks,
		msgMap: {
			usrid,
			lockflag,
			dSource
		}
	};
	lockrequest.call(this, param,async);
}

/**
 * 加解锁请求
 * @param {*} param 
 */
export function lockrequest(param,async) {
	ajax({
		url: pageConfig.url.lockcard,
		data: param,
		async,
		success: (res) => {},
		error: (res) => {}
	});
}

/**
 * 设置合计信息
 * @param {*} props 
 * @param {*} data 
 */
export function setTotalInfo(props, data) {
	let length = data.bodyvos.rows.length;
	let purgerevenueTotal = 0;
	let purgefeeTotal = 0;
	for (let bvo of data.bodyvos.rows) {
		purgerevenueTotal = Number(purgerevenueTotal) + Number(bvo.values.purgerevenue.value || 0);
		purgefeeTotal = Number(purgefeeTotal) + Number(bvo.values.purgefee.value || 0);
	}
	this.setState({
		numtotal: length,
		purgerevenueTotal,
		purgefeeTotal
	});
}

function batchAlter(props) {
	let num = props.cardTable.getNumberOfRows(tableId);
	if (num <= 1) {
		return;
	}

	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);

	if (pageConfig.disableBatchFiled.indexOf(changeData.batchChangeKey) < 0) {
		// 如果需要批改的字段需要特殊的处理，则调用后端的批改操作
		let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);

		ajax({
			url: url.batchAlterUrl,
			data: {
				card: cardData,
				batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
				batchChangeKey: changeData.batchChangeKey, // 原始数据key
				batchChangeValue: changeData.batchChangeValue, // 原始数据value
				// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
				tableCode: ''
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//把数据设置到界面上
					setValue.call(this, props, data);
				}
			},
			error: (res) => {
				if (res && res.message) {
					toast({ color: 'danger', content: res.message });
				}
			}
		});
	}
}

/**
 * 审批详情
 * @param {*} props 
 */
export function queryAboutBillFlow(props, _this) {
	_this.setState({
		showApprove: true,
		transi_type: props.form.getFormItemsValue(formId, 'transi_type').value,
		pk_bill: props.form.getFormItemsValue(formId, pkField).value
	});
}

/**
 * 联查来源单据
 * @param {*} props 
 */
export function queryAboutBusiness(props) {
	// 获取选择数据
	let CardData = this.props.createMasterChildDataSimple(pagecode, this.formId, this.tableId);

	let bill_type_src = CardData.head.card_head.rows[0].values.bill_type_src.value;
	let pk_bill_src = CardData.head.card_head.rows[0].values.pk_bill_src.value;
	let urlData = {
		transtype: bill_type_src,
		id: pk_bill_src
	};
	ajax({
		url: url.querySrcBilUrl,
		data: urlData,
		success: (res) => {
			if (res && res.data) {
				props.openTo(res.data.url, {
					...res.data,
					status: 'browse'
				});
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	setCardValue.call(this, props, data);
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	if (status == UISTATE.browse) {
		setBrowseBtnsVisible.call(this, props);
	} else {
		setBodyBtnsEnable.call(this, props, tableId);
	}
}

/**
 * 浏览态设置按钮显示隐藏
 * @param {*} props 
 */
function setBrowseBtnsVisible(props) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	if (status != UISTATE.browse) {
		props.button.setButtonVisible(browseBtns, false);
	} else {
		let pkVal = props.form.getFormItemsValue(formId, pkField);
		if (!pkVal || !pkVal.value) {
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible('Add', true);
			return;
		}
		props.button.setButtonVisible(browseBtns, true);
		dealWithBillFlowBtnsVisible.call(this, props, formId, pkField);
	}
}

//根据单据状态控制按钮的显隐性
function dealWithBillFlowBtnsVisible(props, formId, pkField) {
	//调用公共方法
	setBillFlowBtnsVisible.call(this, props, formId, pkField);
	//减少单 预览凭证功能 , 不需要根据状态过滤设置为显示
	props.button.setButtonVisible('QueryAboutVoucher', true);
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk) {
	if (!pk || pk == 'null') {
		setValue.call(this, props, undefined);
		return;
	}
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData) {
		setValue.call(this, props, cachData);
	} else {
		getDataByPk.call(this, props, pk);
	}
}

/**
* 修改
* @param {*} props 
*/
export function edit(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		pk = props.getUrlParam('id');
	}
	if (!pk) {
		return;
	}
	props.cardTable.closeExpandedRow(tableId);

	let data = {
		pk,
		resourceCode
	};
	ajax({
		url: url.editUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					toast({ color: 'warning', content: data });
				} else {
					setStatus.call(this, props, UISTATE.edit);
				}
			}
		}
	});
}

/**
* 删除
* @param {*} props 
*/
export function delConfirm(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delReal });
}

/**
 * 删除
 * @param {*} props 
 * @param {*} operatorType 	
 * @param {*} commitType 
 */
function delReal(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'DELETE',
		commitType: 'commit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.deleteUrl,
		data: CardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let callback = (newpk) => {
					// 更新参数
					props.setUrlParam({ id: newpk });
					// 加载下一条数据
					loadDataByPk.call(this, props, newpk);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					undefined,
					true,
					callback
				);
			}
		}
	});
}

/**
* 保存
* @param {*} props 
*/
export function save(props) {
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let reqUrl = url.saveUrl; //新增保存
	let oldstatus = props.form.getFormStatus(formId);
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (oldstatus == UISTATE.edit && pk) {
		reqUrl = url.updateUrl; //修改保存
	}

	// 保存前执行验证公式
	props.validateToSave(cardData, () => {
		// 调用Ajax保存数据
		ajax({
			url: reqUrl,
			data: cardData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					afterSave.call(this, props, data, oldstatus);
				}
			}
		});
	});
}

/**
* 保存前校验
* @param {*} props 
*/
function validateBeforeSave(props) {
	props.cardTable.closeModel(tableId);
	// 表头必输校验
	let flag = props.form.isCheckNow(formId);
	if (!flag) {
		return false;
	}
	//过滤表格空行
	props.cardTable.filterEmptyRows(
		tableId,
		[ 'pk_card', 'purgerevenue', 'purgefee', 'pk_cumandoc', 'pk_reducestyle', 'pk_reason', 'memo' ],
		'include'
	);
	// 表体必输校验
	let allRows = props.cardTable.getVisibleRows(tableId);
	if (!allRows || allRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.BodyNotNull });
		return false;
	}
	flag = props.cardTable.checkTableRequired(tableId);
	if (!flag) {
		return false;
	}
	return true;
}

/**
* 返回
* @param {*} props 
*/
export function backToList(props) {
	// 返回前解锁卡片
	let status = props.cardTable.getStatus(tableId);
	if (status == UISTATE.edit) {
		lockcards.call(this, this.props, 'unlock');
	}
	props.pushTo(listRouter, {pagecode: pagecode});
}

/**
* 取消
* @param {*} props 
*/
function cancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancel });
}

/**
* 取消
* @param {*} props 
*/
export function cancel(props) {
	// 解锁卡片
	lockcards.call(this, props, 'unlock');
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	props.form.cancel(formId);
	props.cardTable.resetTableData(tableId);
	setStatus.call(this, props, UISTATE.browse);
	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}
	//恢复卡片的可编辑性
	props.table.setTableValueDisabled(tableId, 'pk_card', false);	
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	loadDataByPk.call(this, props, pk);
	if (!pk) {
		props.form.EmptyAllFormValue(formId); //如果没有上一条数据 , 清空表头数据 (清除可能由于设置默认值遗留的残留数据)
	}
}

/**
* 新增行
* @param {*} props 
* @param {*} isFocus 是否聚焦
*/
export function addLine(props,isFocus = false) {
	props.cardTable.addRow(tableId, undefined, {}, isFocus);
	setBodyBtnsEnable.call(this, props, tableId);
}

/**
* 删除行
* @param {*} props 
*/
export function delLine(props) {
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	let allpks = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
		// 若删除行为新增数据，则记录为需解锁卡片
		if (item.data.status == 2 && item.data.values.pk_card.value) {
			allpks.push(item.data.values.pk_card.value);
		}
	});
	// 若待解锁卡片数量大于0，则解锁
	let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
	let usrid = getContext('userId', dataSource);
	if (allpks.length > 0) {
		let param = {
			id: 'unlock',
			allpks,
			msgMap: {
				usrid,
				lockflag: 'unlock',
				dSource
			}
		};
		lockrequest.call(this, param);
	}

	props.cardTable.delRowsByIndex(tableId, checkedIndex);
	setBodyBtnsEnable.call(this, props, tableId);
}

/**
* 保存后
* @param {*} props 
*/
export function afterSave(props, data, oldstatus) {
	//恢复卡片的可编辑性
	props.table.setTableValueDisabled(tableId, 'pk_card', false);

	setValue.call(this, props, data);
	setStatus.call(this, props, UISTATE.browse);
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let cachData = this.props.createMasterChildData(pagecode, formId, tableId);
	// 保存成功后处理缓存
	if (oldstatus == UISTATE.add) {
		cardCache.addCache(pk, cachData, formId, dataSource);
	} else {
		cardCache.updateCache(pkField, pk, cachData, formId, dataSource);
	}
	setBrowseBtnsVisible.call(this, props);
	showMessage.call(this,props,{type: MsgConst.Type.SaveSuccess});
}

/**
 * 通过消息中心打开
 * @param {*} props 
 * @param {*} pkMsg 
 */
export function getReduceMsg(props, pkMsg) {
	ajax({
		url: url.openReduceMsgURL,
		data: {
			pkMsg: pkMsg,
			pagecode: pagecode
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					props.resMetaAfterPkorgEdit();
					//通过消息打开, 控制卡片不可编辑
					props.table.setTableValueDisabled(tableId, 'pk_card', true);
				} else {
					toast({ content: getMultiLangByID('facommon-000018') /*国际化处理：'单据已经弃审!'*/, color: 'warning' });
					props.form.EmptyAllFormValue(formId);
					setStatus.call(this, props, UISTATE.browse);
					setValue.call(this, props, undefined);//减少单消息打开报错提示后跳转到浏览态
				}
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
				props.form.EmptyAllFormValue(formId);
				setStatus.call(this, props, UISTATE.browse);
				setValue.call(this, props, undefined);//减少单消息打开报错提示后跳转到浏览态
			}
		}
	});
}
