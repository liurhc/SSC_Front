import {lockrequest,setBodyBtnsEnable} from '../events/buttonClick';
import { pageConfig } from './../const.js';
import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { getContext} = LoginContext;
const { dataSource,ds } = pageConfig;
/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 * @param {*} moduleId 
 */
export default function(props, key, text, record, index, tableId) {
	switch (key) {
		// 表格操作按钮
		case 'OpenCard':
			openCard.call(this, props, record, index, tableId);
			break;
		case 'DelLine':
			delLine.call(this, props,record, index, tableId);
			break;
		default:
			break;
	}
}

/**
 * 表格行展开
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function openCard(props, record, index, tableId) {
	let status = props.cardTable.getStatus(tableId);
	if (status == 'edit') {
		props.cardTable.setClickRowIndex(tableId, { record, index });
		props.cardTable.openModel(tableId, status, record, index);
	} else if (status == 'browse') {
		props.cardTable.toggleRowView(tableId, record);
	}
}


/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 */
function delLine(props,record, index, tableId) {
	props.cardTable.delRowsByIndex(tableId, index);
	 // 若行数据为新增态，则需解锁卡片
	 if (record.status === '2') {
		if (record.values.pk_card && record.values.pk_card.value) {
			let dSource = getContext(ds, dataSource);
			let usrid = getContext('userId', dataSource);
			let param = {
				allpks: [record.values.pk_card.value],
				msgMap:{
					usrid,
					lockflag:'unlock',
					dSource
				}
			};
			lockrequest.call(this, param);
		}
	}

	setBodyBtnsEnable.call(this, props, tableId);
}
