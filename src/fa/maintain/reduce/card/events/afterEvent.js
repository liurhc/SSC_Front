import { ajax,toast } from 'nc-lightapp-front';
import { setValue,addLine,lockrequest } from './buttonClick';
import { pageConfig } from '../const';
import ampub from 'ampub';

const { components,utils } = ampub;
const { OrgChangeEvent,LoginContext } = components;
const { orgChangeEvent } = OrgChangeEvent;
const { loginContextKeys, getContext} = LoginContext;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { formId, tableId, pagecode, url ,orgDisableButton,dataSource} = pageConfig;

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	let data;
	if (key == 'pk_org_v') {

		let callback = () => {
		
			data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
			ajax({
				url: url.headAfterEditUrl,
				data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						setValue.call(this, props, data);
						if (value.value) {
							defaultAddLine.call(this,props);
						}
					}
				}
			});
		}
		
		orgChangeEvent.call(this,props,pagecode,formId,tableId,key,value,oldValue,false,orgDisableButton,callback);
	}
	data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	if (key == 'pk_accbook') {		
		ajax({
			url: url.headAfterEditUrl,
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					setValue.call(this, props, data);
					defaultAddLine.call(this,props);
				}
			}
		});		
	}

	if (key == 'business_date') {
		if (value && typeof value.value == 'string') {
			ajax({
				url: url.headAfterEditUrl,
				data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						setValue.call(this, props, data);
					}
				}
			});
		}
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows);
	data.index = index;
	let i = index;

	if (key === 'pk_card') {

		//所选数据为空不需要走编辑后事件
		if(changedRows && changedRows.length == 1 && changedRows[0].oldvalue.value == null
			&& changedRows[0].oldvalue.value == changedRows[0].newvalue.value){
			return;
		}
	
		ajax({
			url: url.bodyAfterEditUrl,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let userObj = undefined;
					if(res.data.userjson){
						userObj = JSON.parse(res.data.userjson);
					}	
					// 工作量法的卡片减少时提示信息
					let workLoanRowMsg = userObj&&userObj.workLoanRowMsg;
					// 工作量法的卡片所属行号
					let workLoanRowList = userObj&&userObj.workLoanRowList;
					
					setValue.call(this, props, data);
					let num = props.cardTable.getNumberOfRows(tableId);
					//如果为最后一行 , 自动增行
					if (num == index + 1) {
						addLine.call(this, props);
					}
					if(workLoanRowMsg){
						props.ncmodal.show(`${pagecode}-confirm`, {
							title: getMultiLangByID('facommon-000017') /*国际化处理：'询问'*/,
							content: workLoanRowMsg,
							beSureBtnClick: () => {
								//确认不需要额外操作
							},
							cancelBtnClick: () => {
								//用于解锁卡片
								let allpks = [];
								//清空工作量卡片
								for (let row of workLoanRowList){
									let item = props.cardTable.getValByKeyAndIndex(tableId, row, 'pk_card');
									allpks.push(item.value);
									clearBodyDataByIndex(props,row);
								}
								// 若待解锁卡片数量大于0，则解锁
								let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
								let usrid = getContext('userId', dataSource);
								if (allpks.length > 0) {
									let param = {
										id: 'unlock',
										allpks,
										msgMap: {
											usrid,
											lockflag: 'unlock',
											dSource
										}
									};
									lockrequest.call(this, param);
								}
							}
						});
					}
				}
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
				//清行数据
				props.cardTable.delRowsByIndex(tableId, i);
				props.cardTable.addRow(tableId, i, {}, false);
			}
		});

		
	}
}

/**
 * 根据索引清除当前行数据
 * @param {*} props 
 * @param {*} index 
 */
function clearBodyDataByIndex(props, index) {
	//清行数据
	props.cardTable.delRowsByIndex(tableId, index);
	props.cardTable.addRow(tableId, index, {}, false);
}

//编辑后事件后如果表体被清空默认增行
function defaultAddLine(props){
	let allRows = props.cardTable.getAllRows(tableId);
	let rowCount = 0;
	if (allRows) {
		rowCount = allRows.length;
	}
	if (rowCount == 0){
		addLine.call(this,props);
	}
}