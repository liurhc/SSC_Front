import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
import { pageConfig } from './const.js';

import {
	buttonClick,
	initTemplate,
	headAfterEvent,
	bodyAfterEvent,
	bodyBeforeEvent,
	pageInfoClick,
	save,
	getDataByPk,
	backToList,
	rowSelected,
	lockcards,
	lockrequest,
	getReduceMsg,
	commit
} from './events';
import ampub from 'ampub';

const { components, utils, commonConst } = ampub;
const { ApprovalTrans,LoginContext } = components;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { loginContextKeys, getContext} = LoginContext;

const { NCAffix, NCBackBtn } = base;
const { ApproveDetail } = high;
const { title, formId, tableId, pagecode, bill_type, dataSource, moduleId, pkField, ds } = pageConfig;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = formId;
		this.moduleId = moduleId;
		this.tableId = tableId;

		this.state = {
			pk_org: '',
			bill_status: '',
			numtotal: '',
			totalNum: '',
			bill_type: '',
			transi_type: '',
			pk_bill: props.getUrlParam('id'),
			showApprove: false,
			compositedisplay: false,
			compositedata: {},
			// 当前用户
			usrid: ''
		};

		initTemplate.call(this, props);
	}
	componentDidMount() {
		//查询单据详情
		let status = this.props.getUrlParam('status');
		if (status != UISTATE.add) {
			let pk = this.props.getUrlParam('id');
			if (pk && pk != 'undefined' && pk != 'null') {
				getDataByPk.call(this, this.props, pk);
			} else {
				//处理消息打开
				let pkMsg = this.props.getUrlParam('pk_msg');
				if (pkMsg && pkMsg != 'undefined' && pkMsg != 'null') {
					getReduceMsg.call(this, this.props, pkMsg);
				}
			}
		}
	}

	componentWillMount() {
		window.onbeforeunload = () => {
            // 解锁卡片
            let status = this.props.cardTable.getStatus(tableId);
            if (status == 'edit') {
                lockcards.call(this, this.props, 'unlock', false);
            }
        };
        if (window.history && window.history.pushState) {
            window.onpopstate = () => {
                lockcards.call(this, this.props, 'unlock', false);
            };
        }
	}

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	// 侧拉删行前解锁
	modelDelRowBefore = (props, moduleId, moduleIndex, row) => {
        let allpks = [ row.values.pk_card.value ];
        let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
        let usrid = getContext('userId', dataSource);
        let param = {
            allpks,
            msgMap: {
                usrid,
                lockflag: 'unlock',
                dSource
            }
        };
        lockrequest.call(this, param);
        return true;
    };

	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		{
			return (
				<div className="shoulder-definition-area">
					<div className="definition-icons">
						{createButtonApp({
							area: 'card_body',
							buttonLimit: 4,
							onButtonClick: (props, id) => {
								buttonClick.call(this, props, id, tableId);
							},
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
			);
		}
	};

	//提交及指派 回调
	getAssginUsedr = (content) => {
		commit.call(this, this.props, content);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { cardTable, form, ncmodal, cardPagination, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		const { createCardPagination } = cardPagination;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createModal } = ncmodal;
		let status = this.props.getUrlParam('status') || UISTATE.browse;
		return (
			<div id="nc-bill-card" className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						{/* 头部 header */}
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title : title,
								formId,
								backBtnClick: backToList
							})}
							{/* 按钮区 btn-area */}
							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: save.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							onBeforeEvent: bodyBeforeEvent.bind(this),
							showCheck: true,
							showIndex: true,
							modelDelRowBefore: this.modelDelRowBefore.bind(this),
							selectedChange: rowSelected.bind(this)
						})}
					</div>
				</div>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')/* 国际化处理： 指派*/}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/* 附件*/}
				{createNCUploader('uploader', {disableModify:this.props.form.getFormItemsValue("card_head", 'bill_status')&&this.props.form.getFormItemsValue("card_head", 'bill_status').value!="0"})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
			</div>
		);
	}
}

const CardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	},
	mutiLangCode: moduleId
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));

export default CardBase;
