import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
// 页面配置
const pageConfig = {
	//应用名称
	title: getMultiLangByID('201202520A-000000') /* 国际化处理：资产减少*/,
	// 小应用主键moudleId
	moduleId: '2012',
	// app编码
	appcode: '201202520A',
	// 页面编码
	pagecode: '201202520A_list',
	// 主键字段
	pkField: 'pk_reduce',
	//单据类型
	bill_type: 'HF',
	// 浏览态查询区域编码
	searchId: 'searchArea',
	// 浏览态主表表格区域
	tableId: 'list_head',
	// 卡片态表头
	formId: 'card_head',
	// 浏览态查询区查询类型
	querytype: 'tree',
	url: {
		//分页请求
		queryPageUrl: '/nccloud/fa/reduce/querypagelistbypks.do',
		//打印请求
		printUrl: '/nccloud/fa/reduce/printCard.do',
		//提交
		commitUrl: '/nccloud/fa/reduce/commit.do',
		//列表态请求
		listQueryUrl: '/nccloud/fa/reduce/query.do',
		//列表态删除
		listDelteUrl: '/nccloud/fa/reduce/delete.do',
		//联查来源单据
		querySrcBilUrl: '/nccloud/ampub/common/amLinkQuery.do',
		//编辑
		editUrl: '/nccloud/fa/reduce/edit.do',
		//凭证联查单据
		queryByVoucherUrl: '/nccloud/fa/reduce/queryByVoucher.do'
	},
	//过滤配置
	defaultConfig: {
		searchId: 'searchArea',
		formId: 'card_head',
		bodyIds: [],

		specialFields: {
			'bodyvos.pk_card': {
				orgMulti: 'pk_org', //添加左上角主组织参照
				data: [
					{
						fields: [ 'pk_org', 'pk_org_v' ], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					}
				]
			}
		},
		linkageData: {
			//联动链
			//参照字段:['被过滤字段1'，'被过滤字段2']
			pk_org: [
				'pk_accbook',
				'pk_card',
				'bodyvos.pk_card',
				'bodyvos.pk_card.pk_mandept', //管理部门
				'bodyvos.pk_card.pk_usedept', //使用部门
				'bodyvos.pk_card.pk_category' //资产类别
			]
		},
		defPrefix: {
			search: [ 'bodyvos.def', 'bodyvos.pk_card.def', 'def' ],
			body: [ 'def', 'pk_card.def' ]
		}
	},
	// 打印模板节点标识
	printFilename: getMultiLangByID('201202520A-000000') /* 国际化处理：资产减少*/,
	printNodekey: null,
	resourceCode: '2012056010',
	cardRouter: '/card',
	dataSource: 'fa.maintain.reduce.main',
	ds: 'dataSourceCode',
	uploaderPath: 'fa/reduce/'
};

export { pageConfig };
