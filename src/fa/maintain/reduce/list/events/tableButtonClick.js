import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import { linkToCard } from './buttonClick';
import ampub from 'ampub';

const { components, commonConst } = ampub;
const { ScriptReturnUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { tableId, pagecode, formId, pkField, url, resourceCode,dataSource } = pageConfig;

export default function buttonClick(props, key, text, record, index) {
	switch (key) {
		case 'Edit':
			edit.call(this, props, record, index);
			break;
		case 'Delete':
			delRow.call(this, props, record, index);
			break;
		case 'UnCommit':
			UncommitAction.call(this, props, record, index);
			break;
		case 'Commit':
			commitAction.call(this, props, record, index);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, record);
			break;
	}
}
function UncommitAction(props, record, index) {
	let id = record.pk_reduce.value;
	let ts = record.ts.value;
	let paramInfoMap = {};
	paramInfoMap[id] = ts;

	let params = [ { id: id, index: index } ];

	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'UNSAVE',
			pageid: pagecode.replace('list', 'card')
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, pkField, formId, tableId,false,dataSource);
			// setBatchBtnsEnable.call(this, props, tableId);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}
function commitAction(props, record, index) {
	let id = record.pk_reduce.value;
	let ts = record.ts.value;
	let paramInfoMap = {};
	paramInfoMap[id] = ts;

	let params = [ { id: id, index: index } ];
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'SAVE',
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
			//commitType : 'saveCommit' //保存提交
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, pkField, formId, tableId,false,dataSource);
			// setBatchBtnsEnable.call(this, props, tableId);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}

//删除
function delRow(props, record, index) {
	let id = record.pk_reduce.value;
	let ts = record.ts.value;
	let paramInfoMap = {};
	paramInfoMap[id] = ts;

	let params = [ { id: id, index: index } ];
	ajax({
		url: pageConfig.url.listDelteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card')
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, pkField, formId, tableId, true,dataSource);
			// 清除缓存
			props.table.deleteCacheId(tableId, id);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}

function queryAboutBillFlow(record) {
	let pk = record[pkField].value;
	this.setState({
		pk_bill: pk,
		showApprove: true,
		transi_type: record.transi_type.value
	});
}

/**
 * 表格行修改
 * @param {*} props 
 * @param {*} record 
 */
function edit(props, record) {
	let pk = record[pkField].value;
	let data = {
		pk,
		resourceCode
	};

	ajax({
		url: url.editUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					toast({ color: 'warning', content: data });
				} else {
					linkToCard.call(this, props, record, UISTATE.edit);
				}
			}
		}
	});
}
