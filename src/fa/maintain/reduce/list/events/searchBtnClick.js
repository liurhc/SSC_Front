import { ajax } from 'nc-lightapp-front';
import { setBatchBtnsEnable } from './buttonClick';
import { pageConfig } from './../const.js';
import ampub from 'ampub';

const { components, utils } = ampub;
const { LoginContext } = components;
const { loginContextKeys, getContext } = LoginContext;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;
const { searchId, tableId, pagecode, url, bill_type,dataSource } = pageConfig;

/**
 * 点击查询，获取查询区数据
 * @param {*} props 
 * @param {*} searchVal 
 */
export default function(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}

	// 获取交易类型编码
	let transi_type = getContext(loginContextKeys.transtype, dataSource);
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(searchId);
	}
	// 分页信息
	let pageInfo = props.table.getTablePageInfo(tableId);
	queryInfo.pageInfo = pageInfo;
	queryInfo.pagecode = pagecode;
	queryInfo.billtype = bill_type;
	queryInfo.transtype = transi_type;

	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);

	// 条件查询
	ajax({
		url: url.listQueryUrl,
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
