import { ajax, toast , print, output} from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
import { pageConfig } from './../const.js';
import ampub from 'ampub';

const { components, utils } = ampub;
const { ScriptReturnUtils,queryVocherUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { listUtils, msgUtils, multiLangUtils } = utils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;
const {
	appcode,
	formId,
	tableId,
	searchId,
	pagecode,
	pkField,
	url,
	printNodekey,
	cardRouter,
	printFilename,
	uploaderPath,
	batchBtns = [
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		//联查按钮组
		'QueryAbout',
		'QueryAboutBusiness',
		'QueryAboutVoucher',
		'QueryAboutBillFlow',
		'Print',
		'Preview',
		'Output',
		'ReceiptGroup',
		//影像
		'Receipt',
		'FAReceiptScan',
		'FAReceiptShow'
	],
	dataSource
} = pageConfig;

export default function (props, id) {
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'UnCommit':
			UncommitAction.call(this, props);
			break;
		case 'Commit':
			commitAction.call(this, props);
			break;
		case 'Print':
            printTemp.call(this, props);
			break;
		case 'Output':
            outputTemp.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this,props);	
			break;
		case 'QueryAboutVoucher':
			queryAboutVoucher(props, tableId, pkField, appcode,true);
			break;
		case 'QueryAboutBusiness':
			queryAboutBusiness.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this,props);
			break;
        default:
            break;

	}
}
export function commitAction(props, content){
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_reduce.value;
		let ts = v.data.values.ts.value;
		paramInfoMap[id] = ts;

		let index = v.index;
		return {
			id, index
		}
	})
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'SAVE',
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit',  //提交
			content : content
			//commitType : 'saveCommit' //保存提交
		},
		success: (res) => {
			if(content){
				this.setState({
					compositedisplay : false
				});
			}
			getScriptListReturn.call(this,params, res, props, pkField, formId, tableId,false,dataSource);
			setBatchBtnsEnable.call(this, props, tableId);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}
const UncommitAction = (props) => {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_reduce.value;
		let ts = v.data.values.ts.value;
		paramInfoMap[id] = ts;
		let index = v.index;
		return {
			id, index
		}
	})
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'UNSAVE',
			pageid: pagecode.replace('list', 'card')
		},
		success: (res) => {
			getScriptListReturn.call(this,params, res, props, 'pk_reduce', formId, tableId,false,dataSource);
			setBatchBtnsEnable.call(this, props, tableId);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}
export function printTemp(props) {

	let printData = getPrintData.call(this, props, 'print');
    
    if (!printData) {
		showMessage.call(this,props,{type: MsgConst.Type.ChoosePrint});
        return;
    }
    print(
        'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
        url.printUrl, // 后台打印服务url
        printData
    );

}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
    let printData = getPrintData.call(this, props, 'output');
    if (!printData) {	
		showMessage.call(this,props,{type: MsgConst.Type.ChooseOutput});
        return;
    }
	output({
		url: url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[pkField].value);
	});
	let printData = {
		filename: printFilename, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType : outputType // 输出类型
	};

	return printData;
}


/**
 * 联查来源单据
 * @param {*} props 
 */
export function queryAboutBusiness(props) {
	// 获取选择数据
	let data = props.table.getCheckedRows(tableId);
	if (data.length === 0) {
		toast({ content: getMultiLangByID('201202520A-000001'), color: 'warning' })/* 国际化处理： 请选择需要联查的数据！*/;
	} else {
		let bill_type_src = data[0].data.values.bill_type_src.value;
		let pk_bill_src = data[0].data.values.pk_bill_src.value;
		let urlData = {
			transtype : bill_type_src,
			id : pk_bill_src
		};
		ajax({
			url: url.querySrcBilUrl,
			data: urlData,
			success: (res) => {
				if (res && res.data) {
					props.openTo(res.data.url, {
						...res.data,
						status: 'browse'
					});
				}
			},
			error: (res) => {
				if (res && res.message) {
					toast({ color: 'danger', content: res.message });
				}
			}
		});
	}
}

/**
 * 重新加载页面
 * @param {*} props 
 */
export function refresh(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

//确认删除
export function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {	
		showMessage.call(this,props,{type: MsgConst.Type.ChooseDelete});
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: deleteAction });
}

//删除
function deleteAction(props){
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {
	};
	let params = data.map((v) => {
		let id = v.data.values.pk_reduce.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id, index
		}
	});
	ajax({
		url: url.listDelteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card')
		},
		success: (res) => {
			getScriptListReturn.call(this,params, res, props, pkField, formId, tableId,true,dataSource);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}

/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props){
	let checkedrows = props.table.getCheckedRows(tableId);
	if (!checkedrows || checkedrows.length == 0) {		
		showMessage.call(this,props,{type: MsgConst.Type.ChooseOne});
		return;
	}

	// billId 是单据主键
    let billId=checkedrows[0].data.values[pkField].value;
    props.ncUploader.show('uploader', {
        billId : uploaderPath + billId
    });

}

/**
 * 新增
 * @param {*} props 
 */
export function add(props) {
	linkToCard.call(this, props, undefined, 'add');
}

/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	props.pushTo(cardRouter, {
		status,
		id: record[pkField] ? record[pkField].value : '',
		pagecode: pagecode
	});
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, moduleId);
}