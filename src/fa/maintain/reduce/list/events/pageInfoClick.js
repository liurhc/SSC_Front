import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';

const { tableId, pagecode, url } = pageConfig;

/**
 * 分页处理
 * @param {*} props 
 */
export default (props, config, pks) => {
	let data = {
		allpks: pks,
		pagecode
	};
	//得到数据渲染到页面
	ajax({
		url: url.queryPageUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableId, data[tableId]);
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
				}
			}
		}
	});
};
