import { linkToCard } from './buttonClick';

/**
 * 行双击事件
 * @param {*} record 
 * @param {*} index 
 */
export default function(record, index, props) {
	linkToCard.call(this, props, record);
}
