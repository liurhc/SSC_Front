import { linkToCard, setBatchBtnsEnable } from './buttonClick';
import tableButtonClick from './tableButtonClick';
import { pageConfig } from './../const.js';

import ampub from 'ampub';
import fa from 'fa';

const { components, utils } = ampub;
const { LoginContext } = components;
const { loginContext, getContext } = LoginContext;
const { listUtils, multiLangUtils } = utils;
const { createOprationColumn } = listUtils;
const { getMultiLangByID } = multiLangUtils;
const { fa_components } = fa;
const { ReferFilter, AccbookAndPeriod } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;
const { getAccbookAndPeriod } = AccbookAndPeriod;
const { defaultConfig, tableId, pagecode, dataSource, ds, searchId } = pageConfig;



export default function(props) {
	props.createUIDom(
		{
			// 页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, dataSource);
					// 获取数据源
					getContext(ds, dataSource);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));/* 国际化处理： 确定要删除吗？*/
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	setBatchBtnsEnable.call(this, props, tableId);
	getAccbookAndPeriod.call(this, props, searchId);
}

function seperateDate(date) {
	if (typeof date !== 'string') return;
	let result = date.split(' ') && date.split(' ')[0];
	return result;
}

function modifierMeta(props, meta, _this) {
	//添加参照过滤
	addSearchAreaReferFilter(props, meta, defaultConfig);

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('201202520A-000002')/* 国际化处理： 减少单号*/}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, 'browse');
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		} else if (item.attrcode == 'business_date') {
			item.render = (text, record, index) => {
				return <span>{record.business_date && seperateDate(record.business_date.value)}</span>;
			};
		} else if (item.attrcode == 'billmaketime') {
			item.render = (text, record, index) => {
				return <span>{record.billmaketime && seperateDate(record.billmaketime.value)}</span>;
			};
		}
		return item;
	});

	// 添加表格操作列
	let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
	meta[tableId].items.push(oprCol);
	return meta;
}
