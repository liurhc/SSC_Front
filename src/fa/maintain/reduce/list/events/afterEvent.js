import {pageConfig} from '../const';
import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage,AccbookAndPeriod } = fa_components;
const { referLinkageClear } = ReferLinkage;
const { getAccbookAndPeriod } = AccbookAndPeriod;
const{defaultConfig} = pageConfig;

export default function afterEvent(key, value) {
    if (key === 'pk_org') {
        //清空查询区关联项
        referLinkageClear.call(this,this.props, key, defaultConfig);
        //根据财务组织加载默认账簿
        getAccbookAndPeriod.call(this,this.props,pageConfig.searchId);
    }
}
