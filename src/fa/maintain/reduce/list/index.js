import React, { Component } from 'react';
import { createPage, base, high, createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, searchBtnClick, pageInfoClick, tableModelConfirm, doubleClick, rowSelected, afterEvent,commitAction,setBatchBtnsEnable  } from './events';
import {pageConfig} from './const.js';
import ampub from 'ampub';

const { components, utils } = ampub;
const { ApprovalTrans, queryVocherSrcUtils} = components;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { queryVoucherSrc } = queryVocherSrcUtils;

const { NCAffix } = base;
const { ApproveDetail } = high;
const {title,pkField,bill_type, moduleId,searchId,tableId,dataSource,pagecode } = pageConfig;
class List extends Component {
	constructor(props) {
		super(props);
		this.moduleId = moduleId;
		this.searchId = searchId;
		this.tableId = tableId;
	
		this.state = {
			transi_type: '',
			show: false,
			showApprove: false,
			pk_bill: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
		
	}
	
	componentDidMount() {
		//凭证联查来源单据
		queryVoucherSrc(this.props,tableId,pkField,pagecode);
	}

	//取消 指派
    turnOff = () => {
        this.setState({
            compositedisplay: false
		});
		this.props.table.selectAllRows(tableId, false);
		rowSelected.call(this, this.props, tableId);
	};
	
  	//提交及指派 回调，注意保存提交时候需要调用保存提交的方法
  	getAssginUsedr = (content) => {
		commitAction.call(this, this.props, content);//原提交方法添加参数content 
	};

	render() {
		let { table, search,ncmodal, ncUploader  } = this.props;
		let { createNCUploader } =ncUploader;
		let { createModal } = ncmodal;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
	
		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{title}</h2>
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
					{/* 查询区 search-area */}			
					<div className="nc-bill-search-area">
						{NCCreateSearch(this.searchId, {
							onAfterEvent: afterEvent.bind(this),
							clickSearchBtn: searchBtnClick.bind(this)							
						})}
					</div>
				{/* 列表区 table-area */}
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						tableModelConfirm: tableModelConfirm,
						handlePageInfoChange: pageInfoClick.bind(this),
						onRowDoubleClick: doubleClick.bind(this),
						showCheck: true,
						showIndex: true,
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						pkname: pkField,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				 {/* 提交及指派 */}
				 {this.state.compositedisplay ? (
                    <ApprovalTrans
                        title={getMultiLangByID('451012504A-000004')}/* 国际化处理： 指派*/
                        data={this.state.compositedata}
                        display={this.state.compositedisplay}
                        getResult={this.getAssginUsedr}
                        cancel={this.turnOff}
                    />
                ) : (
                    ''
                )}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false })
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{createModal(`${this.tableId}-del-confirm`, { color: 'warning' })}
				{/* 附件*/}
                {createNCUploader('uploader', {disableModify:(this.props.cardTable.getAllData('list_head').rows[0]==null ? '1': (this.props.table.getCheckedRows('list_head').length>0&&this.props.table.getCheckedRows('list_head')[0].data.values.bill_status.value))!='0'})}
			</div>
		);
	}
}

const ListBase = createPage({})(List);

// 渲染页面
// ReactDOM.render(<List />, document.querySelector('#app'));

export default ListBase;
