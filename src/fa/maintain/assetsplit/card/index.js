import React, { Component } from 'react';
import { createPage, base, high, ajax } from 'nc-lightapp-front';
import { pageConfig } from './const';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { ApprovalTrans } = components;
import {
	buttonClick,
	initTemplate,
	formAfterEvent,
	pageInfoClick,
	bodyAfterEvent,
	lockcards,
	loadDataByPk,
	backToList,
	save,
	rowSelected,
	beforeEvent,
	commitrunScript,
	saveCommit
} from './events';
const { NCAffix } = base;
const { ApproveDetail } = high;
const { formId, tableId, pagecode, dataSource, url } = pageConfig;

class MasterChildCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showApprove: false,
			pk_bill: '',
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		this.is_allow_dept_midlev = 'N'; //是否允许部门非末级(组织级数据)

		initTemplate.call(this, props);
	}
	componentWillMount() {
		// 解锁卡片
		window.onbeforeunload = () => {
			let status = this.props.cardTable.getStatus(tableId);
			if (status == 'edit') {
				lockcards.call(this, this.props, 'unlock', false);
			}
		};
		if (window.history && window.history.pushState) {
			window.onpopstate = () => {
				lockcards.call(this, this.props, 'unlock', false);
			};
		}
	}
	// 移除事件
	componentWillUnmount() {
		window.onbeforeunload = null;
	}
	componentDidMount() {
		if (this.props.getUrlParam('status') != 'add') {
			let pk = this.props.getUrlParam('id');
			if (pk && pk != 'undefined') {
				loadDataByPk.call(this, this.props, pk);
			}
		}
	}
	//提交及指派 回调
	getAssginUsedr = (value) => {
		let status = this.props.form.getFormStatus(formId) || UISTATE.browse;
		if (status == UISTATE.browse) {
			commitrunScript.call(this, this.props, 'SAVE', 'commit', value);
		} else {
			saveCommit.call(this, this.props, value);
		}
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};
	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};
	/**
	 * 侧拉增行设置默认值
	 */
	modelAddRow = (props, moduleId, moduleIndex) => {
		let rowNum = props.cardTable.getNumberOfRows(tableId);
		let formdata = props.form.getAllFormValue(formId);
		let values = formdata.rows[0].values;
		let pk_cardval = values.pk_card.value;
		if (pk_cardval) {
			let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
			let newrows = [];
			let bodysrow = cardData.body[tableId].rows;
			for (let bodyrow of bodysrow) {
				if (bodyrow.status != 3) {
					newrows.push(bodyrow);
				}
			}
			cardData.body[tableId].rows = newrows;

			ajax({
				url: url.addlineUrl,
				data: cardData,
				success: (res) => {
					if (res.success) {
						if (res.data) {
							if (res.data.body && res.data.body[tableId]) {
								let tableval = res.data.body[tableId].rows;
								let bodylen = tableval.length;
								let lastbody = tableval[bodylen - 1];
								let formusedeptval = props.form.getFormItemsValue(formId, 'usedept');
								lastbody.values.usedept.display = formusedeptval.display;
								lastbody.values.usedept.value = formusedeptval.value;
								props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
							}
						}
					}
				}
			});
		}
	};
	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: (props, id) => {
							buttonClick.call(this, props, id, tableId);
						},
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	render() {
		let { cardTable, form, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createModal } = ncmodal;

		return (
			<div id="nc-bill-card" className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID('201202512A-000000') /*国际化处理：资产拆分*/,
								formId,
								backBtnClick: backToList
							})}

							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>

					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: formAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: save.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this),
							showCheck: true,
							showIndex: true,
							modelAddRow: this.modelAddRow.bind(this),
							selectedChange: rowSelected.bind(this)
						})}
					</div>
				</div>
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/* 附件管理框 */}
				{createNCUploader(`${pagecode}-uploader`, {})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} /*国际化处理：指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

const MasterChildCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(MasterChildCard);

export default MasterChildCardBase;
