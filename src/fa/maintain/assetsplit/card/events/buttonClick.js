import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import formAfterEvent from './formAfterEvent';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils, CommonKeys } = commonConst;
const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = cardUtils;
const { LoginContext, ScriptReturnUtils } = components;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getContext, loginContextKeys } = LoginContext;
const { UISTATE } = StatusUtils;
const { approveConst } = CommonKeys;
import fa from 'fa';
const { fa_components } = fa;
const { ImageMng, ReferLinkage } = fa_components;
const { referAlterClear } = ReferLinkage;
const { faImageScan, faImageView } = ImageMng;

const {
	formId,
	tableId,
	pagecode,
	editBtns,
	browseBtns,
	pkField,
	url,
	printNodekey,
	listRouter,
	resourceCode,
	dataSource,
	bill_type,
	ds
} = pageConfig;
export default function(props, id) {
	switch (id) {
		case 'Save':
			save.call(this, props);
			break;
		case 'Add':
			add.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'AddLine':
			// 在最后一行增行
			addLine.call(this, props);
			break;
		case 'DelLine':
			// 删除最后一行
			delLine.call(this, props);
			break;
		case 'BatchAltert':
			BatchAltert.call(this, props);
			break;
		case 'BalanceAdjust':
			BalanceAdjust.call(this, props);
			break;
		case 'UnCommit':
			unCommit.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props);
			break;
		case 'SaveCommit':
			saveCommit.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'output':
			outputTemp.call(this, props);
			break;
		case 'FAReceiptScan':
			faReceiptScan.call(this, props);
			break;
		case 'FAReceiptShow':
			faReceiptShow.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		case 'QueryAboutBillFlow': // 审批详情
			queryAboutBillFlow.call(this, props);
			break;
		default:
			break;
	}
}
//影像扫描
function faReceiptScan(props) {
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageScan(props, imageData);
}

//影像查看
function faReceiptShow(props) {
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageView(props, imageData);
}

/** 
* 删除行
* @param {*} props 
*/
export function delLine(props) {
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(tableId, checkedIndex);
	setBatchBtnsEnable.call(this, props, tableId);
}
/**
* 保存前校验
* @param {*} props 
*/
function validateBeforeSave(props) {
	// 过滤空行
	props.cardTable.filterEmptyRows(tableId);
	// 表头必输校验
	let flag = props.form.isCheckNow(formId);
	if (!flag) {
		return false;
	}
	// 表体必输校验
	let bodyvalidate = props.cardTable.checkTableRequired(tableId);
	if (!bodyvalidate) {
		return false;
	}
	let allRows = props.cardTable.getAllRows(tableId, true);
	let business_date = props.form.getFormItemsValue(formId, 'business_date').value;
	let newallrows = [];
	for (let row of allRows) {
		if (row.status != 3) {
			newallrows.push(row);
		}
	}
	if (!newallrows || newallrows.length <= 1) {
		toast({ color: 'warning', content: getMultiLangByID('201202512A-000005') /*国际化处理：拆分后的卡片最少有2张！*/ });
		return false;
	}
	let datarow;
	let valuerow;
	let salarow;
	let raitrow;
	let equip = [];
	for (let i = 0; i < newallrows.length; i++) {
		let body_begin_date = newallrows[i].values.begin_date.value;
		let localoriginvalue = newallrows[i].values.localoriginvalue.value;
		let pk_equip = newallrows[i].values.pk_equip.value;
		let salvage = newallrows[i].values.salvage.value;
		let netrating = newallrows[i].values.netrating.value;
		if (body_begin_date > business_date) {
			let num = i + 1;
			if (!datarow) {
				datarow = num;
			} else {
				datarow = datarow + ',' + num;
			}
		}
		if (localoriginvalue < 0 || !localoriginvalue) {
			let num = i + 1;
			if (!valuerow) {
				valuerow = num;
			} else {
				valuerow = valuerow + ',' + num;
			}
		}
		if (!salvage || !netrating) {
			let num = i + 1;
			if (!salarow) {
				salarow = num;
			} else {
				salarow = salarow + ',' + num;
			}
		}
		if (Number(netrating) < Number(salvage)) {
			let num = i + 1;
			if (!raitrow) {
				raitrow = num;
			} else {
				raitrow = raitrow + ',' + num;
			}
		}
		if (pk_equip) {
			if (equip.indexOf(pk_equip) >= 0) {
				toast({
					color: 'warning',
					content: getMultiLangByID('201202512A-000006') /*国际化处理：'一个设备对应了多个固定资产卡片，请修改！'*/
				});
				return false;
			} else {
				equip.push(pk_equip);
			}
		}
	}
	if (datarow) {
		toast({
			color: 'warning',
			content: getMultiLangByID('201202512A-000007', {
				num: datarow
			}) /*国际化处理：'第' + datarow + '行表体开始使用日期不能晚于被拆分卡片的业务日期''*/
		});
		return false;
	}
	if (valuerow) {
		toast({
			color: 'warning',
			content: getMultiLangByID('201202512A-000008', {
				num: valuerow
			}) /*国际化处理：'第' + valuerow + '行本币原值不能为负数或者为空''*/
		});
		return false;
	}
	if (salarow) {
		toast({
			color: 'warning',
			content: getMultiLangByID('201202512A-000009', {
				num: salarow
			}) /*国际化处理：'第' + salarow + '行净额和净残值不能为空，请检查。'*/
		});
		return false;
	}
	if (raitrow) {
		toast({
			color: 'warning',
			content: getMultiLangByID('201202512A-000010', {
				num: raitrow
			}) /*国际化处理：'第' + raitrow + '行净额不能小于净残值，请检查。''*/
		});
		return false;
	}
	return true;
}
/**
 * 保存提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function saveCommit(props, content) {
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	let cardData = this.props.createMasterChildDataSimple(pagecode, formId, tableId);
	let bodysrow = cardData.body[tableId].rows;
	let headrow = cardData.head[formId].rows[0].values;
	let newrows = [];
	let wholeNum = 0;
	for (let bodyrow of bodysrow) {
		if (bodyrow.status != 3) {
			wholeNum = Number(wholeNum) + Number(bodyrow.values.card_num.value);
			newrows.push(bodyrow);
		}
	}
	cardData.body[tableId].rows = newrows;
	if (Number(headrow['pk_card.card_num'].value) != wholeNum && Number(headrow['pk_card.card_num'].value) != 1) {
		let numVal = wholeNum - Number(headrow['pk_card.card_num'].value);

		props.ncmodal.show(`${pagecode}-confirm`, {
			title: getMultiLangByID('201202512A-000011') /* 国际化处理：询问*/,
			content: getMultiLangByID('201202512A-000012', {
				num: numVal
			}) /* 国际化处理：'当前单据表体数量合计值不等于表头数量，相差' + numVal + '，是否继续保存？',*/,

			beSureBtnClick: () => {
				let oldstatus = props.form.getFormStatus(formId);

				let pk = props.form.getFormItemsValue(formId, pkField).value;
				let ts = props.form.getFormItemsValue(formId, 'ts').value;
				let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
				let paramInfoMap = {
					[pk]: ts
				};
				let obj = {
					dataType: 'cardData',
					OperatorType: 'SAVE',
					commitType: 'saveCommit',
					pageid: pagecode,
					paramInfoMap: paramInfoMap,
					content: content
				};
				CardData.userjson = JSON.stringify(obj);
				if (content) {
					ajax({
						url: url.commitUrl,
						data: CardData,
						success: (res) => {
							let { success, data } = res;
							if (success) {
								this.setState({
									compositedisplay: false
								});
								let callback = () => {
									if (data.success === approveConst.HASAPPROVEALTRAN) {
									} else {
										afterCommitSave.call(this, props, data.cardVos[0], oldstatus);
									}
									setBrowseBtnsVisible.call(this, props);
								};
								getScriptCardReturnData.call(
									this,
									res,
									this.props,
									formId,
									tableId,
									'pk_assetsplit',
									dataSource,
									null,
									false,
									callback
								);
							}
						}
					});
				} else {
					props.validateToSave(CardData, () => {
						ajax({
							url: url.commitUrl,
							data: CardData,
							success: (res) => {
								let { success, data } = res;

								if (success) {
									let callback = () => {
										if (data.success === approveConst.HASAPPROVEALTRAN) {
										} else {
											afterCommitSave.call(this, props, data.cardVos[0], oldstatus);
										}
										setBrowseBtnsVisible.call(this, props);
									};
									getScriptCardReturnData.call(
										this,
										res,
										this.props,
										formId,
										tableId,
										'pk_assetsplit',
										dataSource,
										null,
										false,
										callback
									);
								}
							}
						});
					});
				}
			},
			cancelBtnClick: () => {}
		});
	} else {
		ajax({
			url: url.balancecheck,
			data: cardData,
			success: (res) => {
				if (res.success) {
					if (res.data) {
						let message = '';
						let returnFlag = res.data;
						if (returnFlag.netFlag != 'Y') {
							message = getMultiLangByID('201202512A-000013', {
								message: message,
								num: returnFlag.netFlag
							}); /* 国际化处理：message + '当前单据表体项目净值合计值不等于表头项目净值,两者相差' + returnFlag.netFlag + '。\n',*/
						}
						if (returnFlag.originFlag != 'Y') {
							message = getMultiLangByID('201202512A-000014', {
								message: message,
								num: returnFlag.originFlag
							}); /* 国际化处理：message + '当前单据表体项目原币原值合计值不等于表头项目原币原值,两者相差' + returnFlag.originFlag + '。\n',*/
						}
						if (returnFlag.localFlag != 'Y') {
							message = getMultiLangByID('201202512A-000015', {
								message: message,
								num: returnFlag.localFlag
							}); /* 国际化处理： message + '当前单据表体项目本币原值合计值不等于表头项目本币原值,两者相差' + returnFlag.localFlag + '。\n';*/
						}
						if (returnFlag.netratingFlag != 'Y') {
							message = getMultiLangByID('201202512A-000016', {
								message: message,
								num: returnFlag.netratingFlag
							}); /* 国际化处理：message + '当前单据表体项目净额合计值不等于表头项目净额,两者相差' + returnFlag.netratingFlag + '。\n';\n';*/
						}
						if (returnFlag.currmoneyFlag != 'Y') {
							message = getMultiLangByID('201202512A-000017', {
								message: message,
								num: returnFlag.currmoneyFlag
							}); /* 国际化处理：message + '当前单据表体项目购买价款合计值不等于表头项目购买价款,两者相差' + returnFlag.currmoneyFlag + '。\n*/
						}

						if (returnFlag.other_costFlag != 'Y') {
							message = getMultiLangByID('201202512A-000018', {
								message: message,
								num: returnFlag.other_costFlag
							}); /* 国际化处理： message + '当前单据表体项目其他费用合计值不等于表头项目其他费用,两者相差' + returnFlag.other_costFlag + '。\n*/
						}
						if (returnFlag.tax_inputFlag != 'Y') {
							message = getMultiLangByID('201202512A-000019', {
								message: message,
								num: returnFlag.tax_inputFlag
							}); /* 国际化处理：  message + '当前单据表体项目进项税合计值不等于表头项目进项税,两者相差' + returnFlag.tax_inputFlag + '。\n'*/
						}
						if (returnFlag.predevaluateFlag != 'Y') {
							message = getMultiLangByID('201202512A-000020', {
								message: message,
								num: returnFlag.predevaluateFlag
							}); /* 国际化处理：message + '当前单据表体项目减值准备合计值不等于表头项目减值准备,两者相差' + returnFlag.predevaluateFlag + '。\n'*/
						}
						if (returnFlag.dismant_costFlag != 'Y') {
							message = getMultiLangByID('201202512A-000021', {
								message: message,
								num: returnFlag.dismant_costFlag
							}); /* 国际化处理：message + '当前单据表体项目弃置费用合计值不等于表头项目弃置费用,两者相差' + returnFlag.dismant_costFlag + '。\n'*/
						}
						if (returnFlag.accudepFlag != 'Y') {
							message = getMultiLangByID('201202512A-000022', {
								message: message,
								num: returnFlag.accudepFlag
							}); /* 国际化处理：message + '当前单据表体项目累计折旧合计值不等于表头项目累计折旧,两者相差' + returnFlag.accudepFlag + '。\n'*/
						}
						if (returnFlag.tax_costFlag != 'Y') {
							message = getMultiLangByID('201202512A-000023', {
								message: message,
								num: returnFlag.tax_costFlag
							}); /* 国际化处理：message + '当前单据表体项目相关税费合计值不等于表头项目相关税费,两者相差' + returnFlag.tax_costFlag + '。\n''*/
						}
						if (returnFlag.revalued_amountFlag != 'Y') {
							message = getMultiLangByID('201202512A-000024', {
								message: message,
								num: returnFlag.revalued_amountFlag
							}); /* 国际化处理：message + '当前单据表体项目评估值合计值不等于表头项目评估值,两者相差' + returnFlag.revalued_amountFlag + '。\n';*/
						}
						if (message) {
							message = getMultiLangByID('201202512A-000025', {
								message: message
							}); /* 国际化处理：message + '不能保存。请选择表体某行，然后点击差额调整按钮，将表头表体差额归结到该行，或者手工调整然后再进行保存。' + '。\n'*/

							let errorstr = message.split('。');
							let returnstr = JSON.stringify(errorstr);
							let resultstr = returnstr.substring(1, returnstr.length - 1);
							toast({ color: 'danger', content: resultstr });
							return;
						} else {
							let oldstatus = props.form.getFormStatus(formId);

							let pk = props.form.getFormItemsValue(formId, pkField).value;
							let ts = props.form.getFormItemsValue(formId, 'ts').value;
							let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
							let paramInfoMap = {
								[pk]: ts
							};
							let obj = {
								dataType: 'cardData',
								OperatorType: 'SAVE',
								commitType: 'saveCommit',
								pageid: pagecode,
								paramInfoMap: paramInfoMap,
								content: content
							};
							CardData.userjson = JSON.stringify(obj);
							if (content) {
								ajax({
									url: url.commitUrl,
									data: CardData,
									success: (res) => {
										let { success, data } = res;
										if (success) {
											this.setState({
												compositedisplay: false
											});
											let callback = () => {
												if (data.success === approveConst.HASAPPROVEALTRAN) {
												} else {
													afterCommitSave.call(this, props, data.cardVos[0], oldstatus);
												}
												setBrowseBtnsVisible.call(this, props);
											};
											getScriptCardReturnData.call(
												this,
												res,
												this.props,
												formId,
												tableId,
												'pk_assetsplit',
												dataSource,
												null,
												false,
												callback
											);
										}
									}
								});
							} else {
								props.validateToSave(CardData, () => {
									ajax({
										url: url.commitUrl,
										data: CardData,
										success: (res) => {
											let { success, data } = res;

											if (success) {
												let callback = () => {
													if (data.success === approveConst.HASAPPROVEALTRAN) {
													} else {
														afterCommitSave.call(this, props, data.cardVos[0], oldstatus);
													}
													setBrowseBtnsVisible.call(this, props);
												};
												getScriptCardReturnData.call(
													this,
													res,
													this.props,
													formId,
													tableId,
													'pk_assetsplit',
													dataSource,
													null,
													false,
													callback
												);
											}
										}
									});
								});
							}
						}
					}
				}
			}
		});
	}
}
/**
* 保存后
* @param {*} props 
*/
export function afterSave(props, data, oldstatus) {
	setValue.call(this, props, data);
	setStatus.call(this, props, UISTATE.browse);
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	// 保存成功后处理缓存
	let cachData = this.props.createMasterChildData(pagecode, formId, tableId);
	if (oldstatus == UISTATE.add) {
		cardCache.addCache(pk, cachData, formId, dataSource);
	} else {
		cardCache.updateCache(pkField, pk, cachData, formId, dataSource);
	}
	//getDataByPk.call(this, props, pk);
	showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
}
/**
* 保存提交后
* @param {*} props 
*/
export function afterCommitSave(props, data, oldstatus) {
	setStatus.call(this, props, UISTATE.browse);
}
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });

		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });

		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(formId, pkField);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: getMultiLangByID('201202512A-000000') /**国际化处理：资产合并 */, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

const BatchAltert = (props) => {
	//获取变化的值
	let changeData = props.cardTable.getTableItemData(tableId);
	let index = changeData.batchChangeIndex;
	let value = changeData.batchChangeValue;
	let display = changeData.batchChangeDisplay;
	if (changeData.batchChangeKey === 'pk_equip') {
		return;
	} else if (changeData.batchChangeKey === 'usedept') {
		let visiblerows = props.cardTable.getVisibleRows(tableId);
		let equip_orgval = visiblerows[index].values.pk_equiporg_v.value;
		for (let row of visiblerows) {
			let otherVal = row.values.pk_equiporg_v.value;
			if (equip_orgval != otherVal) {
				toast({
					color: 'warning',
					content: getMultiLangByID('201202512A-000031') /*国际化处理：当前批改字段所依赖字段存在不同的值，不支持批改操作！*/
				});
				return;
			}
		}
	} else if (changeData.batchChangeKey === 'pk_mandept') {
		let visiblerows = props.cardTable.getVisibleRows(tableId);
		let equip_orgval = visiblerows[index].values.pk_ownerorg_v.value;
		for (let row of visiblerows) {
			let otherVal = row.values.pk_ownerorg_v.value;
			if (equip_orgval != otherVal) {
				toast({
					color: 'warning',
					content: getMultiLangByID('201202512A-000031') /*国际化处理：当前批改字段所依赖字段存在不同的值，不支持批改操作！*/
				});
				return;
			}
		}
	} else if (changeData.batchChangeKey === 'pk_assetuser') {
		let visiblerows = props.cardTable.getVisibleRows(tableId);
		let equip_orgval = visiblerows[index].values.pk_equiporg_v.value;
		for (let row of visiblerows) {
			let otherVal = row.values.pk_equiporg_v.value;
			if (equip_orgval != otherVal) {
				toast({
					color: 'warning',
					content: getMultiLangByID('201202512A-000031') /*国际化处理：当前批改字段所依赖字段存在不同的值，不支持批改操作！*/
				});
				return;
			}
		}
	}
	//批改货主管理组织，清空管理部门
	if (changeData.batchChangeKey === 'pk_ownerorg_v') {
		referAlterClear(props, formId, tableId, 'pk_ownerorg_v', [ 'pk_mandept' ], index, value, display);
	} else if (changeData.batchChangeKey === 'pk_equiporg_v') {
		//批改使用权，清空使用部门，使用人，成本中心
		referAlterClear(
			props,
			formId,
			tableId,
			'pk_equiporg_v',
			[ 'usedept', 'pk_assetuser', 'pk_costcenter' ],
			index,
			value,
			display
		);
	}
	let returnData = props.cardTable.batchChangeTableData(tableId);
	let editCode = returnData.code;
	let afterArry = [
		'originvalue',
		'localoriginvalue',
		'predevaluate',
		'salvagerate',
		'accudep',
		'currmoney',
		'pk_ownerorg_v',
		'pk_equiporg_v',
		'pk_equip_usedept_v'
	];
	if (editCode != 'card_num') {
		let data = {
			code: editCode,
			value: returnData.value
		};
		props.cardTable.filterEmptyRows(tableId);
		let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
		CardData.userjson = JSON.stringify(data);
		ajax({
			url: url.BatchAltert,
			data: CardData,
			success: (res) => {
				if (res.success) {
					if (res.data) {
						if (res.data.body && res.data.body[tableId]) {
							props.cardTable.setTableData(tableId, res.data.body[tableId]);
						}
						if (res.data.userjson) {
							toast({ color: 'warning', content: res.data.userjson });
						}
					}
				}
			}
		});
	} else if (editCode === 'card_num') {
		let headVal = props.form.getFormItemsValue(formId, 'pk_card.card_num').value;
		let rownum = props.cardTable.getNumberOfRows(tableId);
		if (Number(headVal) === 1) {
			return;
		} else if ((Number(returnData.value) > Number(headVal) && Number(headVal) != 1) || !returnData.value) {
			let bodyallvalue = {
				originvalue: { value: null, display: null },
				localoriginvalue: { value: null, display: null },
				accudep: { value: null, display: null },
				netvalue: { value: null, display: null },
				netrating: { value: null, display: null },
				predevaluate: { value: null, display: null },
				currmoney: { value: null, display: null },
				tax_cost: { value: null, display: null },
				tax_input: { value: null, display: null },
				other_cost: { value: null, display: null },
				dismant_cost: { value: null, display: null },
				revalued_amount: { value: null, display: null },
				salvage: { value: null, display: null }
			};
			for (let i = 0; i < rownum; i++) {
				props.cardTable.setValByKeysAndIndex(tableId, i, bodyallvalue);
			}
			return;
		} else {
			let data = {
				code: editCode,
				value: returnData.value
			};
			props.cardTable.filterEmptyRows(tableId);
			let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
			CardData.userjson = JSON.stringify(data);
			ajax({
				url: url.BatchAltert,
				data: CardData,
				success: (res) => {
					if (res.success) {
						if (res.data) {
							if (res.data.body && res.data.body[tableId]) {
								props.cardTable.setTableData(tableId, res.data.body[tableId]);
							}
						}
					}
				}
			});
		}
	}
};
function refresh(props) {
	// 编辑态刷新，解锁卡片TODO
	let id = props.form.getFormItemsValue(formId, pkField);
	if (!id.value) {
		showMessage.call(this, props, { type: MsgConst.Type.RefreshFailed }); /* 国际化处理：'刷新失败'*/

		return;
	} else {
		showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		/* 国际化处理：'刷新成功'*/
	}
	getDataByPk.call(this, props, id.value);
}
/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk) {
	ajax({
		url: url.queryCardUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					cardCache.updateCache(pkField, pk, data, formId, dataSource);
				} else {
					setValue.call(this, props, undefined);
					cardCache.deleteCacheById(pkField, pk, dataSource);
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted }); /* 国际化处理：'此数据已删除！'*/
				}
			}
		}
	});
}
/**
 * 加锁，解锁卡片
 * @param {*} props 
 */
export function lockcards(props, lockflag, pk_card, async = true) {
	let data = props.form.getAllFormValue(formId);
	let allpks = [];
	if (pk_card) {
		allpks.push(pk_card);
	} else {
		// 取消时，解锁表体所有卡片，包括新增的，修改的
		let val = data.rows[0].values.pk_card.value;
		if (val) {
			allpks.push(val);
		}
	}
	let dSource = getContext(ds, dataSource);
	let usrid = getContext('userId', dataSource);
	let param = {
		allpks,
		msgMap: {
			usrid,
			lockflag,
			dSource
		}
	};
	if (allpks && allpks.length > 0 && usrid && dSource) {
		lockrequest.call(this, param, async);
	}
}
export function lockrequest(param, async = true) {
	ajax({
		url: '/nccloud/fa/facard/lockcard.do',
		data: param,
		async,
		success: (res) => {},
		error: (res) => {}
	});
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	// 单据编码,主键
	let billId = props.form.getFormItemsValue(formId, pkField).value;
	props.ncUploader.show(`${pagecode}-uploader`, {
		billId: 'fa/assetsplit/' + billId
	});
}
/**
* 新增
* @param {*} props 
*/
export function add(props) {
	props.cardTable.closeExpandedRow(tableId);
	setStatus.call(this, props, UISTATE.add);
	this.is_allow_dept_midlev = 'N';
	// 清空数据
	setValue.call(this, props, undefined);
	setDefaultValue.call(this, props);
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (pk_org_v && pk_org_v.value) {
		formAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		setBodyBtnsEnable.call(this, props, tableId);
	}
}
/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAltert', 'BalanceAdjust' ], true);
		return;
	}
	props.button.setButtonDisabled([ 'AddLine', 'BatchAltert', 'BalanceAdjust' ], false);
	let checkedRows = [];
	let num = props.cardTable.getNumberOfRows(moduleId, false);
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}
/**
 * 新增设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	// 模板默认值处理

	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId, dataSource);
	let groupName = getContext(loginContextKeys.groupName, dataSource);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype, dataSource);
	let pk_transitype = getContext(loginContextKeys.pk_transtype, dataSource);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org, dataSource);
	let org_Name = getContext(loginContextKeys.org_Name, dataSource);
	let pk_org_v = getContext(loginContextKeys.pk_org_v, dataSource);
	let org_v_Name = getContext(loginContextKeys.org_v_Name, dataSource);
	let businessDate = getContext(loginContextKeys.businessDate, dataSource);
	let usrid = getContext(loginContextKeys.userId, dataSource);
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		transi_type: { value: transi_type },
		bill_type: { value: bill_type },
		pk_transitype: { value: pk_transitype },
		business_date: { value: businessDate },
		bill_status: {
			value: '0',
			display: getMultiLangByID('statusUtils-000000') /*国际化处理：自由态*/
		}
	});
}
/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	// 更新参数
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	props.setUrlParam({ status, id: pkVal ? pkVal.value : '' });
	switch (status) {
		case UISTATE.add:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			break;
		case UISTATE.edit:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});

			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			setBrowseBtnsVisible.call(this, props);
			break;
		default:
			//console.log(status);
			break;
	}
	setHeadAreaData.call(this, props, { status });
}
/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	//先清空表头数据蛮清楚表头平台编辑后事件设置的值
	//props.form.EmptyAllFormValue(formId);
	setCardValue.call(this, props, data);
	if (status == UISTATE.browse) {
		setBrowseBtnsVisible.call(this, props);
	} else {
		setBodyBtnsEnable.call(this, props, tableId);
	}
}

/**
 * 浏览态设置按钮显示隐藏
 * @param {*} props 
 */
function setBrowseBtnsVisible(props) {
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	if (!pkVal || !pkVal.value) {
		props.button.setButtonVisible(browseBtns, false);
		props.button.setButtonVisible('Add', true);
		return;
	}
	props.button.setButtonVisible(browseBtns, true);
	setBillFlowBtnsVisible.call(this, props, formId, pkField);
}
/**
* 删除
* @param {*} props 
*/
export function delConfirm(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delReal });
}
/**
 * 删除
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function delReal(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'DELETE',
		commitType: 'commit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: CardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data.success === 'allsuccess') {
					// 清除缓存
					cardCache.deleteCacheById(pkField, pk, dataSource);
					toast({ content: data.successMsg, color: 'success' });
					// 加载下一条数据
					let newpk = cardCache.getNextId(pk, dataSource);
					loadDataByPk.call(this, props, newpk);
				} else {
					toast({ content: data.errorMsg, color: 'warning' });
				}
			}
		}
	});
}

export function updateDeptRefLevel(that, is_allow_dept_midlev_new) {
	if (is_allow_dept_midlev_new === undefined || is_allow_dept_midlev_new === null) {
		return;
	}
	that.is_allow_dept_midlev = is_allow_dept_midlev_new;
	let meta = that.props.meta.getMeta();
	if (is_allow_dept_midlev_new == 'N') {
		meta[tableId].items.map((item) => {
			// 管理部门
			if (item.attrcode === 'pk_mandept' || item.attrcode === 'pk_mandept_v') {
				item.onlyLeafCanSelect = true;
			}
			// 使用部门
			if (item.attrcode === 'usedept') {
				item.onlyLeafCanSelect = true;
			}
		});
	} else {
		meta[tableId].items.map((item) => {
			// 管理部门
			if (item.attrcode === 'pk_mandept' || item.attrcode === 'pk_mandept_v') {
				item.onlyLeafCanSelect = false;
			}
			// 使用部门
			if (item.attrcode === 'usedept') {
				item.onlyLeafCanSelect = false;
			}
		});
	}
	that.props.meta.setMeta(meta);
}
function cancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancel });
}
/**
* 修改
* @param {*} props 
*/
export function edit(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		pk = props.getUrlParam('id');
	}
	if (!pk) {
		return;
	}
	props.cardTable.closeExpandedRow(tableId);
	let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
	ajax({
		url: pageConfig.url.editUrl,
		data: {
			pk,
			resourceCode: resourceCode,
			pk_org: pk_org
		},
		success: (res) => {
			if (res.data.enableLeaf) {
				updateDeptRefLevel(this, res.data.enableLeaf);
			}
			if (res.data.result) {
				toast({ color: 'warning', content: res.data.result });
			} else {
				setStatus.call(this, props, UISTATE.edit);
				props.resMetaAfterPkorgEdit();
				props.form.setFormItemsDisabled(formId, { pk_org_v: true });
			}
		}
	});
}
/**
 * 取消
 * @param {*} props 
 */
export function cancel(props) {
	// 解锁卡片
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	lockcards.call(this, props, 'unlock');
	props.form.cancel(formId);
	props.cardTable.resetTableData(tableId);
	setStatus.call(this, props, UISTATE.browse);
	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理

	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}

	loadDataByPk.call(this, props, pk);
}
/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk) {
	if (!pk || pk == 'null') {
		setValue.call(this, props, undefined);
		return;
	}
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData) {
		setValue.call(this, props, cachData);
	} else {
		getDataByPk.call(this, props, pk);
	}
}
/**
* 新增行
* @param {*} props 
*/
export function addLine(props) {
	let rowNum = props.cardTable.getNumberOfRows(tableId);
	let formdata = props.form.getAllFormValue(formId);
	let values = formdata.rows[0].values;
	let pk_cardval = values.pk_card.value;
	if (pk_cardval) {
		props.cardTable.addRow(tableId, rowNum);

		let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
		let newrows = [];
		let bodysrow = cardData.body[tableId].rows;
		for (let bodyrow of bodysrow) {
			if (bodyrow.status != 3) {
				newrows.push(bodyrow);
			}
		}
		cardData.body[tableId].rows = newrows;

		ajax({
			url: url.addlineUrl,
			data: cardData,
			success: (res) => {
				if (res.success) {
					if (res.data) {
						if (res.data.body && res.data.body[tableId]) {
							let tableval = res.data.body[tableId].rows;
							let bodylen = tableval.length;
							let lastbody = tableval[bodylen - 1];
							let formusedeptval = props.form.getFormItemsValue(formId, 'usedept');
							lastbody.values.usedept.display = formusedeptval.display;
							lastbody.values.usedept.value = formusedeptval.value;
							props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
						}
					}
				}
			}
		});
	} else {
		props.cardTable.addRow(tableId, rowNum);
	}

	setBatchBtnsEnable.call(this, props, tableId);
}

export function afterAdd(props) {
	let rowNum = props.cardTable.getNumberOfRows(tableId);

	props.cardTable.addRow(tableId, rowNum, {}, false);
	props.cardTable.addRow(tableId, rowNum + 1, {}, false);

	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let newrows = [];
	let bodysrow = cardData.body[tableId].rows;
	for (let bodyrow of bodysrow) {
		if (bodyrow.status != 3) {
			newrows.push(bodyrow);
		}
	}
	cardData.body[tableId].rows = newrows;

	ajax({
		url: url.afteraddUrl,
		data: cardData,
		success: (res) => {
			if (res.success) {
				if (res.data) {
					if (res.data.body && res.data.body[tableId]) {
						let tableval = res.data.body[tableId].rows;
						let bodylen = tableval.length;
						let lastbody = tableval[bodylen - 1];
						let secondbody = tableval[bodylen - 2];
						let formusedeptval = props.form.getFormItemsValue(formId, 'usedept');
						lastbody.values.usedept.display = formusedeptval.display;
						lastbody.values.usedept.value = formusedeptval.value;
						secondbody.values.usedept.display = formusedeptval.display;
						secondbody.values.usedept.value = formusedeptval.value;
						props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
					}
				}
			}
		}
	});

	setBatchBtnsEnable.call(this, props, tableId);
}
/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	let num = props.cardTable.getNumberOfRows(moduleId);
	let checkedRows = [];
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	props.button.setButtonDisabled([ 'BatchAlter' ], num <= 0);
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}
/**
 * 差额调整
 * @param {*} props 
 */
export function BalanceAdjust(props) {
	let pk_card = props.form.getFormItemsValue(formId, 'pk_card');
	if (!pk_card.value) {
		toast({ color: 'warning', content: getMultiLangByID('201202512A-000026') /*国际化处理：'请先在表头选择被拆分卡片再进行差额调整！' */ });
		return;
	}
	let checkrow = props.cardTable.getCheckedRows(tableId);
	if (checkrow.length > 1) {
		toast({ color: 'warning', content: getMultiLangByID('201202512A-000027') /*国际化处理：'请只对一行做差额调整！' */ });
		return;
	}
	let index;

	//如果没选中，默认最后一行
	if (checkrow.length === 0) {
		let allrows = props.cardTable.getAllRows(tableId);
		let notdelrows = [];
		for (let row of allrows) {
			if (row.status != 3) {
				notdelrows.push(row);
			}
		}
		index = notdelrows.length - 1;
	} else {
		index = checkrow[0].index;
	}
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let datarow = cardData.body.bodyvos.rows;
	let deleterow = [];
	let newdatarow = [];
	for (let row of datarow) {
		if (row.status == 3) {
			deleterow.push(row);
		} else {
			newdatarow.push(row);
		}
	}
	cardData.body.bodyvos.rows = newdatarow;
	let tableData = props.cardTable.getAllRows(tableId);
	for (let i = 0; i < tableData.length; i++) {
		let cardNum = tableData[i].values.card_num.value;
		if ((cardNum === null || cardNum === '' || cardNum === undefined) && i != index && tableData[i].status != 3) {
			toast({ color: 'warning', content: getMultiLangByID('201202512A-000027') /*国际化处理：'请只对一行做差额调整！' */ });
			return;
		}
	}
	cardData.userjson = index;

	ajax({
		url: url.balancejustUrl,
		data: cardData,
		success: (res) => {
			if (res.success) {
				if (res.data) {
					if (res.data.body && res.data.body[tableId]) {
						let tablerow = res.data.body[tableId].rows;
						for (let delrow of deleterow) {
							tablerow.push(delrow);
						}
						props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}
				}
			}
		}
	});
}

/**
* 返回
* @param {*} props 
*/
export function backToList(props) {
	props.pushTo(listRouter, { pagecode: pagecode });
}

/**
* 保存
* @param {*} props 
*/
export function save(props) {
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	if (flag) {
		let cardData = this.props.createMasterChildDataSimple(pagecode, formId, tableId);
		let bodysrow = cardData.body[tableId].rows;
		let headrow = cardData.head[formId].rows[0].values;
		let newrows = [];
		let wholeNum = 0;
		for (let bodyrow of bodysrow) {
			if (bodyrow.status != 3) {
				wholeNum = Number(wholeNum) + Number(bodyrow.values.card_num.value);
				newrows.push(bodyrow);
			}
		}
		cardData.body[tableId].rows = newrows;
		if (Number(headrow['pk_card.card_num'].value) != wholeNum && Number(headrow['pk_card.card_num'].value) != 1) {
			let numVal = wholeNum - Number(headrow['pk_card.card_num'].value);

			props.ncmodal.show(`${pagecode}-confirm`, {
				title: getMultiLangByID('201202512A-000011') /* 国际化处理：询问*/,
				content: getMultiLangByID('201202512A-000012', {
					num: numVal
				}) /* 国际化处理：'当前单据表体数量合计值不等于表头数量，相差' + numVal + '，是否继续保存？',*/,

				beSureBtnClick: () => {
					saveok.call(this, props);
				},
				cancelBtnClick: () => {}
			});
		} else {
			ajax({
				url: url.balancecheck,
				data: cardData,
				success: (res) => {
					if (res.success) {
						if (res.data) {
							let message = '';
							let returnFlag = res.data;
							if (returnFlag.netFlag != 'Y') {
								message = getMultiLangByID('201202512A-000013', {
									message: message,
									num: returnFlag.netFlag
								}); /* 国际化处理：message + '当前单据表体项目净值合计值不等于表头项目净值,两者相差' + returnFlag.netFlag + '。\n',*/
							}
							if (returnFlag.originFlag != 'Y') {
								message = getMultiLangByID('201202512A-000014', {
									message: message,
									num: returnFlag.originFlag
								}); /* 国际化处理：message + '当前单据表体项目原币原值合计值不等于表头项目原币原值,两者相差' + returnFlag.originFlag + '。\n',*/
							}
							if (returnFlag.localFlag != 'Y') {
								message = getMultiLangByID('201202512A-000015', {
									message: message,
									num: returnFlag.localFlag
								}); /* 国际化处理： message + '当前单据表体项目本币原值合计值不等于表头项目本币原值,两者相差' + returnFlag.localFlag + '。\n';*/
							}
							if (returnFlag.netratingFlag != 'Y') {
								message = getMultiLangByID('201202512A-000016', {
									message: message,
									num: returnFlag.netratingFlag
								}); /* 国际化处理：message + '当前单据表体项目净额合计值不等于表头项目净额,两者相差' + returnFlag.netratingFlag + '。\n';\n';*/
							}
							if (returnFlag.currmoneyFlag != 'Y') {
								message = getMultiLangByID('201202512A-000017', {
									message: message,
									num: returnFlag.currmoneyFlag
								}); /* 国际化处理：message + '当前单据表体项目购买价款合计值不等于表头项目购买价款,两者相差' + returnFlag.currmoneyFlag + '。\n*/
							}

							if (returnFlag.other_costFlag != 'Y') {
								message = getMultiLangByID('201202512A-000018', {
									message: message,
									num: returnFlag.other_costFlag
								}); /* 国际化处理： message + '当前单据表体项目其他费用合计值不等于表头项目其他费用,两者相差' + returnFlag.other_costFlag + '。\n*/
							}
							if (returnFlag.tax_inputFlag != 'Y') {
								message = getMultiLangByID('201202512A-000019', {
									message: message,
									num: returnFlag.tax_inputFlag
								}); /* 国际化处理：  message + '当前单据表体项目进项税合计值不等于表头项目进项税,两者相差' + returnFlag.tax_inputFlag + '。\n'*/
							}
							if (returnFlag.predevaluateFlag != 'Y') {
								message = getMultiLangByID('201202512A-000020', {
									message: message,
									num: returnFlag.predevaluateFlag
								}); /* 国际化处理：message + '当前单据表体项目减值准备合计值不等于表头项目减值准备,两者相差' + returnFlag.predevaluateFlag + '。\n'*/
							}
							if (returnFlag.dismant_costFlag != 'Y') {
								message = getMultiLangByID('201202512A-000021', {
									message: message,
									num: returnFlag.dismant_costFlag
								}); /* 国际化处理：message + '当前单据表体项目弃置费用合计值不等于表头项目弃置费用,两者相差' + returnFlag.dismant_costFlag + '。\n'*/
							}
							if (returnFlag.accudepFlag != 'Y') {
								message = getMultiLangByID('201202512A-000022', {
									message: message,
									num: returnFlag.accudepFlag
								}); /* 国际化处理：message + '当前单据表体项目累计折旧合计值不等于表头项目累计折旧,两者相差' + returnFlag.accudepFlag + '。\n'*/
							}
							if (returnFlag.tax_costFlag != 'Y') {
								message = getMultiLangByID('201202512A-000023', {
									message: message,
									num: returnFlag.tax_costFlag
								}); /* 国际化处理：message + '当前单据表体项目相关税费合计值不等于表头项目相关税费,两者相差' + returnFlag.tax_costFlag + '。\n''*/
							}
							if (returnFlag.revalued_amountFlag != 'Y') {
								message = getMultiLangByID('201202512A-000024', {
									message: message,
									num: returnFlag.revalued_amountFlag
								}); /* 国际化处理：message + '当前单据表体项目评估值合计值不等于表头项目评估值,两者相差' + returnFlag.revalued_amountFlag + '。\n';*/
							}
							if (message) {
								message = getMultiLangByID('201202512A-000025', {
									message: message
								}); /* 国际化处理：message + '不能保存。请选择表体某行，然后点击差额调整按钮，将表头表体差额归结到该行，或者手工调整然后再进行保存。' + '。\n'*/

								//message = message + '不能保存。请选择表体某行，然后点击差额调整按钮，将表头表体差额归结到该行，或者手工调整然后再进行保存。\n';
								let errorstr = message.split('。');

								let returnstr = JSON.stringify(errorstr);
								let resultstr = returnstr.substring(1, returnstr.length - 1);
								toast({ color: 'danger', content: resultstr });
								return;
							} else {
								let oldstatus = props.form.getFormStatus(formId);
								let saveData = props.createMasterChildDataSimple(pagecode, formId, tableId);
								let reqUrl = url.insertUrl; //新增保存
								if (oldstatus === 'edit') {
									bodysrow.map((currow) => {
										if (!currow.values.pk_assetsplit_b.value) {
											currow.status = 2;
										}
										currow.values.pk_assetsplit = {
											value: this.props.form.getFormItemsValue(formId, pkField).value
										};
									});
									reqUrl = url.updateUrl; //修改保存
								}
								props.validateToSave(saveData, () => {
									ajax({
										url: reqUrl,
										data: saveData,
										success: (res) => {
											let { success, data } = res;
											if (success) {
												props.cardTable.closeModel(tableId);
												afterSave.call(this, props, data, oldstatus);
											}
										}
									});
								});
							}
						}
					}
				}
			});
		}
	}
}
function saveok(props) {
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let newrows = [];
	let bodysrow = cardData.body[tableId].rows;
	for (let bodyrow of bodysrow) {
		if (bodyrow.status != 3) {
			newrows.push(bodyrow);
		}
	}
	cardData.body[tableId].rows = newrows;
	ajax({
		url: url.balancecheck,
		data: cardData,
		success: (res) => {
			if (res.success) {
				if (res.data) {
					let message = '';
					let returnFlag = res.data;
					if (returnFlag.netFlag != 'Y') {
						message = getMultiLangByID('201202512A-000013', {
							message: message,
							num: returnFlag.netFlag
						}); /* 国际化处理：message + '当前单据表体项目净值合计值不等于表头项目净值,两者相差' + returnFlag.netFlag + '。\n',*/
					}
					if (returnFlag.originFlag != 'Y') {
						message = getMultiLangByID('201202512A-000014', {
							message: message,
							num: returnFlag.originFlag
						}); /* 国际化处理：message + '当前单据表体项目原币原值合计值不等于表头项目原币原值,两者相差' + returnFlag.originFlag + '。\n',*/
					}
					if (returnFlag.localFlag != 'Y') {
						message = getMultiLangByID('201202512A-000015', {
							message: message,
							num: returnFlag.localFlag
						}); /* 国际化处理： message + '当前单据表体项目本币原值合计值不等于表头项目本币原值,两者相差' + returnFlag.localFlag + '。\n';*/
					}
					if (returnFlag.netratingFlag != 'Y') {
						message = getMultiLangByID('201202512A-000016', {
							message: message,
							num: returnFlag.netratingFlag
						}); /* 国际化处理：message + '当前单据表体项目净额合计值不等于表头项目净额,两者相差' + returnFlag.netratingFlag + '。\n';\n';*/
					}
					if (returnFlag.currmoneyFlag != 'Y') {
						message = getMultiLangByID('201202512A-000017', {
							message: message,
							num: returnFlag.currmoneyFlag
						}); /* 国际化处理：message + '当前单据表体项目购买价款合计值不等于表头项目购买价款,两者相差' + returnFlag.currmoneyFlag + '。\n*/
					}

					if (returnFlag.other_costFlag != 'Y') {
						message = getMultiLangByID('201202512A-000018', {
							message: message,
							num: returnFlag.other_costFlag
						}); /* 国际化处理： message + '当前单据表体项目其他费用合计值不等于表头项目其他费用,两者相差' + returnFlag.other_costFlag + '。\n*/
					}
					if (returnFlag.tax_inputFlag != 'Y') {
						message = getMultiLangByID('201202512A-000019', {
							message: message,
							num: returnFlag.tax_inputFlag
						}); /* 国际化处理：  message + '当前单据表体项目进项税合计值不等于表头项目进项税,两者相差' + returnFlag.tax_inputFlag + '。\n'*/
					}
					if (returnFlag.predevaluateFlag != 'Y') {
						message = getMultiLangByID('201202512A-000020', {
							message: message,
							num: returnFlag.predevaluateFlag
						}); /* 国际化处理：message + '当前单据表体项目减值准备合计值不等于表头项目减值准备,两者相差' + returnFlag.predevaluateFlag + '。\n'*/
					}
					if (returnFlag.dismant_costFlag != 'Y') {
						message = getMultiLangByID('201202512A-000021', {
							message: message,
							num: returnFlag.dismant_costFlag
						}); /* 国际化处理：message + '当前单据表体项目弃置费用合计值不等于表头项目弃置费用,两者相差' + returnFlag.dismant_costFlag + '。\n'*/
					}
					if (returnFlag.accudepFlag != 'Y') {
						message = getMultiLangByID('201202512A-000022', {
							message: message,
							num: returnFlag.accudepFlag
						}); /* 国际化处理：message + '当前单据表体项目累计折旧合计值不等于表头项目累计折旧,两者相差' + returnFlag.accudepFlag + '。\n'*/
					}
					if (returnFlag.tax_costFlag != 'Y') {
						message = getMultiLangByID('201202512A-000023', {
							message: message,
							num: returnFlag.tax_costFlag
						}); /* 国际化处理：message + '当前单据表体项目相关税费合计值不等于表头项目相关税费,两者相差' + returnFlag.tax_costFlag + '。\n''*/
					}
					if (returnFlag.revalued_amountFlag != 'Y') {
						message = getMultiLangByID('201202512A-000024', {
							message: message,
							num: returnFlag.revalued_amountFlag
						}); /* 国际化处理：message + '当前单据表体项目评估值合计值不等于表头项目评估值,两者相差' + returnFlag.revalued_amountFlag + '。\n';*/
					}
					if (message) {
						message = getMultiLangByID('201202512A-000025', {
							message: message
						}); /* 国际化处理：message + '不能保存。请选择表体某行，然后点击差额调整按钮，将表头表体差额归结到该行，或者手工调整然后再进行保存。' + '。\n'*/
						//message = message + '不能保存。请选择表体某行，然后点击差额调整按钮，将表头表体差额归结到该行，或者手工调整然后再进行保存。\n';
						let errorstr = message.split('。');
						let returnstr = JSON.stringify(errorstr);
						let resultstr = returnstr.substring(1, returnstr.length - 1);
						toast({ color: 'danger', content: resultstr });
						return;
					} else {
						let oldstatus = props.form.getFormStatus(formId);
						let saveData = props.createMasterChildDataSimple(pagecode, formId, tableId);
						let reqUrl = url.insertUrl; //新增保存
						if (oldstatus === 'edit') {
							bodysrow.map((currow) => {
								if (!currow.values.pk_assetsplit_b.value) {
									currow.status = 2;
								}
								currow.values.pk_assetsplit = {
									value: this.props.form.getFormItemsValue(formId, pkField).value
								};
							});
							reqUrl = url.updateUrl; //修改保存
						}
						ajax({
							url: reqUrl,
							data: saveData,
							success: (res) => {
								let { success, data } = res;
								if (success) {
									afterSave.call(this, props, data, oldstatus);
								}
							}
						});
					}
				}
			}
		}
	});
}
/**
 * 提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function commit(props) {
	commitrunScript.call(this, props, 'SAVE', 'commit');
}

export function commitrunScript(props, operatorType, commitType, content) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};

	let obj = {
		dataType: 'cardData',
		OperatorType: operatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: CardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let callback = () => {
					setStatus.call(this, props, UISTATE.browse);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					null,
					false,
					callback
				);
			}
		},
		error: (res) => {
			if (res && res.message) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}
/**
 * 收回
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function unCommit(props) {
	runScript.call(this, props, 'UNSAVE', 'commit');
}

/**
 * 提交\收回
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function runScript(props, operatorType, commitType, content) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: operatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: CardData,
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			let { success, data } = res;
			if (success) {
				if (data.success === 'allsuccess') {
					let callback = () => {
						setBrowseBtnsVisible.call(this, props);
					};
					getScriptCardReturnData.call(
						this,
						res,
						this.props,
						formId,
						tableId,
						'pk_assetsplit',
						dataSource,
						null,
						false,
						callback
					);
				} else {
					toast({ content: data.errorMsg, color: 'warning' });
					setBrowseBtnsVisible.call(this, props);
				}
			}
		}
	});
}
/**
 * 审批详情
 * @param {*} props 
 */
function queryAboutBillFlow(props) {
	let id = props.form.getFormItemsValue(formId, pkField).value;
	let transi_type = props.form.getFormItemsValue(formId, 'transi_type').value;
	this.setState({
		showApprove: true,
		pk_bill: id,
		transi_type
	});
}
