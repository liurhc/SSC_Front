import { ajax, toast } from 'nc-lightapp-front';
import { addhideBtns } from './setBtnVisible';
import { pageConfig } from '../const';
import { setValue, afterAdd } from './buttonClick';
import { lockcards, updateDeptRefLevel } from './buttonClick';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { OrgChangeEvent } = components;
const { orgChangeEvent } = OrgChangeEvent;
const { formId, tableId, pagecode, bill_type } = pageConfig;

/**
 * 表头编辑后
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldvalue 
 * @param {*} val 
 */
export default function(props, moduleId, key, value, oldValue, val) {
	//createHeadAfterEventData 初始化为后台平台可以使用的格式
	let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	if (key == 'pk_org_v') {
		let callback = () => {
			ajax({
				url: pageConfig.url.headAfterEditUrl,
				data,
				success: (res) => {
					let is_allow_dept_midlev_new = res.data.userjson;
					res.data.userjson = null;
					setValue.call(this, props, res.data);
					updateDeptRefLevel(this, is_allow_dept_midlev_new);
				}
			});
		};
		orgChangeEvent.call(this, props, pagecode, formId, tableId, key, value, oldValue, false, addhideBtns, callback);
	}
	if (key === 'pk_card') {
		if (!value.value) {
			let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
			let business_date = props.form.getFormItemsValue(formId, 'business_date');
			let pk_group = props.form.getFormItemsValue(formId, 'pk_group');
			let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
			let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
			let pk_bill = props.form.getFormItemsValue(formId, 'pk_assetsplit');
			let pk_transitype = props.form.getFormItemsValue(formId, 'pk_transitype');
			let transi_type = props.form.getFormItemsValue(formId, 'transi_type');
			let dr = props.form.getFormItemsValue(formId, 'dr');
			let bill_code = props.form.getFormItemsValue(formId, 'bill_code');

			let ts = props.form.getFormItemsValue(formId, 'ts');
			props.form.EmptyAllFormValue(formId);
			props.form.setFormItemsValue(formId, {
				pk_group: { value: pk_group.value, display: pk_group.display },
				pk_org: { value: pk_org.value, display: pk_org.display },
				pk_org_v: { value: pk_org_v.value, display: pk_org_v.display },
				transi_type: { value: transi_type.value },
				bill_type: { value: bill_type },
				pk_transitype: { value: pk_transitype.value },
				business_date: { value: business_date.value },
				dr: { value: dr.value },
				bill_code: { value: bill_code.value },
				pk_assetsplit: { value: pk_bill.value },
				ts: { value: ts.value },
				bill_status: {
					value: bill_status.value,
					display: bill_status.display
				}
			});
			let uistatus = this.props.getUrlParam('status');
			if (uistatus == 'add') {
				props.cardTable.setTableData(tableId, { rows: [] });
			} else {
				let checkrows = props.cardTable.getAllRows(tableId);
				for (let i = 0; i < checkrows.length; i++) {
					props.cardTable.setRowStatusByIndexs(tableId, [ { index: i, status: '3' } ]);
				}
				props.cardTable.setTableData(tableId, { rows: checkrows });
			}

			if (oldValue.value) {
				lockcards.call(this, props, 'unlock', oldValue.value);
			}

			return;
		} else {
			if (oldValue.value) {
				lockcards.call(this, props, 'unlock', oldValue.value);
			}
		}
		let checkdata = {};
		checkdata.id = value.value;
		ajax({
			url: pageConfig.url.checkmethodUrl,
			data: checkdata,
			success: (res) => {
				let result = res.data.result;
				if (result) {
					props.ncmodal.show(`${pagecode}-confirm`, {
						title: getMultiLangByID('201202512A-000011') /*国际化处理：'询问'*/,
						content: getMultiLangByID(
							'201202512A-000029' /*国际化处理：'您卡片的折旧方法为工作量法，需要预先录入本月工作量，才能准确地计算预提折旧。请确认卡片是否已经录入了本月工作量，如果尚未录入，请到折旧与摊销节点录入本月工作量，然后再进行本次操作？',*/
						),

						beSureBtnClick: () => {
							let cardData = props.createHeadAfterEventData(
								pagecode,
								formId,
								tableId,
								moduleId,
								key,
								value
							);
							ajax({
								url: pageConfig.url.headAfterEditUrl,
								data: cardData,
								success: (res) => {
									if (res.data) {
										let values = res.data.head.card_head.rows[0].values;
										if (res.data.body) {
											let bodyrows = res.data.body.bodyvos.rows;

											for (let body of bodyrows) {
												if (body.status != 3) {
													body.values.usedept.display = values['usedept'].display;
													body.values.usedept.value = values['usedept'].value;
												}
											}
										}
										setValue.call(this, props, res.data);
										afterAdd.call(this, props);
									}
								},
								error: (res) => {
									toast({ color: 'danger', content: res.message });
									let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
									let business_date = props.form.getFormItemsValue(formId, 'business_date');
									let pk_group = props.form.getFormItemsValue(formId, 'pk_group');
									let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
									let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
									let pk_transitype = props.form.getFormItemsValue(formId, 'pk_transitype');
									let transi_type = props.form.getFormItemsValue(formId, 'transi_type');
									let dr = props.form.getFormItemsValue(formId, 'dr');
									let pk_bill = props.form.getFormItemsValue(formId, 'pk_assetsplit');
									let bill_code = props.form.getFormItemsValue(formId, 'bill_code');
									let ts = props.form.getFormItemsValue(formId, 'ts');
									props.form.EmptyAllFormValue(formId);
									props.form.setFormItemsValue(formId, {
										pk_group: { value: pk_group.value, display: pk_group.display },
										pk_org: { value: pk_org.value, display: pk_org.display },
										pk_org_v: { value: pk_org_v.value, display: pk_org_v.display },
										transi_type: { value: transi_type.value },
										bill_type: { value: bill_type },
										pk_transitype: { value: pk_transitype.value },
										business_date: { value: business_date.value },
										ts: { value: ts.value },
										bill_code: { value: bill_code.value },
										pk_assetsplit: { value: pk_bill.value },
										dr: { value: dr.value },
										bill_status: {
											value: bill_status.value,
											display: bill_status.display
										}
									});

									let uistatus = this.props.getUrlParam('status');
									if (uistatus == 'add') {
										props.cardTable.setTableData(tableId, { rows: [] });
									} else {
										let checkrows = props.cardTable.getAllRows(tableId);
										for (let i = 0; i < checkrows.length; i++) {
											props.cardTable.setRowStatusByIndexs(tableId, [
												{ index: i, status: '3' }
											]);
										}
										props.cardTable.setTableData(tableId, { rows: checkrows });
									}
								}
							});
						},
						cancelBtnClick: () => {
							let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
							let business_date = props.form.getFormItemsValue(formId, 'business_date');
							let pk_group = props.form.getFormItemsValue(formId, 'pk_group');
							let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
							let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
							let pk_transitype = props.form.getFormItemsValue(formId, 'pk_transitype');
							let transi_type = props.form.getFormItemsValue(formId, 'transi_type');
							let dr = props.form.getFormItemsValue(formId, 'dr');
							let pk_bill = props.form.getFormItemsValue(formId, 'pk_assetsplit');
							let ts = props.form.getFormItemsValue(formId, 'ts');
							let bill_code = props.form.getFormItemsValue(formId, 'bill_code');
							props.form.EmptyAllFormValue(formId);
							props.form.setFormItemsValue(formId, {
								pk_group: { value: pk_group.value, display: pk_group.display },
								pk_org: { value: pk_org.value, display: pk_org.display },
								pk_org_v: { value: pk_org_v.value, display: pk_org_v.display },
								transi_type: { value: transi_type.value },
								bill_type: { value: bill_type },
								pk_transitype: { value: pk_transitype.value },
								business_date: { value: business_date.value },
								ts: { value: ts.value },
								pk_assetsplit: { value: pk_bill.value },
								dr: { value: dr.value },
								bill_code: { value: bill_code.value },
								bill_status: {
									value: bill_status.value,
									display: bill_status.display
								}
							});

							let uistatus = this.props.getUrlParam('status');
							if (uistatus == 'add') {
								props.cardTable.setTableData(tableId, { rows: [] });
							} else {
								let checkrows = props.cardTable.getAllRows(tableId);
								for (let i = 0; i < checkrows.length; i++) {
									props.cardTable.setRowStatusByIndexs(tableId, [ { index: i, status: '3' } ]);
								}
								props.cardTable.setTableData(tableId, { rows: checkrows });
							}
							let pkval = oldValue.value;
							if (pkval) {
								lockcards.call(this, this.props, 'unlock', pkval);
							}
						}
					});
				} else {
					let cardData = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
					ajax({
						url: pageConfig.url.headAfterEditUrl,
						data: cardData,
						success: (res) => {
							if (res.data) {
								let values = res.data.head.card_head.rows[0].values;
								if (res.data.body) {
									let bodyrows = res.data.body.bodyvos.rows;

									for (let body of bodyrows) {
										if (body.status != 3) {
											body.values.usedept.display = values['usedept'].display;
											body.values.usedept.value = values['usedept'].value;
										}
									}
								}

								setValue.call(this, props, res.data);
								afterAdd.call(this, props);
							}
						},
						error: (res) => {
							toast({ color: 'danger', content: res.message });
							let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
							let business_date = props.form.getFormItemsValue(formId, 'business_date');
							let pk_group = props.form.getFormItemsValue(formId, 'pk_group');
							let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
							let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
							let pk_transitype = props.form.getFormItemsValue(formId, 'pk_transitype');
							let transi_type = props.form.getFormItemsValue(formId, 'transi_type');
							let dr = props.form.getFormItemsValue(formId, 'dr');
							let pk_bill = props.form.getFormItemsValue(formId, 'pk_assetsplit');
							let bill_code = props.form.getFormItemsValue(formId, 'bill_code');
							let ts = props.form.getFormItemsValue(formId, 'ts');
							props.form.EmptyAllFormValue(formId);
							props.form.setFormItemsValue(formId, {
								pk_group: { value: pk_group.value, display: pk_group.display },
								pk_org: { value: pk_org.value, display: pk_org.display },
								pk_org_v: { value: pk_org_v.value, display: pk_org_v.display },
								transi_type: { value: transi_type.value },
								bill_type: { value: bill_type },
								pk_transitype: { value: pk_transitype.value },
								business_date: { value: business_date.value },
								dr: { value: dr.value },
								bill_code: { value: bill_code.value },
								pk_assetsplit: { value: pk_bill.value },
								ts: { value: ts.value },
								bill_status: {
									value: bill_status.value,
									display: bill_status.display
								}
							});
							let uistatus = this.props.getUrlParam('status');
							if (uistatus == 'add') {
								props.cardTable.setTableData(tableId, { rows: [] });
							} else {
								let checkrows = props.cardTable.getAllRows(tableId);
								for (let j = 0; j < checkrows.length; j++) {
									props.cardTable.setRowStatusByIndexs(tableId, [ { index: j, status: '3' } ]);
								}
								props.cardTable.setTableData(tableId, { rows: checkrows });
							}
						}
					});
				}
			}
		});
	}
}
