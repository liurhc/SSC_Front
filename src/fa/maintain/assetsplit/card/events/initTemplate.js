import { tableButtonClick } from '../events';
import buttonClick, { setStatus } from './buttonClick';
import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { cardUtils } = utils;
const { StatusUtils } = commonConst;
const { createOprationColumn } = cardUtils;
const { LoginContext, faQueryAboutUtils } = components;
const { openAssetCardByPk } = faQueryAboutUtils;
const { getContext, loginContext } = LoginContext;
const { UISTATE } = StatusUtils;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addHeadAreaReferFilter, addBodyReferFilter } = ReferFilter;
const { formId, tableId, pagecode, searchAreaId, body_edit, dataSource, ds } = pageConfig;

let defaultConfig = {
	searchId: searchAreaId,
	formId: formId,
	bodyIds: [ tableId, body_edit ],
	defPrefix: {
		search: [ 'def', 'bodyvos.def' ],
		head: 'def',
		body: 'def'
	},
	specialFields: {
		//货主管理组织
		pk_ownerorg_v: {
			addOrgRelation: 'pk_orgs',
			RefActionExtType: 'TreeRefActionExt',
			class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_OWNERORG_VSqlBuilder', //货主管理组织
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		}
	}
};
export default function(props) {
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, dataSource);
					// 获取数据源
					getContext(ds, dataSource);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta(props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	addHeadAreaReferFilter.call(this, props, meta, defaultConfig);
	addBodyReferFilter.call(this, props, meta, defaultConfig);
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	// 表头参照参数过滤
	meta[formId].items.map((item) => {
		// 卡片超链,过滤
		if (item.attrcode == 'pk_card') {
			item.renderStatus = 'browse';
			item.render = (cardval, record) => {
				return (
					<a
						className="code-detail-link"
						onClick={() => {
							openAssetCardByPk.call(this, props, cardval.value);
						}}
					>
						{cardval && cardval.display}
					</a>
				);
			};
			item.isMultiSelectedEnabled = false;
		}
	});
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_equip') {
			item.itemtype = 'refer';
			item.metadataProperty = 'aim.EquipHeadVO.equip_code';
			item.refcode = 'aim/refer/equip/EquipGridRef/index';
			item.maxlength = '20';
			item.queryCondition = () => {
				let cardVal = props.form.getFormItemsValue(formId, 'pk_card').value;
				return {
					GridRefActionExt: 'nccloud.web.fa.common.refCondition.AssetSplitEquipRefFilter',
					fa_card: cardVal
				};
			};
		}
		if (item.attrcode == 'asset_code') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				if (record.values.pk_card.value) {
					return (
						<span
							className="code-detail-link"
							onClick={() => {
								openAssetCardByPk.call(this, props, record.values.pk_card.value);
							}}
						>
							{record && record.values.asset_code && record.values.asset_code.value}
						</span>
					);
				}
			};
		}
	});
	meta[body_edit].items.map((item) => {
		if (item.attrcode == 'pk_equip') {
			item.itemtype = 'refer';
			item.metadataProperty = 'aim.EquipHeadVO.equip_code';
			item.refcode = 'aim/refer/equip/EquipGridRef/index';
			item.maxlength = '20';
			item.queryCondition = () => {
				let cardVal = props.form.getFormItemsValue(formId, 'pk_card').value;
				return {
					GridRefActionExt: 'nccloud.web.fa.common.refCondition.AssetSplitEquipRefFilter',
					fa_card: cardVal
				};
			};
		}
	});

	let material_event = createOprationColumn.call(this, props, { tableButtonClick });
	meta[tableId].items.push(material_event);
	return meta;
}
/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status') || UISTATE.browse;
	if (status == UISTATE.add) {
		buttonClick.call(this, props, 'Add');
	} else {
		setStatus.call(this, props, status);
	}
}
