import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
// import { updateBodyData } from '../../../../../ampub/common/utils/cardUtils';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { updateBodyData } = cardUtils;

const { formId, tableId, pagecode } = pageConfig;

export default function afterEvent(props, moduleId, key, value, changedrows, index, record, type, method) {
	let pk_card = props.form.getFormItemsValue(formId, 'pk_card').value;
	if (!pk_card) {
		toast({ color: 'warning', content: getMultiLangByID('201202512A-000002') /*国际化处理：'请先选择一张固定资产卡片'*/ });
		props.cardTable.setValByKeyAndIndex(moduleId, index, key, { value: null, display: null });
		return;
	}
	let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedrows);
	data.index = index;
	if (key === 'pk_equiporg_v') {
		if (!value) {
			return;
		}
		props.cardTable.setValByKeyAndIndex(moduleId, index, 'usedept', { value: null, display: null });
		props.cardTable.setValByKeyAndIndex(moduleId, index, 'pk_assetuser', { value: null, display: null });
	}
	if (key === 'pk_mandept') {
		if (!value.refname) {
			props.cardTable.setValByKeyAndIndex(moduleId, index, 'pk_mandept_v', { value: null, display: null });
		}
	}
	if (key === 'pk_ownerorg_v') {
		if (!value) {
			return;
		}
		props.cardTable.setValByKeyAndIndex(moduleId, index, 'pk_mandept', { value: null, display: null });
	}
	if (key === 'pk_equip') {
		if (!value) {
			return;
		}
		let name = value.refname;
		if (name) {
			props.cardTable.setValByKeyAndIndex(moduleId, index, 'equip_name', { value: name, display: name });
		} else {
			props.cardTable.setValByKeyAndIndex(moduleId, index, 'equip_name', { value: null, display: null });
		}
	}
	if (
		key === 'originvalue' ||
		key === 'localoriginvalue' ||
		key === 'predevaluate' ||
		key === 'accudep' ||
		key === 'currmoney'
	) {
		ajax({
			url: pageConfig.url.bodyAfterEditUrl,
			data: data,
			async: false,
			success: (res) => {
				if (res.data && res.data.body) {
					let { data } = res;
					updateBodyData.call(this, props, { data, tableId });
				}
			}
		});
	}

	if (key === 'card_num') {
		if (!value) {
			return;
		}
		let headVal = props.form.getFormItemsValue(formId, 'pk_card.card_num').value;
		if (value) {
			if (value.indexOf('-') != -1) {
				setTimeout(() => {
					props.cardTable.setValByKeyAndIndex(tableId, index, 'card_num', { value: null });
				}, 0);

				toast({ color: 'warning', content: getMultiLangByID('201202512A-000003') /*国际化处理：'数量字段不能是负数!' */ });
				return;
			}
			if (value.indexOf('.') != -1) {
				props.cardTable.setValByKeyAndIndex(tableId, 'card_num', { value: null, display: null });
				toast({ color: 'warning', content: getMultiLangByID('201202512A-000004') /*国际化处理：'数量字段不能是小数!'*/ });
				return;
			}
		}
		if ((Number(value) > Number(headVal) && Number(headVal) != 1) || !value) {
			let bodyallvalue = {
				originvalue: { value: null, display: null },
				localoriginvalue: { value: null, display: null },
				accudep: { value: null, display: null },
				netvalue: { value: null, display: null },
				netrating: { value: null, display: null },
				predevaluate: { value: null, display: null },
				currmoney: { value: null, display: null },
				tax_cost: { value: null, display: null },
				tax_input: { value: null, display: null },
				other_cost: { value: null, display: null },
				dismant_cost: { value: null, display: null },
				revalued_amount: { value: null, display: null },
				salvage: { value: null, display: null }
			};
			setTimeout(() => {
				props.cardTable.setValByKeysAndIndex(tableId, index, bodyallvalue);
			}, 0);

			return;
		}
		if (Number(headVal) === 1) {
			return;
		}
		ajax({
			url: pageConfig.url.bodyAfterEditUrl,
			data: data,
			async: false,
			success: (res) => {
				if (res.data && res.data.body) {
					let { data } = res;
					updateBodyData.call(this, props, { data, tableId });
				}
			}
		});
	}

	if (key === 'business_date') {
		if (!value) {
			return;
		}
		let rows = props.cardTable.getAllRows(tableId);
		for (let i = 0; i < rows.length; i++) {
			props.cardTable.setValByKeyAndIndex(tableId, i, 'business_date', { value: value, display: value });
		}
	}

	if (
		key === 'salvage' ||
		key === 'salvagerate' ||
		key === 'pk_category' ||
		key === 'begin_date' ||
		key === 'pk_depmethod'
	) {
		if (JSON.stringify(value) == '{}' || !value) {
			return;
		}
		let cardData = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedrows, index);
		let newRecord = JSON.parse(JSON.stringify(record || {}));
		cardData.card.body[moduleId] = {
			...cardData.card.body[moduleId],
			rows: [ newRecord ]
		};
		cardData.index = 0; //修改编辑行为0
		if (key === 'pk_depmethod') {
			props.cardTable.setEditableByIndex(moduleId, index, [ 'dep_start_date' ], true);
		}
		ajax({
			url: pageConfig.url.bodyAfterEditUrl,
			data: cardData,
			async: false,
			success: (res) => {
				if (res.data) {
					//新增功能---差异化处理
					props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
					let userObj = JSON.parse(res.data.userjson);
					if (userObj && userObj.alterMsg) {
						toast({ color: 'warning', content: userObj.alterMsg });
					}
				}
			}
		});
	}
}
