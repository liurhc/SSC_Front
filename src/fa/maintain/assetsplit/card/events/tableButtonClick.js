import { pageConfig } from '../const';
const { tableId } = pageConfig;

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'OpenCard':
			let status = props.cardTable.getStatus(tableId);
			if (status === 'browse') {
				props.cardTable.toggleRowView(tableId, record);
			} else {
				props.cardTable.setClickRowIndex(tableId, { record, index });
				props.cardTable.openModel(tableId, 'edit', record, index);
			}
			break;
		case 'DelLine':
			props.cardTable.delRowsByIndex(tableId, index);
			break;
	}
}
