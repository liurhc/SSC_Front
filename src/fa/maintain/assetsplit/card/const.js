// 页面配置
export const pageConfig = {
	// 应用主键
	// 应用编码
	appcode: '201202512A',
	// 应用名称
	//title: '资产拆分',
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表单区域id
	formId: 'card_head',
	// 表格区域id
	tableId: 'bodyvos',
	//编辑区id
	body_edit: 'bodyvos_edit',
	// 页面编码
	pagecode: '201202512A_card',
	//printFilename: '资产拆分',
	// 编辑态按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'OpenEdit', 'AddLine', 'DelLine', 'BatchAltert', 'BalanceAdjust' ],
	// 浏览态按钮
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'OpenBrowse',
		'Print',
		'ReceiptGroup',
		'Refresh',
		'QueryAbout'
	],
	addhideBtns: [ 'AddLine', 'DelLine', 'BatchAltert', 'BalanceAdjust' ],
	// 主键字段
	pkField: 'pk_assetsplit',
	// 单据类型
	bill_type: 'HM',
	// 交易类型
	transi_type: 'HM-01',
	// 请求链接
	url: {
		queryCardUrl: '/nccloud/fa/assetsplit/querycard.do',
		insertUrl: '/nccloud/fa/assetsplit/save.do',
		updateUrl: '/nccloud/fa/assetsplit/update.do',
		editUrl: '/nccloud/fa/assetsplit/edit.do',
		printUrl: '/nccloud/fa/assetsplit/printCard.do',
		commitUrl: '/nccloud/fa/assetsplit/commit.do',
		deleteUrl: '/nccloud/fa/assetsplit/delete.do',
		//unCommitUrl: '/nccloud/aum/sale/unCommit.do',
		headAfterEditUrl: '/nccloud/fa/assetsplit/headAfterEdit.do',
		bodyAfterEditUrl: '/nccloud/fa/assetsplit/bodyAfterEdit.do',
		bodyBeforeEditUrl: '/nccloud/fa/assetsplit/bodyBeforeEdit.do',
		deleteLine: '/nccloud/fa/assetsplit/delline.do',
		balancecheck: '/nccloud/fa/assetsplit/balancecheck.do',
		BatchAltert: '/nccloud/fa/assetsplit/batchAltert.do',
		balancejustUrl: '/nccloud/fa/assetsplit/balanceadjust.do',
		checkmethodUrl: '/nccloud/fa/assetsplit/checkmethod.do',
		addlineUrl: '/nccloud/fa/assetsplit/addline.do',
		afteraddUrl: '/nccloud/fa/assetsplit/afteradd.do'
	},
	// 打印模板节点标识
	printNodekey: null,
	listUrl: '/fa/maintain/assetsplit/list/index.html',
	cardUrl: '/fa/maintain/assetsplit/card/index.html',
	// 权限资源编码
	resourceCode: '2012040010',
	listRouter: '/list',
	dataSource: 'fa.maintain.assetsplit.main',
	ds: 'dataSource'
};
