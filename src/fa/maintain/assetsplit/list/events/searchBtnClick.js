import { ajax } from 'nc-lightapp-front';
import { setBatchBtnsEnable } from '../events';
import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils, components } = ampub;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;
const { LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

const { searchAreaId, tableId, pagecode, bill_type, dataSource } = pageConfig;
//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	if (searchVal) {
		// 获取交易类型编码
		let transi_type = getContext(loginContextKeys.transtype, dataSource);
		if (!queryInfo) {
			queryInfo = props.search.getQueryInfo(searchAreaId);
		}
		let pageInfo = props.table.getTablePageInfo(tableId);
		queryInfo.pageInfo = pageInfo;
		queryInfo.pagecode = pagecode;
		queryInfo.billtype = bill_type;
		queryInfo.transtype = transi_type;

		// 缓存查询条件
		setQueryInfoCache.call(this, queryInfo, isRefresh, type);
		ajax({
			url: pageConfig.url.queryUrl,
			data: queryInfo,
			success: (res) => {
				setListValue.call(
					this,
					props,
					res,
					tableId,
					isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
				);
				setBatchBtnsEnable.call(this, props, tableId);
			}
		});
	}
}
