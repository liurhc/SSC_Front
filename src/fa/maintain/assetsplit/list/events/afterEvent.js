import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage } = fa_components;
const { referLinkageClear } = ReferLinkage;
export default function afterEvent(field, val) {
	const defaultConfig = {
		//查询区
		searchId: 'searchArea',
		linkageData: {
			pk_org: [
				'pk_accbook',
				'pk_equiporg',
				'pk_assetuser',
				'pk_ownerorg',
				'pk_mandept',
				'bodyvos.pk_equip_usedept',
				'pk_card',
				'pk_costcenter',
				'usedept',
				'bodyvos.pk_accbook',
				'bodyvos.pk_equiporg',
				'bodyvos.pk_assetuser',
				'bodyvos.pk_ownerorg',
				'bodyvos.pk_mandept',
				'bodyvos.pk_card',
				'bodyvos.pk_costcenter',
				'bodyvos.usedept'
			],
			pk_equiporg: [
				'pk_costcenter',
				'usedept',
				'pk_assetuser',
				'bodyvos.pk_costcenter',
				'bodyvos.usedept',
				'bodyvos.pk_assetuser'
			],
			'bodyvos.pk_equiporg': [ 'bodyvos.pk_costcenter', 'bodyvos.usedept', 'bodyvos.pk_assetuser' ],
			pk_ownerorg: [ 'pk_mandept', 'bodyvos.pk_mandept' ],
			'bodyvos.pk_ownerorg': [ 'bodyvos.pk_mandept' ]
		}
	};
	referLinkageClear.call(this, this.props, field, defaultConfig);
}
