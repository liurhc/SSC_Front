import { ajax, toast, print, output } from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
import { pageConfig } from '../const';
const { tableId, pagecode, pkField, url, printNodekey, cardRouter, dataSource } = pageConfig;

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, msgUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { batchRefresh } = listUtils;
const { ScriptReturnUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { showConfirm, MsgConst } = msgUtils;

export default function buttonClick(props, id) {
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'UnCommit':
			unCommit.call(this, props, 'UNSAVE');
			break;
		case 'Commit':
			commit.call(this, props, 'SAVE');
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		default:
			// console.log('buttonClick', id);
			break;
	}
}
/**
 * 刷新
 * @param {} props 
 */
const refresh = (props) => {
	batchRefresh.call(this, props, searchBtnClick);
};

export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		toast({ content: getMultiLangByID('msgUtils-000021') /*国际化处理：'请选择需要打印的数据'*/, color: 'warning' });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		pageConfig.url.printUrl, // 后台打印服务url
		printData
	);
}
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		toast({ content: getMultiLangByID('msgUtils-000022') /*国际化处理：'请选择需要输出的数据'*/, color: 'warning' });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[pkField].value);
	});
	let printData = {
		filename: getMultiLangByID('201202512A-000000' /*国际化处理：资产拆分*/), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	if (!checkedrows || checkedrows.length == 0) {
		toast({ content: getMultiLangByID('msgUtils-000025') /*国际化处理：'请选择一行数据！'*/, color: 'warning' });
		return;
	}
	// billNo 是单据主键
	let billId = checkedrows[0].data.values[pkField].value;
	props.ncUploader.show(`${pagecode}-uploader`, {
		billId: 'fa/assetsplit/' + billId
	});
}

/**
 * 新增
 * @param {*} props 
 */
export function add(props) {
	linkToCard.call(this, props, undefined, 'add');
}

/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	props.pushTo(cardRouter, {
		status,
		pagecode: '201202512A_card',
		id: record[pkField] ? record[pkField].value : ''
	});
}

/**
 * 批量删除
 * @param {*} props 
 */
export function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		toast({ content: getMultiLangByID('msgUtils-000023') /*国际化处理：'请先选中需要删除的数据''*/, color: 'warning' });
		return;
	}

	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: delReal });
}

/**
 * 提交
 * 
 * @param {*} props 
 */
export function runScript(props, actionType, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pkField].value;
		let ts = v.data.values['ts'].value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: pageConfig.url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: actionType,
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit', //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			let isDelete = actionType == 'DELETE';
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				isDelete,
				dataSource
			);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'warning' });
			}
		}
	});
}

/**
 * 删除
 * 
 * @param {*} props 
 */
function delReal(props) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pkField].value;
		let ts = v.data.values['ts'].value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: url.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit'
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, pkField, tableId.replace('list', 'card'), tableId, true);
			// 平台还没有支持批量，暂时用单个循环
			params.map((item) => {
				props.table.deleteCacheId(tableId, item.id);
			});
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'warning' });
			}
		}
	});
}
/**
 * 提交
 * 
 * @param {*} props 
 */
function commit(props) {
	runScript.call(this, props, 'SAVE');
}
/**
 * 收回
 * 
 * @param {*} props 
 */
function unCommit(props) {
	runScript.call(this, props, 'UNSAVE');
}
