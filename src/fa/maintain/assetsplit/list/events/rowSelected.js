import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setBillFlowBtnsEnable } = listUtils;

function rowSelected(props, moduleId, record, index, status) {
	setBatchBtnsEnable.call(this, props, moduleId);
}
/**
 * 
 * 列表设置批量按钮的可用性
 *
 * @param {*} props 
 * @param {*} moduleId 
 */
function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
}
export { rowSelected };
export { setBatchBtnsEnable };
