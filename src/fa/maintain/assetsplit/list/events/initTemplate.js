import tableButtonClick from './tableButtonClick';
import { linkToCard } from './buttonClick';
import { setBatchBtnsEnable } from './rowSelected';
import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, listUtils } = utils;
const { createOprationColumn } = listUtils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils, CommonKeys } = commonConst;
const { LoginContext, faQueryAboutUtils } = components;
const { openAssetCardByPk } = faQueryAboutUtils;
const { getContext, loginContext } = LoginContext;
const { BILLSTATUS } = StatusUtils;
const { IBusiRoleConst } = CommonKeys;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;
const { searchAreaId, tableId, pagecode, dataSource, ds } = pageConfig;

let defaultConfig = {
	searchId: searchAreaId,
	bodyIds: [ tableId ],
	defPrefix: {
		search: [ 'def', 'bodyvos.def' ],
		head: 'def',
		body: 'def'
	},
	//特殊过滤
	specialFields: {
		//使用部门
		usedept: {
			orgMulti: 'pk_org',
			isRunWithChildren: true, //是否执行时包含下级
			defaultRunWithChildren: true, //是否默认勾选执行时包含下级
			data: [
				{
					fields: [ 'pk_equiporg', 'pk_org', 'pk_equiporg_v' ], //可被多个字段过滤 取第一个有的值
					returnName: 'pk_org' //被过滤字段名后台需要参数
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		}
	}
};
/**
 * 模板初始化
 * @param {*} props 
 */
export default function(props) {
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, dataSource);
					// 获取数据源
					getContext(ds, dataSource);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					setBatchBtnsEnable.call(this, props, tableId);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta);
				}

				// 行删除时悬浮框提示
				props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /*国际化处理：'确认要删除吗？'*/);
			}
		}
	);
}

function seperateDate(date) {
	if (typeof date !== 'string') return;
	let result = date.split(' ') && date.split(' ')[0];
	return result;
}

function modifierMeta(props, meta) {
	addSearchAreaReferFilter.call(this, props, meta, defaultConfig);
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div
						class="simple-table-td"
						field="bill_code"
						fieldname={getMultiLangByID('201202512A-000039') /*国际化处理："拆分单号"*/}
					>
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, 'browse');
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		} else if (item.attrcode == 'business_date') {
			item.render = (text, record, index) => {
				return <span>{record.business_date && seperateDate(record.business_date.value)}</span>;
			};
		} else if (item.attrcode == 'pk_card.asset_code') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							openAssetCardByPk.call(this, props, record.pk_card.value);
						}}
					>
						{record && record['pk_card.asset_code'] && record['pk_card.asset_code'].display}
					</span>
				);
			};
		}
		return item;
	});
	//添加操作列

	let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
	meta[tableId].items.push(oprCol);
	return meta;
}
