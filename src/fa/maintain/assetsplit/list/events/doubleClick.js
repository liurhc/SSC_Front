import { linkToCard } from './buttonClick';

/**
 *双击事件 
 * @param {*} record 
 * @param {*} index 
 * @param {*} props 
 */
export default function doubleClick(record, index, props) {
	linkToCard.call(this, props, record);
}
