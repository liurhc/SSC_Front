import { ajax, toast } from 'nc-lightapp-front';
import { linkToCard } from './buttonClick';
import { pageConfig } from '../const';

import ampub from 'ampub';
const { components, commonConst } = ampub;
const { ScriptReturnUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { tableId, pagecode, pkField, url, resourceCode, dataSource } = pageConfig;

export default function buttonClick(props, key, text, record, index) {
	switch (key) {
		case 'Delete':
			singleDel.call(this, props, record, index);
			break;
		case 'Edit':
			edit.call(this, props, record);
			break;
		case 'UnCommit':
			unCommit.call(this, props, record, index);
			break;
		case 'Commit':
			commit.call(this, props, record, index);
			break;
		case 'QueryAboutBillFlow': //审批详情
			this.setState({
				showApprove: true,
				pk_bill: record.pk_assetsplit.value,
				transi_type: record.transi_type.value
			});
			break;
		default:
			// console.log(key, index);
			break;
	}
}

/**
 * 表格行修改
 * @param {*} props 
 * @param {*} record 
 */
function edit(props, record) {
	let pk = record[pkField].value;
	let data = {
		pk,
		resourceCode
	};
	ajax({
		url: pageConfig.url.editUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data.result) {
					toast({ color: 'warning', content: data.result });
				} else {
					linkToCard.call(this, props, record, UISTATE.edit);
				}
			}
		}
	});
}
/**
 * 提交、收回
 * 
 * @param {*} props 
 */
function runScript(props, actionType, record, index) {
	let id = record[pkField].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};

	ajax({
		url: pageConfig.url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: actionType,
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
		},
		success: (res) => {
			let isDelete = actionType == 'DELETE';
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				isDelete,
				dataSource
			);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'warning' });
			}
		}
	});
}
/**
 * 删除
 * 
 * @param {*} props 
 */
function singleDel(props, record, index) {
	let id = record[pkField].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};

	ajax({
		url: url.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, pkField, tableId.replace('list', 'card'), tableId, true);
			// 清除缓存
			props.table.deleteCacheId(tableId, id);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'warning' });
			}
		}
	});
}

/**
 * 提交
 * 
 * @param {*} props 
 */
function commit(props, record, index) {
	runScript.call(this, props, 'SAVE', record, index);
}
/**
 * 收回
 * 
 * @param {*} props 
 */
function unCommit(props, record, index) {
	runScript.call(this, props, 'UNSAVE', record, index);
}
