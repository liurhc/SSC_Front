import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick, { runScript } from './buttonClick';
import doubleClick from './doubleClick';
import tableButtonClick from './tableButtonClick';
import afterEvent from './afterEvent';
import { rowSelected, setBatchBtnsEnable } from './rowSelected';
export {
	searchBtnClick,
	pageInfoClick,
	initTemplate,
	buttonClick,
	doubleClick,
	tableButtonClick,
	rowSelected,
	setBatchBtnsEnable,
	afterEvent,
	runScript
};
