import React, { Component } from 'react';
import { createPage, base, high, createPageIcon } from 'nc-lightapp-front';
const { NCAffix } = base;
const { searchAreaId, tableId, pagecode, dataSource, pkField } = pageConfig;

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { queryVocherSrcUtils, ApprovalTrans } = components;
const { queryVoucherSrc } = queryVocherSrcUtils;
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	doubleClick,
	rowSelected,
	afterEvent,
	runScript
} from './events';
const { ApproveDetail } = high;
import { pageConfig } from './const';
/**
 * 列表入口
*/
class MasterChildList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pk_bill: this.props.getUrlParam('id'),
			showApprove: false,
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}
	componentDidMount() {
		queryVoucherSrc(this.props, tableId, pkField, pagecode);
	}
	//提交及指派 回调
	getAssginUsedr = (value) => {
		runScript.call(this, this.props, 'SAVE', value);
	};
	//取消指派
	turnOff = () => {
		this.props.table.selectAllRows(tableId, false);
		rowSelected.call(this, this.props, tableId);
		this.setState({
			compositedisplay: false
		});
	};
	render() {
		let { table, button, search, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;

		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">
								{getMultiLangByID('201202512A-000000' /*国际化处理：资产拆分 */)}
							</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchAreaId, {
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: afterEvent.bind(this),
						dataSource: dataSource
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						showCheck: true,
						showIndex: true,
						handlePageInfoChange: pageInfoClick.bind(this),
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						pkname: pkField,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						}
					})}
					{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				</div>
				{/* 附件*/}
				{createNCUploader(`${pagecode}-uploader`, {})}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002' /*国际化处理：'指派'*/)}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

const MasterChildListBase = createPage({})(MasterChildList);

export default MasterChildListBase;
