// 页面配置
export const pageConfig = {
	// 应用主键
	// 应用编码
	appcode: '201202512A',
	// 应用名称
	//title: '资产拆分',
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '201202512A_list',
	// 主键字段
	pkField: 'pk_assetsplit',
	// 单据类型
	bill_type: 'HM',
	// 交易类型
	transi_type: 'HM-01',
	//输出打印名称
	//printFilename: '资产拆分',
	// 请求链接
	url: {
		queryUrl: '/nccloud/fa/assetsplit/query.do',
		queryPageUrl: '/nccloud/fa/assetsplit/querypagelistbypks.do',
		editUrl: '/nccloud/fa/assetsplit/edit.do',
		printUrl: '/nccloud/fa/assetsplit/printCard.do',
		commitUrl: '/nccloud/fa/assetsplit/commit.do',
		deleteUrl: '/nccloud/fa/assetsplit/delete.do'
		//unCommitUrl: '/nccloud/aum/sale/unCommit.do'
	},

	// 权限资源编码
	resourceCode: '2012040010',
	// 打印模板节点标识
	printNodekey: null,
	cardRouter: '/card',
	cardUrl: '/fa/maintain/assetsplit/card/index.html',
	dataSource: 'fa.maintain.assetsplit.main',
	ds: 'DataSourceCode'
};
