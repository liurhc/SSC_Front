//@zhangypm切换到正确的导入
import ampub from 'ampub';
// import ampub from '../../../../ampub/common/collection/release';
const { IBusiRoleConst } = ampub.commonConst.CommonKeys;

// 节点固定变量数据值
export const staticVariable = {
	pagecode: '201202508A_card', //单据模板pageID
	moduleId: '2012', //所属模块编码
	searchId: '2012036010', //查询区域编码
	tableId: 'bodyvos', //列表表体编码
	formId: 'card_head', //列表表头编码
	appid: '201202508A',
	// 打印模板所在NC节点及标识
	printFuncode: '2012036010',
	printNodekey: null,
	title: '201202508A-000000', // 国际化处理：'资产减值单',
	dataSource: 'fa.maintain.predevaluate.main',
	listRouter: '/list',
	pkfield: 'pk_predevaluate',
	bill_type: 'HQ'
};
// 过滤参数
export const defaultConfig = {
	searchId: 'searchArea',
	formId: 'card_head',
	bodyIds: [ 'bodyvos', 'bodyvos_edit' ],
	specialFields: {
		pk_assetgroup: {
			// 资产组
			RefActionExtType: 'GridRefActionExt',
			class: 'nccloud.web.fa.maintain.predevaluate.refcondition.PK_ASSETGROUPSqlBuilder',
			data: [
				{
					fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
					returnName: 'pk_org' //被过滤字段名后台需要参数
				}
			]
		},
		recorder: {
			//经办人
			data: [
				{
					fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
					returnName: 'pk_org' //被过滤字段名后台需要参数
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		pk_card: {
			//资产卡片
			orgMulti: 'pk_org',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					fields: [ 'pk_accbook' ],
					returnName: 'pk_accbook'
				}
			]
		}
	},
	defPrefix: {
		search: [ 'bodyvos.def', 'bodyvos.pk_card.def', 'def' ],
		head: 'def',
		body: [ 'def', 'pk_card.def' ]
	}
};
// ajax请求链接
export const reqUrl = {
	insertUrl: '/nccloud/fa/predevaluate/insert.do',
	updateUrl: '/nccloud/fa/predevaluate/update.do',
	listQueryUrl: '/nccloud/fa/predevaluate/listquery.do',
	cardQueryUrl: '/nccloud/fa/predevaluate/cardquery.do',
	deleteUrl: '/nccloud/fa/predevaluate/delete.do',
	commitUrl: '/nccloud/fa/predevaluate/commit.do',
	pageDataByPksUrl: '/nccloud/fa/predevaluate/querypagegridbypks.do', // 分页查询
	billCodeGenUrl: '/nccloud/fa/predevaluate/billcodegenbackevent.do',
	headAfterEdit: '/nccloud/fa/predevaluate/headAfterEdit.do',
	bodyAfterEdit: '/nccloud/fa/predevaluate/bodyAfterEdit.do',
	printUrl: '/nccloud/fa/predevaluate/printCard.do',
	formula: '/nccloud/fa/predevaluate/formulaedit.do',
	batchAlter: '/nccloud/fa/predevaluate/batchAltert.do',
	lockcard: '/nccloud/fa/facard/lockcard.do',
	edit: '/nccloud/fa/predevaluate/edit.do'
};
export const buttonsStaus = {
	// 编辑态按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'AddLine', 'DelLine', 'BatchAlter', 'Formula', 'BatchFormula' ],
	// 浏览态按钮
	browseBtns: [ 'Add', 'Edit', 'Delete', 'Commit', 'UnCommit', 'Attachment', 'Refresh', 'More' ],
	// 表肩按钮
	hideBodyBtns: [ 'AddLine', 'DelLine', 'BatchAlter', 'Formula', 'BatchFormula' ]
};
