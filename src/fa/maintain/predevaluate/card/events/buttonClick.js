import { ajax, toast, print, output, cardCache } from 'nc-lightapp-front';
import { staticVariable, reqUrl, buttonsStaus } from '../constants/constant';
import { headAfterEvent } from './afterEvent';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = cardUtils;
const { LoginContext, ScriptReturnUtils, queryVocherUtils, saveValidatorsUtil } = components;
const { loginContextKeys } = LoginContext;
const { beforeSaveValidator } = saveValidatorsUtil;
const { queryAboutVoucher } = queryVocherUtils;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getContext } = LoginContext;
const { UISTATE } = StatusUtils;
import fa from 'fa';
const { fa_components } = fa;
const { ImageMng } = fa_components;
const { faImageScan, faImageView } = ImageMng;

const { appid, pagecode, formId, tableId, dataSource, pkfield, listRouter, bill_type } = staticVariable;
const { editBtns, browseBtns } = buttonsStaus;
export default function(props, id) {
	switch (id) {
		case 'Add':
			AddAction.call(this, props);
			break;
		case 'Save':
			saveAction.call(this, props);
			break;
		case 'Edit':
			EditAction.call(this, props);
			break;
		case 'Delete':
			showConfirm.call(this, props, {
				type: MsgConst.Type.Delete,
				beSureBtnClick: DeleteAction
			});
			break;
		case 'Cancel':
			showConfirm.call(this, props, {
				type: MsgConst.Type.Cancel,
				beSureBtnClick: CancelAction
			});
			break;
		case 'Commit':
			CommitAction.call(this, props, 'SAVE', 'commit');
			break;
		case 'SaveCommit':
			CommitAction.call(this, props, 'SAVE', 'saveCommit');
			break;
		case 'UnCommit':
			CommitAction.call(this, props, 'UNSAVE', '');
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'AddLine':
			addrowaction.call(this, props);
			break;
		case 'DelLine':
			delrowaction.call(this, props);
			break;
		case 'BatchAlter': // 批改
			BatchAltert.call(this, props);
			break;
		case 'Formula': // 公式
			if (this.state.set_field == '') {
				toast({ content: getMultiLangByID('201202508A-000029'), color: 'danger' }); // 多语处理：'该项不支持公式'
				return;
			}
			this.setState({ show: true, signature: '0' });
			break;
		case 'BatchFormula': // 批量公式
			if (this.state.set_field == '') {
				toast({ content: getMultiLangByID('201202508A-000029'), color: 'danger' }); // 多语处理：'该项不支持公式'
				return;
			}
			this.setState({ show: true, signature: '1' });
			break;
		case 'FAReceiptScan': // 影像扫描
			FAReceiptScan.call(this, props);
			break;
		case 'FAReceiptShow': // 影像查看
			FAReceiptShow.call(this, props);
			break;
		case 'Attachment': // 附件
			attachment.call(this, props);
			break;
		case 'QueryAboutBillFlow': // 审批详情
			queryAboutBillFlow.call(this, props);
			break;
		case 'QueryAboutVoucher': // 联查凭证
			queryAboutVoucher(props, formId, pkfield, appid);
			break;
		default:
			//console.log(id + ', no this button ');
			break;
	}
}
// add
function AddAction(props) {
	props.cardTable.closeExpandedRow(tableId);
	// 设置新增态
	setStatus.call(this, props, UISTATE.add);
	// 清空数据
	setValue.call(this, props, undefined);
	// 设置默认值
	setDefaultValue.call(this, props);
	// 设置字段编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (pk_org_v && pk_org_v.value) {
		headAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		setBodyBtnsEnable.call(this, props, tableId);
	}
}
/**
 * 设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId, dataSource);
	let groupName = getContext(loginContextKeys.groupName, dataSource);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype, dataSource);
	let pk_transitype = getContext(loginContextKeys.pk_transtype, dataSource);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org, dataSource);
	let org_Name = getContext(loginContextKeys.org_Name, dataSource);
	let pk_org_v = getContext(loginContextKeys.pk_org_v, dataSource);
	let org_v_Name = getContext(loginContextKeys.org_v_Name, dataSource);
	let businessdate = getContext(loginContextKeys.businessDate, dataSource);
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype },
		bill_status: { display: getMultiLangByID('statusUtils-000000'), value: '0' }, // 国际化处理：自由态
		business_date: { value: businessdate }
	});
}
// edit
function EditAction(props) {
	let pk = props.form.getFormItemsValue(formId, pkfield).value || props.getUrlParam('id');
	ajax({
		url: reqUrl.edit,
		data: {
			pk,
			resourceCode: '2012036010'
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'danger', content: res.data });
			} else {
				props.cardTable.closeExpandedRow(tableId);
				setStatus.call(this, props, UISTATE.edit);
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
// delete
function DeleteAction(props) {
	let paramInfoMap = {};
	let pk = props.form.getFormItemsValue(formId, pkfield).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	paramInfoMap[pk] = ts;
	ajax({
		url: reqUrl.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			let { success } = res;
			if (success) {
				let callback = (newpk) => {
					// 更新参数
					props.setUrlParam({ id: newpk });
					// 加载下一条数据
					loadDataByPk.call(this, props, newpk);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkfield,
					dataSource,
					undefined,
					true,
					callback
				);
			}
		}
	});
}
// Save
export function saveAction(props) {
	// 保存前校验
	let keys = [ 'netrating', 'netvalue' ];
	let validatorFlag = beforeSaveValidator.call(this, props, formId, tableId, keys);
	// 如果校验失败，则返回
	if (!validatorFlag) {
		return;
	}

	// 取得要保存的数据，这里加了处理，过滤掉因多选造成的多了一条错误垃圾数据，平台暂时没解决，自己处理了
	// let data = getVOData.call(this, props);
	let billdata = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let pk = props.form.getFormItemsValue(formId, pkfield).value;
	let url = reqUrl.insertUrl;
	let oldstatus = UISTATE.add;
	if (pk && pk != null) {
		url = reqUrl.updateUrl;
		oldstatus = UISTATE.edit;
		billdata.body[tableId].rows.map((currow) => {
			currow.values.pk_predevaluate = { value: props.form.getFormItemsValue(formId, pkfield).value };
		});
	} else {
		let newrow = [];
		billdata.body[tableId].rows.map((currow) => {
			if (currow.status != '3') {
				newrow.push(currow);
			}
		});
		billdata.body[tableId].rows = newrow;
	}
	// 保存前执行验证公式
	props.validateToSave(billdata, () => {
		ajax({
			url: url,
			data: billdata,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//关闭侧拉
					props.cardTable.closeModel(tableId);
					afterSave.call(this, props, data, oldstatus);
				}
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
			}
		});
	});
}
/**
 * 取得要保存的数据，同时过滤表体卡片多选产生的脏数据
 * @param {*} props 
 */
function getVOData(props) {
	let data = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let body = props.cardTable.getAllData(tableId);
	let newbody = {};
	newbody.areacode = tableId;
	newbody.areaType = body.areaType;
	let rows = [];
	body.rows.map((ele) => {
		let pk_card = ele.values.pk_card.value;
		let pk_cards = pk_card.split(',');
		let length = pk_cards.length;
		if (length == 1) {
			rows.push(ele);
		}
	});
	newbody.rows = rows;
	data.body[tableId] = newbody;

	return data;
}
/**
* 保存后
* @param {*} props  
*/
export function afterSave(props, data, oldstatus) {
	// 性能优化，关闭表单和表格渲染
	props.beforeUpdatePage();
	setValue.call(this, props, data);
	setStatus.call(this, props, UISTATE.browse);
	let pk = props.form.getFormItemsValue(formId, pkfield).value;
	let cacheData = props.createMasterChildData(pagecode, formId, tableId);
	// 保存成功后处理缓存
	if (oldstatus == UISTATE.add) {
		cardCache.addCache(pk, cacheData, formId, dataSource);
	} else {
		cardCache.updateCache(pkfield, pk, cacheData, formId, dataSource);
	}
	showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess }); //
	setStatus.call(this, props, UISTATE.browse);
	// 性能优化，表单和表格统一渲染
	props.updatePage(formId, tableId);
}
// Cancel
function CancelAction(props) {
	// 解锁卡片
	lockcards.call(this, props, 'unlock');

	props.form.cancel(formId);
	props.cardTable.resetTableData(tableId);

	setStatus.call(this, props, UISTATE.browse);

	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	let pk = props.form.getFormItemsValue(formId, pkfield).value;
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	loadDataByPk.call(this, props, pk);
}

/**
 * 提交
 * @param {*} props 
 * @param {*} OperatorType 
 * @param {*} commitType 
 */
export function CommitAction(props, OperatorType, commitType, content) {
	//保存提交加校验
	if (commitType === 'saveCommit') {
		let keys = [ 'netrating', 'netvalue' ];
		let savevalidator = beforeSaveValidator.call(this, props, formId, tableId, keys);
		// 校验失败，则返回
		if (!savevalidator) {
			return;
		}
	}
	let pk = props.form.getFormItemsValue(formId, pkfield).value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	// 保存提交情况，需要判断是否为指派情况,决定是否验证公式
	if (content) {
		ajax({
			url: reqUrl.commitUrl,
			data: CardData,
			success: (res) => {
				if (res.success) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					let callback = () => {
						setStatus.call(this, props, UISTATE.browse);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkfield,
						dataSource,
						null,
						false,
						callback,
						pagecode
					);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	} else {
		// 保存前执行验证公式
		props.validateToSave(CardData, () => {
			ajax({
				url: reqUrl.commitUrl,
				data: CardData,
				success: (res) => {
					if (res.success) {
						if (content) {
							this.setState({
								compositedisplay: false
							});
						}
						// 性能优化，关闭表单和表格渲染
						props.beforeUpdatePage();
						let callback = () => {
							setStatus.call(this, props, UISTATE.browse);
						};
						getScriptCardReturnData.call(
							this,
							res,
							props,
							formId,
							tableId,
							pkfield,
							dataSource,
							null,
							false,
							callback,
							pagecode
						);
						// 性能优化，表单和表格统一渲染
						props.updatePage(formId, tableId);
					}
				}
			});
		});
	}
}
/**
 * 表间增行按钮
 * @param {*} props 
 */
function addrowaction(props) {
	props.cardTable.addRow(tableId, undefined, {}, false);
	setBodyBtnsEnable.call(this, props, tableId);
}
/**
 * 表间删行按钮
 * @param {*} props 
 */
function delrowaction(props) {
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	let allpks = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
		// 若删除行为新增数据，则记录为需解锁卡片,默认赋值第一行状态为1修改
		if (item.data.status === '2' || item.data.status === '1') {
			allpks.push(item.data.values.pk_card.value);
		}
	});
	// 若待解锁卡片数量大于0，则解锁
	let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
	let usrid = getContext('userId', dataSource);
	if (allpks.length > 0) {
		let param = {
			allpks,
			msgMap: {
				usrid,
				lockflag: 'unlock',
				dSource
			}
		};
		lockrequest.call(this, param);
	}

	props.cardTable.delRowsByIndex(tableId, checkedIndex);
	setBodyBtnsEnable.call(this, props, tableId);
}
/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintParam.call(this, props);
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}

	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printUrl, // 后台打印服务url
		printData
	);
}
/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintParam.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: reqUrl.printUrl,
		data: printData
	});
}
/**
 * 打印输出参数
 * @param {*} props 
 * @param {*} output 
 */
function getPrintParam(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(formId, pkfield);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let param = {
		filename: pagecode, // 文件名称
		appcode: appid, // 功能节点编码，即模板编码
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};

	return param;
}
/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk) {
	if (!pk || pk == 'null') {
		setValue.call(this, props, undefined);
		return;
	}
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData) {
		setValue.call(this, props, cachData);
	} else {
		getDataByPk.call(this, props, pk);
	}
}
/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk, isRefresh) {
	ajax({
		url: reqUrl.cardQueryUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					if (isRefresh) {
						showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
					}
				} else {
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				}
			}
		}
	});
}
/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	setCardValue.call(this, props, data);
	setBrowseBtnsVisible.call(this, props);
	if (status != UISTATE.browse) {
		setBodyBtnsEnable.call(this, props, tableId);
	}
}
/**
 * 浏览态设置按钮显示隐藏
 * @param {*} props 
 */
function setBrowseBtnsVisible(props) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	if (status != UISTATE.browse) {
		props.button.setButtonVisible(browseBtns, false);
	} else {
		let pkVal = props.form.getFormItemsValue(formId, pkfield);
		if (!pkVal || !pkVal.value) {
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible('Add', true);
			return;
		}
		props.button.setButtonVisible(browseBtns, true);
		setBillFlowBtnsVisible.call(this, props, formId, pkfield);
	}
}
/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.button.setButtonDisabled(
			[ 'AddLine', 'OpenCard', 'DelLine', 'BatchAlter', 'Formula', 'BatchFormula' ],
			true
		);
		return;
	}
	props.button.setButtonDisabled([ 'AddLine', 'BatchAlter', 'Formula', 'BatchFormula' ], false);
	let checkedRows = [];
	let num = props.cardTable.getNumberOfRows(moduleId, false);
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	// 获取表头资产组的值
	let pk_assetgroup = props.form.getFormItemsValue(formId, 'pk_assetgroup');
	// 如果资产组为空，则删行可用，否则不可用
	if (pk_assetgroup && pk_assetgroup) {
		props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
	}
}
/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	// 更新参数
	let pkVal = props.form.getFormItemsValue(formId, pkfield);
	props.setUrlParam({ status, id: pkVal ? pkVal.value : '' });
	switch (status) {
		case UISTATE.add:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			setBrowseBtnsVisible.call(this, props);
			props.button.setButtonVisible(editBtns, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.edit:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			setBrowseBtnsVisible.call(this, props);
			props.button.setButtonVisible(editBtns, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			setBrowseBtnsVisible.call(this, props);
			break;
		default:
			//console.log(status);
			break;
	}
	setHeadAreaData.call(this, props, { status });
}
/**
 * 批改
 * @param {*} props 
 */
function BatchAltert(props) {
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);
	// 不支持批改字段
	let canNotAlter = [ 'pk_card' ];
	if (canNotAlter.indexOf(changeData.batchChangeKey) != -1) {
		return;
	}
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let data = {
		card: CardData,
		batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
		batchChangeKey: changeData.batchChangeKey, // 原始数据key
		batchChangeValue: changeData.batchChangeValue, // 原始数据value
		tableCode: ''
	};
	ajax({
		url: reqUrl.batchAlter,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				//把数据设置到界面上
				setValue.call(this, props, data);
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			props.cardTable.setColValue(tableId, changeData.batchChangeKey, { display: null, value: null });
			if (changeData.batchChangeKey === 'currentresume') {
				props.cardTable.setColValue(tableId, 'returnmoney', { display: null, value: null });
			} else if (changeData.batchChangeKey === 'returnmoney') {
				props.cardTable.setColValue(tableId, 'currentresume', { display: null, value: null });
			}
		}
	});
}
/**
 * 加锁，解锁
 * @param {*} props 
 * @param {*} lockflag Lock：加锁；unlock:解锁 
 * @param {*} allcards true:表体全部数据加锁;false:新增数据加锁
 */
export function lockcards(props, lockflag, async = true) {
	let data = props.cardTable.getAllRows(tableId);
	let allpks = [];
	// 取消时，解锁表体所有卡片，包括新增的，修改的
	for (let val of data) {
		// 判断如果是新增删除，在删除时已解锁，这里不解锁，若是修改删除，在这里需要解锁
		if (!val.values.pk_predevaluate_b.value && val.status === '3') {
			//console.log(val.values.pk_card.value);
		} else {
			if (val.values.pk_card.value) {
				allpks.push(val.values.pk_card.value);
			}
		}
	}
	let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
	let usrid = getContext('userId', dataSource);
	let param = {
		allpks,
		msgMap: {
			usrid,
			lockflag,
			dSource
		}
	};
	if (allpks && allpks.length > 0 && lockflag && usrid && dSource) {
		lockrequest.call(this, param, async);
	}
}
/**
 * 加解锁请求
 * @param {*} param 
 */
export function lockrequest(param, async = true) {
	ajax({
		url: reqUrl.lockcard,
		data: param,
		async,
		success: (res) => {
			//console.log(param + '  lock cards successfully!!');
		},
		error: (res) => {}
	});
}
/**
 * 影像扫描
 * @param {*} props 
 */
function FAReceiptScan(props) {
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField: pkfield
	};
	faImageScan(props, imageData);
}
/**
 * 影像查看
 * @param {*} props 
 */
function FAReceiptShow(props) {
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField: pkfield
	};
	faImageView(props, imageData);
}
/**
 * refresh
 * @param {*} props 
 */
function refresh(props) {
	let id = props.form.getFormItemsValue(formId, pkfield);
	if (!id || !id.value) {
		return;
	}
	if (!id.value) {
		toast({ content: getMultiLangByID('201202508A-000010'), color: 'danger' }); // 多语处理：'刷新失败'
		return;
	}
	getDataByPk.call(this, props, id.value, true);
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	// 主键
	let billId = props.form.getFormItemsValue(formId, pkfield).value;
	props.ncUploader.show('uploader', {
		billId: 'fa/predevaluate/' + billId
	});
}
/**
 * 审批详情
 * @param {*} props 
 */
function queryAboutBillFlow(props) {
	let id = props.form.getFormItemsValue(formId, pkfield).value;
	let transi_type = props.form.getFormItemsValue(formId, 'transi_type').value;
	this.setState({
		showApprove: true,
		pk_bill: id,
		transi_type
	});
}
/**
 * 卡片返回列表
 * @param {*} props 
 */
export function backToList(props) {
	props.pushTo(listRouter, { pagecode: pagecode });
}
