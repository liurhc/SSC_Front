import { staticVariable } from '../constants/constant';
import { lockrequest } from '../events/buttonClick';

import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

const { tableId, dataSource } = staticVariable;
export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		case 'OpenCard':
			let status = props.cardTable.getStatus(tableId);
			if (status === 'browse') {
				props.cardTable.toggleRowView(tableId, record);
			} else {
				props.cardTable.openModel(tableId, 'edit', record, index);
			}
			break;
		case 'DelLine':
			props.cardTable.delRowsByIndex(tableId, index);
			// 若行数据为新增态，则需解锁卡片
			if (record.values.pk_card && record.values.pk_card.value) {
				let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
				let usrid = getContext('userId', dataSource);
				let param = {
					allpks: [ record.values.pk_card.value ],
					msgMap: {
						usrid,
						lockflag: 'unlock',
						dSource
					}
				};
				lockrequest.call(this, param);
			}
			break;
		default:
			// console.log(key, index, '  no this button ');
			break;
	}
}
