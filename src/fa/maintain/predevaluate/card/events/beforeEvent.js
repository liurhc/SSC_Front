import { staticVariable, defaultConfig } from '../constants/constant';

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addBodyReferFilter } = ReferFilter;
const { tableId } = staticVariable;
/**
 * 减值编辑前事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} index 
 * @param {*} record 
 */
export default function beforeEvent(props, moduleId, key, value, index, record) {
	let pk_card = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_card');
	// 当未选择卡片时，可回收金额，本次计提字段不可编辑
	// if (pk_card && !pk_card.value) {
	// 	props.cardTable.setEditableByIndex(tableId, index, [ 'returnmoney', 'currentresume' ], false);
	// }
	// 可回收金额/本次计提
	if (key === 'returnmoney' || key === 'currentresume') {
		if (pk_card && !pk_card.value) {
			props.cardTable.setEditableByIndex(tableId, index, [ 'returnmoney', 'currentresume' ], false);
		} else {
			this.setState({
				set_field: key,
				row_number: index
			});
		}
	}
	let meta = props.meta.getMeta();
	addBodyReferFilter.call(this, props, meta, defaultConfig, record);
	return true;
}
