import { tableButtonClick, buttonClick, setStatus } from '../events';
import { staticVariable, defaultConfig } from '../constants/constant';

import ampub from 'ampub';
const { utils, components } = ampub;
const { cardUtils } = utils;
const { createOprationColumn, afterModifyCardMeta } = cardUtils;
const { LoginContext, faQueryAboutUtils } = components;
const { openAssetCardByPk } = faQueryAboutUtils;
const { loginContext } = LoginContext;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addHeadAreaReferFilter } = ReferFilter;

const { pagecode, formId, tableId, dataSource } = staticVariable;

export default function(props) {
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, dataSource);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}
/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status');
	if (status == 'add') {
		buttonClick.call(this, props, 'Add');
	} else if (status == 'edit') {
		buttonClick.call(this, props, 'Edit');
	} else {
		setStatus.call(this, props, status);
	}
}

function modifierMeta(props, meta) {
	// 参照过滤
	refFilter.call(this, props, meta);

	// 添加表格操作列
	let oprCol = createOprationColumn.call(this, props, {
		formId: formId,
		tableId: tableId,
		tableButtonClick: tableButtonClick
	});
	meta[tableId].items.push(oprCol);

	return meta;
}
/**
 * 参照过滤
 * @param {*} props 
 * @param {*} meta 
 */
function refFilter(props, meta) {
	// 表头参照过滤
	addHeadAreaReferFilter(props, meta, defaultConfig);
	// 表体参照过滤
	meta[tableId].items.map((item) => {
		// 卡片超链,过滤
		if (item.attrcode == 'pk_card') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openAssetCardByPk.call(this, props, record.values.pk_card.value);
							}}
						>
							{record && record.values.pk_card && record.values.pk_card.display}
						</span>
					</div>
				);
			};
			item.isMultiSelectedEnabled = true;
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
				pk_org = pk_org && pk_org.value;
				return {
					pk_org
				};
			};
		}
	});
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);
}
