import { ajax, toast } from 'nc-lightapp-front';
import { staticVariable, reqUrl, buttonsStaus } from '../constants/constant';
import { setValue, lockcards } from './buttonClick';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { OrgChangeEvent } = components;
const { orgChangeEvent } = OrgChangeEvent;

const { pagecode, formId, tableId, dataSource } = staticVariable;
const { hideBodyBtns } = buttonsStaus;
/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key  
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	if (key == 'pk_org_v') {
		handlePkOrgAfterEditEvent.call(this, props, key, value, oldValue, data);
	} else if (key === 'pk_accbook') {
		// 资产账簿项目变更
		handlePkAccbookAfterEditEvent.call(this, props, data);
	} else if (key === 'pk_assetgroup') {
		// 资产组项目变更
		handleAssetGroupAfterEditEvent.call(this, props, data);
	} else if (key === 'currentresume') {
		// 本次计提项目变更
		if (!value.value) {
			return;
		}
		handleCurrentresumeAfterEditEvent.call(this, props, data);
	} else if (key === 'business_date') {
		// 业务日期项目变更
		if (!value.value) {
			return;
		}
		handleBusinessDataAfterEditEvent.call(this, props, data);
	}
}
/**
 * 财务组织编辑后
 * @param {*} props 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} data 
 */
function handlePkOrgAfterEditEvent(props, key, value, oldValue, data) {
	//表体清空解锁
	lockcards.call(this, props, 'unlock');
	let callback = () => {
		let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v').value;
		if (!pk_org_v) {
			//更换组织之后需要清空表体行
			props.cardTable.setTableData(tableId, { rows: [] });
		} else {
			ajax({
				url: reqUrl.headAfterEdit,
				data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						setValue.call(this, props, data);
						if (value.value) {
							props.cardTable.addRow(tableId, undefined, {}, false);
						}
					}
				},
				error: (res) => {
					toast({ color: 'danger', content: res.message });
					// 若出错，本次计提置空
					props.form.setFormItemsValue(formId, { key: { value: null, display: null } });
				}
			});
		}
	};
	orgChangeEvent.call(this, props, pagecode, formId, tableId, key, value, oldValue, false, hideBodyBtns, callback);
}
/**
 * 资产账簿变更
 * @param {*} props 
 * @param {*} data 
 */
function handlePkAccbookAfterEditEvent(props, data) {
	//表体清空解锁
	lockcards.call(this, props, 'unlock');
	// 清空原表体数据，并新增一行
	setBodyToInitState.call(this, props);
	props.cardTable.addRow(tableId);
	// 业务日期是否为空
	let business_date = props.form.getFormItemsValue(formId, 'business_date').value;
	if (business_date) {
		handleBusinessDataAfterEditEvent.call(this, props, data);
	}
}
/**
 * 业务日期变更
 * @param {*} props 
 * @param {*} data 
 */
function handleBusinessDataAfterEditEvent(props, data) {
	// 资产组主键
	let pk_assetgroup = props.form.getFormItemsValue(formId, 'pk_assetgroup').value;
	// 如果是资产组减值单，则重新取得资产组信息
	if (pk_assetgroup) {
		handleAssetGroupAfterEditEvent.call(this, props, data);
	}
}
/**
 * 资产组变更
 * @param {*} props 
 * @param {*} data 
 */
function handleAssetGroupAfterEditEvent(props, data) {
	// 清空单据：表头5字段，表体清空追加一行
	setBillFormToInitState.call(this, props);

	// 资产组主键
	let pk_assetgroup = props.form.getFormItemsValue(formId, 'pk_assetgroup').value;
	let business_date = props.form.getFormItemsValue(formId, 'business_date').value;

	if (pk_assetgroup && business_date) {
		ajax({
			url: reqUrl.headAfterEdit,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					setValue.call(this, props, data);
				}
				// 资产组选中值，则设置表头本次计提可编辑，表体本次计提，可回收金额,资产卡片不可编辑
				setHeadItmesEdit.call(this, props, false);
				setBodyItemsEdit.call(this, props, true);
				// 设置表间增行删行按钮不可用

				props.button.setButtonDisabled([ 'AddLine', 'DelLine' ], false);
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
				props.form.setFormItemsValue(formId, { pk_assetgroup: { value: null, display: null } });
				setHeadItmesEdit.call(this, props, true);
			}
		});
	} else {
		// 设置表体中的“卡片编号”、“可回收金额”、“本次计提”项目可编辑。
		setBodyItemsEdit.call(this, props, false);
		// 设置表头中“本次计提”项目不可编辑。
		setHeadItmesEdit.call(this, props, true);
		// 设置表间增行按钮可用
		props.button.setButtonDisabled([ 'AddLine' ], true);
	}
}
/**
 * 设置表体字段编辑性:本次计提，可回收金额，资产卡片
 * @param {*} props 
 * @param {*} editflag 
 */
function setBodyItemsEdit(props, editflag) {
	props.cardTable.setColEditableByKey(tableId, 'currentresume', editflag);
	props.cardTable.setColEditableByKey(tableId, 'returnmoney', editflag);
	props.cardTable.setColEditableByKey(tableId, 'pk_card', editflag);
}
/**
 * 本次计提变更
 * @param {*} props 
 * @param {*} data 
 */
function handleCurrentresumeAfterEditEvent(props, data) {
	// 资产组主键
	let pk_assetgroup = props.form.getFormItemsValue(formId, 'pk_assetgroup').value;
	// 当前为资产组减值单，进行
	if (pk_assetgroup) {
		ajax({
			url: reqUrl.headAfterEdit,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					setValue.call(this, props, data);
				}
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message || getMultiLangByID('201202508A-000001') }); //'本次计提出错'
				// 若出错，本次计提置空
				props.form.setFormItemsValue(formId, { currentresume: { value: null, display: null } });
			}
		});
	} else {
		// [本次计提]可编辑，则资产组不能为空
		toast({ color: 'danger', content: getMultiLangByID('201202508A-000002') }); // '[本次计提]可编辑，则资产组不能为空'
		// 设置表头中“本次计提”项目为空，且不可编辑。
		setHeadItmesEdit.call(this, props, false);
	}
}
/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} i 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	//使用createBodyAfterEventData 初始化为后台平台可以使用的格式
	let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, index);
	if (key === 'pk_card') {
		handlePkCardAfterEditEvent.call(this, props, moduleId, key, value, changedRows, index, data);
	} else if (key === 'currentresume' || key === 'returnmoney') {
		if (!value) {
			return;
		}
		//新增功能---差异化处理
		let newRecord = JSON.parse(JSON.stringify(record || {}));
		data.card.body[moduleId] = {
			...data.card.body[moduleId],
			rows: [ newRecord ]
		};
		data.index = 0; //修改编辑行为0
		// 本次计提或可回收金额变动
		ajax({
			url: reqUrl.bodyAfterEdit,
			data: data,
			async: false,
			success: (res) => {
				let { success, data } = res;
				if (res.data && res.data.body && res.data.body[tableId]) {
					// setValue.call(this, props, data);
					//新增功能---差异化处理
					props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
				}
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
				// 若出错，本次计提置空
				props.cardTable.setValByKeyAndIndex(tableId, index, 'currentresume', { display: null, value: null });
				props.cardTable.setValByKeyAndIndex(tableId, index, 'returnmoney', { display: null, value: null });
			}
		});
	}
}
/**
 * 卡片编辑后
 * @param {*} props 
 * @param {*} data 
 */
function handlePkCardAfterEditEvent(props, moduleId, key, value, changedRows, index, data) {
	// 固定资产卡片减值时，资产账簿项目不能为空。
	let accbook = props.form.getFormItemsValue(formId, 'pk_accbook').value;
	// 资产账簿项目为空时，提示用户信息。
	if (!accbook) {
		// 则清空表头固定字段值，清空表体值，表体加空行并给出提示
		setBillFormToInitState.call(this, props);
		toast({ color: 'danger', content: getMultiLangByID('201202508A-000003') }); // '请先选择资产账簿'
		return;
	}
	// 清空当前编辑行的本次计提与可回收金额字段
	props.cardTable.setValByKeyAndIndex(tableId, index, 'currentresume', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'returnmoney', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'predevaluate', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'predevaluate_old', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'accudep', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'localoriginvalue', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'netrating', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'netvalue', { value: null, display: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'salvage', { value: null, display: null });

	data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, index);
	// 卡片联动处理
	ajax({
		url: reqUrl.bodyAfterEdit,
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				setValue.call(this, props, data, index);
			}
		},
		error: (res) => {
			// 用于选择卡片加锁时，清空字段值
			clearPartColValue.call(this, props, index);
			toast({ color: 'danger', content: res.message });
		}
	});
}
/**
 * 清空卡片部分字段数值
 * @param {*} props 
 * @param {*} index 
 */
function clearPartColValue(props, index) {
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card', { display: null, value: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card.asset_name', { display: null, value: null });
	props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_card.card_code', { display: null, value: null });
	// 选择卡片已加锁时，清空旧值
	props.cardTable.delChangedRowsOldValue(tableId, index, 'pk_card');
	props.cardTable.delRowsByIndex(tableId, index);
}
/*
 * 设置表头中“本次计提”项目是否可编辑
 */
function setHeadItmesEdit(props, editflag) {
	// 表头“本次计提”项目编辑。
	props.form.setFormItemsDisabled(formId, { currentresume: editflag });
	props.form.setFormItemsValue(formId, { currentresume: { value: null, display: null } });
}
/**
 * <p>
 * 清空单据，设置为初始化状态。
 * </p>
 */
function setBillFormToInitState(props) {
	// 清空单据表头各个项目的值。
	setHeadToInitState.call(this, props);
	// 将之前单据表体中选择的卡片解锁，并清空各表体行。
	setBodyToInitState.call(this, props);
}
/**
 * <p>
 * 清空单据表头各个项目的值。
 * </p>
 */
function setHeadToInitState(props) {
	// 资产组账面价值，总部资产账面价值，账面价值，本次计提，总部资产主键
	props.form.setFormItemsValue(formId, { assetgroup_carring_account: { value: null, display: null } });
	props.form.setFormItemsValue(formId, { headquarters_carring_account: { value: null, display: null } });
	props.form.setFormItemsValue(formId, { carring_account: { value: null, display: null } });
	props.form.setFormItemsValue(formId, { currentresume: { value: null, display: null } });
	props.form.setFormItemsValue(formId, { pk_assetgroup_headquarters: { value: null, display: null } });
}
/**
 * <p>
 * 将之前单据表体中选择的卡片解锁，并清空原有表体行。
 * </p>
 */
function setBodyToInitState(props) {
	lockcards(props, 'unlock');
	props.cardTable.setTableData(tableId, { rows: [] });
}
