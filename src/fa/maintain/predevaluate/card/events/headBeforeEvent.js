import { staticVariable, defaultConfig } from '../constants/constant';

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addBodyReferFilter } = ReferFilter;
const { tableId } = staticVariable;
/**
 * 减值表头编辑前事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 

 */
export default function headBeforeEvent(props, moduleId, key, value) {
	let pk_assetgroup = props.form.getFormItemsValue(moduleId, 'pk_assetgroup');
	if (key === 'currentresume') {
		if (pk_assetgroup && !pk_assetgroup.value) {
			props.form.setFormItemsDisabled(moduleId, { currentresume: true });
		}
	}
	return true;
}
