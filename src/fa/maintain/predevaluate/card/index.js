import React, { Component } from 'react';
import { createPage, base, high, ajax, toast } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	beforeEvent,
	pageInfoClick,
	getDataByPk,
	saveAction,
	lockcards,
	lockrequest,
	backToList,
	rowSelected,
	bodyAfterEvent,
	headAfterEvent,
	CommitAction,
	headBeforeEvent
} from './events';
import { staticVariable, reqUrl } from './constants/constant';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

const { NCAffix } = base;
const { FormulaEditor, ApproveDetail } = high;
const { formId, tableId, pagecode, title, dataSource } = staticVariable;
/**
 * 资产减值
 * wagnwhf
 * 2018/08/08
 */
class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			//公式处理的值
			show: false,
			set_field: '',
			row_number: '',
			signature: 'default',
			// 审批详情
			pk_bill: this.props.getUrlParam('id'),
			showApprove: false,
			transi_type: 'HQ-01',
			// 提交指派
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}
	componentWillMount() {
		window.onbeforeunload = () => {
			// 解锁卡片
			let status = this.props.cardTable.getStatus(tableId);
			if (status == 'edit') {
				lockcards.call(this, this.props, 'unlock', false);
			}
		};
		if (window.history && window.history.pushState) {
			window.onpopstate = () => {
				lockcards.call(this, this.props, 'unlock', false);
			};
		}
	}
	// 移除事件
	componentWillUnmount() {
		window.onbeforeunload = null;
	}
	componentDidMount() {
		//查询单据详情
		let status = this.props.getUrlParam('status');
		if (status != 'add') {
			let pk = this.props.getUrlParam('id');
			if (pk && pk != 'undefined') {
				getDataByPk.call(this, this.props, pk);
			}
		}
	}
	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};
	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 4,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};
	// 公式弹框显示字段
	FormulaClos = ({ setName, setExplain, name }) => {
		return (
			<ul class="tab-content">
				                    <li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202508A-000012'));
					}}
					onDoubleClick={() => {
						setName('netvalue');
					}}
				>
					{getMultiLangByID('201202508A-000012')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202508A-000013'));
					}}
					onDoubleClick={() => {
						setName('salvage');
					}}
				>
					{getMultiLangByID('201202508A-000013')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202508A-000014'));
					}}
					onDoubleClick={() => {
						setName('netrating');
					}}
				>
					{getMultiLangByID('201202508A-000014')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202508A-000015'));
					}}
					onDoubleClick={() => {
						setName('predevaluate');
					}}
				>
					{getMultiLangByID('201202508A-000015')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202508A-000016'));
					}}
					onDoubleClick={() => {
						setName('localoriginvalue');
					}}
				>
					{getMultiLangByID('201202508A-000016')}
				</li>
				<li
					class="tab-content-item"
					onClick={() => {
						setExplain(getMultiLangByID('201202508A-000017'));
					}}
					onDoubleClick={() => {
						setName('accudep');
					}}
				>
					{getMultiLangByID('201202508A-000017')}
				</li>
			</ul>
		);
	};
	// 公式编辑后处理
	formulaAfter = (str) => {
		// 如果没有先选择字段及行号则不处理
		if (!str || this.state.set_field === '' || this.state.row_number === '') {
			this.setState({ show: false });
			toast({ content: getMultiLangByID('201202508A-000029'), color: 'danger' }); // 多语处理：'该项不支持公式'
			return;
		}
		// 获取billcard
		let card_data = this.props.createMasterChildData(pagecode, formId, tableId);

		ajax({
			url: reqUrl.formula,
			data: {
				card: card_data,
				set_field: this.state.set_field,
				row_number: this.state.row_number,
				input_formula: str,
				isBatch: this.state.signature
			},
			success: (res) => {
				if (res.success) {
					if (res.data) {
						// 公式
						if (this.state.signature == '0') {
							//行号
							let row_number = this.state.row_number;
							this.props.cardTable.updateDataByIndexs(tableId, [
								{
									index: row_number,
									data: { status: '1', values: res.data.body.bodyvos.rows[0].values }
								}
							]);
							this.setState({ signature: 'default' });
							// 批量公式
						} else if (this.state.signature == '1') {
							this.props.cardTable.setTableData(tableId, res.data.body.bodyvos);
							this.setState({ signature: 'default' });
						}
					} else {
						toast({ color: 'warning', content: getMultiLangByID('201202508A-000018') }); // '失败'
					}
				}
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
				// 清空一行的值
				if (this.state.signature == '0') {
					this.props.cardTable.setValByKeyAndIndex(
						tableId,
						this.state.row_number,
						this.state.set_field,
						null
					);
					// 清空多行的值
				} else {
					this.props.cardTable.setColValue(tableId, this.state.set_field, { display: null, value: null });
				}
			}
		});
		this.setState({ show: false });
	};
	// 侧拉删行前解锁卡片的处理
	modelDelRowBefore = (props, moduleId, moduleIndex, row) => {
		let allpks = [ row.values.pk_card.value ];
		let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
		let usrid = getContext('userId', dataSource);
		let param = {
			allpks,
			msgMap: {
				usrid,
				lockflag: 'unlock',
				dSource
			}
		};
		lockrequest.call(this, param);
		return true;
	};
	//提交及指派 回调
	getAssginUsedr = (value) => {
		let status = this.props.form.getFormStatus(formId) || UISTATE.browse;
		if (status == UISTATE.browse) {
			CommitAction.call(this, this.props, 'SAVE', 'commit', value);
		} else {
			CommitAction.call(this, this.props, 'SAVE', 'saveCommit', value);
		}
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};
	render() {
		let { cardTable, form, button, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		return (
			<div id="nc-bill-card" className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title),
								formId,
								backBtnClick: backToList
							})}

							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this),
							onBeforeEvent: headBeforeEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: saveAction.bind(this),
							showCheck: true,
							showIndex: true,
							isAddRow: true,
							onBeforeEvent: beforeEvent.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							modelDelRowBefore: this.modelDelRowBefore.bind(this),
							selectedChange: rowSelected.bind(this)
						})}
					</div>
				</div>
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				<FormulaEditor
					zIndex={300}
					ref="formulaEditor"
					show={this.state.show}
					formulaConfig={[
						{
							tab: getMultiLangByID('201202508A-000019'), //'卡片项目',
							TabPaneContent: this.FormulaClos,
							key: 'formula',
							params: { name: this.state.name }
						}
					]}
					noShowFormula={[
						getMultiLangByID('201202508A-000020'),
						getMultiLangByID('201202508A-000021'),
						getMultiLangByID('201202508A-000022'),
						getMultiLangByID('201202508A-000023'),
						getMultiLangByID('201202508A-000024'),
						getMultiLangByID('201202508A-000025'),
						getMultiLangByID('201202508A-000026'),
						getMultiLangByID('201202508A-000027'),
						getMultiLangByID('201202508A-000028')
					]} //'其它类型', '日期', '数字', '自定义', '数学', '字符串', '数据库', '财务', '常用' ]}
					noShowAttr={[ 'all' ]}
					onOk={(str) => {
						this.formulaAfter(str);
					}} //点击确定回调
					onCancel={(a) => {
						this.setState({ show: false });
					}} //点击确定回调
					onHide={(a) => {
						this.setState({ show: false });
					}}
				/>
				{/* 附件*/}
				{createNCUploader('uploader', {disableModify:this.props.form.getFormItemsValue("card_head", 'bill_status')&&this.props.form.getFormItemsValue("card_head", 'bill_status').value!="0"})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay && (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} //'指派'}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				)}
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(Card);

export default Card;
