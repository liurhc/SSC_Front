import React, { Component } from 'react';
import { createPage, base, high, createPageIcon } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	doubleClick,
	rowSelected,
	commitAction,
	setBatchBtnsEnable,
	searchAfterEvent
} from './events';
import { staticVariable } from './constants/constant';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { queryVocherSrcUtils, ApprovalTrans } = components;
const { queryVoucherSrc } = queryVocherSrcUtils;

const { ApproveDetail } = high;
const { NCAffix } = base;
const { searchId, tableId, dataSource, title, pagecode, pkfield } = staticVariable;
/**
 * 资产减值列表
 * wangwhf
 * 2018/08/08
 */
class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// 审批详情
			pk_bill: this.props.getUrlParam('id'),
			showApprove: false,
			transi_type: 'HQ-01',
			compositedisplay: false,
			compositedata: {}
		};

		initTemplate.call(this, props);
	}
	componentDidMount() {
		//凭证联查来源单据
		queryVoucherSrc(this.props, tableId, 'pk_predevaluate', pagecode);
	}
	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, 'SAVE', this.props, value);
	};
	//取消 指派
	turnOff = () => {
		this.props.table.selectAllRows(tableId, false);
		rowSelected.call(this, this.props, tableId);
		this.setState({
			compositedisplay: false
		});
	};
	render() {
		let { table, button, search, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createSimpleTable } = table;
		let { createButtonApp } = button;
		let { NCCreateSearch } = search;
		let { createModal } = ncmodal;
		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						dataSource: dataSource,
						onAfterEvent: searchAfterEvent.bind(this)
					})}
				</div>

				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						handlePageInfoChange: pageInfoClick.bind(this),
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						showIndex: true,
						showCheck: true,
						dataSource: dataSource,
						pkname: pkfield,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				{createModal('Delete', {})}
				{/* 附件*/}
				{createNCUploader('uploader', {disableModify:(this.props.cardTable.getAllData('list_head').rows[0]==null ? '1': (this.props.table.getCheckedRows('list_head').length>0&&this.props.table.getCheckedRows('list_head')[0].data.values.bill_status.value))!='0'})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay && (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} //'指派'}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				)}
			</div>
		);
	}
}

List = createPage({})(List);

export default List;
