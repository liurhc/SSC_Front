// 节点固定变量数据值
export const staticVariable = {
	pagecode: '201202508A_list', //单据模板pageID
	moduleId: '2012', //所属模块编码
	searchId: 'searchArea', //查询区域编码
	tableId: 'list_head', //列表表体编码
	formId: 'card_head', //列表表头编码
	appid: '201202508A',
	node_code: '2012036010',
	// 打印模板所在NC节点及标识
	printFuncode: '2012036010',
	printNodekey: null,
	title: '201202508A-000000', //多语处理：'资产减值单',
	dataSource: 'fa.maintain.predevaluate.main',
	cardRouter: '/card',
	billtype: 'HQ',
	pkfield: 'pk_predevaluate'
};
// 过滤参数
export const defaultConfig = {
	searchId: 'searchArea',
	formId: 'card_head',
	bodyIds: [ 'list_head' ],
	specialFields: {
		recorder: {
			// 经办人
			data: [
				{
					fields: [ 'pk_org' ], //可被多个字段过滤 取第一个有的值
					returnName: 'pk_org' //被过滤字段名后台需要参数
				}
			]
		}
	},
	defPrefix: {
		search: [ 'def', 'bodyvos.def' ],
		head: 'def',
		body: 'def'
	},
	linkageData: {
		//联动链
		//参照字段:['被过滤字段1'，'被过滤字段2']
		pk_org: [
			'pk_accbook', // 资产账簿
			'recorder', //经办人
			'bodyvos.pk_card', // 卡片编号
			'bodyvos.pk_card.pk_category', //资产类别
			'bodyvos.pk_card.pk_usedept', //使用部门
			'bodyvos.pk_card.pk_mandept' //管理部门
		]
	}
};
// ajax请求链接
export const reqUrl = {
	insertUrl: '/nccloud/fa/predevaluate/insert.do',
	updateUrl: '/nccloud/fa/predevaluate/update.do',
	listQueryUrl: '/nccloud/fa/predevaluate/listquery.do',
	cardQueryUrl: '/nccloud/fa/predevaluate/cardquery.do',
	deleteUrl: '/nccloud/fa/predevaluate/delete.do',
	commitUrl: '/nccloud/fa/predevaluate/commit.do',
	pageDataByPksUrl: '/nccloud/fa/predevaluate/querypagegridbypks.do', // 分页查询
	printUrl: '/nccloud/fa/predevaluate/printCard.do',
	edit: '/nccloud/fa/predevaluate/edit.do'
};
