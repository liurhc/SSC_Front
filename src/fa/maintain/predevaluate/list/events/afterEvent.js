import { staticVariable, defaultConfig } from '../constants/constant';

import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage, AccbookAndPeriod } = fa_components;
const { referLinkageClear } = ReferLinkage;
const { getAccbookAndPeriod } = AccbookAndPeriod;

const { searchId } = staticVariable;
export default function searchAfterEvent(field, value) {
	//联动处理
	referLinkageClear(this.props, field, defaultConfig);

	if (field === 'pk_org') {
		this.props.search.setSearchValByField(searchId, 'pk_accbook', {}); //清空账簿
		getAccbookAndPeriod.call(this, this.props, searchId);
	}
}
