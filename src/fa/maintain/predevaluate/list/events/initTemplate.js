import { tableButtonClick } from '../events';
import { setBatchBtnsEnable, linkToCard } from './buttonClick';
import { staticVariable, defaultConfig } from '../constants/constant';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createOprationColumn } = listUtils;
const { LoginContext } = components;
const { loginContext } = LoginContext;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter, AccbookAndPeriod } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;
const { getAccbookAndPeriod } = AccbookAndPeriod;

const { pagecode, tableId, dataSource, searchId } = staticVariable;

export default function(props) {
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, dataSource);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001')); // '确认要删除吗？'
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}
/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	setBatchBtnsEnable.call(this, props, tableId);
	getAccbookAndPeriod.call(this, props, searchId);
}
/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	// 参照过滤
	refFilter.call(this, props, meta);

	// 添加表格操作列
	let oprCol = createOprationColumn.call(this, props, { tableButtonClick });

	meta[tableId].items.push(oprCol);

	return meta;
}
/**
 * 参照过滤
 * @param {*} props 
 * @param {*} meta 
 */
function refFilter(props, meta) {
	// 查询条件过滤
	addSearchAreaReferFilter(props, meta, defaultConfig);
	// 超链
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, 'browse');
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
}
