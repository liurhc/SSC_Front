import { ajax } from 'nc-lightapp-front';
import { staticVariable, reqUrl } from '../constants/constant';
import { setBatchBtnsEnable } from './buttonClick';

import ampub from 'ampub';
const { utils, components } = ampub;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;
const { LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

const { pagecode, searchId, tableId, billtype, dataSource } = staticVariable;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	// 获取交易类型编码
	let transi_type = getContext(loginContextKeys.transtype, dataSource);
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(searchId);
	}
	let pageInfo = props.table.getTablePageInfo(tableId);
	queryInfo.pageInfo = pageInfo;
	queryInfo.pagecode = pagecode;
	queryInfo.billtype = billtype;
	queryInfo.transtype = transi_type;

	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);

	ajax({
		url: reqUrl.listQueryUrl,
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
