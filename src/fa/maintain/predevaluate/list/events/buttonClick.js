import { ajax, toast, print, output } from 'nc-lightapp-front';
import { staticVariable, reqUrl } from '../constants/constant';
import searchBtnClick from './searchBtnClick';

import ampub from 'ampub';
const { utils, components } = ampub;
const { msgUtils, listUtils } = utils;
const { batchRefresh, setBillFlowBtnsEnable } = listUtils;
const { ScriptReturnUtils, queryVocherUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;

const { pagecode, appid, tableId, cardRouter, dataSource } = staticVariable;
const { commitUrl } = reqUrl;
export default function buttonClick(props, id) {
	switch (id) {
		case 'Add':
			linkToCard.call(this, props, undefined, 'add');
			break;
		case 'Delete':
			showConfirm.call(this, props, {
				type: MsgConst.Type.DelSelect,
				beSureBtnClick: DeleteAction
			});
			break;
		case 'Commit':
			commitAction.call(this, 'SAVE', props);
			break;
		case 'UnCommit':
			commitAction.call(this, 'UNSAVE', props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refreshAction.call(this, props);
			break;
		case 'Attachment': // 附件
			attachment.call(this, props);
			break;
		case 'QueryAboutVoucher': // 联查凭证
			queryAboutVoucher(props, tableId, 'pk_predevaluate', appid, true);
			break;
		default:
			//console.log(key, index, ', no this button');
			break;
	}
}
/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	props.pushTo(cardRouter, {
		status,
		pagecode: '201202508A_list',
		id: record['pk_predevaluate'] ? record['pk_predevaluate'].value : ''
	});
}
// delete
function DeleteAction(props) {
	let data = props.table.getCheckedRows(tableId);
	if (data.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_predevaluate.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			if (res.data.errorMsg) {
				toast({ color: 'danger', content: res.data.errorMsg });
			} else {
				getScriptListReturn(params, res, props, 'pk_predevaluate', tableId, tableId, true);
				// 平台还没有支持批量，暂时用单个循环
				params.map((item) => {
					props.table.deleteCacheId(tableId, item.id);
				});
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}
/**
 * 提交 、收回 
 */
export function commitAction(OperatorType, props, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_predevaluate.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecode,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(this, params, res, props, 'pk_predevaluate', tableId, tableId, false, dataSource);
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}
/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let param = getPrintParam.call(this, props);
	if (!param) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}

	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printUrl, // 后台打印服务url
		param
	);
}
/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintParam.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: reqUrl.printUrl,
		data: printData
	});
}

export function getPrintParam(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values.pk_predevaluate.value);
	});
	let param = {
		filename: pagecode, // 文件名称
		appcode: appid, // 功能节点编码，即模板编码
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType
	};

	return param;
}
/**
 * 重新加载页面
 * @param {*} props 
 */
function refreshAction(props) {
	batchRefresh.call(this, props, searchBtnClick);
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billNo 是单据主键
	let billId = checkedrows[0].data.values['pk_predevaluate'].value;
	props.ncUploader.show('uploader', {
		billId: 'fa/predevaluate/' + billId,
		billNo
	});
}
