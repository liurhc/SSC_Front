import { ajax, toast } from 'nc-lightapp-front';
import { staticVariable, reqUrl } from '../constants/constant';
import { linkToCard } from './buttonClick';

import ampub from 'ampub';
const { components, commonConst } = ampub;
const { ScriptReturnUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

const { pagecode, tableId, dataSource } = staticVariable;
const { commitUrl } = reqUrl;
/**
 * 列表操作列按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Edit':
			edit.call(this, props, record, index);
			break;
		case 'Delete':
			delConfirm.call(this, props, record, index);
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', props, tableId, index, record);
			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', props, tableId, index, record);
			break;
		case 'QueryAboutBillFlow': //审批详情
			this.setState({
				showApprove: true,
				pk_bill: record.pk_predevaluate.value,
				transi_type: record.transi_type.value
			});
			break;
		default:
			//console.log(key, index, ', no this button ');
			break;
	}
}
/**
 * 修改
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function edit(props, record, index) {
	ajax({
		url: reqUrl.edit,
		data: {
			pk: record.pk_predevaluate.value,
			resourceCode: staticVariable.node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'danger', content: res.data });
			} else {
				linkToCard.call(this, props, record, UISTATE.edit);
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
/**
 * 删除
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function delConfirm(props, record, index) {
	let id = record['pk_predevaluate'].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};
	ajax({
		url: commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			if (res.data.errorMsg) {
				toast({ color: 'danger', content: res.data.errorMsg });
			} else {
				getScriptListReturn(params, res, props, 'pk_predevaluate', tableId, tableId, true);
				// 清除缓存
				props.table.deleteCacheId(tableId, id);
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}
/**
 * 提交 收回 动作
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 * @param {*} record 
 */
function commitClick(OperatorType, props, tableId, index, record) {
	let id = record['pk_predevaluate'].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecode,
			commitType
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, 'pk_predevaluate', tableId, tableId, false, dataSource);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'warning' });
			}
		}
	});
}
