import { ajax } from 'nc-lightapp-front';
import { staticVariable, reqUrl } from '../constants/constant';
import { setBatchBtnsEnable } from './buttonClick';

import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;

const { pagecode, tableId } = staticVariable;
/**
 * 分页查询
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export default function(props, config, pks, isRefresh) {
	let data = {
		allpks: pks,
		pagecode
	};
	ajax({
		url: reqUrl.pageDataByPksUrl,
		data: data,
		success: function(res) {
			setListValue.call(this, props, res);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
