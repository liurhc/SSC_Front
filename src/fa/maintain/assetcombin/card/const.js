// 页面配置
export const pageConfig = {
	// 应用编码
	appcode: '201202516A',
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表单区域id
	formId: 'card_head',
	// 表格区域id
	tableId: 'bodyvos',
	//编辑区id
	body_edit: 'bodyvos_edit',
	// 页面编码
	pagecode: '201202516A_card',
	// 编辑态按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'OpenEdit', 'AddLine', 'DelLine', 'BatchAltert' ],
	// 浏览态按钮
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'OpenBrowse',
		'Print',
		'ReceiptGroup',
		'Refresh',
		'QueryAbout'
	],
	cancelhideBtns: [
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'OpenBrowse',
		'Print',
		'ReceiptGroup',
		'Refresh'
	],
	addhideBtns: [ 'AddLine', 'DelLine', 'BatchAltert' ],
	// 主键字段
	pkField: 'pk_assetcombin',
	// 单据类型
	bill_type: 'HO',
	// 交易类型
	transi_type: 'HO-01',
	// 请求链接
	url: {
		queryCardUrl: '/nccloud/fa/assetcombin/querycard.do',
		insertUrl: '/nccloud/fa/assetcombin/save.do',
		updateUrl: '/nccloud/fa/assetcombin/update.do',
		editUrl: '/nccloud/fa/assetcombin/edit.do',
		printUrl: '/nccloud/fa/assetcombin/printCard.do',
		commitUrl: '/nccloud/fa/assetcombin/commit.do',
		deleteUrl: '/nccloud/fa/assetcombin/delete.do',
		//unCommitUrl: '/nccloud/aum/sale/unCommit.do',
		headAfterEditUrl: '/nccloud/fa/assetcombin/headAfterEdit.do',
		bodyAfterEditUrl: '/nccloud/fa/assetcombin/bodyAfterEdit.do',
		deleteLine: '/nccloud/fa/assetcombin/delline.do',
		mutideleline: '/nccloud/fa/assetcombin/mutidelline.do',
		CheckUrl: '/nccloud/fa/fapub/checkaim.do',
		checkmethod: '/nccloud/fa/fapub/checkmethod.do',
		BatchAltert: '/nccloud/fa/assetcombin/batchAltert.do',
		checkmethodUrl: '/nccloud/fa/assetcombin/checkmethod.do'
	},
	// 打印模板节点标识
	printNodekey: null,
	listUrl: '/fa/maintain/assetcombin/list/index.html',
	cardUrl: '/fa/maintain/assetcombin/card/index.html',
	// 权限资源编码
	resourceCode: '2012044010',
	listRouter: '/list',
	dataSource: 'fa.maintain.assetcombin.main',
	ds: 'dataSource'
};
