import { ajax } from 'nc-lightapp-front';
import { lockrequest } from '../events/buttonClick';
import { pageConfig } from '../const';

import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { getContext } = LoginContext;
const { formId, tableId, pagecode, url, ds, dataSource } = pageConfig;

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'OpenCard':
			let status = props.cardTable.getStatus(tableId);
			if (status === 'browse') {
				props.cardTable.toggleRowView(tableId, record);
			} else {
				props.cardTable.setClickRowIndex(tableId, { record, index });
				props.cardTable.openModel(tableId, 'edit', record, index);
			}
			break;
		case 'DelLine':
			// 若行数据为新增态，则需解锁卡片
			if (record.status == '2') {
				if (record.values.pk_card && record.values.pk_card.value) {
					let dSource = getContext(ds, dataSource);
					let usrid = getContext('userId', dataSource);
					let param = {
						allpks: [ record.values.pk_card.value ],
						msgMap: {
							usrid,
							lockflag: 'unlock',
							dSource
						}
					};
					lockrequest.call(this, param);
				}
			}
			props.cardTable.delRowsByIndex(tableId, index);
			let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
			let dataStr = JSON.stringify(cardData);
			let backdata = JSON.parse(dataStr);
			let bodydata = backdata.body.bodyvos.rows;
			let realbody = [];
			for (let bodyrow of bodydata) {
				if (bodyrow.status != 3) {
					realbody.push(bodyrow);
				}
			}
			backdata.body.bodyvos.rows = realbody;
			ajax({
				url: url.deleteLine,
				data: backdata,
				success: (res) => {
					if (res.data) {
						props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						if (res.data.userjson == 'false') {
							props.form.setFormItemsValue(formId, {
								pk_currency: { value: null, display: null },
								originvalue: { value: null, display: null }
							});
							props.form.setFormItemsDisabled(formId, { pk_currency: false, originvalue: false });
						} else {
							props.form.setFormItemsDisabled(formId, { pk_currency: true, originvalue: true });
						}
					}
				}
			});

			break;
	}
}
