/**
 * 根据单据状态设置按钮可见性
 * @param {} props 
 */
// 编辑态按钮
export const editBtns = [ 'Save', 'SaveCommit', 'Cancel', 'OpenEdit', 'AddLine', 'DelLine', 'BatchAltert' ];
export const browseBtns = [
	//'EditGroup',
	'Add',
	'Edit',
	'Delete',
	'Commit',
	'UnCommit',
	'Attachment',
	'OpenBrowse',
	'Print',
	'ReceiptGroup',
	'Refresh'
];
export const cancelhideBtns = [
	'Edit',
	'Delete',
	'Commit',
	'UnCommit',
	'Attachment',
	'OpenBrowse',
	'Print',
	'ReceiptGroup',
	'Refresh'
];
export const addhideBtns = [ 'AddLine', 'DelLine', 'BatchAltert' ];
const formId = 'card_head';
const tableId = 'bodyvos';
export function setBtnVisibleByBillStatus(props) {
	//let status = props.getUrlParam('status');
	let status = props.form.getFormStatus(formId);
	if (status == 'browse') {
		// 根据单据状态隐藏按钮
		let newHiddenBtns = [ 'Edit', 'Delete', 'Commit', 'UnCommit', 'QueryAboutBillFlow' ];
		let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
		if (bill_status) {
			let bill_status_value = bill_status.value;
			if (bill_status_value === '0') {
				// 自由
				newHiddenBtns.remove('Edit');
				newHiddenBtns.remove('Delete');
				newHiddenBtns.remove('Commit');
			} else if (bill_status_value === '1') {
				// 已提交
				newHiddenBtns.remove('UnCommit');
			} else if (bill_status_value === '2') {
				// 审批中
				newHiddenBtns.remove('QueryAboutBillFlow');
			} else if (bill_status_value === '3') {
				// 审批通过
				newHiddenBtns.remove('QueryAboutBillFlow');
				newHiddenBtns.remove('UnCommit');
			} else if (bill_status_value === '4') {
				// 审批不通过
				newHiddenBtns.remove('QueryAboutBillFlow');
			}
		}
		props.button.setButtonVisible(newHiddenBtns, false);
	}
}
/**
 * 设置按钮的可见性
 * @param {*} props 
 * @param {*} status 
 */
export function setBtnVisibleByOper(props, status) {
	switch (status) {
		case 'add':
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, 'edit');
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			break;
		case 'edit':
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			break;
		case 'browse':
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			props.button.setButtonVisible(browseBtns, true);
			break;
		default:
			// console.log(status);
			break;
	}
}
