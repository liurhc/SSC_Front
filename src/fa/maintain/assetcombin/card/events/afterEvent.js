import { ajax, toast } from 'nc-lightapp-front';
import { addhideBtns } from './setBtnVisible';
import { pageConfig } from '../const';
import { setValue, updateDeptRefLevel } from './buttonClick';
const { formId, tableId, pagecode } = pageConfig;

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { OrgChangeEvent } = components;
const { orgChangeEvent } = OrgChangeEvent;

/**
 * 表头编辑后
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldvalue 
 * @param {*} val 
 */
export function afterEvent(props, moduleId, key, value, oldvalue, val) {
	let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	if (key === 'pk_org_v') {
		let callback = () => {
			ajax({
				url: pageConfig.url.headAfterEditUrl,
				data,
				success: (res) => {
					let is_allow_dept_midlev_new = res.data.userjson;
					res.data.userjson = null;
					setValue.call(this, props, res.data);

					if (value.value) {
						props.cardTable.addRow(tableId, undefined, {}, false);
					}
					updateDeptRefLevel(this, is_allow_dept_midlev_new);
				}
			});
		};
		orgChangeEvent.call(this, props, pagecode, formId, tableId, key, value, oldvalue, false, addhideBtns, callback);
	}

	if (key === 'salvagerate') {
		if (!value.value) {
			return;
		}
		let val = props.form.getFormItemsValue(formId, 'salvagerate').value;

		if (!val) {
			toast({ content: getMultiLangByID('201202516A-000001') /*国际化处理：'请输入数值'*/, color: 'warning' });
			return;
		}
		if (parseFloat(val) < 0 || parseFloat(val) > 100) {
			toast({
				content: getMultiLangByID('201202516A-000002') /*国际化处理：'净残值率输入值必须在在[0,100]之间'*/,
				color: 'warning'
			});
			props.form.setFormItemsValue(formId, { salvagerate: { value: '' } });
			return;
		}
		ajax({
			url: pageConfig.url.headAfterEditUrl,
			data,
			success: (res) => {
				setValue.call(this, props, res.data);
			}
		});
	}
	if (key === 'pk_equip') {
		let name = val.refname;
		props.form.setFormItemsValue(formId, { equip_name: { value: name, display: name } });
	}
	if (key === 'pk_equiporg_v') {
		if (oldvalue.refpk) {
			if (value.refpk) {
				if (oldvalue.refpk != value.refpk) {
					props.form.setFormItemsValue(formId, {
						usedept: { value: null, display: null },
						pk_costcenter: { value: null, display: null },
						pk_assetuser: { value: null, display: null }
					});
				}
			} else {
				props.form.setFormItemsValue(formId, {
					usedept: { value: null, display: null },
					pk_costcenter: { value: null, display: null },
					pk_assetuser: { value: null, display: null }
				});
			}
		} else {
			if (value.refpk) {
				props.form.setFormItemsValue(formId, {
					usedept: { value: null, display: null },
					pk_costcenter: { value: null, display: null },
					pk_assetuser: { value: null, display: null }
				});
			}
		}
	}

	if (key === 'pk_ownerorg_v') {
		if (oldvalue.refpk) {
			if (value.refpk) {
				if (oldvalue.refpk != value.refpk) {
					props.form.setFormItemsValue(formId, {
						pk_mandept: { value: null, display: null }
						//pk_ownerorg: { value: value.refpk, display: value.refname }
					});
				}
			} else {
				props.form.setFormItemsValue(formId, {
					pk_mandept: { value: null, display: null }
				});
			}
		} else {
			props.form.setFormItemsValue(formId, {
				pk_mandept: { value: null, display: null }
			});
		}
	}

	if (key === 'pk_depmethod') {
		if (!value.value) {
			props.form.setFormItemsValue(formId, {
				allworkloan: { value: null },
				accuworkloan: { value: null },
				workloanunit: { value: null }
			});

			props.form.setFormItemsDisabled(formId, { allworkloan: true, accuworkloan: true, workloanunit: true });
			return;
		} else {
			ajax({
				url: pageConfig.url.headAfterEditUrl,
				data,
				success: (res) => {
					setValue.call(this, props, res.data);
					props.form.setFormItemsDisabled(formId, {
						dep_start_date: false
					});
					if (res.data.userjson === 'true') {
						props.form.setFormItemsDisabled(formId, {
							allworkloan: false,
							accuworkloan: false,
							workloanunit: false
						});
					} else {
						props.form.setFormItemsValue(formId, {
							allworkloan: { value: null },
							accuworkloan: { value: null },
							workloanunit: { value: null }
						});
						props.form.setFormItemsDisabled(formId, {
							allworkloan: true,
							accuworkloan: true,
							workloanunit: true
						});
					}
				}
			});
		}
	}
	if (key === 'pk_category' || key === 'begin_date' || key === 'pk_currency') {
		if (!value.value || value.value === oldvalue.value) {
			return;
		} else {
			ajax({
				url: pageConfig.url.headAfterEditUrl,
				data,
				success: (res) => {
					setValue.call(this, props, res.data);
				}
			});
		}
	}
}
