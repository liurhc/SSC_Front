import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { setValue, addLine } from './buttonClick';
import { lockrequest } from '../events/buttonClick';
const { formId, tableId, pagecode, url, ds, dataSource } = pageConfig;

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { getContext } = LoginContext;
/**
 * 表体编辑后
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} method 
 * @param {*} flag 
 */
export default function afterEvent(props, moduleId, key, value, changedrows, index) {
	let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedrows);
	data.index = index;
	if (key === 'pk_card') {
		if (
			changedrows &&
			changedrows.length == 1 &&
			changedrows[0].oldvalue.value == null &&
			changedrows[0].oldvalue.value == changedrows[0].newvalue.value
		) {
			return;
		}
		let kstr = null;
		for (let k = 0; k < changedrows.length; k++) {
			if (
				changedrows[k].newvalue.value != '' &&
				changedrows[k].newvalue.value != undefined &&
				changedrows[k].newvalue.value
			) {
				if (kstr != null) {
					kstr = kstr + ',' + changedrows[k].newvalue.value;
				} else {
					kstr = changedrows[k].newvalue.value;
				}
			}
		}
		let checkdata = {};
		checkdata.id = kstr;
		if (!kstr) {
			ajax({
				url: pageConfig.url.bodyAfterEditUrl,
				data: data,
				success: (res) => {
					setValue.call(this, props, res.data);
					let num = props.cardTable.getNumberOfRows(tableId);
					//如果为最后一行 , 自动增行
					if (num == index + 1) {
						addLine.call(this, props);
					}
					let userjsonVal = JSON.parse(res.data.userjson);
					if (userjsonVal.editEnabel === 'false') {
						props.form.setFormItemsValue(formId, {
							pk_currency: { value: null, display: null },
							originvalue: { value: null, display: null }
						});
						props.form.setFormItemsDisabled(formId, { pk_currency: false, originvalue: false });
					} else {
						props.form.setFormItemsDisabled(formId, { pk_currency: true, originvalue: true });
					}
				},
				error: (res) => {
					toast({ color: 'danger', content: res.message });
					props.cardTable.delRowsByIndex(tableId, index);
					props.cardTable.addRow(tableId, index, {}, false);
				}
			});
		} else {
			ajax({
				url: pageConfig.url.checkmethodUrl,
				data: checkdata,
				success: (res) => {
					let result = res.data.result;
					if (result) {
						props.ncmodal.show(`${pagecode}-confirm`, {
							title: getMultiLangByID('201202516A-000006') /*国际化处理：'询问'*/,
							content: getMultiLangByID(
								'201202516A-000007' /*国际化处理：'您卡片的折旧方法为工作量法，需要预先录入本月工作量，才能准确地计算预提折旧。请确认卡片是否已经录入了本月工作量，如果尚未录入，请到折旧与摊销节点录入本月工作量，然后再进行本次操作？',*/
							),

							beSureBtnClick: () => {
								ajax({
									url: pageConfig.url.bodyAfterEditUrl,
									data: data,
									success: (res) => {
										setValue.call(this, props, res.data);
										let num = props.cardTable.getNumberOfRows(tableId);
										//如果为最后一行 , 自动增行
										if (num == index + 1) {
											addLine.call(this, props);
										}
										let userjsonVal = JSON.parse(res.data.userjson);
										if (userjsonVal.editEnabel === 'false') {
											props.form.setFormItemsValue(formId, {
												pk_currency: { value: null, display: null },
												originvalue: { value: null, display: null }
											});
											props.form.setFormItemsDisabled(formId, {
												pk_currency: false,
												originvalue: false
											});
										} else {
											props.form.setFormItemsDisabled(formId, {
												pk_currency: true,
												originvalue: true
											});
										}
									},
									error: (res) => {
										toast({ color: 'danger', content: res.message });
										props.cardTable.delRowsByIndex(tableId, index);
										props.cardTable.addRow(tableId, index, {}, false);
									}
								});
							},
							cancelBtnClick: () => {
								let pk_old = changedrows[0].oldvalue.value;
								if (pk_old != null && pk_old != undefined && pk_old != '') {
									let dSource = getContext(ds, dataSource);
									let usrid = getContext('userId', dataSource);
									let param = {
										allpks: [ pk_old ],
										msgMap: {
											usrid,
											lockflag: 'unlock',
											dSource
										}
									};
									lockrequest.call(this, param);
								}
								props.cardTable.delRowsByIndex(tableId, index);
								let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
								let bodydata = cardData.body.bodyvos.rows;
								let realbody = [];
								for (let bodyrow of bodydata) {
									if (bodyrow.status != 3) {
										realbody.push(bodyrow);
									}
								}
								cardData.body.bodyvos.rows = realbody;
								ajax({
									url: url.deleteLine,
									data: cardData,
									success: (res) => {
										if (res.data) {
											props.form.setAllFormValue({ [formId]: res.data.head[formId] });
											if (res.data.userjson == 'false') {
												props.form.setFormItemsValue(formId, {
													pk_currency: { value: null, display: null },
													originvalue: { value: null, display: null }
												});
												props.form.setFormItemsDisabled(formId, {
													pk_currency: false,
													originvalue: false
												});
											} else {
												props.form.setFormItemsDisabled(formId, {
													pk_currency: true,
													originvalue: true
												});
											}
										}
									}
								});
								setBatchBtnsEnable.call(this, props, tableId);
							}
						});
					} else {
						ajax({
							url: pageConfig.url.bodyAfterEditUrl,
							data: data,
							success: (res) => {
								setValue.call(this, props, res.data);
								let num = props.cardTable.getNumberOfRows(tableId);
								//如果为最后一行 , 自动增行
								if (num == index + 1) {
									addLine.call(this, props);
								}
								let userjsonVal = JSON.parse(res.data.userjson);
								if (userjsonVal.editEnabel === 'false') {
									props.form.setFormItemsValue(formId, {
										pk_currency: { value: null, display: null },
										originvalue: { value: null, display: null }
									});
									props.form.setFormItemsDisabled(formId, { pk_currency: false, originvalue: false });
								} else {
									props.form.setFormItemsDisabled(formId, { pk_currency: true, originvalue: true });
								}
							},
							error: (res) => {
								toast({ color: 'danger', content: res.message });
								props.cardTable.delRowsByIndex(tableId, index);
								props.cardTable.addRow(tableId, index, {}, false);
							}
						});
					}
				}
			});
		}
	}
}
