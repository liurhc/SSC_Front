import { tableButtonClick } from '../events';
import buttonClick, { setStatus } from './buttonClick';
import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { cardUtils } = utils;
const { StatusUtils, CommonKeys } = commonConst;
const { createOprationColumn } = cardUtils;
const { LoginContext, faQueryAboutUtils } = components;
const { openAssetCardByPk } = faQueryAboutUtils;
const { getContext, loginContext } = LoginContext;
const { UISTATE } = StatusUtils;
const { IBusiRoleConst } = CommonKeys;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addHeadAreaReferFilter, addBodyReferFilter } = ReferFilter;
const { formId, tableId, pagecode, searchAreaId, body_edit, ds, dataSource } = pageConfig;

let defaultConfig = {
	searchId: searchAreaId,
	formId: formId,
	bodyIds: [ tableId, body_edit ],
	//特殊过滤
	specialFields: {
		pk_mandept: {
			//管理部门
			isRunWithChildren: true, //是否执行时包含下级
			defaultRunWithChildren: true, //是否默认勾选执行时包含下级
			orgMulti: 'pk_org', //参照左上业务单元参照框 按data中returnName的过滤字段取 即管理部门优先货主管理组织过滤其次组织其次组织版本
			data: [
				{
					fields: [ 'pk_ownerorg', 'pk_org' ],
					returnName: 'pk_org'
				},
				{
					returnConst: IBusiRoleConst.ASSETORG,
					returnName: 'busifuncode'
				}
			]
		},
		//货主管理组织
		pk_ownerorg_v: {
			addOrgRelation: 'pk_orgs',
			RefActionExtType: 'TreeRefActionExt',
			class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_OWNERORG_VSqlBuilder', //货主管理组织
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		},
		pk_card: {
			RefActionExtType: 'GridRefActionExt',
			class: 'nccloud.web.fa.common.refCondition.PK_CARDSqlBuilder',
			data: [
				{
					fields: [ 'pk_org' ],
					returnName: 'pk_org'
				}
			]
		}
	},
	defPrefix: {
		search: [ 'def', 'bodyvos.def' ],
		head: 'def',
		body: 'def'
	}
};
export default function(props) {
	props.createUIDom(
		{
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context, dataSource);
					// 获取数据源
					getContext(ds, dataSource);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	addHeadAreaReferFilter.call(this, props, meta, defaultConfig);
	addBodyReferFilter.call(this, props, meta, defaultConfig);
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	meta[formId].items.map((item) => {
		// 卡片超链,过滤
		if (item.attrcode == 'card_code') {
			item.renderStatus = 'browse';
			item.render = (cardval, record) => {
				return (
					<a
						className="code-detail-link"
						onClick={() => {
							openAssetCardByPk.call(this, props, record.pk_card.value);
						}}
					>
						{cardval && cardval.value}
					</a>
				);
			};
		}
	});
	meta[tableId].items.map((item) => {
		// 卡片超链,过滤
		if (item.attrcode == 'pk_card') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							openAssetCardByPk.call(this, props, record.values.pk_card.value);
						}}
					>
						{record && record.values.pk_card && record.values.pk_card.display}
					</span>
				);
			};
			item.isMultiSelectedEnabled = true;
		}
		if (item.attrcode == 'usedept') {
			item.itemtype = 'refer';
		}
	});
	meta[body_edit].items.map((item) => {
		if (item.attrcode == 'pk_card') {
			item.isMultiSelectedEnabled = true;
		}
	});
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_equip') {
			// item.itemtype = 'refer';
			// item.metadataProperty = 'aim.EquipHeadVO.equip_code';
			// item.refcode = 'aim/refer/equip/EquipGridRef/index';
			// item.maxlength = '20';
			item.queryCondition = () => {
				let cards;
				let bodyrows = props.cardTable.getVisibleRows(tableId);
				for (let bodyrow of bodyrows) {
					let pk_card = bodyrow.values.pk_card.value;
					if (!cards) {
						cards = pk_card;
					} else {
						cards = cards + ',' + pk_card;
					}
				}
				return {
					GridRefActionExt: 'nccloud.web.fa.common.refCondition.AssetCombinEquipRefFilter',
					fa_cards: cards
				};
			};
		}
	});

	let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
	meta[tableId].items.push(oprCol);

	return meta;
}
/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status') || UISTATE.browse;
	if (status == UISTATE.add) {
		buttonClick.call(this, props, 'Add');
	} else {
		setStatus.call(this, props, status);
	}
}
