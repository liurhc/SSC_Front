import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { formId, tableId, searchAreaId, body_edit, url } = pageConfig;
import ampub from 'ampub';
const { utils, commonConst } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addHeadAreaReferFilter, addBodyReferFilter } = ReferFilter;

export default function beforeEvent(props, moduleId, key, value, index, record) {
	if (key === 'pk_equip') {
		ajax({
			url: url.CheckUrl,
			data: null,
			success: (res) => {
				let result = res.data.result;
				if (!result) {
					props.form.setFormItemsDisabled(formId, { pk_equip: true });
					toast({ content: getMultiLangByID('201202516A-000003') /*国际化处理：'没有安装资产信息管理'*/, color: 'warning' });
					return false;
				}
			}
		});
	} else {
		let flag = this.is_allow_dept_midlev;
		let usedeptflag;
		if (flag == 'N') {
			usedeptflag = true;
		} else {
			usedeptflag = false;
		}
		let defaultConfig = {
			searchId: searchAreaId,
			formId: formId,
			bodyIds: [ tableId, body_edit ],
			onlyLeafCanSelect: {
				// 使用部门
				usedept: usedeptflag
			},
			//特殊过滤
			specialFields: {
				usedept: {
					data: [
						{
							fields: [ 'pk_equiporg', 'pk_org' ], //可被多个字段过滤 取第一个有的值
							returnName: 'pk_org' //被过滤字段名后台需要参数
						},
						{
							returnConst: IBusiRoleConst.ASSETORG,
							returnName: 'busifuncode'
						}
					]
				},
				pk_mandept: {
					//管理部门
					isRunWithChildren: true, //是否执行时包含下级
					defaultRunWithChildren: true, //是否默认勾选执行时包含下级
					orgMulti: 'pk_org', //参照左上业务单元参照框 按data中returnName的过滤字段取 即管理部门优先货主管理组织过滤其次组织其次组织版本
					data: [
						{
							fields: [ 'pk_ownerorg', 'pk_org' ],
							returnName: 'pk_org'
						},
						{
							returnConst: IBusiRoleConst.ASSETORG,
							returnName: 'busifuncode'
						}
					]
				},
				//货主管理组织
				pk_ownerorg_v: {
					addOrgRelation: 'pk_orgs',
					RefActionExtType: 'TreeRefActionExt',
					class: 'nccloud.web.fa.newasset.transasset.refcondition.PK_OWNERORG_VSqlBuilder', //货主管理组织
					data: [
						{
							fields: [ 'pk_org' ],
							returnName: 'pk_org'
						}
					]
				},
				pk_card: {
					RefActionExtType: 'GridRefActionExt',
					class: 'nccloud.web.fa.common.refCondition.PK_CARDSqlBuilder',
					data: [
						{
							fields: [ 'pk_org' ],
							returnName: 'pk_org'
						}
					]
				}
			},
			defPrefix: {
				search: [ 'def', 'bodyvos.def' ],
				head: 'def',
				body: 'def'
			}
		};
		let meta = props.meta.getMeta();
		addHeadAreaReferFilter.call(this, props, meta, defaultConfig);
		addBodyReferFilter.call(this, props, meta, defaultConfig);
	}
	return true;
}
