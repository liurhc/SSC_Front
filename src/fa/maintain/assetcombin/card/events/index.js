import buttonClick, {
	lockcards,
	getDataByPk,
	backToList,
	save,
	loadDataByPk,
	setStatus,
	runScript,
	commitrunScript,
	saveCommit,
	lockrequest,
	updateDeptRefLevel
} from './buttonClick';
import initTemplate from './initTemplate';
import { afterEvent } from './afterEvent';
import pageInfoClick from './pageInfoClick';
import tableButtonClick from './tableButtonClick';
import bodyAfterEvent from './bodyAfterEvent';
import rowSelected from './rowSelected';
import beforeEvent from './beforeEvent';
export {
	buttonClick,
	afterEvent,
	initTemplate,
	pageInfoClick,
	tableButtonClick,
	bodyAfterEvent,
	lockcards,
	getDataByPk,
	backToList,
	save,
	loadDataByPk,
	setStatus,
	rowSelected,
	runScript,
	saveCommit,
	beforeEvent,
	commitrunScript,
	lockrequest,
	updateDeptRefLevel
};
