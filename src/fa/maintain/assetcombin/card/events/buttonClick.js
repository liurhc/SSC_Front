import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { afterEvent } from './afterEvent';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils, CommonKeys } = commonConst;
const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = cardUtils;
const { LoginContext, ScriptReturnUtils } = components;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getContext, loginContextKeys } = LoginContext;
const { UISTATE } = StatusUtils;
const { approveConst } = CommonKeys;
import fa from 'fa';
const { fa_components, fa_utils } = fa;
const { DefaultDate } = fa_utils;
const { ImageMng } = fa_components;
const { faImageScan, faImageView } = ImageMng;
const { dateFormat } = DefaultDate;

const {
	formId,
	tableId,
	pagecode,
	editBtns,
	browseBtns,
	pkField,
	url,
	listRouter,
	resourceCode,
	dataSource,
	bill_type,
	ds
} = pageConfig;

export default function(props, id) {
	dateFormat();
	switch (id) {
		case 'Save':
			save.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'output':
			outputTemp.call(this, props);
			break;
		case 'Add':
			add.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'AddLine':
			addLine.call(this, props);
			break;
		case 'DelLine':
			delLine.call(this, props);
			break;
		case 'BatchAltert':
			//获取变化的值
			BatchAltert.call(this, props);
			break;
		case 'UnCommit':
			unCommit.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props);
			break;
		case 'SaveCommit':
			saveCommit.call(this, props);
			break;
		case 'FAReceiptScan':
			faReceiptScan.call(this, props);
			break;
		case 'FAReceiptShow':
			faReceiptShow.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		case 'QueryAboutBillFlow': // 审批详情
			queryAboutBillFlow.call(this, props);
			break;
		default:
			break;
	}
}

const BatchAltert = (props) => {
	let changeData = props.cardTable.getTableItemData(tableId);
	if (changeData.batchChangeKey != 'pk_card') {
		let returnData = props.cardTable.batchChangeTableData(tableId);
		let editCode = returnData.code;
		let data = {
			code: editCode,
			value: returnData.value
		};
		props.cardTable.filterEmptyRows(tableId, [ 'pk_card', 'pk_assetcombin_b' ], 'include');
		let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
		CardData.userjson = JSON.stringify(data);
		ajax({
			url: url.BatchAltert,
			data: CardData,
			success: (res) => {
				if (res.success) {
					if (res.data) {
						if (res.data.body && res.data.body[tableId]) {
							props.cardTable.setTableData(tableId, res.data.body[tableId]);
						}
					}
				}
			}
		});
	}
};
//影像扫描
function faReceiptScan(props) {
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageScan(props, imageData);
}

//影像查看
function faReceiptShow(props) {
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	faImageView(props, imageData);
}

/**
 * 收回
 * @param {*} props 
 */
function unCommit(props) {
	runScript.call(this, props, 'UNSAVE', 'commit');
}
/**
 * 提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function commit(props) {
	commitrunScript.call(this, props, 'SAVE', 'commit');
}
/** 
 * * 提交\收回
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */

export function commitrunScript(props, operatorType, commitType, content) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};

	let obj = {
		dataType: 'cardData',
		OperatorType: operatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: CardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let callback = () => {
					setStatus.call(this, props, UISTATE.browse);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					null,
					false,
					callback
				);
			}
		},
		error: (res) => {
			if (res && res.message) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}

/** 
 * * 提交\收回
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */

export function runScript(props, operatorType, commitType, content) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: operatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: CardData,
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			let { success, data } = res;
			if (success) {
				let callback = () => {
					setBrowseBtnsVisible.call(this, props);
				};
				if (data.success === 'allsuccess') {
					getScriptCardReturnData.call(
						this,
						res,
						this.props,
						formId,
						tableId,
						'pk_assetcombin',
						dataSource,
						null,
						false,
						callback
					);
				} else {
					toast({ content: data.errorMsg, color: 'warning' });
					setBrowseBtnsVisible.call(this, props);
				}
			}
		}
	});
}

export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		//toast({ content: '请选择需要打印的数据', color: 'warning' });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		//toast({ content: '请选择需要输出的数据', color: 'warning' });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(formId, pkField);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: getMultiLangByID('201202516A-000000') /*国际化处理：资产合并*/, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}
function refresh(props) {
	let id = props.form.getFormItemsValue(formId, pkField);
	if (!id.value) {
		showMessage.call(this, props, { type: MsgConst.Type.RefreshFailed });
		//toast({ content: '刷新失败', color: 'error' });
		return;
	} else {
		showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		//toast({ content: '刷新成功', color: 'success' });
	}
	getDataByPk.call(this, props, id.value);
}

/**
 * 加锁，解锁卡片
 * @param {*} props 
 */
export function lockcards(props, lockflag, async = true) {
	let data = props.cardTable.getAllRows(tableId);
	let allpks = [];
	// 取消时，解锁表体所有卡片，包括新增的，修改的
	for (let val of data) {
		// 判断如果是新增删除，在删除时已解锁，这里不解锁，若是修改删除，在这里需要解锁
		if (!val.values.pk_assetcombin_b.value && val.status === '3') {
		} else {
			if (val.values.pk_card.value) {
				allpks.push(val.values.pk_card.value);
			}
		}
	}
	let dSource = getContext(ds, dataSource);
	let usrid = getContext('userId', dataSource);
	let param = {
		allpks,
		msgMap: {
			usrid,
			lockflag,
			dSource
		}
	};
	if (allpks && allpks.length > 0 && usrid && dSource) {
		lockrequest.call(this, param, async);
	}
}
export function lockrequest(param, async = true) {
	ajax({
		url: '/nccloud/fa/facard/lockcard.do',
		data: param,
		async,
		success: (res) => {},
		error: (res) => {}
	});
}
/**
 * 附件上传
 * @param {*} props 
 */
function attachment(props) {
	// 单据主键
	let billId = props.form.getFormItemsValue(formId, pkField).value;
	props.ncUploader.show(`${pagecode}-uploader`, {
		billId: 'fa/assetcombin/' + billId
	});
}

/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk) {
	ajax({
		url: url.queryCardUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					cardCache.updateCache(pkField, pk, data, formId, dataSource);
				} else {
					setValue.call(this, props, undefined);
					cardCache.deleteCacheById(pkField, pk, dataSource);
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted }); /**国际化处理：此数据已删除！ */
					//toast({ content: '此数据已删除！', color: 'warning' });
				}
			}
		}
	});
}

/**
* 返回
* @param {*} props 
*/
export function backToList(props) {
	props.pushTo(listRouter, { pagecode: pagecode });
}

/**
* 新增
* @param {*} props 
*/
export function add(props) {
	props.cardTable.closeExpandedRow(tableId);
	setStatus.call(this, props, UISTATE.add);
	this.is_allow_dept_midlev = 'N';
	// 清空数据
	setValue.call(this, props, undefined);
	setDefaultValue.call(this, props);
	// 设置字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (pk_org_v && pk_org_v.value) {
		afterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		setBodyBtnsEnable.call(this, props, tableId);
	}
}

/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAltert' ], true);
		return;
	}
	props.button.setButtonDisabled([ 'AddLine', 'BatchAltert' ], false);
	let checkedRows = [];
	let num = props.cardTable.getNumberOfRows(moduleId, false);
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}
/**
 * 新增设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	// 模板默认值处理

	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId, dataSource);
	let groupName = getContext(loginContextKeys.groupName, dataSource);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype, dataSource);
	let pk_transitype = getContext(loginContextKeys.pk_transtype, dataSource);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org, dataSource);
	let org_Name = getContext(loginContextKeys.org_Name, dataSource);
	let pk_org_v = getContext(loginContextKeys.pk_org_v, dataSource);
	let org_v_Name = getContext(loginContextKeys.org_v_Name, dataSource);
	let businessDate = getContext(loginContextKeys.businessDate, dataSource);
	let usrid = getContext(loginContextKeys.userId, dataSource);
	let defaultAccbookPk = getContext(loginContextKeys.defaultAccbookPk, dataSource);
	let defaultAccbookName = getContext(loginContextKeys.defaultAccbookName, dataSource);
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		pk_accbook: { value: defaultAccbookPk, display: defaultAccbookName },
		transi_type: { value: transi_type },
		bill_type: { value: bill_type },
		pk_transitype: { value: pk_transitype },
		business_date: { value: businessDate },
		bill_status: {
			value: '0',
			display: getMultiLangByID('statusUtils-000000') /*国际化处理：'自由态'*/
		},
		createcard_date: {
			value: new Date().Format('yyyy-MM-dd hh:mm:ss')
		}
	});
}
/**
* 修改
* @param {*} props 
*/
export function edit(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		pk = props.getUrlParam('id');
	}
	if (!pk) {
		return;
	}
	props.cardTable.closeExpandedRow(tableId);
	let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
	ajax({
		url: pageConfig.url.editUrl,
		data: {
			pk,
			resourceCode: resourceCode,
			pk_org
		},
		success: (res) => {
			if (res.data.enableLeaf) {
				updateDeptRefLevel(this, res.data.enableLeaf);
			}
			if (res.data.result) {
				toast({ color: 'warning', content: res.data.result });
			} else {
				setStatus.call(this, props, UISTATE.edit);
				props.resMetaAfterPkorgEdit();
				props.form.setFormItemsDisabled(formId, { pk_org_v: true });
			}
		}
		// error: (res) => {
		// 	if (res && res.message) {
		// 		toast({ color: 'danger', content: res.message });
		// 	}
		// }
	});
}
export function updateDeptRefLevel(that, is_allow_dept_midlev_new) {
	if (is_allow_dept_midlev_new === undefined || is_allow_dept_midlev_new === null) {
		return;
	}
	that.is_allow_dept_midlev = is_allow_dept_midlev_new;
	let meta = that.props.meta.getMeta();
	if (is_allow_dept_midlev_new == 'N') {
		meta[formId].items.map((item) => {
			// 管理部门
			if (item.attrcode === 'pk_mandept' || item.attrcode === 'pk_mandept_v') {
				item.onlyLeafCanSelect = true;
			}
			// 使用部门
			if (item.attrcode === 'usedept') {
				item.onlyLeafCanSelect = true;
			}
		});
	} else {
		meta[formId].items.map((item) => {
			// 管理部门
			if (item.attrcode === 'pk_mandept' || item.attrcode === 'pk_mandept_v') {
				item.onlyLeafCanSelect = false;
			}
			// 使用部门
			if (item.attrcode === 'usedept') {
				item.onlyLeafCanSelect = false;
			}
		});
	}
	that.props.meta.setMeta(meta);
}
/**
* 删除
* @param {*} props 
*/
export function delConfirm(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delReal });
}
/**
 * 删除
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function delReal(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'DELETE',
		commitType: 'commit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: CardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data.success === 'allsuccess') {
					// 清除缓存
					cardCache.deleteCacheById(pkField, pk, dataSource);
					toast({ content: data.successMsg, color: 'success' });
					// 加载下一条数据
					let newpk = cardCache.getNextId(pk, dataSource);
					loadDataByPk.call(this, props, newpk);
				} else {
					toast({ content: data.errorMsg, color: 'warning' });
				}
				//setBillFlowActionVisible.call(this, props);
			}
		}
	});
}

/**
* 保存
* @param {*} props 
*/
export function save(props) {
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	if (flag) {
		let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
		let reqUrl = url.insertUrl; //新增保存
		let oldstatus = props.form.getFormStatus(formId);
		if (oldstatus == UISTATE.edit) {
			reqUrl = url.updateUrl; //修改保存
		}
		props.validateToSave(cardData, () => {
			ajax({
				url: reqUrl,
				data: cardData,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.cardTable.closeModel(tableId);
						afterSave.call(this, props, data, oldstatus);
					}
				}
			});
		});
	}
}
function cancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancel });
}
export function cancel(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	// 解锁卡片
	lockcards.call(this, props, 'unlock');
	props.form.cancel(formId);
	props.cardTable.resetTableData(tableId);
	setStatus.call(this, props, UISTATE.browse);
	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}

	loadDataByPk.call(this, props, pk);
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	setCardValue.call(this, props, data);
	if (status == UISTATE.browse) {
		setBrowseBtnsVisible.call(this, props);
	} else {
		setBodyBtnsEnable.call(this, props, tableId);
	}
}
/**
 * 浏览态设置按钮显示隐藏
 * @param {*} props 
 */
function setBrowseBtnsVisible(props) {
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	if (!pkVal || !pkVal.value) {
		props.button.setButtonVisible(browseBtns, false);
		props.button.setButtonVisible('Add', true);
		return;
	}
	props.button.setButtonVisible(browseBtns, true);
	setBillFlowBtnsVisible.call(this, props, formId, pkField);
}
/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	// 更新参数
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	props.setUrlParam({ status, id: pkVal ? pkVal.value : '' });
	switch (status) {
		case UISTATE.add:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			``;
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			break;
		case UISTATE.edit:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});

			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			//props.button.setButtonVisible(browseBtns, true);
			setBrowseBtnsVisible.call(this, props);
			break;
		default:
			// console.log(status);
			break;
	}
	setHeadAreaData.call(this, props, { status });
}
/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	let num = props.cardTable.getNumberOfRows(moduleId);
	let checkedRows = [];
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	props.button.setButtonDisabled([ 'BatchAlter' ], num <= 0);
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk) {
	if (!pk || pk == 'null') {
		setValue.call(this, props, undefined);
		return;
	}
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData) {
		setValue.call(this, props, cachData);
	} else {
		getDataByPk.call(this, props, pk);
	}
}
/**
* 保存前校验
* @param {*} props 
*/
function validateBeforeSave(props) {
	// 过滤空行
	props.cardTable.filterEmptyRows(tableId, [ 'pk_card', 'pk_assetcombin_b' ], 'include');
	// 表头必输校验
	let flag = props.form.isCheckNow(formId);
	if (!flag) {
		return false;
	}
	// 表体必输校验
	let bodyvalidate = props.cardTable.checkTableRequired(tableId);
	if (!bodyvalidate) {
		return false;
	}
	let allRows = props.cardTable.getAllRows(tableId, true);
	let i = allRows.length;
	for (let row of allRows) {
		if (row.status == 3) {
			i--;
		}
	}
	if (i <= 1) {
		toast({ color: 'danger', content: getMultiLangByID('201202516A-000004') /*国际化处理：'合并的卡片最少有2张!'*/ });
		return false;
	}
	return true;
}

/**
* 保存后
* @param {*} props 
*/
export function afterSave(props, data, oldstatus) {
	setValue.call(this, props, data);
	setStatus.call(this, props, UISTATE.browse);
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	// 保存成功后处理缓存
	let cachData = this.props.createMasterChildData(pagecode, formId, tableId);
	if (oldstatus == UISTATE.add) {
		cardCache.addCache(pk, cachData, formId, dataSource);
	} else {
		cardCache.updateCache(pkField, pk, cachData, formId, dataSource);
	}
	setBrowseBtnsVisible.call(this, props);
	showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
}

/**
* 新增行
* @param {*} props 
*/
export function addLine(props) {
	props.cardTable.addRow(tableId, undefined, {}, false);
	setBatchBtnsEnable.call(this, props, tableId);
}
/**
* 删除行
* @param {*} props 
*/
export function delLine(props) {
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	let allpks = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
		// 若删除行为新增数据，则记录为需解锁卡片
		if (item.data.status == 2) {
			if (item.data.values.pk_card.value) {
				allpks.push(item.data.values.pk_card.value);
			}
		}
	});
	//若待解锁卡片数量大于0，则解锁
	if (allpks.length > 0) {
		let dSource = getContext(ds, dataSource);
		let usrid = getContext('userId', dataSource);
		let param = {
			allpks,
			msgMap: {
				usrid,
				lockflag: 'unlock',
				dSource
			}
		};
		lockrequest.call(this, param);
	}
	props.cardTable.delRowsByIndex(tableId, checkedIndex);
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let bodydata = cardData.body.bodyvos.rows;
	let realbody = [];
	for (let bodyrow of bodydata) {
		if (bodyrow.status != 3) {
			realbody.push(bodyrow);
		}
	}
	cardData.body.bodyvos.rows = realbody;
	ajax({
		url: url.deleteLine,
		data: cardData,
		success: (res) => {
			if (res.data) {
				props.form.setAllFormValue({ [formId]: res.data.head[formId] });
				if (res.data.userjson == 'false') {
					props.form.setFormItemsValue(formId, {
						pk_currency: { value: null, display: null },
						originvalue: { value: null, display: null }
					});
					props.form.setFormItemsDisabled(formId, { pk_currency: false, originvalue: false });
				} else {
					props.form.setFormItemsDisabled(formId, { pk_currency: true, originvalue: true });
				}
			}
		}
	});
	setBatchBtnsEnable.call(this, props, tableId);
}

/**
 * 保存提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */

/**
 * 保存提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function saveCommit(props, content) {
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	let oldstatus = props.form.getFormStatus(formId);
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'SAVE',
		commitType: 'saveCommit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	if (content) {
		ajax({
			url: url.commitUrl,
			data: CardData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					this.setState({
						compositedisplay: false
					});
					let callback = () => {
						if (data.success === approveConst.HASAPPROVEALTRAN) {
						} else {
							afterCommitSave.call(this, props, data.cardVos[0], oldstatus);
						}
						setBrowseBtnsVisible.call(this, props);
					};
					getScriptCardReturnData.call(
						this,
						res,
						this.props,
						formId,
						tableId,
						'pk_assetcombin',
						dataSource,
						null,
						false,
						callback
					);
				}
			}
		});
	} else {
		props.validateToSave(CardData, () => {
			ajax({
				url: url.commitUrl,
				data: CardData,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						let callback = () => {
							if (data.success === approveConst.HASAPPROVEALTRAN) {
							} else {
								afterCommitSave.call(this, props, data.cardVos[0], oldstatus);
							}
							setBrowseBtnsVisible.call(this, props);
						};
						getScriptCardReturnData.call(
							this,
							res,
							this.props,
							formId,
							tableId,
							'pk_assetcombin',
							dataSource,
							null,
							false,
							callback
						);
					}
				}
			});
		});
	}
}
/**
* 保存提交后
* @param {*} props 
*/
export function afterCommitSave(props, data, oldstatus) {
	setStatus.call(this, props, UISTATE.browse);
}
/**
 * 审批详情
 * @param {*} props 
 */
function queryAboutBillFlow(props) {
	let id = props.form.getFormItemsValue(formId, pkField).value;
	let transi_type = props.form.getFormItemsValue(formId, 'transi_type').value;
	this.setState({
		showApprove: true,
		pk_bill: id,
		transi_type
	});
}
