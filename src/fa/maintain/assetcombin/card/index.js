import React, { Component } from 'react';
import { createPage, base, high, ajax } from 'nc-lightapp-front';
import { pageConfig } from './const';
import {
	buttonClick,
	initTemplate,
	afterEvent,
	pageInfoClick,
	bodyAfterEvent,
	lockcards,
	loadDataByPk,
	backToList,
	save,
	rowSelected,
	commitrunScript,
	beforeEvent,
	lockrequest,
	saveCommit
} from './events';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

const { ApproveDetail } = high;
const { formId, tableId, pagecode, dataSource, url, ds } = pageConfig;
const { NCAffix } = base;
class MasterChildCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pk_org: '',
			bill_status: '',
			showApprove: false,
			pk_bill: '',
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		this.is_allow_dept_midlev = 'N'; //是否允许部门非末级(组织级数据)
		initTemplate.call(this, props);
	}
	componentWillMount() {
		// 解锁卡片
		window.onbeforeunload = () => {
			let status = this.props.cardTable.getStatus(tableId);
			if (status == 'edit') {
				lockcards.call(this, this.props, 'unlock', false);
			}
		};
		if (window.history && window.history.pushState) {
			window.onpopstate = () => {
				lockcards.call(this, this.props, 'unlock', false);
			};
		}
	}
	// 移除事件
	componentWillUnmount() {
		window.onbeforeunload = null;
	}
	componentDidMount() {
		if (this.props.getUrlParam('status') != 'add') {
			let pk = this.props.getUrlParam('id');
			if (pk && pk != 'undefined') {
				loadDataByPk.call(this, this.props, pk);
			}
		}
	}
	//提交及指派 回调
	getAssginUsedr = (value) => {
		let status = this.props.form.getFormStatus(formId) || UISTATE.browse;
		if (status == UISTATE.browse) {
			commitrunScript.call(this, this.props, 'SAVE', 'commit', value);
		} else {
			saveCommit.call(this, this.props, value);
		}
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};
	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: (props, id) => {
							buttonClick.call(this, props, id, tableId);
						},
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};
	// // 侧拉删行解锁
	modelDelRow = (props, moduleId, moduleIndex, row) => {
		let cardData = props.createMasterChildData(pagecode, formId, tableId);
		let bodydata = cardData.body.bodyvos.rows;
		let realbody = [];
		for (let bodyrow of bodydata) {
			if (bodyrow.status != 3) {
				realbody.push(bodyrow);
			}
		}
		cardData.body.bodyvos.rows = realbody;
		ajax({
			url: url.deleteLine,
			data: cardData,
			success: (res) => {
				if (res.data) {
					props.form.setAllFormValue({ [formId]: res.data.head[formId] });
				}
			}
		});
	};
	modelDelRowBefore = (props, moduleId, moduleIndex, row) => {
		let allpks = [ row.values.pk_card.value ];
		let dSource = getContext(loginContextKeys.dataSourceCode, dataSource);
		let usrid = getContext('userId', dataSource);
		let param = {
			allpks,
			msgMap: {
				usrid,
				lockflag: 'unlock',
				dSource
			}
		};
		lockrequest.call(this, param);
		return true;
	};
	render() {
		let { cardTable, form, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createModal } = ncmodal;
		let status = form.getFormStatus(formId) || UISTATE.browse;
		return (
			<div id="nc-bill-card" className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID('201202516A-000000') /*国际化处理：资产合并*/,
								formId,
								backBtnClick: backToList
							})}

							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>

					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: save.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							showCheck: true,
							showIndex: true,
							selectedChange: rowSelected.bind(this),
							modelDelRowBefore: this.modelDelRowBefore.bind(this),
							modelDelRow: this.modelDelRow.bind(this)
						})}
					</div>
				</div>
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/* 附件管理框 */}
				{createNCUploader(`${pagecode}-uploader`, {})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002') /*国际化处理：'指派'*/}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

const MasterChildCardBase = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(MasterChildCard);

export default MasterChildCardBase;
