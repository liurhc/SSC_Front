import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick, { runScript } from './buttonClick';
import doubleClick from './doubleClick';
import tableButtonClick from './tableButtonClick';
import { rowSelected, setBatchBtnsEnable } from './rowSelected';
import afterEvent from './afterEvent';
export {
	searchBtnClick,
	pageInfoClick,
	initTemplate,
	buttonClick,
	doubleClick,
	tableButtonClick,
	rowSelected,
	setBatchBtnsEnable,
	afterEvent,
	runScript
};
