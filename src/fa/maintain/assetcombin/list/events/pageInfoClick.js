import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { setBatchBtnsEnable } from '../events';

import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;

const { tableId, pagecode } = pageConfig;
/**
 * 分页处理
 * @param {*} props 
 */
export default function(props, config, pks) {
	let data = {
		allpks: pks,
		pagecode
	};
	ajax({
		url: pageConfig.url.queryPageUrl,
		data: data,
		success: function(res) {
			let { success, data } = res;
			if (success) {
				setListValue.call(this, props, res);
				setBatchBtnsEnable.call(this, props, tableId);
			}
		}
	});
}
