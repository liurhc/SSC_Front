// 页面配置
export const pageConfig = {
	// 应用编码
	appcode: '201202516A',
	// 应用名称
	title: '资产合并',
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '201202516A_list',
	// 主键字段
	pkField: 'pk_assetcombin',
	// 单据类型
	bill_type: 'HO',
	// 交易类型
	transi_type: 'HO-01',
	// 请求链接
	url: {
		queryUrl: '/nccloud/fa/assetcombin/query.do',
		queryPageUrl: '/nccloud/fa/assetcombin/querypagelistbypks.do',
		editUrl: '/nccloud/fa/assetcombin/edit.do',
		printUrl: '/nccloud/fa/assetcombin/printCard.do',
		commitUrl: '/nccloud/fa/assetcombin/commit.do',
		deleteUrl: '/nccloud/fa/assetcombin/delete.do'
		//unCommitUrl: '/nccloud/aum/sale/unCommit.do'
	},

	// 权限资源编码
	resourceCode: '2012044010',
	// 打印模板节点标识
	printNodekey: null,
	cardRouter: '/card',
	cardUrl: '/fa/maintain/assetcombin/card/index.html',
	dataSource: 'fa.maintain.assetcombin.main',
	ds: 'DataSourceCode'
};
