import { ajax, toast } from 'nc-lightapp-front';
import { commonConfig } from '../const';
// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { toastColor, pageCode, router, field, modelContent } = commonConfig;
/**
 * 与后台交互基础方法
 * @param {*} props 
 * @param {*} url 地址
 * @param {*} data 数据
 * @param {*} success 成功后回调事件
 * @param {*} error 失败后回调事件，默认弹出提示框
 */
export function commonAjax(props, url, data, success = (res) => {}, error) {
	let that = this;
	function defalutError(res) {
		if (res && res.message) {
			toast({ color: toastColor.error, content: res.message }); //提示错误信息
		}
	}
	ajax({
		url: url,
		data: data,
		success: success.bind(that),
		error: error || defalutError
	});
}

/**
 * 跳转至卡片态方法
 * @param {*} props 
 * @param {*} record 行数据
 */
export function toCardPage(props, record) {
	props.pushTo(router.cardRouter, {
		id: record[field.pkField] && record[field.pkField].value,
		pagecode: pageCode.cardPgaeCode
	});
}

/**
 * 跳转至新增页面方法
 * @param {}} props 
 */
export function toAddPage(props) {
	props.pushTo(router.addRouter, { pagecode: pageCode.addPageCode });
}

export function toListPage(props) {
	props.pushTo(router.listRouter, { pagecode: pageCode.listPageCode });
}
/**
 * 调用模态框的基本方法
 * @param {*} props 
 * @param {*} pagecode 当前页面编码
 * @param {*} buttonId 按钮ID
 * @param {*} confirm 确认函数
 */
export function showModal(props, pagecode, buttonId, confirm = () => {}) {
	props.ncmodal.show(`${pagecode}-confirm`, {
		title: getMultiLangByID(modelContent[buttonId].title), //模态框标题
		content: getMultiLangByID(modelContent[buttonId].content), //模态框内容
		beSureBtnClick: confirm
	});
}

/**
 * 批量toast
 * @param {*} content 显示内容 
 * @param {*} msg 展开显示内容，数组
 * @param {*} color 颜色 默认waring
 * @param {*} time 超时时间 默认不超时
 */
export function groupToast(content = '', msg = [], color = 'danger', time = 'infinity') {
	content = getMultiLangByID(content); //处理多语
	let toastMsg = '';
	msg &&
		msg.map((m) => {
			toastMsg += getMultiLangByID(m) + '\n';
		}); //处理多语
	toast({
		content: toastMsg,
		duration: time, //消失时间，默认是3秒; 值为 infinity 时不消失,非必输
		color: color
	});
}
