import { asyncComponent } from 'nc-lightapp-front';
import { commonConfig } from '../const';

const List = asyncComponent(() =>
	import(/* webpackChunkName: "fa/terminal/simulatedep/list/list" */ /* webpackMode: "eager" */ '../list')
); //列表
const Card = asyncComponent(() =>
	import(/* webpackChunkName: "fa/terminal/simulatedep/card/card" */ /* webpackMode: "eager" */ '../card')
); //卡片
const Add = asyncComponent(() =>
	import(/* webpackChunkName: "fa/terminal/simulatedep/add/add" */ /* webpackMode: "eager" */ '../add')
); //新增

const { router } = commonConfig;

const routes = [
	{
		path: router.default,
		component: List,
		exact: true
	},
	{
		path: router.listRouter,
		component: List
	},
	{
		path: router.cardRouter,
		component: Card
	},
	{
		path: router.addRouter,
		component: Add
	}
];

export default routes;
