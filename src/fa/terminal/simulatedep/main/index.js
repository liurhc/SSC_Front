import { RenderRouter } from 'nc-lightapp-front';
import routes from './router';

// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

(function main(routers, htmlTagid) {
	initMultiLangByModule({ fa: [ '201204024A' ], ampub: [ 'common' ] }, () => {
		RenderRouter(routers, htmlTagid);
	});
})(routes, 'app');
