import { commonAjax, groupToast } from '../../components/commonUtil';
import { pageConfig } from '../const';
// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
// import {setBillFlowBtnsEnable,setQueryInfoCache,setListValue,listConst} from '../../../../../ampub/common/utils/listUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setBillFlowBtnsEnable, setQueryInfoCache, setListValue, listConst } = listUtils;

const { areaCode, pageCode, url } = pageConfig;
/**
 * 查询按钮点击事件
 * @param {object} props 扩展后的props
 * @param {object} items 查询条件
 * @param {string} type 
 * @param {object} isRefresh
 */
export function searchButtonClick(props, items, type, queryInfo, isRefresh) {
	//查询条件为空
	if (items === false) {
		return;
	}
	//如果查询条件为空
	if (!queryInfo || !queryInfo.querycondition || !queryInfo.querycondition.conditions) {
		queryInfo = props.search.getQueryInfo(areaCode.searchId);
	}

	//判断开始日期是否大于结束日期
	let valid = true;
	let validMsg = [];
	queryInfo &&
		queryInfo.querycondition &&
		queryInfo.querycondition.conditions &&
		queryInfo.querycondition.conditions.map((condition) => {
			if (condition.field == 'bill_date' || condition.field == 'begindate' || condition.field == 'enddate') {
				let name = '';
				if (condition.field == 'bill_date') {
					name = '201204024A-000019' /*国际化处理：单据日期*/;
				} else if (condition.field == 'begindate') {
					name = '201204024A-000020' /*国际化处理：模拟折旧开始日期*/;
				} else {
					name = '01204024A-000021' /*国际化处理：模拟折旧结束日期*/;
				}
				if (condition.value.secondvalue && condition.value.secondvalue < condition.value.firstvalue) {
					validMsg.push(
						`${getMultiLangByID(name)}${getMultiLangByID('201204024A-000023')}`
					) /*国际化处理：结束日期早于开始日期！*/;
					valid = false;
				}
			}
		});
	if (!valid) {
		groupToast(getMultiLangByID('201204024A-000022'), validMsg);
		return;
	}
	//如果是刷新，不需要设置queryInfo的数据
	if (!isRefresh) {
		queryInfo.pageInfo = props.table.getTablePageInfo(areaCode.tableId);
		queryInfo.pagecode = pageCode.listPageCode;
		setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	}
	//执行查询
	commonAjax.call(this, props, url.queryUrl, queryInfo, (res) => {
		let { success, data } = res;
		if (success) {
			setListValue.call(
				this,
				props,
				data,
				areaCode.tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBillFlowBtnsEnable.call(this, props, { tableId: areaCode.tableId });
		}
	});
}
