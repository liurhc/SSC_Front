import { pageConfig } from '../const';
import { tableButtonClick } from '.';
// import { loginContext } from '../../../../../ampub/common/components/AMInitInfo/loginContext';
// import { setBillFlowBtnsEnable } from '../../../../../ampub/common/utils/listUtils';
// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
// import { addSearchAreaReferFilter } from '../../../../common/components/ReferFilter';
import ampub from 'ampub';
const { components, utils } = ampub;
const { LoginContext } = components;
const { multiLangUtils, listUtils } = utils;
const { loginContext } = LoginContext;
const { getMultiLangByID } = multiLangUtils;
const { setBillFlowBtnsEnable } = listUtils;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

const {
	areaCode,
	pageCode,
	buttonId,
	router,
	field,
	tableCol,
	tableButtonId,
	buttonAreaCode,
	queryConfig,
	modelContent
} = pageConfig;

export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pageCode.listPageCode //页面编码
		},
		function(data) {
			if (data) {
				if (data.button) {
					let button = data.button; //按钮模板
					props.button.setButtons(button);
					props.button.setPopContent(tableButtonId.delete, getMultiLangByID(modelContent.Delete.content));
					props.button.setButtonDisabled(buttonId.delete, true); //默认禁用删除按钮
				}
				if (data.template) {
					let meta = data.template; //页面模板
					modifierMeta(props, meta); //添加超链接与操作列，参照过滤
					props.meta.setMeta(meta, () => {
						setBillFlowBtnsEnable.call(that, props, { tableId: areaCode.tableId });
					});
				}
				getData(props, data.context); //查询数据
			}
		}
	);
}

/**
 * 增加超链接、参照过滤、操作列
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	//在资产编码下添加超链接，链接到卡片页面
	meta[areaCode.tableId].items = meta[areaCode.tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == field.bill_code) {
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							props.pushTo(router.cardRouter, {
								id: record.pk_simulatedep && record.pk_simulatedep.value,
								pagecode: pageCode.cardPageCode
							});
						}}
					>
						{record && record.bill_code && record.bill_code.value}
					</span>
				);
			};
		}
		return item;
	});

	//查询区参照过滤
	addSearchAreaReferFilter(props, meta, queryConfig);

	//增加操作列，删除按钮和查看按钮
	meta[areaCode.tableId].items.push({
		label: getMultiLangByID(tableCol.name),
		visible: true,
		attrcode: tableCol.code,
		itemtype: 'customer',
		className: 'table-opr',
		width: '100px',
		render: (text, record, index) => {
			let buttonAry = [ tableButtonId.delete, tableButtonId.query ];
			return props.button.createOprationButton(buttonAry, {
				area: buttonAreaCode.list_inner, //按钮区域
				buttonLimit: 2, //按钮显示个数
				onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index) //行按钮点击
			});
		}
	});
}

/**
 * 查询页面数据，并为查询区赋默认值(如果有上次查询条件)
 * @param {*} props 
 * @param {*} context 
 */
function getData(props, context) {
	loginContext(context);
}
