import { pageConfig } from '../const';

const { buttonId } = pageConfig;
/**
 * 全选回调事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} status 选取还是选消 ture/false
 * @param {*} length 选择个数
 */
export function onSelectedAll(props, moduleId, status, length) {
	setDeleteButtonEnable(props, moduleId);
}
/**
 * 单选回调事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} record 
 * @param {*} index 
 * @param {*} status 
 */
export function onSelected(props, moduleId, record, index, status) {
	setDeleteButtonEnable(props, moduleId);
}

/**
 * 设置删除按钮是否可用
 * @param {*} params 
 */
function setDeleteButtonEnable(props, moduleId) {
	let tableData = props.cardTable.getCheckedRows(moduleId); //获取选中行数据
	let flag = tableData.length > 0 ? false : true; //如果有选中行，则启用删除按钮
	props.button.setButtonDisabled(buttonId.delete, flag);
}
