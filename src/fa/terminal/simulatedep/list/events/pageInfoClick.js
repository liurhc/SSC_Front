import { commonAjax } from '../../components/commonUtil';
import { pageConfig } from '../const';
// import { setListValue } from '../../../../../ampub/common/utils/listUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;

const { url, pageCode, areaCode } = pageConfig;
/**
 * 分页事件
 * @param {*} props 
 * @param {*} handlePageInfoChange handlePageInfoChange
 * @param {*} pks  所有的pk
 */
export function pageInfoClick(props, handlePageInfoChange, pks) {
	let data = {
		allpks: pks,
		pagecode: pageCode.listPageCode
	};
	commonAjax.call(this, props, url.queryPageUrl, data, (res) => {
		let { success, data } = res;
		if (success) {
			setListValue.call(this, props, data, areaCode.tableId);
		}
	});
}
