import { toCardPage } from '../../components/commonUtil';
/**
 * 双击事件
 * @param {*} item 行数据
 * @param {*} index 行号
 */
export function tableDoubleClick(item, index, props) {
	toCardPage.call(this, props, item);
}
