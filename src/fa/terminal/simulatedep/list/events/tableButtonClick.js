import { pageConfig } from '../const';
import { commonDelete } from '.';
import { toCardPage } from '../../components/commonUtil';

const { tableButtonId, field } = pageConfig;
/**
 * 行按钮点击事件
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		case tableButtonId.delete:
			deleteRow.call(this, props, key, record, index); //调用删除方法
			break;
		case tableButtonId.query:
			toCardPage.call(this, props, record); //调用跳转至卡片方法
			break;
		default:
			break;
	}
}

/**
 * 行删除方法
 * @param {*} props 
 * @param {*} id 按钮ID
 * @param {*} record 行数据
 * @param {*} index 行号
 */
function deleteRow(props, id, record, index) {
	let ids = [ record[field.pkField] && record[field.pkField].value ]; //需要删除的主键数组
	let delIndexs = []; //需要删除的行号
	delIndexs.push(index);
	commonDelete.call(this, props, ids, delIndexs); //调用公共删除方法
}
