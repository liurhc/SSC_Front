import { commonDelete, buttonClick } from './buttonClick';
import initTemplate from './initTemplate';
import { onSelected, onSelectedAll } from './onSelected';
import { pageInfoClick } from './pageInfoClick';
import { searchButtonClick } from './searchButtonClick';
import { tableButtonClick } from './tableButtonClick';
import { tableDoubleClick } from './tableDoubleClick';

export {
	initTemplate,
	commonDelete,
	buttonClick,
	onSelected,
	onSelectedAll,
	pageInfoClick,
	searchButtonClick,
	tableButtonClick,
	tableDoubleClick
};
