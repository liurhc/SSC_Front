import { toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { commonAjax, toAddPage, showModal } from '../../components/commonUtil';
import { searchButtonClick } from '.';
// import { batchRefresh } from '../../../../../ampub/common/utils/listUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { batchRefresh } = listUtils;

const { pageCode, areaCode, url, toastColor, buttonId } = pageConfig;
/**
 * 顶部按钮点击事件
 * @param {*} props 
 * @param {*} id 
 */
export function buttonClick(props, id) {
	switch (id) {
		case buttonId.simulateDep:
			toAddPage.call(this, props); //跳转至新增界面方法
			break;
		case buttonId.delete:
			deleteRows.call(this, props, id); //删除方法
			break;
		case buttonId.Refresh:
			refreh.call(this, props); //刷新方法
			break;
		default:
			break;
	}
}

/**
 * 删除行数据的方法
 * @param {*} props 
 * @param {*} id 按钮id
 */
function deleteRows(props, id) {
	showModal(props, pageCode.listPageCode, id, () => {
		let checkedData = props.table.getCheckedRows(areaCode.tableId); //获取被选中的行数据
		let ids = []; //需要删除的主键数组
		let delIndexs = []; //需要删除的行号
		checkedData.map((item) => {
			ids.push(item.data.values.pk_simulatedep.value); //将被选中行的主键放入数组中
			delIndexs.push(item.index); //将被选中行的行号放入数组中
		});
		commonDelete.call(this, props, ids, delIndexs); //调用公共删除方法
	});
}

/**
 * 删除的公共方法，供表头按钮与行内按钮使用
 * @param {*} props 
 * @param {*} ids 需要删除数据的主键数组
 * @param {*} delIndexs 需要删除数据的行号
 */
export function commonDelete(props, ids = [], delIndexs = []) {
	commonAjax.call(this, props, url.deleteUrl, { ids: ids }, (res) => {
		let { success, data } = res;
		let { deleteCacheId } = props.table;
		if (success) {
			toast({ color: toastColor.success, content: data.delmsg }); //提示删除成功
			props.table.deleteTableRowsByIndex(areaCode.tableId, delIndexs); //删除界面行数据
			deleteCacheId(areaCode.tableId, ids); //删除缓存中的数据
		}
	});
}

/**
 * 刷新界面的方法
 * @param {*} props 
 */
function refreh(props) {
	batchRefresh.call(this, props, searchButtonClick);
}
