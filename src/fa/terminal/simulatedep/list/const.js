import { commonConfig } from '../const';
/**
 * 列表界面常量
 */
export const pageConfig = {
	...commonConfig,
	//后台请求路径常量
	url: {
		queryUrl: '/nccloud/fa/simulatedep/query.do', //查询请求路径
		deleteUrl: '/nccloud/fa/simulatedep/delete.do', //删除请求路径
		queryPageUrl: '/nccloud/fa/simulatedep/querypage.do' //翻页请求路径
	},
	//按钮区域编码常量
	buttonAreaCode: {
		list_head: 'list_head', //头部
		list_inner: 'list_inner' //行内
	},
	//按钮ID常量
	buttonId: {
		simulateDep: 'SimulateDep', //模拟折旧按钮
		delete: 'Delete', //删除按钮
		Refresh: 'Refresh' //刷新按钮
	},
	//行内按钮ID常量
	tableButtonId: {
		delete: 'Delete', //删除按钮
		query: 'Query' //查看按钮
	},
	//操作列
	tableCol: {
		name: 'amcommon-000000' /*国际化处理： 操作*/,
		code: 'opr'
	},
	//区域ID编码常量
	areaCode: {
		tableId: 'bodyvos',
		searchId: '201204024search'
	},
	//参照过滤需要常量
	queryConfig: {
		searchId: '201204024search',
		formId: '',
		bodyIds: [ 'bodyvos' ]
	},
	//校验提示消息常量
	validatorMsg: {
		noDataMsg: '201204024A-000001' /*国际化处理： 未查询出符合条件的数据！*/
	}
};
