import React, { Component } from 'react';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
import {
	initTemplate,
	buttonClick,
	searchButtonClick,
	pageInfoClick,
	onSelectedAll,
	onSelected,
	tableDoubleClick
} from './events';
import { pageConfig } from './const';

// import { setBillFlowBtnsEnable } from '../../../../ampub/common/utils/listUtils';
// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setBillFlowBtnsEnable } = listUtils;

const { NCAffix } = base;
const { buttonAreaCode, areaCode, pageCode, appConfig, dataSource, field } = pageConfig;
/**
 * 模拟折旧列表界面
 */
class MasterChildList extends Component {
	constructor(props) {
		super(props);
		initTemplate.call(this, props); //加载模板
	}
	render() {
		let { table, button, search, ncmodal } = this.props;
		let { createButtonApp } = button; //按钮区
		let { createSimpleTable } = table; //表体
		let { NCCreateSearch } = search; //查询区
		let { createModal } = ncmodal; //模态框
		return (
			<div id="fa-originvalue-list" className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(appConfig.appName)}</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({
								area: buttonAreaCode.list_head,
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this), //按钮点击事件
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(areaCode.searchId, {
						clickSearchBtn: searchButtonClick.bind(this) //查询按钮点击事件
					})}
				</div>
				<div className="table-area">
					{createSimpleTable(areaCode.tableId, {
						handlePageInfoChange: pageInfoClick.bind(this), //分页器操作的回调函数
						showCheck: true, //显示复选框
						showIndex: true, //显示序号
						onRowDoubleClick: tableDoubleClick.bind(this), //行操作事件
						onSelectedAll: onSelectedAll.bind(this), //全选事件
						onSelected: onSelected.bind(this), //选择事件
						dataSource: dataSource,
						pkname: field.pkField,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBillFlowBtnsEnable.call(this, this.props, { tableId: areaCode.tableId });
						}
					})}
				</div>
				{createModal(`${pageCode.listPageCode}-confirm`, {})}
			</div>
		);
	}
}

const List = createPage({})(MasterChildList);

export default List;
