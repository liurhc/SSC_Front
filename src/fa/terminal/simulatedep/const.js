/**
 * 公共常量
 */
export const commonConfig = {
	//应用相关常量
	appConfig: {
		appid: '0001Z910000000004E6P',
		appcode: '201204024A',
		appName: '201204024A-000000' /*国际化处理： 模拟折旧*/
	},
	//路由目标常量
	router: {
		default: '/',
		listRouter: '/list', //列表
		addRouter: '/add', //新增
		cardRouter: '/card' //卡片
	},
	//页面编码常量
	pageCode: {
		listPageCode: '201204024A_list', //列表
		addPageCode: '201204024A_add', //新增
		cardPgaeCode: '201204024A_card' //卡片
	},
	//状态相关常量
	status: {
		edit: 'edit'
	},
	//字段编码常量
	field: {
		pkField: 'pk_simulatedep', //主键
		bill_date: 'bill_date', //单据日期
		pk_org: 'pk_org', //组织
		pk_accbook: 'pk_accbook', //账簿
		bill_code: 'bill_code', //单据号
		pk_category: 'fa_cardhistory.pk_category', //资产类别
		pk_usedept: 'fa_cardhistory.pk_usedept', //使用部门
		pk_mandept: 'fa_cardhistory.pk_mandept', //管理部门
		pk_assetuser: 'fa_card.pk_assetuser', //使用人
		pk_costcenter: 'fa_cardhistory.pk_costcenter', //成本中心
		pk_addreducestyle: 'fa_card.pk_addreducestyle', //增加方式
		pk_usingstatus: 'fa_cardhistory.pk_usingstatus', //使用状况
		pk_currency: 'fa_card.pk_currency', //币种
		accyear: 'accyear', //年度
		period: 'period', //期间
		tb_flag: 'tb_flag', //预算取数
		begindate: 'begindate', //模拟折旧开始日期
		enddate: 'enddate', //模拟折旧截止日期
		depamount1: 'depamount1', //折旧额
		depamount2: 'depamount2',
		depamount3: 'depamount3',
		depamount4: 'depamount4',
		accudep1: 'accudep1', //累计折旧
		accudep2: 'accudep2',
		accudep3: 'accudep3',
		accudep4: 'accudep4',
		depamountdiffer: 'depamountdiffer', //折旧额差额
		accudepdiffer: 'accudepdiffer', //累计折旧差额
		pk_card_card_code: 'pk_card.card_code', //卡片编号，由于不能带.用_代替
		pk_card_asset_code: 'pk_card.asset_code', //资产编码
		pk_card_asset_name: 'pk_card.asset_name', //资产名称
		pk_category_cate_code: 'pk_category.cate_code', //类别编码
		pk_category_cate_name: 'pk_category.cate_name' //类别名称
	},
	//模态框提示内容常量
	modelContent: {
		//常量名大写是为了便于调用
		Delete: {
			title: 'msgUtils-000000' /*国际化处理： 删除*/,
			content: 'msgUtils-000004' /*国际化处理： 确定要删除所选数据吗？*/
		},
		Cancel: {
			title: 'msgUtils-000002' /*国际化处理： 取消*/,
			content: 'msgUtils-000003' /*国际化处理： 确定要取消吗？*/
		},
		CardDelete: {
			title: 'msgUtils-000000' /*国际化处理： 删除*/,
			content: 'msgUtils-000001' /*国际化处理： 确定要删除吗？*/
		}
	},
	//模态框颜色常量
	toastColor: {
		warning: 'warning', //警告，黄色
		success: 'success', //成功，绿色
		error: 'danger', //错误，红色
		info: 'info' //帮助蓝色
	},
	dataSource: 'fa.terminal.simulatedep.main'
};
