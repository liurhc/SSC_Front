//主子表卡片
import React, { Component } from 'react';
import { createPage, base } from 'nc-lightapp-front';
import { initTemplate, buttonClick, handleButtonClick } from './events';
import { pageConfig } from './const';
// import { createCardTitleArea } from '../../../../ampub/common/utils/cardUtils';
// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea } = cardUtils;

const { NCAffix, NCBackBtn } = base;
const { areaCode, buttonAreaCode, appConfig, pageCode, queryStep } = pageConfig;

class MasterChildCard extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.step = queryStep.init; //标识联查步骤
		this.cacheNames = []; //记录所有缓存的名字，刷新是清空缓存
		this.lastCacheName = '';
		this.opr = {}; //操作列数据
		initTemplate.call(this, props);
	}

	render() {
		let { editTable, form, button, ncmodal } = this.props;
		let { createForm } = form;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		let { createEditTable } = editTable;

		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								{createCardTitleArea.call(this, this.props, {
									title: getMultiLangByID(appConfig.appName),
									formId: areaCode.formId,
									backBtnClick: handleButtonClick.bind(this)
								})}
							</div>
							<div className="header-button-area">
								{createButtonApp({
									area: buttonAreaCode.card_head,
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">{createForm(areaCode.formId, {})}</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createEditTable(areaCode.tableId, {
							showIndex: true
						})}
					</div>
					{createModal(`${pageCode.cardPgaeCode}-confirm`, {})}
				</div>
			</div>
		);
	}
}
const Card = createPage({
	billinfo: {
		billtype: 'card',
		pageCode: pageCode.cardPgaeCode,
		headcode: areaCode.formId,
		bodycode: areaCode.tableId
	}
})(MasterChildCard);
export default Card;
