import { commonConfig } from '../const';

export const pageConfig = {
	...commonConfig,
	//后台请求路径常量
	url: {
		queryCardUrl: '/nccloud/fa/simulatedep/cardquery.do', //查询卡片路径
		queryCardDetailUrl: '/nccloud/fa/simulatedep/querycarddetail.do', //联查期间明细路径
		queryPeriodDetailUrl: '/nccloud/fa/simulatedep/queryperioddetail.do', //联查卡片明细路径
		printUrl: '/nccloud/fa/simulatedep/print.do', //打印请求路径
		outputUrl: '/nccloud/fa/simulatedep/output.do', //输出请求路径
		deleteUrl: '/nccloud/fa/simulatedep/delete.do' //删除请求路径
	},
	urlParam: {
		id: 'id'
	},
	//按钮区域编码常量
	buttonAreaCode: {
		card_head: 'card_head', //头部
		card_body_inner: 'card_body_inner' //行内
	},
	//按钮ID常量
	buttonId: {
		simulateDep: 'SimulateDep', //模拟折旧按钮
		delete: 'Delete', //删除按钮
		print: 'Print', //打印按钮
		output: 'Output', //输出按钮
		refresh: 'Refresh' //刷新按钮
	},
	//行内按钮ID常量
	tableButtonId: {
		queryDetail: 'QueryDetail' //联查明细按钮
	},
	// 超链接
	a: {
		asset_code: {
			field: 'pk_card.asset_code',
			fieldName: '201204024A-000002' /*国际化处理：资产编码*/
		}
	},
	//操作列
	tableCol: {
		name: 'amcommon-000000' /*国际化处理：操作*/,
		code: 'opr'
	},
	//区域ID编码常量
	areaCode: {
		tableId: 'bodyvos',
		formId: 'head'
	},
	//校验提示消息常量
	validatorMsg: {
		printError: '201204024A-000017' /*国际化处理：请选择需要打印的数据*/,
		outputError: '201204024A-000018' /*国际化处理：请选择需要输出的数据*/
	},
	other: {
		print: 'print',
		output: 'output',
		printType: {
			default: 'pdf',
			pdf: 'pdf',
			html: 'html'
		},
		fileName: '201204024A-000003' /*国际化处理：模拟折旧打印文件*/ //打印文件名
	},
	queryStep: {
		init: 1, //初始界面
		periodDetail: 2, //期间明细
		cardDetail: 3 //卡片明细
	},
	col: {
		differ: {
			code: 'differ',
			name: '201204024A-000004' /*国际化处理：差额*/
		},
		minus1: {
			code: 'minus1'
		},
		minus2: {
			code: 'minus2'
		},
		minus3: {
			code: 'minus3'
		},
		minus4: {
			code: 'minus4'
		}
	}
};
