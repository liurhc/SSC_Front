import { toast, print, cardCache, output } from 'nc-lightapp-front';
import { initTemplate } from '.';
import { pageConfig } from '../const';
import { showModal, commonAjax, toListPage, toAddPage } from '../../components/commonUtil';

// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const {
	areaCode,
	pageCode,
	buttonId,
	url,
	toastColor,
	validatorMsg,
	field,
	queryStep,
	other,
	urlParam,
	dataSource
} = pageConfig;

/**
 * 按钮操作
 * @param {*} props 
 * @param {*} key
 */
export function buttonClick(props, key) {
	switch (key) {
		case buttonId.simulateDep:
			simulatedep.call(this, props, key);
			break;
		case buttonId.delete:
			deleteCard.call(this, props, key);
			break;
		case buttonId.print:
			prints.call(this, props);
			break;
		case buttonId.output:
			outputs.call(this, props);
			break;
		case buttonId.refresh:
			refresh.call(this, props);
			break;
		default:
			break;
	}
}
/**
 * 模拟折旧按钮点击事件
 * @param {*} props 
 * @param {*} id 按钮ID
 */
function simulatedep(props, id) {
	clearCache.call(this, props);
	toAddPage.call(this, props);
}

/**
 * 删除卡片的方法
 * @param {*} props 
 * @param {*} id 按钮ID
 */
function deleteCard(props, id) {
	let that = this;
	showModal.call(that, props, pageCode.cardPgaeCode, 'CardDelete', () => {
		let id = props.getUrlParam(urlParam.id);
		let ids = []; //需要删除的主键数组
		ids.push(id);
		let data = {
			ids: ids
		};
		commonAjax.call(that, props, url.deleteUrl, data, (res) => {
			let { success, data } = res;
			if (success) {
				toast({ color: toastColor.success, content: data.delmsg });
				cardCache.deleteCacheById(field.pkField, id, dataSource);
				clearCache.call(that, props);
				toListPage.call(that, props);
			}
		});
	});
}

/**
 * 打印方法
 * @param {*} props 
 */
function prints(props) {
	let printData = getPrintData.call(this, props, other.print);
	if (!printData) {
		toast({ content: validatorMsg.printError, color: toastColor.warning });
		return;
	}
	print(
		other.printType.default, // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}
/**
 * 输出方法
 * @param {*} props 
 */
function outputs(props) {
	let printData = getPrintData.call(this, props, other.output);
	if (!printData) {
		toast({ content: validatorMsg.outputError, color: toastColor.warning });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}
/**
 * 刷新方法
 * @param {*} props 
 */
function refresh(props) {
	clearCache.call(this, props); //清空缓存
	initTemplate.call(this, props, () => {
		toast({ color: toastColor.success, content: getMultiLangByID('msgUtils-000020') /*国际化处理： 刷新成功*/ });
	}); //由于数据与模板交互深，需要一起刷新
}

export function clearCache(props) {
	this.step = queryStep.init; //恢复到初始步骤
	//清除所有缓存
	this.cacheNames.map((cacheName) => {
		cardCache.setDefData(cacheName, dataSource, null);
	});
	this.cacheNames = [];
	this.lastCacheName = '';
}
/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 输出类型
 */
function getPrintData(props, outputType = other.print) {
	let pk_simulatedep = props.form.getFormItemsValue(areaCode.formId, field.pkField); //主键
	if (!pk_simulatedep || !pk_simulatedep.value) {
		return false;
	}
	let pks = [];
	pks.push(pk_simulatedep.value);
	let printData = {
		filename: pageCode.cardPagecode, // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType: outputType // 输出类型
	};
	return printData;
}
