import { cardCache } from 'nc-lightapp-front';
import { toListPage } from '../../components/commonUtil';
import { columnVisiable, clearCache, buttonVisible } from '.';
import { pageConfig } from '../const';

const { dataSource, areaCode, queryStep } = pageConfig;
/**
 * 返回按钮点击事件
 */
export function handleButtonClick() {
	if (this.step == queryStep.init) {
		//未联查明细状态
		//清空缓存
		clearCache.call(this, this.props);
		toListPage.call(this, this.props);
	} else if (this.step == queryStep.periodDetail) {
		//区间明细状态
		/*
		第二页返回第一页的时候，读取缓存名字为1的即可，同时需要将lastCacheName置空
		*/
		this.step -= 1;
		this.props.editTable.setTableData(areaCode.tableId, cardCache.getDefData(this.step, dataSource));
		columnVisiable.call(this, this.props, null, this.step);
		this.lastCacheName = '';
	} else {
		//卡片明细状态
		/*
		第三页返回第二页的时候，读取lastCacheName指向的缓存即可
		*/
		this.props.editTable.setTableData(areaCode.tableId, cardCache.getDefData(this.lastCacheName, dataSource));
		this.step -= 1;
		columnVisiable.call(this, this.props, null, this.step);
	}
	buttonVisible.call(this, this.props);
}
