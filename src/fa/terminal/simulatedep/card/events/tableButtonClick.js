import { cardCache } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { commonAjax } from '../../components/commonUtil';
import { columnVisiable, buttonVisible } from '.';

const { areaCode, pageCode, field, url, dataSource, queryStep } = pageConfig;
/**
 * 表体按钮操作
 * @param {} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 点击的行数
 */
export function tableButtonClick(props, key, text, record, index) {
	let requestUrl = '';
	let data = {
		pk: props.form.getFormItemsValue(areaCode.formId, field.pkField).value, //主键
		areacode: areaCode.tableId,
		pagecode: pageCode.cardPgaeCode,
		queryCondition: {
			pk_category: record.values.pk_category.value //资产类别
		}
	};
	if (!record.values.accyear.value) {
		//联查期间
		requestUrl = url.queryPeriodDetailUrl;
	} else {
		//联查卡片
		data.queryCondition.accyear = props.editTable.getValByKeyAndIndex(areaCode.tableId, index, field.accyear).value; //年度
		data.queryCondition.period = props.editTable.getValByKeyAndIndex(areaCode.tableId, index, field.period).value; //期间
		requestUrl = url.queryCardDetailUrl;
	}
	//是否有缓存数据
	/*
	*先通过lastCacheName加当前的页数于点击的函数来组成缓存名字，如果是第一页点第二页，lastCacheName不存在，
	*如果有缓存数据，则需要更新lastCacheName，使其指向第二页，否则会在点击其他行的数据时出现指向错误
	*如果是第二页点第三页，lastCacheName应该是*-*的形式，所以加上页数于行号为第三页的缓存数据
	*/
	let cacheName = '';
	cacheName = `${this.lastCacheName ? this.lastCacheName + '-' : ''}${this.step}-${index}`; //缓存名字，步骤数-行数
	let currentData = cardCache.getDefData(cacheName, dataSource); //获取下一步的缓存
	if (currentData) {
		//有缓存数据，优先读取缓存
		columnVisiable.call(this, props, null, this.step + 1); //设置行显隐
		props.editTable.setTableData(areaCode.tableId, currentData); //设置行数据
		if (!this.lastCacheName) {
			this.lastCacheName = `${this.step}-${index}`;
		}
		this.step += 1; //步骤数加一
		buttonVisible.call(this, props);
	} else {
		//没有缓存 查数据
		commonAjax.call(this, props, requestUrl, data, (res) => {
			let { success, data } = res;
			if (success) {
				if (this.step == queryStep.init) {
					/*
					*第一页跳转至第二页时，把第一页第二页都缓存.
					*先存第一页的数据:缓存名字就是1
					*为了可以正确将数据设置到界面上，先改变行的显隐性，然后再设置行数据
					*再存第二页的数据:缓存的名字是1-第一页点击明细的行号
					*将lastCacheName指向第二页缓存的数据
					*将两次缓存的名字都放到缓存名字数组中，便于后续清空缓存
					*/
					cacheName = this.step;
					cardCache.setDefData(cacheName, dataSource, props.editTable.getAllData(areaCode.tableId));
					this.cacheNames.push(cacheName);
					columnVisiable.call(this, props, null, this.step + 1);
					props.editTable.setTableData(areaCode.tableId, data[areaCode.tableId]);
					cacheName += `-${index}`;
					this.lastCacheName = cacheName;
					cardCache.setDefData(cacheName, dataSource, data[areaCode.tableId]);
					this.cacheNames.push(cacheName);
				} else {
					/*
					*第二页到第三页的时候，lastCacheName指向的一定是第二页的数据，也就是*-*的这种形式，
					*所以以*-*-*-*来存储第三页的数据，同时将缓存的名字放入缓存名字数组中
					*这里lastName不需要更新，因为不会再有下一页了。
					*/
					cacheName = `${this.lastCacheName}-${this.step}-${index}`;
					columnVisiable.call(this, props, null, this.step + 1);
					props.editTable.setTableData(areaCode.tableId, data[areaCode.tableId]);
					cardCache.setDefData(cacheName, dataSource, data[areaCode.tableId]);
					this.cacheNames.push(cacheName);
				}
				this.step += 1; //步骤数加一
				buttonVisible.call(this, props);
			}
		});
	}
}
/**
 * 记一下缓存规则
 * 第一页所有结果都要记住 名字叫1
 * 第二页的缓存名字：1-点击的第一页行数
 * 第三页的缓存名字：1-点击的第一页行数-2-第二页点击的行数
 * 这样就可以确定唯一缓存了
 * lastCacheName指向规则：
 * 如果是在第一页：为空
 * 如果在第二页：指向当前页的缓存数据
 * 如果是第三页：指向上一页的缓存数据，也就是不变
 */
