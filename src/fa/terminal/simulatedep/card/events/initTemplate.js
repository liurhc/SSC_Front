import { cardCache } from 'nc-lightapp-front';
import { tableButtonClick, columnVisiable } from '.';
import { pageConfig } from '../const';
import { commonAjax, toListPage } from '../../components/commonUtil';

// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
// import { openAssetCardByPk } from '../../../../../ampub/common/components/QueryAbout/faQueryAboutUtils';
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { faQueryAboutUtils } = components;
const { openAssetCardByPk } = faQueryAboutUtils;

let {
	pageCode,
	areaCode,
	buttonAreaCode,
	url,
	queryStep,
	dataSource,
	tableCol,
	urlParam,
	tableButtonId,
	a,
	col,
	field
} = pageConfig;
export default function(props, callback) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pageCode.cardPgaeCode //页面id
		},
		function(data) {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					getData.call(that, props, data.template); //在页面已经加载完成的情况下取查数据，通过数据决定显示哪些列
				}
				if (callback && 'function' == typeof callback) {
					callback();
				}
			}
		}
	);
}
/**
 * 查询卡片数据
 * @param {*} props 
 * @param {*} meta 
 */
function getData(props, meta) {
	let addData = cardCache.getDefData('AddToCard', dataSource);
	if (addData) {
		setData.call(this, props, addData, meta);
		cardCache.setDefData('AddToCard', dataSource, null); //清除缓存
	} else {
		let id = props.getUrlParam(urlParam.id);
		if (!id) {
			toListPage(props);
			return;
		}
		let data = {
			pk: id,
			pagecode: pageCode.cardPgaeCode
		};
		commonAjax.call(this, props, url.queryCardUrl, data, (res) => {
			let { success, data } = res;
			if (success) {
				setData.call(this, props, data, meta);
			}
		});
	}
}

/**
 * 设置卡片态的值
 * @param {*} props 
 * @param {*} data 
 */
function setData(props, data, meta) {
	let { body, head } = data;
	head.head.rows[0].status = 'browse';
	let formValues = {
		head: head.head
	};
	props.form.setAllFormValue(formValues); //设置表头的值

	if (body) {
		body.bodyvos.rows[0].status = 'browse';
		props.editTable.setTableData(areaCode.tableId, body.bodyvos); //设置表体的值
	}

	let gridMultiple = setAccbooks.call(this, formValues.head); //设置表体显示账簿名称
	props.meta.setMeta(meta, () => {
		meta = modifierMeta.call(this, props, meta, gridMultiple);
		columnVisiable(props, gridMultiple, queryStep.init); //设置表体列显隐性
	});
}
/**
 * 设置表体账簿
 */
function setAccbooks(accbooks) {
	let names = backColName.call(this, accbooks);
	let gridMultiple = {
		// gridMultiple 是个对象 {}
		bodyvos: [
			// 区域Id为键，值为数组[] ，代表此区域Id有多组多表头关系
			{
				// 每组多表头关系细则
				name: names.name1, // name 为多表头的显示名称
				code: col.minus1.code, // code 为多表头的键名字（唯一标识，自己取，不和任何键重复即可）
				children: [ field.depamount1, field.accudep1 ] // childen 为数组，值为 visible为true的item的键，最好键与键是按顺序连续排列的
			},
			{
				name: names.name2,
				code: col.minus2.code,
				children: [ field.depamount2, field.accudep2 ]
			},
			{
				name: names.name3,
				code: col.minus3.code,
				children: [ field.depamount3, field.accudep3 ]
			},
			{
				name: names.name4,
				code: col.minus4.code,
				children: [ field.depamount4, field.accudep4 ]
			},
			{
				name: names.name5,
				code: col.differ.code,
				children: [ field.depamountdiffer, field.accudepdiffer ]
			}
		]
	};
	return gridMultiple;
}
/**
 * 设置账簿折旧列名称 形式：序号.账簿名称(模拟折旧方案名称)
 * @param {}} params 
 */
function backColName(accbooks) {
	let name = {}; //返回的名称 序号.账簿名称(模拟折旧设置名称)形式
	let isCompare = []; //差异比较的两个
	for (let index = 1; index < 5; index++) {
		let accbook = `pk_accbook${index}`;
		let pk_program = `pk_program${index}.program_name`;
		let iscompare = `iscompare${index}`;
		let tempName = `name${index}`;
		let tempNameValue = '';
		if (accbooks.rows[0].values[accbook].display) {
			tempNameValue = accbooks.rows[0].values[pk_program]
				? `${index}.${accbooks.rows[0].values[accbook].display}(${accbooks.rows[0].values[pk_program].display})`
				: `${index}.${accbooks.rows[0].values[accbook].display}`;
		}
		name[tempName] = tempNameValue;
		if (accbooks.rows[0].values[iscompare].value == true) {
			isCompare.push(index);
		}
	}
	name.name5 = getMultiLangByID(col.differ.name);
	if (isCompare.length == 2) {
		name.name5 += `(${isCompare[0]}-${isCompare[1]})`;
	}
	return name;
}
/**
 * 添加操作列
 * @param {*} props 
 * @param {*} meta 
 * @param {*} gridMultiple 
 */
function modifierMeta(props, meta, gridMultiple) {
	//增加操作列
	this.opr = {
		label: getMultiLangByID(tableCol.name),
		visible: true,
		attrcode: tableCol.code,
		itemtype: 'customer',
		className: 'table-opr',
		width: '100px',
		render: (text, record, index) => {
			let buttonAry = [ tableButtonId.queryDetail ];
			return props.button.createOprationButton(buttonAry, {
				area: buttonAreaCode.card_body_inner,
				buttonLimit: 1,
				onButtonClick: (props, key) => tableButtonClick.call(this, props, key, text, record, index)
			});
		}
	};
	meta[areaCode.tableId].items.push(this.opr);
	//添加超链接
	meta[areaCode.tableId].items.map((item) => {
		if (item.attrcode == a.asset_code.field) {
			//给资产编码列添加超链接
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div
						class="simple-table-td"
						field={a.asset_code.field}
						fieldname={getMultiLangByID(a.asset_code.fieldName)}
					>
						<span
							className="code-detail-link"
							onClick={() => {
								openAssetCardByPk(props, record.values.pk_card.value); //联查固定资产卡片
							}}
						>
							{record.values[a.asset_code.field] && record.values[a.asset_code.field].display}
						</span>
					</div>
				);
			};
		}
	});
	meta = props.meta.multipleMeta(meta);
	meta = props.meta.handleMultiple(meta, gridMultiple);
	return meta;
}
