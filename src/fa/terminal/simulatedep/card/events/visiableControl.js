import { pageConfig } from '../const';

let { areaCode, queryStep, field, buttonId, buttonAreaCode, tableCol, tableButtonId } = pageConfig;

/**
 * 列显示控制
 * @param {*} props 
 * @param {*} accbooks 
 * @param {*} code 
 */
export function columnVisiable(props, accbooks, code) {
	let hidecols = [];
	let showcols = [];
	switch (code) {
		case queryStep.init:
			if (accbooks) {
				accbooks.bodyvos.map((accbook) => {
					if (!accbook.name || accbook.name == 'null') {
						hidecols.push(accbook.code);
					}
				});
			}
			hidecols.push(field.accyear, field.period); //隐藏年度、期间
			break;
		case queryStep.periodDetail:
			hidecols.push(field.pk_card_card_code, field.pk_card_asset_code, field.pk_card_asset_name); //隐藏卡片编号、资产编码、资产名称
			showcols.push(field.accyear, field.period, field.pk_category_cate_code, field.pk_category_cate_name); //显示年度、期间、类别编码、类别名称、操作列
			oprVisible.call(this, props, true);
			break;
		case queryStep.cardDetail:
			hidecols.push(field.pk_category_cate_code, field.pk_category_cate_name); //隐藏类别编码、类别名称、操作列
			showcols.push(field.pk_card_card_code, field.pk_card_asset_code, field.pk_card_asset_name); //显示卡片编号、资产编码、资产名称
			oprVisible.call(this, props, false);
			break;
		default:
			break;
	}
	props.editTable.hideColByKey(areaCode.tableId, hidecols);
	props.editTable.showColByKey(areaCode.tableId, showcols);
}
/**
 * 控制按钮显隐性
 */
export function buttonVisible(props) {
	let isShowRefresh = false;
	if (this.step == queryStep.init) {
		isShowRefresh = true;
	}
	props.button.setButtonVisible(buttonId.refresh, isShowRefresh);
}
/**
 * 显示或隐藏操作列
 * @param {*} props 
 * @param {*} visible true:显示 false:隐藏
 */
function oprVisible(props, visible) {
	let meta = props.meta.getMeta();
	let hasOpr = false;
	let oprIndex;
	meta[areaCode.tableId].items.map((item, index) => {
		if (item && item.attrcode && item.attrcode == 'opr') {
			hasOpr = true;
			oprIndex = index;
		}
	});
	if (visible && !hasOpr) {
		meta[areaCode.tableId].items.push(this.opr);
	} else if (!visible && hasOpr) {
		meta[areaCode.tableId].items.splice(oprIndex, 1);
	}
}
