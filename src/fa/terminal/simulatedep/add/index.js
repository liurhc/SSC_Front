import React, { Component } from 'react';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
import { initTemplate, buttonClick, searchButtionClick, simulatedepAreaAfterEvent } from './events';
import './index.less';
import { pageConfig } from './const';
const { areaCode, pageCode, areaName, appConfig, buttonAreaCode } = pageConfig;

// import closeBrowserUtils from '../../../../ampub/common/utils/closeBrowserUtils';
// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { NCRow, NCCol, NCAffix } = base;
/**
 * 页面入口文件
 */
class MasterChildAdd extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		closeBrowserUtils(props, {
			form: [ areaCode.nameArea, areaCode.simulatedepArea ],
			editTable: [ areaCode.accbookArea ]
		});
		initTemplate.call(this, props);
	}

	render() {
		let { button, search, form, editTable, ncmodal } = this.props;
		let { createButtonApp } = button;
		let { createEditTable } = editTable;
		let { NCCreateSearch } = search;
		const { createForm } = form;
		let { createModal } = ncmodal;
		return (
			<div className="nc-bill-list" id="simulation-dep">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(appConfig.appName)}</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({
								area: buttonAreaCode.add_head,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-card-area dep-info">
					<NCRow>
						<NCCol md={1} style={{ width: '150px' }}>
							<p>{getMultiLangByID(areaName.nameArea)}</p>
						</NCCol>
						<NCCol md={10}>{createForm(areaCode.nameArea, {})}</NCCol>
					</NCRow>
					<NCRow>
						<NCCol md={1} style={{ width: '150px' }}>
							<p>{getMultiLangByID(areaName.simulatedepArea)}</p>
						</NCCol>
						<NCCol md={11}>
							{createForm(areaCode.simulatedepArea, {
								onAfterEvent: simulatedepAreaAfterEvent.bind(this)
							})}
						</NCCol>
					</NCRow>
					<NCRow>
						<NCCol md={1}>
							<p>{getMultiLangByID(areaName.accbookArea)}</p>
						</NCCol>
						<NCCol md={11}>
							{createEditTable(areaCode.accbookArea, {
								showIndex: true //显示行号
							})}
						</NCCol>
					</NCRow>
					<NCRow>
						<NCCol md={1}>
							<p>{getMultiLangByID(areaName.searchArea)}</p>
						</NCCol>
						<NCCol md={11}>
							{NCCreateSearch(areaCode.searchArea, {
								showSearchBtn: false,
								clickSearchBtn: searchButtionClick.bind(this),
								searchBtnName: getMultiLangByID('201204024A-000016')
							})}
						</NCCol>
					</NCRow>
				</div>
				{createModal(`${pageCode.addPageCode}-confirm`, {})}
			</div>
		);
	}
}

const Add = createPage({})(MasterChildAdd);
export default Add;
