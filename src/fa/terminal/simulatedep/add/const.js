import { commonConfig } from '../const';

/**
 * 新增界面常量
 */

export const pageConfig = {
	...commonConfig,
	//后台请求路径常量
	url: {
		simulatedepUrl: '/nccloud/fa/simulatedep/simulatedep.do' //模拟折旧新增后台请求路径
	},
	//按钮区域编码常量
	buttonAreaCode: {
		add_head: 'add_head' //表头
	},
	//按钮ID常量
	buttonId: {
		save: 'Save', //删除按钮
		cancel: 'Cancel' //取消按钮
	},
	//区域ID编码常量
	areaCode: {
		accbookArea: 'accbookArea', //账簿范围
		simulatedepArea: 'simulatedepArea', //折旧范围
		searchArea: 'searchArea', //查询区
		nameArea: 'nameArea', //名称
		cardHeadCode: 'head'
	},
	//区域名称常量
	areaName: {
		accbookArea: '201204024A-000005' /*国际化处理：账簿范围*/,
		simulatedepArea: '201204024A-000006' /*国际化处理：折旧范围*/,
		searchArea: '201204024A-000007' /*国际化处理：查询区*/,
		nameArea: '201204024A-000008' /*国际化处理：模拟折旧名称*/
	},
	//参照过滤需要常量
	queryConfig: {
		searchId: 'searchArea', //查询区ID
		formId: 'simulatedepArea', //主表区域ID
		bodyIds: [ 'accbookArea' ] //子表区域ID
	},
	//校验提示消息常量
	validatorMsg: {
		content: '201204024A-000009' /*国际化处理：数据输入有误！*/,
		pk_orgError: '201204024A-000010' /*国际化处理：模拟折旧组织必须设置！*/,
		beginDateError: '201204024A-000011' /*国际化处理：模拟开始日期必须设置！*/,
		endDateError: '201204024A-000012' /*国际化处理：模拟结束日期必须设置！*/,
		DateCompareError: '201204024A-000013' /*国际化处理：模拟结束日期必须大于模拟开始日期！*/,
		accbookError: '201204024A-000014' /*国际化处理：模拟折旧账簿必须设置！*/,
		depprogramError: '201204024A-000015' /*国际化处理：模拟折旧只能选择两种方案进行对比！*/
	}
};
