import { pageConfig } from '../const';
// import {loginContext,getContext,loginContextKeys} from '../../../../../ampub/common/components/AMInitInfo/loginContext';
// import { addHeadAreaReferFilter, addBodyReferFilter } from '../../../../common/components/ReferFilter';
import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { loginContext, getContext, loginContextKeys } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addHeadAreaReferFilter, addBodyReferFilter } = ReferFilter;

const { areaCode, pageCode, queryConfig, field } = pageConfig;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pageCode.addPageCode //页面id
		},
		function(data) {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = filterOther(props, meta);
					addSearchAreaReferFilter(props, meta);
					addHeadAreaReferFilter(props, meta, queryConfig); //模拟折旧范围参照过滤
					addBodyReferFilter(props, meta, queryConfig, null, 'editTable'); //账簿范围参照过滤
					props.meta.setMeta(meta, () => {
						props.editTable.setStatus(areaCode.accbookArea, 'edit');
						props.form.setFormStatus(areaCode.nameArea, meta.nameArea.areastatus);
						props.form.setFormStatus(areaCode.simulatedepArea, meta.simulatedepArea.areastatus);
						props.editTable.addRow(areaCode.accbookArea);
						props.editTable.addRow(areaCode.accbookArea);
						props.editTable.addRow(areaCode.accbookArea);
						props.editTable.addRow(areaCode.accbookArea);
					});
				}
				if (data.context) {
					loginContext(data.context);
					props.form.setFormItemsValue(areaCode.simulatedepArea, {
						[field.pk_org]: {
							value: getContext(loginContextKeys.pk_org),
							display: getContext(loginContextKeys.org_Name)
						}
					});
				}
			}
		}
	);
}

function filterOther(props, meta) {
	meta.nameArea.items.map((item) => {
		item.col = 6;
	});
	return meta;
}
/**
 * 查询区参照过滤
 * @param {*} props 
 * @param {*} meta 
 */
function addSearchAreaReferFilter(props, meta) {
	meta[areaCode.searchArea].items.map((item) => {
		let itemCode = item.attrcode;
		// 除了币种 其他参照控件 显示[显示停用数据]选项
		if (item && item.itemtype == 'refer' && itemCode != field.pk_currency) {
			item.isShowDisabledData = true;
		}
		switch (itemCode) {
			case field.pk_category: //资产类别
			case field.pk_usedept: //使用部门
			case field.pk_mandept: //管理部门
				item.onlyLeafCanSelect = false;
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			case field.pk_assetuser: //使用人
			case field.pk_costcenter: //成本中心
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(areaCode.simulatedepArea, field.pk_org); //模拟折旧范围的财务组织
					if (pk_org && pk_org.value) {
						return { pk_org: pk_org.value };
					}
				};
				break;
			case field.pk_addreducestyle:
			case field.pk_usingstatus:
				item.onlyLeafCanSelect = false;
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
				break;
			default:
				break;
		}
	});
}
