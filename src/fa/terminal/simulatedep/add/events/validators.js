import { pageConfig } from '../const';
const { areaCode, validatorMsg, field } = pageConfig;
/**
 * 模拟折旧新增校验
 * @param {*} props 
 */
export function saveValidator(props) {
	let validateRes = new Object();
	validateRes.valid = false; //校验是否通过,false是不通过，true是通过
	validateRes.msg = []; //校验信息
	validateRes.other = {}; //其他要传递回的内容
	let formData = props.form.getAllFormValue([ areaCode.simulatedepArea, areaCode.nameArea ]); //模拟折旧范围数据
	//校验模拟折旧组织是否存在
	if (!hasFormItemValue(formData, field.pk_org)) {
		validateRes.msg.push(validatorMsg.pk_orgError);
	}
	//校验模拟折旧开始日期是否存在
	if (!hasFormItemValue(formData, field.begindate)) {
		validateRes.msg.push(validatorMsg.beginDateError);
	}
	//校验模拟折旧截止日期是否存在
	if (!hasFormItemValue(formData, field.enddate)) {
		validateRes.msg.push(validatorMsg.endDateError);
	}
	//校验模拟折旧开始日期是否早于模拟折旧截止日期
	if (hasFormItemValue(formData, field.begindate) && hasFormItemValue(formData, field.enddate)) {
		//已经判断了value是否存在，所以下一步getFormItemValue不会返回false，不需要加判断
		if (getFormItemValue(formData, field.begindate) > getFormItemValue(formData, field.enddate)) {
			validateRes.msg.push(validatorMsg.DateCompareError);
		}
	}
	//校验账簿范围
	let tableData = props.editTable.getAllData(areaCode.accbookArea); //获取账簿范围的数据
	let flag = false; //是否至少选取一个模拟折旧账簿标识
	let indexs = []; //是否差异计算集合，用于计数;
	tableData.rows.map((item) => {
		let rowFlag = false; //本行是否选取模拟折旧账簿标识
		if (item.values.pk_accbook.value == '') {
			rowFlag = false; //本行没有选取
		} else {
			rowFlag = true; //本行选取
		}
		flag = flag || rowFlag; //只要有一行选取，flag为ture，否则为false
		if (item.values.iscompare.value) {
			indexs.push('1'); //本行是否选择差异计算
		}
	});
	//校验是否选择模拟折旧账簿
	if (!flag) {
		validateRes.msg.push(validatorMsg.accbookError);
	}
	//校验差异计算集合(只有选取两个或者一个都不选取才可通过)
	if (indexs.length != 2 && indexs.length != 0) {
		validateRes.msg.push(validatorMsg.depprogramError);
	}

	//所有校验都通过
	if (validateRes.msg.length <= 0) {
		validateRes.valid = true;
	}
	//返回校验信息
	return validateRes;
}
/**
 * 判断表单数据是否存在
 * @param {*} formData 
 * @param {*} fieldName 
 */
function hasFormItemValue(formData, fieldName) {
	let flag = false;
	//防止报undefined
	if (
		formData &&
		formData.simulatedepArea &&
		formData.simulatedepArea.rows[0] &&
		formData.simulatedepArea.rows[0].values
	) {
		let fieldValue = formData.simulatedepArea.rows[0].values[fieldName];
		if (fieldValue && fieldValue.value) {
			flag = true;
		}
	}
	return flag;
}

/**
 * 获取表单数据，如果数据不存在返回false
 * @param {*} formData 
 * @param {*} fieldName 
 */
function getFormItemValue(formData, fieldName) {
	let flag = hasFormItemValue(formData, fieldName); //判断该数据是否存在
	if (flag) {
		let value = formData.simulatedepArea.rows[0].values[fieldName].value; //因为已经校验过数据是否存在，所以可以放心取数据
		return value;
	} else {
		return flag;
	}
}
