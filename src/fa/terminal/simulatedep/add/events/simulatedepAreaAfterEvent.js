import { pageConfig } from '../const';

const { areaCode, field } = pageConfig;
/**
 * 折旧范围编辑后事件
 * @param {*} props 
 * @param {*} moduleId 组件id
 * @param {*} key 修改的字段key
 * @param {*} value 修改后的value
 * @param {*} oldValue 修改前的value
 */
export default function(props, moduleId, key, value, oldValue) {
	if (key == 'tb_flag') {
		//预算取数:如果勾选，账簿范围改为一行，否则为四行
		if (value.value) {
			props.editTable.deleteTableRowsByIndex(areaCode.accbookArea, [ 0, 1, 2, 3 ], true); //删除所有行
			props.editTable.addRow(areaCode.accbookArea); //新增一行
		} else {
			props.editTable.deleteTableRowsByIndex(areaCode.accbookArea, [ 0 ], true); //删除所有行
			//新增四行
			for (let index = 0; index < 4; index++) {
				props.editTable.addRow(areaCode.accbookArea);
			}
		}
	}
	if (key == 'pk_org') {
		//获取财务组织的值，联动事件，清空账簿范围，重置预算取数
		//清空账簿范围
		let tb_flag = props.form.getFormItemsValue(areaCode.simulatedepArea, field.tb_flag);
		if (tb_flag && tb_flag.value) {
			props.editTable.deleteTableRowsByIndex(areaCode.accbookArea, [ 0 ], true); //删除所有行
		} else {
			props.editTable.deleteTableRowsByIndex(areaCode.accbookArea, [ 0, 1, 2, 3 ], true); //删除所有行
		}
		//新增四行
		for (let index = 0; index < 4; index++) {
			props.editTable.addRow(areaCode.accbookArea);
		}
		//预算取数设置为空
		props.form.setFormItemsValue(areaCode.simulatedepArea, {
			[field.tb_flag]: { value: false, display: false }
		});
	}
}
