import { buttonClick } from '.';
import { pageConfig } from '../const';

const { buttonId } = pageConfig;
/**
 * 查询按钮点击事件，作用与确定按钮一样
 * @param {*} props 
 * @param {*} data 
 * @param {*} type 
 * @param {*} queryInfo 
 */
export function searchButtionClick(props, data, type, queryInfo) {
	buttonClick(props, buttonId.save);
}
