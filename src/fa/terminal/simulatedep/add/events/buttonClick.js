import { cardCache } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { saveValidator } from '.';
import { showModal, toListPage, commonAjax, toCardPage, groupToast } from '../../components/commonUtil';

// import { getContext, loginContextKeys } from '../../../../../ampub/common/components/AMInitInfo/loginContext';
import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

const { pageCode, url, areaCode, buttonId, field, dataSource, validatorMsg } = pageConfig;

export function buttonClick(props, id) {
	switch (id) {
		case buttonId.cancel:
			cancel.call(this, props, id); //取消事件
			break;
		case buttonId.save:
			save.call(this, props, id);
			break;
		default:
			break;
	}
}

/**
 * 取消方法
 * @param {*} props 
 * @param {*} id 按钮id
 */
function cancel(props, id) {
	showModal.call(this, props, pageCode.addPageCode, id, () => {
		toListPage.call(this, props);
	});
}

/**
 * 保存方法
 * @param {*} props 
 * @param {*} id 
 */
function save(props, id) {
	let valid = saveValidator(props); //保存前校验
	if (!valid.valid) {
		groupToast(validatorMsg.content, valid.msg);
		return;
	}
	let tableData = props.editTable.getAllData(areaCode.accbookArea); //获取账簿范围数据
	let accbookConditionVOs = []; //账簿范围VO
	tableData.rows.map((item) => {
		let data = {
			pk_accbook: (() => {
				if (item.values.pk_accbook.value == '') {
					return null;
				} else {
					return item.values.pk_accbook.value;
				}
			})(),
			pk_depprogram: (() => {
				if (item.values.pk_depprogram.value == '') {
					return null;
				} else {
					return item.values.pk_depprogram.value;
				}
			})(),
			iscompare: item.values.iscompare.value
		};
		accbookConditionVOs.push(data);
	});
	let dtoData = {}; //返回后台的值
	let simdepcondition = {}; //模拟折旧条件
	dtoData.querytree = props.search.getQueryInfo(areaCode.searchArea, false); //查询区条件
	dtoData.querytree.pagecode = pageCode.cardPgaeCode; //页面编码

	let formData = props.form.getAllFormValue([ areaCode.simulatedepArea, areaCode.nameArea ]); //获取名字与折旧范围
	simdepcondition.pk_org = formData.simulatedepArea.rows[0].values.pk_org.value; //财务组织
	simdepcondition.begindate = formData.simulatedepArea.rows[0].values.begindate.value; //开始日期
	simdepcondition.enddate = formData.simulatedepArea.rows[0].values.enddate.value; //截至日期
	simdepcondition.accbookConditionVOs = accbookConditionVOs; //账簿范围
	simdepcondition.tb_flag = formData.simulatedepArea.rows[0].values.tb_flag.value
		? formData.simulatedepArea.rows[0].values.tb_flag.value
		: false; //预算取数
	simdepcondition.simdepname_text = {
		text: formData.nameArea.rows[0].values.simulatedep_name.value,
		text2: null,
		text3: null,
		text4: null,
		text5: null,
		text6: null
	}; //模拟折旧名称
	simdepcondition.bill_date = getContext(loginContextKeys.businessDate); //单据日期
	dtoData.simdepcondition = simdepcondition;
	commonAjax.call(this, props, url.simulatedepUrl, dtoData, (res) => {
		let { success, data } = res;
		if (success) {
			cardCache.setDefData('AddToCard', dataSource, data); //缓存保存之后的数据，避免跳到卡片态的时候又重新查一遍
			let record = data.head.head.rows[0].values; //新增的数据去除数据外层包装之后的具体数据
			cardCache.addCache(record[field.pkField].value, res.data, areaCode.cardHeadCode, dataSource); //列表态缓存插入
			toCardPage.call(this, props, record); //进入卡片页面
		}
	});
}
