const pageConfig = {
	// 小应用主键
	appid: '0001Z910000000004E6M',
	// 小应用编码
	appcode: '201204012A',
	// 页面编码
	pagecode: '201204012A_list',
	//查询区
	searchArea: 'searchArea',
	//模板区域
	gridArea: 'gridArea',

	// 折旧与摊销的小应用编码
	linkappcode: '201204004A',
	// 折旧与摊销的小应用页面编码
	linkpagecode: '201204004A_list',
	// 联查凭证的交易类型
	billtype: 'HH',

	// 区分是联查凭证还是生成凭证
	linkVoucher: 'Link',
	makeVoucher: 'Make',

	// 打印模板所在NC节点及标识
	printFuncode: '2012060010',
	// 打印名称
	printFilename: '201204012A-000016' /** 集中计提折旧打印 */,
	// nodeKey
	nodeKey: 'depssc',

	// 请求链接
	url: {
		depQueryVoucherUrl: '/nccloud/fa/depreciationssc/queryAboutvoucher.do',
		depPrintUrl: '/nccloud/fa/depreciationssc/depsscPrint.do',
		depMultidep: '/nccloud/fa/depreciationssc/multidep.do',
		depQueryDate: '/nccloud/fa/depreciationssc/queryDate.do',
		depReadssc: '/nccloud/fa/depreciationssc/readsscdep.do',
		queryAccbookAndPeriodUrl: '/nccloud/fa/fapub/queryAccbookAndPeriod.do', //查询账簿与会计期间url
		gatherMsgDeal: '/nccloud/fa/depreciationssc/gatherMsgDeal.do' // 循环请求最后一次处理汇总金额
	}
};

export { pageConfig };
