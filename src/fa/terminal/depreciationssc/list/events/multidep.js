import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { searchArea, appcode, pagecode, gridArea, url } = pageConfig;
const separate = 5; //默认每次计提5条数据
export default function multidep() {
	let checkdata = this.props.editTable.getCheckedRows(gridArea);
	if (checkdata == null || checkdata.length == 0) {
		toast({ content: getMultiLangByID('201204012A-000019'), color: 'warning' }); /** '请选择需要计提的账簿！' */
		return;
	}
	let colnum = null;
	for (let val of checkdata) {
		val.data.status = '1';
		colnum = colnum + val.index + ',';
	}

	//计算一共需要几次请求
	let timer = Math.ceil(checkdata.length / separate);
	//把请求回来的汇总数据清空，防止数据累加
	this.totalResData = [];
	//进入等待滚动条界面
	this.setState({ pageStatus: 'waiting', progress: 0 }, () => {
		this.props.modal.show('waiting-modal', {}, () => {
			onceAjax.call(this, checkdata, timer, 0);
		});
	});
}

/**
 * 组装数据，循环发送请求，每次取5条数据
 * @param {*} checkdata :已选中的总数据
 * @param {*} timer : 根据已选中的总数据计算出来的一共需要几次请求
 * @param {*} index : 递归调用，当前发送的是第几次请求 ，从第0次开始
 */
function onceAjax(checkdata = [], timer, index = 0) {
	if (checkdata.length == 0) {
		toast({ content: getMultiLangByID('201204012A-000019'), color: 'warning' }); /** '请选择需要计提的账簿！' */
		return;
	}
	//当所有请求发送完毕时候，进行后续数据处理
	if (index == timer) {
		//把循环请求回来的数据再次发送到后台进行精度处理
		let pkAccbooks = []; //获取到所选中的行数据的pk_accbook，后台根据这个字段进行精度处理
		checkdata.forEach((element) => {
			pkAccbooks.push(element.data.values.pk_accbook.value || '');
		});
		//进行精度处理的请求数据格式组装
		let summariesData = {
			totalResData: [ ...this.totalResData ],
			pkAccbooks
		};
		ajax({
			url: url.gatherMsgDeal,
			loading: false, //去掉请求的等待界面
			data: summariesData,
			success: (res) => {
				this.props.modal.close('waiting-modal');
				let selectrows = this.props.editTable.getCheckedRows(gridArea);
				for (let key in this.mapdata) {
					if (key === 'remove') break;
					let pk_accbook = this.mapdata[key].values.pk_accbook.value;
					let result = this.mapdata[key].values.remark.value;
					let depAmountAll = this.mapdata[key].values.depAmountAll.value;
					for (let selectrow in selectrows) {
						let pk_accbookslc = selectrows[selectrow].data.values.pk_accbook.value;
						if (pk_accbook === pk_accbookslc) {
							let rowflag = selectrows[selectrow].data.rowid;
							this.props.editTable.setValByKeyAndRowId(gridArea, rowflag, 'remark', { value: result });
							this.props.editTable.setValByKeyAndRowId(gridArea, rowflag, 'depAmountAll', {
								value: depAmountAll
							});
							break;
						}
					}
				}
				this.setState({
					pageStatus: 'result',
					successNum: res.data.successnum,
					failNum: res.data.failnum,
					summsg: res.data.summsg
				});
				this.props.editTable.selectAllRows(this.tableId, false);
				// 计提折旧后，设置行未勾选，同时设置凭证相关按钮不可用
				this.props.button.setButtonDisabled('MakeVoucher', true);
				this.props.button.setButtonDisabled('FABillQueryAboutVoucher', true);
			}
		});
		return;
	} else {
		//取得本次循环的请求的请求行数据，然后进行组装
		let checkdatas = [];
		for (let j = index * separate; j < Math.min(checkdata.length, (index + 1) * separate); j++) {
			checkdatas.push(checkdata[j].data);
		}
		// 组装后台传递数据信息
		let data = {
			pageid: pagecode,
			model: {
				areaType: 'table',
				areacode: gridArea,
				rows: checkdatas
			}
		};
		ajax({
			url: url.depMultidep,
			loading: false,
			data,
			success: (res) => {
				//把每次请求回来的数据暂存
				this.totalResData.push({
					successNum: res.data.successNum || 0,
					failnum: res.data.failNum || 0,
					totalNum: res.data.totalNum || 0,
					summsg: { ...res.data.summsg } || {}
				});
				this.mapdata = [ ...this.mapdata, ...res.data.data.gridArea.rows ];
				//更新进度，使滚动条发生变化
				this.setState(
					{
						progress: ((index + 1) / timer).toFixed(2)
					},
					() => {
						//递归调用请求
						onceAjax.call(this, checkdata, timer, index + 1);
					}
				);
			}
		});
	}
}

/**
 * 这是本来的代码，暂时没有删除。以便比较
 */
function pre() {
	// 获得当前勾选行信息,并重新组装
	let checkdatas = new Array();
	for (var i = 0; i < checkdata.length; i++) {
		checkdatas[i] = checkdata[i].data;
	}

	// 组装后台传递数据信息
	let data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: gridArea,
			rows: checkdatas
		}
	};
	let _this = this;
	ajax({
		url: url.depMultidep,
		data,
		success: function(res) {
			if (res.data) {
				let successnum = res.data.successNum;
				let failnum = res.data.failNum;
				let totalNum = res.data.totalNum;
				let summsg = res.data.summsg;
				let mapdata = res.data.data.gridArea.rows;

				let selectrows = _this.props.editTable.getChangedRows(gridArea);

				for (let key in mapdata) {
					if (key === 'remove') break;
					let pk_accbook = mapdata[key].values.pk_accbook.value;
					let result = mapdata[key].values.remark.value;
					let depAmountAll = mapdata[key].values.depAmountAll.value;
					for (let selectrow in selectrows) {
						let pk_accbookslc = selectrows[selectrow].values.pk_accbook.value;
						if (pk_accbook === pk_accbookslc) {
							let rowflag = selectrows[selectrow].rowid;
							_this.props.editTable.setValByKeyAndRowId(gridArea, rowflag, 'remark', { value: result });
							_this.props.editTable.setValByKeyAndRowId(gridArea, rowflag, 'depAmountAll', {
								value: depAmountAll
							});
							break;
						}
					}
				}
				_this.setState({ pageStatus: 'result', successNum: successnum, failNum: failnum, summsg: summsg });
				_this.props.editTable.selectAllRows(_this.tableId, false);
				// 计提折旧后，设置行未勾选，同时设置凭证相关按钮不可用
				_this.props.button.setButtonDisabled('MakeVoucher', true);
				_this.props.button.setButtonDisabled('FABillQueryAboutVoucher', true);
			}
		}
	});
}
