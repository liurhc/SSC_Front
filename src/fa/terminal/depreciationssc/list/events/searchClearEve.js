import { pageConfig } from '../const';
const { searchArea } = pageConfig;
/**
 * 数据清空时，设置所有参照及期间不可编辑
 */
export default function searchClearEve() {
    this.props.search.setSearchValByField(searchArea, 'pk_org', { value: null, display: null });
    this.props.search.setSearchValByField(searchArea, 'pk_accbook', { value: null, display: null });
    this.props.search.setSearchValByField(searchArea, 'pk_accperiodscheme', { value: null, display: null });
    this.props.search.setSearchValByField(searchArea, 'accyear', { value: '', display: '' });
    this.props.search.setSearchValByField(searchArea, 'period', { value: '', display: '' });
    this.props.search.setDisabledByField(searchArea, 'pk_org', true);
    this.props.search.setDisabledByField(searchArea, 'pk_accbook', true);
    this.props.search.setDisabledByField(searchArea, 'pk_accperiodscheme', true);
    this.props.search.setDisabledByField(searchArea, 'accyear', true);
    this.props.search.setDisabledByField(searchArea, 'period', true);
}