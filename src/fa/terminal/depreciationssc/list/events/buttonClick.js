/**
 * 右肩  按钮组   点击事件
 */
import { ajax, toast, print, output } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import multidep from './multidep';
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { queryVocherUtils } = components;
const { queryAboutVoucher } = queryVocherUtils;
import fa from 'fa';
const { fa_components } = fa;
const { VoucherUtils } = fa_components;
const { index } = VoucherUtils;

const {
	appcode,
	gridArea,
	searchArea,
	pagecode,
	billtype,
	linkVoucher,
	makeVoucher,
	printFuncode,
	printFilename,
	nodeKey,
	url
} = pageConfig;
export default function buttonClick(props, id) {
	let btnArr = [ 'Dep', 'MakeVoucher', 'FABillQueryAboutVoucher', 'Print', 'OutPut' ];
	switch (id) {
		case 'Dep': //重新计提
			multidep.call(this);
			break;
		case 'MakeVoucher': // 生成凭证
			depQueryVoucher.call(this, this.props, makeVoucher);
			break;
		case 'FABillQueryAboutVoucher': //联查凭证
			depQueryVoucher.call(this, this.props, linkVoucher);
			break;
		case 'Print': //打印
			printTemp.call(this, this.props);
			break;
		case 'OutPut': //输出
			outputTemp.call(this, this.props);
			break;
		default:
			break;
	}
}

/**
 * 联查凭证或者生成凭证方法
   * @param {*} props 
   * @param {*} data
 */
export function depQueryVoucher(props, opr) {
	let checkdata = this.props.editTable.getCheckedRows(gridArea);
	if (checkdata.length < 1) {
		toast({ content: getMultiLangByID('201204012A-000017'), color: 'warning' }); /* '请选择需要联查的数据！'*/
		return;
	}
	let colnum = null;
	for (let val of checkdata) {
		val.data.status = '1';
		colnum = colnum + val.index + ',';
	}
	// 获得当前勾选行信息,并重新组装
	let checkdatas = new Array();
	for (var i = 0; i < checkdata.length; i++) {
		checkdatas[i] = checkdata[i].data;
	}
	// 组装后台传递数据信息
	let data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: gridArea,
			rows: checkdatas
		}
	};
	let _this = this;
	ajax({
		url: url.depQueryVoucherUrl,
		data,
		success: function(res) {
			if (res.data && res.data.error) {
				toast({ content: res.data.error, color: 'warning' });
				return;
			} else {
				if (opr === makeVoucher) {
					index.call(_this, appcode, _this.props, res.data.pks, billtype, res.data.pk_group, res.data);
				} else if (opr === linkVoucher) {
					queryAboutVoucher.call(
						_this,
						_this.props,
						gridArea,
						'',
						appcode,
						false,
						false,
						true,
						res.data.pks,
						billtype,
						res.data.pk_group,
						res.data
					);
				}
			}
		}
	});
}

/**
 * 打印
   * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintParam.call(this, props, 'print');
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.depPrintUrl, // 后台打印服务url
		printData
	);
}

/**
  * 输出
  * @param {*} props 
  */
export function outputTemp(props) {
	let printData = getPrintParam.call(this, props, 'output');
	output({
		url: url.depPrintUrl,
		data: printData
	});
}

/**
* 打印输出参数
* @param {*} props 
* @param {*} output 
*/
function getPrintParam(props, outputType) {
	let checkdata = this.props.editTable.getAllRows(gridArea);
	// let colnum = null;
	// for(let val of checkdata){
	// 	val.data.status = '1';
	// 	colnum = colnum+val.index+',';
	// }
	// // 获得当前勾选行信息,并重新组装
	// let checkdatas = new Array();
	// for(var i=0;i<checkdata.length;i++){
	// 	checkdatas[i] = checkdata[i].data;
	// }
	// 组装后台传递数据信息
	let data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: gridArea,
			rows: checkdata
		}
	};
	let printData = JSON.stringify(data);
	let param = {
		filename: getMultiLangByID(printFilename), // 文件名称
		nodekey: nodeKey, // 模板节点标识
		oids: [ printData ] //将过滤条件通过主键字段进行传递+将查询条件以userjson字段进行传递
	};
	if (outputType) {
		param.outputType = outputType;
	}
	return param;
}
