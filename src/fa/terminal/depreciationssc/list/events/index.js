import initTemplate from './initTemplate';
import searchBtnClick from './serchButtonClick';
import multidep from './multidep';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import onAfterEvent from './onAfterEvent';
import onSelectedAllFn from './onSelectedAllFn';
import onSelectedFn from './onSelectedFn';
import searchClearEve from './searchClearEve';

export { initTemplate, searchBtnClick, multidep, pageInfoClick, buttonClick, onAfterEvent,onSelectedAllFn, onSelectedFn, searchClearEve};