import { ajax, cacheTools } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { searchArea, appcode, pagecode, gridArea, url } = pageConfig;
//点击查询，获取查询区数据
export default function clickSearchBtn(props) {
	let searchId = searchArea;
	let tableId = gridArea;

	let data = null;
	let orgSlct_flag = null;
	let isallorg_flag = props.search.getSearchValByField(searchId, 'isallorg_flag');
	let accyear = props.search.getSearchValByField(searchId, 'accyear').value.firstvalue;
	let period = props.search.getSearchValByField(searchId, 'period').value.firstvalue;
	let isallorg_flagval = isallorg_flag.value.firstvalue;
	if (isallorg_flagval === '') {
		//默认打开时，查询条件为空
		props.search.setSearchValByField(searchArea, 'isallorg_flag', {
			value: 'all',
			display: getMultiLangByID('201204012A-000020') /** '全部组织' */
		});
		props.search.setDisabledByField(searchArea, 'pk_org', true);
		props.search.setDisabledByField(searchArea, 'pk_accbook', true);
		//props.search.setDisabledByField(searchArea, 'pk_accperiodscheme', true);
		props.search.setDisabledByField(searchArea, 'accyear', true);
		props.search.setDisabledByField(searchArea, 'period', true);
		isallorg_flag = 'Y';
		orgSlct_flag = 'N';
		data = {
			isallorg_flag: isallorg_flag,
			orgSlct_flag: orgSlct_flag,
			accyear: accyear,
			period: period
		};
	} else if (isallorg_flagval === 'option') {
		let pk_org = props.search.getSearchValByField(searchId, 'pk_org');
		let orgVal = pk_org.value.firstvalue;
		let pk_accbook = props.search.getSearchValByField(searchId, 'pk_accbook');
		let bookVal = pk_accbook.value.firstvalue;
		isallorg_flag = 'N';
		orgSlct_flag = 'Y';
		data = {
			isallorg_flag: isallorg_flag,
			orgSlct_flag: orgSlct_flag,
			pk_orgs: [ orgVal ],
			pk_accbooks: [ bookVal ],
			accyear: accyear,
			period: period
		};
	} else {
		isallorg_flag = 'N';
		orgSlct_flag = 'Y';
		data = {
			isallorg_flag: isallorg_flag,
			orgSlct_flag: orgSlct_flag,
			accyear: accyear,
			period: period
		};
	}
	let newthis = this;
	ajax({
		url: url.depReadssc,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					let colNum = data[tableId].rows.length;
					props.editTable.setTableData(tableId, data[tableId]);
					props.editTable.selectAllRows(tableId, true);
					newthis.setState({ totalNum: colNum });
					newthis.setState({ pageStatus: 'can' });
					if (colNum != '0') {
						// 如果有选中行，则设置按钮可用
						props.button.setButtonDisabled('MakeVoucher', false);
						props.button.setButtonDisabled('FABillQueryAboutVoucher', false);
					} else {
						// 如果没有选中行，则设置按钮不可用
						props.button.setButtonDisabled('MakeVoucher', true);
						props.button.setButtonDisabled('FABillQueryAboutVoucher', true);
					}
				} else {
					// 如果没有选中行，则设置按钮不可用
					props.button.setButtonDisabled('MakeVoucher', true);
					props.button.setButtonDisabled('FABillQueryAboutVoucher', true);
					props.editTable.setTableData(tableId, { rows: [] });
					newthis.setState({ totalNum: 0 });
				}
			}
		}
	});
}
