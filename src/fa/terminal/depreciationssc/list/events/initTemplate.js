import { pageConfig } from '../const';
import serchButtonClick from './serchButtonClick';
const { pagecode, appcode, searchArea, gridArea, linkappcode, linkpagecode } = pageConfig;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;

					meta = contentfilter(props, meta);
					props.meta.setMeta(meta, () => {
						let searchdata = [ { isallorg_flag: 'all' } ];
						props.search.setSearchValue(searchArea, searchdata);
						serchButtonClick.call(this, props);
					});
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	);
}

function contentfilter(props, meta) {
	meta.searchArea.items.map((item) => {
		if (item['attrcode'] === 'pk_accbook') {
			item.disabled = true;
		}
		if (item['attrcode'] === 'pk_org') {
			item.disabled = true;
		}
	});
	let defaultConfig = {
		searchId: searchArea,
		fieldMap: {
			pk_org: 'pk_org', //财务组织
			pk_accbook: 'pk_accbook'
		}
	};
	addSearchAreaReferFilter(props, meta, defaultConfig);

	// 金额字段添加超链接
	meta[gridArea].items.map((item) => {
		if (item.attrcode == 'depAmountAll') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td" field="depAmountAll" fieldname={getMultiLangByID('201204012A-000018')}>
						{' '}
						{/** "月折旧额" */}
						<span
							className="code-detail-link"
							onClick={() => {
								let param = {
									pk_org: record.values.pk_org.value,
									pk_org_name: record.values.pk_org.display,
									pk_accbook: record.values.pk_accbook.value,
									pk_accbook_name: record.values.pk_accbook.display,
									accyear: record.values.accyear.value,
									period: record.values.period.value
								};
								openDepDetailByParam(props, param);
							}}
						>
							{record.values['depAmountAll'] && record.values['depAmountAll'].value}
						</span>
					</div>
				);
			};
		}
		return item;
	});

	return meta;
}

/**
 * 联查折旧与摊销明细页面，传递查询条件
 * @param {*} props 
 */
export const openDepDetailByParam = (props, param) => {
	props.openTo('/fa/terminal/depreciation/list/index.html', {
		...param,
		pagecode: linkpagecode,
		appcode: linkappcode
	});
};
