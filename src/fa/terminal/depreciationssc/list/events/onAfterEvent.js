/**
 * 查询区  编辑后事件
 */
import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { searchArea, url } = pageConfig;
import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

export default function onAfterEvent(field, val) {
	let searchData = this.props.search.getAllSearchData(searchArea);
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}
	if (JSON.stringify(data) === '{}') {
		return;
	}
	switch (field) {
		case 'isallorg_flag':
			let isallorg_flag = this.props.search.getSearchValByField(searchArea, 'isallorg_flag');
			if (isallorg_flag.value.firstvalue === 'option') {
				this.props.search.setDisabledByField(searchArea, 'pk_org', false);
				this.props.search.setDisabledByField(searchArea, 'pk_accbook', true);
				this.props.search.setDisabledByField(searchArea, 'pk_accperiodscheme', true);
				this.props.search.setDisabledByField(searchArea, 'accyear', true);
				this.props.search.setDisabledByField(searchArea, 'period', true);
				this.props.search.setSearchValByField(searchArea, 'pk_accperiodscheme', { value: null, display: null });
				this.props.search.setSearchValByField(searchArea, 'accyear', { value: '', display: '' });
				this.props.search.setSearchValByField(searchArea, 'period', { value: '', display: '' });
			} else {
				// 如果为全部组织或者清空 需要清除当前查询条件中的数据
				clearKeyAfterEditEvent.call(this, this.props, searchArea, field);
			}
			break;
		case 'pk_org':
			let pk_org = this.props.search.getSearchValByField(searchArea, 'pk_org');
			if (!pk_org || pk_org.value.firstvalue === '') {
				clearKeyAfterEditEvent.call(this, this.props, searchArea, field);
				return;
			} else {
				this.props.search.setDisabledByField(searchArea, 'pk_accbook', false);
				let pk_orgs = pk_org.value.firstvalue.split(',');
				if (pk_orgs.length > 1) {
					this.props.search.setDisabledByField(searchArea, 'pk_accperiodscheme', true);
				} else {
					let dataquery = {
						businessDate: getContext(loginContextKeys.businessDate),
						pk_org: pk_org.value.firstvalue
					};
					let _this = this;
					// 如果组织单选，自动带出账簿信息，如果账簿非空，则设置会计期间方案可编辑
					ajax({
						url: url.queryAccbookAndPeriodUrl,
						data: dataquery,
						success: (res) => {
							if (res.data) {
								let accbook_display = res.data.accbook_display;
								let accbook_value = res.data.accbook_value;
								if (accbook_value && accbook_value != '') {
									_this.props.search.setDisabledByField(searchArea, 'pk_accperiodscheme', false);
									_this.props.search.setSearchValByField(searchArea, 'pk_accbook', {
										value: accbook_value,
										display: accbook_display
									});
								} else {
									_this.props.search.setDisabledByField(searchArea, 'pk_accperiodscheme', true);
								}
							}
						}
					});
				}
			}
			break;
		case 'pk_accbook':
			let pk_accbook = this.props.search.getSearchValByField(searchArea, 'pk_accbook');
			if (!pk_accbook || pk_accbook.value.firstvalue === '') {
				clearKeyAfterEditEvent.call(this, this.props, searchArea, field);
				return;
			} else {
				this.props.search.setDisabledByField(searchArea, 'pk_accperiodscheme', false);
			}
			break;
		case 'pk_accperiodscheme':
			let pk_accperiodscheme = this.props.search.getSearchValByField(searchArea, 'pk_accperiodscheme');
			if (!pk_accperiodscheme || pk_accperiodscheme.value.firstvalue === '') {
				clearKeyAfterEditEvent.call(this, this.props, searchArea, field);
				return;
			} else {
				this.props.search.setDisabledByField(searchArea, 'accyear', false);
				this.props.search.setDisabledByField(searchArea, 'period', false);
			}
			let _this = this;
			ajax({
				url: url.depQueryDate,
				data,
				success: (res) => {
					if (res.data) {
						// 将查询结果设置到this中
						_this.yearPeriod = res.data;
						if (res.data.year) {
							// 获取所有当前会计期间方案的年份
							let selectData = res.data.year.map((year) => {
								return {
									display: year,
									value: year
								};
							});
							let accyear = res.data.year[0];
							_this.setState({ yearPeriod: res.data.period });
							// 获取所有当前会计期间方案的期间
							let periodData = res.data.period[accyear].map((period) => {
								return {
									display: period,
									value: period
								};
							});
							let period = res.data.period[accyear][0];

							// 设置当前期间方案的备选值
							let meta = _this.props.meta.getMeta();
							meta.searchArea.items.forEach((item) => {
								if (item.attrcode === 'accyear') {
									item.options = selectData;
								}
								if (item.attrcode === 'period') {
									item.options = periodData;
								}
							});
							_this.props.meta.setMeta(meta);
							_this.props.search.setSearchValByField(searchArea, 'accyear', {
								value: accyear,
								display: accyear
							});
							_this.props.search.setSearchValByField(searchArea, 'period', {
								value: period,
								display: period
							});
						}
					} else {
						// 集中计提折旧-会计期间方案为空时直接反回空值避免出现异常提示
						// 设置当前期间方案的备选值
						let meta = _this.props.meta.getMeta();
						meta.searchArea.items.forEach((item) => {
							if (item.attrcode === 'accyear') {
								item.options = [ '' ];
							}
							if (item.attrcode === 'period') {
								item.options = [ '' ];
							}
						});
						_this.props.meta.setMeta(meta);
						_this.props.search.setSearchValByField(searchArea, 'accyear', {
							value: '',
							display: ''
						});
						_this.props.search.setSearchValByField(searchArea, 'period', {
							value: '',
							display: ''
						});
					}
				}
			});
			break;
		case 'accyear':
			if (val === '__clear__') {
				//年度清空的状态
				let meta = this.props.meta.getMeta();
				meta.searchArea.items = meta.searchArea.items.map((item) => {
					if (item.attrcode === 'period') {
						return Object.assign({}, item, { options: [] });
					}
					return item;
				});
				this.props.meta.setMeta(meta);
				this.props.search.setSearchValByField(searchArea, 'accyear', {
					value: '',
					display: ''
				});
				this.props.search.setSearchValByField(searchArea, 'period', {
					value: '',
					display: ''
				});
			} else {
				//切换年度
				let accyear = this.props.search.getSearchValByField(searchArea, 'accyear');
				if (accyear.value.firstvalue === '') {
					this.props.search.setSearchValByField(searchArea, 'period', { value: '', display: '' });
					this.props.search.setDisabledByField(searchArea, 'period', true);
				} else {
					//获取当前年度所有期间信息
					let periodData = this.state.yearPeriod[accyear.value.firstvalue].map((period) => {
						return {
							display: period,
							value: period
						};
					});
					let period = this.state.yearPeriod[accyear.value.firstvalue][0];
					this.props.search.setSearchValByField(searchArea, 'period', { value: period, display: period });
					this.props.search.setDisabledByField(searchArea, 'period', false);
					// 设置当前期间方案的备选值
					let meta = this.props.meta.getMeta();
					meta.searchArea.items.forEach((item) => {
						if (item.attrcode === 'period') {
							item.options = periodData;
						}
					});
				}
			}
			break;
		case 'period':
			if (val === '__clear__') {
				this.props.search.setSearchValByField(searchArea, 'period', {
					value: '',
					display: ''
				});
				this.setState({ pageStatus: 'undepreciation' });
			}
			break;
		default:
			break;
	}
}

function clearKeyAfterEditEvent(props, moduleId, field) {
	switch (field) {
		case 'isallorg_flag':
			// 如果组织全选或者清空，清空账簿，同时清空会计年、期间及会计期间方案
			props.search.setSearchValByField(moduleId, 'pk_org', { value: null, display: null });
			props.search.setSearchValByField(moduleId, 'pk_accbook', { value: null, display: null });
			props.search.setSearchValByField(moduleId, 'pk_accperiodscheme', { value: null, display: null });
			props.search.setSearchValByField(moduleId, 'accyear', { value: '', display: '' });
			props.search.setSearchValByField(moduleId, 'period', { value: '', display: '' });
			props.search.setDisabledByField(moduleId, 'pk_org', true);
			props.search.setDisabledByField(moduleId, 'pk_accbook', true);
			props.search.setDisabledByField(moduleId, 'pk_accperiodscheme', false);
			props.search.setDisabledByField(moduleId, 'accyear', true);
			props.search.setDisabledByField(moduleId, 'period', true);
			break;
		case 'pk_org':
			// 如果组织为空，清空账簿，同时清空会计年、期间及会计期间方案
			props.search.setSearchValByField(moduleId, 'pk_accbook', { value: null, display: null });
			props.search.setSearchValByField(moduleId, 'pk_accperiodscheme', { value: null, display: null });
			props.search.setSearchValByField(moduleId, 'accyear', { value: '', display: '' });
			props.search.setSearchValByField(moduleId, 'period', { value: '', display: '' });
			props.search.setDisabledByField(moduleId, 'pk_accbook', true);
			props.search.setDisabledByField(moduleId, 'pk_accperiodscheme', true);
			props.search.setDisabledByField(moduleId, 'accyear', true);
			props.search.setDisabledByField(moduleId, 'period', true);
			break;
		case 'pk_accbook':
			// 清空账簿，清空会计年、期间及凭证日期
			props.search.setSearchValByField(moduleId, 'pk_accperiodscheme', { value: null, display: null });
			props.search.setSearchValByField(moduleId, 'accyear', { value: '', display: '' });
			props.search.setSearchValByField(moduleId, 'period', { value: '', display: '' });
			props.search.setDisabledByField(moduleId, 'pk_accperiodscheme', true);
			props.search.setDisabledByField(moduleId, 'accyear', true);
			props.search.setDisabledByField(moduleId, 'period', true);
			break;
		case 'pk_accperiodscheme':
			// 清空账簿，清空会计年、期间及凭证日期
			props.search.setSearchValByField(moduleId, 'pk_accperiodscheme', { value: null, display: null });
			props.search.setSearchValByField(moduleId, 'accyear', { value: '', display: '' });
			props.search.setSearchValByField(moduleId, 'period', { value: '', display: '' });
			props.search.setDisabledByField(moduleId, 'pk_accperiodscheme', false);
			props.search.setDisabledByField(moduleId, 'accyear', true);
			props.search.setDisabledByField(moduleId, 'period', true);
			break;
	}
}
