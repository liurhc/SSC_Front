import { pageConfig } from '../const';
const { gridArea } = pageConfig;
/**
 * 表格区域选择所有行数据回调函数
 */
export default function onSelectedAllFn() {
    let checkdata = this.props.editTable.getCheckedRows(gridArea);
    let length = checkdata.length;
    if (length != '0') {
        // 如果没有选中行，则设置按钮不可用
        this.props.button.setButtonDisabled('MakeVoucher', false);
        this.props.button.setButtonDisabled('FABillQueryAboutVoucher', false);
   } else {
       // 如果没有选中行，则设置按钮不可用
       this.props.button.setButtonDisabled('MakeVoucher', true);
       this.props.button.setButtonDisabled('FABillQueryAboutVoucher', true);
   }
    // 重置选中的行数
    this.setState({totalNum: length, pageStatus:'can'});
}