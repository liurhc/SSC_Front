import { ajax } from 'nc-lightapp-front';
const tableId = 'bodyvos';
import { pageConfig } from '../const';
const { gridArea, pagecode } = pageConfig;
export default function(props, config, pks) {
	let pageInfo = props.table.getTablePageInfo(gridArea);
	let data = {
		pk_accbooks: pks,
		pageid: pagecode,
		pageSize: pageInfo.pageSize
	};
	//得到数据渲染到页面
	let that = this;
	ajax({
		url: '/nccloud/fa/depreciationssc/changepage.do',
		data: data,
		success: function(res) {
			let { success, data } = res;
			if (success) {
				if (data) {
					that.props.table.setAllTableData(tableId, data);
				} else {
					that.props.table.setAllTableData(tableId, { rows: [] });
				}
			}
		}
	});
}
