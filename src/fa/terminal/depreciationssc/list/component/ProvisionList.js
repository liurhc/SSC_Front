import React, { Component } from 'react';
import { base, ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { NCTabs } = base;
const { NCButton } = base;
const NCTabPane = NCTabs.NCTabPane;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export default class ProvisionList extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="provision">
				<div className="provisionlist-txt">
					<p>
						<span>本月折旧总额 30,000.00</span>
					</p>
					<p>
						<span>计提原值 2,211,453,896.00</span>
						<span>累计折旧 512,130,895.87</span>
						<span>净值 1,699,323,000.13</span>
						<span>计提折旧卡片数 1,111,868</span>
					</p>
				</div>
				<NCTabs navtype="turn" contenttype="moveleft" defaultActiveKey="list" onChange={(key) => {}}>
					<NCTabPane tab={getMultiLangByID('201204012A-000021')} key="list">
						{' '}
						{/** 折旧清单 */}
						折旧清单table
					</NCTabPane>
					<NCTabPane tab={getMultiLangByID('201204012A-000022')} key="allot">
						{' '}
						{/** 折旧分配汇总 */}
						折旧分配汇总table
					</NCTabPane>
				</NCTabs>
			</div>
		);
	}
}
