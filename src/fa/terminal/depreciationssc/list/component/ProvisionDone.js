import React, { Component } from 'react';
import { base, ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { NCButton } = base;
export default class ProvisionDone extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="provision">
				<div className="provisiondone-txt">
					<p>
						<span>2016年5月</span>
						<span>折旧总额</span>
						<span>760000</span>
					</p>
					<p>
						<span>
							参与计提的公司{`4`}个，未计提{` 1 `}个
						</span>
					</p>
					<p>
						<span>计提原值 2,211,453,896.00</span>
						<span>累计折旧 512,130,895.87</span>
						<span>净值 1,699,323,000.13</span>
						<span>计提折旧卡片数 1,111,868</span>
					</p>
				</div>
			</div>
		);
	}
}
