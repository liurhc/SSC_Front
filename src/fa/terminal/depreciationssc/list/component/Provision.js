import React, { Component } from 'react';
import { base, ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { NCButton } = base;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export default class Provision extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="provision">
				<div className="provision-txt">
					<h4>{getMultiLangByID('201204012A-000000')}</h4> {/*已经为您选择了所有待计提折旧的公司，请点击*/}
					<NCButton colors="primary" disabled="false">
						{getMultiLangByID('201204012A-000024')} {/*计提折旧*/}
					</NCButton>
				</div>
			</div>
		);
	}
}
