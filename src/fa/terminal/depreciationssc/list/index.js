import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
const { NCTabs, NCButton, NCDropdown, NCMenu, NCAffix, NCSelect, NCIcon } = base;
import {
	initTemplate,
	searchBtnClick,
	multidep,
	pageInfoClick,
	buttonClick,
	onAfterEvent,
	onSelectedAllFn,
	onSelectedFn,
	searchClearEve
} from './events';
import { Provision, ProvisionDone, ProvisionList } from './component';
import { pageConfig } from './const';
import './index.less';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;
const { searchArea, appcode, pagecode, gridArea } = pageConfig;

class Depreciationssc extends Component {
	constructor(props) {
		super(props);
		this.moduleId = '2012';
		this.tableId = gridArea;
		this.state = {
			pageStatus: 'provisionlist', //页面状态
			successNum: 0,
			failNum: 0,
			totalNum: 0,
			summsg: {},
			yearPeriod: {},
			progress: 0 //计提折旧进度，默认0%
		};
		this.multidep = multidep.bind(this);
		initTemplate.call(this, props);
		//暂存请求回来的数据
		this.totalResData = [];
		this.mapdata = [];
	}

	//等待界面
	waitingContent = () => {
		let { progress } = this.state;
		return (
			<div className="waiting-page">
				<div className="content-box">
					<div className="box-top" />
					<div className="box-center">{getMultiLangByID('201204012A-000025')}</div>
					{/*正在计提折旧中，请耐心等待... */}
					<div className="box-bottom">
						<p className="progressBar">
							<p className="done" style={{ width: parseInt(progress * 100) + '%' }} />
						</p>
						<p className="percent">{parseInt(progress * 100) + '%'}</p>
					</div>
				</div>
			</div>
		);
	};

	render() {
		let { editTable, button, search, modal } = this.props;
		let { createEditTable } = editTable;
		let { createModal } = modal;
		let { pageStatus, progress } = this.state;
		let { NCCreateSearch, successNum, failNum, totalNum, summsg } = search;
		let { createButtonApp } = button;
		return (
			<div className="nc-bill-list deprenciationssc">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID('201204012A-000004')}</h2>{' '}
							{/*集中计提折旧*/}
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'list_head',
								buttonLimit: 4,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area dep-search">
					{NCCreateSearch(searchArea, {
						clickSearchBtn: searchBtnClick.bind(this),
						showAdvBtn: false,
						onAfterEvent: onAfterEvent.bind(this),
						advSearchClearEve: searchClearEve.bind(this) // 查询条件清空处理
					})}
				</div>
				<div className="nc-bill-table-area contnet-area">
					{pageStatus === 'can' && (
						<div className="can-dep">
							<NCIcon type="uf-exc-c" />
							<p>
								{getMultiLangByID('201204012A-000003')}
								<span>
									({this.state.totalNum}
									{getMultiLangByID('201204012A-000007')})
								</span>{' '}
								{getMultiLangByID('201204012A-000001')} {/**已经为你选择了可以计提折旧的公司 个 ，请计提！*/}
							</p>
							<NCButton
								colors="primary"
								onClick={this.multidep}
								disabled={this.state.totalNum === 0 ? true : false}
							>
								{getMultiLangByID('201204012A-000024')} {/*计提折旧*/}
							</NCButton>
						</div>
					)}
					{pageStatus === 'result' && (
						<div className="result-dep">
							<div>
								{/* <NCIcon type="uf-correct" /> */}
								<p>
									<span className="total">{getMultiLangByID('201204012A-000002')}</span> {/* 计提折旧： */}
									<span className="total-num">{this.state.summsg.depamount}</span>
									<span className="title-name">{getMultiLangByID('201204012A-000006')}</span>{' '}
									{/* 参与计提的公司： */}
									<span className="title-num">
										{this.state.totalNum}
										{getMultiLangByID('201204012A-000007')}
									</span>{' '}
									{/* 个 */}
									<span className="title-name">{getMultiLangByID('201204012A-000023')}</span>{' '}
									{/* 成功： */}
									<span className="title-num">
										{this.state.successNum}
										{getMultiLangByID('201204012A-000007')}
									</span>{' '}
									{/* 个 */}
									<span className="title-name">{getMultiLangByID('201204012A-000008')}</span>{' '}
									{/* 失败： */}
									<span className="title-num">
										{this.state.failNum}
										{getMultiLangByID('201204012A-000007')}
									</span>{' '}
									{/* 个 */}
								</p>
								<p>
									{getMultiLangByID('201204012A-000009')}
									<span>
										{' '}
										{/*计提原值：*/}
										{this.state.summsg.localoriginvalue ? (
											this.state.summsg.localoriginvalue
										) : (
											''
										)}{' '}
									</span>
									{getMultiLangByID('201204012A-000010')}
									<span>{this.state.summsg.accudep ? this.state.summsg.accudep : ''}</span>{' '}
									{/*累计折旧：*/}
									{getMultiLangByID('201204012A-000011')}
									<span>{this.state.summsg.netvalue ? this.state.summsg.netvalue : ''}</span> {' '}
									{/*净值：*/}
									{getMultiLangByID('201204012A-000012')}
									<span>{this.state.summsg.card_num ? this.state.summsg.card_num : ''}</span>{' '}
									{/*计提折旧卡片数：*/}
								</p>
							</div>
						</div>
					)}
					{pageStatus == 'waiting' &&
						createModal('waiting-modal', {
							noFooter: true,
							content: this.waitingContent(),
							className: 'junior'
						})}
				</div>
				<div className="nc-singleTable-table-area">
					{createEditTable(this.tableId, {
						handlePageInfoChange: pageInfoClick.bind(this),
						showCheck: true,
						showIndex: true,
						onSelected: onSelectedFn.bind(this), // 左侧选择列单个选择框回调
						onSelectedAll: onSelectedAllFn.bind(this) // 左侧选择列全选选择框回调
					})}
				</div>
			</div>
		);
	}
}

Depreciationssc = createPage({})(Depreciationssc);

// 加载多语文件
initMultiLangByModule({ fa: [ '201204012A', 'facommon' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Depreciationssc />, document.querySelector('#app'));
});
