import { pageConfig } from '../const';

//总账那边使用的是script标签加载代码，导致引用的ampub公共打包代码，无法在当前页面被引入，需要使用import
import { loginContext } from '../../../../../ampub/common/components/AMInitInfo/loginContext';

const { areaCode, appConfig } = pageConfig;
export default function(props) {
	let that = this;
	props.createUIDom(
		{
			appcode: appConfig.appcode,
			pagecode: appConfig.pagecode //页面id
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(that, props, meta);
					props.meta.setMeta(meta);
				}
				if (data.context) {
					loginContext(data.context);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	let that = this;
	meta[areaCode.formId].items = meta[areaCode.formId].items.map((item, key) => {
		let itemcode = item.attrcode;
		// 为参照添加多选
		if (item.itemtype == 'refer') {
			item.isMultiSelectedEnabled = true;
		}
		if (itemcode == 'pk_mandepts' || itemcode == 'pk_usedepts' || itemcode == 'pk_jobmngfils') {
			// 判断是否是集团节点，是否要多组织过滤
			if (!that.isShowUnit) {
				// 单选添加过滤条件，获取总账提供的pk_org参数
				item.queryCondition = () => {
					let pk_org = that.getPk_org();
					if (pk_org) {
						return { pk_org: pk_org };
					}
				};
			} else {
				financeOrgMultiRefFilter.call(that, item);
			}
		}
		return item;
	});
	let multiLang = props.MutiInit.getIntl('2052');
	meta[areaCode.formId].status = props.status; //界面状态
	meta[areaCode.formId].multiLang = multiLang;
	return meta;
}

/**
 * 多组织过滤方法
 * @param {*} item 
 */
function financeOrgMultiRefFilter(item) {
	item.isShowUnit = true;
	item.unitProps = {
		multiLang: {
			domainName: 'uapbd',
			// currentLocale: 'simpchn',
			moduleId: 'refer_uapbd'
		},
		refType: 'tree',
		refName: 'refer-000201' /* 国际化处理： 业务单元*/,
		placeholder: 'refer-000201' /* 国际化处理： 业务单元*/,
		refcode: 'uapbd/refer/org/FinanceOrgTreeRef/index',
		queryTreeUrl: '/nccloud/uapbd/org/FinanceOrgTreeRef.do',
		isMultiSelectedEnabled: 0,
		isShowDisabledData: !0,
		key: 'pk_org'
	};
	item.unitCondition = () => {
		let retData = {};
		retData['TreeRefActionExt'] = 'nccloud.web.ampub.common.refCodition.FinanceOrgSqlBuilder';
		return retData;
	};
}
