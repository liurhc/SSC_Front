export const pageConfig = {
	appConfig: {
		appid: '0001Z91000000000177M', //小应用id
		appcode: '201204028A', //小应用编码
		moduleId: '2012', //所属模块编码
		pagecode: '201204028A_card', //单据模板pageID
		oid: '1001Z91000000000C69T' // 查询模板id
	},
	areaCode: {
		formId: 'bodyvos'
	},
	collateCodition: [
		'moneyType',
		'pk_categorys',
		'pk_mandepts',
		'pk_usedepts',
		'pk_jobmngfils',
		'pk_addreducestyles',
		'pk_usingstatus'
	],
	dataSource: 'fa.terminal.accbalance.main'
};
