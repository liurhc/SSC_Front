import React, { Component } from 'react';
import { initTemplate } from './events';
import { createPage, cardCache } from 'nc-lightapp-front';
import { pageConfig } from './const';
//总账那边使用的是script标签加载代码，导致引用的ampub公共打包代码，无法在当前页面被引入，需要使用import
import { getContext, loginContextKeys } from '../../../../ampub/common/components/AMInitInfo/loginContext';
const { areaCode, dataSource, collateCodition } = pageConfig;

/**
 * 与总账对账设置
 */
class AccBalance extends Component {
	constructor(props) {
		super(props);
		this.state = {
			busireconData: {},
			status: null
		};
		this.isShowUnit = props.getUrlParam('nodetype') == 'group' ? true : false; //是否是集团节点
		initTemplate.call(this, this.props);
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.status) {
			//设置状态
			let status = nextProps.status;
			if (status != this.state.status) {
				this.setState(
					{
						status: status
					},
					() => {
						this.props.form.setFormStatus(areaCode.formId, status);
					}
				);
			}
		}
	}

	componentDidMount() {
		this.props.onRef(this);
		if (this.props.status) {
			//设置状态
			let status = this.props.status;
			if (status != this.state.status) {
				this.props.form.setFormStatus(areaCode.formId, status);
				this.setState(
					{
						status: status
					},
					() => {
						//渲染数据，如果有缓存，说明是新增转浏览，直接设置值即可，否则即为直接转浏览态，接收传回来的数据
						let formData = cardCache.getDefData('formData', dataSource);
						this.props.form.setFormStatus(this.state.status);
						if (formData) {
							cardCache.setDefData('formData', dataSource, null);
							this.props.form.setAllFormValue({ [areaCode.formId]: formData });
						} else if (this.props.busireconData) {
							let busireconData = this.props.busireconData.value;
							if (busireconData && busireconData != this.state.busireconData) {
								let data = 'object' == typeof busireconData ? busireconData : JSON.parse(busireconData);
								this.setState(
									{
										busireconData: busireconData
									},
									() => {
										this.initialData(this.props, data);
									}
								);
							}
						}
					}
				);
			}
		}
	}

	/* 传入的数据渲染界面 */
	initialData = (props, data) => {
		if (data && data[areaCode.formId] && data[areaCode.formId].rows) {
			let rows = data[areaCode.formId].rows[0];
			if (rows && rows.values) {
				let values = rows.values;
				collateCodition.map((cellName) => {
					if (values || values[cellName]) {
						let value = (() => {
							if (values[cellName].value && 'object' == typeof values[cellName].value) {
								return values[cellName].value.toString();
							} else {
								return values[cellName].value;
							}
						})();
						let display = (() => {
							if (values[cellName].display) {
								return values[cellName].display;
							} else if (values[cellName].value && 'object' == typeof values[cellName].value) {
								return values[cellName].value.toString();
							} else {
								return values[cellName].value;
							}
						})();
						props.form.setFormItemsValue(areaCode.formId, {
							[cellName]: {
								value: value,
								display: display
							}
						});
					}
				});
			}
		}
	};

	/* 获取界面中的值，拼装VO */
	getAllFiledAndValues = () => {
		//vo中需要的字段
		let data = {};
		let formData = this.props.form.getAllFormValue(areaCode.formId); //获取表单的值
		if (formData && formData.rows && formData.rows[0] && formData.rows[0].values) {
			let values = formData.rows[0].values;
			collateCodition.map((code) => {
				if (values[code]) {
					if (code == 'moneyType') {
						data[code] = values[code].value; //金额类型的值
					} else if (values[code].value) {
						data[code] = values[code].value.split(','); //多选的其他值
					} else {
						data[code] = []; //如果没有值，赋值空数组
					}
				}
			});
		}
		data.pk_org = this.props.pk_org ? this.props.pk_org : getContext(loginContextKeys.pk_org); //设置组织数据
		if (this.isShowUnit) {
			data.isIncludeSubGroup = formData.rows[0].values['pk_categorys'].isIncludeSubGroup
				? formData.rows[0].values['pk_categorys'].isIncludeSubGroup
				: false;
		} else {
			data.isIncludeSubGroup = false;
		}
		//由于返回后台的值与界面上要设置的值格式不一样，且总账那里要重新渲染业务系统组件，所以新增的时候需要将界面上的值缓存起来
		cardCache.setDefData('formData', dataSource, formData);
		return data;
	};

	/* 表单必输项校验 */
	checkCondition = () => {
		return this.props.form.isCheckNow(areaCode.formId);
	};

	/* 获取最新的pk_org 用于参照过滤 */
	getPk_org = () => {
		return this.props.pk_org;
	};

	render() {
		let { form } = this.props;
		let { createForm } = form;
		return (
			<div>
				<div className="nc-bill-form-area">{createForm(areaCode.formId, {})}</div>
			</div>
		);
	}
}

AccBalance = createPage({})(AccBalance);

export default function(props = {}) {
	var conf = {};
	return <AccBalance {...Object.assign(conf, props)} />;
}
