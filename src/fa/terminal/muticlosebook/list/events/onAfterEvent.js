import { pageConfig } from '../const';
const { searchId } = pageConfig;
export default function onAfterEvent(field, val) {
	let _this = this;
	switch (field) {
		case 'isallorg_flag':
			let isallorg_flag = this.props.search.getSearchValByField(searchId, 'isallorg_flag');
			if (isallorg_flag.value.firstvalue === 'option') {
				this.props.search.setDisabledByField(searchId, 'pk_org', false);
				//this.props.search.setDisabledByField(searchId, 'pk_accbook', false);
			} else {
				this.props.search.setDisabledByField(searchId, 'pk_org', true);
				this.props.search.setDisabledByField(searchId, 'pk_accbook', true);
				this.props.search.setSearchValByField(searchId, 'pk_org', { value: null, display: null });
				this.props.search.setSearchValByField(searchId, 'pk_accbook', { value: null, display: null });
			}
			break;
		case 'pk_org':
			let length = val.length;
			if (length == 0) {
				this.props.search.setDisabledByField(searchId, 'pk_accbook', true);
				this.props.search.setSearchValByField(searchId, 'pk_accbook', { value: null, display: null });
			} else {
				this.props.search.setDisabledByField(searchId, 'pk_accbook', false);
				this.props.search.setSearchValByField(searchId, 'pk_accbook', { value: null, display: null });
			}
			break;

		default:
			break;
	}
}
