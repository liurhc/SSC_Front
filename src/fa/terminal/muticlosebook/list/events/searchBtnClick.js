import { ajax, toast } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

//点击查询，获取查询区数据

import { pageConfig } from '../const';
const { searchId, tableId, url } = pageConfig;
export default function clickSearchBtn(props) {
	let data = null;
	let closebook_flag = null;
	let org_flag = null;
	let isallorg_flag = props.search.getSearchValByField(searchId, 'isallorg_flag');
	let isallorg_flagval = isallorg_flag.value.firstvalue;
	if (!isallorg_flagval) {
		return;
	}
	if (isallorg_flagval === 'option') {
		let pk_org = props.search.getSearchValByField(searchId, 'pk_org');

		let orgVal = pk_org.value.firstvalue;
		if (!orgVal) {
			toast({ color: 'warning', content: getMultiLangByID('201204020A-000009') /*国际化处理：请选择组织！*/ });
			return;
		}
		let pk_accbook = props.search.getSearchValByField(searchId, 'pk_accbook');

		let bookVal = pk_accbook.value.firstvalue;
		org_flag = 'N';
		closebook_flag = props.search.getSearchValByField(searchId, 'closebook_flag').value.firstvalue;
		if (!closebook_flag) {
			return;
		}
		data = {
			pk_org: orgVal,
			pk_accbook: bookVal,
			org_flag: org_flag,
			state: closebook_flag
		};
	} else {
		org_flag = 'Y';
		closebook_flag = props.search.getSearchValByField(searchId, 'closebook_flag').value.firstvalue;
		data = {
			org_flag: org_flag,
			state: closebook_flag
		};
	}
	let _this = this;
	if (isallorg_flagval && closebook_flag) {
		ajax({
			url: url.query,
			data,
			success: (res) => {
				let { success, data } = res;
				let meta = _this.props.meta.getMeta();
				let itemsRow = meta[tableId].items;
				let flag = true;
				for (let i = 0; i < itemsRow.length; i++) {
					if (itemsRow[i].label === getMultiLangByID('amcommon-000000' /*国际化处理：操作*/)) {
						itemsRow.splice(i, 1);
						break;
					}
				}
				if (success) {
					if (data) {
						let colNum = data.list_head.rows.length;
						let rowdata = data[tableId].rows;
						for (let rowda of rowdata) {
							rowda.values.context_show.display = rowda.values.show_flag.value;
						}
						props.editTable.setTableData(tableId, data[tableId]);
						props.editTable.selectAllRows(tableId, true);
						_this.setState({ totalNum: colNum });
						if (closebook_flag == 'Y') {
							_this.setState({ pageStatus: 'cancel' });
						} else _this.setState({ pageStatus: 'can' });
					} else {
						_this.props.editTable.setTableData(tableId, { rows: [] });
						_this.setState({ pageStatus: 'none' });
					}
				}
			}
		});
	}
}
//}
