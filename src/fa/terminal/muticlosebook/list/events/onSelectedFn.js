import { pageConfig } from '../const';
const { tableId } = pageConfig;
/**
 * 勾选回掉函数
 */
export default function onSelectedFn() {
	let checkdata = this.props.editTable.getCheckedRows(tableId);
	let length = checkdata.length;
	// 重置选中的行数
	this.setState({ totalNum: length });
}
