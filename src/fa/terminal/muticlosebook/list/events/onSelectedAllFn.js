import { pageConfig } from '../const';
const { tableId } = pageConfig;
/**
 * 表格区域选择所有行数据回调函数
 */
export default function onSelectedAllFn() {
	let checkdata = this.props.editTable.getCheckedRows(tableId);
	let length = checkdata.length;
	// 重置选中的行数
	this.setState({ totalNum: length });
}
