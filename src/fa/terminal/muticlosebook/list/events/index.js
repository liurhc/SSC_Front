import initTemplate from './initTemplate';
import searchBtnClick from './searchBtnClick';
import muticlosebook from './muticlosebook';
import mutideclosebook from './mutideclosebook';
import onAfterEvent from './onAfterEvent';
import tableButtonClick from './tableButtonClick';
import onSelectedAllFn from './onSelectedAllFn';
import onSelectedFn from './onSelectedFn';
export {initTemplate,searchBtnClick,muticlosebook,mutideclosebook,onAfterEvent,tableButtonClick,onSelectedAllFn,onSelectedFn};