import { ajax } from 'nc-lightapp-front';
import { tableButtonClick } from '../events';
import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { tableId, pagecode, url } = pageConfig;
export default function muticlosebook() {
	let checkdata = this.props.editTable.getCheckedRows(tableId);
	let colnum = null;
	for (let val of checkdata) {
		let rowindex = val.index;
		this.props.editTable.setRowStatus(tableId, rowindex, 1);
		colnum = colnum + rowindex + ',';
	}
	let changeRows = this.props.editTable.getChangedRows(tableId);
	let data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: changeRows
		},
		userjson: colnum
	};
	let _this = this;
	ajax({
		url: url.muticlosebookUrl,
		data,
		success: function(res) {
			if (res.data) {
				let successnum = 0;
				let failnum = 0;
				let mapdata = res.data;
				for (let key in mapdata) {
					let result = mapdata[key].show_flag;
					result = result.replace(/^\s+|\s+$/g, '');
					if (result.includes(getMultiLangByID('201204020A-000030'))) {
						successnum = successnum + 1;
						_this.props.editTable.setValByKeyAndIndex(tableId, key, 'context_show', {
							value: getMultiLangByID('201204020A-000002') /*国际化处理：结账成功 */,
							display: getMultiLangByID('201204020A-000002') /*国际化处理：结账成功 */
						});
						_this.props.editTable.setValByKeyAndIndex(tableId, key, 'show_flag', { value: result });
					} else {
						failnum = failnum + 1;
						_this.props.editTable.setValByKeyAndIndex(tableId, key, 'context_show', {
							value: getMultiLangByID('201204020A-000003') /*国际化处理：结账失败 */,
							display: getMultiLangByID('201204020A-000003') /*国际化处理：结账失败 */
						});
						_this.props.editTable.setValByKeyAndIndex(tableId, key, 'show_flag', { value: result });
					}
				}
				let material_event = {
					label: getMultiLangByID('amcommon-000000') /*国际化处理：操作*/,
					itemtype: 'customer',
					attrcode: 'opr',
					width: '200px',
					visible: true,

					render: (text, record, index) => {
						let colresult = record.values.context_show.display;
						colresult = colresult.replace(/^\s+|\s+$/g, '');
						if (
							!(colresult == getMultiLangByID('201204020A-000002' /*国际化处理：结账成功*/)) &&
							!(colresult == getMultiLangByID('201204020A-000031' /*国际化处理：未结账*/))
						) {
							let buttonAry = [ 'DealMessage' ];
							return _this.props.button.createOprationButton(buttonAry, {
								area: 'list_body_inner',
								onButtonClick: (props, key) => tableButtonClick(_this, props, key, text, record, index)
							});
						}
					}
				};
				let meta = _this.props.meta.getMeta();
				let itemsRow = meta[tableId].items;
				let flag = true;
				for (let ite of itemsRow) {
					if (ite.label === getMultiLangByID('amcommon-000000' /*国际化处理：操作*/)) {
						flag = false;
						break;
					}
				}
				if (flag) {
					meta[tableId].items.push(material_event);
				}
				_this.setState({ pageStatus: 'result' });
				_this.setState({ successNum: successnum });
				_this.setState({ failNum: failnum });
				_this.props.editTable.selectAllRows(tableId, false);
			}
		}
	});
}
