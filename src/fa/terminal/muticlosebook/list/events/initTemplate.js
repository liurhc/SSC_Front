import clickSearchBtn from './searchBtnClick';
import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

const { searchId, pagecode } = pageConfig;
let defaultConfig = {
	searchId: searchId
};
export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = contentfilter(props, meta);
					props.meta.setMeta(meta, () => {
						props.search.setSearchValByField(searchId, 'isallorg_flag', {
							value: 'all',
							display: getMultiLangByID('201204020A-000000') /*国际化处理：全部组织 */
						});
						props.search.setSearchValByField(searchId, 'closebook_flag', {
							value: 'N',
							display: getMultiLangByID('201204020A-000001') /*国际化处理：待结账*/
						});
						clickSearchBtn.call(this, props);
					});
				}
			}
		}
	);
}
function contentfilter(props, meta) {
	addSearchAreaReferFilter.call(this, props, meta, defaultConfig);
	meta.searchArea.items.map((item) => {
		if (item['attrcode'] === 'pk_accbook') {
			item.disabled = true;
		}
		if (item['attrcode'] === 'pk_org') {
			item.disabled = true;
		}
	});
	return meta;
}
