const pageConfig = {
	appcode: '201204020A',
	// 页面编码
	pagecode: '201204020A_list',
	//查询区
	searchId: 'searchArea',

	tableId: 'list_head',
	//组织
	pk_org: 'pk_org',
	//账簿
	pk_accbook: 'pk_accbook',
	//会计期间
	accyear: 'accyear',
	url: {
		muticlosebookUrl: '/nccloud/fa/muticlosebook/muticlosebook.do',
		mutideclosebookUrl: '/nccloud/fa/muticlosebook/mutideclosebook.do',
		query: '/nccloud/fa/muticlosebook/query.do'
	}
};

export { pageConfig };
