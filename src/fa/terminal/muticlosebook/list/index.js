import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;

const { NCButton, NCAffix, NCIcon } = base;
import {
	searchBtnClick,
	initTemplate,
	muticlosebook,
	mutideclosebook,
	onAfterEvent,
	onSelectedAllFn,
	onSelectedFn
} from './events';
import './index.less';
import { pageConfig } from './const';
const { searchId, tableId } = pageConfig;

class MutiCloseBook extends Component {
	constructor(props) {
		super(props);
		this.moduleId = '2012';
		this.searchId = 'searchArea';
		this.tableId = 'list_head';
		this.state = {
			pageStatus: 'none', //页面状态
			successNum: 0,
			failNum: 0,
			totalNum: 0,
			detail: ''
		};
		this.muticlosebook = muticlosebook.bind(this);
		this.mutideclosebook = mutideclosebook.bind(this);
		this.onAfterEvent = onAfterEvent.bind(this);
		initTemplate.call(this, props);
	}
	modalContent = () => {
		let notpassstr = getMultiLangByID('201204020A-000012' /*国际化处理：还未审核通过！*/);
		let notfreeStr = getMultiLangByID('201204020A-000014' /*国际化处理：不是自由态！*/);
		let detail = this.state.detail;
		detail = detail.replace(/\s+/g, '');
		let arrNopassData = detail.split(notpassstr);
		let arrNofreeData = detail.split(notfreeStr);
		let noPasslen = arrNopassData.length;
		let noFreelen = arrNofreeData.length;
		let flag = true;
		let arrData;
		let len;
		let name = ''; //保存用来拼接的头部信息
		let title = ''; //保存用来拼接的尾部提示信息
		if (noPasslen > 1 || noFreelen > 1) {
			if (noPasslen > 1) {
				arrData = arrNopassData;
				len = noPasslen;
			} else {
				arrData = arrNofreeData;
				len = noFreelen;
				flag = false;
			}
			return (
				<div className="addModal">
					{arrData.map((item, index) => {
						//根据数据尾部的提示信息，给title赋值
						if (flag) {
							title = getMultiLangByID('201204020A-000012' /*国际化处理：还未审核通过！ */);
						} else {
							title = getMultiLangByID('201204020A-000014' /*国际化处理：不是自由态! */);
						}

						//当数据只有一条或者数据比较多的时候的最后两条数据
						if ((len == 2 && index != 0) || index > len - 2) {
							return (
								<div className="modal-msg">
									<span style={{ marginLeft: 0 }}>{item}</span>
								</div>
							);
						}
						return (
							<div className="modal-msg">
								{item.split(';').map((ev, index) => {
									//截取每条数据的头部，用来拼接使用
									if (ev.indexOf('：') > 0) {
										name = ev.split('：')[0];
										ev = ev.split('：')[1];
									}

									//截取掉数据单据号后面的提示信息
									let msgstr = getMultiLangByID('201204020A-000011' /*国际化处理：还未审核通过*/);
									if (ev.includes(msgstr)) {
										ev = ev.substring(0, ev.length - msgstr.length);
									}
									//截取掉数据单据号后面的提示信息
									let freeStr = getMultiLangByID('201204020A-000013' /*国际化处理：不是自由态*/);
									if (ev.includes(freeStr)) {
										ev = ev.substring(0, ev.length - freeStr.length);
									}
									return (
										<p>
											{name && <span>{name}：</span>}
											{ev} <span>{title}</span>
										</p>
									);
								})}
							</div>
						);
					})}
				</div>
			);
		} else {
			return (
				<div className="addModal">
					<p style={{ 'word-break': 'break-word' }}>{this.state.detail}</p>
				</div>
			);
		}
	};
	render() {
		let { editTable, search, modal } = this.props;
		let { createEditTable } = editTable;
		let { createModal } = modal;
		let { NCCreateSearch } = search;
		let { pageStatus, successNum, failNum, totalNum } = this.state;
		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">
								{' '}
								{getMultiLangByID('201204020A-000017') /*国际化处理：集中结账 */}
							</h2>
						</div>
					</div>

					<div className="nc-bill-search-area  search-info">
						{NCCreateSearch(searchId, {
							clickSearchBtn: searchBtnClick.bind(this),
							showAdvBtn: false,
							onAfterEvent: this.onAfterEvent
						})}
					</div>
				</NCAffix>
				<div className="nc-bill-table-area msg-info">
					{pageStatus === 'can' && (
						<div className="can-closebook ">
							<NCIcon type="uf-exc-c" />
							<p>
								{getMultiLangByID('201204020A-000018', {
									totalNum: totalNum
								}) /*国际化处理：已经为你选择了可以结账的公司,请结账！ */}
							</p>
							<NCButton
								colors="primary"
								onClick={this.muticlosebook}
								disabled={totalNum === 0 ? true : false}
							>
								{getMultiLangByID('201204020A-000019', {
									totalNum: totalNum
								}) /*国际化处理：结账 */}
							</NCButton>
						</div>
					)}
					{pageStatus === 'cancel' && (
						<div className="can-declosebook ">
							{/* <NCIcon type="uf-exc-c" /> */}
							<NCIcon type="uf-exc-c" />
							<p>
								{getMultiLangByID('201204020A-000020', {
									totalNum: totalNum
								}) /*国际化处理：已经为您选择了所有可以取消结账的公司，取消结账将删除你当月所有的业务单据，确认取消结账吗？*/}
							</p>
							<NCButton
								colors="primary"
								onClick={this.mutideclosebook}
								className="buttons"
								disabled={totalNum === 0 ? true : false}
							>
								{getMultiLangByID('201204020A-000021') /*国际化处理：取消结账*/}
							</NCButton>
						</div>
					)}
					{pageStatus === 'result' &&
					successNum !== 0 &&
					failNum != 0 && (
						<div className="result-close">
							{/* <NCIcon type="uf-correct" /> */}
							<NCIcon type="uf-exc-c" />
							<p>
								{getMultiLangByID('201204020A-000023', {
									successNum: successNum,
									failNum: failNum
								}) /*国际化处理：successNum个账簿结账成功！failNum个账簿结账失败！*/}
							</p>
						</div>
					)}
					{pageStatus === 'result' &&
					successNum !== 0 &&
					failNum == 0 && (
						<div className="result-close">
							{/* <NCIcon type="uf-correct" /> */}
							<NCIcon type="uf-exc-c" />
							<p>
								{getMultiLangByID('201204020A-000022', {
									successNum: successNum
								}) /*国际化处理：successNum个账簿结账成功！*/}
							</p>
						</div>
					)}
					{pageStatus === 'result' &&
					successNum == 0 &&
					failNum != 0 && (
						<div className="result-close">
							{/* <NCIcon type="uf-correct" /> */}
							<NCIcon type="uf-exc-c" />
							<p>
								{getMultiLangByID('201204020A-000024', {
									failNum: failNum
								}) /*国际化处理：failNum个账簿结账失败！*/}
							</p>
						</div>
					)}
					{pageStatus === 'deresult' &&
					successNum != 0 &&
					failNum != 0 && (
						<div className="result-declose ">
							{/* <NCIcon type="uf-correct" /> */}
							<NCIcon type="uf-exc-c" />
							<p>
								{getMultiLangByID('201204020A-000027', {
									successNum: successNum,
									failNum: failNum
								}) /*国际化处理：{successNum}个账簿取消结账成功！{failNum}个账簿取消结账失败！*/}
							</p>
						</div>
					)}
					{pageStatus === 'deresult' &&
					successNum != 0 &&
					failNum == 0 && (
						<div className="result-declose ">
							{/* <NCIcon type="uf-correct" /> */}
							<NCIcon type="uf-exc-c" />
							<p>
								{getMultiLangByID('201204020A-000025', {
									successNum: successNum
								}) /*国际化处理：{successNum}个账簿取消结账成功！*/}
							</p>
						</div>
					)}
					{pageStatus === 'deresult' &&
					successNum == 0 &&
					failNum != 0 && (
						<div className="result-declose ">
							{/* <NCIcon type="uf-correct" /> */}
							<NCIcon type="uf-exc-c" />
							<p>
								{getMultiLangByID('201204020A-000026', {
									failNum: failNum
								}) /*国际化处理：{failNum}个账簿取消结账失败！*/}
							</p>
						</div>
					)}
				</div>
				<div className="nc-singleTable-table-area">
					{createEditTable(tableId, {
						showCheck: true,
						showIndex: true,
						onSelected: onSelectedFn.bind(this), // 左侧选择列单个选择框回调
						onSelectedAll: onSelectedAllFn.bind(this) // 左侧选择列全选选择框回调
					})}
				</div>
				{createModal('detailmodal', {
					title: getMultiLangByID('201204020A-000016'),
					rightBtnName: '', //左侧按钮名称,默认关闭
					leftBtnName: '', //右侧按钮名称， 默认确认
					//size: '200', //  模态框大小 sm/lg/xlg
					//noFooter: false, //是否需要底部按钮,默认true

					content: this.modalContent()
				})}
			</div>
		);
	}
}
MutiCloseBook = createPage({})(MutiCloseBook);

initMultiLangByModule({ fa: [ '201204020A', 'facommon' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<MutiCloseBook />, document.querySelector('#app'));
});
