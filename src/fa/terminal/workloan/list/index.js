import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
import { pageConfig } from './const';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	afterEvent,
	tableAfterEvent,
	searchClearEve,
	onSelectedFn
} from './events';
import './index.less';
const { NCAffix, NCUpload } = base;
const { editBtns, browseBtns, pageId } = pageConfig;

class List extends Component {
	constructor(props) {
		super(props);
		this.searchId = 'searchArea';
		this.tableId = 'list_head';
		this.state = {
			pageStatus: 'none', //页面状态
			allworkloan: 0.0,
			premonthworkloan: 0.0,
			monthworkloan: 0.0,
			accuworkloan: 0.0,
			remainingworkloan: 0.0,
			cardNum: 0,
			isFipMakeVoucher: 'N',
			isMinUnCloseBookPeriod: 'Y',
			config: {},
			hideBtnAreaFlag: false //查询区域按钮隐藏
		};
		this.firstEditValue = {}; // 用于取消处理，记录修改前的数据
		this.flag = 0; //个性化设置标志位
		initTemplate.call(this, props);
	}
	componentWillUpdate() {}
	componentDidMount() {
		this.props.button.setButtonVisible(editBtns, false);
		this.props.button.setButtonVisible(browseBtns, true);
		this.getData();
	}

	getData = () => {};

	render() {
		let { editTable, search, ncmodal } = this.props;
		const { createEditTable } = editTable;
		let { NCCreateSearch } = search;
		let { createModal } = ncmodal;
		let {
			pageStatus,
			allworkloan,
			premonthworkloan,
			monthworkloan,
			accuworkloan,
			remainingworkloan,
			cardNum,
			hideBtnAreaFlag
		} = this.state;
		return (
			<div className="nc-bill-list work-loan">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">
								{getMultiLangByID('201204008A-000013' /*国际化处理：工作量录入*/)}
							</h2>
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>

					<div className="nc-bill-search-area">
						{NCCreateSearch(this.searchId, {
							clickSearchBtn: searchBtnClick.bind(this),
							onAfterEvent: afterEvent.bind(this),
							showAdvBtn: false,
							hideBtnArea: hideBtnAreaFlag,
							advSearchClearEve: searchClearEve.bind(this)
						})}
					</div>
				</NCAffix>
				{pageStatus === 'ok' && (
					<div className="nc-bill-table-area">
						<div className="ok">
							<div className="bigfont">
								<span>{getMultiLangByID('201204008A-000014' /*国际化处理：本月工作量：*/)}</span>
								<span>{monthworkloan}</span>
								<span>{getMultiLangByID('201204008A-000015' /*国际化处理：上月工作量：*/)}</span>
								<span>{premonthworkloan}</span>
							</div>
							<div className="smallfont">
								<p>
									<span>{getMultiLangByID('201204008A-000016' /*国际化处理：工作总量：*/)}</span>
									<span>{allworkloan}</span>
								</p>
								<p>
									<span>{getMultiLangByID('201204008A-000017' /*国际化处理：累计工作量：*/)}</span>
									<span>{accuworkloan}</span>
								</p>
								<p>
									<span>{getMultiLangByID('201204008A-000018' /*国际化处理：剩余工作量：*/)}</span>
									<span>{remainingworkloan}</span>
								</p>
								<p>
									<span>{getMultiLangByID('201204008A-000019' /*国际化处理：计提折旧卡片数：*/)}</span>
									<span>{cardNum}</span>
								</p>
							</div>
						</div>
					</div>
				)}
				{createModal(`${pageId}-confirm`, { color: 'warning' })}
				<div className="nc-singleTable-table-area">
					{createEditTable(this.tableId, {
						onAfterEvent: tableAfterEvent.bind(this),
						onSelected: onSelectedFn.bind(this),
						onSelectedAll: onSelectedFn.bind(this),
						showCheck: true,
						showIndex: true
					})}
				</div>
				<NCUpload {...this.state.config}>
					<div className="import" />
				</NCUpload>
			</div>
		);
	}
}

List = createPage({})(List);

initMultiLangByModule({ fa: [ '201204008A', 'facommon' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<List />, document.querySelector('#app'));
});
