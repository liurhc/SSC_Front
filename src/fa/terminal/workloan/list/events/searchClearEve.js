import { pageConfig } from '../const';
const { searchId, tableId } = pageConfig;
/**
 * 数据清空时，设置所有参照及期间不可编辑
 */
export default function searchClearEve() {
	this.props.search.setDisabledByField(searchId, 'pk_accbook', true);
	this.props.search.setDisabledByField(searchId, 'accyear', true);
	this.props.search.setDisabledByField(searchId, 'period', true);
	this.setState({ pageStatus: 'none' });
	this.props.editTable.setTableData(tableId, { rows: [] });
}
