import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import searchBtnClick from './searchBtnClick';
const { searchId, url, tableId } = pageConfig;
export default function afterEvent(fileds, val) {
	let searchData = this.props.search.getAllSearchData(searchId, false);
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}
	switch (fileds) {
		case 'pk_org':
			if (JSON.stringify(val) === '{}') {
				this.props.search.setDisabledByField(searchId, 'pk_accbook', true);
				this.props.search.setDisabledByField(searchId, 'accyear', true);
				this.props.search.setDisabledByField(searchId, 'period', true);
				this.props.search.setSearchValByField(searchId, 'pk_accbook', {
					value: '',
					display: ''
				});
				this.props.search.setSearchValByField(searchId, 'accyear', {
					value: '',
					display: ''
				});
				this.props.search.setSearchValByField(searchId, 'period', {
					value: '',
					display: ''
				});
				this.setState({ pageStatus: 'none' });
				this.props.editTable.setTableData(tableId, { rows: [] });
				return;
			}

			data.pk_accbook = null;
			this.props.search.setDisabledByField(searchId, 'pk_accbook', false);
			let _this = this;
			ajax({
				url: url.orgchangeUrl,
				data,
				success: (res) => {
					if (res.data) {
						if (res.data.pk_accbook) {
							_this.props.search.setSearchValByField(searchId, 'pk_accbook', res.data.pk_accbook);
						}
						if (res.data.years) {
							_this.props.search.setDisabledByField(searchId, 'accyear', false);
							_this.props.search.setDisabledByField(searchId, 'period', false);
							let selectData = res.data.years.map((year) => {
								return {
									display: year,
									value: year
								};
							});
							let accyear = res.data.mincloseperiod.accyear;
							let periodData = res.data.yearperiod[accyear].map((item) => {
								return {
									display: item,
									value: item
								};
							});
							let meta = _this.props.meta.getMeta();

							meta.searchArea.items.forEach((item) => {
								if (item.attrcode === 'accyear') {
									item.options = selectData;
								}
								if (item.attrcode === 'period') {
									item.options = periodData;
								}
							});
							_this.props.meta.setMeta(meta);

							let path = res.data.mincloseperiod;
							searchAreaSetValue.call(_this, _this.props, searchId, 'accyear', {
								value: path.accyear,
								display: path.accyear
							});
							searchAreaSetValue.call(_this, _this.props, searchId, 'period', {
								value: path.period,
								display: path.period
							});
						}
					}
					searchBtnClick.call(this, this.props);
				}
			});
			break;
		case 'pk_accbook':
			if (JSON.stringify(val) === '{}') {
				this.props.search.setDisabledByField(searchId, 'accyear', true);
				this.props.search.setDisabledByField(searchId, 'period', true);
				this.props.search.setSearchValByField(searchId, 'accyear', {
					value: '',
					display: ''
				});
				this.props.search.setSearchValByField(searchId, 'period', {
					value: '',
					display: ''
				});
				this.setState({ pageStatus: 'none' });
				this.props.editTable.setTableData(tableId, { rows: [] });
				return;
			}

			ajax({
				url: url.orgchangeUrl,
				data,
				success: (res) => {
					if (res.data) {
						if (res.data.years) {
							this.props.search.setDisabledByField(searchId, 'accyear', false);
							this.props.search.setDisabledByField(searchId, 'period', false);
							let selectData = res.data.years.map((year) => {
								return {
									display: year,
									value: year
								};
							});
							let accyear = res.data.mincloseperiod.accyear;
							let periodData = res.data.yearperiod[accyear].map((item) => {
								return {
									display: item,
									value: item
								};
							});
							let meta = this.props.meta.getMeta();
							meta.searchArea.items = meta.searchArea.items.map((item) => {
								if (item.attrcode === 'accyear') {
									return Object.assign({}, item, { options: selectData });
								}
								if (item.attrcode === 'period') {
									return Object.assign({}, item, { options: periodData });
								}
								return item;
							});
							this.props.meta.setMeta(meta);
							let path = res.data.mincloseperiod;
							this.props.search.setSearchValByField(searchId, 'accyear', {
								value: path.accyear,
								display: path.accyear
							});
							this.props.search.setSearchValByField(searchId, 'period', {
								value: path.period,
								display: path.period
							});
						}
					}
					searchBtnClick.call(this, this.props);
				}
			});
			break;
		case 'accyear':
			if (val === '__clear__') {
				//年度清空的状态
				let meta = this.props.meta.getMeta();
				meta.searchId.items = meta.searchId.items.map((item) => {
					if (item.attrcode === 'period') {
						return Object.assign({}, item, { options: [] });
					}
					return item;
				});
				this.props.meta.setMeta(meta);
				this.props.search.setSearchValByField(searchId, 'accyear', {
					value: '',
					display: ''
				});
				this.props.search.setSearchValByField(searchId, 'period', {
					value: '',
					display: ''
				});
			} else {
				if (JSON.stringify(val) === '{}' || !val) {
					this.props.search.setDisabledByField(searchId, 'period', true);
					this.props.search.setSearchValByField(searchId, 'period', {
						value: '',
						display: ''
					});
					return;
				}
				ajax({
					url: url.orgchangeUrl,
					data,
					success: (res) => {
						if (res.data) {
							if (res.data.years) {
								this.props.search.setDisabledByField(searchId, 'period', false);
								let selectData = res.data.years.map((year) => {
									return {
										display: year,
										value: year
									};
								});
								let accyear = res.data.mincloseperiod.accyear;
								let periodData = res.data.yearperiod[accyear].map((item) => {
									return {
										display: item,
										value: item
									};
								});
								let meta = this.props.meta.getMeta();
								meta.searchArea.items = meta.searchArea.items.map((item) => {
									if (item.attrcode === 'accyear') {
										return Object.assign({}, item, { options: selectData });
									}
									if (item.attrcode === 'period') {
										return Object.assign({}, item, { options: periodData });
									}
									return item;
								});
								this.props.meta.setMeta(meta);
								let path = res.data.mincloseperiod;
								this.props.search.setSearchValByField(searchId, 'accyear', {
									value: val === '__clear__' ? '' : val,
									display: val === '__clear__' ? '' : val
								});
								this.props.search.setSearchValByField(searchId, 'period', {
									value: val === '__clear__' ? '' : path.period,
									display: val === '__clear__' ? '' : path.period
								});
							}
						}
						searchBtnClick.call(this, this.props);
					}
				});
			}

			break;
		case 'period':
			if (val === '__clear__') {
				//清空期间的状态
				this.props.search.setSearchValByField(searchArea, 'period', {
					value: '',
					display: ''
				});
			} else {
				ajax({
					url: url.orgchangeUrl,
					data,
					success: (res) => {
						if (res.data) {
							if (res.data.years) {
								this.props.search.setSearchValByField(searchId, 'period', {
									value: val === '__clear__' ? '' : val,
									display: val === '__clear__' ? '' : val
								});
							}
						}
						searchBtnClick.call(this, this.props);
					}
				});
			}

			break;
	}
}
/**
 *  查询区域赋值
   * @param {*} props 
   * @param {*} data
 */
export function searchAreaSetValue(props, searchArea, item, data) {
	props.search.setSearchValByField(searchArea, item, { value: data.value, display: data.display });
}
