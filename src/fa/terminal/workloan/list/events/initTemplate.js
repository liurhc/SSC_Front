import { ajax } from 'nc-lightapp-front';
import clickSearchBtn from './searchBtnClick';

import { pageConfig } from '../const';

import ampub from 'ampub';
const { components } = ampub;
const { LoginContext, faQueryAboutUtils } = components;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { openAssetCardByPk } = faQueryAboutUtils;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

const { searchId, tableId, pageId, url } = pageConfig;
let defaultConfig = {
	searchId: searchId
};
export default function(props) {
	let linkpk_org = props.getUrlParam('pk_org');
	let linkpk_org_name = props.getUrlParam('pk_org_name');
	let linkpk_accbook = props.getUrlParam('pk_accbook');
	let linkpk_accbook_name = props.getUrlParam('pk_accbook_name');
	let linkaccyear = props.getUrlParam('accyear');
	let linkperiod = props.getUrlParam('period');
	let _this = this;
	props.createUIDom(
		{
			pagecode: pageId
		},
		(data) => {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.context) {
					//初始化上下文变量
					loginContext.call(_this, data.context);
				}
				if (data.template) {
					let meta = data.template;
					meta = filterItems(props, meta, _this);
					meta.context = data.context;
					props.meta.setMeta(meta, () => {
						if (linkpk_org && linkpk_accbook && linkaccyear && linkperiod) {
							let backdata = {};
							backdata.pk_org = linkpk_org;
							ajax({
								url: url.orgchangeUrl,
								data: backdata,
								success: (res) => {
									if (res.data) {
										if (res.data.years) {
											props.search.setDisabledByField(searchId, 'accyear', false);
											props.search.setDisabledByField(searchId, 'period', false);
											let selectData = res.data.years.map((year) => {
												return {
													display: year,
													value: year
												};
											});
											let accyear = res.data.mincloseperiod.accyear;
											let periodData = res.data.yearperiod[accyear].map((item) => {
												return {
													display: item,
													value: item
												};
											});
											let meta = props.meta.getMeta();
											meta.searchArea.items.forEach((item) => {
												if (item.attrcode === 'accyear') {
													item.options = selectData;
												}
												if (item.attrcode === 'period') {
													item.options = periodData;
												}
											});
											props.meta.setMeta(meta, () => {
												props.search.setSearchValByField(searchId, 'accyear', {
													value: linkaccyear,
													display: linkaccyear
												});
												props.search.setSearchValByField(searchId, 'period', {
													value: linkperiod,
													display: linkperiod
												});
												props.search.setSearchValByField(searchId, 'pk_org', {
													value: linkpk_org,
													display: linkpk_org_name
												});
												props.search.setSearchValByField(searchId, 'pk_accbook', {
													value: linkpk_accbook,
													display: linkpk_accbook_name
												});
											});
										}
									}
									clickSearchBtn.call(_this, props, true);
								}
							});
						} else {
							let pk_org = getContext(loginContextKeys.pk_org);
							let org_Name = getContext(loginContextKeys.org_Name);
							if (!pk_org || pk_org === '') {
								props.search.setSearchValByField(searchId, 'pk_accbook', { value: '', display: '' });
								props.search.setSearchValByField(searchId, 'accyear', { value: '', display: '' });
								props.search.setSearchValByField(searchId, 'period', { value: '', display: '' });
								props.search.setDisabledByField(searchId, 'pk_accbook', true);
								props.search.setDisabledByField(searchId, 'accyear', true);
								props.search.setDisabledByField(searchId, 'period', true);
							} else {
								props.search.setSearchValByField(searchId, 'pk_org', {
									value: pk_org,
									display: org_Name
								});
								let data = {};
								data['pk_org'] = pk_org;
								ajax({
									url: url.orgchangeUrl,
									data,
									success: (res) => {
										if (res.data) {
											if (res.data.pk_accbook) {
												_this.props.search.setSearchValByField(
													searchId,
													'pk_accbook',
													res.data.pk_accbook
												);
											}
											if (res.data.years) {
												_this.props.search.setDisabledByField(searchId, 'accyear', false);
												_this.props.search.setDisabledByField(searchId, 'period', false);
												let selectData = res.data.years.map((year) => {
													return {
														display: year,
														value: year
													};
												});
												let accyear = res.data.mincloseperiod.accyear;
												let periodData = res.data.yearperiod[accyear].map((item) => {
													return {
														display: item,
														value: item
													};
												});
												let meta = _this.props.meta.getMeta();

												meta.searchArea.items.forEach((item) => {
													if (item.attrcode === 'accyear') {
														item.options = selectData;
													}
													if (item.attrcode === 'period') {
														item.options = periodData;
													}
												});
												_this.props.meta.setMeta(meta);
												let path = res.data.mincloseperiod;
												_this.props.search.setSearchValByField(searchId, 'accyear', {
													value: path.accyear,
													display: path.accyear
												});
												_this.props.search.setSearchValByField(searchId, 'period', {
													value: path.period,
													display: path.period
												});
											}
										}
										clickSearchBtn.call(_this, props);
									}
								});
							}
						}
					});
				}
			}
		}
	);
}

function filterItems(props, meta, _this) {
	addSearchAreaReferFilter.call(this, props, meta, defaultConfig);
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'asset_code') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							openAssetCardByPk.call(_this, props, record.values.pk_card.value);
						}}
					>
						{record && record.values.asset_code && record.values.asset_code.value}
					</span>
				);
			};
		}
		return item;
	});
	return meta;
}
