import { pageConfig } from '../const';
const { tableId } = pageConfig;
/**
 * 勾选回掉函数
 */
export default function onSelectedFn() {
	let checkdata = this.props.editTable.getCheckedRows(tableId);
	this.props.button.setButtonDisabled([ 'InheritWorkloan' ], !(checkdata && checkdata.length > 0));
}
