import { ajax, toast, print, output } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { queryData } from './searchBtnClick';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { MsgConst, showMessage } = msgUtils;

const { editBtns, browseBtns, middleBtns, searchId, tableId, pageId, url } = pageConfig;
export default function buttonClick(props, id) {
	switch (id) {
		case 'Save':
			let changeRows = props.editTable.getChangedRows(tableId);
			let pk_org = props.search.getSearchValByField(searchId, 'pk_org');
			let orgVal = pk_org.value.firstvalue;
			let pk_accbook = props.search.getSearchValByField(searchId, 'pk_accbook');
			//先写上判断，正常情况都会有值
			let bookVal = null,
				accyearVal = null,
				periodVal = null;
			if (pk_accbook) {
				bookVal = pk_accbook.value.firstvalue;
			}

			let accyear = props.search.getSearchValByField(searchId, 'accyear');
			if (accyear) {
				accyearVal = accyear.value.firstvalue;
			}

			let period = props.search.getSearchValByField(searchId, 'period');
			if (period) {
				periodVal = period.value.firstvalue;
			}
			let otherDate = orgVal + ',' + bookVal + ',' + accyearVal + ',' + periodVal;
			const data = {
				pageid: pageId,
				model: {
					areaType: 'table',
					areacode: tableId,
					rows: changeRows
				},
				userjson: otherDate
			};
			ajax({
				url: url.saveUrl,
				data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data.flag) {
							let allrows = props.editTable.getAllRows(tableId);
							let allworkloan = 0,
								premonthworkloan = 0,
								monthworkloan = 0,
								accuworkloan = 0,
								remainingworkloan = 0,
								cardlengthNum = 0;
							for (let rowval of allrows) {
								cardlengthNum++;
								allworkloan = Number(allworkloan, 2) + Number(rowval.values.allworkloan.value, 2);
								premonthworkloan =
									Number(premonthworkloan, 2) + Number(rowval.values.premonthworkloan.value, 2);
								monthworkloan = Number(monthworkloan, 2) + Number(rowval.values.monthworkloan.value, 2);
								accuworkloan = Number(accuworkloan, 2) + Number(rowval.values.accuworkloan.value, 2);
								remainingworkloan =
									Number(remainingworkloan, 2) + Number(rowval.values.remainingworkloan.value, 2);
							}

							this.setState({ allworkloan: allworkloan.toFixed(2) });
							this.setState({ premonthworkloan: premonthworkloan.toFixed(2) });
							this.setState({ monthworkloan: monthworkloan.toFixed(2) });
							this.setState({ accuworkloan: accuworkloan.toFixed(2) });
							this.setState({ remainingworkloan: remainingworkloan.toFixed(2) });
							this.setState({ cardNum: cardlengthNum });
							this.setState({ pageStatus: 'ok' });
							showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
							props.button.setButtonVisible(editBtns, false);
							props.button.setButtonVisible(browseBtns, true);
							props.editTable.setStatus(tableId, 'browse');
							this.props.search.setDisabled(searchId, false);
							this.setState({ hideBtnAreaFlag: false });
						}
					}
				}
			});
			break;

		case 'InheritWorkloan':
			InheritWorkloan.call(this, props);
			break;
		case 'Edit':
			let searchData = props.search.getAllSearchData(searchId);
			let datas = {};
			if (searchData.conditions) {
				searchData.conditions.map((item) => {
					datas[item.field] = item.value.firstvalue;
				});
			}
			if (!(datas['pk_org'] && datas['pk_accbook'] && datas['accyear'] && datas['period'])) {
				toast({ color: 'warning', content: getMultiLangByID('201204008A-000001' /*国际化处理：请输入查询条件！*/) });
				return;
			}

			ajax({
				url: url.editenable,
				data: datas,
				success: (res) => {
					if (res.data || res.error) {
						toast({ content: res.data, color: 'danger' });
						return;
					} else {
						let allrow = props.editTable.getAllRows(tableId);
						if (allrow.length == 0) {
							toast({
								content: getMultiLangByID('201204008A-000002' /*国际化处理：当前无可修改数据！*/),
								color: 'warning'
							});
							return;
						}
						this.props.search.setDisabled(searchId, true);
						props.button.setButtonVisible(editBtns, true);
						props.button.setButtonVisible(browseBtns, false);
						let datas = props.editTable.getCheckedRows(tableId);
						if (datas.length === 0) {
							props.button.setButtonDisabled([ 'InheritWorkloan' ], true);
						} else {
							props.button.setButtonDisabled([ 'InheritWorkloan' ], false);
						}
						props.button.setButtonVisible(middleBtns, true);
						this.setState({ hideBtnAreaFlag: true });
						this.firstEditValue = allrow;
						props.editTable.setStatus(tableId, 'edit');
						this.setState({ pageStatus: 'none' });
					}
				}
			});

			break;
		case 'Cancel':
			cancelConfirm.call(this, props);

			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'output':
			outputTemp.call(this, props);
			break;
		case 'Import':
			let backData = this.props.editTable.getAllRows(tableId);
			const newdata = {
				pageid: pageId,
				model: {
					areaType: 'table',
					areacode: tableId,
					rows: backData
				}
			};
			let _this = this;
			let config = {
				showUploadList: false,
				name: 'file',
				data: {
					pagecode: pageId,
					areacode: tableId,
					backData: newdata ? JSON.stringify(newdata) : ''
				},
				action: url.ImportUrl,
				headers: {
					authorization: 'authorization-text'
				},
				NcUploadOnChange(info) {
					if (info.file.status === 'done') {
						if (info.file.response.success) {
							if (info.file.response.data.res.list_head) {
								let data = info.file.response.data.res;
								let returnErrorMsg = info.file.response.data.errorMsg;
								let rowdata = data.list_head;
								if (returnErrorMsg) {
									let errorstr = returnErrorMsg.split('。');
									let returnstr = JSON.stringify(errorstr);
									let resultstr = returnstr.substring(1, returnstr.length - 1);
									resultstr = resultstr.replace(/\","/g, '"。"');
									toast({
										color: 'danger',
										content: resultstr
									});
								} else {
									toast({
										color: 'success',
										content: getMultiLangByID('201204008A-000003' /*国际化处理：全部数据导入成功！*/)
									});
								}
								_this.props.editTable.setTableData(tableId, rowdata);
							} else {
								let errorMsg = info.file.response.data.errorMsg;

								if (errorMsg) {
									toast({ color: 'danger', content: errorMsg });
								}
							}
						}
					}
				}
			};
			this.setState({ config: config });
			document.getElementsByClassName('import')[0].click();
			break;
		case 'BathAltert':
			BatchAltert.call(this, props);
			break;
		case 'Export':
			onExport.call(this, props);
			break;
		case 'Refresh': //刷新
			//刷新
			refreshAction.call(this, props);
			break;
	}
}

//刷新
function refreshAction(props) {
	let searchData = props.search.getAllSearchData(searchId);
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}
	queryData.call(this, props, data, searchData, true);
}

function cancelConfirm(props) {
	props.ncmodal.show(`${pageId}-confirm`, {
		title: getMultiLangByID('msgUtils-000002' /*国际化处理：取消*/),
		content: getMultiLangByID('msgUtils-000003' /*国际化处理：确认取消*/),
		beSureBtnClick: () => {
			this.props.search.setDisabled(searchId, false);
			props.editTable.cancelEdit(tableId);
			props.editTable.setStatus(tableId, 'browse');
			this.setState({ pageStatus: 'ok', hideBtnAreaFlag: false });
			props.button.setButtonVisible(editBtns, false);
			props.button.setButtonVisible(browseBtns, true);
			props.editTable.setTableData(tableId, { rows: this.firstEditValue });
		}
	});
}
export function outputTemp(props) {
	let alldata = props.editTable.getAllRows(tableId);
	if (alldata.length == 0) {
		toast({ color: 'warning', content: getMultiLangByID('201204008A-000004' /*国际化处理：没有可以输出的数据！*/) });
		return;
	}
	let searchData = props.search.getAllSearchData(searchId);
	let wholedata = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			wholedata[item.field] = item.value.firstvalue;
		});
	}

	let afterdata = {
		pageid: pageId,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: alldata
		}
	};

	let userjson = JSON.stringify(afterdata);
	let filterdate =
		'{"pk_org":' +
		"'" +
		wholedata['pk_org'] +
		"'" +
		',"pk_accbook":' +
		"'" +
		wholedata['pk_accbook'] +
		"'" +
		',"accyear":' +
		wholedata['accyear'] +
		',"period":' +
		wholedata['period'] +
		'}';
	let printData = {
		filename: getMultiLangByID('201204008A-000000' /*国际化处理：录入工作量*/), // 文件名称
		nodekey: 'workloan', // 模板节点标识
		oids: [ userjson, filterdate ], //将过滤条件通过主键字段进行传递+将查询条件以userjson字段进行传递
		outputType: 'output'
	};
	output({
		url: url.printUrl,
		data: printData
	});
}

export function printTemp(props) {
	let alldata = props.editTable.getAllRows(tableId);
	if (alldata.length == 0) {
		toast({ color: 'warning', content: getMultiLangByID('201204008A-000005' /*国际化处理：没有可以打印的数据！*/) });
		return;
	}
	let searchData = props.search.getAllSearchData(searchId);
	let wholedata = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			wholedata[item.field] = item.value.firstvalue;
		});
	}

	let afterdata = {
		pageid: pageId,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: alldata
		}
	};

	let userjson = JSON.stringify(afterdata);
	let filterdate =
		'{"pk_org":' +
		"'" +
		wholedata['pk_org'] +
		"'" +
		',"pk_accbook":' +
		"'" +
		wholedata['pk_accbook'] +
		"'" +
		',"accyear":' +
		wholedata['accyear'] +
		',"period":' +
		wholedata['period'] +
		'}';
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		{
			filename: getMultiLangByID('201204008A-000000' /*国际化处理：录入工作量*/), // 文件名称
			nodekey: 'workloan', // 模板节点标识
			oids: [ userjson, filterdate ], //将过滤条件通过主键字段进行传递+将查询条件以userjson字段进行传递
			outputType: 'print'
		}
	);
}

const BatchAltert = (props) => {
	let returnData = props.editTable.batchChangeTableData(tableId);
	if (!returnData) {
		return;
	}
	if (!returnData.value) {
		returnData.value = 0;
	}
	let editCode = returnData.code;
	let afterdata = {
		code: editCode,
		value: returnData.value
	};
	let datas = props.editTable.getAllRows(tableId);
	for (let dataValue of datas) {
		let index = dataValue.index;
		props.editTable.setRowStatus(tableId, index, 1);
	}
	let changeRows = props.editTable.getChangedRows(tableId);

	const data = {
		pageid: pageId,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: datas
		}
	};
	data.userjson = JSON.stringify(afterdata);
	ajax({
		url: url.batchAltertUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.editTable.setTableData(tableId, data.data[tableId]);
					if (data.result != null) {
						let result = data.result;
						let rowarr = result.split(',').length;
						toast({
							//getMultiLangByID('201204008A-000000'),
							content: getMultiLangByID(
								'201204008A-000006',
								{
									rowarr: rowarr,
									result: result
								} /*国际化处理：rowarr + '条批改失败：' + '序号为' + result + ' 的剩余工作量不能小于0，请重新录入！*/
							),

							color: 'danger'
						});
					}
				} else {
					props.editTable.setTableData(tableId, { rows: [] });
				}
			}
		}
	});
};
const onExport = (props) => {
	let alldata = props.editTable.getAllRows(tableId);
	for (let i = 0; i < alldata.length; i++) {
		props.editTable.setRowStatus(tableId, i, 0);
	}
	const newdata = {
		pageid: pageId,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: alldata
		}
	};
	let data = {
		pagecode: pageId,
		areacode: tableId,
		datas: newdata ? JSON.stringify(newdata) : ''
	};
	props.downLoad({ data, url: url.exportUrl });
};

const InheritWorkloan = (props) => {
	let datas = props.editTable.getCheckedRows(tableId);
	if (datas.length === 0) {
		toast({ content: getMultiLangByID('201204008A-000007' /*国际化处理：请选择数据！*/), color: 'warning' });
		return;
	}
	for (let value of datas) {
		let index = value.index;
		props.editTable.setRowStatus(tableId, index, 1);
	}
	let changeRows = props.editTable.getChangedRows(tableId);
	let newrows = [];
	for (let changeRow of changeRows) {
		if (changeRow.selected) {
			newrows.push(changeRow);
		}
	}
	const data = {
		pageid: pageId,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: newrows
		}
	};
	ajax({
		url: url.inheritUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					let alldata = props.editTable.getAllRows(tableId);
					let changedata = data.data[tableId].rows;
					for (let realdata of alldata) {
						for (let valuedata of changedata) {
							let changeval = realdata.values;
							let copval = valuedata.values;
							if (changeval.pk_card.value === copval.pk_card.value) {
								changeval.monthworkloan.value = copval.monthworkloan.value;
								changeval.accuworkloan.value = copval.accuworkloan.value;
								changeval.remainingworkloan.value = copval.remainingworkloan.value;
							}
						}
					}
					data.data[tableId].rows = alldata;
					props.editTable.setTableData(tableId, data.data[tableId]);
					if (data.result === '1') {
						toast({
							content: getMultiLangByID('201204008A-000008' /*国际化处理：剩余工作量不能小于0!*/),
							color: 'warning'
						});
					}
				} else {
					props.editTable.setTableData(tableId, { rows: [] });
				}
			}
		}
	});
};
