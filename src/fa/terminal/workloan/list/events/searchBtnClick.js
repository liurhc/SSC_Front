import { ajax, toast } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;
//点击查询，获取查询区数据

import { pageConfig } from '../const';
const { editBtns, browseBtns, searchId, url, tableId, middleBtns } = pageConfig;
export default function clickSearchBtn(props, flag) {
	let pk_org = props.search.getSearchValByField(searchId, 'pk_org');
	let orgVal = pk_org.value.firstvalue;
	if (!orgVal) {
		toast({ content: getMultiLangByID('201204008A-000009' /*国际化处理：请选择组织！*/), color: 'warning' });
		return;
	}
	let pk_accbook = props.search.getSearchValByField(searchId, 'pk_accbook');
	//先写上判断，正常情况都会有值
	let bookVal = null,
		accyearVal = null,
		periodVal = null;
	if (pk_accbook) {
		bookVal = pk_accbook.value.firstvalue;
	}
	if (!bookVal) {
		toast({ content: getMultiLangByID('201204008A-000022' /*国际化处理：请选择账簿！*/), color: 'warning' });
		return;
	}

	let accyear = props.search.getSearchValByField(searchId, 'accyear');
	if (accyear) {
		accyearVal = accyear.value.firstvalue;
	}

	let period = props.search.getSearchValByField(searchId, 'period');
	if (period) {
		periodVal = period.value.firstvalue;
	}

	let data = {
		pk_org: orgVal,
		pk_accbook: bookVal,
		accyear: accyearVal,
		period: periodVal
	};
	queryData.call(this, props, data, flag);
}
export function queryData(props, data, flag, isRefresh = false) {
	if (data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period']) {
		ajax({
			url: url.queryUrl,
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						let num = data.data[tableId].rows.length;
						props.editTable.setTableData(tableId, data.data[tableId]);
						if (isRefresh) {
							showMessage.call(this, props, {
								type: MsgConst.Type.RefreshSuccess
							}); /* 国际化处理：'刷新成功'*/
						} else {
							showMessage.call(this, props, {
								color: 'success',
								content: getMultiLangByID('msgUtils-000028', { num }) /* 国际化处理： 查询成功，共 {num} 条*/
							});
						}

						this.setState({ allworkloan: data.allworkloan });
						this.setState({ premonthworkloan: data.premonthworkloan });
						this.setState({ monthworkloan: data.monthworkloan });
						this.setState({ accuworkloan: data.accuworkloan });
						this.setState({ remainingworkloan: data.remainingworkloan });
						this.setState({ cardNum: data.numtotal });
						this.setState({ isFipMakeVoucher: data.isFipMakeVoucher });
						this.setState({ isMinUnCloseBookPeriod: data.isMinUnCloseBookPeriod });
						this.setState({ pageStatus: 'ok' });
						props.editTable.setStatus(tableId, 'browse');
						props.button.setButtonVisible(editBtns, false);
						props.button.setButtonVisible(browseBtns, true);
					} else {
						props.editTable.setTableData(tableId, { rows: [] });
						this.setState({ pageStatus: 'none' });
						if (isRefresh) {
							showMessage.call(this, props, {
								type: MsgConst.Type.RefreshSuccess
							}); /* 国际化处理：'刷新成功'*/
						} else {
							showMessage.call(this, props, { type: MsgConst.Type.QueryNoData });
						}
					}
					if (flag == true) {
						let allrow = props.editTable.getAllRows(tableId);
						if (allrow.length == 0) {
							toast({
								content: getMultiLangByID('201204008A-000010' /*国际化处理：当前无数据！*/),
								color: 'warning'
							});
						} else {
							props.search.setDisabled(searchId, true);
							props.button.setButtonVisible(editBtns, true);
							props.button.setButtonVisible(browseBtns, false);
							props.button.setButtonDisabled([ 'InheritWorkloan' ], true);
							props.button.setButtonVisible(middleBtns, true);
							this.setState({ hideBtnAreaFlag: true });
							this.firstEditValue = allrow;
							props.editTable.setStatus(tableId, 'edit');
							this.setState({ pageStatus: 'none' });
						}
					}
				}
			}
		});
	}
}
