import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { tableId, url, pageId } = pageConfig;
export default function afterEvent(props, moduleId, key, value, changedrows, index, record) {
	if (key === 'monthworkloan') {
		let oldVal = null;
		let accuworkloan = null;
		let allworkloan = null;
		//取出最开始的值
		let tableVal = props.editTable.getCacheDataById(tableId).rows;
		let orginVal = tableVal[index];
		oldVal = orginVal.values.monthworkloan.value;
		accuworkloan = orginVal.values.accuworkloan.value;
		allworkloan = orginVal.values.allworkloan.value;
		let remainingworkloanVal = orginVal.values.remainingworkloan.value;
		let newVal = changedrows[0].newvalue.value;
		let accuworkloanVal = null;
		if (value < 0) {
			toast({ content: getMultiLangByID('201204008A-000011' /*国际化处理：输入的值不能小于0*/), color: 'warning' });
			setTimeout(() => {
				props.editTable.setValByKeyAndIndex(tableId, index, 'monthworkloan', {
					value: oldVal,
					display: oldVal
				});
				props.editTable.setValByKeyAndIndex(tableId, index, 'accuworkloan', {
					value: accuworkloan,
					display: accuworkloan
				});
				props.editTable.setValByKeyAndIndex(tableId, index, 'remainingworkloan', {
					value: remainingworkloanVal,
					display: remainingworkloanVal
				});
			}, 0);
			return;
		}
		if (oldVal) {
			let monthworkloan = Number(newVal - oldVal);
			accuworkloanVal = monthworkloan + Number(accuworkloan);
		} else {
			accuworkloanVal = Number(value) + Number(accuworkloan);
		}
		let remainingworkloan = allworkloan - accuworkloanVal;
		if (remainingworkloan < 0) {
			setTimeout(() => {
				props.editTable.setValByKeyAndIndex(tableId, index, 'accuworkloan', {
					value: accuworkloan,
					display: accuworkloan
				});
				props.editTable.setValByKeyAndIndex(tableId, index, 'remainingworkloan', {
					value: remainingworkloanVal,
					display: remainingworkloanVal
				});
				props.editTable.setValByKeyAndIndex(tableId, index, 'monthworkloan', {
					value: oldVal,
					display: oldVal
				});
			}, 0);
			toast({ content: getMultiLangByID('201204008A-000012' /*国际化处理：输入的值不能小于0*/), color: 'warning' });
			return;
		}

		props.editTable.setValByKeyAndIndex(tableId, index, 'accuworkloan', {
			value: accuworkloanVal,
			display: accuworkloanVal,
			scale: 2
		});
		props.editTable.setValByKeyAndIndex(tableId, index, 'remainingworkloan', {
			value: remainingworkloan,
			display: remainingworkloan,
			scale: 2
		});

		let realRow = [];
		let changerowval = record.values.asset_code.value;
		let arterChangeRows = props.editTable.getChangedRows(tableId);
		for (let dataValue of arterChangeRows) {
			if (dataValue.values.asset_code.value === changerowval) {
				realRow.push(dataValue);
				break;
			}
		}
		const data = {
			pageid: pageId,
			model: {
				areaType: 'table',
				areacode: tableId,
				rows: realRow
			}
		};
		ajax({
			url: url.scaleUrl,
			async: false,
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						let rowval = data[tableId].rows[0];

						props.editTable.setValByKeyAndIndex(tableId, index, 'accuworkloan', {
							value: rowval.values.accuworkloan.value,
							display: rowval.values.accuworkloan.value
						});
						props.editTable.setValByKeyAndIndex(tableId, index, 'remainingworkloan', {
							value: rowval.values.remainingworkloan.value,
							display: rowval.values.remainingworkloan.value
						});
						props.editTable.setValByKeyAndIndex(tableId, index, 'monthworkloan', {
							value: rowval.values.monthworkloan.value,
							display: rowval.values.monthworkloan.value
						});
					} else {
						// Promise.resolve(true).then(() => {
						// 	props.editTable.setTableData(tableId, { rows: [] });
						// });
						props.editTable.setTableData(tableId, { rows: [] });
					}
				}
			}
		});
	}
}
