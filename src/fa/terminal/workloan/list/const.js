const pageConfig = {
	appcode: '201204008A',
	// 页面编码
	pageId: '201204008A_list',
	//查询区
	searchId: 'searchArea',

	tableId: 'list_head',
	//组织
	pk_org: 'pk_org',
	//账簿
	pk_accbook: 'pk_accbook',
	//会计期间
	accyear: 'accyear',
	editBtns: [ 'Save', 'Cancel', 'InheritWorkloan', 'BathAltert', 'Import', 'Export' ],
	browseBtns: [ 'Edit', 'Export', 'Filter', 'printgroup', 'Print', 'Refresh' ],
	middleBtns: [ 'Export' ],
	url: {
		orgchangeUrl: '/nccloud/fa/workloan/orgchange.do',
		printUrl: '/nccloud/fa/workloan/printList.do',
		saveUrl: '/nccloud/fa/workloan/save.do',
		ImportUrl: '/nccloud/fa/workloan/import.do',
		batchAltertUrl: '/nccloud/fa/workloan/batchAltert.do',
		exportUrl: '/nccloud/fa/workloan/export.do',
		inheritUrl: '/nccloud/fa/workloan/inherit.do',
		queryUrl: '/nccloud/fa/workloan/query.do',
		scaleUrl: '/nccloud/fa/workloan/scale.do',
		editenable: '/nccloud/fa/workloan/editenable.do'
		//unCommitUrl: '/nccloud/aum/sale/unCommit.do'
	}
};

export { pageConfig };
