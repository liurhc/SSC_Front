import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
import {
	initTemplate,
	buttonClick,
	onSureModal,
	onAfterEvent,
	onSearchBtn,
	onDepreciation,
	onTabChange,
	onPaginationChange,
	onSelectChange,
	onEditTableAfterEvent,
	onEditTableBeforeEvent,
	modalContent,
	onShowedHerit,
	cancelBtnHerit,
	searchClearEve,
	onOpentoWorkloan,
	onContinue
} from './events';

import { Undepreciation, Allot } from './component';
import './index.less';
import { pageConfig } from './const';
const { NCAffix, NCButton } = base;
const { searchArea, filterArea, pagecode, detailArea } = pageConfig;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, closeBrowserUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;
import fa from 'fa';
const { fa_components } = fa;
const { WorkloadModal } = fa_components;

class Terminal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pageStatus: 'undepreciation', //页面
			hideBtnAreaFlag: false, //查询区域按钮隐藏
			btnStatus: 'show', //按钮组
			modalShow: false, //模态框
			summsg: {}, //折旧总额等信息
			alertTxt: '', //提示信息
			pageinfo: {}, //折旧清单分页数据
			// worklodaModal: false, //弹框
			modalId: 'FilterModal', //弹框ID
			newInput: 0, //批改模态框里的值
			proportion: 0, // 精度调整中录入的值
			tab: 'depList' //折旧清单页签默认激活页签
		};
		(this.years = {}), // 可参照的会计年
			(this.periods = {}), //所有会计年对应的会计期间
			(this.enddate = {}), //所有期间的最后一天，作为凭证日期
			(this.pks = []);
		this.allpks = [];
		this.yearPeriod = {};
		this.editTableIndex = 0;
		this.isTabChanged = true; //是否允许页签切换标志
		this.tabSelect = detailArea;
		this.modalContent = modalContent.bind(this);
		this.onSureModal = onSureModal.bind(this);
		this.onShowedHerit = onShowedHerit.bind(this);
		this.cancelBtnHerit = cancelBtnHerit.bind(this);
		this.onOpentoWorkloan = onOpentoWorkloan.bind(this);
		this.onContinue = onContinue.bind(this);
		this.btnDisabled = true; //计提折旧 按钮禁用状态
		this.firstEditValue = {}; // 用于取消处理，记录修改前的数据
		closeBrowserUtils.call(this, props, { editTable: [ detailArea ] });
		initTemplate.call(this, props);
	}

	//重新计提  弹框
	replenishModal = () => {
		return (
			<div>
				<p>
					<span className="iconfont icon-warning" />
					{getMultiLangByID('201204004A-000010')}
				</p>{' '}
				{/**本月有需要录入工作量的卡片，请选择如下操作*/}
				<span className="cut-off-rule" />
				<div>
					<NCButton size="lg" colors="primary" onClick={this.onOpentoWorkloan}>
						{' '}
						{getMultiLangByID('201204004A-000012')} {/** 录入工作量 */}
					</NCButton>
					<NCButton colors="info" onClick={this.onContinue}>
						{' '}
						{getMultiLangByID('201204004A-000013')} {/** 继续折旧 */}
					</NCButton>
					<NCButton colors="info" onClick={() => this.props.modal.close('replenish-area')}>
						{' '}
						{getMultiLangByID('msgUtils-000002')} {/** 取消 */}
					</NCButton>
				</div>
			</div>
		);
	};

	render() {
		let { button, search, form, modal } = this.props;
		let { createButtonApp } = button;
		let { NCCreateSearch } = search;
		let { createForm } = form;
		let { pageStatus, btnStatus, alertTxt, modalId, hideBtnAreaFlag } = this.state;
		let { createModal } = modal;
		let config = {
			// workedModal:false,
			modalId: 'worked', //弹框的ID
			defaultValue: {
				//默认值
				total: 1.0, //总工作量
				accumulation: 1.0, //累计工作量
				unit: getMultiLangByID('201204004A-000040') //工作量单位  '天'
			},
			// onModalShow:()=>this.
			onSureClick: () => {}
		};
		return (
			<div className="nc-bill-list" style={{ background: '#fff' }}>
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID('201204004A-000045')}</h2> {/*折旧与摊销*/}
						</div>
						{btnStatus === 'show' && (
							<div className="header-button-area">
								{createButtonApp({
									area: 'list_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
						)}
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchArea, {
						showAdvBtn: false,
						onAfterEvent: onAfterEvent.bind(this),
						clickSearchBtn: onSearchBtn.bind(this, false),
						hideBtnArea: hideBtnAreaFlag,
						advSearchClearEve: searchClearEve.bind(this) // 查询条件清空处理
					})}
				</div>

				<div className="nc-bill-table-area">
					{pageStatus === 'undepreciation' && (
						<Undepreciation
							pageStatus={pageStatus}
							onDepreciation={onDepreciation.bind(this)}
							alertTxt={alertTxt}
							btnDisabled={this.btnDisabled}
						/>
					)}
				</div>
				<div className="nc-bill-table-area">
					{pageStatus === 'allot' && (
						<Allot
							{...this.props}
							onTabChange={onTabChange.bind(this)}
							onPaginationChange={onPaginationChange.bind(this)}
							onSelectChange={onSelectChange.bind(this)}
							onEditTableAfterEvent={onEditTableAfterEvent.bind(this)}
							onEditTableBeforeEvent={onEditTableBeforeEvent.bind(this)}
							{...this.state}
						/>
					)}
				</div>
				{createModal('FilterModal', {
					title: getMultiLangByID('201204004A-000041'), // 弹框表头信息  '过滤'
					// content: createForm(filterArea, {}),
					content: this.modalContent(modalId),
					beSureBtnClick: this.onSureModal, //点击确定按钮事件
					size: modalId === 'FilterModal' ? 'lg' : 'sm'
				})}
				{createModal('RateModify', {
					title: getMultiLangByID('201204004A-000042'), // 弹框表头信息  '按比例调整月折旧额'
					content: this.modalContent('RateModify'),
					beSureBtnClick: this.onSureModal, //点击确定按钮事件
					size: 'sm'
				})}
				{createModal('isShowedHerit', {
					title: getMultiLangByID('201204004A-000043'), // 弹框表头信息 '提示'
					// content: createForm(filterArea, {}),
					content: getMultiLangByID('201204004A-000044'), //'月折旧额已手工更改，以后各月计提折旧时是否继承该值？'
					beSureBtnClick: this.onShowedHerit, //点击确定按钮事件
					cancelBtnClick: this.cancelBtnHerit, //点击取消按钮事件
					className: 'junior'
				})}
				{createModal('replenish-area', {
					title: getMultiLangByID('201204004A-000043') /**国际化处理：提示*/,
					content: this.replenishModal(),
					className: 'junior',
					noFooter: true
				})}
				<WorkloadModal {...this.props} config={config} />
			</div>
		);
	}
}
Terminal = createPage({
	billinfo: {
		billtype: 'list',
		pagecode: pagecode,
		headcode: filterArea,
		bodycode: detailArea
	}
})(Terminal);
// 加载多语文件
initMultiLangByModule({ fa: [ '201204004A', 'facommon' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Terminal />, document.querySelector('#app'));
});
