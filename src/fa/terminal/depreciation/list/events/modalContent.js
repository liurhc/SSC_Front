/**
 * 模态框 内容
 */
import { pageConfig } from '../const';
import { base } from 'nc-lightapp-front';
const { NCFormControl } = base;
const { searchArea, filterArea } = pageConfig;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export default function(modalId) {
	let { button, search, form, modal } = this.props;
	let { createButtonApp } = button;
	let { NCCreateSearch } = search;
	let { createForm } = form;
	let jsx = '';
	switch (modalId) {
		case 'FilterModal':
			jsx = createForm(filterArea, {});
			this.props.form.setFormStatus(filterArea, 'edit');
			break;
		case 'batch':
			jsx = (
				<div>
					<p>{getMultiLangByID('201204004A-000026')}</p> {/** 请输入批量修改值： */}
					<NCFormControl
						className="demo-input"
						type="number"
						min="0.00"
						step="0.01"
						size="sm"
						value={this.state.newInputold}
						onChange={(e) => {
							if (e.split('.')[1] && e.split('.')[1].length > 2) {
								return;
							}
							this.setState({ newInputold: e });
						}}
					/>
				</div>
			);
			break;
		case 'RateModify':
			jsx = (
				<div>
					<p>{getMultiLangByID('201204004A-000027')}</p> {/*请输入比例（%）：*/}
					<NCFormControl
						className="demo-input"
						type="number"
						min="0.00"
						step="0.01"
						size="sm"
						value={this.state.proportion}
						onChange={(value) => {
							if (value.split('.')[1] && value.split('.')[1].length > 2) {
								return;
							}
							this.setState({ proportion: value });
						}}
					/>
				</div>
			);
			break;
		default:
			break;
	}
	return jsx;
}
