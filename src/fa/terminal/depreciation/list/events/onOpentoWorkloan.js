import { pageConfig } from '../const';
const { detailArea, searchArea, pagecode, workAppcode, workPagecode } = pageConfig;

export default function onOpentoWorkloan() {
	this.props.modal.close('replenish-area');
	let searchData = this.props.search.getAllSearchData(searchArea);
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}
	let param = {
		pk_org: data['pk_org'],
		pk_org_name: this.props.search.getSearchValByField(searchArea, 'pk_org').display,
		pk_accbook: data['pk_accbook'],
		pk_accbook_name: this.props.search.getSearchValByField(searchArea, 'pk_accbook').display,
		accyear: data['accyear'],
		period: data['period']
	};

	this.props.openTo('/fa/terminal/workloan/list/index.html', {
		...param,
		pagecode: workPagecode,
		appcode: workAppcode
	});
}
