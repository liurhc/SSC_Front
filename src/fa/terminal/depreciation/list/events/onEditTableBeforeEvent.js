/**
 * 折旧清单   编辑态  编辑前事件
 */
import { ajax, toast } from 'nc-lightapp-front';

export default function(props, moduleId, item, index, value, record) {
        if(record.values.asset_state.value != 'exist'){
                return false;
        } else {
                return true;
        }
}