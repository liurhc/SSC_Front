import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { detailArea, searchArea, url } = pageConfig;
export default function onPaginationChange(eventKey) {
	let pageIndex = this.state.pageinfo.pageIndex ? this.state.pageinfo.pageIndex : 1;
	if (eventKey == pageIndex) {
		return;
	}
	// 表体行无数据，只切换分页
	let editDate = this.props.editTable.getAllData(detailArea);
	if (editDate.rows.length == 0) {
		return;
	}

	let searchData = this.props.search.getAllSearchData(searchArea);
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}

	let datapage = {
		pk_cards: this.pks[eventKey - 1],
		pk_org: data['pk_org'],
		pk_accbook: data['pk_accbook']
	};
	let _this = this;
	ajax({
		url: url.depPageChange,
		data: datapage,
		success: function(res) {
			if (res.data) {
				_this.props.editTable.setTableData(detailArea, res.data.detailArea);
				let pageIndex = eventKey;
				_this.firstEditValue = JSON.parse(JSON.stringify(res.data.detailArea));
				_this.setState({ pageinfo: Object.assign({}, _this.state.pageinfo, { pageIndex }) });
			}
		}
	});
}
