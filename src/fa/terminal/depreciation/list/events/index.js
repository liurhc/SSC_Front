import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import onSureModal from './onSureModal';
import onAfterEvent from './onAfterEvent';
import onSearchBtn from './onSearchBtn';
import onDepreciation from './onDepreciation';
import onContinue from './onContinue';
import onTabChange from './onTabChange';
import onPaginationChange from './onPaginationChange';
import onSelectChange from './onSelectChange';
import onEditTableAfterEvent from './onEditTableAfterEvent';
import onEditTableBeforeEvent from './onEditTableBeforeEvent';
import onEditTableData from './onEditTableData';
import modalContent from './modalContent';
import onShowedHerit from './onShowedHerit';
import cancelBtnHerit from './cancelBtnHerit';
import onOpentoWorkloan from './onOpentoWorkloan';
import searchClearEve from './searchClearEve';

export {
	initTemplate,
	buttonClick,
	onSureModal,
	onAfterEvent,
	onSearchBtn,
	onDepreciation,
	onContinue,
	onTabChange,
	onOpentoWorkloan,
	onPaginationChange,
	onSelectChange,
	onEditTableAfterEvent,
	onEditTableBeforeEvent,
	onEditTableData,
	modalContent,
	onShowedHerit,
	cancelBtnHerit,
	searchClearEve
};
