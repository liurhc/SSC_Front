/**
 * 是否继承当月折旧弹框处理,取消
 */
import { pageConfig } from '../const';
const { searchArea, detailArea } = pageConfig;

export default function cancelBtnHerit() {
	// 如果为取消，则将是否继承标识修改为否
	this.props.editTable.setValByKeyAndIndex(detailArea, this.editTableIndex, 'herit_flag', {
		value: false,
		display: false
	});
}
