import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { searchArea, depgatherArea, detailArea, url } = pageConfig;

export default function onTabChange(val) {
	let btnDetailArr = [ 'Edit', 'Filter', 'Dep', 'MakeVoucher', 'FABillQueryAboutVoucher', 'Print', 'Refresh' ];
	let btnGatehrArr = [ 'GatherChose', 'MakeVoucher', 'FABillQueryAboutVoucher', 'Print', 'Refresh' ];
	//当是修改状态的时候，禁止切换页签
	if (this.isTabChanged === false) {
		toast({ content: getMultiLangByID('201204004A-000038'), color: 'warning' }); /** '折旧汇总页签不支持修改，请勿切换页签' */
		return;
	}
	if (val === 'depList') {
		// 打印用状态
		this.tabSelect = detailArea;
		// 切换按钮组
		this.props.button.setButtonVisible(btnGatehrArr, false);
		this.props.button.setButtonVisible(btnDetailArr, true);

		let searchData = this.props.search.getAllSearchData(searchArea);
		let data = {};
		if (searchData.conditions) {
			searchData.conditions.map((item) => {
				data[item.field] = item.value.firstvalue;
			});
		}
		this.setState({ tab: val });
	}

	if (val === 'depAllot') {
		this.tabSelect = depgatherArea;
		this.setState({ tab: val });
		let searchData = this.props.search.getAllSearchData(searchArea);
		let data = {};
		if (searchData.conditions) {
			searchData.conditions.map((item) => {
				data[item.field] = item.value.firstvalue;
			});
		}
		// let len = this.props.editTable.getAllData(depgatherArea).rows.length;
		if (data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period']) {
			ajax({
				url: url.depReadGather,
				data,
				success: (res) => {
					if (res.data) {
						let meta = this.props.meta.getMeta();
						meta.depgatherArea.items.length = 0;
						meta.depgatherArea.items = res.data.itemjson;
						this.props.meta.setMeta(meta);
						this.props.table.setAllTableData(depgatherArea, res.data.gathervos.depgatherArea);
						// this.props.editTable.setTableData(depgatherArea, res.data.gathervos.depgatherArea.rows);
					} else {
						this.props.table.setAllTableData(depgatherArea, { rows: [] });
					}
				}
			});
		}
		// 切换按钮组
		this.props.button.setButtonVisible(btnDetailArr, false);
		this.props.button.setButtonVisible(btnGatehrArr, true);
	}
}
