/**
 * 是否继承当月折旧弹框处理
 */
import { pageConfig } from '../const';
const { searchArea, detailArea } = pageConfig;

export default function onShowHerit() {
	// 如果为确认，则将是否继承标识修改为是
	this.props.editTable.setValByKeyAndIndex(detailArea, this.editTableIndex, 'herit_flag', {
		value: true,
		display: true
	});
}
