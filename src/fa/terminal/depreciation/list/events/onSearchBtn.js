import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import onTabChange from './onTabChange';
const { searchArea, detailArea, depgatherArea, url } = pageConfig;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;

export default function onSearchBtn(isrefresh) {
	let searchData = this.props.search.getAllSearchData(searchArea);
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}
	let tabSelect = this.tabSelect;
	if (tabSelect === detailArea) {
		queryDepMsg.call(this, data, isrefresh);
	} else {
		onTabChange.call(this, 'depList');
		queryDepMsg.call(this, data, isrefresh);
	}
}

export function queryDepMsg(data, isrefresh) {
	let size = this.state.pageinfo ? this.state.pageinfo.pageSize : '100';
	let _this = this;
	data['pageSize'] = size;
	if (data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period']) {
		ajax({
			url: url.depReadDetail,
			data,
			success: (res) => {
				if (res.data) {
					// 若大于最小未结账月，则清空所有按钮，并要求重新选择期间
					if (res.data.isminunclose) {
						let allButtons = _this.props.button.getButtons();
						_this.props.button.setButtonVisible(allButtons, false);
						_this.setState({
							pageStatus: 'undepreciation',
							alertTxt: getMultiLangByID('201204004A-000032')
						}); /** '所选期间大于最小未结账月，请重新选择期间' */
						_this.btnDisabled = true; //设置  计提折旧 按钮禁用状态
						return;
					}
					_this.setState({ pageStatus: 'allot' });
					let btnShowArr = [
						'Edit',
						'Filter',
						'Dep',
						'MakeVoucher',
						'FABillQueryAboutVoucher',
						'Print',
						'Refresh'
					];
					let btnHideArr = [ 'Save', 'Cancle', 'BatchAlter', 'RateModify' ];
					// 设置按钮可见性
					_this.props.button.setButtonVisible(btnShowArr, true);
					// 设置汇总信息
					if (res.data.summsg) {
						_this.setState({
							summsg: Object.assign({}, res.data.summsg)
						});
					} else {
						_this.setState({
							summsg: {}
						});
					}
					// 设置主键信息
					if (res.data.groupkey) {
						_this.pks = [ ...res.data.groupkey ];
						_this.allpks = [ ...res.data.allpks ];
					} else {
						_this.pks = [];
						_this.allpks = [];
					}
					// 设置分页信息
					if (res.data.pageinfo) {
						_this.setState({ pageinfo: Object.assign({}, res.data.pageinfo) });
					} else {
						_this.setState({ pageinfo: {} });
					}
					// 设置明细页签数据
					if (JSON.stringify(res.data.data) !== '[]') {
						_this.props.editTable.setTableData(detailArea, res.data.data.detailArea);
						let meta = _this.props.meta.getMeta();
						meta.detailArea.items = meta.detailArea.items.map((item) => {
							if (item['attrcode'] === 'depamount' || item['attrcode'] === 'herit_flag') {
								return Object.assign({}, item, { disabled: false });
							} else {
								return Object.assign({}, item, { disabled: true });
							}
						});
						_this.props.meta.setMeta(meta);
					} else {
						_this.props.editTable.setTableData(detailArea, { rows: [] });
					}
					if (isrefresh) {
						showMessage.call(this, _this.props, { type: MsgConst.Type.RefreshSuccess }); /* 国际化处理：'刷新成功'*/
					} else {
						toast({ color: 'success', title: getMultiLangByID('201204004A-000033') }); /** '折旧信息查询完成' */
					}
				}
			}
		});
	} else {
		toast({ content: getMultiLangByID('201204004A-000034'), color: 'warning' }); /** '请选择需要查询的组织账簿及期间信息！' */
		return;
	}
}
