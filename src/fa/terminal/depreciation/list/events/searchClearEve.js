import { pageConfig } from '../const';
const { searchArea } = pageConfig;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/**
 * 数据清空时，设置所有参照及期间不可编辑
 */
export default function searchClearEve() {
	// 如果组织为空，清空账簿，同时清空会计年、期间及凭证日期
	this.props.search.setSearchValByField(searchArea, 'pk_accbook', { value: null, display: null });
	this.props.search.setSearchValByField(searchArea, 'accyear', { value: '', display: '' });
	this.props.search.setSearchValByField(searchArea, 'period', { value: '', display: '' });
	this.props.search.setSearchValByField(searchArea, 'voucher_date', { value: '', display: '' });
	// 设置查询区域可编辑性
	this.props.search.setDisabledByField(searchArea, 'pk_accbook', true);
	this.props.search.setDisabledByField(searchArea, 'accyear', true);
	this.props.search.setDisabledByField(searchArea, 'period', true);
	this.props.search.setDisabledByField(searchArea, 'voucher_date', true);
	// 设置按钮显隐性
	this.btnDisabled = true;

	// 设置所有按钮隐藏
	let btnArr = [
		'Edit',
		'Filter',
		'Dep',
		'MakeVoucher',
		'FABillQueryAboutVoucher',
		'Print',
		'Refresh',
		'GatherChose',
		'MakeVoucher',
		'FABillQueryAboutVoucher',
		'Print',
		'Refresh'
	];
	this.props.button.setButtonVisible(btnArr, false);

	// 设置界面为选择财务组织界面
	this.setState({ pageStatus: 'undepreciation', alertTxt: getMultiLangByID('201204004A-000015') });
	{
		/** '请选择财务组织和会计期间'  */
	}
}
