import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import onTabChange from './onTabChange';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { searchArea, detailArea, url } = pageConfig;
export default function onContinus() {
	let searchData = this.props.search.getAllSearchData(searchArea);
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}
	let tabSelect = this.tabSelect;
	let size = this.state.pageinfo ? this.state.pageinfo.pageSize : '100';
	data['pageSize'] = size;
	let _this = this;
	if (data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period']) {
		ajax({
			url: url.depDirectUrl,
			data,
			success: function(res) {
				_this.props.modal.close('replenish-area');
				if (res.message) {
					toast({ duration: 'infinity', duration: 'infinity', content: res.message, color: 'warning' });
					return;
				}
				if (res.data && res.data.error && res.data.error !== '') {
					toast({ duration: 'infinity', content: res.data.error, color: 'warning' });
					return;
				}
				if (res.data) {
					let rows = res.data.data.detailArea.rows;
					_this.props.editTable.setTableData(detailArea, { rows });
					// 设置汇总信息
					if (res.data.summsg) {
						_this.setState({
							summsg: Object.assign({}, res.data.summsg)
						});
					}
					// 设置主键信息
					if (res.data.groupkey) {
						_this.pks = [ ...res.data.groupkey ];
						_this.allpks = [ ...res.data.allpks ];
					}
					// 设置分页信息
					if (res.data.pageinfo) {
						_this.setState({ pageinfo: Object.assign({}, res.data.pageinfo) });
					}

					_this.setState({ pageStatus: 'allot' });
					// 停留在汇总页签时，组织切换后，重新计提切回明细页签
					if (tabSelect != detailArea) {
						onTabChange.call(_this, 'depList');
					}

					// 设置明细页签数据
					if (JSON.stringify(res.data.data) !== '[]') {
						_this.props.editTable.setTableData(detailArea, res.data.data.detailArea);
						let meta = _this.props.meta.getMeta();
						meta.detailArea.items = meta.detailArea.items.map((item) => {
							if (item['attrcode'] === 'depamount' || item['attrcode'] === 'herit_flag') {
								return Object.assign({}, item, { disabled: false });
							} else {
								return Object.assign({}, item, { disabled: true });
							}
						});
						_this.props.meta.setMeta(meta);
					}
					// 重新设置按钮可见性，否则第一次会导致没有按钮
					let btnShowArr = [
						'Edit',
						'Filter',
						'Dep',
						'MakeVoucher',
						'FABillQueryAboutVoucher',
						'Print',
						'Refresh'
					];

					_this.props.button.setButtonVisible(btnShowArr, true);
					// 设置汇总信息
					toast({ color: 'success', title: getMultiLangByID('201204004A-000028') }); /** '计提完成' */
				}
			}
		});
	}
}
