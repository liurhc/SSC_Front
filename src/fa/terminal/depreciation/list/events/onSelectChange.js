import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { searchArea, detailArea, url } = pageConfig;
export default function onSelectChange(value) {
	let totalPage = Math.ceil(this.allpks.length / value);
	this.pks.length = 0;
	this.pks = chunk(this.allpks, value * 1);

	let searchData = this.props.search.getAllSearchData(searchArea);
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}

	let datapage = {
		pk_cards: this.pks[0],
		pk_org: data['pk_org'],
		pk_accbook: data['pk_accbook']
	};

	// 重新设置分页信息
	let pageinfo_new = this.state.pageinfo;
	pageinfo_new.totalPage = totalPage;
	pageinfo_new.pageSize = value;
	pageinfo_new.pageIndex = 1;

	this.setState({ pageinfo: pageinfo_new });
	let _this = this;
	ajax({
		url: url.depPageChange,
		data: datapage,
		success: function(res) {
			if (res.data) {
				// 点击修改后，再进行当前页行数调整，调整后取消，未回退到修改前bug修改
				_this.firstEditValue = JSON.parse(JSON.stringify(res.data.detailArea));
				_this.props.editTable.setTableData(detailArea, res.data.detailArea);
			}
		}
	});
}

function chunk(arr, size) {
	let arr2 = [];
	for (let i = 0; i < arr.length; i = i + size) {
		arr2.push(arr.slice(i, i + size));
	}
	return arr2;
}
