/**
 * 查询区  编辑后事件
 */
import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import onSearchBtn from './onSearchBtn';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { searchArea, filterArea, url } = pageConfig;
export default function onAfterEvent(field, val) {
	let btnArr = [
		'Edit',
		'Filter',
		'Dep',
		'MakeVoucher',
		'FABillQueryAboutVoucher',
		'Print',
		'Refresh',
		'GatherChose',
		'MakeVoucher',
		'FABillQueryAboutVoucher',
		'Print',
		'Refresh'
	];
	let searchData = this.props.search.getAllSearchData(searchArea, false);
	// 修改查询条件后，需要清空过滤条件中的信息
	this.props.form.EmptyAllFormValue(filterArea);
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}
	if (JSON.stringify(data) === '{}') {
		return;
	}
	switch (field) {
		case 'pk_org':
			let pk_org = this.props.search.getSearchValByField(searchArea, 'pk_org');
			if (!pk_org || pk_org === '' || pk_org.value.firstvalue === '') {
				// toast({ color: 'danger', content: '请先选择资产账簿' });
				// 则清空表头固定字段值，清空表体值，表体加空行并给出提示
				clearKeyAfterEditEvent.call(this, this.props, searchArea, field);
				this.btnDisabled = true;
				this.setState({ pageStatus: 'undepreciation', alertTxt: getMultiLangByID('201204004A-000015') });
				{
					/** '请选择财务组织和会计期间'  */
				}
				this.props.button.setButtonVisible(btnArr, false);
				return;
			}
			data.accyear = '';
			data.period = '';
			orgChangeHandler.call(this, this.props, data);
			break;
		case 'pk_accbook':
			let pk_accbook = this.props.search.getSearchValByField(searchArea, 'pk_accbook');
			if (!pk_accbook || pk_accbook === '' || pk_accbook.value.firstvalue === '') {
				clearKeyAfterEditEvent.call(this, this.props, searchArea, field);
				this.setState({ pageStatus: 'undepreciation', alertTxt: getMultiLangByID('201204004A-000015') });
				{
					/** '请选择财务组织和会计期间'  */
				}
				return;
			}
			data.period = '';
			data.voucher_date = '';
			let _this = this;
			ajax({
				url: url.depQueryDate,
				data,
				success: (res) => {
					if (res.data) {
						if (res.data.isDepflag) {
							if (res.data.isDepflag['3']) {
								setTimeout(() => {
									onSearchBtn.call(_this, false);
								}, 500);
							} else {
								_this.setState({ alertTxt: res.data.isDepflag['1'] || res.data.isDepflag['2'] });
								_this.setState({ pageStatus: 'undepreciation' });
								_this.btnDisabled = false;
							}
						}
						if (res.data.years) {
							// 将返回的年、期间及凭证日期信息存入state,避免重复查询
							_this.enddate = res.data.endate;
							_this.years = res.data.years;
							_this.periods = res.data.yearperiod;
							// 年份有值则设置期间及凭证日志可编辑
							_this.props.search.setDisabledByField(searchArea, 'accyear', false);
							_this.props.search.setDisabledByField(searchArea, 'period', false);
							_this.props.search.setDisabledByField(searchArea, 'voucher_date', false);
							let selectData = res.data.years.map((year) => {
								return {
									display: year,
									value: year
								};
							});
							let accyear = res.data.mincloseperiod.accyear;
							let periodData = res.data.yearperiod[accyear].map((item) => {
								return {
									display: item,
									value: item
								};
							});
							let meta = _this.props.meta.getMeta();
							meta.searchArea.items.forEach((item) => {
								if (item.attrcode === 'accyear') {
									item.options = selectData;
								}
								if (item.attrcode === 'period') {
									item.options = periodData;
								}
							});
							_this.props.meta.setMeta(meta);
							let path = res.data.mincloseperiod;
							searchAreaSetValue.call(_this, _this.props, searchArea, 'accyear', {
								value: path.accyear,
								display: path.accyear
							});
							searchAreaSetValue.call(_this, _this.props, searchArea, 'period', {
								value: path.period,
								display: path.period
							});
							searchAreaSetValue.call(_this, _this.props, searchArea, 'voucher_date', {
								value: res.data.mincloseperiod.enddate,
								display: res.data.mincloseperiod.enddate
							});
						}
					}
				}
			});
			break;
		case 'accyear':
			let accyear = this.props.search.getSearchValByField(searchArea, 'accyear');
			if (val === '__clear__' || !accyear || accyear.value.firstvalue === '') {
				//年度清空的状态
				this.props.search.setDisabledByField(searchArea, 'voucher_date', true);
				this.props.search.setDisabledByField(searchArea, 'period', true);
				let meta = this.props.meta.getMeta();
				meta.searchArea.items = meta.searchArea.items.map((item) => {
					if (item.attrcode === 'period') {
						return Object.assign({}, item, { options: [] });
					}
					return item;
				});
				this.props.meta.setMeta(meta);
				searchAreaSetValue.call(this, this.props, searchArea, 'accyear', { value: '', display: '' });
				searchAreaSetValue.call(this, this.props, searchArea, 'period', { value: '', display: '' });
				searchAreaSetValue.call(this, this.props, searchArea, 'voucher_date', { value: '', display: '' });
				this.setState({ pageStatus: 'undepreciation' });
			} else {
				//切换年度
				this.props.search.setDisabledByField(searchArea, 'period', false);
				// 重置年度及相应的会计期间选项
				let accyear = this.props.search.getSearchValByField(searchArea, 'accyear');
				let periodData = this.periods[accyear.value.firstvalue].map((item) => {
					return {
						display: item,
						value: item
					};
				});
				let meta = this.props.meta.getMeta();
				meta.searchArea.items.forEach((item) => {
					if (item.attrcode === 'period') {
						item.options = periodData;
					}
				});
				this.props.meta.setMeta(meta);
				// 清空当前组织信息
				searchAreaSetValue.call(this, this.props, searchArea, 'period', { value: '', display: '' });
				searchAreaSetValue.call(this, this.props, searchArea, 'voucher_date', { value: '', display: '' });
			}
			break;
		case 'period':
			let period = this.props.search.getSearchValByField(searchArea, 'period');
			if (val === '__clear__' || !period || period.value.firstvalue === '') {
				//清空期间的状态
				this.props.search.setDisabledByField(searchArea, 'voucher_date', true);
				searchAreaSetValue.call(this, this.props, searchArea, 'voucher_date', { value: '', display: '' });
				this.setState({ pageStatus: 'undepreciation' });
				// 清空后，需要重置备选值，有疑问  todo
				let accyear = this.props.search.getSearchValByField(searchArea, 'accyear');
				let periodData = this.periods[accyear.value.firstvalue].map((item) => {
					return { display: item, value: item };
				});
				let meta = this.props.meta.getMeta();
				meta.searchArea.items = meta.searchArea.items.map((item) => {
					if (item.attrcode === 'period') {
						return Object.assign({}, item, { options: periodData });
					}
					return item;
				});
				this.props.meta.setMeta(meta);
			} else {
				let yearperiodstring =
					this.props.search.getSearchValByField(searchArea, 'accyear').value.firstvalue +
					this.props.search.getSearchValByField(searchArea, 'period').value.firstvalue;
				let vouch_date = this.enddate[yearperiodstring];
				// 重置凭证日期
				searchAreaSetValue.call(this, this.props, searchArea, 'voucher_date', {
					value: vouch_date,
					display: vouch_date
				});
				this.props.search.setDisabledByField(searchArea, 'voucher_date', false);
				onSearchBtn.call(this, false);
			}
			break;
		case 'voucher_date':
			break;
		default:
			break;
	}
}

function clearKeyAfterEditEvent(props, moduleId, key) {
	switch (key) {
		case 'pk_org':
			// 如果组织为空，清空账簿，同时清空会计年、期间及凭证日期
			props.search.setSearchValByField(moduleId, 'pk_accbook', { value: null, display: null });
			props.search.setSearchValByField(moduleId, 'accyear', { value: '', display: '' });
			props.search.setSearchValByField(moduleId, 'period', { value: '', display: '' });
			props.search.setSearchValByField(moduleId, 'voucher_date', { value: '', display: '' });
			// 设置账簿、会计年、期间及凭证日期不能编辑
			props.search.setDisabledByField(moduleId, 'pk_accbook', true);
			props.search.setDisabledByField(moduleId, 'accyear', true);
			props.search.setDisabledByField(moduleId, 'period', true);
			props.search.setDisabledByField(moduleId, 'voucher_date', true);
			break;
		case 'pk_accbook':
			// 清空账簿，清空会计年、期间及凭证日期
			props.search.setSearchValByField(moduleId, 'accyear', { value: '', display: '' });
			props.search.setSearchValByField(moduleId, 'period', { value: '', display: '' });
			props.search.setSearchValByField(moduleId, 'voucher_date', { value: '', display: '' });
			// 设置账簿、会计年、期间及凭证日期不能编辑
			props.search.setDisabledByField(moduleId, 'accyear', true);
			props.search.setDisabledByField(moduleId, 'period', true);
			props.search.setDisabledByField(moduleId, 'voucher_date', true);
			break;
	}
}

/**
 * 组织切换后事件
   * @param {*} props 
   * @param {*} data
 */
export function orgChangeHandler(props, data) {
	let btnArr = [
		'Edit',
		'Filter',
		'Dep',
		'MakeVoucher',
		'FABillQueryAboutVoucher',
		'Print',
		'Refresh',
		'GatherChose',
		'MakeVoucher',
		'FABillQueryAboutVoucher',
		'Print',
		'Refresh'
	];
	// 设置账簿可编辑
	this.props.search.setDisabledByField(searchArea, 'pk_accbook', false);
	let _this = this;
	data.pk_accbook = '';
	data.period = '';
	data.voucher_date = '';
	ajax({
		url: '/nccloud/fa/depreciation/querydate.do',
		data,
		success: (res) => {
			if (res.data) {
				if (res.data.isDepflag) {
					if (res.data.isDepflag['3']) {
						_this.btnDisabled = false;
						setTimeout(() => {
							onSearchBtn.call(this, false);
						}, 500);
					} else {
						_this.setState({ alertTxt: res.data.isDepflag['1'] || res.data.isDepflag['2'] });
						_this.setState({ pageStatus: 'undepreciation' });
						_this.props.button.setButtonVisible(btnArr, false);
						_this.btnDisabled = false;
					}
				}
				if (res.data.pk_accbook) {
					_this.props.search.setSearchValByField(searchArea, 'pk_accbook', res.data.pk_accbook);
				}
				_this.setState({ pageinfo: {} });
				if (res.data.years) {
					// 将返回的年、期间及凭证日期信息存入state,避免重复查询
					_this.enddate = res.data.endate;
					_this.years = res.data.years;
					_this.periods = res.data.yearperiod;
					// 年份有值则设置期间及凭证日志可编辑
					_this.props.search.setDisabledByField(searchArea, 'accyear', false);
					_this.props.search.setDisabledByField(searchArea, 'period', false);
					_this.props.search.setDisabledByField(searchArea, 'voucher_date', false);

					let selectData = res.data.years.map((year) => {
						return {
							display: year,
							value: year
						};
					});
					let minaccyear = res.data.mincloseperiod.accyear;
					let periodData = res.data.yearperiod[minaccyear].map((item) => {
						return {
							display: item,
							value: item
						};
					});
					let meta = _this.props.meta.getMeta();
					meta.searchArea.items.forEach((item) => {
						if (item.attrcode === 'accyear') {
							item.options = selectData;
						}
						if (item.attrcode === 'period') {
							item.options = periodData;
						}
					});
					_this.props.meta.setMeta(meta);
					let path = res.data.mincloseperiod;
					searchAreaSetValue.call(_this, _this.props, searchArea, 'accyear', {
						value: path.accyear,
						display: path.accyear
					});
					searchAreaSetValue.call(_this, _this.props, searchArea, 'period', {
						value: path.period,
						display: path.period
					});
					searchAreaSetValue.call(_this, _this.props, searchArea, 'voucher_date', {
						value: res.data.mincloseperiod.enddate,
						display: res.data.mincloseperiod.enddate
					});
				} else {
					_this.setState({ pageStatus: 'undepreciation', alertTxt: getMultiLangByID('201204004A-000015') });
					{
						/** '请选择财务组织和会计期间'  */
					}
					return;
				}
			} else {
				searchAreaSetValue.call(_this, _this.props, searchArea, 'pk_accbook', {
					value: '',
					display: ''
				});
				searchAreaSetValue.call(_this, _this.props, searchArea, 'accyear', {
					value: '',
					display: ''
				});
				searchAreaSetValue.call(_this, _this.props, searchArea, 'period', {
					value: '',
					display: ''
				});
				searchAreaSetValue.call(_this, _this.props, searchArea, 'voucher_date', {
					value: '',
					display: ''
				});
				_this.props.search.setDisabledByField(searchArea, 'accyear', true);
				_this.props.search.setDisabledByField(searchArea, 'period', true);
				_this.props.search.setDisabledByField(searchArea, 'voucher_date', true);
				_this.setState({ pageStatus: 'undepreciation', alertTxt: getMultiLangByID('201204004A-000015') });
				{
					/** '请选择财务组织和会计期间'  */
				}
				_this.props.button.setButtonVisible(btnArr, false);
			}
		}
	});
}

/**
 *  查询区域赋值
   * @param {*} props 
   * @param {*} data
 */
export function searchAreaSetValue(props, searchArea, item, data) {
	props.search.setSearchValByField(searchArea, item, { value: data.value, display: data.display });
}
