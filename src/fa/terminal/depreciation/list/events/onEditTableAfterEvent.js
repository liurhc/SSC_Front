/**
 * 折旧清单   编辑态  编辑后事件
 */
import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { detailArea } = pageConfig;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
import { onEditTableData } from '../events';
export default function(props, moduleId, key, value, changedrows, index, record) {
	//取出最开始的值
	let tableVal = props.editTable.getCacheDataById(moduleId).rows;
	let orginVal = tableVal[index];
	let oldVal = changedrows[0].oldvalue && changedrows[0].oldvalue.value * 1;
	let newVal = changedrows[0].newvalue && changedrows[0].newvalue.value * 1;
	switch (key) {
		case 'depamount': //月折旧额
			if (oldVal == newVal) {
				return;
			} else if (record.values.asset_state.value == 'reduce') {
				props.editTable.setValByKeyAndIndex(moduleId, index, 'depamount', { value: oldVal });
				toast({
					duration: 'infinity',
					content: getMultiLangByID('201204004A-000030'),
					color: 'warning'
				}); /** '已经减少卡片不允许修改月折旧额' */
				return;
			} else {
				//获取当前行的数据
				//发送请求
				//返回处理结果
				//符合规则 -- 弹框是否继承  并插入这条数据
				//不符合规则 --弹框  异常信息 并恢复编辑前的值
				onEditTableData.call(this, props, changedrows, index, record, 'single');
			}
			break;
		case '': //是否继承
			break;
		default:
			break;
	}
}
