/**
 * 计提折旧 按钮功能
 */
import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { searchArea, btnEditArr, detailArea, url } = pageConfig;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export default function onDepreciation() {
	// this.setState({ pageStatus: 'replenish' });
	let searchData = this.props.search.getAllSearchData(searchArea);
	this.isTabChanged = true; //设置允许切换tab页签
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}
	let size = this.state.pageinfo ? this.state.pageinfo.pageSize : '100';
	data['pageSize'] = size;
	let _this = this;
	if (!data['voucher_date']) {
		toast({ duration: 'infinity', color: 'warning', content: getMultiLangByID('201204004A-000046') });
		return;
	}
	if (data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period'] && data['voucher_date']) {
		ajax({
			url: url.depUrl,
			data,
			success: function(res) {
				if (res.data.error) {
					toast({ duration: 'infinity', content: res.data.error, color: 'warning' });
					return;
				}
				if (res.data === false) {
					_this.props.modal.show('replenish-area');
					return;
				}
				_this.setState({ pageStatus: 'allot' });
				// 计提折旧页面Tab页签在折旧分配汇总时候，更改资产账簿后，重新计提折旧，页面没有发生变化，只有点了折旧清单页签后，页面才刷新
				_this.setState({ tab: 'depList' });
				// 设置按钮可见性
				_this.props.button.setButtonVisible(btnEditArr, true);
				// 设置汇总信息
				if (res.data.summsg && JSON.stringify(res.data.summsg) != '{}') {
					_this.setState({ summsg: Object.assign({}, res.data.summsg) });
				} else {
					_this.setState({
						summsg: {}
					});
				}
				// 设置主键信息
				if (res.data.groupkey) {
					_this.pks = [ ...res.data.groupkey ];
					_this.allpks = [ ...res.data.allpks ];
				} else {
					_this.pks = [];
					_this.allpks = [];
				}
				// 设置分页信息
				if (res.data.pageinfo) {
					_this.setState({ pageinfo: Object.assign({}, res.data.pageinfo) });
				} else {
					_this.setState({ pageinfo: {} });
				}
				// 设置明细页签数据
				if (res.data && res.data.data && JSON.stringify(res.data.data) !== '[]') {
					_this.props.editTable.setTableData(detailArea, res.data.data.detailArea);
					let meta = _this.props.meta.getMeta();
					meta.detailArea.items = meta.detailArea.items.map((item) => {
						if (item['attrcode'] === 'depamount' || item['attrcode'] === 'herit_flag') {
							return Object.assign({}, item, { disabled: false });
						} else {
							return Object.assign({}, item, { disabled: true });
						}
					});
					_this.props.meta.setMeta(meta);
				} else {
					_this.props.editTable.setTableData(detailArea, { rows: [] });
				}

				_this.isTabXChanged = true; //设置允许切换tab页签
				// showMessage.call(_this, props, { type: MsgConst.Type.SaveSuccess });
				toast({ color: 'success', title: getMultiLangByID('201204004A-000028') });
			}
		});
	} else {
		toast({ duration: 'infinity', color: 'warning', content: getMultiLangByID('201204004A-000024') });
		return;
	}
}
