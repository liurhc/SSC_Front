/**
 * 右肩  按钮组   点击事件
 */
import { ajax, toast, print, output, base, promptBox } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { onEditTableData, onSureModal } from '../events';
import onDepreciation from './onDepreciation';
import onSearchBtn from './onSearchBtn';
const {
	appcode,
	detailArea,
	depgatherArea,
	searchArea,
	pagecode,
	filterArea,
	optionAppcode,
	optionPagecode,
	btnEditArr,
	btnSaveArr,
	btnGatherArr,
	billtype,
	printFilename,
	url
} = pageConfig;
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { queryVocherUtils } = components;
const { queryAboutVoucher } = queryVocherUtils;
import fa from 'fa';
const { fa_components } = fa;
const { VoucherUtils } = fa_components;
const { index } = VoucherUtils;

export default function buttonClick(props, id) {
	let searchData = props.search.getAllSearchData(searchArea);
	let data = {};
	if (searchData.conditions) {
		searchData.conditions.map((item) => {
			data[item.field] = item.value.firstvalue;
		});
	}
	let size = this.state.pageinfo ? this.state.pageinfo.pageSize : '100';
	data['pageSize'] = size;

	switch (id) {
		case 'Edit': //修改
			depEdit.call(this, this.props, data);
			break;
		case 'Filter': //过滤
			props.modal.show('FilterModal');
			break;
		case 'Dep': //重新计提
			onDepreciation.call(this);
			break;
		case 'Save': //保存
			depSave.call(this, this.props, data);
			break;
		case 'Cancle': //取消
			promptBox({
				color: 'warning',
				title: getMultiLangByID('msgUtils-000002') /** 取消*/,
				content: getMultiLangByID('msgUtils-000003') /** 确认要取消吗？ */,
				beSureBtnClick: () => {
					depCancle.call(this, props);
				}
			});
			break;
		case 'BatchAlter': //批改
			this.isTabChanged = false; //设置禁止切换tab页签
			// 获取变化的值
			let changeData = this.props.cardTable.getTableItemData(detailArea);
			let editCode = changeData.batchChangeKey;
			// 如果不是月折旧额，不允许批改
			if (editCode != 'depamount') {
				return;
			}
			BatchAltert.call(this, this.props);
			break;
		case 'RateModify': //按比例调整
			this.isTabChanged = false; //设置禁止切换tab页签
			this.setState({ modalId: 'RateModify', newInput: 0 }, () => {
				props.modal.show(this.state.modalId);
			});
			break;
		case 'MakeVoucher': //生成凭证按钮
			depMakeVoucher.call(this, this.props, data);
			break;
		case 'FABillQueryAboutVoucher': //联查凭证按钮
			depQueryVoucher.call(this, this.props, data);
			break;
		case 'Print': //打印
			printTemp.call(this, this.props, data);
			break;
		case 'Output': //输出
			outputTemp.call(this, this.props, data);
			break;
		case 'GatherChose': //分摊项目选择
			gatherChose.call(this, this.props, data);
			break;
		case 'Refresh': //刷新
			onSearchBtn.call(this, true);
			break;
		default:
			break;
	}
}

const BatchAltert = (props) => {
	let allData = props.editTable.getAllData(detailArea);
	let returnData = props.editTable.batchChangeTableData(detailArea);
	let editCode = returnData.code;
	if (editCode != 'depamount') {
		return;
	}
	let afterArry = [ 'depamount' ];

	let search = props.search.getAllSearchData(searchArea);
	let searchData = {};
	if (search.conditions) {
		search.conditions.map((item) => {
			searchData[item.field] = item.value.firstvalue;
		});
	}

	let userjsonstr = {
		pk_org: searchData['pk_org'],
		pk_accbook: searchData['pk_accbook'],
		accyear: searchData['accyear'],
		period: searchData['period'],
		isBatch_flag: true,
		isProportion: false,
		depamount_new: returnData.value,
		voucher_date: searchData['voucher_date']
	};

	let data = {};
	if (afterArry.indexOf(editCode) != -1) {
		data = {
			pageid: pagecode,
			model: allData,
			userjson: JSON.stringify(userjsonstr)
		};
		ajax({
			url: url.depEventHandler,
			data: data,
			success: (res) => {
				if (res.success) {
					if (res.data) {
						if (res.data.data && res.data.data.detailArea) {
							props.editTable.setTableData(detailArea, res.data.data.detailArea);
						}
					}
					if (res.data.error) {
						toast({
							duration: 'infinity',
							color: 'warning',
							content: res.data.error
						});
					}
				}
			}
		});
	}
};

/**
 * 打印
   * @param {*} props 
 */
export function printTemp(props, data) {
	if (!(data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period'])) {
		toast({
			duration: 'infinity',
			color: 'warning',
			content: getMultiLangByID('201204004A-000019')
		}); /** "当前无可打印数据，请录入查询条件！" */
		return;
	}
	let printData = getPrintParam.call(this, props, data, 'print');
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.depPrintUrl, // 后台打印服务url
		printData
	);
}

/**
  * 输出
  * @param {*} props 
  */
export function outputTemp(props, data) {
	if (!(data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period'])) {
		toast({
			duration: 'infinity',
			color: 'warning',
			content: getMultiLangByID('201204004A-000020')
		}); /** "当前无可输出数据，请录入查询条件！" */
		return;
	}
	let printData = getPrintParam.call(this, props, data, 'output');
	output({
		url: url.depPrintUrl,
		data: printData
	});
}

/**
* 打印输出参数
* @param {*} props 
* @param {*} output 
*/
function getPrintParam(props, data, outputType) {
	let rows = [ props.form.getAllFormValue(filterArea).rows[0] ];
	let filterData = {
		pageid: pagecode,
		model: {
			areaType: 'form',
			areaCode: filterArea,
			rows: rows
		}
	};
	let filterdate = JSON.stringify(filterData);
	let userjsonStr = {
		pk_org: data['pk_org'],
		pk_accbook: data['pk_accbook'],
		accyear: data['accyear'],
		period: data['period'],
		voucher_date: data['voucher_date']
	};
	let userjson = JSON.stringify(userjsonStr);
	let nodeKey = 'depdetail';
	if (this.tabSelect === depgatherArea) {
		nodeKey = 'gather';
	}
	let param = {
		filename: printFilename, // 文件名称
		nodekey: nodeKey, // 模板节点标识
		oids: [ filterdate, userjson ] //将过滤条件通过主键字段进行传递+将查询条件以userjson字段进行传递
	};
	if (outputType) {
		param.outputType = outputType;
	}
	return param;
}

/**
* 分摊项目选择
* @param {*} props 
* @param {*} data 
*/
function gatherChose(props, data) {
	// 修改校验
	if (data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period'] && data['voucher_date']) {
		let _this = this;
		ajax({
			url: url.gatherChoseUrl,
			data,
			success: function(res) {
				if (res.data || res.error) {
					_this.isTabChanged = true;
					toast({ duration: 'infinity', content: res.data, color: 'warning' });
					return;
				} else {
					let param = {
						pk_org: data['pk_org'],
						pk_org_name: props.search.getSearchValByField(searchArea, 'pk_org').display,
						pk_accbook: data['pk_accbook'],
						pk_accbook_name: props.search.getSearchValByField(searchArea, 'pk_accbook').display,
						accyear: data['accyear'],
						period: data['period']
					};
					props.openTo('/fa/fabase/option/list/index.html', {
						...param,
						pagecode: optionPagecode,
						appcode: optionAppcode
					});
				}
			}
		});
	}
}

/**
 * 编辑按钮
   * @param {*} props 
 */
export function depEdit(props, data) {
	// 表体行无数据，不允许点击修改
	let editDate = props.editTable.getAllData(detailArea);
	if (editDate.rows.length == 0) {
		toast({
			duration: 'infinity',
			color: 'warning',
			content: getMultiLangByID('201204004A-000021')
		}); /** "当前无可修改数据！"  */
		return;
	}
	if (!(data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period'] && data['voucher_date'])) {
		toast({
			duration: 'infinity',
			color: 'warning',
			content: getMultiLangByID('201204004A-000022')
		}); /** "请输入查询条件！" */
	}

	this.isTabChanged = false; //设置禁止切换tab页签
	// 修改校验
	if (data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period'] && data['voucher_date']) {
		let _this = this;
		ajax({
			url: url.depEditUrl,
			data,
			success: function(res) {
				if (res.data || res.error) {
					_this.isTabChanged = true;
					toast({ duration: 'infinity', content: res.data, color: 'warning' });
					return;
				} else {
					_this.props.search.setDisabled(searchArea, true);
					_this.setState({ hideBtnAreaFlag: true });
					_this.props.button.setButtonVisible(btnSaveArr, true);
					_this.props.button.setButtonVisible(btnEditArr, false);
					_this.props.button.setButtonVisible(btnGatherArr, false);

					let responseData = _this.props.editTable.getAllRows(detailArea, false);
					responseData = responseData.map((data, index) => {
						let { asset_state, depamount, herit_flag } = data.values;
						// 根据当前资产状态判断是否允许修改月折旧额
						if (asset_state.value !== 'exist') {
							_this.props.editTable.setEditableByKey(detailArea, index * 1, depamount, false);
							_this.props.editTable.setEditableByKey(detailArea, index * 1, herit_flag, false);
						}
						return data;
					});
					_this.props.editTable.setTableData(detailArea, { rows: responseData });
					_this.props.editTable.setStatus(detailArea, 'edit');
					// 设置修改前参数信息，用于取消时设置值
					_this.firstEditValue = _this.props.editTable.getAllRows(detailArea);
					_this.setState({});
				}
			}
		});
	}
}

/**
 * 取消按钮
   * @param {*} props 
 */
export function depCancle() {
	// 设置按钮可见性
	this.props.button.setButtonVisible(btnEditArr, true);
	this.props.button.setButtonVisible(btnSaveArr, false);
	this.props.button.setButtonVisible(btnGatherArr, false);
	//设置允许切换tab页签
	this.isTabChanged = true;
	// 重置当前页签数据
	if (this.firstEditValue.rows) {
		this.props.editTable.setTableData(detailArea, { rows: this.firstEditValue.rows });
	} else {
		this.props.editTable.setTableData(detailArea, { rows: this.firstEditValue });
	}
	this.props.editTable.setStatus(detailArea, 'browse');
	this.props.search.setDisabled(searchArea, false);
	this.setState({ hideBtnAreaFlag: false });
	this.setState({ modalId: 'FilterModal' });
}

/**
 * 编辑按钮
   * @param {*} props 
   * @param {*} data
 */
export function depSave(props, data) {
	this.isTabChanged = true; //设置允许切换tab页签
	let userjson = {
		pk_org: data['pk_org'],
		pk_accbook: data['pk_accbook'],
		accyear: data['accyear'],
		period: data['period'],
		voucher_date: data['voucher_date']
	};
	let saveData = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areaCode: detailArea,
			rows: props.editTable.getAllRows(detailArea)
		},
		userjson: JSON.stringify(userjson)
	};
	let newthis = this;
	ajax({
		url: url.depSaveUrl,
		data: saveData,
		success: function(res) {
			if (res.data && res.data.summsg) {
				props.editTable.setStatus(detailArea, 'browse');
				props.button.setButtonVisible(btnEditArr, true);
				props.button.setButtonVisible(btnSaveArr, false);
				props.button.setButtonVisible(btnGatherArr, false);

				newthis.setState({
					summsg: Object.assign({}, res.data.summsg)
				});
				showMessage.call(newthis, props, { type: MsgConst.Type.SaveSuccess });
				newthis.props.search.setDisabled(searchArea, false);
				newthis.setState({ hideBtnAreaFlag: false });
				// 保存成功后，重置过滤弹框
				newthis.setState({ modalId: 'FilterModal' });
			}
		},
		error: (res) => {
			toast({ duration: 'infinity', color: 'warning', content: res.message });
		}
	});
}

/**
 * 生成凭证按钮
   * @param {*} props 
   * @param {*} data
 */
export function depMakeVoucher(props, data) {
	if (data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period'] && data['voucher_date']) {
		let _this = this;
		ajax({
			url: url.depQueryVoucherUrl, // 联查凭证及生成凭证用相同的url
			data,
			success: function(res) {
				if (res.data && res.data.error) {
					toast({ duration: 'infinity', content: res.data.error, color: 'warning' });
					return;
				} else {
					index.call(_this, appcode, _this.props, res.data.pks, billtype, res.data.pk_group, data['pk_org']);
				}
			}
		});
	} else {
		toast({
			duration: 'infinity',
			color: 'warning',
			content: getMultiLangByID('201204004A-000024')
		}); /** '请录入查询条件！' */
		return;
	}
}
/**
 * 联查凭证按钮
   * @param {*} props 
   * @param {*} data
 */
export function depQueryVoucher(props, data) {
	if (data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period'] && data['voucher_date']) {
		let _this = this;
		ajax({
			url: url.depQueryVoucherUrl,
			data,
			success: function(res) {
				if (res.data && res.data.error) {
					toast({ duration: 'infinity', content: res.data.error, color: 'warning' });
					return;
				} else {
					queryAboutVoucher.call(
						_this,
						_this.props,
						detailArea,
						'',
						appcode,
						false,
						false,
						true,
						res.data.pks,
						billtype,
						res.data.pk_group,
						data['pk_org']
					);
				}
			}
		});
	} else {
		toast({
			duration: 'infinity',
			color: 'warning',
			content: getMultiLangByID('201204004A-000024')
		}); /** '请录入查询条件！' */
		return;
	}
}
