/**
 * 模态框 确定 按钮  事件
 */
import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { onEditTableData } from '../events';
const { searchArea, detailArea, filterArea, pagecode, url } = pageConfig;

export default function onSureModal() {
	let modalId = this.state.modalId;
	switch (modalId) {
		case 'FilterModal':
			let searchData = this.props.search.getAllSearchData(searchArea);
			let data = {};
			if (searchData.conditions) {
				searchData.conditions.map((item) => {
					data[item.field] = item.value.firstvalue;
				});
			}

			if (!(data['pk_org'] && data['pk_accbook'] && data['accyear'] && data['period'])) {
				toast({ duration: 'infinity', color: 'warning', content: getMultiLangByID('201204004A-000024') });
				return;
			}

			let size = JSON.stringify(this.state.pageinfo) !== '{}' ? this.state.pageinfo.pageSize : '100';
			let userjsonStr = {
				pk_org: data['pk_org'],
				pk_accbook: data['pk_accbook'],
				accyear: data['accyear'],
				period: data['period'],
				pageSize: size,
				voucher_date: data['voucher_date']
			};
			let userjson = JSON.stringify(userjsonStr);
			let rows = [ this.props.form.getAllFormValue(filterArea).rows[0] ];
			let filterData = {
				pageid: pagecode,
				model: {
					areaType: 'form',
					areaCode: filterArea,
					rows: rows
				},
				userjson: userjson
			};
			let _this = this;
			ajax({
				url: url.depFilter,
				data: filterData,
				success: (res) => {
					if (!res.data) {
						// 过滤后返回无数据，则清空页签数据及分页信息
						_this.allpks = null;
						_this.props.editTable.setTableData(detailArea, null);
						_this.setState({ pageinfo: Object.assign({}, null) });
						return;
					}
					// 设置分页主键信息
					if (res.data.groupkey) {
						_this.pks = [ ...res.data.groupkey ];
						_this.allpks = [ ...res.data.allpks ];
					} else {
						_this.pks = [];
						_this.allpks = [];
					}

					// 重置分页数据
					if (res.data.pageinfo) {
						_this.setState({ pageinfo: Object.assign({}, res.data.pageinfo) });
					} else {
						_this.setState({ pageinfo: Object.assign({}, null) });
					}
					// 更新明细页签当前页数据
					_this.props.editTable.setTableData(detailArea, res.data.data.detailArea);

					// 设置月折旧额及是否继承标识可编辑
					let meta = _this.props.meta.getMeta();
					meta.detailArea.items = meta.detailArea.items.map((item) => {
						if (item['attrcode'] === 'depamount' || item['attrcode'] === 'herit_flag') {
							return Object.assign({}, item, { disabled: false });
						} else {
							return Object.assign({}, item, { disabled: true });
						}
					});
					_this.props.meta.setMeta(meta);
				}
			});
			break;
		case 'batch':
			onEditTableData(this.props, this, 'batch');
			break;
		case 'RateModify':
			let proportion = this.state.proportion;
			onEditTableData(this.props, null, null, null, 'scale', proportion);
		default:
			break;
	}
}
