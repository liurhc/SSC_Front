import { ajax, cacheTools, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import onSearchBtn from './onSearchBtn';
import { queryVoucherSrc } from './queryVoucherSrcUtils4HH';
import { orgChangeHandler } from './onAfterEvent';
const { pagecode, appcode, searchArea, filterArea, detailArea, url } = pageConfig;
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext, faQueryAboutUtils } = components;
const { openAssetCardByPk } = faQueryAboutUtils;
const { getContext, loginContextKeys, loginContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

export default function(props) {
	//从地址栏获取相应查询条件
	let linkpk_org = props.getUrlParam('pk_org');
	let linkpk_org_name = props.getUrlParam('pk_org_name');
	let linkpk_accbook = props.getUrlParam('pk_accbook');
	let linkpk_accbook_name = props.getUrlParam('pk_accbook_name');
	let linkaccyear = props.getUrlParam('accyear');
	let linkperiod = props.getUrlParam('period');
	let _this = this;
	props.createUIDom(
		{
			pagecode: pagecode, //页面id
			appcode: appcode
		},
		function(data) {
			if (data) {
				if (data.context) {
					//初始化上下文变量
					loginContext.call(this, data.context);
				}
				if (data.template) {
					let meta = data.template;
					// 添加卡片超链接、参照过滤
					meta = modifierMeta.call(_this, props, meta);
					props.meta.setMeta(meta, () => {
						//凭证联查单据
						let src = props.getUrlParam('scene');
						if (src) {
							//fip代表会计平台
							if ('fip' == src) {
								//执行第2步
								let checkedData = [];
								checkedData = cacheTools.get('checkedData');
								let data = {
									checkedData,
									pagecode
								};
								queryVoucherSrc.call(_this, _this.props, data);
							}
						} else {
							if (
								!(
									!linkpk_org ||
									!linkpk_org_name ||
									!linkpk_org_name ||
									!linkpk_accbook ||
									!linkpk_accbook_name ||
									!linkaccyear ||
									!linkperiod
								)
							) {
								openLink.call(
									_this,
									props,
									linkpk_org,
									linkpk_org_name,
									linkpk_accbook,
									linkpk_accbook_name,
									linkaccyear,
									linkperiod,
									searchArea,
									detailArea
								);
							} else {
								let pk_org = getContext(loginContextKeys.pk_org);
								let org_Name = getContext(loginContextKeys.org_Name);
								if (!pk_org || pk_org === '') {
									props.search.setSearchValByField(searchArea, 'pk_accbook', {
										value: '',
										display: ''
									});
									props.search.setSearchValByField(searchArea, 'accyear', { value: '', display: '' });
									props.search.setSearchValByField(searchArea, 'period', { value: '', display: '' });
									props.search.setSearchValByField(searchArea, 'voucher_date', {
										value: '',
										display: ''
									});
									props.search.setDisabledByField(searchArea, 'pk_accbook', true);
									props.search.setDisabledByField(searchArea, 'accyear', true);
									props.search.setDisabledByField(searchArea, 'period', true);
									props.search.setDisabledByField(searchArea, 'voucher_date', true);
								} else {
									// 如果设置默认财务组织，需要进行数据设置
									setDeafultValueOrg.call(_this, _this.props, pk_org, org_Name, searchArea);
								}
							}
						}
					});
					props.form.setFormStatus(filterArea, 'edit');

					if (data.button) {
						let button = data.button;
						props.button.setButtons(button);
						let btnArr = [
							'Edit',
							'Filter',
							'Dep',
							'MakeVoucher',
							'FABillQueryAboutVoucher',
							'Print',
							'Save',
							'Cancle',
							'BatchAlter',
							'RateModify',
							'GatherChose',
							'Refresh'
						];
						props.button.setButtonVisible(btnArr, false);
					}
				}
			}
		}
	);
}
function modifierMeta(props, meta) {
	let defaultConfig = {
		searchId: searchArea,
		fieldMap: {
			pk_org: 'pk_org', //财务组织
			pk_accbook: 'pk_accbook' //财务核算账簿
		}
	};
	addSearchAreaReferFilter(props, meta, defaultConfig);

	// 使用部门及管理部门添加组织复选框/类别及卡片增加组织过滤
	meta[filterArea].items.map((item) => {
		//显示停用勾选框
		if (
			item.attrcode == 'pk_ownerOrgs' ||
			item.attrcode == 'pk_equipOrgs' ||
			item.attrcode == 'pk_usingstatuses' ||
			item.attrcode == 'pk_addstyles' ||
			item.attrcode == 'pk_depmethods'
		) {
			item.isShowDisabledData = true;
		}

		if (item.attrcode == 'pk_mngdept' || item.attrcode == 'pk_usedept') {
			item.isShowUnit = true;
			item.isShowDisabledData = true; //显示停用勾选框
		}
		if (item.attrcode == 'pk_cards') {
			item.queryCondition = () => {
				let pk_org = props.search.getSearchValByField(searchArea, 'pk_org').value.firstvalue;
				let pk_accbook = props.search.getSearchValByField(searchArea, 'pk_accbook').value.firstvalue;
				return { pk_org, pk_accbook, haveNotReduceCondition: false }; // 根据pk_org过滤
			};
		}
		if (item.attrcode == 'pk_categorys') {
			item.isShowDisabledData = true; //显示停用勾选框
			item.queryCondition = () => {
				let pk_org = props.search.getSearchValByField(searchArea, 'pk_org').value.firstvalue;
				return { pk_org }; // 根据pk_org过滤
			};
		}
		if (item.attrcode != 'isDepAll') {
			item.isMultiSelectedEnabled = true;
		}
		return item;
	});

	// 表体添加卡片超链接
	meta[detailArea].items.map((item) => {
		if (item.attrcode == 'asset_code') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td" field="asset_code" fieldname={getMultiLangByID('201204004A-000025')}>
						{' '}
						{/*"资产编码"*/}
						<span
							className="code-detail-link"
							onClick={() => {
								openAssetCardByPk(props, record.values.pk_card.value);
							}}
						>
							{record.values['asset_code'] && record.values['asset_code'].value}
						</span>
					</div>
				);
			};
		}
		return item;
	});

	return meta;
}

function setDeafultValueOrg(props, pk_org, org_Name, searchArea) {
	this.props.search.setSearchValByField(searchArea, 'pk_org', { value: pk_org, display: org_Name });
	// 设置账簿可编辑
	this.props.search.setDisabledByField(searchArea, 'pk_accbook', false);
	let searchdata = { pk_org: pk_org };
	// 设置组织编辑后事件
	orgChangeHandler.call(this, this.props, searchdata);
}

function openLink(
	props,
	linkpk_org,
	linkpk_org_name,
	linkpk_accbook,
	linkpk_accbook_name,
	linkaccyear,
	linkperiod,
	searchArea,
	detailArea
) {
	// 查询区设置值
	props.search.setSearchValByField(searchArea, 'pk_org', { value: linkpk_org, display: linkpk_org_name });
	props.search.setSearchValByField(searchArea, 'pk_accbook', { value: linkpk_accbook, display: linkpk_accbook_name });
	props.search.setSearchValByField(searchArea, 'accyear', { value: linkaccyear, display: linkaccyear });
	props.search.setSearchValByField(searchArea, 'period', { value: linkperiod, display: linkperiod });
	props.search.setDisabledByField(searchArea, 'pk_org', false);
	props.search.setDisabledByField(searchArea, 'pk_accbook', false);
	props.search.setDisabledByField(searchArea, 'accyear', false);
	props.search.setDisabledByField(searchArea, 'period', false);
	props.search.setDisabledByField(searchArea, 'voucher_date', false);

	let searchdata = { pk_org: linkpk_org, pk_accbook: linkpk_accbook };
	let _this = this;
	ajax({
		url: url.depQueryDate,
		data: searchdata,
		success: (res) => {
			if (res.data) {
				// 设置当前会计年、期间及凭证日期
				if (res.data.years) {
					// 将返回的年、期间及凭证日期信息存入state,避免重复查询
					_this.enddate = res.data.endate;
					_this.years = res.data.years;
					_this.periods = res.data.yearperiod;
					// 遍历设置所有会计年
					let selectData = res.data.years.map((year) => {
						return {
							display: year,
							value: year
						};
					});
					// 遍历获取最小未结账月的会计期间
					let minaccyear = res.data.mincloseperiod.accyear;
					let periodData = res.data.yearperiod[minaccyear].map((item) => {
						return {
							display: item,
							value: item
						};
					});
					// 设置会计年及会计期间信息至备选值
					let meta = _this.props.meta.getMeta();

					meta.searchArea.items.forEach((item) => {
						if (item.attrcode === 'accyear') {
							item.options = selectData;
						}
						if (item.attrcode === 'period') {
							item.options = periodData;
						}
					});
					_this.props.meta.setMeta(meta);

					let yearperiodstring = linkaccyear + linkperiod;
					let vouch_date = _this.enddate[yearperiodstring];
					// 重置凭证日期
					_this.props.search.setSearchValByField(searchArea, 'voucher_date', {
						value: vouch_date,
						display: vouch_date
					});
				}
				setTimeout(() => {
					onSearchBtn.call(_this, false);
				}, 500);
			}
		}
	});
}
