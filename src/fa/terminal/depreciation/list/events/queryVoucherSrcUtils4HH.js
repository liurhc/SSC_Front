import { toast, ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import onTabChange from './onTabChange';
import onSearchBtn from './onSearchBtn';
const { searchArea, url } = pageConfig;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/**
 * 凭证联查来源单据
 * @param {*} props
 * @param {*} data 列表表体编码
 */
export function queryVoucherSrc(props, data) {
	let _this = this;
	//凭证联查单据
	ajax({
		url: url.queryVoucherSrcUrl,
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					if (res.data) {
						if (res.data.pk_accbook) {
							_this.props.search.setSearchValByField(searchArea, 'pk_accbook', res.data.pk_accbook);
						}
						if (res.data.pk_org) {
							_this.props.search.setSearchValByField(searchArea, 'pk_org', res.data.pk_org);
						}
						// 设置当前会计年、期间及凭证日期
						if (res.data.years) {
							// 将返回的年、期间及凭证日期信息存入state,避免重复查询
							_this.enddate = res.data.endate;
							_this.years = res.data.years;
							_this.periods = res.data.yearperiod;
							// 遍历设置所有会计年
							let selectData = res.data.years.map((year) => {
								return {
									display: year,
									value: year
								};
							});
							// 遍历获取最小未结账月的会计期间
							let minaccyear = res.data.mincloseperiod.accyear;
							let periodData = res.data.yearperiod[minaccyear].map((item) => {
								return {
									display: item,
									value: item
								};
							});
							// 设置会计年及会计期间信息至备选值
							let meta = _this.props.meta.getMeta();

							meta.searchArea.items.forEach((item) => {
								if (item.attrcode === 'accyear') {
									item.options = selectData;
								}
								if (item.attrcode === 'period') {
									item.options = periodData;
								}
							});
							_this.props.meta.setMeta(meta);

							let yearperiodstring = res.data.linkaccyear + res.data.linkperiod;
							_this.props.search.setSearchValByField(searchArea, 'accyear', {
								value: res.data.linkaccyear,
								display: res.data.linkaccyear
							});
							_this.props.search.setSearchValByField(searchArea, 'period', {
								value: res.data.linkperiod,
								display: res.data.linkperiod
							});
							_this.props.search.setSearchValByField(searchArea, 'voucher_date', {
								value: vouch_date,
								display: vouch_date
							});
							let vouch_date = _this.enddate[yearperiodstring];
							// 重置凭证日期
							_this.props.search.setSearchValByField(searchArea, 'voucher_date', {
								value: vouch_date,
								display: vouch_date
							});
						}
						setTimeout(() => {
							onSearchBtn.call(_this, false);
							onTabChange.call(_this, 'depAllot');
						}, 500);
					}
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
					toast({
						duration: 'infinity',
						color: 'warning',
						content: getMultiLangByID('201204004A-000039')
					}); /** '未联查到数据' */
				}
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ duration: 'infinity', color: 'warning', content: res.message });
			}
		}
	});
}
