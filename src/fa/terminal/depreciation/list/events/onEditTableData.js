/**
 * 对 折旧清单  数据进行编辑时候
 */
import { ajax, base, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { detailArea, pagecode, searchArea, url } = pageConfig;
export default function(props, changedrows, index, record, mark, proportion) {
	let rows = [],
		depamount_old = '',
		depamount_new = '';
	if (mark == 'single') {
		this.editTableIndex = index;
		// let rowid = changedrows[0].rowid;
		rows = [ record ];
		let tableVal = props.editTable.getCacheDataById(detailArea).rows;
		let orginVal = tableVal[index];
		let oldVal = orginVal.values.depamount.value;
		depamount_old = changedrows[0].oldvalue.value;
		depamount_new = changedrows[0].newvalue.value;
		if (depamount_new == '') {
			depamount_new = 0;
		}
	} else if (mark == 'batch') {
		rows = props.editTable.getAllRows(detailArea);
		depamount_new = changedrows.state.newInput;
		if (depamount_new == '') {
			depamount_new = 0;
		}
	} else {
		rows = props.editTable.getAllRows(detailArea);
	}

	let pk_org = props.search.getSearchValByField(searchArea, 'pk_org').value.firstvalue;
	let pk_accbook = props.search.getSearchValByField(searchArea, 'pk_accbook').value.firstvalue;
	let userjsonstr = {
		pk_org: pk_org,
		pk_accbook: pk_accbook,
		depamount_old: mark === 'single' ? depamount_old : 0,
		depamount_new: mark !== 'scale' ? depamount_new : 0,
		isProportion: mark === 'scale' ? true : false, //按比例调整
		proportion: mark === 'scale' ? proportion : 0,
		isBatch_flag: mark === 'batch' ? true : false
	};
	let data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areaCode: detailArea,
			rows: rows
		},
		userjson: JSON.stringify(userjsonstr)
	};

	let _this = this;

	switch (mark) {
		case 'single': //单行修改
			ajax({
				url: url.depEventHandler,
				data,
				// async:false,
				success: function(res) {
					if (res.data && res.data.error) {
						let newrows = res.data.data.detailArea.rows;
						let index_after = _this.editTableIndex;
						//props.editTable.setRowByIndex(detailArea, newrows[0], index);
						props.editTable.setValByKeyAndIndex(detailArea, index, 'depamount', {
							value: newrows[0].values.depamount.value,
							display: newrows[0].values.depamount.value
						});
						toast({
							duration: 'infinity',
							color: 'warning',
							content: res.data.error
						});
					} else {
						if (res.data && res.data.data && res.data.data.detailArea) {
							let newrows = res.data.data.detailArea.rows;
							// 当前行设置单个字段值，否则下次编辑有问题
							props.editTable.setValByKeyAndIndex(detailArea, index, 'depamount', {
								value: newrows[0].values.depamount.value,
								display: newrows[0].values.depamount.value
							});
							// 当前行设置单个字段值，否则下次编辑有问题
							props.editTable.setValByKeyAndIndex(detailArea, index, 'accudep', {
								value: newrows[0].values.accudep.value,
								display: newrows[0].values.accudep.value
							});
							// props.editTable.setRowByIndex(detailArea, newrows[0], index);
							// 是否继承弹框
							props.modal.show('isShowedHerit');
							// Promise.resolve(true).then(()=>{
							// });
						}
					}
				}
			});
			break;
		case 'batch': //批改
			break;
		case 'scale': //按比例调整
			ajax({
				url: url.depEventHandler,
				data,
				success: function(res) {
					if (res.data && res.data.data && res.data.data.detailArea) {
						props.editTable.setTableData(detailArea, res.data.data.detailArea);
						if (res.data.error) {
							toast({
								duration: 'infinity',
								color: 'warning',
								content: res.data.error
							});
						}
					}
				}
			});
			break;
		default:
			break;
	}
}
