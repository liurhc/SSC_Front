const pageConfig={
    // 小应用编码
    appcode:'201204004A',
    // 页面编码
    pagecode:'201204004A_list',
    //查询区
    searchArea:'searchArea',
    //折旧清单
    detailArea:'detailArea',
    //折旧分配
    depgatherArea:'depgatherArea',
    //过滤
    filterArea:'filterArea',
    // 打印模板所在NC节点及标识
    printFuncode: '2012060010',
    // 折旧清单浏览态按钮
    btnEditArr : ['Edit', 'Filter', 'Dep', 'MakeVoucher', 'FABillQueryAboutVoucher', 'Print','Refresh'],
    // 折旧清单编辑态按钮
    btnSaveArr : ['Save', 'Cancle', 'BatchAlter', 'RateModify'],
    // 分摊项目按钮
	btnGatherArr : ['GatherChose'],

    // 按钮打开参数设置
    optionPagecode:'201200544A_list',
    optionAppcode:'201200544A',
    // 按钮打开工作量法设置
    workAppcode:'201204008A',
    workPagecode:'201204008A_list',
    // 联查凭证的交易类型
    billtype : 'HH',
    // 打印名称
    printFilename:'201204004A-000017',
    
    // 请求链接
	url: {
        depQueryVoucherUrl: '/nccloud/fa/depreciation/queryAboutvoucher.do',
        depEditUrl: '/nccloud/fa/depreciation/editenable.do',
        depSaveUrl: '/nccloud/fa/depreciation/save.do',
        depPrintUrl:'/nccloud/fa/depreciation/print.do',
        depQueryDate: '/nccloud/fa/depreciation/querydate.do',
        depEventHandler: '/nccloud/fa/depreciation/depeventhandler.do',
        depPageChange: '/nccloud/fa/depreciation/pagechange.do',
        queryVoucherSrcUrl: '/nccloud/fa/depreciation/querySrc.do' ,
        depDirectUrl: '/nccloud/fa/depreciation/depdirect.do',
        depUrl:'/nccloud/fa/depreciation/dep.do',
        depReadDetail:'/nccloud/fa/depreciation/readdetail.do',
        depFilter:'/nccloud/fa/depreciation/filter.do',
        depReadGather:'/nccloud/fa/depreciation/readgather.do',
        gatherChoseUrl:'/nccloud/fa/depreciation/gatherChose.do'
        
	}

}

export {pageConfig}