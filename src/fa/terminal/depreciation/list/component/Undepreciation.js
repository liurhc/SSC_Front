import React, { Component } from 'react';
import { base } from 'nc-lightapp-front';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { NCButton } = base;

export default class Undepreciation extends Component {
	constructor(props) {
		super(props);
		this.onDepreciation = this.props.onDepreciation;
	}

	render() {
		return (
			<div className="undepreciation-area">
				<span className="placeholder" />
				<p style={{ marginBottom: 10 }}>
					{this.props.alertTxt === '' ? getMultiLangByID('201204004A-000015') : this.props.alertTxt}
				</p>{' '}
				{/**'请选择财务组织和会计期间'  */}
				<NCButton colors="primary" disabled={this.props.btnDisabled} onClick={this.onDepreciation}>
					{' '}
					{getMultiLangByID('201204004A-000016')} {/** 计提折旧*/}
				</NCButton>
			</div>
		);
	}
}
