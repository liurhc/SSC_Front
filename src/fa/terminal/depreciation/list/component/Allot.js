import React, { Component } from 'react';
import { base, ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { detailArea, depgatherArea } = pageConfig;
const { NCTabs, NCPagination, NCSelect } = base;
const NCTabPane = NCTabs.NCTabPane;
const NCOption = NCSelect.NCOption;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default class Allot extends Component {
	constructor(props) {
		super(props);
		this.state = {
			summsg: props.summsg,
			tableStatus: props.tableStatus,
			pageinfo: props.pageinfo,
			tab: props.tab //被激活的页签
		};
		this.onTabChange = props.onTabChange;
		this.onPaginationChange = props.onPaginationChange;
		this.onSelectChange = props.onSelectChange;
		this.onEditTableAfterEvent = props.onEditTableAfterEvent;
		this.onEditTableBeforeEvent = props.onEditTableBeforeEvent;
	}
	componentWillReceiveProps(nextProps) {
		this.setState({
			summsg: nextProps.summsg,
			tableStatus: nextProps.tableStatus,
			pageinfo: nextProps.pageinfo,
			tab: nextProps.tab
		});
	}

	render() {
		let { form, editTable, table, modal } = this.props;
		let { createSimpleTable } = table;
		let { createEditTable } = editTable;
		let { createModal } = modal;
		let { summsg, pageinfo, tab } = this.state;
		let totalPage = pageinfo.totalPage ? pageinfo.totalPage * 1 : 0;
		let pageIndex = pageinfo.pageIndex ? pageinfo.pageIndex * 1 : 1;
		return (
			<div className="allot-area">
				<div>
					<h3>
						{getMultiLangByID('201204004A-000002')} {/**本月折旧总额：*/}
						<span>{summsg.depamount ? summsg.depamount : ''}</span>
					</h3>
					<p>
						{getMultiLangByID('201204004A-000003')} {/**计提原值：*/}
						<span>{summsg.localoriginvalue ? summsg.localoriginvalue : ''}</span>
						{getMultiLangByID('201204004A-000004')} {/**累计折旧：*/}
						<span>{summsg.accudep ? summsg.accudep : ''}</span>
						{getMultiLangByID('201204004A-000005')} {/**净值：*/}
						<span>{summsg.netvalue ? summsg.netvalue : ''}</span>
						{getMultiLangByID('201204004A-000006')} {/**计提折旧卡片数：*/}
						<span>{summsg.card_num ? summsg.card_num : ''}</span>
					</p>
				</div>
				<NCTabs
					navtype="turn"
					contenttype="moveleft"
					activeKey={tab}
					defaultActiveKey="depList"
					onChange={this.onTabChange}
				>
					<NCTabPane tab={getMultiLangByID('201204004A-000000')} key="depList">
						{createEditTable(detailArea, {
							onAfterEvent: this.onEditTableAfterEvent,
							onBeforeEvent: this.onEditTableBeforeEvent,
							showIndex: true //显示行号
						})}
						<div className="pagination-area">
							<div className="pagination-select">
								<NCSelect
									defaultValue={pageinfo.pageSize ? pageinfo.pageSize + '' : '100'}
									style={{ width: '100%' }}
									onChange={this.onSelectChange}
								>
									<NCOption value={'10'}>10{getMultiLangByID('201204004A-000007')}</NCOption>
									<NCOption value={'100'}>100{getMultiLangByID('201204004A-000007')}</NCOption>
									<NCOption value={'500'}>500{getMultiLangByID('201204004A-000007')}</NCOption>
									<NCOption value={'1000'}>1000{getMultiLangByID('201204004A-000007')}</NCOption>
									<NCOption value={'5000'}>5000{getMultiLangByID('201204004A-000007')}</NCOption>
								</NCSelect>
								<span className="total">
									{getMultiLangByID('201204004A-000008', {
										num: pageinfo.total ? pageinfo.total : 0
									})}
								</span>
							</div>
							<div className="pagination-info">
								<NCPagination
									first={false}
									last={false}
									prev
									next
									boundaryLinks
									maxButtons={5}
									items={totalPage}
									activePage={pageIndex}
									onSelect={this.onPaginationChange}
								/>
							</div>
						</div>
					</NCTabPane>
					<NCTabPane tab={getMultiLangByID('201204004A-000001')} key="depAllot">
						{createSimpleTable(depgatherArea, {
							showIndex: true,
							onAfterEvent: () => {}
						})}
					</NCTabPane>
				</NCTabs>
			</div>
		);
	}
}
