import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;

import fa from 'fa';
const { fa_components } = fa;
const { CardLinkToBillUtil } = fa_components;
const { CardLinkToBill } = CardLinkToBillUtil;

import {
	initTemplate,
	onAfterEvent,
	onCancalCheckout,
	onCheckout,
	onCancal,
	getModalContent,
	continueCancal,
	refreshPage
} from './events';
import { pageConfig } from './const';

import './index.less';

const { searchArea, pk_accbook, pk_org, cancalCheckout } = pageConfig;
const { NCButton } = base;

const { NCModal } = base;

class CloseBook extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pageStatus: 'info', //页面状态
			responsMsg: {}, //不能结账返回信息
			cancalMsg: [], //取消上月结账
			successMsg: [], //结账完成
			showModal: false,
			btnStatus: true //取消上月结账按钮是否禁用状态
		};
		this.onAfterEvent = onAfterEvent.bind(this);
		this.onCancalCheckout = onCancalCheckout.bind(this);
		this.onCheckout = onCheckout.bind(this);
		this.onCancal = onCancal.bind(this);
		this.getModalContent = getModalContent.bind(this);
		this.continueCancal = continueCancal.bind(this);
		this.refreshPage = refreshPage.bind(this);
		this.flag = 0; //个性化设置标志位
		this.resData = {}; //暂存查询返回的数据
		initTemplate.call(this, props);
	}
	componentWillUpdate() {}

	responsMsgArray = (responsMsg = {}) => {
		let arr = Object.keys(responsMsg);
		if (arr[0] != 'markresult') {
			for (let i = 0; i < arr.length; i++) {
				if (arr[i] == 'markresult') {
					let msg = arr[0];
					arr[i] = msg;
					arr[0] = 'markresult';
				}
			}
		}
		if (arr[arr.length - 1] != 'errorresult') {
			for (let i = 0; i < arr.length; i++) {
				if (arr[i] == 'errorresult') {
					let msg = arr[arr.length - 1];
					arr[i] = msg;
					arr[arr.length - 1] = 'errorresult';
				}
			}
		}
		return arr || [];
	};

	render() {
		let { button, search, modal } = this.props;
		let { NCCreateSearch } = search;
		let { createModal } = modal;
		let { pageStatus, responsMsg, cancalMsg, successMsg, btnStatus } = this.state;
		return (
			<div className="nc-bill-list" id="closebook-info">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{getMultiLangByID('201204016A-000005' /*国际化处理:结账*/)}</h2>
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchArea, {
						showAdvBtn: false,
						hideBtnArea: true,
						onAfterEvent: this.onAfterEvent
					})}

					<NCButton
						colors="info"
						// disabled={pageStatus === 'info' || pageStatus === 'infoAccbook' || btnStatus ? true : false}
						disabled={btnStatus ? true : false}
						onClick={this.onCancalCheckout}
					>
						{getMultiLangByID('201204016A-000004' /*国际化处理:取消上月结账*/)}
					</NCButton>
				</div>
				<div className="nc-bill-table-area closebook-content">
					{(pageStatus === 'info' || pageStatus === 'infoAccbook') && (
						<div className="info-check-out ">
							<div>
								<span className="placeholder" />
							</div>
							<div>
								{pageStatus === 'infoAccbook' && (
									<p>{getMultiLangByID('201204016A-000006' /*国际化处理:请选择'资产账簿'*/)}</p>
								)}
								{pageStatus === 'info' && (
									<p>{getMultiLangByID('201204016A-000007' /*国际化处理:请选择'财务组织'和'资产账簿'*/)}</p>
								)}
							</div>
						</div>
					)}
					{pageStatus === 'can' && (
						<div className="can-check-out ">
							<div>
								<p>
									<span className="iconfont icon-wancheng" />
									{getMultiLangByID('201204016A-000008' /*国际化处理:本月可以结账*/)}
								</p>
								<p>{getMultiLangByID('201204016A-000009' /*国际化处理:本月所有单据都已经审批通过，本月折旧已计提完成*/)}</p>
							</div>
							<NCButton colors="primary" onClick={this.onCheckout}>
								{getMultiLangByID('201204016A-000005' /*国际化处理:结账*/)}
							</NCButton>
						</div>
					)}
					{pageStatus === 'success' && (
						<div className="success-check-out ">
							<div>
								<p>
									<span className="iconfont icon-wancheng" />
									{search.getSearchValByField(searchArea, pk_accbook).display}
								</p>
								<p>
									{successMsg[0]} {getMultiLangByID('201204016A-000011' /*国际化处理:结账成功*/)}
								</p>
							</div>
							<NCButton colors="primary" onClick={this.refreshPage}>
								{getMultiLangByID('201204016A-000012' /*国际化处理:刷新*/)}
							</NCButton>
						</div>
					)}
					{pageStatus === 'cannot' && (
						<div className="cannot-check-out ">
							<div className="responsmsg">
								{this.responsMsgArray(responsMsg).map((key) => {
									//key = 减少单
									if (key == 'errorresult') {
										//"当月已计提，但发生了影响折旧的操作，需要重新计提！"
										return (
											<div className="detailmessage">
												<p>{responsMsg[key]['errormessage']['detailmessage']}</p>
											</div>
										);
									} else if (key == 'markresult') {
										if (responsMsg[key]['markmessage']['dataFlag'] == 'yes') {
											if (responsMsg[key]['markmessage']['detailmark'] == 'noclose') {
												return (
													<div className="markresultmsg">
														<p className="alert-info">
															<span className="iconfont icon-zhuyi1" />
															{getMultiLangByID('201204016A-000013' /*国际化处理:本月不能结账*/)}
														</p>
														<p className="alert-text">
															{getMultiLangByID(
																'201204016A-000014' /*国际化处理:本月有单据未审批通过：*/
															)}
														</p>
													</div>
												);
											} else {
												return (
													<div className="markresultmsg">
														<p className="alert-info">
															<span className="iconfont icon-zhuyi1" />
															{getMultiLangByID('201204016A-000015' /*国际化处理:不能取消结账*/)}
														</p>
														<p className="alert-text">
															{getMultiLangByID(
																'201204016A-000016' /*国际化处理:本月有单据不是自由态，系统无法删除：*/
															)}
														</p>
													</div>
												);
											}
										} else {
											return (
												<div className="msg-list">
													<p>{responsMsg['errorresult']['errormessage']['detailmessage']}</p>
												</div>
											);
										}
									} else if (key.indexOf('&') > 0) {
										//掉出单对应的调入单
										return Object.keys(responsMsg[key]).map((result) => {
											return (
												<div className="out-in-msg">
													<div>
														<p>{key.split('&')[0]}：</p>
														<p
															className="active"
															onClick={() => {
																CardLinkToBill.call(
																	this,
																	this.props,
																	responsMsg[key][result]['outpkbill'],
																	responsMsg[key][result]['outbill_type']
																);
															}}
														>
															{responsMsg[key][result]['outbill_code']}
														</p>
														<p>
															{responsMsg[key][result]['outbillmaketime'] &&
																responsMsg[key][result]['outbillmaketime']
																	.replace(/-/, getMultiLangByID('201204016A-000021'))
																	.replace(/-/, getMultiLangByID('201204016A-000022'))
																	.replace(/&/g, ` `) /*国际化处理:月*/ /*国际化处理:年*/}
														</p>
														<p>{responsMsg[key][result]['outbillmaker']}</p>
													</div>
													<div>
														<p>{key.split('&')[1]}</p>
														<p
															className="active"
															onClick={() => {
																CardLinkToBill.call(
																	this,
																	this.props,
																	responsMsg[key][result]['inpkbill'],
																	responsMsg[key][result]['inbill_type']
																);
															}}
														>
															{responsMsg[key][result]['inbill_code']}
														</p>
														<p>
															{responsMsg[key][result]['inbillmaketime'] &&
																responsMsg[key][result]['inbillmaketime']
																	.replace(/-/, getMultiLangByID('201204016A-000021'))
																	.replace(/-/, getMultiLangByID('201204016A-000022'))
																	.replace(/&/g, ` `) /*国际化处理:月*/ /*国际化处理:年*/}
														</p>
														<p>{responsMsg[key][result]['inbillmaker']}</p>
													</div>
												</div>
											);
										});
									} else {
										return Object.keys(responsMsg[key]).map((result) => {
											return (
												<div className="msg-list">
													<p>{key}</p>
													<p
														className="active"
														onClick={() => {
															CardLinkToBill.call(
																this,
																this.props,
																responsMsg[key][result]['pk_bill'],
																responsMsg[key][result]['bill_type']
															);
														}}
													>
														{responsMsg[key][result]['bill_code']}
													</p>
													<p>
														{responsMsg[key][result]['billmaketime'] &&
															responsMsg[key][result]['billmaketime']
																.split(' ')[0]
																.replace(/-/, getMultiLangByID('201204016A-000021'))
																.replace(/-/, getMultiLangByID('201204016A-000022'))
																.replace(/&/g, ` `) /*国际化处理:月*/ /*国际化处理:年*/}
													</p>
													<p>{responsMsg[key][result]['billmaker']}</p>
												</div>
											);
										});
									}
								})}
							</div>
							<NCButton colors="primary" onClick={this.refreshPage}>
								{getMultiLangByID('201204016A-000012' /*国际化处理:刷新*/)}
							</NCButton>
						</div>
					)}
					{pageStatus === 'cancel' && (
						<div className="cancel-check-out ">
							<div>
								<p>
									{getMultiLangByID(
										'201204016A-000012',
										{
											cancalMsg1: cancalMsg[1],
											cancalMsg0: cancalMsg[0]
										} /*国际化处理:取消 {cancalMsg[1]} 的结账，将会删除 {cancalMsg[0]} 新增的所有业务单据，是否仍然要取消结账？*/
									)}
								</p>
							</div>
							<NCButton colors="primary" onClick={this.onCancal}>
								{' '}
								{getMultiLangByID('201204016A-000019' /*国际化处理:取消结账*/)}{' '}
							</NCButton>
						</div>
					)}
					{pageStatus === 'cancelDone' && (
						<div className="cancelDone-check-out ">
							<div>
								<p>{this.props.search.getSearchValByField(searchArea, pk_accbook).display}</p>
								<p>
									{cancalMsg[1]} {getMultiLangByID('201204016A-000020' /*国际化处理:取消结账完成*/)}
								</p>
							</div>
							<NCButton colors="primary" onClick={this.refreshPage}>
								{getMultiLangByID('201204016A-000012' /*国际化处理:刷新*/)}
							</NCButton>
						</div>
					)}
				</div>
				{/* <div>
					<WorkloadModal 
						{...this.props}
						config ={wwwconfig}
						showModal={this.state.showModal}
						
					/>
				</div> */}
				{createModal(cancalCheckout, {
					title: getMultiLangByID('201204016A-000004' /*国际化处理:取消上月结账*/),
					content: this.getModalContent(),
					beSureBtnClick: this.continueCancal, //点击确定按钮事件
					// size: '100',
					className: 'senior'
				})}
			</div>
		);
	}
}
CloseBook = createPage({})(CloseBook);
initMultiLangByModule({ fa: [ '201204016A', 'facommon' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<CloseBook />, document.querySelector('#app'));
});
