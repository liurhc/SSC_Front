const pageConfig = {
	// 页面编码
	pagecode: '201204016A_list',
	//查询区
	searchArea: 'searchArea',
	//组织
	pk_org: 'pk_org',
	//账簿
	pk_accbook: 'pk_accbook',
	//会计期间
	accyear: 'accyear',
	//取消结账弹框
	cancalCheckout: 'cancalCheckout'
};

export { pageConfig };
