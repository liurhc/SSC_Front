import { pageConfig } from '../const';
const { searchArea, pk_accbook, accyear, pk_org } = pageConfig;
export default function onAfterEvent() {
	let org =
		this.props.search.getSearchValByField(searchArea, pk_org) &&
		this.props.search.getSearchValByField(searchArea, pk_org).value.firstvalue;
	let accbook =
		this.props.search.getSearchValByField(searchArea, pk_accbook) &&
		this.props.search.getSearchValByField(searchArea, pk_accbook).value.firstvalue;
	let yearInput =
		this.props.search.getSearchValByField(searchArea, accyear) &&
		this.props.search.getSearchValByField(searchArea, accyear).value.firstvalue;
	let year = yearInput.length > 1 ? yearInput.split('-')[0] : '';
	let per = yearInput.length > 1 ? yearInput.split('-')[1] : '';
	return { org, accbook, year, per };
}
