/**
 * 取消上月结账  按钮功能
 */
import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { getResponsMsg } from '../events';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { cancalCheckout } = pageConfig;
export default function onCancalCheckout() {
	let { org, accbook, year, per } = getResponsMsg.call(this);
	let _this = this;
	let newYear = year,
		newPer = per;
	if (newPer == 1) {
		newPer = 12;
		newYear -= 1;
	} else if (newPer < 11) {
		newPer -= 1;
		newPer = '0' + newPer;
		newYear = year;
	} else {
		newPer -= 1;
		newYear = year;
	}
	ajax({
		url: '/nccloud/fa/closebook/declosebookcheck.do',
		data: {
			pk_org: org,
			pk_accbook: accbook,
			accyear: newYear,
			period: newPer
		},
		success: function(res) {
			_this.props.modal.show(cancalCheckout);
			let data = [];
			data[0] = getMultiLangByID('201204016A-000003', {
				year: year,
				month: per
			}); /*国际化处理：year + '年' + per + '月'*/
			data[1] = getMultiLangByID('201204016A-000003', {
				year: newYear,
				month: newPer
			}); /*国际化处理：year + '年' + per + '月'*/
			_this.setState({ cancalMsg: data });
			_this.resData = { ...res };
		}
	});
}
