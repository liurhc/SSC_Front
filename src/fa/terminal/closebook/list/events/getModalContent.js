/**
 * @author:zhangxxu
 * @description:模态框的内容
 *     点击 取消上月结账 按钮时候，弹出一个模态框，
 */

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default function getModalContent() {
	return (
		<div className="cancal-checkout-modal">
			<div className="iconfont-area">
				<span className="iconfont icon-warning" />
			</div>
			<div>
				<p className="content-p1">{getMultiLangByID('201204016A-000000') /*国际化处理：确认取消上月结账？*/}</p>
				<p className="content-p2">
					{getMultiLangByID('201204016A-000001', {
						cancalMsg: this.state.cancalMsg[1]
					}) /*国际化处理：取消{this.state.cancalMsg[1]}的结账，*/}
				</p>
				<p className="content-p2">
					{getMultiLangByID('201204016A-000002', {
						cancalMsg: this.state.cancalMsg[0]
					}) /*国际化处理：将会删除{this.state.cancalMsg[0]}新增的所有业务单据*/}
				</p>
			</div>
		</div>
	);
}
