/**
 * @author:zhangxxu
 * @description: 账簿发生变化时候发送的请求
 *  
 */
import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { getResponsMsg } from '../events';
const { searchArea, pk_accbook, accyear, pk_org } = pageConfig;

export default function afterAjax(res, val) {
	let org, accbook, year, per;
	let _this = this;
	if (res === '') {
		let data = { ...getResponsMsg.call(_this) };
		org = data['org'];
		accbook = data['accbook'];
		year = data['year'];
		per = data['per'];
	} else {
		org = val.refpk;
		accbook = res.accbookStr;
		if (org == accbook) {
			org = _this.props.search.getSearchValByField(searchArea, 'pk_org').value.firstvalue;
		}
		year = res.accperiodVO && res.accperiodVO['accyear'];
		per = res.accperiodVO && res.accperiodVO['period'];
	}
	if (org && accbook && year && per) {
		ajax({
			url: '/nccloud/fa/closebook/closebookcheck.do',
			data: { pk_org: org, pk_accbook: accbook, accyear: year, period: per },
			success: function(returndata) {
				if (JSON.stringify(returndata.data) === '{}') {
					_this.setState({ pageStatus: 'can', btnStatus: false });
				} else {
					_this.setState({
						pageStatus: 'cannot',
						btnStatus: false,
						responsMsg: returndata.data
					});
				}
			},
			error: (returndata) => {
				toast({ content: returndata.message, color: 'danger' });
			}
		});
	}
}
