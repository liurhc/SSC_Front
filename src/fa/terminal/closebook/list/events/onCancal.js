/**
 * 取消结账  按钮功能
 */
import { ajax, toast } from 'nc-lightapp-front';
import { getResponsMsg } from '../events';
export default function onCancal() {
	let { org, accbook, year, per } = getResponsMsg.call(this);
	if (per == 1) {
		per = 12;
		year -= 1;
	} else if (per < 11) {
		per -= 1;
		per = '0' + per;
	} else {
		per -= 1;
	}
	let _this = this;
	ajax({
		url: '/nccloud/fa/closebook/declosebook.do',
		data: {
			pk_org: org,
			pk_accbook: accbook,
			accyear: year,
			period: per
		},
		success: function(res) {
			if (res.data === true) {
				_this.setState({ pageStatus: 'cancelDone' });
			}
		},
		error: (res) => {
			toast({ content: res.message, color: 'danger' });
		}
	});
}
