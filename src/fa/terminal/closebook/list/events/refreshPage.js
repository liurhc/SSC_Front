/**
 * @author:zhangxxu
 * @description:结账完成、取消结账完成  页面上的刷新按钮功能
 *     点击刷新按钮  页面返回初始界面
 */
import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { getResponsMsg, afterAjax } from '../events';
const { searchArea, accyear } = pageConfig;

export default function refreshPage() {
	let { org, accbook } = getResponsMsg.call(this);
	let _this = this;
	ajax({
		url: '/nccloud/fa/closebook/accbookchange.do',
		data: {
			pk_org: org,
			pk_accbook: accbook
		},
		success: (res) => {
			if (res.data) {
				_this.props.search.setSearchValByField(searchArea, accyear, {
					value: res.data.accperiodVO ? res.data.accperiodVO.accyear + '-' + res.data.accperiodVO.period : '',
					display: ''
				});
			}
			afterAjax.call(_this, res.data, { refpk: org });
		},
		error: (res) => {
			toast({ content: res.message, color: 'danger' });
		}
	});
}
