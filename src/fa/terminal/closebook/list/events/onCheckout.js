/**
 * 结账  按钮功能
 */
import { ajax } from 'nc-lightapp-front';
import { getResponsMsg } from '../events';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default function onCheckout() {
	let { org, accbook, year, per } = getResponsMsg.call(this);
	let _this = this;
	ajax({
		url: '/nccloud/fa/closebook/closebook.do',
		data: {
			pk_org: org,
			pk_accbook: accbook,
			accyear: year,
			period: per
		},
		success: function(res) {
			if (res.data === true) {
				let data = [];
				data.push(
					getMultiLangByID('201204016A-000003', {
						year: year,
						month: per
					}) /*国际化处理：year + '年' + per + '月'*/
				);
				_this.setState({ pageStatus: 'success', successMsg: data });
			}
		}
	});
}
