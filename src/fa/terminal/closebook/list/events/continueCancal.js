/**
 * @author:zhangxxu
 * @description:模态框 的确认按钮事件
 *     点击  确认  继续执行取消上月结账操作，
 */
import { onCancal } from '../events';

export default function continueCancal() {
	let res = { ...this.resData };
	this.setState({ btnStatus: true });
	let returnData = JSON.stringify(res.data);
	let arr = Object.keys(res.data);
	if (arr == 0) {
		onCancal.call(this);
		// this.setState({ pageStatus: 'cancel' });
	} else {
		this.setState({
			pageStatus: 'cannot',
			responsMsg: res.data
		});
	}
}
