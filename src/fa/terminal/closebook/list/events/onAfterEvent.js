import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { getResponsMsg, afterAjax } from '../events';

const { searchArea, pk_accbook, accyear, pk_org } = pageConfig;
export default function onAfterEvent(field, val) {
	let _this = this;
	switch (field) {
		case 'pk_org':
			//当组织为空时候，将账簿，期间也置空
			if (JSON.stringify(val) === '{}') {
				this.props.search.setDisabledByField(searchArea, pk_accbook, true);
				this.props.search.setSearchValByField(searchArea, pk_accbook, {
					value: '',
					display: ''
				});
				this.props.search.setSearchValByField(searchArea, accyear, {
					value: '',
					display: ''
				});
				this.setState({ pageStatus: 'info', btnStatus: true });
				return;
			}
			let pk_org = val.refpk;
			ajax({
				url: '/nccloud/fa/closebook/orgchange.do',
				data: { pk_org },
				success: function(res) {
					if (res.data) {
						_this.props.search.setDisabledByField(searchArea, pk_accbook, false);
						_this.props.search.setSearchValByField(searchArea, pk_accbook, {
							value: res.data.accbookStr,
							display: res.data.accbookname
						});
						_this.props.search.setSearchValByField(searchArea, accyear, {
							value: res.data.accperiodVO
								? res.data.accperiodVO.accyear + '-' + res.data.accperiodVO.period
								: '',
							display: ''
						});
					}
					res.data.accperiodVO && afterAjax.call(_this, res.data, val);
				},
				error: (res) => {
					toast({ content: res.message, color: 'danger' });
				}
			});
			break;
		case 'pk_accbook':
			//当账簿为空时候，将期间也置空
			if (JSON.stringify(val) === '{}') {
				this.props.search.setSearchValByField(searchArea, accyear, {
					value: '',
					display: ''
				});
				this.setState({ pageStatus: 'infoAccbook', btnStatus: true });
				return;
			}
			this.setState({ btnStatus: false });
			let { org, accbook } = getResponsMsg.call(this);
			ajax({
				url: '/nccloud/fa/closebook/accbookchange.do',
				data: {
					pk_org: org,
					pk_accbook: accbook
				},
				success: (res) => {
					if (res.data) {
						_this.props.search.setSearchValByField(searchArea, accyear, {
							value: res.data.accperiodVO
								? res.data.accperiodVO.accyear + '-' + res.data.accperiodVO.period
								: '',
							display: ''
						});
					}
					afterAjax.call(_this, res.data, val);
				},
				error: (res) => {
					toast({ content: res.message, color: 'danger' });
				}
			});
			break;
		default:
			break;
	}
}
