import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { afterAjax } from '../events';

import ampub from 'ampub';
const { components } = ampub;
const { LoginContext } = components;
const { getContext, loginContext, loginContextKeys } = LoginContext;

import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

const { pagecode, searchArea } = pageConfig;
let defaultConfig = {
	searchId: searchArea,
	specialFields: {
		//账簿
		pk_accbook: {
			//财务核算账簿
			RefActionExtType: 'TreeRefActionExt',
			class: 'nccloud.web.fa.common.refCondition.AccbookOrgRefFilter',
			data: [
				//字段过滤
				{
					fields: [ 'pk_org' ], //使用组织 组织版本过滤 顺序即优先级
					returnName: 'pk_org'
				},
				{
					returnConst: true,
					returnName: 'isDataPowerEnable'
				},
				{
					returnConst: 'FADefault',
					returnName: 'DataPowerOperationCode'
				}
			]
		}
	}
};

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: pagecode //页面id
		},
		function(data) {
			if (data) {
				if (data.context) {
					//初始化上下文变量
					loginContext.call(_this, data.context);
				}
				if (data.template) {
					let meta = data.template;
					meta = filterAccbook(props, meta);
					meta.context = data.context;
					props.meta.setMeta(meta, () => {
						let pk_org = getContext(loginContextKeys.pk_org);
						let org_Name = getContext(loginContextKeys.org_Name);
						if (pk_org) {
							props.search.setSearchValByField(searchArea, 'pk_org', {
								value: pk_org,
								display: org_Name
							});
							let val = { refpk: pk_org };
							ajax({
								url: '/nccloud/fa/closebook/orgchange.do',
								data: { pk_org },
								success: function(res) {
									if (res.data) {
										_this.props.search.setDisabledByField(searchArea, 'pk_accbook', false);
										_this.props.search.setSearchValByField(searchArea, 'pk_accbook', {
											value: res.data.accbookStr,
											display: res.data.accbookname
										});
										_this.props.search.setSearchValByField(searchArea, 'accyear', {
											value: res.data.accperiodVO
												? res.data.accperiodVO.accyear + '-' + res.data.accperiodVO.period
												: '',
											display: ''
										});
									}
									res.data.accperiodVO && afterAjax.call(_this, res.data, val);
								},
								error: (res) => {
									toast({ content: res.message, color: 'danger' });
								}
							});
						}
					});
				}
			}
		}
	);
}
function filterAccbook(props, meta) {
	addSearchAreaReferFilter.call(this, props, meta, defaultConfig);
	meta.searchArea.items.map((item) => {
		if (item['attrcode'] === 'pk_accbook') {
			item.disabled = true;
		}
		if (item['attrcode'] === 'accyear') {
			item.disabled = true;
		}
	});
	return meta;
}
