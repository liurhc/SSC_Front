/* 检测类型是否为数组 */
export function isArray(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'Array';
}

/* 检测类型是否为数组 */
export function isString(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'String';
}

/* 检测类型是否为数组 */
export function isFunction(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'Function';
}
