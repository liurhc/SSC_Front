import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Complex, reportConfig } from '../../base';

// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

const { code } = reportConfig;
/**
 * 增减变动查询
 */

export default class Addreduce extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return <Complex {...this.props} appcode={code.addreduce.appcode} pagecode={code.addreduce.pagecode} />;
	}
}

initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Addreduce />, document.getElementById('app'));
});
