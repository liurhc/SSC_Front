import React from 'react';
import ReactDOM from 'react-dom';
import { Simple } from '../../base';

// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
/**
 * 役龄统计表
 */
initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Simple />, document.getElementById('app'));
});
