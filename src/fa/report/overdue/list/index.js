import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Simple, reportConfig } from '../../base';
import { toast } from 'nc-lightapp-front';
import { commonValidator, onAfterEvent, groupToast, setDefaultVal } from '../../base/event';

// import { getMultiLangByID, initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;

const { validatorMsg, areaCode } = reportConfig;
/**
 * 逾龄统计表
 */
export class Overdue extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	setDefaultVal(searchId, props, flag, templateData) {
		//由于预置的查询条件中逾龄最大期限与最小期限存储的是中文年和月，需要进行多语替换
		if (templateData && templateData.template) {
			let template = templateData.template;
			let queryschemes = template[searchId].queryschemes; //查询方案数组
			queryschemes &&
				queryschemes.map((queryscheme) => {
					//conditionobj4web值修改
					queryscheme.conditionobj4web &&
						queryscheme.conditionobj4web.public &&
						queryscheme.conditionobj4web.public.conditions &&
						queryscheme.conditionobj4web.public.conditions.map((condition) => {
							if (
								condition.field &&
								(condition.field == 'overBoundMonth_Min' ||
									(condition.field == 'overBoundMonth_Max' && condition.value.firstvalue))
							) {
								let mlValue = condition.value.firstvalue
									.replace('年', getMultiLangByID('201204500-000034'))
									.replace('月', getMultiLangByID('201204500-000035'));
								condition.value.firstvalue = mlValue;
								condition.display = mlValue;
							}
						});
					// renderobj4web值修改;
					queryscheme.renderobj4web &&
						queryscheme.renderobj4web.map((condition) => {
							if (
								condition.attrcode &&
								(condition.attrcode == 'overBoundMonth_Min' ||
									(condition.attrcode == 'overBoundMonth_Max' && condition.initialvalue.value))
							) {
								let mlValue = condition.initialvalue.value
									.replace('年', getMultiLangByID('201204500-000034'))
									.replace('月', getMultiLangByID('201204500-000035'));
								condition.initialvalue.value = mlValue;
								condition.initialvalue.display = mlValue;
							}
						});
				});
		}
		setDefaultVal.call(this, searchId, props, flag, templateData);
	}

	/**
     * props: props
     * searchId: 查询区需要的searchId参数
     * field: 编辑后的key
     * val: 编辑后的value
    */
	afterEvent(props, searchId, field, val) {
		if (field == 'overBoundMonth_Min' || field == 'overBoundMonth_Max') {
			let fieldVal = props.search.getSearchValByField(searchId, field);

			//这里这样处理的原因是：平台对于文本输入框，onchange事件返回的val是有值的，onblur返回的val是undefined
			if (!val || (val && typeof val != 'string')) {
				val = fieldVal.value.firstvalue;
				if (!val) {
					props.search.setSearchValByField(searchId, field, { value: val, display: val });
					return;
				}
			} else {
				return;
			}

			//查询区编辑后事件
			let reg1 = new RegExp(
				`^\\d+${getMultiLangByID('201204500-000034')}\\d+${getMultiLangByID('201204500-000035')}$`
			); //某年某月
			let reg2 = new RegExp(`^\\d+${getMultiLangByID('201204500-000034')}$`); //某年
			let reg3 = new RegExp(`^\\d+${getMultiLangByID('201204500-000035')}$`); //某月
			let reg4 = /^\d+$/; //正则表达，正整数

			if (reg4.test(val) && val > 0) {
				//如果是纯数字
				val = `${parseInt(val / 12)}${getMultiLangByID('201204500-000034')}${val % 12}${getMultiLangByID(
					'201204500-000035'
				)}`;
				props.search.setSearchValByField(searchId, field, { value: val, display: val });
			} else if (reg1.test(val) || reg2.test(val) || reg3.test(val)) {
				//doNothing
			} else {
				if (val != '') {
					toast({
						color: 'warning',
						content: getMultiLangByID('201204500-000032') /*国际化处理：请输入1到+∞之间的整数*/
					});
				}
			}
		} else {
			onAfterEvent.call(this, props, searchId, field, val);
		}
	}

	expandSearchVal(items) {
		//通用校验
		let commonValid = commonValidator.call(this, this.props, items.conditions);
		if (!commonValid.valid) {
			groupToast(validatorMsg.content, commonValid.msg);
			return true;
		}
		let newItems = JSON.parse(JSON.stringify(items.conditions));
		let checkMonth = {
			display: false,
			field: 'checkMonth',
			isIncludeSub: false,
			oprtype: '=',
			value: {
				firstvalue: false,
				secondvalue: null
			}
		};
		let checkNetValue = {
			display: false,
			field: 'checkNetValue',
			isIncludeSub: false,
			oprtype: '=',
			value: {
				firstvalue: false,
				secondvalue: null
			}
		};
		let checkAccWork = {
			display: false,
			field: 'checkAccWork',
			isIncludeSub: false,
			oprtype: '=',
			value: {
				firstvalue: false,
				secondvalue: null
			}
		};
		if (newItems) {
			newItems.map((newItem) => {
				switch (newItem.field) {
					case 'overBoundMonth_Min':
						newItem.field = 'minMonth';
						newItem.value.firstvalue = this.props.changeStringToInt(
							newItem.value.firstvalue,
							this.bprops,
							areaCode.searchId,
							'overBoundMonth_Min'
						);
						newItem.display = newItem.value.firstvalue;
						break;
					case 'overBoundMonth_Max':
						newItem.field = 'maxMonth';
						newItem.value.firstvalue = this.props.changeStringToInt(
							newItem.value.firstvalue,
							this.bprops,
							areaCode.searchId,
							'overBoundMonth_Max'
						);
						newItem.display = newItem.value.firstvalue;
						break;
					case 'overdueCondition':
						switch (newItem.value.firstvalue) {
							case 'checkMonth':
								checkMonth.value.firstvalue = true;
								checkMonth.display = true;
								break;
							case 'checkNetValue':
								checkNetValue.value.firstvalue = true;
								checkMonth.display = true;
								break;
							case 'checkAccWork':
								checkAccWork.value.firstvalue = true;
								checkAccWork.display = true;
								break;
							default:
								break;
						}
						break;
					default:
						break;
				}
			});
		}
		newItems.push(checkMonth);
		newItems.push(checkNetValue);
		newItems.push(checkAccWork);
		newItems.splice(newItems.findIndex((item) => item.field == 'overdueCondition'), 1);
		items.conditions = newItems;
		return items;
	}
	changeStringToInt(str, props, searchId, field) {
		if (str) {
			let reg4 = /^\d+$/; //正则表达，正整数
			if (reg4.test(str)) {
				let newVal = `${parseInt(str / 12)}${getMultiLangByID('201204500-000034')}${str % 12}${getMultiLangByID(
					'201204500-000035'
				)}`;
				props.search.setSearchValByField(searchId, field, { value: newVal, display: newVal });
			} else {
				let strs = str
					.replace(getMultiLangByID('201204500-000034') /*国际化处理：年 */, ',')
					.replace(getMultiLangByID('201204500-000035') /*国际化处理：月 */, '')
					.split(',');
				if (strs.length == 1) {
					strs[1] = strs[0];
					strs[0] = '0';
				}
				strs.map((item, index) => {
					if (!item) strs[index] = '0';
				});

				str = parseInt(strs[0]) * 12 + parseInt(strs[1]);
			}
			return str;
		}
	}

	render() {
		return (
			<Simple
				setDefaultVal={this.setDefaultVal}
				onAfterEvent={this.afterEvent}
				expandSearchVal={this.expandSearchVal}
				{...this.props}
				changeStringToInt={this.changeStringToInt}
			/>
		);
	}
}
initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Overdue />, document.getElementById('app'));
});
