import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Simple, reportConfig } from '../../base';
import { onAfterEvent, reportGetAccbookAndPeriod } from '../../base/event';

// import { referLinkageClear } from '../../../common/components/ReferLinkage';
// import { config } from '../../../common/components/AccbookAndPeriod/const';
// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage, AccbookAndPeriod } = fa_components;
const { referLinkageClear } = ReferLinkage;
const { config } = AccbookAndPeriod;

const { code, queryConfig } = reportConfig;
const { defaultField } = config;
/**
 * 总 账
 */
export class General extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	afterEvent(props, searchId, field, val) {
		if (field === 'pk_org') {
			referLinkageClear.call(this, props, field, queryConfig);
			props.search.setSearchValByField(searchId, 'pk_accbook', {}); //清空账簿
			reportGetAccbookAndPeriod.call(this, props, searchId, null, null, (backProps, res, moduleId) => {
				let backData = res.data;
				let { accyear } = backData.period;
				let date = `${accyear}-01`;
				backProps.search.setSearchValByField(moduleId, defaultField.start_period, {
					value: date,
					display: date
				}); //起始期间
			});
		} else {
			onAfterEvent.call(this, props, searchId, field, val);
		}
	}
	render() {
		return (
			<Simple
				onAfterEvent={this.afterEvent}
				{...this.props}
				appcode={code.general.appcode}
				pagecode={code.general.pagecode}
			/>
		);
	}
}
initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<General />, document.getElementById('app'));
});
