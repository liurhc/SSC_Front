import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Complex } from '../../base';

// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
/**
 * 附属设备明细表
 */

export default class Subequip extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return <Complex planC={true} {...this.props} />;
	}
}
initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Subequip />, document.getElementById('app'));
});
