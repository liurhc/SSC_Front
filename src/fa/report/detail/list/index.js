import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Simple, reportConfig } from '../../base';

// import { initMultiLangByModule, getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule, getMultiLangByID } = multiLangUtils;

const { code } = reportConfig;

/**
 * 明细账
 */
export class Detail extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.printOther = [
			//update by dongzheng9 默认进入的时候 将页维度连续打印按钮置灰
			{ key: 'pagePrint', display: getMultiLangByID('201204500-000000'), disabled: true }
		]; /*国际化处理：页维度连续打印*/
	}

	render() {
		return (
			<Simple
				printOther={this.printOther}
				appcode={code.detail.appcode}
				pagecode={code.detail.pagecode}
				{...this.props}
			/>
		);
	}
}
initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Detail />, document.getElementById('app'));
});
