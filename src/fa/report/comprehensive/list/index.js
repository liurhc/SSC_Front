import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Simple } from '../../base';
import { setDefaultVal } from '../../base/event';

// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

/**
 * 资产上市公司报表
 */
export class Comprehensive extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	//资产上市报表的特殊处理，删除空级次
	setDefaultVal(searchId, props, flag, templateData) {
		setDefaultVal.call(this, searchId, props, flag, templateData);
		let meta = props.meta.getMeta();
		meta &&
			meta.light_report &&
			meta.light_report.items &&
			meta.light_report.items.map((item) => {
				if (item.attrcode.includes('Level')) {
					item.options.splice(0, 1);
				}
			});
	}

	render() {
		return <Simple {...this.props} setDefaultVal={this.setDefaultVal} />;
	}
}
initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Comprehensive />, document.getElementById('app'));
});
