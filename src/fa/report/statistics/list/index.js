import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Complex } from '../../base';

// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
/**
 * 固定资产统计分析表
 */

export default class Statistics extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return <Complex {...this.props} />;
	}
}
initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Statistics />, document.getElementById('app'));
});
