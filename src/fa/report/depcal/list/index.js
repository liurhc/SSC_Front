import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Complex } from '../../base';

// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
/**
 * 折旧计算汇总表
 */
export default class Depcal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return <Complex planB={true} {...this.props} />;
	}
}

initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Depcal />, document.getElementById('app'));
});
