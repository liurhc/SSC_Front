import React from 'react';
import ReactDOM from 'react-dom';
import { Simple } from '../../base';

// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
/**
 *折旧汇总明细页面
 */
initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Simple />, document.getElementById('app'));
});
