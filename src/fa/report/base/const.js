export const reportConfig = {
	//插叙对象模板常量，觉得可以优化一哈
	meta: {
		metaA: {
			object: {
				items: [
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000007' /*国际化处理：查询对象*/,
						maxlength: '8',
						required: true,
						col: '1',
						//width: '25%',
						attrcode: 'query_obj',
						options: [
							{
								display: '201204500-000008' /*国际化处理：增减方式*/,
								value: 'pk_addreducestyle'
							},
							{
								display: '201204500-000009' /*国际化处理：财务组织*/,
								value: 'pk_org'
							},
							{
								display: '201204500-000010' /*国际化处理：资产类别*/,
								value: 'pk_category'
							},
							{
								display: '201204500-000011' /*国际化处理：使用部门*/,
								value: 'pk_usedept'
							},
							{
								display: '201204500-000012' /*国际化处理：管理部门*/,
								value: 'pk_mandept'
							},
							{
								display: '201204500-000013' /*国际化处理：使用人*/,
								value: 'pk_assetuser'
							},
							{
								display: '201204500-000014' /*国际化处理：使用状况*/,
								value: 'pk_usingstatus'
							},
							{
								display: '201204500-000015' /*国际化处理：折旧方法*/,
								value: 'pk_depmethod'
							}
						]
					},
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000017' /*国际化处理：起始级次*/,
						maxlength: '8',
						required: true,
						col: '1',
						//width: '15%',
						attrcode: 'begin_level',
						options: [
							{
								display: '1',
								value: '1'
							},
							{
								display: '2',
								value: '2'
							},
							{
								display: '3',
								value: '3'
							},
							{
								display: '4',
								value: '4'
							},
							{
								display: '5',
								value: '5'
							},
							{
								display: '6',
								value: '6'
							},
							{
								display: '7',
								value: '7'
							},
							{
								display: '8',
								value: '8'
							},
							{
								display: '201204500-000019' /*国际化处理：明细*/,
								value: '9'
							}
						]
					},
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000018' /*国际化处理：终止级次*/,
						required: true,
						col: '1',
						maxlength: '8',
						//width: '15%',
						attrcode: 'end_level',
						options: [
							{
								display: '1',
								value: '1'
							},
							{
								display: '2',
								value: '2'
							},
							{
								display: '3',
								value: '3'
							},
							{
								display: '4',
								value: '4'
							},
							{
								display: '5',
								value: '5'
							},
							{
								display: '6',
								value: '6'
							},
							{
								display: '7',
								value: '7'
							},
							{
								display: '8',
								value: '8'
							},
							{
								display: '201204500-000019' /*国际化处理：明细*/,
								value: '9'
							}
						]
					},
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000020' /*国际化处理：显示位置*/,
						required: true,
						col: '1',
						maxlength: '8',
						//width: '20%',
						attrcode: 'view_posion',
						options: [
							{
								display: '201204500-000021' /*国际化处理：表头*/,
								value: 'head'
							},
							{
								display: '201204500-000001' /*国际化处理：表体*/,
								value: 'body'
							}
						]
					}
				],
				moduletype: 'table',
				pagination: false,
				code: 'body',
				name: '201204500-000001' /*国际化处理：表体*/,
				oid: null
			}
		},
		metaB: {
			object: {
				items: [
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000007' /*国际化处理：查询对象*/,
						maxlength: '8',
						required: true,
						col: '1',
						//width: '25%',
						attrcode: 'query_obj',
						options: [
							{
								display: '201204500-000008' /*国际化处理：增减方式*/,
								value: 'pk_addreducestyle'
							},
							{
								display: '201204500-000009' /*国际化处理：财务组织*/,
								value: 'pk_org'
							},
							{
								display: '201204500-000010' /*国际化处理：资产类别*/,
								value: 'pk_category'
							},
							{
								display: '201204500-000011' /*国际化处理：使用部门*/,
								value: 'pk_usedept'
							},
							{
								display: '201204500-000012' /*国际化处理：管理部门*/,
								value: 'pk_mandept'
							},
							{
								display: '201204500-000013' /*国际化处理：使用人*/,
								value: 'pk_assetuser'
							},
							{
								display: '201204500-000014' /*国际化处理：使用状况*/,
								value: 'pk_usingstatus'
							},
							{
								display: '201204500-000015' /*国际化处理：折旧方法*/,
								value: 'pk_depmethod'
							},
							{
								display: '201204500-000016' /*国际化处理：折旧承担部门*/,
								value: 'pk_paydept'
							}
						]
					},
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000017' /*国际化处理：起始级次*/,
						maxlength: '8',
						required: true,
						col: '1',
						//width: '15%',
						attrcode: 'begin_level',
						options: [
							{
								display: '1',
								value: '1'
							},
							{
								display: '2',
								value: '2'
							},
							{
								display: '3',
								value: '3'
							},
							{
								display: '4',
								value: '4'
							},
							{
								display: '5',
								value: '5'
							},
							{
								display: '6',
								value: '6'
							},
							{
								display: '7',
								value: '7'
							},
							{
								display: '8',
								value: '8'
							},
							{
								display: '201204500-000019' /*国际化处理：明细*/,
								value: '9'
							}
						]
					},
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000018' /*国际化处理：终止级次*/,
						required: true,
						col: '1',
						maxlength: '8',
						//width: '15%',
						attrcode: 'end_level',
						options: [
							{
								display: '1',
								value: '1'
							},
							{
								display: '2',
								value: '2'
							},
							{
								display: '3',
								value: '3'
							},
							{
								display: '4',
								value: '4'
							},
							{
								display: '5',
								value: '5'
							},
							{
								display: '6',
								value: '6'
							},
							{
								display: '7',
								value: '7'
							},
							{
								display: '8',
								value: '8'
							},
							{
								display: '201204500-000019' /*国际化处理：明细*/,
								value: '9'
							}
						]
					},
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000020' /*国际化处理：显示位置*/,
						required: true,
						col: '1',
						maxlength: '8',
						//width: '20%',
						attrcode: 'view_posion',
						options: [
							{
								display: '201204500-000021' /*国际化处理：表头*/,
								value: 'head'
							},
							{
								display: '201204500-000001' /*国际化处理：表体*/,
								value: 'body'
							}
						]
					}
				],
				moduletype: 'table',
				pagination: false,
				code: 'body',
				name: '201204500-000001' /*国际化处理：表体*/,
				oid: null
			}
		},
		metaC: {
			object: {
				items: [
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000007' /*国际化处理：查询对象*/,
						maxlength: '8',
						required: true,
						col: '1',
						//width: '15%',
						attrcode: 'query_obj',
						options: [
							{
								display: '201204500-000009' /*国际化处理：财务组织*/,
								value: 'pk_org'
							},
							{
								display: '201204500-000010' /*国际化处理：资产类别*/,
								value: 'pk_category'
							},
							{
								display: '201204500-000011' /*国际化处理：使用部门*/,
								value: 'pk_usedept'
							}
						]
					},
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000017' /*国际化处理：起始级次*/,
						maxlength: '8',
						required: true,
						col: '1',
						//width: '15%',
						attrcode: 'begin_level',
						options: [
							{
								display: '1',
								value: '1'
							},
							{
								display: '2',
								value: '2'
							},
							{
								display: '3',
								value: '3'
							},
							{
								display: '4',
								value: '4'
							},
							{
								display: '5',
								value: '5'
							},
							{
								display: '6',
								value: '6'
							},
							{
								display: '7',
								value: '7'
							},
							{
								display: '8',
								value: '8'
							},
							{
								display: '201204500-000019' /*国际化处理：明细*/,
								value: '9'
							}
						]
					},
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000018' /*国际化处理：终止级次*/,
						required: true,
						col: '1',
						maxlength: '8',
						//width: '15%',
						attrcode: 'end_level',
						options: [
							{
								display: '1',
								value: '1'
							},
							{
								display: '2',
								value: '2'
							},
							{
								display: '3',
								value: '3'
							},
							{
								display: '4',
								value: '4'
							},
							{
								display: '5',
								value: '5'
							},
							{
								display: '6',
								value: '6'
							},
							{
								display: '7',
								value: '7'
							},
							{
								display: '8',
								value: '8'
							},
							{
								display: '201204500-000019' /*国际化处理：明细*/,
								value: '9'
							}
						]
					},
					{
						itemtype: 'select',
						visible: true,
						label: '201204500-000020' /*国际化处理：显示位置*/,
						required: true,
						col: '1',
						maxlength: '8',
						//width: '20%',
						attrcode: 'view_posion',
						options: [
							{
								display: '201204500-000021' /*国际化处理：表头*/,
								value: 'head'
							},
							{
								display: '201204500-000001' /*国际化处理：表体*/,
								value: 'body'
							}
						]
					}
				],
				moduletype: 'table',
				pagination: false,
				code: 'body',
				name: '201204500-000001' /*国际化处理：表体*/,
				oid: null
			}
		}
	},
	//请求常量
	url: {
		queryAccbookAndPeriodUrl: '/nccloud/fa/fapub/queryAccbookAndPeriod.do', //查询账簿与会计期间url
		pagePrintUrl: '/nccloud/fa/report/fapagedimprint.do', //页维度连续打印请求url
		queryAccbookPeriodSchemeUrl: '/nccloud/fa/fapub/queryAccbookPeriodScheme.do', //查询会计期间方案url
		reportToCardUrl: '/nccloud/fa/report/fareporttocard.do', //联查卡片url
		reportToVoucherUrl: '/nccloud/fa/report/fareporttovoucher.do', //联查凭证url
		reportToBillUrl: '/nccloud/fa/report/fareporttobill.do', //获取联查单据信息的url
		openBillUrl: '/nccloud/ampub/common/amLinkQuery.do', //打开单据的url
		generalToDetail: '/nccloud/fa/report/checklinkgeneral.do', //总账联查明细账控制修改
		modelPrintUrl: '/nccloud/fa/report/fareportmodelprint.do' //模板打印
	},
	//参超过滤区域常量
	queryConfig: {
		searchId: 'light_report',
		formId: '',
		bodyIds: [ 'object' ],
		specialFields: {
			//项目档案
			pk_jobmngfil:{
				data: [
					{
						fields: [ 'pk_org'], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
				]
			},
			//货主管理组织
			pk_ownerorg: {
				data: [
					{
						fields: [ 'pk_org' ],
						returnName: 'pk_orgs'
					}
				]
			},
			//供应商
			provider:{
				data: [
					{
						fields: [ 'pk_org'], //可被多个字段过滤 取第一个有的值
						returnName: 'pk_org' //被过滤字段名后台需要参数
					},
				]
			}
		}
	},
	buttonName: {
		clear: '201204500-000031' /*国际化处理：清空*/
	},
	areaCode: {
		searchId: 'light_report',
		tableId: 'object'
	},
	validatorMsg: {
		content: '201204500-000022' /*国际化处理：数据输入有误！*/,
		common: {
			noConditionError: '201204500-000023' /*国际化处理：无查询条件！*/,
			dateError: '201204500-000024' /*国际化处理：起始会计期间不能大于终止会计期间！*/,
			overBoundMonth_MinError: '201204500-000025' /*国际化处理：逾龄最小范围请输入1到+∞之间的整数！*/,
			overBoundMonth_MaxError: '201204500-000026' /*国际化处理：逾龄最大范围请输入1到+∞之间的整数！*/,
			betweenError: '201204500-000050' /**国际化处理：条件不完整！ */
		},
		complex: {
			levelError: '201204500-000027' /*国际化处理：的起始级次不能大于结束级次！*/,
			objectNullError: '201204500-000028' /*国际化处理：查询对象为空，请至少选择一个查询对象！*/,
			objectRepeatError: '201204500-000029' /*国际化处理：不能重复选择！*/,
			viewPosionError: '201204500-000030' /*国际化处理：查询对象至少1个显示位置在表体！*/,
			addreduceObjectError: '201204500-000042' /*国际化处理：请保证查询对象中包含“增减方式” */
		}
	},
	fieldName: {
		categoryLevel: '201204500-000043' /*国际化处理：类别级次 */,
		usedeptLevel: '201204500-000044' /*国际化处理：部门级次 */,
		usestatusLevel: '201204500-000045' /*国际化处理：使用状况级次 */,
		styleLevel: '201204500-000046' /*国际化处理：方式级次 */
	},
	dataSource: 'fa.report.base.main',
	linktoVoucher: {
		appCode: '10170410',
		pageCode: '10170410_1017041001',
		linkCacheWord: '_LinkVouchar'
	},
	code: {
		detail: {
			appcode: '201204516A',
			pagecode: '201204516A_list'
		},
		general: {
			appcode: '201204512A',
			pagecode: '201204512A_list'
		},
		addreduce: {
			appcode: '201204536A',
			pagecode: '201204536A_list'
		}
	},
	drillCode: {
		queryBIll: 'queryBill',
		queryCard: 'queryCard',
		queryVoucher: 'queryVoucher',
		queryReport: 'queryReport'
	},
	connectionCode: {
		BILL_TYPE_SRC: 'BILL_TYPE_SRC',
		bill_type_src: 'bill_type_src',
		BILL_TYPE: 'BILL_TYPE',
		bill_type: 'bill_type',
		TRANSI_TYPE_SRC: 'TRANSI_TYPE_SRC',
		transi_type_src: 'transi_type_src',
		PK_BILL_SRC: 'PK_BILL_SRC',
		pk_bill_src: 'pk_bill_src',
		PK_CARD: 'PK_CARD',
		pk_card: 'pk_card',
		pk_cardhistory: 'pk_cardhistory',
		PK_CARFHISTORY: 'PK_CARDHISTORY',
		asset_code: 'asset_code',
		ASSET_CODE: 'ASSET_CODE'
	},
	fxs: {
		'#mainorg#': '#mainorg#',
		'#accountingbook#': '#accountingbook#',
		'#firstaccperiod#': '#firstaccperiod#',
		'#currentaccperiod#': '#currentaccperiod#',
		'#mainaccperiod#': '#mainaccperiod#',
		'#mainaccbook#': '#mainaccbook#'
	},
	allFx: '#mainorg##accountingbook##firstaccperiod##currentaccperiod##mainaccperiod##mainaccbook#'
};
