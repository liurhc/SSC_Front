import { reportConfig } from '../const';

import { getPeriodScheme } from '.';

// import { addSearchAreaReferFilter } from '../../../common/components/ReferFilter';
import fa from 'fa';
const { fa_components } = fa;
const { ReferFilter } = fa_components;
const { addSearchAreaReferFilter } = ReferFilter;

const { queryConfig, areaCode } = reportConfig;
/**
 * 参照过滤类
 * @param {*} areacode 
 * @param {*} meta 
 * @param {*} props 
 */
export function filter(meta, props) {
	addSearchAreaReferFilter(props, meta, queryConfig); //调用公共过滤方法
	let that = this;
	// 特殊处理
	let items = meta[areaCode.searchId].items;
	// 遍历所有项目
	items.forEach((titem) => {
		// 字段编码
		let itemcode = titem.attrcode;
		// 会计期间
		if (itemcode == 'query_period' || itemcode == 'start_period' || itemcode == 'end_period') {
			// 获得当前选择的财务组织
			titem.queryCondition = () => {
				return { pk_accperiodscheme: getPeriodScheme() };
			};
		}

		//变动单通过单据类型过滤交易类型
		if (itemcode == 'fa_alter.pk_transitype') {
			titem.queryCondition = () => {
				return { parentbilltype: 'HG' };
			};
		}
		if (itemcode.includes('pk_category') || itemcode.includes('pk_card')) {
			titem.queryCondition = () => {
				return { DataPowerOperationCode: 'default', isDataPowerEnable: true };
			};
		}
		//明细账、总账、上市报表、折旧二维表特殊处理
		if (itemcode == 'queryCard') {
			titem.queryCondition = () => {
				return { DataPowerOperationCode: 'default', isDataPowerEnable: true };
			};
		}
	});
}
