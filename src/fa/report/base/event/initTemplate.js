import { reportConfig } from '../const';

// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { areaCode, meta, buttonName } = reportConfig;
/**
 * 查询对象模板
 * @param {object} props 
 */
export default function(props) {
	let currentMeta = {};
	if (props.planB) {
		currentMeta = meta.metaB;
	} else if (props.planC) {
		currentMeta = meta.metaC;
	} else {
		currentMeta = meta.metaA;
	}
	//多语处理
	for (const key in currentMeta.object) {
		if (currentMeta.object.hasOwnProperty(key)) {
			const element = currentMeta.object[key];
			if (key === 'items') {
				element.map((item) => {
					//item的外层元素
					for (const innerkey in item) {
						if (item.hasOwnProperty(innerkey)) {
							const innerelement = item[innerkey];
							if (innerkey === 'options') {
								innerelement.map((option) => {
									if (option.display && option.display.includes('201204500-')) {
										option.display = getMultiLangByID(option.display);
									}
								});
							} else if (
								innerelement &&
								typeof innerelement === 'string' &&
								innerelement.includes('201204500-')
							) {
								item[innerkey] = getMultiLangByID(innerelement);
							}
						}
					}
				});
			} else if (element && typeof element === 'string' && element.includes('201204500-')) {
				currentMeta.object[key] = getMultiLangByID(element);
			}
		}
	}
	currentMeta = modifyMeta(currentMeta, props);
	props.meta.setMeta(currentMeta);
}

/**
 * 增加操作列
 * @param {*} meta 
 * @param {*} props 
 */
function modifyMeta(currentMeta, props) {
	currentMeta[areaCode.tableId].items.push({
		label: getMultiLangByID('amcommon-000000') /*国际化处理：操作 */,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		width: '10%',
		render: (text, record, index) => {
			return (
				<div className="opr-col">
					<span
						className="opr-del"
						onClick={() => {
							clearDataByIndex(props, index);
						}}
					>
						{getMultiLangByID(buttonName.clear)}
					</span>
				</div>
			);
		}
	});
	let pageMeta = props.meta.getMeta();
	return Object.assign(pageMeta, currentMeta);
}

//根据行号，清空数据
export function clearDataByIndex(props, index) {
	let clears = [ 'query_obj', 'begin_level', 'end_level', 'view_posion' ];
	clears.map((clear) => {
		props.editTable.setValByKeyAndIndex(areaCode.tableId, index, clear, {
			value: null,
			display: null,
			scale: null,
			isEdit: null
		});
	});
	props.editTable.setRowStatus(areaCode.tableId, index, 2); //更改行状态为新增态
}
