import { toast } from 'nc-lightapp-front';
import { commonValidator, existValidator, complexValidator } from '.';
import { reportConfig } from '../const';

// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { areaCode, validatorMsg } = reportConfig;
/**
 * 有查询对象的条件扩展
 * @param {*} items 
 */
export function complexExpandSearchVal(items) {
	let isHead = true; //是否是禁用页维度打印
	let logic = items.logic;
	//通用校验
	let commonValid = commonValidator.call(this, this.props, items.conditions);
	if (!commonValid.valid) {
		groupToast(validatorMsg.content, commonValid.msg);
		return true;
	}

	let newItems = JSON.parse(JSON.stringify(items.conditions)); //赋值查询条件，因为平台要调用三次这个方法，所以需要深复制一个来操作

	let oldTableData = this.props.editTable.getAllRows(areaCode.tableId, true); //获取查询对象数据
	let tableData = [];
	oldTableData.map((item) => {
		if (item.status == '1') {
			tableData.push(item);
		}
	});

	//查询对象是否存在校验
	let existValid = existValidator.call(this, this.props, newItems, tableData);
	if (existValid === false) {
		return true;
	} else if (!existValid.valid) {
		groupToast(validatorMsg.content, existValid.msg);
		return true;
	}

	let tableDataStr = JSON.stringify(tableData); //获取查询对象字符串

	//将查询对象扩展至查询条件
	if (tableData.length > 0) {
		let query_str = '';
		//遍历所有查询对象，拼接字符串
		for (let index = 0; index < tableData.length; index++) {
			let values = tableData[index].values;

			//单个查询对象内容校验
			let complexValid = complexValidator.call(this, this.props, tableDataStr, values);
			if (!complexValid.valid) {
				groupToast(validatorMsg.content, complexValid.msg);
				return true;
			}

			query_str += `${values.query_obj.value}+${values.begin_level.value}+${values.end_level.value}+${values
				.view_posion.value};`;
		}

		if (query_str.includes('head')) {
			isHead = false;
		}

		//扩展对象
		let obj = {
			field: 'query_object',
			oprtype: '=',
			value: {
				firstvalue: query_str,
				secondvalue: null
			}
		};

		newItems.push(obj); //注入查询条件
	}
	items.conditions = newItems;

	//禁启用页维度打印
	let printOther = this.state.printOther;
	if (printOther) {
		printOther.map((print) => {
			if (print.key && print.key == 'pagePrint') {
				print.disabled = isHead;
			}
		});
		this.setState({
			printOther: printOther
		});
	}

	return items;
}

/**
 * 无查询对象的报表的条件扩展
 * @param {*} items 
 */
export function simpleExpandSearchVal(items) {
	let isHead = true; //是否禁用页维度打印
	// 通用校验
	let commonValid = commonValidator.call(this, this.props, items.conditions);
	if (!commonValid.valid) {
		groupToast(validatorMsg.content, commonValid.msg);
		return true;
	}
	items.conditions &&
		items.conditions.map((condition) => {
			if (condition.field === 'cardListShow') {
				if (condition.value && condition.value.firstvalue) {
					if (condition.value.firstvalue == 'Y') {
						isHead = false;
					}
				}
			}
		});
	//禁启页维度打印
	let printOther = this.state.printOther;
	if (printOther) {
		printOther.map((print) => {
			if (print.key && print.key == 'pagePrint') {
				print.disabled = isHead;
			}
		});
		this.setState({
			printOther: printOther
		});
	}
	return items;
}

/**
 * 批量toast
 * @param {*} content 显示内容 
 * @param {*} msg 展开后显示 数组
 * @param {*} color 颜色 默认警告
 * @param {*} time 超时时间，默认不超时
 */
export function groupToast(content = '', msg = [], color = 'danger', time = 'infinity') {
	content = getMultiLangByID(content); //处理多语
	let toastMsg = '';
	msg &&
		msg.map((m) => {
			toastMsg += getMultiLangByID(m) + '\n';
		}); //处理多语
	toast({
		content: toastMsg,
		duration: time, //消失时间，默认是3秒; 值为 infinity 时不消失,非必输
		color: color
	});
}
