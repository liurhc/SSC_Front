import { reportConfig } from '../const';
import { reportCommonAjax } from '.';

// import { linkQueryConst } from '../../../../ampub/common/const/CommonKeys';
// import { openToVouchar } from '../../../../ampub/common/components/QueryVocher/queryVocherUtils';
// import { openAssetCardByPk } from '../../../../ampub/common/components/QueryAbout/faQueryAboutUtils';
import ampub from 'ampub';
const { components, commonConst } = ampub;
const { faQueryAboutUtils, queryVocherUtils } = components;
const { CommonKeys } = commonConst;
const { openAssetCardByPk } = faQueryAboutUtils;
const { openToVouchar } = queryVocherUtils;
const { linkQueryConst } = CommonKeys;

const { url, linktoVoucher, drillCode, connectionCode, code } = reportConfig;
/**
 * 联查卡片、联查明细、联查单据、联查凭证
 * @param {*} transSaveObject 
 * @param {*} obj 
 * @param {*} data 
 * @param {*} props 
 * @param {*} url 
 * @param {*} urlParams 
 * @param {*} sessonKey 
 */
export function setConnectionSearch(transSaveObject, obj, data, props, url, urlParams, sessonKey) {
	let drillType = 0; //联查类型
	let bill_type = null; //卡片类型
	let transi_type_src = null; //来源交易类型
	let bill_type_src = null; //来源单据类型
	let primary_key = null; //主键
	let pk_history = null; //卡片历史
	let { dirllcode, fldName } = data;
	if (dirllcode.includes(drillCode.queryBIll)) {
		transi_type_src = connectionCode.TRANSI_TYPE_SRC;
		bill_type_src = connectionCode.BILL_TYPE_SRC;
		primary_key = connectionCode.PK_BILL_SRC;
		drillType = 1; //联查单据
	} else if (dirllcode.includes(drillCode.queryCard)) {
		if (fldName.includes(connectionCode.ASSET_CODE)) {
			bill_type = connectionCode.BILL_TYPE_SRC;
			primary_key = connectionCode.PK_CARD;
			pk_history = connectionCode.PK_CARFHISTORY;
		} else if (fldName.includes(connectionCode.asset_code)) {
			bill_type = connectionCode.bill_type_src;
			primary_key = connectionCode.pk_card;
			pk_history = connectionCode.pk_cardhistory;
		}
		drillType = 2; //联查卡片
	} else if (dirllcode.includes(drillCode.queryReport)) {
		sessionStorage.setItem(sessonKey, transSaveObject);
		if (this && this.props && this.props.appcode && this.props.appcode == code.general.appcode) {
			generalToDetail.call(this, props, data, transSaveObject, url, urlParams); //联查卡片
		} else {
			props.openTo(url, urlParams); //联查明细
		}
	} else if (dirllcode.includes(drillCode.queryVoucher)) {
		transi_type_src = connectionCode.TRANSI_TYPE_SRC;
		primary_key = connectionCode.PK_BILL_SRC;
		drillType = 3; //明细账联查凭证
	}

	if (drillType === 2) {
		reportToCard.call(this, props, data, primary_key, bill_type, transSaveObject, pk_history); //联查卡片
	} else if (drillType === 1) {
		reportToBill.call(this, props, data, primary_key, bill_type_src, transi_type_src, transSaveObject); //联查单据
	} else if (drillType === 3) {
		reportToVoucher.call(this, props, data, primary_key, transi_type_src, transSaveObject); //联查凭证
	}
}

function generalToDetail(props, data, transSaveObject, targerUrl, urlParams) {
	let that = this;
	// data['querycondition'] = data['condition'];
	data['transSaveObject'] = transSaveObject;
	reportCommonAjax.call(that, props, url.generalToDetail, data, (res) => {
		if (res.success) {
			if (res.data) {
				props.openTo(targerUrl, urlParams); //联查明细
			}
		}
	});
}

function reportToBill(props, data, primary_key, bill_type_src, transi_type_src, transSaveObject) {
	let that = this;
	// data['querycondition'] = data['condition'];
	data['transSaveObject'] = transSaveObject;
	data['bill_type_src'] = bill_type_src;
	data['transi_type_src'] = transi_type_src;
	data['primary_key'] = primary_key;
	reportCommonAjax.call(that, props, url.reportToBillUrl, data, (res) => {
		if (res.success) {
			if (res && res.data) {
				let linkData = res.data;
				linkData['status'] = 'browse';
				props.openTo(linkData[linkQueryConst.URL], linkData);
			}
		}
	});
}

/**
 * 报表联查卡片
 * @param {*} props 
 * @param {*} data 
 * @param {*} primary_key 主键字段，如：“pk_assign”
 * @param {*} bill_type 单据类型字段,如："bill_type
 */
function reportToCard(props, data, primary_key, bill_type, transSaveObject, pk_history) {
	let that = this;
	// data['querycondition'] = data['condition'];
	data['transSaveObject'] = transSaveObject;
	data['bill_type'] = bill_type;
	data['pk'] = primary_key;
	data['pk_history'] = pk_history;
	reportCommonAjax.call(that, props, url.reportToCardUrl, data, (res) => {
		if (res.success) {
			if (res && res.data) {
				let { pk_card, pk_cardhistory } = res.data;
				openAssetCardByPk(props, pk_card, { pk_cardhistory });
			}
		}
	});
}
/**
 * 联查凭证的方法
 * @param {*} props 
 * @param {*} data 
 * @param {*} primary_key 
 * @param {*} transi_type_src 
 * @param {*} transSaveObject 
 */
function reportToVoucher(props, data, primary_key, transi_type_src, transSaveObject) {
	let that = this;
	// data['querycondition'] = data['condition'];
	data['transSaveObject'] = transSaveObject;
	data['bill_type'] = transi_type_src;
	data['primary_key'] = primary_key;
	reportCommonAjax.call(that, props, url.reportToVoucherUrl, data, (res) => {
		if (res.success) {
			if (res && res.data) {
				toVoucherSuccess.call(that, props, res.data);
			}
		}
	});
}

/**
 * 联查凭证
 * @param {*} props 
 * @param {*} dataArray 
 */
function toVoucherSuccess(props, dataArray) {
	// 命名格式为 appid_LinkVouchar,其中appid为自己小应用的应用编码
	let appcode = this.props.appcode;
	let appId_LinkVouchar = appcode + linktoVoucher.linkCacheWord;
	openToVouchar(props, appId_LinkVouchar, dataArray);
}
