import { toast, ajax, formDownload } from 'nc-lightapp-front';
import { reportConfig } from '../const';
// import {getAccbookAndPeriod,getAccbookAndPeriodsFxValues,getOrgByAccbook} from '../../../common/components/AccbookAndPeriod';
// import {loginContext,getContext,loginContextKeys,setContext} from '../../../../ampub/common/components/AMInitInfo/loginContext';
// import { config } from '../../../common/components/AccbookAndPeriod/const';
// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';

import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { loginContext, getContext, loginContextKeys, setContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { AccbookAndPeriod } = fa_components;
const { config, getAccbookAndPeriod, getAccbookAndPeriodsFxValues, getOrgByAccbook } = AccbookAndPeriod;

const { url, fxs, allFx, areaCode } = reportConfig;
const { defaultField } = config;
/**
 * 查询区默认值设置
 * @param {*} searchId 
 * @param {*} props 
 */
export function setDefaultVal(searchId, props, flag, templateData) {
	this.bprops = props; //将平台props缓存以便后续使用
	let that = this;
	if (templateData) {
		let { template, context } = templateData;
		//上下文信息
		if (context) {
			loginContext(context);
		}
		//联查条件
		let linkReport = localStorage.getItem('LinkReport');
		if (!linkReport) {
			linkReport = sessionStorage.getItem('LinkReport');
		}
		//如果联查条件不是对象，转换为对象的形式
		if ('object' != typeof linkReport) {
			linkReport = JSON.parse(linkReport);
		}
		if (linkReport && flag) {
			if (linkReport.conditions && linkReport.conditions.length > 0) {
				props.search.setSearchValue(searchId, linkReport); //如果LinkReport有值，将值设置到查询区中
			} else {
				let userDefObj = localStorage.getItem('LinkReportUserDefObj'); //自定义传值
				if (!userDefObj) {
					userDefObj = sessionStorage.getItem('LinkReportUserDefObj');
				}
				//如果不是对象，转换为对象的形式
				if ('object' != typeof userDefObj) {
					userDefObj = JSON.parse(userDefObj);
				}
				//总账联查明细账处理
				if (userDefObj && userDefObj.linkType === 'gl') {
					let linkData = userDefObj && userDefObj.linkData;
					let condition = userDefObj && userDefObj.condition;
					let pk_accbook = linkData ? linkData.linkaccountingbook : '';
					let pk_unit = linkData ? linkData.pk_unit : '';
					//通过账簿获取组织，并将其与总账传递过来的值设置到查询区中
					getOrgByAccbook.call(that, pk_accbook, pk_unit, false, (res) => {
						if (res.success) {
							let { pk_org, pk_accbook } = res.data;
							let start_period = `${condition && condition.beginYear}-${condition &&
								condition.beginPeriod}`; //开始期间
							let end_period = `${condition && condition.endYear}-${condition && condition.endPeriod}`; //结束期间
							//设置查询区的值
							props.search.setSearchValByField(searchId, 'pk_org', pk_org);
							props.search.setSearchValByField(searchId, 'pk_accbook', pk_accbook);
							props.search.setSearchValByField(searchId, 'start_period', {
								value: start_period,
								display: start_period
							});
							props.search.setSearchValByField(searchId, 'end_period', {
								value: end_period,
								display: end_period
							});
							//对查询方案进行处理
							if (template) {
								let queryschemes = template[searchId].queryschemes; //查询方案数组
								queryAllFxValues(queryschemes, false, () => {
									queryschemes.map((queryscheme) => {
										clickPlanEve.call(that, props, queryscheme, false, false, false, false);
									});
								});
							}
							if (!pk_accbook || (pk_accbook && !pk_accbook.value)) {
								localStorage.removeItem('LinkReport');
								sessionStorage.removeItem('LinkReport');
								localStorage.removeItem('LinkReportUserDefObj');
								sessionStorage.removeItem('LinkReportUserDefObj');
								toast({
									content: getMultiLangByID('201204500-000041') /*国际化处理：该资产账簿未启用*/,
									color: 'warning'
								});
							} else {
								getAccbookPeriodScheme.call(that, props, [ { refpk: pk_accbook.value } ]); //设置会计期间方案
							}
						}
					});
				}
			}
		} else {
			if (template) {
				let queryschemes = template[searchId].queryschemes; //查询方案数组
				let hasQuick = false; //是否存在快速查询方案
				if (queryschemes) {
					queryAllFxValues(queryschemes, false, () => {
						queryschemes.map((queryscheme) => {
							if (queryscheme.isquickqs) {
								hasQuick = true;
								clickPlanEve.call(that, props, queryscheme, true, true, true, true); //查询方案点击前事件
							} else {
								clickPlanEve.call(that, props, queryscheme, false, false, false, false); //查询方案点击前事件
							}
						});
						if (!hasQuick) {
							//没有快速查询方案
							setAdvDefaultValue.call(that, props, searchId);
						}
					});
				}
			}
		}
	}
}

/**
 * 通过组织设置账簿和期间
 * @param {*} props 
 * @param {*} searchId 
 * @param {*} defConfig 
 * @param {*} defData 
 */
export function reportGetAccbookAndPeriod(props, searchId, defConfig, defData, callback) {
	let that = this;
	getAccbookAndPeriod(props, searchId, defConfig, defData, (backProps, res, moduleId) => {
		let success = res.success;
		let backData = res.data;
		if (success) {
			let accbook = {
				value: backData.accbook_value,
				display: backData.accbook_display
			};
			backProps.search.setSearchValByField(moduleId, defaultField.pk_accbook, accbook); //设置默认账簿
			let { accyear, period } = backData.period;
			let date = `${accyear}-${period}`;
			backProps.search.setSearchValByField(moduleId, defaultField.start_period, {
				value: date,
				display: date
			}); //起始期间
			backProps.search.setSearchValByField(moduleId, defaultField.end_period, {
				value: date,
				display: date
			}); //终止期间
			backProps.search.setSearchValByField(moduleId, defaultField.query_period, {
				value: date,
				display: date
			}); //会计期间
			getAccbookPeriodScheme.call(that, props, [ { refpk: accbook.value } ]);
			if (callback && 'function' === typeof callback) {
				callback(backProps, res, moduleId);
			}
		}
	});
}

/**
 * 通过账簿主键查询会计期间方案主键
 * @param {*} props 
 * @param {*} pk_accbook 
 */
export function getAccbookPeriodScheme(props, pk_accbook) {
	if (pk_accbook && pk_accbook.length == 1 && pk_accbook[0].refpk) {
		reportCommonAjax.call(
			this,
			props,
			url.queryAccbookPeriodSchemeUrl,
			{ pk_accbook: pk_accbook[0].refpk },
			(res) => {
				let { success, data } = res;
				if (success) {
					setPeriodScheme(data);
				}
			}
		);
	} else {
		setPeriodScheme(null); //无账簿或多账簿的时候，清空会计期间方案
	}
}

/**
 * 高级面板点击前事件
 */
export function clickAdvBtnEve(props, searchId) {
	//只有第一次打开的时候设置默认值
	if (this.isFirstLoad) {
		this.isFirstLoad = false;
		if (this.reportType == 'complex' && this.isLoaded) {
			let allIndex = this.times;
			let currentRows = this.props.editTable.getNumberOfRows(areaCode.tableId);
			let leftIndex = parseInt(allIndex) - parseInt(currentRows);
			for (let index = 0; index < leftIndex; index++) {
				this.props.editTable.addRow(areaCode.tableId, 0);
			}
		}
		setAdvDefaultValue.call(this, props, searchId);
	}
}

/**
 * 自定义打印接口(页维度连续打印)
 */
export function printExpendFun(data) {
	//导出
	let formDownloadUrl = url.pagePrintUrl;
	formDownload({
		params: data,
		url: formDownloadUrl,
		enctype: 2
	});
}

/**
 * 报表的后台请求基本方法
 * @param {} params 
 */
export function reportCommonAjax(props, url, data, success = (res) => {}, error) {
	let that = this;
	function defalutError(res) {
		if (res && res.message) {
			toast({ color: 'danger', content: res.message }); //提示错误信息
		}
	}
	ajax({
		url: url,
		data: data,
		success: success.bind(that),
		error: error || defalutError
	});
}

/**
 * 设置会计期间查询方案
 * @param {*} value 
 */
export function setPeriodScheme(value) {
	setContext('pk_accbookperiodscheme', value);
}
/**
 * 获取会计期间查询方案
 */
export function getPeriodScheme() {
	return getContext('pk_accbookperiodscheme');
}

/**
 * 获取查询区字段的值，如果没有值返回false
 * @param {*} props 
 * @param {*} searchId 
 * @param {*} fieldCode 
 */
export function getSearchFieldValue(props, searchId, fieldCode) {
	let flag = false;
	let field = props.search.getSearchValByField(searchId, fieldCode);
	//防止报undefined
	if (field && field.value && field.firstvalue) {
		flag = true;
		return field;
	}
	return flag;
}

/**
 * 设置高级查询面板查询字段默认值
 * @param {*} props 
 * @param {*} searchId 
 */
function setAdvDefaultValue(props, searchId) {
	let that = this;
	//明细账设置卡片列示默认值
	props.search.setSearchValByField(searchId, 'cardListShow', {
		value: 'N',
		display: getMultiLangByID('201204500-000003') /*国际化处理：否*/
	});
	//折旧计算明细表查询范围默认值
	props.search.setSearchValByField(searchId, 'query_area', {
		value: '1',
		display: getMultiLangByID('201204500-000004') /*国际化处理：计提折旧*/
	});
	//资产状态默认值
	props.search.setSearchValByField(searchId, 'fa_cardhistory.asset_state', {
		value: 'exist',
		display: getMultiLangByID('201204500-000005') /*国际化处理：在役卡片*/
	});
	//逾龄条件默认值
	props.search.setSearchValByField(searchId, 'overdueCondition', {
		value: 'checkMonth',
		display: getMultiLangByID('201204500-000006') /*国际化处理：查询期间减开始使用期间大于等于使用月限*/
	});
	let pk_org = getSearchFieldValue(props, searchId, 'pk_org'); //获取界面上的财务组织
	if (pk_org) {
		//页面模板得的默认值
		pk_org = pk_org.value.firstvalue;
	} else {
		//个性化中心的值
		pk_org = getContext(loginContextKeys.pk_org);
		let org_name = getContext(loginContextKeys.org_Name);
		if (pk_org && org_name) {
			props.search.setSearchValByField(searchId, 'pk_org', { value: pk_org, display: org_name });
		}
	}

	//根据组织和业务日期设置账簿、期间，如果组织不存在，只获取期间
	reportGetAccbookAndPeriod.call(that, props, searchId, null, {
		pk_org: pk_org,
		businessDate: getContext(loginContextKeys.businessDate)
	});
}

/**
 * 查询方案赋值前时间，需要绑定基础组件的this
 * @param {*} props props
 * @param {*} queryscheme 查询方案 
 * @param {*} isQuick 是否是快速查询
 * @param {*} isSetSearch 是否需要设置快速查询区值
 * @param {*} isLoading 是否需要显示加载框
 * @param {*} isSetPeriodScheme 是否需要设置会计期间方案
 */
export function clickPlanEve(
	props,
	queryscheme,
	isQuick = false,
	isSetSearch = true,
	isLoading = true,
	isSetPeriodScheme = true
) {
	let pk_org = ''; //要返回后台的值
	let default_org = getContext(loginContextKeys.pk_org); //个性化中心设置的默认主组织的主键
	let schemeOrg = ''; //查询方案中的设置的组织的主键
	let queryschemeStr = JSON.stringify(queryscheme);
	//如果查询方案中有组织，就进行判断，如果没有传到后台一个默认空字符串即可
	if (queryschemeStr.includes('pk_org')) {
		//获取查询方案中的组织主键
		queryscheme.renderobj4web &&
			queryscheme.renderobj4web.map((condition) => {
				if (condition.attrcode == 'pk_org') {
					schemeOrg = condition.initialvalue.value;
				}
			});
		//如果查询方案中组织主键是函数，则传个性化中心的值，否则传查询方案中的值
		if (allFx.includes(schemeOrg)) {
			pk_org = default_org;
		} else {
			pk_org = schemeOrg;
		}
	}
	//获取缓存中的函数翻译值
	let fxCacheFxValue = getContext(`${pk_org}fxValue`);
	//如果缓存中有，取缓存中的值，如果没有去后台查询(这里涉及到一个问题：数据的即时性。解决方法：刷新界面)
	if (fxCacheFxValue) {
		replaceFxValue.call(this, props, queryscheme, isQuick, fxCacheFxValue, isSetSearch, isSetPeriodScheme); //替换查询方案中的函数值
	} else {
		getAccbookAndPeriodsFxValues.call(this, pk_org, isLoading, (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setContext(`${pk_org}fxValue`, data); //将查询结果缓存
					replaceFxValue.call(this, props, queryscheme, isQuick, data, isSetSearch, isSetPeriodScheme); //替换查询方案中的函数值
				}
			}
		});
	}
}

/**
 * 替换查询方案中的函数值，需要绑定基础组件的this
 * @param {*} props 平台的props
 * @param {*} queryscheme 完整的单个查询方案
 * @param {*} isQuick 是否是快速查询 true/false
 * @param {*} data 查询方案的翻译值
 * @param {*} isSetSearch 是否需要设置查询区的值，默认是true
 */
function replaceFxValue(props, queryscheme, isQuick, data, isSetSearch = true, isSetPeriodScheme = true) {
	//普通查询区赋值
	//conditionobj4web值修改
	queryscheme.conditionobj4web &&
		queryscheme.conditionobj4web.public &&
		queryscheme.conditionobj4web.public.conditions &&
		queryscheme.conditionobj4web.public.conditions.map((condition) => {
			if (allFx.includes(condition.value.firstvalue)) {
				//如果值是函数
				let fx = fxs[condition.value.firstvalue];
				let fxValue = getFxValue(fx, data); //取出fx代表的真实值
				//替换查询方案中的值
				condition.value.firstvalue = fxValue.value;
				condition.display = fxValue.display;
			}
			if (condition.field && condition.field.includes('Level') && typeof condition.display == 'string') {
				condition.display = condition.display.split('~');
			}
		});
	// renderobj4web值修改;
	let searchValues = []; //要设置到查询区中的值
	let pk_accbook = '';
	queryscheme.renderobj4web &&
		queryscheme.renderobj4web.map((condition) => {
			if (allFx.includes(condition.initialvalue.value)) {
				//如果值是函数
				let fx = fxs[condition.initialvalue.value];
				let fxValue = getFxValue(fx, data); //取出fx代表的真实值
				//替换查询方案中的值
				condition.initialvalue.value = fxValue.value;
				condition.initialvalue.display = fxValue.display;
				//创建要设置到查询区中的字段值
				if (condition.attrcode === 'pk_accbook') {
					pk_accbook = condition.initialvalue.value;
				}
				let searchValue = {
					field: condition.attrcode, //字段编码
					display: condition.initialvalue.display, //显示值
					value: {
						firstvalue: condition.initialvalue.value,
						secondvalue: null
					},
					oprtype: condition.operationSign //操作符
				};
				searchValues.push(searchValue); //放到数组中
			}
			if (condition.attrcode && condition.attrcode.includes('Level') && condition.initialvalue) {
				if (typeof condition.initialvalue.display == 'string') {
					condition.initialvalue.display = [ condition.initialvalue.display, condition.initialvalue.display ];
				}
			}
		});
	//如果是快速查询区,直接修改界面的值
	isSetSearch && props.search.setSearchValue(areaCode.searchId, searchValues);
	if (isSetPeriodScheme) {
		getAccbookPeriodScheme.call(this, props, [ { refpk: pk_accbook } ]);
	}
	//查询对象赋值 快速查询无需赋值
	if (queryscheme.conditionobj4web && queryscheme.conditionobj4web.nonpublic && !isQuick && isSetSearch) {
		this.props.editTable.setTableData(areaCode.tableId, queryscheme.conditionobj4web.nonpublic); //设置查询对象表格值
	}
}

export function getFxValue(fx, data) {
	//以{mainaccbook{value:'',display''},...}形式返回
	let { mainaccbook, firstperiod, mainperiod, currentperiod } = data;
	let value = {};
	switch (fx) {
		case '#mainorg#':
			value.value = getContext(loginContextKeys.pk_org);
			value.display = getContext(loginContextKeys.org_Name);
			break;
		case '#accountingbook#':
		case '#mainaccbook#':
			value = mainaccbook;
			break;
		case '#firstaccperiod#':
			value = firstperiod;
			break;
		case '#currentaccperiod#':
			value = currentperiod;
			break;
		case '#mainaccperiod#':
			value = mainperiod;
			break;
		default:
			break;
	}
	value.value = value.value ? value.value : '';
	value.display = value.display ? value.display : '';
	return value;
}

/**
 * 保存查询方案前事件，用于将实际值替换为函数值
 */
export function saveSearchPlanEve() {
	let fxCache = this.fxCache;
	for (const key in fxCache) {
		if (fxCache.hasOwnProperty(key)) {
			const element = fxCache[key];
			this.bprops.search.setSearchValByField(areaCode.searchId, key, {
				value: element.fxvalue,
				display: element.display + ' '
			});
		}
	}
}

/**
 * 查询出查询方案中的所有fx的值并放入缓存中
 * @param {*} queryschemes 
 */
function queryAllFxValues(queryschemes, isLoading, callback) {
	let pk_orgs = '';
	if (queryschemes) {
		queryschemes.map((queryscheme) => {
			let pk_org = ''; //要返回后台的值
			let default_org = getContext(loginContextKeys.pk_org); //个性化中心设置的默认主组织的主键
			let schemeOrg = ''; //查询方案中的设置的组织的主键
			let queryschemeStr = JSON.stringify(queryscheme);
			//如果查询方案中有组织，就进行判断，如果没有传到后台一个默认空字符串即可
			if (queryschemeStr.includes('pk_org')) {
				//获取查询方案中的组织主键
				queryscheme.renderobj4web &&
					queryscheme.renderobj4web.map((condition) => {
						if (condition.attrcode == 'pk_org') {
							schemeOrg = condition.initialvalue.value;
						}
					});
				//如果查询方案中组织主键是函数，则传个性化中心的值，否则传查询方案中的值
				if (allFx.includes(schemeOrg)) {
					pk_org = default_org;
				} else {
					pk_org = schemeOrg;
				}
			}
			pk_orgs += `${pk_org},`;
		});
	}
	getAccbookAndPeriodsFxValues.call(this, pk_orgs, isLoading, (res) => {
		let { success, data } = res;
		if (success) {
			if (data) {
				for (const key in data) {
					if (data.hasOwnProperty(key)) {
						const element = data[key];
						setContext(`${key}fxValue`, element); //将查询结果缓存
					}
				}
				if (callback && 'function' === typeof callback) {
					callback();
				}
			}
		}
	});
}

/**
 * 模板打印
 * @param {*} nodekey 
 */
export function printUrlChange(nodekey) {
	let reg = /^\d+A$/;
	let printFileUrlStr = url.modelPrintUrl;
	if (reg.test(nodekey)) {
		return printFileUrlStr;
	}
}
