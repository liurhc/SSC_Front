import initTemplate, { clearDataByIndex } from './initTemplate';
import {
	setDefaultVal,
	afterEvent,
	clickAdvBtnEve,
	printExpendFun,
	reportCommonAjax,
	reportGetAccbookAndPeriod,
	setPeriodScheme,
	getPeriodScheme,
	getAccbookPeriodScheme,
	clickPlanEve,
	getFxValue,
	saveSearchPlanEve,
	printUrlChange
} from './util';
import { filter } from './filter';
import { complexValidator, existValidator, commonValidator } from './validator';
import { onAfterEvent } from './onAfterEvent';
import { disposeSearch } from './disposeSearch';
import { complexExpandSearchVal, simpleExpandSearchVal, groupToast } from './expandSearchVal';
import { setConnectionSearch } from './setConnectionSearch';

export {
	initTemplate,
	setDefaultVal,
	afterEvent,
	clearDataByIndex,
	clickAdvBtnEve,
	printExpendFun,
	filter,
	commonValidator,
	complexValidator,
	existValidator,
	onAfterEvent,
	disposeSearch,
	complexExpandSearchVal,
	simpleExpandSearchVal,
	reportCommonAjax,
	getAccbookPeriodScheme,
	reportGetAccbookAndPeriod,
	setPeriodScheme,
	getPeriodScheme,
	setConnectionSearch,
	groupToast,
	clickPlanEve,
	getFxValue,
	saveSearchPlanEve,
	printUrlChange
};
