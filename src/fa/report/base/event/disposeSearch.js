import { filter } from '.';
/**
 * 参照过滤方法
 * @param {*} meta 
 * @param {*} props 
 */
export function disposeSearch(meta, props) {
	filter.call(this, meta, props); //添加参照过滤
	return meta;
}
