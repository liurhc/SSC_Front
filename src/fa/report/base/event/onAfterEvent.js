import { reportGetAccbookAndPeriod, getAccbookPeriodScheme, getFxValue } from '.';
import { reportConfig } from '../const';

// import { referLinkageClear } from '../../../common/components/ReferLinkage';
// import { getAccbookAndPeriodsFxValues } from '../../../common/components/AccbookAndPeriod';
// import { getContext, setContext } from '../../../../ampub/common/components/AMInitInfo/loginContext';
// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { components, utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { getContext, setContext } = LoginContext;
import fa from 'fa';
const { fa_components } = fa;
const { ReferLinkage, AccbookAndPeriod } = fa_components;
const { referLinkageClear } = ReferLinkage;
const { getAccbookAndPeriodsFxValues } = AccbookAndPeriod;

const { queryConfig } = reportConfig;
/**
 * 编辑后事件，用于关联查询区条件
 * props: props
 * searchId: 查询区需要的searchId参数
 * field: 编辑后的key
 * val: 编辑后的value
*/
export function onAfterEvent(props, searchId, field, val) {
	let that = this;
	let org = props.search.getSearchValByField(searchId, 'pk_org');
	let pk_org = org && org.value && org.value.firstvalue;
	let fun = {
		accbookAfterChoseFx: (data) => {
			props.search.setSearchValByField(searchId, field, getFxValue(val[0].refpk, data));
			that.fxCache[field] = {
				fxvalue: '#mainaccbook#',
				fxdisplay: getMultiLangByID('201204500-000002') /*国际化处理：默认主账簿*/,
				display: getFxValue(val[0].refpk, data).display,
				value: getFxValue(val[0].refpk, data).value
			};
			val[0].refpk = getFxValue(val[0].refpk, data).value;
			getAccbookPeriodScheme.call(that, props, val); //获取该账簿的会计期间方案
		},
		periodAfterChoseFx: (data) => {
			props.search.setSearchValByField(searchId, field, getFxValue(val.refpk, data));
			that.fxCache[field] = {
				fxvalue: val.refpk,
				fxdisplay: val.refname,
				display: getFxValue(val.refpk, data).display,
				value: getFxValue(val.refpk, data).value
			};
		}
	};
	if (field === 'pk_org') {
		referLinkageClear.call(that, props, field, queryConfig);
		props.search.setSearchValByField(searchId, 'pk_accbook', {}); //清空账簿
		reportGetAccbookAndPeriod.call(that, props, searchId, null, null);
	}
	if (field === 'pk_accbook') {
		if (val && val.length == 1 && (val[0].refpk === '#accountingbook#' || val[0].refpk === '')) {
			val[0].refpk = '#accountingbook#';
		}
		if (val && val.length == 1 && localStorage) {
			let accbookRefer = localStorage.getItem('referHistory');
			accbookRefer = JSON.parse(accbookRefer);
			if (accbookRefer) {
				let last = accbookRefer['/nccloud/uapbd/ref/AccountBookTreeRef.do'];
				if (last && last[0]) {
					val[0].refpk = last[0].refpk;
				}
			}
		}
		if (val && val.length == 1 && val[0].refpk === '#accountingbook#') {
			//多组织选择默认账簿函数置空
			if (pk_org && pk_org.includes(',')) {
				props.search.setSearchValByField(searchId, field, {});
			} else {
				//获取缓存中的函数翻译值
				let fxCacheFxValue = getContext(`${pk_org}fxValue`);
				if (fxCacheFxValue) {
					fun.accbookAfterChoseFx(fxCacheFxValue);
				} else {
					getAccbookAndPeriodsFxValues.call(that, pk_org, true, (res) => {
						if (res.success) {
							setContext(`${pk_org}fxValue`, res.data); //将查询结果缓存
							fun.accbookAfterChoseFx(res.data);
						}
					});
				}
			}
		} else {
			getAccbookPeriodScheme.call(that, props, val); //获取该账簿的会计期间方案
		}
	}
	if (field === 'query_period' || field === 'start_period' || field === 'end_period') {
		if (val.refpk && !val.refpk.includes('-')) {
			if (pk_org && pk_org.includes(',')) {
				pk_org = '';
			}
			let fxCacheFxValue = getContext(`${pk_org}fxValue`);
			if (fxCacheFxValue) {
				fun.periodAfterChoseFx(fxCacheFxValue);
			} else {
				getAccbookAndPeriodsFxValues.call(that, pk_org, true, (res) => {
					if (res.success) {
						setContext(`${pk_org}fxValue`, res.data); //将查询结果缓存
						fun.periodAfterChoseFx(res.data);
					}
				});
			}
		}
	}
}
