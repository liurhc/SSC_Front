import { reportConfig } from '../const';

// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { validatorMsg, allFx, code } = reportConfig;
const { complex, common } = validatorMsg;
/**
 * 校验每一个查询对象的内容
 * @param {*} props 
 * @param {*} value 
 */
export function complexValidator(props, tableDataStr, values) {
	let validateRes = new Object();
	validateRes.valid = false; //校验是否通过,false是不通过，true是通过
	validateRes.msg = []; //校验信息
	validateRes.other = {};

	if (values.begin_level.value > values.end_level.value) {
		validateRes.msg.push(`${values.query_obj.display}${getMultiLangByID(complex.levelError)}`);
	}
	if (tableDataStr.indexOf(values.query_obj.display) != tableDataStr.lastIndexOf(values.query_obj.display)) {
		validateRes.msg.push(`${values.query_obj.display}${getMultiLangByID(complex.objectRepeatError)}`);
	}
	if (!tableDataStr.includes('body')) {
		validateRes.msg.push(`${getMultiLangByID(complex.viewPosionError)}`);
	}

	//所有校验都通过
	if (validateRes.msg.length <= 0) {
		validateRes.valid = true;
	}
	return validateRes;
}
/**
 * 校验是否存在查询对象，以及是否有空行
 * @param {*} props 
 * @param {*} items 
 * @param {*} tableData 
 */
export function existValidator(props, items, tableData) {
	let validateRes = new Object();
	validateRes.valid = false; //校验是否通过,false是不通过，true是通过
	validateRes.msg = []; //校验信息
	validateRes.other = {};

	//判断是否存在查询对象
	if (tableData.length <= 0 || (tableData.length == 1 && tableData[0].values.query_obj.value == null)) {
		validateRes.msg.push(`${getMultiLangByID(complex.objectNullError)}`);
	}
	//如果是增减变动查询，必须选择增减方式
	if (this && this.props.appcode && this.props.appcode === code.addreduce.appcode) {
		let tableDataStr = JSON.stringify(tableData);
		if (!tableDataStr.includes('pk_addreducestyle')) {
			validateRes.msg.push(`${getMultiLangByID(complex.addreduceObjectError)}`);
		}
	}

	//所有校验都通过
	if (validateRes.msg.length <= 0) {
		validateRes.valid = true;
	}
	return validateRes;
}

/**
 * 查询区校验
 * @param {*} props 
 * @param {*} items 
 */
export function commonValidator(props, items) {
	let validateRes = new Object();
	validateRes.valid = false; //校验是否通过,false是不通过，true是通过
	validateRes.msg = []; //校验信息
	validateRes.other = {};

	if (!items) {
		validateRes.msg.push(getMultiLangByID(common.noConditionError));
	}
	//一次遍历，获取所有需要校验的值
	let meta = this.bprops.meta.getMeta();
	let validItem = {};
	let betweenItems = []; //介于的值
	for (let index = 0; index < items.length; index++) {
		let item = items[index];
		let hasSecond = false;
		switch (item.field) {
			case 'categoryLevel':
			case 'usedeptLevel':
			case 'usestatusLevel':
			case 'styleLevel':
			case 'mandeptLevel':
			case 'usingstatusLevel':
				hasSecond = true;
			case 'pk_accbook':
			case 'query_period':
			case 'start_period':
			case 'end_period':
			case 'overBoundMonth_Min':
			case 'overBoundMonth_Max':
				if (item.value.firstvalue) {
					if (allFx.includes(item.value.firstvalue)) {
						item.value.firstvalue = this.fxCache[item.field].value;
					}
					validItem[item.field] = item.value.firstvalue;
				}
				if (hasSecond) {
					if (item.value.secondvalue) {
						if (validItem[item.field]) {
							validItem[item.field] = `${validItem[item.field]}@@${item.value.secondvalue}`;
						} else {
							validItem[item.field] = `@@${item.value.secondvalue}`;
						}
					}
				}
				break;
			//需要校验的项，按照上述方式获取值，在下方进行校验
			default:
				if (item.oprtype && item.oprtype == 'between') {
					betweenItems.push({
						fieldName: (() => {
							let label = '';
							meta &&
								meta.light_report &&
								meta.light_report.items &&
								meta.light_report.items.map((metaItem) => {
									if (!label && metaItem.attrcode == item.field) {
										label = metaItem.label;
									}
								});
							return label;
						})(),
						field: item.field,
						value: item.value
					});
				}
				break;
		}
	}

	//校验介于条件是否只输入了一个值
	betweenItems.map((betweenItem) => {
		let value = betweenItem.value;
		if (!value.firstvalue || !value.secondvalue) {
			validateRes.msg.push(`${betweenItem.fieldName}${getMultiLangByID(common.betweenError)}`);
		}
	});

	//"#currentaccperiod#""#firstaccperiod#""#mainaccperiod#"这三个函数如果在前台校验的话需要发请求，所以放到后台校验
	//开始期间不能大于结束期间（判断这两个是否存在，如果存在再进行校验）
	if (validItem['start_period'] && validItem['end_period']) {
		if (allFx.includes(validItem['start_period']) || allFx.includes(validItem['end_period'])) {
		} else {
			if (validItem['start_period'] > validItem['end_period']) {
				validateRes.msg.push(getMultiLangByID(common.dateError));
			}
		}
	}
	//类别级次、方式级次、部门级次、使用状况级次等 起始级次不能大于结束级次
	let levelNames = [
		'categoryLevel',
		'usedeptLevel',
		'usestatusLevel',
		'styleLevel',
		'mandeptLevel',
		'usingstatusLevel'
	];
	levelNames.map((levelName) => {
		if (validItem[levelName]) {
			let isBoth = true; //是否需要比较
			let label = '';
			meta &&
				meta.light_report &&
				meta.light_report.items &&
				meta.light_report.items.map((metaItem) => {
					if (metaItem.attrcode == levelName) {
						label = metaItem.label;
					}
				});
			if (validItem[levelName].startsWith('@@')) {
				isBoth = false; //没有起始级次
				validateRes.msg.push(
					`${getMultiLangByID('201204500-000047')}${label}${getMultiLangByID('201204500-000017')}`
				);
			}
			if (!validItem[levelName].includes('@@')) {
				isBoth = false; //没有结束级次
				validateRes.msg.push(
					`${getMultiLangByID('201204500-000047')}${label}${getMultiLangByID('201204500-000048')}`
				);
			}
			if (isBoth) {
				let levels = validItem[levelName].split('@@');
				//起始级次大于结束级次 或者 起始级次为明细级结束级次不是明细级
				if ((levels[1] != 0 && levels[0] > levels[1]) || (levels[0] == 0 && levels[1] != 0)) {
					validateRes.msg.push(`${label}${getMultiLangByID('201204500-000049')}`);
				}
			}
		}
	});

	//逾龄统计表范围校验
	let reg1 = new RegExp(`^\\d+${getMultiLangByID('201204500-000034')}\\d+${getMultiLangByID('201204500-000035')}$`); //某年某月
	let reg2 = new RegExp(`^\\d+${getMultiLangByID('201204500-000034')}$`); //某年
	let reg3 = new RegExp(`^\\d+${getMultiLangByID('201204500-000035')}$`); //某月
	let reg4 = /^\d+$/; //正则表达，正整数
	let minVal = '';
	let maxVal = '';
	if (validItem['overBoundMonth_Min']) {
		minVal = validItem['overBoundMonth_Min'];
	}
	if (validItem['overBoundMonth_Max']) {
		maxVal = validItem['overBoundMonth_Max'];
	}
	//如果值是空也校验通过
	if (minVal) {
		if (reg1.test(minVal) || reg2.test(minVal) || reg3.test(minVal) || (reg4.test(minVal) && minVal > 0)) {
			//minVal正常
		} else {
			validateRes.msg.push(getMultiLangByID(common.overBoundMonth_MinError));
		}
	}

	if (maxVal) {
		if (reg1.test(maxVal) || reg2.test(maxVal) || reg3.test(maxVal) || (reg4.test(maxVal) && maxVal > 0)) {
			//maxVal正常
		} else {
			validateRes.msg.push(getMultiLangByID(common.overBoundMonth_MaxError));
		}
	}

	//所有校验都通过
	if (validateRes.msg.length <= 0) {
		validateRes.valid = true;
	}
	return validateRes;
}
