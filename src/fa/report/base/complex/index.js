import React, { Component } from 'react';
import { SimpleReport } from 'nc-report';
import { createPage } from 'nc-lightapp-front';
import {
	initTemplate,
	setDefaultVal,
	setConnectionSearch,
	clearDataByIndex,
	complexExpandSearchVal,
	disposeSearch,
	clickAdvBtnEve,
	printExpendFun,
	onAfterEvent,
	setPeriodScheme,
	clickPlanEve,
	saveSearchPlanEve,
	printUrlChange
} from '../event';
import '../index.less';
import { reportConfig } from '../const';

// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const { areaCode } = reportConfig;

/**
 * 带查询对象的报表
 */

class Complex extends Component {
	constructor(props) {
		super(props);
		this.state = {
			printOther: {}
		};
		setPeriodScheme(''); //设置会计期间方案
		this.isLoaded = false;
		this.isFirstLoad = true; //高级面板是否第一次打开
		this.times = (() => {
			if (props.planB) {
				return 9;
			} else if (props.planC) {
				return 3;
			} else {
				return 8;
			}
		})();
		this.reportType = 'complex';
		this.fxCache = {}; //编辑后的查询方案暂存
		this.bprops = {};
	}

	componentDidMount() {
		const printOther = [
			{ key: 'pagePrint', display: getMultiLangByID('201204500-000000'), disabled: false }
		]; /*国际化处理：页维度连续打印*/
		//udpate by dongzheng9 20181123 如果是联查过来的没有页维度 不显示页维度打印按钮
		let drillCode = this.props && this.props.getUrlParam && this.props.getUrlParam('drillCode');
		if (!drillCode) {
			this.setState({
				printOther
			});
		}
	}

	// 清空值，清空查询对象
	advSearchClearEve = () => {
		if (this.props.advSearchClearEve) {
			this.props.advSearchClearEve();
		} else {
			for (let index = 0; index < this.times; index++) {
				clearDataByIndex(this.props, index);
			}
		}
	};

	// 添加高级查询区自定义查询条件Dom
	addAdvBody = () => {
		let { editTable } = this.props;
		let { createEditTable } = editTable;
		this.isLoaded = true;
		return (
			<div className="nc-bill-table-area innerTable">
				{createEditTable(areaCode.tableId, {
					showIndex: true,
					height: '150px',
					onAfterEvent: this.onAfterEvent.bind(this)
				})}
			</div>
		);
	};

	//查询对象编辑后事件 除了增减方式默认终止级次为1
	onAfterEvent = (props, moduleId, item, field, value, index) => {
		if (item == 'query_obj') {
			if (field != 'pk_addreducestyle') {
				props.editTable.setValByKeyAndIndex(moduleId, index, 'end_level', {
					value: '1',
					display: '1',
					scale: 0,
					isEdit: false
				});
			} else {
				props.editTable.setValByKeyAndIndex(moduleId, index, 'end_level', {
					value: '2',
					display: '2',
					scale: 0,
					isEdit: false
				});
			}
			props.editTable.setValByKeyAndIndex(moduleId, index, 'begin_level', {
				value: '1',
				display: '1',
				scale: 0,
				isEdit: false
			});
			props.editTable.setValByKeyAndIndex(moduleId, index, 'view_posion', {
				value: 'body',
				display: getMultiLangByID('201204500-000001') /*国际化处理：表体*/,
				scale: 0,
				isEdit: false
			});
			this.props.editTable.setRowStatus(moduleId, index, 1);
		}
	};

	//保存查询方案
	saveSearchPlan = () => {
		saveSearchPlanEve.call(this);
		let tableData = this.props.editTable.getAllData(areaCode.tableId); //获取查询对象数据
		return tableData;
	};

	render() {
		return (
			<div className="table">
				<SimpleReport
					expandSearchVal={this.props.expandSearchVal || complexExpandSearchVal.bind(this)}
					disposeSearch={this.props.disposeSearch || disposeSearch.bind(this)}
					showAdvBtn={true}
					advSearchClearEve={this.props.advSearchClearEve || this.advSearchClearEve.bind(this)} //清空全部
					addAdvBody={this.props.addAdvBody || this.addAdvBody.bind(this)} //增加自定义查询
					onAfterEvent={this.props.onAfterEvent || onAfterEvent.bind(this)}
					addAdvTabs={this.props.addAdvTabs}
					replaceAdvBody={this.props.replaceAdvBody}
					replaceRightBody={this.props.replaceRightBody}
					saveSearchPlan={this.props.saveSearchPlan || this.saveSearchPlan.bind(this)} //查询方案保存前操作
					clickPlanEve={this.props.clickPlanEve || clickPlanEve.bind(this)} //查询方案点击前操作
					CreateNewSearchArea={this.props.CreateNewSearchArea} //按扭区域新增按钮
					setConnectionSearch={this.props.setConnectionSearch || setConnectionSearch.bind(this)}
					showHighSearchBtn={true}
					showSearchArea={true}
					clickAdvBtnEve={this.props.clickAdvBtnEve || clickAdvBtnEve.bind(this)}
					setDefaultVal={this.props.setDefaultVal || setDefaultVal.bind(this)}
					printOther={this.state.printOther}
					printExpendFun={this.props.printExpendFun || printExpendFun.bind(this)}
					uSearchWidth={200}
					printWidth={200}
					printUrlChange={this.props.printUrlChange || printUrlChange.bind(this)}
				/>
			</div>
		);
	}
}
Complex = createPage({ initTemplate: initTemplate })(Complex);
export default Complex;
