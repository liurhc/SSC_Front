import React, { Component } from 'react';
import { SimpleReport } from 'nc-report';
import { createPage } from 'nc-lightapp-front';
import {
	setDefaultVal,
	setConnectionSearch,
	disposeSearch,
	simpleExpandSearchVal,
	onAfterEvent,
	setPeriodScheme,
	clickAdvBtnEve,
	printExpendFun,
	clickPlanEve,
	saveSearchPlanEve,
	printUrlChange
} from '../event';

/**
 * 不带查询对象的报表
 */
class Simple extends Component {
	constructor(props) {
		super(props);
		this.state = {
			printOther: '' //自定义打印字段
		};
		this.isFirstLoad = true; //高级面板是否第一次打开
		setPeriodScheme(''); //设置会计期间查询方案
		this.bprops = {}; //平台props
		this.reportType = 'simple'; //报表类型
		this.fxCache = {}; //编辑后的查询方案暂存
	}

	componentDidMount() {
		const printOther = this.props.printOther; //获取父组件传递的扩展打印参数
		this.setState({
			printOther
		});
	}
	// 添加高级查询区自定义查询条件Dom
	addAdvBody = () => {
		//不做任何操作，主要目的是可以调用clickPlanEve方法
	};

	render() {
		return (
			<div className="table">
				<SimpleReport
					addAdvBody={this.props.addAdvBody || this.addAdvBody.bind(this)} //查询区组件高级面板中增加自定义查询区域
					clickPlanEve={this.props.clickPlanEve || clickPlanEve.bind(this)} //查询方案点击前事件
					expandSearchVal={
						(this.props.expandSearchVal && this.props.expandSearchVal.bind(this)) ||
						simpleExpandSearchVal.bind(this)
					} //查询条件返回后台前对返回值的处理与扩展
					disposeSearch={this.props.disposeSearch || disposeSearch.bind(this)} //处理参照过滤
					showAdvBtn={true} //是否显示查询区高级按钮
					printUrlChange={this.props.printUrlChange || printUrlChange.bind(this)} //对不用打印操作使用不同的打印请求
					onAfterEvent={
						(this.props.onAfterEvent && this.props.onAfterEvent.bind(this)) || onAfterEvent.bind(this)
					} //查询区编辑后事件
					setDefaultVal={
						(this.props.setDefaultVal && this.props.setDefaultVal.bind(this)) || setDefaultVal.bind(this)
					} //界面加载的时候默认值处理
					setConnectionSearch={this.props.setConnectionSearch || setConnectionSearch.bind(this)} //报表联查处理
					clickAdvBtnEve={this.props.clickAdvBtnEve || clickAdvBtnEve.bind(this)} //查询区高级按钮点击前事件
					printOther={this.state.printOther} //打印下拉菜单中的扩展按钮
					printExpendFun={this.props.printExpendFun || printExpendFun.bind(this)} //自定义打印接口
					uSearchWidth={200} //联查下拉菜单的宽度
					printWidth={200} //打印下拉菜单的宽度
					saveSearchPlan={this.props.saveSearchPlan || saveSearchPlanEve.bind(this)} //查询方案保存前操作
				/>
			</div>
		);
	}
}

Simple = createPage({})(Simple);
export default Simple;
