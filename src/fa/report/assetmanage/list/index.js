import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Simple } from '../../base';

// import { getMultiLangByID, initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule, getMultiLangByID } = multiLangUtils;

/**
 * 卡片台账
 */
export class Assetmanage extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.printOther = [
			{ key: 'mandeptPrint', display: getMultiLangByID('201204500-000036') /*国际化处理：打印清单:按管理部门*/ },
			{ key: 'usedeptPrint', display: getMultiLangByID('201204500-000037') /*国际化处理：打印清单:按使用部门*/ },
			{ key: 'categoryPrint', display: getMultiLangByID('201204500-000038') /*国际化处理：打印清单:按资产类别*/ },
			{ key: 'usecategoryPrint', display: getMultiLangByID('201204500-000039') /*国际化处理：打印清单:按使用部门和资产类别*/ },
			{ key: 'mandcategoryPrint', display: getMultiLangByID('201204500-000040') /*国际化处理：打印清单:按管理部门和资产类别*/ }
		];
	}
	printUrlChange = (nodekey) => {
		//默认值平台提供的公共url
		let printFileUrlStr = '/nccloud/report/widget/printreportbypdfaction.do';
		switch (nodekey) {
			case '201204504A_notchild':
				printFileUrlStr = '/nccloud/fa/report/assetnotchildprintreportbypdfaction.do';
				break;
			case '201204504A_havechild':
				printFileUrlStr = '/nccloud/fa/report/assethavechildprintreportbypdfaction.do';
				break;
		}
		return printFileUrlStr;
	};
	render() {
		return <Simple printUrlChange={this.printUrlChange.bind(this)} printOther={this.printOther} {...this.props} />;
	}
}
initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Assetmanage />, document.getElementById('app'));
});
