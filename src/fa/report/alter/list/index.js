import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Simple } from '../../base';
import { setDefaultVal } from '../../base/event';
import { getBusinessInfo } from 'nc-lightapp-front';

// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

/**
 * 卡片台账
 */
export class Alter extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	//变动单台账的特殊处理，设置默认值
	setDefaultVal(searchId, props, flag, templateData) {
		setDefaultVal.call(this, searchId, props, flag, templateData);
		//变动单台账的特殊处理,设置制单日期，需要在模板设置中将字段设为默认显示才可以设置
		let businessInfo = getBusinessInfo(); //业务日期
		//临时处理获取不到businessInfo
		if (!businessInfo) {
			businessInfo = window.parent.businessInfo;
		}
		if (!businessInfo) {
			return;
		}
		let bussinessDate = businessInfo.bussinessDate;
		let billDate = []; //制单
		if (bussinessDate) {
			let year = bussinessDate.substr(0, 4);
			let month = bussinessDate.substr(5, 2);
			let day = bussinessDate.substr(8, 2);
			let time = bussinessDate.split(' ')[1];
			billDate.push(`${year}-${month}-01 00:00:00`);
			billDate.push(`${year}-${month}-${day} 23:59:59`);
			props.search.setSearchValByField(searchId, 'fa_alter.billmaketime', {
				value: [ billDate[0], billDate[1] ],
				display: ''
			});
		}
	}
	render() {
		return <Simple setDefaultVal={this.setDefaultVal} {...this.props} />;
	}
}

initMultiLangByModule({ fa: [ '201204500' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<Alter />, document.getElementById('app'));
});
