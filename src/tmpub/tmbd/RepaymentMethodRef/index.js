import { high } from 'nc-lightapp-front';
const { Refer } = high;

export default function( props = {} ) {
    let conf = {
        refName: '还款方式',
        refcode: 'tmpub.refer.tmbd.RepaymentMethodRef',
        queryGridUrl: '/nccloud/tmpub/refer/repaymentmethodgridref.do',
        refType: 'grid',
        columnConfig:[{
            name: ['编码', '还款方式名称', '还本方式','还本周期','付息方式','付息周期'], 
            code: ['code', 'name', 'repay_prcpl_method', 'repay_prcpl_period', 'repay_intst_method', 'repay_intst_period']
        }]
    };
    return <Refer {...Object.assign(conf, props)} />
}