import { high } from 'nc-lightapp-front';
const { Refer } = high;

export default function( props = {} ) {
    let conf = {
        refName: '非银行金融机构类别',
        refcode: 'tmpub.refer.tmbd.NonbankTypeGridRef',
        queryGridUrl: '/nccloud/tmpub/refer/nonbanktypegridref.do',
        refType: 'grid',
        columnConfig:[{
            name: ['名称', '编码'], 
            code: ['name', 'code']
        }]
    };
    return <Refer {...Object.assign(conf, props)} />
}