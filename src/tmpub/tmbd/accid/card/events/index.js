import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import initButton from './initButton';
import pageInfoClick from './pageInfoClick';
export { buttonClick, afterEvent, initTemplate, initButton, pageInfoClick};
