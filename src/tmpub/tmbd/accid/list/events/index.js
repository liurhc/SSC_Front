import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import tableButtonClick from './tableButtonClick';
import setButtonUsability  from './setButtonUsability';
export { searchBtnClick, pageInfoClick, initTemplate, buttonClick, tableButtonClick, setButtonUsability };
