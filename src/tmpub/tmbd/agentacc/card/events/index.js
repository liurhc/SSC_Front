import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import buttonVisible from './buttonVisible';
export { buttonClick, afterEvent, initTemplate, pageInfoClick, buttonVisible };
