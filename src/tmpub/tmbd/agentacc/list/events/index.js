import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import onSearchAfterEvent from './searchAfterEvent';
import buttonUsability  from './buttonUsability';
export { searchBtnClick, pageInfoClick, initTemplate, buttonClick, onSearchAfterEvent, buttonUsability };