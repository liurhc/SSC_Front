import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default  function( props = {} ) {
    let conf = {
        multiLang: {
            domainName: 'tmpub',
            currentLocales: 'zh-CN',
            moduleId: 'tmpubRefer'
        },
        refType: 'grid',
        refName: 'refer-0021',
        refCode: 'tmpub.refer.tmbd.CreditOrgListRef',
        queryGridUrl: '/nccloud/tmpub/refer/CCTypeGridRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [{name : ['refer-0005', 'refer-0010'],code: ['refcode', 'refname']}]
    };

    return <Refer {...Object.assign(conf,props)} />
}