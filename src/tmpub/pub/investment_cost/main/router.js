import { asyncComponent } from 'nc-lightapp-front';
import ConferTable from '../list';

const card = asyncComponent(() => import(/* webpackChunkName: "cmp/billmanagement/confer/card/card" */'../card'));

const routes = [
	{
		path: '/',
		component: ConferTable,
		exact: true
	},
	{
		path: '/list',
		component: ConferTable
	},
	{
		path: '/card',
		component: card
	}
];

export default routes;
