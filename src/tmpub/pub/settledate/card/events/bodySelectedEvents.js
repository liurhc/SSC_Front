export function bodySelectedEvent() {
	let checkedRows = this.props.cardTable.getCheckedRows(this.tableId);
	if (checkedRows.length > 0) {
		if (checkedRows.length == 1) {
			this.props.button.setButtonDisabled([ 'deleteRow', 'copyRow' ], false);
		} else {
			this.props.button.setButtonDisabled([ 'deleteRow' ], false);
		}
	} else {
		this.props.button.setButtonDisabled([ 'deleteRow', 'copyRow' ], true);
	}
}
