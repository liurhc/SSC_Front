import { list, card, appCode, btnLimit } from '../../cons/constant.js';
import { searchBtnOperation } from '../../../public/container/list/search';
import { listBodyBtnClick } from '../../../public/container/list/listBodyBtnClick.js';

export default function(props, json, inlt) {
	props.createUIDom(
		{
			pagecode: this.pageId, //页面code
			appcode: appCode
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta, json);
					props.meta.setMeta(meta);
					this.setState({ showToast: false });
					searchBtnOperation.call(this, props);
				}
				if (data.button) {
					/* 按钮适配  第一步：将请求回来的按钮组数据设置到页面的 buttons 属性上 */
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('delete', json['36010ISDC-000021']); /* 国际化处理： 确认要删除吗?*/
					props.button.setButtonDisabled(list.disabled_btn, true);
				}
			}
		}
	);
}

function modifierMeta(props, meta, json) {
	meta[this.tableId].pagination = true;
	meta[this.tableId].items = meta[this.tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'code') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{ cursor: 'pointer' }}
						onClick={(e) => {
							e.stopPropagation();
							props.pushTo('/card', {
								status: 'browse',
								id: record[this.primaryId].value,
								pagecode: card.pageCode,
								sysMark: record.advanceddata.value
							});
						}}
					>
						{record && record.code && record.code.value}
					</a>
				);
			};
		}
		if (item.attrcode == 'code') {
			item.width = '80px';
		}
		if (item.attrcode == 'settleway') {
			item.width = '100px';
		}
		if (item.attrcode == 'settlecycle') {
			item.width = '90px';
		}
		if (item.attrcode == 'cycleunit') {
			item.width = '100px';
		}
		if (item.attrcode == 'endsettledate') {
			item.width = '81px';
		}
		return item;
	});

	//添加操作列
	meta[this.tableId].items.push({
		itemtype: 'customer',
		attrcode: 'opr',
		label: json['36010ISDC-000015'] /* 国际化处理： 操作*/,
		width: 200,
		fixed: 'right',
		className: 'table-opr',
		visible: true,
		render: (text, record, index) => {
			let buttonAry = [];
			let status = record.enablestate && record.enablestate.value;
			switch (status) {
				case '0':
					buttonAry = [ 'delete', 'stop', 'edit' ];
					break;
				case '1':
					buttonAry = [ 'delete', 'start', 'edit' ];
					break;
			}
			return props.button.createOprationButton(buttonAry, {
				area: list.bodyCode,
				buttonLimit: btnLimit,
				onButtonClick: (props, key) => listBodyBtnClick.call(this, key, record)
			});
		}
	});
	return meta;
}
