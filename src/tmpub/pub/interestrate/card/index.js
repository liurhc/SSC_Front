/* 
 基准利率设置卡片页
 created by：liyaoh 2018-08-13
*/

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, high, cardCache } from 'nc-lightapp-front';
import {
	getCardData,
	pageClick,
	setItemDisabled,
	setEditStatus,
	clearAll,
	initForm,
	initByBtn,
	billHeadVisible
} from './events/page';
import { CARD, API_URL, app_code, DATA_SOURCE, nodekey } from '../cons/constant.js';
import {
	initTemplate,
	buttonClick,
	afterEvent,
	buttonVisible,
	bodyButtonClick,
	afterTableEvent,
	bodySelectedEvent,
	bodySelectedAllEvent
} from './events';
let { updateCache, getNextId, deleteCacheById } = cardCache;
let { NCScrollElement, NCAffix } = base;
const { PrintOutput } = high;

export default class Card extends Component {
	constructor(props) {
		super(props);
		this.appcode = app_code;
		this.formId = CARD.form_id;
		this.primaryId = CARD.primary_id; //主键ID
		this.pageId = CARD.page_id;
		this.dataSource = DATA_SOURCE; //缓存key
		this.nodekey = nodekey;
		this.tabCode = CARD.tab_code; //tab区域code编码
		this.tabOrder = CARD.tab_order; //tab排序
		this.interestId = CARD.interest_id;
		this.moduleId = props.moduleId;
		if (this.props.pageType === 'group') {
			//集团
			this.appcode = '36010IRCG';
			this.pageId = '36010IRCG_card';
			this.nodekey = '36010IRCG_card';
			this.dataSource = 'tm.pub.interestrateGroup.datasource';
			this.moduleId = '36010IRCG';
		} else if (props.pageType === 'global') {
			//全局
			this.appcode = '36010IRC';
			this.pageId = '36010IRC_card';
			this.nodekey = '36010IRC_card';
			this.dataSource = 'tm.pub.interestrateGlobal.datasource';
			this.moduleId = '36010IRC';
		}
		this.state = {
			//输出用
			outputData: {
				funcode: '', //功能节点编码，即模板编码
				nodekey: '', //模板节点标识
				oids: [],
				outputType: 'output'
			}
		};
		initTemplate.call(this, props);
	}

	componentWillMount() {
		let callback = (json, status, inlt) => {
			if (status) {
				initTemplate.call(this, this.props);
				this.setState({ json, inlt });
				this.afterGetLang(json);
			} else {
				console.log('未加载到多语资源');
			}
		};
		this.props.MultiInit.getMultiLang({ moduleId: [ this.moduleId, '36010IR' ], domainName: 'tmpub', callback });
	}

	afterGetLang() {
		window.onbeforeunload = () => {
			if (![ 'browse' ].includes(this.props.getUrlParam('status'))) {
				return this.state.json['36010IR-000050'];
			}
		};
	}

	componentWillUnmount() {
		window.onbeforeunload = () => {
			// 停止事件
		};
	}

	componentDidMount() {}

	//返回按钮事件配置
	handleBackClick() {
		this.props.pushTo('/list');
	}

	btnOperation = (path, content) => {
		let pkMapTs = new Map();
		let pk = this.props.form.getFormItemsValue(this.formId, this.primaryId).value;
		let ts = this.props.form.getFormItemsValue(this.formId, 'ts').value;
		//主键与tsMap
		if (pk && ts) {
			pkMapTs.set(pk, ts);
		}
		ajax({
			url: API_URL[path],
			data: {
				pks: [ pk ],
				pkMapTs,
				pageCode: this.pageId
			},
			success: (res) => {
				if (res.success) {
					toast({ color: 'success', content });
					if (path === 'delete') {
						// 获取下一条数据的id
						let nextId = getNextId(pk, this.dataSource);
						//删除缓存
						deleteCacheById(this.primaryId, pk, this.dataSource);
						if (nextId) {
							this.props.setUrlParam({ id: nextId });
							getCardData.call(this, nextId);
						} else {
							// 删除的是最后一个的操作
							this.props.setUrlParam({ id: '' });
							setEditStatus.call(this, 'browse');
							clearAll.call(this, this.props);
							billHeadVisible.call(this, true, false);
						}
					} else if (res.data) {
						updateCache(this.primaryId, pk, res.data, this.formId, this.dataSource);
						buttonVisible.call(this, this.props);
					}
				}
			}
		});
	};

	//获取列表肩部信息
	getTableHead = () => {
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{this.props.button.createButtonApp({
						area: CARD.shoulder_btn_code,
						// buttonLimit: btnLimit,
						onButtonClick: bodyButtonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	tabsChange = (key) => {
		console.log(`当前切换到${key}区域了-----`);
		let interestObj = {
			rationrate: 'rationflag',
			advancerate: 'headflag',
			overduerate: 'overdueflag'
		};
		let curIntersetFlag = this.props.form.getFormItemsValue(this.formId, interestObj[key]).value;
		this.props.button.setButtonDisabled([ 'addRow' ], !curIntersetFlag);
	};

	render() {
		let { cardTable, form, button, cardPagination, BillHeadInfo } = this.props;
		let { createTabsTable } = cardTable;
		let { createBillHeadInfo } = BillHeadInfo;
		let { createForm } = form;
		let { createCardPagination } = cardPagination;
		let { createButtonApp } = button;
		let status = this.props.getUrlParam('status') === 'browse';
		let scene = this.props.getUrlParam('scene') === 'linksce';
		let showPagination = status && !scene;
		const langData = this.props.MultiInit.getLangData(this.moduleId);
		return (
			<div className="nc-bill-extCard">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								{createBillHeadInfo({
									title: this.state.json && this.state.json[this.props.pageTitle], //标题
									backBtnClick: () => {
										//返回按钮的点击事件
										this.handleBackClick();
									}
								})}
							</div>
							<div className="header-button-area">
								{createButtonApp({
									area: CARD.head_btn_code,
									onButtonClick: buttonClick.bind(this)
								})}
							</div>
							{showPagination && (
								<div className="header-cardPagination-area" style={{ float: 'right' }}>
									{createCardPagination({
										dataSource: this.dataSource,
										handlePageInfoChange: pageClick.bind(this)
									})}
								</div>
							)}
						</div>
					</NCAffix>
					<NCScrollElement name="forminfo">
						<div className="nc-bill-form-area">
							{createForm(this.formId, {
								onAfterEvent: afterEvent.bind(this),
								expandArr: [ this.interestId ]
							})}
						</div>
					</NCScrollElement>
				</div>
				<div className="nc-bill-bottom-area">
					<NCScrollElement name="businfo">
						<div className="nc-bill-table-area">
							{createTabsTable(this.tabCode, {
								showCheck: !status,
								showIndex: true,
								onAfterEvent: afterTableEvent.bind(this),
								tableHead: this.getTableHead.bind(this),
								onTabChange: this.tabsChange.bind(this),
								onSelected: bodySelectedEvent.bind(this), // 左侧选择列单个选择框回调
								onSelectedAll: bodySelectedAllEvent.bind(this) // 左侧选择列全选回调
							})}
						</div>
					</NCScrollElement>
				</div>
				<PrintOutput
					ref="printOutput"
					url={API_URL.print}
					data={this.state.outputData}
					callback={this.onSubmit}
				/>
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		billtype: 'extcard'
	},
	orderOfHotKey: [ 'head', 'rationrate' ]
})(Card);
