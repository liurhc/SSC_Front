import { searchBtnClick, pageInfoClick } from './searchBtnClick';
import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import { selectedEvent } from './selectedEvent';
import { bodyButtonClick, bodyBtnOperation } from './bodyButtonClick';
export { bodyButtonClick, bodyBtnOperation, searchBtnClick, initTemplate, buttonClick, pageInfoClick, selectedEvent };