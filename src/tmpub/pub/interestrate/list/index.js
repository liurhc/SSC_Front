/* 
 基准利率设置列表页
 created by：liyaoh 2018-08-13
*/
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, high, createPageIcon } from 'nc-lightapp-front';
let { NCAffix } = base;
const { PrintOutput } = high;
import { buttonClick, initTemplate, searchBtnClick, pageInfoClick, selectedEvent } from './events';
import { LIST, app_code, module_id, DATA_SOURCE, nodekey, API_URL } from '../cons/constant';

export default class List extends Component {
	constructor(props) {
		super(props);
		this.appcode = app_code;
		this.moduleId = '36010IRCO';
		this.searchId = LIST.search_id;
		this.tableId = LIST.table_id;
		this.pageId = LIST.page_id;
		this.nodekey = nodekey; //打印输出编码
		this.dataSource = DATA_SOURCE; //缓存key
		this.oid = LIST.search_oid; //查询区oid
		this.pageType = props.pageType;
		this.primaryId = LIST.primary_id;
		if (props.pageType === 'group') {
			//集团
			this.appcode = '36010IRCG';
			this.pageId = '36010IRCG_list';
			this.nodekey = '36010IRCG_card';
			this.dataSource = 'tm.pub.interestrateGroup.datasource';
			this.oid = '0001Z610000000024Y41';
			this.moduleId = '36010IRCG';
		} else if (props.pageType === 'global') {
			//全局
			this.appcode = '36010IRC';
			this.pageId = '36010IRC_list';
			this.nodekey = '36010IRC_card';
			this.dataSource = 'tm.pub.interestrateGlobal.datasource';
			this.oid = '0001Z610000000028M29';
			this.moduleId = '36010IRC';
		}
		this.state = {
			//输出用
			outputData: {
				funcode: '', //功能节点编码，即模板编码
				nodekey: '', //模板节点标识
				oids: [],
				outputType: 'output'
			}
		};
	}

	componentWillMount() {
		let callback = (json, status, inlt) => {
			if (status) {
				this.setState({ json, inlt }, () => {
					initTemplate.call(this, this.props, json);
				});
			} else {
				console.log('未加载到多语资源');
			}
		};
		this.props.MultiInit.getMultiLang({ moduleId: [ this.moduleId, '36010IR' ], domainName: 'tmpub', callback });
	}

	onSearchClick = () => {
		searchBtnClick.call(this, this.props);
	};

	handleDoubleClick = (record, index, props) => {
		props.pushTo('/card', {
			status: 'browse',
			id: record[this.primaryId].value,
			pagecode: this.pageId
		});
	};

	render() {
		let { table, button, search } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = button;

		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">
								{this.state.json && this.state.json[this.props.pageTitle]}
							</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({ area: LIST.head_btn_code, onButtonClick: buttonClick.bind(this) })}
						</div>
					</div>
				</NCAffix>

				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						showAdvBtn: true, //  显示高级按钮
						oid: this.oid //查询模板的oid，用于查询查询方案
					})}
				</div>
				<div className="nc-bill-table-area nc-table nc-single-table">
					{createSimpleTable(this.tableId, {
						handlePageInfoChange: pageInfoClick.bind(this),
						showCheck: true,
						dataSource: this.dataSource,
						pkname: this.primaryId,
						onRowDoubleClick: this.handleDoubleClick.bind(this),
						onSelected: selectedEvent.bind(this),
						onSelectedAll: selectedEvent.bind(this),
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						}
					})}
				</div>
				<PrintOutput
					ref="printOutput"
					url={API_URL.print}
					data={this.state.outputData}
					callback={this.onSubmit}
				/>
			</div>
		);
	}
}

// List = createPage({
// 	// mutiLangCode: module_id
// })(List);
// ReactDOM.render(<List />, document.querySelector('#app'));
