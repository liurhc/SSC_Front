/** 
* 整表编辑列表组件
* @author dongyue7
*/

import React, { Component } from 'react';
import { base, createPageIcon } from 'nc-lightapp-front';
import { onSearchClick } from './event/editListSearch';
import { buttonClick } from './event/editListButtonClick';
import { selectedEvent } from './event/events';
let { NCAffix } = base;

class BaseEditList extends Component {
	constructor(props) {
		super(props);
		for(let k in props.constant) {
            this[k] = props.constant[k];
        }
		this.state = {
			showToast: true,                    // 查询提示标识
			showSearch: true,                   // 查询区显示标识
			json: {},							// 多语json
			inlt: null,							// 多语资源
			status: 'browse',					// 页面状态
			editDelData: {                      // 编辑态删除暂存数据结构
                pageid: this.pageId,
                templetid: this.tableOid,
                model: {
                    areaType: 'table',
                    areacode: this.tableId,
                    rows: []
                }
			}
		};
		// props._initTemplate.call(this, props, props._afterSetMeta, props._beforeSetMeta, this.props.json);	// 节点传入的initTemplate，目前public中有公共的，若节点有其他需求可传自己的方法
	}

	componentWillMount() {
		let callback = (json, status, inlt) => {
			if (status) {
				this.setState({json, inlt}, () => {
					this.props._initTemplate.call(this, this.props, this.props._afterSetMeta, this.props._beforeSetMeta, json);
				});
			} else {
				console.log('未加载到多语资源')
			}
		}
		this.props.MultiInit.getMultiLang({moduleId: [this.moduleId, '36010PUBLIC'], domainName: 'tmpub', callback})
	}

	componentDidCatch(error, info) {
		console.log({ error, info });
	}

	render() {
		let { button, search, editTable, _afterEvent, _searchAfterEvent } = this.props;
		let { showSearch } = this.state;
		let { createEditTable } = editTable;
		let { NCCreateSearch } = search;
		let { createButtonApp} = button;
		return (
			<div className="nc-single-table" >
				{/* 标题及肩部按钮区域 */}
				<NCAffix>
					<div className="nc-singleTable-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{this.state.json[this.pageTitle]}</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({ area: this.btnCode, onButtonClick:buttonClick.bind(this) })}
						</div>
					</div>
				</NCAffix>
				{/* 查询区域 */}
				{
					showSearch &&
					<div className="nc-singleTable-search-area">
						{NCCreateSearch(this.searchId,{
							oid: this.searchOid,
							showAdvBtn: false, 
							showClearBtn:false,
							clickSearchBtn: onSearchClick.bind(this),
							onAfterEvent: _searchAfterEvent && _searchAfterEvent.bind(this)
						})}
					</div>
				}
				{/* 表体区域 */}
				<div className="nc-singleTable-table-area">
					{createEditTable(this.tableId, {
						showCheck: true,
						showIndex: this.showIndex || true,
						onSelected: selectedEvent.bind(this, this.props),
						onSelectedAll: selectedEvent.bind(this, this.props),
						onAfterEvent: _afterEvent && _afterEvent.bind(this)
					})}
				</div>
			</div>
		);
	}
}

export default BaseEditList