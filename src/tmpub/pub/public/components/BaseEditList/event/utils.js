/**
 * 基础档案整表编辑工具函数
 * @author dongyue7
 */

// 去前后空格
export function trimStr(str) {
	if (str === null || !str) {
		return '';
	}
	return str.replace(/(^\s*)|(\s*$)/g, '');
}

// 去除数组中重复元素
export function dedupe(array) {
	return Array.from(new Set(array));
}
