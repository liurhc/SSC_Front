/* 
 基准利率设置-组织列表页
 created by：liyaoh 2018-08-13
*/
import React, { Component } from 'react';
import { createPage, ajax, base, toast, high } from 'nc-lightapp-front';
import List from '../../interestrate/list';

class InterestrateList extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.appcode = '36010IRCO';
		this.pageId = '36010IRCO_list';
		this.tableId = 'table'; //initTemplate里用到
		this.searchId = 'search'; //initTemplate里用到
		this.pageType = 'org'; //todo 有时间重构
		this.props.pageType = 'org';
	}
	render() {
		return (
			<List
				// pageType="org"
				pageTitle="36010IRCO-000000"
				moduleId="36010IRCO"
				{...this.props}
			/>
		);
	}
}

InterestrateList = createPage(
	{
		// initTemplate: initTemplate
	}
)(InterestrateList);
export default InterestrateList;
// ReactDOM.render(<InterestrateList />, document.querySelector('#app'));
