/** 
* 非银行金融机构银行账户卡片
* @author：dongyue7
*/

import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
import { buttonClick, initTemplate, pageClick, getCardData } from './events';
import { baseReqUrl, javaUrl, accCard, accPrintData, acc_data_source, moduleId } from '../cons/constant.js';
import './index.less';
import { billHeadVisible, getTableHead } from './events/page';
import { handleClick } from '../public/event';
import { afterEvent } from './events/afterEvents';
import { bodySelectedEvent } from './events/bodySelectedEvent';
import { afterTableEvent } from './events/afterTableEvent';
let { PrintOutput } = high;
let { NCAffix } = base;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = accCard.headCode; //主表区域
		this.tableId = accCard.tableCode; //子表区域
		this.tablePrimaryId = accCard.tableCode; //子表区域
		this.pageId = accCard.pageCode; //card页面code
		this.primaryId = accCard.primaryId; //主键ID
		this.dataSource = acc_data_source; //缓存
		this.moduleId = moduleId; //多语ID
		this.state = {
			isPaste: false, //table处是否粘贴
			checkedRows: [], //table处当前选中行的index集合
			printOut: {
				//打印输出使用
				...accPrintData,
				outputType: 'output'
			},
			showPagination: true
		};
		// initTemplate.call(this, props);
	}

	componentWillMount() {
		let callback = (json, status, inlt) => {
			if (status) {
				initTemplate.call(this, this.props, json);
				this.setState({ json, inlt });
				this.afterGetLang(json);
			} else {
				console.log('未加载到多语资源');
			}
		};
		this.props.MultiInit.getMultiLang({ moduleId: this.moduleId, domainName: 'tmpub', callback });
	}

	afterGetLang() {
		window.onbeforeunload = () => {
			if (![ 'browse' ].includes(this.props.getUrlParam('status'))) {
				return this.state.json['36010NBFO-000015']; /* 国际化处理： 当前单据未保存, 您确定离开此页面?*/
			}
		};
	}

	componentWillUnmount() {
		window.onbeforeunload = () => {
			// 停止事件
		};
	}

	componentDidMount() {
		let id = this.props.getUrlParam('id');
		let status = this.props.getUrlParam('status');
		if (id) {
			getCardData.call(this, id, true);
			if (status === 'edit') {
				this.props.form.setFormItemsDisabled(this.formId, { code: true });
			}
		} else {
			this.props.form.setFormItemsValue(this.formId, {
				nonbankfininstitution: {
					value: this.props.getUrlParam('namePk'),
					display: this.props.getUrlParam('name')
				},
				accountproperty: {
					value: '0',
					display: this.state.json['36010NBFO-000009'] /* 国际化处理： 公司*/
				},
				enable_state: {
					value: '1',
					display: this.state.json['36010NBFO-000016'] /* 国际化处理： 已启用*/
				}
			});
			billHeadVisible.call(this, false, false);
		}
		this.setState({ name: this.props.getUrlParam('name') });
	}

	render() {
		let { cardTable, form, button, cardPagination, BillHeadInfo } = this.props;
		let { printOut } = this.state;
		let { createCardTable } = cardTable;
		let { createBillHeadInfo } = BillHeadInfo;
		let { createForm } = form;
		let { createCardPagination } = cardPagination;
		let { createButtonApp } = button;
		return (
			<div className="nc-bill-extCard fmc-demo">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								<span>
									{createBillHeadInfo({
										title: this.state.json && this.state.json['36010NBFO-000017'], //标题/* 国际化处理： 非银行金融机构银行账户*/
										backBtnClick: () => {
											//返回按钮的点击事件
											let namepk = this.props.getUrlParam('namePk');
											handleClick.call(this, 'acclist', {
												namePk: namepk
											});
										}
									})}
								</span>
							</div>
							<div className="header-button-area">
								{createButtonApp({
									area: accCard.btnCode,
									onButtonClick: buttonClick.bind(this)
								})}
							</div>
							{this.state.showPagination && (
								<div className="header-cardPagination-area" style={{ float: 'right' }}>
									{createCardPagination({
										dataSource: this.dataSource,
										handlePageInfoChange: pageClick.bind(this)
									})}
								</div>
							)}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(this.tableId, {
							tableHead: getTableHead.bind(this),
							showCheck: true,
							showIndex: true,
							onSelected: bodySelectedEvent.bind(this),
							onSelectedAll: bodySelectedEvent.bind(this),
							onAfterEvent: afterTableEvent.bind(this)
						})}
					</div>
				</div>
				<PrintOutput ref="printOutput" url={`${baseReqUrl}${javaUrl.accPrint}.do`} data={printOut} />
			</div>
		);
	}
}

Card = createPage({
	orderOfHotKey: [ 'head', 'pk_bankacc_b' ]
})(Card);

export default Card;
