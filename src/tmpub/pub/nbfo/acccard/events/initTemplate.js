import { appCode } from '../../cons/constant.js';
import { buttonVisible } from './buttonVisible';

export default function (props, json) {
	props.createUIDom(
		{
			pagecode: this.pageId,//页面id
			appcode: appCode
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					props.meta.setMeta(meta);
					if (props.getUrlParam('status') === 'add') {
						props.cardTable.addRow(this.tableId, 0);
					}
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('Delete_i', json['36010NBFO-000013']);/* 国际化处理： 确认要删除吗?*/
					props.button.setButtonDisabled(['Endefault_i', 'deleterow_i'], true);
					buttonVisible.call(this, props);
				}
			}
		}
	)
}
