import { ajax, cardCache, toast } from 'nc-lightapp-front';
import { buttonVisible } from './buttonVisible';
import { baseReqUrl, javaUrl, accList } from '../../cons/constant.js';
import { billHeadVisible } from '../../public/event';
let { getCacheById, setDefData, getDefData } = cardCache;

/**
 * 
 * @param {*} props  页面内置对象
 * @param {*} pks    下一页的pks
 */
export function pageClick(props, pks) {
	getCardData.call(this, pks);
	props.setUrlParam(pks);
}

/**
 * 卡片页查询
 * @param {*} id         单据id
 * @param {*} isFirst    是否首次进入，是(didmount)的话要addCache，否updateCache, 默认否
 * @param {*} isRefresh  是否刷新按钮，是的话不取缓存数据，直接调取接口，并addCache, 默认否
 */
export function getCardData(id, isFirst = false, isRefresh = false) {
    let cardData = getCacheById(id, this.cache);
    let cardTableData = getDefData(this.cardDataCache.key, this.cardDataCache.dataSource);
    let serData = getQueryData.call(this, id);
	if (cardData && !isRefresh) {
		//有缓存且不是刷新按钮
        this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
        cardTableData && this.props.cardTable.setTableData(this.tableId, { rows: cardTableData });
		buttonVisible.call(this, this.props);
		if (this.props.getUrlParam('status') !== 'browse') {
			billHeadVisible.call(this, false, true, cardData.head[this.formId].rows[0].values.name.value);
		} else if (this.props.getUrlParam('status') === 'browse') {
			billHeadVisible.call(this, true, true, cardData.head[this.formId].rows[0].values.name.value);
		}
		return;
	}
	ajax({
		url: `${baseReqUrl}${javaUrl.card}.do`,
		data: {
			pk: id,
			pageCode: this.pageId
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data && data.head) {
					this.props.form.setAllFormValue({ [this.formId]: data.head[this.formId] });
					if (this.props.getUrlParam('status') === 'browse') {
						billHeadVisible.call(this, true, true, data.head[this.formId].rows[0].values.name.value);
						this.setState({ showPagination: true });
					} else if (this.props.getUrlParam('status') !== 'browse') {
						billHeadVisible.call(this, false, true, data.head[this.formId].rows[0].values.name.value);
						this.setState({ showPagination: false });
					}
				}
				ajax({
					url: `${baseReqUrl}${javaUrl.accListQuery}.do`,
					data: serData,
					success: (res) => {
						let { success, data } = res;
						if (success) {
							this.props.cardTable.setTableData(this.tableId, { rows: data.grid.table.rows });
                            buttonVisible.call(this, this.props);
                            setDefData(this.cardDataCache.key, this.cardDataCache.dataSource, data.grid.table.rows);
						}
					}
				});
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message && res.message.message });
			this.props.cardTable.setTableData(this.tableId, { rows: [] });
			buttonVisible.call(this, this.props);
		}
	});
}

export function getQueryData(id) {
	return {
		// 子表查询传参
		querycondition: {
			logic: 'and',
			conditions: [
				{
					field: 'nonbankfininstitution',
					datetype: '',
					display: '',
					isIncludedSub: false,
					oprtype: '=',
					refurl: '',
					value: {
						firstvalue: id,
						secondvalue: ''
					}
				}
			]
		},
		custcondition: {},
		pageInfo: {
			pageIndex: 0,
			pageSize: '10'
		},
		pageCode: '36010NBFO_bankacc_list',
		queryAreaCode: 'search',
		oid: accList.searchOid,
		querytype: 'tree'
	};
}
