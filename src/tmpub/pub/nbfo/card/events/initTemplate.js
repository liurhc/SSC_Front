import { appCode, card } from '../../cons/constant.js';
import { buttonVisible } from './buttonVisible';
import { onInit, exdInit } from './treeEvent.js';

export default function(props, json) {
	props.createUIDom(
		{
			pagecode: card.pageCode, //页面id
			appcode: appCode
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, meta);
					// meta.tabsRelation = props.meta.renderTabs(meta);
					props.meta.setMeta(meta);
					exdInit.call(this, json); // 初始化全部子节点并处理
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						// props.button.setPopContent('delete', '确认要删除吗?');
						buttonVisible.call(this, props);
					});
				}
			}
		}
	);
}

function modifierMeta(meta) {
	return meta;
}
