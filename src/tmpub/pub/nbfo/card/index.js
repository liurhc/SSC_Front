/** 
* 非银行金融机构卡片
* @author：dongyue7
*/

import React, { Component } from 'react';
import { createPage, high } from 'nc-lightapp-front';
import { buttonClick, initTemplate, pageClick, getCardData } from './events';
import { moduleId, card, baseReqUrl, javaUrl, insPrintData, accList } from '../cons/constant.js';
import './index.less';
import { billHeadVisible, handleClick, cancel } from '../public/event';
let { PrintOutput } = high;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = card.headCode; //主表区域
		this.tableId = card.tableCode; //子表区域
		this.tablePrimaryId = card.tablePrimaryId; //子表区域
		this.moduleId = moduleId; //多语使用
		this.pageId = card.pageCode; //card页面code
		this.primaryId = card.primaryId; //主键ID
		this.cache = card.cardCache; //缓存key
		this.dataSource = card.cardCache; //调用列表界面缓存pks
		this.treeId = 'tree'; //树id
		this.cardDataCache = accList.cardDataCache; //子表缓存
		this.state = {
			isPaste: false, //tabs处是否粘贴
			checkedRows: [], //tabs处当前选中行的index集合
			printOut: {
				//打印输出使用
				...insPrintData,
				outputType: 'output'
			},
			type: {},
			pk: [],
			selectedPk: props.getUrlParam('typePk'),
			newId: '',
			reviseStatus: true,
			showPagination: true
		};
		// initTemplate.call(this, props);
	}

	componentWillMount() {
		let callback = (json, status, inlt) => {
			if (status) {
				initTemplate.call(this, this.props, json);
				this.setState({ json, inlt });
				this.afterGetLang(json);
			} else {
				console.log('未加载到多语资源');
			}
		};
		this.props.MultiInit.getMultiLang({ moduleId: this.moduleId, domainName: 'tmpub', callback });
	}

	afterGetLang() {
		window.onbeforeunload = () => {
			if (![ 'browse' ].includes(this.props.getUrlParam('status'))) {
				return this.state.json['36010NBFO-000015']; /* 国际化处理： 当前单据未保存, 您确定离开此页面?*/
			}
		};
	}

	componentWillUnmount() {
		window.onbeforeunload = () => {
			// 停止事件
		};
	}

	componentDidMount() {
		let id = this.props.getUrlParam('id');
		if (id) {
			getCardData.call(this, id, true);
		} else {
			billHeadVisible.call(this, false, false);
			this.setState({ showPagination: false });
			this.props.form.setFormItemsValue(this.formId, {
				enable_state: { value: '1', display: this.state.json && this.state.json['36010NBFO-000016'] }
			}); /* 国际化处理： 已启用*/
		}
	}

	render() {
		let { cardTable, form, button, ncmodal, cardPagination, DragWidthCom, syncTree, BillHeadInfo } = this.props;
		let { printOut } = this.state;
		let { createCardTable } = cardTable;
		let { createBillHeadInfo } = BillHeadInfo;
		let { createForm } = form;
		let { createCardPagination } = cardPagination;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		let { createSyncTree } = syncTree;
		return (
			<div className="nc-bill-tree-card">
				{/* 头部 header*/}
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						<span>
							{createBillHeadInfo({
								title: this.state.json && this.state.json['36010NBFO-000028'], //标题/* 国际化处理： 非银行金融机构*/
								backBtnClick: () => {
									//返回按钮的点击事件
									handleClick.call(this, 'list', {
										bckPk: this.state.selectedPk,
										bckFlag: true
									});
								}
							})}
						</span>
					</div>
					<div className="header-button-area">
						{createButtonApp({
							area: card.btnCode,
							onButtonClick: buttonClick.bind(this)
						})}
					</div>
					{this.state.showPagination && (
						<div className="header-cardPagination-area" style={{ float: 'right' }}>
							{createCardPagination({
								handlePageInfoChange: pageClick.bind(this)
							})}
						</div>
					)}
				</div>
				{/* 树卡区域 */}
				<div className="tree-card">
					<DragWidthCom
						leftDom={
							<div
								className="tree-area"
								style={{ marginLeft: '10px' }}
							>
								{createSyncTree({
									treeId: 'tree',
									needEdit: false, //不启用编辑
									showLine: true, //显示连线
									needSearch: true, //是否需要搜索框,
									showModal: true, //是否使用弹出式编辑
									hiddenDefaultIcon: true, //隐藏默认的文件夹图标
									disabledSearch: true
								})}
							</div>
						}     //左侧区域dom
						rightDom={
							<div>
								<div className="nc-bill-form-area">
									{createForm(
										this.formId,
										{
											// onAfterEvent: afterEvent.bind(this)
										}
									)}
								</div>
								<div className="nc-bill-bottom-area">
										<div className="nc-bill-table-area">
											{createCardTable(this.tableId, {
												showIndex: true
											})}
										</div>
								</div>
							</div>
						}     //右侧区域dom
						defLeftWid='21%'      // 默认左侧区域宽度，px/百分百
					/>
					<PrintOutput ref="printOutput" url={`${baseReqUrl}${javaUrl.print}.do`} data={printOut} />
					{createModal('cancelModal', {
						title: this.state.json && this.state.json['36010NBFO-000007'] /* 国际化处理： 取消*/,
						content: this.state.json && this.state.json['36010NBFO-000030'] /* 国际化处理： 确定要取消么?*/,
						beSureBtnClick: () => {
							cancel.call(this, this.props);
						}
					})}
				</div>
			</div>
		);
	}
}

Card = createPage({
	orderOfHotKey: [ 'head', 'bankaccount' ]
})(Card);

export default Card;
