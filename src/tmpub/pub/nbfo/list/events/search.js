import { ajax, toast } from 'nc-lightapp-front';
import { list, baseReqUrl, javaUrl } from '../../cons/constant.js';
import { selectedEvent } from './page.js';

/**
 * 点击查询，获取查询区数据
 * @param {*} props           
 * @param {*} condition       
 * @param {*} props           
 * @param {*} condition       
 */
export function searchBtnClick(props, condition, type, querycondition) {
	//查询区域查询条件(判断查询参数是否传了，若没传，则将目前的查询参数交给它)
	if (!querycondition) {
		querycondition = props.search.getAllSearchData(this.searchId);
		if (!querycondition) {
			return;
		}
	}
	if (!condition) {
		condition = props.search.getAllSearchData(this.searchId);
	}
	let pageInfo = props.editTable.getTablePageInfo(list.tableCode);
	let searchdata = {
		querycondition: condition,
		pageInfo: pageInfo,
		pagecode: list.pageCode,
		queryAreaCode: list.searchCode, //查询区编码
		oid: list.searchOid, //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
		querytype: 'tree'
	};
	if (this.state.typeQueryPk && this.state.typeQueryPk !== '-1') {
		searchdata.querycondition.conditions.push({
			field: 'type',
			oprtype: '=',
			value: {
				firstvalue: this.state.typeQueryPk,
				secondvalue: null
			}
		});
	}
	getListData.call(this, javaUrl.list, searchdata);
}

/**
 * 点击分页、改变每页条数
 * @param {*} props           页面内置对象
 * @param {*} config          大家查一下文档，没细看，貌似没用上
 * @param {*} pks             拿到当前页的所有pks
 */
export function pageInfoClick(props, config, pks) {
	let data = {
		pks,
		pageCode: this.pageId
	};
	this.setState({ showToast: false });
	getListData.call(this, javaUrl.pks, data);
}

/**
 * 请求列表接口
 * @param {*} path       接口地址
 * @param {*} data       数据
 */
function getListData(path, data) {
	ajax({
		url: `${baseReqUrl}${path}.do`,
		data,
		success: (res) => {
			listRender.call(this, res);
			this.props.button.setButtonDisabled('refresh', false);
		},
		error: () => {
			listRender.call(this, { success: false });
		}
	});
}

/**
 * 拿到返回的数据，对列表进行渲染
 * @param {*} res            后台返回的res
 */
function listRender(res) {
	let { success, data } = res;
	if (success && data && data.grid && data.grid[this.tableId]) {
		this.props.table.setAllTableData(this.tableId, data.grid[this.tableId]);
		this.props.button.setButtonDisabled('Refresh', false);
		selectedEvent.call(this, this.props);
		if (this.state.showToast) {
			toast({
				color: 'success',
				content: `${this.state.json['36010NBFO-000024']}，${this.state.json['36010NBFO-000025']}${data.grid[
					this.tableId
				].pageInfo.total}${this.state.json['36010NBFO-000026']}。`
			}); /* 国际化处理： 查询成功,共有,条数据*/
		}
	} else {
		this.props.table.setAllTableData(this.tableId, { rows: [] });
	}
}
