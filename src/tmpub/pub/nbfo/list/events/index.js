import { searchBtnClick, pageInfoClick} from './search';
import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import { bodyButtonClick, bodyBtnOperation } from './bodyButtonClick';
export { bodyButtonClick, bodyBtnOperation, searchBtnClick, pageInfoClick, initTemplate, buttonClick };