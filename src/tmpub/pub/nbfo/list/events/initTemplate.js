import { bodyButtonClick } from './bodyButtonClick';
import { list, card, appCode, btnLimit } from '../../cons/constant.js';
import { searchBtnClick } from './search';
import { exdInit } from './treeEvent';

export default function(props, json) {
	const treeForm = {
		// 同步树弹出框meta配置
		moduletype: 'form',
		items: [
			{
				visible: true,
				label: json['36010NBFO-000035'] /* 国际化处理： 机构类别编码*/,
				attrcode: 'code',
				col: 4,
				leftspace: 0,
				rightspace: 0,
				placeholder: '',
				rows: 4,
				itemtype: 'input',
				verify: true,
				required: true
			},
			{
				visible: true,
				label: json['36010NBFO-000036'] /* 国际化处理： 机构类别名称*/,
				attrcode: 'name',
				col: 4,
				leftspace: 0,
				rightspace: 0,
				placeholder: '121',
				rows: 4,
				itemtype: 'residtxt',
				verify: true,
				required: true,
				languageMeta: [
					{
						index: '1',
						languageCode: 'simpchn',
						languageType: 'ZH'
					},
					{
						// 有多语盘的环境要加上以下
						index: '2',
						languageCode: 'english',
						languageType: 'EN'
					},
					{
						index: '3',
						languageCode: 'tradchn',
						languageType: 'ZF'
					}
				]
			},
			{
				visible: true,
				label: json['36010NBFO-000037'] /* 国际化处理： 简称*/,
				attrcode: 'abbreviation',
				col: 4,
				leftspace: 0,
				rightspace: 0,
				placeholder: '',
				rows: 4,
				itemtype: 'input',
				verify: true
			}
		]
	};
	props.createUIDom(
		{
			pagecode: this.pageId, //页面code
			appcode: appCode
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta, json);
					meta.treeForm = treeForm;
					props.meta.setMeta(meta);
					afterSetMata.call(this, props);
				}
				if (data.button) {
					/* 按钮适配  第一步：将请求回来的按钮组数据设置到页面的 buttons 属性上 */
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('Delete_i', json['36010NBFO-000005']); /* 国际化处理： 确定要删除吗?*/
					props.button.setButtonVisible([ 'Account', 'Copy', 'Enable', 'Disenable', 'PrintOut' ], false);
					if (!props.getUrlParam('bckPk')) {
						props.button.setButtonDisabled(list.disabled_btn, true);
					}
				}
				if (data.context) {
					this.setState({ langSeq: data.context.currentLangSeq || '1' });
				}
			}
		}
	);
}

function modifierMeta(props, meta, json) {
	meta[this.tableId].pagination = true;
	meta[this.tableId].items = meta[this.tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'code') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{ cursor: 'pointer' }}
						onClick={(e) => {
							e.stopPropagation();
							props.pushTo('/card', {
								status: 'browse',
								id: record[this.primaryId].value,
								pagecode: card.pageCode,
								name: record.name.value,
								typePk: record.type.value // 用于树的展开点击
							});
						}}
					>
						{record && record.code && record.code.value}
					</a>
				);
			};
		}
		return item;
	});

	//添加操作列
	meta[this.tableId].items.push({
		itemtype: 'customer',
		attrcode: 'opr',
		label: json['36010NBFO-000023'] /* 国际化处理： 操作*/,
		width: 200,
		fixed: 'right',
		className: 'table-opr',
		visible: true,
		render: (text, record, index) => {
			let buttonAry = [];
			let enableState = record.enable_state && record.enable_state.value;
			if (enableState === '1') {
				buttonAry = [ 'Revise_i', 'Delete_i', 'Disenable_i' ];
			} else {
				buttonAry = [ 'Revise_i', 'Delete_i', 'Enable_i' ];
			}
			return props.button.createOprationButton(buttonAry, {
				area: list.bodyCode,
				buttonLimit: btnLimit,
				onButtonClick: (props, key) => bodyButtonClick.call(this, key, record)
			});
		}
	});
	return meta;
}

function afterSetMata(props) {
	exdInit.call(this); // 树表content加载
	props.button.setButtonDisabled(list.disabled_btn, true);
	let bckPk = props.getUrlParam('bckPk');
	if (bckPk) {
		props.syncTree.setNodeSelected(this.treeId, [ bckPk ]);
		props.button.setButtonDisabled('Add', false);
		this.setState({
			typePk: bckPk,
			typeQueryPk: bckPk
		});
	}
	if (this.props.getUrlParam('bckFlag')) {
		this.setState({ showToast: false });
		searchBtnClick.call(this, this.props);
	}
}
