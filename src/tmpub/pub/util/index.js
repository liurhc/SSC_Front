/**
 * 资金领域 公共api
 * 
 * @author tangleic
 */
import { print, ajax, toast } from 'nc-lightapp-front'
import { SCENE, URL_PARAM, COMMON_URL } from "../cons/constant";

/**
 * 判断是否是联查场景
 * @param {*} props 页面内置对象
 */
export const isLinkScene = function (props) {
    //获取场景标志
    let scene = props.getUrlParam(URL_PARAM.SCENE);
    //是否预算反联查(鄙视，预算反联查没有联查标志，只能通过是否有参数值来判断是否预算反联查)
    let isTbbLink = !props.getUrlParam(URL_PARAM.TBB_LINK) ? false : true;
    //被联查场景(凭证反联查以及预算反联查)不渲染查询区域，故无需加载默认业务单元到查询区域
    return (isTbbLink || scene == SCENE.LINK || scene == SCENE.FIP) ? true : false;
}

/**
 * 判断是否有默认业务单元数据
 * @param {*} data createUIDom请求返回数据
 */
export const hasDefaultOrg = function (data) {
    return data && data.context && data.context && data.context.pk_org;
}

/**
 * 判断查询区域查询条件是否有值
 * @param {*} props 页面内置对象
 * @param {*} areaCode 查询区域编码
 * @param {*} item 查询条件字段名
 */
const hasSearchValue = function (props, areaCode, item) {
    if (!props || !props.search || !areaCode || !item) {
        return false;
    }
    let searchValue = props.search.getSearchValByField(areaCode, item);
    return searchValue && searchValue.value && (searchValue.value.firstvalue || searchValue.value.secondvalue) ? true : false;
}
/**
 * 给卡片头部区域赋默认业务单元(在setMeta之后使用)
 * @param {*} props 页面内置对象
 * @param {*} areaCode 卡片头部区域编码
 * @param {*} data  createUIDom请求返回数据
 */
export const setDefOrg2Form = function (props, areaCode, data) {
    //判空
    if (!props || !areaCode || !hasDefaultOrg(data)) {
        return;
    }
    let { pk_org, org_Name, pk_org_v, org_v_Name } = data.context;
    //将默认业务单元加载到头部表单
    props.form.setFormItemsValue(areaCode, {
        'pk_org': { value: pk_org, display: org_Name },
        'pk_org_v': { value: pk_org_v, display: org_v_Name },
    })
}

/**
 * 给列表查询区域赋默认业务单元(在setMeta之后使用)
 * @param {*} props 页面内置对象
 * @param {*} areaCode 列表查询区域编码
 * @param {*} data  createUIDom请求返回数据
 */
export const setDefOrg2ListSrchArea = function (props, areaCode, data) {
    //判空
    if (!props || !props.search || !areaCode || !hasDefaultOrg(data)) {
        return;
    }
    //联查场景不渲染查询区域
    if (isLinkScene(props)) {
        return;
    }
    //判断查询区域组织是否有值，如果有则表明快速查询方案已个性化定制。无需加载默认业务单元
    if (hasSearchValue(props, areaCode, 'pk_org')) {
        return;
    }
    //获取默认业务单元
    let { pk_org, org_Name } = data.context;
    let searchData = { 'display': org_Name, 'value': pk_org };
    //更新列表查询区域
    props.search.setSearchValByField(areaCode, 'pk_org', searchData);
}

/**
 * 给高级查询区域赋默认业务单元(在setMeta之前使用)
 * 
 * @param {*} props 页面内置对象
 * @param {*} areaCode 列表查询区域编码
 * @param {*} data  createUIDom请求返回数据
 */
export const setDefOrg2AdvanceSrchArea = function (props, areaCode, data) {
    //判空
    if (!props || !props.search || !areaCode || !hasDefaultOrg(data) || !data.template) {
        return;
    }
    //联查场景不渲染查询区域
    if (isLinkScene(props)) {
        return;
    }
    let meta = data.template;
    //获取默认业务单元
    let { pk_org, org_Name } = data.context;
    //遍历查询区域字段，将默认业务单元赋值给组织字段
    meta[areaCode].items.map((item) => {
        if (item.attrcode == 'pk_org') {
            item.initialvalue = { 'display': org_Name, 'value': pk_org }
        }
    });
}

//获取拓展数据对象的键
const getExtObjKey = function (appCode) {
    return appCode + "_" + "extObj";
}

const getMultiLangKey = function () {
    return 'multiLang';
}
/**
 * 将数据存储到页面级缓存
 * @param {*} props 页面内置对象
 * @param {*} appCode 应用编码
 * @param {*} key 键
 * @param {*} value 值 
 */
export const setPropCache = function (props, appCode, key, value) {
    //参数判空
    if (!props || !appCode || !key) {
        return;
    }
    const extObjKey = getExtObjKey(appCode);
    //从页面级缓存中获取拓展对象
    let extObj = props.ViewModel.getData(extObjKey);
    if (!extObj) {
        extObj = {};
    }
    //将键值对存储到拓展对象
    extObj[key] = value;
    //将拓展对象存储到页面级缓存
    props.ViewModel.setData(extObjKey, extObj);
}

/**
 * 获取页面级缓存中指定键的数据
 * @param {*} props 页面内置对象
 * @param {*} appCode 应用编码
 * @param {*} key 键
 */
export const getPropCache = function (props, appCode, key) {
    //参数判空
    if (!props || !appCode || !key) {
        return null;
    }
    const extObjKey = getExtObjKey(appCode);
    //从页面级缓存中获取拓展对象
    let extObj = props.ViewModel.getData(extObjKey);
    if (!extObj || !extObj.hasOwnProperty(key)) {
        return null;
    }
    //从页面级缓存中获取指定键的值
    return extObj[key];
}

/**
 * 存储多语资源
 * @param {*} props 
 * @param {*} multiLang 
 */
export const saveMultiLangRes = function (props, multiLang) {
    if (!props || !multiLang) {
        return;
    }
    let key = getMultiLangKey();
    props.ViewModel.setData(key, multiLang);
}

/**
 * 追加多语资源
 * @param {*} props 页面内置对象
 * @param {*} multiLang 多语资源
 */
export const appendMultiLangRes = function (props, multiLang) {
    if (!props || !multiLang) {
        return;
    }
    //获取多语资源
    let lang = getMultiLangRes(props);
    if (!lang) {
        saveMultiLangRes(props, multiLang);
    } else {
        Object.assign(lang, multiLang);
    }
}
/**
 * 获取多语资源对象
 * @param {*} props 
 */
export const getMultiLangRes = function (props) {
    if (!props) {
        return;
    }
    let key = getMultiLangKey();
    return props.ViewModel.getData(key);
}

/**
 * 加载多语信息
 * @param {*} props 页面内置对象
 * @param {*} key 多语资源键
 */
export const loadMultiLang = function (props, key) {
    //获取多语资源
    const lang = getMultiLangRes(props);
    if (!lang) {
        return '';
    }
    return lang[key] || '';
}

/**
 * 构建编辑后事件表头数据
 * @param {*} props 页面内置对象
 * @param {*} headCode 表头区域编码
 */
const buildAfterEditHeadData = function (props, headCode) {
    let data = {};
    let formData = props.form.getAllFormValue(headCode);
    formData['areacode'] = headCode;
    data[headCode] = formData;
    return data;
}

/**
 * 构建表体编辑后事件数据
 * @param {*} props 页面内置对象
 * @param {*} bodyCode 表体区域编码
 */
const buildAfterEditBodyData = function (props, bodyCode) {
    let bodyData = {
        'rows': props.cardTable.getChangedRows(bodyCode),
        'areaType': 'table',
        'areacode': bodyCode
    };
    let data = {};
    data[bodyCode] = bodyData;
    return data;
}

/**
 * 构建编辑后事件的卡片数据
 * @param {*} props 页面内置对象
 * @param {*} pageCode 页面编码
 * @param {*} headCode 表头区域编码
 * @param {*} bodyCode 表体区域编码（当前修改的表体）
 * @param {*} attrcode 修改的字段
 * @param {*} changedrows 修改的行信息
 * @param {*} index 行索引
 * @param {*} isSingleBody 是否单表体
 */
const buildAfterEditEventData = function (props, pageCode, headCode, bodyCode, attrcode, changedrows, index, isSingleBody) {
    let card = {
        'head': buildAfterEditHeadData(props, headCode),
        'pageid': pageCode
    };
    if (isSingleBody) {
        card['body'] = buildAfterEditBodyData(props, bodyCode);
    } else {
        card['bodys'] = buildAfterEditBodyData(props, bodyCode);
    }
    return {
        'areacode': bodyCode,
        attrcode,
        card,
        changedrows,
        index,
    };
}
/**
 * 构建精简表体编辑后事件数据(旨在替代原来平台API：createBodyAfterEventData，只保留修改的当前行数据)
 * @param {*} props 页面内置对象
 * @param {*} pageCode 页面编码
 * @param {*} headCode 表头区域编码
 * @param {*} handleBodyCode 当前操作表体区域编码
 * @param {*} attrCode 编辑的字段
 * @param {*} changeRows 编辑的行信息
 * @param {*} index 行索引
 * @param {*} isSingleBody 是否是单表体
 */
export const buildLightBodyAfterEditData = function (props, pageCode, headCode, handleBodyCode, attrCode, changeRows, index, isSingleBody = false) {
    try {
        //参数判空
        if (!props || !pageCode || !headCode || !handleBodyCode || !attrCode || !changeRows) {
            throw new Error("参数缺失！");
        }
        //构建表体编辑后事件数据
        let eventData = buildAfterEditEventData(props, pageCode, headCode, handleBodyCode, attrCode, changeRows, index, isSingleBody);
        let { card } = eventData;
        let { body, bodys } = card;
        //当编辑后事件数据只有一行表体时，不做额外处理。有多行时，对多行表体过滤，只保留当前编辑行
        if ((isSingleBody && body[handleBodyCode].rows.length == 1)
            || (!isSingleBody && bodys[handleBodyCode].rows.length == 1)) {
            return eventData;
        }
        let newRowArr = [];
        //修改行的行ID
        let changeRowID = changeRows[0].rowid;
        //获取当前编辑的表体
        body = isSingleBody ? body[handleBodyCode] : bodys[handleBodyCode];
        if (!body) {
            throw new Error("未获取到指定的表体[" + handleBodyCode + "]!");
        }
        let { rows } = body;
        for (let row of rows) {
            let { rowid } = row;
            //过滤非当前修改的行
            if (!rowid || rowid != changeRowID) {
                continue;
            }
            newRowArr.push(row);
            break;
        }
        if (newRowArr.length == 0) {
            throw new Error("未找到修改的行!");
        }
        body.rows = newRowArr;
        return eventData;
    } catch (e) {
        console.log("构建精简表体编辑后数据时出错！:" + e.message);
        throw e;
    }
}

/**
 * 清空值为空的字段
 * @param {*} rows 字段数组
 */
const filterEmptyItem = function (rows) {
    if (!rows || rows.length == 0) {
        return null;
    }
    //遍历行数据
    for (let row of rows) {
        if (!row || !row.values || Object.keys(row.values).length == 0) {
            continue;
        }
        let { values } = row;
        let keys = Object.keys(values);
        //遍历一行数据中所有字段，过滤空值字段
        for (let key of keys) {
            let item = values[key];
            if (!item || Object.keys(item).length == 0 || !item.value) {
                delete values[key];
            }
        }
    }
}
/**
 * 构建一主一子页面数据
 * @param {*} props 页面内置对象
 * @param {*} pageCode 页面编码
 * @param {*} headCode 头部区域编码
 * @param {*} bodyCode 表体区域编码
 * @param {*} clearEmptyItem 是否过滤空值字段
 */
const createSimpleBillDataOneBody = function (props, pageCode, headCode, bodyCode, clearEmptyItem) {
    let billData = props.createMasterChildDataSimple(pageCode, headCode, bodyCode);
    let { head, body } = billData;
    if (clearEmptyItem) {
        filterEmptyItem(head[headCode].rows);
        filterEmptyItem(body[bodyCode].rows);
    }
    return billData;
}

/**
 * 构建一主多子页面数据
 * @param {*} props 页面内置对象
 * @param {*} pageCode 页面编码
 * @param {*} headCode 头部区域编码
 * @param {*} bodyCodeArr 表体区域编码数组
 * @param {*} clearEmptyItem 是否过滤空值字段
 */
const createSimpleBillDataMultiBody = function (props, pageCode, headCode, bodyCodeArr, clearEmptyItem) {
    let billData = props.createExtCardDataSimple(pageCode, headCode, bodyCodeArr);
    let { head, bodys } = billData;
    if (clearEmptyItem) {
        filterEmptyItem(head[headCode].rows);
        for (let bodyCode of bodyCodeArr) {
            filterEmptyItem(bodys[bodyCode].rows);
        }
    }
    return billData;
}

/**
 * 构建轻量级的页面数据(适合保存操作)
 * @param {*} props 页面内置对象
 * @param {*} pageCode 页面编码
 * @param {*} headCode 头部区域编码
 * @param {*} bodyCodes 表体区域编码（一主多子为表体区域编码数组）
 * @param {*} clearEmptyItem 是否过滤空值字段(默认不过滤)
 */
export const createSimpleBillData = function (props, pageCode, headCode, bodyCodes, clearEmptyItem = false) {
    if (!props || !pageCode || !headCode || !bodyCodes) {
        return null;
    }
    //根据表体区域编码参数来判断是一主一子还是一主多子
    let isMultiBody = Array.isArray(bodyCodes) ? (bodyCodes.length > 1) : false;
    let bodyCodeArr = Array.isArray(bodyCodes) ? bodyCodes : [bodyCodes];
    let billData = null;
    //一主一子单据处理
    if (!isMultiBody) {
        billData = createSimpleBillDataOneBody(props, pageCode, headCode, bodyCodeArr[0], clearEmptyItem);
    }
    //一主多子单据处理
    else {
        billData = createSimpleBillDataMultiBody(props, pageCode, headCode, bodyCodeArr, clearEmptyItem);
    }
    return billData;
}

/**
 * 电子签章列表打印
 * @param {*} props 
 * @param {*} param1 
 */
export const elecSignListPrint = function (props, {
    //打印地址
    url,
    //是否正式打印(默认补充打印)
    offical = false,
    //应用编码
    appCode,
    //模版标示
    nodeKey,
    //表格区域编码
    tableCode,
    //主键字段
    field_id,
    //单据编号字段(默认vbillno)
    field_billno = 'vbillno',
    //获取组织字段的值（不注入默认按照pk_org来获取）
    getOrgFunc,
    //数据校验逻辑（没有则默认每一条数据都可以进行电子签章打印,如果不通过需要返回返回异常信息）
    validateFunc
}) {
    //参数判空
    if (!url || !appCode || !tableCode || !field_id || !field_billno) {
        throw new Error("参数缺失！");
    }
    let selectDatas = props.table.getCheckedRows(tableCode);
    //判断是否有选中行
    if (selectDatas == null || selectDatas.length == 0) {
        toast({ color: 'warning', content: loadMultiLang(props, '3601-000010') });/* 国际化处理： 未选中行！*/
        return;
    }
    let detail = [];
    let errMessArr = [];
    //遍历选中数据，获取打印需要的参数
    for (let selectData of selectDatas) {
        //主键
        let id = selectData && selectData.data && selectData.data.values && selectData.data.values[field_id] && selectData.data.values[field_id].value;
        if (!id) {
            continue;
        }
        //单据编号
        let vbillno = selectData && selectData.data && selectData.data.values && selectData.data.values[field_billno] && selectData.data.values[field_billno].value;
        //组织
        let pk_org = selectData && selectData.data && selectData.data.values && selectData.data.values['pk_org'] && selectData.data.values['pk_org'].value;
        //行索引
        let index = selectData.index;
        //获取自定义的组织
        if (getOrgFunc && (typeof getOrgFunc == 'function')) {
            pk_org = getOrgFunc(selectData);
        }
        //业务自定义的校验
        let flag = true;
        if (validateFunc && (typeof validateFunc == 'function')) {
            let errMess = validateFunc(selectData);
            if (errMess) {
                errMessArr.push(buildErrMess(props, errMess, vbillno, index));
                flag = false;
            }
        }
        if (flag) {
            detail.push({ id, vbillno, pk_org, index });
        }
    }
    elecSignPrint(props, {
        url, offical, appCode, nodeKey, detail, errMessArr
    });
}

/**
 * 电子签章卡片打印
 * @param {*} props 
 * @param {*} param1 
 */
export const elecSignCardPrint = function (props, {
    //打印地址
    url,
    //是否正式打印(默认补充打印)
    offical = false,
    //应用编码
    appCode,
    //模版标示
    nodeKey,
    //表格区域编码
    headCode,
    //主键字段
    field_id,
    //单据编号字段(默认vbillno)
    field_billno = 'vbillno',
    //获取组织字段的值（不注入默认按照pk_org来获取）
    getOrgFunc,
    //数据校验逻辑（没有则默认每一条数据都可以进行电子签章打印,如果不通过需要返回返回异常信息）
    validateFunc }
) {
    //参数判空
    if (!url || !appCode || !headCode || !field_id || !field_billno) {
        throw new Error("参数缺失！");
    }
    //主键
    let id = props.form.getFormItemsValue(headCode, field_id).value;
    //单据编号
    let vbillno = props.form.getFormItemsValue(headCode, field_billno).value;
    //组织
    let pk_org = props.form.getFormItemsValue(headCode, 'pk_org').value;
    //如果有自定义获取组织的逻辑则采用自定义的逻辑来获取组织
    if (getOrgFunc && (typeof getOrgFunc == 'function')) {
        pk_org = getOrgFunc();
    }
    let errMessArr = [];
    if (validateFunc && (typeof validateFunc == 'function')) {
        let errMess = validateFunc();
        if (errMess) {
            errMessArr.push(buildErrMess(props, errMess, vbillno, 0));
        }
    }
    elecSignPrint(props, {
        url,
        offical,
        appCode,
        nodeKey,
        detail: [{
            id, vbillno, pk_org
        }],
        errMessArr
    })
}

/**
 * 电子签章打印
 * @param {*} props 
 * @param {*} param 
 */
const elecSignPrint = function (props, {
    //打印地址
    url,
    //是否正式打印
    offical,
    //应用编码
    appCode,
    //模版标示 
    nodeKey,
    //打印数据明细
    detail,
    //异常信息数组
    errMessArr = [] }
) {
    //没有要检查的数据，但是有异常信息，则直接提示，不再与后端交互
    if ((errMessArr && errMessArr.length > 0) && (!detail || detail.length == 0)) {
        //提示
        toast({
            duration: 'infinity',
            color: 'danger',
            TextArr: [loadMultiLang(props, '36300-000009'), loadMultiLang(props, '36300-000010'), loadMultiLang(props, '36300-000011')],/* 国际化处理： 展开,收起,我知道了*/
            groupOperation: true,
            groupOperationMsg: errMessArr
        });
        return;
    }
    //构建检查参数
    const checkParam = {
        offical, detail
    }
    ajax({
        url: COMMON_URL.ELECSIGNPRINTCHECK,
        data: checkParam,
        success: (res) => {
            let { passPKs, passInfos, unPassInfos } = res.data;
            if (errMessArr.length > 0 || (unPassInfos && unPassInfos.length > 0)) {
                //遍历检查不通过的数据，组装提示信息
                for (let unPassInfo of unPassInfos) {
                    let { vbillno, mess, index } = unPassInfo;
                    let errMess = buildErrMess(props, mess, vbillno, index);
                    errMessArr.push(errMess);
                }
                //提示
                toast({
                    duration: 'infinity',
                    color: 'danger',
                    TextArr: [loadMultiLang(props, '36300-000009'), loadMultiLang(props, '36300-000010'), loadMultiLang(props, '36300-000011')],/* 国际化处理： 展开,收起,我知道了*/
                    groupOperation: true,
                    groupOperationMsg: errMessArr
                });
            }
            //有检查通过的数据，则进行打印
            if (passPKs && passInfos) {
                const printParam = {
                    offical,
                    detail: passInfos
                }
                print(
                    'pdf',
                    url,
                    {
                        nodekey: nodeKey,//模版标示
                        appcode: appCode,//应用编码
                        oids: passPKs,//单据主键
                        userjson: JSON.stringify(printParam)
                    }
                );
            }
        }
    })
}

//组装异常信息
const buildErrMess = function (props, errMess, vbillno, index) {
    return loadMultiLang(props, '3601-000008') + vbillno + loadMultiLang(props, '3601-000009') + errMess || '';
}