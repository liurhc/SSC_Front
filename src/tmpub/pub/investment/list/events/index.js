import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import { bodyButtonClick, bodyBtnOperation } from './bodyButtonClick';
export { bodyButtonClick, bodyBtnOperation, initTemplate, buttonClick };