import { toast, ajax } from 'nc-lightapp-front';
import { javaUrl, list, baseReqUrl } from '../../cons/constant.js';
import { bodyBtnOperation } from './bodyButtonClick';
import { multiToast } from '../../../public/events'
import { searchButtonClick } from './search';
import { listButtonVisible } from './buttonVisible';
/**
 * 按钮交互
 * @param {*} props        页面内置对象
 * @param {*} id           注册按钮编码
 */
export default function buttonClick(props, id) {
    let selectDatas = props.editTable.getCheckedRows(this.tableId);//获取已勾选数据
    let { addRow, setStatus } = props.editTable;
    let pks= selectDatas && selectDatas.map(item => item.data.values && item.data.values[this.primaryId] && item.data.values[this.primaryId].value);
    //非新增、刷新、取消、保存按钮需判断是否已勾选数据
    if (!['Add', 'Refresh','Cancel','Save','Edit'].includes(id) && !selectDatas.length) {
        toast({color: 'warning', content: '请勾选数据!'});
        return;
    }
    switch (id) {
        //头部 新增
        case 'Add':
            let number = props.editTable.getNumberOfRows(list.tableCode);
            addRow(list.tableCode, number);
            props.button.setPopContent('Delete_i', '');
            props.editTable.setValByKeyAndIndex(list.tableCode, number, 'code', {value: '', display:'', scale:0, isEdit: true});
            listButtonVisible.call(this,props);
            break;
        //头部 修改
        case 'Edit':
            setStatus(list.tableCode, 'edit', () => {
                listButtonVisible.call(this, props);
            });
            break;
        //头部 删除
        case 'Delete':
            deleteBill.call(this, props);
            break;
        //头部 保存
        case 'Save':
            saveBill.call(this, props);
            break;
        //头部 取消
        case 'Cancel':
            props.modal.show('cancelModal');
            break;
        case 'Refresh':
            this.setState({showToast: true})
            searchButtonClick.call(this,props);
        break;
        default:
            break;
    }
}

/**
 * 保存
 * @param {*} props  页面内置对象
 */
function saveBill(props) {
    let { setStatus, getChangedRows, checkRequired } = props.editTable;
    let saveRows = getChangedRows(list.tableCode);//获取修改或新增的行
    let length = props.editTable.getChangedRows(list.tableCode,true).length;
    saveRows.map( e => {
        e.values.name.value = trimStr(e.values.name.value);
        e.values.code.value = trimStr(e.values.code.value);
    });
    let flag = checkRequired(list.tableCode, saveRows);//必填项校验标识
    if (!flag) {
        return;
    }
    if( length == 0 && this.state.editDelData.model.rows.length === 0 ) {
        props.editTable.setStatus(list.tableCode,'browse');
        props.button.setButtonVisible({Save:false,Cancel:false});
        props.button.setMainButton('Add', true);
        return;
    }
    // 删除保存数据中已在编辑态批量删除过的数据
    for ( let i=0;i<saveRows.length;i++ ) {
        for ( let j=0;j<this.state.editDelData.model.rows.length;j++ ) {
            if( saveRows[i].values[this.primaryId].value == this.state.editDelData.model.rows[j].values[this.primaryId].value ) {
                saveRows.splice( i, 1 );
            }
        }
    }
    let saveData = {
        pageid: list.pageCode,
        templetid:list.listOid,
        model: {
            areaType: 'table',
            areacode: list.tableCode,
            rows: saveRows
        }
    }
    // 若在编辑态时，删除了几条数据，先删掉这些数据
    if ( this.state.editDelData.model.rows.length != 0 ) {
        ajax({
            url: `${baseReqUrl}${javaUrl.delete}.do`,
            data: this.state.editDelData,
            async: false,
            success: (res) => {
                if (res.success) {
                    console.log('delete success!!!')
                }
            }
        });
        //清空编辑态暂存数据
        this.state.editDelData.model.rows.length = 0;
    }
    this.setState({showToast: false});
    let repFlag = bodyBtnOperation.call(this,saveData, javaUrl.save, '保存成功!');
    if ( !repFlag ) {
        setStatus(list.tableCode, 'browse');//将表格置为浏览态
        props.button.setPopContent('Delete_i', '确定要删除吗?');
        listButtonVisible.call(this,props);
        searchButtonClick.call(this, props, '0');
    }
}

/**
 * 删除
 * @param {*} props  页面内置对象
 */
function deleteBill(props) {
    let { getCheckedRows } = props.editTable;
    let checkDelDataLen = getCheckedRows(list.tableCode).length;//获取勾选的行
    let delRows = getCheckedRows(list.tableCode).map((e)=> e.data);//得到勾选行的数据
    let pks = delRows && delRows.map( e => e.values && e.values[this.primaryId] && e.values[this.primaryId].value );
    let tableStatus = props.editTable.getStatus(list.tableCode);
    //判断是否勾选
    if (checkDelDataLen == 0) {
        toast({
            color: 'warning', content: '请选择至少一条数据!'
        });
        return;
    }
    if (checkDelDataLen) {
        delRows.map((e) => {
            if(e.values && e.values.pk_finvariety && e.values.pk_finvariety.value == "") {
                props.editTable.deleteTableRowsByRowId(list.tableCode,e.rowid);
            }
        })
    }
    // 过滤数组 
    delRows = delRows.filter((e) => {return !(e.values && e.values.pk_finvariety && e.values.pk_finvariety.value == "")} )
    let delData = {
            pageid: list.pageCode,
            templetid: list.listOid,
            model: {
                areaType: 'table',
                areacode: list.tableCode,
                rows: delRows
            }
    };
    if ( tableStatus === 'edit' ) {
        ajax({
            url: `${baseReqUrl}${javaUrl.checkRef}.do`,
            data: {pks: pks},
            success: (res) => {
                if (res.success) {
                    let { succVOs, failVOs } = res.data;
                    let oprName = {
                        del: '删除'
                    };
                    if ( failVOs.length !== 0 ) {
                        multiToast.call(this, 'del', oprName, res.data);
                    }
                    if ( succVOs.length !== 0 ) {
                        delRows.map( e => { // 前台删除编辑态选择的数据
                            if ( e.values[this.primaryId].value === '' ) {
                                props.editTable.deleteTableRowsByRowId(list.tableCode, e.rowid);
                            }
                        } )
                        // 前台删除没被引用的行
                        for ( let i=0;i<delRows.length;i++ ) {
                            for ( let j=0;j<succVOs.length;j++ ) {
                                if( delRows[i].values[this.primaryId].value == succVOs[j][this.primaryId] ) {
                                    this.state.editDelData.model.rows.push( delRows[i] );
                                    props.editTable.deleteTableRowsByRowId(list.tableCode, delRows[i].rowid );
                                }
                            }
                        }
                    }
                }
            }
        });
    } else {
        //将delData传到state中，以便调用模态框组件进行删除确认并传参
        if( checkDelDataLen == 1 ) {
            this.setState(
                { delData, showToast: false }, () => {
                    props.modal.show('deleteModal');
                }
            );
        } else {
            this.setState(
                { delData, showToast: false }, () => {
                    props.modal.show('deleteModalBatch');
                }
            );
        }
    }
}

// 去前后空格
function trimStr(str){
    return str.replace(/(^\s*)|(\s*$)/g,"");
}