import { ajax, toast } from 'nc-lightapp-front';
import { list } from '../../cons/constant.js';

export function searchButtonClick (props, isFirst = '1', data, type, queryInfo) {
    // 分页信息
    let pageInfo = props.editTable.getTablePageInfo(list.pagecode);
    let querycondition = props.search.getAllSearchData(list.searchCode);
    querycondition.conditions.map( e => delete e.langSeq ); // 去掉langSeq解决条件查询后端报错问题
    let searchdata = {
        querycondition:querycondition,
        pageInfo:pageInfo,
        pagecode: list.pagecode,
        queryAreaCode: list.searchCode,
        oid: list.searchOid,  
        querytype: 'tree',
    };
    ajax({
        url: '/nccloud/tmpub/tmbd/cctypequery.do',
        data:  searchdata,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                if (data) {
                    if (data.grid) {
                        props.editTable.setTableData(this.tableId, data.grid[this.tableId]);
                        let preSysRows = data.grid[this.tableId].rows.map(e => e.values.systemflag.value);
                        let sysRowId = [];
                        let sysIndex = [];
                        for ( let i=0;i<preSysRows.length;i++ ) {
                            if ( preSysRows[i] ) {
                                sysRowId.push(data.grid[this.tableId].rows[i].rowid);
                                sysIndex.push(i);
                            }
                        }
                        if( sysRowId != [] ) {
                            props.editTable.setEditableRowByRowId(this.tableId,sysRowId,false);
                        }
                        props.editTable.setCheckboxDisabled(this.tableId,sysIndex,false);
                        props.button.setButtonDisabled(list.disabled_btn, true);
                        if( isFirst !== '0' ) {
                            if( this.state.showToast ) {
                                toast({ color: 'success', content: `查询成功，共有${data.grid[this.tableId].pageInfo.total}条数据。` });
                            }
                        }
                    } else {
                        this.props.editTable.setTableData(list_table_id, { rows: [] });
                    }
                } 
            }
        },
        error: (err) => {
            toast({ color: 'danger', content: err.message });
            this.props.editTable.setTableData(list_table_id, { rows: [] });
        }
    });
};