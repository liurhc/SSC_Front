import { ajax, toast } from 'nc-lightapp-front';
import { searchButtonClick } from './search';
import { baseReqUrl, javaUrl, list } from '../../cons/constant.js';
import { multiToast } from '../../../public/events'
/**
 * 表体操作列按钮点击交互
 * @param {*} key     注册按钮编码
 * @param {*} record  当前单据的全数据
 */
export function bodyButtonClick (props, key, record) {
    
    let data = {
        pageid: list.pageCode,
        templetid: list.listOid,
        model: {
            areaType: 'table',
            areacode: list.tableCode,
            rows: [record]
        }
    };
    switch (key) {
        case 'DelLine':  //删除
            deleteBill.call(this, props, record, data);
            break;
        case 'StartLine':  //启用
            this.setState({showToast: false})
            bodyBtnOperation.call(this, data, javaUrl.start, '启用成功!');
            break;
        case 'StopLine'://停用
            this.setState({showToast: false})
            bodyBtnOperation.call(this, data, javaUrl.stop, '停用成功!');
            break;
        default:
            break;
    }
}

/**
 * 删除
 * @param {*} props  页面内置对象
 */
function deleteBill(props, record, data) {
    let tableStatus = props.editTable.getStatus(list.tableCode);
    if ( tableStatus === 'edit' ) {
        if ( record.values[this.primaryId].value === '' ) {
            // 刚新增还没保存的数据，前台直接删
            props.editTable.deleteTableRowsByRowId(list.tableCode, record.rowid);
        } else {
            ajax({
                url: `${baseReqUrl}${javaUrl.checkRef}.do`,
                data: {pks: [record.values[this.primaryId].value]},
                success: (res) => {
                    if (res.success) {
                        if ( res.data && res.data.failNum && res.data.failNum !== '0' ) {
                            // 被引用，提示被引用数据不能删除
                            toast({ color: 'danger', content: '该条数据已被引用，删除失败！' });
                        } else {
                            // 未被引用，传入待删除暂存字段,并前台删除本条数据
                            this.state.editDelData.model.rows.push(record);
                            props.editTable.deleteTableRowsByRowId(list.tableCode, record.rowid, true);
                        }
                    }
                }
            });
        }
    } else {
        this.setState({showToast: false})
        bodyBtnOperation.call(this, data, javaUrl.delete, '删除成功!');
    }
}

/**
 * 按钮传参
 * @param {*} data         数据
 * @param {*} path         接口地址
 * @param {*} content      toast弹框显示内容
 * @param {*} isBatch      是否是批量操作
 */
export function bodyBtnOperation ( data, path, content, isBatch= false) {
    let errFlag;
    ajax({
        url: `${baseReqUrl}${path}.do`,
        data,
        async: false,
        success: (res) => {
            if (res.success) {
                if ( !isBatch ) {
                    if ( res.data && res.data.errormessages && res.data.errormessages.length != 0 ) {
                        toast({ color: 'danger', content: '该条数据已被引用，删除失败！' });
                    }else {
                        toast({ color: 'success', content });
                    }
                } else {
                    let oprName = {
                        commit: '提交',
                        uncommit: '收回',
                        del: '删除'
                    };
                    multiToast.call(this, 'del', oprName, res.data);
                }
                searchButtonClick.call(this, this.props)
            }
        },
        error: (err) => {
            toast({ color: 'danger', content: err.message });
            errFlag = err;
        }
    });
    return errFlag
}