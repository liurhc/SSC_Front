import { bodyButtonClick } from './bodyButtonClick';
import { list, appCode, btnLimit } from '../../cons/constant.js';
import { searchButtonClick } from './search';

export default function (props) {
	props.createUIDom(
		{
			pagecode: list.pagecode,//页面code
			appcode: appCode
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta)
					props.meta.setMeta(meta);
					// searchBtnClick.call(this, props);
					searchButtonClick.call(this, props, '0');
				}
				if (data.button) {
					let button = data.button; //将请求到的按钮赋给button属性
					props.button.setButtons(button);
					props.button.setPopContent('DelLine', '确定要删除吗?'); //表体删除按钮气泡弹框适配
					props.button.setButtonDisabled(list.disabled_btn,true);
					props.button.setButtonVisible(['Save','Cancel','Enable','Disenable'],false);
				}
			}
		}
	)
}

function modifierMeta(props, meta) {
	meta[this.tableId].pagination = true;
	meta[this.tableId].items = meta[this.tableId].items.map((item, key) => {
		item.width = 150;
		return item;
	});
	
	//添加操作列
	meta[this.tableId].items.push({
		itemtype: 'customer',
		attrcode: 'opr',
		label: '操作',
		width: 160,
		fixed: 'right',
		className: "table-opr",
		visible: true,
		itemtype:'customer',
		render: (text, record, index) => {
			let buttonAry = [];
			if(!record.values.systemflag.value){
				if( record.values.enablestate.value === '0' ) {
					buttonAry = ['DelLine', 'StopLine'];
				} else {
					buttonAry = ['DelLine', 'StartLine'];
				}
			}
			return props.button.createOprationButton(buttonAry, {
				area: list.bodyCode,
				buttonLimit: btnLimit,
				onButtonClick: (props, key) => bodyButtonClick.call(this, props,key, record)
			});
		}
	});
	return meta;
}
