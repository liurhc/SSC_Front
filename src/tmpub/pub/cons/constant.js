/** 地址栏参数 */
export const URL_PARAM = {
  /** 主键 */
  ID: 'id',
  /** 界面状态 */
  STATE: 'status',
  /** 场景标志 */
  SCENE: 'scene',
  /** 预算联查参数 */
  TBB_LINK: 'pk_ntbparadimvo',
  /** 联查主键 */
  LINK_ID: 'linkId',
  /** 来源单据主键 */
  PK_SRC: 'pk_src'
}

/**
* 场景标志
*/
export const SCENE = {
  /**
  * 默认场景
  */
  DEFAULT: 'defaultsce',
  dd:'ppp',
  /**
  * 审批场景
  */
  APPROVE: 'approvesce',
  /**
  * 联查
  */
  LINK: 'linksce',
  /**
  * 凭证反联查
  */
  FIP: 'fip',
  /**
  * 其他
  */
  OTHER: 'othersce'
}

/**
* 联查类型
*/
export const LINKTYPE = {
  /**普通联查 */
  NORMAL: 'NORMAL',
  /**单据追溯 */
  BILL_REVIEW: 'BILL_REVIEW'
}

/**
 * 联查地址栏参数
 */
export const LINK_PARAM = {
  ARAP: {
    FLAG: "flag",
    FLAG_VALUE: 'ftsLinkArap'
  }
}
/**
 * 模块信息
 */
export const MODULE_INFO = {
  TMPUB: 'tmpub',
  TMPUB_NUM: '3601'
}

/** 公共请求URL定义 */
export const COMMON_URL = {
  //电子签章打印检查
  ELECSIGNPRINTCHECK: '/nccloud/tmpub/pub/elecsignprintcheck.do'
}