import { ajax, toast } from 'nc-lightapp-front';
import { searchButtonClick } from './search';
import { baseReqUrl, javaUrl, list } from '../../cons/constant.js';
import { multiToast } from '../../../public/events'

/**
 * table-button点击事件
 * @param {*} key     注册按钮编码
 * @param {*} record  当前单据的全数据
 */
export function bodyButtonClick (props,key, record) {
    let pk = record.values[list.primaryId] && record.values[list.primaryId].value;
    let index = record.values.numberindex && record.values.numberindex.value;
    switch (key) {
        case 'DelLine':  //删除
            deleteBill.call(this ,props, record);
            break;
        case 'StartLine':  //启用
            this.setState({showToast: false})
            bodyBtnOperation.call(this, {pks: [pk]}, javaUrl.start, '启用成功!', record);
            break;
        case 'StopLine'://停用
            this.setState({showToast: false})
            bodyBtnOperation.call(this, {pks: [pk]}, javaUrl.stop, '停用成功!', record);
            break;
        default:
            break;
    }
}

/**
 * 删除
 * @param {*} props  页面内置对象
 */
function deleteBill(props, record) {
    let pk = record.values[list.primaryId] && record.values[list.primaryId].value;
    let tableStatus = props.editTable.getStatus(list.tableCode);
    if ( tableStatus === 'edit' ) {
        if ( record.values[this.primaryId].value === '' ) {
            // 刚新增还没保存的数据，前台直接删
            props.editTable.deleteTableRowsByRowId(list.tableCode, record.rowid);
        } else {
            ajax({
                url: `${baseReqUrl}${javaUrl.checkRef}.do`,
                data: {pks: [record.values[this.primaryId].value]},
                success: (res) => {
                    if (res.success) {
                        if ( res.data && res.data.failNum && res.data.failNum !== '0' ) {
                            // 被引用，提示被引用数据不能删除
                            toast({ color: 'danger', content: '该条数据已被引用，删除失败！' });
                        } else {
                            // 未被引用，传入待删除暂存字段,并前台删除本条数据
                            this.state.delPks.pks.push(record.values[this.primaryId].value);
                            props.editTable.deleteTableRowsByRowId(list.tableCode, record.rowid, true);
                        }
                    }
                }
            });
        }
    } else {
        this.setState({showToast: false})
        bodyBtnOperation.call(this, {pks: [pk]}, javaUrl.delete, '删除成功!', record);
    }
}

/**
 * 按钮交互
 * @param {*} data         数据
 * @param {*} path         接口地址
 * @param {*} content      toast弹框显示内容
 * @param {*} isBatch      是否是批量操作
 */
export function bodyBtnOperation ( data, path, content, record, isBatch= false) {
    if (isBatch && !data.pks.length) { //批量不针对保存按钮，所有保存按钮不要加isBatch参数
        toast({
            color: 'warning', content: '请选择至少一条数据!'
        });
        return;
    }
    let errFlag;
    ajax({
        url: `${baseReqUrl}${path}.do`,
        data,
        async: false,
        success: (res) => {
            if (res.success) {
                if ( !isBatch ) {
                    if ( res.data && res.data.errormessages && res.data.errormessages.length != 0 ) {
                        toast({ color: 'danger', content: '该条数据已被引用，删除失败！' });
                    }else {
                        toast({ color: 'success', content });
                    }
                } else {
                    let oprName = {
                        commit: '提交',
                        uncommit: '收回',
                        del: '删除'
                    };
                    multiToast.call(this, 'del', oprName, res.data);
                }
                searchButtonClick.call(this, this.props);
            }
        },
        error: (res) => {
            toast({
                color: 'danger', content: res.message
            });
            errFlag = res;
        }
    });
    return errFlag
}