import { bodyButtonClick } from './bodyButtonClick';
import { list, appCode, btnLimit } from '../../cons/constant.js';
import { searchButtonClick } from './search';
import { oprBtnVisible } from '../../../public/events';

export default function (props) {
	props.createUIDom(
		{
			pagecode: list.pagecode,//页面code
			appcode: appCode
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta)
					props.meta.setMeta(meta);
					props.table.setColScale([{areacode: list.tableCode, filedcode: 'name', scale: "1"}]);
					searchButtonClick.call(this, this.props, '0');
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('DelLine', '确定要删除吗?');
					props.button.setButtonDisabled(list.disabled_btn,true);
				}
			}
		}
	)
}

function modifierMeta(props, meta) {
	// meta[this.searchId].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true; //不需要多选参照就注销
	// console.log(meta[this.tableId]);
	
	meta[this.tableId].pagination = true;
	meta[this.tableId].items = meta[this.tableId].items.map((item, key) => {
		item.width = 150;
		return item;
	});
	
	//添加操作列
	meta[this.tableId].items.push({
		itemtype: 'customer',
		attrcode: 'opr',
		label: '操作',
		width: 180,
		fixed: 'right',
		className: "table-opr",
		visible: true,
		itemtype:'customer',
		render: (text, record, index) => {
			let buttonAry = [];
			if(!record.values.system_flag.value){
				if( record.values.enable_state.value === '1' ) {
					buttonAry = ['DelLine', 'StopLine'];
				} else {
					buttonAry = ['DelLine', 'StartLine'];
				}
			}
			return props.button.createOprationButton(buttonAry, {
				area: list.bodyCode,
				buttonLimit: btnLimit,
				onButtonClick: (props, key) => bodyButtonClick.call(this, props,key, record)
			});
		}
	});
	return meta;
}
