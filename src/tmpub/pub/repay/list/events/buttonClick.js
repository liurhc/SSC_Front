import { ajax, toast } from 'nc-lightapp-front';
import { baseReqUrl, javaUrl, list } from '../../cons/constant.js';
import { bodyBtnOperation } from './bodyButtonClick';
import { multiToast } from '../../../public/events'
import { searchButtonClick } from './search';
import { listButtonVisible } from './buttonVisible';

/**
 * 按钮交互
 * @param {*} props        页面内置对象
 * @param {*} id           注册按钮编码
 */
export default function buttonClick(props, id) {
    let selectDatas = props.editTable.getCheckedRows(this.tableId);//获取已勾选数据
    if (!['Add', 'Refresh','Cancel','Save','Edit'].includes(id) && !selectDatas.length) {//非新增刷新按钮时要判断是否已勾选数据
        toast({color: 'warning', content: '请勾选数据!'});
        return; 
    }
    let pks= selectDatas && selectDatas.map(item => item.data.values && item.data.values[this.primaryId] && item.data.values[this.primaryId].value);
    let { addRow } = props.editTable;
    switch (id) {
        //头部 新增
        case 'Add':
            let number = props.editTable.getNumberOfRows(list.tableCode);
            addRow(list.tableCode, number);
            this.setState({showSearchBtn: true})
            props.button.setPopContent('DelLine', '');
            props.editTable.setValByKeyAndIndex(list.tableCode, number, 'code', {value: '', display:'', scale:0, isEdit: true});
            listButtonVisible.call(this,props);
            break;
        //头部 修改
        case 'Edit':
            props.editTable.setStatus(list.tableCode, 'edit', () => {
                listButtonVisible.call(this, props);
            });
            break;
        //头部 删除
        case 'Delete':
            deleteBill.call(this, props, pks, selectDatas);
            break;
        //头部 保存
        case 'Save':
            saveBill.call(this, props);
            break;
        //头部 取消
        case 'Cancel':
            props.modal.show('cancelModal');
            break;
        //头部 启用
        case 'Enable':
            let enableData = {
                pks: pks
            };
            bodyBtnOperation.call(this,enableData,javaUrl.start,'启用成功', selectDatas, true);
            break;
        //头部 停用
        case 'Disenable':
            let disenableData = {
                pks: pks
            };
            bodyBtnOperation.call(this,disenableData,javaUrl.stop,'停用成功', selectDatas, true);
            break;
        case 'Refresh':
            this.setState({showToast: true})
            searchButtonClick.call(this,props);
        break;
        default:
            break;
    }
}

/**
 * 删除
 * @param {*} props  页面内置对象
 * @param {*} pks  待删除的pk
 */
function deleteBill(props, pks, selectDatas) {
    let tableStatus = props.editTable.getStatus(list.tableCode);
    let checkDelDataLen = props.editTable.getCheckedRows(list.tableCode).length;//获取勾选的行
    if ( tableStatus === 'edit' ) {
        ajax({
            url: `${baseReqUrl}${javaUrl.checkRef}.do`,
            data: {pks: pks},
            success: (res) => {
                if (res.success) {
                    let { succVOs, failVOs } = res.data;
                    let oprName = {
                        del: '删除'
                    };
                    if ( failVOs.length !== 0 ) {
                        multiToast.call(this, 'del', oprName, res.data);
                    }
                    if ( succVOs.length !== 0 ) {
                        selectDatas.map( e => { // 前台删除编辑态新增但没保存的数据
                            if ( e.data.values.pk_repaymentmethod.value === '' ) {
                                props.editTable.deleteTableRowsByRowId(list.tableCode, e.data.rowid);
                            }
                        } )
                        succVOs.map( e => {
                            this.state.delPks.pks.push( e.pk_repaymentmethod );
                        } );
                        // 前台删除没被引用的行
                        for ( let i=0;i<selectDatas.length;i++ ) {
                            for ( let j=0;j<succVOs.length;j++ ) {
                                if( selectDatas[i].data.values.pk_repaymentmethod.value == succVOs[j].pk_repaymentmethod ) {
                                    props.editTable.deleteTableRowsByRowId(list.tableCode, selectDatas[i].data.rowid );
                                }
                            }
                        }
                    }
                }
            }
        });
    } else {
        if( checkDelDataLen == 1 ) {
            this.setState(
                { pks, showToast: false }, () => {
                    props.modal.show('deleteModal');
                }
            );
        } else {
            this.setState(
                { pks, showToast: false }, () => {
                    props.modal.show('deleteModalBatch');
                }
            );
        }
    }
}

/**
 * 保存
 * @param {*} props  页面内置对象
 */
function saveBill(props) {
    let length = props.editTable.getChangedRows(list.tableCode,true).length;
    if( length == 0 && this.state.delPks.length === 0 ) {
        props.editTable.setStatus(list.tableCode,'browse');
        props.button.setButtonVisible({Save:false,Cancel:false,Edit:true,Refresh:true});
        props.button.setMainButton('Add', true);
        return;
    }
    let saveData =  props.editTable.getChangedRows(list.tableCode,false);
    saveData.map( e => {
        // 去空格
        e.values.name.value = trimStr(e.values.name.value);
        e.values.code.value = trimStr(e.values.code.value);
    });
    // 删除保存数据中已在编辑态批量删除过的数据
    for ( let i=0;i<saveData.length;i++ ) {
        for ( let j=0;j<this.state.delPks.pks.length;j++ ) {
            if( saveData[i].values.pk_repaymentmethod.value == this.state.delPks.pks[j] ) {
                saveData.splice( i, 1 );
            }
        }
    }
    // 必输项校验
    let saveFlag = props.editTable.checkRequired(list.tableCode, saveData);
    if (!saveFlag) {
        return;
    }
    let record = {
        pageid: list.tableCode,
        templetid: list.listOid,
        model: {
            areaType: 'table',
            areacode: list.tableCode,
            rows: saveData
        }
    } 
    // 若在编辑态时，删除了几条数据，先删掉这些数据
    if ( this.state.delPks.pks.length != 0 ) {
        ajax({
            url: `${baseReqUrl}${javaUrl.delete}.do`,
            data: this.state.delPks,
            async: false,
            success: (res) => {
                if (res.success) {
                    console.log('delete success!!!')
                }
            }
        });
        //清空编辑态暂存数据
        this.state.delPks.pks.length = 0;
    }
    this.setState({showToast: false});
    // 前台解决空格校验
    record.model.rows.map(e => {
        if ( e.values.repay_intst_method.value === '5' ) {
            e.values.repay_intst_period.value = '-1';
        }
        if ( e.values.repay_prcpl_method.value === '5' ) {
            e.values.repay_prcpl_period.value = '-1';
        }
    } )
    let repFlag = bodyBtnOperation.call(this,record, javaUrl.save, '保存成功!');
    if ( !repFlag ) {
        props.editTable.setStatus(list.tableCode,'browse');
        this.setState({showSearchBtn: false})
        listButtonVisible.call(this,props);
    }
}

// 去前后空格
function trimStr(str){
    return str.replace(/(^\s*)|(\s*$)/g,"");
}