import { ajax, toast } from 'nc-lightapp-front';
import { list, baseReqUrl, javaUrl } from '../../cons/constant.js';

export function searchButtonClick (props, isFirst = '1', data, type, queryInfo) {
    // 分页信息
    let pageInfo = props.editTable.getTablePageInfo(list.pagecode);
    let querycondition = props.search.getAllSearchData(list.searchCode);
    querycondition.conditions.map( e => delete e.langSeq ); // 去掉langSeq解决后端相关报错
    let searchdata = {
        querycondition:querycondition,
        pageInfo:pageInfo,
        pagecode: list.pagecode,
        queryAreaCode: list.searchCode,
        oid: list.searchOid,  
        querytype: 'tree',
    };
    ajax({
        url: '/nccloud/tmpub/tmbd/repaymentmethodquery.do',
        data:  searchdata,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                if (data) {
                    if (data.grid) {
                        // 传空格通不过校验的权宜之计
                        data.grid[this.tableId].rows.map(e => {
                            if ( e.values.repay_prcpl_period.value === '-1' ) {
                                e.values.repay_prcpl_period.value = ' ';
                            }
                            if ( e.values.repay_intst_period.value === '-1' ) {
                                e.values.repay_intst_period.value = ' ';
                            }
                        });
                        props.editTable.setTableData(this.tableId, data.grid[this.tableId]);
                        let preSysRows = data.grid[this.tableId].rows.map(e => e.values.system_flag.value);
                        let prcplMethod = data.grid[this.tableId].rows.map(e => e.values.repay_prcpl_method.value);
                        let intstMethod = data.grid[this.tableId].rows.map(e => e.values.repay_intst_method.value);
                        let sysRowId = [];
                        let sysIndex = [];
                        let prcplMethodIndex = [];
                        let intstMethodIndex = [];
                        for ( let i=0;i<preSysRows.length;i++ ) {
                            if ( preSysRows[i] ) {
                                sysRowId.push(data.grid[this.tableId].rows[i].rowid);
                                sysIndex.push(i);
                            }
                        }
                        for ( let i=0;i<prcplMethod.length;i++ ) {
                            if( prcplMethod[i] === '5' ) {
                                prcplMethodIndex.push(i);
                            }
                        }
                        for ( let i=0;i<intstMethod.length;i++ ) {
                            if( intstMethod[i] === '5' ) {
                                intstMethodIndex.push(i);
                            }
                        }
                        if( sysRowId != [] ) {
                            props.editTable.setEditableRowByRowId(this.tableId,sysRowId,false);
                        }
                        if( prcplMethodIndex != [] ) {
                            props.editTable.setEditableRowKeyByIndex(this.tableId, prcplMethodIndex, 'repay_prcpl_period', false);
                        }
                        props.editTable.setCheckboxDisabled(this.tableId,sysIndex,false);
                        if( intstMethodIndex != [] ) {
                            props.editTable.setEditableRowKeyByIndex(this.tableId, intstMethodIndex, 'repay_intst_period', false);
                        }
                        props.editTable.setColScale([{areacode: list.tableCode, filedcode: 'name', scale: "1"},]);
                        if( isFirst !== '0' ) {
                            if( this.state.showToast ) {
                                toast({ color: 'success', content: `查询成功，共有${data.grid[this.tableId].pageInfo.total}条数据。` });
                            }
                        }
                    } else {
                        this.props.editTable.setTableData(this.tableId, { rows: [] });
                    }
                } 
            }
        },
        error: (err) => {
            toast({ color: 'danger', content: err.message });
            this.props.editTable.setTableData(this.tableId, { rows: [] });
        }
    });
};

/**
 * 点击查询，获取查询区数据
 * @param {*} props           页面内置对象
 * @param {*} condition       大家查一下文档，没细看
 * @param {*} props           ...
 * @param {*} condition       ...
 */
export function searchBtnClick (props,isFirst=1, condition, type, querycondition) {
    //查询区域查询条件(判断查询参数是否传了，若没传，则将目前的查询参数交给它)
    if (!querycondition) {
        querycondition = props.search.getAllSearchData(this.searchId);
        if (!querycondition) {
            return;
        }
    }
    if (!condition) {
        condition = props.search.getAllSearchData(this.searchId);
    }

    let pageInfo = props.editTable.getTablePageInfo(list.tableCode);
    let searchdata = {
        querycondition: condition,
        pageInfo: pageInfo,
        pagecode: list.pageCode,
        queryAreaCode: list.searchCode,  //查询区编码
        oid: list.searchOid,  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
        querytype: 'tree'
    };
    getListData.call(this, props, javaUrl.list, searchdata, isFirst);
};

/**
 * 获取分组查询条件
 * @param {*} groupKey 分组键
 */
const getGroupCondition = function (groupKey) {
    let groupCondition;
    switch (groupKey) {
        //待提交
        case 0:
            groupCondition = {
                field: 'billstatus',
                value: {
                    firstvalue: 1,
                    secondvalue: null
                },
                oprtype: '='
            };
            break;
        //待经办
        case 1:
            groupCondition = {
                field: 'billstatus',
                value: {
                    firstvalue: 0,
                    secondvalue: null
                },
                oprtype: '='
            };
            break;
        //待审批
        case 2:
            groupCondition = {
                field: 'vbillstatus',
                value: {
                    firstvalue: 2,
                    secondvalue: null
                },
                oprtype: '='
            };
            break;
        //待支付
        case 3:
            groupCondition = {
                field: 'billstatus',
                value: {
                    firstvalue: 3,
                    secondvalue: null
                },
                oprtype: '='
            };
            break;
        //支付中
        case 4:
            groupCondition = {
                field: 'billstatus',
                value: {
                    firstvalue: 4,
                    secondvalue: null
                },
                oprtype: '='
            };
            break;
        //全部
        case 5:
            groupCondition = {};
            break;
        //默认作为全部处理
        default:
            groupCondition = {};
            break;
    }
    return groupCondition;
}

/**
 * 点击分页、改变每页条数
 * @param {*} props           页面内置对象
 * @param {*} config          大家查一下文档，没细看，貌似没用上
 * @param {*} pks             拿到当前页的所有pks
 */
export function pageInfoClick (props,  isFirst = '1', config, pks) {
    let data = {
        pks,
        pageCode: this.pageId
    };
    getListData.call(this ,props, javaUrl.pks, data, isFirst);
}

/**
 * 请求列表接口
 * @param {*} path       接口地址
 * @param {*} data       数据
 */
function getListData (props, path, data, isFirst = '1') {
    ajax({
        url: `${baseReqUrl}${path}.do`,
        data,
        success: (res) => {
            listRender.call(this,props, res, isFirst);
        },
        error: () => {
            listRender.call(this,props, {success: false}, isFirst);
        }
    });
}

/**
 * 拿到返回的数据，对列表进行渲染
 * @param {*} res            后台返回的res
 */
function listRender (props,res, isFirst=1) {
    let { success, data } = res;
    if (success && data && data.grid && data.grid[this.tableId]) {
        props.editTable.setTableData(this.tableId, data.grid[this.tableId]);
        let preSysRows = data.grid[this.tableId].rows.map(e => e.values.system_flag.value);
        let prcplMethod = data.grid[this.tableId].rows.map(e => e.values.repay_prcpl_method.value);
        let intstMethod = data.grid[this.tableId].rows.map(e => e.values.repay_intst_method.value);
        let sysRowId = [];
        let sysIndex = [];
        let prcplMethodIndex = [];
        let intstMethodIndex = [];
        for ( let i=0;i<preSysRows.length;i++ ) {
            if ( preSysRows[i] ) {
                sysRowId.push(data.grid[this.tableId].rows[i].rowid);
                sysIndex.push(i);
            }
        }
        for ( let i=0;i<prcplMethod.length;i++ ) {
            if( prcplMethod[i] === '5' ) {
                prcplMethodIndex.push(i);
            }
        }
        for ( let i=0;i<intstMethod.length;i++ ) {
            if( intstMethod[i] === '5' ) {
                intstMethodIndex.push(i);
            }
        }
        if( sysRowId === [] ) {
            props.editTable.setEditableRowByRowId(this.tableId,sysRowId,false);
        }
        if( prcplMethodIndex === [] ) {
            props.editTable.setEditableRowKeyByIndex(this.tableId, prcplMethodIndex, 'repay_prcpl_period', false);
        }
        props.editTable.setCheckboxDisabled(this.tableId,sysIndex,false);
        if( intstMethodIndex === [] ) {
            props.editTable.setEditableRowKeyByIndex(this.tableId, intstMethodIndex, 'repay_intst_period', false);
        }
        props.editTable.setColScale([{areacode: list.tableCode, filedcode: 'name', scale: "1"},]);
        console.log(data.grid[this.tableId])
        // if( isFirst !== '0' ) {
            toast({ color: 'success', content: `查询成功，共有${data.grid[this.tableId].pageInfo.total}条数据。` });
        // }
    } else {
        props.editTable.setTableData(this.tableId, { rows: [] });
    }
}