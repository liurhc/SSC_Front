import { list } from '../../cons/constant.js';
/**
 * 根据table的状态（browse或edit）渲染页面上的组件
 * @param {*} props  页面内置对象
 */

export function listButtonVisible (props) {
    let { getStatus } = props.editTable;
    let { setButtonVisible,setMainButton,setPopContent } = props.button;
    let statusOfTable = getStatus(list.tableCode);//获取表格状态（编辑or浏览）
    let isBrowse = statusOfTable === "browse";
    if(isBrowse){
        setButtonVisible(['Save','Cancel'],false);
        setButtonVisible(['Refresh', 'Edit'], true);
        setMainButton('Add', true);
        setPopContent('DelLine', '确定要删除吗?');
        this.setState({
            showSearch: true
        });
    }else{
        setButtonVisible(['Save','Cancel'],true);
        setButtonVisible(['Refresh', 'Edit'], false);
        setMainButton('Add', false);
        setPopContent('DelLine', '');
        this.setState({
            showSearch: false
        });
    }
}