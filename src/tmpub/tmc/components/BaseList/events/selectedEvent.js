import { toggleListHeadBtnDisabled } from '../../../container/page';

//单选
export function selectedEvent(props, moduleId, record, index, status) {
    toggleListHeadBtnDisabled.call(this);
    this.props.onTableSelect && this.props.onTableSelect(props, moduleId, record, index, status);
}

export function selectedAllEvent(props, moduleId, status, length) {
    toggleListHeadBtnDisabled.call(this);
    this.props.onTableSelectAll && this.props.onTableSelectAll(props, moduleId, status, length);
}