import React from 'react';
import { base, createPageIcon } from 'nc-lightapp-front';
const { NCAffix } = base;

const Header = ({ title, children }) => (
    <NCAffix>
        <div className="nc-bill-header-area">
            <div className="header-title-search-area">
                {createPageIcon()}
                <h2 className="title-search-detail">{title}</h2>
            </div>
            <div className="header-button-area">{children}</div>
        </div>
    </NCAffix>
)
export default Header;