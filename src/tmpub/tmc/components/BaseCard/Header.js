import React from 'react';
import { base } from 'nc-lightapp-front';
const { NCDiv: Div } = base;

const Header = ({ children }) => {
    return (
		<Div
			areaCode={Div.config.HEADER}
			className="nc-bill-header-area"
		>
			<div className="nc-bill-header-area">
				<div>{children[0]}</div>
				<div className="header-button-area">{children[1]}</div>
				<div className="header-cardPagination-area">{children[2]}</div>
			</div>
		</Div>
    )
}
	
export default Header;
