/* 
 开始、结束日期弹框组件
 created by: liyaoh 2018-12-03
*/
import React, { Component } from 'react';
import { base, toast } from 'nc-lightapp-front';
import moment from 'moment';
const { NCModal, NCDatePicker, NCButton, NCHotKeys } = base;

export default class BeginEndDateModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            inlt: null,
            beginDate: props.begin.value || null,
            endDate: props.end.value || null
        }
    }

    componentWillMount() {
        let callback = (json, status, inlt) => { // json 多语json格式参数； status： 是否请求到json多语，可用来判断多语资源是否请求到并进行一些别的操作； inlt： 可用来进行占位符的一些操作
            if (status) {
                this.setState({ json, inlt })       // 保存json和inlt到页面state中并刷新页面
            }
        }
        this.props.MultiInit.getMultiLang({ moduleId: 'TMCPUB', domainName: 'tmpub', callback });
    }

    //关闭弹窗
    handleClose = () => {
        this.props.onClose();
    }

    //清空日期框的值
    clearDateValue = () => {
        this.setState({
            beginDate: null,
            endDate: null
        });
    }

    //确定
    handleConfirm = () => {
        const { beginDate, endDate } = this.state;
        if(this.valid()){
            this.props.onConfirm({
                beginDate, 
                endDate
            });
        }
    }

    //校验
    valid = () => {
        let valid = true;
        let { beginDate, endDate } = this.state;
        let { begin, end } = this.props;
        if(!beginDate){
            toast({duration: 3, color: 'warning', content: `${this.state.json['TMCPUB-000051']}${begin.label}`});/* 国际化处理： 请输入*/
            valid = false;
            return;
        }else if(!endDate){
            toast({duration: 3, color: 'warning', content: `${this.state.json['TMCPUB-000051']}${end.label}`});/* 国际化处理： 请输入*/
            valid = false;
            return;
        } else if (!moment(beginDate).isSame(endDate) && !moment(beginDate).isBefore(endDate)){
            toast({duration: 3, color: 'warning', content: `${end.label}${this.state.json['TMCPUB-000052']}${begin.label}`});/* 国际化处理： 必须晚于*/
            valid = false;
            return;
        }
        return valid;
    }

    render(){
        const { beginDate, endDate, beginOpen, endOpen } = this.state;
        const { showModal, title, begin, end } = this.props;
        return <NCModal
            show={showModal}
            style={{ width: 420 }}
            size='sm'
            onHide={this.handleClose}
            onExited={this.clearDateValue}
        >
            <NCModal.Header closeButton={'true'}>
                <NCModal.Title>{title}</NCModal.Title>
            </NCModal.Header>
            <NCModal.Body size="sm">
                <div className="lightapp-component-form">
                    <div className="form-item" style={{width: '100%'}}>
                        <div className="form-item-label">{begin.label}</div>
                        <div className="form-item-control">
                            <NCDatePicker
                                value={beginDate}
                                placeholder={begin.placeholder}
                                onChange={val => {
                                    this.setState({
                                        beginDate: val,
                                        beginOpen: false
                                    });
                                }}
                            />
                        </div>
                    </div>
                    <div className="form-item" style={{width: '100%'}}>
                        <div className="form-item-label">{end.label}</div>
                        <div className="form-item-control">
                            <NCDatePicker
                                value={endDate}
                                placeholder={end.placeholder}
                                onChange={val => {
                                    this.setState({
                                        endDate: val,
                                        endOpen: false
                                    });
                                }}
                            />
                        </div>
                    </div>
                </div>
            </NCModal.Body>
            <NCModal.Footer>
                <NCHotKeys
                    // 定制class
                    className="reset-hotkeys-wrapper"
                    keyMap={{
                        confirmActionSign: ["NC_MODAL_CONFIRM", "Alt+Y"],
                            cancelActionSign: ["NC_MODAL_CALCEL", "Alt+N"],
                    }}
                    handlers={{
                        confirmActionSign: () => {
                            this.handleConfirm()
                        },
                        cancelActionSign: () => {
                            this.handleClose()
                        }
                    }}
                    // 触发区域  默认为 document.body
                    focused={true}
                />
                <NCButton colors="primary" onClick={this.handleConfirm}>{this.state.json['TMCPUB-000053']}</NCButton>{/* 国际化处理： 确定*/}
                <NCButton onClick={this.handleClose}>{this.state.json['TMCPUB-000019']}</NCButton>{/* 国际化处理： 取消*/}
            </NCModal.Footer>
        </NCModal>
    }
}
