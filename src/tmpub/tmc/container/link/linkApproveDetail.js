/**
 * 联查审批详情
 *
 * @param {*} billId - 主键id
 */
export default function(billId) {
    this.setState({
        showApproveDetail: true,
        billInfo: { billId }
    });
}