import { ajax, base, toast, cacheTools, print ,cardCache,promptBox} from 'nc-lightapp-front';
import { tableId, formId,dataSource,pkname} from '../constants';
import { headButton } from '../../../../public/components/pubUtils/buttonName.js';
import arapLinkReport from "../../../../public/components/arapBillLinkReport.js";
import linkvouchar from '../../../../public/components/linkvouchar.js'
import madeBill from '../../../../public/components/madeBill.js';
let {getDefData,updateCache ,deleteCacheById} = cardCache;
export default function (props, id) {
  switch (id) {
    case headButton.Save:
      this.saveBill();
      break
    case headButton.Edit:
      ajax({
        url: '/nccloud/arap/arappub/edit.do',
        data: {
          pk_bill: this.props.getUrlParam('id'),
          billType: this.billType
        },
        success: (res) => {
          if (res.success) {
            props.setUrlParam({status:'edit'})
            this.props.cardTable.setStatus(this.tableId, 'edit');
            this.props.form.setFormStatus(this.formId, 'edit');
            this.toggleShow()
          }
        }
      });
      break;
    case headButton.Cancel:
      promptBox({
        color: 'warning',                 
        title: this.state.json['estireceivable-000000'],                  /* 国际化处理： 取消*/
        content: this.state.json['estireceivable-000001'],             /* 国际化处理： ​确定要取消吗？*/
        noFooter: false,                 
        noCancelBtn: false,              
        beSureBtnName: this.state.json['estireceivable-000002'],           /* 国际化处理： 确定*/
        cancelBtnName: this.state.json['estireceivable-000000'] ,          /* 国际化处理： 取消*/
        beSureBtnClick: this.cancel  
      });
      break
    case headButton.Pausetrans://挂起操作
      this.pause('/nccloud/arap/arappub/pause.do');
      break;
    case headButton.Cancelpause://取消挂起操作
      this.pause('/nccloud/arap/arappub/cancelpause.do');
      break;
      case headButton.Approve://审批
      this.pubajax('/nccloud/arap/arappub/approve.do');
      break;
    case headButton.UnApprove://取消审批
      this.pubajax('/nccloud/arap/arappub/unapprove.do');
      break;
    case headButton.BillLinkQuery://联查单据
      this.setState({ showBillTrack: true })
      break;
    case headButton.LinkBal://联查余额表
        arapLinkReport(this.props,this.props.form.getFormItemsValue(this.formId, 'pk_estirecbill').value,this.billType,this.props.form.getFormItemsValue(this.formId, 'objtype').value);
        break;
    case headButton.LinkVouchar://联查凭证
      linkvouchar(this,props, this.props.form.getAllFormValue(this.formId), this.props.form.getFormItemsValue(this.formId, 'pk_estirecbill').value, this.props.getSearchParam('c'));
      break;
    case headButton.MadeBill://制单
      let madeData = [{
        pk_bill: props.form.getFormItemsValue(formId, 'pk_estirecbill').value,
        billType: this.billType,
        tradeType: props.form.getFormItemsValue(formId, 'pk_tradetype').value
      }]
      madeBill(this,props,madeData,'',props.getSearchParam('c'));
      break;
    case 'Print'://打印
      this.onPrint();
      break;
    case headButton.Output://打印输出
      this.printOutput();
      break;
    case headButton.OfficalPrint://正式打印
      this.officalPrintOutput();
      break;
    case headButton.CancelPrint://取消正式打印
      this.cancelPrintOutput();
      break;
    case headButton.AttachManage:
      let flag = props.getUrlParam('status'); 
      if (flag == 'add'||props.getUrlParam('copyFlag')=='copy'||props.getUrlParam('writebackFlag')=='redBack') {
        toast({ color: 'warning', content: this.state.json['estireceivable-000003'] });/* 国际化处理： 请保存后再进行上传附件！*/
        return;
      }
      this.setState({
        showUploader: true,
        target: null
      })
      break;
    case headButton.Refresh://刷新
      ajax({
        url: '/nccloud/arap/arappub/cardRefresh.do',
        data: {
          pk_bill: this.props.getUrlParam('id'),
          pageId: this.getPagecode(),
          billType: this.billType
        },
        success: (res) => {
          if (res.data) {
            toast({ color: 'success', title: this.state.json['estireceivable-000022']});/* 国际化处理： 刷新成功*///
            updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
            if (res.data.head) {
              this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
            }
            if (res.data.body) {
              this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
            }
          } else {
            this.props.form.EmptyAllFormValue(this.formId);
            this.props.cardTable.setTableData(this.tableId, { rows: [] });
          }
          this.toggleShow();
        },
        error: (res) => {
          this.props.form.EmptyAllFormValue(this.formId);
          this.props.cardTable.setTableData(this.tableId, { rows: [] });
          deleteCacheById(pkname, this.props.getUrlParam('id'), dataSource);
          this.toggleShow();
          let str = String(res);
          let content = str.substring(6, str.length);
          toast({ color: 'danger', content: content });
        }
      });
      break;
    default:
      break
  }
}
