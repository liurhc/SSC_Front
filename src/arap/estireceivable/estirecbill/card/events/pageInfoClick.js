import {ajax,cardCache} from 'nc-lightapp-front';
import { dataSource,pkname} from '../constants';
let { getCacheById, updateCache } = cardCache;
export default function (props, pk) {
    if(!pk){//如果刷新了浏览器，那么pk将不会存在，如果pk不存在，return
        return;
    }
    // 后台还没更新，暂不可用
    let cardData = getCacheById(pk, dataSource);
    if(cardData){
        props.setUrlParam({id:pk})
        props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
        props.cardTable.setTableData(this.tableId, cardData.body[this.tableId],null, null, true);              
        this.toggleShow();
        
    }else{
        ajax({
            url: '/nccloud/arap/estirecbill/cardquery.do',
            data: {
                pk_bill: pk
            },
            success: (res) => {
                if (res.data) {
                    if (res.data.head) {
                        this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                    }
                    if (res.data.body) {
                        this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId],null, null, true);
                    }
                    updateCache(pkname,pk,res.data,this.formId,dataSource);
                    props.setUrlParam({id:pk})  
                    this.toggleShow();
                } 
            }
        });


    }
}
