//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, cacheTools, high, print, cardCache, getMultiLang } from 'nc-lightapp-front';
let { NCFormControl, NCButton } = base;
import { buttonClick, initTemplate, afterEvent, pageInfoClick } from './events';
import {
	buttonVisible,
	onSelectedCardBodyEditControl,
	initCardBodybrowseControl
} from '../../../public/components/pubUtils/buttonvisible.js';
import { tableId, formId, billType, nodekey, dataSource, pkname, linkPageId } from './constants';
import { bodyBeforeEvent } from '../../../public/components/pubUtils/arapTableRefFilter';
import { formBeforeEvent } from '../../../public/components/pubUtils/arapFormRefFilter';
import linkSourceCard from '../../../public/components/linkSourceCard.js';
import { dealCardData } from '../../../public/components/pubUtils/dealCardData';
let {
	setDefData,
	getDefData,
	addCache,
	getNextId,
	deleteCacheById,
	getCacheById,
	updateCache,
	getCurrentLastId
} = cardCache;
let { NCBackBtn } = base;
const { NCUploader, BillTrack, PrintOutput } = high;
class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = formId;
		this.tableId = tableId;
		this.billType = billType;
		this.state = {
			showBillTrack: false, //控制单据追溯是否显示
			showUploader: false, //附件
			target: null,
			json: {}
		};
		this.printData = {
			billtype: billType, //单据类型
			appcode: props.getSearchParam('c'), //功能节点编码，即模板编码
			nodekey: props.getSearchParam('p'), //模板节点标识  =交易类型
			oids: [ this.props.getUrlParam('id') ], // 功能节点的数据主键
			userjson: billType //单据类型,billtype不是必需字段，后台没有设置接收字段，以userjson代替
		};
		this.outputData = {
			billtype: billType, //单据类型
			appcode: props.getSearchParam('c'),
			funcode: props.getSearchParam('c'),
			nodekey: props.getSearchParam('p'), //模板节点标识  =交易类型
			oids: [ this.props.getUrlParam('id') ], // 功能节点的数据主键
			userjson: billType, //单据类型
			outputType: 'output'
		};
		this.Info = {
			allButtonsKey: [],
			isModelSave: false //是否是整单保存，默认为false
		};
	}
	componentDidMount() {}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				window.onbeforeunload = () => {
					let status = this.props.getUrlParam('status');
					if (status == 'edit' || status == 'add') {
						return '';
					}
				};
				initTemplate.call(this, this.props, this.initShow);
			});
		};
		getMultiLang({
			moduleId: [ 'estireceivable', 'public' ],
			domainName: 'arap',
			currentLocale: 'simpchn',
			callback
		});
	}
	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		});
	};

	getPagecode = () => {
		let pagecode = this.props.getUrlParam('pagecode');
		if (!pagecode) {
			pagecode = this.props.getSearchParam('p');
		}
		return pagecode;
	};

	//页面初始化
	initShow = () => {
		if (this.props.getUrlParam('scene') && this.props.getUrlParam('scene') == 'fip') {
			linkSourceCard(
				this.props,
				linkPageId,
				'pk_estirecbill',
				this.formId,
				this.tableId,
				pkname,
				dataSource,
				this
			);
		} else if (!this.props.getUrlParam('status')) {
			this.toggleShow();
		} else if (this.props.getUrlParam('status') != 'add') {
			ajax({
				url: '/nccloud/arap/estirecbill/cardquery.do',
				data: { pk_bill: this.props.getUrlParam('id') },
				success: (res) => {
					this.props.beforeUpdatePage(); //打开开关
					if (res.data) {
						updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}

					let status = this.props.getUrlParam('status');
					this.props.cardTable.setStatus(this.tableId, status);
					this.props.form.setFormStatus(this.formId, status);
					this.props.updatePage(this.formId, this.tableId); //关闭开关
					this.props.setUrlParam({ pagecode: res.data.head[this.formId].rows[0].values.pk_tradetype.value });
					this.toggleShow();
				}
			});
		}
	};

	beforeUpload(billId, fullPath, file, fileList) {
		// 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表
		const isJPG = true;
		if (!isJPG) {
			alert(this.state.json['estireceivable-000005']); /* 国际化处理： 只支持png格式图片*/
		}
		const isLt2M = file.size / 1024 / 1024 < 10;
		if (!isLt2M) {
			alert(this.state.json['estireceivable-000006']); /* 国际化处理： 上传大小小于10M*/
		}
		return isJPG && isLt2M;
		// 备注： return false 不执行上传  return true 执行上传
	}

	componentWillReceiveProps(nextProps) {}
	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		if (!status) {
			status = 'browse';
		} //如果未定义，默认给值浏览态
		let trueBtn = []; //可见的按钮
		let falseBtn = []; //不可见的按钮
		for (let i = 0; i < this.Info.allButtonsKey.length; i++) {
			let flag = buttonVisible(
				status,
				this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId).head[this.formId]
					.rows[0].values,
				this.Info.allButtonsKey[i],
				'card',
				this
			);
			if (flag) {
				trueBtn.push(this.Info.allButtonsKey[i]);
			} else {
				falseBtn.push(this.Info.allButtonsKey[i]);
			}
		}
		//初始化肩部按钮信息增行等按钮的控制
		if (status == 'browse') {
			initCardBodybrowseControl(this);
		}
		this.props.button.setButtonVisible(trueBtn, true);
		this.props.button.setButtonVisible(falseBtn, false);
		if (status != 'browse') {
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: false });
		} else {
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: true });
		}
	};

	//卡片表体点击行事件
	onSelected = () => {
		onSelectedCardBodyEditControl(this);
	};

	//挂起和取消挂起操作
	pause = (url) => {
		let selectedData = this.props.cardTable.getCheckedRows(this.tableId);
		if (selectedData.length == 0) {
			toast({ color: 'warning', content: this.state.json['estireceivable-000007'] }); /* 国际化处理： 请选择表体行!*/
			return;
		}
		let pauseObj = [];
		selectedData.forEach((val) => {
			pauseObj.push(val.data.values.pk_estirecitem.value);
		});
		let data = {
			pk_items: pauseObj,
			pk_bill: this.props.getUrlParam('id'),
			ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
			pageId: this.getPagecode(),
			billType: this.billType
		};
		ajax({
			url: url,
			data: data,
			success: (res) => {
				if (res.data.message) {
					toast({
						duration: 'infinity',
						color: res.data.PopupWindowStyle,
						content: res.data.message
					});
				} else {
					toast({ color: 'success', content: this.state.json['estireceivable-000008'] }); /* 国际化处理： 操作成功*/
				}
				if (res.data.billCard) {
					if (res.data.billCard.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.billCard.head[this.formId] });
					}
					if (res.data.billCard.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.billCard.body[this.tableId]);
					}
					let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
					updateCache(pkname, this.props.getUrlParam('id'), newCardData, this.formId, dataSource);
				}
				this.onSelected();
			}
		});
	};

	//保存单据
	saveBill = () => {
		if (!this.props.form.isCheckNow(this.formId)) {
			//表单验证
			return;
		}
		if (!this.props.cardTable.checkTableRequired(this.tableId)) {
			//表格验证
			return;
		}
		let cardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
		let newCardData = dealCardData(this, cardData); //去掉空值，减少压缩时间
		let datas = {
			cardData: newCardData,
			uiState: this.props.getUrlParam('status')
		};
		let callback = () => {
			ajax({
				url: '/nccloud/arap/arappub/save.do',
				data: datas,
				success: (res) => {
					let pk_estirecbill = null;
					let pk_tradetype = null;
					if (res.success) {
						if (res.data) {
							toast({
								color: 'success',
								content: this.state.json['estireceivable-000009']
							}); /* 国际化处理： 保存成功*/
							this.props.beforeUpdatePage(); //打开开关
							if (res.data.head && res.data.head[this.formId]) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
								pk_estirecbill = res.data.head[this.formId].rows[0].values.pk_estirecbill.value;
								pk_tradetype = res.data.head[this.formId].rows[0].values.pk_tradetype.value;
							}
							if (res.data.body && res.data.body[this.tableId]) {
								this.props.cardTable.updateDataByRowId(this.tableId, res.data.body[this.tableId]);
							}
						}
						this.props.cardTable.setStatus(this.tableId, 'browse');
						this.props.form.setFormStatus(this.formId, 'browse');
						this.props.updatePage(this.formId, this.tableId); //关闭开关
						let newCardData = this.props.createMasterChildData(
							this.getPagecode(),
							this.formId,
							this.tableId
						);
						updateCache(pkname, pk_estirecbill, newCardData, this.formId, dataSource); //修改之后更新缓存
						this.props.setUrlParam({ status: 'browse', id: pk_estirecbill });
					}

					if (this.Info.isModelSave) {
						this.Info.isModelSave = false;
						this.props.cardTable.closeModel(this.tableId);
					}
					this.toggleShow();
				}
			});
		};

		this.props.validateToSave(datas.cardData, callback, { table1: 'cardTable' }, 'card');
	};

	pubajax = (url) => {
		ajax({
			url: url,
			data: {
				pk_bill: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
				pageId: this.getPagecode(),
				billType: this.billType
			},
			success: (res) => {
				if (res.data) {
					toast({ color: 'success', content: this.state.json['estireceivable-000008'] }); /* 国际化处理： 操作成功*/
					updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.updateDataByRowId(this.tableId, res.data.body[this.tableId]);
					}
					this.toggleShow();
				}
			}
		});
	};

	//获取列表肩部信息
	getTableHead = (buttons) => {
		return (
			<span>
				{this.props.button.createButtonApp({
					area: 'card_body',
					buttonLimit: 3,
					onButtonClick: buttonClick.bind(this),
					popContainer: document.querySelector('.header-button-area')
				})}
			</span>
		);
	};

	//打印
	onPrint = () => {
		this.printData.oids = [ this.props.getUrlParam('id') ];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/arap/arappub/print.do', //后台服务url
			this.printData,
			false
		);
	};

	//正式打印
	officalPrintOutput = () => {
		this.printData.oids = [ this.props.getUrlParam('id') ];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		ajax({
			url: '/nccloud/arap/arappub/officialPrint.do',
			data: this.printData,
			async: false,
			success: (res) => {
				if (res.success) {
					print(
						'html', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
						'/nccloud/arap/arappub/print.do', //后台服务url
						this.printData,
						false
					);
				}
			}
		});
	};
	//取消正式打印
	cancelPrintOutput = () => {
		this.printData.oids = [ this.props.getUrlParam('id') ];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		ajax({
			url: '/nccloud/arap/arappub/cancelPrint.do',
			data: this.printData,
			success: (res) => {
				if (res.success) {
					//取消正式打印提示框
					toast({ color: 'success', content: res.data });
				}
			}
		});
	};
	cancel = () => {
		//取消按钮
		if (this.props.getUrlParam('status') === 'edit') {
			this.props.setUrlParam({ status: 'browse' });
			let id = this.props.getUrlParam('id');
			let cardData = getCacheById(id, dataSource);
			if (cardData) {
				this.props.beforeUpdatePage(); //打开开关
				this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
				this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
				this.props.cardTable.setStatus(this.tableId, 'browse');
				this.props.form.setFormStatus(this.formId, 'browse');
				this.props.updatePage(this.formId, this.tableId); //关闭开关
				this.toggleShow();
			} else {
				this.initShow();
			}
		}
	};
	//打印输出
	printOutput = () => {
		this.outputData.oids = [ this.props.getUrlParam('id') ];
		this.outputData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		this.refs.printOutput.open();
	};
	//输出成功回调函数
	onSubmit() {}
	//返回列表
	backList = () => {
		this.props.pushTo('/list', {
			pagecode: '200605ERB_LIST'
		});
	};

	/**
	 * 刷新
	 * @param refresh
	*/
	refresh = () => {
		let data = { pk_bill: this.props.getUrlParam('id'), pageId: this.pageId };

		ajax({
			url: '/nccloud/arap/estirecbill/cardquery.do',
			data: data,
			success: (res) => {
				if (res.data) {
					updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
					}
				} else {
					this.props.form.setAllFormValue({ [this.formId]: { rows: [ { values: {} } ] } });
					this.props.cardTable.setTableData(this.tableId, { rows: [ { values: {} } ] });
				}
				this.toggleShow();
			}
		});
	};

	//整单保存事件
	modelSaveClick = () => {
		this.Info.isModelSave = true;
		this.saveBill();
	};

	render() {
		let { cardTable, form, button, modal, cardPagination } = this.props;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		const { createCardPagination } = cardPagination;
		let { showUploader, target } = this.state;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createBillHeadInfo({
								title:this.state.json['estireceivable-000010'],// 国际化处理： 未确认应收单管理
								backBtnClick: () => {
									this.backList();
								}
							})}
						</div>
						{/* 国际化处理： 未确认应收单管理*/}
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'card_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
							<div className="header-cardPagination-area" style={{ float: 'right' }}>
								{createCardPagination({
									handlePageInfoChange: pageInfoClick.bind(this),
									dataSource: dataSource
								})}
							</div>
						</div>
					</div>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: formBeforeEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(this.tableId, {
							tableHead: this.getTableHead.bind(this, buttons),
							modelSave: this.modelSaveClick.bind(this),
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: bodyBeforeEvent.bind(this),
							onSelected: this.onSelected.bind(this),
							onSelectedAll: this.onSelected.bind(this),
							showCheck: true,
							showIndex: true,
							hideAdd: true,
							hideDel: true
						})}
					</div>
				</div>
				{/* {单据追溯} */}
				<BillTrack
					show={this.state.showBillTrack}
					close={() => {
						this.setState({ showBillTrack: false });
					}}
					pk={this.props.getUrlParam('id')} //单据id
					type={
						this.props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? (
							this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
						) : null
					} //单据类型
				/>
				{showUploader && (
					<NCUploader
						billId={
							this.props.form.getFormItemsValue(this.formId, 'pk_estirecbill') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_estirecbill').value
							) : null
						}
						billNo={
							this.props.form.getFormItemsValue(this.formId, 'billno') ? (
								this.props.form.getFormItemsValue(this.formId, 'billno').value
							) : null
						}
						target={target}
						placement={'bottom'}
						beforeUpload={this.beforeUpload}
						onHide={this.onHideUploader}
					/>
				)}

				{/* {打印输出} */}
				<PrintOutput
					ref="printOutput"
					url="/nccloud/arap/arappub/outPut.do"
					data={this.outputData}
					callback={this.onSubmit}
				/>
			</div>
		);
	}
}

Card = createPage({
	//initTemplate: initTemplate,
	mutiLangCode: '2052'
})(Card);

export default Card;
