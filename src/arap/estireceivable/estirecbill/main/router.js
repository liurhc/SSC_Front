import {asyncComponent} from 'nc-lightapp-front';
import List from '../list';


const card = asyncComponent(() => import(/* webpackChunkName: "arap/estireceivable/estirecbill/card/card" */ /* webpackMode: "eager" */'../card'));

const routes = [
  {
		path: '/',
		component: List,
		exact: true
	},
  {
    path: '/list',
    component: List,
    exact: true,
  },
  {
    path: '/card',
    component: card,
  }
];

export default routes;
