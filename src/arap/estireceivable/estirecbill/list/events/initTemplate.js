import tableButtonClick from './tableButtonClick.js';
import { getLinkconferQuery } from '../../../../public/components/pubUtils/billPubUtil.js';
import { buttonVisible, getButtonsKey, getInnerButtonkey } from '../../../../public/components/pubUtils/buttonvisible';
import { modifierSearchMetas } from '../../../../public/components/pubUtils/arapListSearchRefFilter';
import { tableId, searchId, billType, searchKey, dataSource } from '../constants';
import {OperationColumn} from '../../../../public/components/pubUtils/arapConstant';
import { cardCache } from 'nc-lightapp-front';
let { setDefData } = cardCache;
import setDefOrgBilldateSrchArea from '../../../../public/components/defOrgBilldateSrchArea.js';

export default function (props, callback) {
	const that = this;
	let pagecode = that.getPagecode();
	let appcode = props.getSearchParam('c') ? props.getSearchParam('c') : props.getUrlParam('c');

	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode,//注册按钮的id
			reqDataQuerypage: {
				rqUrl: '/arap/arappub/querypage.do',
                rqJson: `{\n  \"pagecode\": \"${pagecode}\",\n  \"appcode\": \"${appcode}\"\n}`,
                rqCode: 'template'
			}
		},
		function (data) {
			if (data) {
				if(!data.template[tableId]){
					return;
				}
				if (data.template) {
					//高级查询设置财务组织默认值
					setDefOrgBilldateSrchArea(props, searchId, data);
					let meta = data.template;
					let innerButton = getInnerButtonkey(data.button);
					meta = modifierMeta(props, meta, innerButton, that);
					modifierSearchMetas(searchId, props, meta, billType, null, that);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					getButtonsKey(button, that.Info.allButtonsKey);//获取所有按钮
					props.button.setButtons(button);
				}
				if (callback) {
					callback()
				}
			}
		}
	)
}

function modifierMeta(props, meta, innerButton, that) {

	meta[tableId].items = meta[tableId].items.map((item, key) => {

		if (item.attrcode == 'billno') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							//表页跳转的时候，获取所有查询区条件，并放入缓存
							let searchVal = props.search.getAllSearchData(searchId);
							if (searchVal) {
								setDefData(searchKey, dataSource, searchVal);
							}
							props.pushTo('/card', {
								status: 'browse',
								id: record.pk_estirecbill.value,
								pagecode: record.pk_tradetype.value
							});
						}}
					>
						{record && record.billno && record.billno.value}
					</a>
				);
			};
		}
		return item;
	});



	let url = window.parent.location.href;
	let linkData = getLinkconferQuery(url);
	if (linkData.status & linkData.src) {
		return meta;

	} else {
		let material_event = {
			label: that.state.json['estireceivable-000004'],/* 国际化处理： 操作*/
			itemtype: 'customer',
			attrcode: 'opr',
			width: OperationColumn,
			visible: true,
			fixed: 'right',
			render: (text, record, index) => {
				let buttonAry = innerButton ? innerButton : [];
				let trueBtn = [];
				for (let i = 0; i < buttonAry.length; i++) {
					let flag = buttonVisible('browse', record, buttonAry[i]);
					if (flag) {
						trueBtn.push(buttonAry[i]);
					}
				}
				return props.button.createOprationButton(trueBtn, {
					area: "list_inner",
					buttonLimit: 3,
					onButtonClick: (props, key) => tableButtonClick(that, props, key, text, record, index)
				});
			}
		};
		meta[tableId].items.push(material_event);
		return meta;
	}

	return meta;
}
