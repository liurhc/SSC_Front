import { ajax, base, toast, cacheTools, print, promptBox, cardCache } from 'nc-lightapp-front';
import { headButton } from '../../../../public/components/pubUtils/buttonName';
import arapLinkReport from '../../../../public/components/arapBillLinkReport.js';
import { billType, dataSource, searchId } from '../constants';
import linkvouchar from '../../../../public/components/linkvouchar.js';
import madeBill from '../../../../public/components/madeBill.js';
let { setDefData, getDefData } = cardCache;

export default function buttonClick(props, id) {
	switch (id) {
		case 'Delete':
			const selectedData = this.props.table.getCheckedRows(this.tableId);
			if (selectedData.length == 0) {
				return;
			}
			let pauseObj = [];
			let deleteIndexs = [];

			selectedData.forEach((val) => {
				let data = {
					pk_bill: val.data.values.pk_estirecbill.value,
					ts: val.data.values.ts.value,
					billType: this.billType
				};
				pauseObj.push(data);

				deleteIndexs.push(val.index + 1);
			});
			ajax({
				url: '/nccloud/arap/estirecbill/delete.do',
				data: pauseObj,
				success: (res) => {
					if (res.success) {
						toast({ color: 'success', content: this.state.json['estireceivable-000011'] }); /* 国际化处理： 删除成功*/

						props.table.deleteTableRowsByIndex(this.tableId, deleteIndexs);
					}
				}
			});
			break;
		case headButton.MadeBill:
			madeBill(
				this,
				props,
				this.props.table.getCheckedRows(this.tableId),
				'pk_estirecbill',
				props.getSearchParam('c'),
				true
			);
			break;
		case 'LinkConfer':
			let checkData = props.table.getCheckedRows(this.tableId);
			if (checkData.length == 0) return;
			let dataArray = [];
			let pks = [];
			checkData.forEach((val) => {
				let object = {
					pk_estirecbill: ''
				};
				object.pk_estirecbill = val.data.values.pk_estirecbill.value;
				dataArray.push(object);
				pks.push(object.pk_estirecbill);
			});

			ajax({
				url: '/nccloud/arap/arappub/linkconfer.do',
				async: false,
				data: {
					pk_bill: pks[0],
					pageId: props.getSearchParam('p'),
					billType: this.billType
				},
				success: (res) => {
					if (res.success) {
						let pk = res.data;
						cacheTools.set('checkedDataArap', pk);
						//window.parent.openNew({ code: '20080PBM', name: '应付单管理', pk_appregister: '0001AA1000000002KJTZ' }, null, '&status=browse&src=arap')
					}
				}
			});
			break;
		case 'TaxChecked':
			this.handleTaxInformation();
			break;
		case headButton.BillLinkQuery: //联查
			let billInfo = getFirstCheckedData(this.props, this.tableId);
			this.Info.pk_bill = billInfo.data.values.pk_estirecbill.value;
			this.Info.pk_tradetype = billInfo.data.values.pk_tradetype.value;
			this.setState({
				showBillTrack: true
			});
			break;
		case 'Print': //打印
			let selectData = this.props.table.getCheckedRows(this.tableId);
			if (selectData.length == 0) {
				toast({ color: 'warning', content: this.state.json['estireceivable-000012'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			let pk_bills = [];
			selectData.forEach((val) => {
				pk_bills.push(val.data.values.pk_estirecbill.value);
			});
			this.printData.oids = pk_bills;
			this.printData.nodekey = 'card';
			this.onPrint();
			break;
		case headButton.PrintList: //打印清单
			selectData = this.props.table.getCheckedRows(this.tableId);
			pk_bills = [];
			selectData.forEach((val) => {
				pk_bills.push(val.data.values.pk_estirecbill.value);
			});
			if (pk_bills.length == 0) {
				toast({ color: 'warning', content: this.state.json['estireceivable-000012'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			this.printData.oids = pk_bills;
			this.printData.nodekey = 'list';
			this.onPrint();
			break;
		case headButton.Output: //打印输出
			selectData = this.props.table.getCheckedRows(this.tableId);
			if (selectData.length == 0) {
				toast({ color: 'warning', content: this.state.json['estireceivable-000012'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			pk_bills = [];
			selectData.forEach((val) => {
				pk_bills.push(val.data.values.pk_estirecbill.value);
			});
			this.outputData.oids = pk_bills;
			this.outputData.nodekey = 'card';
			this.printOutput();
			break;
		case headButton.OfficalPrint: //正式打印
			selectData = this.props.table.getCheckedRows(this.tableId);
			if (selectData.length == 0) {
				toast({ color: 'warning', content: this.state.json['estireceivable-000012'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			pk_bills = [];
			selectData.forEach((val) => {
				pk_bills.push(val.data.values.pk_estirecbill.value);
			});
			this.printData.oids = pk_bills;
			this.printData.nodekey = 'card';
			this.officalPrintOutput();
			break;
		case headButton.CancelPrint: //取消正式打印
			selectData = this.props.table.getCheckedRows(this.tableId);
			if (selectData.length == 0) {
				toast({ color: 'warning', content: this.state.json['estireceivable-000012'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			pk_bills = [];
			selectData.forEach((val) => {
				pk_bills.push(val.data.values.pk_estirecbill.value);
			});
			this.printData.oids = pk_bills;
			this.printData.nodekey = 'card';
			this.cancelPrintOutput();
			break;
		case headButton.Refresh: //刷新
			let data = getDefData(searchId, dataSource);
			if (data) {
				ajax({
					url: '/nccloud/arap/estirecbill/query.do',
					data: data,
					success: (res) => {
						let { success, data } = res;
						if (success) {
							if (data) {
								toast({
									color: 'success',
									title: this.state.json['estireceivable-000022']
								}); /* 国际化处理： 刷新成功*/
								this.props.table.setAllTableData(this.tableId, data[this.tableId]);
							} else {
								toast({
									color: 'success',
									content: this.state.json['estireceivable-000013']
								}); /* 国际化处理： 未查询到数据*/
								this.props.table.setAllTableData(this.tableId, { rows: [] });
							}
							setDefData(this.tableId, dataSource, data); //放入缓存
							this.onSelected();
						}
					}
				});
			} else {
				toast({ color: 'success', title: this.state.json['estireceivable-000022'] }); /* 国际化处理： 刷新成功*/
				this.props.table.setAllTableData(this.tableId, { rows: [] });
			}

			break;
		case headButton.LinkBal: //联查余额表
			arapLinkReport(
				this.props,
				getFirstCheckedData(this.props, this.tableId).data.values.pk_estirecbill.value,
				billType,
				getFirstCheckedData(this.props, this.tableId).data.values.objtype.value
			);
			break;
		case headButton.LinkVouchar: //联查凭证
			let voucharInfo = getFirstCheckedData(this.props, this.tableId);
			linkvouchar(
				this,
				props,
				voucharInfo,
				voucharInfo.data.values.pk_estirecbill.value,
				props.getSearchParam('c')
			);
			break;

		case headButton.UnApprove: //取消审批
			let unApproveInfo = props.table.getCheckedRows(this.tableId);
			if (unApproveInfo.length == 0) {
				toast({ color: 'warning', content: this.state.json['estireceivable-000012'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			let unApprovepks = [];
			let unApproveIndexInfo = new Map();
			unApproveInfo.forEach((val) => {
				let pk = val.data.values.pk_estirecbill.value;
				let index = val.index;
				unApprovepks.push(pk);
				unApproveIndexInfo.set(pk, index);
			});
			ajax({
				url: '/nccloud/arap/arappub/listunapprove.do',
				data: {
					pk_bills: unApprovepks,
					billType: billType,
					pageId: props.getSearchParam('p'),
					indexInfo: unApproveIndexInfo
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						//更新当前行数据
						if (data.grid) {
							let grid = data.grid;
							let updateValue = [];
							for (let key in grid) {
								updateValue.push({ index: key, data: { values: grid[key].values } });
							}
							props.table.updateDataByIndexs(this.tableId, updateValue);
						}

						if (data.message) {
							toast({
								duration: 'infinity',
								color: data.PopupWindowStyle,
								content: data.message
							});
						}
					}
				}
			});
			break;
		case headButton.Approve: //审批
			let ApproveInfo = props.table.getCheckedRows(this.tableId);
			if (ApproveInfo.length == 0) {
				toast({ color: 'warning', content: this.state.json['estireceivable-000012'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			let Approvepks = [];
			let ApproveIndexInfo = new Map();
			ApproveInfo.forEach((val) => {
				let pk = val.data.values.pk_estirecbill.value;
				let index = val.index;
				Approvepks.push(pk);
				ApproveIndexInfo.set(pk, index);
			});
			ajax({
				url: '/nccloud/arap/arappub/listapprove.do',
				data: {
					pk_bills: Approvepks,
					billType: billType,
					pageId: props.getSearchParam('p'),
					indexInfo: ApproveIndexInfo
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						//更新当前行数据
						if (data.grid) {
							let grid = data.grid;
							let updateValue = [];
							for (let key in grid) {
								updateValue.push({ index: key, data: { values: grid[key].values } });
							}
							props.table.updateDataByIndexs(this.tableId, updateValue);
						}

						if (data.message) {
							toast({
								duration: 'infinity',
								color: data.PopupWindowStyle,
								content: data.message
							});
						}
					}
				}
			});
			break;
		case headButton.AttachManage:
			let AttachInfo = getFirstCheckedData(this.props, this.tableId);
			this.Info.pk_bill = AttachInfo.data.values.pk_estirecbill.value;
			this.Info.billno = AttachInfo.data.values.billno.value;
			this.setState({
				showUploader: true,
				target: null
			});
			break;
		default:
			break;
	}
}

//获取选中数据的第一行,选中多行的时候只取第一行数据
let getFirstCheckedData = function(props, tableId) {
	let checkedData = props.table.getCheckedRows(tableId);
	let checkedObj;
	if (checkedData.length > 0) {
		checkedObj = checkedData[0];
	} else {
		toast({ color: 'warning', content: this.state.json['estireceivable-000012'] }); /* 国际化处理： 请选中一行数据!*/
		return;
	}
	return checkedObj;
};

//获取选中数据的id和billType
let getAllCheckedData = function(props, tableId, billType) {
	let checkedData = props.table.getCheckedRows(tableId);
	let checkedObj = [];
	checkedData.forEach((val) => {
		checkedObj.push({
			pk_bill: val.data.values.pk_estirecbill.value,
			ts: val.data.values.ts.value,
			billType: billType
		});
	});
	return checkedObj;
};
