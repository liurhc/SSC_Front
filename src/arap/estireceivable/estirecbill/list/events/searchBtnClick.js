import {ajax,cacheTools,cardCache,toast} from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
import { searchId,dataSource} from '../constants';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    if(searchVal){
        let pageInfo = props.table.getTablePageInfo(this.tableId);
        let queryInfo = props.search.getQueryInfo(this.searchId);
        queryInfo.pageInfo = pageInfo;
        let data = {
            pageId: props.getSearchParam('p'),
            queryInfo:queryInfo
        }; 
        setDefData(searchId, dataSource, data);//放入缓存
        ajax({
            url: '/nccloud/arap/estirecbill/query.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        toast({ color: 'success', content: this.state.json['estireceivable-000014']+data[this.tableId].allpks.length+this.state.json['estireceivable-000015'] });/* 国际化处理： 查询成功，共,条*/
                        this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                    }else{
                        toast({ color: 'warning', content: this.state.json['estireceivable-000016'] });/* 国际化处理： 未查询出符合条件的数据*/
                        this.props.table.setAllTableData(this.tableId, {rows:[]});
                    }
                    setDefData(this.tableId, dataSource, data);//放入缓存
                    this.onSelected()
                }
            }
        });
    }
    
};
