import { ajax, base, toast } from 'nc-lightapp-front';
import { billType,tableId} from '../constants';
import {innerButton} from '../../../../public/components/pubUtils/buttonName.js';
import madeBill from '../../../../public/components/madeBill.js';
const tableButtonClick = (that,props, key, text, record, index) => {
    let pagecode = props.getSearchParam('p');
    switch (key) {
       
            case innerButton.Edit_inner:
            ajax({
                url: '/nccloud/arap/arappub/edit.do',
                data: {
                    pk_bill: record.pk_estirecbill.value,
                    billType: billType
                },
                success: (res) => {
                    if (res.success) {
                        props.pushTo('/card', {
                            status: 'edit',
                            id: record.pk_estirecbill.value,
                            pagecode:record.pk_tradetype.value,
                        })
                    }
                }
            });
            break;
            case innerButton.Approve_inner://审核
            ajax({
                url: '/nccloud/arap/arappub/approve.do',
                data:{
                    pk_bill:record.pk_estirecbill.value,
                    ts:record.ts.value,
                    pageId: props.getSearchParam("p"),
                    billType : billType,
                    type :1 
                },
                success: (res) => { 
                    if (res.success) {
                        toast({color: 'success', content: that.state.json['estireceivable-000008']});/* 国际化处理： 操作成功*/
                        //更新当前行数据
                        props.table.updateDataByIndexs(tableId,
                            [{ index: index, data: { values: res.data[tableId].rows[0].values } }]);
                    }
                }
            });
            break;
        case innerButton.UnApprove_inner://取消审核
            ajax({
                url: '/nccloud/arap/arappub/unapprove.do',
                data:{
                    pk_bill:record.pk_estirecbill.value,
                    ts:record.ts.value,
                    pageId: props.getSearchParam("p"),
                    billType : billType,
                    type :1 
                },
                success: (res) => { 
                    if (res.success) {
                        toast({color: 'success', content: that.state.json['estireceivable-000008']});/* 国际化处理： 操作成功*/
                        //更新当前行数据
                        props.table.updateDataByIndexs(tableId,
                            [{ index: index, data: { values: res.data[tableId].rows[0].values } }]);
                    }
                }
            });
            break;
        case innerButton.MadeBill_inner://制单
            let madeData = [{
                pk_bill: record.pk_estirecbill.value,
                billType:billType,
                tradeType: record.pk_tradetype.value,
            }]
            madeBill(that,props,madeData,'',props.getSearchParam('c'));
            break;
       
        default:
            break;
    }
};
export default tableButtonClick;
