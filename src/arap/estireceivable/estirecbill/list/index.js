//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, cacheTools, toast, high, print, cardCache, getMultiLang,createPageIcon } from 'nc-lightapp-front';
let { NCTabsControl, NCButton } = base;
import { buttonClick, initTemplate, searchBtnClick, pageInfoClick, tableModelConfirm, doubleClick } from './events';
import { getBodyAmountValue, getLinkconferQuery } from '../../../public/components/pubUtils/billPubUtil.js';
import { buttonVisible, getButtonsKey, onListButtonControl } from '../../../public/components/pubUtils/buttonvisible';
import { tableId, searchId, nodekey, billType, pkname, dataSource, searchKey } from './constants';
import linkSourceList from '../../../public/components/linkSourceList.js';
import afterEvent from '../../../public/components/searchAreaAfterEvent'; //查询区编辑后事件
let { setDefData, getDefData } = cardCache;
const { BillTrack, PrintOutput, NCUploader } = high;
class List extends Component {
	constructor(props) {
		super(props);
		this.moduleId = '2052';
		this.searchId = searchId;
		this.tableId = tableId;
		this.pageid = props.getSearchParam('p');
		this.billType = billType;
		let { form, button, table, insertTable, search } = this.props;
		let { setSearchValue, setSearchValByField, getAllSearchData } = search;
		this.setSearchValByField = setSearchValByField; //设置查询区某个字段值
		this.getAllSearchData = getAllSearchData; //获取查询区所有字段数据
		this.state = {
			json: {},
			isTaxInformationModelShow: false,
			showBillTrack: false, //单据追溯模态框的显示状态
			showUploader: false, //附件管理
			target: null //附件管理
		};
		this.printData = {
			billtype: billType, //单据类型
			appcode: props.getSearchParam('c'), //功能节点编码，即模板编码
			nodekey: nodekey, //模板节点标识
			oids: null, // 功能节点的数据主键
			userjson: billType //单据类型,billtype不是必需字段，后台没有设置接收字段，以userjson代替
		};
		this.outputData = {
			billtype: billType, //单据类型
			appcode: props.getSearchParam('c'), //功能节点编码，即模板编码
			funcode: props.getSearchParam('c'),
			nodekey: nodekey, //模板节点标识
			oids: null, // 功能节点的数据主键
			userjson: billType, //单据类型,billtype不是必需字段，后台没有设置接收字段，以userjson代
			outputType: 'output'
		};
		this.Info = {
			allButtonsKey: [],
			pk_tradetype: null, //当前选中第一行行的交易类型code
			pk_bill: null, //当前选中第一行的主键pk
			billno: null //当前选中第一行的单据号
		};
	}

	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				initTemplate.call(this, this.props, this.initShow);
			});
		};
		getMultiLang({
			moduleId: [ 'estireceivable', 'public' ],
			domainName: 'arap',
			currentLocale: 'simpchn',
			callback
		});
	}

	//在第一次渲染后调用
	componentDidMount() {}

	getPagecode = () => {
		let pagecode = '200605ERB_LIST';
		return pagecode;
	};

	initShow = () => {
		if (!this.props.table.hasCacheData(dataSource)) {
			this.onSelected(); //缓存不存在，就控制按钮
			//联查
			if ('browse' == this.props.getUrlParam('status')) {
				if ('fip' == this.props.getUrlParam('scene')) {
					linkSourceList(this.props, this.tableId);
				}
			}
		}
	};

	componentWillReceiveProps(nextProps) {}

	//列表控制按钮
	onSelected = () => {
		onListButtonControl(this);
	};

	//页签筛选
	navChangeFun = (status, className, e) => {
		let serval;
		switch (status) {
			case '0':
				serval = [
					{
						field: 'dbilldate',
						value: {
							firstvalue: '2017-04-10',
							secondvalue: '2018-04-20'
						},
						oprtype: 'between'
					}
				];
				this.getData(serval);
				break;
			case '1':
				serval = [
					{
						field: 'dbilldate',
						value: {
							firstvalue: '2017-04-10',
							secondvalue: '2018-04-20'
						},
						oprtype: 'between'
					}
				];
				this.getData(serval);
				break;
			case '2':
				serval = [
					{
						field: 'dbilldate',
						value: {
							firstvalue: '2017-04-10',
							secondvalue: '2018-04-20'
						},
						oprtype: 'between'
					}
				];
				this.getData(serval);
				break;
			default:
				break;
		}
	};

	/* 添加高级查询区中的页签 */
	addAdvTabs = () => {
		return [
			{
				name: this.state.json['estireceivable-000017'], //页签名称/* 国际化处理： 页签2*/
				content: (
					<div>
						{this.state.json['estireceivable-000020']}2{this.state.json['estireceivable-000021']}
					</div>
				) //页签内容/* 国际化处理： 页签,内容*/
			},
			{
				name: this.state.json['estireceivable-000018'] /* 国际化处理： 页签3*/,
				content: (
					<div>
						{this.state.json['estireceivable-000020']}3{this.state.json['estireceivable-000021']}
					</div>
				) /* 国际化处理： 页签,内容*/
			}
		];
	};

	/* 替换高级查询body区域 */
	replaceAdvBody = () => {
		return <div>3333</div>;
	};

	//补差按钮
	handleTaxInformation = () => {
		let checkedRowData = this.props.editTable.getCheckedRows(this.tableId);
		let { isTaxInformationModelShow } = this.state;
		if (checkedRowData.length == 1) {
			pk_estirecbill = checkedRowData[0].data.values.pk_estirecbill.value;
			this.setState({
				isTaxInformationModelShow: !this.state.isTaxInformationModelShow
			});
		} else {
			toast({ content: this.state.json['estireceivable-000019'] }); /* 国际化处理： 请选择一行*/
		}
	};

	//打印
	onPrint = () => {
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/arap/arappub/print.do', //后台服务url
			this.printData,
			false
		);
	};

	//打印输出
	printOutput = () => {
		this.refs.printOutput.open();
	};
	//输出成功回调函数
	onSubmit() {}
	//正式打印
	officalPrintOutput = () => {
		ajax({
			url: '/nccloud/arap/arappub/officialPrint.do',
			data: this.printData,
			async: false,
			success: (res) => {
				if (res.success) {
					print(
						'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
						'/nccloud/arap/arappub/print.do', //后台服务url
						this.printData,
						false
					);
				}
			}
		});
	};
	//取消正式打印
	cancelPrintOutput = () => {
		ajax({
			url: '/nccloud/arap/arappub/cancelPrint.do',
			data: this.printData,
			success: (res) => {
				if (res.success) {
					//取消正式打印提示框
					toast({ color: 'success', content: res.data });
				}
			}
		});
	};
	beforeUpload(billId, fullPath, file, fileList) {
		// 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表
		const isJPG = true;
		if (!isJPG) {
			alert(this.state.json['estireceivable-000005']); /* 国际化处理： 只支持png格式图片*/
		}
		const isLt2M = file.size / 1024 / 1024 < 10;
		if (!isLt2M) {
			alert(this.state.json['estireceivable-000006']); /* 国际化处理： 上传大小小于10M*/
		}
		return isJPG && isLt2M;
		// 备注： return false 不执行上传  return true 执行上传
	}
	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		});
	};

	// 查询区渲染完成回调函数
	renderCompleteEvent = () => {
		let cachesearch = getDefData(searchKey, dataSource);
		if (cachesearch && cachesearch.conditions) {
			// this.props.search.setSearchValue(this.searchId, cachesearch);
			for (let item of cachesearch.conditions) {
				if (item.field == 'billdate') {
					// 时间类型特殊处理
					let time = [];
					time.push(item.value.firstvalue);
					time.push(item.value.secondvalue);
					this.props.search.setSearchValByField(this.searchId, item.field, {
						display: item.display,
						value: time
					});
				} else {
					this.props.search.setSearchValByField(this.searchId, item.field, {
						display: item.display,
						value: item.value.firstvalue
					});
				}
			}
		}
	};

	render() {
		let { form, table, button, insertTable, search } = this.props;
		let buttons = this.props.button.getButtons();
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		let { showUploader, target } = this.state;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon ? createPageIcon() : null}
						<h2 className="title-search-detail">{this.state.json['estireceivable-000010']}</h2>
						{/* 国际化处理： 未确认应收单管理*/}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						showAdvBtn: true, //  显示高级按钮
						onAfterEvent: afterEvent.bind(this), //编辑后事件
						renderCompleteEvent: this.renderCompleteEvent // 查询区渲染完成回调函数
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						dataSource: dataSource,
						pkname: pkname,
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						showCheck: true,
						showIndex: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: this.onSelected.bind(this),
						onSelectedAll: this.onSelected.bind(this),
						componentInitFinished: () => {
							this.onSelected();
						}
					})}
				</div>
				{/* {单据追溯} */}
				<BillTrack
					show={this.state.showBillTrack}
					close={() => {
						this.setState({ showBillTrack: false });
					}}
					pk={this.Info.pk_bill} //单据id
					type={this.Info.pk_tradetype} //单据类型
				/>
				{showUploader && (
					<NCUploader
						billId={this.Info.pk_bill}
						billNo={this.Info.billno}
						target={target}
						placement={'bottom'}
						beforeUpload={this.beforeUpload}
						onHide={this.onHideUploader}
					/>
				)}
				{/* {打印输出} */}
				<PrintOutput
					ref="printOutput"
					url="/nccloud/arap/arappub/outPut.do"
					data={this.outputData}
					callback={this.onSubmit}
				/>
			</div>
		);
	}
}

List = createPage({
	mutiLangCode: '2052'
})(List);

export default List;
