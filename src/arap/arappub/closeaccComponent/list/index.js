import { Component } from 'react';
import { createPage, ajax, base, high, toast, getMultiLang } from 'nc-lightapp-front';
const { NCTable } = base;

let columns = [];
class CloseaccComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: [], //保存表格数据
			reportTitle: null,
			json: {}
		};
	}

	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				/**
 				 * 关账组件
 				 */
				columns = [
					{
						title: this.state.json['arappub-000005'] /* 国际化处理： 序号*/,
						dataIndex: 'serial',
						key: 'serial',
						width: '30%'
					},
					{
						title: this.state.json['arappub-000006'] /* 国际化处理： 月末检查不合格单据*/,
						dataIndex: 'notQualified',
						key: 'notQualified',
						width: '40%'
					},
					{
						title: this.state.json['arappub-000007'] /* 国际化处理： 检查结果*/,
						dataIndex: 'result',
						key: 'result'
					}
				];

				if (!this.props.pk_checkitem) {
					return;
				} else if (!this.props.data.exdata.pk_org) {
					toast({ content: this.state.json['arappub-000008'], color: 'warning' }); /* 国际化处理： 请先选择财务组织*/
					return false;
				} else if (!this.props.period) {
					toast({ content: this.state.json['arappub-000009'], color: 'warning' }); /* 国际化处理： 请先选择会计期间*/
					return false;
				} else {
					this.getTableData(this.props.data.exdata.pk_org, this.props.pk_checkitem, this.props.period);
				}
			});
		};
		getMultiLang({ moduleId: 'arappub', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	componentWillReceiveProps(nextProps) {}

	//获取表格数据
	getTableData = (pk_org, pk_checkitem, period) => {
		ajax({
			url: '/nccloud/arap/arappub/checkcloseacc.do',
			data: {
				pk_org: pk_org,
				pk_checkitem: pk_checkitem,
				period: period
			},
			success: (res) => {
				if (res.data.dataList.length > 0) {
					this.setState({
						tableData: res.data.dataList,
						reportTitle: res.data.reportTitle
					});
				} else {
					this.setState({
						tableData: [],
						reportTitle: res.data.reportTitle
					});
				}
			}
		});
	};

	print(cfg = {}) {
		//打印
	}

	printPreVIew(cfg = {}) {
		//预览
	}

	output(cfg = {}) {
		//输出
	}

	render() {
		let { tableData, reportTitle } = this.state;
		return (
			<div>
				<h3>{reportTitle}</h3>
				<NCTable columns={columns} bordered isDrag data={tableData} style={{ marginRight: '20px' }} />
			</div>
		);
	}
}

export default function(props = {}) {
	var conf = {};

	return <CloseaccComponent {...Object.assign(conf, props)} />;
}
