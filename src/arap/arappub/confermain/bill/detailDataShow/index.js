import { base } from 'nc-lightapp-front';
const { NCFormControl: FormControl, NCRow: Row, NCCol: Col, NCForm } = base;
const NCFormItem = NCForm.NCFormItem;

export default (props) => {
	let { DetailDatas } = props;
	let DetailDataArr = [
		'scomment',
		'receivers',
		'busitype',
		'sourcesystem',
		'sourceorg',
		'sourcebillname',
		'targetsystem',
		'targetorg',
		'targetbillname'
	];
	for (var i = 0; i < DetailDataArr.length; i++) {
		if (!(DetailDataArr[i] in DetailDatas)) {
			DetailDatas[DetailDataArr[i]] = '';
		}
	}
	if (DetailDatas.sourcesystemVal && DetailDatas.targetsystemVal) {
		if (
			DetailDatas.sourcesystemVal.indexOf('D0') != -1 ||
			DetailDatas.sourcesystemVal.indexOf('D2') != -1 ||
			DetailDatas.sourcesystemVal.indexOf('F0-') != -1 ||
			DetailDatas.sourcesystemVal.indexOf('F2-') != -1
		) {
			DetailDatas.sourcesystem = props.json['arappub-000010'];/* 国际化处理： 应收*/
			DetailDatas.targetsystem = props.json['arappub-000011'];/* 国际化处理： 应付*/
		} else {
			DetailDatas.sourcesystem = props.json['arappub-000011'];/* 国际化处理： 应付*/
			DetailDatas.targetsystem = props.json['arappub-000010'];/* 国际化处理： 应收*/
		}
	}
	return (
		<div className="content-style">
			<div className="bill-instruction">
				<div />
				<div>
					<NCFormItem showMast={false} labelName={props.json['arappub-000021']} isRequire={true} inline={true}>{/* 国际化处理： 说明：*/}
						<span>{DetailDatas.scomment}</span>
					</NCFormItem>
					<NCFormItem showMast={false} labelName={props.json['arappub-000022']} isRequire={true} inline={true}>{/* 国际化处理： 协同消息接收人：*/}
						<span>{DetailDatas.receivers}</span>
					</NCFormItem>
					<NCFormItem showMast={false} labelName={props.json['arappub-000023']} isRequire={true} inline={true}>{/* 国际化处理： 流程名称：*/}
						<span>{DetailDatas.busitype}</span>
					</NCFormItem>
				</div>
			</div>
			<div className="bill-send-cont">
				<div className="txt">{props.json['arappub-000019']}</div>{/* 国际化处理： 发送方*/}
				<div>
					<NCFormItem showMast={false} labelName={props.json['arappub-000024']} isRequire={true} inline={true}>{/* 国际化处理： 来源系统：*/}
						<span>{DetailDatas.sourcesystem}</span>
					</NCFormItem>
					<NCFormItem showMast={false} labelName={props.json['arappub-000025']} isRequire={true} inline={true}>{/* 国际化处理： 财务组织：*/}
						<span>{DetailDatas.sourceorg}</span>
					</NCFormItem>
					<NCFormItem showMast={false} labelName={props.json['arappub-000026']} isRequire={true} inline={true}>{/* 国际化处理： 交易类型：*/}
						<span>{DetailDatas.sourcebillname}</span>
					</NCFormItem>
				</div>
			</div>
			<div className="bill-receive-cont">
				<div className="txt">{props.json['arappub-000020']}</div>{/* 国际化处理： 接收方*/}
				<div>
					<NCFormItem showMast={false} labelName={props.json['arappub-000027']} isRequire={true} inline={true}>{/* 国际化处理： 目的系统：*/}
						<span>{DetailDatas.targetsystem}</span>
					</NCFormItem>
					<NCFormItem showMast={false} labelName={props.json['arappub-000025']} isRequire={true} inline={true}>{/* 国际化处理： 财务组织：*/}
						<span>{DetailDatas.targetorg}</span>
					</NCFormItem>
					<NCFormItem showMast={false} labelName={props.json['arappub-000026']} isRequire={true} inline={true}>{/* 国际化处理： 交易类型：*/}
						<span>{DetailDatas.targetbillname}</span>
					</NCFormItem>
				</div>
			</div>
		</div>
	);
};
