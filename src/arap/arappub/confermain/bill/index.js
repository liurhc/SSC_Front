import React, { Component } from 'react';
import { ajax, base, createPage, toast, getMultiLang, createPageIcon } from 'nc-lightapp-front';
import { initTemplate, buttonClick } from './events';
import DetailDataShow from './detailDataShow'; //点击显示详细内容
import AddContions from './addContions'; //新增页面
import ModifyContions from './modifyContions'; //修改页面
import promptbox from '../../../public/components/promptbox';
import './index.less';
let pagecode;
const { NCPopconfirm, NCBackBtn, NCTooltip, NCTable } = base;
let appcode;
let title;
let jishiData = {
	pageId: ''
};
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};

class Bill extends Component {
	constructor(props) {
		super(props);
		appcode = props.getSearchParam('c'); //获取小应用编码
		pagecode = props.getSearchParam('p'); //获取页面编码
		this.tableid = appcode + '_list';
		jishiData.pageId = appcode + '_LIST';
		this.billDataClone = {};
		this.allBillData = []; //初始所有数据
		this.allBillDataClone = []; //初始所有数据Clone
		this.selectRowTransData = {}; // 选中行需要传递后端数据
		this.selectRowTransDataClone = {}; // 选中行数据clone
		this.clickIndex = ''; //双击行下标
		this.state = {
			json: {},
			mockData: jishiData,
			billData: [], // 单据协同数据
			index: '', //点击下标
			isTable: true, // 表格是否显示
			isClick: false, // 点击表格每一行显示内容
			DetailDatas: {}, //点击行获取行数据
			isAdd: false, // 点击新增按钮
			isSelectRow: '', // 点击行下标
			recordData: {}, //点击修改获取行数据
			isModify: false, // 修改
			isRealModify: 0, // 未修改数据
			isFirstReder: true,
			selectedRow: [],
			isModelShow: false, // 测试弹窗展示与否
			isBtnStyle: true,
			isBack: false, //返回
			mainData: {
				receivers: { display: '', value: '' }
			},
			referData: {
				receiver: { display: '', value: '' }
			},
			addMainData: {}, //新增数据
			columns: []
		};
		this.handleReturn = this.handleReturn.bind(this);
		this.onRowClick = this.onRowClick.bind(this);
		this.handleOnRowClick = this.handleOnRowClick.bind(this);
		this.getAddDatas = this.getAddDatas.bind(this);
	}

	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				this.madeTable();
				this.getInitialData();
				title =
					appcode.substring(0, 4) == '2006'
						? this.state.json['arappub-000030']
						: this.state.json['arappub-000031']; /* 国际化处理： 应收单据协同设置,应付单据协同设置*/
			});
		};
		getMultiLang({ moduleId: 'arappub', domainName: 'arap', callback });
		window.onbeforeunload = () => {
			if (this.state.isModify || this.state.isAdd) return '';
		};
	}

	componentDidMount() {
		this.props.button.setButtonVisible([ 'Edit', 'Delete' ], false);
	}

	madeTable = () => {
		this.setState({
			addMainData: {
				scomment: { display: null, value: null }, //说明
				receivers: [ { refname: null, refpk: null } ], //协同消息接收人
				busitype: { display: null, value: null }, //流程名称
				sourcesystem: { display: this.state.json['arappub-000010'], value: 'F0,F2' }, //来源系统/* 国际化处理： 应收*/
				sourceorg: { display: null, value: null }, //财务组织-发送方
				sourcebillname: { display: null, value: null, code: null }, //交易类型-发送方
				targetsystem: { display: this.state.json['arappub-000011'], value: 'F1,F3' }, //目的系统/* 国际化处理： 应付*/
				targetorg: { display: null, value: null }, //财务组织-接收方
				targetbillname: { display: null, value: null, code: null } //交易类型-接收方
			}, //新增数据
			columns: [
				{
					title: this.state.json['arappub-000028'] /* 国际化处理： 编号*/,
					dataIndex: 'number',
					key: 'number',
					width: '100px',
					fixed: 'left'
				},
				{
					title: this.state.json['arappub-000019'] /* 国际化处理： 发送方*/,
					width: '240px',
					children: [
						{
							title: this.state.json['arappub-000016'] /* 国际化处理： 财务组织*/,
							dataIndex: 'sourceorg',
							key: 'sourceorg',
							width: '120px'
						},
						{
							title: this.state.json['arappub-000017'] /* 国际化处理： 交易类型*/,
							dataIndex: 'sourcebillname',
							key: 'sourcebillname',
							width: '120px'
						}
					]
				},
				{
					title: this.state.json['arappub-000020'] /* 国际化处理： 接收方*/,
					width: '240px',
					children: [
						{
							title: this.state.json['arappub-000016'] /* 国际化处理： 财务组织*/,
							dataIndex: 'targetorg',
							key: 'targetorg',
							width: '120px'
						},
						{
							title: this.state.json['arappub-000017'] /* 国际化处理： 交易类型*/,
							dataIndex: 'targetbillname',
							key: 'targetbillname',
							width: '120px'
						}
					]
				},
				{
					title: this.state.json['arappub-000012'] /* 国际化处理： 说明*/,
					dataIndex: 'scomment',
					key: 'scomment',
					width: '240px',
					render: (text, record, index) => {
						let tip = <div>{text}</div>;
						return (
							<div className="scomment">
								<NCTooltip inverse={true} overlay={tip} trigger="hover" placement="top">
									<span>{text}</span>
								</NCTooltip>
							</div>
						);
					}
				},
				{
					title: this.state.json['arappub-000013'] /* 国际化处理： 协同消息接收人*/,
					dataIndex: 'receivers',
					key: 'receivers',
					width: '240px'
				},
				{
					title: this.state.json['arappub-000014'] /* 国际化处理： 流程名称*/,
					dataIndex: 'busitype',
					key: 'busitype',
					width: '100px'
				},
				{
					title: this.state.json['arappub-000000'] /* 国际化处理： 操作*/,
					dataIndex: 'operation',
					key: 'operation',
					width: '120px',
					fixed: 'right',
					render: (text, record, index) => {
						return (
							<div style={{ position: 'relative' }} title={text}>
								<a
									tooltip={text}
									onClick={() => {
										this.handleModify(index);
									}}
									style={{
										position: 'absolute',
										top: -8,
										left: 4,
										cursor: 'pointer'
									}}
								>
									{this.state.json['arappub-000039']}
									{/* 国际化处理： 修改*/}
								</a>
								{this.state.billData.length > 0 ? (
									<NCPopconfirm
										content={this.state.json['arappub-000029']} /* 国际化处理： 确定要删除吗?*/
										placement="left"
										id="aa"
										onClose={this.handleDelete(index)}
									>
										<a
											tooltip={text}
											style={{
												position: 'absolute',
												top: -8,
												left: 62,
												cursor: 'pointer'
											}}
										>
											{this.state.json['arappub-000040']}
											{/* 国际化处理： 删除*/}
										</a>
									</NCPopconfirm>
								) : null}
							</div>
						);
					}
				}
			]
		});
	};
	componentWillReceiveProps(nextProps) {
		this.setState({
			columns: nextProps.columns
		});
	}

	getInitialData = () => {
		let self = this;
		let { mockData, isTable } = this.state;
		if (isTable) {
			this.props.button.setButtonVisible([ 'Return', 'Save', 'Cancel' ], false);
		}
		var arrData = [],
			brr = [],
			systemBrr = [],
			obj = {};
		let url = '/nccloud/arap/billconfer/query.do';
		ajax({
			url: url,
			data: mockData,
			success: function(response) {
				const { data, success } = response;
				if (success && data) {
					const bill = data[appcode + '_list'].rows;
					if (bill.length > 0) {
						for (let j = 0, len = bill.length; j < len; j++) {
							arrData.push(bill[j].values);
						}
					}
					self.allBillDataClone = JSON.parse(JSON.stringify(arrData));
					self.allBillData = arrData;
					if (arrData.length > 0) {
						for (let i = 0, len = arrData.length; i < len; i++) {
							obj.number = i + 1;
							obj.key = String(i + 1);
							obj.sourceorg = arrData[i].sourceorg && arrData[i].sourceorg.display;
							obj.sourcebillname = arrData[i].sourcebillname && arrData[i].sourcebillname.value;
							obj.targetorg = arrData[i].targetorg && arrData[i].targetorg.display;
							obj.targetbillname = arrData[i].targetbillname && arrData[i].targetbillname.value;
							obj.scomment = arrData[i].scomment && arrData[i].scomment.value;
							obj.receivers = arrData[i].receivers && arrData[i].receivers.display;
							obj.busitype = arrData[i].busitype && arrData[i].busitype.display;
							obj.sourcesystemVal = arrData[i].sourcebill && arrData[i].sourcebill.value;
							obj.targetsystemVal = arrData[i].targetbill && arrData[i].targetbill.value;
							brr.push(JSON.parse(JSON.stringify(obj)));
						}
					}
					self.setState({
						billData: brr,
						selectedRow: new Array(brr.length) //状态同步
					});
					setTimeout(() => {
						self.billDataClone = JSON.parse(JSON.stringify(self.state.billData));
					}, 1000);
				}
			}
		});
	};

	rowClassNameHandler = (record, index, indent) => {
		if (this.state.selectedRow[index]) {
			return 'selected';
		} else {
			return '';
		}
	};

	//双击行
	onRowClick = (record, index, event) => {
		this.clickIndex = index;
		this.setState(
			{
				isClick: true,
				isTable: false,
				isBack: true,
				DetailDatas: record
			},
			() => {
				this.props.button.setButtonVisible([ 'Return', 'Edit', 'Delete' ], true);
			}
		);
	};
	//点击返回按钮
	handleReturn = () => {
		this.setState(
			{
				isClick: false,
				isBack: false,
				isTable: true
			},
			() => {
				this.props.button.setButtonVisible([ 'Return', 'Edit', 'Delete' ], false);
				this.props.button.setButtonVisible([ 'Add' ], true);
			}
		);
	};

	//点击新增按钮
	handleAdd = () => {
		this.setState(
			{
				isAdd: true,
				isTable: false,
				isBtnStyle: false,
				isClick: false,
				isBack: false
			},
			() => {
				this.props.button.setButtonVisible([ 'Save', 'Cancel' ], true);
				this.props.button.setButtonVisible([ 'Add', 'Refresh', 'Edit', 'Delete' ], false);
			}
		);
	};
	//点击取消按钮
	handleAddCancel = () => {
		promptbox(
			this.state.json['arappub-000032'],
			this.callback_OK,
			'warning',
			this.state.json['arappub-000033']
		); /* 国际化处理： ​确定要取消吗？,取消*/
	};
	callback_OK = () => {
		this.setState(
			{
				isAdd: false,
				isTable: true,
				isBtnStyle: true
			},
			() => {
				this.props.button.setButtonVisible([ 'Save', 'Cancel' ], false);
				this.props.button.setButtonVisible([ 'Add', 'Refresh' ], true);
			}
		);
	};

	//保存按钮
	handleSave = () => {
		let { isAdd } = this.state;
		if (isAdd) {
			this.handleAddKeep();
		} else {
			this.handleModifyKeep();
		}
	};

	//新增保存按钮
	handleAddKeep = () => {
		let { addMainData } = this.state;
		let {
			busitype,
			receivers,
			scomment,
			sourcebillname,
			sourceorg,
			targetorg,
			targetbillname,
			sourcesystem,
			targetsystem
		} = addMainData;

		if (busitype.value == null) {
			toast({ content: this.state.json['arappub-000034'], color: 'warning' }); /* 国际化处理： 请指定流程名称*/
			return;
		}
		if (sourcebillname.value == null) {
			toast({ content: this.state.json['arappub-000035'], color: 'warning' }); /* 国际化处理： 请指定发送方交易类型*/
			return;
		}
		if (targetbillname.value == null) {
			toast({ content: this.state.json['arappub-000036'], color: 'warning' }); /* 国际化处理： 请指定接收方交易类型*/
			return;
		}
		if (busitype.value && sourcebillname.value && targetbillname.value) {
			if (sourcesystem.value != targetsystem.value) {
				if (sourceorg.value && targetorg.value && sourceorg.value == targetorg.value) {
					toast({
						content: this.state.json['arappub-000037'],
						color: 'warning'
					}); /* 国际化处理： 对于发送方为应付系统供应商付款单，未选择有效的接收方设置，请查看协同帮助*/
				} else {
					let arr = [],
						length = receivers.length;
					for (var i = 0; i < length; i++) {
						arr.push(receivers[i].refpk);
					}
					let brr = arr.join();
					let data = {
						busitype: busitype.value,
						docontrol: '0',
						dr: '0',
						sourcebill: sourcebillname.code,
						sourcebillname: sourcebillname.display,
						pk_bconfer: '1001AA1000000000L419',
						pk_group: '0001A310000000000E3L',
						receivers: brr,
						scomment: scomment.value,
						sourceorg: sourceorg.value,
						status: '0',
						targetbill: targetbillname.code,
						targetbillname: targetbillname.display,
						targetorg: targetorg.value
					};
					let url = '/nccloud/arap/billconfer/selfinsert.do';
					ajax({
						url: url,
						data: data,
						success: (res) => {
							this.getInitialData();
							this.setState(
								{
									isAdd: false,
									isTable: true
								},
								() => {
									toast({
										content: this.state.json['arappub-000038'],
										color: 'success'
									}); /* 国际化处理： 保存成功！*/
									this.props.button.setButtonVisible([ 'Save', 'Cancel' ], false);
									this.props.button.setButtonVisible([ 'Add', 'Refresh' ], true);
								}
							);
						}
					});
				}
			} else {
				toast({
					content: this.state.json['arappub-000037'],
					color: 'warning'
				}); /* 国际化处理： 对于发送方为应付系统供应商付款单，未选择有效的接收方设置，请查看协同帮助*/
			}
		}
	};

	//点击行
	handleOnRowClick = (record, index, indent) => {
		let selectedRow = new Array(this.state.billData.length);
		selectedRow[index] = true;
		let recordData;
		let { isRealModify } = this.state;
		if (isRealModify == 2) {
			recordData = this.billDataClone[index];
		} else {
			recordData = this.allBillData[index];
		}
		this.setState({
			factoryValue: record,
			selectedRow: selectedRow,
			isSelectRow: index,
			recordData: recordData
		});
	};

	//表格里  修改按钮
	handleModify = (index) => {
		this.clickIndex = index;
		this.modifyAction(index);
	};

	// 标题行上 修改按钮
	handleEdit = () => {
		this.modifyAction(this.clickIndex);
	};

	//修改事件
	modifyAction = (index) => {
		this.selectRowTransData = this.allBillData[index];
		this.selectRowTransData.sourceorg.refpk2 = this.selectRowTransData.sourceorg.value;
		this.selectRowTransData.targetorg.refpk2 = this.selectRowTransData.targetorg.value;
		this.allBillDataClone[index].sourceorg.refpk2 = this.allBillDataClone[index].sourceorg.value;
		this.allBillDataClone[index].targetorg.refpk2 = this.allBillDataClone[index].targetorg.value;
		this.selectRowTransDataClone = JSON.parse(JSON.stringify(this.allBillDataClone[index]));
		this.setState(
			{
				recordData: this.allBillData[index],
				isModify: true,
				isTable: false,
				isBtnStyle: false,
				index: index,
				isClick: false,
				isBack: false
			},
			() => {
				this.props.button.setButtonVisible([ 'Save', 'Cancel' ], true);
				this.props.button.setButtonVisible([ 'Add', 'Refresh', 'Edit', 'Delete' ], false);
			}
		);
	};

	//取消按钮
	handleCancel = () => {
		let { isModify } = this.state;
		if (!isModify) {
			this.handleAddCancel();
		} else {
			this.handleModifyCancel();
		}
	};

	//点击修改保存按钮
	handleModifyKeep = () => {
		let modifyData = this.child.getModifyData();
		let receiversData = modifyData.receivers;
		let receiversArr = [];
		receiversData.map((item) => {
			receiversArr.push(item.refpk);
		});
		receiversArr = receiversArr.join(',');
		let data = {
			docontrol: '0',
			dr: '0',
			sourcebill: modifyData.sourcebill.value,
			sourcebillname: modifyData.sourcebill.display,
			receivers: receiversArr,
			scomment: modifyData.scomment.value,
			sourceorg: modifyData.sourceorg.value,
			targetbill: modifyData.targetbill.value,
			targetbillname: modifyData.targetbill.display,
			targetorg: modifyData.targetorg.value,
			busitype: modifyData.busitype.value,
			pk_bconfer: this.selectRowTransData.pk_bconfer.value
		};
		if (data.busitype == undefined) {
			toast({ content: this.state.json['arappub-000034'], color: 'warning' }); /* 国际化处理： 请指定流程名称*/
			return;
		}
		if (data.sourcebillname == undefined) {
			toast({ content: this.state.json['arappub-000035'], color: 'warning' }); /* 国际化处理： 请指定发送方交易类型*/
			return;
		}
		if (data.targetbillname == undefined) {
			toast({ content: this.state.json['arappub-000036'], color: 'warning' }); /* 国际化处理： 请指定接收方交易类型*/
			return;
		}
		if (data.busitype && data.sourcebillname && data.targetbillname) {
			if (modifyData.sourcesystem.value != modifyData.targetsystem.value) {
				if (
					modifyData.sourceorg.value &&
					modifyData.targetorg.value &&
					modifyData.sourceorg.value == modifyData.targetorg.value
				) {
					toast({
						content: this.state.json['arappub-000037'],
						color: 'warning'
					}); /* 国际化处理： 对于发送方为应付系统供应商付款单，未选择有效的接收方设置，请查看协同帮助*/
				} else {
					ajax({
						url: '/nccloud/arap/billconfer/selfupdate.do',
						data: data,
						success: (res) => {
							this.setState(
								{
									isModify: false,
									isRealModify: 1,
									isTable: true
								},
								() => {
									toast({
										content: this.state.json['arappub-000038'],
										color: 'success'
									}); /* 国际化处理： 保存成功！*/
									this.getInitialData();
									this.props.button.setButtonVisible([ 'Save', 'Cancel' ], false);
									this.props.button.setButtonVisible([ 'Add', 'Refresh' ], true);
								}
							);
						}
					});
				}
			} else {
				toast({
					content: this.state.json['arappub-000037'],
					color: 'warning'
				}); /* 国际化处理： 对于发送方为应付系统供应商付款单，未选择有效的接收方设置，请查看协同帮助*/
			}
		}
	};

	//点击修改取消按钮
	handleModifyCancel = () => {
		promptbox(
			this.state.json['arappub-000032'],
			this.callbackOK,
			'warning',
			this.state.json['arappub-000033']
		); /* 国际化处理： ​确定要取消吗？,取消*/
	};

	callbackOK = () => {
		let { DetailDatas } = this.state;
		let selectRowData = this.selectRowTransData;
		const OldbillData = this.billDataClone;
		DetailDatas = {
			busitype: selectRowData.busitype.display,
			receivers: selectRowData.receivers.display,
			scomment: selectRowData.scomment.display,
			sourcebillname: selectRowData.sourcebillname.value,
			sourceorg: selectRowData.sourceorg.display,
			sourcesystem: selectRowData.sourcesystem ? selectRowData.sourcesystem.display : null,
			targetbillname: selectRowData.targetbillname.value,
			targetorg: selectRowData.targetorg.display,
			targetsystem: selectRowData.targetorg.display,
			sourcesystemVal: selectRowData.sourcebill.value,
			targetsystemVal: selectRowData.targetbill.value
		};
		this.setState(
			{
				DetailDatas,
				isModify: false,
				isRealModify: 2,
				isClick: true,
				isBack: true,
				isBtnStyle: true,
				isSelectRow: '',
				billData: OldbillData
			},
			() => {
				this.props.button.setButtonVisible([ 'Save', 'Cancel', 'Refresh' ], false);
				this.props.button.setButtonVisible([ 'Add', 'Edit', 'Delete' ], true);
			}
		);
	};

	//删除数据时，数据重新排序
	getArrayIndex = (tableData) => {
		for (var i = 0, len = tableData.length; i < len; i++) {
			tableData[i].number = i + 1;
		}
		return tableData;
	};

	//删除按钮 表格行上的
	handleDelete = (index) => {
		let that = this;
		return () => {
			let data = {
				pk_bill: this.allBillData[index].pk_bconfer.value
			};
			ajax({
				url: '/nccloud/arap/billconfer/delete.do',
				data: data,
				success: (res) => {
					let { billData } = that.state;
					billData.splice(index, 1);
					this.allBillData.splice(index, 1);
					billData = this.getArrayIndex(billData);
					that.setState({ billData });
				}
			});
		};
	};

	handleAddCancel = () => {
		promptbox(
			this.state.json['arappub-000032'],
			this.callback_OK,
			'warning',
			this.state.json['arappub-000033']
		); /* 国际化处理： ​确定要取消吗？,取消*/
	};
	callback_OK = () => {
		this.setState(
			{
				isAdd: false,
				isTable: true,
				isBtnStyle: true
			},
			() => {
				this.props.button.setButtonVisible([ 'Save', 'Cancel' ], false);
				this.props.button.setButtonVisible([ 'Add', 'Refresh' ], true);
			}
		);
	};

	//删除按钮  标题上的
	handleHeadDelete = () => {
		promptbox(
			this.state.json['arappub-000071'],
			this.callback_Del,
			'warning',
			this.state.json['arappub-000033']
		); /* 国际化处理： ​确定要取消吗？,取消*/
	};

	callback_Del = () => {
		this.deleteAction(this.clickIndex);
	};

	//删除事件
	deleteAction = (index) => {
		let that = this;
		let data = {
			pk_bill: this.allBillData[index].pk_bconfer.value
		};
		ajax({
			url: '/nccloud/arap/billconfer/delete.do',
			data: data,
			success: (res) => {
				let { billData } = that.state;
				billData.splice(index, 1);
				billData = this.getArrayIndex(billData);
				that.setState({
					billData,
					isTable: true,
					isClick: false,
					isBack: false
				});
				this.props.button.setButtonVisible([ 'Edit', 'Delete' ], false);
			}
		});
	};

	handleClick = () => {
		this.setState({
			isModelShow: !this.state.isModelShow
		});
	};

	//获取add数据
	getAddDatas = (data) => {
		this.setState({
			addMainData: data
		});
	};

	render() {
		let { button } = this.props;
		let { createButtonApp } = button;
		let { billData, isClick, isAdd, isTable, isModify, isBtnStyle, isBack, index } = this.state;
		return (
			<div>
				<div id="billId">
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							<h2 className="title-search-detail">
								{isBack ? (
									<NCBackBtn onClick={this.handleReturn} />
								) : (
									createPageIcon && createPageIcon()
								)}
								{title}
							</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 4,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>

					{!isTable && <div style={{ borderBottom: '1px solid #d0d0d0' }} />}
					<div className="nc-bill-table-area">
						{this.state.isTable && (
							<div className="bill-conent-table">
								<NCTable
									columns={this.state.columns}
									bordered
									isDrag
									data={billData}
									style={{ marginRight: '20px' }}
									onRowDoubleClick={this.onRowClick}
									onRowClick={this.handleOnRowClick}
									rowClassName={this.rowClassNameHandler}
									loading={false}
								/>
							</div>
						)}

						{isClick && <DetailDataShow DetailDatas={this.state.DetailDatas} json={this.state.json} />}

						{isAdd && <AddContions getAddDatas={this.getAddDatas} appcode={appcode} />}

						{isModify && (
							<ModifyContions
								selectRowTransDataClone={this.selectRowTransDataClone}
								recordData={this.state.recordData}
								appcode={appcode}
								onRef={(ref) => {
									this.child = ref;
								}}
							/>
						)}
					</div>
				</div>
			</div>
		);
	}
}

Bill.defaultProps = defaultProps12;

Bill = createPage({
	initTemplate: initTemplate
})(Bill);

ReactDOM.render(<Bill />, document.querySelector('#app'));
