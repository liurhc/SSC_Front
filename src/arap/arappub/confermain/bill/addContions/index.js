import React, { Component } from 'react';
import { base, getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../../../public/ReferLoader';
import FinanceOrgTreeRef from '../../../../../uapbd/refer/org/FinanceOrgTreeRef'; //财务组织
import Transtype from '../../../../../uap/refer/riart/transtype'; //交易类型参照
import getRefer22 from '../../../../public/ReferLoader2';
import './index.less';
const { NCFormControl: FormControl, NCRow: Row, NCCol: Col, NCForm, NCSelect } = base;
const NCFormItem = NCForm.NCFormItem;
const NCOption = NCSelect.NCOption;
let NCOptionData = [];
let appcode;
export default class AddContions extends Component {
	constructor(props) {
		super(props);

		this.state = {
			json: {},
			addMainData: {
				scomment: { display: null, value: null }, //说明
				receivers: [ { refname: null, refpk: null } ], //协同消息接收人
				busitype: { display: null, value: null }, //流程名称
				sourcesystem: { display: '', value: 'F0,F2' }, //来源系统/* 国际化处理： 应收*/
				sourceorg: { display: null, value: null }, //财务组织-发送方
				sourcebillname: { display: null, value: null, code: null }, //交易类型-发送方
				targetsystem: { display: '', value: 'F1,F3' }, //目的系统/* 国际化处理： 应付*/
				targetorg: { display: null, value: null }, //财务组织-接收方
				targetbillname: { display: null, value: null, code: null } //交易类型-接收方
			},
			billType: ''
		};
	}

	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				NCOptionData = [
					{ display: this.state.json['arappub-000010'], value: 'F0,F2' },
					{ display: this.state.json['arappub-000011'], value: 'F1,F3' }
				]; /* 国际化处理： 应收,应付*/
				this.setState({
					addMainData: {
						scomment: { display: null, value: null }, //说明
						receivers: [ { refname: null, refpk: null } ], //协同消息接收人
						busitype: { display: null, value: null }, //流程名称
						sourcesystem: { display: this.state.json['arappub-000010'], value: 'F0,F2' }, //来源系统/* 国际化处理： 应收*/
						sourceorg: { display: null, value: null }, //财务组织-发送方
						sourcebillname: { display: null, value: null, code: null }, //交易类型-发送方
						targetsystem: { display: this.state.json['arappub-000011'], value: 'F1,F3' }, //目的系统/* 国际化处理： 应付*/
						targetorg: { display: null, value: null }, //财务组织-接收方
						targetbillname: { display: null, value: null, code: null } //交易类型-接收方
					}
				});
				appcode = this.props.appcode;
				let billType = appcode.substring(0, 4) == '2006' ? 'F0,F2' : 'F1,F3';
				this.setState({
					billType
				});
			});
		};
		getMultiLang({ moduleId: 'arappub', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	//获取参照
	getRefer = (tag, refcode, key, addMainData, name) => {
		return (
			<NCFormItem showMast={false} labelName={name} isRequire={true} inline={true}>
				<Referloader
					tag={tag}
					refcode={refcode}
					value={{
						refname: key.display,
						refpk: key.value,
						refpk2: key.refpk2
					}}
					queryCondition={() => {
						return {
							// DataPowerOperationCode: 'fi', //使用权组
							// AppCode: appcode,
							// TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
							DataPowerOperationCode: 'fi', //使用权组
							isDataPowerEnable: 'Y'
						};
					}}
					onChange={(v) => {
						key.value = v.refpk;
						key.display = v.refname;
						key.refpk2 = v.refpk2;
						this.setState({
							addMainData
						});
						this.props.getAddDatas(addMainData);
					}}
					placeholder={''}
				/>
			</NCFormItem>
		);
	};

	//参照展示
	getRefer2 = (Refer, addMainData, name, key) => {
		return (
			<NCFormItem showMast={false} labelName={name} isRequire={true} inline={true}>
				<div className="finance">
					<div className="finance-refer">
						<Refer
							value={{
								refname: key.display,
								refpk: key.value,
								refpk2: key.refpk2
							}}
							queryCondition={() => {
								return {
									// DataPowerOperationCode: 'fi', //使用权组
									// AppCode: appcode,
									// TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y'
								};
							}}
							onChange={(v) => {
								key.value = v.refpk;
								key.display = v.refname;
								key.refpk2 = v.refpk2;
								this.setState({
									addMainData
								});
								this.props.getAddDatas(addMainData);
							}}
							placeholder={''}
						/>
					</div>
				</div>
			</NCFormItem>
		);
	};

	render() {
		let { addMainData } = this.state;
		let {
			scomment,
			receivers,
			busitype,
			sourcesystem,
			sourceorg,
			sourcebillname,
			targetsystem,
			targetorg,
			targetbillname
		} = addMainData;
		const NCOptions = NCOptionData.map((province) => <NCOption value={province}>{province.display}</NCOption>);
		return (
			<div className="content-style">
				<div className="bill-instruction">
					<div />
					<div>
						<div className="explainStyle">
							<NCFormItem
								showMast={false}
								labelName={this.state.json['arappub-000012']}
								isRequire={true}
								inline={true}
							>
								{/* 国际化处理： 说明*/}
								<FormControl
									className="bill-instruction-input"
									style={{
										height: '30px',
										maxHeight: '64px',
										width: '210px',
										maxWidth: '230px'
									}}
									componentClass="textarea"
									value={scomment.value || ''}
									onChange={(v) => {
										addMainData.scomment.value = v;
										this.setState({
											addMainData
										});
										this.props.getAddDatas(addMainData);
									}}
								/>
							</NCFormItem>
						</div>
						<NCFormItem
							showMast={false}
							labelName={this.state.json['arappub-000013']}
							isRequire={true}
							inline={true}
						>
							{/* 国际化处理： 协同消息接收人*/}
							<Referloader
								tag="roleRefer"
								refcode="uap/refer/riart/roleRefer/index.js"
								value={receivers}
								queryCondition={() => {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										isAuthFilter: false
									};
								}}
								onChange={(v) => {
									this.state.addMainData.receivers = v;
									this.setState({
										addMainData
									});
									this.props.getAddDatas(addMainData);
								}}
								placeholder={''}
							/>
						</NCFormItem>
						{/* {this.getRefer('busiType','uap/refer/riart/busitype/index.js',busitype,addMainData,'流程名称')} */}
						{getRefer22(
							this,
							'busiType',
							'uap/refer/riart/busitype/index.js',
							busitype,
							addMainData,
							this.state.json['arappub-000014'] /* 国际化处理： 流程名称*/,
							this.state.billType,
							false,
							false,
							true
						)}
					</div>
				</div>
				<div className="bill-send-cont">
					<div className="txt">{this.state.json['arappub-000019']}</div>
					{/* 国际化处理： 发送方*/}
					<div>
						<NCFormItem
							showMast={false}
							labelName={this.state.json['arappub-000015']}
							isRequire={true}
							inline={true}
						>
							{/* 国际化处理： 来源系统*/}
							<NCSelect
								value={sourcesystem.display}
								style={{ width: '210px' }}
								onChange={(v) => {
									sourcesystem.display = v.display;
									sourcesystem.value = v.value;
									sourcebillname.display = null;
									sourcebillname.value = null;
									this.setState({
										addMainData
									});
									this.props.getAddDatas(addMainData);
								}}
							>
								{NCOptions}
							</NCSelect>
						</NCFormItem>
						{this.getRefer(
							'FinanceOrgTreeRef',
							'uapbd/refer/org/FinanceOrgTreeRef/index.js',
							sourceorg,
							addMainData,
							this.state.json['arappub-000016'] /* 国际化处理： 财务组织*/
						)}
						<NCFormItem
							showMast={true}
							labelName={this.state.json['arappub-000017']}
							isRequire={true}
							inline={true}
						>
							{/* 国际化处理： 交易类型*/}
							<Referloader
								tag="transTypes"
								refcode="uap/refer/riart/transtype/index.js"
								value={{
									refname: sourcebillname.display,
									refpk: sourcebillname.value
								}}
								queryCondition={() => {
									let condition = {
										parentbilltype: sourcesystem.value
									};
									return condition;
								}}
								onChange={(v) => {
									sourcebillname.value = v.refpk;
									sourcebillname.display = v.refname;
									sourcebillname.code = v.refcode;
									this.setState({
										addMainData
									});
									this.props.getAddDatas(addMainData);
								}}
								placeholder={''}
							/>
						</NCFormItem>
					</div>
				</div>
				<div className="bill-receive-cont">
					<div className="txt">{this.state.json['arappub-000020']}</div>
					{/* 国际化处理： 接收方*/}
					<div>
						<NCFormItem
							showMast={false}
							labelName={this.state.json['arappub-000018']}
							isRequire={true}
							inline={true}
						>
							{/* 国际化处理： 目的系统*/}
							<NCSelect
								value={targetsystem.display}
								style={{ width: '210px' }}
								onChange={(v) => {
									targetsystem.display = v.display;
									targetsystem.value = v.value;
									targetbillname.display = null;
									targetbillname.value = null;
									this.setState({
										addMainData
									});
									this.props.getAddDatas(addMainData);
								}}
							>
								{NCOptions}
							</NCSelect>
						</NCFormItem>
						{this.getRefer2(FinanceOrgTreeRef, addMainData, this.state.json['arappub-000016'], targetorg)}
						{/* 国际化处理： 财务组织*/}
						{/* {this.getRefer('FinanceOrgTreeRefff','uapbd/refer/org/FinanceOrgTreeRef/index.js',targetorg,addMainData,'财务组织')} */}
						<NCFormItem
							showMast={true}
							labelName={this.state.json['arappub-000017']}
							isRequire={true}
							inline={true}
						>
							{/* 国际化处理： 交易类型*/}
							<div className="finance">
								<div className="finance-refer">
									<div className="refer-wrapper">
										{/* <span className="required-star">*</span> */}
										{/* <Transtype
											value={{
												refname: targetbillname.display,
												refpk: targetbillname.value
											}}
											queryCondition={() => {
												let condition = {
													parentbilltype: targetsystem.value
												};
												return condition;
											}}
											onChange={(v) => {
												targetbillname.value = v.refpk;
												targetbillname.display = v.refname;
												targetbillname.code = v.refcode;
												this.setState({
													addMainData
												});
												this.props.getAddDatas(addMainData);
											}}
											placeholder={''}
										/> */}
										<Referloader
											tag="transTypes"
											refcode="uap/refer/riart/transtype/index.js"
											value={{
												refname: targetbillname.display,
												refpk: targetbillname.value
											}}
											queryCondition={() => {
												let condition = {
													parentbilltype: targetsystem.value
												};
												return condition;
											}}
											onChange={(v) => {
												targetbillname.value = v.refpk;
												targetbillname.display = v.refname;
												targetbillname.code = v.refcode;
												this.setState({
													addMainData
												});
												this.props.getAddDatas(addMainData);
											}}
											placeholder={''}
										/>
									</div>
								</div>
							</div>
						</NCFormItem>
					</div>
				</div>
			</div>
		);
	}
}
