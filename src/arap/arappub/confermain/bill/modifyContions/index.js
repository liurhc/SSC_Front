import React, { Component } from 'react';
import { base, getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../../../public/ReferLoader';
import FinanceOrgTreeRef from '../../../../../uapbd/refer/org/FinanceOrgTreeRef'; //财务组织
import getRefer22 from '../../../../public/ReferLoader2';
const { NCFormControl: FormControl, NCRow: Row, NCCol: Col, NCForm, NCSelect } = base;
const NCFormItem = NCForm.NCFormItem;
const NCOption = NCSelect.NCOption;
let NCOptionData = [];
let appcode;
let billType;
export default class ModifyContions extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {},
			mainData: {
				scomment: { display: null, value: null }, //说明
				receivers: [], //协同消息接收人
				busitype: { display: null, value: null }, //流程名称
				sourcesystem: { display: '', value: 'F0,F2' }, //来源系统/* 国际化处理： 应收*/
				sourceorg: { display: null, value: null }, //财务组织-发送方
				sourcebill: { display: null, value: null, code: null }, //交易类型-发送方
				targetsystem: { display: '', value: 'F1,F3' }, //目的系统/* 国际化处理： 应付*/
				targetorg: { display: null, value: null }, //财务组织-接收方
				targetbill: { display: null, value: null, code: null } //交易类型-接收方
			}
		};
	}

	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				NCOptionData = [
					{ display: this.state.json['arappub-000010'], value: 'F0,F2' },
					{ display: this.state.json['arappub-000011'], value: 'F1,F3' }
				]; /* 国际化处理： 应收,应付*/
				this.setState(
					{
						mainData: {
							scomment: { display: null, value: null }, //说明
							receivers: [], //协同消息接收人
							busitype: { display: null, value: null }, //流程名称
							sourcesystem: { display: this.state.json['arappub-000010'], value: 'F0,F2' }, //来源系统/* 国际化处理： 应收*/
							sourceorg: { display: null, value: null }, //财务组织-发送方
							sourcebill: { display: null, value: null, code: null }, //交易类型-发送方
							targetsystem: { display: this.state.json['arappub-000011'], value: 'F1,F3' }, //目的系统/* 国际化处理： 应付*/
							targetorg: { display: null, value: null }, //财务组织-接收方
							targetbill: { display: null, value: null, code: null } //交易类型-接收方
						}
					},
					() => {
						this.props.onRef(this);
						let { mainData } = this.state;
						let recordData = this.props.recordData;
						let selectRowDataClone = this.props.selectRowTransDataClone;
						appcode = this.props.appcode;
						billType = appcode.substring(0, 4) == '2006' ? 'F0,F2,23E0' : 'F1,F3,23E1';
						for (var i in recordData) {
							if (recordData[i].value == undefined) {
								delete recordData[i];
							}
						}
						let recordDataArr = [
							'scomment',
							'receivers',
							'busitype',
							'sourcesystem',
							'sourceorg',
							'sourcebill',
							'sourcebillname',
							'targetsystem',
							'targetorg',
							'targetbill',
							'targetbillname'
						];
						for (var i = 0; i < recordDataArr.length; i++) {
							if (!(recordDataArr[i] in recordData)) {
								recordData[recordDataArr[i]] = { display: null, value: null };
							}
						}
						for (var i in recordData) {
							if (recordData[i].display === undefined) {
								recordData[i]['display'] = null;
							}
						}
						let receiver = [];
						if (recordData.receivers.value) {
							let displays = recordData.receivers.display.split(',');
							let values = recordData.receivers.value.split(',');
							for (var i = 0, l = values.length; i < l; i++) {
								receiver.push({
									refname: displays[i],
									refpk: values[i]
								});
							}
						}
						mainData.scomment = selectRowDataClone.scomment;
						mainData.receivers = selectRowDataClone.receivers ? receiver : selectRowDataClone.receivers;
						mainData.busitype = selectRowDataClone.busitype;
						mainData.sourceorg = selectRowDataClone.sourceorg;
						mainData.sourcebill.display = selectRowDataClone.sourcebillname.value;
						mainData.sourcebill.value = selectRowDataClone.sourcebill.value;
						mainData.targetorg = selectRowDataClone.targetorg;
						mainData.targetbill.display = selectRowDataClone.targetbillname.value;
						mainData.targetbill.value = selectRowDataClone.targetbill.value;
						if (
							recordData.targetbill.value == 'D0' ||
							recordData.targetbill.value == 'D2' ||
							recordData.targetbill.value.indexOf('F0-') != -1 ||
							recordData.targetbill.value.indexOf('F2-') != -1
						) {
							mainData.targetsystem = {
								display: this.state.json['arappub-000010'] /* 国际化处理： 应收*/,
								value: 'F0,F2'
							};
						}
						if (
							recordData.sourcebill.value == 'D1' ||
							recordData.sourcebill.value == 'D3' ||
							recordData.sourcebill.value.indexOf('F1-') != -1 ||
							recordData.sourcebill.value.indexOf('F3-') != -1
						) {
							mainData.sourcesystem = {
								display: this.state.json['arappub-000011'] /* 国际化处理： 应付*/,
								value: 'F1,F3'
							};
						}

						this.setState({
							mainData
						});
					}
				);
			});
		};
		getMultiLang({ moduleId: 'arappub', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	//获取参照
	getRefer = (tag, refcode, key, mainData, name) => {
		return (
			<NCFormItem showMast={false} labelName={name} isRequire={true} inline={true}>
				<Referloader
					tag={tag}
					refcode={refcode}
					value={{
						refname: key.display,
						refpk: key.value,
						refpk2: key.refpk2
					}}
					queryCondition={() => {
						return {
							DataPowerOperationCode: 'fi', //使用权组
							isDataPowerEnable: 'Y'
						};
					}}
					onChange={(v) => {
						key.value = v.refpk;
						key.display = v.refname;
						key.refpk2 = v.refpk2;
						this.setState({
							mainData
						});
					}}
				/>
			</NCFormItem>
		);
	};

	//参照展示
	getRefer2 = (Refer, mainData, name, key) => {
		return (
			<NCFormItem showMast={false} labelName={name} isRequire={true} inline={true}>
				<div className="finance">
					<div className="finance-refer">
						<Refer
							value={{
								refname: key.display,
								refpk: key.value,
								refpk2: key.refpk2
							}}
							queryCondition={() => {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y'
								};
							}}
							onChange={(v) => {
								key.value = v.refpk;
								key.display = v.refname;
								key.refpk2 = v.refpk2;
								this.setState({
									mainData
								});
							}}
						/>
					</div>
				</div>
			</NCFormItem>
		);
	};

	//获取修改条件数据
	getModifyData = () => {
		let { mainData } = this.state;
		return mainData;
	};

	render() {
		let { mainData } = this.state;

		let {
			scomment,
			receivers,
			busitype,
			sourcesystem,
			sourceorg,
			sourcebill,
			targetsystem,
			targetorg,
			targetbill
		} = mainData;
		const NCOptions = NCOptionData.map((province) => <NCOption value={province}>{province.display}</NCOption>);
		return (
			<div className="content-style">
				<div className="bill-instruction">
					<div />
					<div>
						<div className="explainStyle">
							<NCFormItem
								showMast={false}
								labelName={this.state.json['arappub-000012']}
								isRequire={true}
								inline={true}
							>
								{/* 国际化处理： 说明*/}
								<FormControl
									className="bill-instruction-input"
									style={{
										height: '30px',
										maxHeight: '64px',
										width: '210px',
										maxWidth: '230px'
									}}
									componentClass="textarea"
									value={scomment.value || ''}
									onChange={(v) => {
										mainData.scomment.value = v;
										this.setState({
											mainData
										});
									}}
								/>
							</NCFormItem>
						</div>
						<NCFormItem
							showMast={false}
							labelName={this.state.json['arappub-000013']}
							isRequire={true}
							inline={true}
						>
							{/* 国际化处理： 协同消息接收人*/}
							<Referloader
								tag="roleRefer"
								refcode="uap/refer/riart/roleRefer/index.js"
								value={receivers}
								queryCondition={() => {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										isAuthFilter: false
									};
								}}
								onChange={(v) => {
									this.state.mainData.receivers = v;
									this.setState({
										mainData
									});
								}}
								placeholder={''}
							/>
						</NCFormItem>
						{/* {this.getRefer('busiType','uap/refer/riart/busitype/index.js',busitype,mainData,'流程名称')} */}
						{getRefer22(
							this,
							'busiType',
							'uap/refer/riart/busitype/index.js',
							busitype,
							mainData,
							this.state.json['arappub-000014'] /* 国际化处理： 流程名称*/,
							billType,
							true,
							false,
							true
						)}
					</div>
				</div>
				<div className="bill-send-cont">
					<div className="txt">{this.state.json['arappub-000019']}</div>
					{/* 国际化处理： 发送方*/}
					<div>
						<NCFormItem
							showMast={false}
							labelName={this.state.json['arappub-000015']}
							isRequire={true}
							inline={true}
						>
							{/* 国际化处理： 来源系统*/}
							<NCSelect
								value={sourcesystem.display}
								style={{ width: '210px' }}
								onChange={(v) => {
									sourcesystem.display = v.display;
									sourcesystem.value = v.value;
									sourcebill.display = null;
									sourcebill.value = null;
									this.setState({
										mainData
									});
								}}
							>
								{NCOptions}
							</NCSelect>
						</NCFormItem>
						{this.getRefer(
							'FinanceOrgTreeRef',
							'uapbd/refer/org/FinanceOrgTreeRef/index.js',
							sourceorg,
							mainData,
							this.state.json['arappub-000016'] /* 国际化处理： 财务组织*/
						)}
						<NCFormItem
							showMast={true}
							labelName={this.state.json['arappub-000017']}
							isRequire={true}
							inline={true}
						>
							{/* 国际化处理： 交易类型*/}
							<Referloader
								tag="transTypes"
								refcode="uap/refer/riart/transtype/index.js"
								value={{
									refname: sourcebill.display,
									refpk: sourcebill.value
								}}
								queryCondition={() => {
									let condition = {
										parentbilltype: sourcesystem.value
									};
									return condition;
								}}
								onChange={(v) => {
									sourcebill.value = v.refcode;
									sourcebill.display = v.refname;
									sourcebill.code = v.refpk;
									this.setState({
										mainData
									});
								}}
							/>
						</NCFormItem>
					</div>
				</div>
				<div className="bill-receive-cont">
					<div className="txt">{this.state.json['arappub-000020']}</div>
					{/* 国际化处理： 接收方*/}
					<div>
						<NCFormItem
							showMast={false}
							labelName={this.state.json['arappub-000018']}
							isRequire={true}
							inline={true}
						>
							{/* 国际化处理： 目的系统*/}
							<NCSelect
								value={targetsystem.display}
								style={{ width: '210px' }}
								onChange={(v) => {
									targetsystem.display = v.display;
									targetsystem.value = v.value;
									targetbill.display = null;
									targetbill.value = null;
									this.setState({
										mainData
									});
								}}
							>
								{NCOptions}
							</NCSelect>
						</NCFormItem>
						{this.getRefer2(FinanceOrgTreeRef, mainData, this.state.json['arappub-000016'], targetorg)}
						{/* 国际化处理： 财务组织*/}
						<NCFormItem
							showMast={true}
							labelName={this.state.json['arappub-000017']}
							isRequire={true}
							inline={true}
						>
							{/* 国际化处理： 交易类型*/}
							<div className="finance">
								<div className="finance-refer">
									{/* <Transtype
										value={{
											refname: targetbill.display,
											refpk: targetbill.value
										}}
										queryCondition={() => {
											let condition = {
												parentbilltype: targetsystem.value
											};
											return condition;
										}}
										onChange={(v) => {
											targetbill.value = v.refcode;
											targetbill.display = v.refname;
											targetbill.code = v.refpk;
											this.setState({
												mainData
											});
										}}
									/> */}
									<Referloader
										tag="transTypes"
										refcode="uap/refer/riart/transtype/index.js"
										value={{
											refname: targetbill.display,
											refpk: targetbill.value
										}}
										queryCondition={() => {
											let condition = {
												parentbilltype: targetsystem.value
											};
											return condition;
										}}
										onChange={(v) => {
											targetbill.value = v.refcode;
											targetbill.display = v.refname;
											targetbill.code = v.refpk;
											this.setState({
												mainData
											});
										}}
									/>
								</div>
							</div>
						</NCFormItem>
					</div>
				</div>
			</div>
		);
	}
}
