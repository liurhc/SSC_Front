import { headButton } from '../../../../public/components/pubUtils/buttonName';

export default function(props, id) {
	switch (id) {
		case headButton.Add: //新增
			this.handleAdd();
			break;
		case headButton.Save: //保存
			this.handleSave();
			break;
		case headButton.Cancel: //取消
			this.handleCancel();
			break;
		case headButton.Edit: //修改（标题上）
			this.handleEdit();
			break;
		case headButton.Delete: //删除 （标题上）
			this.handleHeadDelete();
			break;
	}
}
