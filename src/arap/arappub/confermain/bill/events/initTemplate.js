import {ajax} from 'nc-lightapp-front';

export default function (props) {
	let appcode = props.getSearchParam('c'); //获取小应用编码
	let pagecode = props.getSearchParam('p'); //获取页面编码
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do',
		data: {
			pagecode: pagecode,
			appcode: appcode
		},
		async: false,
		success: (res) => {
			if (res.data) {
				let button  = res.data;
				props.button.setButtons(button);
			}
		}
	})
}

