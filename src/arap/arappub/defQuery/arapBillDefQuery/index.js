/*
 * 收付单据在会计平台注册的自定义查询条件
 * @Author: xiejhk 
 */
import React, { Component } from 'react';
import { createPage,cacheTools,ajax,getMultiLang } from 'nc-lightapp-front';
import { modifierSearchMetas } from '../../../public/components/pubUtils/arapListSearchRefFilter';

const SEARCHAREA = 'query';
const TABLE = 'list';
const searchId = 'query';
class ArapBillDefQuery extends Component {
	constructor(props) {
		super(props);
		//从缓存中获取查询模板所需数据
		let typeData = cacheTools.get('checkedPage');
		let billType;
		if(typeData.billType){
			billType = typeData.billType;
		}
		let tradeType = billType;
		if(typeData.tradeType){
			tradeType = typeData.tradeType;
		}
		//根据交易类型查询对应的小应用编码和页面编码
		this.getAppMessage(billType,tradeType);
		
		this.state = {
			json: {}
		}
	}
	componentWillMount(){
		let callback = (json) => {
			this.setState({ json: json });
		}
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {
		this.props.onRef && this.props.onRef(this);
	}

	/**
	 * 根据交易类型(单据类型)查询相应小应用信息
	 */
	getAppMessage = (billType,tradeType) => {
		ajax({
			url: '/nccloud/arap/arappub/getAppMessageByBillType.do',
			data: {
				billType : tradeType,
				sence : 1 //查询默认场景下对应小应用
			},
			success: (res) => {
				let appMessage = res.data;
				if(appMessage){
					let appPageVO = appMessage.appPageVO;
					let APPCODE = appPageVO.pagecode.split('_')[0];
					let PAGECODE = APPCODE+'_LIST';
					// 初始化模板
					this.initTemplate(APPCODE,PAGECODE,billType);
				}
			}
		});
	}



	initTemplate = (APPCODE,PAGECODE,billType) => {
		this.props.createUIDom(
			{
				appcode: APPCODE,
				pagecode: PAGECODE
			},
			(data) => {
				if (data) {
					if (data.template) {
						let meta = data.template;
                        //参照过滤
                        modifierSearchMetas(searchId, this.props, meta, billType, data.context.paramMap ? data.context.paramMap.transtype : null,this);
						this.props.meta.setMeta(meta);
					}
				}
			}
		);
	};

	renderComplete = () => {};

	/**
     * 获取关联字段
     */
	getRelationIDField = () => {
		//return FieldConst.cdetailledgerid;
	};

	/**
     * 返回要扩展显示的字段
     */
	getTempletMeta = () => {
		let meta = this.props.meta.getMeta();
		return { pageid: meta['pageid'], meta: meta[TABLE] };
	};

	/**
     * 获取自定义查询条件
     */
	getQueryCondition = () => {
		return JSON.stringify(this.props.search.getQueryInfo(SEARCHAREA));
	};

	render() {
		let { search } = this.props;
		let { NCCreateSearch } = search;
		return (
			<div>
				{NCCreateSearch(SEARCHAREA, {
					onlyShowAdvArea: true,
					renderCompleteEvent: this.renderComplete.bind(this)
				})}
			</div>
		);
	}
}

ArapBillDefQuery = createPage({})(ArapBillDefQuery);
export default function (props = {}) {
	var conf = {
	};
	
	return <ArapBillDefQuery {...Object.assign(conf, props)} />
	}
