/*
 * @Author: liueh 
 * @PageInfo: ar51（传金税的数据合并原则-集团级参数） 参数面板
 * @Date: 2018-07-24 16:07:45 
 * @Last Modified by: yechd5
 * @Last Modified time: 2018-10-26 10:50:34
 */

/**
  * 规则
  * 单选：
  * 不合并 0
  * 按存货合并	1
  * 按存货类合并	2
  * 组合： 
  * 不合并 不能勾复选
  * 按存货合并 按价格合并 5
  * 按存货合并 销售发票是否多单合并 9
  * 按存货合并 按价格合并 销售发票是否多单合并 13
  * 按存货类合并 按价格合并 6
  * 按存货类合并 销售发票是否多单合并 10
  * 按存货类合并 按价格合并 销售发票是否多单合并 14
  */
import React, { Component } from 'react';
import { base ,getMultiLang} from 'nc-lightapp-front';
let { NCModal: Modal, NCButton: Button, NCRadio: Radio, NCCheckbox: Checkbox } = base;
let { NCRadioGroup: RadioGroup } = Radio;
let { Header, Body, Footer } = Modal;
import './index.less';

export default class ParamPanel extends Component {
	constructor(props) {
		super(props);
		this.data = props.data || {};
		let radioVal = '0',
			checkVal1 = false,
			checkVal2 = false;
		// 传过来的props.data.sysinitvo.value 是个字符串数字，代表的含义见上面的规则
		if (this.data.sysinitvo && this.data.sysinitvo.value) {
			switch (this.data.sysinitvo.value) {
				case '0':
				case '1':
				case '2':
					radioVal = this.data.sysinitvo.value;
					break;
				case '5':
					radioVal = '1';
					checkVal1 = true;
					checkVal2 = false;
					break;
				case '6':
					radioVal = '2';
					checkVal1 = true;
					checkVal2 = false;
					break;
				case '9':
					radioVal = '1';
					checkVal1 = false;
					checkVal2 = true;
					break;
				case '10':
					radioVal = '2';
					checkVal1 = false;
					checkVal2 = true;
					break;
				case '13':
					radioVal = '1';
					checkVal1 = true;
					checkVal2 = true;
					break;
				case '14':
					radioVal = '2';
					checkVal1 = true;
					checkVal2 = true;
					break;
				default:
					break;
			}
		}
		this.state = {
			radioVal,
			checkVal1,
			checkVal2,
			// 多语
			json: {},
			inlt: null
		};
	}

	//  确定按钮按上面的规则将勾选状态转换成数字，传递给父组件
	sureBtnClick = () => {
		debugger
		if (this.data.sysinitvo) {
			let value,
				{ radioVal, checkVal1, checkVal2 } = this.state;
			if (radioVal === '0') {
				value = '0';
			} else if (radioVal === '1') {
				if (!checkVal1 && !checkVal2) {
					value = '1';
				} else if (checkVal1 && !checkVal2) {
					value = '5';
				} else if (!checkVal1 && checkVal2) {
					value = '9';
				} else if (checkVal1 && checkVal2) {
					value = '13';
				}
			} else if (radioVal === '2') {
				if (!checkVal1 && !checkVal2) {
					value = '2';
				} else if (checkVal1 && !checkVal2) {
					value = '6';
				} else if (!checkVal1 && checkVal2) {
					value = '10';
				} else if (checkVal1 && checkVal2) {
					value = '14';
				}
			}
			this.data.sysinitvo.value = value;
			this.props.valueChange && this.props.valueChange(this.data);
		}
	};

	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json },() => {
			});
		};
		getMultiLang({ moduleId: 'arappub', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	// 关闭模态框需要调用 props.valueChange ,传递原来旧的数据即可
	closeModal = () => {
		this.props.valueChange && this.props.valueChange(this.data);
	};

	raidoChange = (value) => {
		if (value === '0') {
			this.setState({ checkVal1: false, checkVal2: false });
		}
		this.setState({ radioVal: value });
	};

	render() {
		return (
			<div>
				<Modal show={true} className="param-panel-modal">
					<Header>
						<span className="title">{this.state.json['arappub-000065'] /*国际化处理： 动态参数设置*/}</span>
						<i className="iconfont icon-guanbi" onClick={this.closeModal} />
					</Header>
					<Body>
						<div className="param-panel-form">
							<div className="title">
								{this.state.json['arappub-000057'] /* 国际化处理：
							传金税数据合并规则*/}
							</div>
							<RadioGroup selectedValue={this.state.radioVal} onChange={this.raidoChange}>
								<Radio value="0">{this.state.json['arappub-000058'] /* 国际化处理： 不合并*/}</Radio>
								<Radio value="1">
									{this.state.json['arappub-000059'] /* 国际化处理： 按物料合并*/}
								</Radio>
								<Radio value="2">
									{this.state.json['arappub-000060'] /* 国际化处理： 按物料分类合并*/}
								</Radio>
							</RadioGroup>
							<Checkbox
								checked={this.state.checkVal1}
								disabled={this.state.radioVal === '0'}
								onChange={() => this.setState({ checkVal1: !this.state.checkVal1 })}
							>
								{this.state.json['arappub-000061'] /* 国际化处理： 按价格合并*/}
							</Checkbox>
							<Checkbox
								checked={this.state.checkVal2}
								disabled={this.state.radioVal === '0'}
								onChange={() => this.setState({ checkVal2: !this.state.checkVal2 })}
							>
								{this.state.json['arappub-000062'] /* 国际化处理： 应收单是否多单合并*/}
							</Checkbox>
						</div>
					</Body>
					<Footer>
						<Button className="button-primary" onClick={this.sureBtnClick}>
							{this.state.json['arappub-000063'] /* 国际化处理： 确定*/}
						</Button>
						<Button onClick={this.closeModal}>
							{this.state.json['arappub-000064'] /* 国际化处理：
						取消*/}
						</Button>
					</Footer>
				</Modal>
			</div>
		);
	}
}
