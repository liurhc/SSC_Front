import { Component }  from 'react';
import {  createPage, ajax, base, toast,getMultiLang} from 'nc-lightapp-front';
import { initTemplate,afterEvent } from './events';
import { formId, pageId,appcode} from './constants';


class ApCard extends Component {
    constructor(props) {
        super(props);
        this.state ={
            busireconData:null,
            status : null,
            json:{}
        }
    }

    componentDidMount() {
        this.props.onRef(this);     
    }
    
    componentWillReceiveProps(nextProps) {
        if(nextProps.status){//设置状态
            let status = nextProps.status;
            if(status != this.state.status){
                this.props.form.setFormStatus(formId, status);
                this.setState({
                    status :status
                })
            }
           
        }
        if(nextProps.busireconData && nextProps.busireconData.value){//渲染数据
            let busireconData = nextProps.busireconData;
            if(busireconData != this.state.busireconData){
                this.initialData(busireconData.value);
                this.setState({
                    busireconData:busireconData
                })
            }
        }
    }

    //传入的数据
    initialData = (datas) =>{
        let that = this;
        let data ={
            allData : datas,
            appCode :appcode,
            pageCode : pageId,
            headCode : formId
        }
        ajax({
            url: ' /nccloud/arap/glaccount/transformcard.do',
            data: data,
		success: function(res) {
            if (res.data) {
                that.props.form.setAllFormValue({ [formId]: res.data[formId] });
            }
		}
	});

    }

    getAllFiledAndValues = () => {
        let data ={
            allData : this.props.createMasterChildData(pageId,formId, null).head[formId].rows[0].values,
            appCode :appcode,
            pageCode : pageId,
            headCode : formId
        }

        let backData = null
        ajax({
            url: ' /nccloud/arap/glaccount/transformvo.do',
            data: data,
            async: false,
            success: function (res) {
                if (res.data) {
                    backData = res.data;
                }
            }
        });
        return backData; 
    }

    //表单必输项校验
    checkCondition =() =>{
        return this.props.form.isCheckNow(formId);
    }
    
    render () {
        let { form} = this.props;
        let { createForm } = form;
        return (
            <div>
                <div className="nc-bill-form-area">
                    {createForm(formId, {
                        onAfterEvent: afterEvent.bind(this)
                    })}
                </div>
            </div>
        )
    }
}

ApCard = createPage({
	initTemplate: initTemplate,
	mutiLangCode: '2052'
})(ApCard);


export default function (props = {}) {
    var conf = {
    };
    return <ApCard  {...Object.assign(conf, props)} />
}
