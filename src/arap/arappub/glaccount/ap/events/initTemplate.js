import { base, ajax } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
import { formId, pageId,appcode} from '../constants';
import {modifierFormMeta} from '../../public/referFilter.js';

export default function(props) {
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: appcode//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierFormMeta(formId,props,meta,'AP');
					props.meta.setMeta(meta);
				}
			}
		}
	)

}





