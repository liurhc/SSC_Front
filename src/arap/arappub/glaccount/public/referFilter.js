
import { getBusinessInfo } from "nc-lightapp-front";
/**
 * 收付与总账对账的参照过滤
 * @param {*} formId  
 * @param {*} props 
 * @param {*} meta 
 * @param {*} type  AR代表应收  AP代表应付
 */
function modifierFormMeta(formId, props, meta ,type){
	meta[formId].items.map((item) => {
		if(item.itemtype == 'refer'){
			item.isMultiSelectedEnabled = true;
		}else{
            return;//非参照不过滤
        }
		let attrcode = item.attrcode;
		switch(attrcode){
            case 'pk_billtype'://单据类型
                if(type == 'AR'){
                    item.queryCondition = () => {
                        return {
                            pk_billtypecode : 'F0,F2,23E0'
                        }
                    }
                }else if(type == 'AP'){
                    item.queryCondition = () => {
                        return {
                            pk_billtypecode : 'F1,F3,23E1'
                        }
                    }
                }
				break;
			case 'pk_tradetypeid'://交易类型
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_billtype : props.form.getFormItemsValue(formId, 'pk_billtype').value,
						GridRefActionExt: 'nccloud.web.arap.ref.before.TranstypeRefSqlBuilder'
					}
				}
				break;
			case 'supplier'://供应商(应付)
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.pk_org ? props.pk_org : null
					}
				}
				break;
			case 'pk_deptid'://部门
				item.isShowUnit =true,
                item.unitCondition =()=>{
                    return {
                        pkOrgs : props.pk_org ? props.pk_org : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.pk_org ? props.pk_org : null
					}
				}
				break;
			case 'pk_psndoc'://业务员
				item.isShowUnit =true,
                item.unitCondition =()=>{
                    return {
                        pkOrgs : props.pk_org ? props.pk_org : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.pk_org ? props.pk_org : null,
						pk_dept: props.form.getFormItemsValue(formId, 'pk_deptid') ? props.form.getFormItemsValue(formId, 'pk_deptid').value : null
					}
				}
				break;
			case 'material'://物料
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.pk_org ? props.pk_org : null
					};
				}
				break;
			case 'pk_subjcode'://收支项目
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.pk_org ? props.pk_org : null
					};
				}
				break;
			case 'pk_pcorg'://利润中心
                item.queryCondition = () => {
                    return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
                    };
                }
				break;
			case 'costcenter'://成本中心(根据利润中心) 
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_pcorg') ? props.form.getFormItemsValue(formId, 'pk_pcorg').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.CostCenterSqlBuilder'
                    };
                }
				break;
			case 'project'://项目
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.pk_org ? props.pk_org : null
					};
				}
				break;
			case 'cashitem'://现金流量项目
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.pk_org ? props.pk_org : null
					};
				}
				break;
			case 'bankrollprojet'://资金计划项目 
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.pk_org ? props.pk_org : null
					};
				}
				break;
			case 'pu_org'://业务组织(业务单元（财务组织委托）) (应付)
				item.queryCondition = () => {
					return {
						isDataPowerEnable: 'Y',
						pkOrgs: props.pk_org ? props.pk_org : null,
						TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
					};
				}
                break;
            case 'su_org'://业务组织(应收)
                item.queryCondition = () => {
                    return {
                        isDataPowerEnable: 'Y',
                        pkOrgs: props.pk_org ? props.pk_org : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            case 'so_deptid'://业务部门(应收)
			case 'pu_deptid'://业务部门 (应付)
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org:  props.form.getFormItemsValue(formId, 'pu_org') ? props.form.getFormItemsValue(formId, 'pu_org').value : null,
					};
				}
                break;
            case 'so_psndoc'://业务人员(应收)
			case 'pu_psndoc'://业务人员（应收）
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.form.getFormItemsValue(formId, 'pu_org') ? props.form.getFormItemsValue(formId, 'pu_org').value : null,
					};
				}
                break;
            case 'customer'://客户(应收)
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.pk_org ? props.pk_org : null
                    };

                }
                break;
            case 'checkelement'://责任核算要素
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_pcorg') ? props.form.getFormItemsValue(formId, 'pk_pcorg').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
 
                break;
            case 'h_subjcode'://会计科目表头
            case 'subjcode'://会计科目表体
                item.refcode = 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index';
                if(props.pk_org){
                    item.isAccountRefer = true; // true: 组织级科目参照，false：集团或全局政策科目
                    item.queryCondition = () => {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_accountingbook: props.pk_accountingbook,
                            pk_org :getBusinessInfo().groupId,
                            dateStr :getBusinessInfo().businessDate.split(' ')[0] 
                        }
                    }
                }else{
                    item.isAccountRefer = false;
                    item.queryCondition = () => {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_setofbook: props.pk_setofbook ,
                            pk_org :getBusinessInfo().groupId,
                            dateStr :getBusinessInfo().businessDate.split(' ')[0]
                        }
                    }
                }  
                break;
            default :
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.pk_org ? props.pk_org : null
                    }
                }
                break;

		}
		return item;
	})
	return meta;
}


export { modifierFormMeta }
