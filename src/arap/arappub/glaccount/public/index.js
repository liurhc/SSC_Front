import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, getMultiLang } from 'nc-lightapp-front';
import ApCard from '../ap';
import ArCard from '../ar';
//import ArCard from '../../ar/page';
//与总账对账测试代码文件（可删除）
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
  NCRow: Row, NCCol: Col, NCTree: Tree, NCMessage: Message, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
  NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel, NCModal: Modal, NCForm, NCButtonGroup: ButtonGroup,
} = base;

const jsons = {};
const jsons1 = {};
class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json: {}
    }
  }

  componentDidMount() {
    let callback = (json) => {
      this.setState({ json: json }, () => {
        this.madeTable();
      });
    }
    getMultiLang({ moduleId: 'arappub', domainName: 'arap', currentLocale: 'simpchn', callback });
  }

  madeTable = () => {
    const jsons = {
      "data": {
        "pageid": "200260SETG_brbase_card",
        "userjson": null,
        "head": {
          "pageid": null,
          "userjson": null,
          "gl_brsetting": {
            "areacode": "gl_brsetting",
            "rows": [
              {
                "rowid": null,
                "status": "0",
                "values": {
                  "brmap": {
                    "value": [
                      {
                        "brmap": "1001Z310000000012ZJZ",
                        "pk_brmapping": "1001Z310000000013I2J",
                        "linenum": null,
                        "accitemvalue": "0001Z0100000000005CS",
                        "pk_brrelation": "AP00Z300000000000003",
                        "pk_classid": "pk_psndoc",
                        "dr": "0",
                        "ts": "2018-07-05 00:16:59",
                        "status": "0",
                        "m_isDirty": false
                      }
                    ]
                  },
                  "qrytemplate": {
                    "value": {
                      "map": {
                        "pk_psndoc": null,
                        "pk_deptid": null,
                        "pk_subjcode": null,
                        "pk_tradetypeid": null,
                        "subjcode": null,
                        "pk_pcorg": null,
                        "material": null,
                        "productline": null,
                        "cashitem": null,
                        "project": null,
                        "pu_deptid": null,
                        "pu_psndoc": null,
                        "checkelement": null,
                        "pu_org": null,
                        "bankrollprojet": null,
                        "costcenter": null,
                        "supplier": [
                          "1001Z31000000000BNRH",
                          "1001AA1000000000BUWE"
                        ],
                        "pk_recpaytype": [
                          "GLOBZ300000000000001",
                          "GLOBZ300000000000002",
                          "GLOBZ300000000000003"
                        ],
                        "pk_billtype": [
                          "F3", "F1"
                        ]
                      },
                      "submap": {
                        "material.pk_marbasclass": null,
                        "supplier.pk_supplierclass": null
                      },
                      "billtypepks": null,
                      "anaDate": "2",
                      "pk_pcorg": null,
                      "costcenter": null,
                      "sett_org": null,
                      "pk_busitype": null,
                      "h_subjcode": null,
                      "recaccount": null,
                      "payaccount": null,
                      "cashaccount": null,
                      "project": null,
                      "project_task": null,
                      "productline": null,
                      "cashitem": null,
                      "h_def30": null,
                      "h_def29": null,
                      "h_def28": null,
                      "h_def27": null,
                      "h_def26": null,
                      "h_def25": null,
                      "h_def24": null,
                      "h_def23": null,
                      "h_def22": null,
                      "h_def21": null,
                      "h_def20": null,
                      "h_def19": null,
                      "h_def18": null,
                      "h_def17": null,
                      "h_def16": null,
                      "h_def15": null,
                      "h_def14": null,
                      "h_def13": null,
                      "h_def12": null,
                      "h_def11": null,
                      "h_def10": null,
                      "h_def9": null,
                      "h_def8": null,
                      "h_def7": null,
                      "h_def6": null,
                      "h_def5": null,
                      "h_def4": null,
                      "h_def3": null,
                      "h_def2": null,
                      "h_def1": null,
                      "so_org": null,
                      "so_deptid": null,
                      "so_psndoc": null,
                      "so_ordertype": null,
                      "so_transtype": null,
                      "pk_recterm": null,
                      "outstoreno": null,
                      "pu_org": null,
                      "pu_deptid": null,
                      "pu_psndoc": null,
                      "pk_payterm": null,
                      "instoreno": null,
                      "equipmentcode": null,
                      "facard": null,
                      "innerorderno": null,
                      "checkelement": null,
                      "bankrollprojet": null,
                      "checktype": null,
                      "src_billtype": null,
                      "src_tradetype": null,
                      "freecust": null,
                      "contractno": null,
                      "purchaseorder": null,
                      "matcustcode": null,
                      "h_def31": null,
                      "h_def32": null,
                      "h_def33": null,
                      "h_def34": null,
                      "h_def35": null,
                      "h_def36": null,
                      "h_def37": null,
                      "h_def38": null,
                      "h_def39": null,
                      "h_def40": null,
                      "h_def41": null,
                      "h_def42": null,
                      "h_def43": null,
                      "h_def44": null,
                      "h_def45": null,
                      "h_def46": null,
                      "h_def47": null,
                      "h_def48": null,
                      "h_def49": null,
                      "h_def50": null,
                      "h_def51": null,
                      "h_def52": null,
                      "h_def53": null,
                      "h_def54": null,
                      "h_def55": null,
                      "h_def56": null,
                      "h_def57": null,
                      "h_def58": null,
                      "h_def59": null,
                      "h_def60": null,
                      "h_def61": null,
                      "h_def62": null,
                      "h_def63": null,
                      "h_def64": null,
                      "h_def65": null,
                      "h_def66": null,
                      "h_def67": null,
                      "h_def68": null,
                      "h_def69": null,
                      "h_def70": null,
                      "h_def71": null,
                      "h_def72": null,
                      "h_def73": null,
                      "h_def74": null,
                      "h_def75": null,
                      "h_def76": null,
                      "h_def77": null,
                      "h_def78": null,
                      "h_def79": null,
                      "h_def80": null,
                      "pk_currtype": null,
                      "pk_orgs": null,
                      "brief": null,
                      "objtype": "1",
                      "customer": null,
                      "supplier": null,
                      "pk_deptid": null,
                      "pk_psndoc": null,
                      "billclass": null,
                      "pk_billtype": [
                        "F3"
                      ],
                      "pk_tradetype": null,
                      "pk_tradetypeid": null,
                      "material": null,
                      "pk_subjcode": null,
                      "subjcode": null,
                      "ordercubasdoc": null,
                      "prepay": null,
                      "effectstatus": null,
                      "billstatus": null,
                      "def1": null,
                      "def2": null,
                      "def3": null,
                      "def4": null,
                      "def5": null,
                      "def6": null,
                      "def7": null,
                      "def8": null,
                      "def9": null,
                      "def10": null,
                      "def11": null,
                      "def12": null,
                      "def13": null,
                      "def14": null,
                      "def15": null,
                      "def16": null,
                      "def17": null,
                      "def18": null,
                      "def19": null,
                      "def20": null,
                      "def21": null,
                      "def22": null,
                      "def23": null,
                      "def24": null,
                      "def25": null,
                      "def26": null,
                      "def27": null,
                      "def28": null,
                      "def29": null,
                      "def30": null,
                      "pk_recpaytype": null,
                      "def31": null,
                      "def32": null,
                      "def33": null,
                      "def34": null,
                      "def35": null,
                      "def36": null,
                      "def37": null,
                      "def38": null,
                      "def39": null,
                      "def40": null,
                      "def41": null,
                      "def42": null,
                      "def43": null,
                      "def44": null,
                      "def45": null,
                      "def46": null,
                      "def47": null,
                      "def48": null,
                      "def49": null,
                      "def50": null,
                      "def51": null,
                      "def52": null,
                      "def53": null,
                      "def54": null,
                      "def55": null,
                      "def56": null,
                      "def57": null,
                      "def58": null,
                      "def59": null,
                      "def60": null,
                      "def61": null,
                      "def62": null,
                      "def63": null,
                      "def64": null,
                      "def65": null,
                      "def66": null,
                      "def67": null,
                      "def68": null,
                      "def69": null,
                      "def70": null,
                      "def71": null,
                      "def72": null,
                      "def73": null,
                      "def74": null,
                      "def75": null,
                      "def76": null,
                      "def77": null,
                      "def78": null,
                      "def79": null,
                      "def80": null,
                      "status": "0",
                      "m_isDirty": false
                    }
                  },
                  "pk_unit": {},
                  "ts": {
                    "value": "2018-07-05 00:16:59"
                  },
                  "pk_unit_v": {},
                  "braccasoas": {
                    "value": [
                      {
                        "braccasoas": "1001Z310000000012ZJZ",
                        "pk_braccasoa": "1001Z310000000012ZK0",
                        "pk_accasoa": "1001Z31000000000OVUF",
                        "dr": "0",
                        "ts": "2018-07-04 14:29:30",
                        "status": "0",
                        "m_isDirty": false
                      },
                      {
                        "braccasoas": "1001Z310000000012ZJZ",
                        "pk_braccasoa": "1001Z310000000012ZK1",
                        "pk_accasoa": "1001Z31000000000OVUH",
                        "dr": "0",
                        "ts": "2018-07-04 14:29:30",
                        "status": "0",
                        "m_isDirty": false
                      },
                      {
                        "braccasoas": "1001Z310000000012ZJZ",
                        "pk_braccasoa": "1001Z310000000012ZK2",
                        "pk_accasoa": "0001Z0100000000001A0",
                        "dr": "0",
                        "ts": "2018-07-04 14:29:30",
                        "status": "0",
                        "m_isDirty": false
                      },
                      {
                        "braccasoas": "1001Z310000000012ZJZ",
                        "pk_braccasoa": "1001Z310000000013I2K",
                        "pk_accasoa": "0001Z0100000000001A2",
                        "dr": "0",
                        "ts": "2018-07-05 00:16:59",
                        "status": "0",
                        "m_isDirty": false
                      }
                    ]
                  },
                  "type_end_ocr": {
                    "value": false
                  },
                  "creatorg": {},
                  "desp": {},
                  "con_locamount": {
                    "value": false
                  },
                  "creator": {},
                  "glnodecode": {},
                  "type_balance": {
                    "value": true
                  },
                  "pk_currtype": {},
                  "pk_org": {
                    "value": "0001A3100000000002FJ",
                    "display": this.state.json['arappub-000041']/* 国际化处理： 用友集团*/
                  },
                  "name": {
                    "value": "009"
                  },
                  "pk_defdoc": {
                    "value": "aaaaZ33aaaaaaaaaaaa2"
                  },
                  "pk_brsetting": {
                    "value": "1001Z310000000012ZJZ"
                  },
                  "pk_setofbook": {
                    "value": "0001Z01000000000019Y",
                    "display": this.state.json['arappub-000042']/* 国际化处理： 基准账簿*/
                  },
                  "con_amount": {
                    "value": true
                  },
                  "dr": {
                    "value": "0"
                  },
                  "balanceori": {
                    "value": "1",
                    "display": this.state.json['arappub-000043']/* 国际化处理： 贷*/
                  },
                  "status": {
                    "value": "0"
                  },
                  "closeaccctrltype": {
                    "value": "1",
                    "display": this.state.json['arappub-000044']/* 国际化处理： 不检查*/
                  },
                  "code": {
                    "value": "009"
                  },
                  "name5": {},
                  "creationtime": {},
                  "modifier": {
                    "value": "1001A310000000000006"
                  },
                  "name6": {},
                  "name3": {},
                  "name4": {},
                  "type_endbal": {
                    "value": false
                  },
                  "con_quantity": {
                    "value": false
                  },
                  "pk_accountingbook": {},
                  "modifiedtime": {
                    "value": "2018-07-05 00:16:59"
                  },
                  "pk_group": {
                    "value": "0001A3100000000002FJ",
                    "display": this.state.json['arappub-000041']/* 国际化处理： 用友集团*/
                  },
                  "name2": {}
                }
              }
            ]
          }
        },
        "bodys": {
          "brmap": {
            "areacode": "brmap",
            "rows": [
              {
                "rowid": null,
                "status": "0",
                "values": {
                  "brmap": {
                    "value": "1001Z310000000012ZJZ"
                  },
                  "pk_classid": {
                    "value": "pk_psndoc"
                  },
                  "ts": {
                    "value": "2018-07-05 00:16:59"
                  },
                  "status": {
                    "value": "0"
                  },
                  "pk_brrelation": {
                    "value": "AP00Z300000000000003",
                    "display": this.state.json['arappub-000045']/* 国际化处理： 人员*/
                  },
                  "pk_brmapping": {
                    "value": "1001Z310000000013I2J"
                  },
                  "linenum": {},
                  "accitemvalue": {
                    "value": "0001Z0100000000005CS",
                    "display": this.state.json['arappub-000046']/* 国际化处理： 部门*/
                  },
                  "dr": {
                    "value": "0"
                  }
                }
              }
            ]
          },
          "braccasoa": {
            "areacode": "braccasoa",
            "rows": [
              {
                "rowid": null,
                "status": "0",
                "values": {
                  "pk_braccasoa": {
                    "value": "1001Z310000000012ZK0"
                  },
                  "ts": {
                    "value": "2018-07-04 14:29:30"
                  },
                  "status": {
                    "value": "0"
                  },
                  "braccasoas": {
                    "value": "1001Z310000000012ZJZ"
                  },
                  "pk_accasoa": {
                    "value": "1001Z31000000000OVUF",
                    "display": this.state.json['arappub-000047']/* 国际化处理： 1001\\库存现金*/
                  },
                  "dr": {
                    "value": "0"
                  }
                }
              },
              {
                "rowid": null,
                "status": "0",
                "values": {
                  "pk_braccasoa": {
                    "value": "1001Z310000000012ZK1"
                  },
                  "ts": {
                    "value": "2018-07-04 14:29:30"
                  },
                  "status": {
                    "value": "0"
                  },
                  "braccasoas": {
                    "value": "1001Z310000000012ZJZ"
                  },
                  "pk_accasoa": {
                    "value": "1001Z31000000000OVUH",
                    "display": this.state.json['arappub-000048']/* 国际化处理： 1002\\银行存款*/
                  },
                  "dr": {
                    "value": "0"
                  }
                }
              },
              {
                "rowid": null,
                "status": "0",
                "values": {
                  "pk_braccasoa": {
                    "value": "1001Z310000000012ZK2"
                  },
                  "ts": {
                    "value": "2018-07-04 14:29:30"
                  },
                  "status": {
                    "value": "0"
                  },
                  "braccasoas": {
                    "value": "1001Z310000000012ZJZ"
                  },
                  "pk_accasoa": {
                    "value": "0001Z0100000000001A0",
                    "display": this.state.json['arappub-000049']/* 国际化处理： 100201\\银行存款\\中国银行*/
                  },
                  "dr": {
                    "value": "0"
                  }
                }
              },
              {
                "rowid": null,
                "status": "0",
                "values": {
                  "pk_braccasoa": {
                    "value": "1001Z310000000013I2K"
                  },
                  "ts": {
                    "value": "2018-07-05 00:16:59"
                  },
                  "status": {
                    "value": "0"
                  },
                  "braccasoas": {
                    "value": "1001Z310000000012ZJZ"
                  },
                  "pk_accasoa": {
                    "value": "0001Z0100000000001A2",
                    "display": this.state.json['arappub-000050']/* 国际化处理： 1011\\存放同业*/
                  },
                  "dr": {
                    "value": "0"
                  }
                }
              }
            ]
          }
        }
      },
      "error": null,
      "formulamsg": null,
      "success": true
    };
    const jsons1 = {
      "data": {
        "pageid": "200260SETG_brbase_card",
        "templetid": null,
        "userjson": null,
        "head": {
          "pageid": null,
          "templetid": null,
          "userjson": null,
          "gl_brsetting": {
            "areacode": "gl_brsetting",
            "rows": [
              {
                "rowid": null,
                "status": "0",
                "values": {
                  "brmap": {
                    "value": [
                      {
                        "brmap": "1001Z310000000014BE3",
                        "pk_brmapping": "1001Z310000000014BE4",
                        "linenum": null,
                        "accitemvalue": "0001Z0100000000005CS",
                        "pk_brrelation": "AR00Z300000000000001",
                        "pk_classid": "pk_deptid",
                        "dr": "0",
                        "ts": "2018-07-06 10:03:03",
                        "status": "0",
                        "m_isDirty": false
                      },
                      {
                        "brmap": "1001Z310000000014BE3",
                        "pk_brmapping": "1001Z310000000014BE5",
                        "linenum": null,
                        "accitemvalue": "0001Z0100000000005CT",
                        "pk_brrelation": "AR00Z300000000000003",
                        "pk_classid": "pk_psndoc",
                        "dr": "0",
                        "ts": "2018-07-06 10:03:03",
                        "status": "0",
                        "m_isDirty": false
                      }
                    ]
                  },
                  "qrytemplate": {
                    "value": {
                      "map": {
                        "so_transtype": null,
                        "pk_psndoc": null,
                        "pk_deptid": null,
                        "pk_subjcode": null,
                        "pk_tradetypeid": null,
                        "so_deptid": null,
                        "subjcode": null,
                        "pk_pcorg": null,
                        "so_ordertype": null,
                        "customer": null,
                        "material": null,
                        "productline": null,
                        "cashitem": null,
                        "project": null,
                        "checkelement": null,
                        "bankrollprojet": null,
                        "costcenter": null,
                        "so_org": null,
                        "pk_recpaytype": null,
                        "so_psndoc": null,
                        "pk_billtype": [
                          "23E0"
                        ]
                      },
                      "submap": {
                        "material.pk_marbasclass": null,
                        "customer.pk_custclass": null
                      },
                      "billtypepks": [
                        "0000Z3000000000023E0"
                      ],
                      "anaDate": "1",
                      "pk_pcorg": null,
                      "costcenter": null,
                      "sett_org": null,
                      "pk_busitype": null,
                      "h_subjcode": null,
                      "recaccount": null,
                      "payaccount": null,
                      "cashaccount": null,
                      "project": null,
                      "project_task": null,
                      "productline": null,
                      "cashitem": null,
                      "h_def30": null,
                      "h_def29": null,
                      "h_def28": null,
                      "h_def27": null,
                      "h_def26": null,
                      "h_def25": null,
                      "h_def24": null,
                      "h_def23": null,
                      "h_def22": null,
                      "h_def21": null,
                      "h_def20": null,
                      "h_def19": null,
                      "h_def18": null,
                      "h_def17": null,
                      "h_def16": null,
                      "h_def15": null,
                      "h_def14": null,
                      "h_def13": null,
                      "h_def12": null,
                      "h_def11": null,
                      "h_def10": null,
                      "h_def9": null,
                      "h_def8": null,
                      "h_def7": null,
                      "h_def6": null,
                      "h_def5": null,
                      "h_def4": null,
                      "h_def3": null,
                      "h_def2": null,
                      "h_def1": null,
                      "so_org": null,
                      "so_deptid": null,
                      "so_psndoc": null,
                      "so_ordertype": null,
                      "so_transtype": null,
                      "pk_recterm": null,
                      "outstoreno": null,
                      "pu_org": null,
                      "pu_deptid": null,
                      "pu_psndoc": null,
                      "pk_payterm": null,
                      "instoreno": null,
                      "equipmentcode": null,
                      "facard": null,
                      "innerorderno": null,
                      "checkelement": null,
                      "bankrollprojet": null,
                      "checktype": null,
                      "src_billtype": null,
                      "src_tradetype": null,
                      "freecust": null,
                      "contractno": null,
                      "purchaseorder": null,
                      "matcustcode": null,
                      "h_def31": null,
                      "h_def32": null,
                      "h_def33": null,
                      "h_def34": null,
                      "h_def35": null,
                      "h_def36": null,
                      "h_def37": null,
                      "h_def38": null,
                      "h_def39": null,
                      "h_def40": null,
                      "h_def41": null,
                      "h_def42": null,
                      "h_def43": null,
                      "h_def44": null,
                      "h_def45": null,
                      "h_def46": null,
                      "h_def47": null,
                      "h_def48": null,
                      "h_def49": null,
                      "h_def50": null,
                      "h_def51": null,
                      "h_def52": null,
                      "h_def53": null,
                      "h_def54": null,
                      "h_def55": null,
                      "h_def56": null,
                      "h_def57": null,
                      "h_def58": null,
                      "h_def59": null,
                      "h_def60": null,
                      "h_def61": null,
                      "h_def62": null,
                      "h_def63": null,
                      "h_def64": null,
                      "h_def65": null,
                      "h_def66": null,
                      "h_def67": null,
                      "h_def68": null,
                      "h_def69": null,
                      "h_def70": null,
                      "h_def71": null,
                      "h_def72": null,
                      "h_def73": null,
                      "h_def74": null,
                      "h_def75": null,
                      "h_def76": null,
                      "h_def77": null,
                      "h_def78": null,
                      "h_def79": null,
                      "h_def80": null,
                      "pk_currtype": null,
                      "pk_orgs": null,
                      "brief": null,
                      "objtype": "0",
                      "customer": null,
                      "supplier": null,
                      "pk_deptid": null,
                      "pk_psndoc": null,
                      "billclass": null,
                      "pk_billtype": [
                        "23E0"
                      ],
                      "pk_tradetype": null,
                      "pk_tradetypeid": null,
                      "material": null,
                      "pk_subjcode": null,
                      "subjcode": null,
                      "ordercubasdoc": null,
                      "prepay": null,
                      "effectstatus": null,
                      "billstatus": null,
                      "def1": null,
                      "def2": null,
                      "def3": null,
                      "def4": null,
                      "def5": null,
                      "def6": null,
                      "def7": null,
                      "def8": null,
                      "def9": null,
                      "def10": null,
                      "def11": null,
                      "def12": null,
                      "def13": null,
                      "def14": null,
                      "def15": null,
                      "def16": null,
                      "def17": null,
                      "def18": null,
                      "def19": null,
                      "def20": null,
                      "def21": null,
                      "def22": null,
                      "def23": null,
                      "def24": null,
                      "def25": null,
                      "def26": null,
                      "def27": null,
                      "def28": null,
                      "def29": null,
                      "def30": null,
                      "pk_recpaytype": null,
                      "def31": null,
                      "def32": null,
                      "def33": null,
                      "def34": null,
                      "def35": null,
                      "def36": null,
                      "def37": null,
                      "def38": null,
                      "def39": null,
                      "def40": null,
                      "def41": null,
                      "def42": null,
                      "def43": null,
                      "def44": null,
                      "def45": null,
                      "def46": null,
                      "def47": null,
                      "def48": null,
                      "def49": null,
                      "def50": null,
                      "def51": null,
                      "def52": null,
                      "def53": null,
                      "def54": null,
                      "def55": null,
                      "def56": null,
                      "def57": null,
                      "def58": null,
                      "def59": null,
                      "def60": null,
                      "def61": null,
                      "def62": null,
                      "def63": null,
                      "def64": null,
                      "def65": null,
                      "def66": null,
                      "def67": null,
                      "def68": null,
                      "def69": null,
                      "def70": null,
                      "def71": null,
                      "def72": null,
                      "def73": null,
                      "def74": null,
                      "def75": null,
                      "def76": null,
                      "def77": null,
                      "def78": null,
                      "def79": null,
                      "def80": null,
                      "status": "0",
                      "m_isDirty": false
                    }
                  },
                  "pk_unit": {},
                  "ts": {
                    "value": "2018-07-09 15:08:16"
                  },
                  "pk_unit_v": {},
                  "braccasoas": {
                    "value": [
                      {
                        "braccasoas": "1001Z310000000014BE3",
                        "pk_braccasoa": "1001Z310000000014BE6",
                        "pk_accasoa": "0001Z0100000000001A1",
                        "dr": "0",
                        "ts": "2018-07-06 10:03:03",
                        "status": "0",
                        "m_isDirty": false
                      }
                    ]
                  },
                  "type_end_ocr": {
                    "value": false
                  },
                  "creatorg": {},
                  "desp": {},
                  "con_locamount": {
                    "value": true
                  },
                  "creator": {
                    "value": "1001A310000000000006"
                  },
                  "glnodecode": {},
                  "type_balance": {
                    "value": true
                  },
                  "pk_currtype": {},
                  "pk_org": {
                    "value": "0001A3100000000002FJ",
                    "display": this.state.json['arappub-000041']/* 国际化处理： 用友集团*/
                  },
                  "name": {
                    "value": "sdf"
                  },
                  "pk_defdoc": {
                    "value": "aaaaZ33aaaaaaaaaaaa1",
                    "display": this.state.json['arappub-000051']/* 国际化处理： 应收管理*/
                  },
                  "pk_brsetting": {
                    "value": "1001Z310000000014BE3"
                  },
                  "pk_setofbook": {
                    "value": "0001Z01000000000019Y",
                    "display": this.state.json['arappub-000042']/* 国际化处理： 基准账簿*/
                  },
                  "con_amount": {
                    "value": true
                  },
                  "dr": {
                    "value": "0"
                  },
                  "balanceori": {
                    "value": "0",
                    "display": this.state.json['arappub-000052']/* 国际化处理： 借*/
                  },
                  "status": {
                    "value": "0"
                  },
                  "closeaccctrltype": {
                    "value": "1",
                    "display": this.state.json['arappub-000044']/* 国际化处理： 不检查*/
                  },
                  "code": {
                    "value": "sdf11"
                  },
                  "name5": {},
                  "creationtime": {
                    "value": "2018-07-06 10:03:02"
                  },
                  "modifier": {
                    "value": "1001A310000000000006"
                  },
                  "name6": {},
                  "name3": {},
                  "name4": {},
                  "type_endbal": {
                    "value": false
                  },
                  "con_quantity": {
                    "value": true
                  },
                  "pk_accountingbook": {},
                  "modifiedtime": {
                    "value": "2018-07-09 15:08:16"
                  },
                  "pk_group": {
                    "value": "0001A3100000000002FJ",
                    "display": this.state.json['arappub-000041']/* 国际化处理： 用友集团*/
                  },
                  "name2": {}
                }
              }
            ]
          }
        },
        "bodys": {
          "brmap": {
            "areacode": "brmap",
            "rows": [
              {
                "rowid": null,
                "status": "0",
                "values": {
                  "brmap": {
                    "value": "1001Z310000000014BE3"
                  },
                  "pk_classid": {
                    "value": "pk_deptid"
                  },
                  "ts": {
                    "value": "2018-07-06 10:03:03"
                  },
                  "status": {
                    "value": "0"
                  },
                  "pk_brrelation": {
                    "value": "AR00Z300000000000001",
                    "display": this.state.json['arappub-000046']/* 国际化处理： 部门*/
                  },
                  "pk_brmapping": {
                    "value": "1001Z310000000014BE4"
                  },
                  "linenum": {},
                  "accitemvalue": {
                    "value": "0001Z0100000000005CS",
                    "display": this.state.json['arappub-000046']/* 国际化处理： 部门*/
                  },
                  "dr": {
                    "value": "0"
                  }
                }
              },
              {
                "rowid": null,
                "status": "0",
                "values": {
                  "brmap": {
                    "value": "1001Z310000000014BE3"
                  },
                  "pk_classid": {
                    "value": "pk_psndoc"
                  },
                  "ts": {
                    "value": "2018-07-06 10:03:03"
                  },
                  "status": {
                    "value": "0"
                  },
                  "pk_brrelation": {
                    "value": "AR00Z300000000000003",
                    "display": this.state.json['arappub-000045']/* 国际化处理： 人员*/
                  },
                  "pk_brmapping": {
                    "value": "1001Z310000000014BE5"
                  },
                  "linenum": {},
                  "accitemvalue": {
                    "value": "0001Z0100000000005CT",
                    "display": this.state.json['arappub-000053']/* 国际化处理： 人员档案*/
                  },
                  "dr": {
                    "value": "0"
                  }
                }
              }
            ]
          },
          "braccasoa": {
            "areacode": "braccasoa",
            "rows": [
              {
                "rowid": null,
                "status": "0",
                "values": {
                  "pk_braccasoa": {
                    "value": "1001Z310000000014BE6"
                  },
                  "ts": {
                    "value": "2018-07-06 10:03:03"
                  },
                  "status": {
                    "value": "0"
                  },
                  "braccasoas": {
                    "value": "1001Z310000000014BE3"
                  },
                  "pk_accasoa": {
                    "value": "0001Z0100000000001A1",
                    "display": this.state.json['arappub-000054']/* 国际化处理： 1003\\存放中央银行款项*/
                  },
                  "dr": {
                    "value": "0"
                  }
                }
              }
            ]
          }
        }
      },
      "error": null,
      "formulamsg": null,
      "success": true
    };
  }
  getAllData = () => {
    let self = this;

  }
  check = () => {
    let self = this;
  }

  onRef = (ref) => {
    this.child = ref
  }


  render() {
    if(!jsons1.data || (!jsons.data)){
      return;
    }
    return (
      <div>
        <ApCard
          onRef={this.onRef}
          status='edit'
          busireconData={jsons1.data.head.gl_brsetting.rows[0].values.qrytemplate}
          pk_org={'0001A310000000000NN6'}
          pk_accountingbook={'1001A3100000000000PE'}
          pk_setofbook={'0001Z01000000000019Y'}
        />
        {/* <ArCard 
                    onRef={this.onRef}
                    status ='edit'
                    data ={jsons}
                /> */}

        <Button
          colors="primary"
          size="lg"
          className="demo-margin"
          onClick={this.getAllData.bind(this)}>
          {this.state.json['arappub-000055']}{/* 国际化处理： 获取界面*/}
        </Button>
        <Button
          colors="primary"
          size="lg"
          className="demo-margin"
          onClick={this.check.bind(this)}>
          {this.state.json['arappub-000056']}{/* 国际化处理： 检查必输项*/}
        </Button>


        {/* <ArCard 
                    onRef={this.onRef}
                    status ='edit'
                    busireconData  ={jsons.data.head.gl_brsetting.rows[0].values.qrytemplate}
                /> */}
      </div>
    )
  }
}


ReactDOM.render(<List />, document.querySelector('#app'));
