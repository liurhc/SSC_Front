import { ajax, base, toast,cardCache } from 'nc-lightapp-front';
import { tableId, formId, leftarea, dataSource } from '../constants';
let { setDefData, getDefData, addCache, deleteCacheById, getCacheById, updateCache } = cardCache;

export default function transferButtonClick(props, id) {
	switch (id) {
		case 'Save':
			{
				if (!this.props.form.isCheckNow(this.formId)) {//表单验证
					return;
				}
				if (!this.props.cardTable.checkTableRequired(this.tableId)) {//表格验证
					return;
				}
				let url;
				if (this.props.getUrlParam('status') === "add") {
					url = '/nccloud/arap/arappub/releasetoarapsave.do'
				} else {
					url = '/nccloud/arap/arappub/save.do'
				}
				let data = {
					cardData: this.props.createMasterChildData(this.pagecode, this.formId, this.tableId),
					billType: this.billType,
					uiState: this.props.getUrlParam('status')
				}
				ajax({
					url: url,
					data: data,
					success: (res) => {
						if (res.success) {
							setValue.call(this, props, res)
						}
					}
				})
			}
			break;
		case 'Cancel':
			{
				if(this.props.getUrlParam('id')){
					this.props.setUrlParam({ status: 'browse' });
					let id = this.props.getUrlParam('id');
					let cardData = getCacheById(id, dataSource);
					if (cardData) {
						this.props.beforeUpdatePage();//打开开关
						this.props.form.EmptyAllFormValue(this.formId);
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
						this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
						this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
						this.props.cardTable.setStatus(this.tableId, 'browse');
						this.props.form.setFormStatus(this.formId, 'browse');
						this.props.updatePage(this.formId, this.tableId);//关闭开关
						this.toggleShow();
					}

				}else {
					// props.transferTable.setTransformFormStatus(leftarea, {
					// 	status: false,
					// 	onChange: (current, next) => {
					// 		// toast({ color: 'success', content: '取消成功' });
					// 	}
					// })
					this.props.setUrlParam({ status: 'browse' })
					this.toggleShow()
				}
			}
			break;
		case 'Edit':
			if (this.props.getUrlParam('id') && this.props.form.getFormItemsValue(this.formId, this.pkName).value) {
				ajax({
					url: '/nccloud/arap/arappub/edit.do',
					data: {
						pk_bill: this.props.getUrlParam('id'),
						billType: this.billType
					},
					success: (res) => {
						if (res.success) {
							props.setUrlParam({ status: 'edit' })
							this.toggleShow()
						}
					}
				})
			} else {
				props.setUrlParam({ status: 'add' })
				this.toggleShow()
			}
			break;
		case 'Quit':
			this.props.linkTo('/cmp/informerrelease/SscRelease/list/index.html', {
				appcode: '36070AIPSSC',
				pagecode: '36070AIPSSC_L01',
			})
			break;
		default:
			break;
	}
}

export function setValue(props, res) {
	if (res.data.head && res.data.head[formId]) {
		props.form.setAllFormValue({ [formId]: res.data.head[formId] });
	}
	if (res.data.body && res.data.body[tableId]) {
		if(props.getUrlParam('status') === "add"){
			props.cardTable.setTableData(tableId, res.data.body[tableId]);
		}else{
			props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
		}
	}
	let pk_bill = this.props.form.getFormItemsValue(this.formId, this.pkName).value
	let newCardData = this.props.createMasterChildData(
		this.pagecode, this.formId, this.tableId
	);
	if (props.getUrlParam('status') == 'add') {
		addCache(pk_bill, newCardData, formId, dataSource)
	}else {
		updateCache(this.pkname, pk_bill, newCardData, formId, dataSource);
	}
	props.transferTable.setTransformFormStatus(leftarea, {
		status: true,
		// onChange: (current, next, currentIndex) => {
		// 	toast({ color: 'success', content: this.state.json['arappub-000002'] });/* 国际化处理： 操作成功*/
		// 	this.transferIndex = currentIndex + 1;
		// 	this.isNeedSelect = true
		// 	props.transferTable.setTransferListValueByIndex(
		// 		leftarea,
		// 		newCardData,
		// 		currentIndex
		// 	)
		// }
	})
}

