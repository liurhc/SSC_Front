import { ajax, getBusinessInfo, promptBox } from 'nc-lightapp-front';
import { currentTypeAfterFormEvents } from '../../../../public/components/pubUtils/currentTypeAfterEvent';
import { getColvalues, getRowIds } from '../../../../public/components/pubUtils/billPubUtil';
import {
	checknoDisplayAfterEvent,
	checktypeAfterEvent
} from '../../../../public/components/pubUtils/specialFieldAfterEvent.js';
import { formulamsgHint, renderData, headAfterEventRenderData, bodyAfterEventRenderData, errorDeal } from '../../../../public/components/afterEventPub/afterEventPubDeal';
import { moneyAndRateFields } from '../../../../public/components/pubUtils/specialFieldAfterEvent.js';

export default function afterEvent(props, moduleId, key, value, changedrows, i, record, g) {
	//如果当前输入值和上一次值相同，则直接返回，参照字段和非参照字段参数不同
	if (changedrows instanceof Array) {
		if (changedrows[0].newvalue.value == changedrows[0].oldvalue.value) {
			return;
		}
	}
	let index = 0;
	if (moduleId == this.formId) {
		index = 0;
	} else if (moduleId == this.tableId) {
		index = i;
	}
	let pagecode =this.pagecode;

	//表头编辑后事件
	if (moduleId == this.formId) {
		let data = {
			pageId: this.pagecode,
			event: this.props.createHeadAfterEventData(this.pagecode, this.formId, this.tableId, moduleId, key, value),
			uiState: this.props.getUrlParam('status')
		}
		let url;
		if (this.billType === "F2") {
			url = '/nccloud/arap/gatheringbill/cardheadafteredit.do'
		} else if (this.billType === "F3") {
			url = '/nccloud/arap/paybill/cardheadafteredit.do'
		}
		switch (key) {
			case 'customer':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'payaccount', 'pk_currtype', 'customer', 'buysellflag',
						'objtype', "direction", "pk_billtype", "top_billtype"].concat(moneyAndRateFields)),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows, url)
				break;
			case 'pk_deptid_v':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_deptid', 'pk_deptid_v', 'pk_currtype', 'customer', 'taxtype', 'buysellflag', 'objtype', "direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows, url)
				break;
			case 'pk_psndoc':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_psndoc', 'pk_deptid', 'pk_deptid_v', 'isrefused', 'prepay', 'isdiscount', 'objtype', 'direction', 'agentreceivelocal']),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows, url)
				break;
			case 'pk_currtype':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ["pk_org", "pk_group", "pk_currtype", "pk_billtype", "billdate", "rate",
						"buysellflag", "taxprice", "local_taxprice", "taxrate", "occupationmny", "money_bal", "local_money_bal",
						"globalcrebit", "globalnotax_cre", "globaltax_cre", "groupcrebit", "groupnotax_cre", "grouptax_cre",
						"local_money_cr", "local_notax_cr", "local_tax_cr", "money_cr", "notax_cr", "quantity_cr"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows, url)
				//币种事件发送完要判断汇率
				currentTypeAfterFormEvents(this.formId, props, key);
				break;
			default:
				data = {
					pageId: pagecode,
					event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows, url)
				break;

		}
	}

	//表头币种编辑后事件
	if (moduleId == this.formId && key == "pk_currtype") {
		currentTypeAfterFormEvents(this.formId, props, key);
	}


	//表体编辑后事件
	if (moduleId == this.tableId) {
		//票据类型
		if (key == 'checktype') {
			checktypeAfterEvent(this.props, this.tableId, key, value, i);
		}
		//非元数据字段，票据号
		if (key == 'checkno_display') {
			checknoDisplayAfterEvent(this.props, this.tableId, key, value, i);
			key = 'checkno';
		}
		let url;
		if (this.billType === "F2") {
			url = '/nccloud/arap/gatheringbill/cardbodyafteredit.do'
		} else if (this.billType === "F3") {
			url = '/nccloud/arap/paybill/cardbodyafteredit.do'
		}

		ajax({
			url: url,
			data: {
				rowindex: 0,
				editindex: index,
				pageId: this.pagecode,
				tableId: this.tableId,
				changedrows: changedrows,
				body: props.cardTable.getDataByIndex(this.tableId, index),
				formEvent: props.createFormAfterEventData(this.pagecode, this.formId, this.tableId, key, value),
				uiState: this.props.getUrlParam('status')
			},
			async: false,
			success: (res) => {
				//渲染数据
				bodyAfterEventRenderData(this, res);

				if (this.props.getUrlParam('type') == 'transfer') {
					this.synTransferData();
				}
				//表体改变表头税率编辑性
				if (i == 0 && key == 'pk_currtype') {
					currentTypeAfterFormEvents(this.formId, props, 'pk_currtype');
				}
				//编辑公式提示
				formulamsgHint(this, res);
			},
			error: (res) => {
				let str = String(res);
				let content = str.substring(6, str.length);
				if (content.substring(1, 17) == 'convertException') {
					promptBox({
						color: 'warning',
						title: this.state.json['gatheringbill-000000'] /* 国际化处理： 折算误差*/,
						content: content.substring(17, content.length),
						closeByClickBackDrop: false,
						beSureBtnClick: () => {
							afterTableEvent(this, props, i, this.pagecode, moduleId, key, changedrows, 'sure', index, value, url);
						},
						cancelBtnClick: () => {
							afterTableEvent(this, props, i, this.pagecode, moduleId, key, changedrows, 'cancel', index, value, url);
						}
					});
				} else {
					this.props.cardTable.setValByKeyAndRowId(this.tableId, i, key, changedrows);
					toast({ color: 'danger', content: content });
				}
			}
		});

	}

}
export function headFieldAfterRequest(requestData, key, changedrows, url) {
	ajax({
		url: url,
		data: requestData,
		async: false,
		success: (res) => {
			//渲染数据
			headAfterEventRenderData(this, res);

			if (key == 'pk_org') {
				let pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
				if (pk_org) {
					this.props.resMetaAfterPkorgEdit();
					this.state.buttonfalg = true;
				} else {
					this.state.buttonfalg = null;
				}
				this.toggleShow();
			}
			//编辑公式提示
			formulamsgHint(this, res);

		},
		error: (res) => {
			errorDeal(this, res, changedrows);
		}
	});
}
function afterTableEvent(that, props, i, pagecode, moduleId, key, changedrows, isCalculateConvert, index, value, url) {
	ajax({
		url: url,
		data: {
			rowindex: 0,
			editindex: index,
			pageId: pagecode,
			changedrows: changedrows,
			tableId: that.tableId,
			body: props.cardTable.getDataByIndex(that.tableId, index),
			formEvent: props.createFormAfterEventData(pagecode, that.formId, that.tableId, key, value),
			uiState: that.props.getUrlParam('status'),
			isCalculateConvert: isCalculateConvert
		},
		async: false,
		success: (res) => {
			//渲染数据
			bodyAfterEventRenderData(that, res);
			if (that.props.getUrlParam('type') == 'transfer') {
				that.synTransferData();
			}
			//表体改变表头税率编辑性
			if (i == 0 && key == 'pk_currtype') {
				currentTypeAfterFormEvents(that.formId, props, 'pk_currtype');
			}
			//编辑公式提示
			formulamsgHint(that, res);

		}
	});
}