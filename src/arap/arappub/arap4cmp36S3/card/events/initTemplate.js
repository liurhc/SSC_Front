import { ajax, base, toast } from 'nc-lightapp-front';
import { tableId, formId, leftarea } from '../constants';

export default function (props, callback) {
	let that = this;
	props.createUIDom(
		{
			appcode: this.appcode,
			pagecode: this.pagecode
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta,that);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('Delete_inner', that.state.json['arappub-000001']);/* 国际化处理： 确认删除？*/
				}
				if(callback){
					callback();
				}
			}
		}
	)
}


export function modifierMeta(props, meta,that) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;

		return item;
	})
	//添加操作列
	meta[tableId].items.push({
		label: that.state.json['arappub-000000'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: '100px',
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let buttonAry = ["Delete_inner"];
			return props.button.createOprationButton(buttonAry, {
				area: "card_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(that, props, key, text, record, index)
			})
		}
	})
	return meta;
}

export function tableButtonClick(that, props, key, text, record, index) {
	switch (key) {
		case 'Delete_inner':
			// props.button.setPopContent('Delete_inner', that.state.json['arappub-000001']);/* 国际化处理： 确认删除？*/
			props.table.deleteTableRowsByIndex(tableId, index);
			break;
	}

}
