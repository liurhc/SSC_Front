import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,getMultiLang, createPageIcon } from 'nc-lightapp-front';
import { initTemplate, afterEvent, transferButtonClick } from './events';
import { tableId, formId, leftarea, dataSource } from './constants';
import { bodyBeforeEvent } from '../../../public/components/pubUtils/arapTableRefFilter';
import { formBeforeEvent } from '../../../public/components/pubUtils/arapFormRefFilter';
/**
 * 收款单为资金到账通知提供卡片页面
 */
class Card extends Component {
	constructor(props) {
		super(props);
		this.appcode = this.props.getSearchParam('c')
		this.pagecode = this.props.getSearchParam("p");
		this.formId = formId;
		this.tableId = tableId;
		this.leftarea = leftarea;
		this.tradetype = this.props.getUrlParam("billtype")
		if(this.tradetype && (this.tradetype.substring(0, 2) == ("F2") || this.tradetype.substring(0, 2) == ("D2"))){
			this.billType = "F2"
		} else if (this.tradetype && (this.tradetype.substring(0, 2) == ("F3") || this.tradetype.substring(0, 2) == ("D3"))){
			this.billType = "F3"
		}
		this.pkName = this.billType == 'F2' ? 'pk_gatherbill' : 'pk_paybill';
		this.transferIndex = 0; //转单时左侧选择的数据序号
		this.state = {
			json:{}
		};
	}

	componentDidMount() {
		let callback = (json) => {
			this.setState({json:json},() => {
				initTemplate.call(this, this.props, this.initShow);
			});
		}
		getMultiLang({moduleId:['arappub','public'],domainName :'arap',currentLocale:'simpchn',callback});
	}

	//页面初始化
	initShow = () => {
		if (this.props.getUrlParam('src') === '36S3') {
			let ids = this.props.getUrlParam('pks');
			let source = this.props.getUrlParam('source');
			if (ids) {
				let data = {
					pk_bill: ids,
					pageId: this.pagecode,
					billType: this.tradetype,
					source: source
				};
				ajax({
					url: '/nccloud/arap/arappub/releasetoarap.do',
					data: data,
					success: (res) => {
						if (res && res.data) {
							this.props.transferTable.setTransferListValue(
								this.leftarea,
								res.data
							)
						}					
						this.toggleShow()
					}
				})
			} else {
				this.props.transferTable.setTransferListValue(this.leftarea, []);
			}
		}
	}

	//同步单据信息到转单控件
	synTransferData = () => {
		//重取界面现有数据赋值到转单中
		let amount = this.props.transferTable.getTransformFormAmount(this.leftarea)
		if (amount != 1) {
			let cardData = this.props.createMasterChildData(this.pagecode, this.formId, this.tableId);
			this.props.transferTable.setTransferListValueByIndex(this.leftarea, cardData, this.transferIndex);
		}
	}

	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		let flag = status === 'browse' ? true : false;
		this.props.button.setButtonVisible(['Edit'], flag);
		this.props.button.setButtonVisible(['Save', 'Cancel', 'Delete_inner'], !flag);
		this.props.button.setButtonVisible(['Quit'], true);
		this.props.form.setFormStatus(formId, status);
		this.props.cardTable.setStatus(tableId, status);
		if (status == 'add') {
			this.props.cardTable.setStatus(this.tableId, 'edit');
		} else {
			this.props.cardTable.setStatus(this.tableId, status);
		}
		this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true });
	}

	//获取列表肩部信息
	getTableHead = (buttons) => {
		return (
			<span>
				{this.props.button.createButtonApp({
					area: 'card_body',
					buttonLimit: 3,
					// onButtonClick: buttonClick.bind(this),
					popContainer: document.querySelector('.header-button-area')
				})}
			</span>
		)
	}


	render() {
		let { cardTable, form, button, modal, transferTable } = this.props;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButton } = button;
		let { createModal } = modal;
		const { createTransferList } = transferTable;
		return (
			<div id="transferCard" className="nc-bill-transferList">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{this.billType == 'F2' ? this.state.json['arappub-000003'] : this.state.json['arappub-000004']}</h2>
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'card_head',
							buttonLimit: 3,
							onButtonClick: transferButtonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-transferList-content">
					{createTransferList({
						headcode: this.formId,
						bodycode: this.tableId,
						transferListId: this.leftarea,
						onTransferItemSelected: (record, status, index) => {
							//转单缩略图被选中时的钩子函数
							this.transferIndex = index;
							let isEdit = status ? 'browse' : 'edit';
							if (isEdit == 'browse') {
								this.props.setUrlParam({ status: 'browse' })
								let id = record.head[this.formId].rows[0].values[this.pkName].value
								this.props.setUrlParam({ id: id })
							} else {
								this.props.setUrlParam({ status: 'add' })
								this.props.delUrlParam('id')
							}
							this.toggleShow(record.head[this.formId]);
							this.props.form.setAllFormValue({ [this.formId]: record.head[this.formId] });
							this.props.cardTable.setTableData(this.tableId, record.body[this.tableId]);
							this.props.form.setFormStatus(this.formId, isEdit);
							this.props.cardTable.setStatus(this.tableId, isEdit);
							}
					})}
					<div className="transferList-content-right nc-bill-card" >
						<div className="nc-bill-form-area">
							{createForm(this.formId, {
								onAfterEvent: afterEvent.bind(this),
								onBeforeEvent: formBeforeEvent.bind(this)
							})}
						</div>
						<div className="nc-bill-table-area">
							{createCardTable(this.tableId, {
								tableHead: this.getTableHead.bind(this, buttons),
								modelSave: this.saveBill,
								onAfterEvent: afterEvent.bind(this),
								onBeforeEvent: bodyBeforeEvent.bind(this),
								showCheck: true,
								showIndex: true,
								isAddRow: false
							})}
						</div>
					</div>
				</div>
			</div>
		)
	}
}

Card = createPage({})(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
