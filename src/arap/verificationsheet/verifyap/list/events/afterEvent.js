import { ajax, toast, getBusinessInfo } from 'nc-lightapp-front';
import {
	appcode, pagecode, funCode, verifySearchId, autoVerifySearchId, tableId_bf, tableId_df,
	cytjCode, bfCode, dfCode, publicCode, hxgzCode,
	cytjCode_auto, bfCode_auto, dfCode_auto, publicCode_auto, hxgzCode_auto, hxfsCode
} from '../constants';
import { setFormEditable } from '../../../../public/components/pubUtils/billPubUtil.js';
import { setDefaultValue } from './buttonClick';

export default function afterEvent(props, moduleId, key, value, oldValue) {
	// props, moduleId(区域id), key(操作的键), value（当前值），oldValue(旧值)
	let that = this
	let cytjCode = this.cytjCode;
	let bfCode = this.bfCode;
	let dfCode = this.dfCode;
	let publicCode = this.publicCode;
	let hxgzCode = this.hxgzCode;

	let meta = props.meta.getMeta();
	//常用条件编辑后事件
	if (moduleId == cytjCode || moduleId == cytjCode_auto) {
		if (moduleId == cytjCode_auto) {
			cytjCode = cytjCode_auto
			bfCode = bfCode_auto
			dfCode = dfCode_auto
			publicCode = publicCode_auto
			hxgzCode = hxgzCode_auto
		}
		if (key == 'pk_org' && value.value != null && value.value != "") {
			//设置本方单据类型	对方单据类型不可编辑
			setFormEditable(props, cytjCode, cytjCode, true);
			setFormEditable(props, cytjCode, bfCode, true);
			setFormEditable(props, cytjCode, dfCode, true);
			setFormEditable(props, cytjCode, publicCode, true);
			props.form.setFormItemsDisabled(cytjCode, { jfBillTypeBox: true });
			props.form.setFormItemsDisabled(cytjCode, { dfBillTypeBox: true });
			props.form.setFormItemsRequired(hxgzCode,{'jefxCmbBox':false})

			if (!oldValue || value.value != oldValue.value) {
				loadVerifyFa(props, cytjCode, hxgzCode, value);
			}
		}

		//方案加载
		if (key == 'verifyFa') {
			if (value.value != "null") {
				ajax({
					url: '/nccloud/arap/verifyfa/edit.do',
					data: {
						pk_bill: value.value,
						pageId: pagecode,
						cytjCode: cytjCode,
						hxgzCode: hxgzCode,
						hxfsCode: hxfsCode
					},
					success: function (res) {
						if (res.success) {
							if (res.data) {
								writeBack(that, res, props, moduleId, cytjCode, bfCode, dfCode, publicCode, hxgzCode, hxfsCode);
								props.form.setFormItemsValue(cytjCode, { 'verifyFa': value });
							}
						}
					}
				})
			} else {
				let pk_org = props.form.getFormItemsValue(cytjCode, 'pk_org')
				let verifyFa = props.form.getFormItemsValue(cytjCode, 'verifyFa')
				//设置默认值
				setDefaultValue(this, props, cytjCode, bfCode, dfCode, publicCode, hxgzCode)
				props.form.setFormItemsValue(cytjCode, { 'pk_org': pk_org })
				props.form.setFormItemsValue(cytjCode, { 'verifyFa': verifyFa })
				afterEvent.call(this, props, cytjCode, 'pk_org', pk_org, pk_org)
			}
		}

		// 本方核销对象,修改本方核销对象，联动本方对象名称参照类型、联动本方单据类型、联动本方交易类型过滤条件
		if (key == 'jf_hxdxCBox') {
			let meta = props.meta.getMeta()
			if (value.value == 'kh') {
				meta[bfCode].items.map((item) => {
					if (item.attrcode == 'jf_dxmcRef') {
						item.itemtype = 'refer'
						item.refName = this.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
						item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
						item.queryCondition = () => {
							return {
								DataPowerOperationCode: 'fi',//使用权组
								isDataPowerEnable: 'Y',
								pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
							}
						}
					}
					return item;
				})
				meta[dfCode].items.map((item) => {
					if (item.attrcode == 'df_dxmcRef') {
						item.itemtype = 'refer'
						item.refName = this.state.json['verificationsheet-000039']/* 国际化处理： 供应商档案*/
						item.refcode = 'uapbd/refer/supplier/SupplierRefTreeGridRef/index'
						item.queryCondition = () => {
							return {
								DataPowerOperationCode: 'fi',//使用权组
								isDataPowerEnable: 'Y',
								pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
							}
						}
					}

					return item;
				})
				props.form.setFormItemsValue(cytjCode, { 'jfBillTypeBox': { value: 'khys', display: this.state.json['verificationsheet-000040'] } });/* 国际化处理： 应收单*/
				props.form.setFormItemsValue(cytjCode, { 'jf_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsValue(cytjCode, { 'df_hxdxCBox': { value: 'gys', display: this.state.json['verificationsheet-000001'] } });/* 国际化处理： 供应商*/
				props.form.setFormItemsValue(cytjCode, { 'dfBillTypeBox': { value: 'gysyf', display: this.state.json['verificationsheet-000041'] } });/* 国际化处理： 应付单*/
				props.form.setFormItemsValue(cytjCode, { 'df_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsDisabled(cytjCode, { df_hxdxCBox: true });
			}
			if (value.value == 'gys' || value.value == 'bm' || value.value == 'ywy') {
				if (value.value == 'gys') {
					meta[bfCode].items.map((item) => {
						if (item.attrcode == 'jf_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000039']/* 国际化处理： 供应商档案*/
							item.refcode = 'uapbd/refer/supplier/SupplierRefTreeGridRef/index'
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
								}
							}
						}

						return item;
					})
				} else if (value.value == 'bm') {
					meta[bfCode].items.map((item) => {
						if (item.attrcode == 'jf_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000002']/* 国际化处理： 部门*/
							item.refcode = 'uapbd/refer/org/DeptTreeRef/index'
							item.unitCondition = () => {
								return {
									pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
									TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
								}
							}
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									busifuncode: 'all',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
								}
							}
						}
						return item;
					})
				} else if (value.value == 'ywy') {
					meta[bfCode].items.map((item) => {
						if (item.attrcode == 'jf_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000042']/* 国际化处理： 人员*/
							item.refcode = 'uapbd/refer/psninfo/PsndocTreeGridRef/index'
							item.isShowUnit = true,
								item.unitCondition = () => {
									return {
										pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
										TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
									}
								}
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									busifuncode: 'all',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
									pk_dept: props.form.getFormItemsValue(cytjCode, 'deptRef').value ? props.form.getFormItemsValue(cytjCode, 'deptRef').value : null
								}
							}
						}
						return item;
					})
				}
				props.form.setFormItemsValue(cytjCode, { 'jfBillTypeBox': { value: 'gysfk', display: this.state.json['verificationsheet-000043'] } });/* 国际化处理： 付款单*/
				props.form.setFormItemsValue(cytjCode, { 'jf_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsValue(cytjCode, { 'df_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsDisabled(cytjCode, { df_hxdxCBox: false });
			}
			//重新渲染本方对象名称
			props.renderItem('form', bfCode, 'jf_dxmcRef', null);
			props.renderItem('form', dfCode, 'df_dxmcRef', null);
		}

		// 对方核销对象，修改对方核销对象，联动对方对象名称参照类型、联动对方单据类型、联动对方交易类型过滤条件
		// 当对方核销对象为供应商,并且本方核销对象为供应商时，对方单据类型可编辑
		if (key == 'df_hxdxCBox') {
			let meta = props.meta.getMeta()
			if (value.value == 'kh') {
				meta[dfCode].items.map((item) => {
					if (item.attrcode == 'df_dxmcRef') {
						item.itemtype = 'refer'
						item.refName = this.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
						item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
						item.queryCondition = () => {
							return {
								DataPowerOperationCode: 'fi',//使用权组
								isDataPowerEnable: 'Y',
								pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
							}
						}
					}
					return item;
				})
				meta[bfCode].items.map((item) => {
					if (item.attrcode == 'jf_dxmcRef') {
						item.itemtype = 'refer'
						item.refName = this.state.json['verificationsheet-000039']/* 国际化处理： 供应商档案*/
						item.refcode = 'uapbd/refer/supplier/SupplierRefTreeGridRef/index'
						item.queryCondition = () => {
							return {
								DataPowerOperationCode: 'fi',//使用权组
								isDataPowerEnable: 'Y',
								pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
							}
						}
					}
					return item;
				})
				props.form.setFormItemsValue(cytjCode, { 'dfBillTypeBox': { value: 'khsk', display: this.state.json['verificationsheet-000044'] } });/* 国际化处理： 收款单*/
				props.form.setFormItemsValue(cytjCode, { 'jf_hxdxCBox': { value: 'gys', display: this.state.json['verificationsheet-000001'] } });/* 国际化处理： 供应商*/
				props.form.setFormItemsValue(cytjCode, { 'jf_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsValue(cytjCode, { 'df_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsDisabled(cytjCode, { jf_hxdxCBox: true });
			}
			if (value.value == 'gys' || value.value == 'bm' || value.value == 'ywy') {
				if (value.value == 'gys') {
					meta[dfCode].items.map((item) => {
						if (item.attrcode == 'df_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000039']/* 国际化处理： 供应商档案*/
							item.refcode = 'uapbd/refer/supplier/SupplierRefTreeGridRef/index'
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
								}
							}
						}
						return item;
					})
				} else if (value.value == 'bm') {
					meta[dfCode].items.map((item) => {
						if (item.attrcode == 'df_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000002']/* 国际化处理： 部门*/
							item.refcode = 'uapbd/refer/org/DeptTreeRef/index'
							item.isShowUnit = true,
								item.unitCondition = () => {
									return {
										pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
										TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
									}
								}
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									busifuncode: 'all',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
								}
							}
						}
						return item;
					})

				} else if (value.value == 'ywy') {
					meta[dfCode].items.map((item) => {
						if (item.attrcode == 'df_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000042']/* 国际化处理： 人员*/
							item.refcode = 'uapbd/refer/psninfo/PsndocTreeGridRef/index'
							item.isShowUnit = true,
								item.unitCondition = () => {
									return {
										pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
										TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
									}
								}
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									busifuncode: 'all',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
									pk_dept: props.form.getFormItemsValue(cytjCode, 'deptRef').value ? props.form.getFormItemsValue(cytjCode, 'deptRef').value : null
								}
							}
						}
						return item;
					})
				}
				props.form.setFormItemsValue(cytjCode, { 'dfBillTypeBox': { value: 'gysyf', display: this.state.json['verificationsheet-000041'] } });/* 国际化处理： 应付单*/
				props.form.setFormItemsValue(cytjCode, { 'jf_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsValue(cytjCode, { 'df_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsDisabled(cytjCode, { jf_hxdxCBox: false });
			}
			//重新渲染对方对象名称
			props.renderItem('form', bfCode, 'jf_dxmcRef', null);
			props.renderItem('form', dfCode, 'df_dxmcRef', null);
		}

		//设置本方单据类型	对方单据类型不可编辑
		props.form.setFormItemsDisabled(cytjCode, { jfBillTypeBox: true });
		props.form.setFormItemsDisabled(cytjCode, { dfBillTypeBox: true });
		if (props.form.getFormItemsValue(cytjCode, 'jf_hxdxCBox').value == 'gys' && props.form.getFormItemsValue(cytjCode, 'df_hxdxCBox').value == 'gys' && props.form.getFormItemsValue(cytjCode, 'pk_org').value) {
			props.form.setFormItemsDisabled(cytjCode, { dfBillTypeBox: false });
		}
	}

	//核销规则编辑后事件
	if (moduleId == hxgzCode || moduleId == hxgzCode_auto) {
		if (moduleId == hxgzCode_auto) {
			cytjCode = cytjCode_auto
			bfCode = bfCode_auto
			dfCode = dfCode_auto
			publicCode = publicCode_auto
			hxgzCode = hxgzCode_auto
		}

		//红蓝对冲
		if (key == 'hldcChkBox') {
			if (value.value == true) {
				props.form.setFormItemsRequired(hxgzCode,{'jefxCmbBox':false})
				props.form.setFormItemsDisabled(hxgzCode, { jefxCmbBox: true });
				props.form.setFormItemsValue(hxgzCode, { 'jefxCmbBox': { value: null, display: null } });
			} else {
				props.form.setFormItemsRequired(hxgzCode,{'jefxCmbBox':true})
				props.form.setFormItemsDisabled(hxgzCode, { jefxCmbBox: false });
				props.form.setFormItemsValue(hxgzCode, { 'jefxCmbBox': { value: 0, display: this.state.json['verificationsheet-000025'] } });/* 国际化处理： 正*/
			}
		}

		//同币种核销，控制异币种核销信息字段不可编辑
		if (key == 'tbzhxChkBox') {
			if (value.value == true) {
				props.form.setFormItemsDisabled(hxgzCode, {
					zjbzRef: true, yxwcTxtF: true,
					jfbzRef: true, jfbz_dzjbzhlTxtF: true,
					jfbz_dbbhlTxtF: true, dfbzRef: true,
					dfbz_dbbhlTxtF: true, dfbz_dzjbzhlTxtF: true, hxbzRef: false
				});
				props.form.setFormItemsValue(hxgzCode, { 'ybzhxChkBox': { value: null, display: null } });
				let pk_org = props.form.getFormItemsValue(cytjCode, 'pk_org');
				if (pk_org && pk_org.value) {
					props.form.setFormItemsValue(hxgzCode, { 'hxbzRef': { value: '1002Z0100000000001K1', display: this.state.json['verificationsheet-000045'] } });/* 国际化处理： 人民币*/
				}
			}
		}

		//异币种核销
		if (key == 'ybzhxChkBox') {
			if (value.value == true) {
				props.form.setFormItemsDisabled(hxgzCode, {
					zjbzRef: false, yxwcTxtF: false,
					jfbzRef: false, jfbz_dzjbzhlTxtF: false,
					jfbz_dbbhlTxtF: false, dfbzRef: false,
					dfbz_dbbhlTxtF: false, dfbz_dzjbzhlTxtF: false, hxbzRef: true
				});
				props.form.setFormItemsValue(hxgzCode, { 'tbzhxChkBox': { value: null, display: null } });
				props.form.setFormItemsValue(hxgzCode, { 'hxbzRef': { value: null, display: null } });
			} else {
				props.form.setFormItemsDisabled(hxgzCode, {
					zjbzRef: true, yxwcTxtF: true,
					jfbzRef: true, jfbz_dzjbzhlTxtF: true,
					jfbz_dbbhlTxtF: true, dfbzRef: true,
					dfbz_dbbhlTxtF: true, dfbz_dzjbzhlTxtF: true, hxbzRef: false
				});
			}
		}

		//中间币种
		if (key == 'zjbzRef' && value.value) {
			let zjbz = props.form.getFormItemsValue(hxgzCode, 'zjbzRef').value;
			let jfbz = props.form.getFormItemsValue(hxgzCode, 'jfbzRef').value;
			let dfbz = props.form.getFormItemsValue(hxgzCode, 'dfbzRef').value;
			let pk_org = props.form.getFormItemsValue(cytjCode, 'pk_org').value;

			// if ((jfbz == "" && dfbz == "") || pk_org == "") {
			// 	return;
			// }
			if (pk_org == "") {
				return;
			}
			ajax({
				url: '/nccloud/arap/verifyfa/afterEvent.do',
				data: {
					key: key,
					value: value.value,
					zjbz: zjbz,
					jfbz: jfbz,
					dfbz: dfbz,
					pk_org: pk_org,
				},
				success: (res) => {
					if (res.success) {
						if (res.data) {
							let yxwc = props.form.getFormItemsValue(hxgzCode, 'yxwcTxtF');
							let yxwc_scale = res.data['zjbz']['yxwc'][0] * 1
							let yxwc_value = yxwc.value == null ? null : parseFloat(yxwc.value).toFixed(yxwc_scale)
							props.form.setFormItemsValue(hxgzCode, { 'yxwcTxtF': { value: yxwc_value, display: yxwc_value, scale: yxwc_scale } });							
							if (jfbz) {
								if(res.data['jfbz'] && res.data['jfbz']['midrate']){
									props.form.setFormItemsValue(hxgzCode, { 'jfbz_dzjbzhlTxtF': { value: res.data['jfbz']['midrate'][0], display: res.data['jfbz']['midrate'][0], scale: res.data['jfbz']['midrate'][1] * 1 } });
									if(res.data['jfbz']['midrate'][0]==1){
										props.form.setFormItemsDisabled(hxgzCode, {
											jfbz_dzjbzhlTxtF: true
									   });
									}else{
										props.form.setFormItemsDisabled(hxgzCode, {
											jfbz_dzjbzhlTxtF: false
									   });
									}
								}
								if(res.data['jfbz'] && res.data['jfbz']['hl']){
									props.form.setFormItemsValue(hxgzCode, { 'jfbz_dbbhlTxtF': { value: res.data['jfbz']['hl'][0], display: res.data['jfbz']['hl'][0], scale: res.data['jfbz']['hl'][1] * 1 } });
									if(res.data['jfbz']['hl'][0]==1){
										props.form.setFormItemsDisabled(hxgzCode, {
											jfbz_dbbhlTxtF: true
									   });
									}else{
										props.form.setFormItemsDisabled(hxgzCode, {
											jfbz_dbbhlTxtF: false
									   });
									}
								}
							}
							if (dfbz) {
								if(res.data['dfbz'] && res.data['dfbz']['midrate']){
									props.form.setFormItemsValue(hxgzCode, { 'dfbz_dzjbzhlTxtF': { value: res.data['dfbz']['midrate'][0], display: res.data['dfbz']['midrate'][0], scale: res.data['dfbz']['midrate'][1] * 1 } });
									if(res.data['dfbz']['midrate'][0]==1){
										props.form.setFormItemsDisabled(hxgzCode, {
											dfbz_dzjbzhlTxtF: true
									   });
									}else{
										props.form.setFormItemsDisabled(hxgzCode, {
											dfbz_dzjbzhlTxtF: false
									   });
									}
									
								}
								if(res.data['dfbz'] && res.data['dfbz']['hl']){
									props.form.setFormItemsValue(hxgzCode, { 'dfbz_dbbhlTxtF': { value: res.data['dfbz']['hl'][0], display: res.data['dfbz']['hl'][0], scale: res.data['dfbz']['hl'][1] * 1 } });
									if(res.data['dfbz']['hl'][0]==1){
										props.form.setFormItemsDisabled(hxgzCode, {
											dfbz_dbbhlTxtF: true
									   });
									}else{
										props.form.setFormItemsDisabled(hxgzCode, {
											dfbz_dbbhlTxtF: false
									   });
									}
								}
							}
						}
					}
				}
			});
		}

		//借方币种
		if (key == 'jfbzRef' && value.value) {
			let zjbz = props.form.getFormItemsValue(hxgzCode, 'zjbzRef').value;
			let jfbz = props.form.getFormItemsValue(hxgzCode, 'jfbzRef').value;
			let pk_org = props.form.getFormItemsValue(cytjCode, 'pk_org').value;

			if (zjbz == "" || pk_org == "") {
				return;
			}
			ajax({
				url: '/nccloud/arap/verifyfa/afterEvent.do',
				data: {
					key: key,
					value: value.value,
					zjbz: zjbz,
					jfbz: jfbz,
					pk_org: pk_org,
				},
				success: (res) => {
					if (res.success) {
						if (res.data) {
							if(res.data['jfbz'] && res.data['jfbz']['midrate']){
								props.form.setFormItemsValue(hxgzCode, { 'jfbz_dzjbzhlTxtF': { value: res.data['jfbz']['midrate'][0], display: res.data['jfbz']['midrate'][0], scale: res.data['jfbz']['midrate'][1] * 1 } });
								if(res.data['jfbz']['midrate'][0]==1){
									props.form.setFormItemsDisabled(hxgzCode, {
										jfbz_dzjbzhlTxtF: true
								   });
								}else{
									props.form.setFormItemsDisabled(hxgzCode, {
										jfbz_dzjbzhlTxtF: false
								   });
								}
							}
							if(res.data['jfbz'] && res.data['jfbz']['hl']){
								props.form.setFormItemsValue(hxgzCode, { 'jfbz_dbbhlTxtF': { value: res.data['jfbz']['hl'][0], display: res.data['jfbz']['hl'][0], scale: res.data['jfbz']['hl'][1] * 1 } });
								if(res.data['jfbz']['hl'][0]==1){
									props.form.setFormItemsDisabled(hxgzCode, {
										jfbz_dbbhlTxtF: true
								   });
								}else{
									props.form.setFormItemsDisabled(hxgzCode, {
										jfbz_dbbhlTxtF: false
								   });
								}
							}
						}
					}
				}
			});
		}

		//借方币种-对中间币种汇率 
		if (key == 'jfbz_dzjbzhlTxtF') {
			let scale1 = props.form.getFormItemsValue(hxgzCode, 'jfbz_dzjbzhlTxtF').scale
			let scale2 = props.form.getFormItemsValue(hxgzCode, 'jfbz_dbbhlTxtF').scale
			if (value.value) {
				let val = parseFloat(value.value).toFixed(scale2)
				props.form.setFormItemsValue(hxgzCode, { 'jfbz_dbbhlTxtF': { value: val, display: val, scale: scale2 } });
			} else {
				let val1 = parseFloat('0.00000000').toFixed(scale1)
				let val2 = parseFloat('0.00000000').toFixed(scale2)
				props.form.setFormItemsValue(hxgzCode, { 'jfbz_dzjbzhlTxtF': { value: val1, display: val1, scale: scale1 } });
				props.form.setFormItemsValue(hxgzCode, { 'jfbz_dbbhlTxtF': { value: val2, display: val2, scale: scale2 } });
			}
		}

		//借方币种-对本币汇率 
		if (key == 'jfbz_dbbhlTxtF') {
			let scale1 = props.form.getFormItemsValue(hxgzCode, 'jfbz_dzjbzhlTxtF').scale
			let scale2 = props.form.getFormItemsValue(hxgzCode, 'jfbz_dbbhlTxtF').scale
			if (value.value) {
				let val = parseFloat(value.value).toFixed(scale1)
				props.form.setFormItemsValue(hxgzCode, { 'jfbz_dzjbzhlTxtF': { value: val, display: val, scale: scale1 } });
			} else {
				let val1 = parseFloat('0.00000000').toFixed(scale1)
				let val2 = parseFloat('0.00000000').toFixed(scale2)
				props.form.setFormItemsValue(hxgzCode, { 'jfbz_dzjbzhlTxtF': { value: val1, display: val1, scale: scale1 } });
				props.form.setFormItemsValue(hxgzCode, { 'jfbz_dbbhlTxtF': { value: val2, display: val2, scale: scale2 } });
			}
		}

		//贷方币种
		if (key == 'dfbzRef' && value.value) {
			let zjbz = props.form.getFormItemsValue(hxgzCode, 'zjbzRef').value;
			let dfbz = props.form.getFormItemsValue(hxgzCode, 'dfbzRef').value;
			let pk_org = props.form.getFormItemsValue(cytjCode, 'pk_org').value;

			if (dfbz == "" || pk_org == "") {
				return;
			}
			ajax({
				url: '/nccloud/arap/verifyfa/afterEvent.do',
				data: {
					key: key,
					value: value.value,
					zjbz: zjbz,
					dfbz: dfbz,
					pk_org: pk_org,
				},
				success: (res) => {
					if (res.success) {
						if (res.data) {
							if(res.data['dfbz'] && res.data['dfbz']['midrate']){
								props.form.setFormItemsValue(hxgzCode, { 'dfbz_dzjbzhlTxtF': { value: res.data['dfbz']['midrate'][0], display: res.data['dfbz']['midrate'][0], scale: res.data['dfbz']['midrate'][1] * 1 } });
								if(res.data['dfbz']['midrate'][0]==1){
									props.form.setFormItemsDisabled(hxgzCode, {
										dfbz_dzjbzhlTxtF: true
								   });
								}else{
									props.form.setFormItemsDisabled(hxgzCode, {
										dfbz_dzjbzhlTxtF: false
								   });
								}
							}
							if(res.data['dfbz'] && res.data['dfbz']['hl']){
								props.form.setFormItemsValue(hxgzCode, { 'dfbz_dbbhlTxtF': { value: res.data['dfbz']['hl'][0], display: res.data['dfbz']['hl'][0], scale: res.data['dfbz']['hl'][1] * 1 } });
								if(res.data['dfbz']['hl'][0]==1){
									props.form.setFormItemsDisabled(hxgzCode, {
										dfbz_dbbhlTxtF: true
								   });
								}else{
									props.form.setFormItemsDisabled(hxgzCode, {
										dfbz_dbbhlTxtF: false
								   });
								}
							}
						}
					}
				}
			});
		}

		//贷方币种-对中间币种汇率 
		if (key == 'dfbz_dzjbzhlTxtF') {
			let scale1 = props.form.getFormItemsValue(hxgzCode, 'dfbz_dzjbzhlTxtF').scale
			let scale2 = props.form.getFormItemsValue(hxgzCode, 'dfbz_dbbhlTxtF').scale
			if (value.value) {
				let val = parseFloat(value.value).toFixed(scale2)
				props.form.setFormItemsValue(hxgzCode, { 'dfbz_dbbhlTxtF': { value: val, display: val, scale: scale2 } });
			} else {
				let val1 = parseFloat('0.00000000').toFixed(scale1)
				let val2 = parseFloat('0.00000000').toFixed(scale2)
				props.form.setFormItemsValue(hxgzCode, { 'dfbz_dzjbzhlTxtF': { value: val1, display: val1, scale: scale1 } });
				props.form.setFormItemsValue(hxgzCode, { 'dfbz_dbbhlTxtF': { value: val2, display: val2, scale: scale2 } });
			}
		}

		//贷方币种-对本币汇率 
		if (key == 'dfbz_dbbhlTxtF') {
			let scale1 = props.form.getFormItemsValue(hxgzCode, 'dfbz_dzjbzhlTxtF').scale
			let scale2 = props.form.getFormItemsValue(hxgzCode, 'dfbz_dbbhlTxtF').scale
			if (value.value) {
				let val = parseFloat(value.value).toFixed(scale1)
				props.form.setFormItemsValue(hxgzCode, { 'dfbz_dzjbzhlTxtF': { value: val, display: val, scale: scale1 } });
			} else {
				let val1 = parseFloat('0.00000000').toFixed(scale1)
				let val2 = parseFloat('0.00000000').toFixed(scale2)
				props.form.setFormItemsValue(hxgzCode, { 'dfbz_dzjbzhlTxtF': { value: val1, display: val1, scale: scale1 } });
				props.form.setFormItemsValue(hxgzCode, { 'dfbz_dbbhlTxtF': { value: val2, display: val2, scale: scale2 } });
			}
		}

	}

}

//加载核销方案和默认币种
function loadVerifyFa(props, cytjCode, hxgzCode, value) {
	let meta = props.meta.getMeta();
	ajax({
		url: '/nccloud/arap/verify/loadVerifyFa.do',
		data: {
			funCode: funCode,
			pk_org: value.value
		},
		success: function (res) {
			let faOptions = [];
			faOptions.push({ display: "", value: null })
			let obj = {};
			res.data['verifyfa'].map((item) => {
				obj = {
					display: item.name,
					value: item.pk_verify_fa
				}
				faOptions.push(obj);
			})
			meta[cytjCode].items.map((item) => {
				if (item.attrcode == 'verifyFa') {
					item.itemtype = 'select'
					item.options = faOptions;
				}
				return item;
			})
			props.renderItem('form', cytjCode, 'verifyFa', null);
			let hxbz = res.data['currtype'];
			props.form.setFormItemsValue(cytjCode, { 'verifyFa': { value: null, display: null } },);
			props.form.setFormItemsValue(hxgzCode, { 'hxbzRef': { value: hxbz.value, display: hxbz.name } });
		}
	});
}



function writeBack(that, res, props, moduleId, cytjCode, bfCode, dfCode, publicCode, hxgzCode, hxfsCode) {
	props.form.EmptyAllFormValue(cytjCode)
	props.form.EmptyAllFormValue(hxgzCode)
	props.form.EmptyAllFormValue(hxfsCode)
	if (res) {
		if (res.data[0]) {
			props.form.setAllFormValue({ [cytjCode]: res.data[0][cytjCode] })
			//设置方案名称、本方单据类型、对方单据类型不可编辑
			props.form.setFormItemsDisabled(cytjCode, { name: true });
			props.form.setFormItemsDisabled(cytjCode, { jfBillTypeBox: true });
			props.form.setFormItemsDisabled(cytjCode, { dfBillTypeBox: true });
			//根据本方核销对象设置本方对象名称参照
			let meta = props.meta.getMeta()
			let jf_hxdxCBox = props.form.getFormItemsValue(cytjCode, 'jf_hxdxCBox').value;
			let df_hxdxCBox = props.form.getFormItemsValue(cytjCode, 'df_hxdxCBox').value;
			setRefCodeByHxdx(that, props, meta, jf_hxdxCBox, 'jf_dxmcRef', bfCode);
			setRefCodeByHxdx(that, props, meta, df_hxdxCBox, 'df_dxmcRef', dfCode);
			let date = getBusinessInfo().businessDate.substring(0, 10)
			let beginDate = date 
			let endDate = date 
			// 本方开始日期	本方结束日期
			props.form.setFormItemsValue(cytjCode, { 'jfBeginDateRef': { value: beginDate }, 'jfEndDateRef': { value: endDate } });
			// 对方开始日期	对方结束日期
			props.form.setFormItemsValue(cytjCode, { 'dfBeginDateRef': { value: beginDate }, 'dfEndDateRef': { value: endDate } });

		}
		if (res.data[1]) {
			props.form.setAllFormValue({ [hxgzCode]: res.data[1][hxgzCode] })
			props.form.setFormItemsValue(hxgzCode, { 'yxwcTxtF': { value: null, display: null } });
			props.form.setFormItemsValue(hxgzCode, { 'jfbz_dzjbzhlTxtF': { value: null, display: null } });
			props.form.setFormItemsValue(hxgzCode, { 'dfbz_dzjbzhlTxtF': { value: null, display: null } });
			//设置金额方向不可编辑
			let hldcChkBox = props.form.getFormItemsValue(hxgzCode, 'hldcChkBox');
			if (hldcChkBox.value == true) {
				props.form.setFormItemsDisabled(hxgzCode, { jefxCmbBox: true });
			}
		}
		if (res.data[2] && moduleId == cytjCode_auto) {
			props.form.EmptyAllFormValue(hxfsCode)
			props.form.setAllFormValue({ [hxfsCode]: res.data[2][hxfsCode] })
		}
	}


}

function setRefCodeByHxdx(that, props, meta, hxdxValue, dxmcField, areaCode) {
	if (hxdxValue == 'kh') {
		meta[areaCode].items.map((item) => {
			if (item.attrcode == dxmcField) {
				item.itemtype = 'refer'
				item.refName = that.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
				item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
			}
			return item;
		})
	} else if (hxdxValue == 'gys') {
		meta[areaCode].items.map((item) => {
			if (item.attrcode == dxmcField) {
				item.itemtype = 'refer'
				item.refName = that.state.json['verificationsheet-000039']/* 国际化处理： 供应商档案*/
				item.refcode = 'uapbd/refer/supplier/SupplierRefTreeGridRef/index'
			}
			return item;
		})
	} else if (hxdxValue == 'bm') {
		meta[areaCode].items.map((item) => {
			if (item.attrcode == dxmcField) {
				item.itemtype = 'refer'
				item.refName = that.state.json['verificationsheet-000002']/* 国际化处理： 部门*/
				item.refcode = 'uapbd/refer/org/DeptTreeRef/index'
			}
			return item;
		})
	} else if (hxdxValue == 'ywy') {
		meta[areaCode].items.map((item) => {
			if (item.attrcode == dxmcField) {
				item.itemtype = 'refer'
				item.refName = that.state.json['verificationsheet-000042']/* 国际化处理： 人员*/
				item.refcode = 'uapbd/refer/psninfo/PsndocTreeGridRef/index'
			}
			return item;
		})
	}
	//重新渲染本方对象名称、对方对象名称
	props.renderItem('form', areaCode, dxmcField, null);
}
