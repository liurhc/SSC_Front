import { ajax, promptBox } from 'nc-lightapp-front';
import { appcode, pagecode, tableId, verifySearchId, tableId_bf, tableId_df, hxgzCode, cytjCode, funCode } from '../constants';


export default function tableAfterEvent(props, moduleId, key, value, changedrows, index, record) {
    // props, moduleId(区域id), key(操作的键), value（当前值）, changedrows（新旧值集合）, record（行数据）, index（当前index）

    let disction = 0;//本次折扣
    let settleMoney = 0;//本次结算
    let mid_sett = 0;//中币结算
    let sum = 0;//结算金额+折扣金额
    let faultindex = index * 1 + 1;
    let money_bal = props.cardTable.getValByKeyAndIndex(moduleId, index, 'money_bal');
    let mid_scale = 2;//中间币种小数精度;

    //本方编辑后事件
    if (moduleId == tableId_bf) {
        //本次结算
        if (key == 'settleMoney') {
            if (this.isUnsameVerify) {
                let m_jfbz2zjbzHL = this.rule[2].m_jfbz2zjbzHL//借方币种对中间币种汇率
                sum = value * 1 
                if(sum > Math.abs(money_bal.value)){
                    promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000073'] + faultindex + this.state.json['verificationsheet-000074'] })/* 国际化处理： 第,行，结算金额+折扣金额大于原币余额*/
                    cancelSelectedRow.call(this, props, moduleId, record, index, false)
                    return
                }
                mid_scale = this.rule[2].m_verifyCurrPrecision
                let jfmid_sett = (value * 1 * m_jfbz2zjbzHL).toFixed(mid_scale)
                this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'mid_sett', { value: jfmid_sett, display: jfmid_sett});
            } else {
                if(props.cardTable.getValByKeyAndIndex(moduleId, index, 'disction')){
                    disction = props.cardTable.getValByKeyAndIndex(moduleId, index, 'disction').value * 1;
                }
                settleMoney = value * 1;
                sum = Math.abs(settleMoney) + Math.abs(disction);
                if (sum > Math.abs(money_bal.value)) {
                    promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000073'] + faultindex + this.state.json['verificationsheet-000074'] })/* 国际化处理： 第,行，结算金额+折扣金额大于原币余额*/
                    cancelSelectedRow.call(this, props, moduleId, record, index, false)
                    return
                }
                if (settleMoney * money_bal.value < 0) {
                    promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000073'] + faultindex + this.state.json['verificationsheet-000075'] })/* 国际化处理： 第,行，结算金额和原币余额方向相反*/
                    cancelSelectedRow.call(this, props, moduleId, record, index, false)
                    return
                }
                if(sum == 0){
                    let money_bal = props.cardTable.getValByKeyAndIndex(moduleId, index, 'money_bal')
                    this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'settleMoney', money_bal);
                }
            }

        }
        //中币结算
        if (key == 'mid_sett') {
            mid_scale = this.rule[2].m_verifyCurrPrecision
            let m_jfbz2zjbzHL = this.rule[2].m_jfbz2zjbzHL//借方币种对中间币种汇率
            let monbal_zb = (money_bal.value * 1 * m_jfbz2zjbzHL).toFixed(mid_scale)
            mid_sett = value * 1;
            if (Math.abs(mid_sett) > Math.abs(monbal_zb)) {
                promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000076'] })/* 国际化处理： 中币结算金额大于原币余额*/
                cancelSelectedRow.call(this, props, moduleId, record, index, false)
                return
            }
            if (mid_sett * monbal_zb < 0) {
                promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000077'] })/* 国际化处理： 中币结算金额和原币余额方向相反*/
                cancelSelectedRow.call(this, props, moduleId, record, index, false)
                return
            }
            settleMoney = (value * 1 / m_jfbz2zjbzHL).toFixed(money_bal.scale)
            this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'settleMoney', { value: settleMoney, display: settleMoney });
        }
    }

    //对方编辑后事件
    if (moduleId == tableId_df) {
        //本次结算
        if (key == 'settleMoney') {
            if (this.isUnsameVerify) {
                let m_dfbz2zjbzHL = this.rule[2].m_dfbz2zjbzHL//借方币种对中间币种汇率
                sum = value * 1 
                if(sum > Math.abs(money_bal.value)){
                    promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000073'] + faultindex + this.state.json['verificationsheet-000074'] })/* 国际化处理： 第,行，结算金额+折扣金额大于原币余额*/
                    cancelSelectedRow.call(this, props, moduleId, record, index, false)
                    return
                }
                mid_scale = this.rule[2].m_verifyCurrPrecision
                let dfmid_sett = (value * 1 * m_dfbz2zjbzHL).toFixed(mid_scale)
                this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'mid_sett', { value: dfmid_sett, display: dfmid_sett });
            } else {
                if(props.cardTable.getValByKeyAndIndex(moduleId, index, 'disction')){
                    disction = props.cardTable.getValByKeyAndIndex(moduleId, index, 'disction').value * 1;
                }
                settleMoney = value * 1;
                sum = Math.abs(settleMoney) + Math.abs(disction);
                if (sum > Math.abs(money_bal.value)) {
                    promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000073'] + faultindex + this.state.json['verificationsheet-000074'] })/* 国际化处理： 第,行，结算金额+折扣金额大于原币余额*/
                    cancelSelectedRow.call(this, props, moduleId, record, index, false)
                    return
                }
                if (settleMoney * money_bal.value < 0) {
                    promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000073'] + faultindex + this.state.json['verificationsheet-000075'] })/* 国际化处理： 第,行，结算金额和原币余额方向相反*/
                    cancelSelectedRow.call(this, props, moduleId, record, index, false)
                    return
                }
                if(sum == 0){
                    let money_bal = props.cardTable.getValByKeyAndIndex(moduleId, index, 'money_bal')
                    this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'settleMoney', money_bal);
                }
            }
        }

        //中币结算
        if (key == 'mid_sett') {
            mid_scale = this.rule[2].m_verifyCurrPrecision
            let m_dfbz2zjbzHL = this.rule[2].m_dfbz2zjbzHL//借方币种对中间币种汇率
            let monbal_zb = (money_bal.value * 1 * m_dfbz2zjbzHL).toFixed(mid_scale)
            mid_sett = value * 1;
            if (Math.abs(mid_sett) > Math.abs(monbal_zb)) {
                promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000076'] })/* 国际化处理： 中币结算金额大于原币余额*/
                cancelSelectedRow.call(this, props, moduleId, record, index, false)
                return
            }
            if (mid_sett * monbal_zb < 0) {
                promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000077'] })/* 国际化处理： 中币结算金额和原币余额方向相反*/
                cancelSelectedRow.call(this, props, moduleId, record, index, false)
                return
            }
            settleMoney = (value * 1 / m_dfbz2zjbzHL).toFixed(money_bal.scale)
            this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'settleMoney', { value: settleMoney, display: settleMoney });

        }
    }

    //本次折扣
    if (key == 'disction') {
        disction = value * 1;
        settleMoney = props.cardTable.getValByKeyAndIndex(moduleId, index, 'settleMoney').value * 1;
        sum = Math.abs(settleMoney) + Math.abs(disction);
        if (sum > Math.abs(money_bal.value)) {
            promptBox({
                color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000073'] + faultindex + this.state.json['verificationsheet-000078']/* 国际化处理： 第,行，折扣金额+本次结算金额>原币余额。*/
                + this.state.json['verificationsheet-000079'] + (settleMoney + disction) + this.state.json['verificationsheet-000080'] + money_bal.value/* 国际化处理： 折扣金额+本次结算金额：,,原币余额：*/
            })
            cancelSelectedRow.call(this, props, moduleId, record, index, false)
            return
        }
        if (disction * money_bal.value < 0) {
            promptBox({ color: "warning", noCancelBtn: true, content: this.state.json['verificationsheet-000073'] + faultindex + this.state.json['verificationsheet-000081'] })/* 国际化处理： 第,行，折扣金额和原币余额方向相反*/
            cancelSelectedRow.call(this, props, moduleId, record, index, false)
            return
        }
    }

}

export function cancelSelectedRow(props, moduleId, record, index, flag){
    setTimeout(
        function (){
            props.cardTable.unSelectRowsByIndex(moduleId, index, flag)
        }
        ,0) 
    this.onSelected(props, moduleId, record, index, flag)
}




