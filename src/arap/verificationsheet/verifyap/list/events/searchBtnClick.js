import { ajax, base, promptBox, toast } from 'nc-lightapp-front';
import { funCode, verifySearchId, pagecode, tableId, cytjCode, hxgzCode, tableId_bf, tableId_df } from '../constants';


//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
    //前端需要传递（queryVO	ofternConditionsVO	VerifyRulesVO  bfCode	dfCode	funCode	 pageId	 arapFlag  pk_org）
    let that = this 
    if (!validate(props, that)) {
        return true;
    };
    //本方核销对象
    let jf_hxdxCBox = props.form.getFormItemsValue(cytjCode, 'jf_hxdxCBox').value
    //对方核销对象
    let df_hxdxCBox = props.form.getFormItemsValue(cytjCode, 'df_hxdxCBox').value

    let queryInfo = this.props.search.getQueryInfo(verifySearchId);
    let querydata = {
        queryVO: queryInfo,
        ofternConditionsVO: {
            model: props.form.getAllFormValue(cytjCode),
            pageid: pagecode
        },
        verifyRulesVO: {
            model: props.form.getAllFormValue(hxgzCode),
            pageid: pagecode
        },
        bfCode: tableId_bf,
        dfCode: tableId_df,
        pageId: pagecode,
        funCode: funCode,
        arapFlag: 'ap',
        pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value
    };
    ajax({
        url: '/nccloud/arap/verify/query.do',
        data: querydata,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                if (data['bf'] || data['df']) {
                    toast({color:"success"})
                    props.beforeUpdatePage();//打开开关
                    this.props.cardTable.selectAllRows(tableId_bf, false);
                    this.props.cardTable.selectAllRows(tableId_df, false);
                    this.setBtnVisible(props, false);
                    if (data['bf'] && data['bf'][tableId_bf]) {
                        dealFieldShowByHxdx(tableId_bf, props,"jf_hxdxCBox",jf_hxdxCBox)
                        props.cardTable.setTableData(tableId_bf, data['bf'][tableId_bf]);
                    } else {
                        props.cardTable.setTableData(tableId_bf, { rows: [] });
                    }
                    if (data['df'] && data['df'][tableId_df]) {
                        dealFieldShowByHxdx(tableId_df, props,"df_hxdxCBox",df_hxdxCBox)
                        props.cardTable.setTableData(tableId_df, data['df'][tableId_df]);
                    } else {
                        props.cardTable.setTableData(tableId_df, { rows: [] });
                    }
                    if (data['rule']) {
                        this.rule = data['rule']
                        if(data['rule'][2] && data['rule'][2].m_verifyName == "UN_SAME_VERIFY"){
                            this.isUnsameVerify = true
                        }else {
                            this.isUnsameVerify = false
                        }
                    }
                    if (data['hxMode']) {
                        dealFieldShow(this, props, data['hxMode'])
                    }
                    setScale.call(this, props, data)
                    props.updatePage(null, [tableId_bf,tableId_df]);//关闭开关
                } else {
                    props.cardTable.setTableData(tableId_bf, { rows: [] });
                    props.cardTable.setTableData(tableId_df, { rows: [] });
                    toast({ content: this.state.json['verificationsheet-000046'], color: 'warning'});/* 国际化处理： 未查询到符合条件的数据*/
                }
            }
        }
    });




};

//核销校验
function validate(props, that) {
    if (!props.form.isCheckNow(cytjCode,'warning')) {
        return false;
    }
    let hldcChkBox = props.form.getFormItemsValue(hxgzCode, 'hldcChkBox').value;// 红蓝对冲
    let tbzhxChkBox = props.form.getFormItemsValue(hxgzCode, 'tbzhxChkBox').value;// 同币种核销
    let hxbzRef = props.form.getFormItemsValue(hxgzCode, 'hxbzRef').value;// 核销币种
    let ybzhxChkBox = props.form.getFormItemsValue(hxgzCode, 'ybzhxChkBox');// 异币种核销

    if (!hldcChkBox && !tbzhxChkBox && ( ybzhxChkBox == undefined || ybzhxChkBox.value == null || ybzhxChkBox.value == false)) {
        promptBox({ color: "warning", content: that.state.json['verificationsheet-000050'], noCancelBtn: true })/* 国际化处理： 红蓝对冲,同币种核销,异币种核销 至少选择一种*/
        return false;
    }

    if ((hxbzRef == "" || hxbzRef == null || hxbzRef == undefined) && (ybzhxChkBox.value == null || ybzhxChkBox.value == false)) {
        if (hldcChkBox) {
            promptBox({ color: "warning", content: that.state.json['verificationsheet-000051'], noCancelBtn: true })/* 国际化处理： 红蓝对冲:核销币种必须设置*/
            return false;
        } else if (tbzhxChkBox) {
            promptBox({ color: "warning", content: that.state.json['verificationsheet-000052'], noCancelBtn: true })/* 国际化处理： 同币种核销:核销币种必须设置*/
            return false;
        }
    }

    if (ybzhxChkBox && ybzhxChkBox.value) {
        let zjbzRef = props.form.getFormItemsValue(hxgzCode, 'zjbzRef').value;
        let yxwcTxtF = props.form.getFormItemsValue(hxgzCode, 'yxwcTxtF').value;
        let jfbzRef = props.form.getFormItemsValue(hxgzCode, 'jfbzRef').value;
        let jfbz_dzjbzhlTxtF = props.form.getFormItemsValue(hxgzCode, 'jfbz_dzjbzhlTxtF').value;
        let jfbz_dbbhlTxtF = props.form.getFormItemsValue(hxgzCode, 'jfbz_dbbhlTxtF').value;
        let dfbzRef = props.form.getFormItemsValue(hxgzCode, 'dfbzRef').value;
        let dfbz_dbbhlTxtF = props.form.getFormItemsValue(hxgzCode, 'dfbz_dbbhlTxtF').value;
        let dfbz_dzjbzhlTxtF = props.form.getFormItemsValue(hxgzCode, 'dfbz_dzjbzhlTxtF').value;
        let warnmessage = "";
        if (!zjbzRef) {
            warnmessage = warnmessage + that.state.json['verificationsheet-000064'];/* 国际化处理： 中间币种必须设置,*/
        }
        if (!jfbzRef) {
            warnmessage = warnmessage + that.state.json['verificationsheet-000065'];/* 国际化处理： 借方币种必须设置,*/
        }
        if (!dfbzRef) {
            warnmessage = warnmessage + that.state.json['verificationsheet-000066'];/* 国际化处理： 贷方币种必须设置,*/
        }
        if (!jfbz_dbbhlTxtF || jfbz_dbbhlTxtF * 1 == 0) {
            warnmessage = warnmessage + that.state.json['verificationsheet-000067'];/* 国际化处理： 借方对本币汇率必须设置,*/
        }
        if (!jfbz_dzjbzhlTxtF || jfbz_dzjbzhlTxtF * 1 == 0) {
            warnmessage = warnmessage + that.state.json['verificationsheet-000068'];/* 国际化处理： 借方对中间币种汇率必须设置,*/
        }
        if (!dfbz_dbbhlTxtF || dfbz_dbbhlTxtF * 1 == 0) {
            warnmessage = warnmessage + that.state.json['verificationsheet-000069'];/* 国际化处理： 贷方对本币汇率必须设置,*/
        }
        if (!dfbz_dzjbzhlTxtF || dfbz_dzjbzhlTxtF * 1 == 0) {
            warnmessage = warnmessage + that.state.json['verificationsheet-000070'];/* 国际化处理： 贷方对中间币种汇率必须设置,*/
        }
        if (warnmessage != "") {
            promptBox({ color: "warning", content: that.state.json['verificationsheet-000071'] + warnmessage, noCancelBtn: true })/* 国际化处理： 异币种核销:  */
            return false;
        }
        if (jfbzRef == dfbzRef) {
            promptBox({ color: "warning", content: that.state.json['verificationsheet-000072'], noCancelBtn: true })/* 国际化处理： 异币种核销:  借方币种和贷方币种必须设置为不同的币种*/
            return false;
        }

    }
    return true;
}


//根据核销方式控制字段的显隐性	hxMode=0 按单据	hxMode=1按表体
function dealFieldShow(that, props, hxMode) {
    let cacheHxMode = that.hxMode
    if (hxMode != null && hxMode == '0' && cacheHxMode && cacheHxMode == '1') {
        //按整单核销
        props.cardTable.showColByKey(tableId_bf, 'sum_headmoney');
        props.cardTable.showColByKey(tableId_df, 'sum_headmoney');
        props.cardTable.hideColByKey(tableId_bf, 'sum_itemmoney');
        props.cardTable.hideColByKey(tableId_df, 'sum_itemmoney');
    } else if ((cacheHxMode == null && hxMode == '1') || (cacheHxMode && cacheHxMode == '0' && hxMode == '1')){
        //按表体核销
        props.cardTable.showColByKey(tableId_bf, 'sum_itemmoney');
        props.cardTable.showColByKey(tableId_df, 'sum_itemmoney');
        props.cardTable.hideColByKey(tableId_bf, 'sum_headmoney');
        props.cardTable.hideColByKey(tableId_df, 'sum_headmoney');
    }

    //异币种核销
    let cacheIsUnsameVerify = that.cacheIsUnsameVerify
    if ((cacheIsUnsameVerify == null && that.isUnsameVerify == true) || (cacheIsUnsameVerify!= null  && cacheIsUnsameVerify == false && that.isUnsameVerify == true )) {
        //异币种核销
        props.cardTable.showColByKey(tableId_bf, 'mid_sett');
        props.cardTable.showColByKey(tableId_df, 'mid_sett');
        props.cardTable.hideColByKey(tableId_bf, 'disction');
        props.cardTable.hideColByKey(tableId_df, 'disction');
    } else if(cacheIsUnsameVerify!= null && cacheIsUnsameVerify == true && that.isUnsameVerify == false ){
        //同币种核销
        props.cardTable.showColByKey(tableId_bf, 'disction');
        props.cardTable.showColByKey(tableId_df, 'disction');
        props.cardTable.hideColByKey(tableId_bf, 'mid_sett');
        props.cardTable.hideColByKey(tableId_df, 'mid_sett');
    }
    //缓存下来，与下次查询比较
    that.hxMode = hxMode
    that.cacheIsUnsameVerify = that.isUnsameVerify
}


function dealFieldShowByHxdx(moduleId, props, key, value){
    if(key && value && (key == 'jf_hxdxCBox' || key == 'df_hxdxCBox')){
        if(value == 'kh'){
            props.cardTable.showColByKey(moduleId, 'customer');
            props.cardTable.hideColByKey(moduleId, ['supplier','pk_deptid','pk_psndoc']);
        }else if (value == 'gys'){
            props.cardTable.showColByKey(moduleId, 'supplier');
            props.cardTable.hideColByKey(moduleId, ['pk_deptid','customer','pk_psndoc']);
        }else if (value == 'bm'){
            props.cardTable.showColByKey(moduleId, 'pk_deptid');
            props.cardTable.hideColByKey(moduleId, ['supplier','customer','pk_psndoc']);
        }else if (value == 'ywy'){
            props.cardTable.showColByKey(moduleId, 'pk_psndoc');
            props.cardTable.hideColByKey(moduleId, ['supplier','customer','pk_deptid']);
        }
    }
}


export function setScale(props, data) {
    let scale = -1;
    if (data['bf'] && data['bf'][tableId_bf]) {
        scale = data['bf'][tableId_bf].rows[0].values.money_bal.scale
        props.cardTable.setColScale([{ areacode: tableId_bf, fieldcode: 'disction', scale: scale }, { areacode: tableId_bf, fieldcode: 'settleMoney', scale: scale }])
    }
    if (data['df'] && data['df'][tableId_df]) {
        scale = data['df'][tableId_df].rows[0].values.money_bal.scale
        props.cardTable.setColScale([{ areacode: tableId_df, fieldcode: 'disction', scale: scale }, { areacode: tableId_df, fieldcode: 'settleMoney', scale: scale }])
    }
    if (data['rule'] && data['rule'][2] && data['rule'][2].m_verifyName == "UN_SAME_VERIFY") {
        scale = data['rule'][2].m_verifyCurrPrecision;//中间币种小数精度
        props.cardTable.setColScale([{ areacode: tableId_bf, fieldcode: 'mid_sett', scale: scale }])
        props.cardTable.setColScale([{ areacode: tableId_df, fieldcode: 'mid_sett', scale: scale }])        
    }
}
