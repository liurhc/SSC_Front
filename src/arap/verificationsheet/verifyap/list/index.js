import { ajax, base, createPage, getMultiLang, createPageIcon } from 'nc-lightapp-front';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { getColvalues, getRowIds } from '../../../public/components/pubUtils/billPubUtil';
import { setFormEditable } from '../../../public/components/pubUtils/billPubUtil.js';
import {
	verifySearchId,
	autoVerifySearchId,
	cytjCode,
	bfCode,
	dfCode,
	publicCode,
	hxgzCode,
	cytjCode_auto,
	bfCode_auto,
	dfCode_auto,
	publicCode_auto,
	hxgzCode_auto,
	hxfsCode,
	tableId_bf,
	tableId_df
} from './constants';
import {
	afterEvent,
	beforeEvent,
	buttonClick,
	setDefaultValue,
	initTemplate,
	searchBtnClick,
	autoSearchBtnClick,
	tableAfterEvent
} from './events';
import './index.less';

/**
 *  应付管理，手工核销
 */
class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.verifySearchId = verifySearchId;
		this.autoVerifySearchId = autoVerifySearchId;
		this.tableId_bf = tableId_bf;
		this.tableId_df = tableId_df;
		this.cytjCode = cytjCode;
		this.bfCode = bfCode;
		this.dfCode = dfCode;
		this.publicCode = publicCode;
		this.hxgzCode = hxgzCode;
		this.cytjCode_auto = cytjCode_auto;
		this.bfCode_auto = bfCode_auto;
		this.dfCode_auto = dfCode_auto;
		this.publicCode_auto = publicCode_auto;
		this.hxgzCode_auto = hxgzCode_auto;
		this.hxfsCode = hxfsCode;
		this.rule = null; //核销规则
		this.hxMode = null;//hxMode=0 按单据	hxMode=1按表体
		this.isUnsameVerify = false; //是否异币种核销
		this.cacheIsUnsameVerify = null;//缓存是否异币种核销
		this.isFirstQuery = true; //是否第一次打开查询框
		this.isFirstAutoVerify = true; //是否第一次打开自动核销查询框
		this.state = {
			json: {}
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		getMultiLang({
			moduleId: ['verificationsheet', 'public'],
			domainName: 'arap',
			currentLocale: 'simpchn',
			callback
		});
	}
	componentDidMount() {
		initTemplate.call(this, this.props);
		this.props.button.setButtonDisabled(['Verify', 'Jfmath', 'Dfmath'], true);
	}

	//列表选中事件
	onSelected(props, moduleId, record, index, status) {
		this.setBtnVisible(props, status);
		let money_bal = record.values.money_bal;
		if (status) {
			this.props.cardTable.setStatus(moduleId, 'edit');
			if (this.isUnsameVerify) {
				let mid_scale = this.rule[2].m_verifyCurrPrecision; //中间币种小数精度
				if (moduleId == tableId_bf) {
					let m_jfbz2zjbzHL = this.rule[2].m_jfbz2zjbzHL; //借方币种对中间币种汇率
					let jfmid_sett = (money_bal.value * m_jfbz2zjbzHL).toFixed(mid_scale);
					this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'settleMoney', money_bal);
					this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'mid_sett', {
						value: jfmid_sett,
						display: jfmid_sett
					});
				} else {
					let m_dfbz2zjbzHL = this.rule[2].m_dfbz2zjbzHL; //贷方币种对中间币种汇率
					let dfmid_sett = (money_bal.value * m_dfbz2zjbzHL).toFixed(mid_scale);
					this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'settleMoney', money_bal);
					this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'mid_sett', {
						value: dfmid_sett,
						display: dfmid_sett
					});
				}
			} else {
				this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'settleMoney', money_bal);
			}
		} else {
			if (this.isUnsameVerify) {
				this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'settleMoney', {
					value: null,
					display: null
				});
				this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'mid_sett', { value: null, display: null });
			} else {
				this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'settleMoney', {
					value: null,
					display: null
				});
				this.props.cardTable.setValByKeyAndIndex(moduleId, index, 'disction', { value: null, display: null });
			}
			let selectedRows = this.props.cardTable.getCheckedRows(moduleId);
			if (!selectedRows || selectedRows == null || selectedRows.length == 0) {
				this.props.cardTable.setStatus(moduleId, 'browse');
			}
		}
	}

	//列表全选事件
	onSelectedAll(props, moduleId, status, length) {
		//props, moduleId(区域id), status（选框值）, length（选中数量）
		if (length == 0) {
			return;
		}
		if (!status) {
			this.props.cardTable.setColValue(moduleId, 'settleMoney', { value: null, display: null });
			this.props.cardTable.setColValue(moduleId, 'disction', { value: null, display: null });
			this.props.cardTable.setColValue(moduleId, 'mid_sett', { value: null, display: null });
			this.props.cardTable.setStatus(moduleId, 'browse');
			this.setBtnVisible(this.props, status);
			return;
		}
		let rate = null;
		let scale = null;
		if (this.isUnsameVerify) {
			scale = this.rule[2].m_verifyCurrPrecision; //中间币种小数精度
			if (moduleId == tableId_bf) {
				rate = this.rule[2].m_jfbz2zjbzHL; //借方币种对中间币种汇率
			} else {
				rate = this.rule[2].m_dfbz2zjbzHL; //贷方币种对中间币种汇率
			}
		}
		ajax({
			url: '/nccloud/arap/verify/selectAll.do',
			data: {
				// gridData: {
				// 	model: {
				// 		areacode: moduleId,
				// 		areaType: 'table',
				// 		rows: this.props.cardTable.getAllRows(moduleId)
				// 	},
				// 	pageid: this.pagecode
				// },
				colValues: getColvalues(this.props, moduleId, ['money_bal', 'settleMoney', 'disction', 'mid_sett']),
				rowids: getRowIds(this.props, moduleId),
				tableId: moduleId,
				isSelected: status,
				isUnsameVerify: this.isUnsameVerify,
				rate: rate,
				scale: scale
			},
			success: (res) => {
				if (res.data) {
					this.props.beforeUpdatePage(); //打开开关
					if (status) {
						this.props.cardTable.setStatus(moduleId, 'edit');
					} else {
						this.props.cardTable.setStatus(moduleId, 'browse');
					}
					this.setBtnVisible(this.props, status);
					this.props.cardTable.updateDataByRowId(moduleId, res.data[moduleId]);
					// this.props.cardTable.updateTableData(moduleId, res.data[moduleId]);
					this.props.cardTable.selectAllRows(moduleId, status);
					this.props.updatePage(null, [tableId_bf, tableId_df]); //关闭开关
				}
			}
		});
	}

	setBtnVisible = (props, status) => {
		if (status) {
			props.button.setButtonDisabled(['Verify', 'Jfmath', 'Dfmath'], false);
		} else {
			if (
				(!this.props.cardTable.getCheckedRows(this.tableId_bf) ||
					this.props.cardTable.getCheckedRows(this.tableId_bf).length == 0) &&
				(!this.props.cardTable.getCheckedRows(this.tableId_df) ||
					this.props.cardTable.getCheckedRows(this.tableId_df).length == 0)
			) {
				props.cardTable.setStatus(tableId_bf, 'browse');
				props.cardTable.setStatus(tableId_df, 'browse');
				props.button.setButtonDisabled(['Verify', 'Jfmath', 'Dfmath'], true);
			}

		}
	};

	//控制disction、settleMoney、mid_sett字段可编辑
	beforeEvent(props, moduleId, key, value, index, record, status) {
		// props 内部方法，moduleId(区域id), item(模版当前列的项), index（当前索引）,value（当前值）, record（行数据）
		let selected = props.cardTable.getClickRowIndex(moduleId).record.selected;
		if (selected && (key == 'disction' || key == 'settleMoney' || key == 'mid_sett')) {
			return true;
		} else {
			return false;
		}
	}

	/* 手工核销查询添加页签 */
	addAdvTabs = () => {
		let { createForm } = this.props.form;
		return [
			{
				name: this.state.json['verificationsheet-000082'] /* 国际化处理： 常用条件*/,
				content: (
					<div className="nc-bill-form-area normal-area">
						{createForm(this.cytjCode, {
							expandArr: [this.bfCode, this.dfCode, this.publicCode],
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				),
				defaultOpen: true
			},
			{
				name: this.state.json['verificationsheet-000083'] /* 国际化处理： 核销规则*/,
				content: (
					<div className="nc-bill-form-area">
						{createForm(hxgzCode, {
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				)
			}
		];
	};

	/* 自动核销查询添加页签 */
	autoVerifyAddAdvTabs = () => {
		let { createForm } = this.props.form;
		return [
			{
				name: this.state.json['verificationsheet-000082'] /* 国际化处理： 常用条件*/,
				content: (
					<div className="nc-bill-form-area normal-area">
						{createForm(this.cytjCode_auto, {
							expandArr: [this.bfCode_auto, this.dfCode_auto, this.publicCode_auto],
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				),
				defaultOpen: true
			},
			{
				name: this.state.json['verificationsheet-000083'] /* 国际化处理： 核销规则*/,
				content: (
					<div className="nc-bill-form-area">
						{createForm(this.hxgzCode_auto, {
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				)
			},
			{
				name: this.state.json['verificationsheet-000084'] /* 国际化处理： 核销方式*/,
				content: (
					<div className="nc-bill-form-area">
						{createForm(this.hxfsCode, {
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				)
			}
		];
	};

	/**
	 * 高级查询面板中的清空按钮 点击事件钩子，用于业务组清除自定义查询条件值
	 * isauto 是否自动核销
	 */
	clearSearchEve = (isauto) => {
		let cytjCode = this.cytjCode
		let bfCode = this.bfCode
		let dfCode = this.dfCode
		let publicCode = this.publicCode
		let hxgzCode = this.hxgzCode
		if (isauto) {
			cytjCode = cytjCode_auto;
			bfCode = bfCode_auto;
			dfCode = dfCode_auto;
			publicCode = publicCode_auto;
			hxgzCode = hxgzCode_auto;
		}
		this.props.form.setFormItemsDisabled(hxgzCode, {
			zjbzRef: true,
			yxwcTxtF: true,
			jfbzRef: true,
			jfbz_dzjbzhlTxtF: true,
			jfbz_dbbhlTxtF: true,
			dfbzRef: true,
			dfbz_dbbhlTxtF: true,
			dfbz_dzjbzhlTxtF: true,
			hxbzRef: false
		});
		//设置默认值
		setDefaultValue(this, this.props, cytjCode, bfCode, dfCode, publicCode, hxgzCode, isauto);
		setFormEditable(this.props, cytjCode, cytjCode, false);
		setFormEditable(this.props, cytjCode, bfCode, false);
		setFormEditable(this.props, cytjCode, dfCode, false);
		setFormEditable(this.props, cytjCode, publicCode, false);
		this.props.form.setFormItemsDisabled(cytjCode, { pk_org: false });
	}

	render() {
		let { search, modal } = this.props;
		let { NCCreateSearch } = search;
		let { createCardTable } = this.props.cardTable;
		return (
			<div className="nc-bill-list" id="verifyapId">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{this.state.json['verificationsheet-000087']}</h2>
						{/* 国际化处理： 应付核销*/}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				{NCCreateSearch(this.verifySearchId, {
					clickSearchBtn: searchBtnClick.bind(this),
					addAdvTabs: this.addAdvTabs,
					advSearchClearEve: this.clearSearchEve.bind(this, false),//高级查询面板中的清空按钮 点击事件钩子，用于业务组清除自定义查询条件值
					onlyShowSuperBtn: true, // 只显示高级按钮
					hideBtnArea: true // 隐藏查询按钮区域,可以使用其他按钮调用props.search.openAdvSearch(verifySearchId, true)显示查询框
				})}
				{NCCreateSearch(this.autoVerifySearchId, {
					clickSearchBtn: autoSearchBtnClick.bind(this),
					addAdvTabs: this.autoVerifyAddAdvTabs,
					advSearchClearEve: this.clearSearchEve.bind(this, true),//高级查询面板中的清空按钮 点击事件钩子，用于业务组清除自定义查询条件值
					onlyShowSuperBtn: true, // 只显示高级按钮
					searchBtnName: this.state.json['verificationsheet-000085'] /* 国际化处理： 核销*/,
					hideBtnArea: true
				})}
				<div style={{ borderBottom: '1px solid #d0d0d0' }} />
				<div className="nc-singleTable-table-area&quot;">
					{createCardTable(tableId_bf, {
						onBeforeEvent: this.beforeEvent.bind(this),
						onAfterEvent: tableAfterEvent.bind(this),
						onSelected: this.onSelected.bind(this),
						onSelectedAll: this.onSelectedAll.bind(this),
						showCheck: true,
						showIndex: true,
						pageSize: 10,
						hideSwitch: () => {
							return false;
						}
					})}
				</div>
				<div style={{ borderBottom: '1px solid #d0d0d0' }} />
				<div className="nc-singleTable-table-area&quot;">
					{createCardTable(tableId_df, {
						onBeforeEvent: this.beforeEvent.bind(this),
						onAfterEvent: tableAfterEvent.bind(this),
						onSelected: this.onSelected.bind(this),
						onSelectedAll: this.onSelectedAll.bind(this),
						showCheck: true,
						showIndex: true,
						pageSize: 10,
						hideSwitch: () => {
							return false;
						}
					})}
				</div>
				<div style={{ borderBottom: '1px solid #d0d0d0' }} />
			</div>
		);
	}
}

SingleTable = createPage(
	{
		// initTemplate: initTemplate
	}
)(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
