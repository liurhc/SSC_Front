import React, { Component } from 'react';
import { ajax, base, createPage, toast, sum, getMultiLang } from 'nc-lightapp-front';
import { buttonClick } from '../events';
import SupplementModel from '../supplementModel'; //补差模态框
import { accAdd, Subtr } from '../../../public/components/method';
import { modifierSearchMetas } from '../../../public/components/pubUtils/arapVerificatRefFilter';
import SearchContions from '../../public/searchContions';
import { businessDateH, businessDate, getHMS, getPreMonth } from '../../../public/components/businessDate';
import { updatePaCFV } from '../../../public/components/pubUtils/updatePaCFV';
import './index.less';
const { NCSelect, NCForm, NCBackBtn, NCNumber } = base;
const NCFormItem = NCForm.NCFormItem;
const NCOption = NCSelect.NCOption;
let NCOptionData = [
	{ value: '0', display: '' } /* 国际化处理： 客户*/,
	{ value: '1', display: '' } /* 国际化处理： 供应商*/,
	{ value: '2', display: '' } /* 国际化处理： 部门*/,
	{ value: '3', display: '' } /* 国际化处理： 业务员*/
];
let transferData;
let verificationObjDis = null,
	objTypeValueDis = null;
let verificationObjVal = null,
	objTypeValueVal = null;
let tableid = 'table';
let pagecode = ''; //页面编码
let searchId = 'query';
let appcode = ''; //小应用编码
let pk_org; //财务组织
let billType; //单据类型
let businessDates, businessPreMonthDates, pageCode, appCode;

//获取并初始化模板
let initTemplate = (props, that) => {
	props.createUIDom(
		{
			pagecode: pagecode, //页面id
			appcode: appcode //注册按钮的id
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta);
					props.meta.oid = meta[searchId].oid;
					props.meta.setMeta(meta);
					modifierSearchMetas(searchId, props, meta, billType, pk_org, that);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	);
};

//对表格模板进行加工操作
function modifierMeta(props, meta) {
	meta[tableid].showindex = true; //表格显示序号
	meta[tableid].items = meta[tableid].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'billno') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							billLink(record, props);
						}}
					>
						{record.values.billno.value}
					</a>
				);
			};
		}
		return item;
	});
	return meta;
}

//联查单据
function billLink(text, props) {
	ajax({
		url: '/nccloud/arap/arappub/linkarapbill.do',
		async: false,
		data: {
			tradeType: text.values.pk_tradetype.value,
			pk_bill: text.values.pk_bill.value
		},
		success: (res) => {
			props.openTo(res.data.url, res.data.condition);
		}
	});
}

class Cancellation extends Component {
	constructor(props) {
		super(props);
		this.oldVal = '';
		this.tableId = 'table';
		this.this_sett = '';
		this.defScale = '';
		this.transferDatas = {};
		this.state = {
			json: {},
			verificationObj: { display: '', value: '0' }, //核销对象/* 国际化处理： 客户*/
			objTypeValue: { display: null, value: null }, //核销对象值
			pk_tradetype: { display: null, value: null }, //交易类型
			value: [], //单据日期
			settlement: { display: null, value: null }, //结算金额
			pk_org: null,
			isSupplementModelShow: false, //补差模态框
			modelData: {}, //加载模态框时的数据
			ts: null,
			scale: '',
			schemeData: {
				objType: { display: '', value: 0 }, //核销对象/* 国际化处理： 客户*/
				objTypeValue: { display: '', value: null }, //核销对象值
				pk_tradetype: { display: '', value: null }, //交易类型
				value: [] //单据日期
			}
		};
		this.handleSupplement = this.handleSupplement.bind(this);
		this.onAfterEvent = this.onAfterEvent.bind(this);
	}

	componentWillMount() {
		window.onbeforeunload = () => {
			return '';
		};
		initTemplate(this.props, this);
		pagecode = this.props.getSearchParam('p');
		appcode = this.props.getSearchParam('c');
		transferData = this.props.ViewModel.getData('WholeVerifyDatas');
		if (window.top === window.parent) {
			if (appcode != '20062002' && appcode != '20082002') {
				updatePaCFV(this, '#/nowv');
				pagecode = this.props.getSearchParam('p');
				appcode = this.props.getSearchParam('c');
			}
		}
		if (transferData != undefined) {
			verificationObjDis = transferData.objtype.display;
			verificationObjVal = transferData.objtype.value;
			objTypeValueDis = transferData.objTypeValue.display;
			objTypeValueVal = transferData.objTypeValue.value;
			pk_org = transferData.pk_org.value; //财务组织
			billType = transferData.billType.value; //单据类型
			this.setState({
				ts: transferData.ts.value,
				pk_org: pk_org
			});
		}
		if (!window.top === window.parent) {
			pagecode = this.props.getUrlParam('pagecode');
			appcode = this.props.getUrlParam('appcode');
		}
		let callback = (json) => {
			this.setState({ json: json }, () => {
				NCOptionData = [
					{ value: '0', display: this.state.json['verificationsheet-000000'] } /* 国际化处理： 客户*/,
					{ value: '1', display: this.state.json['verificationsheet-000001'] } /* 国际化处理： 供应商*/,
					{ value: '2', display: this.state.json['verificationsheet-000002'] } /* 国际化处理： 部门*/,
					{ value: '3', display: this.state.json['verificationsheet-000003'] } /* 国际化处理： 业务员*/
				];
				this.setState({
					verificationObj: {
						display: verificationObjDis ? verificationObjDis : this.state.json['verificationsheet-000000'],
						value: verificationObjVal ? verificationObjVal : '0'
					},
					objTypeValue: {
						display: objTypeValueDis ? objTypeValueDis : '',
						value: objTypeValueVal ? objTypeValueVal : null
					}, //核销对象值
					schemeData: {
						objType: { display: this.state.json['verificationsheet-000000'], value: 0 }, //核销对象/* 国际化处理： 客户*/
						objTypeValue: { display: '', value: null }, //核销对象值
						pk_tradetype: { display: '', value: null }, //交易类型
						value: [] //单据日期
					}
				});
				if (transferData == undefined) {
					toast({
						duration: 3,
						color: 'warning',
						content: this.state.json['verificationsheet-000004'] /* 国际化处理： 刷新浏览器会清除数据，请关闭本页签，重新进入！*/
					});
				}
			});
		};
		getMultiLang({
			moduleId: [ 'verificationsheet', 'public' ],
			domainName: 'arap',
			currentLocale: 'simpchn',
			callback
		});
	}

	componentDidMount() {
		let { value, schemeData } = this.state;
		businessDates = businessDate();
		businessPreMonthDates = getPreMonth(businessDates);
		value = [ businessPreMonthDates, businessDates ];
		schemeData.value = [ businessPreMonthDates, businessDates ];
		this.setState({
			value,
			schemeData
		});
		if (transferData != undefined) {
			this.getData();
		}
		pageCode = this.props.getUrlParam('transPageCode');
		appCode = this.props.getUrlParam('transAppCode');
		if (!window.top === window.parent) {
			//审批等状态下
			pageCode = this.props.getUrlParam('pagecode');
			appCode = this.props.getUrlParam('appcode');
		}
	}

	//获取模态框数据
	getModelData = (data) => {
		this.setState({
			modelData: data
		});
	};

	//请求列表数据
	getData = () => {
		//如果showOff为true，则显示停用数据，在请求参数data中增加显示停用参数，根据业务前后端自行处理
		let { ts, settlement } = this.state;
		let verifyConditionVO = {
			objType: verificationObjVal, //核销对象
			objTypeValue: transferData && transferData.objTypeValue.value, //核销对象值
			pk_tradetype: null, //交易类型
			beginDate: businessPreMonthDates, //开始日期
			endDate: businessDates //结束日期
		};
		let data = {
			queryTreeFormatVO: null,
			verifyConditionVO: verifyConditionVO,
			pageId: pagecode,
			pk_bill: transferData && transferData.pk_bill.value,
			pk_item: transferData && transferData.pk_item,
			billType: transferData && transferData.billType.value,
			ts: ts
		};
		ajax({
			url: '/nccloud/arap/billverify/nowVerifyQuery.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let datas = data.grid;
					this.state.ts = data.ts;
					this.defScale = data.scale;
					this.this_sett = this.handleAccuracy(data.this_sett, data.scale);
					this.state.settlement.value = this.handleAccuracy(data.this_sett, data.scale);
					this.setState(
						{
							settlement,
							ts
						},
						() => {
							this.getAccuracy(settlement, data.scale);
						}
					);
					if (datas && datas[tableid]) {
						this.props.editTable.setTableData(tableid, datas[tableid]);
					} else {
						this.props.editTable.setTableData(tableid, { rows: [] });
						this.props.button.setButtonDisabled('Share', true);
					}
				}
			}
		});
	};

	// 获取本次结算精度
	getAccuracy = (settlement, scale) => {
		let settlementVal = settlement.value;
		let scalel;
		if (settlementVal.indexOf('.') != -1) {
			scalel = settlementVal.split('.')[1].length;
		} else {
			scalel = scale;
		}
		this.setState({
			scale: scalel
		});
	};

	//点击查询按钮 重新渲染数据
	handleQueryBtn = () => {
		let { verificationObj, objTypeValue, pk_tradetype, value, ts } = this.state;
		let verifyConditionVO;
		if (value.length > 0) {
			verifyConditionVO = {
				objType: verificationObj.value, //核销对象
				objTypeValue: objTypeValue.value, //对象名称
				pk_tradetype: pk_tradetype.value, //交易类型
				beginDate: value[0], //开始时间
				endDate: value[1] //结束时间
			};
		} else {
			verifyConditionVO = {
				objType: verificationObj.value, //核销对象
				objTypeValue: objTypeValue.value, //对象名称
				pk_tradetype: pk_tradetype.value //交易类型
			};
		}
		let data = {
			pageId: pagecode,
			pk_bill: transferData.pk_bill.value,
			pk_item: transferData.pk_item,
			billType: transferData.billType.value,
			ts: ts,
			verifyConditionVO: verifyConditionVO
		};
		ajax({
			url: '/nccloud/arap/billverify/nowVerifyQuery.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let datas = data.grid;
					this.state.ts = data.ts;
					this.this_sett = this.handleAccuracy(data.this_sett, data.scale);
					this.state.settlement.value = this.handleAccuracy(data.this_sett, data.scale);
					this.setState({
						ts
					});
					if (datas && datas[tableid]) {
						this.props.editTable.setTableData(tableid, datas[tableid]);
						this.props.button.setButtonDisabled('Share', false);
						toast({
							color: 'success',
							content:
								this.state.json['verificationsheet-000126'] +
								datas[tableid].rows.length +
								this.state.json['verificationsheet-000127']
						}); /* 国际化处理： 已成功共几条数据！*/
					} else {
						toast({
							color: 'warning',
							content: this.state.json['verificationsheet-000125']
						}); /* 国际化处理： 未查询出相应的数据！*/
						this.props.editTable.setTableData(tableid, {
							rows: []
						});
						this.props.button.setButtonDisabled('Share', true);
					}
				}
			}
		});
	};

	//处理精度
	handleAccuracy = (str, s) => {
		let integer = str.split('.')[0];
		let decimal = str.split('.')[1].slice(0, Number(s));
		return integer + '.' + decimal;
	};

	//表格编辑后事件
	onAfterEvent(props, moduleId, key, changerows, value, index, data) {
		let money_bal = data.values.money_bal.value;
		if (key == 'this_sett') {
			if (money_bal < 0) {
				if (value[0].newvalue.value * 1 > money_bal * 1 && value[0].newvalue.value * 1 < 0) {
					props.editTable.setValByKeyAndRowId(moduleId, data.rowid, 'this_sett', {
						value: value[0].newvalue.value
					});
				} else {
					props.editTable.setValByKeyAndRowId(moduleId, data.rowid, 'this_sett', { value: money_bal });
				}
			} else {
				if (value[0].newvalue.value * 1 > 0 && value[0].newvalue.value * 1 < money_bal * 1) {
					props.editTable.setValByKeyAndRowId(moduleId, data.rowid, 'this_sett', {
						value: value[0].newvalue.value
					});
				} else {
					props.editTable.setValByKeyAndRowId(moduleId, data.rowid, 'this_sett', { value: money_bal });
				}
			}
		}
	}

	//补差按钮弹出模态框
	handleSupplement = () => {
		this.transferDatas = this.getVerifyNowData();
		this.props.ViewModel.setData('transferCardData', this.transferDatas);
	};

	//清空按钮
	handleEmpty = () => {
		let { objTypeValue, pk_tradetype, value } = this.state;
		objTypeValue = { display: null, value: null };
		pk_tradetype = { display: null, value: null };
		value = [];
		this.setState({
			objTypeValue,
			pk_tradetype,
			value
		});
	};

	//修改高级查询常用条件
	handleAddAdvBodyCons = (schemeData) => {
		this.setState({
			schemeData: schemeData
		});
	};

	//分摊按钮
	handleShare = () => {
		let allDatas = this.props.editTable.getAllRows(tableid, true);
		let { settlement } = this.state;
		let settlementNew = settlement.value.replace('-', '');
		let brrIndex = [];
		let len = allDatas.length;
		for (var j = 0; j < len; j++) {
			allDatas[j].values.this_sett = { value: '' };
			this.props.editTable.setValByKeyAndRowId(tableid, allDatas[j].rowid, 'this_sett', { value: '' });
			brrIndex.push(j);
		}
		setTimeout(() => {
			this.props.editTable.selectTableRows(tableid, brrIndex, false);
		}, 0);
		for (var i = 0; i < len; i++) {
			let this_sett = allDatas[i].values.money_bal.value.replace('-', '');
			let this_sett_t = allDatas[i].values.money_bal.value;
			if (settlementNew.indexOf('-') == -1) {
				if (sum(this_sett, '-' + settlementNew).indexOf('-') == -1) {
					allDatas[i].values.this_sett = { value: this_sett_t > 0 ? settlementNew : '-' + settlementNew };
					settlementNew = sum(settlementNew, '-' + this_sett);
				} else {
					allDatas[i].values.this_sett = { value: this_sett_t };
					settlementNew = sum(settlementNew, '-' + this_sett);
				}
			}
		}
		let arrIndex = [];
		for (var i = 0; i < len; i++) {
			if (allDatas[i].values.this_sett.value * 1 != 0) {
				arrIndex.push(i);
			}
		}
		for (var i = 0; i < len; i++) {
			if (allDatas[i].values.this_sett.value) {
				this.props.editTable.setValByKeyAndRowId(tableid, allDatas[i].rowid, 'this_sett', {
					value: allDatas[i].values.this_sett.value
				});
			}
		}
		setTimeout(() => {
			this.props.editTable.setStatus(tableid, 'edit');
			this.props.editTable.setEditableRowKeyByIndex(tableid, arrIndex, 'this_sett', true);
			this.props.editTable.selectTableRows(tableid, arrIndex, true);
		}, 0);
	};

	//获取选中行数据
	getCheckedDatas = () => {
		let linkVOs = [];
		let editTableData = this.props.editTable.getCheckedRows(tableid);
		for (var i = 0, len = editTableData.length; i < len; i++) {
			let objTypeValue;
			let DataValues = editTableData[i].data.values;
			let objtype = DataValues.objtype.value;
			if (objtype == 0) {
				objTypeValue = DataValues.customer.value;
			} else if (objtype == 1) {
				objTypeValue = DataValues.supplier.value;
			} else if (objtype == 2) {
				objTypeValue = DataValues.pk_deptid.value;
			} else if (objtype == 3) {
				objTypeValue = DataValues.pk_psndoc.value;
			}
			let o = {
				pk_bill: editTableData[i].data.values.pk_bill.value,
				pk_item: (editTableData[i].data.values.pk_item && editTableData[i].data.values.pk_item.value) || '',
				billType: editTableData[i].data.values.pk_billtype.value,
				this_sett: editTableData[i].data.values.this_sett.value,
				objTypeValue: objTypeValue
			};
			linkVOs.push(o);
		}
		return linkVOs;
	};

	//获取及时核销数据
	getVerifyNowData = () => {
		let { settlement } = this.state;
		let editTableData = this.props.editTable.getCheckedRows(tableid);
		let this_sett_sum = 0;
		let len = editTableData.length;
		for (var i = 0; i < len; i++) {
			this_sett_sum = accAdd(this_sett_sum, Math.abs(editTableData[i].data.values.this_sett.value * 1));
		}
		let linkVOs = [];
		if (Math.abs(settlement.value) != this_sett_sum) {
			for (var i = 0; i < len; i++) {
				let objTypeValue;
				let DataValues = editTableData[i].data.values;
				let objtype = DataValues.objtype.value;
				if (objtype == 0) {
					objTypeValue = DataValues.customer.value;
				} else if (objtype == 1) {
					objTypeValue = DataValues.supplier.value;
				} else if (objtype == 2) {
					objTypeValue = DataValues.pk_deptid.value;
				} else if (objtype == 3) {
					objTypeValue = DataValues.pk_psndoc.value;
				}
				let o = {
					pk_bill: editTableData[i].data.values.pk_bill.value,
					pk_item: (editTableData[i].data.values.pk_item && editTableData[i].data.values.pk_item.value) || '',
					billType: editTableData[i].data.values.pk_billtype.value,
					this_sett: editTableData[i].data.values.this_sett.value,
					linkedPk: editTableData[i].data.values.linkedPk.value,
					objTypeValue: objTypeValue
				};
				linkVOs.push(o);
			}
			this.setState({
				isSupplementModelShow: !this.state.isSupplementModelShow
			});
		} else {
			toast({
				duration: 3,
				color: 'warning',
				title: this.state.json['verificationsheet-000006'] /* 国际化处理： 请注意*/,
				content: this.state.json['verificationsheet-000099'] /* 国际化处理： 传入数据的本方金额和对方金额的绝对值相等！*/
			});
			return;
		}
		let data = {
			pageId: this.props.getSearchParam('p'), //页面编码
			pk_bill: transferData.pk_bill.value, //单据主键
			pk_item: transferData.pk_item, //按表体核销时，设置行主键
			billType: transferData.billType.value, //单据类型
			ts: transferData.ts.value, //表头ts
			this_sett: settlement.value, //核销金额
			linkVOs: linkVOs //选中数据
		};
		return data;
	};

	//即时核销按钮
	handleCancellation = () => {
		let { settlement } = this.state;
		let editTableData = this.props.editTable.getCheckedRows(tableid);
		let len = editTableData.length;
		let this_sett_sum = '';
		for (var i = 0; i < len; i++) {
			this_sett_sum = sum(this_sett_sum, editTableData[i].data.values.this_sett.value.replace('-', ''));
		}
		//修改逗号只替换一个的问题
		if (Number(settlement.value.replace('-', '')) == Number(this_sett_sum.replace(/\,/g, ''))) {
			let linkVOs = [];
			for (var i = 0; i < len; i++) {
				let objTypeValue;
				let DataValues = editTableData[i].data.values;
				let objtype = DataValues.objtype.value;
				if (objtype == 0) {
					objTypeValue = DataValues.customer.value;
				} else if (objtype == 1) {
					objTypeValue = DataValues.supplier.value;
				} else if (objtype == 2) {
					objTypeValue = DataValues.pk_deptid.value;
				} else if (objtype == 3) {
					objTypeValue = DataValues.pk_psndoc.value;
				}
				let o = {
					pk_bill: editTableData[i].data.values.pk_bill.value,
					pk_item: (editTableData[i].data.values.pk_item && editTableData[i].data.values.pk_item.value) || '',
					billType: editTableData[i].data.values.pk_billtype.value,
					this_sett: editTableData[i].data.values.this_sett.value,
					linkedPk: editTableData[i].data.values.linkedPk.value,
					objTypeValue: objTypeValue
				};
				linkVOs.push(o);
			}
			let data = {
				pageId: this.props.getSearchParam('p'), //页面编码
				pk_bill: transferData.pk_bill.value, //单据主键
				pk_item: transferData.pk_item, //按表体核销时，设置行主键
				billType: transferData.billType.value, //单据类型
				ts: this.state.ts, //表头ts
				this_sett: settlement.value, //核销金额
				linkVOs: linkVOs //选中数据
			};
			let scene = this.props.getUrlParam('scene');
			let parameter =
				'appcode=' +
				appCode +
				'&pagecode=' +
				pageCode +
				'&c=' +
				appCode +
				'&p=' +
				pageCode +
				'&scene=' +
				scene +
				'&id=' +
				transferData.pk_bill.value +
				'&status=browse';
			ajax({
				url: '/nccloud/arap/billverify/nowVerify.do',
				data: data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (window.top === window.parent) {
							this.props.pushTo('/card', {
								id: transferData.pk_bill.value, //单据主键
								pagecode: pageCode,
								appcode: appCode,
								p: pageCode,
								c: appCode,
								status: 'browse'
							});
						} else {
							window.location.hash = '/card?' + parameter;
							window.parent.location.hash = '/card?' + parameter;
						}
					}
				}
			});
		} else {
			toast({
				duration: 3,
				color: 'warning',
				title: this.state.json['verificationsheet-000006'] /* 国际化处理： 请注意*/,
				content: this.state.json['verificationsheet-000007'] /* 国际化处理： 传入数据的本方金额和对方金额的绝对值不相等！*/
			});
		}
	};

	//点击单个选择框函数
	onSelectedFn = (props, moduleId, record, index, status) => {
		let allCheckedData = this.props.editTable.getAllRows(moduleId);
		this.oldVal = '';
		let this_sett = record.values.money_bal.value;
		if (status) {
			props.editTable.setStatus(tableid, 'edit');
			props.editTable.setValByKeyAndRowId(tableid, allCheckedData[index].rowid, 'this_sett', {
				value: this_sett
			});
			props.editTable.setEditableRowKeyByIndex(tableid, index, 'this_sett', true);
		} else {
			props.editTable.setValByKeyAndRowId(tableid, allCheckedData[index].rowid, 'this_sett', { value: '' });
			props.editTable.setEditableRowKeyByIndex(tableid, index, 'this_sett', false);
			for (let item of allCheckedData) {
				if (item.selected) {
					return;
				}
			}
			props.editTable.setStatus(tableid, 'browse');
		}
	};

	//全选按钮回调
	onSelectedAllFn = (props, moduleId, status, length) => {
		let allCheckedData = this.props.editTable.getAllRows(moduleId);
		let arrIndex = [];
		for (let i = 0; i < length; i++) {
			let this_sett = allCheckedData[i].values.money_bal.value;
			let rowid = allCheckedData[i].rowid;
			arrIndex.push(i);
			if (status) {
				props.editTable.setStatus(tableid, 'edit');
				props.editTable.setValByKeyAndRowId(tableid, rowid, 'this_sett', { value: this_sett });
				props.editTable.setEditableRowKeyByIndex(tableid, arrIndex, 'this_sett', true);
			} else {
				props.editTable.setValByKeyAndRowId(tableid, rowid, 'this_sett', { value: '' });
				props.editTable.setStatus(tableid, 'browse');
				props.editTable.setEditableRowKeyByIndex(tableid, arrIndex, 'this_sett', false);
			}
		}
	};

	//即时核销（表头区以及查询条件区）
	getNoRelation = (NCOptions, createButtonApp, createEditTable, NCCreateSearch) => {
		return (
			<div>
				{SearchContions(this, NCOptions, createButtonApp, NCCreateSearch, searchId)}
				{/* 表格 */}
				<div className={'table-area'}>
					{createEditTable(tableid, {
						//列表区
						onAfterEvent: this.onAfterEvent, // 控件的编辑后事件
						useFixedHeader: true,
						onSelected: this.onSelectedFn, // 左侧选择列单个选择框回调
						onSelectedAll: this.onSelectedAllFn, // 左侧选择列全选回调
						showCheck: true //显示复选框
					})}
				</div>
			</div>
		);
	};

	//返回按钮
	handleBack = () => {
		let id = transferData ? transferData.pk_bill.value : '',
			scene = this.props.getUrlParam('scene'),
			transAppCode = transferData.appcode,
			transPageCode = transferData.pageId;
		let parameter =
			'appcode=' +
			transAppCode +
			'&c=' +
			transAppCode +
			'&pagecode=' +
			transPageCode +
			'&id=' +
			id +
			'&scene=' +
			scene +
			'&status=browse';
		if (window.top === window.parent) {
			if (transferData != undefined) {
				this.props.pushTo('/card', {
					pagecode: transPageCode,
					appcode: transAppCode,
					id: id,
					scene: scene,
					status: 'browse'
				});
			}
		} else {
			window.location.hash = '/card?' + parameter;
			window.parent.location.hash = '/card?' + parameter;
		}
	};

	render() {
		let { button, search, editTable } = this.props;
		let { NCCreateSearch } = search;
		let { createEditTable } = editTable;
		let { createButtonApp } = button;
		const NCOptions = NCOptionData.map((province) => <NCOption value={province}>{province.display}</NCOption>);
		let { settlement, scale } = this.state;
		return (
			<div id="RelationNowId">
				<div className="relation-tit">
					<div className="title-left">
						<NCBackBtn onClick={this.handleBack.bind(this)} />
						<span className="title-style">{this.state.json['verificationsheet-000100']}</span>
						{/* 国际化处理： 即时核销*/}
					</div>
					<div className="title-right">
						<div />
						<div className=" contions-box-set">
							<div className="title-settle">
								{this.state.json['verificationsheet-000012']}：{this.this_sett}
							</div>
							{/* 国际化处理： 可结算*/}
						</div>
						<div className="title-rr">
							<div className="contions-box number-style">
								<NCFormItem
									showMast={false}
									labelName={this.state.json['verificationsheet-000010']}
									isRequire={true}
									inline={true}
								>
									{/* 国际化处理： 本次结算：*/}
									<NCNumber
										style={{ 'text-align': 'left' }}
										className="bill-instruction-input"
										value={settlement.value}
										scale={this.defScale}
										onBlur={(v) => {
											if (this.this_sett * 1 > 0) {
												if (v >= this.this_sett * 1 || v <= 0) {
													settlement.value = this.this_sett;
												}
											}
											if (this.this_sett * 1 < 0) {
												if (v >= 0 || v <= this.this_sett * 1) {
													settlement.value = this.this_sett;
												}
											}
											this.setState({
												settlement
											});
										}}
										onChange={(v) => {
											settlement.value = v;
										}}
									/>
								</NCFormItem>
							</div>
							<div className="header-button-area1">
								{createButtonApp({
									area: 'list_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this)
									//popContainer: document.querySelector('.header-button-area')
								})}
							</div>
						</div>
					</div>
				</div>
				<div className="divider" />
				<div>{this.getNoRelation(NCOptions, createButtonApp, createEditTable, NCCreateSearch)}</div>
				<SupplementModel
					show={this.state.isSupplementModelShow}
					handleModel={this.handleSupplement}
					modelData={this.state.modelData}
					makeUpSelectData={this.getCheckedDatas()}
					transferCardData={this.transferDatas}
					createButtonApp={createButtonApp}
				/>
			</div>
		);
	}
}

Cancellation = createPage({
	mutiLangCode: ''
})(Cancellation);

export default Cancellation;
