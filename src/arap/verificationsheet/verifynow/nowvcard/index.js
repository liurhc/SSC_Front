//主子表卡片
import React, { Component } from 'react';
import { createPage, ajax, base, getMultiLang } from 'nc-lightapp-front';
import { formBeforeEvent } from '../../../public/components/pubUtils/arapFormRefFilter';
import { bodyBeforeEvent } from '../../../public/components/pubUtils/arapTableRefFilter';
let { NCButton } = base;
import { initTemplate, afterEvent } from './events';
let arr = [
	'MONEY_DE',
	'LOCAL_MONEY_DE',
	'QUANTITY_DE',
	'MONEY_BAL',
	'LOCAL_MONEY_BAL',
	'QUANTITY_BAL',
	'LOCAL_TAX_DE',
	'NOTAX_DE',
	'LOCAL_NOTAX_DE',
	'GROUPDEBIT',
	'GROUPTAX_DE',
	'GROUPNOTAX_DE',
	'GLOBALDEBIT',
	'GLOBALTAX_DE',
	'GLOBALNOTAX_DE',
	'QUANTITY_CR',
	'LOCAL_MONEY_CR',
	'MONEY_CR',
	'LOCAL_TAX_CR',
	'NOTAX_CR',
	'LOCAL_NOTAX_CR',
	'GROUPCREBIT',
	'GROUPTAX_CRE',
	'GROUPNOTAX_CRE',
	'GLOBALCREBIT',
	'GLOBALTAX_CRE',
	'GLOBALNOTAX_CRE',
	'OCCUPATIONMNY',
	'PRICE',
	'GROUPBALANCE',
	'GLOBALBALANCE',
	'TAXPRICE',
	'CALTAXMNY',
	'NOSUBTAX',
	'MONEY',
	'GROUPLOCAL',
	'GLOBALLOCAL',
	'PK_CURRTYPE',
	'PK_TRADETYPE',
	'PK_TRADETYPEID',
	'PK_BILLTYPE',
	'RATE',
	'GROUPRATE',
	'GLOBALRATE'
];

//大写转小写方法
let ToLowerCase = () => {
	return arr.map((item, index, array) => {
		return item.toLowerCase();
	});
};
let brr = ToLowerCase();
let billType;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = 'head';
		this.tableId = 'bodys';
		this.pageId = this.props.getUrlParam('tradeType');
		this.state = {
			json: {}
		};
		this.handleConfirm = this.handleConfirm.bind(this);
		this.handleCancel = this.handleCancel.bind(this);
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		getMultiLang({
			moduleId: [ 'verificationsheet', 'public' ],
			domainName: 'arap',
			currentLocale: 'simpchn',
			callback
		});
	}
	componentDidMount() {
		initTemplate.call(this, this.props);
		this.getDatas();
		billType = this.props.getUrlParam('billType');
	}

	getDatas = () => {
		let data = this.props.ViewModel.getData('transToCardDatas').JumpcardDatas;
		ajax({
			url: '/nccloud/arap/billverify/varyMoney.do',
			data: data,
			success: (res) => {
				if (res.data) {
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						this.props.form.setFormItemsDisabled(this.formId, {
							pk_org: true,
							pk_currtype: true,
							approvestatus: true,
							approvedate: true,
							approvedated: true,
							billstatus: true,
							effectstatus: true,
							effectuser: true,
							creator: true,
							effectdate: true,
							src_syscode: true,
							pk_org_v: true,
							rate: true,
							globalrate: true,
							grouprate: true
						});
					}
					if (res.data.body) {
						this.props.cardTable.setStatus(this.tableId, 'edit');
						this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						setTimeout(() => {
							this.props.cardTable.setColEditableByKey(this.tableId, brr, true);
						}, 500);
					}
				} else {
					this.props.form.setAllFormValue({ [this.formId]: { rows: [ { values: {} } ] } });
					this.props.cardTable.setTableData(this.tableId, { rows: [ { values: {} } ] });
				}
			}
		});
	};

	//确定按钮
	handleConfirm = () => {
		//不等于暂存的时候开启检验
		if (!this.props.form.isCheckNow(this.formId)) {
			//表单验证
			return;
		}
		if (!this.props.cardTable.checkTableRequired(this.tableId)) {
			//表格验证
			return;
		}
		let transferCardData = this.props.ViewModel.getData('transToCardDatas').transferCardDatas;
		let WholeVerifyDatas = this.props.ViewModel.getData('transToCardDatas').transferData;
		let id = WholeVerifyDatas.pk_bill == null ? null : WholeVerifyDatas.pk_bill.value;
		let data = {
			cardData: this.props.createMasterChildData(this.pageId, this.formId, this.tableId),
			...transferCardData
		};
		let scene = this.props.getUrlParam('scene');
		let sence = 1;
		if (scene == 'approve' || scene == 'approvesce' || scene == 'zycl') {
			sence = 3;
		}
		ajax({
			url: '/nccloud/arap/billverify/varyMoneyConfirm.do',
			data: data,
			success: (res) => {
				ajax({
					url: '/nccloud/arap/arappub/linkarapbill.do',
					async: false,
					data: {
						pk_bill: WholeVerifyDatas.pk_bill == null ? null : WholeVerifyDatas.pk_bill.value,
						tradeType: WholeVerifyDatas.pk_tradetype.value,
						sence: sence
					},
					async: false,
					success: (res) => {
						this.props.setUrlParam({ status: 'browse' });
						if (window.top === window.parent) {
							this.props.pushTo('/card', {
								pagecode: this.props.getUrlParam('pageCode'),
								appcode: this.props.getUrlParam('appCode'),
								id: id,
								scene: this.props.getUrlParam('scene'),
								status: 'browse'
							});
						} else {
							let parameter =
								'pagecode=' +
								res.data.condition.pagecode +
								'&appcode=' +
								res.data.condition.appcode +
								'&scene=' +
								scene +
								'&p=' +
								res.data.condition.pagecode +
								'&c=' +
								res.data.condition.appcode +
								'&id=' +
								id +
								'&status=browse';
							window.location.hash = '/card?' + parameter;
							window.parent.location.hash = '/card?' + parameter;
						}
					}
				});
			}
		});
	};

	//取消按钮
	handleCancel = () => {
		let WholeVerifyDatas = this.props.ViewModel.getData('transToCardDatas').transferData;
		this.props.ViewModel.setData('WholeVerifyDatas', WholeVerifyDatas);
		if (window.top === window.parent) {
			this.props.pushTo('/nowv', {
				pagecode: this.props.getUrlParam('pageCode'),
				appcode: this.props.getUrlParam('appCode')
			});
		} else {
			let scene = this.props.getUrlParam('scene');
			let parameter =
				'pagecode=' +
				this.props.getUrlParam('pageCode') +
				'&appcode=' +
				this.props.getUrlParam('appCode') +
				'&scene=' +
				scene +
				'&p=' +
				this.props.getUrlParam('pageCode') +
				'&c=' +
				this.props.getUrlParam('appCode');
			window.location.hash = '/nowv?' + parameter;
			window.parent.location.hash = '/nowv?' + parameter;
		}
	};

	render() {
		let { cardTable, form } = this.props;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<div className="nc-bill-header-area">
						<NCButton onClick={this.handleConfirm}>{this.state.json['verificationsheet-000103']}</NCButton>
						{/* 国际化处理： 确认*/}
						<NCButton onClick={this.handleCancel}>{this.state.json['verificationsheet-000049']}</NCButton>
						{/* 国际化处理： 取消*/}
					</div>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onBeforeEvent: formBeforeEvent.bind(this),
							onAfterEvent: afterEvent.bind(this, billType)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(this.tableId, {
							onBeforeEvent: bodyBeforeEvent.bind(this),
							onAfterEvent: afterEvent.bind(this, billType)
						})}
					</div>
				</div>
			</div>
		);
	}
}

Card = createPage({})(Card);

export default Card;
