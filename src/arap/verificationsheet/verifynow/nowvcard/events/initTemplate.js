export default function(props) {
	const that = this;
	props.createUIDom(
		{
			pagecode: this.props.getUrlParam('pagecode'), //页面id
			appcode: this.props.getUrlParam('appcode') //注册按钮的id
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta['head'].status = status;
	//meta['bodys'].status = status;
	meta['bodys'].items.forEach((item, key) => {
		item.width = 150;
	});
	return meta;
}
