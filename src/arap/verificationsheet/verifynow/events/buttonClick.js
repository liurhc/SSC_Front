import { ajax } from 'nc-lightapp-front';
import { headButton, modelButton } from '../../../public/components/pubUtils/buttonName';

export default function buttonClick(props, id) {
	switch (id) {
		case headButton.MakeUp: //补差按钮
			let WholeVerifyDatas = props.ViewModel.getData('WholeVerifyDatas');
			let varimoney = {
				pk_bill: WholeVerifyDatas.pk_bill == null ? null : WholeVerifyDatas.pk_bill.value,
				pk_item: WholeVerifyDatas.pk_item == null ? null : WholeVerifyDatas.pk_item,
				billType: WholeVerifyDatas.billType == null ? null : WholeVerifyDatas.billType.value,
				this_sett: this.state.settlement.value,
				linkVOs: this.getCheckedDatas() || []
			};
			ajax({
				url: '/nccloud/arap/billverify/varyMoneyPanel.do',
				data: varimoney,
				success: (res) => {
					if (res.success) {
						this.getModelData(res.data);
						this.handleSupplement();
					}
				}
			});

			break;
		case headButton.NowVerify: //即时核销按钮
			this.handleCancellation();
			break;
		case headButton.Share: //分摊按钮
			this.handleShare();
			break;
		case headButton.Return: //返回按钮
			this.handleBack();
			break;
		case modelButton.Bill: //单据按钮
			this.handleConfirm();
			break;
		case modelButton.Cancel: //取消按钮
			this.handleCancel();
			break;
		default:
			break;
	}
}
