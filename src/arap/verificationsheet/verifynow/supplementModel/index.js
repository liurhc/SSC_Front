import React, { Component } from 'react';
import { createPage, ajax, base, getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../../public/ReferLoader';
import { buttonClick } from '../events';
import './index.less';
import { fail } from 'assert';
const { NCModal, NCFormControl, NCSelect } = base;
const NCOption = NCSelect.NCOption;
let recOptionData = [ { value: '0', display: '' }, { value: '2', display: '' }, { value: '3', display: '' } ]; //应收单下拉框/* 国际化处理： 客户,部门,业务员*/
let payOptionData = [ { value: '1', display: '' }, { value: '2', display: '' }, { value: '3', display: '' } ]; //应付单下拉框/* 国际化处理： 供应商,部门,业务员*/
let modelData;
let transferData;
let billTypes;
let pageCode, appCode;

class SupplementModel extends Component {
	constructor(props) {
		super(props);
		this.linkVOs = [];
		this.transferCardDatas = {};
		this.state = {
			json: {},
			selectedValue: '' /* 国际化处理： 应收余额表*/,
			schemeData: {
				contactObj: { display: '', value: '' }, //往来对象
				objName: { display: '', value: '' }, //对象名称
				transactionType: { display: null, value: null, code: null }, //交易类型
				supplementMoney: { display: '', value: '' }, //补差金额
				budgetObj: { display: '', value: '' }, //收支项目
				settlementMethod: { display: '', value: '' }, //结算方式
				supplementMoneyType: { value: null } //补差金额类型
			}
		};
	}

	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				recOptionData = [
					{ value: '0', display: this.state.json['verificationsheet-000000'] },
					{ value: '2', display: this.state.json['verificationsheet-000002'] },
					{ value: '3', display: this.state.json['verificationsheet-000003'] }
				]; //应收单下拉框/* 国际化处理： 客户,部门,业务员*/
				payOptionData = [
					{ value: '1', display: this.state.json['verificationsheet-000001'] },
					{ value: '2', display: this.state.json['verificationsheet-000002'] },
					{ value: '3', display: this.state.json['verificationsheet-000003'] }
				]; //应付单下拉框/* 国际化处理： 供应商,部门,业务员*/
				this.setState({
					selectedValue: this.state.json['verificationsheet-000107'] /* 国际化处理： 应收余额表*/
				});
				transferData = this.props.ViewModel.getData('WholeVerifyDatas');
				if (transferData != undefined) {
					billTypes = transferData.billType.value; // 是应付单还是应收单
				} else {
					billTypes == 'F1';
				}
			});
		};
		getMultiLang({
			moduleId: [ 'verificationsheet', 'public' ],
			domainName: 'arap',
			currentLocale: 'simpchn',
			callback
		});
	}

	componentWillReceiveProps(nextProps) {
		let { schemeData } = this.state;
		this.linkVOs = nextProps.makeUpSelectData;
		this.transferCardDatas = nextProps.transferCardData;
		if (nextProps.modelData) {
			schemeData.objName = nextProps.modelData.objName;
			this.setState({
				schemeData
			});
		}
	}

	componentDidMount() {
		let { schemeData } = this.state;
		let { contactObj } = schemeData;
		(pageCode = this.props.getSearchParam('p')), (appCode = this.props.getSearchParam('c'));
		if (billTypes == 'F1' || billTypes == 'F3') {
			contactObj.display = this.state.json['verificationsheet-000001']; /* 国际化处理： 供应商*/
			contactObj.value = '1';
		} else {
			contactObj.display = this.state.json['verificationsheet-000000']; /* 国际化处理： 客户*/
			contactObj.value = '0';
		}
		this.setState({
			schemeData
		});
		pageCode = this.props.getUrlParam('transPageCode');
		appCode = this.props.getUrlParam('transAppCode');
		if (!window.top === window.parent) {
			//审批等状态下
			pageCode = this.props.getUrlParam('pagecode');
			appCode = this.props.getUrlParam('appcode');
		}
		if (this.props.getUrlParam('scene') == 'bz') {
			pageCode = this.props.getUrlParam('transPageCode');
			appCode = this.props.getUrlParam('transAppCode');
		}
	}

	//单据
	handleConfirm = () => {
		let that = this;
		let { schemeData } = this.state;
		let {
			contactObj,
			transactionType,
			supplementMoney,
			objName,
			budgetObj,
			settlementMethod,
			supplementMoneyType
		} = schemeData;
		let varyMoneyFilterVO = {
			billType: supplementMoneyType.value /* 单据类型 */,
			tradeType: transactionType.code /* 交易类型 */,
			tradeTypeid: transactionType.value /* 交易类型 */,
			m_dVaryYb: supplementMoney.value /* 补差金额Yb */,
			m_dVaryBb: null /* 补差金额Bb */,
			m_iWldx: contactObj.value /* 往来对象 */,
			m_sHbbm: null /* 客商基本档案Pk */,
			supplier: null /* 供应商Pk */,
			m_sBmbm: null /* 部门 */,
			m_sYwybm: null /* 业务员 */,
			pk_subjcode: budgetObj.value,
			pj_jsfs: settlementMethod.value
		};
		switch (contactObj.value) {
			case '0':
				varyMoneyFilterVO.m_sHbbm = objName.value;
				break;
			case '1':
				varyMoneyFilterVO.supplier = objName.value;
				break;
			case '2':
				varyMoneyFilterVO.m_sBmbm = objName.value;
				break;
			case '3':
				varyMoneyFilterVO.m_sYwybm = objName.value;
				break;
		}

		ajax({
			url: '/nccloud/arap/arappub/linkarapbill.do',
			async: false,
			data: {
				billType: supplementMoneyType.value,
				tradeType: transactionType.code,
				pk_bill: transferData.pk_bill.value
			},
			success: (res) => {
				let data = {
					pk_bill: transferData.pk_bill == null ? null : transferData.pk_bill.value,
					pk_item: transferData.pk_item == null ? null : transferData.pk_item,
					billType: transferData.billType.value,
					this_sett: transferData.occupationmnySum == null ? null : transferData.occupationmnySum,
					result: 3,
					pageId: transactionType.code,
					ts: transferData.ts.value,
					linkVOs: this.linkVOs || [],
					pk_subjcode: budgetObj.value,
					pj_jsfs: settlementMethod.value,
					varyMoneyFilterVO: varyMoneyFilterVO
				};
				let transToCardDatas = {
					JumpcardDatas: data,
					transferCardDatas: this.props.ViewModel.getData('transferCardData'),
					transferData: transferData
				};
				that.props.ViewModel.setData('transToCardDatas', transToCardDatas);
				if (window.top === window.parent) {
					that.props.pushTo('/nowvcard', {
						pagecode: transactionType.code,
						appcode: res.data.condition.appcode,
						tradeType: transactionType.code,
						billType: supplementMoneyType.value,
						status: 'edit',
						pageCode: pageCode,
						appCode: appCode,
						scene: this.props.getUrlParam('scene')
					});
				} else {
					let scene = this.props.getUrlParam('scene');
					let parameter =
						'pagecode=' +
						transactionType.code +
						'&appcode=' +
						res.data.condition.appcode +
						'&tradeType=' +
						transactionType.code +
						'&billType=' +
						supplementMoneyType.value +
						'&scene=' +
						scene +
						'&pageCode=' +
						pageCode +
						'&appCode=' +
						appCode +
						'&p=' +
						transactionType.code +
						'&c=' +
						res.data.condition.appcode +
						'&status=edit';
					window.location.hash = '/nowvcard?' + parameter;
					window.parent.location.hash = '/nowvcard?' + parameter;
				}
			}
		});
		this.props.handleModel();
	};

	//取消
	handleCancel = () => {
		let { schemeData } = this.state;
		schemeData.transactionType = { display: null, value: null, code: null }; //交易类型
		schemeData.supplementMoney = { display: '', value: '' }; //补差金额
		this.setState({
			schemeData
		});
		this.props.handleModel();
	};

	//参照展示
	getRefer = (tag, refcode, key, mainData, name, num) => {
		return (
			<div className="finance">
				<span className="lable">{name}</span>
				<div className="finance-refer">
					<Referloader
						tag={tag}
						refcode={refcode}
						value={{
							refname: key.display,
							refpk: key.value
						}}
						onChange={(v) => {
							key.value = v.refpk;
							key.display = v.refname;
							this.setState(
								{
									mainData
								},
								() => {
									if (num == 1 && this.state.mainData.transactionType.value) {
										this.getTransTypeData();
									}
								}
							);
						}}
						queryCondition={() => {
							if (tag == 'InoutBusiClassTreeRef') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									pk_org: transferData.pk_org == null ? null : transferData.pk_org.value
								};
							} else if (tag == 'BalanceTypeGridRef') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									pk_org: transferData.pk_org == null ? null : transferData.pk_org.value
								};
							}
						}}
					/>
				</div>
			</div>
		);
	};

	//交易类型参照
	getTranstypeRefer = (tag, refcode, key, mainData, name, num) => {
		return (
			<div className="finance">
				<span className="lable">{name}</span>
				<div className="finance-refer">
					<Referloader
						tag={tag}
						refcode={refcode}
						value={{
							refname: key.display,
							refpk: key.value
						}}
						onChange={(v) => {
							key.value = v.refpk;
							key.display = v.refname;
							key.code = v.refcode;
							this.setState(
								{
									mainData
								},
								() => {
									if (num == 1 && this.state.mainData.transactionType.value) {
										this.getTransTypeData();
									}
								}
							);
						}}
						queryCondition={() => {
							if (tag == 'Transtype') {
								if (transferData.billType.value == 'F0' || transferData.billType.value == 'F2') {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										parentbilltype: 'F0,F2'
									};
								} else {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										parentbilltype: 'F1,F3'
									};
								}
							}
						}}
					/>
				</div>
			</div>
		);
	};

	getReferr = (tag, refcode, key, mainData, name, showUnitFlg) => {
		let WholeVerifyDatas = this.props.ViewModel.getData('WholeVerifyDatas');
		return (
			<Referloader
				tag={tag}
				refcode={refcode}
				value={{
					refname: key.display,
					refpk: key.value
				}}
				onChange={(v) => {
					key.value = v.refpk;
					key.display = v.refname;
					this.setState({
						mainData
					});
				}}
				placeholder={key.display || name}
				isShowUnit={showUnitFlg}
				//单元组织的条件过滤
				unitCondition={() => {
					if (tag == 'DeptTreeRef') {
						return {
							pkOrgs: WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value,
							TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						};
					} else if (tag == 'PsndocTreeGridRef') {
						return {
							pkOrgs: WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value,
							TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						};
					}
				}}
				queryCondition={() => {
					if (tag == 'SupplierRefTreeGridRef') {
						return {
							DataPowerOperationCode: 'fi', //使用权组
							isDataPowerEnable: 'Y',
							pk_org: WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value
						};
					} else if (tag == 'DeptTreeRef') {
						return {
							busifuncode: 'all',
							DataPowerOperationCode: 'fi', //使用权组
							isDataPowerEnable: 'Y',
							pk_org: WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value
						};
					} else if (tag == 'PsndocTreeGridRef') {
						return {
							busifuncode: 'all',
							DataPowerOperationCode: 'fi', //使用权组
							isDataPowerEnable: 'Y',
							pk_org: WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value
						};
					} else if (tag == 'CustomerDefaultTreeGridRef') {
						return {
							DataPowerOperationCode: 'fi', //使用权组
							isDataPowerEnable: 'Y',
							pk_org: WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value
						};
					}
				}}
			/>
		);
	};

	//对象名称显示不同参照
	getDifferentRefer = (schemeData) => {
		let { contactObj, objName } = schemeData;
		switch (contactObj.value) {
			case '0':
				return this.getReferr(
					'CustomerDefaultTreeGridRef',
					'uapbd/refer/customer/CustomerDefaultTreeGridRef/index.js',
					objName,
					schemeData,
					this.state.json['verificationsheet-000000'] /* 国际化处理： 客户*/,
					false
				);
				break;
			case '1':
				return this.getReferr(
					'SupplierRefTreeGridRef',
					'uapbd/refer/supplier/SupplierRefTreeGridRef/index.js',
					objName,
					schemeData,
					this.state.json['verificationsheet-000001'] /* 国际化处理： 供应商*/,
					false
				);
				break;
			case '2':
				return this.getReferr(
					'DeptTreeRef',
					'uapbd/refer/org/DeptTreeRef/index.js',
					objName,
					schemeData,
					this.state.json['verificationsheet-000002'] /* 国际化处理： 部门*/,
					false
				);
				break;
			case '3':
				return this.getReferr(
					'PsndocTreeGridRef',
					'uapbd/refer/psninfo/PsndocTreeGridRef/index.js',
					objName,
					schemeData,
					this.state.json['verificationsheet-000003'] /* 国际化处理： 业务员*/,
					false
				);
				break;
			default:
				break;
		}
	};

	//选择交易类型 请求数据时间
	getTransTypeData = () => {
		let { schemeData } = this.state;
		let { transactionType } = schemeData;
		let data = {
			tradeType: transactionType.value
		};
		ajax({
			url: '/nccloud/arap/billverify/getbillType.do',
			data: data,
			success: (res) => {
				if (res.success) {
					this.state.schemeData.supplementMoneyType.value = res.data;
					this.state.schemeData.contactObj = modelData.contactObj;
					this.state.schemeData.objName = modelData.objName;
					this.setState(
						{
							schemeData
						},
						() => {
							let transactionType = modelData.transactionType;
							let o = {};
							for (var i = 0, len = transactionType.length; i < len; i++) {
								o[transactionType[i].display] = transactionType[i].value;
							}
							switch (this.state.schemeData.supplementMoneyType.value) {
								case 'F0':
									this.state.schemeData.supplementMoney.value = o.F0;
									this.setState({
										schemeData
									});
									break;
								case 'F1':
									this.state.schemeData.supplementMoney.value = o.F1;
									this.setState({
										schemeData
									});
									break;
								case 'F2':
									this.state.schemeData.supplementMoney.value = o.F2;
									this.setState({
										schemeData
									});
									break;
								case 'F3':
									this.state.schemeData.supplementMoney.value = o.F3;
									this.setState({
										schemeData
									});
									break;
								case '':
									this.state.schemeData.supplementMoney.value = o[''];
									this.setState({
										schemeData
									});
									break;
							}
						}
					);
				}
			}
		});
	};

	//不可编辑input展示
	getInput = (name, key) => {
		return (
			<div className="finance">
				<span className="lable">{name}</span>
				<div className="finance-refer">
					<NCFormControl value={key.value} />
				</div>
			</div>
		);
	};

	render() {
		let { show, createButtonApp } = this.props;
		let { schemeData } = this.state;
		let { transactionType, supplementMoney, budgetObj, settlementMethod, contactObj } = schemeData;
		modelData = this.props.modelData;
		let NCOptionData = billTypes == 'F1' || billTypes == 'F3' ? payOptionData : recOptionData;
		const NCOptions = NCOptionData.map((province) => <NCOption value={province}>{province.display}</NCOption>);
		return (
			<div>
				<NCModal
					id="supplemodalId"
					className="query-modal"
					show={show}
					onHide={this.handleCancel}
					backdrop={true}
				>
					<NCModal.Header closeButton>
						<NCModal.Title> {this.state.json['verificationsheet-000111']} </NCModal.Title>
						{/* 国际化处理： 即时核销补差处理*/}
					</NCModal.Header>
					<NCModal.Body>
						<div className="steps-content">
							<div>
								{this.getTranstypeRefer(
									'Transtype',
									'uap/refer/riart/transtype/index.js',
									transactionType,
									schemeData,
									this.state.json['verificationsheet-000016'] /* 国际化处理： 交易类型*/,
									1
								)}
							</div>
							<div>{this.getInput(this.state.json['verificationsheet-000108'], supplementMoney)}</div>
							{/* 国际化处理： 补差金额*/}
							<div>
								<div className="finance">
									<span className="lable">{this.state.json['verificationsheet-000112']}</span>
									{/* 国际化处理： 往来对象*/}
									<div className="finance-refer">
										<NCSelect
											value={contactObj.display && contactObj.display}
											onChange={(v) => {
												contactObj.display = v.display;
												contactObj.value = v.value;
												schemeData.objName = { display: null, value: null };
												this.setState({
													schemeData
												});
											}}
										>
											{NCOptions}
										</NCSelect>
									</div>
								</div>
							</div>
							<div>
								{modelData &&
								modelData.objName && (
									<div className="finance">
										<span className="lable">{this.state.json['verificationsheet-000021']}</span>
										{/* 国际化处理： 对象名称*/}
										<div className="finance-refer">{this.getDifferentRefer(schemeData)}</div>
									</div>
								)}
							</div>
							<div>
								{this.getRefer(
									'InoutBusiClassTreeRef',
									'uapbd/refer/fiacc/InoutBusiClassTreeRef/index.js',
									budgetObj,
									schemeData,
									this.state.json['verificationsheet-000109'] /* 国际化处理： 收支项目*/
								)}
							</div>
							<div>
								{this.getRefer(
									'BalanceTypeGridRef',
									'uapbd/refer/sminfo/BalanceTypeGridRef/index.js',
									settlementMethod,
									schemeData,
									this.state.json['verificationsheet-000110'] /* 国际化处理： 结算方式*/
								)}
							</div>
						</div>
					</NCModal.Body>
					<NCModal.Footer>
						{createButtonApp({
							area: 'list_bottom',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</NCModal.Footer>
				</NCModal>
			</div>
		);
	}
}

SupplementModel = createPage({})(SupplementModel);

export default SupplementModel;
