import { cytjCode, hxfsCode, hxgzCode, searchId, bfCode, dfCode, publicCode } from '../constants';
import { setFormEditable } from '../../../../public/components/pubUtils/billPubUtil.js';
import afterEvent from './afterEvent';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';

export function buttonClick(props, id) {
    switch (id) {
        case 'Add':
            props.search.openAdvSearch(searchId, true , ()=>{
                props.search.clearSearchArea(searchId);
            
                setDefaultValue(this, props, cytjCode, bfCode, dfCode, publicCode, hxgzCode)

                let pk_org = getContext(loginContextKeys.pk_org);
                let org_Name = getContext(loginContextKeys.org_Name);
                if (pk_org && org_Name) {
                    props.form.setFormItemsValue(cytjCode, { 'pk_org': { value: pk_org, display: org_Name } });
                    afterEvent.call(this, props, cytjCode, 'pk_org', { value: pk_org, display: org_Name })
                    return
                }
               
                //新增，常用条件，组织为空，只有名称、描述、组织可编辑
                setFormEditable(props, cytjCode, cytjCode, false);
                setFormEditable(props, cytjCode, bfCode, false);
                setFormEditable(props, cytjCode, dfCode, false);
                setFormEditable(props, cytjCode, publicCode, false);
                props.form.setFormItemsDisabled(cytjCode, { name: false, pk_org: false, descripe: false });
    
            });

            break;
        default:
            break
    }
}


export function setDefaultValue(that, props, cytjCode, bfCode, dfCode, publicCode, hxgzCode, hxfsCode) {
    //设置默认值
    props.form.EmptyAllFormValue(cytjCode);
    props.form.EmptyAllFormValue(hxgzCode);
    props.form.EmptyAllFormValue(hxfsCode);

    // 本方核销对象		对方核销对象
    props.form.setFormItemsValue(cytjCode, { 'jf_hxdxCBox': { value: 'kh', display: that.state.json['verificationsheet-000000'] }, 'df_hxdxCBox': {  value: 'kh', display: that.state.json['verificationsheet-000000']} });/* 国际化处理： 客户,客户*/
    //	本方单据类型	对方单据类型
    props.form.setFormItemsValue(cytjCode, { 'jfBillTypeBox': { value: 'khys', display: that.state.json['verificationsheet-000040'] }, 'dfBillTypeBox': { value: 'khsk', display: that.state.json['verificationsheet-000044'] } });/* 国际化处理： 应收单,收款单*/
    // 红蓝对冲	 同币种核销 
    props.form.setFormItemsValue(hxgzCode, { 'hldcChkBox': { value: true, display: that.state.json['verificationsheet-000062'] }, 'tbzhxChkBox': { value: true, display: that.state.json['verificationsheet-000062'] } });/* 国际化处理： 是,是*/

    //设置本方对象名称、对方对象名称参照
    let meta = props.meta.getMeta();
    meta[bfCode].items.map((item) => {
        if (item.attrcode == 'jf_dxmcRef') {
            item.itemtype = 'refer'
            item.refName = that.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
            item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
        }
        return item;
    })
    meta[dfCode].items.map((item) => {
        if ( item.attrcode == 'df_dxmcRef' ) {
            item.itemtype = 'refer'
            item.refName = that.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
            item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
        }
        return item;
    })
    props.renderItem('form', bfCode, 'jf_dxmcRef', null);
    props.renderItem('form', dfCode, 'df_dxmcRef', null);
    
    //设置金额方向不可编辑
    props.form.setFormItemsDisabled(hxgzCode, { jefxCmbBox: true });
    that.pk_bill = null;
    that.ts = null;
}