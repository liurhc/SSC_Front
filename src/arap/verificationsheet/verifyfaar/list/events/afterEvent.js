import { ajax } from 'nc-lightapp-front';
import { cytjCode, hxfsCode, hxgzCode, pageCode, searchId, tableId, funCode,bfCode, dfCode, publicCode } from '../constants';
import { setFormEditable } from '../../../../public/components/pubUtils/billPubUtil.js';

export default function afterEvent(props, moduleId, key, value, oldValue) {
	// props, moduleId(区域id), key(操作的键), value（当前值），oldValue(旧值)
	//常用条件编辑后事件
	if (moduleId == cytjCode) {

		if (key == 'pk_org' && value.value != null && value.value != "") {
			setFormEditable(props, cytjCode, cytjCode, true);
			setFormEditable(props, cytjCode, bfCode, true);
			setFormEditable(props, cytjCode, dfCode, true);
			setFormEditable(props, cytjCode, publicCode, true);
			setOrgLocalCurrPK(props, key, value);
			props.form.setFormItemsRequired(hxgzCode,{'jefxCmbBox':false})
		}

		// 本方核销对象
		if (key == 'jf_hxdxCBox') {
			let meta = props.meta.getMeta()
			if (value.value == 'gys') {
				meta[bfCode].items.map((item) => {
					if (item.attrcode == 'jf_dxmcRef') {
						item.itemtype = 'refer'
						item.refName = this.state.json['verificationsheet-000039']/* 国际化处理： 供应商档案*/
						item.refcode = 'uapbd/refer/supplier/SupplierRefTreeGridRef/index'
						item.queryCondition = () => {
							return {
								DataPowerOperationCode: 'fi',//使用权组
								isDataPowerEnable: 'Y',
								pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
							}
						}
					}
					return item;
				})
				meta[dfCode].items.map((item) => {
					if (item.attrcode == 'df_dxmcRef') {
						item.itemtype = 'refer'
						item.refName = this.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
						item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
						item.queryCondition = () => {
							return {
								DataPowerOperationCode: 'fi',//使用权组
								isDataPowerEnable: 'Y',
								pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
							}
						}
					}
					return item;
				})
				props.form.setFormItemsValue(cytjCode, { 'jfBillTypeBox': { value: 'gysfk', display: this.state.json['verificationsheet-000043'] } });/* 国际化处理： 付款单*/
				props.form.setFormItemsValue(cytjCode, { 'jf_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsValue(cytjCode, { 'df_hxdxCBox': { value: 'kh', display: this.state.json['verificationsheet-000000'] } });/* 国际化处理： 客户*/
				props.form.setFormItemsValue(cytjCode, { 'df_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsValue(cytjCode, { 'dfBillTypeBox': { value: 'khsk', display: this.state.json['verificationsheet-000044'] } });/* 国际化处理： 收款单*/
				props.form.setFormItemsDisabled(cytjCode, { df_hxdxCBox: true });
			}
			if (value.value == 'kh' || value.value == 'bm' || value.value == 'ywy') {
				if (value.value == 'kh') {
					meta[bfCode].items.map((item) => {
						if (item.attrcode == 'jf_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
							item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
								}
							}
						}

						return item;
					})
				} else if (value.value == 'bm') {
					meta[bfCode].items.map((item) => {
						if (item.attrcode == 'jf_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000002']/* 国际化处理： 部门*/
							item.refcode = 'uapbd/refer/org/DeptTreeRef/index'
							item.isShowUnit = true,
							item.unitCondition = () => {
								return {
									pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
									TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
								}
							}
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									busifuncode:'all',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
								}
							}
						}

						return item;
					})
				} else if (value.value == 'ywy') {
					meta[bfCode].items.map((item) => {
						if (item.attrcode == 'jf_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000042']/* 国际化处理： 人员*/
							item.refcode = 'uapbd/refer/psninfo/PsndocTreeGridRef/index'
							item.isShowUnit = true,
							item.unitCondition = () => {
								return {
									pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
									TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
								}
							}
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									busifuncode:'all',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
									pk_dept: props.form.getFormItemsValue(cytjCode, 'deptRef').value ? props.form.getFormItemsValue(cytjCode, 'deptRef').value : null
								}
							}
						}

						return item;
					})
				}
				props.form.setFormItemsValue(cytjCode, { 'jfBillTypeBox': { value: 'khys', display: this.state.json['verificationsheet-000040'] } });/* 国际化处理： 应收单*/
				props.form.setFormItemsValue(cytjCode, { 'jf_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsValue(cytjCode, { 'df_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsDisabled(cytjCode, { df_hxdxCBox: false });
			}
			props.renderItem('form', bfCode, 'jf_dxmcRef', null);
			props.renderItem('form', dfCode, 'df_dxmcRef', null);
			
		}

		// 对方核销对象
		if (key == 'df_hxdxCBox') {
			let meta = props.meta.getMeta()
			if (value.value == 'gys') {
				meta[dfCode].items.map((item) => {
					if (item.attrcode == 'df_dxmcRef') {
						item.itemtype = 'refer'
						item.refName = this.state.json['verificationsheet-000039']/* 国际化处理： 供应商档案*/
						item.refcode = 'uapbd/refer/supplier/SupplierRefTreeGridRef/index'
						item.queryCondition = () => {
							return {
								DataPowerOperationCode: 'fi',//使用权组
								isDataPowerEnable: 'Y',
								pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
							}
						}
					}
					return item;
				})
				meta[bfCode].items.map((item) => {
					if (item.attrcode == 'jf_dxmcRef') {
						item.itemtype = 'refer'
						item.refName = this.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
						item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
						item.queryCondition = () => {
							return {
								DataPowerOperationCode: 'fi',//使用权组
								isDataPowerEnable: 'Y',
								pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
							}
						}
					}
					return item;
				})
				props.form.setFormItemsValue(cytjCode, { 'dfBillTypeBox': { value: 'gysyf', display: this.state.json['verificationsheet-000041'] } });/* 国际化处理： 应付单*/
				props.form.setFormItemsValue(cytjCode, { 'df_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsValue(cytjCode, { 'jf_hxdxCBox': { value: 'kh', display: this.state.json['verificationsheet-000000'] } });/* 国际化处理： 客户*/
				props.form.setFormItemsValue(cytjCode, { 'jf_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsDisabled(cytjCode, { jf_hxdxCBox: true });
			}
			if (value.value == 'kh' || value.value == 'bm' || value.value == 'ywy') {
				if (value.value == 'kh') {
					meta[dfCode].items.map((item) => {
						if (item.attrcode == 'df_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
							item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
								}
							}
						}

						return item;
					})
				} else if (value.value == 'bm') {
					meta[dfCode].items.map((item) => {
						if (item.attrcode == 'df_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000002']/* 国际化处理： 部门*/
							item.refcode = 'uapbd/refer/org/DeptTreeRef/index'
							item.isShowUnit = true,
							item.unitCondition = () => {
								return {
									pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
									TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
								}
							}
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									busifuncode:'all',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
								}
							}
						}

						return item;
					})
				} else if (value.value == 'ywy') {
					meta[dfCode].items.map((item) => {
						if (item.attrcode == 'df_dxmcRef') {
							item.itemtype = 'refer'
							item.refName = this.state.json['verificationsheet-000042']/* 国际化处理： 人员*/
							item.refcode = 'uapbd/refer/psninfo/PsndocTreeGridRef/index'
							item.isShowUnit = true,
							item.unitCondition = () => {
								return {
									pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
									TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
								}
							}
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									busifuncode:'all',
									pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
									pk_dept: props.form.getFormItemsValue(cytjCode, 'deptRef').value ? props.form.getFormItemsValue(cytjCode, 'deptRef').value : null
								}
							}
						}
						return item;
					})
				}
				props.form.setFormItemsValue(cytjCode, { 'dfBillTypeBox': { value: 'khsk', display: this.state.json['verificationsheet-000044'] } });/* 国际化处理： 收款单*/
				props.form.setFormItemsValue(cytjCode, { 'jf_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsValue(cytjCode, { 'df_dxmcRef': { value: null, display: null } });
				props.form.setFormItemsDisabled(cytjCode, { jf_hxdxCBox: false });
			}
			//重新渲染对方对象名称
			props.renderItem('form', bfCode, 'jf_dxmcRef', null);
			props.renderItem('form', dfCode, 'df_dxmcRef', null);
		}


		//设置本方单据类型	对方单据类型不可编辑
		props.form.setFormItemsDisabled(cytjCode, { jfBillTypeBox: true });
		props.form.setFormItemsDisabled(cytjCode, { dfBillTypeBox: true });
		if (props.form.getFormItemsValue(cytjCode, 'jf_hxdxCBox').value == 'kh' && props.form.getFormItemsValue(cytjCode, 'df_hxdxCBox').value == 'kh' && props.form.getFormItemsValue(cytjCode, 'pk_org').value) {
			props.form.setFormItemsDisabled(cytjCode, { jfBillTypeBox: false });
		}
	}

	//核销规则编辑后事件
	if (moduleId == hxgzCode) {
		//红蓝对冲
		if (key == 'hldcChkBox') {
			if (value.value == true) {
				props.form.setFormItemsRequired(hxgzCode,{'jefxCmbBox':false})
				props.form.setFormItemsDisabled(hxgzCode, { jefxCmbBox: true });
				props.form.setFormItemsValue(hxgzCode, { 'jefxCmbBox': { value: null, display: null } });
			} else {
				props.form.setFormItemsRequired(hxgzCode,{'jefxCmbBox':true})
				props.form.setFormItemsDisabled(hxgzCode, { jefxCmbBox: false });
				props.form.setFormItemsValue(hxgzCode, { 'jefxCmbBox': { value: 0, display: this.state.json['verificationsheet-000025'] } });/* 国际化处理： 正*/
			}
		}

		//同币种核销，控制异币种核销信息字段不可编辑
		if (key == 'tbzhxChkBox') {
			if (value.value == true) {
				props.form.setFormItemsDisabled(hxgzCode, {
					zjbzRef: true, yxwcTxtF: true,
					jfbzRef: true, jfbz_dzjbzhlTxtF: true,
					jfbz_dbbhlTxtF: true, dfbzRef: true,
					dfbz_dbbhlTxtF: true, dfbz_dzjbzhlTxtF: true,
				});

			}
		}



	}

	//核销方式编辑后事件
	if (moduleId == hxfsCode) {

	}


}

//根据组织设置默认核销币种
function setOrgLocalCurrPK(props, key, value) {
	if (key == 'pk_org') {
		if (value == null || value.value == null) {
			props.form.setFormItemsValue(hxgzCode, { 'hxbzRef': { value: null, display: null } });
		} else {
			ajax({
				url: '/nccloud/arap/verifyfa/afterEvent.do',
				data: {
					key: key,
					value: value.value
				},
				success: (res) => {
					if (res.success) {
						if (res.data) {
							props.form.setFormItemsValue(hxgzCode, { 'hxbzRef': { value: res.data.value, display: res.data.name } });
						}
					}
				}
			});
		}
	}

}


