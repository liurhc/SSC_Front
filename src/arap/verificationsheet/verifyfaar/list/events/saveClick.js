import { ajax,toast,promptBox } from 'nc-lightapp-front';
import { funCode, searchId, pageCode, tableId, cytjCode, hxgzCode, hxfsCode } from '../constants';

//点击查询，获取查询区数据
export default function saveClick(props, searchVal) {
    let that = this
    let data = null;
    let url = null;
    let pk_bill = this.pk_bill;
    let ts = this.ts;
    if(!validate(props,that)){
        return true;
    };
    if(pk_bill != null && ts != null){
        data = {
            ofternConditionsVO: {
                model: props.form.getAllFormValue(cytjCode),
                pageid: pageCode
            },
            verifyRulesVO: {
                model: props.form.getAllFormValue(hxgzCode),
                pageid: pageCode
            },
            verifyModeVO: {
                model: props.form.getAllFormValue(hxfsCode),
                pageid: pageCode
            },
            pageId: pageCode,
            funCode: funCode,
            pk_bill:pk_bill,
            ts : ts
        };
        url='/nccloud/arap/verifyfa/modify.do'
    }else{
        //前端需要传递（cytjvo	hxgzvo	hsfsvo  hsfsvo	funCode	 pageId）
        data = {
            ofternConditionsVO: {
                model: props.form.getAllFormValue(cytjCode),
                pageid: pageCode
            },
            verifyRulesVO: {
                model: props.form.getAllFormValue(hxgzCode),
                pageid: pageCode
            },
            verifyModeVO: {
                model: props.form.getAllFormValue(hxfsCode),
                pageid: pageCode
            },
            pageId: pageCode,
            funCode: funCode
        };
        url='/nccloud/arap/verifyfa/add.do'
    }

    ajax({
        url: url,
        data: data,
        success: (res) => {
            if (res.success) {
                if (res.data) {
                    props.editTable.setTableData(tableId, res.data[tableId]);
                }
            }
        },
        error: function(data) {
            toast({color:"danger",content:data.message})
        },
    });

};

function validate(props,that){
    //修改保存
    if(!props.form.isCheckNow(cytjCode,'warning')){
        return false;
    }
    let hldcChkBox = props.form.getFormItemsValue(hxgzCode, 'hldcChkBox').value;// 红蓝对冲
    let tbzhxChkBox = props.form.getFormItemsValue(hxgzCode, 'tbzhxChkBox').value;// 同币种核销
    let hxbzRef = props.form.getFormItemsValue(hxgzCode, 'hxbzRef').value;
    if (!hldcChkBox && !tbzhxChkBox ) {
        promptBox({ color: "warning", content: that.state.json['verificationsheet-000091'], noCancelBtn: true })/* 国际化处理： 红蓝对冲,同币种核销 至少选择一种*/
        return false;
    }
    if(hxbzRef == null || hxbzRef == ""){
        if(hldcChkBox && tbzhxChkBox){
            promptBox({color:"warning",content:that.state.json['verificationsheet-000092'],noCancelBtn: true})/* 国际化处理： 红蓝对冲，同币种核销：核销币种必须设置*/
        } else if (hldcChkBox == null && tbzhxChkBox) {
            promptBox({color:"warning",content:that.state.json['verificationsheet-000093'],noCancelBtn: true})/* 国际化处理： 同币种核销：核销币种必须设置*/
        }
        return false;
    }
    return true;
}
