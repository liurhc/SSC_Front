import { ajax, base, createPage, toast, getMultiLang, createPageIcon } from 'nc-lightapp-front';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { setFormEditable } from '../../../public/components/pubUtils/billPubUtil.js';
import {
	cytjCode,
	funCode,
	hxfsCode,
	hxgzCode,
	pageCode,
	searchId,
	tableId,
	bfCode,
	dfCode,
	publicCode
} from './constants';
import { initTemplate, saveClick, afterEvent, buttonClick, setDefaultValue, beforeEvent } from './events';
import './index.less';

/**
 *  应付管理，核销方案设置
 */
class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.searchId = searchId;
		this.tableId = tableId;
		this.cytjCode = cytjCode;
		this.bfCode = bfCode;
		this.dfCode = dfCode;
		this.publicCode = publicCode;
		this.hxgzCode = hxgzCode;
		this.hxfsCode = hxfsCode;
		this.pk_bill = null;
		this.ts = null;
		this.state = {
			json: {}
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		getMultiLang({
			moduleId: ['verificationsheet', 'public'],
			domainName: 'arap',
			currentLocale: 'simpchn',
			callback
		});
	}
	componentDidMount() {
		initTemplate.call(this, this.props);

		this.getData();
	}

	//请求列表数据
	getData = () => {
		//如果showOff为true，则显示停用数据，在请求参数data中增加显示停用参数，根据业务前后端自行处理
		ajax({
			url: '/nccloud/arap/verifyfa/query.do',
			data: {
				funCode: funCode,
				pageId: pageCode
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data == undefined) {
						// toast({ content: '无数据！', color: 'warning'});
					} else {
						this.props.editTable.setTableData(tableId, data[tableId]);
					}
				}
			}
		});
	};

	onSelected(props, moduleId, record, index, status) {
		this.props.editTable.setStatus(tableId, 'edit');
	}

	//删除单据
	delConfirm = () => {
		ajax({
			url: '/nccloud/arap/verifyfa/delete.do',
			data: {
				pk_bill: this.pk_bill,
				ts: this.ts,
				pageId: pageCode,
				funCode: funCode
			},
			success: (res) => {
				if (res.data) {
					this.props.editTable.setTableData(tableId, res.data[tableId]);
					toast({ content: this.state.json['verificationsheet-000094'], color: 'success' }); /* 国际化处理： 操作成功*/
				} else {
					this.props.editTable.setTableData(tableId, { rows: [] });
				}
			}
		});
	};

	/* 添加高级查询区中的页签 */
	addAdvTabs = () => {
		let { createForm } = this.props.form;
		return [
			{
				name: this.state.json['verificationsheet-000082'] /* 国际化处理： 常用条件*/,
				content: (
					<div className="nc-bill-form-area normal">
						{createForm(cytjCode, {
							expandArr: [this.bfCode, this.dfCode, this.publicCode],
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				),
				defaultOpen: true
			},
			{
				name: this.state.json['verificationsheet-000083'] /* 国际化处理： 核销规则*/,
				content: (
					<div className="nc-bill-form-area">
						{createForm(hxgzCode, {
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				)
			},
			{
				name: this.state.json['verificationsheet-000084'] /* 国际化处理： 核销方式*/,
				content: (
					<div className="nc-bill-form-area">
						{createForm(hxfsCode, {
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				)
			}
		];
	};

	/**
	 * 高级查询面板中的清空按钮 点击事件钩子，用于业务组清除自定义查询条件值
	 */
	clearSearchEve = () => {
		this.props.form.setFormItemsDisabled(hxgzCode, {
			zjbzRef: true,
			yxwcTxtF: true,
			jfbzRef: true,
			jfbz_dzjbzhlTxtF: true,
			jfbz_dbbhlTxtF: true,
			dfbzRef: true,
			dfbz_dbbhlTxtF: true,
			dfbz_dzjbzhlTxtF: true,
			hxbzRef: false
		});
		//设置默认值
		setDefaultValue(this, this.props, cytjCode, bfCode, dfCode, publicCode, hxgzCode, hxfsCode);
		setFormEditable(this.props, cytjCode, cytjCode, false);
		setFormEditable(this.props, cytjCode, bfCode, false);
		setFormEditable(this.props, cytjCode, dfCode, false);
		setFormEditable(this.props, cytjCode, publicCode, false);
		this.props.form.setFormItemsDisabled(cytjCode, { pk_org: false });
	}

	render() {
		let { editTable, search, modal } = this.props;
		let { NCCreateSearch } = search;
		let { createEditTable } = editTable;
		return (
			<div className="nc-bill-list" id="verifyfaarId">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{this.state.json['verificationsheet-000098']}</h2>
						{/* 国际化处理： 应收核销方案设置*/}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				{NCCreateSearch(this.searchId, {
					clickSearchBtn: saveClick.bind(this),
					showAdvBtn: true, //  显示高级按钮
					addAdvTabs: this.addAdvTabs, // 添加高级查询区自定义页签 (fun), return Dom
					advSearchClearEve: this.clearSearchEve.bind(this),//高级查询面板中的清空按钮 点击事件钩子，用于业务组清除自定义查询条件值
					onlyShowSuperBtn: true, // 只显示高级按钮
					replaceSuperBtn: this.state.json['verificationsheet-000095'] /* 国际化处理： 新增*/,
					searchBtnName: this.state.json['verificationsheet-000096'] /* 国际化处理： 保存*/,
					hideBtnArea: true // 隐藏查询按钮区域,可以使用其他按钮调用props.search.openAdvSearch(searchId, true)显示查询框
				})}
				<div className="nc-singleTable-table-area&quot;">
					{createEditTable(tableId, {
						//列表区
						useFixedHeader: true,
						showIndex: true //显示序号
					})}
				</div>
			</div>
		);
	}
}

SingleTable = createPage(
	{
		// initTemplate: initTemplate
	}
)(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
