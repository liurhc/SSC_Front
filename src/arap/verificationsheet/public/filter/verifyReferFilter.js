
import { ajax, getBusinessInfo } from "nc-lightapp-front";

/**
 * 核销常用条件参照过滤
 */
function modifierFormMeta(cytjCode, moduleId, props, meta) {
	meta[moduleId].items.map((item) => {
		if (item.itemtype != 'refer') {
			return;//非参照不过滤
		}
		item.unitValueIsNeeded = false; //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
		let attrcode = item.attrcode;
		switch (attrcode) {
			case 'headSubjRef':	//表头科目
				item.queryCondition = () => {
					let pkOrgValue = props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null
					let pkAccountingbook = null;
					ajax({
						url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
						data: {
							pk_org: pkOrgValue,
						},
						async: false,
						success: (res) => {
							if (res.success) {
								pkAccountingbook = res.data;
							}
						}
					})
					return {
						isDataPowerEnable: 'Y',
						pk_accountingbook: pkAccountingbook,
						DataPowerOperationCode: 'fi',//使用权组
						datestr: getBusinessInfo().businessDate
					};
				}
				break;
			case 'itemSubjRef':	//表体科目
				item.queryCondition = () => {
					let pkOrgValue = props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null
					let pkAccountingbook = null;
					ajax({
						url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
						data: {
							pk_org: pkOrgValue,
						},
						async: false,
						success: (res) => {
							if (res.success) {
								pkAccountingbook = res.data;
							}
						}
					})
					return {
						isDataPowerEnable: 'Y',
						pk_accountingbook: pkAccountingbook,
						DataPowerOperationCode: 'fi',//使用权组
						datestr: getBusinessInfo().businessDate
					};
				}
				break;
			case 'pk_org'://财务组织(根据集团过滤)
				item.queryCondition = () => {
					item.isShowUnit = false;
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						AppCode: props.getSearchParam('c'),
						TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
					};
				}
				break;
			case 'pk_org_v'://财务组织版本(根据集团过滤)
				item.isShowUnit = false;
				item.queryCondition = () => {
					item.isShowUnit = false;
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						AppCode: props.getSearchParam('c'),
						VersionStartDate:getBusinessInfo().businessDate,
						TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
					}
				}
				break;
			case 'deptRef'://部门
				item.isShowUnit = true,
					item.unitCondition = () => {
						return {
							pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
							TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						}
					}
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
					}
				}
				break;
			case 'psnDocRef'://业务员
				item.isShowUnit = true,
					item.unitCondition = () => {
						return {
							pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
							TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						}
					}
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						busifuncode: 'all',
						pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
					}
				}
				break;
			default:
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value ? props.form.getFormItemsValue(cytjCode, 'pk_org').value : null,
					}
				}
				break;

		}
		return item;
	})
	return meta;
}


export { modifierFormMeta }
