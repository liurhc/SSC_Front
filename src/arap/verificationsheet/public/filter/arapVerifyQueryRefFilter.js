import { getBusinessInfo, ajax } from "nc-lightapp-front";

/**
 * 手工核销,自动核销查询条件过滤
 * @param {*} props 
 * @param {*} searchId 当前查询区的moduleId
 * @param {*} formId 表单id,用于获取表单上的pk_org等作为过滤条件
 * @param {*} meta meta
 */
function modifierSearchMeta(searchId, formId, props, meta) {
    let falg = true; //用来控制单元格是否可操作
    meta[searchId].items.map((item) => {
        item.isShowUnit = false;
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = true;
            item.unitValueIsNeeded = false; //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
        }
        //是否显示停用数据
        item.isShowDisabledData = true;
        let attrcode = item.attrcode;
        if(attrcode.indexOf(".")!= -1){
            attrcode = attrcode.substring(attrcode.indexOf(".")+1,attrcode.length);
        }
        switch (attrcode) {
            case 'pk_tradetype'://交易类型
                item.refName = '交易类型参照'
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: "F0,F1,F2,F3"
                    }
                }
                break;
            case 'pk_tradetype2'://交易类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: "F0,F1,F2,F3"
                    }
                }
                break;
            case 'customer'://客户
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };
                }
                break;
            case 'supplier'://供应商
                item.isShowDisabledData = false;
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };

                }
                break;
                case 'pk_deptid'://部门
                item.isShowUnit = true,
                item.unitCondition = () => {
                    return {
                        pkOrgs: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        VersionStartDate:getBusinessInfo().businessDate,
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };
                }
                break;
            case 'pk_psndoc'://业务员
                item.isShowUnit = true,
                item.unitCondition = () => {
                    return {
                        pkOrgs: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_dept: props.form.getFormItemsValue(formId, 'deptRef').value ? props.form.getFormItemsValue(formId, 'deptRef').value : null,
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                    };
                }
                break;
            case 'pk_costsubj'://收支项目
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };
                }
                break;
            case 'material'://物料
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        matcustcode: props.search.getSearchValByField(searchId, 'bodys.matcustcode') ? props.search.getSearchValByField(searchId, 'bodys.matcustcode').value.firstvalue : null,
                        GridRefActionExt: 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder'
                    };
                }
                break;
            case 'ordercubasdoc'://订单客户
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };
                }
                break;
            case 'pk_org'://财务组织(根据集团过滤)
            item.queryCondition = () => {
                item.isShowUnit = false;
                return {
                    DataPowerOperationCode: 'fi',//使用权组
                    isDataPowerEnable: 'Y',
                    AppCode: props.getSearchParam('c'),
                    TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
                };
            }
            break;
            case 'creator'://创建人
            item.queryCondition = () => {
                return {
                    DataPowerOperationCode: 'fi',//使用权组
                    isDataPowerEnable: 'Y',
                    pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                    GridRefActionExt: 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder',
                    UsualGridRefActionExt :'nccloud.web.arap.ref.before.CrossRuleSqlBuilder'
                };
            }
            break;
            
        default:
            item.queryCondition = () => {
                return {
                    DataPowerOperationCode: 'fi',//使用权组
                    isDataPowerEnable: 'Y',
                    pk_org: props.form.getFormItemsValue(formId, 'pk_org').value ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                }
            }
            break;
        }
    })
    props.meta.setMeta(meta);
    return falg; //默认单元格都可操作

}

export { modifierSearchMeta }
