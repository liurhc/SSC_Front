import { getBusinessInfo, ajax } from "nc-lightapp-front";

/**
 * 手工核销,自动核销查询条件过滤
 * @param {*} props 
 * @param {*} searchId 当前查询区的moduleId
 * @param {*} formId 表单id,用于获取表单上的pk_org等作为过滤条件
 * @param {*} meta meta
 */
function modifierSearchMeta(searchId, formId, props, meta) {

    let falg = true; //用来控制单元格是否可操作
    meta[searchId].items.map((item) => {
        item.isShowUnit = false;
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = true;
            item.unitValueIsNeeded = false; //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
        }
        let attrcode = item.attrcode;
        switch (attrcode) {
            case 'pk_tradetype'://交易类型
                item.refName = '交易类型参照'
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: "F0,F1,F2,F3"
                    }
                }
                break;
            case 'customer'://客户
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };
                }
                break;
            case 'supplier'://供应商
                item.isShowDisabledData = false;
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };

                }
                break;
            case 'pk_deptid'://部门
                item.isShowUnit = true,
                item.unitCondition = () => {
                    return {
                        pkOrgs: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        VersionStartDate:getBusinessInfo().businessDate,
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };
                }
                break;
            case 'pk_psndoc'://业务员
                item.isShowUnit = true,
                item.unitCondition = () => {
                    return {
                        pkOrgs: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_dept: props.form.getFormItemsValue(formId, 'deptRef').value ? props.form.getFormItemsValue(formId, 'deptRef').value : null,
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                    };
                }
                break;
            case 'pk_costsubj'://收支项目
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };
                }
                break;
            case 'material'://物料
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        GridRefActionExt: 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder'
                    };
                }
                break;
            case 'ordercubasdoc'://订单客户
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };
                }
                break;
            case 'pk_balatype'://结算方式(根据pk_billtype)
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        GridRefActionExt: 'nccloud.web.arap.ref.before.BalatypeSqlBuilder',
                    };
                }
                break;
            case 'sett_org'://结算财务组织(根据集团) 且根据财务组织联动
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            case 'so_ordertype'://销售交易类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: "30",
                    }
                }
                break;
            case 'so_transtype'://销售渠道类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    }
                }
                break;
            case 'so_org'://业务组织(业务单元（财务组织委托）) @atuo:zhangygw ---- ok
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_orgs: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            case 'so_deptid'://业务部门(根据组织)
                item.isShowUnit = true
                item.unitCondition = () => {
                    return {
                        pk_orgs: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };
                }
                break;
            case 'so_psndoc'://业务员(根据部门+组织过滤)
                item.isShowUnit = true
                item.unitCondition = () => {
                    return {
                        pk_orgs: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    item.isShowUnit = true;

                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                    };
                }
                break;
            case 'subjcode'://科目
                item.queryCondition = () => {
                    let pkAccountingbook = null;
                    ajax({
                        url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
                        data: {
                            pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        },
                        async: false,
                        success: (res) => {
                            if (res.success) {
                                pkAccountingbook = res.data;
                            }
                        }
                    })
                    return {
                        isDataPowerEnable: 'Y',
                        pk_accountingbook: pkAccountingbook,
                        DataPowerOperationCode: 'fi',//使用权组
                        datestr: getBusinessInfo().businessDate
                    };
                }
                break;
            case 'pk_pcorg'://利润中心
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };

                }
                break;
            case 'pu_org'://业务组织(业务单元（财务组织委托）) @atuo:zhangygw ---- ok
                item.queryCondition = () => {
                    return {
                        isDataPowerEnable: 'Y',
                        pk_orgs: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            case 'pu_org_v'://业务单元版本(财务组织委托)@auto zhangygw --- ok 
                item.isShowUnit = false;
                item.queryCondition = () => {
                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            case 'pu_deptid'://业务部门
                item.isShowUnit = true
                item.unitCondition = () => {
                    return {
                        pk_orgs: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };

                }
                break;
            case 'pu_psndoc'://业务人员
                item.isShowUnit = true
                item.unitCondition = () => {
                    return {
                        pk_orgs: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                    };
                }
                break;
            case 'pk_areacl'://地区
                item.queryCondition = () => {
                    return {
                        isDataPowerEnable: 'Y',
                    }
                }
                break;
            case 'project'://项目
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null
                    };
                }
                break;
            case 'project_task'://项目任务
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        project: props.search.getSearchValByField(searchId, 'project') ? props.search.getSearchValByField(searchId, 'project').value.firstvalue : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.ProjectTaskSqlBuilder'
                    };
                }
                break;
            case 'costcenter'://成本中心
                item.queryCondition = () => {
                    let tmporg = props.search.getSearchValByField(searchId, 'pk_pcorg') ? props.search.getSearchValByField(searchId, 'pk_pcorg').value.firstvalue : null;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_pcorg: tmporg,
                        pk_org: tmporg,
                        orgType: "pk_profitcenter",
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.CostCenterSqlBuilder'
                    };
                }
                break;
            default:
                item.queryCondition = () => {
                    return {
                        pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
        }
    })
    props.meta.setMeta(meta);
    return falg; //默认单元格都可操作

}

export { modifierSearchMeta }
