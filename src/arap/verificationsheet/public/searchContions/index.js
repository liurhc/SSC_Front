import { base, getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../../public/ReferLoader';
import { searchBtnClick, buttonClick, advSearchClearEve } from './events';
import Comconditions from '../comcondition';
import './index.less';
const { NCDatePicker, NCSelect } = base;
const { NCRangePicker } = NCDatePicker;
const format3 = 'YYYY-MM-DD';

export default function SearchContions(that, NCOptions, createButtonApp, NCCreateSearch, searchId) {
	let { verificationObj, pk_tradetype, objTypeValue, value } = that.state;
	return (
		<div className="search-contions">
			<div className="querycons-style">
				<div className="contions-box contions-box-select">
					<NCSelect
						value={verificationObj.display}
						onChange={(v) => {
							verificationObj.display = v.display;
							verificationObj.value = v.value;
							objTypeValue = { display: null, value: null };
							that.setState({
								verificationObj,
								objTypeValue
							});
						}}
					>
						{NCOptions}
					</NCSelect>
				</div>
				<div className="contions-box">
					<div className="refer">
						<div className="refer-style">{getDifferentRefer(that)}</div>
					</div>
				</div>
				<div className="contions-box">
					<div className="refer">
						<div className="refer-style">
							<Referloader
								tag="Transtype"
								refcode="uap/refer/riart/transtype/index.js"
								value={{
									refname: pk_tradetype.display,
									refpk: pk_tradetype.value
								}}
								queryCondition={() => {
									//根据集团过滤财务组织
									return {
										parentbilltype: 'F0,F1,F2,F3'
									};
								}}
								onChange={(v) => {
									pk_tradetype.value = v.refpk;
									pk_tradetype.display = v.refname;
									that.setState({
										pk_tradetype
									});
								}}
								placeholder={
									pk_tradetype.display || that.state.json['verificationsheet-000016']
								} /* 国际化处理： 交易类型*/
							/>
						</div>
					</div>
				</div>
				<div className="contions-box picker">
					<div className="refer">
						<div className="refer-style">
							<NCRangePicker
								format={format3}
								onChange={(d) => {
									that.setState({
										value: d
									});
								}}
								showClear={true}
								showOk={true}
								className={'range-fixed'}
								value={value}
								placeholder={that.state.json['verificationsheet-000017']} /* 国际化处理： 开始 ~ 结束*/
								dateInputPlaceholder={[
									that.state.json['verificationsheet-000018'],
									that.state.json['verificationsheet-000019']
								]} /* 国际化处理： 开始,结束*/
							/>
						</div>
					</div>
				</div>
				<div className="contions-box-query">
					<div className="refer-query">
						<div className="search">
							{createButtonApp({
								area: 'list_head_query',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(that, that),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
						<div className="heigh-search">
							{NCCreateSearch(
								searchId, //模块id
								{
									clickSearchBtn: searchBtnClick.bind(that, that), //   点击按钮事件
									showAdvBtn: true, //  显示高级按钮
									addAdvBody: addAdvBody.bind(that, that), // 添加高级查询区自定义查询条件Dom (fun) , return Dom
									oid: that.props.meta.oid, //查询模板的oid，用于查询查询方案
									onlyShowSuperBtn: true, // 只显示高级按钮
									replaceSuperBtn: that.state.json['verificationsheet-000024'] /* 国际化处理： 高级*/,
									advSearchClearEve: advSearchClearEve.bind(that, that)
								}
							)}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

//对象名称显示不同参照
function getDifferentRefer(that) {
	//
	let { verificationObj, objTypeValue, pk_org } = that.state;
	switch (verificationObj.value) {
		case '0':
			return (
				<Referloader
					tag="CustomerDefaultTreeGridRef"
					refcode="uapbd/refer/customer/CustomerDefaultTreeGridRef/index.js"
					value={{
						refname: objTypeValue.display,
						refpk: objTypeValue.value
					}}
					queryCondition={() => {
						//根据集团过滤财务组织
						return {
							DataPowerOperationCode: 'fi', //使用权组
							isDataPowerEnable: 'Y',
							pk_org: pk_org
						};
					}}
					isMultiSelectedEnabled={false}
					onChange={(v) => {
						objTypeValue.value = v.refpk;
						objTypeValue.display = v.refname;
						that.setState(
							{
								objTypeValue
							},
							() => {}
						);
					}}
					placeholder={objTypeValue.display || that.state.json['verificationsheet-000000']} /* 国际化处理： 客户*/
				/>
			);
			break;
		case '1':
			return (
				<Referloader
					tag="SupplierRefTreeGridRef"
					refcode="uapbd/refer/supplier/SupplierRefTreeGridRef/index.js"
					value={{
						refname: objTypeValue.display,
						refpk: objTypeValue.value
					}}
					queryCondition={() => {
						//根据集团过滤财务组织
						return {
							DataPowerOperationCode: 'fi', //使用权组
							isDataPowerEnable: 'Y',
							pk_org: pk_org
						};
					}}
					onChange={(v) => {
						objTypeValue.value = v.refpk;
						objTypeValue.display = v.refname;
						that.setState({
							objTypeValue
						});
					}}
					placeholder={objTypeValue.display || that.state.json['verificationsheet-000001']} /* 国际化处理： 供应商*/
				/>
			);
			break;
		case '2':
			return (
				<Referloader
					tag="DeptTreeRef"
					refcode="uapbd/refer/org/DeptTreeRef/index.js"
					value={{
						refname: objTypeValue.display,
						refpk: objTypeValue.value
					}}
					isShowUnit={true}
					unitCondition={() => {
						return {
							pkOrgs: pk_org,
							TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						};
					}}
					queryCondition={() => {
						//根据集团过滤财务组织
						return {
							busifuncode: 'all',
							DataPowerOperationCode: 'fi', //使用权组
							isDataPowerEnable: 'Y',
							pk_org: pk_org
						};
					}}
					onChange={(v) => {
						objTypeValue.value = v.refpk;
						objTypeValue.display = v.refname;
						that.setState({
							objTypeValue
						});
					}}
					placeholder={objTypeValue.display || that.state.json['verificationsheet-000002']} /* 国际化处理： 部门*/
				/>
			);
			break;
		case '3':
			return (
				<Referloader
					tag="PsndocTreeGridRef"
					refcode="uapbd/refer/psninfo/PsndocTreeGridRef/index.js"
					value={{
						refname: objTypeValue.display,
						refpk: objTypeValue.value
					}}
					isShowUnit={true}
					unitCondition={() => {
						return {
							pkOrgs: pk_org,
							TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						};
					}}
					queryCondition={() => {
						//根据集团过滤财务组织
						return {
							DataPowerOperationCode: 'fi', //使用权组
							isDataPowerEnable: 'Y',
							pk_org: pk_org
						};
					}}
					onChange={(v) => {
						objTypeValue.value = v.refpk;
						objTypeValue.display = v.refname;
						that.setState({
							objTypeValue
						});
					}}
					placeholder={objTypeValue.display || that.state.json['verificationsheet-000003']} /* 国际化处理： 业务员*/
				/>
			);
			break;
		default:
			break;
	}
}

function addAdvBody(that) {
	let transferData = that.props.ViewModel.getData('WholeVerifyDatas');
	let { schemeData } = that.state;
	return (
		<Comconditions
			pkOrg={transferData && transferData.pk_org.value}
			schemeData={schemeData}
			handleAddAdvBodyCons={that.handleAddAdvBodyCons}
		/>
	);
}
