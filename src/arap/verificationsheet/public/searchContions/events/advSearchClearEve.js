export default function advSearchClearEve(that) {
    let { schemeData } = that.state;
		schemeData.objTypeValue = {display: null, value: null};
		schemeData.pk_tradetype = {display: null, value: null};
		schemeData.value = [];
		that.setState({
      schemeData
		})
};
