import { ajax, toast } from 'nc-lightapp-front';
let searchId = 'query';
//点击查询，获取查询区数据
export default function clickSearchBtn(that) {
    let {
        schemeData
    } = that.state;
    let WholeVerifyData = that.props.ViewModel.getData('WholeVerifyDatas');
    let effectstatus = WholeVerifyData.effectstatus;
    let pagecode = that.props.getSearchParam('p');
    let verifyConditionVO = {
        objType: schemeData.objType.value,
        objTypeValue: schemeData.objTypeValue.value,
        pk_tradetype: schemeData.pk_tradetype.value,
        beginDate: schemeData.value[0],
        endDate: schemeData.value[1]
    }
    let data = {
        queryTreeFormatVO: that.props.search.getQueryInfo(searchId, true),
        verifyConditionVO: verifyConditionVO,
        pageId: pagecode,
        pk_bill: WholeVerifyData.pk_bill.value,
        pk_item: WholeVerifyData.pk_item,
        billType: WholeVerifyData.billType.value,
        ts: that.state.ts
    };
    let url = effectstatus == 0 ? '/nccloud/arap/billverify/preVerifyLinkingQuery.do' : '/nccloud/arap/billverify/nowVerifyQuery.do';
    ajax({
        url: url,
        data: data,
        success: (res) => {
            let {
                success,
                data
            } = res;
            if (success) {
                let {
                    settlement
                } = that.state;
                let datas = data.grid;
                that.state.ts = data.ts;
                that.this_sett = Number(data.this_sett).toFixed(data.scale);
                that.state.settlement.value = Number(data.this_sett).toFixed(data.scale);
                that.setState({
                    settlement
                });
                if (datas && datas[that.tableId]) {
                    that.newTableData = datas[that.tableId];
                    that.props.editTable.setTableData(that.tableId, datas[that.tableId]);
                } else {
                    that.props.editTable.setTableData(that.tableId, {
                        rows: []
                    });
                }
            }
        }
    });
}
