import { headButton } from '../../../../public/components/pubUtils/buttonName';

export default function buttonClick(that, props, id) {
	switch (id) {
		case headButton.Query: //快速查询
			that.handleQueryBtn();
			break;
		case headButton.Empty: //快速查询
			that.handleEmpty();
			break;
		default:
			break;
	}
}
