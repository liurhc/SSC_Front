import React, { Component } from 'react';
import { base,getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../../public/ReferLoader';
import './index.less';
const { NCFormControl, NCDatePicker, NCRow, NCCol, NCSelect } = base;
const NCOption = NCSelect.NCOption;
let NCOptionData = [
	{ value: 0, display: '' },/* 国际化处理： 客户*/
	{ value: 1, display: '' },/* 国际化处理： 供应商*/
	{ value: 2, display: '' },/* 国际化处理： 部门*/
	{ value: 3, display: '' }/* 国际化处理： 业务员*/
];
const { NCRangePicker } = NCDatePicker;
const format3 = 'YYYY-MM-DD';

export default class Comconditions extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {}
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({json:json},() => {
				NCOptionData = [
					{ value: 0, display: this.state.json['verificationsheet-000000'] },/* 国际化处理： 客户*/
					{ value: 1, display: this.state.json['verificationsheet-000001'] },/* 国际化处理： 供应商*/
					{ value: 2, display: this.state.json['verificationsheet-000002'] },/* 国际化处理： 部门*/
					{ value: 3, display: this.state.json['verificationsheet-000003'] }/* 国际化处理： 业务员*/
				];
			});
		}
		getMultiLang({moduleId:['verificationsheet','public'],domainName :'arap',currentLocale:'simpchn',callback});
	}
	//获取条件数据
	getConsDatas = () => {
		let { schemeData } = this.state;
		return schemeData;
	};

	//input展示
	getInput = (name, key) => {
		return (
			<div className="finance">
				<span className="lable">{name}</span>
				<div className="finance-refer">
					<NCFormControl value={key.value} />
				</div>
			</div>
		);
	};

	//参照展示
	getRefer = (tag, refcode, key, schemeData, name) => {
		return (
			<div className="com-finance">
				<span className="com-lable">{name}</span>
				<div className="com-finance-refer">
					<div className="com-refer">
						<Referloader
							tag={tag}
							refcode={refcode}
							value={{
								refname: key.display,
								refpk: key.value
							}}
							queryCondition={() => {
								//根据集团过滤财务组织
								return {
									parentbilltype: 'F0,F1,F2,F3'
								};
							}}
							onChange={(v) => {
								key.value = v.refpk;
								key.display = v.refname;
								// this.setState({
								// 	mainData
								// })
								this.props.handleAddAdvBodyCons(schemeData);
							}}
						/>
					</div>
				</div>
			</div>
		);
	};

	//对象名称显示不同参照
	getDifferentRefer = (objTypeValue, objType, pk_org, schemeData) => {
		switch (objType.value) {
			case 0:
				return (
					<Referloader
						tag="CustomerDefaultTreeGridRef"
						refcode="uapbd/refer/customer/CustomerDefaultTreeGridRef/index.js"
						value={{
							refname: objTypeValue.display,
							refpk: objTypeValue.value
						}}
						queryCondition={() => {
							//根据集团过滤财务组织
							return {
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: pk_org
							};
						}}
						isMultiSelectedEnabled={false}
						onChange={(v) => {
							objTypeValue.value = v.refpk;
							objTypeValue.display = v.refname;
							// this.setState({
							// 	schemeData
							// })
							this.props.handleAddAdvBodyCons(schemeData);
						}}
						placeholder={objTypeValue.display || this.state.json['verificationsheet-000000']}/* 国际化处理： 客户*/
					/>
				);
				break;
			case 1:
				return (
					<Referloader
						tag="SupplierRefTreeGridRef"
						refcode="uapbd/refer/supplier/SupplierRefTreeGridRef/index.js"
						value={{
							refname: objTypeValue.display,
							refpk: objTypeValue.value
						}}
						queryCondition={() => {
							//根据集团过滤财务组织
							return {
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: pk_org
							};
						}}
						onChange={(v) => {
							objTypeValue.value = v.refpk;
							objTypeValue.display = v.refname;
							// this.setState({
							// 	schemeData
							// })
							this.props.handleAddAdvBodyCons(schemeData);
						}}
						placeholder={objTypeValue.display || this.state.json['verificationsheet-000001']}/* 国际化处理： 供应商*/
					/>
				);
				break;
			case 2:
				return (
					<Referloader
						tag="DeptTreeRef"
						refcode="uapbd/refer/org/DeptTreeRef/index.js"
						value={{
							refname: objTypeValue.display,
							refpk: objTypeValue.value
						}}
						isShowUnit={true}
						unitCondition={() => {
							return {
								pkOrgs: pk_org,
								TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
							};
						}}
						queryCondition={() => {
							//根据集团过滤财务组织
							return {
								busifuncode: 'all',
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: pk_org
							};
						}}
						onChange={(v) => {
							objTypeValue.value = v.refpk;
							objTypeValue.display = v.refname;
							// this.setState({
							// 	schemeData
							// })
							this.props.handleAddAdvBodyCons(schemeData);
						}}
						placeholder={objTypeValue.display || this.state.json['verificationsheet-000002']}/* 国际化处理： 部门*/
					/>
				);
				break;
			case 3:
				return (
					<Referloader
						tag="PsndocTreeGridRef"
						refcode="uapbd/refer/psninfo/PsndocTreeGridRef/index.js"
						value={{
							refname: objTypeValue.display,
							refpk: objTypeValue.value
						}}
						isShowUnit={true}
						unitCondition={() => {
							return {
								pkOrgs: pk_org,
								TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
							};
						}}
						queryCondition={() => {
							//根据集团过滤财务组织
							return {
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: pk_org
							};
						}}
						onChange={(v) => {
							objTypeValue.value = v.refpk;
							objTypeValue.display = v.refname;
							// this.setState({
							// 	schemeData
							// })
							this.props.handleAddAdvBodyCons(schemeData);
						}}
						placeholder={objTypeValue.display || this.state.json['verificationsheet-000003']}/* 国际化处理： 业务员*/
					/>
				);
				break;
			default:
				break;
		}
	};

	//常用条件页签内的查询条件框
	getReferShow = (schemeData, NCOptions) => {
		let pk_org = this.props.pkOrg;
		let { objType, objTypeValue, pk_tradetype, value } = schemeData;

		return (
			<div>
				<div className="commons">
					<NCRow>
						<NCCol md={12} xs={12} sm={12}>
							<div className="com-finance">
								<span className="com-lable">{this.state.json['verificationsheet-000020']}</span>{/* 国际化处理： 核销对象*/}
								<div className="com-finance-refer">
									<div className="com-refer">
										<NCSelect
											value={objType.display}
											onChange={(v) => {
												objType.display = v.display;
												objType.value = v.value;
												schemeData.objTypeValue = { display: null, value: null };
												this.props.handleAddAdvBodyCons(schemeData);
											}}
										>
											{NCOptions}
										</NCSelect>
									</div>
								</div>
							</div>
						</NCCol>
						<NCCol md={12} xs={12} sm={12}>
							<div className="com-finance">
								<span className="com-lable">{this.state.json['verificationsheet-000021']}</span>{/* 国际化处理： 对象名称*/}
								<div className="com-finance-refer">
									<div className="com-refer">
										{this.getDifferentRefer(objTypeValue, objType, pk_org, schemeData)}
									</div>
								</div>
							</div>
						</NCCol>
						<NCCol md={12} xs={12} sm={12}>
							{this.getRefer(
								'Transtype',
								'uap/refer/riart/transtype/index.js',
								pk_tradetype,
								schemeData,
								this.state.json['verificationsheet-000016']/* 国际化处理： 交易类型*/
							)}
						</NCCol>
						<NCCol md={12} xs={12} sm={12}>
							<div className="com-finance date">
								<span className="com-lable">{this.state.json['verificationsheet-000022']}</span>{/* 国际化处理： 单据日期*/}
								<div className="com-finance-refer">
									<div className="com-refer">
										<NCRangePicker
											format={format3}
											onChange={(d) => {
												schemeData.value = d;
												this.props.handleAddAdvBodyCons(schemeData);
											}}
											showClear={true}
											showOk={true}
											className={'range-fixed'}
											value={schemeData.value}
											placeholder={this.state.json['verificationsheet-000017']}/* 国际化处理： 开始 ~ 结束*/
											dateInputPlaceholder={[ this.state.json['verificationsheet-000018'], this.state.json['verificationsheet-000019'] ]}/* 国际化处理： 开始,结束*/
										/>
									</div>
								</div>
							</div>
						</NCCol>
					</NCRow>
				</div>
			</div>
		);
	};

	render() {
		let { schemeData } = this.props;
		const NCOptions = NCOptionData.map((province) => <NCOption value={province}>{province.display}</NCOption>);
		return <div id="common">{this.getReferShow(schemeData, NCOptions)}</div>;
	}
}
