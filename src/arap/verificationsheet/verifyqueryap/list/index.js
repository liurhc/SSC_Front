import { ajax, base, createPage, cacheTools, getMultiLang, createPageIcon, getBusinessInfo } from 'nc-lightapp-front';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { searchId, tableId, publicCode, hxcode, cytjcode } from './constants';
import { buttonClick, searchBtnClick, initTemplate, afterEvent, beforeEvent } from './events';
import { setFormEditable } from '../../../public/components/pubUtils/billPubUtil.js';
import { loginContext, getContext, loginContextKeys } from '../../../public/components/arapInitInfo/loginContext';

import Table1 from '../Table1'; //表格1
import Table2 from '../Table2'; //表格2
import './index.less';
const { NCTabs } = base;
const NCTabPane = NCTabs.NCTabPane;
/**
 *  自动结果查询
 */
class SingleTable extends Component {
	constructor(props) {
		super(props);
		//this.props = props;
		this.searchId = searchId;
		this.tableId = tableId;
		this.publicCode = publicCode;
		this.cytjcode = cytjcode;
		this.hxcode = hxcode;
		this.props.button.setButtonsVisible({});
		this.state = {
			pk_bill: null,
			ts: null,
			transferData: [], //接收子组件传递过来的数据
			table1datas: null,
			table2datas: null,
			table2Keys: [],
			isFirstQuery: true,
			active: '1',
			status: false,
			json: {},
			allpks: []
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		getMultiLang({
			moduleId: [ 'verificationsheet', 'public' ],
			domainName: 'arap',
			currentLocale: 'simpchn',
			callback
		});
	}
	componentDidMount() {
		//联查
		if ('browse' == this.props.getUrlParam('status')) {
			let pks = [];
			let dataArr = [];
			let linkurl = '';
			let data = {};
			if ('fip' == this.props.getUrlParam('scene')) {
				dataArr = cacheTools.get('checkedData');
				linkurl = '/nccloud/arap/arappub/queryGridByPK.do';
				if (dataArr) {
					data = {
						fipLink: dataArr,
						pageId: this.props.getSearchParam('p'),
						tableCode: tableId
					};
				}
				ajax({
					url: linkurl,
					data: data,
					success: (res) => {
						let { success, data } = res;
						if (success) {
							if (data) {
								//this.props.editTable.setTableData(tableId,data[tableId]);
								// cacheTools.set('searchDatas', data);
								// cacheTools.set('2008VQ_query', searchVal);
								if (data != null || data.grid != undefined) {
									this.setState(
										{
											table1datas: data.grid[tableId]
										},
										() => {
											this.ChangeTable1Datas(data.grid[tableId]);
										}
									);
								} else {
									toast({
										color: 'success',
										content: this.state.json['verificationsheet-000120']
									}); /* 国际化处理： 无数据！*/
								}
								this.props.button.setButtonDisabled('CancelVerify', false);
							}
						}
					}
				});
			}
		} else {
			this.props.button.setButtonDisabled('CancelVerify', true);
		}
		initTemplate.call(this, this.props);

		// this.getData(false);
	}

	//请求列表数据
	getData = (showOff = false) => {};

	/* 添加高级查询区中的页签 */
	addAdvTabs = () => {
		let { createForm } = this.props.form;
		return [
			{
				name: this.state.json['verificationsheet-000082'] /* 国际化处理： 常用条件*/,
				content: (
					<div className="nc-bill-form-area">
						{createForm('cytjcode', {
							expandArr: [ this.hxcode, this.publicCode ],
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this)
						})}
					</div>
				),
				defaultOpen: true
			}
		];
	};

	onRef1 = (ref) => {
		this.child1 = ref;
	};
	onRef2 = (ref) => {
		this.child2 = ref;
	};

	handleChangeTable2 = () => {
		let selectedData = this.getTable1Data();
		let selectdata2 = this.getTable2Data();
		if (this.state.active == '2') {
			if (selectdata2.length > 0) {
				this.props.button.setButtonDisabled('CancelVerify', false);
			} else {
				this.props.button.setButtonDisabled('CancelVerify', true);
			}
		} else {
			if (selectedData.length > 0) {
				this.props.button.setButtonDisabled('CancelVerify', false);
			} else {
				this.props.button.setButtonDisabled('CancelVerify', true);
			}
		}
		let checkedObj = [];
		if (selectedData.length > 0) {
			selectedData.forEach((val) => {
				checkedObj.push(val.data.values.pk_verify.value);
			});
		}
		if (checkedObj.length == this.state.transferData.length) {
			if (checkedObj.length > 1) {
				for (let i = 0; i < checkedObj.length; i++) {
					if (checkedObj[i] != this.state.transferData[i]) {
						this.setState(
							{
								transferData: checkedObj
							},
							() => {
								if (this.state.active == '2') {
									if (this.state.transferData) {
										this.child2.getData(this.state.transferData);
									}
								}
							}
						);
						break;
					}
				}
			} else {
				if (checkedObj[0] != this.state.transferData[0]) {
					this.setState(
						{
							transferData: checkedObj
						},
						() => {
							if (this.state.active == '2') {
								if (this.state.transferData) {
									this.child2.getData(this.state.transferData);
								}
							}
						}
					);
				}
			}
		} else {
			this.setState(
				{
					transferData: checkedObj
				},
				() => {
					if (this.state.active == '2') {
						if (this.state.transferData) {
							this.child2.getData(this.state.transferData);
						}
					}
				}
			);
		}
	};
	//调用子组件1的方法 获取数据
	getTable1Data = () => {
		return this.child1.getSelectedData();
	};
	//调用子组件2的方法 获取数据
	getTable2Data = () => {
		return this.child2.getSelectedData2();
	};
	//调用方子组件的方法（改变表格数据table2）
	ChangeTableDatas = (data) => {
		this.child2.setTableDatas(data);
	};

	//调用方子组件的方法（改变表格数据table1）
	ChangeTable1Datas = (data) => {
		this.child1.setTable1Datas(data);
	};
	//调用方子组件的方法（删除核销掉的数据table2）
	delTableByindex2 = (indexArr) => {
		this.child2.delTableDataByindex(indexArr);
	};
	//调用方子组件的方法（删除核销掉的数据table1）
	delTableByindex1 = (indexArr) => {
		this.child1.delTableDataByindex(indexArr);
	};

	//调用方子组件的方法（获取所有数据table1）
	getTableAllData() {
		if (this.state.active == '1') {
			return this.child1.getAllData();
		} else {
			return this.child2.getAllData();
		}
	}
	//取消禁用取消按钮
	setDisabledButton() {
		setTimeout(() => {
			let checkdata = this.child2.getSelectedData2();
			if (checkdata.length > 0) {
				this.props.button.setButtonDisabled('CancelVerify', false);
			} else {
				this.props.button.setButtonDisabled('CancelVerify', true);
			}
		}, 0);
	}
	//禁用取消核销按钮
	calDisabledButton() {
		setTimeout(() => {
			let checkdata = this.child1.getSelectedData();
			if (checkdata.length > 0) {
				this.props.button.setButtonDisabled('CancelVerify', false);
			} else {
				this.props.button.setButtonDisabled('CancelVerify', true);
			}
		}, 0);
	}

	//改变allpks方法
	changeAllPks = (allpks) => {
		this.setState({
			allpks: allpks
		});
	};
	/**
	 * 高级查询面板中的清空按钮 点击事件钩子，用于业务组清除自定义查询条件值
	 * isauto 是否自动核销
	 */
	clearSearchEve = (isauto) => {
		let cytjCode = this.cytjcode;

		let publicCode = this.publicCode;
		let hxcode = this.hxcode;

		//设置默认值
		this.setDefaultValue(this, this.props, cytjCode, publicCode, hxcode);
		this.props.form.setFormItemsDisabled(cytjCode, { pk_org: false });
	};
	setDefaultValue(that, props, cytjCode, publicCode, hxcode) {
		props.form.EmptyAllFormValue(cytjCode);
		props.form.EmptyAllFormValue(hxcode);
		let pk_org = getContext(loginContextKeys.pk_org);
		let org_Name = getContext(loginContextKeys.org_Name);
		//设置默认值
		props.form.setFormItemsValue(cytjCode, {
			objTypeCBox: { value: 'all', display: this.state.json['verificationsheet-000113'] }
		}); /* 国际化处理： 全部*/
		// 本方开始日期	本方结束日期
		let date = getBusinessInfo().businessDate.substring(0, 10);

		props.form.setFormItemsValue(cytjCode, { beginDateRef: { value: date }, endDateRef: { value: date } });
		props.form.setFormItemsValue(cytjCode, {
			verifyModeBox: { value: 0, display: this.state.json['verificationsheet-000035'] },
			queryModeBox: { value: 0, display: this.state.json['verificationsheet-000114'] }
		}); /* 国际化处理： 同币种核销,汇总查询*/
		//常用条件，组织为空，只有组织可编辑
		if (pk_org && org_Name) {
			props.form.setFormItemsValue(cytjCode, { pk_org: { value: pk_org, display: org_Name } });
			afterEvent(props, cytjCode, 'pk_org', { value: pk_org, display: org_Name });
			return;
		}
		setFormEditable(props, cytjCode, hxcode, false);
		setFormEditable(props, cytjCode, publicCode, false);
		props.renderItem('form', cytjCode, 'objNameRef', null);
	}
	render() {
		let { search } = this.props;
		let { NCCreateSearch } = search;
		let { transferData, table1datas, table2datas, active, table2Keys, allpks } = this.state;
		return (
			<div className="nc-bill-list" id="verifyqueryapId">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon && createPageIcon()}
						<h2 className="title-search-detail">{this.state.json['verificationsheet-000129']}</h2>
						{/* 国际化处理： 核销结果查询*/}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this)
							//popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				{NCCreateSearch(this.searchId, {
					clickSearchBtn: searchBtnClick.bind(this),
					showAdvBtn: true, //  显示高级按钮
					addAdvTabs: this.addAdvTabs, // 添加高级查询区自定义页签 (fun), return Dom
					advSearchClearEve: this.clearSearchEve.bind(this, false), //高级查询面板中的清空按钮 点击事件钩子，用于业务组清除自定义查询条件值
					onlyShowSuperBtn: true, // 只显示高级按钮
					replaceSuperBtn: this.state.json['verificationsheet-000121'] /* 国际化处理： 查询*/,
					hideBtnArea: true // 隐藏查询按钮区域,可以使用其他按钮调用props.search.openAdvSearch(searchId, true)显示查询框
				})}
				<div className="nc-singleTable-table-area&quot;">
					<div className="tabs-area">
						<NCTabs
							navtype="turn"
							activeKey={active}
							contenttype="moveleft"
							defaultActiveKey="1"
							onChange={(v) => {
								this.setState(
									{
										active: v
									},
									() => {
										this.handleChangeTable2();
									}
								);
							}}
						>
							<NCTabPane tab={this.state.json['verificationsheet-000122']} key="1">
								{/* 国际化处理： 核销汇总*/}
								<Table1
									onRef1={this.onRef1}
									table1datas={table1datas}
									caldisbutton={this.calDisabledButton.bind(this)}
								/>
							</NCTabPane>
							<NCTabPane tab={this.state.json['verificationsheet-000123']} key="2">
								{/* 国际化处理： 核销明细*/}
								<Table2
									onRef2={this.onRef2}
									transferData={transferData && transferData}
									table2datas={table2datas}
									table2Keys={table2Keys}
									allpks={allpks}
									ondisbutton={this.setDisabledButton.bind(this)}
									changeAllPks={this.changeAllPks.bind(this)}
								/>
							</NCTabPane>
						</NCTabs>
					</div>
				</div>
			</div>
		);
	}
}

SingleTable = createPage(
	{
		//initTemplate: initTemplate
	}
)(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
