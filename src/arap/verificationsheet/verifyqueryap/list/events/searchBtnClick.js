import {ajax,cacheTools,toast} from 'nc-lightapp-front';
import { funCode, searchId, pageId, cytjcode,tableId,detailId } from '../constants';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
        if(!props.form.isCheckNow(cytjcode,'warning')){
            return true;
        }

        let queryInfo = this.props.search.getQueryInfo(searchId);
        let data = {
            queryVO: queryInfo,
            queryConditionsVO: {
                model: props.form.getAllFormValue(cytjcode),
                pageid: pageId
            },
            pageId: pageId,
            funCode: funCode,
            tableCode:tableId,
            detailCode:detailId,
            arapFlag: 'ap',
            pk_org: props.form.getFormItemsValue(cytjcode,'pk_org')
        };
        ajax({
            url: '/nccloud/arap/verifyquery/query.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        if(data.cxfs==0){
                            if(data.grid){
                                this.setState({
                                    active:'1',
                                    table1datas:data.grid[tableId],
                                    transferData:[]
                                },() => {
                                    this.ChangeTable1Datas(data.grid[tableId]);
                                });
                                toast({ color: 'success', content: this.state.json['verificationsheet-000117']+data.grid[tableId].rows.length+this.state.json['verificationsheet-000118'] });/* 国际化处理： 查询成功，共,条*/
                            }else {
                                this.setState({
                                    active:'1',
                                    transferData:[]
                                },() => {
                                    this.ChangeTable1Datas( { rows: [] });
                                });
                                toast({ color: 'warning', content: this.state.json['verificationsheet-000119'] });/* 国际化处理： 未查询出符合条件的数据*/
                            }
                        }else if(data.cxfs==1){
                            if(data.grid){
                                this.setState({
                                    active:'2',
                                    table2datas:data.grid[detailId],
                                    allpks:data.grid[detailId].allpks,
                                    transferData:[]
                                },() => {
                                    this.ChangeTableDatas(data.grid[detailId]);
                                });
                                
                                toast({ color: 'success', content: this.state.json['verificationsheet-000117']+data.grid[detailId].allpks.length+this.state.json['verificationsheet-000118'] });/* 国际化处理： 查询成功，共,条*/
                             }else {
                                this.setState({
                                    active: '2',
                                    allpks:[],
                                    transferData:[]
                                },() => {
                                    this.ChangeTableDatas( { rows: [] });
                                });
                                toast({ color: 'warning', content: this.state.json['verificationsheet-000119'] });/* 国际化处理： 未查询出符合条件的数据*/
                            }
                        }
                        
                    }
                    
                }
            }
        });
    
};
