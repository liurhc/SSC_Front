import { ajax, createPage } from 'nc-lightapp-front';
import React, { Component } from 'react';
import { searchId, pageId, tableId, detailId } from './constants';
import { initTemplate,pageInfoClick } from './events';
/**
 *  应付管理，手工核销
 */
class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.searchId = searchId;
		this.detailId = detailId;
		this.tableId = tableId;
	}

	componentDidMount() {
		initTemplate.call(this, this.props);
		this.props.onRef2(this);
		this.getData();
	}
	//获取表格选中行数据
	getSelectedData2 = () => {
		return this.props.table.getCheckedRows(detailId);
	};

	//获取表格中所有行数据
	getAllData = () => {
		return this.props.table.getAllRows(detailId, false);
	};

	//请求列表数据
	getData = (transferDatas) => {
		if (this.props.table1datas == null || transferDatas != undefined) {
			if (transferDatas && transferDatas.length > 0) {
				let queryVOs = {
					pk_verifys: transferDatas,
					detailCode: detailId,
					pageId: pageId
				};
				ajax({
					url: '/nccloud/arap/verifyquery/detailquery.do',
					data: queryVOs,
					success: function(res) {
						//此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
						let { success, data } = res;
						if (success) {
							if (data) {
								this.props.table2datas = data[detailId];
								this.props.table.setAllTableData(detailId, data[detailId]);
								//this.props.allpks = data[detailId].allpks;
								this.props.changeAllPks(data[detailId].allpks);
							}
						}
					}.bind(this)
				});
			}else{
				this.props.table.setAllTableData(detailId, { rows: [] });
			}
		} else {
			this.props.table.setAllTableData(detailId, this.props.table2datas);
		}
	};

	//重新渲染表格数据
	setTableDatas = (data) => {
		this.props.table.setAllTableData(detailId, data);
	};
	//删除掉核销明细取消核销的数据
	delTableDataByindex = (indexArr) => {
		this.props.table.deleteTableRowsByIndex(detailId, indexArr, true);
	};

	onSelected(props, moduleId, record, index, status) {
		let datas = this.props.table.getAllTableData(detailId);
		let indexArr = [];
		let pk_verify = record.pk_verify.value; // 核销表表主键
		let busino = record.busino.value; // 核销批次号
		let pk_busidata = record.pk_busidata.value; // 借方业务表Pk
		let rowno = record.rowno.value; // 处理顺序号
		// 匹配条件
		let matchStr = pk_verify + '_' + busino + '_' + pk_busidata + '_' + rowno;
		for (let i = 0, len = datas.rows.length; i < len; i++) {
			let df_pk_verify = datas.rows[i].values.pk_verify.value; // 核销表表主键
			let df_busino = datas.rows[i].values.busino.value; // 核销批次号
			let df_pk_busidata = datas.rows[i].values.pk_busidata2.value; // 借方业务表Pk
			let df_rowno = datas.rows[i].values.rowno.value; // 处理顺序号
			let jf_pk_busidata = datas.rows[i].values.pk_busidata.value; // 加上汇兑损益的
			let df_matchStr = df_pk_verify + '_' + df_busino + '_' + df_pk_busidata + '_' + df_rowno;
			let jf_matchStr = df_pk_verify + '_' + df_busino + '_' + jf_pk_busidata + '_' + df_rowno;
			if (matchStr == df_matchStr || matchStr == jf_matchStr) {
				indexArr.push(i);
			}
		}
		if (status) {
            this.props.table2Keys.push(matchStr);
			this.props.table.selectTableRows(detailId, indexArr, true);
		} else {
			let pk_busidata2 = record.pk_busidata2.value; // 借方业务表Pk
			let matchStr1 = pk_verify + '_' + busino + '_' + pk_busidata2 + '_' + rowno;
            for (let j = 0, lenth = this.props.table2Keys.length; j < lenth; j++) {
                if(matchStr==this.props.table2Keys[j]||matchStr1==this.props.table2Keys[j]){
					this.props.table2Keys.splice(j,1);
					j--;
                }
            }
			this.props.table.selectTableRows(detailId, indexArr, false);
		}

		this.props.ondisbutton();
	}
	onSelectedAll(props, moduleId, status, length) {
		let datas = props.table.getAllTableData(detailId);
		if(length>0){
			for(let i = 0, len = datas.rows.length; i < len; i++){
				let pk_verify = datas.rows[i].values.pk_verify.value; // 核销表表主键
				let busino = datas.rows[i].values.busino.value; // 核销批次号
				let pk_busidata = datas.rows[i].values.pk_busidata.value; // 借方业务表Pk
				let rowno = datas.rows[i].values.rowno.value; // 处理顺序号
				// 匹配条件
				let matchStr = pk_verify + '_' + busino + '_' + pk_busidata + '_' + rowno;
				if(status){
					this.props.table2Keys.push(matchStr);
				}else{
					for (let j = 0, lenth = this.props.table2Keys.length; j < lenth; j++) {
						if(matchStr==this.props.table2Keys[j]){
							this.props.table2Keys.splice(j,1);
							j--;
						}
					}
				}
			}
		}
		this.props.ondisbutton();
	}
	render() {
		let { table, search, modal } = this.props;
		let { NCCreateSearch } = search;
		let { createSimpleTable } = table;
		return (
			<div>
				<div className="nc-singleTable-table-area&quot;">
					{createSimpleTable(detailId, {
						showCheck: true,
                        useFixedHeader: true,
                        handlePageInfoChange: pageInfoClick.bind(this),
						showIndex: true,
						showTotal: false,
						onSelected: this.onSelected.bind(this),
						onSelectedAll: this.onSelectedAll.bind(this)
					})}
				</div>
			</div>
		);
	}
}

SingleTable = createPage(
	{
		//initTemplate: initTemplate
	}
)(SingleTable);

export default SingleTable;
