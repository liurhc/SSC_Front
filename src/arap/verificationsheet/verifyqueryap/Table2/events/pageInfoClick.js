import {ajax} from 'nc-lightapp-front';
import { detailId,pageId} from '../constants';

export default function (props, config, pks) {
    let data = {
        pk_verifyDetails: pks,
        pageId: pageId,
        detailCode:detailId
    };
    ajax({
        url: '/nccloud/arap/verifyquery/detailquerybyids.do',
        data: data,
        success: function (res) {
            let { success, data } = res;
            if (success) {
                if (data) {
                    props.table.setAllTableData(detailId, data[detailId]);
                    let keys = '';
                    let indexArr = [];
                    for (let j = 0, leng = props.table2Keys.length; j < leng; j++) {
                        keys = keys+props.table2Keys[j];
                    }
                    for (let i = 0, len = data[detailId].rows.length; i < len; i++) {
                        let df_pk_verify = data[detailId].rows[i].values.pk_verify.value; // 核销表表主键
                        let df_busino = data[detailId].rows[i].values.busino.value; // 核销批次号
                        let df_pk_busidata = data[detailId].rows[i].values.pk_busidata2.value; // 借方业务表Pk
                        let df_rowno = data[detailId].rows[i].values.rowno.value; // 处理顺序号
                        let jf_pk_busidata = data[detailId].rows[i].values.pk_busidata.value; // 加上汇兑损益的
                        let df_matchStr = df_pk_verify + '_' + df_busino + '_' + df_pk_busidata + '_' + df_rowno;
                        let jf_matchStr = df_pk_verify + '_' + df_busino + '_' + jf_pk_busidata + '_' + df_rowno;
                        
                        if (keys.indexOf(df_matchStr)!=-1 || keys.indexOf(jf_matchStr)!=-1) {
                            indexArr.push(i);
                        }
                    }
                    props.table.selectTableRows(detailId, indexArr, true);
                    props.ondisbutton();
                } else {
                    props.table.setAllTableData(detailId, { rows: [] });
                }
            }
        }
    });
}
