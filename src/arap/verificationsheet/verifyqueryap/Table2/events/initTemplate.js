import { ajax } from 'nc-lightapp-front';
import { appcode, pageId, detailId } from '../constants';

export default function(props) {
	const that = this;
	props.createUIDom(
		{
			pagecode: pageId, //页面code
			appcode: appcode //小应用code
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					// let data1 = meta[detailId].items;
					// for(var i=0;i<data1.length;i++){
					// 	let temlte = data1[i].attrcode.split('.')[1];
					// 	meta[detailId].items[i].attrcode =temlte;
					// }
					modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	meta[detailId].items = meta[detailId].items.map((item, key) => {
		item.width = 160;
		if (item.attrcode == 'billno') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							billLink(record, props);
						}}
					>
						{record.billno && record.billno.value}
					</a>
				);
			};
		}
		if (item.attrcode == 'billno2') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							billLink2(record, props);
						}}
					>
						{record.billno2 && record.billno2.value}
					</a>
				);
			};
		}
		return item;
	});
	return meta;
}
//联查单据
function billLink(record, props) {
	ajax({
		url: '/nccloud/arap/arappub/linkarapbill.do',
		async: false,
		data: {
			pk_bill: record.pk_bill.value,
			pk_tradetype: record.pk_tradetype.value
		},
		success: (res) => {
			props.openTo(res.data.url, res.data.condition);
		}
	});
}
//联查单据
function billLink2(record, props) {
	ajax({
		url: '/nccloud/arap/arappub/linkarapbill.do',
		async: false,
		data: {
			pk_bill: record.pk_bill2.value,
			pk_tradetype: record.pk_tradetype2.value
		},
		success: (res) => {
			props.openTo(res.data.url, res.data.condition);
		}
	});
}
function getVisibleButton(that, data, key) {
	let flag = true; //按钮的状态，默认为true
	let isinit = data.values.isinit.value ? data.values.isinit.value : null; //是否默认
	switch (key) {
		case 'SetDefault': //设为默认
			if (isinit == that.state.json['verificationsheet-000090'] || isinit == '02006ver-0383') {
				/* 国际化处理： 默认*/
				flag = false;
			}
			break;
		case 'CancelDefault': //取消默认
			if (isinit == '' || isinit == null) {
				flag = false;
			}
			break;
		default:
			break;
	}
	return flag;
}
