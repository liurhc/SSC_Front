import { ajax, base } from 'nc-lightapp-front';
import { pagecode, tableId, appcode, funCode, searchId } from '../constants';
const { NCMessage } = base;

export default function (e,props, key, text, record, index) {
    switch (key) {
        case 'Edit':
            props.search.openAdvSearch( searchId, true );
            break;
        case 'Delete':
            e.setState({
                pk_bill: record.values.pk_verify_fa.value,
                ts: record.values.ts.value
            })
            props.modal.show('delete');
            break;
        case 'SetDefault':
            ajax({
                url: '/nccloud/arap/verifyfa/setdefault.do',
                data: {
                    pk_bill: record.values.pk_verify_fa.value,
                    ts: record.values.ts.value,
                    pageId: pagecode,
                    funCode: funCode
                },
                success: (res) => {
                    if (res) {
                        props.editTable.setTableData(tableId, res.data[tableId]);
                        NCMessage.create({ content: e.state.json['verificationsheet-000094'], color: 'success', position: 'bottom' });/* 国际化处理： 操作成功*/
                    }
                }
            });
            break;
        case 'CancelDefault':
            ajax({
                url: '/nccloud/arap/verifyfa/canceldefault.do',
                data: {
                    pk_bill: record.values.pk_verify_fa.value,
                    ts: record.values.ts.value,
                    pageId: pagecode,
                    funCode: funCode
                },
                success: (res) => {
                    if (res) {
                        props.editTable.setTableData(tableId, res.data[tableId]);
                        NCMessage.create({ content: e.state.json['verificationsheet-000094'], color: 'success', position: 'bottom' });/* 国际化处理： 操作成功*/
                    }
                }
            });
            break;
        default:
            break;
    }
};
