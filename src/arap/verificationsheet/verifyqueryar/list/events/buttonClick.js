import { ajax, base, toast, print, getBusinessInfo  } from 'nc-lightapp-front';
import { pageId,detailId, tableId, appcode, cytjcode,hxcode,publicCode, searchId } from '../constants';
import { setFormEditable } from '../../../../public/components/pubUtils/billPubUtil.js';
import { headButton } from '../../../../public/components/pubUtils/buttonName.js';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';
import afterEvent from './afterEvent';
export default function buttonClick(props, id) {
    let pk_org = getContext(loginContextKeys.pk_org);
    let org_Name = getContext(loginContextKeys.org_Name);
    switch (id) {
        case 'Query':
         props.search.openAdvSearch(searchId, true,()=>{
            let isFirstQuery = this.state.isFirstQuery;
            if (!isFirstQuery) {
                return;
            }
            props.search.clearSearchArea(searchId);
            props.form.EmptyAllFormValue(cytjcode);

            //设置默认值
			props.form.setFormItemsValue(cytjcode, { 'objTypeCBox': { value: 'all', display: this.state.json['verificationsheet-000113'] }});/* 国际化处理： 全部*/
			// 本方开始日期	本方结束日期
			let date = getBusinessInfo().businessDate.substring(0, 10)

			props.form.setFormItemsValue(cytjcode, { 'beginDateRef': { value: date }, 'endDateRef': { value: date } });
			props.form.setFormItemsValue(cytjcode, { 'verifyModeBox': { value: 0,display:this.state.json['verificationsheet-000035'] }, 'queryModeBox': { value: 0,display:this.state.json['verificationsheet-000114'] } });/* 国际化处理： 同币种核销,汇总查询*/
             //常用条件，组织为空，只有组织可编辑
            if (pk_org && org_Name) {
                props.form.setFormItemsValue(cytjcode, { 'pk_org': { value: pk_org, display: org_Name } });
                afterEvent(props, cytjcode, 'pk_org', { value: pk_org, display: org_Name })
                return
            }
            setFormEditable(props,cytjcode, hxcode, false);
            setFormEditable(props, cytjcode,publicCode, false);
            props.renderItem('form', cytjcode, 'objNameRef', null);
          
            this.setState({ isFirstQuery: false })
         });

           
            break;
        //取消核销
        case 'CancelVerify':
            let flag =  this.state.active;
            if(flag==1){
                    let selectedData = this.getTable1Data();
                    let indexArr=[];
                    if (selectedData.length == 0) {
                        toast({ color: 'warning', content: this.state.json['verificationsheet-000115'] });/* 国际化处理： 请至少选择一行数据！*/
                        return;
                    }
                    let linkVOs = [];
                    for (var i = 0; i < selectedData.length; i++) {
                        let o = {
                            busino: selectedData[i].data.values.busino.value,
                            pk_group:selectedData[i].data.values.pk_group.value,
                            pk_org:selectedData[i].data.values.pk_org.value,
                            creator:selectedData[i].data.values.creator.value,
                            busidate:selectedData[i].data.values.busidate.value,
                        }
                        linkVOs.push(o);
                        indexArr.push(selectedData[i].index);
                    }
                    let data ={
                        linkVOs:linkVOs,
                        arapFlag:'ar'
                    };
                
                    ajax({
                        url: '/nccloud/arap/verifyquery/cancelVerify.do',
                        data: data,
                        success: (res) => {
                            if (res.success) {
                                toast({ color: 'success', content: this.state.json['verificationsheet-000116'] });/* 国际化处理： 取消核销成功*/
                                this.delTableByindex1(indexArr);
                            }
                        },
                        error: (res) => {
                            if (!res.success) {
                                toast({ color: 'danger', content: res.message });
                            // this.getData();
                            }
                        }
                    });
            }else{
                let selectedData2 = this.getTable2Data();
                if (selectedData2.length == 0) {
                    toast({ color: 'warning', content: this.state.json['verificationsheet-000115'] });/* 国际化处理： 请至少选择一行数据！*/
                    return;
                }
                ajax({
                    url: '/nccloud/arap/verifyquery/cancelDtlVerify.do',
                    data: {
                        pk_verifys:this.state.transferData,
                        cancelKeys:this.state.table2Keys,
                        allpks:this.state.allpks,
                        detailCode:detailId,
                        pageId:pageId,
                        arapFlag:'ar'
                    },
                    success: (res) => {
                        if(res.data){
                            this.setState({
                                active:'2',
                                table2datas:res.data[detailId],
                                table2Keys:[],
                                allpks:res.data[detailId].allpks
                            },() => {
                                this.ChangeTableDatas(res.data[detailId]);
                            });
                            
                        }else{
                            this.setState({
                                active:'2',
                                table2datas:[],
                                table2Keys:[],
                                allpks:[]
                            },() => {
                                this.ChangeTableDatas( { rows: [] });
                            });
                        }
                        if(!this.getTable2Data().length>0){
                            this.props.button.setButtonDisabled('CancelVerify', true);
                        }
                        toast({ color: 'success', content: this.state.json['verificationsheet-000116'] });/* 国际化处理： 取消核销成功*/
                    },
                    error: (res) => {
                        if (!res.success) {
                            toast({ color: 'danger', content: res.message });
                        // this.getData();
                        }
                    }
                });
            }
            
        break;
        case headButton.Print://打印
        if (this.state.active=='1'){
                var resoureData = this.getTableAllData();
                var oids = [];
                for (var i = 0; i < resoureData.length; i++) {
                    oids.push(resoureData[i].values.pk_verify.value);
                }
                var printData = {
                    billtype: '',  //单据类型
                    appcode: appcode,      //功能节点编码，即模板编码   
                    nodekey: 'list',     //模板节点标识  
                    oids: oids,    // 功能节点的数据主键
                    userjson: ''//单据类型,billtype不是必需字段，后台没有设置接收字段，以userjson代替
                }
                print(
                    'html',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                    '/nccloud/arap/arappub/printVerify.do', //后台服务url
                    printData,
                    false
                )
            }else{
               
                var printData = {
                    billtype: '',  //单据类型
                    appcode: appcode,      //功能节点编码，即模板编码   
                    nodekey: 'card',     //模板节点标识  
                    oids: this.state.allpks,    // 功能节点的数据主键
                    userjson: ''//单据类型,billtype不是必需字段，后台没有设置接收字段，以userjson代替
                }
                print(
                    'html',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                    '/nccloud/arap/arappub/printVerifyDetail.do', //后台服务url
                    printData,
                    false
                )
            }
            break;
        default:
            break
    }
}
