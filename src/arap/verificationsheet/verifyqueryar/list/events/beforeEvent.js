import { cytjcode,hxcode,publicCode } from '../constants';
import { ajax, getBusinessInfo } from 'nc-lightapp-front';

export default function beforeEvent(props, moduleId, key, value, data) {
	// props, moduleId(区域id), key(操作的键), value（当前值）,data(当前表单所有值)
	//常用条件编辑后事件
	// if (moduleId == cytjcode) {
		let meta = props.meta.getMeta();
		//表头科目,表体科目
		if (key == 'headSubjRef' || key == 'itemSubjRef') {
			ajax({
				url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
				data: {
					pk_org: props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null,
				},
				async: false,
				success: (res) => {
					if (res.success) {
						meta[publicCode].items.map((item) => {
							if (item.attrcode == 'headSubjRef' || item.attrcode == 'itemSubjRef') {
								item.isAccountRefer = true;          // true: 组织级科目参照，false：集团或全局政策科目
								item.isMultiSelectedEnabled = false;
								item.queryCondition = () => {
									return {
										DataPowerOperationCode: 'fi',//使用权组
										isDataPowerEnable: 'Y',
										pk_accountingbook: res.data,
										datestr: getBusinessInfo().businessDate
									}
								}
							}
							return item;
						})
					}
				}
			});
		}
		if(key == 'pk_org'){
			let meta = props.meta.getMeta();
			
			//this.state.pk_accountingbook = res.data;
			meta[cytjcode].items.map((item) => {
				let attrcode = item.attrcode;
				if (attrcode == 'pk_org') {
					item.queryCondition = () => {
						return {
							AppCode:this.props.getSearchParam('c'),
							DataPowerOperationCode: 'fi',//使用权组
							isDataPowerEnable: 'Y',
							TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
						};
					}
				}
			});
			
		}
		props.meta.setMeta(meta);
	// }


	return true;
}
