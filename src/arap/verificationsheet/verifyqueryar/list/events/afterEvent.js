import { ajax } from 'nc-lightapp-front';
import {  cytjcode,hxcode,publicCode } from '../constants';
import { setFormEditable } from '../../../../public/components/pubUtils/billPubUtil.js';
export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
	

	//常用条件编辑后事件
	if (key == 'pk_org') {
		//设置本方单据类型	对方单据类型不可编辑
		setFormEditable(props,cytjcode, cytjcode, true);
		setFormEditable(props, cytjcode,hxcode, true);
		setFormEditable(props, cytjcode,publicCode, true);
		let objname = props.form.getFormItemsValue(cytjcode,'objTypeCBox');
		if(objname.value=='all'){
			props.form.setFormItemsDisabled(cytjcode,{ 'objNameRef': true })
		}else if(objname.value == 'gys' || objname.value == 'bm' || objname.value == 'ywy'||objname.value == 'kh'){
			props.form.setFormItemsDisabled(cytjcode,{ 'objNameRef': false })
			if (objname.value == 'kh') {
				props.meta.getMeta()[hxcode].items.map((item) => {
					if (item.attrcode == 'objNameRef') {
						item.refName = this.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
						item.itemType = 'refer'
						item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
						item.queryCondition = () => {
							return {
								isDataPowerEnable: 'Y',
								DataPowerOperationCode: 'fi',
								pk_org:props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null
							};
						}
					}
					
					return item;
				})
			}else if (objname.value == 'gys') {
				props.meta.getMeta()[hxcode].items.map((item) => {
					if (item.attrcode == 'objNameRef') {
						item.refName = this.state.json['verificationsheet-000039']/* 国际化处理： 供应商档案*/
						item.itemType = 'refer'
						item.refcode = 'uapbd/refer/supplier/SupplierRefTreeGridRef/index'
						item.queryCondition = () => {
							return {
								isDataPowerEnable: 'Y',
								DataPowerOperationCode: 'fi',
								pk_org:props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null
							};
						}
					}
					return item;
				})
			}else if(objname.value == 'bm'){
				props.meta.getMeta()[hxcode].items.map((item) => {
					if (item.attrcode == 'objNameRef') {
						item.refName = this.state.json['verificationsheet-000002']/* 国际化处理： 部门*/
						item.itemType = 'refer'
						item.refcode = 'uapbd/refer/org/DeptTreeRef/index'
						item.isShowUnit = true
						item.unitCondition = () => {
							return {
								pkOrgs:  props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null,
								TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder',
							};
						}
						item.queryCondition = () => {
							return {
								isDataPowerEnable: 'Y',
								DataPowerOperationCode: 'fi',
								busifuncode:'all',
								pk_org:props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null
							};
						}
					}
					return item;
				})
			}else if(objname.value == 'ywy'){
				props.meta.getMeta()[hxcode].items.map((item) => {
					if (item.attrcode == 'objNameRef') {
						item.refName = this.state.json['verificationsheet-000042']/* 国际化处理： 人员*/
						item.itemType = 'refer'
						item.refcode = 'uapbd/refer/psninfo/PsndocTreeGridRef/index'
						item.isShowUnit = true
						item.unitCondition = () => {
							return {
								pkOrgs:  props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null,
								TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder',
							};
						}
						item.queryCondition = () => {
							return {
								isDataPowerEnable: 'Y',
								DataPowerOperationCode: 'fi',
								busifuncode:'all',
								pk_org:props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null,
								pk_dept: props.form.getFormItemsValue(publicCode, 'deptRef').value ? props.form.getFormItemsValue(cytjCode, 'deptRef').value : null
							};
						}
					}
					return item;
				})
			}
			props.form.setFormItemsValue(cytjcode, { 'objNameRef': { value: null, display: null } });
		}
	}
	if (key == 'verifyModeBox') {
		if(value.value==0){/* 国际化处理： 同币种核销*/
			props.meta.getMeta()[hxcode].items.map((item) => {
				if (item.attrcode == 'currTypeRef') {
					item.itemType = 'refer'
					item.refcode ='uapbd/refer/pubinfo/CurrtypeGridRef/index.js'
					item.disabled = false
					item.queryCondition = () => {
						return {
							isDataPowerEnable: 'Y',
							DataPowerOperationCode: 'fi',
						};
					}
				}
				
				return item;
			})
		}else{
			props.meta.getMeta()[hxcode].items.map((item) => {
				if (item.attrcode == 'currTypeRef') {
					item.itemType = 'input'
					item.disabled = true
				}
				
				return item;
			})
			
		}
		props.form.setFormItemsValue(cytjcode, { 'currTypeRef': { value: null, display: null } });
		props.renderItem('form', hxcode, 'currTypeRef', null) ;
	}
	if (key == 'objTypeCBox') {
		if (value.value == 'gys' || value.value == 'bm' || value.value == 'ywy'||value.value == 'kh') {
			props.form.setFormItemsDisabled(cytjcode,{ 'objNameRef': false })
			if (value.value == 'kh') {
				props.meta.getMeta()[hxcode].items.map((item) => {
					if (item.attrcode == 'objNameRef') {
						item.itemType = 'refer'
						item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index'
						item.refName = this.state.json['verificationsheet-000038']/* 国际化处理： 客户档案*/
						item.queryCondition = () => {
							return {
								isDataPowerEnable: 'Y',
								DataPowerOperationCode: 'fi',
								pk_org:props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null
							};
						}
					}
					
					return item;
				})
			}else if (value.value == 'gys') {
				props.meta.getMeta()[hxcode].items.map((item) => {
					if (item.attrcode == 'objNameRef') {
						item.itemType = 'refer'
						item.refcode = 'uapbd/refer/supplier/SupplierRefTreeGridRef/index'
						item.refName = this.state.json['verificationsheet-000039']/* 国际化处理： 供应商档案*/
						item.queryCondition = () => {
							return {
								isDataPowerEnable: 'Y',
								DataPowerOperationCode: 'fi',
								pk_org:props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null
							};
						}
					}
					return item;
				})
			}else if(value.value == 'bm'){
				props.meta.getMeta()[hxcode].items.map((item) => {
					if (item.attrcode == 'objNameRef') {
						item.itemType = 'refer'
						item.refcode = 'uapbd/refer/org/DeptTreeRef/index'
						item.refName = this.state.json['verificationsheet-000002']/* 国际化处理： 部门*/
						item.isShowUnit = true
						item.unitCondition = () => {
							return {
								pkOrgs:  props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null,
								TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder',
							};
						}
						item.queryCondition = () => {
							return {
								isDataPowerEnable: 'Y',
								DataPowerOperationCode: 'fi',
								pk_org:props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null
							};
						}
					}
					return item;
				})
			}else if(value.value == 'ywy'){
				props.meta.getMeta()[hxcode].items.map((item) => {
					if (item.attrcode == 'objNameRef') {
						item.itemType = 'refer'
						item.refcode = 'uapbd/refer/psninfo/PsndocTreeGridRef/index'
						item.refName = this.state.json['verificationsheet-000042']/* 国际化处理： 人员*/
						item.isShowUnit = true
						item.unitCondition = () => {
							return {
								pkOrgs:  props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null,
								TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder',
							};
						}
						item.queryCondition = () => {
							return {
								isDataPowerEnable: 'Y',
								DataPowerOperationCode: 'fi',
								pk_org:props.form.getFormItemsValue(cytjcode, 'pk_org') ? props.form.getFormItemsValue(cytjcode, 'pk_org').value : null,
								pk_dept: props.form.getFormItemsValue(publicCode, 'deptRef').value ? props.form.getFormItemsValue(cytjCode, 'deptRef').value : null
							};
						}
					}
					return item;
				})
			}
			props.form.setFormItemsValue(cytjcode, { 'objNameRef': { value: null, display: null } });
		}else if(value.value == 'all'){
			props.form.setFormItemsDisabled(cytjcode,{ 'objNameRef': true })
			props.form.setFormItemsValue(cytjcode, { 'objNameRef': { value: null, display: null } });
		}
		//重新渲染对象名称
		props.renderItem('form', hxcode, 'objNameRef', null) ;
	}

}
