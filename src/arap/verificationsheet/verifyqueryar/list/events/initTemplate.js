import { appcode, pageId, tableId, searchId, cytjcode, hxcode, publicCode } from '../constants';
import { modifierSearchMeta } from '../../../public/filter/arapVerifyQueryRefFilter';
import { modifierFormMeta } from '../../../public/filter/verifyReferFilter';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';

export default function (props) {
	const that = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面code
			appcode: appcode//小应用code
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(that, props, meta)
					//添加参照过滤
					modifierSearchMeta(searchId, cytjcode, props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
			}
		}
	)
}

function modifierMeta(e, props, meta) {

	meta[searchId].items = meta[searchId].items.map((item, key) => {
		item.col = '3';
		return item;
	})
	meta[cytjcode].status = 'edit';
	//添加参照过滤
	modifierFormMeta(cytjcode, cytjcode, props, meta);
	modifierFormMeta(cytjcode, hxcode, props, meta);
	modifierFormMeta(cytjcode, publicCode, props, meta);
	return meta;
}

