
/**
 * 查询区域
 */
export const searchId = '20060VQ_query';



/**
 * 表体区域
 */
export const tableId = '20060VQ_body_main';

/**
 * 表体区域
 */
export const detailId = '20060VQ_body_detail';

/**
 * 页面编码
 */
export const pageId = '20060VQ_LIST';


/**
 * 小应用ID
 */
export const appcode = '20060VQ';

/**
 * nc端功能节点号
 */
export const funCode = '20060VQ';
