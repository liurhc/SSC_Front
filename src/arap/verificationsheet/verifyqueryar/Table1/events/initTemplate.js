import { ajax } from 'nc-lightapp-front';
import { appcode, pageId, tableId, searchId,tableId_bf,tableId_df } from '../constants';

export default function (props) {
	const that = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面code
			appcode: appcode//小应用code
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(that, props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function modifierMeta(e, props, meta) {
	let { editTable, search } = props;
	let { NCCreateSearch } = search;
	return meta;
}

function getVisibleButton(that,data, key) {
	let flag = true;//按钮的状态，默认为true
	let isinit = data.values.isinit.value ? data.values.isinit.value : null;//是否默认
	switch (key) {
		case 'SetDefault'://设为默认
			if (isinit == this.state.json['verificationsheet-000090'] || isinit == "02006ver-0383") {/* 国际化处理： 默认*/
				flag = false;
			}
			break;
		case 'CancelDefault'://取消默认
			if (isinit == "" || isinit == null) {
				flag = false;
			}
			break;
		default:
			break;
	}
	return flag;
}
