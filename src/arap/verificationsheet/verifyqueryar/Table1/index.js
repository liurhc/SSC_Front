import { ajax, base, createPage, cacheTools } from 'nc-lightapp-front';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { funCode, searchId, pageId,tableId } from './constants';
import { initTemplate } from './events';
//import './index.less';
const { NCMessage } = base;

/**
 *  应付管理，手工核销
 */
class SingleTable extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.searchId = searchId;
        this.tableId = tableId;
        this.state = {
            pk_bill: null,
            ts: null,
        }
    }
    componentDidMount() {
        this.props.onRef1(this);
        initTemplate.call(this, this.props);
       // let pk_verifys = ['0001Z31000000005YJ0T'];
        let pks = this.props.getUrlParam('pks');
        if(pks!=null){
            this.getData(false);
        }
    
    }

    //请求列表数据
    getData = (showOff = false) => {
    
        let pks = this.props.getUrlParam('pks');
        let pksArr = pks.split('_');
        let queryVOs = {
            pks:pksArr,
            tableCode:tableId,
            pageId:pageId
        };
        ajax({
            url: '/nccloud/arap/verifyquery/verifyMainquery.do',
            data:queryVOs,
            success: function (res)  {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
                let { success,data} = res;
                if (success) {
                    if(data){
                        this.props.table2datas = data[tableId];
                        this.props.editTable.setTableData(tableId,data[tableId]);
                        
                    }
                }
            }.bind(this)
        });
       
    };

    //获取表格选中行数据
    getSelectedData = () => {
        return this.props.editTable.getCheckedRows(tableId);
    }

    //获取表格中所有行数据
    getAllData = () => {
        return this.props.editTable.getAllRows(tableId, false);
    }
    
    onSelected(props, moduleId, record, index, status) {
        let datas =  this.props.editTable.getAllRows(tableId);
        let indexArr = [];
        let busino = record.values.busino.value
        for(let i=0;i<datas.length;i++){
            if (busino==datas[i].values.busino.value) {  
                    indexArr.push(i);
            }
        }
        if(status){
            this.props.editTable.selectTableRows(tableId,indexArr,true);
        }else{
            this.props.editTable.selectTableRows(tableId,indexArr,false);
        }
        this.props.caldisbutton();
    }
    onSelectedAll(props, moduleId, status, length) {
        
        this.props.caldisbutton();
    }
    //重新渲染表格数据
    setTable1Datas = (data) => {
        this.props.editTable.setTableData(tableId, data);
    }

    //删除掉核销明细取消核销的数据
    delTableDataByindex = (indexArr) => {
        this.props.editTable.deleteTableRowsByIndex(tableId, indexArr,true);
    }

    render() {
        let { editTable, search, modal } = this.props;
        let { NCCreateSearch } = search;
        let { createEditTable } = editTable;
        let searchDatas = cacheTools.get('searchDatas');
        return (
            <div>
                <div className='nc-singleTable-table-area"'>
                    {createEditTable(tableId, {
                        showCheck: true,
                        useFixedHeader: true,
                        showIndex: true,		
                        showTotal: false, 
                        onSelected:this.onSelected.bind(this),
                        onSelectedAll:this.onSelectedAll.bind(this)
                       // onSelected: this.getSelectedData.bind(this)
                    })}
                </div>
            </div>
        );
    }
}

SingleTable = createPage({
    //initTemplate: initTemplate
})(SingleTable);

export default SingleTable;
