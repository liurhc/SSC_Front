import {
	cytjCode,
	hxgzCode,
	hxfsCode,
	searchId,
	tableId,
	appCode,
	pageCode,
	bfCode,
	dfCode,
	publicCode
} from '../constants';
import tableButtonClick from './tableButtonClick.js';
import { modifierFormMeta } from '../../../public/filter/verifyReferFilter';
import { modifierSearchMeta } from '../../../public/filter/arapVerificatDefRefFilter';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';

export default function(props) {
	const that = this;
	props.createUIDom(
		{
			pagecode: pageCode, //页面code
			appcode: appCode //小应用code
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(that, props, meta);
					props.meta.setMeta(meta);
					modifierSearchMeta(searchId, cytjCode, props, meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('Delete', '确定删除吗？'); /*删除信息提示框*/
				}
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
			}
		}
	);
}

function modifierMeta(that, props, meta) {
	meta[cytjCode].status = 'add';
	meta[hxgzCode].status = 'add';
	meta[hxfsCode].status = 'add';

	//添加操作列
	meta[tableId].items.push({
		label: that.state.json['verificationsheet-000014'] /* 国际化处理： 操作*/,
		itemtype: 'customer',
		attrcode: 'opr',
		width: '160px',
		visible: true,
		// fixed: 'right',
		render: (text, record, index) => {
			let buttonAry = [ 'Edit_inner', 'Delete_inner', 'SetDefault_inner', 'CancelDefault_inner' ];
			let trueBtn = [];
			for (let i = 0; i < buttonAry.length; i++) {
				let flag = getVisibleButton(that, record, buttonAry[i]);
				if (flag) {
					trueBtn.push(buttonAry[i]);
				}
			}
			return props.button.createOprationButton(trueBtn, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(that, props, key, text, record, index)
			});
		}
	});

	//添加参照过滤
	modifierFormMeta(cytjCode, cytjCode, props, meta);
	modifierFormMeta(cytjCode, bfCode, props, meta);
	modifierFormMeta(cytjCode, dfCode, props, meta);
	modifierFormMeta(cytjCode, publicCode, props, meta);

	return meta;
}

function getVisibleButton(that, data, key) {
	let flag = true; //按钮的状态，默认为true
	let isinit = data.values.isinit.value ? data.values.isinit.value : null; //是否默认
	switch (key) {
		case 'SetDefault_inner': //设为默认
			if (isinit == that.state.json['verificationsheet-000090'] || isinit == '02006ver-0383') {
				/* 国际化处理： 默认*/
				flag = false;
			}
			break;
		case 'CancelDefault_inner': //取消默认
			if (isinit == '' || isinit == null) {
				flag = false;
			}
			break;
		default:
			break;
	}
	return flag;
}
