import { buttonClick, setDefaultValue } from './buttonClick';
import initTemplate from './initTemplate';
import tableButtonClick from './tableButtonClick';
import saveClick from './saveClick';
import afterEvent from './afterEvent';
import beforeEvent from './beforeEvent';
export { buttonClick, setDefaultValue, initTemplate, tableButtonClick, saveClick, afterEvent,beforeEvent };
