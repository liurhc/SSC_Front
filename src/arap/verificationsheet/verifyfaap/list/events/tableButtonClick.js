import { ajax, base, toast } from 'nc-lightapp-front';
import {
	cytjCode,
	hxgzCode,
	hxfsCode,
	searchId,
	tableId,
	appCode,
	pageCode,
	funCode,
	bfCode,
	dfCode,
	publicCode
} from '../constants';

export default function(that, props, key, text, record, index) {
	switch (key) {
		case 'Edit_inner':
			that.pk_bill = record.values.pk_verify_fa.value;
			that.ts = record.values.ts.value;
			//前端需要传递（pk_bill	cytjCode	hxgzCode	hxfsCode	 pageId）
			ajax({
				url: '/nccloud/arap/verifyfa/edit.do',
				data: {
					pk_bill: record.values.pk_verify_fa.value,
					pageId: pageCode,
					cytjCode: cytjCode,
					hxgzCode: hxgzCode,
					hxfsCode: hxfsCode
				},
				success: (res) => {
					if (res.success) {
						if (res.data) {
							writeBack(that, res, props);
						}
					}
				},
				error: function(data) {
					toast({ color: 'danger', content: data.message });
				}
			});

			props.search.openAdvSearch(searchId, true);
			break;
		case 'Delete_inner':
			that.pk_bill = record.values.pk_verify_fa.value;
			that.ts = record.values.ts.value;
			that.delConfirm();
			break;
		case 'SetDefault_inner':
			ajax({
				url: '/nccloud/arap/verifyfa/setdefault.do',
				data: {
					pk_bill: record.values.pk_verify_fa.value,
					ts: record.values.ts.value,
					pageId: pageCode,
					funCode: funCode
				},
				success: (res) => {
					if (res) {
						props.editTable.setTableData(tableId, res.data[tableId]);
						toast({
							content: that.state.json['verificationsheet-000094'],
							color: 'success'
						}); /* 国际化处理： 操作成功*/
					}
				},
				error: function(data) {
					toast({ color: 'warning', content: data.message });
				}
			});
			break;
		case 'CancelDefault_inner':
			ajax({
				url: '/nccloud/arap/verifyfa/canceldefault.do',
				data: {
					pk_bill: record.values.pk_verify_fa.value,
					ts: record.values.ts.value,
					pageId: pageCode,
					funCode: funCode
				},
				success: (res) => {
					if (res) {
						props.editTable.setTableData(tableId, res.data[tableId]);
						toast({
							content: that.state.json['verificationsheet-000094'],
							color: 'success'
						}); /* 国际化处理： 操作成功*/
					}
				},
				error: function(data) {
					toast({ color: 'warning', content: data.message });
				}
			});
			break;
		default:
			break;
	}
}

function writeBack(that, res, props) {
	if (res.data[0]) {
		props.form.EmptyAllFormValue(cytjCode);
		props.form.setAllFormValue({ [cytjCode]: res.data[0]['cytjArea'] });
		//设置方案名称、本方单据类型、对方单据类型不可编辑
		props.form.setFormItemsDisabled(cytjCode, { name: true });
		props.form.setFormItemsDisabled(cytjCode, { jfBillTypeBox: true });
		props.form.setFormItemsDisabled(cytjCode, { dfBillTypeBox: true });
		//根据本方核销对象设置本方对象名称参照
		let meta = props.meta.getMeta();
		let jf_hxdxCBox = props.form.getFormItemsValue(cytjCode, 'jf_hxdxCBox').value;
		let df_hxdxCBox = props.form.getFormItemsValue(cytjCode, 'df_hxdxCBox').value;
		setRefCodeByHxdx(that, props, meta, jf_hxdxCBox, 'jf_dxmcRef', bfCode);
		setRefCodeByHxdx(that, props, meta, df_hxdxCBox, 'df_dxmcRef', dfCode);
	}
	if (res.data[1]) {
		props.form.EmptyAllFormValue(hxgzCode);
		props.form.setAllFormValue({ [hxgzCode]: res.data[1][hxgzCode] });
		//设置金额方向不可编辑
		let hldcChkBox = props.form.getFormItemsValue(hxgzCode, 'hldcChkBox');
		if (hldcChkBox.value == true) {
			props.form.setFormItemsDisabled(hxgzCode, { jefxCmbBox: true });
		}
	}
	if (res.data[2]) {
		props.form.EmptyAllFormValue(hxfsCode);
		props.form.setAllFormValue({ [hxfsCode]: res.data[2][hxfsCode] });
	}
}

function setRefCodeByHxdx(that, props, meta, hxdxValue, dxmcField, areaCode) {
	if (hxdxValue == 'kh') {
		meta[areaCode].items.map((item) => {
			if (item.attrcode == dxmcField) {
				item.itemtype = 'refer';
				item.refName = that.state.json['verificationsheet-000038']; /* 国际化处理： 客户档案*/
				item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index';
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi', //使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value
							? props.form.getFormItemsValue(cytjCode, 'pk_org').value
							: null
					};
				};
			}
			return item;
		});
	} else if (hxdxValue == 'gys') {
		meta[areaCode].items.map((item) => {
			if (item.attrcode == dxmcField) {
				item.itemtype = 'refer';
				item.refName = that.state.json['verificationsheet-000039']; /* 国际化处理： 供应商档案*/
				item.refcode = 'uapbd/refer/supplier/SupplierRefTreeGridRef/index';
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi', //使用权组
						isDataPowerEnable: 'Y',
						pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value
							? props.form.getFormItemsValue(cytjCode, 'pk_org').value
							: null
					};
				};
			}
			return item;
		});
	} else if (hxdxValue == 'bm') {
		meta[areaCode].items.map((item) => {
			if (item.attrcode == dxmcField) {
				item.itemtype = 'refer';
				item.refName = that.state.json['verificationsheet-000002']; /* 国际化处理： 部门*/
				item.refcode = 'uapbd/refer/org/DeptTreeRef/index';
				(item.isShowUnit = true),
					(item.unitCondition = () => {
						return {
							pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value
								? props.form.getFormItemsValue(cytjCode, 'pk_org').value
								: null,
							TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						};
					});
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi', //使用权组
						isDataPowerEnable: 'Y',
						busifuncode: 'all',
						pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value
							? props.form.getFormItemsValue(cytjCode, 'pk_org').value
							: null
					};
				};
			}
			return item;
		});
	} else if (hxdxValue == 'ywy') {
		meta[areaCode].items.map((item) => {
			if (item.attrcode == dxmcField) {
				item.itemtype = 'refer';
				item.refName = that.state.json['verificationsheet-000042']; /* 国际化处理： 人员*/
				item.refcode = 'uapbd/refer/psninfo/PsndocTreeGridRef/index';
				(item.isShowUnit = true),
					(item.unitCondition = () => {
						return {
							pkOrgs: props.form.getFormItemsValue(cytjCode, 'pk_org').value
								? props.form.getFormItemsValue(cytjCode, 'pk_org').value
								: null,
							TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						};
					});
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi', //使用权组
						isDataPowerEnable: 'Y',
						busifuncode: 'all',
						pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value
							? props.form.getFormItemsValue(cytjCode, 'pk_org').value
							: null
					};
				};
			}
			return item;
		});
	}
	//重新渲染本方对象名称、对方对象名称
	props.renderItem('form', areaCode, dxmcField, null);
}
