
/**
 * 查询区域
 */
export const searchId = '20080vps_query';

/**
 * 常用条件
 */
export const cytjCode = 'cytjArea';
export const bfCode = 'bf';
export const dfCode = 'df';
export const publicCode = 'public';

/**
 * 核销规则
 */
export const hxgzCode = 'hxgzArea';

/**
 * 核销方式
 */
export const hxfsCode = 'hxfsArea';

/**
 * 表体区域
 */
export const tableId = '20080vps_list';


/**
 * 页面编码
 */
export const pageCode = '20080VPS_FA';


/**
 * 小应用ID
 */
export const appCode = '20080VPS';

/**
 * nc端功能节点号
 */
export const funCode = '20080VPS';
