import { headButton } from '../../../public/components/pubUtils/buttonName';

export default function buttonClick(props, id) {
	switch (id) {
		case headButton.VerifyLink: //核销关联按钮
			this.handleVerification();
			break;
		case headButton.Delete: //批量删除按钮
			this.handleBatchDelete();
			break;
		case headButton.Share: //分摊
			this.handleShare();
			break;
		default:
			break;
	}
}
