import { ajax } from 'nc-lightapp-front';
import tableButtonClick from './tableButtonClick.js';
let tableId = 'table';
let appcode;
let pagecode;
let transferData;
export default function(props, that) {
	pagecode = this.props.getSearchParam('p');
	appcode = this.props.getSearchParam('c');
	props.createUIDom(
		{
			pagecode: pagecode, //页面id
			appcode: appcode //注册按钮的id
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta, that);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent(
						'delete',
						that.state.json['verificationsheet-000013']
					); /* 国际化处理： 确认要删除该信息吗？*/
				}
			}
		}
	);
}

function modifierMeta(props, meta, that) {
	transferData = props.ViewModel.getData('WholeVerifyDatas');
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'billno') {
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							billLink(text, props);
						}}
					>
						{text && text.values && text.values.billno.value}
					</a>
				);
			};
		}
		return item;
	});
	let material_event = {
		label: that.state.json['verificationsheet-000014'] /* 国际化处理： 操作*/,
		itemtype: 'customer',
		attrcode: 'opr',
		width: '150px',
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let objTypeValue;
			let recordV = record.values;
			let objtype = recordV.objtype.value;
			if (objtype == 0) {
				objTypeValue = recordV.customer.value;
			} else if (objtype == 1) {
				objTypeValue = recordV.supplier.value;
			} else if (objtype == 2) {
				objTypeValue = recordV.pk_deptid.value;
			} else if (objtype == 3) {
				objTypeValue = recordV.pk_psndoc.value;
			}
			let linkVOs = [
				{
					pk_bill: recordV.pk_bill.value,
					pk_item: (recordV.pk_item && recordV.pk_item.value) || '',
					billType: recordV.pk_billtype.value,
					linkedPk: recordV.linkedPk.value,
					objTypeValue: objTypeValue
				}
			];
			let datas = {
				pk_bill: transferData.pk_bill.value, //单据主键
				pk_item: transferData.pk_item, //按表体核销时，设置行主键
				billType: transferData.billType.value, //单据类型
				ts: '', //表头ts
				linkVOs: linkVOs //选中数据
			};
			let buttonAry = [ 'Delete_inner' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index, datas, that)
			});
		}
	};
	meta[tableId].items.push(material_event);
	return meta;
}
//联查单据
function billLink(text, props) {
	ajax({
		url: '/nccloud/arap/arappub/linkarapbill.do',
		async: false,
		data: {
			tradeType: text.values.pk_tradetype.value,
			pk_bill: text.values.pk_bill.value
		},
		success: (res) => {
			props.openTo(res.data.url, res.data.condition);
		}
	});
}
