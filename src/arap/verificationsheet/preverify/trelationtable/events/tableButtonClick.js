import { ajax, toast } from 'nc-lightapp-front';
let tableId = 'table';
let tableButtonClick = (props, key, text, record, index, data, that) => {
	switch (key) {
		// 表格操作按钮
		case 'Delete_inner':
			data.ts = props.ts;
			ajax({
				url: '/nccloud/arap/billverify/preVerifyCancelLink.do',
				data: data,
				success: (res) => {
					if (res.success) {
						toast({
							color: 'success',
							content: that.state.json['verificationsheet-000015']
						}); /* 国际化处理： 删除成功*/
						props.editTable.deleteTableRowsByIndex(tableId, index);
						props.changeTs(res.data.ts);
					}
				}
			});
			break;
		default:
			break;
	}
};

export default tableButtonClick;
