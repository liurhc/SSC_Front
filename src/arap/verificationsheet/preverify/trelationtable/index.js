//主子表列表
import React, { Component } from 'react';
import { createPage, ajax, toast, getMultiLang } from 'nc-lightapp-front';
import { initTemplate } from './events';
import './index.less';
let tableId = 'table';
let pagecode = '';
let WholeVerifyData;

class TrelationTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {}
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		getMultiLang({ moduleId: 'verificationsheet', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {
		initTemplate.call(this, this.props, this);
		WholeVerifyData = this.props.ViewModel.getData('WholeVerifyDatas');
		pagecode = this.props.getSearchParam('p');
		this.props.onRef(this);
		if (WholeVerifyData != undefined) {
			this.getData();
		}
	}
	getData = () => {
		let data = {
			pageId: pagecode,
			pk_bill: WholeVerifyData.pk_bill.value,
			pk_item: WholeVerifyData.pk_item,
			billType: WholeVerifyData.billType.value,
			ts: WholeVerifyData.ts.value
		};
		ajax({
			url: '/nccloud/arap/billverify/preVerifyLinkedQuery.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let datas = data.grid;
					this.props.changeTs(data.ts);
					if (datas) {
						this.props.editTable.setTableData(tableId, datas[tableId]);
					} else {
						this.props.editTable.setTableData(tableId, { rows: [] });
					}
				}
			}
		});
	};

	//批量删除
	handleBatchDelete = () => {
		let checkedDatas = this.props.editTable.getCheckedRows(tableId);
		let indexArr = [];
		let linkVOs = [];
		for (var i = 0, len = checkedDatas.length; i < len; i++) {
			let objTypeValue;
			let DataValues = checkedDatas[i].data.values;
			let objtype = DataValues.objtype.value;
			if (objtype == 0) {
				objTypeValue = DataValues.customer.value;
			} else if (objtype == 1) {
				objTypeValue = DataValues.supplier.value;
			} else if (objtype == 2) {
				objTypeValue = DataValues.pk_deptid.value;
			} else if (objtype == 3) {
				objTypeValue = DataValues.pk_psndoc.value;
			}
			indexArr.push(checkedDatas[i].index);
			let o = {
				pk_bill: checkedDatas[i].data.values.pk_bill.value,
				pk_item: (checkedDatas[i].data.values.pk_item && checkedDatas[i].data.values.pk_item.value) || '',
				billType: checkedDatas[i].data.values.pk_billtype.value,
				linkedPk: checkedDatas[i].data.values.linkedPk.value,
				objTypeValue: objTypeValue
			};
			linkVOs.push(o);
		}
		let data = {
			pk_bill: WholeVerifyData.pk_bill.value, //单据主键
			pk_item: WholeVerifyData.pk_item, //按表体核销时，设置行主键
			billType: WholeVerifyData.billType.value, //单据类型
			ts: this.props.ts, //表头ts
			linkVOs: linkVOs //选中数据
		};
		ajax({
			url: '/nccloud/arap/billverify/preVerifyCancelLink.do',
			data: data,
			success: (res) => {
				if (res.success) {
					toast({ color: 'success', content: this.state.json['verificationsheet-000015'] }); /* 国际化处理： 删除成功*/
					this.props.editTable.deleteTableRowsByIndex(tableId, indexArr);
					this.props.changeTs(res.data.ts);
				}
			}
		});
	};

	render() {
		let { editTable } = this.props;
		let { createEditTable } = editTable;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-table-area">
					{createEditTable(tableId, {
						//列表区
						useFixedHeader: true,
						showCheck: true //显示复选框
					})}
				</div>
			</div>
		);
	}
}

TrelationTable = createPage({
	mutiLangCode: ''
})(TrelationTable);

export default TrelationTable;
