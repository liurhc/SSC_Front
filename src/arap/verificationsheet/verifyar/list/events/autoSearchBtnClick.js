import { ajax, base, promptBox, toast } from 'nc-lightapp-front';
import { funCode, autoVerifySearchId, pagecode, cytjCode_auto, hxgzCode_auto, hxfsCode } from '../constants';
const { NCMessage } = base;


//点击查询，获取查询区数据
export default function autoSearchBtnClick(props, searchVal) {
    let that = this;
    //前端需要传递（queryVO	ofternConditionsVO	VerifyRulesVO  bfCode	dfCode	funCode	 pageId	 arapFlag  pk_org）
    if (!validate(props, that)) {
        return true;
    };

    let queryInfo = {
        querycondition: searchVal,
        queryAreaCode: autoVerifySearchId,  //查询区编码
        querytype: 'tree'
    };
    let querydata = {
        queryVO: queryInfo,
        ofternConditionsVO: {
            model: props.form.getAllFormValue(cytjCode_auto),
            pageid: pagecode
        },
        verifyRulesVO: {
            model: props.form.getAllFormValue(hxgzCode_auto),
            pageid: pagecode
        },
        verifyModeVO: {
            model: props.form.getAllFormValue(hxfsCode),
            pageid: pagecode
        },
        pageId: pagecode,
        funCode: funCode,
        arapFlag: 'ap',
        pk_org: props.form.getFormItemsValue(cytjCode_auto, 'pk_org').value
    };
    let size = getSelectedMode(props)
    if (size > 0) {
        ajax({
            url: '/nccloud/arap/verify/autoVerify.do',
            data: querydata,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if (data) {
                        props.openTo('/arap/verificationsheet/verifyqueryar/list/index.html', {
                            pks: data,
                            pagecode: '20060VQ_LIST',
                            appcode: '20060VQ'
                        });
                    } else {
                        toast({ content: that.state.json['verificationsheet-000046'], color: 'warning' });/* 国际化处理： 未查询到符合条件的数据*/
                    }
                }
            }
        });
    } else {
        setTimeout(() => {
            promptBox({
                color: 'warning',
                title: that.state.json['verificationsheet-000006'],/* 国际化处理： 请注意*/
                content: that.state.json['verificationsheet-000047'],/* 国际化处理： 核销方式未设置,是否继续？*/
                beSureBtnName: that.state.json['verificationsheet-000048'],/* 国际化处理： 继续*/
                cancelBtnName: that.state.json['verificationsheet-000049'],/* 国际化处理： 取消*/
                beSureBtnClick: () => {
                    ajax({
                        url: '/nccloud/arap/verify/autoVerify.do',
                        data: querydata,
                        success: (res) => {
                            let { success, data } = res;
                            if (success) {
                                if (data) {
                                    props.openTo('/arap/verificationsheet/verifyqueryar/list/index.html', {
                                        pks: data,
                                        pagecode: '20060VQ_LIST',
                                        appcode: '20060VQ'
                                    });
                                } else {
                                    toast({ content: that.state.json['verificationsheet-000046'], color: 'warning' });/* 国际化处理： 未查询到符合条件的数据*/
                                }
                            }
                        }
                    })
                },
                cancelBtnClick: () => {
                },
            })
        }
            , 0)
    }
}

//核销校验
function validate(props, that) {
    if (!props.form.isCheckNow(cytjCode_auto, 'warning')) {
        return false;
    }
    let hldcChkBox = props.form.getFormItemsValue(hxgzCode_auto, 'hldcChkBox').value;// 红蓝对冲
    let tbzhxChkBox = props.form.getFormItemsValue(hxgzCode_auto, 'tbzhxChkBox').value;// 同币种核销
    let hxbzRef = props.form.getFormItemsValue(hxgzCode_auto, 'hxbzRef').value;// 核销币种

    if (!hldcChkBox && !tbzhxChkBox) {
        promptBox({ color: "warning", content: that.state.json['verificationsheet-000130'], noCancelBtn: true })/* 国际化处理： 红蓝对冲,同币种核销 至少选择一种*/
        return false;
    }

    if (hxbzRef == "" || hxbzRef == null || hxbzRef == undefined) {
        if (hldcChkBox) {
            promptBox({ color: "warning", noCancelBtn: true, content: that.state.json['verificationsheet-000051'], noCancelBtn: true })/* 国际化处理： 红蓝对冲:核销币种必须设置*/
            return false;
        } else if (tbzhxChkBox) {
            promptBox({ color: "warning", noCancelBtn: true, content: that.state.json['verificationsheet-000052'], noCancelBtn: true })/* 国际化处理： 同币种核销:核销币种必须设置*/
            return false;
        }
    }
    return true;
}

function getSelectedMode(props) {
    let items = props.meta.getMeta()[hxfsCode].items
    let values = props.form.getAllFormValue(hxfsCode).rows[0].values
    let size = 0
    for (let i = 0; i < items.length; i++) {
        let item = items[i];
        let value = values[item.attrcode]
        if (value && value.value) {
            size++
        }
    }
    return size
}
