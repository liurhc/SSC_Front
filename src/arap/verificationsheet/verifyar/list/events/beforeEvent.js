import { ajax, getBusinessInfo } from 'nc-lightapp-front';
import {
    appcode, pagecode, verifySearchId, autoVerifySearchId, tableId_bf, tableId_df,
    cytjCode, bfCode, dfCode, publicCode, hxgzCode,
    cytjCode_auto, bfCode_auto, dfCode_auto, publicCode_auto, hxgzCode_auto, hxfsCode
} from '../constants';

export default function beforeEvent(props, moduleId, key, value, data) {
	// props, moduleId(区域id), key(操作的键), value（当前值）,data(当前表单所有值)
	let cytjCode = this.cytjCode;
	let bfCode = this.bfCode;
	let dfCode = this.dfCode;
	let publicCode = this.publicCode;
	let hxgzCode = this.hxgzCode;
	if(moduleId == cytjCode_auto){
		cytjCode = cytjCode_auto
		bfCode = bfCode_auto
		dfCode = dfCode_auto
		publicCode = publicCode_auto
	}
	if(moduleId == hxgzCode_auto){
		hxgzCode = hxgzCode_auto
	}

	let meta = props.meta.getMeta()
	//本方交易类型
	if (key == 'jfTradeRef') {
		let jfBillTypeBox = props.form.getFormItemsValue(cytjCode, 'jfBillTypeBox').value;
		if (jfBillTypeBox == 'gysfk') {
			meta[bfCode].items.map((item) => {
				if (item.attrcode == 'jfTradeRef') {
					item.isMultiSelectedEnabled = true;
					item.refName = this.state.json['verificationsheet-000023']/* 国际化处理： 交易类型参照*/
					item.queryCondition = () => {
						return {
							parentbilltype: 'F3' //过滤条件
						};
					}
				}
				return item;
			})
		} else if (jfBillTypeBox == 'khys') {
			meta[bfCode].items.map((item) => {
				if (item.attrcode == 'jfTradeRef') {
					item.isMultiSelectedEnabled = true;
					item.refName = this.state.json['verificationsheet-000023']/* 国际化处理： 交易类型参照*/
					item.queryCondition = () => {
						return {
							parentbilltype: 'F0' //过滤条件
						};
					}
				}
				return item;
			})
		}
	}

	//对方交易类型
	if (key == 'dfTradeRef') {
		let dfBillTypeBox = props.form.getFormItemsValue(cytjCode, 'dfBillTypeBox').value;
		if (dfBillTypeBox == 'khsk') {
			meta[dfCode].items.map((item) => {
				if (item.attrcode == 'dfTradeRef') {
					item.refName = this.state.json['verificationsheet-000023']/* 国际化处理： 交易类型参照*/
					item.isMultiSelectedEnabled = true;
					item.queryCondition = () => {
						return {
							parentbilltype: 'F2' //过滤条件
						};
					}
				}
				return item;
			})
		} else if (dfBillTypeBox == 'gysyf') {
			meta[dfCode].items.map((item) => {
				if (item.attrcode == 'dfTradeRef') {
					item.isMultiSelectedEnabled = true;
					item.refName = this.state.json['verificationsheet-000023']/* 国际化处理： 交易类型参照*/
					item.queryCondition = () => {
						return {
							parentbilltype: 'F1' //过滤条件
						};
					}
				}
				return item;
			})
		}
	}

	//表头科目,表体科目
	if (key == 'headSubjRef' || key == 'itemSubjRef') {
		ajax({
			url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
			data: {
				pk_org: props.form.getFormItemsValue(moduleId, 'pk_org') ? props.form.getFormItemsValue(moduleId, 'pk_org').value : null,
			},
			async: false,
			success: (res) => {
				if (res.success) {
					meta[publicCode].items.map((item) => {
						if (item.attrcode == 'headSubjRef' || item.attrcode == 'itemSubjRef') {
							item.queryCondition = () => {
								return {
									DataPowerOperationCode: 'fi',//使用权组
									isDataPowerEnable: 'Y',
									pk_accountingbook: res.data,
									datestr: getBusinessInfo().businessDate
								}
							}
						}
						return item;
					})
				}
			}
		});
	}

	props.meta.setMeta(meta);

	return true;
}
