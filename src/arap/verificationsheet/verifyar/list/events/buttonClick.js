import { ajax, base, promptBox, toast, cardCache, getBusinessInfo } from 'nc-lightapp-front';
import { setFormEditable } from '../../../../public/components/pubUtils/billPubUtil.js';
import { getCheckedRowColvalues } from '../../../../public/components/pubUtils/billPubUtil';
import {
	appcode,
	pagecode,
	verifySearchId,
	autoVerifySearchId,
	tableId_bf,
	tableId_df,
	cytjCode,
	bfCode,
	dfCode,
	publicCode,
	hxgzCode,
	cytjCode_auto,
	bfCode_auto,
	dfCode_auto,
	publicCode_auto,
	hxgzCode_auto,
	hxfsCode
} from '../constants';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';
import afterEvent from './afterEvent';
import { floatObj } from '../../../../public/components/pubUtils/floatObj.js';

export function buttonClick(props, id) {
	let meta = props.meta.getMeta();
	let pk_org = getContext(loginContextKeys.pk_org);
	let org_Name = getContext(loginContextKeys.org_Name);
	switch (id) {
		case 'Query':
			props.search.openAdvSearch(verifySearchId, true, () => {
				let isFirstQuery = this.isFirstQuery;
				if (!isFirstQuery) {
					return;
				}
				props.search.clearSearchArea(verifySearchId);
				props.form.setFormItemsDisabled(hxgzCode, {
					zjbzRef: true,
					yxwcTxtF: true,
					jfbzRef: true,
					jfbz_dzjbzhlTxtF: true,
					jfbz_dbbhlTxtF: true,
					dfbzRef: true,
					dfbz_dbbhlTxtF: true,
					dfbz_dzjbzhlTxtF: true,
					hxbzRef: false
				});
				//设置默认值
				setDefaultValue(this, props, cytjCode, bfCode, dfCode, publicCode, hxgzCode, false);
				this.isFirstQuery = false;
				if (pk_org && org_Name) {
					props.form.setFormItemsValue(cytjCode, { pk_org: { value: pk_org, display: org_Name } });
					afterEvent.call(this, props, cytjCode, 'pk_org', { value: pk_org, display: org_Name });
					return;
				}

				//常用条件，组织为空，只有组织可编辑
				setFormEditable(props, cytjCode, cytjCode, false);
				setFormEditable(props, cytjCode, bfCode, false);
				setFormEditable(props, cytjCode, dfCode, false);
				setFormEditable(props, cytjCode, publicCode, false);
				props.form.setFormItemsDisabled(cytjCode, { pk_org: false });
			});

			break;
		case 'Verify':
			// 前端需要传递（ jf_vos df_vos rule pageId arapFlag, pk_org）
			if (!verifyValidate(this, props)) {
				return;
			}
			let data = {
				jfValues: getCheckedRowColvalues(this.props, this.tableId_bf, [
					'customer', 'pk_deptid', 'pk_psndoc', 'supplier',
					'settleMoney', 'mid_sett', 'disction', 'linkedPk', 'pk_bill', 'pk_item', 'ts'
				]),
				dfValues: getCheckedRowColvalues(this.props, this.tableId_df, [
					'customer', 'pk_deptid', 'pk_psndoc', 'supplier',
					'settleMoney', 'mid_sett', 'disction', 'linkedPk', 'pk_bill', 'pk_item', 'ts'
				]),
				jfCode: this.tableId_bf,
				dfCode: this.tableId_df,
				rule: this.rule,
				pageId: pagecode,
				arapFlag: 'ar',
				pk_org: props.form.getFormItemsValue(cytjCode, 'pk_org').value,
				jf_hxdx: getObjtype(props.form.getFormItemsValue(cytjCode, 'jf_hxdxCBox').value),
				df_hxdx: getObjtype(props.form.getFormItemsValue(cytjCode, 'df_hxdxCBox').value)
			};
			ajax({
				url: '/nccloud/arap/verify/verify.do',
				data: data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data) {
							let index_bf = getSelectedIndex(props, tableId_bf);
							let index_df = getSelectedIndex(props, tableId_df);
							props.cardTable.delRowsByIndex(tableId_bf, index_bf, true);
							props.cardTable.delRowsByIndex(tableId_df, index_df, true);
							this.setBtnVisible(props, false);
							props.openTo('/arap/verificationsheet/verifyqueryar/list/index.html', {
								pks: data,
								pagecode: '20060VQ_LIST',
								appcode: '20060VQ'
							});
						}
					}
				},
				error: function(data) {
					promptBox({ color: 'danger', noCancelBtn: true, content: data.message });
				}
			});
			break;
		case 'AutoVerify': //自动核销
			props.search.openAdvSearch(autoVerifySearchId, true, () => {
				let isFirstAutoVerify = this.isFirstAutoVerify;
				if (!isFirstAutoVerify) {
					return;
				}
				props.search.clearSearchArea(autoVerifySearchId);
				//设置默认值
				setDefaultValue(
					this,
					props,
					cytjCode_auto,
					bfCode_auto,
					dfCode_auto,
					publicCode_auto,
					hxgzCode_auto,
					true
				);
				this.isFirstAutoVerify = false;
				if (pk_org && org_Name) {
					props.form.setFormItemsValue(cytjCode_auto, { pk_org: { value: pk_org, display: org_Name } });
					afterEvent.call(this, props, cytjCode_auto, 'pk_org', { value: pk_org, display: org_Name });
					return;
				}
				//常用条件，组织为空，只有组织可编辑
				setFormEditable(props, cytjCode_auto, cytjCode_auto, false);
				setFormEditable(props, cytjCode_auto, bfCode_auto, false);
				setFormEditable(props, cytjCode_auto, dfCode_auto, false);
				setFormEditable(props, cytjCode_auto, publicCode_auto, false);
				props.form.setFormItemsDisabled(cytjCode_auto, { pk_org: false });
			});

			break;
		case 'VerifyQuery': //核销查询
			props.openTo('/arap/verificationsheet/verifyqueryar/list/index.html', {
				pagecode: '20060VQ_LIST',
				appcode: '20060VQ'
			});
			break;
		case 'Jfmath': //按借方匹配
			let jfData = props.cardTable.getCheckedRows(tableId_bf);
			//1,如果选中数据为空，清空贷方选中数据
			if (!jfData || jfData.length == 0) {
				props.cardTable.setColValue(tableId_df, 'settleMoney', { value: null, display: null });
				props.cardTable.setColValue(tableId_df, 'disction', { value: null, display: null });
				props.cardTable.setColValue(tableId_df, 'mid_sett', { value: null, display: null });
				props.cardTable.selectAllRows(tableId_df, false);
				this.setBtnVisible(props, false);
				break;
			}
			//2,异币种核销，获得选中行中币结算字段sum，依次选中贷方行sum=sum-settleMoney，直到sum=0，如果sum>0,贷方已全部选中，提示：借贷方结算金额不相等
			if (this.isUnsameVerify) {
				let jfSum = getSelectedSumField(props, tableId_bf, 'mid_sett');
				selectTableByMidsettMoney(this, props, tableId_df, jfSum);
			} else {
				//2,获得选中行本次结算字段sum，依次选中贷方行sum=sum-settleMoney，直到sum=0，如果sum>0,贷方已全部选中，提示：借贷方结算金额不相等
				let jfSum = getSelectedSumField(props, tableId_bf, 'settleMoney');
				selectTableBySettMoney(this, props, tableId_df, jfSum);
			}

			break;
		case 'Dfmath': //按贷方匹配
			let dfData = props.cardTable.getCheckedRows(tableId_df);
			//1,如果选中数据为空，清空贷方选中数据
			if (!dfData || dfData.length == 0) {
				props.cardTable.setColValue(tableId_bf, 'settleMoney', { value: null, display: null });
				props.cardTable.setColValue(tableId_bf, 'disction', { value: null, display: null });
				props.cardTable.setColValue(tableId_bf, 'mid_sett', { value: null, display: null });
				props.cardTable.selectAllRows(tableId_bf, false);
				this.setBtnVisible(props, false);
				break;
			}
			//2,异币种核销，获得选中行中币结算字段sum，依次选中贷方行sum=sum-settleMoney，直到sum=0，如果sum>0,贷方已全部选中，提示：借贷方结算金额不相等
			if (this.isUnsameVerify) {
				let dfSum = getSelectedSumField(props, tableId_df, 'mid_sett');
				selectTableByMidsettMoney(this, props, tableId_bf, dfSum);
			} else {
				//2,获得选中行本次结算字段sum，依次选中贷方行sum=sum-settleMoney，直到sum=0，如果sum>0,贷方已全部选中，提示：借贷方结算金额不相等
				let dfSum = getSelectedSumField(props, tableId_df, 'settleMoney');
				selectTableBySettMoney(this, props, tableId_bf, dfSum);
			}
			break;

		default:
			break;
	}
}

//核销校验
function verifyValidate(that, props) {
	let bfdata = props.cardTable.getCheckedRows(tableId_bf);
	let dfdata = props.cardTable.getCheckedRows(tableId_df);
	if ((!bfdata || bfdata == null || bfdata.length == 0) && (!dfdata || dfdata == null || dfdata.length == 0)) {
		toast({ content: that.state.json['verificationsheet-000053'], color: 'warning' }); /* 国际化处理： 请先选择对应的单据,再进行核销*/
		return false;
	}

	let jfSum = null;
	let dfSum = null;
	//异币种核销
	if (that.isUnsameVerify) {
		jfSum = getSelectedSumField(props, tableId_bf, 'mid_sett');
		dfSum = getSelectedSumField(props, tableId_df, 'mid_sett');
		let m_maxError = that.rule[2].m_maxError == null ? 0 : that.rule[2].m_maxError; //允许最大误差
		if (Math.abs(jfSum * 1 - dfSum * 1) > m_maxError * 1) {
			promptBox({
				color: 'warning',
				noCancelBtn: true,
				content:
					that.state.json['verificationsheet-000054'] /* 国际化处理： 非严格匹配（手工核销）异币种核销时，借贷方结算中间币种金额总和不在允许误差范围之内。*/ +
					that.state.json['verificationsheet-000055'] +
					jfSum /* 国际化处理：   借方结算中间币种金额总和：*/ +
					that.state.json['verificationsheet-000056'] +
					dfSum /* 国际化处理：   贷方结算中间币种金额总和：*/ +
					that.state.json['verificationsheet-000057'] +
					m_maxError /* 国际化处理：   允许最大误差范围：*/
			});
			return false;
		}
	} else {
		jfSum = getSelectedSumField(props, tableId_bf, 'settleMoney');
		dfSum = getSelectedSumField(props, tableId_df, 'settleMoney');
		if (jfSum != dfSum) {
			promptBox({
				color: 'warning',
				noCancelBtn: true,
				content:
					that.state.json['verificationsheet-000058'] /* 国际化处理： 非严格匹配（手工核销）同币种核销时，借贷方结算原币金额总和不相等。*/ +
					that.state.json['verificationsheet-000059'] +
					jfSum /* 国际化处理： 借方结算原币金额总和：*/ +
					that.state.json['verificationsheet-000060'] +
					dfSum /* 国际化处理：   贷方结算原币金额总和：*/
			});
			return false;
		}
	}
	return true;
}

//获得选中行数据数组
function getSelectedData(props, areaCode) {
	let selectedData = props.cardTable.getCheckedRows(areaCode);
	if (selectedData.length == 0 || !selectedData) {
		return null;
	}
	let dataArr = [];
	selectedData.forEach((val) => {
		if (val.data.selected) {
			dataArr.push(val.data);
		}
	});
	return dataArr;
}

function getSelectedIndex(props, areaCode) {
	let indexArr = [];
	let selData = props.cardTable.getCheckedRows(areaCode);
	if (selData.length == 0 || !selData) {
		return null;
	}
	selData.forEach((val) => {
		if (val.data.selected) {
			indexArr.push(val.index);
		}
	});
	return indexArr;
}

//获得选中行数据本次结算金额sum
function getSelectedSumField(props, areaCode, field) {
	let sum = 0;
	let selectedData = props.cardTable.getCheckedRows(areaCode);
	if (selectedData.length != 0 && selectedData) {
		selectedData.forEach((val) => {
			if (val.data.selected) {
				let fieldValue = val.data.values[field].value;
				fieldValue = Number(fieldValue);
				if (!isNaN(fieldValue)) {
					sum = floatObj.add(sum, fieldValue);
				}
			}
		});
	}
	return sum;
}

//根据选中行结算金额sum选中另一区域行
function selectTableBySettMoney(that, props, areaCode, sum) {
	props.cardTable.selectAllRows(areaCode, false);
	props.cardTable.setStatus(areaCode, 'edit');
	let rownum = props.cardTable.getNumberOfRows(areaCode);
	if (rownum == 0) {
		return;
	}
	let scale = props.cardTable.getValByKeyAndIndex(areaCode, 0, 'money_bal').scale;
	props.cardTable.setColValue(areaCode, 'settleMoney', { value: null, display: null });
	props.cardTable.setColValue(areaCode, 'disction', { value: null, display: null });
	if (sum > 0) {
		let needSelIndex = [];
		for (let i = 0; i < rownum; i++) {
			let money_bal = props.cardTable.getValByKeyAndIndex(areaCode, i, 'money_bal').value;
			money_bal = Number(money_bal);
			if (sum == 0 || money_bal < 0) {
				continue;
			}
			if (!isNaN(money_bal)) {
				sum = floatObj.subtract(sum, money_bal);
			}
			needSelIndex.push(i);
			let settleMoney = 0;
			if (sum < 0) {
				settleMoney = floatObj.add(sum, money_bal).toFixed(scale);
				props.cardTable.setValByKeyAndIndex(areaCode, i, 'settleMoney', {
					value: settleMoney,
					display: settleMoney
				});
				props.cardTable.setValByKeyAndIndex(areaCode, i, 'disction', { value: null, display: null });
				break;
			}
			settleMoney = money_bal.toFixed(scale);
			props.cardTable.setValByKeyAndIndex(areaCode, i, 'settleMoney', {
				value: settleMoney,
				display: settleMoney
			});
			props.cardTable.setValByKeyAndIndex(areaCode, i, 'disction', { value: null, display: null });
		}
		if (sum > 0) {
			props.cardTable.setColValue(areaCode, 'settleMoney', { value: null, display: null });
			props.cardTable.setColValue(areaCode, 'disction', { value: null, display: null });
			props.cardTable.selectAllRows(areaCode, false);
			promptBox({
				color: 'warning',
				noCancelBtn: true,
				content: that.state.json['verificationsheet-000061']
			}); /* 国际化处理： 借贷方结算金额不相等！*/
			return;
		}
		props.cardTable.selectRowsByIndex(areaCode, needSelIndex);
	} else if (sum < 0) {
		let needSelIndex = [];
		for (let i = 0; i < rownum; i++) {
			let money_bal = props.cardTable.getValByKeyAndIndex(areaCode, i, 'money_bal').value;
			money_bal = Number(money_bal);
			if (sum == 0 || money_bal > 0) {
				continue;
			}
			if (!isNaN(money_bal)) {
				sum = floatObj.subtract(sum, money_bal);
			}
			needSelIndex.push(i);
			let settleMoney = 0;
			if (sum > 0) {
				settleMoney = floatObj.add(sum, money_bal).toFixed(scale);
				props.cardTable.setValByKeyAndIndex(areaCode, i, 'settleMoney', {
					value: settleMoney,
					display: settleMoney
				});
				props.cardTable.setValByKeyAndIndex(areaCode, i, 'disction', { value: null, display: null });
				break;
			}
			settleMoney = money_bal.toFixed(scale);
			props.cardTable.setValByKeyAndIndex(areaCode, i, 'settleMoney', {
				value: settleMoney,
				display: settleMoney
			});
			props.cardTable.setValByKeyAndIndex(areaCode, i, 'disction', { value: null, display: null });
		}
		if (sum < 0) {
			props.cardTable.setColValue(areaCode, 'settleMoney', { value: null, display: null });
			props.cardTable.setColValue(areaCode, 'disction', { value: null, display: null });
			props.cardTable.selectAllRows(areaCode, false);
			promptBox({
				color: 'warning',
				noCancelBtn: true,
				content: that.state.json['verificationsheet-000061']
			}); /* 国际化处理： 借贷方结算金额不相等！*/
			return;
		}
		props.cardTable.selectRowsByIndex(areaCode, needSelIndex);
	}
}

//根据选中行中币结算金额sum选中另一区域行
function selectTableByMidsettMoney(that, props, areaCode, sum) {
	props.cardTable.selectAllRows(areaCode, false);
	props.cardTable.setStatus(areaCode, 'edit');
	let rownum = props.cardTable.getNumberOfRows(areaCode);
	if (rownum == 0) {
		return;
	}
	let mid_scale = that.rule[2].m_verifyCurrPrecision; //中间币种小数精度;
	let scale = props.cardTable.getValByKeyAndIndex(areaCode, 0, 'money_bal').scale;
	props.cardTable.setColValue(areaCode, 'mid_sett', { value: null, display: null });
	props.cardTable.setColValue(areaCode, 'settleMoney', { value: null, display: null });

	if (sum > 0) {
		let needSelIndex = [];
		let m_bz2zjbzHL = null;
		if (areaCode == tableId_df) {
			m_bz2zjbzHL = that.rule[2].m_dfbz2zjbzHL; //贷方币种对中间币种汇率
		} else {
			m_bz2zjbzHL = that.rule[2].m_jfbz2zjbzHL; //借方币种对中间币种汇率
		}
		for (let i = 0; i < rownum; i++) {
			let money_bal = props.cardTable.getValByKeyAndIndex(areaCode, i, 'money_bal').value;
			money_bal = Number(money_bal);
			if (sum == 0 || money_bal < 0) {
				// break;
			}
			let sum1 = sum;
			if (!isNaN(money_bal)) {
				sum = floatObj.subtract(sum, floatObj.multiply(money_bal * 1, m_bz2zjbzHL)).toFixed(mid_scale);
			}
			needSelIndex.push(i);
			let mid_sett = 0;
			if (sum <= 0) {
				// mid_sett = floatObj.add(sum, floatObj.multiply(money_bal * 1, m_bz2zjbzHL).toFixed(mid_scale) * 1).toFixed(mid_scale);
				mid_sett = sum1;
				props.cardTable.setValByKeyAndIndex(areaCode, i, 'mid_sett', { value: mid_sett, display: mid_sett });
				let settleMoney = floatObj.divide(mid_sett * 1, m_bz2zjbzHL).toFixed(scale);
				props.cardTable.setValByKeyAndIndex(areaCode, i, 'settleMoney', {
					value: settleMoney,
					display: settleMoney
				});
				break;
			}
			mid_sett = floatObj.multiply(money_bal * 1, m_bz2zjbzHL).toFixed(mid_scale);
			props.cardTable.setValByKeyAndIndex(areaCode, i, 'mid_sett', { value: mid_sett, display: mid_sett });
			money_bal = money_bal.toFixed(scale);
			props.cardTable.setValByKeyAndIndex(areaCode, i, 'settleMoney', { value: money_bal, display: money_bal });
		}
		let m_maxError = that.rule[2].m_maxError == null ? 0 : that.rule[2].m_maxError; //允许最大误差
		if (sum > 0 && sum > m_maxError * 1) {
			props.cardTable.setColValue(areaCode, 'mid_sett', { value: null, display: null });
			props.cardTable.setColValue(areaCode, 'settleMoney', { value: null, display: null });
			props.cardTable.selectAllRows(areaCode, false);
			promptBox({
				color: 'warning',
				content: that.state.json['verificationsheet-000054']
			}); /* 国际化处理： 非严格匹配（手工核销）异币种核销时，借贷方结算中间币种金额总和不在允许误差范围之内。*/
			return;
		}
		props.cardTable.selectRowsByIndex(areaCode, needSelIndex);
	} else if (sum < 0) {
		let needSelIndex = [];
		let m_bz2zjbzHL = null;
		if (areaCode == tableId_df) {
			m_bz2zjbzHL = that.rule[2].m_dfbz2zjbzHL; //贷方币种对中间币种汇率
		} else {
			m_bz2zjbzHL = that.rule[2].m_jfbz2zjbzHL; //借方币种对中间币种汇率
		}
		for (let i = 0; i < rownum; i++) {
			let money_bal = props.cardTable.getValByKeyAndIndex(areaCode, i, 'money_bal').value;
			money_bal = Number(money_bal);
			if (sum == 0 || money_bal > 0) {
				// continue
			}
			if (!isNaN(money_bal)) {
				sum = floatObj.subtract(sum, floatObj.multiply(money_bal * 1, m_bz2zjbzHL)).toFixed(mid_scale);
			}
			needSelIndex.push(i);
			let mid_sett = 0;
			if (sum > 0) {
				mid_sett = floatObj.add(sum, floatObj.multiply(money_bal * 1, m_bz2zjbzHL).toFixed(mid_scale));
				props.cardTable.setValByKeyAndIndex(areaCode, i, 'mid_sett', { value: mid_sett, display: mid_sett });
				props.cardTable.setValByKeyAndIndex(areaCode, i, 'settleMoney', {
					value: mid_sett * 1 / m_bz2zjbzHL,
					display: mid_sett * 1 / m_bz2zjbzHL
				});
				break;
			}
			mid_sett = floatObj.multiply(money_bal * 1, m_bz2zjbzHL).toFixed(mid_scale);
			props.cardTable.setValByKeyAndIndex(areaCode, i, 'mid_sett', { value: mid_sett, display: mid_sett });
			money_bal = money_bal.toFixed(scale);
			props.cardTable.setValByKeyAndIndex(areaCode, i, 'settleMoney', { value: money_bal, display: money_bal });
		}
		let m_maxError = that.rule[2].m_maxError == null ? 0 : that.rule[2].m_maxError; //允许最大误差
		if (sum < 0 && Math.abs(sum) > m_maxError) {
			props.cardTable.setColValue(areaCode, 'mid_sett', { value: null, display: null });
			props.cardTable.setColValue(areaCode, 'settleMoney', { value: null, display: null });
			props.cardTable.selectAllRows(areaCode, false);
			promptBox({
				color: 'warning',
				content: that.state.json['verificationsheet-000054']
			}); /* 国际化处理： 非严格匹配（手工核销）异币种核销时，借贷方结算中间币种金额总和不在允许误差范围之内。*/
			return;
		}
		props.cardTable.selectRowsByIndex(areaCode, needSelIndex);
	}
}

export function setDefaultValue(that, props, cytjCode, bfCode, dfCode, publicCode, hxgzCode, isauto) {
	if (isauto) {
		cytjCode = cytjCode_auto;
		bfCode = bfCode_auto;
		dfCode = dfCode_auto;
		publicCode = publicCode_auto;
		hxgzCode = hxgzCode_auto;
	}
	props.form.EmptyAllFormValue(cytjCode);
	props.form.EmptyAllFormValue(hxgzCode);
	if(isauto){
		props.form.EmptyAllFormValue(hxfsCode);
	}

	//设置默认值
	// 本方核销对象		对方核销对象
	props.form.setFormItemsValue(cytjCode, {
		jf_hxdxCBox: { value: 'kh', display: that.state.json['verificationsheet-000000'] },
		df_hxdxCBox: { value: 'kh', display: that.state.json['verificationsheet-000000'] }
	}); /* 国际化处理： 客户,客户*/
	//	本方单据类型	对方单据类型
	props.form.setFormItemsValue(cytjCode, {
		jfBillTypeBox: { value: 'khys', display: that.state.json['verificationsheet-000040'] },
		dfBillTypeBox: { value: 'khsk', display: that.state.json['verificationsheet-000044'] }
	}); /* 国际化处理： 应收单,收款单*/
	// 红蓝对冲	 同币种核销
	props.form.setFormItemsValue(hxgzCode, {
		hldcChkBox: { value: true, display: that.state.json['verificationsheet-000062'] },
		tbzhxChkBox: { value: true, display: that.state.json['verificationsheet-000062'] }
	}); /* 国际化处理： 是,是*/
	let date = getBusinessInfo().businessDate.substring(0, 10);
	let beginDate = date
	let endDate = date
	// 本方开始日期	本方结束日期
	props.form.setFormItemsValue(cytjCode, { jfBeginDateRef: { value: beginDate }, jfEndDateRef: { value: endDate } });
	// 对方开始日期	对方结束日期
	props.form.setFormItemsValue(cytjCode, { dfBeginDateRef: { value: beginDate }, dfEndDateRef: { value: endDate } });

	let meta = props.meta.getMeta();
	//设置本方对象名称、对方对象名称参照
	meta[bfCode].items.map((item) => {
		if (item.attrcode == 'jf_dxmcRef') {
			item.itemtype = 'refer';
			item.refName = that.state.json['verificationsheet-000038']; /* 国际化处理： 客户档案*/
			item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index';
		}
		return item;
	});
	meta[dfCode].items.map((item) => {
		if (item.attrcode == 'df_dxmcRef') {
			item.itemtype = 'refer';
			item.refName = that.state.json['verificationsheet-000038']; /* 国际化处理： 客户档案*/
			item.refcode = 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index';
		}
		return item;
	});
	props.renderItem('form', bfCode, 'jf_dxmcRef', null);
	props.renderItem('form', dfCode, 'df_dxmcRef', null);

	//设置金额方向不可编辑
	props.form.setFormItemsDisabled(hxgzCode, { jefxCmbBox: true });
}

function getObjtype(str){
	if(!str){
		return	
	}
	let objtype =0;
	if(str == 'kh'){
		objtype = 0
	}else if (str == 'gys'){
		objtype = 1
	}else if (str == 'bm'){
		objtype = 2
	}else if (str == 'ywy'){
		objtype = 3
	}
	return objtype
}
