import { ajax, pageTo } from 'nc-lightapp-front';
import {
	appcode,
	pagecode,
	verifySearchId,
	autoVerifySearchId,
	tableId_bf,
	tableId_df,
	cytjCode,
	bfCode,
	dfCode,
	publicCode,
	hxgzCode,
	cytjCode_auto,
	bfCode_auto,
	dfCode_auto,
	publicCode_auto,
	hxgzCode_auto,
	hxfsCode
} from '../constants';
import { modifierFormMeta } from '../../../public/filter/verifyReferFilter';
import { modifierSearchMeta } from '../../../public/filter/arapVerificatDefRefFilter';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';

export default function(props) {
	const that = this;
	props.createUIDom(
		{
			pagecode: pagecode, //页面code
			appcode: appcode //小应用code
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(that, props, meta);
					props.meta.setMeta(meta);
					modifierSearchMeta(verifySearchId, cytjCode, props, meta);
					modifierSearchMeta(autoVerifySearchId, cytjCode_auto, props, meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
			}
		}
	);
}

function modifierMeta(that, props, meta) {
	meta[verifySearchId].status = 'add';
	meta[autoVerifySearchId].status = 'add';
	meta[cytjCode].status = 'add';
	meta[hxgzCode].status = 'add';
	meta[cytjCode_auto].status = 'add';
	meta[hxgzCode_auto].status = 'add';
	meta[hxfsCode].status = 'add';
	meta[tableId_bf].status = 'browse';
	meta[tableId_df].status = 'browse';

	meta[tableId_bf].items = meta[tableId_bf].items.map((item, key) => {
		if (item.attrcode == 'billno') {
			item.renderStatus = 'browse';
			item.render = (a, text, value) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							billLink(that, text, props);
						}}
					>
						{text.values.billno.value}
					</a>
				);
			};
		}
		return item;
	});

	meta[tableId_df].items = meta[tableId_df].items.map((item, key) => {
		if (item.attrcode == 'billno') {
			item.renderStatus = 'browse';
			item.render = (a, text, value) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							billLink(that, text, props);
						}}
					>
						{text.values.billno.value}
					</a>
				);
			};
		}
		return item;
	});

	addformrelation(meta);

	//添加参照过滤
	modifierFormMeta(cytjCode, cytjCode, props, meta);
	modifierFormMeta(cytjCode, bfCode, props, meta);
	modifierFormMeta(cytjCode, dfCode, props, meta);
	modifierFormMeta(cytjCode, publicCode, props, meta);
	modifierFormMeta(cytjCode_auto, cytjCode_auto, props, meta);
	modifierFormMeta(cytjCode_auto, bfCode_auto, props, meta);
	modifierFormMeta(cytjCode_auto, dfCode_auto, props, meta);
	modifierFormMeta(cytjCode_auto, publicCode_auto, props, meta);
	return meta;
}

//联查单据
function billLink(that, text, props) {
	ajax({
		url: '/nccloud/arap/arappub/linkarapbill.do',
		async: false,
		data: {
			pk_bill: text.values.pk_bill.value,
			tradeType: text.values.pk_tradetype.value
		},
		success: (res) => {
			let data = res.data;
			if (data) {
				pageTo.openTo(data.url, data.condition);
			} else {
				toast({ color: 'warning', content: that.state.json['verificationsheet-000063'] }); /* 国际化处理： 未查询到路径!*/
			}
		}
	});
}

function addformrelation(meta) {
	let arr1 = [];
	arr1.push('bf');
	arr1.push('df');
	arr1.push('public');
	let arr2 = [];
	arr2.push('bf_auto');
	arr2.push('df_auto');
	arr2.push('public_auto');
	let formrelation = {
		cytjArea: arr1,
		cytjArea_auto: arr2
	};
	meta.formrelation = formrelation;
}
