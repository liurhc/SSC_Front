
/**
 * 表体区域
 */
export const tableId_bf = '20060MV_body_bf';

/**
 * 表体区域
 */
export const tableId_df = '20060MV_body_df';

/**
 * 手工核销查询区域
 */
export const verifySearchId = 'verify_query';

/**
 * 自动核销查询区域
 */
export const autoVerifySearchId = 'autoVerify_query';

/**
 * 常用条件formId
 */
export const cytjCode = 'cytjArea';
export const bfCode = 'bf';
export const dfCode = 'df';
export const publicCode = 'public';

/**
 * 核销规则formId
 */
export const hxgzCode = 'hxgzArea';

/**
 * 自动核销常用条件formId
 */
export const cytjCode_auto = 'cytjArea_auto';
export const bfCode_auto = 'bf_auto';
export const dfCode_auto = 'df_auto';
export const publicCode_auto = 'public_auto';

/**
 * 自动核销核销规则formId
 */
export const hxgzCode_auto = 'hxgzArea_auto';

/**
 * 核销方式
 */
export const hxfsCode = 'hxfsArea';


/**
 * 页面编码
 */
export const pagecode = '20060MV_LIST';


/**
 * 小应用ID
 */
export const appcode = '20060MV';

/**
 * nc端功能节点号
 */
export const funCode = '20060MV';
