

export default function afterEvent(props, moduleId, key, value, oldValue) {
	// props, moduleId(区域id), key(操作的键), value（当前值），oldValue(旧值)

	//分析方式
	if(key == 'anaPattern'){
		if(value && value.value == 'point'){//点余额
			props.form.setFormItemsDisabled(moduleId,{'enddate' : false})
		} else {//最终余额
			props.form.setFormItemsDisabled(moduleId,{'enddate' : true})
		}
	}

}
