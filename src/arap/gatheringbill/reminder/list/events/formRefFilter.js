
import { getBusinessInfo } from "nc-lightapp-front";


function modifierFormMeta(formId, props, meta) {
	meta[formId].items.map((item) => {
		if (item.itemtype != 'refer') {
			// return;
		}
		let attrcode = item.attrcode;
		switch (attrcode) {
			case 'pk_org':
				item.isMultiSelectedEnabled = true;
				item.queryCondition = () => {
					item.isShowUnit = false;
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						AppCode: props.getSearchParam('c'),
						TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
					};
				}
				break;
			case 'dept'://部门
				item.isShowUnit = true,
					item.unitCondition = () => {
						let pkOrgValue = getFirstOrgValue(props,formId);
						return {
							pkOrgs: pkOrgValue,
							TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						}
					}
				item.queryCondition = () => {
					let pkOrgValue = getFirstOrgValue(props,formId);
					return {
						itemKey: attrcode,
						busifuncode: 'all',
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						VersionStartDate: getBusinessInfo().businessDate,
						pk_org: pkOrgValue,
						TreeRefActionExt: 'nccloud.web.arap.ref.before.CrossRuleDepSqlBuilder'
					}
				}
				break;
			case 'psndoc'://业务员
				item.isShowUnit = true,
					item.unitCondition = () => {
						let pkOrgValue = getFirstOrgValue(props,formId);
						return {
							pkOrgs: pkOrgValue,
							TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						}
					}
				item.queryCondition = () => {
					let pkOrgValue = getFirstOrgValue(props,formId);
					return {
						itemKey: attrcode,
						busifuncode: 'all',
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						VersionStartDate: getBusinessInfo().businessDate,
						pk_org: pkOrgValue,
						GridRefActionExt: 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder'
					}
				}
				break;

			case 'pk_notice':
				item.itemtype = 'refer'
				item.refcode = 'arap/refer/pub/ArapReminderBillRef/index'
				item.queryCondition = () => {
					let pkOrgValue = getFirstOrgValue(props,formId);
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: pkOrgValue,
						pk_curr: props.form.getFormItemsValue(formId, 'pk_currtype').value ? props.form.getFormItemsValue(formId, 'pk_currtype').value : null,
					};
				}
				break;
			case 'accageplan':
				item.itemtype = 'refer'
				item.refcode = 'fipub/ref/pub/TimeControlRef/index'
				item.queryCondition = () => {
					let pkOrgValue = getFirstOrgValue(props,formId);
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: pkOrgValue,
					}
				}
				break;
			default:
				item.queryCondition = () => {
					let pkOrgValue = getFirstOrgValue(props,formId);
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						VersionStartDate: getBusinessInfo().businessDate,
						pk_org: pkOrgValue,
						GridRefActionExt: 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder'
					}
				}
				break;

		}
		return item;
	})
	return meta;
}


//获取选中的业务组织的第一个
function getFirstOrgValue(props,formId) {
	let pk_org = props.form.getFormItemsValue(formId, 'pk_org')
	let pkOrgValue = null
	if(pk_org && pk_org.value){
		let orgValues = pk_org.value 
		if (orgValues != null) {
			let orgArray = orgValues.split(',');
			if (orgArray != null && orgArray.length > 0) {
				pkOrgValue = orgArray[0];
			}
		}
	}
    return pkOrgValue;
}

export { modifierFormMeta }
