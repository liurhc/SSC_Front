import { createPage, ajax, base, toast, pageTo } from 'nc-lightapp-front';
import { modifierFormMeta } from './formRefFilter';
import { modifierSearchMetas } from './searchRefFilter';
import { searchId, headTableCode, bodyTableCode, queryFormCode } from '../constants';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';

export default function(props, newItems) {
	let that = this;
	props.createUIDom(
		{
			pagecode: props.getSearchParam('p'), //页面id
			appcode: props.getSearchParam('c') //注册按钮的id
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta, newItems, that);
					modifierSearchMetas(
						searchId,
						queryFormCode,
						props,
						meta,
						'F0',
						that.state.json['gatheringbill-000075']
					);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
			}
		}
	);
}

function modifierMeta(props, meta, newItems, that) {
	if (newItems) {
		let index = meta[bodyTableCode].items.length + 1;
		newItems.map((val) => {
			meta[bodyTableCode].items.push(buildItem(val[0], val[1], index));
			index = index + 1;
		});
	}

	meta[bodyTableCode].items = meta[bodyTableCode].items.map((item, key) => {
		if (item.attrcode == 'billno') {
			item.renderStatus = 'browse';
			item.render = (a, text, value) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							billLink(text, props, that);
						}}
					>
						{text.values.billno.value}
					</a>
				);
			};
		}
		return item;
	});

	meta[queryFormCode].status = 'edit';

	modifierFormMeta(queryFormCode, props, meta);
	return meta;
}

//联查单据
function billLink(text, props, that) {
	ajax({
		url: '/nccloud/arap/arappub/linkarapbill.do',
		async: false,
		data: {
			pk_bill: text.values.pk_recbill.value,
			pk_tradetype: text.values.pk_tradetype.value
		},
		success: (res) => {
			let data = res.data;
			if (data) {
				pageTo.openTo(data.url, data.condition);
			} else {
				toast({ color: 'warning', content: that.state.json['gatheringbill-000074'] }); /* 国际化处理： 未查询到路径!*/
			}
		}
	});
}

function buildItem(code, field, index) {
	let item = {};
	item.attrcode = code;
	item.color = '#555555';
	item.containlower = false;
	item.dataPowerOperationCode = null;
	item.datatype = '31';
	item.disabled = false;
	item.editAfterFlag = false;
	item.fieldDisplayed = 'refcode';
	item.fieldValued = 'refpk';
	item.formattype = null;
	item.hyperlinkflag = false;
	item.initialvalue = null;
	item.isDataPowerEnable = true;
	item.isMultiSelectedEnabled = null;
	item.isResLabel = false;
	item.isShowUnit = false;
	item.isTreelazyLoad = null;
	item.isnotmeta = false;
	item.isrevise = false;
	item.istotal = true;
	item.itemtype = 'number';
	item.label = field;
	item.languageMeta = null;
	item.max = null;
	item.maxlength = '20';
	item.metadataProperty = null;
	item.min = null;
	item.options = null;
	item.pk_defdoclist = null;
	item.position = '' + index + '';
	item.ratio = null;
	item.refName_db = null;
	item.refcode = null;
	item.required = false;
	item.scale = '8';
	item.unit = null;
	item.visible = true;
	item.width = '120px';
	return item;
}
