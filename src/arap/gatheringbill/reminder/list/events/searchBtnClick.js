import { ajax, toast, cardCache } from 'nc-lightapp-front';
import { searchId, headTableCode, bodyTableCode, queryFormCode, dataSource } from '../constants';
import initTemplate from './initTemplate';
let { setDefData, getDefData } = cardCache;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
    if (!props.form.isCheckNow(queryFormCode)) {
        return true;
    }
    dealFieldShow(props);

    let queryInfo = this.props.search.getQueryInfo(searchId);
    let querydata = {
        queryVO: queryInfo,
        remindCond: {
            model: props.form.getAllFormValue(queryFormCode),
            pageid: props.getSearchParam('p')
        },
        headTableCode: headTableCode,
        bodyTableCode: bodyTableCode,
        pageId: props.getSearchParam('p')
    };
    setDefData(searchId, dataSource, querydata);//放入缓存
    ajax({
        url: '/nccloud/arap/reminder/query.do',
        data: querydata,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                if (data) {
                    toast({color:"success"})
                    if(data.newItems){
                        initTemplate.call(this, props, data.newItems)
                    }
                    this.alldata = res.data
                    props.cardTable.setTableData(headTableCode, data[headTableCode][headTableCode]);
                    props.cardTable.setTableData(bodyTableCode, data[bodyTableCode][0][bodyTableCode]);
                    props.button.setButtonDisabled(['Print'], false)
                } else {
                    toast({ content: this.state.json['gatheringbill-000015'], color: 'warning'});/* 国际化处理： 未查询到符合条件的数据*/
                    props.cardTable.setTableData(headTableCode, {rows: []});
                    props.cardTable.setTableData(bodyTableCode, {rows: []});
                }
            }
        }
    });


};

function dealFieldShow(props) {
    let queryObj = props.form.getFormItemsValue(queryFormCode, 'queryObj')
    if (queryObj && queryObj.value == '0') {
        props.cardTable.showColByKey(headTableCode, 'reminder_customer')
        props.cardTable.hideColByKey(headTableCode, 'reminder_dept')
        props.cardTable.hideColByKey(headTableCode, 'reminder_psndoc')
    } else if (queryObj && queryObj.value == '2') {
        props.cardTable.showColByKey(headTableCode, 'reminder_dept')
        props.cardTable.hideColByKey(headTableCode, 'reminder_customer')
        props.cardTable.hideColByKey(headTableCode, 'reminder_psndoc')
    } else if (queryObj && queryObj.value == '3') {//客户=0,部门=2,业务员=3
        props.cardTable.showColByKey(headTableCode, 'reminder_psndoc')
        props.cardTable.hideColByKey(headTableCode, 'reminder_customer')
        props.cardTable.hideColByKey(headTableCode, 'reminder_dept')
    }
}
