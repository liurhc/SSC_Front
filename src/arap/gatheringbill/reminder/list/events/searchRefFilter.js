import { ajax, getBusinessInfo } from "nc-lightapp-front";

/**
 * 查询区字段过滤
 * @param {*} searchId 查询区id
 * @param {*} formId 表单id,用于获取表单上的pk_org等作为过滤条件
 * @param {*} props 当前props
 * @param {*} meta meta
 * @param {*} billType 交易类型
 * 
 */
function modifierSearchMetas(searchId, formId, props, meta, billType,refname) {

    meta[searchId].items = meta[searchId].items.map((item, key) => {
        let attrcode = item.attrcode;
        item.isShowUnit = false;
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = true;
            item.unitValueIsNeeded = false; //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
        }
        switch (attrcode) {
            case 'pk_org'://财务组织
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        AppCode: props.getSearchParam('c'),
                        TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
                    };
                }
                break;
            case 'bodys.pk_org'://财务组织
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        AppCode: props.getSearchParam('c'),
                        TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
                    };
                }
                break;
            case 'pk_deptid'://部门
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                    };
                }
                break;
            case 'pk_deptid_v'://部门版本
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.pk_deptid'://部门
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                    };
                }
                break;
            case 'bodys.pk_deptid_v'://部门版本
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'supplier'://供应商
                item.queryCondition = () => {
                    //付款单和应付单
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'customer'://客户
                item.queryCondition = () => {
                    //收款单和应收单
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.supplier'://单行供应商
                item.queryCondition = () => {
                    //付款单和应付单
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.customer'://单行客户
                item.queryCondition = () => {
                    //收款单和应收单
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'pk_tradetypeid'://交易类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: billType,
                    }
                }
                break;
            case 'bodys.pk_tradetypeid'://交易类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: billType,
                    }
                }
                break;
            case 'pk_fiorg_v'://废弃财务组织版本(根据集团)
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    if (billType == "F3") {
                        //付款单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                        };
                    }
                }
                break;
            case 'bodys.pk_fiorg_v'://废弃财务组织版本(根据集团)
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    if (billType == "F3") {
                        //付款单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                        };
                    }
                }
                break;
            case 'pk_fiorg'://废弃财务组织(根据集团)
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    if (billType == "F3") {
                        //付款单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                        };
                    }

                }
                break;
            case 'bodys.pk_fiorg'://废弃财务组织(根据集团)
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    if (billType == "F3") {
                        //付款单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                        };
                    }

                }
                break;
            case 'pk_pcorg'://利润中心
                item.queryCondition = () => {
                    if (billType == "F0" || billType == "F2") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                        };
                    }
                }
                break;
            case 'bodys.pk_pcorg'://单行利润中心
                item.queryCondition = () => {
                    if (billType == "F0" || billType == "F2") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                        };
                    }
                }
                break;
            case 'pk_pcorg_v'://利润中心版本
                item.queryCondition = () => {
                    if (billType == "F0" || billType == "F2") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                        };
                    }
                }
                break;
            case 'bodys.pk_pcorg_v'://单行利润中心版本
                item.queryCondition = () => {
                    if (billType == "F0" || billType == "F2") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                        };
                    }
                }
                break;
            case 'sett_org'://结算财务组织(根据集团) 且根据财务组织联动
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            case 'sett_org_v'://结算财务组织版本(根据集团)且根据财务组织联动
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            case 'officialprintuser'://正式打印人
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            case 'creator'://创建人
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            case 'subjcode'://科目
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    let pkAccountingbook = null;
                    ajax({
                        url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
                        data: {
                            pk_org: pkOrgValue,
                        },
                        async: false,
                        success: (res) => {
                            if (res.success) {
                                pkAccountingbook = res.data;
                            }
                        }
                    })
                    return {
                        isDataPowerEnable: 'Y',
                        pk_accountingbook: pkAccountingbook,
                        DataPowerOperationCode: 'fi',//使用权组
                        datestr: getBusinessInfo().businessDate
                    };
                }
                break;
            case 'bodys.subjcode'://科目
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    let pkAccountingbook = null;
                    ajax({
                        url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
                        data: {
                            pk_org: pkOrgValue,
                        },
                        async: false,
                        success: (res) => {
                            if (res.success) {
                                pkAccountingbook = res.data;
                            }
                        }
                    })
                    return {
                        isDataPowerEnable: 'Y',
                        pk_accountingbook: pkAccountingbook,
                        DataPowerOperationCode: 'fi',//使用权组
                        datestr: getBusinessInfo().businessDate
                    };
                }
                break;
            case 'payaccount'://付款银行账户
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    if (billType == "F3" || billType == "F1") {
                        //付款单和应付单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            refnodename: refname,/* 国际化处理： 使用权参照*/
                            pk_org: pkOrgValue,
                            pk_billtype: billType,
                            pk_currtype: props.search.getSearchValByField(searchId, 'pk_currtype') ? props.search.getSearchValByField(searchId, 'pk_currtype').value.firstvalue : null,
                            GridRefActionExt: 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder'
                        };
                    } else {
                        let supplier = props.search.getSearchValByField(searchId, 'supplier') ? props.search.getSearchValByField(searchId, 'supplier').value.firstvalue : null;
                        let customer = props.search.getSearchValByField(searchId, 'customer') ? props.search.getSearchValByField(searchId, 'customer').value.firstvalue : null;
                        let pk_cust = null;
                        let accclass = null;
                        if (supplier) {
                            pk_cust = supplier;
                            accclass = 3;
                        } else if (customer) {
                            pk_cust = customer;
                            accclass = 1;
                        }
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            accclass: accclass,//3代表供应商
                            pk_cust: pk_cust,
                            pk_currtype: props.search.getSearchValByField(searchId, 'pk_currtype') ? props.search.getSearchValByField(searchId, 'pk_currtype').value.firstvalue : null
                        };
                    }
                }
                break;
            case 'bodys.payaccount'://付款银行账户
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    if (billType == "F3" || billType == "F1") {
                        //付款单和应付单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            refnodename: refname,/* 国际化处理： 使用权参照*/
                            pk_org: pkOrgValue,
                            pk_billtype: billType,
                            pk_currtype: props.search.getSearchValByField(searchId, 'bodys.pk_currtype') ? props.search.getSearchValByField(searchId, 'bodys.pk_currtype').value.firstvalue : null,
                            GridRefActionExt: 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder'
                        };
                    } else {
                        let supplier = props.search.getSearchValByField(searchId, 'bodys.supplier') ? props.search.getSearchValByField(searchId, 'bodys.supplier').value.firstvalue : null;
                        let customer = props.search.getSearchValByField(searchId, 'bodys.customer') ? props.search.getSearchValByField(searchId, 'bodys.customer').value.firstvalue : null;
                        let pk_cust = null;
                        let accclass = null;
                        if (supplier) {
                            pk_cust = supplier;
                            accclass = 3;
                        } else if (customer) {
                            pk_cust = customer;
                            accclass = 1;
                        }
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            accclass: accclass,//3代表供应商
                            pk_cust: pk_cust,
                            pk_currtype: props.search.getSearchValByField(searchId, 'bodys.pk_currtype') ? props.search.getSearchValByField(searchId, 'bodys.pk_currtype').value.firstvalue : null
                        };
                    }
                }
                break;
            case 'recaccount'://收款银行账户(1代表客户，2代表客商，3代表供应商) 客商占定 @auto zhangyg --- ok
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    if (billType == "F0" || billType == "F2") {
                        //收款单和应收单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            refnodename: refname,/* 国际化处理： 使用权参照*/
                            pk_org: pkOrgValue,
                            pk_billtype: billType,
                            pk_currtype: props.search.getSearchValByField(searchId, 'pk_currtype') ? props.search.getSearchValByField(searchId, 'pk_currtype').value.firstvalue : null,
                            GridRefActionExt: 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder'
                        };
                    } else {
                        let supplier = props.search.getSearchValByField(searchId, 'supplier') ? props.search.getSearchValByField(searchId, 'supplier').value.firstvalue : null;
                        let customer = props.search.getSearchValByField(searchId, 'customer') ? props.search.getSearchValByField(searchId, 'customer').value.firstvalue : null;
                        let pk_cust = null;
                        let accclass = null;
                        if (supplier) {
                            pk_cust = supplier;
                            accclass = 3;
                        } else if (customer) {
                            pk_cust = customer;
                            accclass = 1;
                        }
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            accclass: accclass,//3代表供应商
                            pk_cust: pk_cust,
                            pk_currtype: props.search.getSearchValByField(searchId, 'pk_currtype') ? props.search.getSearchValByField(searchId, 'pk_currtype').value.firstvalue : null
                        };
                    }
                }
                break;
            case 'bodys.recaccount'://收款银行账户(1代表客户，2代表客商，3代表供应商) 客商占定 @auto zhangyg --- ok
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    if (billType == "F0" || billType == "F2") {
                        //收款单和应收单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            refnodename: refname,/* 国际化处理： 使用权参照*/
                            pk_org: pkOrgValue,
                            pk_billtype: billType,
                            pk_currtype: props.search.getSearchValByField(searchId, 'bodys.pk_currtype') ? props.search.getSearchValByField(searchId, 'bodys.pk_currtype').value.firstvalue : null,
                            GridRefActionExt: 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder'
                        };
                    } else {
                        let supplier = props.search.getSearchValByField(searchId, 'bodys.supplier') ? props.search.getSearchValByField(searchId, 'bodys.supplier').value.firstvalue : null;
                        let customer = props.search.getSearchValByField(searchId, 'bodys.customer') ? props.search.getSearchValByField(searchId, 'bodys.customer').value.firstvalue : null;
                        let pk_cust = null;
                        let accclass = null;
                        if (supplier) {
                            pk_cust = supplier;
                            accclass = 3;
                        } else if (customer) {
                            pk_cust = customer;
                            accclass = 1;
                        }
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            accclass: accclass,//3代表供应商
                            pk_cust: pk_cust,
                            pk_currtype: props.search.getSearchValByField(searchId, 'bodys.pk_currtype') ? props.search.getSearchValByField(searchId, 'bodys.pk_currtype').value.firstvalue : null
                        };
                    }
                }
                break;
            case 'sendcountryid'://发货国
                item.queryCondition = () => {
                    if (billType == "F0") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                        };
                    }
                }
                break;
            case 'taxcountryid'://报税国
                item.queryCondition = () => {
                    if (billType == "F0" || billType == "F1") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                        };
                    }
                }
                break;
            case 'ordercubasdoc'://订单客户
                item.queryCondition = () => {
                    if (billType == "F0") {
                        let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }
                }
                break;
            case 'bodys.ordercubasdoc'://订单客户
                item.queryCondition = () => {
                    if (billType == "F0") {
                        let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }
                }
                break;
            case 'pk_balatype'://结算方式(根据pk_billtype)
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_billtype: billType,
                        GridRefActionExt: 'nccloud.web.arap.ref.before.BalatypeSqlBuilder',
                    };
                }
                break;
            case 'costcenter'://成本中心
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.search.getSearchValByField(searchId, 'pk_pcorg') ? props.search.getSearchValByField(searchId, 'pk_pcorg').value.firstvalue : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.CostCenterSqlBuilder'
                    };
                }
                break;
            case 'pk_psndoc'://业务员(根据部门+组织过滤)
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                    }
                }
                break;
            case 'bodys.pk_psndoc'://业务员(根据部门+组织过滤)
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                    }
                }
                break;
            case 'pk_subjcode'://收支项目(根据组织)
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.pk_subjcode'://收支项目(根据组织)
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'pk_currtype'://币种
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                    };
                }
                break;
            case 'bodys.pk_currtype'://单行币种
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                    };
                }
                break;
            case 'bankrollprojet'://资金计划项目 
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.bankrollprojet'://单行资金计划项目 
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'cashitem'://现金流量项目
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.cashitem'://单行现金流量项目
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'pu_org'://业务组织(业务单元（财务组织委托）)
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        isDataPowerEnable: 'Y',
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            case 'pu_org_v'://业务单元版本(财务组织委托)@auto zhangygw --- ok 
                item.isShowUnit = false;
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            case 'so_org'://业务组织(业务单元（财务组织委托）) @atuo:zhangygw ---- ok
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    if (billType == "F0" || billType == "F2") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pkOrgs: pkOrgValue,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pkOrgs: pkOrgValue,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        };
                    }

                }
                break;
            case 'so_org_v'://业务单元版本(财务组织委托)@auto zhangygw --- ok 
                item.isShowUnit = false;
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    if (billType == "F0" || billType == "F2") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        };
                    }

                }
                break;
            case 'pu_deptid'://业务部门(根据组织)
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pu_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pu_org') || {}).value.firstvalue);
                    if (billType == "F3" || billType == "F1") {
                        //付款单和应付单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }

                }
                break;
            case 'bodys.pu_deptid'://业务部门(根据组织)
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.pu_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.pu_org') || {}).value.firstvalue);
                    if (billType == "F3" || billType == "F1") {
                        //付款单和应付单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }

                }
                break;
            case 'pu_deptid_v'://业务部门版本(根据组织) --- ok
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pu_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pu_org') || {}).value.firstvalue);
                    if (billType == "F3" || billType == "F1") {
                        //付款单和应付单
                        return {
                            isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',//使用权组
                            pk_org: pkOrgValue
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }

                }
                break;
            case 'bodys.pu_deptid_v'://业务部门版本(根据组织) --- ok
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.pu_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.pu_org') || {}).value.firstvalue);
                    if (billType == "F3" || billType == "F1") {
                        //付款单和应付单
                        return {
                            isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',//使用权组
                            pk_org: pkOrgValue
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }

                }
                break;
            case 'pu_psndoc'://业务人员(根据组织\业务部门)@auto zhangygw 
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pu_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pu_org') || {}).value.firstvalue);
                    if (billType == "F3") {
                        //付款单和应付单
                        return {
                            busifuncode: 'all',
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    } else {
                        return {
                            busifuncode: 'all',
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }
                }
                break;
            case 'bodys.pu_psndoc'://业务人员(根据组织\业务部门)@auto zhangygw 
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.pu_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.pu_org') || {}).value.firstvalue);
                    if (billType == "F3") {
                        //付款单和应付单
                        return {
                            busifuncode: 'all',
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    } else {
                        return {
                            busifuncode: 'all',
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }
                }
                break;
            case 'so_deptid'://业务部门(根据组织)
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    if (billType == "F0" || billType == "F2") {
                        //收款单和应收单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }

                }
                break;
            case 'bodys.so_deptid'://业务部门(根据组织)
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.so_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.so_org') || {}).value.firstvalue);
                    if (billType == "F0" || billType == "F2") {
                        //收款单和应收单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }

                }
                break;
            case 'so_deptid_v'://业务部门版本(根据组织) --- ok
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    if (billType == "F0" || billType == "F2") {
                        //收款单和应收单
                        return {
                            isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',//使用权组
                            pk_org: pkOrgValue
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }
                }
                break;
            case 'bodys.so_deptid_v'://业务部门版本(根据组织) --- ok
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.so_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.so_org') || {}).value.firstvalue);
                    if (billType == "F0" || billType == "F2") {
                        //收款单和应收单
                        return {
                            isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',//使用权组
                            pk_org: pkOrgValue
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue
                        };
                    }
                }
                break;
            case 'bodys.so_psndoc'://业务员(根据部门+组织过滤)
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.so_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'bodys.so_org') || {}).value.firstvalue);
                    item.isShowUnit = true;
                    if (billType == "F0" || billType == "F2") {
                        return {
                            busifuncode: 'all',
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue,
                        };
                    } else {
                        return {
                            busifuncode: 'all',
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue,
                        };
                    }
                }
                break;
            case 'so_psndoc'://业务员(根据部门+组织过滤)
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    item.isShowUnit = true;
                    if (billType == "F0" || billType == "F2") {
                        return {
                            busifuncode: 'all',
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue,
                        };
                    } else {
                        return {
                            busifuncode: 'all',
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrgValue,
                        };
                    }
                }
                break;
            case 'checkelement'://责任核算要素
                item.queryCondition = () => {
                    let pk_pcorg = props.search.getSearchValByField(searchId, 'pk_pcorg') ? props.search.getSearchValByField(searchId, 'pk_pcorg').value.firstvalue : null;
                    if (!pk_pcorg) {
                        //设置责任核算要素不可编辑
                        props.search.setDisabledByField(searchId, 'checkelement', true);
                    } else {
                        //设置责任核算要素可编辑
                        props.search.setDisabledByField(searchId, 'checkelement', false);
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pk_pcorg
                        };
                    }
                }
                break;
            case 'bodys.checkelement'://责任核算要素
                item.queryCondition = () => {
                    let pk_pcorg = props.search.getSearchValByField(searchId, 'bodys.pk_pcorg') ? props.search.getSearchValByField(searchId, 'bodys.pk_pcorg').value.firstvalue : null;
                    if (!pk_pcorg) {
                        //设置责任核算要素不可编辑
                        props.search.setDisabledByField(searchId, 'bodys.checkelement', true);
                    } else {
                        //设置责任核算要素可编辑
                        props.search.setDisabledByField(searchId, 'bodys.checkelement', false);
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pk_pcorg
                        };
                    }
                }
                break;
            case 'bodys.so_ordertype'://销售交易类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: billType,
                    }
                }
                break;
            case 'bodys.so_transtype'://销售渠道类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    }
                }
                break;
            case 'bodys.material'://单行物料
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        matcustcode: props.search.getSearchValByField(searchId, 'bodys.matcustcode') ? props.search.getSearchValByField(searchId, 'bodys.matcustcode').value.firstvalue : null,//物料客户码，应收模块需要传值
                        GridRefActionExt: 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder'
                    };
                }
                break;
            case 'bodys.productline'://产品线
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            case 'bodys.pk_payterm'://付款协议(根据财务组织)
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.freecust'://散户  根据供应商和客户判断可编辑性，根据供应商和客户过滤
                item.queryCondition = () => {
                    let supplier_2 = props.search.getSearchValByField(searchId, 'bodys.supplier') ? props.search.getSearchValByField(searchId, 'bodys.supplier').value.firstvalue : null;
                    let customer_2 = props.search.getSearchValByField(searchId, 'bodys.customer') ? props.search.getSearchValByField(searchId, 'bodys.customer').value.firstvalue : null;
                    ajax({
                        url: '/nccloud/arap/ref/freecustcontrol.do',
                        data: {
                            supplier: supplier_2,
                            customer: customer_2
                        },
                        success: (res) => {
                            if (res.success) {
                                falg = res.data;
                            }
                        }
                    });
                    if (flag) {
                        return {
                            isDataPowerEnable: 'Y',
                            supplier: supplier_2,
                            customer: customer_2,
                            GridRefActionExt: 'nccloud.web.arap.ref.before.FreeCustSqlBuilder'
                        };
                    }
                }
                break;
            case 'bodys.taxcodeid'://单行税码(根据报税国和购销类型)
                item.queryCondition = () => {
                    if (billType == "F1") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            taxcountryid: props.search.getSearchValByField(searchId, 'taxcountryid') ? props.search.getSearchValByField(searchId, 'taxcountryid').value.firstvalue : null,//报税国
                            buysellflag: props.search.getSearchValByField(searchId, 'bodys.buysellflag') ? props.search.getSearchValByField(searchId, 'bodys.buysellflag').value.firstvalue : null,//购销类型
                            GridRefActionExt: 'nccloud.web.arap.ref.before.TaxcodeIdSqlBuilder'
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            taxcountryid: props.search.getSearchValByField(searchId, 'taxcountryid') ? props.search.getSearchValByField(searchId, 'taxcountryid').value.firstvalue : null,//报税国
                            buysellflag: props.search.getSearchValByField(searchId, 'bodys.buysellflag') ? props.search.getSearchValByField(searchId, 'bodys.buysellflag').value.firstvalue : null,//购销类型
                            GridRefActionExt: 'nccloud.web.arap.ref.before.TaxcodeIdSqlBuilder'
                        };
                    }

                }
                break;
            case 'bodys.project'://项目
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.project_task'://项目任务
                item.queryCondition = () => {
                    let pkOrgValue = props.form.getFormItemsValue(formId, 'pk_org').value
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        project: props.search.getSearchValByField(searchId, 'bodys.project') ? props.search.getSearchValByField(searchId, 'bodys.project').value.firstvalue : null,//报税国
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.ProjectTaskSqlBuilder'
                    };
                }
                break;
            case 'bodys.rececountryid'://收货国
                item.queryCondition = () => {
                    if (billType == "F0") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                        };
                    }
                }
                break;
            default:
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
        }
        return item;
    })


    return meta;
}

//获取选中的业务组织的第一个
function getFirstOrgValue(orgValues) {
    let pkOrgValue = '';
    if (orgValues != null) {
        let orgArray = orgValues.split(',');
        if (orgArray != null && orgArray.length > 0) {
            pkOrgValue = orgArray[orgArray.length - 1];
        }
    }
    return pkOrgValue;
}

export { modifierSearchMetas }
