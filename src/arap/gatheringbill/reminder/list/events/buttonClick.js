import { ajax, base, toast, getBusinessInfo, cardCache } from 'nc-lightapp-front';
import { searchId, headTableCode, bodyTableCode, queryFormCode, dataSource } from '../constants';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';
import initTemplate from './initTemplate';
let { setDefData, getDefData } = cardCache;

export default function buttonClick(props, id) {
    switch (id) {
        case 'Query':
            props.search.openAdvSearch(searchId, true, () => {
                let isFirstQuery = this.isFirstQuery;
                if (!isFirstQuery) {
                    return
                }
                //设置默认值
                //分析方式 anaPattern ，截止日期 enddate ，单据日期开始 billdate_begin ，单据日期结束 billdate_end ，查询对象 queryObj ，
                //应收范围 recScope ，分析日期 analysedate ，单据状态 billStatus ，包含未到期 includeExpire
                let pk_org = getContext(loginContextKeys.pk_org);
                let org_Name = getContext(loginContextKeys.org_Name);
                let date = getBusinessInfo().businessDate.substring(0, 10)
                if (pk_org && org_Name) {
                    props.form.setFormItemsValue(queryFormCode, { 'pk_org': { value: pk_org, display: org_Name } });
                }
                let beginDate = date.substring(0, 7) + '-01 00:00:00'
                let endDate = date + ' 23:59:59'
                props.form.setFormItemsValue(queryFormCode, {
                    'anaPattern': { value: 'final', display: this.state.json['gatheringbill-000066'] }, 'enddate': { value: endDate },/* 国际化处理： 最终余额*/
                    'billdate_begin': { value: beginDate }, 'billdate_end': { value: endDate },
                    'queryObj': { value: '0', display: this.state.json['gatheringbill-000067'] }, 'recScope': { value: 'allrec', display: this.state.json['gatheringbill-000068'] },/* 国际化处理： 客户,全部应收*/
                    'analysedate': { value: 'expiredate', display: this.state.json['gatheringbill-000069'] }, 'billStatus': { value: 'all', display: this.state.json['gatheringbill-000070'] }, 'includeExpire': { value: '1', display: this.state.json['gatheringbill-000037'] },/* 国际化处理： 到期日,全部,否*/
                });
                this.isFirstQuery = false
            })
            break;
        case 'Refresh':
            let queryData = getDefData(searchId, dataSource);
            if (queryData) {
                ajax({
                    url: '/nccloud/arap/reminder/query.do',
                    data: queryData,
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            if (data) {
                                toast({ color: 'success', title: this.state.json['gatheringbill-000078'] });/* 国际化处理： 刷新成功*/
                                if (data.newItems) {
                                    initTemplate.call(this, props, data.newItems)
                                }
                                this.alldata = res.data
                                props.cardTable.setTableData(headTableCode, data[headTableCode][headTableCode]);
                                props.cardTable.setTableData(bodyTableCode, data[bodyTableCode][0][bodyTableCode]);
                                props.button.setButtonDisabled(['Print'], false)
                            } else {
                                toast({ content: this.state.json['gatheringbill-000015'], color: 'warning' });/* 国际化处理： 未查询到符合条件的数据*/
                                props.cardTable.setTableData(headTableCode, { rows: [] });
                                props.cardTable.setTableData(bodyTableCode, { rows: [] });
                            }
                        }
                    }
                });
            } else {
                toast({ color: 'success', title: this.state.json['gatheringbill-000078'] });/* 国际化处理： 刷新成功*/
            }
            break;
        case 'Print'://打印
            if (this.checkedDatas.length == 0) {
                toast({ content: this.state.json['gatheringbill-000072'], color: 'warning' });/* 国际化处理： 请先选择数据*/
                return;
            }
            let pks = {
                pageid: props.getSearchParam("p"),
                model: {
                    areaType: "table",
                    pageinfo: null,
                    rows: []
                }
            };
            pks.model.rows = this.checkedDatas;
            let printOids = JSON.stringify(pks);
            this.printData.oids = [printOids];
            this.onPrint();
            break;
    }
}
