import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import afterEvent from './afterEvent';
export { searchBtnClick, initTemplate, buttonClick, afterEvent };
