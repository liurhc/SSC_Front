import { ajax, base, createPage, print, pageTo, getMultiLang, createPageIcon, getBusinessInfo } from 'nc-lightapp-front';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { searchId, headTableCode, bodyTableCode, queryFormCode } from './constants';
import { buttonClick, initTemplate, searchBtnClick, afterEvent } from './events';
import './index.less';
let { NCTabsControl } = base;

/**
 * 应收管理，催款单
 */
class List extends Component {
	constructor(props) {
		super(props);
		this.searchId = searchId;
		this.headTableCode = headTableCode;
		this.bodyTableCode = bodyTableCode;
		this.queryFormCode = queryFormCode
		let { form, button, table, insertTable, search } = this.props;
		let { setSearchValue, setSearchValByField, getAllSearchData } = search;
		this.setSearchValByField = setSearchValByField; //设置查询区某个字段值
		this.getAllSearchData = getAllSearchData; //获取查询区所有字段数据
		this.printData = {
			funcode: props.getSearchParam('c'),
			billtype: '', //单据类型
			appcode: props.getSearchParam('c'), //功能节点编码，即模板编码
			nodekey: 'card', //模板节点标识
			oids: [], // 功能节点的数据主键
			userjson: ''
		};
		this.alldata = [];
		this.checkedDatas = [];
		this.isFirstQuery = true;
		this.state = {
			json: {}
		};
	}
	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				initTemplate.call(this, this.props);
				this.props.button.setButtonDisabled([ 'Print' ], true);
			});
		};
		getMultiLang({
			moduleId: [ 'gatheringbill', 'public' ],
			domainName: 'arap',
			currentLocale: 'simpchn',
			callback
		});
	}

	//主表行点击方法
	RowClick = (props, moduleId, record, index) => {
		props.cardTable.setTableData(this.bodyTableCode, this.alldata[this.bodyTableCode][index][this.bodyTableCode]);
		let meta = props.meta.getMeta();
		meta[bodyTableCode].items = meta[bodyTableCode].items.map((item, key) => {
			if (item.attrcode == 'billno') {
				item.renderStatus = 'browse';
				item.render = (a, text, value) => {
					return (
						<a
							style={{ textDecoration: 'none', cursor: 'pointer' }}
							onClick={() => {
								this.billLink(text, props);
							}}
						>
							{text.values.billno.value}
						</a>
					);
				};
			}
			return item;
		});
		props.renderItem('table', bodyTableCode, 'billno', null);

		this.checkedDatas = this.alldata[this.bodyTableCode][index][this.bodyTableCode].rows;
		let pks = {
			pageid: props.getSearchParam('p'),
			model: {
				areaType: 'table',
				pageinfo: null,
				rows: []
			}
		};
		//去除非法字段
		delete record.key;
		//带入打印数据
		pks.model.rows = [
			{
				rowid: null,
				status: '0',
				values: record.values
			}
		];
		this.printData.userjson = JSON.stringify(pks);
	};

	//联查单据
	billLink(text, props) {
		ajax({
			url: '/nccloud/arap/arappub/linkarapbill.do',
			async: false,
			data: {
				pk_bill: text.values.pk_recbill.value,
				pk_tradetype: text.values.pk_tradetype.value
			},
			success: (res) => {
				let data = res.data;
				if (data) {
					pageTo.openTo(data.url, data.condition);
				} else {
					toast({ color: 'warning', content: this.state.json['gatheringbill-000074'] }); /* 国际化处理： 未查询到路径!*/
				}
			}
		});
	}

	// 添加查询区form
	addAdvBody = () => {
		let { createForm } = this.props.form;
		return (
			<div>
				{createForm(queryFormCode, {
					onAfterEvent: afterEvent.bind(this)
				})}
			</div>
		);
	};


	/**
	 * 高级查询面板中的清空按钮 点击事件钩子，用于业务组清除自定义查询条件值
	 */
	clearSearchEve = () => {
		let date = getBusinessInfo().businessDate.substring(0, 10)
		let beginDate = date.substring(0, 7) + '-01 00:00:00'
		let endDate = date + ' 23:59:59'
		this.props.form.EmptyAllFormValue(this.queryFormCode);
		this.props.form.setFormItemsValue(this.queryFormCode, {
			'anaPattern': { value: 'final', display: this.state.json['gatheringbill-000066'] }, 'enddate': { value: endDate },/* 国际化处理： 最终余额*/
			'billdate_begin': { value: beginDate }, 'billdate_end': { value: endDate },
			'queryObj': { value: '0', display: this.state.json['gatheringbill-000067'] }, 'recScope': { value: 'allrec', display: this.state.json['gatheringbill-000068'] },/* 国际化处理： 客户,全部应收*/
			'analysedate': { value: 'expiredate', display: this.state.json['gatheringbill-000069'] }, 'billStatus': { value: 'all', display: this.state.json['gatheringbill-000070'] }, 'includeExpire': { value: '1', display: this.state.json['gatheringbill-000037'] },/* 国际化处理： 到期日,全部,否*/
		});
	}

	//打印
	onPrint = () => {
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/arap/arappub/printGridData.do', //后台服务url
			this.printData,
			false
		);
	};

	render() {
		let { table, search } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createCardTable } = this.props.cardTable;
		return (
			<div className="nc-bill-list" id="reminderId">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{this.state.json['gatheringbill-000077']}</h2>
						{/* 国际化处理： 催款单*/}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div id="reminderSearchArea">
					{NCCreateSearch(this.searchId, {
						onlyShowSuperBtn: true, // 只显示高级按钮
						clickSearchBtn: searchBtnClick.bind(this), //   点击按钮事件
						showAdvBtn: true, //  显示高级按钮
						addAdvBody: this.addAdvBody, // 添加高级查询区自定义查询条件Dom (fun) , return Dom
						advSearchClearEve: this.clearSearchEve.bind(this),//高级查询面板中的清空按钮 点击事件钩子，用于业务组清除自定义查询条件值
						hideBtnArea: true // 隐藏查询按钮区域,可以使用其他按钮调用props.search.openAdvSearch(searchId, true)显示查询框
					})}
				</div>
				<div className="nc-bill-table-area">
					{createCardTable(this.headTableCode, {
						showIndex: true,
						onRowClick: this.RowClick.bind(this),
						pageSize: 10,
						hideSwitch: () => {
							return false;
						}
					})}
				</div>
				<div className="nc-bill-table-area">
					{createCardTable(this.bodyTableCode, {
						showIndex: true,
						pageSize: 10,
						hideSwitch: () => {
							return false;
						}
					})}
				</div>
			</div>
		);
	}
}

List = createPage({})(List);

ReactDOM.render(<List />, document.querySelector('#app'));
