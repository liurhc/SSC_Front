
/**
 * 查询区域
 */
export const searchId = 'reminder_query';

/**
 * 查询区查询条件编码
 */
export const queryFormCode = 'queryFormArea';

/**
 * 主表编码
 */
export const headTableCode = 'reminder_head';

/**
 * 子表编码
 */
export const bodyTableCode = 'reminder_body';

/**
 * 页面编码
 */
export const pageCode = '20060CUFB_LIST';

/**
 * 应用编码
 */
export const appCode = '20060CUFB';

/**
 * 查询模板主键
 */
export const oid = '1001Z31000000000LLXV';

export const dataSource = 'fi.arap.reminder.20060CUFB';


