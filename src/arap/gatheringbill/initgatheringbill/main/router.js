import { asyncComponent } from 'nc-lightapp-front';
import List from '../list';

//收款期初卡片
const card = asyncComponent(() => import(/*webpackChunkName:"/arap/gatheringbill/initgatheringbill/card/card"*/  /* webpackMode: "eager" */ '../card'));


const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: card
	}

];

export default routes;


