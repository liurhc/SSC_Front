import { ajax, base, toast } from 'nc-lightapp-front';
import { tableId, billType, dataSource } from '../constants';
import { innerButton } from '../../../../public/components/pubUtils/buttonName';

export default function (that,props, key, text, record, index) {
    switch (key) {
        // 表格操修改
        case innerButton.Edit_inner:
            let editData = {
                pk_bill: record.pk_gatherbill.value,
                billType: billType
            };
            ajax({
                url: '/nccloud/arap/initgatheringbill/edit.do',
                data: editData,
                success: (res) => {
                    if (res.success) {
                        props.pushTo('/card', {
                            status: 'edit',
                            id: record.pk_gatherbill.value,
                            pagecode: record.pk_tradetype.value,
                        })
                    }
                }
            });
            break;
        case innerButton.Delete_inner:
            ajax({
                url: '/nccloud/arap/initgatheringbill/delete.do',
                data: [{
                    pk_bill: record.pk_gatherbill.value,
                    ts: record.ts.value
                }],
                success: (res) => {
                    if (res.success) {
                        toast({ color: 'success', content: that.state.json['gatheringbill-000017'] });/* 国际化处理： 删除成功*/
                        //删除当前行数据
                        props.table.deleteTableRowsByIndex(tableId, index);
                        //删除缓存数据
                        props.table.deleteCacheId(tableId, record.pk_gatherbill.value);
                        //删行之后控制肩部按钮
                        that.onSelected();
                    }
                }
            });
            break;
        case innerButton.Copy_inner:
            props.pushTo('/card', {
                status: 'add',
                id: record.pk_gatherbill.value,
                type: 'copy',
                pagecode: record.pk_tradetype.value
            })
            break;
        default:
            break;
    }
};
