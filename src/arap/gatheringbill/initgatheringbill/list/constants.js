
/**
 * 查询区域
 */
export const searchId = 'query';

/**
 * 列表区域
 */
export const tableId = 'list';

/**
 * 单据类型
 */
export const billType = 'F2';

/**
 * 默认交易类型
 */
export const tradeType = 'D2';

/**
 * 默认模板节点标识
 */
export const nodekey = 'list';


/**
* 单页应用缓存,命名规范为："领域名.模块名.节点名.自定义名"。 
*/
export const dataSource = 'fi.arap.initgatheringbill.20060RO';


/**
 * 单页应用缓存主键名字
 */
export const pkname  = 'pk_gatherbill';

	/**
 * 单页查询区缓存主键名字
 */
export const searchKey = 'pk_gatherbill_search';
