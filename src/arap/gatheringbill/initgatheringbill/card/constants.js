
/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 表体区域
 */
export const tableId = 'bodys';

/**
 * 单据类型
 */
export const billType = 'F2';

/**
 * 默认交易类型
 */
export const tradeType = 'D2';

/**
 * 默认模板节点标识
 */
export const nodekey = 'card';


/**
* 单页应用缓存,命名规范为："领域名.模块名.节点名.自定义名"。 
*/
export const dataSource = 'fi.arap.initgatheringbill.20060RO';


/**
 * 单页应用缓存主键名字
 */
export const pkname  = 'pk_gatherbill';
