import { ajax, base, toast, cardCache } from 'nc-lightapp-front';
import { tableId, dataSource, formId } from '../constants';
import { innerButton } from '../../../../public/components/pubUtils/buttonName.js';
import { copyInner, resetBodyPk, clearTopInfos, calculateHeadMoney, deleteInner, pasteInner } from '../../../../public/components/pubUtils/billPubUtil.js';
import { cardBodyControl } from '../../../../public/components/pubUtils/buttonvisible.js';
let { getDefData, setDefData } = cardCache;
export default function (that,props, key, text, record, index) {
    switch (key) {
        // 表格操修改
        case innerButton.open_browse:
            props.cardTable.toggleRowView(tableId, record);
            break;
        case innerButton.open_edit:
            props.cardTable.openModel(tableId, 'edit', record, index);
            break;
        case innerButton.Copy_inner:
            copyInner(record, dataSource,that);
            that.setState({
                buttonfalg: false
            }, () => {
                cardBodyControl(props, that.state.buttonfalg);
            })
            break;
        case innerButton.Insert_inner:
            let data = props.createMasterChildData(that.getPagecode(), formId, tableId);
            //清空cardData的表体
            data.body[tableId].rows = [];
            ajax({
                url: '/nccloud/arap/initgatheringbill/addline.do',
                data: data,
                success: (res) => {
                    if (res.data && res.data.body) {
                        props.cardTable.addRow(tableId, index + 1, res.data.body[tableId].rows[0].values);
                    } else {
                        props.cardTable.addRow(tableId);
                    }
                }
            });
            break;
        case innerButton.Delete_inner:
            deleteInner(that, props, tableId, index);
            //删行之后控制肩部按钮
            that.onSelected();
            break;
        case innerButton.Paste_inner:
            pasteInner(that, props, dataSource, tableId, index)
            break;
        default:
            break;
    }
};
