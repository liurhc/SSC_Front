import { ajax, toast, print, cardCache,promptBox } from 'nc-lightapp-front';
import { copyLine, delLine, pasteLine, pasteToEndLine } from '../../../../public/components/pubUtils/billPubUtil.js';
import { headButton, bodyButton } from '../../../../public/components/pubUtils/buttonName';
import { billType, tradeType, formId, tableId, dataSource,pkname } from '../constants';
import {cardBodyControl} from '../../../../public/components/pubUtils/buttonvisible.js';
import { getTitle } from '../../../../public/components/getCartTableData.js';
import initTemplate from './initTemplate';
import arapLinkReport from "../../../../public/components/arapBillLinkReport.js";
import {copyBill} from '../../../../public/components/pubUtils/CopyBill/arapCopyBill';
let { getDefData,updateCache,deleteCacheById } = cardCache;

export default function (props, id) {
  let url = null;
  let data = {};
  let appcode = props.getSearchParam("c");
  switch (id) {
    case headButton.Add:
      let pagecode = getDefData('sessionTradeType', dataSource);
      if (!pagecode) {
        pagecode = this.getPagecode();
      }
      //缓存中的交易类型和链接中交易类型不一样的话，重新加载模板
      if(pagecode !=this.getPagecode()){
				props.setUrlParam({ status: 'add', pagecode: pagecode });
				initTemplate.call(this, this.props);	
			}else{
				props.setUrlParam({ status: 'add', pagecode: pagecode });
			}
      this.initAdd();
      break;

    case headButton.Edit:
      data = {
        pk_bill: this.props.getUrlParam('id'),
        billType: billType
      };
      ajax({
        url: '/nccloud/arap/initgatheringbill/edit.do',
        data: data,
        success: (res) => {
          if (res.success) {
            props.setUrlParam({ status: 'edit' })
            this.state.buttonfalg = true;
            this.props.resMetaAfterPkorgEdit();
            this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true });
            this.props.cardTable.setStatus(this.tableId, 'edit');
            this.props.form.setFormStatus(this.formId, 'edit');
            this.toggleShow()
          }
        }
      });
      break;
    case headButton.Delete:
      promptBox({
        color: 'warning',                 
        title: this.state.json['gatheringbill-000005'],                  /* 国际化处理： 删除*/
        content: this.state.json['gatheringbill-000006'],             /* 国际化处理： ​确定要删除吗？*/
        noFooter: false,                 
        noCancelBtn: false,              
        beSureBtnName: this.state.json['gatheringbill-000004'],           /* 国际化处理： 确定*/
        cancelBtnName: this.state.json['gatheringbill-000002'] ,          /* 国际化处理： 取消*/
        beSureBtnClick: this.delConfirm 
      });
      break
    case headButton.Copy:
      copyBill(this,this.getPagecode());
      // ajax({
      //   url: '/nccloud/arap/arappub/copy.do',
      //   data: {
      //     pk_bill: props.getUrlParam('id'),
      //     ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
      //     pageId: this.getPagecode(),
      //     billType: this.billType,
      //     tradeType: this.getPagecode(),
      //   },
      //   success: (res) => {
      //     if (res.data) {
      //       this.props.beforeUpdatePage();//打开开关
      //       if (res.data.head) {
      //         this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
      //       }
      //       if (res.data.body) {
			// 				this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId])
      //       }
      //     }
      //     props.setUrlParam({ status: 'add', type: 'copy' })
      //     this.state.buttonfalg = true;
      //     props.resMetaAfterPkorgEdit();
      //     this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': false });
      //     this.props.cardTable.setStatus(this.tableId, 'edit');
      //     this.props.form.setFormStatus(this.formId, 'edit');
      //     this.props.updatePage(this.formId, this.tableId);//关闭开关
      //     this.toggleShow()
      //   }
      // });
      break
    case headButton.Save:
      this.saveBill();
      break
    case headButton.Cancel:
      promptBox({
        color: 'warning',                 
        title: this.state.json['gatheringbill-000002'],                  /* 国际化处理： 取消*/
        content: this.state.json['gatheringbill-000003'],             /* 国际化处理： ​确定要取消吗？*/
        noFooter: false,                 
        noCancelBtn: false,              
        beSureBtnName: this.state.json['gatheringbill-000004'],           /* 国际化处理： 确定*/
        cancelBtnName: this.state.json['gatheringbill-000002'] ,          /* 国际化处理： 取消*/
        beSureBtnClick: this.cancel  
      });
      break
    case headButton.Pausetrans://挂起操作
      this.pause('/nccloud/arap/arappub/pause.do');
      break;
    case headButton.Cancelpause://取消挂起操作
      this.pause('/nccloud/arap/arappub/cancelpause.do');
      break;
    case bodyButton.AddLine:
      if (this.props.form.getFormItemsValue(formId, 'pk_org').value != null) {
        let rowNum = props.cardTable.getNumberOfRows(tableId);
        ajax({
          url: '/nccloud/arap/initgatheringbill/addline.do',
          data: this.props.createMasterChildData(this.getPagecode(), formId, tableId),
          success: (res) => {
            if (res.data && res.data.body) {
              this.props.cardTable.addRow(tableId, rowNum, res.data.body[tableId].rows[0].values);
            } else {
              this.props.cardTable.addRow(tableId);
            }
          }
        });
      }
      break
    case headButton.LinkDeal://联查处理情况
      ajax({
        url: '/nccloud/arap/arappub/linkdeal.do',
        data: {
          pk_bill: this.props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value,
          billType: this.billType
        },
        success: (res) => {
          if (res.success) {
            //打开处理情况模态框
            let data = res.data;
            this.Info.combinedExaminationData = data;
            this.handleCombined();
          }
        }
      });
      break;

    case headButton.LinkTerm://联查收付款协议
      let LinkTermdData = this.props.cardTable.getCheckedRows(this.tableId);
      let LinkTermPks = [];
      if (LinkTermdData.length != 0) {
        LinkTermData.forEach((val) => {
          LinkTermPks.push(val.data.values.pk_gatheritem.value);
        });
      }
      ajax({
        url: '/nccloud/arap/arappub/linkterm.do',
        data: {
          pk_bill: this.props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value,
          billType: this.billType,
          pk_items: LinkTermPks,
          moduleId: '2006'
        },
        success: (res) => {
          if (res.success) {
            //打开收付款协议模态框
            let data = res.data;
            this.Info.selectedPKS = seletedPks;
            this.Info.linkTermData = data;
            this.handleLinkTerm();
            
          }
        }
      });
      break;
    case headButton.LinkBal://联查余额表
        arapLinkReport(this.props,this.props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value,this.billType,this.props.form.getFormItemsValue(this.formId, 'objtype').value);
        break;
    case headButton.Refresh:
      ajax({
        url: '/nccloud/arap/arappub/cardRefresh.do',
        data: {
          pk_bill: this.props.getUrlParam('id'),
          pageId: this.getPagecode(),
          billType: this.billType
        },
        success: (res) => {
          if (res.data) {
            toast({ color: 'success', title: this.state.json['gatheringbill-000078']});/* 国际化处理： 刷新成功*/
            updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
            if (res.data.head) {
              this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
            }
            if (res.data.body) {
              this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
            }
          } else {
            this.props.form.EmptyAllFormValue(this.formId);
            this.props.cardTable.setTableData(this.tableId, { rows: [] });
          }
          this.toggleShow();
        },
        error: (res) => {
          this.props.form.EmptyAllFormValue(this.formId);
          this.props.cardTable.setTableData(this.tableId, { rows: [] });
          deleteCacheById(pkname, this.props.getUrlParam('id'), dataSource);
          this.toggleShow();
          let str = String(res);
          let content = str.substring(6, str.length);
          toast({ color: 'danger', content: content });
        }
      });
      break
    case bodyButton.DelLine://删除行
      delLine(this);
      //删行之后控制肩部按钮
      this.onSelected();
      break
    case bodyButton.CopyLine:
      if(copyLine(this, dataSource)){
        this.setState({
          buttonfalg:false
        },()=>{
          cardBodyControl(props,this.state.buttonfalg);
        })
      };
      break;
    case bodyButton.PasteLine:
      pasteLine(this);
      break;
    case bodyButton.PasteToEndLine://粘贴行到末尾
      pasteToEndLine(this, dataSource);
      break;

    case bodyButton.CancelLine://行取消
      this.setState({
        buttonfalg:true
      },()=>{
        cardBodyControl(props,this.state.buttonfalg);
      }) 
      //取消之后控制肩部按钮
      this.onSelected();
      break;
    case 'Print'://打印
      this.onPrint();
      break;
    case headButton.OfficalPrint://正式打印
      this.officalPrintOutput();
      break;
    case headButton.CancelPrint://取消正式打印
      this.cancelPrintOutput();
      break;
    case 'InitBuild':
      this.handlePeriodInformation();
      getTitle("close");
      break;
    case 'CancelInitBuild':
      this.handlePeriodInformation();
      getTitle("alerdyClose");
      break;
    case headButton.BillLinkQuery: //单据追溯
      this.setState({ showBillTrack: true });
      break;
    case headButton.Output://打印输出
      this.printOutput();
      break;
    case headButton.ExportData://导出
      let outbillid = props.getUrlParam('id');
      let pk_bills = [];
      pk_bills.push(outbillid);
      this.Info.selectedPKS = pk_bills;
      this.props.modal.show('exportFileModal');//不需要导出的只执行这行代码
      break;
    case headButton.AttachManage:

      let flag = props.getUrlParam('status');
      if (flag == 'add' || props.getUrlParam('copyFlag') == 'copy' || props.getUrlParam('writebackFlag') == 'redBack') {
        toast({ color: 'warning', content: this.state.json['gatheringbill-000032'] });/* 国际化处理： 请保存后再进行上传附件！*/
        return;
      }
      this.setState({
        showUploader: true,
        target: null
      })
      break;
    default:
      break
  }
}
