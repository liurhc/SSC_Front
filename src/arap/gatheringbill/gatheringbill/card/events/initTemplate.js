import { base, ajax, cacheTools, cardCache, toast, excelImportconfig } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
import tableButtonClick from './tableButtonClick';
import { tableId, formId, billType, tradeType, dataSource } from '../constants';
import { buttonVisible, getButtonsKey, getInnerButtonkey, cardBodyAndInnerButtonVisible } from '../../../../public/components/pubUtils/buttonvisible.js';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';
import {OperationColumn} from '../../../../public/components/pubUtils/arapConstant';
let { setDefData, getDefData } = cardCache;

export default function (props,callback) {
	const that = this;
	//从地址栏获取页面编码
	let appcode = this.props.getSearchParam('c');
	let pagecode = that.getPagecode();

	if (this.props.getUrlParam('srcbilltype')) {
		//处理外系统推单,改变appcode、pagecode
		dealOutToGather.call(this, props, appcode, pagecode)
	}else {
		init(that, props, appcode, pagecode,callback);
	}

}

function init(that, props, appcode, pagecode,callback) {
	let excelimportconfig = excelImportconfig(props, "arap", billType,true,"",{"appcode":appcode,"pagecode":that.getExportPageCode()});
	let tradetype = pagecode
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode,//注册按钮的id
			reqDataQueryallbtns: {
				rqUrl: '/arap/arappub/queryallbtns.do',
				rqJson: `{\n  \"pagecode\": \"${pagecode}\",\n  \"appcode\": \"${appcode}\"\n,\n  \"billtype\": \"${billType}\"\n,\n  \"tradetype\": \"${tradetype}\"\n}`,
				rqCode: 'button'
			},
			reqDataQuerypage: {
				rqUrl: '/arap/arappub/querypage.do',
                rqJson: `{\n  \"pagecode\": \"${pagecode}\",\n  \"appcode\": \"${appcode}\"\n}`,
                rqCode: 'template'
			}
		},
		function (data) {
			if (data) {
				if(!data.template[tableId]){
					return;
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(that, props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button && data.button.button) {
					let button = data.button.button;
					let pullBillInfoVOAry = data.button.pullbillinfo;
					getButtonsKey(button, that.Info.allButtonsKey);//保存所有头部和肩部按钮
					that.Info.pullBillInfoVOAry = pullBillInfoVOAry;
					props.button.setButtons(button);
					props.button.setUploadConfig("ImportData", excelimportconfig);
				}
				if(callback){
					callback();
				}
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
					if(getContext(loginContextKeys.transtype)){
						if (that.refs.tradetypeBtn) {
							that.refs.tradetypeBtn.setVisible(false); 
						}
					}
				}
			}
		}//,
		//false//请求模板不走缓存
	)
}

function modifierMeta(that, props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	
	//添加操作列
	meta[tableId].items.push({
		label: that.state.json['gatheringbill-000007'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: OperationColumn,
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let trueBtn = cardBodyAndInnerButtonVisible(that, that.state.buttonfalg);
			return props.button.createOprationButton(trueBtn, {
				area: "card_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(that, props, key, text, record, index)
			});
		}
	});

	return meta;
}





//处理外系统推收款单
export function dealOutToGather(props, appcode, pagecode) {
	let url = "";
	let data = {};
	let srcbilltype = this.props.getUrlParam('srcbilltype')
	//销售订单推收款单
	if (srcbilltype == '30') {
		url = "/nccloud/arap/gatheringbill/saleordertogather.do",
			data = {
				pk_bill: this.props.getUrlParam('csaleorderid'),
				pageId: pagecode
			}
	} else if (srcbilltype == 'FCT2') {
		url = "/nccloud/arap/gatheringbill/fct2togatherbill.do",
			data = {
				pk_bills: cacheTools.get('fct2ToF2Pks'),
				pageId: pagecode
			}
	}
	ajax({
		url: url,
		data: data,
		success: (res) => {
			if (res.data) {
				pagecode = res.data.head[this.formId].rows[0].values.pk_tradetype.value;
				this.data = res.data
				init(this, props, appcode, pagecode, this.initShow)
			} else {
				this.props.form.EmptyAllFormValue(this.formId);
				this.props.cardTable.setTableData(this.tableId, { rows: [] });
				toast({ color: 'danger', content: this.state.json['gatheringbill-000033'] });/* 国际化处理： 数据异常，请重新操作！*/
			}
		}
	})
}
