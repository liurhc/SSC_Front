import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import transferButtonClick from './transferButtonClick';
export { buttonClick, afterEvent, initTemplate, pageInfoClick, transferButtonClick };
