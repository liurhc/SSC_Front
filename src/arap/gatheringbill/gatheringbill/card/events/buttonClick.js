import { ajax, base, toast, cacheTools, createPage, print, cardCache, promptBox } from 'nc-lightapp-front';
import { headButton, bodyButton } from '../../../../public/components/pubUtils/buttonName.js';
import {
	calculateHeadMoney,
	delLine,
	copyLine,
	pasteLine,
	pasteToEndLine,
	refreshChildVO2HeadVO
} from '../../../../public/components/pubUtils/billPubUtil.js';
import { getTransferInfo } from '../../../../public/components/pubUtils/transferButtonUtil.js';
import { imageScan, imageView } from 'sscrp/rppub/components/image';
import { tableId, formId, billType, dataSource, pkname } from '../constants';
import { cardBodyControl } from '../../../../public/components/pubUtils/buttonvisible.js';
import initTemplate from './initTemplate';
import { BodyVerify, WholeVerify } from '../../../../public/components/pubUtils/arapVerifyCheck';
import arapLinkReport from '../../../../public/components/arapBillLinkReport.js';
import linkvouchar from '../../../../public/components/linkvouchar.js';
import madeBill from '../../../../public/components/madeBill.js';
import { copyBill } from '../../../../public/components/pubUtils/CopyBill/arapCopyBill1';
// import { copyBill } from '../../../../public/components/pubUtils/CopyBill/arapCopyBill';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';
import BankStatement from '../match/bankStatement'
import { RelationContract } from '../../linkcontract';
import { MatchedInfo } from '../match/matchedinfo';
import { MatchedSure } from '../match/matchedsure';

let { getDefData, setDefData, updateCache, deleteCacheById } = cardCache;

export default function (props, id) {
	let code = getContext(loginContextKeys.transtype);
	let pagecode = this.getPagecode();
	switch (id) {
		case headButton.Commit: //提交
			this.Info.tipUrl = '/nccloud/arap/arappub/commit.do';
			this.commitAndUncommit();
			break;
		case headButton.Uncommit: //收回
			this.Info.tipUrl = '/nccloud/arap/arappub/uncommit.do';
			this.commitAndUncommit();
			break;
		case headButton.LinkAprv: //审批详情
			this.setState({ showApproveDetail: true });
			break;
		case headButton.Save: //保存
			this.saveBill('/nccloud/arap/arappub/save.do');
			break;
		case 'SSO'://单点登录跟投系统 add by wby
			let BillCode = this.props.form.getFormItemsValue(this.formId, 'billno').value;
			let def12=this.props.form.getFormItemsValue(this.formId,'def12').value;
			let def1=this.props.form.getFormItemsValue(this.formId,'def1').display;
            ajax({
				url: '/nccloud/arap/HeelThrow/HeelThrowSSO.do',
				async: false,
				data: {
					billno: BillCode,
					def12:def12,
					def1:def1

				},
				success: (result) => {
					window.open(result.data)
					// window.location.href=result.data

				}
			});
		break;
		case headButton.TempSave: //暂存
			let org = this.props.form.getFormItemsValue(formId, 'pk_org').value;
			if (!org) {
				toast({
					color: 'warning',
					content: this.state.json['gatheringbill-000025']
				}); /* 国际化处理： 财务组织为空，不能操作单据！*/
				return;
			}
			this.saveBill('/nccloud/arap/arappub/tempsave.do');
			break;
		case headButton.SaveAndCommit:
			this.saveBill('/nccloud/arap/arappub/saveandcommit.do');
			break;
		case headButton.Add:
			pagecode = getDefData('sessionTradeType', dataSource);
			if (code) {
				pagecode = code;
			} else if (!pagecode) {
				pagecode = this.getPagecode();
			}
			//缓存中的交易类型和链接中交易类型不一样的话，重新加载模板
			if (pagecode != this.getPagecode()) {
				props.setUrlParam({ status: 'add', pagecode: pagecode });
				initTemplate.call(this, this.props);
			} else {
				props.setUrlParam({ status: 'add', pagecode: pagecode });
			}
			this.initAdd();
			break;
		case headButton.Edit:
			let sceneType = 0;
			let scene = props.getUrlParam('scene');
			//获取单据编号
			let djbh = this.props.form.getFormItemsValue(this.formId, 'billno').value;
			let canEdit = true;
			//来源于审批中心
			if (scene == 'approve' || scene == 'approvesce') {
				sceneType = 1;
				//判断单据是否是当前用户待审批
				ajax({
					url: '/nccloud/riart/message/list.do',
					async: false,
					data: {
						billno: djbh,
						isread: 'N'
					},
					success: (result) => {
						if (result.data) {
							if (result.data.total < 1) {
								toast({
									content: this.state.json['gatheringbill-000026'],
									color: 'warning'
								}); /* 国际化处理： 当前单据已审批，不可进行修改操作!*/
								canEdit = false;
							}
						}
					}
				});
			}
			//来源于我的作业
			if (scene == 'zycl') {
				sceneType = 2;
				//判断单据是否是当前用户待处理
				ajax({
					url: '/nccloud/ssctp/sscbd/SSCTaskHandlePendingNumAction.do',
					async: false,
					data: {
						billno: djbh
					},
					success: (result) => {
						if (result.data) {
							if (result.data.total < 1) {
								toast({
									content: this.state.json['gatheringbill-000027'],
									color: 'warning'
								}); /* 国际化处理： 当前单据已处理，不可进行修改操作!*/
								canEdit = false;
							}
						}
					}
				});
			}
			if (!canEdit) {
				return;
			}
			let editData = {
				pk_bill: this.props.getUrlParam('id'),
				billType: this.billType,
				sence: sceneType
			};
			ajax({
				url: '/nccloud/arap/arappub/edit.do',
				data: editData,
				success: (res) => {
					if (res.success) {
						props.setUrlParam({ status: 'edit' });
						this.state.buttonfalg = true;
						this.props.resMetaAfterPkorgEdit();
						this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
						this.props.cardTable.setStatus(this.tableId, 'edit');
						this.props.form.setFormStatus(this.formId, 'edit');
						this.toggleShow();
					}
				}
			});
			break;
		case headButton.Copy:
			copyBill(this, this.getPagecode());
			// ajax({
			// 	url: '/nccloud/arap/arappub/copy.do',
			// 	data: {
			// 		pk_bill: props.getUrlParam('id'),
			// 		ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
			// 		pageId: this.getPagecode(),
			// 		billType: this.billType,
			// 		tradeType: this.getPagecode()
			// 	},
			// 	success: (res) => {
			// 		if (res.data) {
			// 			this.props.beforeUpdatePage(); //打开开关
			// 			if (res.data.head) {
			// 				this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
			// 			}
			// 			if (res.data.body) {
			// 				this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
			// 			}
			// 		}
			// 		props.setUrlParam({ status: 'add', type: 'copy' });
			// 		this.state.buttonfalg = true;
			// 		props.resMetaAfterPkorgEdit();
			// 		this.props.form.setFormItemsDisabled(this.formId, { pk_org: false });
			// 		this.props.cardTable.setStatus(this.tableId, 'edit');
			// 		this.props.form.setFormStatus(this.formId, 'edit');
			// 		this.props.updatePage(this.formId, this.tableId); //关闭开关
			// 		this.toggleShow();
			// 	}
			// });
			break;

		case headButton.Delete:
			promptBox({
				color: 'warning',
				title: this.state.json['gatheringbill-000005'] /* 国际化处理： 删除*/,
				content: this.state.json['gatheringbill-000006'] /* 国际化处理： ​确定要删除吗？*/,
				noFooter: false,
				noCancelBtn: false,
				beSureBtnName: this.state.json['gatheringbill-000004'] /* 国际化处理： 确定*/,
				cancelBtnName: this.state.json['gatheringbill-000002'] /* 国际化处理： 取消*/,
				beSureBtnClick: this.delConfirm
			});
			break;
		case headButton.Cancel:
			promptBox({
				color: 'warning',
				title: this.state.json['gatheringbill-000002'] /* 国际化处理： 取消*/,
				content: this.state.json['gatheringbill-000003'] /* 国际化处理： ​确定要取消吗？*/,
				noFooter: false,
				noCancelBtn: false,
				beSureBtnName: this.state.json['gatheringbill-000004'] /* 国际化处理： 确定*/,
				cancelBtnName: this.state.json['gatheringbill-000002'] /* 国际化处理： 取消*/,
				beSureBtnClick: this.cancel
			});
			break;
		case headButton.Pausetrans: //挂起操作
			this.pause('/nccloud/arap/arappub/pause.do');
			break;
		case headButton.Cancelpause: //取消挂起操作
			this.pause('/nccloud/arap/arappub/cancelpause.do');
			break;
		//预收付
		case headButton.PrePay:
			let prePayDatas = this.props.cardTable.getCheckedRows(this.tableId);
			if (prePayDatas.length == 0) {
				toast({ color: 'warning', content: this.state.json['gatheringbill-000028'] }); /* 国际化处理： 请选择表体行!*/
				return;
			}
			let prePayDatasObj = [];
			prePayDatas.forEach((val) => {
				prePayDatasObj.push(val.data.values.pk_gatheritem.value);
			});
			let prePayDatasdata = {
				pk_items: prePayDatasObj,
				pk_bill: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
				pageId: this.getPagecode(),
				billType: this.billType
			};
			ajax({
				url: '/nccloud/arap/arappub/prepay.do',
				data: prePayDatasdata,
				success: (res) => {
					toast({ color: 'success', content: this.state.json['gatheringbill-000029'] }); /* 国际化处理： 预收付成功*/

					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}
					let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
					updateCache(pkname, this.props.getUrlParam('id'), newCardData, this.formId, dataSource);
					this.onSelected();
				}
			});
			break;
		case headButton.RedBack: //红冲操作
			let writebackData = {
				pk_bill: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
				pageId: this.getPagecode(),
				billType: this.billType,
				tradeType: this.getPagecode()
			};
			ajax({
				url: '/nccloud/arap/arappub/redback.do',
				data: writebackData,
				success: (res) => {
					this.props.beforeUpdatePage(); //打开开关
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						// this.props.cardTable.updateDataByRowId(this.tableId, res.data.body[this.tableId])
					}
					props.setUrlParam({ status: 'add', id: this.props.getUrlParam('id'), type: 'redBack' });
					this.state.buttonfalg = true;
					this.props.cardTable.setStatus(this.tableId, 'edit');
					this.props.form.setFormStatus(this.formId, 'edit');
					this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
					this.props.updatePage(this.formId, this.tableId); //关闭开关
					this.toggleShow();
					this.props.button.setButtonVisible([headButton.TempSave], false);
				}
			});
			break;
		case headButton.LinkConfer: //联查协同单据
			ajax({
				url: '/nccloud/arap/arappub/linkconfer.do',
				async: false,
				data: {
					pk_bill: this.props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value,
					billType: this.billType
				},
				success: (res) => {
					let data = res.data;
					if (data) {
						props.openTo(data.url, data.condition);
					}
				}
			});
			break;
		case headButton.LinkTbb: //联查计划预算
			ajax({
				url: '/nccloud/arap/arappub/linktbb.do',
				data: {
					pk_bill: this.props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value,
					billType: this.billType
				},
				success: (res) => {
					if (res.success) {
						this.Info.tbbLinkSourceData = res.data;
						this.setState({
							isTbbLinkshow: true
						});
					}
				}
			});
			break;
		case headButton.LinkBal: //联查余额表
			arapLinkReport(
				this.props,
				this.props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value,
				this.billType,
				this.props.form.getFormItemsValue(this.formId, 'objtype').value
			);
			break;
		case headButton.MadeBill: //制单
			let madeData = [
				{
					pk_bill: props.form.getFormItemsValue(formId, 'pk_gatherbill').value,
					billType: this.billType,
					tradeType: props.form.getFormItemsValue(formId, 'pk_tradetype').value
				}
			];
			madeBill(this, props, madeData, '', props.getSearchParam('c'));
			break;
		case headButton.LinkVouchar: //联查凭证
			linkvouchar(
				this,
				props,
				this.props.form.getAllFormValue(this.formId),
				this.props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value,
				this.props.getSearchParam('c')
			);
			break;
		case headButton.LinkDeal: //联查处理情况
			ajax({
				url: '/nccloud/arap/arappub/linkdeal.do',
				data: {
					pk_bill: this.props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value,
					billType: this.billType
				},
				success: (res) => {
					if (res.success) {
						//打开处理情况模态框
						let data = res.data;
						this.Info.combinedExaminationData = data;
						this.handleCombined();
					}
				}
			});
			break;
		case headButton.LinkSettleInfo: //联查结算信息
			ajax({
				url: '/nccloud/arap/arappub/linksettleinfo.do',
				data: {
					pk_bill: props.form.getFormItemsValue(formId, 'pk_gatherbill').value,
					billType: billType
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.openTo('/nccloud/resources/cmp/settlementmanagement/settlement/main/index.html#/card', {
							status: 'browse',
							srcid: props.form.getFormItemsValue(formId, 'pk_gatherbill').value,
							src: '2006',
							appcode: '360704SM',
							pagecode: '360704SM_C01'
						});
					}
				}
			});
			break;
		case headButton.ConnectSettleInfo: //关联结算信息
			ajax({
				url: '/nccloud/arap/arappub/validatecmp.do',
				async: false,
				data: null,
				success: (res) => {
					if (res.success) {
						pagecode = getDefData('sessionTradeType', dataSource);
						if (code) {
							pagecode = code;
						} else if (!pagecode) {
							pagecode = this.getPagecode();
						}
						props.openTo('/nccloud/resources/cmp/settlementmanagement/settlepublic/list/index.html', {
							appcode: '360704SM',
							pagecode: '360704SMP_L01',
							callbackappcode: props.getSearchParam('c'),
							callbackpagecode: pagecode,
							src: 0,
							callback: '/nccloud/resources/arap/gatheringbill/gatheringbill/main/index.html#/card'
						});
					}
				}
			});

			break;
		case headButton.LinkInformer: //到账通知
			ajax({
				url: '/nccloud/arap/arappub/linkinformer.do',
				data: {
					pk_bill: this.props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value,
					billType: this.billType
				},
				success: (res) => {
					if (res.success && res.data) {
						let length = res.data.length;
						if (length == 1) {
							props.openTo('/nccloud/resources/cmp/informer/LinkBill/card/index.html', {
								appcode: '36070AISC',
								pagecode: '36070AILLINK',
								status: 'browse',
								id: res.data
							});
						} else {
							props.openTo('/nccloud/resources/cmp/informer/LinkBill/list/index.html', {
								appcode: '36070AISC',
								pagecode: '36070AICLINK',
								status: 'browse',
								ids: res.data
							});
						}
					}
				}
			});
			break;
		case headButton.BillLinkQuery: //联查单据
			this.setState({ showBillTrack: true });
			break;
		case headButton.Refresh: //刷新
			refresh.call(this);
			break;
		//表体肩部的按钮操作
		case bodyButton.AddLine:
			if (this.props.form.getFormItemsValue(this.formId, 'pk_org').value != null) {
				let rowNum = props.cardTable.getNumberOfRows(this.tableId);
				ajax({
					url: '/nccloud/arap/gatheringbill/addline.do',
					data: this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId),
					success: (res) => {
						if (res.data) {
							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							}
							if (res.data.body) {
								this.props.cardTable.addRow(
									this.tableId,
									rowNum,
									res.data.body[this.tableId].rows[0].values
								);
							}
						}
					}
				});
			}
			break;
		case headButton.BodyVerify: //按表体核销
			BodyVerify(props, this.formId, this.tableId, 'pk_gatherbill', 'pk_gatheritem', this);
			break;
		case headButton.WholeVerify: //按整单核销
			WholeVerify(props, this.formId, this.tableId, 'pk_gatherbill', this);
			break;
		case headButton.ReceiptCheck: //影像查看pk_tradetype
			if (props.getUrlParam('status') == 'add') {
				toast({ color: 'warning', content: this.state.json['gatheringbill-000030'] }); /* 国际化处理： 单据未暂存！*/
				return;
			}
			// let pk_tradetype = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
			// let showData = props.createMasterChildData(pk_tradetype, formId, tableId);
			// let openShowbillid = props.getUrlParam('id');
			// imageView(showData, openShowbillid, pk_tradetype, 'iweb');
			var billInfoMap = {};

			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = this.props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value;
			billInfoMap.pk_billtype = this.props.form.getFormItemsValue(this.formId, 'pk_billtype').value;
			billInfoMap.pk_tradetype = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
			billInfoMap.pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
			imageView(billInfoMap, 'iweb');

			break;
		case headButton.ReceiptScan: //影像扫描
			if (props.getUrlParam('status') == 'add') {
				toast({
					color: 'warning',
					content: this.state.json['gatheringbill-000031']
				}); /* 国际化处理： 请先 <暂存> 单据再扫描影像！*/
				return;
			}
			let tradetype = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
			let allData = props.createMasterChildData(tradetype, formId, tableId);
			// let openbillid = props.getUrlParam('id');
			// imageScan(allData, openbillid, tradetype, 'iweb');

			var billInfoMap = {};
			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = allData.head.head.rows[0].values.pk_gatherbill.value;
			billInfoMap.pk_billtype = allData.head.head.rows[0].values.pk_billtype.value;
			billInfoMap.pk_tradetype = allData.head.head.rows[0].values.pk_tradetype.value;
			billInfoMap.pk_org = allData.head.head.rows[0].values.pk_org.value;

			//影像所需 FieldMap
			billInfoMap.BillType = allData.head.head.rows[0].values.pk_tradetype.value;
			billInfoMap.BillDate = allData.head.head.rows[0].values.creationtime.value;
			billInfoMap.Busi_Serial_No = allData.head.head.rows[0].values.pk_gatherbill.value;
			billInfoMap.pk_billtype = allData.head.head.rows[0].values.pk_billtype.value;
			billInfoMap.OrgNo = allData.head.head.rows[0].values.pk_org.value;
			billInfoMap.BillCode = allData.head.head.rows[0].values.billno.value;
			billInfoMap.OrgName = allData.head.head.rows[0].values.pk_org.display;
			billInfoMap.Cash = allData.head.head.rows[0].values.money.value;

			imageScan(billInfoMap, 'iweb');

			break;
		case headButton.AttachManage:
			let flag = props.getUrlParam('status');
			if (flag == 'add' || props.getUrlParam('type') == 'copy' || props.getUrlParam('type') == 'redBack') {
				toast({ color: 'warning', content: this.state.json['gatheringbill-000032'] }); /* 国际化处理： 请保存后再进行上传附件！*/
				return;
			}
			this.setState({
				showUploader: true,
				target: null
			});
			break;
		case 'Print': //打印
			this.onPrint();
			break;
		case headButton.Output: //打印输出
			this.printOutput();
			break;
		case headButton.OfficalPrint: //正式打印
			this.officalPrintOutput();
			break;
		case headButton.CancelPrint: //取消正式打印
			this.cancelPrintOutput();
			break;
		case headButton.ExportData: //导出
			let outbillid = props.getUrlParam('id');
			let pk_bills = [];
			pk_bills.push(outbillid);
			this.Info.selectedPKS = pk_bills; //传递主键数组,之前nc需要导出的加主键
			this.props.modal.show('exportFileModal'); //不需要导出的只执行这行代码

			break;

		//下面是肩部按钮
		case bodyButton.DelLine: //删除行
			delLine(this);
			//删行之后控制肩部按钮
			this.onSelected();
			break;
		case bodyButton.CopyLine: //复制行
			if (copyLine(this, dataSource)) {
				this.setState(
					{
						buttonfalg: false
					},
					() => {
						cardBodyControl(props, this.state.buttonfalg);
					}
				);
			}
			break;
		case bodyButton.PasteLine: //粘贴行
			pasteLine(this);
			break;
		case bodyButton.PasteToEndLine: //粘贴行到末尾
			pasteToEndLine(this, dataSource);
			break;
		case bodyButton.CancelLine: //行取消
			this.setState(
				{
					buttonfalg: true
				},
				() => {
					cardBodyControl(props, this.state.buttonfalg);
				}
			);
			//取消之后控制肩部按钮
			this.onSelected();
			break;
		case 'Match':
			match.call(this);
			break;
		case 'QryMatch':
			qryMatchedData.call(this);
			break;
		case 'linkcontract': // 联查合同
			var tradetype = this.props.form.getFormItemsValue(this.formId,'pk_tradetype'); // 交易类型
			let cont_code = ''; // 合同号
			let pk_contract = ''; // 合同主键
			if(tradetype.value && tradetype.value === 'F2-Cxx-07'){ // 融资合同放款收款单
				cont_code = this.props.form.getFormItemsValue(this.formId,'def3');
			    pk_contract = this.props.form.getFormItemsValue(this.formId,'def4');
			} 
			if(!cont_code.value || !pk_contract.value){
				toast({color:'warning',content:'单据上没有合同编号和合同主键，无法联查合同！'});
				return;
			}
			props.modal.show('linkcontract',{
				title: '联查合同',
				content: <RelationContract cont_code={cont_code} pk_contract={pk_contract}/>
			});
			break;
		case 'goto_link': //   保证金收款单  联查明源链接
		    let a=props.form.getFormItemsValue(this.formId, 'pk_gatherbill').value
		    let b=props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
			ajax({
				url:'/nccloud/nc/myerp/erpurl.do',
				data:{pk_bill:a ,tradeType:b }, 
				success:(result)=>{
					if(result.data.code==1){
					var arr=result.data.url
					    if(arr){
						        window.open(arr)
					    };
					}else{
						toast({color:'danger',title:result.data.msg});
					}
				},
				error:(result)=>{
				toast({color:'danger',title:result.message});
				}
			});
break;
		default:
			let transferInfo = getTransferInfo(this, id);
			if (transferInfo) {
				ajax({
					url: '/nccloud/arap/arappub/queryrelatedapp.do',
					data: {
						billType: transferInfo.src_billtype
					},
					success: (res) => {
						if (res) {
							//将业务流程放入缓存
							setDefData(
								transferInfo.src_billtype + transferInfo.transtypes[0],
								'transfer.dataSource',
								transferInfo.busitypes
							);
							let dest_tradetype = getDefData('sessionTradeType', dataSource);
							if (code) {
								dest_tradetype = code;
							} else if (!dest_tradetype) {
								dest_tradetype = this.getPagecode();
							}
							let url = '/' + transferInfo.src_billtype;
							props.pushTo(url, {
								src_appcode: res.data.appcode, //来源小应用编码
								src_tradetype: transferInfo.transtypes[0], //来源交易类型
								dest_billtype: this.billType, //目的单据类型
								dest_tradetype: dest_tradetype //目的交易类型
							});
						}
					}
				});
			}
			break;
	}
}

function refresh(){
	ajax({
		url: '/nccloud/arap/arappub/cardRefresh.do',
		data: {
			pk_bill: this.props.getUrlParam('id'),
			pageId: this.getPagecode(),
			billType: this.billType
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'success', title: this.state.json['gatheringbill-000078'] }); /* 国际化处理： 刷新成功*/
				updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
				if (res.data.head) {
					this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
				}
				if (res.data.body) {
					this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
				}
			} else {
				this.props.form.EmptyAllFormValue(this.formId);
				this.props.cardTable.setTableData(this.tableId, { rows: [] });
			}
			this.toggleShow();
		},
		error: (res) => {
			this.props.form.EmptyAllFormValue(this.formId);
			this.props.cardTable.setTableData(this.tableId, { rows: [] });
			deleteCacheById(pkname, this.props.getUrlParam('id'), dataSource);
			this.toggleShow();
			let str = String(res);
			let content = str.substring(6, str.length);
			toast({ color: 'danger', content: content });
		}
	});
}

function match() {
		showMatchSureModal.call(this,'auto');
	// showBankStatementModal.call(this);
}

function qryMatchedData() {
	showMatchedModal.call(this);
}

/* function getBankStatement(callback) {

	callback && callback({ isAutoMatch: false });
}

function showMatchModal(data) {
	if (!data.isAutoMatch) {
		showBankStatementModal.call(this, data);
	} else {
		showMatchSureModal.call(this, data);
	}
}*/

// 查看更多
function showBankStatementModal(matchflag) {

	let param ={
		headData:this.props.form.getAllFormValue(formId),
		pk_org:this.props.form.getFormItemsValue(formId,'pk_org'),	//组织
		pk_customer:this.props.form.getFormItemsValue(formId,'customer').value||'',	//客户
		amount:this.props.form.getFormItemsValue(formId,'money').value||'',	// 金额
		parent: this
	}

	this.props.modal.close('matchedsure');
	this.matchChild=null;

	this.props.modal.show('matchModal', {
		title: '银行流水',
		content: <BankStatement {...param}/>,
		leftBtnName: '确认',
		rightBtnName: '取消',
		beSureBtnClick: showMatchSureModal.bind(this,'matchedSure'),
		cancelBtnClick: () => { this.props.modal.close('matchModal') }, //取消按钮事件回调
	})
}

// 匹配确认
function showMatchSureModal(matchflag) {
	
	var selectedData = this.matchChild?this.matchChild.getChildData():null;// 弹框选中表体数据
	/**
	 * 第一次点"匹配"按钮，没有确认匹配的数据可以传到"匹配确认"弹框中。
	 * 如果是，查询更多页面点确认，弹出确认匹配框的时候，将银行流水弹框内选中的数据传到匹配确认弹框。
	 */
	if(matchflag=='auto'){ 
		selectedData = null;
	}

	let param ={
		headData:this.props.form.getAllFormValue(formId),
		pk_org:this.props.form.getFormItemsValue(formId,'pk_org').value||'',	//组织
		pk_customer:this.props.form.getFormItemsValue(formId,'customer').value||'',	//客户
		amount:this.props.form.getFormItemsValue(formId,'money').value||'',	// 金额
		machedSureData: selectedData,
		matchflag: matchflag, // 匹配标记
		parent: this
	}

	this.props.modal.close('matchModal');
	this.matchChild=null;

	this.props.modal.show('matchedsure', {
		title: '匹配确认',
		content: <MatchedSure {...param}/>,
		leftBtnName: '确认',
		rightBtnName: '查询更多',
		beSureBtnClick: updateBill.bind(this) ,
		cancelBtnClick: showBankStatementModal.bind(this,'hand'), //取消按钮事件回调
		userControl: true // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
	})
}

// 查看匹配
function showMatchedModal(data) {

	let batchno = this.props.form.getFormItemsValue(formId,'def71')||{};

	let param ={
		headData:this.props.form.getAllFormValue(formId),
		pk_org:this.props.form.getFormItemsValue(formId,'pk_org').value||'',// 组织
		batchno:batchno.value || null, // 批次号
		showSearchArea:false,	//是否显示查询区
		matchflag:'matched',	//查询已匹配
		parent: this
	}
let bank=param.headData.rows["0"].values;
let aaa=bank&&bank.bankreceiptdata&&JSON.parse(bank.bankreceiptdata.value)?"":"取消匹配";
let bbb=bank&&bank.bankreceiptdata&&JSON.parse(bank.bankreceiptdata.value);

bbb?this.props.modal.show('matchedsure', {
		title: '查询流水',
		content: <MatchedInfo {...param}/>,
		// leftBtnName: "",
		// rightBtnName: '关闭',
		// beSureBtnClick: cancelMatched.bind(this),
		// cancelBtnClick: () => { this.props.modal.close('matchedsure') }, //取消按钮事件回调
		userControl: true, // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
		 noFooter: true
	}):this.props.modal.show('matchedsure', {
		title: '查询流水',
		content: <MatchedInfo {...param}/>,
		leftBtnName: "取消匹配",
		rightBtnName: '关闭',
		beSureBtnClick: cancelMatched.bind(this),
		cancelBtnClick: () => { this.props.modal.close('matchedsure') }, //取消按钮事件回调
		userControl: true, // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
	})
}

function updateBill(){
	doUpdate.call(this,refresh.bind(this))
}

function doUpdate(callback){
	
	var selectedData = this.matchChild?this.matchChild.getChildData():null;
	if(selectedData==null || selectedData.length==0){
		toast({color:'warning', content:'请选择数据！'});
		return;
	}

	let param = {
		bankreceiptData:selectedData, // 流水信息
		amount:this.props.form.getFormItemsValue(formId,'money').value||'',	// 金额
		pk_tradetype: this.props.form.getFormItemsValue(formId,'pk_tradetype').value||'', // 交易类型
		recaccount: this.props.form.getFormItemsValue(formId,'recaccount').value||'', // 收款账户
		recdate:this.props.form.getFormItemsValue(formId,'def8').value||'', // 收款日期
		pk_gatheringbill:this.props.form.getFormItemsValue(formId,'pk_gatherbill').value,
		pk_org: this.props.form.getFormItemsValue(formId,'pk_org').value
	}

	ajax({
		url:'/nccloud/arap/gatheringbill/confirmMatchBankReceipt.do',
		data:param,
		success:(res)=>{
			this.props.modal.close('matchedsure');
			callback && callback();
		}
	})
}

// function getMatchedData(callback) {
	// ajax({
	// 	url:'',
	// 	data:{
	// 		batchno:this.props.form.getFormItemsValue(formId,'batchno').value
	// 	},
	// 	success:(res)=>{
	// 		callback && callback()
	// 	}
	// })
	// callback && callback({})
// }

function cancelMatched(){
	doCancelMatched.call(this,refresh.bind(this));
}

function doCancelMatched(callback){
	var selectedData = this.matchChild?this.matchChild.getChildData():null;
	if(selectedData==null || selectedData.length==0){
		toast({color:'warning', content:'请选择数据！'});
		return;
	}
	ajax({
		url:'/nccloud/arap/gatheringbill/cancelMatchBankReceipt.do',
		data:{
			batchno:this.props.form.getFormItemsValue(formId,'def71').value,
			billstatus:this.props.form.getFormItemsValue(formId,'billstatus').value,
			pk_gatheringbill:this.props.form.getFormItemsValue(formId,'pk_gatherbill').value
		},
		success:()=>{
			this.props.modal.close('matchedsure');
			callback && callback()
		}
	})
}
