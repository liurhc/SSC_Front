import { ajax, getBusinessInfo, promptBox, toast } from 'nc-lightapp-front';
import { currentTypeAfterFormEvents } from '../../../../public/components/pubUtils/currentTypeAfterEvent';
import { autoAddLineKeys } from '../../../../public/components/pubUtils/billPubInfo';
import {
	checknoDisplayAfterEvent,
	checktypeAfterEvent
} from '../../../../public/components/pubUtils/specialFieldAfterEvent.js';
import { getColvalues, getRowIds } from '../../../../public/components/pubUtils/billPubUtil';
import { formulamsgHint, renderData, headAfterEventRenderData, bodyAfterEventRenderData, errorDeal } from '../../../../public/components/afterEventPub/afterEventPubDeal';
import { moneyAndRateFields } from '../../../../public/components/pubUtils/specialFieldAfterEvent.js';

export default function afterEvent(props, moduleId, key, value, changedrows, i, record, g) {
	if (changedrows instanceof Array) {
		if (changedrows[0].newvalue.value == changedrows[0].oldvalue.value) {
			return;
		}
	}
	let pagecode = this.getPagecode();
	let index = 0;
	if (moduleId == this.formId) {
		index = 0;
	} else if (moduleId == this.tableId) {
		index = i;
	}

	//表头编辑后事件
	if (moduleId == this.formId) {
		let data = null
		switch (key) {
			case 'pk_org':
				if (value.value == null || value.value == '') {
					if (this.props.getUrlParam('type') === 'transfer') {
						//转单不允许清空财务组织
						promptBox({
							color: 'warning',
							title: this.state.json['gatheringbill-000021'] /* 国际化处理： 确认修改*/,
							content: this.state.json['gatheringbill-000080'] /* 国际化处理： 来源于上游的单据不允许清空财务组织！*/,
							beSureBtnName: this.state.json['gatheringbill-000004'] /* 国际化处理： 确定*/,
							noCancelBtn: true,
							beSureBtnClick: () => {
								this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
							},
							closeByClickBackDrop: false
						});
					} else {
						promptBox({
							color: 'warning',
							title: this.state.json['gatheringbill-000021'] /* 国际化处理： 确认修改*/,
							content: this.state.json['gatheringbill-000022'] /* 国际化处理： 确定​修改组织，这样会清空您录入的信息?*/,
							beSureBtnName: this.state.json['gatheringbill-000004'] /* 国际化处理： 确定*/,
							cancelBtnName: this.state.json['gatheringbill-000002'] /* 国际化处理： 取消*/,
							beSureBtnClick: () => {
								this.props.form.EmptyAllFormValue(this.formId);
								this.props.cardTable.setTableData(this.tableId, { rows: [] });
								this.initAdd(true);
							},
							cancelBtnClick: () => {
								this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
							},
							closeByClickBackDrop: false
						});
					}
				} else if (changedrows.value != null && changedrows.value != '') {
					//切换组织
					promptBox({
						color: 'warning',
						title: this.state.json['gatheringbill-000021'] /* 国际化处理： 确认修改*/,
						content: this.state.json['gatheringbill-000022'] /* 国际化处理： 确定​修改组织，这样会清空您录入的信息?*/,
						beSureBtnName: this.state.json['gatheringbill-000004'] /* 国际化处理： 确定*/,
						cancelBtnName: this.state.json['gatheringbill-000002'] /* 国际化处理： 取消*/,
						beSureBtnClick: () => {
							ajax({
								url: '/nccloud/arap/gatheringbill/cardheadafteredit.do',
								data: {
									pageId: pagecode,
									event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
									uiState: this.props.getUrlParam('status')
								},
								async: false,
								success: (res) => {
									//渲染数据
									renderData(this, res);
									//编辑公式提示
									formulamsgHint(this, res);
								},
								error: (res) => {
									errorDeal(this, res, changedrows);
								}
							});
						},
						cancelBtnClick: () => {
							this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
						},
						closeByClickBackDrop: false
					});
				} else {
					data = {
						pageId: pagecode,
						event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
						uiState: this.props.getUrlParam('status')
					}
					headFieldAfterRequest.call(this, data, key, changedrows)
				}
				break;
			case 'customer':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'payaccount', 'pk_currtype', 'customer', 'buysellflag',
						'objtype', "direction", "pk_billtype", "top_billtype"].concat(moneyAndRateFields)),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_deptid_v':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_deptid', 'pk_deptid_v', 'pk_currtype', 'customer', 'taxtype', 'buysellflag', 'objtype', "direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_psndoc':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_psndoc', 'pk_deptid', 'pk_deptid_v', 'isrefused', 'prepay', 'isdiscount', 'objtype', 'direction', 'agentreceivelocal']),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_currtype':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ["pk_org","pk_group","pk_currtype","pk_billtype","billdate","rate","grouprate","globalrate",
					"buysellflag","taxprice","local_taxprice","taxrate","occupationmny","money_bal","local_money_bal",
					"globalcrebit","globalnotax_cre","globaltax_cre","groupcrebit","groupnotax_cre","grouptax_cre",
					"local_money_cr","local_notax_cr","local_tax_cr","settlecurr","money_cr","notax_cr","quantity_cr","direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows)
				//币种事件发送完要判断汇率
				currentTypeAfterFormEvents(this.formId, props, key);
				break;
			default:
				data = {
					pageId: pagecode,
					event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
		}
	}

	//表体编辑后事件
	if (moduleId == this.tableId) {
		//票据类型
		if (key == 'checktype') {
			checktypeAfterEvent(this.props, this.tableId, key, value, i);
		}
		//非元数据字段，票据号
		if (key == 'checkno_display') {
			checknoDisplayAfterEvent(this.props, this.tableId, key, value, i);
			key = 'checkno';
		}

		ajax({
			url: '/nccloud/arap/gatheringbill/cardbodyafteredit.do',
			data: {
				rowindex: 0,
				editindex: index,
				pageId: pagecode,
				tableId:this.tableId,
				changedrows: changedrows,
				body: props.cardTable.getDataByIndex(this.tableId, index),
				formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
				uiState: this.props.getUrlParam('status')
			},
			async: false,
			success: (res) => {
				//渲染数据
				bodyAfterEventRenderData(this, res);

				if (this.props.getUrlParam('type') == 'transfer') {
					this.synTransferData();
				}
				//表体改变表头税率编辑性
				if (i == 0 && key == 'pk_currtype') {
					currentTypeAfterFormEvents(this.formId, props, 'pk_currtype');
				}
				//编辑公式提示
				formulamsgHint(this, res);
			},
			error: (res) => {
				let str = String(res);
				let content = str.substring(6, str.length);
				if (content.substring(1, 17) == 'convertException') {
					promptBox({
						color: 'warning',
						title: this.state.json['gatheringbill-000000'] /* 国际化处理： 折算误差*/,
						content: content.substring(17, content.length),
						closeByClickBackDrop: false,
						beSureBtnClick: () => {
							afterTableEvent(this, props, i, pagecode, moduleId, key, changedrows, 'sure', index, value);
						},
						cancelBtnClick: () => {
							afterTableEvent(this, props, i, pagecode, moduleId, key, changedrows, 'cancel', index, value);
						}
					});
				} else {
					this.props.cardTable.setValByKeyAndRowId(this.tableId, i, key, changedrows);
					toast({ color: 'danger', content: content });
				}
			}
		});

		//自动增行
		autoAddline.call(this, moduleId, pagecode, key, i)
	}


}

function afterTableEvent(that, props, i, pagecode, moduleId, key, changedrows, isCalculateConvert, index, value) {
	ajax({
		url: '/nccloud/arap/gatheringbill/cardbodyafteredit.do',
		data: {
			rowindex: 0,
			editindex: index,
			pageId: pagecode,
			changedrows: changedrows,
			tableId:that.tableId,
			body: props.cardTable.getDataByIndex(that.tableId, index),
			formEvent: props.createFormAfterEventData(pagecode, that.formId, that.tableId, key, value),
			uiState: that.props.getUrlParam('status'),
			isCalculateConvert: isCalculateConvert
		},
		async: false,
		success: (res) => {
			//渲染数据
			bodyAfterEventRenderData(that, res);
			if (that.props.getUrlParam('type') == 'transfer') {
				that.synTransferData();
			}
			//表体改变表头税率编辑性
			if (i == 0 && key == 'pk_currtype') {
				currentTypeAfterFormEvents(that.formId, props, 'pk_currtype');
			}
			//编辑公式提示
			formulamsgHint(that, res);

		}
	});
}

export function headFieldAfterRequest(requestData, key, changedrows) {
	ajax({
		url: '/nccloud/arap/gatheringbill/cardheadafteredit.do',
		data: requestData,
		async: false,
		success: (res) => {
			try {//银行流水挂账F2跳转---F2-Cxx-01非房款收款单---F2-Cxx-02跟投款收款单---F2-Cxx-06其他收款单
				if(localStorage.getItem('yhlsgz')=="bianji"){
					var record=JSON.parse(localStorage.getItem('record'));
						       var  heads=res.data.head[this.formId].rows["0"].values;
								var  tables=res.data.body[this.tableId].rows["0"].values
								//tables.top_billid=record.values.m_pk_bankreceipt;//F2表体top_billid--后端要求
								heads.def77 =record.values.m_pk_bankreceipt;//F2表体pk_upbill后端要求
								heads.recaccount=record.values.m_pk_bank;//银行账户-收款银行账号
								heads.payaccount=record.values.otheraccount;//银行账户-收款银行账号
								record.values.num&&record.values.num.value&&(heads.num=record.values.num);//挂账凭证号
                                heads.oppunitname =record.values.oppunitname ;//对方单位
								heads.accounting_status=record.values.accounting_status;//挂账状态
								tables.payaccount=record.values.otheraccount;//银行账户-付款银行账号
								tables.recaccount=record.values.m_pk_bank;//银行账户-付款银行账号
						        heads.customer=tables.customer=record.values.supplier;//对方单位
								heads.pk_balatype=record.values.pk_balatype2;    //结算方式
								tables.pk_balatype=record.values.pk_balatype2;  //结算方式
							    heads.local_money=record.values.m_debitamount;//原币借发生额
								heads.def10=record.values.m_debitamount;//流水金额
								heads.local_money.scale=heads.def10.scale="2"
							    record.values.pk_currtype&&record.values.pk_currtype.value&&(heads.pk_currtype=record.values.pk_currtype)&&(tables.pk_currtype=record.values.pk_currtype);//币种
								heads.def8=record.values.checkdate;//票据日期
								heads.rate=tables.rate=record.values.rate
								if(record.values.explanation&&record.values.explanation.value){
									record.values.explanation.display=record.values.explanation.value;
									heads.scomment=record.values.explanation;//摘要
									tables.scomment=record.values.explanation;//摘要
								}
								tables.notax_cr=tables.money_cr=record.values.m_debitamount;//原币借发生额-收款 金额
								tables.local_notax_cr=tables.local_money_cr={value:parseFloat(record.values.m_debitamount.value)*parseFloat(record.values.rate.value)};//组织本币金额贷方
								tables.notax_cr.scale=tables.local_notax_cr.scale=tables.local_money_cr.scale=tables.money_cr.scale="2";//原币借发生额-收款 金额
								if(heads.pk_tradetype.value=="F2-Cxx-06"){
									heads.customer={ value:"1001A110000000986083", display:"共享虚拟客商"};
                                    tables.customer={ value:"1001A110000000986083", display:"共享虚拟客商"};
                                    tables.payaccount={ value:"1001A110000000AJYDVV", display:""}
									heads.def27=record.values.explanation;
								}
					localStorage.removeItem("yhlsgz");
				}
			}
			catch(err) {
			debugger
			console.log("不需要重置")
			}

			//渲染数据
			headAfterEventRenderData(this, res);

			if (key == 'pk_org') {
				let pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
				if (pk_org) {
					this.props.resMetaAfterPkorgEdit();
					this.state.buttonfalg = true;
				} else {
					this.state.buttonfalg = null;
				}
				this.toggleShow();
			}
			if (this.props.getUrlParam('type') == 'transfer') {
				this.synTransferData();
			}
			//编辑公式提示
			formulamsgHint(this, res);

		},
		error: (res) => {
			errorDeal(this, res, changedrows);
		}
	});
}

export function autoAddline(moduleId, pagecode, key, i) {
	//自动增行
	let allRowsNumber = this.props.cardTable.getNumberOfRows(this.tableId);
	if (moduleId == this.tableId && allRowsNumber == i + 1 && autoAddLineKeys.indexOf(key) != -1) {
		let data = this.props.createMasterChildData(pagecode, this.formId, this.tableId);
		//清空cardData的表体
		data.body[this.tableId].rows = [];
		ajax({
			url: '/nccloud/arap/gatheringbill/addline.do',
			data: data,
			async: false,
			success: (res) => {
				if (res.data) {
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.addRow(this.tableId, allRowsNumber, res.data.body[this.tableId].rows[0].values, false);
					}
				}
			}
		});
	}
}