import { ajax, cardCache } from 'nc-lightapp-front';
import { formId, billType, dataSource, pkname } from '../constants';
import initTemplate from './initTemplate';
let { getCacheById, updateCache, setDefData } = cardCache;

export default function (props, pk) {
    if(!pk){//如果刷新了浏览器，那么pk将不会存在，如果pk不存在，return
        return;
    }
    let cardData = getCacheById(pk, dataSource);
    let tradeType = this.getPagecode();
    if (cardData) {
        if(!props.getUrlParam("scene")){
            let pagecode = cardData.head[this.formId].rows[0].values.pk_tradetype.value;
            props.setUrlParam({id:pk,pagecode:pagecode})
            //点击上一张下一张单据时，要将单据的交易类型放入缓存（和切换交易类型一致），下次点击新增自制时，取缓存中的交易类型
            setDefData('sessionTradeType', dataSource, pagecode);
            if(tradeType !=pagecode){
                initTemplate.call(this, this.props);   
            }
        } else{
            props.setUrlParam({id:pk})
        }
        props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
        props.cardTable.setTableData(this.tableId, cardData.body[this.tableId],null, null, true);
        this.toggleShow();

    } else {
        let data = {
            pk_bill: pk
        };
        ajax({
            url: '/nccloud/arap/gatheringbill/cardquery.do',
            data: data,
            success: (res) => {
                if (res.data) {
                    if (res.data.head) {
                        this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                    }
                    if (res.data.body) {
                        this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId],null, null, true);
                    }
                    updateCache(pkname, pk, res.data, this.formId, dataSource);
                    if(!props.getUrlParam("scene")){
                        let pagecode = res.data.head[formId].rows[0].values.pk_tradetype.value;
                        props.setUrlParam({id:pk,pagecode:pagecode})
                        setDefData('sessionTradeType', dataSource, pagecode);
                        if(tradeType !=pagecode){
                            initTemplate.call(this, this.props);   
                        } 
                    }else{
                        props.setUrlParam({id:pk})
                    } 
                    this.toggleShow();
                } else {
                    this.props.form.EmptyAllFormValue(this.formId);
                    this.props.cardTable.setTableData(this.tableId, { rows: [] });
                }
            }
        });
    }
}
