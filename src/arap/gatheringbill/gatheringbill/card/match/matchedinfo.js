import { Component } from 'react';
import { createPage,ajax } from 'nc-lightapp-front';

/**
 * 查看匹配弹框
 */
export class MatchedInfo extends Component {

    constructor(props) {
        super(props);
        this.searchId = 'SearchArea';
        this.fomrId = 'BillForm';
        this.tableId = 'BankStatementTable';
        this.initTemplate(props);
    }

    initTemplate(props) {
        props.createUIDom(
            {
                pagecode: '20060GBM001400_BankStatement' //页面id
            },
            (data) => {
                if (data) {
                    if (data.template) {
                        let meta = data.template;
                        props.meta.setMeta(meta, this.initData());
                    }
                }
            }
        )
    }

    componentDidMount(){
        // 将弹框子组件传给父组件
        this.props.parent.matchChild = this;
    }

    // 获取弹框内选中数据
    getChildData(){
        let bodyData = this.props.table.getCheckedRows(this.tableId);
        return bodyData;
    }

    initData = () => {
        let param = {
            matchflag:'matched',
            pk_org:this.props.pk_org,
            pk_customer:this.props.pk_customer,
            amount:this.props.amount,
            batchno:this.props.batchno
        }
        this.getData(param, (data) => { this.updatePage(data) });
    }

    onSearch = (data) => {
        this.getData(null, (data) => { this.updatePage(data) });
    }

    getData = (param, callback) => {
        let bank= this.props.headData.rows["0"].values;
        let bankreceiptdata= bank&&bank.bankreceiptdata&&JSON.parse(bank.bankreceiptdata.value);

        if(bankreceiptdata&&bankreceiptdata.pageid){
            callback(bankreceiptdata) ;
        }else{
            let url = '/nccloud/arap/gatheringbill/checkMatchBankReceipt.do';
            ajax({
                url: url,
                data: param,
                success: (res) => {
                    callback && callback(res.data);
                }
            })
        }
    }

    updatePage = (data) => {

        let tableData = (data && data[this.tableId]) || { rows: [] };

        this.props.form.setAllFormValue({ [this.fomrId]: this.props.headData});
        this.props.table.setAllTableData(this.tableId, tableData);
    }

    onSelected =(props, moduleId, record, index, status)=>{
        
    }

    render() {
        let { table, search, form } = this.props;
        let { createSimpleTable } = table;
        let { createForm } = form;
        let { NCCreateSearch } = search;
        return (
            <div className="nc-bill-card">
                <div className="nc-bill-form-area">
                    {createForm(this.fomrId, {})}
                </div>
                <div className="nc-bill-card-area">
                    {createSimpleTable(this.tableId, {
                        showIndex: true,
                        showCheck: true,
                        useFixedHeader:true,              
                        onSelected: this.onSelected.bind(this)
                    })}
                </div>
            </div>
        )
    }
}

MatchedInfo = createPage({
    billinfo:{
        billtype:'card',
        pagecode: '20060GBM001400_BankStatement'
    },
    initTemplate:()=>{}
})(MatchedInfo);
