import { Component } from 'react';
import { createPage,ajax,toast } from 'nc-lightapp-front';

/**
 * 匹配确认弹框
 */
export class MatchedSure extends Component {

    constructor(props) {
        super(props);
        this.searchId = 'SearchArea';
        this.fomrId = 'BillForm';
        this.tableId = 'BankStatementTable';
        this.initTemplate(props);
    }

    initTemplate(props) {
        props.createUIDom(
            {
                pagecode: '20060GBM001400_BankStatement' //页面id
            },
            (data) => {
                if (data) {
                    if (data.template) {
                        let meta = data.template;
                        props.meta.setMeta(meta, this.initData());
                    }
                }
            }
        )
    }

    componentDidMount(){
        // 将弹框子组件传给父组件
        this.props.parent.matchChild = this;
    }

    // 获取弹框内选中数据
    getChildData(){
        let bodyData = this.props.table.getCheckedRows(this.tableId);
        return bodyData;
    }

    initData = () => {
        let matchflag = this.props.matchflag;
        let param = {
            matchflag:matchflag=='matchedSure'?'hand':'auto', // 匹配标志位"确认匹配",属于手动匹配操作
            pk_org:this.props.pk_org,
            pk_customer:this.props.pk_customer,
            amount:this.props.amount,
            pk_bank:this.props.headData.rows[0].values.recaccount.value,
            batchno:this.props.batchno
        }
        this.getData(param, (data) => { this.updatePage(data) });
    }

    getData = (param, callback) => {

        let url = '/nccloud/arap/gatheringbill/match.do';

        // 匹配确认状态不需要再走一遍后台查询
        if (param.matchflag === 'hand') {

            // 表格设置数据时，格式调整
            let newTableData = [];
            this.props.machedSureData.forEach((item)=>{
                newTableData.push(item.data);
            });

            let data = {[this.tableId]:{'rows':newTableData}};
            data && callback && callback(data);
            return;
        }

        ajax({
            url: url,
            data: param,
            success: (res) => {
                if(!res.data && param.matchflag === 'auto'){
                    toast({color:'warning', content:'系统未找到自动匹配数据，请手动匹配！'});
                }
                callback && callback(res.data);
            }
        })
    }

    updatePage = (data) => {

        let tableData = (data && data[this.tableId]) || { rows: [] };

        this.props.form.setAllFormValue({ [this.fomrId]: this.props.headData});
        this.props.table.setAllTableData(this.tableId, tableData);
        // 查询数据为一行时，默认勾选
        if(tableData.rows.length==1){
            this.props.table.selectAllRows(this.tableId,true);
        }
    }

    onSelected =(props, moduleId, record, index, status)=>{
        
    }

    render() {
        let { table, form } = this.props;
        let { createSimpleTable } = table;
        let { createForm } = form;
        return (
            <div className="nc-bill-card">
                <div className="nc-bill-form-area">
                    {createForm(this.fomrId, {})}
                </div>
                <div className="nc-bill-card-area">
                    {createSimpleTable(this.tableId, {
                        showIndex: true,
                        showCheck: true,
                        useFixedHeader:true,                    
                        onSelected: this.onSelected.bind(this)
                    })}
                </div>
            </div>
        )
    }
}

MatchedSure = createPage({
    billinfo:{
        billtype:'card',
        pagecode: '20060GBM001400_BankStatement'
    },
    initTemplate:()=>{}
})(MatchedSure);

