import { Component } from 'react';
import { createPage,ajax } from 'nc-lightapp-front';

/**
 * 银行流水弹框-用于流水，查询更多查询操作
 */
class comp extends Component {

    constructor(props) {
        debugger;
        super(props);
        this.searchId = 'SearchArea';
        this.fomrId = 'BillForm';
        this.tableId = 'BankStatementTable';
        this.initTemplate(props);
    }

    initTemplate(props) {
        props.createUIDom(
            {
                pagecode: '20060GBM001400_BankStatement' //页面编码
            },
            (data) => {
                if (data) {
                    if (data.template) {
                        let meta = data.template;
                        props.meta.setMeta(meta, this.initData());
                    }
                }
            }
        )
    }

    componentDidMount(){
        // 将弹框子组件传给父组件
        this.props.parent.matchChild = this;
    }

    // 获取弹框内表体选中数据
    getChildData(){
        let bodyData = this.props.table.getCheckedRows(this.tableId);
        return bodyData;
    }

    //查询区渲染完回调函数
    renderCompleteEvent(){
        //每次清空查询条件
        this.props.search.clearSearchArea(this.searchId);
        debugger;
        // 财务组织设置默认值
        this.props.search.setSearchValByField(this.searchId,'pk_org',this.props.pk_org);
        //银行账户设置默认值
        this.props.search.setSearchValByField(this.searchId,'pk_bank',this.props.headData.rows[0].values.recaccount);
    }

    initData = () => {
        let param = {
            matchflag:'hand', // 手动匹配
            pk_org:this.props.pk_org.value,
            pk_customer:this.props.pk_customer,
            pk_bank:this.props.headData.rows[0].values.recaccount.value,
            amount:this.props.amount,
            batchno:this.props.batchno
        }
        this.getData(param, (data) => { this.updatePage(data) });
    }

    onSearch = (data) => {
        this.getData(null, (data) => { this.updatePage(data) });
    }

    getData = (param, callback) => {

        let url = '/nccloud/arap/gatheringbill/match.do';

        if (!param){
            let data = this.props.search.getAllSearchData(this.searchId);
            param = {
                querycondition: data==null?{}:data,
                userdefObj:{pk_org:this.props.form.getFormItemsValue(this.fomrId,'pk_org').value||''},
                pagecode: this.pagecode,
                queryAreaCode:this.searchId,  //查询区编码
                oid:this.props.meta.getMeta()[this.searchId].oid,//查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
                querytype:'tree'
            }
            url = '/nccloud/arap/gatheringbill/queryBankReceiptByCondition.do';
        }

        ajax({
            url: url,
            data: param,
            success: (res) => {
                callback && callback(res.data);
            }
        })
    }

    updatePage = (data) => {

        let tableData = (data && data[this.tableId]) || { rows: [] };

        this.props.form.setAllFormValue({ [this.fomrId]: this.props.headData});
        this.props.table.setAllTableData(this.tableId, tableData);
    }

    onSelected =(props, moduleId, record, index, status)=>{
        
    }

    render() {
        let { table, search, form } = this.props;
        let { createSimpleTable } = table;
        let { createForm } = form;
        let { NCCreateSearch } = search;
        return (
            <div className="nc-bill-card">
                <div className="nc-bill-search-area">
                    {NCCreateSearch(this.searchId, {
                        clickSearchBtn: this.onSearch.bind(this),
                        renderCompleteEvent: this.renderCompleteEvent.bind(this), // 查询区渲染完成回调函数 
                    })}
                </div>
                <div className="nc-bill-form-area">
                    {createForm(this.fomrId, {})}
                </div>
                <div className="nc-bill-card-area">
                    {createSimpleTable(this.tableId, {
                        showIndex: true,
                        showCheck: true,
                        useFixedHeader:true,                    
                        onSelected: this.onSelected.bind(this)
                    })}
                </div>
            </div>
        )
    }
}

let BankStatement = createPage({})(comp);

export default BankStatement;

