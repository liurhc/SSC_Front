import { asyncComponent } from 'nc-lightapp-front';
import List from '../list';

//收款单卡片
const card = asyncComponent(() => import(/*webpackChunkName:"/arap/gatheringbill/gatheringbill/card/card"*/  /* webpackMode: "eager" */ '../card'));
//预核销
const prev = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/preverify/list/prev"*/ /* webpackMode: "eager" */ '../../../verificationsheet/preverify/list'));
//及时核销
const nowv = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/verifynow/list/nowv"*/ /* webpackMode: "eager" */ '../../../verificationsheet/verifynow/list'));
//及时核销card
const nowvcard = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/verifynow/nowvcard"*/ /* webpackMode: "eager" */ '../../../verificationsheet/verifynow/nowvcard'));
//应收单转单,转单组件别名取单据类型
const F0 = asyncComponent(() => import(/*webpackChunkName:"/arap/receivablebill/recbill/transfer/f0transfer"*/ /* webpackMode: "eager" */ '../../../receivablebill/recbill/transfer'));
//付款单转单
const F3 = asyncComponent(() => import(/*webpackChunkName:"/arap/paybill/paybill/transfer/f3transfer"*/ /* webpackMode: "eager" */ '../../../paybill/paybill/transfer'));
//收款合同转单
const FCT2 = asyncComponent(() => import(/*webpackChunkName:"/fct/bill/ar/transfer/fct2transfer"*/ /* webpackMode: "eager" */ '../../../../fct/bill/ar/transfer'));

const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: card
	},
	{
		path: '/prev',
		component: prev
	},
	{
		path: '/nowv',
		component: nowv
	},
	{
		path: '/nowvcard',
		component: nowvcard
	},
	{
		path: '/F0',
		component: F0
	},
	{
		path: '/F3',
		component: F3
	},
	{
		path: '/FCT2',
		component: FCT2
	},
];

export default routes;


