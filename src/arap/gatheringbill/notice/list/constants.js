
/**
 * 查询区域
 */
export const searchId = 'query';

/**
 * 列表区域
 */
export const tableId = 'list';

/**
 * 页面编码
 */
export const pageId = '20060CUFT_LIST';

/**
* 单页应用缓存,命名规范为："领域名.模块名.节点名.自定义名"。 
*/
export const dataSource = 'fi.arap.notice.20060CUFT';


/**
 * 单页应用缓存主键名字
 */
export const pkname  = 'pk_notice';

