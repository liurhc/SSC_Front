//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,cardCache,getMultiLang, createPageIcon} from 'nc-lightapp-front';
let { NCTabsControl } = base;
import { buttonClick, initTemplate, searchBtnClick, pageInfoClick, tableModelConfirm, doubleClick, tableButtonClick } from './events';
import { tableId, searchId, pkname, dataSource } from './constants';
let { setDefData, getDefData } = cardCache;

/**
 * 应收管理，催款语气
 */
class List extends Component {
	constructor(props) {
		super(props);
		this.searchId = searchId;
		this.tableId = tableId;
		this.pageId = props.getSearchParam('p');
		this.state = {
			json:{}
		}
	}
	componentDidMount() {
		
		let callback = (json) => {
			this.setState({json:json},() => {
				initTemplate.call(this, this.props);
				if (!this.props.table.hasCacheData(dataSource)) { 
				}
				this.props.button.setButtonDisabled(['Delete'], true);
			});
		}
		getMultiLang({moduleId:['gatheringbill','public'],domainName :'arap',currentLocale:'simpchn',callback});
	}

	//选中行
	onSelected = (props, moduleId, record, index, status) => {
		if (status) {
			props.button.setButtonDisabled(['Delete'], false)
		} else {
			if (props.table.getCheckedRows(moduleId).length == 0) {
				props.button.setButtonDisabled(['Delete'], true)
			}
		}
	}

	//全选中
	onSelectedAll = (props, moduleId, status, length) => {
		if (status) {
			props.button.setButtonDisabled(['Delete'], false)
		} else {
			if (props.table.getCheckedRows(moduleId).length == 0) {
				props.button.setButtonDisabled(['Delete'], true)
			}
		}
	}

	render() {
		let { table, button, search } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		let { createButtonApp } = this.props.button;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{this.state.json['gatheringbill-000065']}</h2>{/* 国际化处理： 催款语气*/}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
				{NCCreateSearch(
						searchId,//模块id
						{clickSearchBtn: searchBtnClick.bind(this),//   点击按钮事件
							showAdvBtn: true,                           //  显示高级按钮
						}
					)}
				</div>
				<div className="tab-definInfo-area">

				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						dataSource: dataSource,
						pkname: pkname,
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						onSelected: this.onSelected.bind(this),
						onSelectedAll: this.onSelectedAll.bind(this),
						showCheck: true,
						showIndex: true,
						onRowDoubleClick: doubleClick.bind(this)
					})}
				</div>
			</div>
		);
	}
}

List = createPage({
	//initTemplate: initTemplate,
})(List);

export default List;
