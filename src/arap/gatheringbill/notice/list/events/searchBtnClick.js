import { ajax, toast, cardCache } from 'nc-lightapp-front';
import { tableId, searchId, dataSource } from '../constants';
let { setDefData, getDefData } = cardCache;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
    if (searchVal) {
        let pageInfo = props.table.getTablePageInfo(tableId);
        let queryInfo = this.props.search.getQueryInfo(searchId);
        queryInfo.pageInfo = pageInfo;
        let data = {
            pageId: props.getSearchParam("p"),
            queryInfo: queryInfo
        };
        setDefData(searchId, dataSource, data);//放入缓存
        ajax({
            url: '/nccloud/arap/notice/serch.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if (data) {
                        props.table.setAllTableData(tableId, data[tableId]);
                    } else {
                        toast({ color: 'success', content: this.state.json['gatheringbill-000011'] });/* 国际化处理： 未查询到数据*/
                        this.props.table.setAllTableData(tableId, { rows: [] });
                    }
                    setDefData(this.tableId, dataSource, data);//放入缓存
                }
            }
        });
    }

};
