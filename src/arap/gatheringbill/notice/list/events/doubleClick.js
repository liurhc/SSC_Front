import { tableId, searchId, pageId, oid } from '../constants';

export default function doubleClick(record, index, e) {
    this.props.pushTo('/card', {
        status: 'browse',
        id: record.pk_notice.value,
        pagecode:'20060CUFT_CARD'
    })
}
