import { ajax } from 'nc-lightapp-front';
import { tableId, searchId } from '../constants';

export default function (props, config, pks) {
    let pageInfo = props.table.getTablePageInfo(tableId);
    let searchVal = props.search.getAllSearchData(searchId);
    // 后台还没更新，暂不可用
    let data = {
        pk_bills: pks,
        pageId: props.getSearchParam("p")
    };
    let that = this;
    ajax({
        url: '/nccloud/arap/notice/querygridbypks.do',
        data: data,
        success: function (res) {
            let { success, data } = res;
            if (success) {
                if (data) {
                    props.table.setAllTableData(tableId, data[tableId]);
                } else {
                    props.table.setAllTableData(tableId, { rows: [] });
                }
            }
        }
    });
}
