import { ajax, base, toast, promptBox, cardCache } from 'nc-lightapp-front';
import { tableId, searchId, pageId, oid, dataSource } from '../constants';
let { setDefData, getDefData } = cardCache;

export default function buttonClick(props, id) {
    switch (id) {
        case 'Add':
            props.pushTo('/card', {
                status: 'add',
                pagecode: '20060CUFT_CARD'
            })
            break;
        case 'Delete':
            const selectedData = this.props.table.getCheckedRows(tableId);
            if (selectedData.length == 0) {
                return;
            }
            promptBox({
                color: 'warning',
                title: this.state.json['gatheringbill-000005'],                  /* 国际化处理： 删除*/
                content: this.state.json['gatheringbill-000006'],             /* 国际化处理： ​确定要删除吗？*/
                noFooter: false,
                noCancelBtn: false,
                beSureBtnName: this.state.json['gatheringbill-000004'],           /* 国际化处理： 确定*/
                cancelBtnName: this.state.json['gatheringbill-000002'],          /* 国际化处理： 取消*/
                beSureBtnClick: () => {
                    let obj = [];
                    let pks = []
                    let deleteIndexs = [];
                    selectedData.forEach((val) => {
                        let data = {
                            pk_bill: val.data.values.pk_notice.value,
                            ts: val.data.values.ts.value
                        };
                        pks.push(val.data.values.pk_notice.value)
                        obj.push(data);
                        deleteIndexs.push(val.index);
                    });
                    ajax({
                        url: '/nccloud/arap/notice/delete.do',
                        data: obj,
                        success: (res) => {
                            if (res.success) {
                                toast({ color: 'success', content: this.state.json['gatheringbill-000017'] });/* 国际化处理： 删除成功*/
                                props.table.deleteTableRowsByIndex(tableId, deleteIndexs);
                                props.table.deleteCacheId(this.tableId, pks);
                                if (props.table.getCheckedRows(tableId).length == 0) {
                                    props.button.setButtonDisabled(['Delete'], true)
                                }
                            }
                        }
                    });
                }
            });
            break;
        case 'Refresh': 
			let data = getDefData(searchId, dataSource);
			if (data) {
				ajax({
                    url: '/nccloud/arap/notice/serch.do',
                    data: data,
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            if (data) {
                                toast({ color: 'success', title: this.state.json['gatheringbill-000078'] });/* 国际化处理： 刷新成功*/
                                props.table.setAllTableData(tableId, data[tableId]);
                            } else {
                                toast({ color: 'success', content: this.state.json['gatheringbill-000011'] });/* 国际化处理： 未查询到数据*/
                                this.props.table.setAllTableData(tableId, { rows: [] });
                            }
                            setDefData(this.tableId, dataSource, data);//放入缓存
                        }
                    }
                });
			} else {
				toast({ color: 'success', title: this.state.json['gatheringbill-000078'] });/* 国际化处理： 刷新成功*/
			}
			break;
    }
}
