import { createPage, ajax, base, toast } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;
import tableButtonClick from './tableButtonClick.js';
import { tableId, searchId, pageId } from '../constants';
import { modifierSearchMetas } from '../../../../public/components/pubUtils/arapListSearchRefFilter';
import { loginContext } from '../../../../public/components/arapInitInfo/loginContext';

export default function (props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: props.getSearchParam("c"),//注册按钮的id
			reqDataQuerypage: {
				rqUrl: '/arap/arappub/querypage.do',
                rqJson: `{\n  \"pagecode\": \"${pageId}\",\n  \"appcode\": \"${props.getSearchParam("c")}\"\n}`,
                rqCode: 'template'
			}
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta, that);
					modifierSearchMetas(searchId, props, meta, 'F2', null, that);
					
					if (data.context) {
						// 初始化上下文变量
						//遍历查询区域字段，将默认业务单元赋值给组织字段
						meta[searchId].items.map((item) => {
							if (item.attrcode == 'pk_org') {
								item.initialvalue = { 'display': data.context.org_Name, 'value': data.context.pk_org }
							}
						});
					}
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('Delete_inner', '确定要删除吗？');/*删除信息提示框*/
				}
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
			}
		}
	)
}

function modifierMeta(props, meta, that) {
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		item.visible = true;
		// item.col = '3';
		if (item.attrcode == 'pk_org' || item.attrcode == 'pk_org_v') {
			item.isMultiSelectedEnabled = true
		}
		return item;
	})

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'code') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							props.pushTo('/card', {
								status: 'browse',
								id: record.pk_notice.value,
								pagecode: '20060CUFT_CARD'
							});
						}}
					>
						{record && record.code && record.code.value}
					</a>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableId].items.push({
		label: that.state.json['gatheringbill-000007'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: '160px',
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let buttonAry = ["Edit_inner", "Delete_inner"];
			return props.button.createOprationButton(buttonAry, {
				area: "list_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(that, props, key, text, record, index)
			});
		}
	});
	return meta;
}
