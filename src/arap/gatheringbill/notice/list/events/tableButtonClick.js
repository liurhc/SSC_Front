import { ajax, base, toast } from 'nc-lightapp-front';
import { tableId, searchId, dataSource } from '../constants';

export default function (that, props, key, text, record, index) {
    switch (key) {
        // 表格操修改
        case 'Edit_inner':
            props.pushTo('/card', {
                status: 'edit',
                id: record.pk_notice.value,
                pagecode: '20060CUFT_CARD',
            })
            break;
        case 'Delete_inner':
            ajax({
                url: '/nccloud/arap/notice/delete.do',
                data: [{
                    pk_bill: record.pk_notice.value,
                    ts: record.ts.value
                }],
                success: (res) => {
                    if (res.success) {
                        toast({ color: 'success', content: that.state.json['gatheringbill-000017'] });/* 国际化处理： 删除成功*/
                        //删除当前行数据
                        props.table.deleteTableRowsByIndex(tableId, index);
                        //删除缓存数据
                        props.table.deleteCacheId(tableId, record.pk_notice.value);
                    }
                }
            });
            break;
        default:
            break;
    }
};
