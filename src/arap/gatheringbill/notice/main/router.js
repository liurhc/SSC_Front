import { asyncComponent } from 'nc-lightapp-front';
import List from '../list';

//催款语气卡片
const card = asyncComponent(() => import(/*webpackChunkName:"/arap/gatheringbill/notice/card/card"*/  /* webpackMode: "eager" */ '../card'));

const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: card
	}

];

export default routes;


