//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, cardCache, getMultiLang, createPageIcon } from 'nc-lightapp-front';
let { NCFormControl, NCBackBtn } = base;
import { buttonClick, initTemplate, afterEvent, tableButtonClick } from './events';
import { tableId, formId, dataSource, pkname } from './constants';
import { formBeforeEvent } from '../../../public/components/pubUtils/arapFormRefFilter';
import { loginContext, getContext, loginContextKeys } from '../../../public/components/arapInitInfo/loginContext';
let { setDefData, getDefData, addCache, getNextId, deleteCacheById, getCacheById, updateCache, getCurrentLastId } = cardCache;


class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = formId;
		this.tableId = tableId;
		this.state = {
			backBtn: false,
			json: {}
		}
	}

	//关闭、刷新弹窗时
	componentWillMount() {

		let callback = (json) => {
			this.setState({ json: json }, () => {
				window.onbeforeunload = () => {
					let status = this.props.getUrlParam('status');
					if (status == 'edit' || status == 'add') {
						return this.state.json['gatheringbill-000039'];/* 国际化处理： 确定要离开吗？*/
					}
				};
				initTemplate.call(this, this.props, this.initShow);
			});
		}
		getMultiLang({ moduleId: ['gatheringbill', 'public'], domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	componentDidMount() {

	}
	//查询单据详情
	initShow = () => {
		if (this.props.getUrlParam('status') != 'add') {
			let data = {
				pk_bill: this.props.getUrlParam('id'),
				pageId: this.props.getUrlParam("pagecode")
			};
			ajax({
				url: '/nccloud/arap/notice/cardquery.do',
				data: data,
				success: (res) => {
					if (res.data) {
						updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
						if (res.data.head) {
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setColScale([{ areacode: this.tableId, filedcode: 'startvalue', scale: "0" }, { areacode: this.tableId, filedcode: 'endvalue', scale: "0" }])
							this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
						}
					} else {
						this.props.form.setAllFormValue({ [formId]: { rows: [] } });
						this.props.cardTable.setTableData(tableId, { rows: [] });
					}
					if (this.props.getUrlParam('status') == 'edit') {
						this.props.resMetaAfterPkorgEdit();
					}
					this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true });
					this.toggleShow();
				}
			});
		} else {
			let pk_org = getContext(loginContextKeys.pk_org);
			let org_Name = getContext(loginContextKeys.org_Name);
			if (pk_org && org_Name) {
				this.props.form.setFormItemsValue(this.formId, { 'pk_org': { value: pk_org, display: org_Name } });
				this.props.resMetaAfterPkorgEdit();
				this.props.form.setFormItemsDisabled(this.formId, { 'code': false, 'name': false, 'type': false, 'pk_curr': false })
			}
			this.toggleShow();
		}

	}

	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		this.props.cardTable.setColScale([{ areacode: this.tableId, fieldcode: 'startvalue', scale: "0" }, { areacode: this.tableId, fieldcode: 'endvalue', scale: "0" }])
		this.setBtnVisible(this.props, status)
		this.props.form.setFormStatus(formId, status);
		if (status == 'add') {
			this.props.cardTable.setStatus(this.tableId, 'edit');
		} else {
			this.props.cardTable.setStatus(this.tableId, status);
		}
	};

	setBtnVisible = (props, status) => {
		let isBrowse = status === 'browse' ? true : false;
		if (isBrowse) {
			if (this.props.getUrlParam('id')) {
				this.props.button.setButtonVisible(['Add', 'Edit', 'Delete', 'Refresh'], true)
				this.props.button.setButtonVisible(['Save', 'Cancel', 'AddLine', 'DelLine'], false);
			} else {
				this.props.button.setButtonVisible(['Add'], true)
				this.props.button.setButtonVisible(['Edit', 'Delete', 'Save', 'Cancel', 'AddLine', 'DelLine'], false);
			}
		} else {
			this.props.button.setButtonVisible(['Save', 'Cancel', 'AddLine', 'DelLine'], true);
			this.props.button.setButtonVisible(['Add', 'Edit', 'Delete', 'Refresh'], false)
		}
		if (this.props.form.getFormItemsValue(this.formId, 'pk_org') && this.props.form.getFormItemsValue(this.formId, 'pk_org').value) {
			this.props.button.setButtonDisabled(['AddLine'], false);
			this.props.button.setButtonDisabled(['DelLine'], true);
		} else {
			this.props.button.setButtonDisabled(['AddLine', 'DelLine'], true);
		}
		if (isBrowse) {
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: true });
			// this.setState({ backBtn: true })
		} else {
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: false });
			// this.setState({ backBtn: false })
		}
	}

	//删除单据
	delConfirm = () => {
		ajax({
			url: '/nccloud/arap/notice/delete.do',
			data: [{
				pk_bill: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(formId, 'ts').value
			}],
			success: (res) => {
				if (res) {
					toast({ color: 'success', content: this.state.json['gatheringbill-000017'] });/* 国际化处理： 删除成功*/
					let id = this.props.getUrlParam("id");
					deleteCacheById(pkname, id, dataSource);
					let nextId = getNextId(id, dataSource);
					if (nextId) {
						this.props.setUrlParam({ id: nextId })
						this.initShow();
					} else {
						this.props.form.EmptyAllFormValue(this.formId);
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
					}
				}
			}
		});
	};

	cancel = () => {//取消按钮
		if ((this.props.getUrlParam('status') === 'edit')) {
			this.props.setUrlParam({ status: 'browse' })
			let id = this.props.getUrlParam('id');
			let cardData = getCacheById(id, dataSource);
			if (cardData) {
				this.props.form.EmptyAllFormValue(this.formId);
				this.props.cardTable.setTableData(this.tableId, { rows: [] });
				this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
				this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
			} else {
				this.initShow();
			}
		} else if (this.props.getUrlParam('status') === 'add') {
			let id = this.props.getUrlParam('id');
			if (!id) {
				id = getCurrentLastId(dataSource);
			}
			if (id) {
				this.props.setUrlParam({ status: 'browse', id: id })
				let cardData = getCacheById(id, dataSource);
				if (cardData) {
					this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
					this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
				} else {
					this.initShow()
				}
			} else {
				this.props.form.EmptyAllFormValue(this.formId);
				this.props.cardTable.setTableData(this.tableId, { rows: [] });
				this.props.setUrlParam({ status: 'browse' })
			}
		}
		this.toggleShow();

	}

	//保存单据
	saveBill = () => {
		if (!this.props.form.isCheckNow(this.formId)) {//表单验证
			return;
		}
		if (!this.props.cardTable.checkTableRequired(this.tableId)) {//表格验证
			return;
		}
		let cardData = this.props.createMasterChildData(this.props.getUrlParam("pagecode"), this.formId, this.tableId);
		let url = '/nccloud/arap/notice/insert.do'//新增保存
		if (this.props.getUrlParam('status') === 'edit') {
			url = '/nccloud/arap/notice/update.do'//修改保存
		}
		ajax({
			url: url,
			data: cardData,
			success: (res) => {
				let pk_notice = null;
				if (res.success) {
					if (res.data) {
						toast({ color: 'success', content: this.state.json['gatheringbill-000008'] });/* 国际化处理： 保存成功*/
						if (res.data.head && res.data.head[formId]) {
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							pk_notice = res.data.head[formId].rows[0].values.pk_notice.value;
						}
						if (res.data.body && res.data.body[tableId]) {
							this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
						}
						if (this.props.getUrlParam('status') == 'add') {
							addCache(pkname, res.data, this.formId, dataSource);//新增
						} else {
							updateCache(pkname, pk_notice, res.data, this.formId, dataSource);//修改之后更新缓存
						}
					}
				}
				this.props.setUrlParam({ status: 'browse', id: pk_notice, pagecode: '20060CUFT_CARD' })
				this.toggleShow();
			}
		});

	};

	//选中行
	onSelected = (props, moduleId, record, index, status) => {
		if (status) {
			props.button.setButtonDisabled(['DelLine'], false)
		} else {
			if (props.cardTable.getCheckedRows(moduleId).length == 0) {
				props.button.setButtonDisabled(['DelLine'], true)
			}
		}
	}

	//全选中
	onSelectedAll = (props, moduleId, status, length) => {
		if (status) {
			props.button.setButtonDisabled(['DelLine'], false)
		} else {
			if (props.cardTable.getNumberOfRows(moduleId) == 0 || props.cardTable.getCheckedRows(moduleId).length == 0) {
				props.button.setButtonDisabled(['DelLine'], true)
			}
		}
	}

	//获取列表肩部信息
	getTableHead = (buttons) => {
		return (
			<div>
				{this.props.button.createButtonApp({
					area: 'card_body',
					buttonLimit: 3,
					onButtonClick: buttonClick.bind(this),
					popContainer: document.querySelector('.header-button-area')
				})}
			</div>
		);
	};

	//返回列表
	backList = () => {
		this.props.pushTo('/list', {
			pagecode: '20060CUFT_LIST'
		})
	}

	render() {
		let { cardTable, form, button, modal, cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let { createModal } = modal;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createBillHeadInfo({
								title: this.state.json['gatheringbill-000065'],//国际化处理： 催款语气
								backBtnClick: () => {
									this.backList();
								}
							})}
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'card_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: formBeforeEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHead: this.getTableHead.bind(this, buttons),
							onAfterEvent: afterEvent.bind(this),
							onSelected: this.onSelected.bind(this),
							onSelectedAll: this.onSelectedAll.bind(this),
							hideSwitch: () => { return false },
							isAddRow: false,
							showCheck: true,
							showIndex: true
						})}
					</div>
				</div>

				{
					createModal('delete', {
						title: this.state.json['gatheringbill-000064'],/* 国际化处理： 确定删除？*/
						beSureBtnClick: this.delConfirm
					})
				}
			</div >
		);
	}
}

Card = createPage({
	// initTemplate: initTemplate,
})(Card);

export default Card;
