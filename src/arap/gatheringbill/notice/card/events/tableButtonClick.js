import { ajax, base, toast } from 'nc-lightapp-front';
import { tableId, formId, pageId, appcode } from '../constants';

export default function (props, key, text, record, index) {
    switch (key) {
        // 表格操修改
        case 'open_browse':
            props.cardTable.toggleRowView(tableId, record);
            break;
        case 'open_edit':
            props.cardTable.openModel(tableId, 'edit', record, index);
            break;
        default:
            
            break;
    }
};
