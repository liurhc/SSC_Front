import { ajax, toast,cardCache,promptBox } from 'nc-lightapp-front';
import { tableId, formId, dataSource, pkname } from '../constants';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';
let { getDefData, setDefData, updateCache } = cardCache;

export default function (props, id) {
	switch (id) {
		case 'Add':
			let pk_org = getContext(loginContextKeys.pk_org);
			let org_Name = getContext(loginContextKeys.org_Name);
			this.props.form.setFormStatus(this.formId, 'edit');
			this.props.cardTable.setStatus(this.tableId, 'edit');
			this.props.form.EmptyAllFormValue(this.formId);
			this.props.cardTable.setTableData(this.tableId, { rows: [] });
			if (pk_org && org_Name) {
				this.props.setUrlParam({ status: 'add' })
				this.props.resMetaAfterPkorgEdit();
				this.props.form.setFormItemsValue(this.formId, { 'pk_org': { value: pk_org, display: org_Name } });
				this.props.form.setFormItemsDisabled(this.formId,{'code':false,'name':false,'type':false,'pk_curr':false})
			}else{
				this.props.setUrlParam({ status: 'add' })
				this.props.initMetaByPkorg();
				this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': false });
			}
			this.toggleShow();
			break;
		case 'Save':
			this.saveBill();
			break
		case 'Edit':
			props.setUrlParam({ status: 'edit' })
			this.props.resMetaAfterPkorgEdit()
			this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true })
			this.toggleShow();
			break;
		case 'Delete':
			promptBox({
				color: 'warning',                 
				title: this.state.json['gatheringbill-000005'],                  /* 国际化处理： 删除*/
				content: this.state.json['gatheringbill-000006'],             /* 国际化处理： ​确定要删除吗？*/
				noFooter: false,                 
				noCancelBtn: false,              
				beSureBtnName: this.state.json['gatheringbill-000004'],           /* 国际化处理： 确定*/
				cancelBtnName: this.state.json['gatheringbill-000002'] ,          /* 国际化处理： 取消*/
				beSureBtnClick: this.delConfirm 
			});
			break
		case 'Cancel':
			promptBox({
				color: 'warning',                 
				title: this.state.json['gatheringbill-000002'],                  /* 国际化处理： 取消*/
				content: this.state.json['gatheringbill-000003'],             /* 国际化处理： ​确定要取消吗？*/
				noFooter: false,                 
				noCancelBtn: false,              
				beSureBtnName: this.state.json['gatheringbill-000004'],           /* 国际化处理： 确定*/
				cancelBtnName: this.state.json['gatheringbill-000002'] ,          /* 国际化处理： 取消*/
				beSureBtnClick: this.cancel  
			});
			break;
		case 'Refresh': //刷新
			ajax({
				url: '/nccloud/arap/notice/cardquery.do',
				data: {
					pk_bill: this.props.getUrlParam('id'),
					pageId: this.props.getUrlParam("pagecode")
				},
				success: (res) => {
					if (res.data) {
						toast({ color: 'success', title: this.state.json['gatheringbill-000078']});/* 国际化处理： 刷新成功*/
						updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
						if (res.data.head) {
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setColScale([{ areacode: this.tableId, filedcode: 'startvalue', scale: "0" }, { areacode: this.tableId, filedcode: 'endvalue', scale: "0" }])
							this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
						}
					} else {
						this.props.form.setAllFormValue({ [formId]: { rows: [] } });
						this.props.cardTable.setTableData(tableId, { rows: [] });
					}
					this.toggleShow();
				}
			});
			break;
		case 'AddLine':
			props.cardTable.addRow(tableId);
			let data = props.cardTable.getAllData(tableId);
			let rowIndex = props.cardTable.getNumberOfRows(tableId) - 1;
			let startvalue = rowIndex == 0 ? null : data.rows[rowIndex - 1].values.startvalue.value;
			let endvalue = rowIndex == 0 ? null : data.rows[rowIndex - 1].values.endvalue.value;
			if (startvalue == "" && endvalue == "" && rowIndex != 0) {
				props.cardTable.delRowsByIndex(tableId, rowIndex);
				rowIndex = rowIndex - 1;
				startvalue = rowIndex == 0 ? null : data.rows[rowIndex - 1].values.startvalue.value;
				endvalue = rowIndex == 0 ? null : data.rows[rowIndex - 1].values.endvalue.value
			}
			ajax({
				url: '/nccloud/arap/notice/addline.do',
				async: false,
				data: {
					type: props.form.getFormItemsValue(formId, 'type').value,
					rowcount: rowIndex,
					startvalue: startvalue,
					endvalue: endvalue
				},
				success: (res) => {
					let start = parseFloat(res.data.startvalue).toFixed(0);
					let end = parseFloat(res.data.endvalue).toFixed(0);
					let startvalue = { value: start, display: '' };
					let endvalue = { value: end, display: '' };
					let type = { value: res.data.type, display: res.data.type == 1 ? this.state.json['gatheringbill-000061'] : this.state.json['gatheringbill-000062'] };/* 国际化处理： 账龄,余额*/
					this.props.form.setFormItemsValue(formId, { 'type': type });
					this.props.cardTable.setValByKeyAndIndex(tableId, rowIndex, 'startvalue', startvalue);
					this.props.cardTable.setValByKeyAndIndex(tableId, rowIndex, 'endvalue', endvalue);
				}
			});
			break;
		case 'DelLine':
			const delData = this.props.cardTable.getCheckedRows(tableId);
			if (delData.length == 0) {
				toast({ color: 'warning', content: this.state.json['gatheringbill-000063'] });/* 国际化处理： 请至少选择一行数据！*/
				return;
			}
			let delIndexs = [];
			delData.forEach((val) => {
				if (val.data.status != '3') {
					delIndexs.push(val.index);
				}
			});
			props.cardTable.delRowsByIndex(tableId, delIndexs);
			if(props.cardTable.getNumberOfRows(tableId) == 0 || props.cardTable.getCheckedRows(tableId).length == 0){
				props.button.setButtonDisabled(['DelLine'], true)
			}
			break;
		default:
			break;
	}
}
