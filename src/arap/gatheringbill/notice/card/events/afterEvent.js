import { ajax, promptBox } from 'nc-lightapp-front';
import { tableId, formId } from '../constants';

export default function afterEvent(props, moduleId, key, value, oldValue, i, s, g) {
    if (key == 'pk_org') {
        if (value.value == null || value.value == "") {
            promptBox({
                color: 'warning',
                title: this.state.json['gatheringbill-000021'],/* 国际化处理： 确认修改*/
                content: this.state.json['gatheringbill-000060'],/* 国际化处理： 是否修改组织，这样会清空您录入的信息?*/
                beSureBtnName: this.state.json['gatheringbill-000004'],/* 国际化处理： 确定*/
                cancelBtnName: this.state.json['gatheringbill-000002'],/* 国际化处理： 取消*/
                beSureBtnClick: () => {
                    this.props.form.EmptyAllFormValue(this.formId);
                    this.props.cardTable.setTableData(this.tableId, { rows: [] });
                    if (this.props.getUrlParam('status') == 'add') {
                        this.props.initMetaByPkorg();
                    }
                },
                cancelBtnClick: () => {
                    this.props.form.setFormItemsValue(this.formId, { 'pk_org': oldValue });
                }
            });
        } else {
            let pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org').value
            if (pk_org) {
                this.props.resMetaAfterPkorgEdit();
                this.props.form.setFormItemsDisabled(this.formId,{'code':false,'name':false,'type':false,'pk_curr':false})
            }
            this.props.cardTable.setColScale([{areacode: this.tableId, fieldcode: 'startvalue', scale: "0"},{areacode: this.tableId, fieldcode: 'endvalue', scale: "0"}])
            this.props.setUrlParam({ status: 'add' })
            this.props.button.setButtonDisabled(['AddLine'], false);
            this.toggleShow();
        }
    }

}
