import { base, ajax } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
import tableButtonClick from './tableButtonClick.js';
import { tableId, formId  } from '../constants';
import { loginContext } from '../../../../public/components/arapInitInfo/loginContext';

export default function (props,callback) {
	props.createUIDom(
		{
			pagecode: props.getUrlParam('pagecode'),
			appcode: props.getSearchParam("c"),
			reqDataQuerypage: {
				rqUrl: '/arap/arappub/querypage.do',
                rqJson: `{\n  \"pagecode\": \"${props.getUrlParam('pagecode')}\",\n  \"appcode\": \"${props.getSearchParam("c")}\"\n}`,
                rqCode: 'template'
			}
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(props, meta)
					props.meta.setMeta(meta);
					if(props.getUrlParam('status') == 'add'){
						props.initMetaByPkorg();
					}
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if(callback){
					callback();
				}
			}
		},
		false//请求模板不走缓存
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	return meta;
}
