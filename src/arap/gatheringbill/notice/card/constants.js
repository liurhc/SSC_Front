
/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 表体区域
 */
export const tableId = 'bodys';


/**
* 单页应用缓存,命名规范为："领域名.模块名.节点名.自定义名"。 
*/
export const dataSource = 'fi.arap.notice.20060CUFT';


/**
 * 单页应用缓存主键名字
 */
export const pkname  = 'pk_notice';

