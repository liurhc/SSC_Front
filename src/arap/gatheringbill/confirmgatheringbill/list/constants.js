
/**
 * 查询区域
 */
export const searchId = 'query';

/**
 * 列表区域
 */
export const tableId = 'list';

/**
 * 默认交易类型
 */
export const tradeType = 'D2';


/**
 * 单据类型
 */
export const billType = 'F2';

/**
 * 单页应用缓存
 */
export const dataSource  = 'fi.arap.confirmgatheringbill.20060GBC';

/**
 * 单页应用缓存主键名字(该名字为表的主键名字)
 */
export const pkname  = 'pk_gatherbill';

	/**
 * 单页查询区缓存主键名字
 */
export const searchKey = 'pk_gatherbill_search';

