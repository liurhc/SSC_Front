//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, cardCache,getMultiLang,createPageIcon} from 'nc-lightapp-front';
import { buttonClick, initTemplate, searchBtnClick, pageInfoClick, tableModelConfirm,doubleClick,tableButtonClick} from './events';
import {buttonVisible,getButtonsKey,onListButtonControl} from '../../../public/components/pubUtils/buttonvisible.js';
import { tableId, searchId,pkname,dataSource,searchKey} from './constants';
import afterEvent from '../../../public/components/searchAreaAfterEvent'//查询区编辑后事件
let { setDefData, getDefData } = cardCache;

class List extends Component {
	constructor(props) {
		super(props);
		this.searchId = searchId;
		this.tableId = tableId;
		this.pageId = props.getSearchParam("p");
		let { search} = this.props;
        let {setSearchValByField, getAllSearchData} = search;
        this.setSearchValByField = setSearchValByField;//设置查询区某个字段值
        this.getAllSearchData = getAllSearchData;//获取查询区所有字段数据
		this.Info = {
			allButtonsKey :[] 
		}
		this.state = {
			json:{}
		}
	}

	componentWillReceiveProps(nextProps){
	}
	
	componentWillMount(){
		let callback = (json) => {
			this.setState({json:json},() => {
				initTemplate.call(this, this.props,this.initShow);
			});
		}
		getMultiLang({moduleId:['gatheringbill','public'],domainName :'arap',currentLocale:'simpchn',callback});
	}

	componentDidMount() {
		
	}
	initShow =()=>{
		if(!this.props.table.hasCacheData(dataSource)){
			this.onSelected();//缓存不存在，就控制按钮
		}
	}

	getPagecode = () =>{
		let pagecode = '20060GBC_LIST';
		return pagecode;
	}

	

    //列表控制按钮
	onSelected = () => {
		onListButtonControl(this);
	};
	
	// 查询区渲染完成回调函数
    renderCompleteEvent = () => {
        let cachesearch = getDefData(searchKey, dataSource);
        if (cachesearch && cachesearch.conditions) {
          // this.props.search.setSearchValue(this.searchId, cachesearch);
          for(let item of cachesearch.conditions){
            if (item.field == 'billdate') {
               // 时间类型特殊处理
               let time = [];
               time.push(item.value.firstvalue);
               time.push(item.value.secondvalue);
               this.props.search.setSearchValByField(this.searchId,item.field,
                        {display:item.display,value:time});
            }else{
                this.props.search.setSearchValByField(this.searchId,item.field,
                {display:item.display,value:item.value.firstvalue});
             }
          }
        }
    }
	
	render() {
		let { table, button, search } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon ? createPageIcon() : null}
						<h2 className="title-search-detail">{this.state.json['gatheringbill-000009']}</h2>{/* 国际化处理： 收款单协同确认*/}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 3, 
							onButtonClick:buttonClick.bind(this), 
							popContainer: document.querySelector('.header-button-area')
                    	})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(
						this.searchId,
						{
							clickSearchBtn: searchBtnClick.bind(this),
							showAdvBtn: true,  
							onAfterEvent: afterEvent.bind(this),//编辑后事件       
							renderCompleteEvent:this.renderCompleteEvent  // 查询区渲染完成回调函数                 
						}
					)}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						dataSource: dataSource,
						pkname: pkname,
						handlePageInfoChange: pageInfoClick.bind(this),
						tableModelConfirm: tableModelConfirm,
						showCheck: true,
						showIndex: true,
						onRowDoubleClick:doubleClick.bind(this),
						onSelected: this.onSelected.bind(this),
						onSelectedAll:this.onSelected.bind(this),	
						componentInitFinished:()=>{
							this.onSelected();
						}					
					})}
				</div>
			</div>
		);
	}
}

List = createPage({
	mutiLangCode: '2052'
})(List);

export default List;
