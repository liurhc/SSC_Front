import { ajax, toast,promptBox,cardCache } from 'nc-lightapp-front';
import { headButton } from '../../../../public/components/pubUtils/buttonName.js';
import { dataSource,pkname } from '../constants';
import { imageScan, imageView } from 'sscrp/rppub/components/image';
let {updateCache ,deleteCacheById} = cardCache;
export default function (props, id) {

  switch (id) {
    case headButton.Save:
      this.saveBill();
      break
    //确认
    case headButton.Confirm:
      ajax({
        url: '/nccloud/arap/arappub/confirm.do',
        data: {
          pageId: this.getPagecode(),
          pk_bill: props.getUrlParam('id'),
          ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
          billType: this.billType
        },
        success: (res) => {
          if (res.data) {
            this.props.beforeUpdatePage();//打开开关
            if (res.data.head) {
              props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
            }
            if (res.data.body) {
              this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId])
            }
          } 
          props.setUrlParam({status:'edit'})
          this.props.cardTable.setStatus(this.tableId, 'edit');
          this.props.form.setFormStatus(this.formId, 'edit');
          this.props.updatePage(this.formId, this.tableId);//关闭开关
          this.toggleShow()
        }
      });

      break;
    //取消确认
    case headButton.CancelConfirm:
      ajax({
        url: '/nccloud/arap/arappub/cancelconfirm.do',
        data: {
          pageId: this.getPagecode(),
          pk_bill: props.getUrlParam('id'),
          ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
          billType: this.billType,
          type :2
        },
        success: (res) => {
          toast({ color: 'success', content: this.state.json['gatheringbill-000001'] });/* 国际化处理： 取消确认成功*/
          if (res.data) {
            if (res.data.head) {
              props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
            }
            if (res.data.body) {
              props.cardTable.updateDataByRowId(this.tableId, res.data.body[this.tableId])
            }
            let newCardData = props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
            updateCache(pkname,props.getUrlParam('id'),newCardData,this.formId,dataSource);//修改之后更新缓存
          } 
          props.setUrlParam({status:'browse'})
          this.toggleShow()
        }
      });

      break;
    case headButton.Cancel:
      promptBox({
        color: 'warning',                 
        title: this.state.json['gatheringbill-000002'],                  /* 国际化处理： 取消*/
        content: this.state.json['gatheringbill-000003'],             /* 国际化处理： ​确定要取消吗？*/
        noFooter: false,                 
        noCancelBtn: false,              
        beSureBtnName: this.state.json['gatheringbill-000004'],           /* 国际化处理： 确定*/
        cancelBtnName: this.state.json['gatheringbill-000002'] ,          /* 国际化处理： 取消*/
        beSureBtnClick: this.cancel  
      });
      break
    case headButton.Delete:
      promptBox({
        color: 'warning',                 
        title: this.state.json['gatheringbill-000005'],                  /* 国际化处理： 删除*/
        content: this.state.json['gatheringbill-000006'],             /* 国际化处理： ​确定要删除吗？*/
        noFooter: false,                 
        noCancelBtn: false,              
        beSureBtnName: this.state.json['gatheringbill-000004'],           /* 国际化处理： 确定*/
        cancelBtnName: this.state.json['gatheringbill-000002'] ,          /* 国际化处理： 取消*/
        beSureBtnClick: this.delConfirm 
      });
      break;
    case headButton.Refresh:
      ajax({
        url: '/nccloud/arap/arappub/cardRefresh.do',
        data: {
          pk_bill: this.props.getUrlParam('id'),
          pageId: this.getPagecode(),
          billType: this.billType
        },
        success: (res) => {
          if (res.data) {
            toast({ color: 'success', title: this.state.json['gatheringbill-000078']});/* 国际化处理： 刷新成功*/
            updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
            if (res.data.head) {
              this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
            }
            if (res.data.body) {
              this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
            }
          } else {
            this.props.form.EmptyAllFormValue(this.formId);
            this.props.cardTable.setTableData(this.tableId, { rows: [] });
          }
          this.toggleShow();
        },
        error: (res) => {
          this.props.form.EmptyAllFormValue(this.formId);
          this.props.cardTable.setTableData(this.tableId, { rows: [] });
          deleteCacheById(pkname, this.props.getUrlParam('id'), dataSource);
          this.toggleShow();
          let str = String(res);
          let content = str.substring(6, str.length);
          toast({ color: 'danger', content: content });
        }
      });
      break;
      case headButton.ReceiptCheck: //影像查看pk_tradetype
			if (props.getUrlParam('status') == 'add') {
				toast({ color: 'warning', content: this.state.json['gatheringbill-000030'] }); /* 国际化处理： 单据未暂存！*/
				return;
			}
		
			var billInfoMap = {};

			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = props.getUrlParam('id');
			billInfoMap.pk_billtype = this.props.form.getFormItemsValue(this.formId, 'pk_billtype').value;
			billInfoMap.pk_tradetype = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
			billInfoMap.pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
			imageView(billInfoMap, 'iweb');

			break;
		case headButton.ReceiptScan: //影像扫描
			if (props.getUrlParam('status') == 'add') {
				toast({
					color: 'warning',
					content: this.state.json['gatheringbill-000031']
				}); /* 国际化处理： 请先 <暂存> 单据再扫描影像！*/
				return;
			}
			;
			let tradetype = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
			let allData = props.createMasterChildData(tradetype, this.formId, this.tableId);

			var billInfoMap = {};
			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = props.getUrlParam('id');
			billInfoMap.pk_billtype = allData.head.head.rows[0].values.pk_billtype.value;
			billInfoMap.pk_tradetype = allData.head.head.rows[0].values.pk_tradetype.value;
			billInfoMap.pk_org = allData.head.head.rows[0].values.pk_org.value;

			//影像所需 FieldMap
			billInfoMap.BillType = allData.head.head.rows[0].values.pk_tradetype.value;
			billInfoMap.BillDate = allData.head.head.rows[0].values.creationtime.value;
			billInfoMap.Busi_Serial_No = allData.head.head.rows[0].values.pk_gatherbill.value;
			billInfoMap.pk_billtype = allData.head.head.rows[0].values.pk_billtype.value;
			billInfoMap.OrgNo = allData.head.head.rows[0].values.pk_org.value;
			billInfoMap.BillCode = allData.head.head.rows[0].values.billno.value==undefined?'':allData.head.head.rows[0].values.billno.value;
			billInfoMap.OrgName = allData.head.head.rows[0].values.pk_org.display;
			billInfoMap.Cash = allData.head.head.rows[0].values.money.value;

			imageScan(billInfoMap, 'iweb');

			break;
    default:
      break
  }
}
