//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, cardCache,getMultiLang } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, pageInfoClick } from './events';
import { buttonVisible, getButtonsKey } from '../../../public/components/pubUtils/buttonvisible.js';
import { tableId, formId, billType, dataSource, pkname, tradeType } from './constants';
import { bodyBeforeEvent } from '../../../public/components/pubUtils/arapTableRefFilter';
import { formBeforeEvent } from '../../../public/components/pubUtils/arapFormRefFilter';
import { dealCardData } from '../../../public/components/pubUtils/dealCardData';
let { NCBackBtn } = base;
let { setDefData, getDefData, addCache, getNextId, deleteCacheById, getCacheById, updateCache } = cardCache;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = formId;
		this.tableId = tableId;
		this.billType = billType;
		this.Info = {
			allButtonsKey: [], //保存所有按钮
			isModelSave: false,//是否是整单保存，默认为false
		};
		this.state = {
			json:{}
		}
	}

	componentWillReceiveProps(nextProps) {}

	componentWillMount() {
		let callback = (json) => {
			this.setState({json:json},() => {
				initTemplate.call(this, this.props, this.initShow);
				window.onbeforeunload = () => {
					let status = this.props.getUrlParam('status');
					if (status == 'edit' || status == 'add') {
						return '';
					}
				};
			});
		}
		getMultiLang({moduleId:['gatheringbill','public'],domainName :'arap',currentLocale:'simpchn',callback});
	}

	getPagecode = () =>{
		let pagecode = this.props.getUrlParam('pagecode')
		if(!pagecode){
			pagecode = this.props.getSearchParam('p')
		}
		return pagecode
	}
	//页面初始化
	initShow = () => {
		if (this.props.getUrlParam('status') == 'browse') {
			this.queryCard(this.props.getUrlParam('id'), this.getPagecode());
		} else if(!this.props.getUrlParam('status')){
			this.toggleShow();
		} else {
			let data = getDefData('confirm' + this.props.getUrlParam('id'), dataSource);
			if (data) {
				this.props.form.setAllFormValue({ [this.formId]: data.head[this.formId] });
				this.props.cardTable.setTableData(this.tableId, data.body[this.tableId]);
				this.props.cardTable.setStatus(this.tableId, 'edit');
				this.props.form.setFormStatus(this.formId, 'edit');
				this.toggleShow();
			} else {
				ajax({
					url: '/nccloud/arap/arappub/confirm.do',
					data: {
						pageId: this.getPagecode(),
						pk_bill: this.props.getUrlParam('id'),
						billType: this.billType
					},
					success: (res) => {
						this.props.beforeUpdatePage();//打开开关
						if (res.data) {
							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							}
							if (res.data.body) {
								this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
							}
						} else {
							this.props.form.EmptyAllFormValue(this.formId);
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
						}
						this.props.cardTable.setStatus(this.tableId, 'edit');
						this.props.form.setFormStatus(this.formId, 'edit');
						this.props.updatePage(this.formId, this.tableId);//关闭开关
						this.toggleShow();
					}
				});
			}
		}
	};

	queryCard = (id, pageId) => {
		let cardData = getCacheById(id, dataSource);
		if (cardData) {
			this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
			this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
			this.toggleShow();
		} else {
			ajax({
				url: '/nccloud/arap/confirmgatheringbill/cardquery.do',
				data: {
					pk_bill: id,
					pageId: pageId
				},
				success: (res) => {
					if (res.data) {
						this.props.beforeUpdatePage();//打开开关
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
						this.props.cardTable.setStatus(this.tableId, 'browse');
						this.props.form.setFormStatus(this.formId, 'browse');
						this.props.updatePage(this.formId, this.tableId);//关闭开关
						updateCache(pkname, id, res.data, this.formId, dataSource);
					}
					this.toggleShow();
				}
			});
		}
	};

	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		if (!status) { status = 'browse'; }//如果未定义，默认给值浏览态
		let trueBtn = []; //可见的按钮
		let falseBtn = []; //不可见的按钮
		for (let i = 0; i < this.Info.allButtonsKey.length; i++) {
			let flag = buttonVisible(
				status,
				this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId).head[
					this.formId
				].rows[0].values,
				this.Info.allButtonsKey[i],'card',this
			);
			if (flag) {
				trueBtn.push(this.Info.allButtonsKey[i]);
			} else {
				falseBtn.push(this.Info.allButtonsKey[i]);
			}
		}
		if (status != 'browse') {
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
		} else {
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);
		}
		this.props.button.setButtonVisible(trueBtn, true);
		this.props.button.setButtonVisible(falseBtn, false);
	};

	//删除单据
	delConfirm = () => {
		ajax({
			url: '/nccloud/arap/arappub/confirmdelete.do',
			data: {
				pk_bill: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
				billType: this.billType
			},
			success: (res) => {
				let id = this.props.getUrlParam('id');
				deleteCacheById(pkname, id, dataSource);
				let nextId = getNextId(id, dataSource);
				if (nextId) {
					this.queryCard(nextId, this.getPagecode());
				} else {
					this.props.setUrlParam({ id: null });
					this.props.form.EmptyAllFormValue(this.formId);
					this.props.cardTable.setTableData(this.tableId, { rows: [] });
					this.toggleShow()
				}
			}
		});
	};

	//保存单据
	saveBill = () => {
		if (!this.props.form.isCheckNow(this.formId)) {
			//表单验证
			return;
		}
		if (!this.props.cardTable.checkTableRequired(this.tableId)) {
			//表格验证
			return;
		}
		let cardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
		let newCardData  = dealCardData(this,cardData);//去掉空值，减少压缩时间
		let datas = {
			cardData: newCardData,
			uiState: this.props.getUrlParam('status'),
			refNode: 'isCoorcomfirm'
		};
		let callback = () => {
			ajax({
				url: '/nccloud/arap/arappub/save.do',
				data: datas,
				success: (res) => {
					let pk_gatherbill = null;
					let pk_tradetype = null;
					if (res.success) {
						if (res.data) {
							toast({ color: 'success', content: this.state.json['gatheringbill-000008'] });/* 国际化处理： 保存成功*/
							this.props.beforeUpdatePage();//打开开关
							if (res.data.head && res.data.head[this.formId]) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
								pk_gatherbill = res.data.head[this.formId].rows[0].values.pk_gatherbill.value;
								pk_tradetype = res.data.head[this.formId].rows[0].values.pk_tradetype.value;
							}
							if (res.data.body && res.data.body[this.tableId]) {
								this.props.cardTable.updateDataByRowId(this.tableId, res.data.body[this.tableId])
							}
							this.props.cardTable.setStatus(this.tableId, 'browse');
							this.props.form.setFormStatus(this.formId, 'browse');
							this.props.updatePage(this.formId, this.tableId);//关闭开关
							let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
							updateCache(pkname, pk_gatherbill, newCardData, this.formId, dataSource); //修改之后更新缓存
							this.props.setUrlParam({ status: 'browse', id: pk_gatherbill, pagecode: pk_tradetype });
						}
					}
					
					if(this.Info.isModelSave){
						this.Info.isModelSave = false;
						this.props.cardTable.closeModel(this.tableId);
					}
					this.toggleShow();
				}
			});
		};
		this.props.validateToSave(datas.cardData, callback, { table1: 'cardTable' }, 'card');
	};

	//获取列表肩部信息
	getTableHead = (buttons) => {
		return (
			<span>
				{this.props.button.createButtonApp({
					area: 'card_body',
					buttonLimit: 3,
					onButtonClick: buttonClick.bind(this),
					popContainer: document.querySelector('.header-button-area')
				})}
			</span>
		);
	};
	cancel = () => {
		this.props.setUrlParam({status : 'browse'})
		let id = this.props.getUrlParam('id');
		let cardData = getCacheById(id, dataSource);
		if (cardData) {
			this.props.beforeUpdatePage();//打开开关
			this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
			this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
			this.props.cardTable.setStatus(this.tableId, 'browse');
			this.props.form.setFormStatus(this.formId, 'browse');
			this.props.updatePage(this.formId, this.tableId);//关闭开关
			this.toggleShow();
		} else {
			this.queryCard(this.props.getUrlParam('id'),this.getPagecode());
		}
	};
	

	//整单保存事件
	modelSaveClick = () => {
		this.Info.isModelSave = true;
		this.saveBill();
	};

	//返回列表
	backList = () => {
		this.props.pushTo('/list', {
			pagecode: '20060GBC_LIST'
		});
	};

	render() {
		let { cardTable, form, button, modal, cardPagination } = this.props;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		const { createCardPagination } = cardPagination;
		let { createButton } = button;
		let { createModal } = modal;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createBillHeadInfo({
								title:this.state.json['gatheringbill-000009'],//国际化处理： 收款单协同确认
								backBtnClick: () => {
									this.backList();
								}
							})}
						</div>
						{/* 国际化处理： 收款单协同确认*/}
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'card_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
						<div className="header-cardPagination-area" style={{ float: 'right' }}>
							{createCardPagination({
								handlePageInfoChange: pageInfoClick.bind(this),
								dataSource: dataSource
							})}
						</div>
					</div>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: formBeforeEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{this.getTableHead(buttons)}
						{createCardTable(this.tableId, {
							tableHead: this.getTableHead.bind(this, buttons),
							modelSave: this.modelSaveClick.bind(this),
							hideAdd: true,
							hideDel: true,
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: bodyBeforeEvent.bind(this),
							showCheck: true,
							showIndex: true
						})}
					</div>
				</div>
			</div>
		);
	}
}

Card = createPage({
	//initTemplate: initTemplate,
	mutiLangCode: '2052'
})(Card);

export default Card;
