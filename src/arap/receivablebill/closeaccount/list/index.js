import React, { Component } from 'react';
import { getMultiLang } from 'nc-lightapp-front';
import Closeaccount from '../../../public/components/closeaccount/index';
class Closeaccount_AR extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {}
		}
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({json:json});
		}
		getMultiLang({moduleId:'receivablebill',domainName :'arap',currentLocale:'simpchn',callback});
	}

	

	render() {
		return (
			<Closeaccount {...{
				title:this.state.json['receivablebill-000003'],/* 国际化处理： 应收结账*/
				moudleId: '2006',// moudleId 2008代表应付   2006代表应收
				prodId: 'AR',//prodId AR代表收 AP代表应付
				sfbz: 'ys',// 收付标志 应付yf 应收ys
			}} />
		)
		
	}
}



ReactDOM.render(<Closeaccount_AR />, document.querySelector('#app'));
