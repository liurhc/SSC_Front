import { ajax, base } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon, NCMessage } = base;
import {pageId ,appcode,tableId} from '../constants';
export default function(props,changedata) {
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: appcode//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					if(changedata){
						meta = modifierMeta(props, meta ,changedata)
					}
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
    )
}


function modifierMeta(props, meta ,changedata) {
	let hiddenList = changedata.hiddenList;//需要隐藏的表头列
	let changeList = changedata.changeList;//需要替换表头名称的值
	let changeListKey =[];
	let changeListValue = [];
	for(let key in changeList){
		changeListKey.push(key);
		changeListValue.push(changeList[key]);
	}
	meta[tableId].items.forEach((item, key) => {
		if(hiddenList.indexOf(item.attrcode) !=-1 ){
			item.visible = false;
		}
		if(changeListKey.indexOf(item.attrcode) !=-1){
			item.label = changeListValue[changeListKey.indexOf(item.attrcode)]
		}

		
	});
	
	

	return meta;
}





