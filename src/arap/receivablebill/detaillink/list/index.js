// 单表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, pageTo, toast, getMultiLang, gzip } from 'nc-lightapp-front';
let { NCButton } = base;
const { NCBreadcrumb } = base;
const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
import { buttonClick, initTemplate } from './events';
import { pageId, appcode, tableId } from './constants';

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			record: null, //保存当前选中行的行信息
			json: {}
		};
	}
	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				this.getData();
			});
		};
		getMultiLang({ moduleId: 'receivablebill', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	//请求列表数据
	getData = () => {
		let pk_ntbparadimvo = this.props.getUrlParam('pk_ntbparadimvo');
		if (pk_ntbparadimvo) {
			//计划预算
			ajax({
				url: '/nccloud/arap/detaillink/ntbquery.do',
				data: {
					pageId: pageId,
					pk_ntbparadimvo: pk_ntbparadimvo
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						initTemplate(this.props, data); //重新刷一次表头

						if (data && data.tableData && data.tableData.length != 0) {
							this.props.cardTable.setTableData(tableId, data.tableData[tableId]);
						} else {
							this.props.cardTable.setTableData(tableId, { rows: [] });
						}
					}
				}
			});
		} else {
			//总账
			let gziptools = new gzip();
			let data = {
				pageId: pageId,
				params: {
					condition: gziptools.unzip(this.props.getUrlParam('condition')),
					linkData: gziptools.unzip(this.props.getUrlParam('linkData')),
					class: this.props.getUrlParam('class')
				}
			};
			ajax({
				url: '/nccloud/arap/detaillink/glquery.do',
				data: data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						initTemplate(this.props, data); //重新刷一次表头

						if (data && data.tableData && data.tableData.length != 0) {
							this.props.cardTable.setTableData(tableId, data.tableData[tableId]);
						} else {
							this.props.cardTable.setTableData(tableId, { rows: [] });
						}
					}
				}
			});
		}
	};

	onRowClick = (a, b, record, index) => {
		if (record.values.pk_bill.value == '' || !record.values.pk_bill.value) {
			//如果单据主键等于空串或者单据不存在，那么联查单据按钮不可操作
			this.props.button.setButtonDisabled([ 'BillLinkQuery' ], true);
		} else {
			this.props.button.setButtonDisabled([ 'BillLinkQuery' ], false);
		}
		this.state.record = record.values;
	};

	linkBill = () => {
		let record = this.state.record;
		if (record) {
			ajax({
				url: '/nccloud/arap/arappub/linkarapbill.do',
				async: false,
				data: {
					billType: record.pk_billtype.value,
					pk_bill: record.pk_bill.value
				},
				success: (res) => {
					let data = res.data;
					if (data) {
						pageTo.openTo(data.url, data.condition);
					} else {
						toast({
							color: 'warning',
							content: this.state.json['receivablebill-000006']
						}); /* 国际化处理： 未查询到路径!*/
					}
				}
			});
		} else {
			toast({ color: 'warning', content: this.state.json['receivablebill-000007'] }); /* 国际化处理： 请选中一行数据!*/
			return;
		}
	};
	render() {
		let { table, button, search, cardTable } = this.props;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						<h2 className="title-search-detail">{this.state.json['receivablebill-000008']}</h2>
						{/* 国际化处理： 明细联查*/}
					</div>
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							onRowClick: this.onRowClick.bind(this),
							hideSwitch: () => {
								return false;
							}
						})}
					</div>
				</div>
			</div>
		);
	}
}

SingleTable = createPage(
	{
		//initTemplate: initTemplate
	}
)(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
