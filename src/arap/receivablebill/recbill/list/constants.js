
/**
 * 查询区域
 */
export const searchId = 'query';

/**
 * 列表区域
 */
export const tableId = 'list';


/**
 * 默认模板节点标识
 */
export const nodekey = 'list';


/**
 * /默认单据类型
 */
export const billType = 'F0';
/**
 * 默认交易类型
 */
export const tradeType = 'D0';

/**
* 单页应用缓存,命名规范为："领域名.模块名.节点名.自定义名"。 
*/
export const dataSource = 'fi.arap.recbill.20060RBM';

/**
 * 单页应用缓存主键名字
 */
export const pkname  = 'pk_recbill';

	/**
 * 单页查询区缓存主键名字
 */
export const searchKey = 'pk_recbill_search';
