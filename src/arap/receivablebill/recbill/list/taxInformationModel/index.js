import React, { Component } from 'react';
import { ajax, base, toast, getMultiLang } from 'nc-lightapp-front';
import './index.less';
const { NCModal, NCTable } = base;
let pk_recbill = '';
let columns = [
	{
		title: '' /* 国际化处理： 客户*/,
		dataIndex: 'a',
		key: 'a',
		width: 100
	},
	{
		title: '' /* 国际化处理： 税码*/,
		dataIndex: 'b',
		key: 'b',
		width: 100
	},
	{
		title: '' /* 国际化处理： 税率*/,
		dataIndex: 'c',
		key: 'c',
		width: 100
	},
	{
		title: '' /* 国际化处理： 扣税类别*/,
		dataIndex: 'd',
		key: 'd',
		width: 100
	},
	{
		title: '' /* 国际化处理： 购销类型*/,
		dataIndex: 'e',
		key: 'e',
		width: 100
	},
	{
		title: '' /* 国际化处理： 计税金额*/,
		dataIndex: 'f',
		key: 'f',
		width: 100
	},
	{
		title: '' /* 国际化处理： 税额*/,
		dataIndex: 'g',
		key: 'g',
		width: 100
	}
];

export default class TaxInformationModel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: [],
			json: {}
		};
	}
	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				columns = [
					{
						title: this.state.json['receivablebill-000054'] /* 国际化处理： 客户*/,
						dataIndex: 'a',
						key: 'a',
						width: 100
					},
					{
						title: this.state.json['receivablebill-000055'] /* 国际化处理： 税码*/,
						dataIndex: 'b',
						key: 'b',
						width: 100
					},
					{
						title: this.state.json['receivablebill-000056'] /* 国际化处理： 税率*/,
						dataIndex: 'c',
						key: 'c',
						width: 100
					},
					{
						title: this.state.json['receivablebill-000057'] /* 国际化处理： 扣税类别*/,
						dataIndex: 'd',
						key: 'd',
						width: 100
					},
					{
						title: this.state.json['receivablebill-000058'] /* 国际化处理： 购销类型*/,
						dataIndex: 'e',
						key: 'e',
						width: 100
					},
					{
						title: this.state.json['receivablebill-000059'] /* 国际化处理： 计税金额*/,
						dataIndex: 'f',
						key: 'f',
						width: 100
					},
					{
						title: this.state.json['receivablebill-000060'] /* 国际化处理： 税额*/,
						dataIndex: 'g',
						key: 'g',
						width: 100
					}
				];
			});
		};
		getMultiLang({ moduleId: 'receivablebill', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentWillReceiveProps(nextProps) {
		pk_recbill = nextProps.pk_recbill;
		if (nextProps.pk_recbill != '') {
			if (nextProps.show) {
				this.getTableData();
			}
		}
	}

	//获取表格数据
	getTableData = () => {
		let obj = {};
		let brr = [];
		let data = {
			pk_bill: pk_recbill,
			billType: 'F0',
			pageId: '20060RBM_CARD'
		};
		ajax({
			url: '/nccloud/arap/recbill/linkQueryTax.do',
			data: data,
			success: (res) => {
				let { data, success } = res;
				if (success) {
					let data1 = {};
					data.body['bodys'].rows.forEach((row) => {
						data1 = row.values;
						obj.a = data1.customer.display;
						obj.b = data1.taxcodeid.display;
						if (data1.taxrate.display) {
							obj.c = data1.taxrate.display;
						} else {
							obj.c = data1.taxrate.value;
						}
						obj.d = data1.taxtype.display;
						obj.e = data1.buysellflag.display;
						if ((obj.f = data1.caltaxmny.display)) {
							obj.f = data1.caltaxmny.display;
						} else {
							obj.f = data1.caltaxmny.value;
						}
						if ((obj.g = data1.local_tax_de.display)) {
							obj.g = data1.local_tax_de.display;
						} else {
							obj.g = data1.local_tax_de.value;
						}
						brr.push(JSON.parse(JSON.stringify(obj)));
					});

					this.setState({
						tableData: brr
					});
				} else {
					toast({ content: this.state.json['receivablebill-000061'], color: 'warning' }); /* 国际化处理： 没有获取数据*/
				}
			}
		});
	};

	render() {
		let { show } = this.props;
		let { tableData } = this.state;
		return (
			<div>
				<NCModal
					id="ModalId"
					className="query-modal"
					show={show}
					size={'lg'}
					onHide={this.props.handleModel}
					backdrop={'static'}
				>
					<NCModal.Header closeButton>
						<NCModal.Title>{this.state.json['receivablebill-000062']}</NCModal.Title>
						{/* 国际化处理： 税务信息*/}
					</NCModal.Header>
					<NCModal.Body>
						<div className="steps-content">
							<NCTable
								columns={columns}
								data={tableData}
								isDrag
								style={{ marginRight: '20px' }}
								scroll={{
									x: columns.length > 6 ? 100 + (columns.length - 6) * 10 + '%' : '100%',
									y: 492
								}}
								loading={false}
							/>
						</div>
					</NCModal.Body>
				</NCModal>
			</div>
		);
	}
}
