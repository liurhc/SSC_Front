import { ajax, base, toast,promptBox } from 'nc-lightapp-front';
import { tableId,billType} from '../constants';
import {innerButton} from '../../../../public/components/pubUtils/buttonName.js';
import madeBill from '../../../../public/components/madeBill.js';
const tableButtonClick = (that,props, key, text, record, index) => {
    let pageId = props.getSearchParam('p');
    switch (key) {
        // 表格操作按钮
        case innerButton.Delete_inner:
            that.Info.pk_bill = record.pk_recbill.value
            that.Info.ts = record.ts.value
            that.Info.index = index;
            that.delConfirm();    
            break;
        case innerButton.Edit_inner:
        ajax({
            url: '/nccloud/arap/arappub/edit.do',
            data: {
                pk_bill: record.pk_recbill.value,
                billType: billType
            },
            success: (res) => {
                if (res.success) {
                    if (props.getUrlParam("scene") == 'linksce' ||props.getUrlParam("scene") == 'fip') {
                        props.pushTo('/card', {
                            status: 'edit',
                            id: record.pk_recbill.value,
                            pagecode:'20060RBM_CARD_LINK',
                            scene:'linksce'
                        })
                    }else{
                        props.pushTo('/card', {
                            status: 'edit',
                            id: record.pk_recbill.value,
                            pagecode:record.pk_tradetype.value,
                        })
                    }
                }
            }
        });
            break;
        case innerButton.Copy_inner:
            if (props.getUrlParam("scene") == 'linksce'  || props.getUrlParam("scene") == 'fip') {
                props.pushTo('/card', {
                    status: 'add',
                    id: record.pk_recbill.value,
                    type: 'copy',
                    pagecode:'20060RBM_CARD_LINK',
                    scene:'linksce'
                })
            }else{
                props.pushTo('/card', {
                    status: 'add',
                    id: record.pk_recbill.value,
                    type: 'copy',
                    pagecode:record.pk_tradetype.value,
                })
            }
             break;
        case innerButton.Commit_inner:
            that.Info.tipUrl = '/nccloud/arap/arappub/commit.do';
            that.Info.record = record;
            that.commitAndUncommit();
            break;
        case innerButton.Uncommit_inner:
            that.Info.tipUrl = '/nccloud/arap/arappub/uncommit.do';
            that.Info.record = record;
            that.commitAndUncommit();
            break;
        case innerButton.MadeBill_inner://制单
            let madeData = [{
                pk_bill: record.pk_recbill.value,
                billType: billType,
                tradeType: record.pk_tradetype.value
            }]
            madeBill(that,props,madeData,'',props.getSearchParam('c'));
            break;
        default:
            break;
    }
};
export default tableButtonClick;
