import {ajax,cacheTools, toast, cardCache} from 'nc-lightapp-front';
import { tableId, searchId, dataSource } from '../constants';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';
let { setDefData, getDefData } = cardCache;
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    if(searchVal){
        cacheTools.set(props.getSearchParam('c')+this.searchId, searchVal);
        let transtype = getContext(loginContextKeys.transtype);
        let pageInfo = props.table.getTablePageInfo(this.tableId);
        let queryInfo = props.search.getQueryInfo(this.searchId);
        queryInfo.pageInfo = pageInfo;
        let data = {
            pageId: props.getSearchParam('p'),
            queryInfo:queryInfo,
            tradeType:transtype
        }; 
        setDefData(searchId, dataSource, data);//放入缓存
        ajax({
            url: '/nccloud/arap/recbill/query.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        toast({ color: 'success', content: this.state.json['receivablebill-000031']+data[this.tableId].allpks.length+this.state.json['receivablebill-000032'] });/* 国际化处理： 查询成功，共,条*/
                        this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                    }else{
                        toast({ color: 'warning', content: this.state.json['receivablebill-000033'] });/* 国际化处理： 未查询出符合条件的数据*/
                        this.props.table.setAllTableData(this.tableId, {rows:[]});
                    }
                    setDefData(this.tableId, dataSource, data);//放入缓存
                    this.onSelected()
                }
            }
        });
    }
    
};
