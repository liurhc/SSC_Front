import searchBtnClick from './searchBtnClick';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import doubleClick from './doubleClick';
import initTemplate from './initTemplate';
export { searchBtnClick, pageInfoClick,buttonClick,doubleClick,initTemplate };
