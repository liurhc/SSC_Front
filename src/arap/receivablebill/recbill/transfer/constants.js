
/**
 * 查询区域
 */
export const searchId = '20060RBM_query';

/**
 * 表头区域
 */
export const headId = '20060RBM_list';

/**
 * 表体区域
 */
export const bodyId = '20060RBM_bodys';


export const pagecode = '20060RBM_TRANSFER';

//主子拉平展示页面编码
export const mainPageCode = '20060RBM_TRANSFERMAIN';

// 主子拉平显示区域编码
export const mainCode =  'transfermain'

/**
 * /默认单据类型
 */
export const billType = 'F0';

/**
 * 转单单页应用缓存,命名规范为："领域名.模块名.节点名.自定义名"。
 */
export const dataSource = 'fi.arap.recbill.transfer';
