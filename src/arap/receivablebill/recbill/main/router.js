import { asyncComponent } from 'nc-lightapp-front';
import List from '../list';

//应收单卡片
const card = asyncComponent(() => import(/*webpackChunkName:"/arap/receivablebill/recbill/card/card"*/  /* webpackMode: "eager" */ '../card'));
//预核销
const prev = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/preverify/list/prev"*/ /* webpackMode: "eager" */ '../../../verificationsheet/preverify/list'));
//及时核销list
const nowv = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/verifynow/list/nowv"*/ /* webpackMode: "eager" */ '../../../verificationsheet/verifynow/list'));
//及时核销card
const nowvcard = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/verifynow/nowvcard"*/ /* webpackMode: "eager" */ '../../../verificationsheet/verifynow/nowvcard'));
//应付单转单
const F1 = asyncComponent(() => import(/*webpackChunkName:"/arap/payablebill/payablebill/transfer/f1transfer"*/ /* webpackMode: "eager" */ '../../../payablebill/payablebill/transfer'));
//收款单
const F2 = asyncComponent(() => import(/*webpackChunkName:"/arap/gatheringbill/gatheringbill/transfer/f2transfer"*/ /* webpackMode: "eager" */ '../../../gatheringbill/gatheringbill/transfer'));
//收款合同
const FCT2 = asyncComponent(() => import(/*webpackChunkName:"/fct/bill/ar/transfer/fct2transfer"*/ /* webpackMode: "eager" */ '../../../../fct/bill/ar/transfer'));
//保险索赔 4A49
const compensation = asyncComponent(() => import(/*webpackChunkName:"/aum/insur/compensationtransfer/source/source"*/ /* webpackMode: "eager" */ '../../../../aum/insur/compensationtransfer/source'));
//资产处置 4A18
const sale = asyncComponent(() => import(/*webpackChunkName:"/aum/reduce/saletransfer/source/source"*/ /* webpackMode: "eager" */ '../../../../aum/reduce/saletransfer/source'));


const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: card
	},
	{
		path: '/prev',
		component: prev
	},
	{
		path: '/nowv',
		component: nowv
	},
	{
		path: '/nowvcard',
		component: nowvcard
	},
	{
		path: '/F1',
		component: F1
	},
	{
		path: '/F2',
		component: F2
	},
	{
		path: '/FCT2',
		component: FCT2
	},
	{
		path: '/4A49',
		component: compensation
	},
	{
		path: '/4A18',
		component: sale
	},

];

export default routes;


