import { ajax, promptBox, toast } from 'nc-lightapp-front';
import { tableId, formId, billType, leftarea, nodekey } from '../constants';
import { currentTypeAfterFormEvents } from '../../../../public/components/pubUtils/currentTypeAfterEvent';
import { autoAddLineKeys } from '../../../../public/components/pubUtils/billPubInfo';
import { getColvalues, getRowIds } from '../../../../public/components/pubUtils/billPubUtil';
import { formulamsgHint, renderData, headAfterEventRenderData, bodyAfterEventRenderData, errorDeal } from '../../../../public/components/afterEventPub/afterEventPubDeal';
import { moneyAndRateFields } from '../../../../public/components/pubUtils/specialFieldAfterEvent.js';
import { accAdd } from '../../../../public/components/pubUtils/method';

export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
	if (changedrows instanceof Array) {
		if (changedrows[0].newvalue.value == changedrows[0].oldvalue.value) {
			return;
		}
	}
	let pagecode = this.getPagecode();
	let index = 0;
	if (moduleId == this.formId) {
		index = 0;
	} else if (moduleId == this.tableId) {
		index = i;
	}

	//应收单编辑后事件
	if (moduleId == formId) {
		let data = null
		switch (key) {
			case 'pk_org':
				if (value.value == null || value.value == '') {
					if (this.props.getUrlParam('type') === 'transfer') {
						promptBox({
							color: 'warning',
							title: this.state.json['receivablebill-000009'] /* 国际化处理： 确认修改*/,
							content: this.state.json['receivablebill-000071'] /* 国际化处理： 来源于上游的单据不允许清空财务组织！*/,
							beSureBtnName: this.state.json['receivablebill-000011'] /* 国际化处理： 确定*/,
							noCancelBtn: true,
							beSureBtnClick: () => {
								this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
							},
							closeByClickBackDrop: false
						});
					} else {
						promptBox({
							color: 'warning',
							title: this.state.json['receivablebill-000009'] /* 国际化处理： 确认修改*/,
							content: this.state.json['receivablebill-000010'] /* 国际化处理： 确定​修改组织，这样会清空您录入的信息?*/,
							beSureBtnName: this.state.json['receivablebill-000011'] /* 国际化处理： 确定*/,
							cancelBtnName: this.state.json['receivablebill-000012'] /* 国际化处理： 取消*/,
							beSureBtnClick: () => {
								this.props.form.EmptyAllFormValue(this.formId);
								this.props.cardTable.setTableData(this.tableId, { rows: [] });
								this.initAdd(true);
							},
							cancelBtnClick: () => {
								this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
							},
							closeByClickBackDrop: false
						});
					}
				} else if (changedrows.value != null && changedrows.value != '') {
					//切换组织
					promptBox({
						color: 'warning',
						title: this.state.json['receivablebill-000009'] /* 国际化处理： 确认修改*/,
						content: this.state.json['receivablebill-000010'] /* 国际化处理： 确定​修改组织，这样会清空您录入的信息?*/,
						beSureBtnName: this.state.json['receivablebill-000011'] /* 国际化处理： 确定*/,
						cancelBtnName: this.state.json['receivablebill-000012'] /* 国际化处理： 取消*/,
						beSureBtnClick: () => {
							ajax({
								url: '/nccloud/arap/recbill/cardHeadAfterEdit.do',
								data: {
									pageId: pagecode,
									event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
									uiState: this.props.getUrlParam('status')
								},
								async: false,
								success: (res) => {
									//渲染数据
									renderData(this, res);
									//编辑公式提示
									formulamsgHint(this, res);
								},
								error: (res) => {
									errorDeal(this, res, changedrows);
								}
							});
						},
						cancelBtnClick: () => {
							this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
						},
						closeByClickBackDrop: false
					});
				} else {
					data = {
						pageId: pagecode,
						event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
						uiState: this.props.getUrlParam('status')
					}
					headFieldAfterRequest.call(this, data, key, changedrows)
				}
				break;
			case 'customer':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'payaccount', 'pk_currtype', 'customer', 'buysellflag',
						'objtype', "direction", "pk_billtype", "top_billtype"].concat(moneyAndRateFields)),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_deptid_v':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_deptid', 'pk_deptid_v', 'pk_currtype', 'customer', 'taxtype', 'buysellflag', 'objtype', "direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_psndoc':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_psndoc', 'pk_deptid', 'pk_deptid_v', 'isrefused', 'prepay', 'isdiscount', 'objtype', 'direction', 'agentreceivelocal']),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_currtype':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ["pk_org","pk_group","pk_currtype","pk_billtype","billdate","rate","grouprate","globalrate",
					"buysellflag","taxprice","local_taxprice","taxrate","occupationmny","money_bal","local_money_bal",
					"globaldebit","globalnotax_de","globaltax_de","groupdebit","groupnotax_de",
					"grouptax_de","local_money_de","local_notax_de","local_tax_de","money_de","notax_de","quantity_de","direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows)
				//币种事件发送完要判断汇率
				currentTypeAfterFormEvents(this.formId, props, key);
				break;
			default:
				data = {
					pageId: pagecode,
					event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
		}
	}

	if (moduleId == tableId) {
		//应收单存在合并计算税的功能，所以把一些合并值传过去
		let colValues =getColvalues(this.props, this.tableId, GROUP_KEYS_LIST);
		ajax({
			url: '/nccloud/arap/recbill/cardBodyAfterEdit.do',
			data: {
				rowindex: 0,
				editindex: index,
				pageId: pagecode,
				changedrows: changedrows,
				tableId:this.tableId,
				body: props.cardTable.getDataByIndex(this.tableId, index),
				formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
				uiState: this.props.getUrlParam('status'),
				colValues :colValues
			},
			async: false,
			success: (res) => {
				//渲染数据
				bodyAfterEventRenderData(this, res);
				if (this.props.getUrlParam('type') == 'transfer') {
					this.synTransferData();
				}
				//表体改变表头税率编辑性
				if (i == 0 && key == 'pk_currtype') {
					currentTypeAfterFormEvents(this.formId, props, 'pk_currtype');
				}
				//编辑公式提示
				formulamsgHint(this, res);

			},
			error: (res) => {
				let str = String(res);
				let content = str.substring(6, str.length);
				if (content.substring(1, 17) == 'convertException') {
					promptBox({
						color: 'warning',
						title: this.state.json['receivablebill-000015'] /* 国际化处理： 折算误差*/,
						content: content.substring(17, content.length),
						closeByClickBackDrop: false,
						beSureBtnClick: () => {
							afterTableEvent(this, props, i, pagecode, moduleId, key, changedrows, 'sure', index,value);
						},
						cancelBtnClick: () => {
							afterTableEvent(this, props, i, pagecode, moduleId, key, changedrows, 'cancel', index,value);
						}
					});
				} else {
					this.props.cardTable.setValByKeyAndRowId(this.tableId, i, key, changedrows);
					toast({ color: 'danger', content: content });
				}
			}
		});

		//自动增行
		autoAddline.call(this, moduleId, pagecode, key, i)
	}
}
//客户，税码，税率，扣税类别，购销类型，借方原币金额，借方本币金额，借方原币无税金额，税额
let GROUP_KEYS_LIST = ['customer','taxcodeid','taxrate','taxtype','buysellflag','money_de',
						'local_money_de','notax_de','local_notax_de','local_tax_de',];



function afterTableEvent(that, props, i, pagecode, moduleId, key, changedrows, isCalculateConvert, index,value) {
	//应收单存在合并计算税的功能，所以把一些合并值传过去
	let colValues =getColvalues(that.props, that.tableId, GROUP_KEYS_LIST);
	ajax({
		url: '/nccloud/arap/recbill/cardBodyAfterEdit.do',
		data: {
			rowindex: 0,
			editindex: index,
			pageId: pagecode,
			changedrows: changedrows,
			tableId:that.tableId,
			body: props.cardTable.getDataByIndex(that.tableId, index),
			formEvent: props.createFormAfterEventData(pagecode, that.formId, that.tableId, key, value),
			uiState: that.props.getUrlParam('status'),
			isCalculateConvert: isCalculateConvert,
			colValues :colValues
		},
		async: false,
		success: (res) => {
			//渲染数据
			bodyAfterEventRenderData(that, res);
			//表体改变表头税率编辑性
			if (i == 0 && key == 'pk_currtype') {
				currentTypeAfterFormEvents(that.formId, props, 'pk_currtype');
			}
			if (that.props.getUrlParam('type') == 'transfer') {
				that.synTransferData();
			}
			//编辑公式提示
			formulamsgHint(that, res);
		}
	});
}

export function headFieldAfterRequest(requestData, key, changedrows) {
	ajax({
		url: '/nccloud/arap/recbill/cardHeadAfterEdit.do',
		data: requestData,
		async: false,
		success: (res) => {
			//渲染数据
			headAfterEventRenderData(this, res);
			//编辑公式提示
			formulamsgHint(this, res);

			if (key == 'pk_org') {
				let pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
				if (pk_org) {
					this.props.resMetaAfterPkorgEdit();
					this.state.buttonfalg = true;
				} else {
					this.state.buttonfalg = null;
				}
				this.toggleShow();
			}

			if (this.props.getUrlParam('type') == 'transfer') {
				this.synTransferData();
			}
		},
		error: (res) => {
			errorDeal(this, res, changedrows);
		}
	});
}

export function autoAddline(moduleId, pagecode, key, i) {
	//自动增行
	let allRowsNumber = this.props.cardTable.getNumberOfRows(this.tableId);
	if (moduleId == this.tableId && allRowsNumber == i + 1 && autoAddLineKeys.indexOf(key) != -1) {
		let data = this.props.createMasterChildData(pagecode, this.formId, this.tableId);
		//清空cardData的表体
		data.body[this.tableId].rows = [];
		ajax({
			url: '/nccloud/arap/recbill/addline.do',
			data: data,
			async: false,
			success: (res) => {
				if (res.data) {
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.addRow(this.tableId, allRowsNumber, res.data.body[this.tableId].rows[0].values, false);
					}
				}
			}
		});
	}
}




