import buttonClick from './buttonClick';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import initTemplate from './initTemplate';
import transferButtonClick from './transferButtonClick';
export { buttonClick, afterEvent, pageInfoClick, initTemplate,transferButtonClick };
