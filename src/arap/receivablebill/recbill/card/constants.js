
/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 表体区域
 */
export const tableId = 'bodys';

/**
 * 转单界面左侧列表区域
 */
export const leftarea = 'left';

/**
 * 默认模板节点标识
 */
export const nodekey = 'card';

/**
 * /默认单据类型
 */
export const billType = 'F0';
/**
 * 默认交易类型
 */
export const tradeType = 'D0';

/**
* 单页应用缓存,命名规范为："领域名.模块名.节点名.自定义名"。 
*/
export const dataSource = 'fi.arap.recbill.20060RBM';

/**
 * 单页应用缓存主键名字
 */
export const pkname  = 'pk_recbill';

/**
 * 列表联查页面pageId
 */
export const linkPageId = '20060RBM_LIST_LINK';
