import React, { Component } from 'react';
import { createPage,getMultiLang } from 'nc-lightapp-front';
import ReactDOM from 'react-dom';
import Closeaccbook from '../../../../uapbd/orgcloseacc/closeaccbook/main';
/**
* 财务组织关账
*/
export default class Closeaccbook_uapbd extends Component{
    constructor(props){
        super(props);
        this.state = {
            json: {}
        }
    }
    componentWillMount() {
		let callback = (json) => {
			this.setState({json:json});
		}
		getMultiLang({moduleId:'receivablebill',domainName :'arap',currentLocale:'simpchn',callback});
	}
    render(){
        /**
         *  后面还要考虑 多语 的情况
         * @type {{title: string, showMoudles: Object}}
         */
        let config = {
            title : this.state.json['receivablebill-000002'],//显示节点名称/* 国际化处理： 应收关账*/
            appCode : this.props.getSearchParam('c'),
            showMoudles : { //传入需要显示的模块 key-value 只需传入本领域对应的可以即可，value为true
                //'2002'  : true,//2002总账
                '2006'  : true,//2006应收管理
                // '2008'  : true,//2008应付管理
                // '2011'  : true,//2011费用管理
                // '3607'  : true,//3607现金管理
                // '2014'  : true,//2014存货核算
                // '3830'  : true,//3830产品成本
                // '3840'  : true,//3840项目成本会计
                // '4008'  : true,//4008库存
                // 'STOREDOC' : true//STOREDOC仓库
            }
        }
        return (
            <Closeaccbook {...{config:config}}/>
        )
    }
}
Closeaccbook_uapbd = createPage({appAutoFocus: false})(Closeaccbook_uapbd);
ReactDOM.render(<Closeaccbook_uapbd />, document.querySelector('#app'));
