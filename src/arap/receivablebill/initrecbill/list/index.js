//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, cacheTools, high, print, toast, cardCache, getMultiLang,createPageIcon } from 'nc-lightapp-front';
let { NCTabsControl, NCButton } = base;
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	tableModelConfirm,
	doubleClick,
	tableButtonClick
} from './events';
import { tableId, searchId, nodekey, billType, pkname, dataSource, searchKey } from './constants';
import {
	buttonVisible,
	getButtonsKey,
	onListButtonControl
} from '../../../public/components/pubUtils/buttonvisible.js';
import PeriodModal from '../../../public/components/periodModel';
import CombinedExaminationModel from '../../../public/components/combinedExaminationModel'; //联查处理情况
import LinkTerm from '../../../public/components/linkTerm'; //联查收付款协议
const { BillTrack, PrintOutput, Refer, NCUploader } = high;
const { ExcelImport } = high;
let { setDefData, getDefData } = cardCache;
import { loginContext, getContext, loginContextKeys } from '../../../public/components/arapInitInfo/loginContext';
import TradeTypeButton from '../../../public/components/tradetype'; //交易类型按钮
import afterEvent from '../../../public/components/searchAreaAfterEvent'; //查询区编辑后事件
import '../../../public/less/tradetype.less';

class List extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.searchId = searchId;
		this.tableId = tableId;
		this.pageId = props.getSearchParam('p');
		this.billType = billType;
		let { form, button, table, insertTable, search } = this.props;
		let { setSearchValue, setSearchValByField, getAllSearchData } = search;
		this.setSearchValByField = setSearchValByField; //设置查询区某个字段值
		this.getAllSearchData = getAllSearchData; //获取查询区所有字段数据
		this.state = {
			isPeriodModelShow: false,
			isCombinedExaminationModelShow: false, //联查处理情况模态框
			isLinkTermModelShow: false, //联查收付款协议模态框
			showBillTrack: false, //单据追溯模态框的显示状态
			showUploader: false, //附件管理
			target: null, //附件管理
			json: {}
		};
		this.printData = {
			billtype: billType, //单据类型
			appcode: props.getSearchParam('c') ? props.getSearchParam('c') : props.getUrlParam('c'), //功能节点编码，即模板编码
			nodekey: nodekey, //模板节点标识
			oids: null, // 功能节点的数据主键
			userjson: billType //单据类型,billtype不是必需字段，后台没有设置接收字段，以userjson代替
		};
		this.outputData = {
			billtype: billType, //单据类型
			appcode: props.getSearchParam('c') ? props.getSearchParam('c') : props.getUrlParam('c'), //功能节点编码，即模板编码
			funcode: props.getSearchParam('c') ? props.getSearchParam('c') : props.getUrlParam('c'),
			nodekey: nodekey, //模板节点标识
			oids: null, // 功能节点的数据主键
			userjson: billType, //单据类型,billtype不是必需字段，后台没有设置接收字段，以userjson代
			outputType: 'output'
		};
		this.Info = {
			allButtonsKey: [], //保存所有头部按钮
			combinedExaminationData: [], //联查处理情况模态框表格数据
			pk_tradetypeid: null, //当前选中第一行行的交易类型
			billno: null, //当前选中第一行的单据号
			linkTermData: [], //联查收付款协议模态框表格数据
			pk_bill: null, //当前选中第一行的主键pk
			approvestatus: null, //当前选中第一行的审批状态
			index: null, //保存当前选中第一行的行号index
			pk_tradetype: null, //当前选中第一行行的交易类型code
			selectedPKS: [] //导出数据的主键pk
		};
	}

	componentDidMount() {}

	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				initTemplate.call(this, this.props, this.initShow);
			});
		};
		getMultiLang({
			moduleId: [ 'receivablebill' ],
			domainName: 'arap',
			currentLocale: 'simpchn',
			callback
		});
	}

	getPagecode = () => {
		let pagecode = '20060ARO_LIST';
		return pagecode;
	};

	initShow = () => {
		if (!this.props.table.hasCacheData(dataSource)) {
			this.onSelected(); //缓存不存在，就控制按钮
		}
	};

	componentWillReceiveProps(nextProps) {}
	//联查处理情况按钮
	handleCombined = () => {
		this.setState({
			isCombinedExaminationModelShow: !this.state.isCombinedExaminationModelShow
		});
	};
	//联查收付款协议
	handleLinkTerm = () => {
		this.setState({
			isLinkTermModelShow: !this.state.isLinkTermModelShow
		});
	};
	//设置当前选中行数据的ts的值
	setFormTsVal = (ts) => {
		if (this.Info.index) {
			let tsCell = { value: ts, display: '', scale: -1 };
			this.props.table.setValByKeyAndIndex(this.tableId, this.Info.index, 'ts', tsCell);
		}
	};

	//期初弹窗控制
	handlePeriodInformation = () => {
		this.setState({
			isPeriodModelShow: !this.state.isPeriodModelShow
		});
	};

	//打印
	onPrint = () => {
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/arap/arappub/print.do', //后台服务url
			this.printData,
			false
		);
	};

	//正式打印
	officalPrintOutput = () => {
		ajax({
			url: '/nccloud/arap/arappub/officialPrint.do',
			async: false,
			data: this.printData,
			success: (res) => {
				if (res.success) {
					print(
						'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
						'/nccloud/arap/arappub/print.do', //后台服务url
						this.printData,
						false
					);
				}
			}
		});
	};
	//取消正式打印
	cancelPrintOutput = () => {
		ajax({
			url: '/nccloud/arap/arappub/cancelPrint.do',
			async: false,
			data: this.printData,
			success: (res) => {
				if (res.success) {
					//取消正式打印提示框
					toast({ color: 'success', content: res.data });
				}
			}
		});
	};

	//打印输出
	printOutput = () => {
		this.refs.printOutput.open();
	};
	//输出成功回调函数
	onSubmit() {}
	beforeUpload(billId, fullPath, file, fileList) {
		// 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表
		const isJPG = true;
		if (!isJPG) {
			alert(this.state.json['receivablebill-000022']); /* 国际化处理： 只支持png格式图片*/
		}
		const isLt2M = file.size / 1024 / 1024 < 10;
		if (!isLt2M) {
			alert(this.state.json['receivablebill-000023']); /* 国际化处理： 上传大小小于10M*/
		}
		return isJPG && isLt2M;
		// 备注： return false 不执行上传  return true 执行上传
	}
	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		});
	};
	//列表控制按钮
	onSelected = () => {
		onListButtonControl(this);
	};

	// 查询区渲染完成回调函数
	renderCompleteEvent = () => {
		let cachesearch = getDefData(searchKey, dataSource);
		if (cachesearch && cachesearch.conditions) {
			// this.props.search.setSearchValue(this.searchId, cachesearch);
			for (let item of cachesearch.conditions) {
				if (item.field == 'billdate') {
					// 时间类型特殊处理
					let time = [];
					time.push(item.value.firstvalue);
					time.push(item.value.secondvalue);
					this.props.search.setSearchValByField(this.searchId, item.field, {
						display: item.display,
						value: time
					});
				} else {
					this.props.search.setSearchValByField(this.searchId, item.field, {
						display: item.display,
						value: item.value.firstvalue
					});
				}
			}
		}
	};

	render() {
		let { table, search, modal } = this.props;
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { showUploader, target } = this.state;
		let { createModal } = modal;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon ? createPageIcon() : null}
						<h2 className="title-search-detail"> {this.state.json['receivablebill-000028']} </h2>
						{/* 国际化处理： 应收期初*/}
					</div>
					<div className="header-button-area">
						{!getContext(loginContextKeys.transtype) ? (
							<div className="trade-type">
								{TradeTypeButton({
									ref: 'tradetypeBtn',
									billtype: 'F0',
									dataSource: dataSource
								})}
							</div>
						) : null}
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(
						this.searchId, //模块id
						{
							clickSearchBtn: searchBtnClick.bind(this), //   点击按钮事件
							showAdvBtn: true, //  显示高级按钮
							onAfterEvent: afterEvent.bind(this), //编辑后事件
							renderCompleteEvent: this.renderCompleteEvent // 查询区渲染完成回调函数
						}
					)}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						dataSource: dataSource,
						pkname: pkname,
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						showCheck: true,
						showIndex: true,
						onSelected: this.onSelected.bind(this),
						onSelectedAll: this.onSelected.bind(this),
						onRowDoubleClick: doubleClick.bind(this),
						componentInitFinished: () => {
							this.onSelected();
						}
					})}
				</div>
				<PeriodModal
					show={this.state.isPeriodModelShow}
					handleModel={this.handlePeriodInformation.bind(this)}
					billFieldType={'AR_INIT_RECEIVE'}
					funCode={'20060ARO'}
				/>
				{/* {联查处理情况} */}
				<CombinedExaminationModel
					show={this.state.isCombinedExaminationModelShow}
					combinedExaminationData={this.Info.combinedExaminationData}
					pk_tradetypeid={this.Info.pk_tradetypeid}
					billno={this.Info.billno}
					handleModel={this.handleCombined.bind(this)}
				/>

				{/* {联查收付款协议} */}
				<LinkTerm
					show={this.state.isLinkTermModelShow}
					linkTermData={this.Info.linkTermData}
					handleModel={this.handleLinkTerm.bind(this)}
					moduleId={'2006'}
					billType={this.billType}
					pk_bill={this.Info.pk_bill}
					setFormTsVal={this.setFormTsVal.bind(this)}
				/>
				{showUploader && (
					<NCUploader
						billId={this.Info.pk_bill}
						billNo={this.Info.billno}
						target={target}
						placement={'bottom'}
						beforeUpload={this.beforeUpload}
						onHide={this.onHideUploader}
					/>
				)}
				{/* 单据追溯组件 */}
				<BillTrack
					show={this.state.showBillTrack}
					close={() => {
						this.setState({ showBillTrack: false });
					}}
					pk={this.Info.pk_bill} //单据id
					type={this.Info.pk_tradetype} //单据类型
				/>

				{/* {打印输出} */}
				<PrintOutput
					ref="printOutput"
					url="/nccloud/arap/arappub/outPut.do"
					data={this.outputData}
					callback={this.onSubmit}
				/>
				{/* {导入} */}
				{createModal('importModal', {
					noFooter: true,
					className: 'import-modal',
					hasBackDrop: false
				})}
				<ExcelImport
					{...Object.assign(this.props)}
					moduleName="arap" //模块名
					billType={billType} //单据类型
					pagecode="20060ARO_CARD"
					appcode={
						this.props.getSearchParam('c') ? this.props.getSearchParam('c') : this.props.getUrlParam('c')
					}
					selectedPKS={this.Info.selectedPKS} //导出主键
					exportTreeUrl={"/nccloud/arap/recbill/recbillexport.do"}
					//referVO={{ignoreTemplate:true}}
				/>
			</div>
		);
	}
}

List = createPage({
	mutiLangCode: '2052'
})(List);

export default List;
