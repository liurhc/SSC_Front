import { createPage, ajax, base, toast, cardCache, excelImportconfig } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;
import tableButtonClick from './tableButtonClick.js';
import { tableId, tradeType, searchId, billType, searchKey, dataSource } from '../constants';
import { buttonVisible, getButtonsKey, getInnerButtonkey } from '../../../../public/components/pubUtils/buttonvisible.js';
import { modifierSearchMetas } from '../../../../public/components/pubUtils/arapListSearchRefFilter';
import { headButton, innerButton } from '../../../../public/components/pubUtils/buttonName.js';
let { setDefData, getDefData } = cardCache;
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';
import setDefOrgBilldateSrchArea from '../../../../public/components/defOrgBilldateSrchArea.js';
import {OperationColumn} from '../../../../public/components/pubUtils/arapConstant';
export default function (props,callback) {
	const that = this;
	let excelimportconfig = excelImportconfig(props, "arap", billType, true, '', {
		isinit: 'true',"appcode":props.getSearchParam('c'),"pagecode":"20060ARO_CARD"
	});
	props.createUIDom(
		{
			pagecode: that.getPagecode(),//页面id
			appcode: props.getSearchParam('c'),//注册按钮的id
			reqDataQuerypage: {
				rqUrl: '/arap/arappub/querypage.do',
                rqJson: `{\n  \"pagecode\": \"${that.getPagecode()}\",\n  \"appcode\": \"${props.getSearchParam('c')}\"\n}`,
                rqCode: 'template'
			}
		},
		function (data) {
			if (data) {
				if(!data.template[tableId]){
					return;
				}
				if (data.button) {
					let button = data.button;
					getButtonsKey(button, that.Info.allButtonsKey);//获取所有按钮
					props.button.setButtons(button);
					props.button.setUploadConfig("ImportData", excelimportconfig);
					props.button.setPopContent(innerButton.Delete_inner, that.state.json['receivablebill-000017']);/*删除信息提示框*//* 国际化处理： 确定要删除吗？*/
				}
				if (data.template) {
					let meta = data.template;
					let innerButton = getInnerButtonkey(data.button);
					//高级查询设置财务组织默认值
					setDefOrgBilldateSrchArea(props, searchId, data);
					meta = modifierMeta(props, meta, innerButton, that);
					modifierSearchMetas(searchId, props, meta, billType, null, that);
					props.meta.setMeta(meta);
				}
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
					if(getContext(loginContextKeys.transtype)){
						if (that.refs.tradetypeBtn) {
							that.refs.tradetypeBtn.setVisible(false); 
						}
					}
				}
				if(callback){
					callback()
				}

			}
		}
	)
}

function modifierMeta(props, meta, innerButton, that) {

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'billno') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							//表页跳转的时候，获取所有查询区条件，并放入缓存
							let searchVal = props.search.getAllSearchData(searchId);
							if (searchVal) {
								setDefData(searchKey, dataSource, searchVal);
							}
							props.pushTo('/card', {
								status: 'browse',
								id: record.pk_recbill.value,
								pagecode: record.pk_tradetype.value
							});
						}}
					>
						{record.billno && record.billno.value}
					</a>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableId].items.push({
		label: that.state.json['receivablebill-000020'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: OperationColumn,
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let buttonAry = innerButton ? innerButton : [];
			let trueBtn = [];
			for (let i = 0; i < buttonAry.length; i++) {
				let flag = buttonVisible('browse', record, buttonAry[i]);
				if (flag) {
					trueBtn.push(buttonAry[i]);
				}
			}
			return props.button.createOprationButton(trueBtn, {
				area: "list_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(that, props, key, text, record, index)
			});
		}
	});
	return meta;
}
