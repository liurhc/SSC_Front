
/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 表体区域
 */
export const tableId = 'bodys';

/**
 * 单据类型
 */
export const billType = 'F0';
/**
 * 默认交易类型
 */
export const tradeType = 'D0';


/**
 * 默认模板节点标识
 */
export const nodekey = "card";

/**
 * 单页应用缓存
 */
export const dataSource  = 'fi.arap.initrecbill.20060ARO';

/**
 * 单页应用缓存主键名字(该名字为表的主键名字)
 */
export const pkname  = 'pk_recbill';

