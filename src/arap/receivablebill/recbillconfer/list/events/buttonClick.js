import { ajax, toast ,promptBox,cardCache} from 'nc-lightapp-front';
import { searchId,dataSource,billType} from '../constants';
import { imageScan, imageView } from 'sscrp/rppub/components/image';
let { setDefData, getDefData } = cardCache;
export default function buttonClick(props, id) {
    let tableId = this.tableId;
    switch (id) {
        case 'Delete':
            promptBox({
                color: 'warning',
                title: this.state.json['receivablebill-000012'],                  /* 国际化处理： 取消*/
                content: this.state.json['receivablebill-000070'],             /* 国际化处理： ​确定要删除所选数据吗？*/
                noFooter: false,
                noCancelBtn: false,
                beSureBtnName: this.state.json['receivablebill-000011'],           /* 国际化处理： 确定*/
                cancelBtnName: this.state.json['receivablebill-000012'],          /* 国际化处理： 取消*/
                beSureBtnClick: () => {
                    let deleteData = getAllCheckedData(this.props, this.tableId, billType);
                    if (deleteData.length == 0) {
                        toast({ color: 'warning', content: this.state.json['receivablebill-000029'] });/* 国际化处理： 请选中至少一行数据!*/
                        return;
                    }
                    ajax({
                        url: '/nccloud/arap/arappub/confirmbatchdelete.do',
                        data: deleteData,
                        success: (res) => {
                            let { success, data } = res;
                            if (success) {
                                if (data.message) {
                                    toast({
                                        duration: 'infinity',
                                        color: data.PopupWindowStyle,
                                        content: data.message,
                                    })
                                }
                                if (data.successIndexs) {
                                    //删除当前行数据
                                    props.table.deleteTableRowsByIndex(tableId, data.successIndexs);
                                }
                                if (data.successPKs) {
                                    //删除缓存数据
                                    props.table.deleteCacheId(tableId, data.successPKs);
                                }
                                this.onSelected();
                            }
                        }
                    });

                }
            });
            
            break;
        case 'Refresh'://刷新
            let data = getDefData(searchId, dataSource);
                if (data) {
                    ajax({
                        url: '/nccloud/arap/recbillconfer/query.do',
                        data: data,
                        success: (res) => {
                            let { success, data } = res;
                            if (success) {
                                if (data) {
                                    toast({ color: 'success', title: this.state.json['receivablebill-000069'] });/* 国际化处理： 刷新成功*/
                                    this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                                } else {
                                    toast({ color: 'success', content: this.state.json['receivablebill-000053'] });/* 国际化处理： 未查询到数据*/
                                    this.props.table.setAllTableData(this.tableId, { rows: [] });
                                }
                            }
                            setDefData(this.tableId, dataSource, data); //放入缓存
                            this.onSelected();
                        }
                    });
            } else {
                toast({ color: 'success', title: this.state.json['receivablebill-000069'] });/* 国际化处理： 刷新成功*/
                this.props.table.setAllTableData(this.tableId, { rows: [] });
            }
            break;
            case 'ReceiptCheck': //影像查看pk_tradetype
			let CheckInfo = getFirstCheckedData(this.props, this.tableId);
			
			var billInfoMap = {};
  
			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = CheckInfo.data.values.pk_recbill.value;
			billInfoMap.pk_billtype = CheckInfo.data.values.pk_billtype.value;
			billInfoMap.pk_tradetype = CheckInfo.data.values.pk_tradetype.value;
			billInfoMap.pk_org = CheckInfo.data.values.pk_org.value;
			imageView(billInfoMap,  'iweb');
			break;
		case 'ReceiptScan': //影像扫描
			let ScanInfo = getFirstCheckedData(this.props, this.tableId);
			
			var billInfoMap = {};
  
			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid =  ScanInfo.data.values.pk_recbill.value;
			billInfoMap.pk_billtype =ScanInfo.data.values.pk_tradetype.value;
			billInfoMap.pk_tradetype = ScanInfo.data.values.pk_tradetype.value;
			billInfoMap.pk_org = ScanInfo.data.values.pk_org.value;
		
			//影像所需 FieldMap
			billInfoMap.BillType = ScanInfo.data.values.pk_tradetype.value;
			billInfoMap.BillDate = ScanInfo.data.values.creationtime.value;
			billInfoMap.Busi_Serial_No =ScanInfo.data.values.pk_recbill.value;
			// billInfoMap.pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
			billInfoMap.OrgNo =  ScanInfo.data.values.pk_org.value;
			billInfoMap.BillCode =  ScanInfo.data.values.billno.value==undefined?'':ScanInfo.data.values.billno.value;
			billInfoMap.OrgName = ScanInfo.data.values.pk_org.display;
			billInfoMap.Cash = ScanInfo.data.values.money.value
			imageScan(billInfoMap, 'iweb');
			break;
    }
}


//获取选中数据的id和billType
let getAllCheckedData = function (props, tableId, billType) {
    let checkedData = props.table.getCheckedRows(tableId);
    let checkedObj = [];
    checkedData.forEach((val) => {
        checkedObj.push({
            pk_bill: val.data.values.pk_recbill.value,
            ts: val.data.values.ts.value,
            billType: billType,
            index: val.index,
        }
        );
    });
    return checkedObj;
}
//获取选中数据的第一行,选中多行的时候只取第一行数据
let getFirstCheckedData = function(props, tableId) {
	let checkedData = props.table.getCheckedRows(tableId);
	let checkedObj;
	if (checkedData.length > 0) {
		checkedObj = checkedData[0];
	} else {
		toast({ color: 'warning', content: this.state.json['receivablebill-000007'] });/* 国际化处理： 请选中一行数据!*/
		return;
	}
	return checkedObj;
};
