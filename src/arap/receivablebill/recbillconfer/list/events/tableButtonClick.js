import { ajax, base, toast ,cardCache} from 'nc-lightapp-front';
import { dataSource,tableId,billType} from '../constants';
let {setDefData, getDefData } = cardCache;
export default function(that,props, key, text, record, index){
    switch (key) {
        // 表格操确认
        case 'Confirm_inner':
            ajax({
                url: '/nccloud/arap/arappub/confirm.do',
                data: {
                    pageId: record.pk_tradetype.value,
                    pk_bill: record.pk_recbill.value,
                    ts: record.ts.value,
                    billType : billType
                },
                success: (res) => {
                    if(res.success){
                        setDefData('confirm'+record.pk_recbill.value, dataSource, res.data);
                        props.pushTo('/card', {
                            status: 'edit',
                            id: record.pk_recbill.value,
                            pagecode:record.pk_tradetype.value
                        })

                    }
                }
            });
            break;
        case 'CancelConfirm_inner':
            ajax({
                url: '/nccloud/arap/arappub/cancelconfirm.do',
                data: {
                    pageId: props.getSearchParam("p"),
                    pk_bill: record.pk_recbill.value,
                    ts: record.ts.value,
                    billType : billType,
                    type :1
                },
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        toast({ color: 'success', content: that.state.json['receivablebill-000066'] });/* 国际化处理： 取消确认成功*/
                        //更新当前行数据
                        props.table.updateDataByIndexs(tableId, 
                            [{index:index ,data: {values :data[tableId].rows[0].values}}]);
                    }
                }
            });
            break;
        case 'Delete_inner':
            props.button.setPopContent('Delete_inner','确认删除该信息吗？');/*删除信息提示框*/
            ajax({
                url: '/nccloud/arap/arappub/confirmdelete.do',
                data: {
                        pk_bill: record.pk_recbill.value,
                        ts: record.ts.value,
                        billType: billType
                    },
                success: (res) => {
                    if (res.success) {
                        toast({color: 'success', content: that.state.json['receivablebill-000034']});/* 国际化处理： 删除成功*/
                        //删除当前行数据
                        props.table.deleteTableRowsByIndex(tableId, index);
                        let {deleteCacheId} = props.table;
                        //删除缓存数据
                        deleteCacheId(tableId,record.pk_recbill.value);
                        that.onSelected();
                    }
                }
            });
            break;
        default:
            break;
    }
};
