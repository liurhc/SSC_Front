import { ajax, toast, promptBox } from 'nc-lightapp-front';
import { currentTypeAfterFormEvents } from '../../../../public/components/pubUtils/currentTypeAfterEvent';
import { formulamsgHint, renderData, headAfterEventRenderData, bodyAfterEventRenderData, errorDeal } from '../../../../public/components/afterEventPub/afterEventPubDeal';
import { getColvalues, getRowIds } from '../../../../public/components/pubUtils/billPubUtil';
import { moneyAndRateFields } from '../../../../public/components/pubUtils/specialFieldAfterEvent.js';

export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
	if (changedrows instanceof Array) {
		if (changedrows[0].newvalue.value == changedrows[0].oldvalue.value) {
			return;
		}
	}
	let pagecode = this.getPagecode();
	let index = 0;
	if (moduleId == this.formId) {
		index = 0;
	} else if (moduleId == this.tableId) {
		index = i;
	}
	//表头编辑后事件
	if (moduleId == this.formId) {
		let data = null
		switch (key) {
			case 'customer':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'payaccount', 'pk_currtype', 'customer', 'buysellflag',
						'objtype', "direction", "pk_billtype", "top_billtype"].concat(moneyAndRateFields)),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_deptid_v':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_deptid', 'pk_deptid_v', 'pk_currtype', 'customer', 'taxtype', 'buysellflag', 'objtype', "direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_psndoc':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_psndoc', 'pk_deptid', 'pk_deptid_v', 'isrefused', 'prepay', 'isdiscount', 'objtype', 'direction', 'agentreceivelocal']),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_currtype':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ["pk_org","pk_group","pk_currtype","pk_billtype","billdate","rate","grouprate","globalrate",
					"buysellflag","taxprice","local_taxprice","taxrate","occupationmny","money_bal","local_money_bal",
					"globaldebit","globalnotax_de","globaltax_de","groupdebit","groupnotax_de",
					"grouptax_de","local_money_de","local_notax_de","local_tax_de","money_de","notax_de","quantity_de","direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows)
				//币种事件发送完要判断汇率
				currentTypeAfterFormEvents(this.formId, props, key);
				break;
			default:
				data = {
					pageId: pagecode,
					event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
		}
	}
	//表体编辑后事件
	if (moduleId == this.tableId) {
		ajax({
			url: '/nccloud/arap/recbillconfer/cardBodyAfterEdit.do',
			data: {
				rowindex: 0,
				editindex: index,
				pageId: pagecode,
				changedrows: changedrows,
				tableId:this.tableId,
				body: props.cardTable.getDataByIndex(this.tableId, index),
				formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
				uiState: this.props.getUrlParam('status')
			},
			async: false,
			success: (res) => {
				//渲染数据
				bodyAfterEventRenderData(this, res);
				//编辑公式提示
				formulamsgHint(this, res);
				//表体改变表头税率编辑性
				if (i == 0 && key == 'pk_currtype') {
					currentTypeAfterFormEvents(this.formId, props, 'pk_currtype');
				}

			},
			error: (res) => {
				let str = String(res);
				let content = str.substring(6, str.length);
				if (content.substring(1, 17) == 'convertException') {
					promptBox({
						color: 'warning',
						title: this.state.json['receivablebill-000015'] /* 国际化处理： 折算误差*/,
						content: content.substring(17, content.length),
						closeByClickBackDrop: false,
						beSureBtnClick: () => {
							afterTableEvent(this, props, i, pagecode, moduleId, key, changedrows, 'sure', index,value);
						},
						cancelBtnClick: () => {
							afterTableEvent(this, props, i, pagecode, moduleId, key, changedrows, 'cancel', index,value);
						}
					});
				} else {
					this.props.cardTable.setValByKeyAndRowId(this.tableId, i, key, changedrows);
					toast({ color: 'danger', content: content });
				}
			}
		});
	}
}

function afterTableEvent(that, props, i, pagecode, moduleId, key, changedrows, isCalculateConvert, index,value) {

	ajax({
		url: '/nccloud/arap/recbillconfer/cardBodyAfterEdit.do',
		data: {
			rowindex: 0,
			editindex: index,
			pageId: pagecode,
			changedrows: changedrows,
			tableId:that.tableId,
			body: props.cardTable.getDataByIndex(that.tableId, index),
			formEvent: props.createFormAfterEventData(pagecode, that.formId, that.tableId, key, value),
			uiState: that.props.getUrlParam('status'),
			isCalculateConvert: isCalculateConvert
		},
		async: false,
		success: (res) => {
			//渲染数据
			bodyAfterEventRenderData(that, res);

			//表体改变表头税率编辑性
			if (i == 0 && key == 'pk_currtype') {
				currentTypeAfterFormEvents(that.formId, props, 'pk_currtype');
			}
			//编辑公式提示
			formulamsgHint(that, res);

		}
	});
}

export function headFieldAfterRequest(requestData, key, changedrows) {
	ajax({
		url: '/nccloud/arap/recbillconfer/cardHeadAfterEdit.do',
		data: requestData,
		async: false,
		success: (res) => {
			//渲染数据
			headAfterEventRenderData(this, res);
			//编辑公式提示
			formulamsgHint(this, res);
		},
		error: (res) => {
			errorDeal(this, res, changedrows);
		}
	});
}