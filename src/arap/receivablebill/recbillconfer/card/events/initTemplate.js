
import tableButtonClick from './tableButtonClick.js';
import { tableId, formId} from '../constants';
import { buttonVisible, getButtonsKey,getInnerButtonkey ,cardBodyAndInnerButtonVisible} from '../../../../public/components/pubUtils/buttonvisible.js';
import {OperationColumn} from '../../../../public/components/pubUtils/arapConstant';
export default function(props,callback) {
	const that = this;
	props.createUIDom(
		{
			pagecode: that.getPagecode(),//页面id
			appcode: props.getSearchParam('c'),//注册按钮的id
			reqDataQuerypage: {
				rqUrl: '/arap/arappub/querypage.do',
                rqJson: `{\n  \"pagecode\": \"${that.getPagecode()}\",\n  \"appcode\": \"${props.getSearchParam('c')}\"\n}`,
                rqCode: 'template'
			}
		}, 
		function (data){
			if(data){
				if(!data.template[tableId]){
					return;
				}
				if(data.template){
					let meta = data.template;
					meta = modifierMeta(props, meta,that);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					getButtonsKey(button, that.Info.allButtonsKey);//保存所有头部和肩部按钮
					props.button.setButtons(button);
				}
				if(callback){
					callback()
				}
			}   
		}
	)
}

function modifierMeta(props, meta,that) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	//添加操作列
	meta[tableId].items.push({
		label: that.state.json['receivablebill-000020'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: OperationColumn,
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let status = props.cardTable.getStatus(tableId);
			let trueBtn = status == 'browse'?['open_browse']:['open_edit'];
			return props.button.createOprationButton(trueBtn, {
				area: "card_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index,that)
			});
		}
	});
	return meta;
}
