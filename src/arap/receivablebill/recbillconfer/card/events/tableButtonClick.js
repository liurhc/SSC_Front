import { ajax, base, toast } from 'nc-lightapp-front';
export default function (props, key, text, record, index,that) {
    switch (key) {
        // 表格操修改
        case 'open_browse':
            props.cardTable.toggleRowView(that.tableId, record);
            break;
        case 'open_edit':
            props.cardTable.openModel(that.tableId, 'edit', record, index);
            break;
        default:
            break;
    }
};
