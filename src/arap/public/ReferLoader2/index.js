import React, { Component } from 'react';
import { ajax, base, createPage, toast } from 'nc-lightapp-front';
import Referloader from '../ReferLoader';
import './index.less';
const { NCRow: Row, NCCol: Col, NCTable: Table, NCPopconfirm, NCForm } = base;
const NCFormItem = NCForm.NCFormItem;

//获取参照

function getRefer(self, tag, refcode, key, mainData, name, primarybilltype, flag, isShowStar, isStar) {
	return (
		<NCFormItem showMast={isStar ? true : false} labelName={name} isRequire={true} inline={true}>
			<div className="refer-wrapper">
				{isShowStar ? <span className="required-star">*</span> : null}
				<Referloader
					tag={tag}
					refcode={refcode}
					value={{
						refname: key.display,
						refpk: key.value
					}}
					onChange={(v) => {
						
						key.value = v.refpk;
						key.display = v.refname;
						self.setState({
							mainData
						});
						if (!flag) {
							self.props.getAddDatas(mainData);
						}
					}}
					placeholder={key.display}
				/>
			</div>
		</NCFormItem>
	);
}
export default getRefer;
