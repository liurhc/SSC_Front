import React, { Component } from 'react';
import { createPage, ajax, base, high, toast, pageTo, tableTotal, getMultiLang } from 'nc-lightapp-front';
const { NCFormControl, NCTable, NCModal, NCButton } = base;
import { buttonClick, initTemplate } from './events';
import refreshImg from '../../assets/images/refesh.jpg';
import './index.less';

let columns = [
	{
		title: '' /* 国际化处理： 序号*/,
		dataIndex: 'a',
		key: 'a',
		width: 100
	},
	{
		title: '' /* 国际化处理： 单据编号*/,
		dataIndex: 'b',
		key: 'b',
		width: 100
	},
	{
		title: '' /* 国际化处理： 单据模板类型*/,
		dataIndex: 'c',
		key: 'c',
		width: 100
	}
];

class TransactionReferModel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: [],
			json: {}
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				columns = [
					{
						title: this.state.json['public-000028'] /* 国际化处理： 序号*/,
						dataIndex: 'a',
						key: 'a',
						width: 100
					},
					{
						title: this.state.json['public-000060'] /* 国际化处理： 单据编号*/,
						dataIndex: 'b',
						key: 'b',
						width: 100
					},
					{
						title: this.state.json['public-000177'] /* 国际化处理： 单据模板类型*/,
						dataIndex: 'c',
						key: 'c',
						width: 100
					}
				];
			});
			initTemplate.call(this, this.props);
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	//弹框确定按钮事件
	beSureBtnClick = () => {
		this.props.handleShow();
	};

	//弹窗关闭按钮事件
	cancelBtnClick = () => {
		this.props.handleShow();
	};

	render() {
		let { show } = this.props;
		let { tableData } = this.state;
		return (
			<div>
				<NCModal id="ModalId" className="query-modal" show={show} size={'lg'}>
					<NCModal.Header closeButton>
						<NCModal.Title>
							<div className="querymodal-tit">
								<span className="querymodal-tit-span">{this.state.json['public-000058']}</span>
								{/* 国际化处理： 详细处理情况查询*/}
								<div className="tit-btn">
									<span>
										<img src={refreshImg} />
									</span>
								</div>
							</div>
						</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>
						<div className="steps-content">
							<div className="search-area">
								<NCFormControl style={{ width: 300 }} type="text" />
								<span className="uf uf-search"> </span>
							</div>
							<NCTable
								columns={columns}
								bordered
								isDrag
								data={tableData}
								style={{ marginRight: '20px' }}
								scroll={{
									x: columns.length > 6 ? 100 + (columns.length - 6) * 10 + '%' : '100%',
									y: 320
								}}
								loading={false}
								//onRowClick={this.onRowClick.bind(this)}
								//rowClassName={this.rowClassNameHandler}
							/>
						</div>
					</NCModal.Body>

					<NCModal.Footer>
						<NCButton onClick={this.beSureBtnClick.bind(this)}>{this.state.json['public-000076']}</NCButton>
						{/* 国际化处理： 确定*/}
						<NCButton onClick={this.cancelBtnClick.bind(this)}>{this.state.json['public-000077']}</NCButton>
						{/* 国际化处理： 取消*/}
					</NCModal.Footer>
				</NCModal>
			</div>
		);
	}
}

TransactionReferModel = createPage({})(TransactionReferModel);

export default TransactionReferModel;
