import {ajax} from 'nc-lightapp-front';
export default function (props, pks) {
    let data = {
        pk: pks,
        pageId: "20080PBM_CARD"
    };
    ajax({
        url: '/nccloud/arap/payablebill/querycard.do',
        data: data,
        success: (res) => {
            if (res.data) {
                if (res.data.head) {
                    this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                }
                if (res.data.body) {
                    this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
                }
                props.setUrlParam(pks)//动态修改地址栏中的id的值
                this.toggleShow();
            } else {
                this.props.form.EmptyAllFormValue(this.formId);
                this.props.cardTable.setTableData(this.tableId, { rows: [] });
            }
        }
    });
}
