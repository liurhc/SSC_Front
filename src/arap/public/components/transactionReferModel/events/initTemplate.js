import { base, ajax } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
import tableButtonClick from './tableButtonClick';

const formId = '20080PBM_head';
const tableId = '20080PBM_bodys';
const pageId = '20080PBM_CARD';
export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appid: '0001AA1000000002KJTZ'//注册按钮的id
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					modifierMeta(props, meta,that)
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
			}   
		}
	)
}

function modifierMeta(props, meta,that) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	meta[tableId].items.forEach((item, key) => {
		item.width = 150;
	});
	let multiLang = props.MutiInit.getIntl('2052');
	//添加操作列
	meta[tableId].items.push({
		label: that.state.json['public-000044'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: '160px',
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let status = props.cardTable.getStatus(tableId);
			let buttonAry = status === 'browse'?
				["open_browse"]
			    :
				["open_edit"];
			return props.button.createOprationButton(buttonAry, {
				area: "card_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index)
			});
		}
	});

	return meta;
}
