import { ajax, base, toast, cacheTools } from 'nc-lightapp-front';
import { getCartData } from '../../getCartTableData.js';

export default function(props, id) {
	switch (id) {
		case 'Commit': //提交
			this.pubajax('/nccloud/arap/arappub/commit.do');
			break;
		case 'Uncommit': //收回
			this.pubajax('/nccloud/arap/arappub/uncommit.do');
			break;
		case 'Approve': // 审批
			this.pubajax('/nccloud/arap/arappub/approve.do');
			break;
		case 'UnApprove': // 取消审批
			this.pubajax('/nccloud/arap/arappub/unapprove.do');
			break;
		case 'Save':
			this.saveBill('/nccloud/arap/arappub/save.do');
			break;
		case 'TempSave': //暂存
			this.saveBill('/nccloud/arap/arappub/tempsave.do');
			break;
		case 'SaveAndCommit':
			this.saveBill('/nccloud/arap/arappub/saveandcommit.do');
			break;
		case 'Edit':
			let editData = {
				pk: this.props.getUrlParam('id'),
				billType: this.billType
			};
			ajax({
				url: '/nccloud/arap/arappub/edit.do',
				data: editData,
				success: (res) => {
					if (res.success) {
						props.linkTo('../card/index.html', {
							status: 'edit',
							id: props.getUrlParam('id')
						});
						this.toggleShow();
					}
				}
			});
			break;
		case 'Copy':
			ajax({
				url: '/nccloud/arap/arappub/copy.do',
				data: {
					id: props.getUrlParam('id'),
					ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
					pageId: this.pageId,
					billType: this.billType
				},
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							this.props.form.EmptyAllFormValue(this.formId);
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}
					props.linkTo('../card/index.html', {
						status: 'edit',
						id: props.getUrlParam('id'),
						copyFlag: 'copy'
					});
					this.toggleShow();
				}
			});
			break;

		case 'Delete':
			this.props.modal.show('delete');
			break;
		case 'Back':
			props.linkTo('../list/index.html');
			break;
		case 'Cancel':
			if (props.getUrlParam('status') === 'edit' || props.getUrlParam('status') === 'add') {
				// 表单返回上一次的值
				props.form.cancel(this.formId);
				// 表格返回上一次的值
				props.linkTo('../card/index.html', {
					status: 'browse',
					id: props.getUrlParam('id')
				});
				this.initShow();
				this.toggleShow();
			}
			break;
		case 'Pausetrans': //挂起操作
			this.pause('/nccloud/arap/arappub/pause.do');
			break;
		case 'Cancelpause': //取消挂起操作
			this.pause('/nccloud/arap/arappub/cancelpause.do');
			break;
		//预收付
		case 'PrePay':
			let prePayDatas = this.props.cardTable.getCheckedRows(this.tableId);
			if (prePayDatas.length == 0) {
				toast({ color: 'success', content: this.state.json['public-000038'] }); /* 国际化处理： 请选择表体行!*/
				return;
			}
			let prePayDatasObj = [];
			prePayDatas.forEach((val) => {
				prePayDatasObj.push(val.data.values.pk_payitem.value);
			});
			let prePayDatasdata = {
				pk_item: prePayDatasObj,
				pk_bill: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
				pageId: this.pageId,
				billType: this.billType
			};
			ajax({
				url: '/nccloud/arap/arappub/prepay.do',
				data: prePayDatasdata,
				success: (res) => {
					toast({ color: 'success', content: this.state.json['public-000039'] }); /* 国际化处理： 预收付成功*/
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}
				}
			});
			break;
		case 'RedBack': //红冲操作
			let writebackData = {
				id: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
				pageId: this.pageId,
				billType: this.billType
			};
			ajax({
				url: '/nccloud/arap/arappub/redback.do',
				data: writebackData,
				success: (res) => {
					if (res.data.head) {
						this.props.form.EmptyAllFormValue(this.formId);
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
						this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
					}
					this.props.linkTo('../card/index.html', {
						status: 'edit',
						writebackFlag: 'redBack'
					});
					this.props.form.setFormStatus(this.formId, 'add');
					this.props.cardTable.setStatus(this.tableId, 'edit', null);
					this.toggleShow();
				}
			});
			break;
		case 'LinkConfer': //联查协同单据
			ajax({
				url: '/nccloud/arap/arappub/linkconfer.do',
				async: false,
				data: {
					pk: this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
					billType: this.billType
				},
				success: (res) => {
					if (res.success) {
						cacheTools.set('checkedDataArap', res.data);
						window.parent.openNew(
							{
								code: '20060RBM',
								name: this.state.json['public-000040'],
								pk_appregister: '0001AA1000000002KYHB'
							},
							null,
							'&status=browse&src=arap'
						); /* 国际化处理： 应收单管理*/
					}
				}
			});
			break;
		case 'LinkTbb': //联查计划预算
			ajax({
				url: '/nccloud/arap/arappub/linktbb.do',
				data: {
					pk: this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
					billType: this.billType
				},
				success: (res) => {
					if (res.success) {
						cacheTools.set('checkedDataArap', res.data);
						//打开计划预算界面
					}
				}
			});
			break;
		case 'LinkBal': //联查余额表
			ajax({
				url: '/nccloud/arap/arappub/linkbal.do',
				data: {
					pk: this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
					billType: this.billType
				},
				success: (res) => {
					if (res.success) {
						cacheTools.set('checkedDataArap', res.data);
						//打开余额表界面
					}
				}
			});
			break;
		case 'LinkVouchar': //联查凭证
			ajax({
				url: '/nccloud/arap/arappub/linkvouchar.do',
				data: {
					pk: this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
					billType: this.billType
				},
				success: (res) => {
					if (res.success) {
						cacheTools.set('checkedDataArap', res.data);
						//打开凭证界面
					}
				}
			});
			break;
		case 'LinkTerm': //联查收付款协议
			let cardData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);
			getCartData(cardData, this.formId, this.tableId);
			let selectedData = this.props.cardTable.getCheckedRows(this.tableId);
			let seletedPks = [];
			if (selectedData.length != 0) {
				selectedData.forEach((val) => {
					seletedPks.push(val.data.values.pk_payableitem.value);
				});
			}
			ajax({
				url: '/nccloud/arap/arappub/linkterm.do',
				data: {
					pk: this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
					billType: this.billType,
					pk_item: seletedPks
				},
				success: (res) => {
					if (res.success) {
						//打开收付款协议模态框
						let data = res.data;
						let { linkTermData } = this.state;
						data.map((item, i) => {
							item.key = i;
						});
						this.setState({
							linkTermData: data
						});
						this.handleLinkTerm();
					}
				}
			});
			break;
		case 'LinkDeal': //联查处理情况
			ajax({
				url: '/nccloud/arap/arappub/linkdeal.do',
				data: {
					pk: this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
					billType: this.billType
				},
				success: (res) => {
					if (res.success) {
						//打开处理情况模态框
						let data = res.data;
						let { combinedExaminationData } = this.state;
						let brr = [];
						let br = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm' ];
						for (let i = 0; i < data.length; i++) {
							let oData = {};
							let o = data[i];
							for (let j = 0; j < o.length; j++) {
								oData[br[j]] = o[j];
							}
							brr.push(oData);
						}
						this.handleCombined();
						this.setState({
							combinedExaminationData: brr
						});
					}
				}
			});
			break;
		//表体肩部的按钮操作
		case 'AddLine':
			if (this.props.form.getFormItemsValue(this.formId, 'pk_org').value != null) {
				let rowNum = props.editTable.getNumberOfRows(this.tableId);
				ajax({
					url: '/nccloud/arap/payablebillpub/addline.do',
					data: this.props.createMasterChildData(this.pageId, this.formId, this.tableId),
					success: (res) => {
						if (res.data) {
							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							}
							if (res.data.body) {
								this.props.cardTable.addRow(
									this.tableId,
									rowNum,
									res.data.body[this.tableId].rows[0].values
								);
							}
						}
					}
				});
			}
			break;
		case 'DelLine': //删除行
			const delData = this.props.editTable.getCheckedRows(this.tableId);
			if (delData.length == 0) {
				toast({ color: 'warning', content: this.state.json['public-000041'] }); /* 国际化处理： 请至少选择一行数据！*/
				return;
			}
			let delIndexs = [];
			delData.forEach((val) => {
				if (val.data.status != '3') {
					delIndexs.push(val.index);
				}
			});
			this.props.cardTable.delRowsByIndex(this.tableId, delIndexs);
			break;
		case 'CopyLine': //复制行
			const copyData = this.props.editTable.getCheckedRows(this.tableId);
			if (copyData.length == 0) {
				toast({ color: 'warning', content: this.state.json['public-000042'] }); /* 国际化处理： 请至少选择一行数据进行复制！*/
				return;
			}
			let copyIndexs = [];
			copyData.forEach((val) => {
				if (val.data.status != '3') {
					copyIndexs.push(val.index);
				}
			});
			if (copyIndexs.length > 1) {
				toast({ color: 'warning', content: this.state.json['public-000043'] }); /* 国际化处理： 请仅选择一行数据进行复制！*/
			}
			props.editTable.pasteRow(this.tableId, copyData, copyIndexs[0]);
			break;
		case 'inspectionBill':
			break;

		default:
			break;
	}
}
