import { ajax, base, toast } from 'nc-lightapp-front';
export default function (props, key, text, record, index) {
    let formId = '20080PBM_head';
    let tableId = '20080PBM_bodys';
    switch (key) {
        // 表格操修改
        case 'open_browse':
            props.cardTable.toggleRowView(tableId, record);
            break;
        case 'open_edit':
            props.cardTable.openModel(tableId, 'edit', record, index);
            break;
        default:
            break;
    }
};
