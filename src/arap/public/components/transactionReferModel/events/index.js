import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import onSelectedfn from './onSelectedfn';
export { buttonClick, afterEvent, initTemplate, pageInfoClick, onSelectedfn};
