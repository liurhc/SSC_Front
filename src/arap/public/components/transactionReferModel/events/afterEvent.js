import { ajax } from 'nc-lightapp-front';

export default function afterEvent(props, moduleId, key,value, changedrows, i, s, g) {
	
	//表头编辑后事件
	if (moduleId == this.formId) {
		let data = {
			pageid: this.pageId,
			event: this.props.createHeadAfterEventData(this.pageId, this.formId, this.tableId, moduleId, key, value),
			uiState: this.props.getUrlParam('status')
		};
		ajax({
			url: '/nccloud/arap/payablebill/cardheadafteredit.do',
			data: data,
			success: (res) => {
				if (res.data) {
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						//this.props.cardTable.setStatus(this.tableId,'edit');
					}
				}
			}
		});

	}
	

	//表体编辑后事件
    if(moduleId == this.tableId){
		let data = {
			rowindex: i,
			pageid: this.pageId,
			event: props.createBodyAfterEventData(this.pageId, this.formId, this.tableId, moduleId, key, changedrows),
			uiState: this.props.getUrlParam('status')
		};
		ajax({
			url: '/nccloud/arap/payablebill/cardbodyafteredit.do',
			data: data,
			success: (res) => {
				if (res.data) {
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						//this.props.cardTable.setStatus(this.tableId,'edit');
					}
				}
			}
		});
	}

	
}
