import React, { Component } from 'react';
import { ajax, base, toast, pageTo, getMultiLang, createPage } from 'nc-lightapp-front';
const { NCModal, NCButton, NCTable } = base;
import './index.less';

let columns = [
	{
		title: '' /* 国际化处理： 行号*/,
		dataIndex: 'pk_dealnum',
		key: 'pk_dealnum',
		width: 100
	},
	{
		title: '' /* 国际化处理： 金额*/,
		children: [
			{
				title: '' /* 国际化处理： 原币*/,
				dataIndex: 'money',
				key: 'money',
				width: 100
			},
			{
				title: '' /* 国际化处理： 本币*/,
				dataIndex: 'local_money',
				key: 'local_money',
				width: 100
			}
		]
	},
	{
		title: '' /* 国际化处理： 余额*/,
		children: [
			{
				title: '' /* 国际化处理： 原币*/,
				dataIndex: 'money_bal',
				key: 'money_bal',
				width: 100
			},
			{
				title: '' /* 国际化处理： 本币*/,
				dataIndex: 'local_money_bal',
				key: 'local_money_bal',
				width: 100
			}
		]
	},
	{
		title: '' /* 国际化处理： 处理摘要*/,
		dataIndex: 'scomment',
		key: 'scomment',
		width: 100
	},
	{
		title: '' /* 国际化处理： 处理日期*/,
		dataIndex: 'busidate',
		key: 'busidate',
		width: 100
	},
	{
		title: '' /* 国际化处理： 借方处理金额*/,
		children: [
			{
				title: '' /* 国际化处理： 原币*/,
				dataIndex: 'money_de_b',
				key: 'money_de_b',
				width: 100
			},
			{
				title: '' /* 国际化处理： 本币*/,
				dataIndex: 'local_money_de_b',
				key: 'local_money_de_b',
				width: 100
			}
		]
	},
	{
		title: '' /* 国际化处理： 贷方处理金额*/,
		children: [
			{
				title: '' /* 国际化处理： 原币*/,
				dataIndex: 'money_de_h',
				key: 'money_de_h',
				width: 100
			},
			{
				title: '' /* 国际化处理： 本币*/,
				dataIndex: 'local_money_de_h',
				key: 'local_money_de_h',
				width: 100
			}
		]
	},
	{
		title: '' /* 国际化处理： 对应单据类型*/,
		dataIndex: 'billTypeName',
		key: 'billTypeName',
		width: 100
	},
	{
		title: '' /* 国际化处理： 对应单据编号*/,
		dataIndex: 'billno',
		key: 'billno',
		width: 100
	}
];

class combinedExaminationModel extends Component {
	constructor(props) {
		super(props);
		this.totalData = [];
		this.table = '';
		this.total = '';
		this.totalColumns = [
			{
				title: '' /* 国际化处理： 合计*/,
				dataIndex: 'pk_dealnum',
				key: 'pk_dealnum',
				width: 100
			},

			{
				title: '' /* 国际化处理： 原币*/,
				dataIndex: 'money',
				key: 'money',
				width: 100
			},
			{
				title: '' /* 国际化处理： 本币*/,
				dataIndex: 'local_money',
				key: 'local_money',
				width: 100
			},
			{
				title: '' /* 国际化处理： 原币*/,
				dataIndex: 'money_bal',
				key: 'money_bal',
				width: 100
			},
			{
				title: '' /* 国际化处理： 本币*/,
				dataIndex: 'local_money_bal',
				key: 'local_money_bal',
				width: 100
			},
			{
				title: '' /* 国际化处理： 处理摘要*/,
				dataIndex: 'scomment',
				key: 'scomment',
				width: 100
			},
			{
				title: '' /* 国际化处理： 处理日期*/,
				dataIndex: 'busidate',
				key: 'busidate',
				width: 100
			},

			{
				title: '' /* 国际化处理： 原币*/,
				dataIndex: 'money_de_b',
				key: 'money_de_b',
				width: 100
			},
			{
				title: '' /* 国际化处理： 本币*/,
				dataIndex: 'local_money_de_b',
				key: 'local_money_de_b',
				width: 100
			},

			{
				title: '' /* 国际化处理： 原币*/,
				dataIndex: 'money_de_h',
				key: 'money_de_h',
				width: 100
			},
			{
				title: '' /* 国际化处理： 本币*/,
				dataIndex: 'local_money_de_h',
				key: 'local_money_de_h',
				width: 100
			},
			{
				title: '' /* 国际化处理： 对应单据类型*/,
				dataIndex: 'billTypeName',
				key: 'billTypeName',
				width: 100
			},
			{
				title: '' /* 国际化处理： 对应单据编号*/,
				dataIndex: 'billno',
				key: 'billno',
				width: 100
			}
		];
		this.state = {
			tableData: [],
			record: null,
			selectedRow: [],
			json: {}
		};
	}

	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				columns = [
					{
						title: this.state.json['public-000045'] /* 国际化处理： 行号*/,
						dataIndex: 'pk_dealnum',
						key: 'pk_dealnum',
						width: 100
					},
					{
						title: this.state.json['public-000046'] /* 国际化处理： 金额*/,
						children: [
							{
								title: this.state.json['public-000047'] /* 国际化处理： 原币*/,
								dataIndex: 'money',
								key: 'money',
								width: 100
							},
							{
								title: this.state.json['public-000048'] /* 国际化处理： 本币*/,
								dataIndex: 'local_money',
								key: 'local_money',
								width: 100
							}
						]
					},
					{
						title: this.state.json['public-000049'] /* 国际化处理： 余额*/,
						children: [
							{
								title: this.state.json['public-000047'] /* 国际化处理： 原币*/,
								dataIndex: 'money_bal',
								key: 'money_bal',
								width: 100
							},
							{
								title: this.state.json['public-000048'] /* 国际化处理： 本币*/,
								dataIndex: 'local_money_bal',
								key: 'local_money_bal',
								width: 100
							}
						]
					},
					{
						title: this.state.json['public-000050'] /* 国际化处理： 处理摘要*/,
						dataIndex: 'scomment',
						key: 'scomment',
						width: 100
					},
					{
						title: this.state.json['public-000051'] /* 国际化处理： 处理日期*/,
						dataIndex: 'busidate',
						key: 'busidate',
						width: 100
					},
					{
						title: this.state.json['public-000052'] /* 国际化处理： 借方处理金额*/,
						children: [
							{
								title: this.state.json['public-000047'] /* 国际化处理： 原币*/,
								dataIndex: 'money_de_b',
								key: 'money_de_b',
								width: 100
							},
							{
								title: this.state.json['public-000048'] /* 国际化处理： 本币*/,
								dataIndex: 'local_money_de_b',
								key: 'local_money_de_b',
								width: 100
							}
						]
					},
					{
						title: this.state.json['public-000053'] /* 国际化处理： 贷方处理金额*/,
						children: [
							{
								title: this.state.json['public-000047'] /* 国际化处理： 原币*/,
								dataIndex: 'money_de_h',
								key: 'money_de_h',
								width: 100
							},
							{
								title: this.state.json['public-000048'] /* 国际化处理： 本币*/,
								dataIndex: 'local_money_de_h',
								key: 'local_money_de_h',
								width: 100
							}
						]
					},
					{
						title: this.state.json['public-000054'] /* 国际化处理： 对应单据类型*/,
						dataIndex: 'billTypeName',
						key: 'billTypeName',
						width: 100
					},
					{
						title: this.state.json['public-000055'] /* 国际化处理： 对应单据编号*/,
						dataIndex: 'billno',
						key: 'billno',
						width: 100
					}
				];
				this.totalColumns = [
					{
						title: this.state.json['public-000056'] /* 国际化处理： 合计*/,
						dataIndex: 'pk_dealnum',
						key: 'pk_dealnum',
						width: 100
					},

					{
						title: this.state.json['public-000047'] /* 国际化处理： 原币*/,
						dataIndex: 'money',
						key: 'money',
						width: 100
					},
					{
						title: this.state.json['public-000048'] /* 国际化处理： 本币*/,
						dataIndex: 'local_money',
						key: 'local_money',
						width: 100
					},
					{
						title: this.state.json['public-000047'] /* 国际化处理： 原币*/,
						dataIndex: 'money_bal',
						key: 'money_bal',
						width: 100
					},
					{
						title: this.state.json['public-000048'] /* 国际化处理： 本币*/,
						dataIndex: 'local_money_bal',
						key: 'local_money_bal',
						width: 100
					},
					{
						title: this.state.json['public-000050'] /* 国际化处理： 处理摘要*/,
						dataIndex: 'scomment',
						key: 'scomment',
						width: 100
					},
					{
						title: this.state.json['public-000051'] /* 国际化处理： 处理日期*/,
						dataIndex: 'busidate',
						key: 'busidate',
						width: 100
					},

					{
						title: this.state.json['public-000047'] /* 国际化处理： 原币*/,
						dataIndex: 'money_de_b',
						key: 'money_de_b',
						width: 100
					},
					{
						title: this.state.json['public-000048'] /* 国际化处理： 本币*/,
						dataIndex: 'local_money_de_b',
						key: 'local_money_de_b',
						width: 100
					},

					{
						title: this.state.json['public-000047'] /* 国际化处理： 原币*/,
						dataIndex: 'money_de_h',
						key: 'money_de_h',
						width: 100
					},
					{
						title: this.state.json['public-000048'] /* 国际化处理： 本币*/,
						dataIndex: 'local_money_de_h',
						key: 'local_money_de_h',
						width: 100
					},
					{
						title: this.state.json['public-000054'] /* 国际化处理： 对应单据类型*/,
						dataIndex: 'billTypeName',
						key: 'billTypeName',
						width: 100
					},
					{
						title: this.state.json['public-000055'] /* 国际化处理： 对应单据编号*/,
						dataIndex: 'billno',
						key: 'billno',
						width: 100
					}
				];
				//initTemplate.call(this, this.props);
			});
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {}

	componentWillReceiveProps(nextProps) {
		if (nextProps.combinedExaminationData == this.props.combinedExaminationData) {
			return false;
		}
		let { tableData } = this.state;
		let totalArr = nextProps.combinedExaminationData.length == 0 ? [] : nextProps.combinedExaminationData.total;
		if (totalArr && totalArr.length > 0) {
			this.totalData = [
				{
					pk_dealnum: totalArr[0],
					money: totalArr[1],
					local_money: totalArr[2],
					money_bal: totalArr[3],
					local_money_bal: totalArr[4],
					scomment: totalArr[5],
					busidate: totalArr[6],
					money_de_b: totalArr[7],
					local_money_de_b: totalArr[8],
					money_de_h: totalArr[9],
					local_money_de_h: totalArr[10],
					billTypeName: totalArr[11],
					billno: totalArr[12]
				}
			];
		}
		this.setState(
			{
				tableData: nextProps.combinedExaminationData.length == 0 ? [] : nextProps.combinedExaminationData.list
			},
			() => {
				this.setState({
					selectedRow: new Array(tableData.length) //状态同步
				});
				if (this.state.tableData.length > 0) {
					let bodyArea = this.table.querySelector('.u-table-body');
					let totalArea = this.total.querySelector('.u-table-body');
					bodyArea.onscroll = () => {
						// 表体滚动条事件
						totalArea.scrollLeft = bodyArea.scrollLeft;
					};
					totalArea.onscroll = () => {
						//合计行滚动条事件
						bodyArea.scrollLeft = totalArea.scrollLeft;
					};
				}
			}
		);
	}

	rowClassNameHandler = (record, index, indent) => {
		if (this.state.selectedRow[index]) {
			return 'selected';
		} else {
			return '';
		}
	};

	onRowClick = (record, index) => {
		let selectedRow = new Array(this.state.tableData.length);
		selectedRow[index] = true;
		this.setState({
			factoryValue: record,
			selectedRow: selectedRow,
			record: record
		});
	};

	linkBill = () => {
		let record = this.state.record;
		if (record) {
			ajax({
				url: '/nccloud/arap/arappub/linkarapbill.do',
				async: false,
				data: {
					billType: record.billType,
					pk_bill: record.pk_bill
				},
				success: (res) => {
					let data = res.data;
					if (data) {
						pageTo.openTo(data.url, data.condition);
					} else {
						toast({ color: 'warning', content: this.state.json['public-000057'] }); /* 国际化处理： 未查询到路径!*/
					}
				}
			});
		} else {
			toast({ color: 'warning', content: this.state.json['public-000014'] }); /* 国际化处理： 请选中一行数据!*/
			return;
		}
	};

	render() {
		let { show, pk_tradetypeid, billno } = this.props;
		let { tableData } = this.state;
		return (
			<div>
				<NCModal
					id="combinedExaminationModelId"
					className="query-modal"
					show={show}
					size={'lg'}
					backdrop={'static'}
					onHide={this.props.handleModel}
				>
					<NCModal.Header closeButton>
						<NCModal.Title>{this.state.json['public-000058']}</NCModal.Title>
						{/* 国际化处理： 详细处理情况查询*/}
					</NCModal.Header>

					<NCModal.Body>
						<div className="steps-content">
							<ul className="content-tit">
								<div className="content-tit-tit">
									<li className="tit-tradetype">
										<span>{this.state.json['public-000059']}：</span>
										{/* 国际化处理： 单据类型*/}
										<span>{pk_tradetypeid}</span>
									</li>
									<li className="tit-billno">
										<span>{this.state.json['public-000060']}：</span>
										{/* 国际化处理： 单据编号*/}
										<span>{billno}</span>
									</li>
								</div>
								<li>
									<NCButton
										className="button-primary"
										style={{ float: 'right' }}
										onClick={this.linkBill}
									>
										{this.state.json['public-000061']}
										{/* 国际化处理： 联查单据*/}
									</NCButton>
								</li>
							</ul>
							<div className="table-area">
								<NCTable
									className="total-rows"
									columns={columns}
									isDrag
									ref={(dom) => {
										this.table = ReactDOM.findDOMNode(dom);
									}}
									data={tableData}
									style={{ marginRight: '20px', height: '350px' }}
									loading={false}
									onRowClick={this.onRowClick.bind(this)}
									rowClassName={this.rowClassNameHandler}
									footer={() => {
										return (
											<NCTable
												className="total-row"
												ref={(dom) => (this.total = ReactDOM.findDOMNode(dom))}
												rowKey={'total'}
												isDrag
												data={this.totalData}
												columns={this.totalColumns}
												showHeader={false}
												scroll={{
													x:
														columns.length > 6
															? 100 + (columns.length - 6) * 10 + '%'
															: '100%'
												}}
											/>
										);
									}}
									scroll={{ x: true, y: 100 }}
								/>
							</div>
						</div>
					</NCModal.Body>
				</NCModal>
			</div>
		);
	}
}
combinedExaminationModel = createPage({})(combinedExaminationModel);
export default combinedExaminationModel;
