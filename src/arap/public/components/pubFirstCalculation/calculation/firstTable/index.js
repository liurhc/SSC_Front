import React, { Component } from 'react';
import { base, createPage, getMultiLang } from 'nc-lightapp-front';
import '../table.less';
const { NCTable, NCCheckbox } = base;
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};

class FirstTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {},
			tableData: [], //表格数据
			checkedData: [], //选中数据
			checkedArray: [], //各行选中判断
			checkedAll: false //是否全选
		};
	}

	componentDidMount() {
		this.props.onRef1(this);
	}

	componentWillReceiveProps(nextProps) {
		if (this.state.json == nextProps.json) return;
		this.setState({
			json: nextProps.json
		});
	}

	setFirstTableDatas = (data) => {
		let loCalData = data;
		let checkedArray = [];
		for (let i = 0, len = loCalData.length; i < len; i++) {
			loCalData[i].key = i;
			checkedArray.push(loCalData[i].xzbz);
		}
		let flag = checkedArray.every((item) => {
				return item;
			}),
			checkedAll = false;
		if (flag) checkedAll = true;
		this.setState({
			tableData: loCalData,
			checkedArray: checkedArray,
			checkedAll: checkedAll
		});
	};

	getTableColumns = () => {
		let columns = [
			{
				title: this.state.json['public-000081'] /* 国际化处理： 币种*/,
				dataIndex: 'bzmc',
				key: 'bzmc',
				width: '20%'
			},
			{
				title: this.state.json['public-000082'] /* 国际化处理： 上次计算时间*/,
				dataIndex: 'jssj',
				key: 'jssj',
				width: '20%'
			},
			{
				title: this.state.json['public-000083'] /* 国际化处理： 损益金额*/,
				dataIndex: 'syje',
				key: 'syje',
				width: '20%'
			},
			{
				title: this.state.json['public-000084'] /* 国际化处理： 摘要*/,
				dataIndex: 'zy',
				key: 'zy',
				width: '20%'
			}
		];
		return columns;
	};

	//获取选中行数据
	getCheckedData = () => {
		let { checkedData, checkedArray, tableData } = this.state;
		checkedData = [];
		for (var i = 0, len = checkedArray.length; i < len; i++) {
			if (checkedArray[i] === true) {
				checkedData.push(tableData[i]);
			}
		}
		this.setState({
			checkedData
		});
		return checkedData;
	};
	//获取数据
	getFirstTableDatas = () => {
		let { tableData } = this.state;
		return JSON.parse(JSON.stringify(tableData));
	};
	//全选
	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		for (var i = 0, len = self.state.checkedArray.length; i < len; i++) {
			checkedArray[i] = !self.state.checkedAll;
			this.state.tableData[i].xzbz = checkedArray[i];
		}
		self.setState(
			{
				checkedAll: !self.state.checkedAll,
				checkedArray: checkedArray
			},
			() => {
				self.props.getSelectedDatas(self.getCheckedData());
			}
		);
	};
	//单选
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		this.state.tableData[index].xzbz = checkedArray[index];
		for (var i = 0, len = self.state.checkedArray.length; i < len; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		self.setState(
			{
				checkedAll: allFlag,
				checkedArray: checkedArray
			},
			() => {
				self.props.getSelectedDatas(self.getCheckedData());
			}
		);
	};
	renderColumnsMultiSelect(columns) {
		const { checkedArray } = this.state;
		const { multiSelect } = this.props;
		let indeterminate_bool = false;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
						<NCCheckbox
							className="table-checkbox"
							checked={this.state.checkedAll}
							indeterminate={indeterminate_bool && !this.state.checkedAll}
							onChange={this.onAllCheckChange}
						/>
					),
					key: 'checkbox',
					dataIndex: 'checkbox',
					width: '5%',
					render: (text, record, index) => {
						return (
							<NCCheckbox
								className="table-checkbox"
								checked={this.state.checkedArray[index]}
								onChange={this.onCheckboxChange.bind(this, text, record, index)}
							/>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}
	//改变全选状态
	changAllCheckBoxStatus = () => {
		this.setState({
			checkedAll: false
		});
	};

	render() {
		let { tableData } = this.state;
		let columnsldad = this.getTableColumns(tableData);
		let columns = this.renderColumnsMultiSelect(columnsldad);
		return (
			<div id="firstableId">
				<div className="content">
					<NCTable
						columns={columns}
						bordered
						isDrag
						data={tableData}
						style={{ marginRight: '20px' }}
						scroll={{
							x: columns.length > 6 ? 100 + (columns.length - 6) * 10 + '%' : '100%',
							y: 400
						}}
						loading={false}
					/>
				</div>
			</div>
		);
	}
}

FirstTable.defaultProps = defaultProps12;

FirstTable = createPage({})(FirstTable);

export default FirstTable;
