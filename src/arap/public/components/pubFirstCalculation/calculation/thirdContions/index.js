import { base } from 'nc-lightapp-front';
import thirdImg from './imgs/third.jpg';
import './index.less';
const { NCButton, NCHotKeys } = base;

function handleReturn(props) {
	props.handleReturn();
}

export default function ThirdContions(props) {
	return (
		<div id="thirdContionsId">
			<div className="contents">
				<div className="conents-style">
					<div className="contents-left">
						{/* <img src={thirdImg} /> */}
						<i className="iconfont icon-wancheng" />
					</div>
					<div className="contents-right">
						<h3 className="tit-h3">{props.jsonlang.json['public-000096']}</h3>
						{/* 国际化处理： 汇兑损益*/}
						<div className="date">
							<span>{props.jsonlang.json['public-000105']}：</span>
							{/* 国际化处理： 计算日期*/}
							<span>{props.date.value.slice(0, 10)}</span>
						</div>
						<div className="pkOrg">
							<span>{props.jsonlang.json['public-000106']}：</span>
							{/* 国际化处理： 业务组织*/}
							<span>{props.pkOrg.display}</span>
						</div>
						<NCHotKeys
							keyMap={{
								returnActionSign: [ 'esc' ]
							}}
							handlers={{
								returnActionSign: () => {
									handleReturn(props);
								}
							}}
							// 是否启用组件
							enabled={true}
							// 是否为聚焦触发
							focused={true}
							attach={document.body}
							// 默认display 可以设置 inline-block 等dispaly属性
							display="inline-block"
						>
							<NCButton onClick={handleReturn.bind(this, props)}>
								{props.jsonlang.json['public-000107']}
							</NCButton>
							{/* 国际化处理： 返回第一步*/}
						</NCHotKeys>
					</div>
				</div>
			</div>
		</div>
	);
}
