import React, { Component } from 'react';
import { base, toast, getMultiLang } from 'nc-lightapp-front';
import '../table.less';
import './index.less';

const { NCTable } = base;
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
//集团本币
let groupTit = {
	title: '' /* 国际化处理： 集团本币*/,
	children: [
		{
			title: '' /* 国际化处理： 调整前余额*/,
			dataIndex: 'jtbbye',
			key: 'jtbbye',
			width: 100
		},
		{
			title: '' /* 国际化处理： 调整汇率*/,
			dataIndex: 'jtbbhl',
			key: 'jtbbhl',
			width: 100
		},
		{
			title: '' /* 国际化处理： 调整后余额*/,
			dataIndex: 'tjtbbye',
			key: 'tjtbbye',
			width: 100
		},
		{
			title: '' /* 国际化处理： 差额*/,
			dataIndex: 'jtbbce',
			key: 'jtbbce',
			width: 100
		}
	]
};
//全局本币
let globalTit = {
	title: '' /* 国际化处理： 全局本币*/,
	children: [
		{
			title: '' /* 国际化处理： 调整前余额*/,
			dataIndex: 'qjbbye',
			key: 'qjbbye',
			width: 100
		},
		{
			title: '' /* 国际化处理： 调整汇率*/,
			dataIndex: 'qjbbce',
			key: 'qjbbce',
			width: 100
		},
		{
			title: '' /* 国际化处理： 调整后余额*/,
			dataIndex: 'tqjbbye',
			key: 'tqjbbye',
			width: 100
		},
		{
			title: '' /* 国际化处理： 差额*/,
			dataIndex: 'qjbbce',
			key: 'qjbbce',
			width: 100
		}
	]
};
let isGroupUse;
let isGlobalUse;
let dataVal;
let pk_orgVal;

class Secondpage extends Component {
	constructor(props) {
		super(props);
		this.columns1 = [
			{
				title: '' /* 国际化处理： 序号*/,
				dataIndex: 'index',
				key: 'index',
				width: '20%'
			},
			{
				title: '' /* 国际化处理： 币种*/,
				dataIndex: 'bzmc',
				key: 'bzmc',
				width: '20%'
			},
			{
				title: '' /* 国际化处理： 原币余额*/,
				dataIndex: 'ybye',
				key: 'ybye',
				width: '20%'
			},
			{
				title: '' /* 国际化处理： 组织本币*/,
				width: '40%',
				children: [
					{
						title: '' /* 国际化处理： 调整前余额*/,
						dataIndex: 'zzbbye',
						key: 'zzbbye',
						width: '10%'
					},
					{
						title: '' /* 国际化处理： 调整汇率*/,
						dataIndex: 'zzbbhl',
						key: 'zzbbhl',
						width: '10%'
					},
					{
						title: '' /* 国际化处理： 调整后余额*/,
						dataIndex: 'tzzbbye',
						key: 'tzzbbye',
						width: '10%'
					},
					{
						title: '' /* 国际化处理： 差额*/,
						dataIndex: 'zzbbce',
						key: 'zzbbce',
						width: '10%'
					}
				]
			}
		];
		this.state = {
			tableData: [], //表格数据
			isshow: false,
			columns: [],
			mainData: {
				date: { display: '', value: dataVal || null }, //计算日期
				pk_org: { display: '', value: pk_orgVal || null } //财务组织
			},
			json: {}
		};
	}

	getMultilingual = (showData, data, transData) => {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				groupTit = {
					title: this.state.json['public-000097'] /* 国际化处理： 集团本币*/,
					children: [
						{
							title: this.state.json['public-000098'] /* 国际化处理： 调整前余额*/,
							dataIndex: 'jtbbye',
							key: 'jtbbye',
							width: 100
						},
						{
							title: this.state.json['public-000099'] /* 国际化处理： 调整汇率*/,
							dataIndex: 'jtbbhl',
							key: 'jtbbhl',
							width: 100
						},
						{
							title: this.state.json['public-000100'] /* 国际化处理： 调整后余额*/,
							dataIndex: 'tjtbbye',
							key: 'tjtbbye',
							width: 100
						},
						{
							title: this.state.json['public-000101'] /* 国际化处理： 差额*/,
							dataIndex: 'jtbbce',
							key: 'jtbbce',
							width: 100
						}
					]
				};
				globalTit = {
					title: this.state.json['public-000102'] /* 国际化处理： 全局本币*/,
					children: [
						{
							title: this.state.json['public-000098'] /* 国际化处理： 调整前余额*/,
							dataIndex: 'qjbbye',
							key: 'qjbbye',
							width: 100
						},
						{
							title: this.state.json['public-000099'] /* 国际化处理： 调整汇率*/,
							dataIndex: 'qjbbce',
							key: 'qjbbce',
							width: 100
						},
						{
							title: this.state.json['public-000100'] /* 国际化处理： 调整后余额*/,
							dataIndex: 'tqjbbye',
							key: 'tqjbbye',
							width: 100
						},
						{
							title: this.state.json['public-000101'] /* 国际化处理： 差额*/,
							dataIndex: 'qjbbce',
							key: 'qjbbce',
							width: 100
						}
					]
				};
				this.columns1 = [
					{
						title: this.state.json['public-000028'] /* 国际化处理： 序号*/,
						dataIndex: 'index',
						key: 'index',
						width: '4%'
					},
					{
						title: this.state.json['public-000081'] /* 国际化处理： 币种*/,
						dataIndex: 'bzmc',
						key: 'bzmc',
						width: '16%'
					},
					{
						title: this.state.json['public-000103'] /* 国际化处理： 原币余额*/,
						dataIndex: 'ybye',
						key: 'ybye',
						width: '16%'
					},
					{
						title: this.state.json['public-000104'] /* 国际化处理： 组织本币*/,
						children: [
							{
								title: this.state.json['public-000098'] /* 国际化处理： 调整前余额*/,
								dataIndex: 'zzbbye',
								key: 'zzbbye',
								width: '16%'
							},
							{
								title: this.state.json['public-000099'] /* 国际化处理： 调整汇率*/,
								dataIndex: 'zzbbhl',
								key: 'zzbbhl',
								width: '16%'
							},
							{
								title: this.state.json['public-000100'] /* 国际化处理： 调整后余额*/,
								dataIndex: 'tzzbbye',
								key: 'tzzbbye',
								width: '16%'
							},
							{
								title: this.state.json['public-000101'] /* 国际化处理： 差额*/,
								dataIndex: 'zzbbce',
								key: 'zzbbce',
								width: '16%'
							}
						]
					}
				];
				this.setState({}, () => {
					this.handleSecData(showData, data, transData);
				});
			});
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	};

	componentDidMount() {
		this.props.onRef(this);
	}

	//改变表格表头宽度
	changeColumns = (columns) => {
		columns.map((value) => {
			if (value.key == 'index') {
				value.width = 60;
			} else {
				value.width = 140;
			}
			if (value.hasOwnProperty('children')) {
				value.children.map((val) => {
					val.width = 140;
				});
			}
		});
		return columns;
	};

	handleSecData = (showData, data, transData) => {
		if (transData) {
			dataVal = transData.queryData.date.value;
			pk_orgVal = transData.queryData.pk_org.value;
		}
		if (showData.length > 0) {
			isGroupUse = data.isGroupUsed;
			isGlobalUse = data.isGlobalUsed;
			if (isGroupUse) {
				this.columns1.push(groupTit);
				this.columns1 = this.changeColumns(this.columns1);
			}
			if (isGlobalUse) {
				this.columns1.push(globalTit);
				this.columns1 = this.changeColumns(this.columns1);
			}
			showData.map((item, i) => {
				item.key = i;
				item.index = i + 1;
			});
			this.setState(
				{
					tableData: showData,
					columns: this.columns1
				},
				() => {
					if (data.error) {
						toast({
							duration: 3, // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
							color: 'danger', // 提示类别，默认是 "success",非必输
							content: data.error // 提示内容,非必输
						});
					}
				}
			);
		}
	};

	render() {
		let { tableData } = this.state;
		return (
			<div id="secondtableId">
				<div className="content">
					{tableData && (
						<NCTable
							columns={this.state.columns}
							bordered
							isDrag
							data={tableData}
							style={{ marginRight: '20px' }}
							scroll={{
								x:
									this.state.columns.length > 4
										? 100 + (this.state.columns.length - 4) * 10 + '%'
										: '100%',
								y: 400
							}}
							loading={false}
						/>
					)}
				</div>
			</div>
		);
	}
}

Secondpage.defaultProps = defaultProps12;

export default Secondpage;
