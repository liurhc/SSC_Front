import { headButton, bottomButton } from '../../pubUtils/buttonName';

export default function (props, id) {
  switch (id) {
    case headButton.ProfitRecord://损益记录
      this.handleProfitRecord();
      break;
    case bottomButton.Next://下一步
      this.handleNext();
      break;
    case bottomButton.Pre://下一步
      this.handleBack();
      break;
    case bottomButton.CancelProfit://取消损益
      this.handleCancelProfit();
      break;
    case bottomButton.Save://保存
      this.handleKeep();
      break;
      
  }
}
