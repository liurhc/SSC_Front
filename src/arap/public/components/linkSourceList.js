import { ajax, cacheTools, cardCache } from 'nc-lightapp-front';
let { updateCache } = cardCache;

/**
 * 联查来源   
 * @author xiejhk
 * @param {*} listUrl 列表页面路径
 * @param {*} tableId 表格编码
 */
export default function linkSourceList(props, tableId) {
    let dataArr = cacheTools.get('checkedData');
    let linkurl = '/nccloud/arap/arappub/queryGridByPK.do';
    let data;
    if (dataArr) {
        data = {
            "fipLink": dataArr,
            "pageId": props.getSearchParam('p')
        };
    }
    ajax({
        url: linkurl,
        data: data,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                if (data) {
                    props.table.setAllTableData(tableId, data[tableId]);
                } else {
                    props.table.setAllTableData(tableId, { rows: [] });
                }
            }
        }
    });
}
