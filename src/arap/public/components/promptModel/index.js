import React, { Component } from 'react';
import { createPage, ajax, base, high,getMultiLang } from 'nc-lightapp-front';
import './index.less';
const { NCModal, NCButton, NCForm } = base;

export default class PromptModel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {}
		};
	}

	componentWillMount() {
		let callback = (json) => {
			this.setState({json:json});
		}
		getMultiLang({moduleId:'public',domainName :'arap',currentLocale:'simpchn',callback});
	}
	//取消按钮
	handleClose = () => {
		this.props.handleModel('n');
	};

	//取消
	handleCancel = () => {
		this.props.handleModel('n');
	};
	//确定按钮
	handleEnsure = () => {
		this.props.handleModel('y');
	};

	render() {
		let { show } = this.props;
		return (
			<div>
				<NCModal id="periodModelId" className="query-modal" show={show}>
					<NCModal.Header>
						<NCModal.Title className="modal-head">
							<div className="modal-tit">
								<span className="modal-tit-name">{this.state.json['public-000078']}</span>{/* 国际化处理： 警告*/}
								<span onClick={this.handleClose.bind(this)} className="querymodal-tit-close">
									X
								</span>
							</div>
						</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>{this.props.tit}</NCModal.Body>

					<NCModal.Footer>
						<NCButton style={{ marginRight: 20 }} type="primary" onClick={() => this.handleEnsure()}>
							{this.state.json['public-000079']}(Y){/* 国际化处理： 是*/}
						</NCButton>
						<NCButton style={{ marginRight: 20 }} type="primary" onClick={() => this.handleCancel()}>
							{this.state.json['public-000080']}(N){/* 国际化处理： 否*/}
						</NCButton>
					</NCModal.Footer>
				</NCModal>
			</div>
		);
	}
}
