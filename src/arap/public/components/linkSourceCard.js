import { ajax, cacheTools, cardCache } from 'nc-lightapp-front';
let { updateCache } = cardCache;

/**
 * 联查来源   
 * @author xiejhk
 * @param {*} pageId 列表页面pageId
 * @param {*} listPageId 页面pageID   
 * @param {*} pk_bill 单据主键属性名
 * @param {*} billType 单据类型
 * @param {*} formId tableId 表格编码
 * @param {*} pkname dataSource 更新单页缓存使用
 */
export default function linkSourceCard(props, pageId, pk_bill, formId, tableId, pkname, dataSource,pagefunction) {
    let dataArr = cacheTools.get('checkedData');
    let linkurl = '/nccloud/arap/arappub/queryBillCardByPK.do';
    let linkdata;
    if (dataArr) {
        linkdata = {
            "fipLink": dataArr,
            "pageId": props.getSearchParam('p')
        };
    }
    ajax({
        url: linkurl,
        data: linkdata,
        success: (res) => {
            let { success, data } = res;
            if (success && res.data) {
                let addpk_bill = res.data[0].head[formId].rows[0].values[pk_bill].value
                props.addUrlParam({id:addpk_bill});
                props.setUrlParam({
                    pk_tradetype: res.data[0].head[formId].rows[0].values['pk_tradetype'].value
                });
                updateCache(pkname, props.getUrlParam('id'), res.data[0], formId, dataSource);
                if (res.data[0].head) {
                    props.form.setAllFormValue({ [formId]: res.data[0].head[formId] });
                }
                if (res.data[0].body) {
                    props.cardTable.setTableData(tableId, res.data[0].body[tableId]);
                }
                pagefunction.toggleShow();
            }
        }
    });
}
