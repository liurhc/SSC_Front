import React, { Component } from 'react';
import { createPage, ajax, base, high, toast, getMultiLang } from 'nc-lightapp-front';
import './periodmodel.less';
const { NCModal, NCButton, NCForm } = base;
const NCFormItem = NCForm.NCFormItem;
import ReferLoader from '../../../public/ReferLoader/index.js';
import InitBillClossRef from '../../../public/components/InitBillClossRef/index.js';
import { title } from '../getCartTableData.js';
export default class PeriodModal extends Component {
	static defaultProps = {
		show: false
	};

	constructor(props) {
		super(props);
		this.state = {
			referData: [],
			cheakedData: [],
			json: {}
		};
	}

	componentDidMount() {}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		setTimeout(() => {
			getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
		}, 0);
	}
	handleClose = () => {
		this.state.referData = [];
		this.props.handleModel();
	};

	//确定按钮
	handleEnsure = () => {
		let { funCode } = this.props;
		let { referData, cheakedData } = this.state;
		let data = {
			pkOrgs: cheakedData,
			funCode: funCode,
			moudleId: funCode.substring(0, 4)
		};
		let content =
			title == 'close'
				? this.state.json['public-000071']
				: this.state.json['public-000072']; /* 国际化处理： 期初建账成功,取消期初建账成功*/
		let url = title == 'close' ? '/nccloud/arap/init/close.do' : '/nccloud/arap/init/cancelclose.do';
		if (cheakedData.length > 0) {
			ajax({
				url: url,
				data: data,
				success: (res) => {
					if (res.success) {
						if (res.data == 'OK') {
							toast({ content: content, color: 'success' });
						} else {
							toast({ content: res.data, color: 'danger' });
						}
					}
				}
			});
			this.state.referData = [];
			this.state.cheakedData = [];
			this.props.handleModel();
		} else {
			toast({ content: this.state.json['public-000073'], color: 'warning' }); /* 国际化处理： 请选择财务组织！*/
		}
	};

	handleCancel = () => {
		this.state.referData = [];
		this.state.cheakedData = [];
		this.props.handleModel();
	};

	render() {
		let { show, vData, tit, billFieldType, funCode } = this.props;
		let { referData, cheakedData } = this.state;
		return (
			<div>
				<NCModal id="periodModelId" className="query-modal" show={show}>
					<NCModal.Header>
						<NCModal.Title className="modal-head">
							{title == 'close' ? this.state.json['public-000074'] : this.state.json['public-000075']}
						</NCModal.Title>
						{/* 国际化处理： 期初建账,取消期初建账*/}
					</NCModal.Header>
					<NCModal.Body>
						<div className="content-tit">
							<span>*</span>
							{this.state.json['public-000024']}
							{/* 国际化处理： 财务组织*/}
						</div>
						<div className="modal-content">
							<ReferLoader
								showStar={false}
								tag="test"
								refcode="uapbd/refer/org/FinanceOrgTreeRef/index.js"
								isMultiSelectedEnabled={true}
								isTreelazyLoad={false}
								isCacheable={false}
								queryCondition={() => {
									let flag = title == 'close' ? 'close' : 'alerdyClose';
									let condition = {
										flag: flag,
										billFieldType: billFieldType,
										DataPowerOperationCode: 'fi', //使用权组
										AppCode: funCode,
										TreeRefActionExt:
											'nccloud.web.arap.ref.before.InitOrgSqlBuilder,nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
									};
									return condition;
								}}
								foolValue={referData}
								onChange={(v, foolValue) => {
									cheakedData = [];
									v.map((cheacdValue) => {
										cheakedData.push({
											pk_org: cheacdValue.refpk,
											pk_orgName: cheacdValue.refname
										});
									});
									referData = v;
									this.setState({
										referData: { ...v, ...foolValue },
										cheakedData
									});
								}}
							/>
						</div>
					</NCModal.Body>
					<NCModal.Footer>
						<NCButton colors="primary" onClick={() => this.handleEnsure()}>
							{this.state.json['public-000076']}
							{/* 国际化处理： 确定*/}
						</NCButton>
						<NCButton type="primary" onClick={() => this.handleCancel()}>
							{this.state.json['public-000077']}
							{/* 国际化处理： 取消*/}
						</NCButton>
					</NCModal.Footer>
				</NCModal>
			</div>
		);
	}
}
