import { ajax, cardCache } from 'nc-lightapp-front';
let { updateCache } = cardCache;

/**
 * 资金联查收付单据卡片页面
 * @author xiejhk
 */
export default function cmpLinkArapCard(props, pk_billtype,pk_bill, formId, tableId, pkname, dataSource,pagefunction) {
    let pk_src = props.getUrlParam('pk_src');
    let linkdata;
    if(pk_src){
        linkdata = {
            "dest_billtype": pk_billtype,
            "pk_src": pk_src,
            "pageId": props.getSearchParam('p')
        };
    }
    let linkurl = '/nccloud/arap/arappub/cmpLinkArapCard.do';
    ajax({
        url: linkurl,
        data: linkdata,
        success: (res) => {
            let { success, data } = res;
            if (success && res.data) {
                let addpk_bill = res.data[0].head[formId].rows[0].values[pk_bill].value
                props.addUrlParam({id:addpk_bill});
                props.setUrlParam({
                    pk_tradetype: res.data[0].head[formId].rows[0].values['pk_tradetype'].value
                });
                updateCache(pkname, props.getUrlParam('id'), res.data[0], formId, dataSource);
                if (res.data[0].head) {
                    props.form.setAllFormValue({ [formId]: res.data[0].head[formId] });
                }
                if (res.data[0].body) {
                    props.cardTable.setTableData(tableId, res.data[0].body[tableId]);
                }
                pagefunction.toggleShow();
            }
        }
    });
}