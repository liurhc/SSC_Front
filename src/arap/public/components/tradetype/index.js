import React, { Component } from 'react';
import {base, high,cardCache,getMultiLang} from 'nc-lightapp-front';
import './index.less'
let { setDefData} = cardCache;
const {  Refer } = high;
let { NCButton } = base;
class TradeTypeButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: true,  //交易类型按钮显示与影藏
            json: {}
        }
    }
    componentWillMount() {
		let callback = (json) => {
			this.setState({json:json});
		}
		getMultiLang({moduleId:'public',domainName :'arap',currentLocale:'simpchn',callback});
	}
    setVisible = (falg) =>{
        this.setState({
            visible:falg
        })
    }

    render() {
        let { billtype, dataSource,switchTemplate} = this.props;
        return (
            <div className={`${this.state.visible ? 'show' : 'hide'}  teadetype-lightapp-component`}>
                <span>
                    <Refer
                        placeholder={this.state.json['public-000177']}/* 国际化处理： 单据模板类型*/
                        refName={this.state.json['public-000139']}/* 国际化处理： 交易类型*/
                        refCode={'tradetype'}
                        refType={'grid'}
                        queryGridUrl={'/nccloud/riart/ref/fiBillTypeTableRefAction.do'}
                        // columnConfig={[
                        //     {
                        //         name: [this.state.json['public-000060'], this.state.json['public-000177']],/* 国际化处理： 单据编号,单据模板类型*/
                        //         code: ['refcode', 'refname']
                        //     }
                        // ]}
                        queryCondition={{
                            parentbilltype: billtype //过滤条件
                        }}
                        onChange={(value) => {
                            setDefData('sessionTradeType', dataSource, value.refcode);
                            //是否做过切换交易类型动作，如果做过，卡片返回列表需要重新刷新列表的按钮区域
                            setDefData('isSwitchTradeType',dataSource,true);
                            if(switchTemplate){
                                switchTemplate();
                            }
                        }}
                        isMultiSelectedEnabled={false}
                        clickContainer={<NCButton colors="primary">{ this.state.json['public-000139']}</NCButton>}/* 国际化处理： 交易类型*/
                    />
                </span>
            </div>
        )
    }
}
export default function (props = {}) {
    var conf = {
    };
    
    return <TradeTypeButton {...Object.assign(conf, props)} />
}
