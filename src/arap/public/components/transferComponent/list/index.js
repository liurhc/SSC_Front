import React, { Component } from 'react';
import { createPage, cardCache, getMultiLang, getBusinessInfo, createPageIcon, base } from 'nc-lightapp-front';
import Querycondition from './querycondition';
import Showcondition from './showcondition';
import { buttonClick, initTemplate } from './events/index';
import { businessDateH, businessDate } from '../../businessDate';
import getInitOrg from '../../pubUtils/getInitOrg';
import { bodyBeforeEvent } from '../../pubUtils/arapTableRefFilter';
import './index.less';
let { NCAffix } = base;
let { setDefData, getDefData } = cardCache;
const tableid = 'list';
let pagecode = '';
let appcode = '';
let title;
let businessDates, businessDatesH;
let selectcons = [
	{
		itemName: '' /* 国际化处理： 对本汇率*/,
		itemType: 'input',
		itemKey: 'm_dBbhl',
		config: { refCode: '' },
		tag: ''
	},
	{
		itemName: '' /* 国际化处理： 币种*/,
		itemType: 'input',
		itemKey: 'm_sCurrency',
		config: { refCode: '' },
		tag: ''
	},
	{
		itemName: '' /* 国际化处理： 转出对象*/,
		itemType: 'input',
		itemKey: 'm_izcWldx',
		config: { refCode: '' },
		tag: ''
	},
	{
		itemName: '' /* 国际化处理： 转出户*/,
		itemType: 'input',
		itemKey: 'm_sFromObBm',
		config: { refCode: '' },
		tag: ''
	},
	{
		itemName: '' /* 国际化处理： 转移日期*/,
		itemType: 'date',
		itemKey: 'm_sClrq',
		config: { refCode: '' },
		tag: ''
	},
	{
		itemName: '' /* 国际化处理： 转入对象*/,
		itemType: 'radio',
		itemKey: 'm_izrWldx',
		config: { refCode: '' },
		tag: ''
	},
	{
		itemName: '' /* 国际化处理： 转入户*/,
		itemType: 'refer',
		itemKey: 'm_sToObBm',
		config: { refCode: '' },
		tag: ''
	}
];

class DebTransList extends Component {
	constructor(props) {
		super(props);
		this.oldVal = '';
		this.isAr29 = false; //是否改变订单客户值
		this.scale = ''; //获取字段精度
		this.allCheckedData = [];
		(this.querycons = [ [], [] ]),
			(this.state = {
				json: {},
				isModelShow: false, //提示模态框是否显示
				pk_bill: null, //当前选中第一行的主键pk
				filterObj: {
					//查询条件
					pk_org: { display: '', value: null }, //财务组织
					m_sFromOb: this.props.isCustomer ? { display: '', value: 0 } : { display: '', value: 1 }, //转出对象/* 国际化处理： 客户,供应商*/
					m_sKsbm: { display: '', value: null }, //客户/供应商
					m_sYwybm: { display: '', value: null }, //业务员
					m_sBmbm: { display: '', value: null }, //部门
					m_sCurrency: { display: '', value: null }, //币种
					m_sDjlx: { display: '', value: null }, //应收类型
					m_zrbm: { display: '', value: null }, //利润中心
					m_sDjbh: [ { display: '', value: null }, { display: '', value: null } ], //最小最大单据编码
					m_sDjrq: [], //单据日期开始结束
					m_sYbje: [ { display: '', value: null }, { display: '', value: null } ] //最小最大原币金额
				},
				selectObj: {
					//右侧查询条件
					m_dBbhl: { display: '', value: '' }, //对本汇率
					m_sCurrency: { display: '', value: null }, //币种
					m_sFromObBm: { display: '', value: null }, //转出户
					m_izcWldx: { display: '', value: null }, //转出对象
					m_sClrq: { value: null, display: null }, //转移日期
					m_izrWldx: { display: this.props.isCustomer ? '' : '', value: this.props.isCustomer ? 0 : 1 }, //转移对象(转入对象)/* 国际化处理： 客户,供应商*/
					m_sToObBm: { display: '', value: null } //转入账户
				},
				condatas: {}, //子组件传递过来的查询条件
				tableData: {} //表格数据
			});
		this.handleMsToObBmVal = this.handleMsToObBmVal.bind(this);
		this.handleGetData = this.handleGetData.bind(this);
		this.handleConData = this.handleConData.bind(this);
		this.handleChangeFilObj = this.handleChangeFilObj.bind(this);
		this.handleEmptyQueryData = this.handleEmptyQueryData.bind(this);
		this.handleChangeSelectObj = this.handleChangeSelectObj.bind(this);
		this.setOrdercubAssign = this.setOrdercubAssign.bind(this);
		this.getSelectedData = this.getSelectedData.bind(this);
		this.handlesetTableData = this.handlesetTableData.bind(this);
		this.onSelectedFn = this.onSelectedFn.bind(this);
		this.onSelectedAllFn = this.onSelectedAllFn.bind(this);
		this.onAfterEvent = this.onAfterEvent.bind(this);
	}

	componentWillMount() {
		let { filterObj, selectObj } = this.state;
		appcode = this.props.getSearchParam('c'); //获取小应用编码
		pagecode = this.props.getSearchParam('p'); //获取页面编码
		let listDatas = getDefData('query', this.props.pagecode);
		let callback = (json) => {
			this.setState({ json: json }, () => {
				this.querycons = [
					[
						{
							itemName: this.state.json['public-000024'] /* 国际化处理： 财务组织*/,
							itemType: 'refer',
							itemKey: 'pk_org',
							config: { refCode: 'uapbd/refer/org/FinanceOrgTreeRef/index.js' },
							tag: 'FinanceOrgTreeReff'

						},
						{
							itemName: this.state.json['public-000178'] /* 国际化处理： 转出对象*/,
							itemType: 'input',
							itemKey: 'm_sFromOb',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.props.isCustomer
								? this.state.json['public-000064']
								: this.state.json['public-000063'] /* 国际化处理： 客户,供应商*/,
							itemType: 'input',
							itemKey: 'm_sKsbm',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000065'] /* 国际化处理： 部门*/,
							itemType: 'input',
							itemKey: 'm_sBmbm',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000066'] /* 国际化处理： 业务员*/,
							itemType: 'input',
							itemKey: 'm_sYwybm',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: '',
							itemType: 'queryBtn',
							itemKey: '',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000081'] /* 国际化处理： 币种*/,
							itemType: 'input',
							itemKey: 'm_sCurrency',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000195'] /* 国际化处理： 应收类型*/,
							itemType: 'input',
							itemKey: 'm_sDjlx',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000196'] /* 国际化处理： 利润中心*/,
							itemType: 'input',
							itemKey: 'm_zrbm',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000197'] /* 国际化处理： 单据日期*/,
							itemType: 'inputDouble',
							itemKey: 'm_sDjrq',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000198'] /* 国际化处理： 单据编码*/,
							itemType: 'inputDouble',
							itemKey: 'm_sDjbh',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000181'] /* 国际化处理： 原币金额*/,
							itemType: 'inputDouble',
							itemKey: 'm_sYbje',
							config: { refCode: '' },
							tag: ''
						}
					],
					[
						{
							itemName: this.state.json['public-000024'] /* 国际化处理： 财务组织*/,
							itemType: 'refer',
							itemKey: 'pk_org',
							config: { refCode: 'uapbd/refer/org/FinanceOrgTreeRef/index.js' },
							tag: 'FinanceOrgTreeReff',
							showUnitFlg: false

						},
						{
							itemName: this.state.json['public-000178'] /* 国际化处理： 转出对象*/,
							itemType: 'select',
							itemKey: 'm_sFromOb',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.props.isCustomer
								? this.state.json['public-000064']
								: this.state.json['public-000063'] /* 国际化处理： 客户,供应商*/,
							itemType: 'refer',
							itemKey: 'm_sKsbm',
							config: {
								refCode: this.props.isCustomer
									? 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index.js'
									: 'uapbd/refer/supplier/SupplierRefTreeGridRef/index.js'
							},
							tag: this.props.isCustomer ? 'CustomerDefaultTreeGridRef' : 'SupplierRefTreeGridRef',
							showUnitFlg: false
						},
						{
							itemName: this.state.json['public-000065'] /* 国际化处理： 部门*/,
							itemType: 'refer',
							itemKey: 'm_sBmbm',
							config: { refCode: 'uapbd/refer/org/DeptNCTreeRef/index.js' },
							tag: 'DeptTreeRef',
							showUnitFlg: true

						},
						{
							itemName: this.state.json['public-000066'] /* 国际化处理： 业务员*/,
							itemType: 'refer',
							itemKey: 'm_sYwybm',
							config: { refCode: 'uapbd/refer/psninfo/PsndocTreeGridRef/index.js' },
							tag: 'PsndocTreeGridRef',
							showUnitFlg: true

						},
						{
							itemName: '',
							itemType: 'queryBtn',
							itemKey: '',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000081'] /* 国际化处理： 币种*/,
							itemType: 'refer',
							itemKey: 'm_sCurrency',
							config: { refCode: 'uapbd/refer/pubinfo/CurrtypeGridRef/index.js' },
							tag: 'CurrtypeGridRef',
							showUnitFlg: false

						},
						{
							itemName: this.state.json['public-000195'] /* 国际化处理： 应收类型*/,
							itemType: 'refer',
							itemKey: 'm_sDjlx',
							config: { refCode: 'uap/refer/riart/transtype/index.js' },
							tag: 'transtype',
							showUnitFlg: false

						},
						{
							itemName: this.state.json['public-000196'] /* 国际化处理： 利润中心*/,
							itemType: 'refer',
							itemKey: 'm_zrbm',
							config: { refCode: 'uapbd/refer/org/LiabilityCenterOrgTreeRef/index.js' },
							tag: 'LiabilityCenterOrgTreeRef',
							showUnitFlg: false

						},
						{
							itemName: this.state.json['public-000197'] /* 国际化处理： 单据日期*/,
							itemType: 'date',
							itemKey: 'm_sDjrq',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000198'] /* 国际化处理： 单据编码*/,
							itemType: 'inputDouble',
							itemKey: 'm_sDjbh',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000181'] /* 国际化处理： 原币金额*/,
							itemType: 'inputDouble',
							itemKey: 'm_sYbje',
							config: { refCode: '' },
							tag: ''
						}
					]
				];
				selectcons = [
					{
						itemName: this.state.json['public-000190'] /* 国际化处理： 对本汇率*/,
						itemType: 'input',
						itemKey: 'm_dBbhl',
						config: { refCode: '' },
						tag: ''
					},
					{
						itemName: this.state.json['public-000081'] /* 国际化处理： 币种*/,
						itemType: 'input',
						itemKey: 'm_sCurrency',
						config: { refCode: '' },
						tag: ''
					},
					{
						itemName: this.state.json['public-000178'] /* 国际化处理： 转出对象*/,
						itemType: 'input',
						itemKey: 'm_izcWldx',
						config: { refCode: '' },
						tag: ''
					},
					{
						itemName: this.state.json['public-000191'] /* 国际化处理： 转出户*/,
						itemType: 'input',
						itemKey: 'm_sFromObBm',
						config: { refCode: '' },
						tag: ''
					},
					{
						itemName: this.state.json['public-000192'] /* 国际化处理： 转移日期*/,
						itemType: 'date',
						itemKey: 'm_sClrq',
						config: { refCode: '' },
						tag: ''
					},
					{
						itemName: this.state.json['public-000193'] /* 国际化处理： 转入对象*/,
						itemType: 'radio',
						itemKey: 'm_izrWldx',
						config: { refCode: '' },
						tag: ''
					},
					{
						itemName: this.state.json['public-000194'] /* 国际化处理： 转入户*/,
						itemType: 'refer',
						itemKey: 'm_sToObBm',
						config: { refCode: '' },
						tag: ''
					}
				];
				filterObj.m_sFromOb = this.props.isCustomer
					? { display: this.state.json['public-000064'], value: 0 }
					: { display: this.state.json['public-000063'], value: 1 };
				(selectObj.m_izrWldx = {
					display: this.props.isCustomer
						? this.state.json['public-000064']
						: this.state.json['public-000063'],
					value: this.props.isCustomer ? 0 : 1
				}), //转移对象(转入对象)/* 国际化处理： 客户,供应商*/
					this.setState({
						filterObj,
						selectObj
					});
				switch (this.props.title) {
					case 'skbz':
						title = this.state.json['public-000199']; /* 国际化处理： 收款并账*/
						break;
					case 'zwzy':
						title = this.state.json['public-000200']; /* 国际化处理： 债务转移*/
						break;
					case 'fkbz':
						title = this.state.json['public-000201']; /* 国际化处理： 付款并账*/
						break;
					case 'zqzy':
						title = this.state.json['public-000202']; /* 国际化处理： 债权转移*/
						break;
				}
			});
			setTimeout(() => {
				businessDates = businessDate && businessDate();
				businessDatesH = businessDateH();
				filterObj.m_sDjrq = [ businessDates, businessDates ];
				selectObj.m_sClrq = { value: businessDates, display: businessDatesH }; //转移日期
				this.setState(
					{
						filterObj,
						selectObj
					},
					() => {
						if (listDatas) {
							this.setState(
								{
									filterObj: listDatas.queryData,
									selectObj: listDatas.selectObjData,
									tableData: listDatas.tableData
								},
								() => {
									this.props.cardTable.setTableData(tableid, { rows: this.state.tableData });
								}
							);
						}
					}
				);
			}, 0);
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {
		getInitOrg(appcode, this.cb);
	}

	cb = (pkOrg) => {
		let { filterObj } = this.state;
		filterObj.pk_org = {
			display: pkOrg.display,
			value: pkOrg.value,
			refpk2: pkOrg.refpk2
		};
		this.setState({
			filterObj
		});
	};

	// 获取转移金额精度
	getAccuracy = (field) => {
		this.scale = String(field.split('.')[1].length);
	};

	//点击单个选择框函数
	onSelectedFn = (props, moduleId, record, index, status) => {
		let allCheckedData = this.props.cardTable.getAllRows(moduleId, true);
		this.oldVal = '';
		let this_sett = record.values.sum_money_bal.value;
		if (status) {
			this.getAccuracy(this_sett);
			props.cardTable.setStatus(tableid, 'edit');
			props.cardTable.setValByKeyAndRowId(tableid, allCheckedData[index].rowid, 'this_sett', {
				value: this_sett
			});
			props.cardTable.setEditableByIndex(tableid, index, [ 'this_sett' ], true);
			props.cardTable.setColScale([ { areacode: tableid, filedcode: 'this_sett', scale: this.scale } ]);
		} else {
			props.cardTable.setValByKeyAndRowId(tableid, allCheckedData[index].rowid, 'this_sett', { value: '' });
			props.cardTable.setEditableByIndex(tableid, index, [ 'this_sett' ], false);
			for (let item of allCheckedData) {
				if (item.selected) {
					return;
				}
			}
			props.cardTable.setStatus(tableid, 'browse');
		}
	};

	//全选按钮回调
	onSelectedAllFn = (props, moduleId, status, length) => {
		let allCheckedData = this.props.cardTable.getAllRows(moduleId, true);
		if (length > 0) {
			let this_settt = allCheckedData[0].values.money_bal.value;
			this.getAccuracy(this_settt);
			let arrIndex = [];
			for (var i = 0; i < length; i++) {
				let this_sett = allCheckedData[i].values.money_bal.value;
				let rowid = allCheckedData[i].rowid;
				arrIndex.push(i);
				if (status) {
					props.cardTable.setStatus(tableid, 'edit');
					props.cardTable.setValByKeyAndRowId(tableid, rowid, 'this_sett', { value: this_sett });
					props.cardTable.setEditableByIndex(tableid, arrIndex, [ 'this_sett' ], true);
					props.cardTable.setColScale([ { areacode: tableid, filedcode: 'this_sett', scale: this.scale } ]);
				} else {
					props.cardTable.setValByKeyAndRowId(tableid, rowid, 'this_sett', { value: '' });
					props.cardTable.setStatus(tableid, 'browse');
					props.cardTable.setEditableByIndex(tableid, arrIndex, [ 'this_sett' ], false);
				}
			}
		}
	};

	//给订单客户字段赋值
	setOrdercubAssign = (msToObBm, flag) => {
		this.allCheckedData = this.props.cardTable.getAllRows(tableid, true);
		let length = this.allCheckedData.length;
		if (flag == 0) {
			if (this.isAr29) {
				if (length > 0) {
					for (var i = 0; i < length; i++) {
						this.allCheckedData[i].values.ordercubasdocClone = JSON.parse(
							JSON.stringify(this.allCheckedData[i].values.ordercubasdoc)
						);
					}
				}
				this.props.cardTable.setColValue(tableid, 'ordercubasdoc', {
					value: msToObBm.value,
					display: msToObBm.display
				});
			}
		} else {
			for (var i = 0; i < length; i++) {
				let rowid = this.allCheckedData[i].rowid;
				let ordercubasdocClone = this.allCheckedData[i].values.ordercubasdocClone;
				if (ordercubasdocClone) {
					this.props.cardTable.setValByKeyAndRowId(tableid, rowid, 'ordercubasdoc', {
						value: ordercubasdocClone.value,
						display: ordercubasdocClone.display
					});
				}
			}
		}
	};

	//表格编辑后事件
	onAfterEvent(props, moduleId, key, changerows, value, index, data) {
		let sum_money_bal = data.values.sum_money_bal.value;
		if (key == 'this_sett') {
			if (sum_money_bal < 0) {
				if (value[0].newvalue.value * 1 > sum_money_bal * 1 && value[0].newvalue.value * 1 < 0) {
					props.cardTable.setValByKeyAndRowId(moduleId, data.rowid, 'this_sett', {
						value: value[0].newvalue.value
					});
				} else {
					if (value[0].newvalue.value * 1 == 0) {
						props.cardTable.setValByKeyAndRowId(moduleId, data.rowid, 'this_sett', { value: null });
					} else {
						props.cardTable.setValByKeyAndRowId(moduleId, data.rowid, 'this_sett', {
							value: sum_money_bal
						});
					}
				}
			} else {
				if (value[0].newvalue.value * 1 > 0 && value[0].newvalue.value * 1 < sum_money_bal * 1) {
					props.cardTable.setValByKeyAndRowId(moduleId, data.rowid, 'this_sett', {
						value: value[0].newvalue.value
					});
				} else {
					if (value[0].newvalue.value * 1 == 0) {
						props.cardTable.setValByKeyAndRowId(moduleId, data.rowid, 'this_sett', { value: null });
					} else {
						props.cardTable.setValByKeyAndRowId(moduleId, data.rowid, 'this_sett', {
							value: sum_money_bal
						});
					}
				}
			}
		}
	}

	//改变查询条件数据
	handleChangeFilObj = (filterObj) => {
		this.setState({
			filterObj: filterObj
		});
	};
	//改变右侧查询条件数据
	handleChangeSelectObj = (selectObj) => {
		this.setState({
			selectObj: selectObj
		});
	};

	// 获取查询条件参数
	handleGetData = (data) => {
		if (!data) {
			this.props.cardTable.setTableData(tableid, { rows: [] });
		} else {
			this.props.cardTable.setTableData(tableid, data[tableid]);
			this.props.cardTable.setStatus(tableid, 'browse');
			this.setState({
				tableData: data
			});
		}
	};
	//获取子组件的查询数据
	handleConData = (condata) => {
		let { selectObj } = this.state;
		selectObj.m_sCurrency = condata.m_sCurrency;
		selectObj.m_sFromObBm.display = condata.m_sFromObMc;
		selectObj.m_sFromObBm.value = condata.m_sFromObBm;
		selectObj.m_izcWldx = condata.m_sFromOb;
		selectObj.m_dBbhl = condata.bbhl;
		selectObj.m_sToObBm = { display: '', value: null };
		this.isAr29 = condata.isAr29;
		this.setState({
			selectObj
		});
	};

	//从新设置表格数据
	handlesetTableData = (arrIndex) => {
		this.props.cardTable.delRowsByIndex(tableid, arrIndex);
	};

	//获取选中数据的第一行,选中多行的时候只取第一行数据
	getFirstCheckedData() {
		let checkedData = this.props.cardTable.getCheckedRows(tableid);
		let checkedObj;
		if (checkedData.length > 0) {
			checkedObj = {
				pk_bill: checkedData[0].data.values.pk_bill.value,
				billType: checkedData[0].data.values.pk_billtype.value
			};
		}
		return checkedObj;
	}
	onRef = (ref) => {
		this.child = ref;
	};

	//转移记录
	handleHisrecord = () => {
		let { filterObj, selectObj } = this.state;
		let datas = {
			queryData: filterObj,
			selectObjData: selectObj,
			tableData: this.props.cardTable.getAllRows(tableid, true)
		};
		this.props.pushTo('/his', {
			pagecode: this.props.pagecode
		});
		setDefData('query', this.props.pagecode, datas);
		this.props.ViewModel.setData('toHisTransData', filterObj);
	};

	//刷新
	refresh = () => {
		let queryCons = getDefData('list-query', appcode);
		this.child.handleQueryBtn(queryCons, 0);
	};

	//清空查询条件
	handleEmptyQueryData = () => {
		let localFilterObj = {
			//查询条件
			pk_org: { display: '', value: null }, //财务组织
			m_sFromOb: this.props.isCustomer
				? { display: this.state.json['public-000064'], value: 0 }
				: { display: this.state.json['public-000063'], value: 1 }, //转出对象/* 国际化处理： 客户,供应商*/
			m_sKsbm: { display: '', value: null }, //客户/供应商
			m_sYwybm: { display: '', value: null }, //业务员
			m_sBmbm: { display: '', value: null }, //部门
			m_sCurrency: { display: '', value: null }, //币种
			m_sDjlx: { display: '', value: null }, //应收类型
			m_zrbm: { display: '', value: null }, //利润中心
			m_sDjbh: [ { display: '', value: null }, { display: '', value: null } ], //最小最大单据编码
			m_sDjrq: [], //单据日期开始结束
			m_sYbje: [ { display: '', value: null }, { display: '', value: null } ] //最小最大原币金额
		};
		let localselectObj = {
			//右侧查询条件
			m_dBbhl: { display: '', value: null }, //对本汇率
			m_sCurrency: { display: '', value: null }, //币种
			m_sFromObBm: { display: '', value: null }, //转出户
			m_izcWldx: { display: '', value: null }, //转出对象
			m_sClrq: { value: null, display: null }, //转移日期
			m_izrWldx: {
				display: this.props.isCustomer ? this.state.json['public-000064'] : this.state.json['public-000063'],
				value: this.props.isCustomer ? 0 : 1
			}, //转移对象(转入对象)/* 国际化处理： 客户,供应商*/
			m_sToObBm: { display: '', value: null } //转入户
		};
		this.setState({
			filterObj: localFilterObj,
			selectObj: localselectObj
		});
	};

	//清空转入账户的值
	handleMsToObBmVal = () => {
		let { filterObj, selectObj } = this.state;
		if (!filterObj.pk_org.value) {
			selectObj.m_sToObBm = { display: null, value: null };
			this.setState({
				selectObj
			});
		}
	};

	//获取选中行数据
	getSelectedData = () => {
		return this.props.cardTable.getCheckedRows(tableid);
	};

	render() {
		let { cardTable, button } = this.props;
		let { createButtonApp } = button;
		let { condatas } = this.state;
		let { createCardTable } = cardTable;
		return (
			<div id="transferId">
				<div className="nc-single-table">
					<div className="nc-bill-list">
						<NCAffix offsetTop={0}>
							<div className="nc-bill-header-area">
								<div className="header-title-search-area">
									{createPageIcon()}
									<h2 className="title-search-detail">{title}</h2>
								</div>
								<div className="header-button-area">
									{createButtonApp({
										area: 'list_head',
										buttonLimit: 3,
										onButtonClick: buttonClick.bind(this)
									})}
								</div>
							</div>
						</NCAffix>
					</div>
					<div className="table-content">
						<div className="table-left">
							<div className="nc-single-table">
								<Querycondition
									createButtonApp={createButtonApp}
									querycons={this.querycons}
									appcode={appcode}
									pagecode={pagecode}
									filterObj={this.state.filterObj}
									selectObj={this.state.selectObj}
									onRef={this.onRef}
									sfbz={this.props.sfbz}
									isCustomer={this.props.isCustomer}
									handleMsToObBmVal={this.handleMsToObBmVal}
									getData={this.handleGetData}
									getConData={this.handleConData}
									handleChangeFilObj={this.handleChangeFilObj}
									handleEmptyQueryData={this.handleEmptyQueryData}
								/>
								<div className="nc-singleTable-table-area">
									{createCardTable(tableid, {
										//列表区
										onSelected: this.onSelectedFn, // 左侧选择列单个选择框回调
										onSelectedAll: this.onSelectedAllFn, // 左侧选择列全选回调
										onAfterEvent: this.onAfterEvent, // 控件的编辑后事件
										onBeforeEvent: bodyBeforeEvent.bind(this),
										pageSize: 20,
										showIndex: true, //显示序号
										showCheck: true, //显示复选框
										hideSwitch: () => {
											return false;
										}
									})}
								</div>
							</div>
						</div>
						<div className="table-right">
							<Showcondition
								createButtonApp={createButtonApp}
								appcode={appcode}
								pagecode={pagecode}
								selectcons={selectcons}
								selectObj={this.state.selectObj}
								filterObj={this.state.filterObj}
								handleChangeSelectObj={this.handleChangeSelectObj}
								setOrdercubAssign={this.setOrdercubAssign}
								getSelectedData={this.getSelectedData}
								handlesetTableData={this.handlesetTableData}
								condatas={condatas}
								sfbz={this.props.sfbz}
								isCustomer={this.props.isCustomer}
								json={this.state.json}
							/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

DebTransList = createPage({
	initTemplate: initTemplate
})(DebTransList);

export default DebTransList;
