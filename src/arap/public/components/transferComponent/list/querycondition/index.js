import React, { Component } from 'react';
import { ajax, base, toast, cardCache, getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../../../ReferLoader';
import { getQueryTransfer } from '../../../getCartTableData.js'; //获取查询条件数据
import { buttonClick } from '../events/index';
import { getBbhlrate } from '../events/transDatas.js';
import './index.less';
let { setDefData, getDefData } = cardCache;
const { NCFormControl, NCNumber, NCRangePicker, NCSelect } = base;
const NCOption = NCSelect.NCOption;
const format = 'YYYY-MM-DD';
const tableid = 'list';
let pagecode, appcode;

class Comconditions extends Component {
	constructor(props) {
		super(props);
		this.NCOptionData = [
			this.props.isCustomer ? { value: 0, display: '' } : { value: 1, display: '' } /* 国际化处理： 客户,供应商*/,
			{ value: 2, display: '' } /* 国际化处理： 部门*/,
			{ value: 3, display: '' } /* 国际化处理： 业务员*/
		];
		this.state = {
			json: {}
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				this.NCOptionData = [
					this.props.isCustomer
						? { value: 0, display: this.state.json['public-000064'] }
						: { value: 1, display: this.state.json['public-000063'] } /* 国际化处理： 客户,供应商*/,
					{ value: 2, display: this.state.json['public-000065'] } /* 国际化处理： 部门*/,
					{ value: 3, display: this.state.json['public-000066'] } /* 国际化处理： 业务员*/
				];
			});
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {
		this.props.onRef(this);
		pagecode = this.props.pagecode;
		appcode = this.props.appcode;
	}

	//input展示
	getInput = (name, key, bool) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCFormControl disabled={'disabled'} value={key.value} placeholder={name} />
				</div>
			</div>
		);
	};

	//input展示 double
	getInputTow = (name, key, filterObj) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCFormControl
						style={{ 'text-align': 'left' }}
						disabled={'disabled'}
						value={filterObj[key][0].value}
						placeholder={name}
					/>
					<span>-</span>
					<NCFormControl
						style={{ 'text-align': 'left' }}
						disabled={'disabled'}
						value={filterObj[key][1].value}
						placeholder={name}
					/>
				</div>
			</div>
		);
	};

	//可编辑的input框
	getEditInputTow = (name, key, filterObj) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCFormControl
						style={{ 'text-align': 'left' }}
						value={filterObj[key][0].display}
						placeholder={name}
						onChange={(v) => {
							filterObj[key][0].display = v;
							filterObj[key][0].value = v;
							this.props.handleChangeFilObj(filterObj);
						}}
					/>
					<span>-</span>
					<NCFormControl
						style={{ 'text-align': 'left' }}
						value={filterObj[key][1].display}
						placeholder={name}
						onChange={(v) => {
							filterObj[key][1].display = v;
							filterObj[key][1].value = v;
							this.props.handleChangeFilObj(filterObj);
						}}
					/>
				</div>
			</div>
		);
	};

	//可编辑的input框 (原币金额)
	getEditInputTowLM = (name, key, filterObj) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCNumber
						style={{ 'text-align': 'left' }}
						value={filterObj[key][0].display}
						placeholder={name}
						scale={4}
						onChange={(v) => {
							filterObj[key][0].display = v;
							filterObj[key][0].value = v;
							this.props.handleChangeFilObj(filterObj);
						}}
					/>
					<span>-</span>
					<NCNumber
						style={{ 'text-align': 'left' }}
						value={filterObj[key][1].display}
						placeholder={name}
						scale={4}
						onChange={(v) => {
							filterObj[key][1].display = v;
							filterObj[key][1].value = v;
							this.props.handleChangeFilObj(filterObj);
						}}
					/>
				</div>
			</div>
		);
	};

	//参照展示
	getRefer = (tag, refcode, key, filterObj, showUnitFlg, flag) => {
		let m_sFromObVal = filterObj.m_sFromOb.value;
		let isShowStar;
		switch (key) {
			case 'pk_org':
			case 'm_sCurrency':
				isShowStar = true;
				break;
		}
		switch (m_sFromObVal) {
			case 0:
			case 1:
				if (key == 'm_sKsbm') {
					isShowStar = true;
				}
				break;
			case 2:
				if (key == 'm_sBmbm') {
					isShowStar = true;
				}
				break;
			case 3:
				if (key == 'm_sYwybm') {
					isShowStar = true;
				}
				break;
		}
		return (
			<div className="finance">
				<div className="finance-refer">
					<Referloader
						showStar={isShowStar}
						tag={tag}
						refcode={refcode}
						unitValueIsNeeded ={false} //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						value={{
							refname: filterObj[key].display,
							refpk: filterObj[key].value,
							refpk2: filterObj[key].refpk2
						}}
						onChange={(v) => {
							filterObj[key].value = v.refpk;
							filterObj[key].refpk2 = v.refpk2;
							filterObj[key].display = v.refname;
							this.props.handleChangeFilObj(filterObj);
							if (key == 'pk_org') {
								this.props.handleMsToObBmVal();
							}
						}}
						isShowUnit={showUnitFlg}
						isShowDisabledData={true}
						//单元组织的条件过滤
						unitCondition={() => {
							if (tag == 'DeptTreeRef') {
								return {
									pkOrgs: filterObj.pk_org.value,
									TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
								};
							} else if (tag == 'PsndocTreeGridRef') {
								return {
									pkOrgs: filterObj.pk_org.value,
									TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
								};
							}
						}}
						queryCondition={() => {
							if (tag == 'FinanceOrgTreeReff') {
								return {
									AppCode: appcode,
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
								};
							} else if (tag == 'CustomerDefaultTreeGridRef') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									pk_org: filterObj.pk_org.value
								};
							} else if (tag == 'LiabilityCenterOrgTreeRef') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y'
								};
							} else if (tag == 'SupplierRefTreeGridRef') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									pk_org: filterObj.pk_org.value
								};
							} else if (tag == 'DeptTreeRef') {
								return {
									busifuncode: 'all',
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									pk_org: filterObj.pk_org.value
								};
							} else if (tag == 'PsndocTreeGridRef') {
								return {
									busifuncode: 'all',
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									pk_org: filterObj.pk_org.value
								};
							} else if (tag == 'CurrtypeGridRef') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y'
								};
							} else if (tag == 'transtype') {
								if (this.props.sfbz == 'Ys') {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										parentbilltype: 'F0'
									};
								} else if (this.props.sfbz == 'Sk') {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										parentbilltype: 'F2'
									};
								} else if (this.props.sfbz == 'Yf') {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										parentbilltype: 'F1'
									};
								} else if (this.props.sfbz == 'Fk') {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										parentbilltype: 'F3'
									};
								}
							}
						}}
					/>
				</div>
			</div>
		);
	};

	//初始条件 浏览态
	getInitialShow = (createButtonApp) => {
		let { querycons, filterObj } = this.props;
		return (
			querycons[0].length > 0 && (
				<div className="querycons-style">
					{querycons[0].map((item, i) => {
						switch (item.itemType) {
							case 'refer':
								return (
									<div className="contions-box">
										{this.getRefer(
											item.tag,
											item.config.refCode,
											item.itemKey,
											filterObj,
											item.showUnitFlg,
											true
										)}
									</div>
								);
							case 'inputDouble':
								if (item.itemKey == 'm_sDjrq') {
									return (
										<div className="contions-box">
											<div className="finance">
												<div className="finance-refer">
													<NCFormControl
														style={{ 'text-align': 'left' }}
														disabled={'disabled'}
														value={filterObj[item.itemKey][0]}
														placeholder={name}
													/>
													<span>-</span>
													<NCFormControl
														style={{ 'text-align': 'left' }}
														disabled={'disabled'}
														value={filterObj[item.itemKey][1]}
														placeholder={name}
													/>
												</div>
											</div>
										</div>
									);
								} else {
									return (
										<div className="contions-box">
											{this.getInputTow(item.itemName, item.itemKey, filterObj)}
										</div>
									);
								}
							case 'input':
								return <div className="contions-box">{this.getInput(item.itemName, item.itemKey)}</div>;
							case 'queryBtn':
								return (
									<div className="contions-box-btn">
										{createButtonApp({
											area: 'list_head_contions',
											buttonLimit: 4,
											onButtonClick: buttonClick.bind(this)
										})}
									</div>
								);
						}
					})}
				</div>
			)
		);
	};

	//财务组件参照选完值后展示界面
	getReferShow = (NCOptions, createButtonApp) => {
		let { querycons, filterObj } = this.props;
		return (
			querycons[1].length > 0 && (
				<div className="querycons-style">
					{querycons[1].map((item, i) => {
						switch (item.itemType) {
							case 'refer':
								return (
									<div className="contions-box">
										{this.getRefer(
											item.tag,
											item.config.refCode,
											item.itemKey,
											filterObj,
											item.showUnitFlg
										)}
									</div>
								);
							case 'inputDouble':
								if (item.itemKey == 'm_sYbje') {
									return (
										<div className="contions-box">
											{this.getEditInputTowLM(item.itemName, item.itemKey, filterObj)}
										</div>
									);
								} else {
									return (
										<div className="contions-box">
											{this.getEditInputTow(item.itemName, item.itemKey, filterObj)}
										</div>
									);
								}
							case 'queryBtn':
								return (
									<div className="contions-box-btn">
										{createButtonApp({
											area: 'list_head_contions',
											buttonLimit: 4,
											onButtonClick: buttonClick.bind(this)
										})}
									</div>
								);
							case 'select':
								return (
									<div className="contions-box">
										<NCSelect
											showStar={true}
											value={filterObj[item.itemKey].display}
											onChange={(v) => {
												filterObj[item.itemKey].display = v.display;
												filterObj[item.itemKey].value = v.value;
												this.props.handleChangeFilObj(filterObj);
											}}
										>
											{NCOptions}
										</NCSelect>
									</div>
								);
							case 'date':
								return (
									<div className="contions-box">
										<NCRangePicker
											showStar={true}
											format={format}
											onChange={(d) => {
												filterObj[item.itemKey] = d;
												this.props.handleChangeFilObj(filterObj);
											}}
											showClear={true}
											showOk={true}
											className={'range-fixed'}
											value={filterObj[item.itemKey]}
											placeholder={this.state.json['public-000142']} /* 国际化处理： 开始 ~ 结束*/
											dateInputPlaceholder={[
												this.state.json['public-000143'],
												this.state.json['public-000144']
											]} /* 国际化处理： 开始,结束*/
										/>
									</div>
								);
						}
					})}
				</div>
			)
		);
	};

	//查询按钮
	handleQueryBtn = (queryCons, flag) => {
		let { filterObj } = this.props;
		let filterConVO;
		if (queryCons) {
			filterConVO = {
				...queryCons,
				m_sSfbz: this.props.sfbz
			};
		} else {
			filterConVO = {
				pk_org: filterObj.pk_org.value,
				m_sFromOb: filterObj.m_sFromOb.value,
				m_sKsbm: filterObj.m_sKsbm.value,
				m_sYwybm: filterObj.m_sYwybm.value,
				m_sBmbm: filterObj.m_sBmbm.value,
				m_sCurrency: filterObj.m_sCurrency.value,
				m_sDjlx: filterObj.m_sDjlx.value,
				m_zrbm: filterObj.m_zrbm.value,
				m_sMinDjbh: filterObj.m_sDjbh[0].value == '' ? null : filterObj.m_sDjbh[0].value,
				m_sMaxDjbh: filterObj.m_sDjbh[1].value == '' ? null : filterObj.m_sDjbh[1].value,
				m_sDjrqBeg: filterObj.m_sDjrq[0] + ' 00:00:00',
				m_sDjrqEnd: filterObj.m_sDjrq[1] + ' 23:59:59',
				m_sMinYbje: filterObj.m_sYbje[0].value == '' ? null : filterObj.m_sYbje[0].value,
				m_sMaxYbje: filterObj.m_sYbje[1].value == '' ? null : filterObj.m_sYbje[1].value,
				m_sSfbz: this.props.sfbz
			};
		}
		let m_sFromObVal = filterObj.m_sFromOb.value;
		switch (m_sFromObVal) {
			case 0:
			case 1:
				filterConVO.m_sFromObMc = filterObj.m_sKsbm.display;
				filterConVO.m_sFromObBm = filterObj.m_sKsbm.value;
				break;
			case 2:
				filterConVO.m_sFromObMc = filterObj.m_sBmbm.display;
				filterConVO.m_sFromObBm = filterObj.m_sBmbm.value;
				break;
			case 3:
				filterConVO.m_sFromObMc = filterObj.m_sYwybm.display;
				filterConVO.m_sFromObBm = filterObj.m_sYwybm.value;
				break;
		}
		if (queryCons) {
			this.handleQueryAjaxFn(filterConVO, filterObj.m_sCurrency, filterObj.m_sFromOb, flag);
		} else {
			if (
				filterConVO.m_sFromObBm != null &&
				filterConVO.m_sCurrency != null &&
				filterConVO.pk_org != null &&
				filterObj.m_sDjrq.length > 0
			) {
				this.handleQueryAjaxFn(filterConVO, filterObj.m_sCurrency, filterObj.m_sFromOb, flag);
			} else {
				if (flag == 0) {
					toast({ color: 'success', title: this.state.json['public-000210'] }); /* 国际化处理： 刷新成功！*/
				} else {
					toast({ color: 'warning', content: this.state.json['public-000189'] }); /* 国际化处理： 带星号的为必选项！*/
				}
			}
		}
		getQueryTransfer(filterObj);
		setDefData('list-query', appcode, JSON.parse(JSON.stringify(filterConVO)));
	};

	//查询数据发起ajax方法
	handleQueryAjaxFn = (filterConVO, m_sCurrency, m_sFromOb, flag) => {
		ajax({
			url: '/nccloud/arap/debtransfer/query.do',
			data: {
				pageId: pagecode,
				filterConVO: filterConVO
			},
			success: (res) => {
				let { success, data } = res;
				if (res.data) {
					let datas = data.grid;
					let conDatas = {
						m_sCurrency: m_sCurrency, //币种
						m_sFromObMc: filterConVO.m_sFromObMc, //转出户
						m_sFromObBm: filterConVO.m_sFromObBm,
						m_sFromOb: m_sFromOb, //转出对象
						bbhl: data.bbhl,
						isAr29: data.isAr29
					};
					getBbhlrate(Object.assign({}, data.bbhl));
					this.props.getConData(conDatas);
					if (success) {
						if (datas == undefined) {
							if (flag == 0) {
								toast({ color: 'success', title: this.state.json['public-000210'] }); /* 国际化处理： 刷新成功！*/
							} else {
								toast({
									color: 'warning',
									content: this.state.json['public-000213']
								}); /* 国际化处理： 未查询出相应的数据！*/
							}
						} else {
							if (flag == 0) {
								toast({ color: 'success', title: this.state.json['public-000210'] }); /* 国际化处理： 刷新成功！*/
							} else {
								toast({
									color: 'success',
									content:
										this.state.json['public-000214'] +
										datas[tableid].rows.length +
										this.state.json['public-000215']
								}); /* 国际化处理： 已成功共几条数据！*/
							}
						}
						this.props.getData(datas);
					}
				} else {
					this.props.getData('');
					if (flag == 0) {
						toast({ color: 'success', title: this.state.json['public-000210'] }); /* 国际化处理： 刷新成功！*/
					} else {
						toast({ color: 'warning', content: this.state.json['public-000209'] }); /* 国际化处理： 已成功！*/
					}
				}
			}
		});
	};

	//清空
	handleEmpty = () => {
		this.props.handleEmptyQueryData();
	};

	render() {
		let { createButtonApp, filterObj } = this.props;
		const NCOptions = this.NCOptionData.map((province) => <NCOption value={province}>{province.display}</NCOption>);
		return (
			<div id="queryconditionsId">
				<div>
					{!filterObj.pk_org.value ? (
						<div>{this.getInitialShow(createButtonApp)}</div>
					) : (
						<div>{this.getReferShow(NCOptions, createButtonApp)}</div>
					)}
				</div>
			</div>
		);
	}
}

export default Comconditions;
