import React, { Component } from 'react';
import { ajax, base, createPage, toast, getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../../../ReferLoader';
import { getHMS } from '../../../businessDate';
import { localBbhl } from '../events/transDatas.js';
import { buttonClick } from '../events/index';
import './index.less';
import '../../../../less/radio.less';
const { NCFormControl, NCDatePicker, NCRadio, NCNumber } = base;
const format = 'YYYY-MM-DD';
let selectedData;
const tableid = 'list';
let Hms;

class Showcondition extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {}
		};
	}

	componentWillMount() {}
	componentDidMount() {
		Hms = getHMS();
	}

	componentWillReceiveProps(nextProps) {
		if (this.state.json == nextProps.json) return;
		this.setState({
			json: nextProps.json
		});
	}

	//input
	getInput = (name, key) => {
		let { selectObj } = this.props;
		return (
			<div className="finance">
				<h2>{name}</h2>
				<div className="finance-refer">
					<NCFormControl disabled={'disabled'} value={selectObj[key].display} />
				</div>
			</div>
		);
	};

	//可编辑对本币汇率Number框
	getEditRateInput = (name, key, scale, localBbhlVal) => {
		let { selectObj } = this.props;
		let flag = '';
		if (selectObj[key].value) {
			if (localBbhlVal == 1) {
				flag = 'disabled';
			}
		} else if (selectObj[key].value == '') {
			flag = 'disabled';
		}
		return (
			<div className="finance">
				<h2>{name}</h2>
				<div className="finance-refer">
					<NCNumber
						style={{ 'text-align': 'left' }}
						className="bill-instruction-input"
						disabled={flag}
						value={selectObj[key].display || ''}
						scale={Number(scale)}
						onChange={(v) => {
							if (v >= 0) {
								if (v == 0) {
									selectObj[key].display = null;
									selectObj[key].value = null;
								} else {
									selectObj[key].display = v;
									selectObj[key].value = v;
								}
							}
							this.props.handleChangeSelectObj(selectObj);
						}}
					/>
				</div>
			</div>
		);
	};

	//对象名称显示不同参照
	getDifferentRefer = () => {
		let { selectObj, filterObj, sfbz } = this.props;
		let { m_izrWldx, m_sToObBm } = selectObj;
		switch (m_izrWldx.value) {
			case 0:
				return (
					<Referloader
						disabled={filterObj.pk_org.value ? '' : 'disabled'}
						showStar={true}
						unitValueIsNeeded ={false} //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						tag="CustomerDefaultTreeGridRef"
						refcode="uapbd/refer/customer/CustomerDefaultTreeGridRef/index.js"
						value={{
							refname: m_sToObBm.display,
							refpk: m_sToObBm.value
						}}
						onChange={(v) => {
							m_sToObBm.value = v.refpk;
							m_sToObBm.display = v.refname;
							this.props.handleChangeSelectObj(selectObj);
							this.props.setOrdercubAssign(m_sToObBm, 0);
						}}
						isShowDisabledData={true}
						placeholder={m_sToObBm.display || this.state.json['public-000064']} /* 国际化处理： 客户*/
						queryCondition={() => {
							return {
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: filterObj.pk_org.value
							};
						}}
					/>
				);
			case 1:
				return (
					<Referloader
						disabled={filterObj.pk_org.value ? '' : 'disabled'}
						showStar={true}
						unitValueIsNeeded ={false} //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						tag="SupplierRefTreeGridRef"
						refcode="uapbd/refer/supplier/SupplierRefTreeGridRef/index.js"
						value={{
							refname: m_sToObBm.display,
							refpk: m_sToObBm.value
						}}
						onChange={(v) => {
							m_sToObBm.value = v.refpk;
							m_sToObBm.display = v.refname;
							this.props.handleChangeSelectObj(selectObj);
							if (sfbz == 'Sk' || sfbz == 'Ys') {
								this.props.setOrdercubAssign(m_sToObBm, 1);
							}
						}}
						placeholder={m_sToObBm.display || this.state.json['public-000063']} /* 国际化处理： 供应商*/
						isShowDisabledData={true}
						queryCondition={() => {
							return {
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: filterObj.pk_org.value
							};
						}}
					/>
				);
			case 2:
				return (
					<Referloader
						disabled={filterObj.pk_org.value ? '' : 'disabled'}
						showStar={true}
						unitValueIsNeeded ={false} //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						tag="DeptTreeRef"
						refcode="uapbd/refer/org/DeptNCTreeRef/index.js"
						value={{
							refname: m_sToObBm.display,
							refpk: m_sToObBm.value
						}}
						onChange={(v) => {
							m_sToObBm.value = v.refpk;
							m_sToObBm.display = v.refname;
							this.props.handleChangeSelectObj(selectObj);
							if (sfbz == 'Sk' || sfbz == 'Ys') {
								this.props.setOrdercubAssign(m_sToObBm, 2);
							}
						}}
						placeholder={m_sToObBm.display || this.state.json['public-000065']} /* 国际化处理： 部门*/
						isShowDisabledData={true}
						isShowUnit={true}
						unitCondition={() => {
							return {
								pkOrgs: filterObj.pk_org.value,
								TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
							};
						}}
						queryCondition={() => {
							return {
								busifuncode: 'all',
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: filterObj.pk_org.value
							};
						}}
					/>
				);
			case 3:
				return (
					<Referloader
						disabled={filterObj.pk_org.value ? '' : 'disabled'}
						showStar={true}
						unitValueIsNeeded ={false} //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						tag="PsndocTreeGridRef"
						refcode="uapbd/refer/psninfo/PsndocTreeGridRef/index.js"
						value={{
							refname: m_sToObBm.display,
							refpk: m_sToObBm.value
						}}
						onChange={(v) => {
							m_sToObBm.value = v.refpk;
							m_sToObBm.display = v.refname;
							this.props.handleChangeSelectObj(selectObj);
							if (sfbz == 'Sk' || sfbz == 'Ys') {
								this.props.setOrdercubAssign(m_sToObBm, 3);
							}
						}}
						placeholder={m_sToObBm.display || this.state.json['public-000066']} /* 国际化处理： 业务员*/
						isShowUnit={true}
						isShowDisabledData={true}
						unitCondition={() => {
							return {
								pkOrgs: filterObj.pk_org.value,
								TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
							};
						}}
						placeholder={m_sToObBm.display || this.state.json['public-000066']} /* 国际化处理： 业务员*/
						queryCondition={() => {
							return {
								busifuncode: 'all',
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: filterObj.pk_org.value
							};
						}}
					/>
				);
			default:
				break;
		}
	};

	//查询条件
	getQueryContions = (createButtonApp) => {
		let { selectcons, selectObj, filterObj } = this.props;
		let bbhlClone = JSON.parse(JSON.stringify(localBbhl));
		return (
			selectcons.length > 0 && (
				<div className="querycons-style">
					{selectcons.map((item, i) => {
						switch (item.itemType) {
							case 'input':
								if (item.itemKey == 'm_dBbhl') {
									return (
										<div className="right-query-input">
											{this.getEditRateInput(
												item.itemName,
												item.itemKey,
												bbhlClone.scale,
												bbhlClone.localbbhl * 1
											)}
										</div>
									);
								}
								return (
									<div className="right-query-input">
										{this.getInput(item.itemName, item.itemKey)}
									</div>
								);
							case 'date':
								return (
									<div className="right-query-input">
										<h2>{item.itemName}</h2>
										<NCDatePicker
											showStar={true}
											format={format}
											onChange={(d) => {
												selectObj[item.itemKey].value = d;
												selectObj[item.itemKey].display = d;
												this.props.handleChangeSelectObj(selectObj);
											}}
											value={selectObj[item.itemKey].display}
										/>
									</div>
								);
							case 'radio':
								return (
									<div className="right-query-radio">
										<NCRadio.NCRadioGroup
											name=""
											selectedValue={selectObj[item.itemKey].value}
											onChange={(v) => {
												selectObj[item.itemKey].value = v;
												selectObj.m_sToObBm = { display: null, value: null };
												this.props.handleChangeSelectObj(selectObj);
											}}
										>
											<h2 className="radios-title">{item.itemName}</h2>
											<div className="radios">
												{this.props.isCustomer ? (
													<div className="radios-box">
														<NCRadio
															value={0}
															disabled={filterObj.pk_org.value ? '' : 'disabled'}
														>
															{this.state.json['public-000064']}
															{/* 国际化处理： 客户*/}
														</NCRadio>
														<NCRadio
															disabled={filterObj.pk_org.value ? '' : 'disabled'}
															//style={{ 'margin-left': '18%' }}
															value={2}
														>
															{this.state.json['public-000065']}
															{/* 国际化处理： 部门*/}
														</NCRadio>
														<NCRadio
															disabled={filterObj.pk_org.value ? '' : 'disabled'}
															//style={{ 'margin-left': '7%' }}
															value={3}
														>
															{this.state.json['public-000066']}
															{/* 国际化处理： 业务员*/}
														</NCRadio>
													</div>
												) : (
													<div className="radios-box">
														<NCRadio
															disabled={filterObj.pk_org.value ? '' : 'disabled'}
															value={1}
														>
															{this.state.json['public-000063']}
															{/* 国际化处理： 供应商*/}
														</NCRadio>
														<NCRadio
															disabled={filterObj.pk_org.value ? '' : 'disabled'}
															//style={{ 'margin-left': '21%' }}
															value={2}
														>
															{this.state.json['public-000065']}
															{/* 国际化处理： 部门*/}
														</NCRadio>
														<NCRadio
															disabled={filterObj.pk_org.value ? '' : 'disabled'}
															//style={{ 'margin-left': '21%' }}
															value={3}
														>
															{this.state.json['public-000066']}
															{/* 国际化处理： 业务员*/}
														</NCRadio>
													</div>
												)}
											</div>
										</NCRadio.NCRadioGroup>
									</div>
								);
							case 'refer':
								return (
									<div className="right-query-input">
										<h2>{item.itemName}</h2>
										{this.getDifferentRefer()}
									</div>
								);
						}
					})}
					<div className="right-button-style">
						{createButtonApp({
							area: 'list_bottom',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this)
						})}
					</div>
				</div>
			)
		);
	};

	//确认转移事件
	handleTransfers = () => {
		let { selectObj, filterObj } = this.props;
		let { m_sFromOb } = filterObj;
		let { m_sToObBm, m_dBbhl, m_sClrq, m_izrWldx } = selectObj;
		let arrIndex = [];
		selectedData = this.props.getSelectedData();
		if (selectedData.length > 0) {
			let mValue = '';
			let m_sFromObVal = filterObj.m_sFromOb.value;
			switch (m_sFromObVal) {
				case 0:
				case 1:
					mValue = filterObj.m_sKsbm.value;
					break;
				case 2:
					mValue = filterObj.m_sBmbm.value;
					break;
				case 3:
					mValue = filterObj.m_sYwybm.value;
					break;
			}
			if (mValue == m_sToObBm.value) {
				toast({
					color: 'warning',
					content: this.state.json['public-000203'] /* 国际化处理： 转出户与转入户不能相同*/
				});
			} else {
				if (m_sToObBm.value && m_dBbhl.value && m_sClrq.value) {
					for (var i = 0, len = selectedData.length; i < len; i++) {
						if (selectedData[i].data.values.this_sett.value != null) {
							arrIndex.push(selectedData[i].index);
						}
					}
					// 右边的条件
					let selectObjData = {
						m_dBbhl: selectObj.m_dBbhl.value, //本币汇率
						m_iBz: 0, //并账标志
						m_iCurrDig: 0, // 币种小数位数
						m_izrWldx: selectObj.m_izrWldx.value, //转入往来对象
						m_izcWldx: selectObj.m_izcWldx.value, //转出往来对象
						m_sClrq: selectObj.m_sClrq.value + Hms, //处理日期
						m_sCurrency: selectObj.m_sCurrency.value, //币种编码
						m_sDwbm: filterObj.pk_org.value, //m_sDwbm;/* 单位编码 */
						m_sFromObBm: selectObj.m_sFromObBm.value, //转出户
						m_sSfbz: this.props.sfbz, //收付标志
						m_sToObBm: selectObj.m_sToObBm.value, //转入户
						pzglh: 0,
						m_bIsDocument: true,
						accountDateZone: selectObj.m_sClrq.value + Hms, //并账日期
						m_sAccountDate: selectObj.m_sClrq.value + Hms //并账日期
					};
					// 左边的条件
					let filterObjData = {
						m_bIsDocument: true, //核销方式
						ar29: false,
						pk_org: filterObj.pk_org.value,
						m_sFromOb: filterObj.m_sFromOb.value,
						m_sKsbm: filterObj.m_sKsbm.value,
						m_sYwybm: filterObj.m_sYwybm.value,
						m_sBmbm: filterObj.m_sBmbm.value,
						m_sCurrency: filterObj.m_sCurrency.value,
						m_sDjlx: filterObj.m_sDjlx.value,
						m_zrbm: filterObj.m_zrbm.value,
						m_sMinDjbh: filterObj.m_sDjbh[0].value,
						m_sMaxDjbh: filterObj.m_sDjbh[1].value,
						m_sDjrqBeg: filterObj.m_sDjrq[0],
						m_sDjrqEnd: filterObj.m_sDjrq[1],
						m_sMinYbje: filterObj.m_sYbje[0].value,
						m_sMaxYbje: filterObj.m_sYbje[1].value,
						m_sFromObBm: selectObj.m_sFromObBm.value, //转出户
						m_sFromObMc: selectObj.m_izcWldx.value, //转出往来对象
						m_sSfbz: this.props.sfbz //收付标志
					};
					let dataArr = [];
					let selectedData1 = JSON.parse(JSON.stringify(selectedData));
					selectedData1.map((item, key) => {
						let valuesObj = item.data.values;
						for (let v in valuesObj) {
							valuesObj[v] = valuesObj[v].value;
						}
						if (valuesObj.this_sett != null) {
							valuesObj.isEditMoney = valuesObj.this_sett == valuesObj.sum_money_bal ? true : false;
							dataArr.push(valuesObj);
						}
					});
					let data = {
						filterConVO: filterObjData,
						selectDataVO: selectObjData,
						busidatas: dataArr
					};
					if (!!dataArr.length) {
						ajax({
							url: '/nccloud/arap/debtransfer/transfer.do',
							data,
							success: (res) => {
								let { success } = res;
								if (success) {
									this.props.editTable.setStatus(tableid, 'browse'); //设置表格状态为浏览态
									setTimeout(() => {
										this.handlesetTable(arrIndex);
									}, 0);
									toast({
										color: 'success',
										content: this.state.json['public-000204'] /* 国际化处理： 转移成功!*/
									});
								}
							}
						});
					} else {
						toast({
							color: 'warning',
							content: this.state.json['public-000216'] /* 国际化处理： 转移金额不能为空!*/
						});
						return;
					}
				} else {
					if (!m_dBbhl.value) {
						toast({
							color: 'warning',
							content: this.state.json['public-000205'] /* 国际化处理： 请选择对本币汇率!*/
						});
						return;
					} else if (!m_sClrq.value) {
						toast({
							color: 'warning',
							content: this.state.json['public-000206'] /* 国际化处理： 请选择转移日期!*/
						});
						return;
					} else {
						toast({
							color: 'warning',
							content: this.state.json['public-000207'] /* 国际化处理： 请选择转入账户!*/
						});
						return;
					}
				}
			}
		} else {
			toast({
				color: 'warning',
				content: this.state.json['public-000208'] /* 国际化处理： 请选择至少一行进行转移!*/
			});
		}
	};

	//调用父组件的方法
	handlesetTable = (arrIndex) => {
		this.props.handlesetTableData(arrIndex);
	};

	render() {
		let { createButtonApp } = this.props;
		return (
			<div id="queryconditionId">
				<div>{this.getQueryContions(createButtonApp)}</div>
			</div>
		);
	}
}

Showcondition = createPage({})(Showcondition);

export default Showcondition;
