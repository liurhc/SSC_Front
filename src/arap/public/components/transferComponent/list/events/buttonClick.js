import { headButton, bottomButton } from '../../../pubUtils/buttonName';

export default function (props, id) {
  switch (id) {
    case headButton.Hisrecord://转移记录
      this.handleHisrecord();
      break;
    case headButton.Refresh://刷新
      this.refresh();
      break;
    case headButton.Query://查询
      this.handleQueryBtn();
      break;
    case headButton.Empty://清空
      this.handleEmpty();
      break;
    case bottomButton.ConfirmTrans://确认转移
      this.handleTransfers();
      break;
  }
}
