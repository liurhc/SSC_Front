import { ajax } from 'nc-lightapp-front';
const tableid = 'list';
export default function(props) {
	let appcode = props.getSearchParam('c'); //获取小应用编码
	let pagecode = props.getSearchParam('p'); //获取页面编码
	props.createUIDom(
		{
			pagecode: pagecode, //页面id
			appcode: appcode //注册按钮的id
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	);
}

//对表格模板进行加工操作
function modifierMeta(props, meta) {
	meta[tableid].showindex = true; //表格显示序号
	meta[tableid].items = meta[tableid].items.map((item, key) => {
		item.width = 160;
		if (item.attrcode == 'billno') {
			item.renderStatus = 'browse';
			item.render = (a, text, value) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							billLink(text, props);
						}}
					>
						{text.values.billno.value}
					</a>
				);
			};
		}
		return item;
	});
	return meta;
}
//联查单据
function billLink(text, props) {
	ajax({
		url: '/nccloud/arap/arappub/linkarapbill.do',
		async: false,
		data: {
			pk_bill: text.values.pk_bill.value,
			tradeType: text.values.pk_tradetype.value
		},
		success: (res) => {
			props.openTo(res.data.url, res.data.condition);
		}
	});
}
