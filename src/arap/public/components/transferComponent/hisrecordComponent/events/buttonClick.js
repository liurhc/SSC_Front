import { headButton, bottomButton } from '../../../pubUtils/buttonName';

export default function(props, id) {
	switch (id) {
		case headButton.CancelTrans: //取消转移
			this.handleCancelTrans();
			break;
		case headButton.Refresh: //刷新
			this.refresh();
			break;
		case headButton.Query: //查询
			this.handleQueryBtn();
			break;
		case headButton.Empty: //清空
			this.handleEmptyBtn();
			break;
		case headButton.Print: //打印
			this.onPrint();
			break;
		case headButton.Output: //输出
			this.printOutput();
			break;
	}
}
