import React, { Component } from 'react';
import {
	createPage,
	ajax,
	base,
	cacheTools,
	toast,
	print,
	high,
	cardCache,
	getMultiLang,
	createPageIcon
} from 'nc-lightapp-front';
import Querycons from './querycons';
import { buttonClick, initTemplate } from './events/index';
import { businessDate } from '../../businessDate';
import getInitOrg from '../../pubUtils/getInitOrg';
import './index.less';
let { getDefData } = cardCache;
let { PrintOutput } = high;
let { NCBackBtn } = base;
let businessDates;
let title;
const tableid = 'list';
let queryconsData = [
	[
		{
			itemName: '' /* 国际化处理： 财务组织*/,
			itemType: 'refer',
			itemKey: 'pk_org',
			config: { refCode: 'uapbd/refer/org/FinanceOrgTreeRef/index.js' },
			tag: 'FinanceOrgTreeRef',
			showUnitFlg: false
		},
		{
			itemName: '' /* 国际化处理： 转出对象*/,
			itemType: 'input',
			itemKey: 'm_sFromOb',
			config: { refCode: '' },
			tag: ''
		},
		{
			itemName: '' /* 国际化处理： 对象名称*/,
			itemType: 'input',
			itemKey: 'm_sToObBm',
			config: { refCode: '' },
			tag: '',
			showUnitFlg: false
		},
		{
			itemName: '' /* 国际化处理： 币种*/,
			itemType: 'input',
			itemKey: 'm_sCurrency',
			config: { refCode: '' },
			tag: '',
			showUnitFlg: false
		},
		{
			itemName: '' /* 国际化处理： 并账日期*/,
			itemType: 'input',
			itemKey: 'm_sDjrq',
			config: { refCode: '' },
			tag: ''
		},
		{
			itemName: '',
			itemType: 'queryBtn',
			itemKey: '',
			config: { refCode: '' },
			tag: ''
		},
		{
			itemName: '' /* 国际化处理： 原币金额*/,
			itemType: 'inputDouble',
			itemKey: 'm_sYbje',
			config: { refCode: '' },
			tag: ''
		}
	],
	[
		{
			itemName: '' /* 国际化处理： 财务组织*/,
			itemType: 'refer',
			itemKey: 'pk_org',
			config: { refCode: 'uapbd/refer/org/FinanceOrgTreeRef/index.js' },
			tag: 'FinanceOrgTreeRef',
			showUnitFlg: false
		},
		{
			itemName: '' /* 国际化处理： 转出对象*/,
			itemType: 'select',
			itemKey: 'm_sFromOb',
			config: { refCode: '' },
			tag: ''
		},
		{
			itemName: '' /* 国际化处理： 对象名称*/,
			itemType: 'refer',
			itemKey: 'm_sToObBm',
			config: { refCode: '' },
			tag: '',
			showUnitFlg: false
		},
		{
			itemName: '' /* 国际化处理： 币种*/,
			itemType: 'refer',
			itemKey: 'm_sCurrency',
			config: { refCode: 'uapbd/refer/pubinfo/CurrtypeGridRef/index.js' },
			tag: 'CurrtypeGridRef',
			showUnitFlg: false
		},
		{
			itemName: '' /* 国际化处理： 并账日期*/,
			itemType: 'date',
			itemKey: 'm_sDjrq',
			config: { refCode: '' },
			tag: ''
		},
		{
			itemName: '',
			itemType: 'queryBtn',
			itemKey: '',
			config: { refCode: '' },
			tag: ''
		},
		{
			itemName: '' /* 国际化处理： 原币金额*/,
			itemType: 'inputDouble',
			itemKey: 'm_sYbje',
			config: { refCode: '' },
			tag: ''
		}
	]
];

class DebTransHisComponent extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		(this.state = {
			json: {},
			schemeData: {
				pk_org: { display: '', value: null }, //财务组织
				m_sFromOb: { display: props.isCustomer ? '' : '', value: props.isCustomer ? 0 : 1 }, //转出对象/* 国际化处理： 客户,供应商*/
				m_sToObBm: { display: null, value: null }, //对象名称
				m_sCurrency: { display: '', value: null }, //币种
				m_sDjrq: [], //并账日期开始结束
				m_sYbje: [ { display: '', value: null }, { display: '', value: null } ] //最小最大原币金额
			}
		}),
			(this.printData = {
				billtype: '', //单据类型
				appcode: props.getSearchParam('c'), //功能节点编码，即模板编码
				oids: null, // 功能节点的数据主键
				nodekey: 'list', //模板节点标识
				userjson: '', //单据类型,billtype不是必需字段，后台没有设置接收字段，以userjson代替
				pageId: props.getSearchParam('p') //页面编码，以便精度处理
			});
		this.outputData = {
			billtype: '', //单据类型
			funcode: this.props.getSearchParam('c'), //功能节点编码，即模板编码
			appcode: this.props.getSearchParam('c'), //功能节点编码，即模板编码
			nodekey: 'list', //模板节点标识
			oids: '', // 功能节点的数据主键 oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			userjson: '', //单据类型
			outputType: 'output'
		};
		this.handleBack = this.handleBack.bind(this);
		this.handleChangeschemeData = this.handleChangeschemeData.bind(this);
		this.handleEmptyQueryData = this.handleEmptyQueryData.bind(this);
		this.handleGetData = this.handleGetData.bind(this);
		this.onSelected = this.onSelected.bind(this);
		this.onSelectedAllFn = this.onSelectedAllFn.bind(this);
	}
	componentWillMount() {
		let { schemeData } = this.state;
		let toHisTransData = this.props.ViewModel.getData('toHisTransData');
		let appcode = this.props.getSearchParam('c');
		if (toHisTransData != undefined) {
			schemeData.pk_org = toHisTransData.pk_org;
			schemeData.m_sDjrq = toHisTransData.m_sDjrq;
		}
		let callback = (json) => {
			this.setState({ json: json }, () => {
				schemeData.m_sFromOb = {
					display: this.props.isCustomer
						? this.state.json['public-000064']
						: this.state.json['public-000063'],
					value: this.props.isCustomer ? 0 : 1
				}; //转出对象/* 国际化处理： 客户,供应商*/
				queryconsData = [
					[
						{
							itemName: this.state.json['public-000024'] /* 国际化处理： 财务组织*/,
							itemType: 'refer',
							itemKey: 'pk_org',
							config: { refCode: 'uapbd/refer/org/FinanceOrgTreeRef/index.js' },
							tag: 'FinanceOrgTreeRef',
							showUnitFlg: false
						},
						{
							itemName: this.state.json['public-000178'] /* 国际化处理： 转出对象*/,
							itemType: 'input',
							itemKey: 'm_sFromOb',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000179'] /* 国际化处理： 对象名称*/,
							itemType: 'input',
							itemKey: 'm_sToObBm',
							config: { refCode: '' },
							tag: '',
							showUnitFlg: false
						},
						{
							itemName: this.state.json['public-000081'] /* 国际化处理： 币种*/,
							itemType: 'input',
							itemKey: 'm_sCurrency',
							config: { refCode: '' },
							tag: '',
							showUnitFlg: false
						},
						{
							itemName: this.state.json['public-000180'] /* 国际化处理： 并账日期*/,
							itemType: 'input',
							itemKey: 'm_sDjrq',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: '',
							itemType: 'queryBtn',
							itemKey: '',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000181'] /* 国际化处理： 原币金额*/,
							itemType: 'inputDouble',
							itemKey: 'm_sYbje',
							config: { refCode: '' },
							tag: ''
						}
					],
					[
						{
							itemName: this.state.json['public-000024'] /* 国际化处理： 财务组织*/,
							itemType: 'refer',
							itemKey: 'pk_org',
							config: { refCode: 'uapbd/refer/org/FinanceOrgTreeRef/index.js' },
							tag: 'FinanceOrgTreeRef',
							showUnitFlg: false
						},
						{
							itemName: this.state.json['public-000178'] /* 国际化处理： 转出对象*/,
							itemType: 'select',
							itemKey: 'm_sFromOb',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000179'] /* 国际化处理： 对象名称*/,
							itemType: 'refer',
							itemKey: 'm_sToObBm',
							config: { refCode: '' },
							tag: '',
							showUnitFlg: false
						},
						{
							itemName: this.state.json['public-000081'] /* 国际化处理： 币种*/,
							itemType: 'refer',
							itemKey: 'm_sCurrency',
							config: { refCode: 'uapbd/refer/pubinfo/CurrtypeGridRef/index.js' },
							tag: 'CurrtypeGridRef',
							showUnitFlg: false
						},
						{
							itemName: this.state.json['public-000180'] /* 国际化处理： 并账日期*/,
							itemType: 'date',
							itemKey: 'm_sDjrq',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: '',
							itemType: 'queryBtn',
							itemKey: '',
							config: { refCode: '' },
							tag: ''
						},
						{
							itemName: this.state.json['public-000181'] /* 国际化处理： 原币金额*/,
							itemType: 'inputDouble',
							itemKey: 'm_sYbje',
							config: { refCode: '' },
							tag: ''
						}
					]
				];
				setTimeout(() => {
					businessDates = businessDate();
					schemeData.m_sDjrq = [ businessDates, businessDates ];
					this.setState({
						schemeData
					});
				}, 0);
				switch (this.props.title) {
					case 'skbzjl':
						title = this.state.json['public-000182']; /* 国际化处理： 收款并账记录*/
						break;
					case 'zwzyjl':
						title = this.state.json['public-000183']; /* 国际化处理： 债务转移记录*/
						break;
					case 'fkbzjl':
						title = this.state.json['public-000184']; /* 国际化处理： 付款并账记录*/
						break;
					case 'zqzyjl':
						title = this.state.json['public-000185']; /* 国际化处理： 债权转移记录*/
						break;
				}
			});
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
		if (toHisTransData == undefined) {
			getInitOrg(appcode, this.cb);
		}
	}

	cb = (pkOrg) => {
		let { schemeData } = this.state;
		schemeData.pk_org = {
			display: pkOrg.display,
			value: pkOrg.value,
			refpk2: pkOrg.refpk2
		};
		this.setState({
			schemeData
		});
	};
	//在第一次渲染后调用
	componentDidMount() {
		this.props.button.setButtonDisabled('CancelTrans', true);
		initTemplate.call(this, this.props);
		if ('browse' == this.props.getUrlParam('status')) {
			let dataArr = [];
			let linkurl = '';
			let data = {};
			if ('fip' == this.props.getUrlParam('scene')) {
				dataArr = cacheTools.get('checkedData');
				linkurl = '/nccloud/arap/arappub/queryGridByPK.do';
				if (dataArr) {
					data = {
						fipLink: dataArr,
						pageId: this.props.getSearchParam('p')
					};
				}
				ajax({
					url: linkurl,
					data: data,
					success: (res) => {
						let { success, data } = res;
						//隐藏所有按钮
						this.props.button.setButtonVisible([ 'Back', 'Cancle', 'Query', 'Empty' ], false);
						if (data) {
							this.props.cardTable.setTableData(tableid, data[tableid]);
						} else {
							this.props.cardTable.setTableData(tableid, { rows: [] });
						}
					}
				});
			}
		} else {
			//getData(this.props);
		}
	}

	// 获取查询条件参数
	handleGetData = (data) => {
		if (!data) {
			this.props.cardTable.setTableData(tableid, { rows: [] });
		} else {
			this.props.cardTable.setTableData(tableid, data);
		}
	};

	//改变查询条件数据
	handleChangeschemeData = (schemeData) => {
		this.setState({
			schemeData: schemeData
		});
	};

	//取消转移按钮
	handleCancelTrans = () => {
		let getConsDatas = getDefData('his-query', this.props.pagecode);
		const selectedData = this.props.cardTable.getCheckedRows(tableid);
		if (selectedData.length == 0) {
			toast({ color: 'warning', content: this.state.json['public-000041'] });
			return;
		}
		let indexArr = [];
		let filterConVO;
		if (getConsDatas) {
			//判断是否为本页面查询的数据  如果getConsDatas为undefined 则表格数据为别处跳转过来的
			filterConVO = {
				m_bIsDocument: true,
				ar29: false,
				pk_org: getConsDatas.pk_org, //组织
				m_sCurrency: getConsDatas.m_sCurrency,
				m_sDjrqBeg: getConsDatas.m_sDjrqBeg, //单据日期开始
				m_sDjrqEnd: getConsDatas.m_sDjrqEnd, // 单据日期结束
				m_sDwbm: getConsDatas.pk_org, //m_sDwbm;/* 单位编码 */
				m_sFromOb: getConsDatas.m_sFromOb,
				m_sKsbm: getConsDatas.m_sKsbm,
				m_sFromObBm: getConsDatas.m_sToObBm,
				m_sFromObMc: getConsDatas.m_sFromObMc,
				m_sSfbz: this.props.sfbz //收付标志
			};
		} else {
			let m_sSfbz = selectedData[0].data.values.billclass.value,
				sfbz;
			switch (m_sSfbz) {
				case 'ys':
					sfbz = 'Ys';
					break;
				case 'yf':
					sfbz = 'Yf';
					break;
				case 'sk':
					sfbz = 'Sk';
					break;
				case 'fk':
					sfbz = 'Fk';
					break;
			}
			filterConVO = {
				pk_org: selectedData[0].data.values.pk_org.value, //组织
				m_sDwbm: selectedData[0].data.values.pk_org.value, //m_sDwbm;/* 单位编码 */
				m_sSfbz: sfbz //收付标志
			};
		}

		let linkVOs = [];
		for (var i = 0, len = selectedData.length; i < len; i++) {
			let o = {
				pk_bill: selectedData[i].data.values.pk_bill.value,
				pk_item: (selectedData[i].data.values.pk_item && selectedData[i].data.values.pk_item.value) || '',
				busino: selectedData[i].data.values.busino.value,
				busidate: selectedData[i].data.values.busidate.value
			};
			linkVOs.push(o);
			indexArr.push(selectedData[i].index);
		}
		let data = {
			filterConVO: filterConVO,
			linkVOs: linkVOs
		};
		ajax({
			url: '/nccloud/arap/debtransfer/cancletransfer.do',
			data: data,
			success: function(res) {
				let { success, data } = res;
				if (success) {
					toast({ color: 'success', content: this.state.json['public-000186'] });
					this.props.cardTable.delRowsByIndex(tableid, indexArr);
					let tableAllDatas = this.props.cardTable.getAllRows(tableid, true);
					tableAllDatas = tableAllDatas.filter((item) => !item.selected == true);
					if (tableAllDatas.length == 0) {
						this.props.button.setButtonDisabled([ 'CancelTrans', 'Print', 'Output' ], true);
					}
				}
			}.bind(this)
		});
	};

	//返回
	handleBack = () => {
		this.props.pushTo('/list', {
			pagecode: this.props.pagecode
		});
	};

	/**
	 * 刷新
	 * @param refresh
	*/
	refresh = () => {
		let ConsDatas = getDefData('his-query', this.props.pagecode);
		this.child.handleQueryBtn(ConsDatas, 1);
	};

	/**
	 * 打印功能
	 */
	onPrint = () => {
		let allData = this.props.cardTable.getAllData(tableid).rows;
		let pk_bills = [];
		for (var i = 0, len = allData.length; i < len; i++) {
			pk_bills.push(allData[i].values.pk_debtstransfer.value);
		}
		if (pk_bills.length == 0) {
			toast({ color: 'warning', content: this.state.json['public-000187'] }); /* 国际化处理： 打印数据为空!*/
			return;
		}
		this.printData.oids = pk_bills;
		this.printData.userjson = this.props.sfbz;
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/arap/arappub/printDebtTransferAction.do', //后台服务url
			this.printData,
			false
		);
	};

	//打印输出
	printOutput = () => {
		let allData = this.props.cardTable.getAllData(tableid).rows;
		let pk_bills = [];
		for (var i = 0, len = allData.length; i < len; i++) {
			pk_bills.push(allData[i].values.pk_debtstransfer.value);
		}
		if (pk_bills.length == 0) {
			toast({ color: 'warning', content: this.state.json['public-000188'] }); /* 国际化处理： 打印输出数据为空!*/
			return;
		}
		this.outputData.oids = pk_bills;
		this.refs.printOutput.open();
	};

	//输出成功回调函数
	onSubmit() {}

	//清空查询条件
	handleEmptyQueryData = () => {
		let localSchemeData = {
			pk_org: { display: '', value: null }, //财务组织
			m_sFromOb: {
				display: this.props.isCustomer ? this.state.json['public-000064'] : this.state.json['public-000063'],
				value: this.props.isCustomer ? 0 : 1
			}, //转出对象/* 国际化处理： 客户,供应商*/
			m_sToObBm: { display: null, value: null }, //对象名称
			m_sCurrency: { display: '', value: null }, //币种
			m_sDjrq: [], //并账日期开始结束
			m_sYbje: [ { display: '', value: null }, { display: '', value: null } ] //最小最大原币金额
		};
		this.setState({
			schemeData: localSchemeData
		});
	};

	//单选逻辑
	onSelected(props, moduleId, record, index, status) {
		let datas = this.props.cardTable.getAllRows(tableid);
		let indexArr = [];
		let busino = record.values.busino.value;
		for (let i = 0, len = datas.length; i < len; i++) {
			if (busino == datas[i].values.busino.value) {
				indexArr.push(i);
			}
		}
		if (status) {
			this.props.cardTable.selectRowsByIndex(tableid, indexArr);
		} else {
			this.props.cardTable.unSelectRowsByIndex(tableid, indexArr);
		}
		setTimeout(() => {
			this.handleBtnVisible();
		}, 0);
	}

	//全选逻辑
	onSelectedAllFn = (props, moduleId, status, length) => {
		if (status) {
			this.props.cardTable.selectAllRows(tableid, true);
		} else {
			this.props.cardTable.selectAllRows(tableid, false);
		}
		setTimeout(() => {
			this.handleBtnVisible();
		}, 0);
	};

	//通过选中数据来判断按钮显隐性
	handleBtnVisible = () => {
		let selectedDatas = this.props.cardTable.getCheckedRows(tableid);
		if (selectedDatas.length > 0) {
			this.props.button.setButtonDisabled('CancelTrans', false);
		} else {
			this.props.button.setButtonDisabled('CancelTrans', true);
		}
	};

	onRef = (ref) => {
		this.child = ref;
	};

	render() {
		let { cardTable, button, isBack } = this.props;
		let { createButtonApp, setButtonDisabled } = button;
		let { createCardTable } = cardTable;
		return (
			<div className="nc-single-table" id="hisrecordId">
				{/* 标题 title */}
				<div className="nc-singleTable-header-area">
					{/* 按钮区  btn-group */}
					{!isBack && createPageIcon && createPageIcon()}
					<h2 className="title">
						{isBack && <NCBackBtn onClick={this.handleBack} />}
						{title}
					</h2>
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				{/* 查询条件区 */}
				<Querycons
					schemeData={this.state.schemeData}
					handleChangeschemeData={this.handleChangeschemeData}
					handleEmptyQueryData={this.handleEmptyQueryData}
					createButtonApp={createButtonApp}
					queryconsData={queryconsData}
					getData={this.handleGetData}
					setButtonDisabled={setButtonDisabled}
					sfbz={this.props.sfbz}
					isCustomer={this.props.isCustomer}
					pagecode={this.props.pagecode}
					onRef={this.onRef}
				/>
				{/* 列表区 */}
				<div className="nc-singleTable-table-area">
					{createCardTable(tableid, {
						//列表区
						showIndex: true, //显示序号
						showCheck: true, //显示复选框
						pageSize: 10,
						onSelected: this.onSelected,
						onSelectedAll: this.onSelectedAllFn,
						hideSwitch: () => {
							return false;
						}
					})}
				</div>
				{/* {打印输出} */}
				<PrintOutput
					ref="printOutput"
					className="print-output"
					url="/nccloud/arap/arappub/outDebtTransferAction.do"
					data={this.outputData}
					callback={this.onSubmit}
				/>
			</div>
		);
	}
}

DebTransHisComponent = createPage({
	initTemplate: initTemplate
})(DebTransHisComponent);

export default DebTransHisComponent;
