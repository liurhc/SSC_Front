import React, { Component } from 'react';
import { ajax, base, createPage, toast, cardCache, getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../../../ReferLoader';
import { buttonClick } from '../events/index';
import './index.less';
let { setDefData, getDefData } = cardCache;
const { NCFormControl, NCNumber, NCDatePicker, NCSelect } = base;
const { NCRangePicker } = NCDatePicker;
const NCOption = NCSelect.NCOption;
const format = 'YYYY-MM-DD';
const tableid = 'list';

class Comconditions extends Component {
	constructor(props) {
		super(props);
		this.NCOptionData = [
			this.props.isCustomer ? { value: 0, display: '' } : { value: 1, display: '' } /* 国际化处理： 客户,供应商*/,
			{ value: 2, display: '' } /* 国际化处理： 部门*/,
			{ value: 3, display: '' } /* 国际化处理： 业务员*/
		];
		this.state = {
			json: {},
			filterObj: {
				pk_org: null, //财务组织
				m_sFromOb: null, //转出对象
				m_sFromObBm: null, //转出户
				m_sFromObMc: null, //转出户display
				m_sCurrency: null, //币种
				m_sDjrqBeg: null, //并账日期开始
				m_sDjrqEnd: null, //并账日期结束
				m_sMaxYbje: null, //最大原币金额
				m_sMinYbje: null, //最小原币金额
				m_sSfbz: this.props.sfbz, //收付标志
				m_sUser: null //用户id
			} //查询条件
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				this.NCOptionData = [
					this.props.isCustomer
						? { value: 0, display: this.state.json['public-000064'] }
						: { value: 1, display: this.state.json['public-000063'] } /* 国际化处理： 客户,供应商*/,
					{ value: 2, display: this.state.json['public-000065'] } /* 国际化处理： 部门*/,
					{ value: 3, display: this.state.json['public-000066'] } /* 国际化处理： 业务员*/
				];
			});
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	componentDidMount() {
		this.props.onRef(this);
	}

	//对象名称显示不同参照
	getDifferentRefer = () => {
		let { schemeData } = this.props;
		let { m_sToObBm, m_sFromOb, pk_org } = schemeData;
		switch (m_sFromOb.value) {
			case 0:
				return (
					<Referloader
						tag="CustomerDefaultTreeGridRef"
						refcode="uapbd/refer/customer/CustomerDefaultTreeGridRef/index.js"
						value={{
							refname: m_sToObBm.display,
							refpk: m_sToObBm.value
						}}
						onChange={(v) => {
							m_sToObBm.value = v.refpk;
							m_sToObBm.display = v.refname;
							this.props.handleChangeschemeData(schemeData);
						}}
						unitValueIsNeeded ={false} //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						isMultiSelectedEnabled={false}
						placeholder={m_sToObBm.display || this.state.json['public-000064']} /* 国际化处理： 客户*/
						queryCondition={() => {
							return {
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: pk_org.value

							};
						}}
					/>
				);
				break;
			case 1:
				return (
					<Referloader
						tag="SupplierRefTreeGridRef"
						refcode="uapbd/refer/supplier/SupplierRefTreeGridRef/index.js"
						value={{
							refname: m_sToObBm.display,
							refpk: m_sToObBm.value
						}}
						onChange={(v) => {
							m_sToObBm.value = v.refpk;
							m_sToObBm.display = v.refname;
							this.props.handleChangeschemeData(schemeData);
						}}
						unitValueIsNeeded ={false} //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						placeholder={m_sToObBm.display || this.state.json['public-000063']} /* 国际化处理： 供应商*/
						queryCondition={() => {
							return {
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: pk_org.value

							};
						}}
					/>
				);
				break;
			case 2:
				return (
					<Referloader
						tag="DeptTreeRef"
						refcode="uapbd/refer/org/DeptNCTreeRef/index.js"
						value={{
							refname: m_sToObBm.display,
							refpk: m_sToObBm.value
						}}
						onChange={(v) => {
							m_sToObBm.value = v.refpk;
							m_sToObBm.display = v.refname;
							this.props.handleChangeschemeData(schemeData);
						}}
						placeholder={m_sToObBm.display || this.state.json['public-000065']} /* 国际化处理： 部门*/
						isShowUnit={true}
						unitValueIsNeeded ={false} //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						unitCondition={() => {
							return {
								pkOrgs: pk_org.value,
								TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
							};
						}}
						queryCondition={() => {
							return {
								busifuncode: 'all',
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: pk_org.value
							};
						}}
					/>
				);
				break;
			case 3:
				return (
					<Referloader
						tag="PsndocTreeGridRef"
						refcode="uapbd/refer/psninfo/PsndocTreeGridRef/index.js"
						value={{
							refname: m_sToObBm.display,
							refpk: m_sToObBm.value
						}}
						onChange={(v) => {
							m_sToObBm.value = v.refpk;
							m_sToObBm.display = v.refname;
							this.props.handleChangeschemeData(schemeData);
						}}
						placeholder={m_sToObBm.display || this.state.json['public-000066']} /* 国际化处理： 业务员*/
						isShowUnit={true}
						unitValueIsNeeded ={false} //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						unitCondition={() => {
							return {
								pkOrgs: pk_org.value,
								TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
							};
						}}
						queryCondition={() => {
							return {
								busifuncode: 'all',
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								pk_org: pk_org.value
							};
						}}
					/>
				);
				break;
			default:
				break;
		}
	};

	//参照展示
	getRefer = (tag, refcode, key, schemeData) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<Referloader
						showStar={key == 'pk_org' ? true : false}
						tag={tag}
						refcode={refcode}
						value={{
							refname: schemeData[key].display,
							refpk: schemeData[key].value,
							refpk2: schemeData[key].refpk2
						}}
						onChange={(v) => {
							schemeData[key].value = v.refpk;
							schemeData[key].refpk2 = v.refpk2;
							schemeData[key].display = v.refname;
							this.props.handleChangeschemeData(schemeData);
						}}
						unitValueIsNeeded ={false} //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						queryCondition={() => {
							if (tag == 'FinanceOrgTreeRef') {
								return {
									AppCode: this.props.getSearchParam('c'),
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'

								};
							} else if (tag == 'CurrtypeGridRef') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y'

								};
							}
						}}
					/>
				</div>
			</div>
		);
	};

	//input展示 double
	getInputTow = (name, key, schemeData) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCFormControl
						style={{ 'text-align': 'left' }}
						disabled={'disabled'}
						value={schemeData[key][0].value}
						placeholder={name}
					/>
					<span>-</span>
					<NCFormControl
						style={{ 'text-align': 'left' }}
						disabled={'disabled'}
						value={schemeData[key][1].value}
						placeholder={name}
					/>
				</div>
			</div>
		);
	};

	//可编辑的input框
	getEditInputTow = (name, key, schemeData) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCNumber
						style={{ 'text-align': 'left' }}
						value={schemeData[key][0].display}
						placeholder={name}
						scale={4}
						onChange={(v) => {
							schemeData[key][0].display = v;
							schemeData[key][0].value = v;
							this.props.handleChangeschemeData(schemeData);
						}}
					/>
					<span>-</span>
					<NCNumber
						style={{ 'text-align': 'left' }}
						value={schemeData[key][1].display}
						placeholder={name}
						scale={4}
						onChange={(v) => {
							schemeData[key][1].display = v;
							schemeData[key][1].value = v;
							this.props.handleChangeschemeData(schemeData);
						}}
					/>
				</div>
			</div>
		);
	};

	//初始条件 浏览态
	getInitialShow = (createButtonApp) => {
		let { queryconsData, schemeData } = this.props;
		return (
			queryconsData[0].length > 0 && (
				<div className="querycons-style">
					{queryconsData[0].map((item, i) => {
						switch (item.itemType) {
							case 'refer':
								return (
									<div className="contions-box">
										{this.getRefer(item.tag, item.config.refCode, item.itemKey, schemeData)}
									</div>
								);
								break;
							case 'inputDouble':
								return (
									<div className="contions-box">
										{this.getInputTow(item.itemName, item.itemKey, schemeData)}
									</div>
								);
								break;
							case 'queryBtn':
								return (
									<div className="contions-box btn">
										{createButtonApp({
											area: 'list_head_cons',
											buttonLimit: 4,
											onButtonClick: buttonClick.bind(this)
										})}
									</div>
								);
								break;
							case 'input':
								if (item.itemKey == 'm_sFromOb') {
									return (
										<div className="contions-box">
											<div className="finance">
												<div className="finance-refer">
													<NCFormControl disabled={'disabled'} placeholder={item.itemName} />
												</div>
											</div>
										</div>
									);
								} else {
									return (
										<div className="contions-box">
											<div className="finance">
												<div className="finance-refer">
													<NCFormControl
														disabled={'disabled'}
														value={schemeData[item.itemKey].value}
														placeholder={item.itemName}
													/>
												</div>
											</div>
										</div>
									);
								}
								break;
						}
					})}
				</div>
			)
		);
	};

	//财务组织选完之后 条件
	getReferShow = (NCOptions, createButtonApp) => {
		let { queryconsData, schemeData } = this.props;
		return (
			queryconsData[1].length > 0 && (
				<div className="querycons-style">
					{queryconsData[1].map((item, i) => {
						switch (item.itemType) {
							case 'refer':
								if (item.itemKey == 'm_sToObBm') {
									return <div className="contions-box">{this.getDifferentRefer()}</div>;
								} else {
									return (
										<div className="contions-box">
											{this.getRefer(item.tag, item.config.refCode, item.itemKey, schemeData)}
										</div>
									);
								}
								break;
							case 'inputDouble':
								return (
									<div className="contions-box">
										{this.getEditInputTow(item.itemName, item.itemKey, schemeData)}
									</div>
								);
								break;
							case 'queryBtn':
								return (
									<div className="contions-box btn">
										{createButtonApp({
											area: 'list_head_cons',
											buttonLimit: 4,
											onButtonClick: buttonClick.bind(this)
										})}
									</div>
								);
								break;
							case 'select':
								return (
									<div className="contions-box">
										<NCSelect
											showStar={true}
											value={schemeData[item.itemKey].display}
											onChange={(v) => {
												schemeData[item.itemKey].display = v.display;
												schemeData[item.itemKey].value = v.value;
												schemeData.m_sToObBm = { display: null, value: null };
												this.props.handleChangeschemeData(schemeData);
											}}
										>
											{NCOptions}
										</NCSelect>
									</div>
								);
								break;
							case 'date':
								return (
									<div className="contions-box">
										<NCRangePicker
											format={format}
											onChange={(d) => {
												schemeData[item.itemKey] = d;
												this.props.handleChangeschemeData(schemeData);
											}}
											showClear={true}
											showOk={true}
											className={'range-fixed'}
											value={schemeData[item.itemKey]}
											placeholder={this.state.json['public-000142']} /* 国际化处理： 开始 ~ 结束*/
											dateInputPlaceholder={[
												this.state.json['public-000143'],
												this.state.json['public-000144']
											]} /* 国际化处理： 开始,结束*/
										/>
									</div>
								);
								break;
						}
					})}
				</div>
			)
		);
	};

	//查询按钮
	handleQueryBtn = (ConsDatas, flag) => {
		let { schemeData } = this.props;
		this.props.setButtonDisabled('CancelTrans', true);
		if (schemeData.pk_org.value) {
			let filterConVO = {
				pk_org: schemeData.pk_org.value,
				m_sCurrency: schemeData.m_sCurrency.value,
				m_sDjrqBeg: schemeData.m_sDjrq[0] ? schemeData.m_sDjrq[0] + ' 00:00:00' : null,
				m_sDjrqEnd: schemeData.m_sDjrq[1] ? schemeData.m_sDjrq[1] + ' 23:59:59' : null,
				m_sFromOb: schemeData.m_sFromOb.value,
				m_sFromObBm: schemeData.m_sToObBm.value,
				m_sFromObMc: schemeData.m_sToObBm.display,
				m_sKsbm: schemeData.m_sToObBm.value,
				m_sMinYbje: schemeData.m_sYbje[0].value == '' ? null : schemeData.m_sYbje[0].value,
				m_sMaxYbje: schemeData.m_sYbje[1].value == '' ? null : schemeData.m_sYbje[1].value,
				m_sSfbz: this.props.sfbz //收付标志k
			};
			setDefData('his-query', this.props.pagecode, JSON.parse(JSON.stringify(filterConVO)));
			ajax({
				url: '/nccloud/arap/debtransfer/hisRecord.do',
				data: {
					pageId: this.props.getSearchParam('p'),
					filterConVO: filterConVO
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data == undefined) {
							if (flag == 1) {
								toast({ color: 'success', title: this.state.json['public-000210'] }); /* 国际化处理： 刷新成功！*/
							} else {
								toast({
									color: 'warning',
									content: this.state.json['public-000209']
								}); /* 国际化处理： 未查询到符合条件的数据*/
							}
							this.props.getData('');
						} else {
							if (flag == 1) {
								toast({ color: 'success', title: this.state.json['public-000210'] }); /* 国际化处理： 刷新成功！*/
							} else {
								toast({
									color: 'success',
									content:
										this.state.json['public-000214'] +
										data[tableid].rows.length +
										this.state.json['public-000215']
								}); /* 国际化处理： 已成功共几条数据！*/
							}

							this.props.getData(data[tableid]);
						}
					}
				}
			});
		} else {
			if (flag == 1) {
				toast({ color: 'success', title: this.state.json['public-000210'] }); /* 国际化处理： 已成功！*/
			} else {
				toast({ color: 'warning', content: this.state.json['public-000189'] }); /* 国际化处理： 带星号的为必选项！*/
			}
		}
	};

	//清空按钮
	handleEmptyBtn = () => {
		this.props.handleEmptyQueryData();
	};

	render() {
		let { createButtonApp, schemeData } = this.props;
		let { pk_org } = schemeData;
		const NCOptions = this.NCOptionData.map((province) => <NCOption value={province}>{province.display}</NCOption>);
		return (
			<div id="re-de-queryconditionId">
				{pk_org.value ? (
					<div>{this.getReferShow(NCOptions, createButtonApp)}</div>
				) : (
					<div>{this.getInitialShow(createButtonApp)}</div>
				)}
			</div>
		);
	}
}

Comconditions = createPage({})(Comconditions);

export default Comconditions;
