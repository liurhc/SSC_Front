import { ajax } from 'nc-lightapp-front';


/**
 * 资金联查收付单据列表页面   
 * @author xiejhk
 *
 */
export default function cmpLinkArapList(props, tableId,pk_billtype) {
    let pk_src = props.getUrlParam('pk_src');
    let linkdata;
    if(pk_src){
        linkdata = {
            "dest_billtype": pk_billtype,
            "pk_src": pk_src,
            "pageId": props.getSearchParam('p')
        };
    }
    let linkurl = '/nccloud/arap/arappub/cmpLinkArapList.do';
    ajax({
        url: linkurl,
        data: linkdata,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                if (data) {
                    props.table.setAllTableData(tableId, data[tableId]);
                } else {
                    props.table.setAllTableData(tableId, { rows: [] });
                }
            }
        }
    });
}