let cartData = {};
let formID = '',
    tableID = '',
    checkedRows,
    bbhl,
    conDatas,
    title = '';
let getCartData = function(data,formId,tableId) {
  cartData = data;
  formID = formId;
  tableID = tableId;
}

let getTitle = function(tit) {
  title = tit;
}

//获取选中数据
let getCheckedRows = function(data) {
  checkedRows = data;
}

//获取汇率
let getBbhlrate = function(rate) {
  bbhl = rate;
}

//获取债权转移查询条件
let getQueryTransfer = function(contions) {
  conDatas = contions;
}

export {
  getCartData,
  getTitle, 
  cartData, 
  formID, 
  tableID, 
  title, 
  getCheckedRows, 
  checkedRows,
  getBbhlrate,
  bbhl,
  getQueryTransfer,
  conDatas
};
