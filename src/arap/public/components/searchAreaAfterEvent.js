
export default function afterEvent(key, props, moduleId) {
    //切换财务组织 情况
    var keys = [

        'pk_deptid', 'bodys.pk_deptid', 'pk_deptid_v', 'bodys.pk_deptid_v', 'supplier', 'customer', 'bodys.supplier', 'bodys.customer', 'subjcode',
        'bodys.subjcode', 'payaccount', 'bodys.payaccount', 'recaccount', 'bodys.recaccount', 'ordercubasdoc', 'bodys.ordercubasdoc', 'pk_psndoc',
        'bodys.pk_psndoc', 'pk_subjcode', 'bodys.pk_subjcode', 'bankrollprojet', 'bodys.bankrollprojet', 'cashitem',
        'bodys.cashitem', 'checkelement', 'bodys.checkelement', 'bodys.material', 'bodys.pk_payterm', 'bodys.project',
        'bodys.project_task'
    ];
    if (key == "pk_org") {
        for (var i = 0; i < keys.length; i++) {
            this.props.search.setSearchValByField(moduleId, keys[i], { value: '', display: '' });
        }
    }

    if (key == 'bodys.objtype') {
        let meta = this.props.meta.getMeta();
        if (this.billType == 'F1' || this.billType == 'F3' || this.billType == "23E1") {
            meta[moduleId].items.map((item) => {
                if (item.attrcode == 'bodys.recaccount' && this.props.search.getSearchValByField(moduleId, 'bodys.objtype') != undefined && this.props.search.getSearchValByField(moduleId, 'bodys.objtype') != null && this.props.search.getSearchValByField(moduleId, 'bodys.objtype').value != null && this.props.search.getSearchValByField(moduleId, 'bodys.objtype').value.firstvalue != null) {
                    var objtypeValue = (this.props.search.getSearchValByField(moduleId, 'bodys.objtype')).value.firstvalue;
                    if (objtypeValue == "1" || objtypeValue == "2" || objtypeValue == "") {
                        item.itemType = 'refer'
                        item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                        item.refName = this.state.json['public-000147']/* 国际化处理： 供应商银行账户*/
                    } else if (objtypeValue == "3") {
                        item.itemType = 'refer'
                        item.refcode = 'uapbd/refer/pubinfo/PsnbankaccGridRef/index'
                        item.refName = this.state.json['public-000148']/* 国际化处理： 个人银行账户*/
                    }  else {
                        item.itemType = 'refer'
                        item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                        item.refName = this.state.json['public-000149']/* 国际化处理： 客户银行账户*/
                    }
                } 
                return item;
            })
            this.props.renderItem('search', moduleId, 'bodys.recaccount', null);
        } else if (this.billType == 'F0' || this.billType == 'F2' || this.billType == "23E0"){
            meta[moduleId].items.map((item) => {
                if (item.attrcode == 'bodys.payaccount' && this.props.search.getSearchValByField(moduleId, 'bodys.objtype') != undefined && this.props.search.getSearchValByField(moduleId, 'bodys.objtype') != null && this.props.search.getSearchValByField(moduleId, 'bodys.objtype').value != null && this.props.search.getSearchValByField(moduleId, 'bodys.objtype').value.firstvalue != null) {
                    var objtypeValue = (this.props.search.getSearchValByField(moduleId, 'bodys.objtype')).value.firstvalue;
                    if (objtypeValue == "1") {
                        item.itemType = 'refer'
                        item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                        item.refName = this.state.json['public-000147']/* 国际化处理： 供应商银行账户*/
                    } else if (objtypeValue == "3") {
                        item.itemType = 'refer'
                        item.refcode = 'uapbd/refer/pubinfo/PsnbankaccGridRef/index'
                        item.refName = this.state.json['public-000148']/* 国际化处理： 个人银行账户*/
                    } else {
                        item.itemType = 'refer'
                        item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                        item.refName = this.state.json['public-000149']/* 国际化处理： 客户银行账户*/
                    }
                } 
                return item;
            })
            this.props.renderItem('search', moduleId, 'bodys.payaccount', null);
        }

    }

}
