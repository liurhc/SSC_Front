import { high } from 'nc-lightapp-front';
const { Refer } = high;


export default function (props = {}) {
    var conf = {
        refType:'tree',
        refName:this.state.json['public-000062'],/* 国际化处理： 建账财务组织*/
        refCode:'arap.public.components.InitBillClossRef',
        queryTreeUrl:'/nccloud/arap/arappub/orgclossref.do',
        isMultiSelectedEnabled:false,
    };
    return <Refer {...Object.assign(conf,props)} />
}

       
