import { ajax,base } from 'nc-lightapp-front';
let { Message } = base;

export default function buttonClick(props, id) {
	switch (id) {
		case 'CloseAccount': //结账
			this.handleEndAccount()
			break;
		case 'UnCloseAccount': //取消结账
			this.onCancelReckoning()
			break;

		case 'Refresh'://刷新按钮
			this.getTableData()
			break;
		
	}
}
