import React, { Component } from 'react';
import { high, base, ajax, toast, getBusinessInfo, createPage, getMultiLang, createPageIcon } from 'nc-lightapp-front';
const { NCButton, NCRow, NCCol, NCTable, NCButtonGroup } = base;
import ReportModal from './reportModal/reportModal';
import ReferLoader from '../../../public/ReferLoader/index.js';
import { buttonClick, initTemplate } from './events';
import './index.less';

/**
 * 结账的组件
 */
class Closeaccount extends Component {
	constructor(props) {
		super(props);
		this.state = {
			nccCloseAccountVOs: [], //表格数据
			showReportModal: false, //是否展示结账报告的模态框
			json: {}
		};
		this.Info = {
			pk_org: { display: '', value: '' }, //PK_ORG
			noSettleMonth: { display: '', value: '' }, //待结账月份
			m_sBegDate: '', //待结账会计期间开始
			m_sEndDate: '', //待结账月份会计期间结束
			reportTableData: '', //结账报告表格数据
			controlSettleButton: true //控制结账按钮是否显示
		};
		this.Defaultcolumns = [];
	}

	componentWillMount() {
		let callback = (json) => {
			//表头数据
			this.Defaultcolumns = [
				{
					title: json['public-000002'] /* 国际化处理： 会计期间 */,
					dataIndex: 'period',
					key: 'period',
					width: 100
				},
				{
					title: json['public-000003'] /* 国际化处理： 会计开始时间 */,
					dataIndex: 'startDate',
					key: 'startDate',
					width: 150
				},
				{
					title: json['public-000004'] /* 国际化处理： 会计结束时间 */,
					dataIndex: 'endDate',
					key: 'endDate',
					width: 150
				},
				{
					title: json['public-000005'] /* 国际化处理： 是否结账*/,
					dataIndex: 'dispReckedOrNot',
					key: 'dispReckedOrNot',
					width: 100
				},
				{
					title: json['public-000006'] /* 国际化处理： 结账人*/,
					dataIndex: 'endaccuser',
					key: 'endaccuser',
					width: 150
				},
				{
					title: json['public-000007'] /* 国际化处理： 结账时间*/,
					dataIndex: 'endacctime',
					key: 'endacctime',
					width: 150
				},
				{
					title: json['public-000008'] /* 国际化处理： 取消结账*/,
					dataIndex: 'unendaccuser',
					key: 'unendaccuser',
					width: 100
				},
				{
					title: json['public-000009'] /* 国际化处理： 取消结账时间*/,
					dataIndex: 'unendacctime',
					key: 'unendacctime',
					width: 100
				}
			];
			this.setState({ json: json }, () => {
				ajax({
					url: '/nccloud/platform/appregister/queryappcontext.do',
					data: {
						appcode: this.props.getSearchParam('c')
					},
					success: (res) => {
						let data = res.data;
						if (data.pk_org) {
							this.Info.pk_org.display = data.org_Name;
							this.Info.pk_org.value = data.pk_org;
							this.Info.pk_org.refpk2 = data.pk_org;
							this.getTableData();
						}
					}
				});
			});
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	getHeadData = () => {
		let { pk_org, noSettleMonth } = this.Info;
		return (
			<div className="report-search-area">
				<div className="finance-org">
					<span className="txt">{this.state.json['public-000024']}：</span>
					{/* 国际化处理： 财务组织*/}
					<ReferLoader
						tag="test"
						refcode="/uapbd/refer/org/FinanceOrgTreeRef/index.js"
						isShowUnit={false} //是否跨集团
						isShowDisabledData={false} //是否显示停用
						onChange={(v) => {
							pk_org.value = v.refpk;
							pk_org.refpk2 = v.refpk2;
							pk_org.display = v.refname;
							this.getTableData();
						}}
						value={{
							refname: pk_org.display,
							refpk: pk_org.value,
							refpk2: pk_org.refpk2
						}}
						queryCondition={() => {
							//根据集团过滤财务组织
							return {
								DataPowerOperationCode: 'fi', //使用权组
								isDataPowerEnable: 'Y',
								AppCode: this.props.getSearchParam('c'),
								TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
							};
						}}
						placeholder={pk_org.display || this.state.json['public-000024']} /* 国际化处理： 财务组织*/
					/>
				</div>
				<div className="settle-month">
					<span>{this.state.json['public-000037']}：</span>
					{/* 国际化处理： 待结账月份*/}
					<span>{noSettleMonth.display}</span>
				</div>
			</div>
		);
	};

	getTableData = () => {
		if (!this.Info.pk_org.value) {
			this.setState({ nccCloseAccountVOs: [] });
			return;
		}
		ajax({
			url: '/nccloud/arap/closeaccount/querybypkorg.do',
			data: {
				pk_org: this.Info.pk_org.value,
				moudleId: this.props.moudleId,
				prodId: this.props.prodId
			},
			success: (res) => {
				if (res.data.nccCloseAccountVOs) {
					this.Info.noSettleMonth = { display: res.data.noSettleMonth, value: res.data.pk_noSettleMonth };
					this.Info.m_sBegDate = res.data.m_sBegDate;
					this.Info.m_sEndDate = res.data.m_sEndDate;
					this.setState({
						nccCloseAccountVOs: res.data.nccCloseAccountVOs
					});
				}
			}
		});
	};

	//点击结账按钮
	handleEndAccount = () => {
		let { sfbz, moudleId, prodId } = this.props;
		let { pk_org, noSettleMonth } = this.Info;
		if (pk_org.value == '' || pk_org.value == undefined) {
			toast({ content: this.state.json['public-000034'], color: 'warning' }); /* 国际化处理： 请先选择财务组织*/
			return false;
		}
		ajax({
			url: '/nccloud/arap/closeaccount/oncheck.do',
			data: {
				pk_org: pk_org.value,
				moudleId: moudleId,
				prodId: prodId,
				period: noSettleMonth.display,
				sfbz: sfbz
			},
			success: (res) => {
				//m_checkMode1,m_checkMode2,m_checkMode3，m_checkMode4，m_checkMode5
				//以上五个全部为null时，为不检查不控制
				//control 为控制
				//check 为检查但不控制
				//当全部不检查不控制的时候就不弹模态框直接结账，其余都需要弹模态框，凡是有一项控制，且该项控制有数据展示，那么完成结账按钮不可操作
				let data = res.data;
				if (res.data) {
					if (
						data.m_checkMode1 ||
						data.m_checkMode2 ||
						data.m_checkMode3 ||
						data.m_checkMode4 ||
						data.m_checkMode5
					) {
						//弹模态框
						this.getSettleReport(res.data);
					} else {
						//不弹模态框，直接结账
						this.onReckoningOver();
					}
				}
			}
		});
	};

	//结账动作(完成结账)
	onReckoningOver = () => {
		let { moudleId } = this.props;
		let { pk_org, noSettleMonth } = this.Info;
		let datas = {
			pk_org: pk_org.value,
			moudleId: moudleId,
			period: noSettleMonth.display,
			voCond: {
				pk_periodmonth: noSettleMonth.value
			}
		};
		ajax({
			url: '/nccloud/arap/closeaccount/oncloseaccountover.do',
			data: datas,
			success: (res) => {
				toast({ color: 'success', content: this.state.json['public-000035'] }); /* 国际化处理： 结账成功*/
				this.getTableData(); //刷新表格数据
				if (this.state.showReportModal) {
					this.setState({
						//隐藏模态框
						showReportModal: false
					});
				}
			}
		});
	};
	//取消结账
	onCancelReckoning = () => {
		let { moudleId, prodId } = this.props;
		let { pk_org } = this.Info;
		if (pk_org.value == '' || pk_org.value == undefined) {
			toast({ content: this.state.json['public-000034'], color: 'warning' }); /* 国际化处理： 请先选择财务组织*/
			return false;
		}
		ajax({
			url: '/nccloud/arap/closeaccount/oncancelcloseAccount.do',
			data: {
				pk_org: pk_org.value,
				moudleId: moudleId,
				prodId: prodId
			},
			success: (res) => {
				toast({ color: 'success', content: this.state.json['public-000036'] }); /* 国际化处理： 取消结账成功*/
				this.getTableData();
			}
		});
	};

	//获取结账报告信息
	getSettleReport = (sysInfo) => {
		let { noSettleMonth, m_sEndDate, m_sBegDate } = this.Info;
		let datas = {
			sysInfo: sysInfo,
			voCond: {
				m_sPeriodState: 'N', //当月结账状态，肯定为未结账
				pk_periodmonth: noSettleMonth.value,
				m_sBegDate: m_sBegDate,
				m_sEndDate: m_sEndDate
			}
		};
		ajax({
			url: '/nccloud/arap/closeaccount/getcloseaccountreport.do',
			data: datas,
			success: (res) => {
				if (res.data.length == 0) {
					//检查无数据，模态框不展示,直接去结账
					this.onReckoningOver();
				} else {
					//检查有数据，模态框展示
					this.Info.reportTableData = res.data.datalist;
					this.Info.controlSettleButton = res.data.state;
					this.handleOpen();
				}
			}
		});
	};

	//控制模态框显示
	handleOpen = () => {
		this.setState({
			showReportModal: true
		});
	};
	//控制模态框隐藏
	handleClose = () => {
		this.setState({
			showReportModal: false
		});
	};

	render() {
		let { reportTableData, controlSettleButton } = this.Info;
		let { nccCloseAccountVOs, showReportModal } = this.state;
		let { button, title } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon ? createPageIcon() : null}
						<h2 className="title-search-detail">{title}</h2>
					</div>
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{/* //表头 */}
					{this.getHeadData()}
				</div>
				<div className="nc-bill-table-area table-area">
					{/* //表体 */}
					<NCTable
						columns={this.Defaultcolumns}
						bordered
						isDrag
						data={nccCloseAccountVOs}
						scroll={{
							x:
								this.Defaultcolumns.length > 8
									? 100 + (this.Defaultcolumns.length - 8) * 15 + '%'
									: '100%',
							y: 500
						}}
					/>
				</div>
				<ReportModal
					showReportModal={showReportModal}
					reportTableData={reportTableData}
					handleOpen={this.handleOpen.bind(this)}
					handleClose={this.handleClose.bind(this)}
					onReckoningOver={this.onReckoningOver.bind(this)}
					controlSettleButton={controlSettleButton}
					title={title}
				/>
			</div>
		);
	}
}

Closeaccount = createPage({
	initTemplate: initTemplate
})(Closeaccount);

export default function(props = {}) {
	var conf = {};

	return <Closeaccount {...Object.assign(conf, props)} />;
}
