import React, { Component } from 'react';
import { high, base, ajax, getMultiLang } from 'nc-lightapp-front';
const { NCTable, NCModal, NCButtonGroup, NCButton } = base;
let columns_report = [
	{
		title: '' /* 国际化处理： 序号*/,
		dataIndex: 'serial',
		key: 'serial'
	},
	{
		title: '' /* 国际化处理： 月检查不合格单据*/,
		dataIndex: 'notQualified',
		key: 'notQualified'
	},
	{
		title: '' /* 国际化处理： 检查结果*/,
		dataIndex: 'result',
		key: 'result'
	}
];
export default class ReportModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {}
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				columns_report = [
					{
						title: this.state.json['public-000028'] /* 国际化处理： 序号*/,
						dataIndex: 'serial',
						key: 'serial'
					},
					{
						title: this.state.json['public-000029'] /* 国际化处理： 月检查不合格单据*/,
						dataIndex: 'notQualified',
						key: 'notQualified'
					},
					{
						title: this.state.json['public-000030'] /* 国际化处理： 检查结果*/,
						dataIndex: 'result',
						key: 'result'
					}
				];
			});
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	render() {
		let { showReportModal, handleClose, reportTableData, onReckoningOver, controlSettleButton, title } = this.props;
		return (
			<div id="finalTreatment_report">
				<NCModal
					className="msg-modal finalTreatment-modal"
					size="lg"
					show={showReportModal}
					onHide={handleClose}
				>
					<NCModal.Header closeButton>
						<NCModal.Title>{title}</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>
						<NCTable
							columns={columns_report}
							bordered
							isDrag
							data={reportTableData}
							scroll={{
								x: columns_report.length > 8 ? 100 + (columns_report.length - 8) * 15 + '%' : '100%',
								y: 468
							}}
						/>
					</NCModal.Body>
					<NCModal.Footer>
						{controlSettleButton ? (
							<NCButtonGroup>
								<NCButton colors="primary" onClick={onReckoningOver} style={{ padding: '0 10px' }}>
									{this.state.json['public-000033']}
									{/* 国际化处理： 完成结账*/}
								</NCButton>
							</NCButtonGroup>
						) : null}
					</NCModal.Footer>
				</NCModal>
			</div>
		);
	}
}
