import React, { Component } from 'react';
import { ajax, base, createPage, toast, cardCache, getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../../ReferLoader';
import { businessDate } from '../../businessDate';
import getInitOrg from '../../pubUtils/getInitOrg';
import { buttonClick } from '../events';
import './index.less';
let { setDefData, getDefData } = cardCache;
const { NCFormControl, NCDatePicker, NCRow, NCCol, NCSelect, NCNumber } = base;
const NCOption = NCSelect.NCOption;
const { NCRangePicker } = NCDatePicker;
let NCOptionData = [ { value: 0, display: '' }, { value: 1, display: '' }, { value: 2, display: '' } ]; //损益类型/* 国际化处理： 计提,回冲*/
let NCOptionData2 = [ { value: 0, display: '' }, { value: 1, display: '' } ]; //查询方式/* 国际化处理： 汇总查询,明细查询*/
let NCOptionData3 = [
	{ value: 'org', display: '' } /* 国际化处理： 组织本币*/,
	{ value: 'group', display: '' } /* 国际化处理： 集团本币*/,
	{ value: 'global', display: '' } /* 国际化处理： 全局本币*/
]; //本币类型
const format = 'YYYY-MM-DD';
let busiTypeVal, busiTypedis, businessDates;

class Comconditions extends Component {
	constructor(props) {
		super(props);
		this.busiTypeVal = this.props.getUrlParam('Pk_org') ? JSON.parse(this.props.getUrlParam('Pk_org')).value : null;
		this.busiTypedis = this.props.getUrlParam('Pk_org')
			? JSON.parse(this.props.getUrlParam('Pk_org')).display
			: null;
		this.refpk2 = this.props.getUrlParam('Pk_org') ? JSON.parse(this.props.getUrlParam('Pk_org')).refpk2 : null;
		this.state = {
			json: {},
			schemeData: {
				busiType: {
					display: this.busiTypedis,
					value: this.busiTypeVal,
					refpk2: this.refpk2
				}, //财务组织
				m_sKsbm: {
					display: '',
					value: null
				}, //客户/供应商
				agiotageType: {
					display: '',
					value: null
				}, //损益类型
				queryType: {
					display: '' /* 国际化处理： 汇总查询*/,
					value: 0
				}, //查询方式
				m_sDjlx: {
					display: '',
					value: null
				}, //交易类型
				m_sCurrency: {
					display: '',
					value: null
				}, //币种
				m_sDjbhBeg: {
					display: '',
					value: null
				}, //单据编号开始
				m_sDjbhEnd: {
					display: '',
					value: null
				}, //单据编号结束
				m_sDateBeg: {
					display: '',
					value: null
				}, //计算日期开始
				m_sDateEnd: {
					display: '',
					value: null
				}, //计算日期结束
				m_sMaxYbje: {
					display: '',
					value: null
				}, //最大本币差额
				m_sMinYbje: {
					display: '',
					value: null
				}, //最小本币差额
				currType: {
					display: '' /* 国际化处理： 组织本币*/,
					value: 'org'
				}, //本币类型
				value: [] //暂存 计算日期开始结束
			}
		};
	}
	componentWillMount() {}
	componentDidMount() {
		this.props.onRef(this);
		let appcode = this.props.getSearchParam('c');
		if (!this.busiTypeVal) {
			getInitOrg(appcode, this.cb);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.state.json == nextProps.json) return;
		let { schemeData } = this.state;
		this.setState(
			{
				json: nextProps.json
			},
			() => {
				NCOptionData = [
					{ value: null, display: '' },
					{ value: 1, display: this.state.json['public-000134'] },
					{ value: 2, display: this.state.json['public-000135'] }
				]; //损益类型/* 国际化处理： 计提,回冲*/
				NCOptionData2 = [
					{ value: 0, display: this.state.json['public-000125'] },
					{ value: 1, display: this.state.json['public-000136'] }
				]; //查询方式/* 国际化处理： 汇总查询,明细查询*/
				NCOptionData3 = [
					{ value: 'org', display: this.state.json['public-000104'] } /* 国际化处理： 组织本币*/,
					{ value: 'group', display: this.state.json['public-000097'] } /* 国际化处理： 集团本币*/,
					{ value: 'global', display: this.state.json['public-000102'] } /* 国际化处理： 全局本币*/
				]; //本币类型
				schemeData.queryType = {
					display: this.state.json['public-000125'] /* 国际化处理： 汇总查询*/,
					value: 0
				}; //查询方式
				schemeData.currType = {
					display: this.state.json['public-000104'] /* 国际化处理： 组织本币*/,
					value: 'org'
				}; //本币类型
				setTimeout(() => {
					businessDates = businessDate();
					let value = [ businessDates, businessDates ];
					schemeData.value = value; //暂存 计算日期开始结束
					this.setState({
						schemeData
					});
				}, 0);
			}
		);
	}

	cb = (pkOrg) => {
		let { schemeData } = this.state;
		if (!this.props.isBack) {
			schemeData.busiType = {
				display: pkOrg.display,
				value: pkOrg.value,
				refpk2: pkOrg.refpk2
			};
			this.setState({
				schemeData
			});
		}
	};

	//input展示
	getInput = (name, key) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCFormControl value={key.value} placeholder={name} disabled={'disabled'} />
				</div>
			</div>
		);
	};

	//损益类型
	getInputA = (name, key) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCFormControl value={key.display} placeholder={name} disabled={'disabled'} />
				</div>
			</div>
		);
	};

	//input展示
	getInputTow = (name, key1, key2) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCFormControl
						value={key1.value}
						placeholder={name}
						disabled={'disabled'}
						style={{ 'text-align': 'left' }}
					/>
					<span>-</span>
					<NCFormControl
						value={key2.value}
						disabled={'disabled'}
						style={{ 'text-align': 'left' }}
						placeholder={name}
					/>
				</div>
			</div>
		);
	};

	//input 展示display
	getInputD = (name, key) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCFormControl value={key.display} placeholder={name} disabled={'disabled'} />
				</div>
			</div>
		);
	};

	// input 展示 计算日期
	getInputDate = (name, key) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<NCFormControl
						value={key.length > 0 ? key[0] + '~' + key[1] : ''}
						placeholder={name}
						disabled={'disabled'}
					/>
				</div>
			</div>
		);
	};

	//可编辑的input框
	getEditInput = (name, key1, key2, a) => {
		let { schemeData } = this.state;
		if (a == 0) {
			return (
				<div className="finance">
					<div className="finance-refer">
						<NCFormControl
							value={key1.display}
							placeholder={name}
							onChange={(v) => {
								key1.display = v;
								key1.value = v;
								this.setState({
									schemeData
								});
								this.props.getQueryCons(schemeData);
							}}
						/>
						<span>-</span>
						<NCFormControl
							value={key2.display}
							placeholder={name}
							onChange={(v) => {
								key2.display = v;
								key2.value = v;
								this.setState({
									schemeData
								});
								this.props.getQueryCons(schemeData);
							}}
						/>
					</div>
				</div>
			);
		} else {
			return (
				<div className="finance">
					<div className="finance-refer">
						<NCNumber
							style={{ 'text-align': 'left' }}
							value={key1.display}
							placeholder={name}
							onChange={(v) => {
								key1.display = v;
								key1.value = v;
								this.setState({
									schemeData
								});
								this.props.getQueryCons(schemeData);
							}}
						/>
						<span>-</span>
						<NCNumber
							style={{ 'text-align': 'left' }}
							value={key2.display}
							placeholder={name}
							onChange={(v) => {
								key2.display = v;
								key2.value = v;
								this.setState({
									schemeData
								});
								this.props.getQueryCons(schemeData);
							}}
						/>
					</div>
				</div>
			);
		}
	};

	//参照展示
	getRefer = (tag, refcode, key, mainData, falg) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<Referloader
						tag={tag}
						showStar={falg}
						refcode={refcode}
						unitValueIsNeeded ={false}//参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
						value={{
							refname: key.display,
							refpk: key.value,
							refpk2: key.refpk2
						}}
						onChange={(v) => {
							key.value = v.refpk;
							key.display = v.refname;
							key.refpk2 = v.refpk2;
							this.setState({
								mainData
							});
							this.props.getQueryCons(mainData);
						}}
						queryCondition={() => {
							if (tag == 'FinanceOrgTreeRef') {
								return {
									AppCode: this.props.getSearchParam('c'),
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder',
								};
							} else if (tag == 'CustomerDefaultTreeGridRef') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									pk_org: this.state.schemeData.busiType.value
								};
							} else if (tag == 'SupplierRefTreeGridRef') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									pk_org: this.state.schemeData.busiType.value
								};
							} else if (tag == 'CurrtypeGridRef') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y'
								};
							} else if (tag == 'Transtype') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									parentbilltype: this.props.sfbz == 'Ys' ? 'F0,F2' : 'F1,F3'
								};
							}
						}}
					/>
				</div>
			</div>
		);
	};

	//初始条件 浏览态
	getInitialShow = (schemeData, createButtonApp) => {
		let {
			busiType,
			m_sKsbm,
			agiotageType,
			queryType,
			m_sDjlx,
			m_sCurrency,
			m_sDjbhBeg,
			m_sDjbhEnd,
			m_sMaxYbje,
			m_sMinYbje,
			currType,
			value
		} = schemeData;
		return (
			<div className="querycons-style">
				<div className="contions-box">
					{this.getRefer(
						'FinanceOrgTreeRef',
						'uapbd/refer/org/FinanceOrgTreeRef/index.js',
						busiType,
						schemeData,
						true
					)}
				</div>
				<div className="contions-box">
					{this.getInput(
						this.props.isCustomer ? this.state.json['public-000064'] : this.state.json['public-000063'],
						m_sKsbm
					)}
				</div>
				{/* 国际化处理： 客户,供应商*/}
				<div className="contions-box">{this.getInputA(this.state.json['public-000137'], agiotageType)}</div>
				{/* 国际化处理： 损益类型*/}
				<div className="contions-box">{this.getInputD(this.state.json['public-000138'], queryType)}</div>
				{/* 国际化处理： 查询方式*/}
				<div className="contions-box">{this.getInput(this.state.json['public-000139'], m_sDjlx)}</div>
				{/* 国际化处理： 交易类型*/}
				<div className="contions-box">
					<div className="list-body-btn">
						{createButtonApp({
							area: 'list_body',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="contions-box">{this.getInput(this.state.json['public-000081'], m_sCurrency, true)}</div>
				{/* 国际化处理： 币种*/}
				<div className="contions-box">
					{this.getInputTow(this.state.json['public-000060'], m_sDjbhBeg, m_sDjbhEnd)}
				</div>
				{/* 国际化处理： 单据编号*/}
				<div className="contions-box">{this.getInputDate(this.state.json['public-000105'], value)}</div>
				{/* 国际化处理： 计算日期*/}
				<div className="contions-box">
					{this.getInputTow(this.state.json['public-000140'], m_sMinYbje, m_sMaxYbje)}
				</div>
				{/* 国际化处理： 本币差额*/}
				<div className="contions-box">{this.getInputD(this.state.json['public-000141'], currType)}</div>
				{/* 国际化处理： 本币类型*/}
				<div className="contions-box" />
			</div>
		);
	};

	//财务组件参照选完值后展示界面
	getReferShow = (schemeData, NCOptions, NCOptions2, NCOptions3, createButtonApp) => {
		let {
			busiType,
			m_sKsbm,
			agiotageType,
			queryType,
			m_sDjlx,
			m_sCurrency,
			m_sDjbhBeg,
			m_sDjbhEnd,
			m_sMaxYbje,
			m_sMinYbje,
			currType
		} = schemeData;
		return (
			<div className="querycons-style">
				<div className="contions-box">
					{this.getRefer(
						'FinanceOrgTreeRef',
						'uapbd/refer/org/FinanceOrgTreeRef/index.js',
						busiType,
						schemeData,
						true
					)}
				</div>
				<div className="contions-box">
					{this.props.isCustomer ? (
						<div>
							{this.getRefer(
								'CustomerDefaultTreeGridRef',
								'uapbd/refer/customer/CustomerDefaultTreeGridRef/index.js',
								m_sKsbm,
								schemeData
							)}
						</div>
					) : (
						<div>
							{this.getRefer(
								'SupplierRefTreeGridRef',
								'uapbd/refer/supplier/SupplierRefTreeGridRef/index.js',
								m_sKsbm,
								schemeData
							)}
						</div>
					)}
				</div>
				<div className="contions-box">
					<NCSelect
						placeholder={this.state.json['public-000137']}
						value={agiotageType.display}
						onChange={(v) => {
							agiotageType.value = v.value;
							agiotageType.display = v.display;
							this.setState({
								schemeData
							});
							this.props.getQueryCons(schemeData);
						}}
					>
						{NCOptions}
					</NCSelect>
				</div>
				<div className="contions-box">
					<NCSelect
						value={queryType.display}
						onChange={(v) => {
							queryType.value = v.value;
							queryType.display = v.display;
							this.setState(
								{
									schemeData
								},
								() => {
									this.props.setButtonDisabled('CancelProfit', true);
									if (queryType.value == 0) {
										this.props.setButtonVisible([ 'ExportData' ], false);
									} else {
										this.props.setButtonVisible([ 'ExportData' ], true);
									}
									this.props.getQueryCons(schemeData);
								}
							);
						}}
					>
						{NCOptions2}
					</NCSelect>
				</div>
				<div className="contions-box">
					{this.getRefer('Transtype', 'uap/refer/riart/transtype/index.js', m_sDjlx, schemeData)}
				</div>
				<div className="contions-box">
					<div className="list-body-btn">
						{createButtonApp({
							area: 'list_body',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="contions-box">
					{this.getRefer(
						'CurrtypeGridRef',
						'uapbd/refer/pubinfo/CurrtypeGridRef/index.js',
						m_sCurrency,
						schemeData
					)}
				</div>
				<div className="contions-box">
					{this.getEditInput(this.state.json['public-000060'], m_sDjbhBeg, m_sDjbhEnd, 0)}
				</div>
				{/* 国际化处理： 单据编号*/}
				<div className="contions-box">
					<NCRangePicker
						format={format}
						showStar={true}
						onChange={(d) => {
							schemeData.value = d;
							this.setState({
								schemeData
							});
						}}
						showClear={true}
						showOk={true}
						className={'range-fixed'}
						value={this.state.schemeData.value}
						placeholder={this.state.json['public-000142']} /* 国际化处理： 开始 ~ 结束*/
						dateInputPlaceholder={[
							this.state.json['public-000143'],
							this.state.json['public-000144']
						]} /* 国际化处理： 开始,结束*/
					/>
				</div>
				<div className="contions-box">
					{this.getEditInput(this.state.json['public-000140'], m_sMinYbje, m_sMaxYbje, 1)}
				</div>
				{/* 国际化处理： 本币差额*/}
				<div className="contions-box">
					<div className="finance-outer">
						<NCSelect
							value={currType.display}
							onChange={(v) => {
								currType.value = v.value;
								currType.display = v.display;
								this.setState({
									schemeData
								});
								this.props.getQueryCons(schemeData);
							}}
						>
							{NCOptions3}
						</NCSelect>
					</div>
				</div>
				<div className="contions-box" />
			</div>
		);
	};

	//查询按钮
	handleQueryBtn = (flag) => {
		let { schemeData } = this.state;
		let { queryType, busiType, value } = schemeData;
		let queryTypeVal = queryType.value;
		if (busiType.value == null) {
			toast({
				duration: 3,
				color: 'warning',
				title: this.state.json['public-000145'] /* 国际化处理： 请注意*/,
				content: this.state.json['public-000073'] /* 国际化处理： 请选择财务组织！*/
			});
			return;
		} else if (value.length == 0) {
			toast({
				duration: 3,
				color: 'warning',
				title: this.state.json['public-000145'] /* 国际化处理： 请注意*/,
				content: this.state.json['public-000146'] /* 国际化处理： 请选择计算日期！*/
			});
			return;
		} else {
			this.getTableDatas(queryTypeVal, busiType.value, schemeData, flag);
		}
	};

	//获取表格数据
	getTableDatas = (queryTypeVal, busiTypeVal, schemeData, flag) => {
		let agiotageVO = {
			busiType: busiTypeVal,
			customers: schemeData.m_sKsbm.value == null ? null : [ schemeData.m_sKsbm.value ],
			agiotageType: schemeData.agiotageType.value,
			queryType: schemeData.queryType.value,
			m_sDjlx: schemeData.m_sDjlx.value,
			m_sCurrency: schemeData.m_sCurrency.value,
			m_sDjbhBeg: schemeData.m_sDjbhBeg.value == '' ? null : schemeData.m_sDjbhBeg.value,
			m_sDjbhEnd: schemeData.m_sDjbhEnd.value == '' ? null : schemeData.m_sDjbhEnd.value,
			m_sDateBeg: schemeData.value[0] + ' 00:00:00',
			m_sDateEnd: schemeData.value[1] + ' 23:59:59',
			m_sMinje: schemeData.m_sMinYbje.value == '' ? null : schemeData.m_sMinYbje.value,
			m_sMaxje: schemeData.m_sMaxYbje.value == '' ? null : schemeData.m_sMaxYbje.value,
			currType: schemeData.currType.value,
			iszgAgiotage: false,
			m_HsMode: false,
			m_sDwbm: schemeData.busiType.value
		};
		let hisrecorddata = {
			sfbz: this.props.sfbz,
			pk_org: busiTypeVal,
			agiotageVO: agiotageVO
		};
		//点击查询缓存查询条件
		setDefData('cal-query', this.props.appcode, JSON.parse(JSON.stringify(hisrecorddata)));
		this.getTableDatasAjax(queryTypeVal, hisrecorddata, schemeData, flag);
	};

	//查询获取表格数据ajax事件
	getTableDatasAjax = (queryTypeVal, hisrecorddata, schemeData, flag) => {
		if (queryTypeVal == 1) {
			let pageSize = getDefData('detail-pageSize', this.props.appcode);
			pageSize = pageSize ? pageSize : 10;
			if (pageSize) {
				hisrecorddata.agiotageVO.pageSize = Number(pageSize);
			}
		}
		ajax({
			url: '/nccloud/arap/agiotage/onhisrecord.do',
			data: hisrecorddata,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (queryTypeVal == 1) {
						this.props.getDetailQueryData(data, flag, 1); //明细
					} else {
						this.props.getSummaryQueryData(data, schemeData, flag); //汇总
					}
				}
			}
		});
	};

	//刷新按钮  查询数据
	handleRefreshGetData = (flag) => {
		let { schemeData } = this.state;
		let { queryType } = schemeData;
		let queryTypeVal = queryType.value;
		let queryConsDatas = getDefData('cal-query', this.props.appcode);
		queryConsDatas.agiotageVO.queryType = queryTypeVal;
		if (queryConsDatas) {
			this.getTableDatasAjax(queryTypeVal, queryConsDatas, schemeData, flag);
		}
	};

	//清空按钮
	handleEmpty = () => {
		let { schemeData } = this.state;
		let schemeDataE = {
			busiType: {
				display: '',
				value: null
			}, //财务组织
			m_sKsbm: {
				display: '',
				value: null
			}, //客户/供应商
			agiotageType: {
				display: '',
				value: null
			}, //损益类型
			queryType: {
				display: schemeData.queryType.display,
				value: schemeData.queryType.value
			}, //查询方式
			m_sDjlx: {
				display: '',
				value: null
			}, //交易类型
			m_sCurrency: {
				display: '',
				value: null
			}, //币种
			m_sDjbhBeg: {
				display: '',
				value: null
			}, //单据编号开始
			m_sDjbhEnd: {
				display: '',
				value: null
			}, //单据编号结束
			m_sDateBeg: {
				display: '',
				value: null
			}, //计算日期开始
			m_sDateEnd: {
				display: '',
				value: null
			}, //计算日期结束
			m_sMaxYbje: {
				display: '',
				value: null
			}, //最大本币差额
			m_sMinYbje: {
				display: '',
				value: null
			}, //最小本币差额
			currType: {
				display: schemeData.currType.display,
				value: schemeData.currType.value
			}, //本币类型
			value: [] //暂存 计算日期开始结束
		};
		this.setState(
			{
				schemeData: schemeDataE
			},
			() => {
				this.props.getQueryCons(schemeData);
			}
		);
	};

	//改变明细表格的查询方式
	changeQueryType = () => {
		let { schemeData } = this.state;
		let { queryType } = schemeData;
		this.state.schemeData.queryType.display = this.state.json['public-000136']; /* 国际化处理： 明细查询*/
		this.state.schemeData.queryType.value = 1;
		this.setState({
			queryType
		});
	};

	render() {
		let { schemeData } = this.state;
		let { busiType } = schemeData;
		let { createButtonApp } = this.props;
		const NCOptions = NCOptionData.map((province) => <NCOption value={province}>{province.display}</NCOption>);
		const NCOptions2 = NCOptionData2.map((province) => <NCOption value={province}>{province.display}</NCOption>);
		const NCOptions3 = NCOptionData3.map((province) => <NCOption value={province}>{province.display}</NCOption>);
		return (
			<div id="queryconditionId">
				<div className="querycondition-style">
					{busiType.display == '' || busiType.display == undefined ? (
						<div>{this.getInitialShow(schemeData, createButtonApp)}</div>
					) : (
						<div>{this.getReferShow(schemeData, NCOptions, NCOptions2, NCOptions3, createButtonApp)}</div>
					)}
				</div>
			</div>
		);
	}
}

Comconditions = createPage({})(Comconditions);

export default Comconditions;
