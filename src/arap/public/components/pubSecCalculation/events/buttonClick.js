import { headButton, bodyButton } from '../../pubUtils/buttonName';

export default function(props, id) {
	switch (id) {
		case headButton.CancelProfit: //取消损益
			this.handleCancelprofit();
			break;
		case bodyButton.Query: //查询
			this.handleQueryBtn();
			break;
		case headButton.Print: //打印
			this.handlePrint();
			break;
		case headButton.ExportData: //导出
			this.handleExport();
			break;
		case headButton.Refresh: //刷新
			this.handleRefresh();
			break;
		case headButton.Empty: //清空
			this.handleEmpty();
			break;
	}
}
