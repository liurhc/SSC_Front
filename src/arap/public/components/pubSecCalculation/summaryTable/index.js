import React, { Component } from 'react';
import { ajax, base, getMultiLang, cardCache } from 'nc-lightapp-front';
const { NCTable, NCCheckbox, NCTooltip } = base;
let { setDefData, getDefData } = cardCache;
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
let linkrecorddata;

class FirstPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {},
			tableData: [], //表格数据
			columns: []
		};
	}

	componentDidMount() {
		this.props.onRef(this);
		let callback = (json) => {
			this.setState({ json: json }, () => {
				this.setState({
					columns: [
						{
							title: this.state.json['public-000108'] /* 国际化处理： 选择标志*/,
							dataIndex: 'xzbz',
							key: 'xzbz',
							width: 100,
							fixed: 'left',
							render: (record, index) => (
								<NCCheckbox
									checked={record.xzbz}
									onChange={this.props.handleSignSelectSum(index, 'xzbz')}
								/>
							)
						},
						{
							title: this.state.json['public-000081'] /* 国际化处理： 币种*/,
							dataIndex: 'bzmc',
							key: 'bzmc',
							width: 100
						},
						{
							title: this.props.isCustomer
								? this.state.json['public-000064']
								: this.state.json['public-000063'] /* 国际化处理： 客户,供应商*/,
							dataIndex: this.props.isCustomer ? 'khbm' : 'gysbm',
							key: this.props.isCustomer ? 'khbm' : 'gysbm',
							width: 100
						},
						{
							title: this.state.json['public-000081'] /* 国际化处理： 币种*/,
							dataIndex: 'bzbm',
							key: 'bzbm',
							width: 100
						},
						{
							title: this.state.json['public-000103'] /* 国际化处理： 原币余额*/,
							dataIndex: 'ybye',
							key: 'ybye',
							width: 100
						},
						{
							title: this.state.json['public-000110'] /* 国际化处理： 组织本币余额*/,
							dataIndex: 'zzbbye',
							key: 'zzbbye',
							width: 100
						},
						{
							title: this.state.json['public-000111'] /* 国际化处理： 集团本币余额*/,
							dataIndex: 'jtbbye',
							key: 'jtbbye',
							width: 100
						},
						{
							title: this.state.json['public-000112'] /* 国际化处理： 全局本币余额*/,
							dataIndex: 'qjbbye',
							key: 'qjbbye',
							width: 100
						},
						{
							title: this.state.json['public-000113'] /* 国际化处理： 调整后原币余额*/,
							dataIndex: 'tybye',
							key: 'tybye',
							width: 130
						},
						{
							title: this.state.json['public-000115'] /* 国际化处理： 调整后组织本币余额*/,
							dataIndex: 'tzzbbye',
							key: 'tzzbbye',
							width: 140
						},
						{
							title: this.state.json['public-000116'] /* 国际化处理： 调整后集团本币余额*/,
							dataIndex: 'tjtbbye',
							key: 'tjtbbye',
							width: 140
						},
						{
							title: this.state.json['public-000117'] /* 国际化处理： 调整后全局本币余额*/,
							dataIndex: 'tqjbbye',
							key: 'tqjbbye',
							width: 140
						},
						{
							title: this.state.json['public-000118'] /* 国际化处理： 组织本币差额*/,
							dataIndex: 'zzbbce',
							key: 'zzbbce',
							width: 100
						},
						{
							title: this.state.json['public-000120'] /* 国际化处理： 集团本币差额*/,
							dataIndex: 'jtbbce',
							key: 'jtbbce',
							width: 100
						},
						{
							title: this.state.json['public-000122'] /* 国际化处理： 全局本币差额*/,
							dataIndex: 'qubbce',
							key: 'qubbce',
							width: 100
						}
					]
				});
				let tableDatas = this.props.tableDatas;
				if (tableDatas.title) {
					let showData = tableDatas.showData;
					this.handleTable(showData, tableDatas);
				}
			});
		};
		setTimeout(() => {
			getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
		}, 400);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.tableDatas.title != null) {
			let tableData = nextProps.tableDatas;
			let showData = tableData.showData;
			this.handleTable(showData, tableData);
		}
	}

	//改变表格标题及表格数据
	handleTable = (showData, tableData) => {
		let that = this;
		showData.map((item, i) => {
			item.key = i;
		});
		let titClone = JSON.parse(JSON.stringify(tableData.title));
		delete titClone.xzbz;
		let newTit = [
			{
				title: this.state.json['public-000108'] /* 国际化处理： 选择标志*/,
				dataIndex: 'xzbz',
				key: 'xzbz',
				fixed: 'left',
				width: 100,
				render: (text, record, index) => (
					<NCCheckbox checked={record.xzbz} onChange={this.props.handleSignSelectSum(index, 'xzbz')} />
				)
			}
		];
		let totit = [
			'bzmc',
			'khbm',
			'gysbm',
			'bzbm',
			'ybye',
			'zzbbye',
			'jtbbye',
			'qjbbye',
			'tybye',
			'tzzbbye',
			'tjtbbye',
			'tqjbbye',
			'zzbbce',
			'jtbbce',
			'qubbce'
		];
		let totalTit = [
				{
					bzmc: this.state.json['public-000081'] /* 国际化处理： 币种*/
				},
				{
					khbm: this.state.json['public-000064'] /* 国际化处理： 客户*/
				},
				{
					gysbm: this.state.json['public-000063'] /* 国际化处理： 供应商*/
				},
				{
					bzbm: this.state.json['public-000081'] /* 国际化处理： 币种*/
				},
				{
					ybye: this.state.json['public-000103'] /* 国际化处理： 原币余额*/
				},
				{
					zzbbye: this.state.json['public-000110'] /* 国际化处理： 组织本币余额*/
				},
				{
					jtbbye: this.state.json['public-000111'] /* 国际化处理： 集团本币余额*/
				},
				{
					qjbbye: this.state.json['public-000112'] /* 国际化处理： 全局本币余额*/
				},
				{
					tybye: this.state.json['public-000113'] /* 国际化处理： 调整后原币余额*/
				},
				{
					tzzbbye: this.state.json['public-000115'] /* 国际化处理： 调整后组织本币余额*/
				},
				{
					tjtbbye: this.state.json['public-000116'] /* 国际化处理： 调整后集团本币余额*/
				},
				{
					tqjbbye: this.state.json['public-000117'] /* 国际化处理： 调整后全局本币余额*/
				},
				{
					zzbbce: this.state.json['public-000118'] /* 国际化处理： 组织本币差额*/
				},
				{
					jtbbce: this.state.json['public-000120'] /* 国际化处理： 集团本币差额*/
				},
				{
					qubbce: this.state.json['public-000122'] /* 国际化处理： 全局本币差额*/
				}
			],
			width;
		for (var i = 0, len = totalTit.length; i < len; i++) {
			if (titClone.hasOwnProperty(totit[i])) {
				switch (totit[i]) {
					case 'tybye':
					case 'tzzbbye':
					case 'tjtbbye':
					case 'tqjbbye':
						width = 150;
						break;
					default:
						width = 100;
				}
				newTit.push({
					title: totalTit[i][totit[i]],
					dataIndex: totit[i],
					key: totit[i],
					width: width,
					render: (text, record, index) => {
						let tip = <div>{text}</div>;
						return (
							<div className="scomment">
								<NCTooltip inverse={true} overlay={tip} trigger="hover" placement="top">
									<span>{text}</span>
								</NCTooltip>
							</div>
						);
					}
				});
			}
		}
		let operationTit = {
			title: this.state.json['public-000044'] /* 国际化处理： 操作*/,
			dataIndex: 'title',
			key: 'title',
			fixed: 'right',
			width: 100,
			render(text, record) {
				return (
					<div style={{ position: 'relative' }} title={text}>
						<a
							href="javascript:void(0)"
							tooltip={text}
							onClick={() => {
								that.props.setButtonDisabled('CancelProfit', true);
								that.props.setSumJumpDetStatus();
								that.getDetailDatas(record);
							}}
							style={{
								position: 'absolute',
								top: -8,
								left: 0
							}}
						>
							{record.title}
						</a>
					</div>
				);
			}
		};
		newTit.push(operationTit);
		this.setState({
			columns: newTit,
			tableData: showData
		});
	};

	//明细按钮操作
	getDetailDatas = (record) => {
		let sumQuCons = this.props.sumQueryCons;
		let agiotageVO = {
			busiType: sumQuCons.busiType.value, //财务组织
			m_sKsbm: sumQuCons.m_sKsbm.value, //客户
			agiotageType: sumQuCons.agiotageType.value, //损益类型
			queryType: sumQuCons.queryType.value, //查询方式
			m_sDjlx: sumQuCons.m_sDjlx.value, //交易类型
			m_sCurrency: sumQuCons.m_sCurrency.value, //币种
			m_sDjbhBeg: sumQuCons.m_sDjbhBeg.value, //单据编号开始
			m_sDjbhEnd: sumQuCons.m_sDjbhEnd.value, //单据编号结束
			m_sDateBeg: sumQuCons.m_sDateBeg.value, //计算日期开始
			m_sDateEnd: sumQuCons.m_sDateEnd.value, //计算日期结束
			m_sMaxYbje: sumQuCons.m_sMaxYbje.value, //最大本币差额
			m_sMinYbje: sumQuCons.m_sMinYbje.value, //最小本币差额
			currType: sumQuCons.currType.value, //本币类型
			m_sDwbm: sumQuCons.busiType.value, //财务组织
			iszgAgiotage: false,
			m_HsMode: false,
			m_sSfbz: this.props.sfbz,
			pageSize: 10 //默认传递10条数据
		};
		let linkVO = [
			{
				pks: record.pks,
				agdpk: record.agdpk
			}
		];
		linkrecorddata = {
			linkrecordInfo: linkVO,
			agiotageVO: agiotageVO
		};
		//汇总跳转明细 缓存明细的查询条件
		setDefData('cal-jump-query', this.props.appcode, JSON.parse(JSON.stringify(linkrecorddata)));
		ajax({
			url: '/nccloud/arap/agiotage/onRecordLinkDetail.do',
			data: linkrecorddata,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					this.props.changeDetailData(data);
				}
			}
		});
	};

	//改变表格数据
	handleChangeTableData = (data) => {
		this.setState({
			tableData: data
		});
	};

	render() {
		let { columns, tableData } = this.state;
		return (
			<div>
				<NCTable
					columns={columns}
					bordered
					isDrag
					data={tableData}
					style={{ marginRight: '20px' }}
					scroll={{ y: 560 }}
					loading={false}
				/>
			</div>
		);
	}
}

FirstPage.defaultProps = defaultProps12;

export default FirstPage;
