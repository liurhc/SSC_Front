import React, { Component } from 'react';
import { base, ajax, createPage, getMultiLang, cardCache } from 'nc-lightapp-front';
import './index.less';
const { NCTable, NCCheckbox, NCTooltip, NCPagination, NCSelect } = base;
let { setDefData, getDefData } = cardCache;
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
const Option = NCSelect.NCOption;
let showData = [],
	appcode;

class Secondpage extends Component {
	constructor(props) {
		super(props);
		this.pksData = []; //获取查询数据的所有pk集合
		this.selectedClbh = ''; //明细被选中pk集合
		this.state = {
			activePage: 1,
			pageSize: 10,
			total: '', //总条数
			totalPage: '', //总页数
			tableData: [], //表格数据
			json: {},
			columns: [], //表格表头
			isshow: false,
			mainData: {
				date: {
					display: '',
					value: ''
				}, //计算日期
				pk_org: {
					display: '',
					value: ''
				} //财务组织
			}
		};
	}

	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				this.setState({
					columns: [
						{
							title: this.state.json['public-000108'] /* 国际化处理： 选择标志*/,
							dataIndex: 'xzbz',
							key: 'xzbz',
							fixed: 'left',
							width: 100,
							render: (text, record, index) => (
								<NCCheckbox
									checked={record.xzbz}
									onChange={this.props.handleSignSelectDet(index, 'xzbz')}
								/>
							)
						},
						{
							title: this.state.json['public-000105'] /* 国际化处理： 计算日期*/,
							dataIndex: 'jsrq',
							key: 'jsrq',
							width: 100
						},
						{
							title: this.state.json['public-000081'] /* 国际化处理： 币种*/,
							dataIndex: 'bzmc',
							key: 'bzmc',
							width: 100
						},
						{
							title: this.state.json['public-000059'] /* 国际化处理： 单据类型*/,
							dataIndex: 'djlx',
							key: 'djlx',
							width: 100
						},
						{
							title: this.state.json['public-000060'] /* 国际化处理： 单据编号*/,
							dataIndex: 'billno',
							key: 'billno',
							width: 100
						},
						{
							title: this.state.json['public-000103'] /* 国际化处理： 原币余额*/,
							dataIndex: 'ybye',
							key: 'ybye',
							width: 100
						},
						{
							title: this.state.json['public-000109'] /* 国际化处理： 调整前汇率*/,
							dataIndex: 'hl',
							key: 'hl',
							width: 100
						},
						{
							title: this.state.json['public-000110'] /* 国际化处理： 组织本币余额*/,
							dataIndex: 'zzbbye',
							key: 'zzbbye',
							width: 100
						},
						{
							title: this.state.json['public-000111'] /* 国际化处理： 集团本币余额*/,
							dataIndex: 'jtbbye',
							key: 'jtbbye',
							width: 100
						},
						{
							title: this.state.json['public-000112'] /* 国际化处理： 全局本币余额*/,
							dataIndex: 'qjbbye',
							key: 'qjbbye',
							width: 100
						},
						{
							title: this.state.json['public-000113'] /* 国际化处理： 调整后原币余额*/,
							dataIndex: 'tybye',
							key: 'tybye',
							width: 100
						},
						{
							title: this.state.json['public-000114'] /* 国际化处理： 调整后汇率*/,
							dataIndex: 'thl',
							key: 'thl',
							width: 100
						},
						{
							title: this.state.json['public-000115'] /* 国际化处理： 调整后组织本币余额*/,
							dataIndex: 'tzzbbye',
							key: 'tzzbbye',
							width: 100
						},
						{
							title: this.state.json['public-000116'] /* 国际化处理： 调整后集团本币余额*/,
							dataIndex: 'tjtbbye',
							key: 'tjtbbye',
							width: 100
						},
						{
							title: this.state.json['public-000117'] /* 国际化处理： 调整后全局本币余额*/,
							dataIndex: 'tqjbbye',
							key: 'tqjbbye',
							width: 100
						},
						{
							title: this.state.json['public-000118'] /* 国际化处理： 组织本币差额*/,
							dataIndex: 'zzbbce',
							key: 'zzbbce',
							width: 100
						},
						{
							title: this.state.json['public-000119'] /* 国际化处理： 组织损益*/,
							dataIndex: 'zzsy',
							key: 'zzsy',
							width: 100
						},
						{
							title: this.state.json['public-000120'] /* 国际化处理： 集团本币差额*/,
							dataIndex: 'jtbbce',
							key: 'jtbbce',
							width: 100
						},
						{
							title: this.state.json['public-000121'] /* 国际化处理： 集团损益*/,
							dataIndex: 'jtsy',
							key: 'jtsy',
							width: 100
						},
						{
							title: this.state.json['public-000122'] /* 国际化处理： 全局本币差额*/,
							dataIndex: 'qjbbce',
							key: 'qjbbce',
							width: 100
						},
						{
							title: this.state.json['public-000123'] /* 国际化处理： 全局损益*/,
							dataIndex: 'qjsy',
							key: 'qjsy',
							width: 100
						},
						{
							title: this.state.json['public-000124'] /* 国际化处理： 处理人*/,
							dataIndex: 'clr',
							key: 'clr',
							width: 100
						}
					] //表格表头
				});
				let tableData = this.props.tableData;
				if (tableData.pksData) {
					this.pksData = tableData.pksData;
				}
				if (tableData.title) {
					let showData = tableData.showData;
					this.handleTable(showData, tableData);
				}
			});
		};
		setTimeout(() => {
			getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
		}, 500);
		this.props.onRef(this);
		appcode = this.props.appcode;
	}

	componentWillReceiveProps(nextprops) {
		if (nextprops.tableData.title != null) {
			let tableData = nextprops.tableData;
			if (tableData.pksData) {
				this.pksData = tableData.pksData;
			}
			if (tableData.activePage) {
				this.setState({
					activePage: tableData.activePage
				});
			}
			showData = tableData.showData;
			this.handleTable(showData, tableData);
		}
	}

	handleTable = (showData, tableData) => {
		let { linkagiotage } = this.props;
		showData.map((item, i) => {
			item.key = i;
		});
		let data = this.handlePaginationData(showData, this.state.pageSize, 1);
		let titClone = JSON.parse(JSON.stringify(tableData.title));
		delete titClone.xzbz;
		let newTit = [
			{
				title: this.state.json['public-000108'] /* 国际化处理： 选择标志*/,
				dataIndex: 'xzbz',
				key: 'xzbz',
				fixed: 'left',
				width: 100,
				render: (text, record, index) => (
					<NCCheckbox checked={record.xzbz} onChange={this.props.handleSignSelectDet(index, 'xzbz')} />
				)
			}
		];
		let totit = [
			'jsrq',
			'bzmc',
			'djlx',
			'billno',
			'ybye',
			'hl',
			'zzbbye',
			'jtbbye',
			'qjbbye',
			'tybye',
			'thl',
			'tzzbbye',
			'tjtbbye',
			'tqjbbye',
			'zzbbce',
			'zzsy',
			'jtbbce',
			'jtsy',
			'qjbbce',
			'qjsy',
			'clr'
		];
		let totalTit = [
				{
					jsrq: this.state.json['public-000105'] /* 国际化处理： 计算日期*/
				},
				{
					bzmc: this.state.json['public-000081'] /* 国际化处理： 币种*/
				},
				{
					djlx: this.state.json['public-000059'] /* 国际化处理： 单据类型*/
				},
				{
					billno: this.state.json['public-000060'] /* 国际化处理： 单据编号*/
				},
				{
					ybye: this.state.json['public-000103'] /* 国际化处理： 原币余额*/
				},
				{
					hl: this.state.json['public-000109'] /* 国际化处理： 调整前汇率*/
				},
				{
					zzbbye: this.state.json['public-000110'] /* 国际化处理： 组织本币余额*/
				},
				{
					jtbbye: this.state.json['public-000111'] /* 国际化处理： 集团本币余额*/
				},
				{
					qjbbye: this.state.json['public-000112'] /* 国际化处理： 全局本币余额*/
				},
				{
					tybye: this.state.json['public-000113'] /* 国际化处理： 调整后原币余额*/
				},
				{
					thl: this.state.json['public-000114'] /* 国际化处理： 调整后汇率*/
				},
				{
					tzzbbye: this.state.json['public-000115'] /* 国际化处理： 调整后组织本币余额*/
				},
				{
					tjtbbye: this.state.json['public-000116'] /* 国际化处理： 调整后集团本币余额*/
				},
				{
					tqjbbye: this.state.json['public-000117'] /* 国际化处理： 调整后全局本币余额*/
				},
				{
					zzbbce: this.state.json['public-000118'] /* 国际化处理： 组织本币差额*/
				},
				{
					zzsy: this.state.json['public-000119'] /* 国际化处理： 组织损益*/
				},
				{
					jtbbce: this.state.json['public-000120'] /* 国际化处理： 集团本币差额*/
				},
				{
					jtsy: this.state.json['public-000121'] /* 国际化处理： 集团损益*/
				},
				{
					qjbbce: this.state.json['public-000122'] /* 国际化处理： 全局本币差额*/
				},
				{
					qjsy: this.state.json['public-000123'] /* 国际化处理： 全局损益*/
				},
				{
					clr: this.state.json['public-000124'] /* 国际化处理： 处理人*/
				}
			],
			width;
		for (var i = 0, len = totalTit.length; i < len; i++) {
			if (titClone.hasOwnProperty(totit[i])) {
				if (totit[i] == 'billno') {
					newTit.push({
						title: totalTit[i][totit[i]],
						dataIndex: totit[i],
						key: totit[i],
						width: 160,
						render: (text, record, index) => {
							let tip = <div>{text}</div>;
							return (
								<div>
									<a
										style={{ textDecoration: 'none', cursor: 'pointer' }}
										onClick={() => {
											this.billLink(record);
										}}
									>
										<NCTooltip inverse={true} overlay={tip} trigger="hover" placement="top">
											<span>{text}</span>
										</NCTooltip>
									</a>
								</div>
							);
						}
					});
				} else {
					switch (totit[i]) {
						case 'tybye':
						case 'tzzbbye':
						case 'tjtbbye':
						case 'tqjbbye':
							width = 150;
							break;
						default:
							width = 100;
					}
					newTit.push({
						title: totalTit[i][totit[i]],
						dataIndex: totit[i],
						key: totit[i],
						width: width,
						render: (text, record, index) => {
							let tip = <div>{text}</div>;
							return (
								<div className="scomment">
									<NCTooltip inverse={true} overlay={tip} trigger="hover" placement="top">
										<span>{text}</span>
									</NCTooltip>
								</div>
							);
						}
					});
				}
			}
		}
		if (linkagiotage) {
			this.setState({
				columns: newTit,
				tableData: data,
				totalPage: Math.ceil(showData.length / this.state.pageSize),
				total: showData.length
			});
		} else {
			this.setState({
				columns: newTit,
				tableData: data,
				totalPage: Math.ceil(this.pksData.length / this.state.pageSize),
				total: this.pksData.length
			});
		}
	};

	//通过 pageSize: 一页展示数量  index: 第几页 来获取数据或者相对应的pk
	handlePaginationData = (arr, pageSize, index) => {
		let result = [];
		if (arr.length > pageSize) {
			result = arr.slice(pageSize * (index - 1), pageSize * index);
		} else {
			result = arr.slice(0, arr.length);
		}
		return result;
	};

	billLink = (record) => {
		ajax({
			url: '/nccloud/arap/arappub/linkarapbill.do',
			async: false,
			data: {
				pk_bill: record.pk_bill,
				pk_tradetype: record.pk_tradetype
			},
			success: (res) => {
				this.props.openTo(res.data.url, res.data.condition);
			}
		});
	};

	//改变表格数据
	handleChangeTableData = (data) => {
		this.setState({
			tableData: data
		});
	};

	//改变表格数据（汇总跳转到明细后）
	handleChangeTableDatas = (detailTableData) => {
		this.setState({
			tableData: detailTableData
		});
	};

	//点击下拉选择事件
	pageSizeChange = (val) => {
		let { linkagiotage } = this.props;
		this.setState(
			{
				pageSize: val
			},
			() => {
				if (linkagiotage) {
					let data = this.handlePaginationData(showData, this.state.pageSize, 1);
					this.setState({
						tableData: data,
						totalPage: Math.ceil(showData.length / this.state.pageSize),
						total: showData.length
					});
				} else {
					setDefData('detail-pageSize', appcode, val);
					let pkagiovageBs = this.handlePaginationData(this.pksData, val, 1);
					this.handleAjaxFn(pkagiovageBs, 1);
					this.setState({
						totalPage: Math.ceil(this.pksData.length / this.state.pageSize),
						total: this.pksData.length
					});
				}
			}
		);
	};

	//点击分页事件
	handleSelect = (eventKey) => {
		let { pageSize } = this.state;
		let { linkagiotage } = this.props;
		if (linkagiotage) {
			let data = this.handlePaginationData(showData, this.state.pageSize, eventKey);
			this.setState({
				tableData: data,
				activePage: eventKey
			});
		} else {
			let pkagiovageBs = this.handlePaginationData(this.pksData, pageSize, eventKey);
			this.handleAjaxFn(pkagiovageBs, eventKey);
		}
	};

	//点击分页与下拉事件请求后台
	handleAjaxFn = (pkagiovageBs, eventKey) => {
		let { sumJumpDetial } = this.props;
		let queryConsData = sumJumpDetial ? getDefData('cal-jump-query', appcode) : getDefData('cal-query', appcode);
		let queryConsDatas = JSON.parse(JSON.stringify(queryConsData));
		let url = sumJumpDetial
			? '/nccloud/arap/agiotage/onRecordLinkDetail.do'
			: '/nccloud/arap/agiotage/onhisrecord.do';
		queryConsDatas.agiotageVO.pkagiovageBs = pkagiovageBs;
		queryConsDatas.agiotageVO.pageSize = this.state.pageSize;
		ajax({
			url: url,
			data: queryConsDatas,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					this.props.handleDetailData(data, this.state.pageSize);
					this.setState({
						activePage: eventKey
					});
				}
			}
		});
	};

	render() {
		let { linkagiotage } = this.props;
		let { tableData, columns } = this.state;
		return (
			<div className="detailed-table">
				<NCTable
					columns={columns}
					bordered
					isDrag
					data={tableData}
					style={{ marginRight: '20px' }}
					scroll={{ y: 460 }}
					loading={false}
				/>
				<div className="paginationDiv">
					<div className="page-size">
						<NCSelect
							value={String(this.state.pageSize)}
							style={{ width: 85 }}
							onSelect={(val) => {
								this.pageSizeChange(val);
							}}
							className="fl"
							showClear={false}
						>
							<Option value={'10'}>{'10' + this.state.json['public-000229']}</Option>
							<Option value={'20'}>{'20' + this.state.json['public-000229']}</Option>
							<Option value={'50'}>{'50' + this.state.json['public-000229']}</Option>
							<Option value={'100'}>{'100' + this.state.json['public-000229']}</Option>
						</NCSelect>
						{linkagiotage ? (
							!!+showData.length && (
								<span className="fl NC_total">
									{this.state.json['public-000230'] +
										showData.length +
										this.state.json['public-000215']}
								</span>
							)
						) : (
							!!+this.pksData.length && (
								<span className="fl NC_total">
									{this.state.json['public-000230'] +
										this.pksData.length +
										this.state.json['public-000215']}
								</span>
							)
						)}
					</div>
					<div className="table-pagination">
						<NCPagination
							prev
							next
							boundaryLinks
							items={Number(this.state.totalPage)}
							maxButtons={Number(this.state.totalPage) === 7 ? 6 : 5}
							activePage={Number(this.state.activePage)}
							onSelect={(val) => {
								this.handleSelect(val);
							}}
						/>
					</div>
				</div>
			</div>
		);
	}
}

Secondpage.defaultProps = defaultProps12;

Secondpage = createPage({})(Secondpage);

export default Secondpage;
