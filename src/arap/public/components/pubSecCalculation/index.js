import React, { Component } from 'react';
import {
	ajax,
	base,
	createPage,
	toast,
	formDownload,
	getMultiLang,
	cardCache,
	createPageIcon
} from 'nc-lightapp-front';
import QueryCondition from './querycondition'; //查询条件
import SummaryTable from './summaryTable'; //汇总表格
import DetailedTable from './detailedTable'; //明细表格
import getInitOrg from '../pubUtils/getInitOrg';
import promptbox from '../promptbox';
import { buttonClick, initTemplate } from './events/index';
import './index.less';
const { NCBackBtn } = base;
let { setDefData, getDefData } = cardCache;
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
let detailDataSource;
let summaryDataSource;
let isCustomer; //是否为客户
let title;
let detailPageSize = 10; //明细页面 下拉选项PageSize  默认为10条/页

class Secondpage extends Component {
	constructor(props) {
		super(props);
		this.busiTypeVal = this.props.getUrlParam('Pk_org') ? JSON.parse(this.props.getUrlParam('Pk_org')).value : null;
		this.busiTypedis = this.props.getUrlParam('Pk_org')
			? JSON.parse(this.props.getUrlParam('Pk_org')).display
			: null;
		this.refpk2 = this.props.getUrlParam('Pk_org') ? JSON.parse(this.props.getUrlParam('Pk_org')).refpk2 : null;
		this.state = {
			json: {},
			mainData: {
				busiType: {
					display: this.busiTypedis,
					value: this.busiTypeVal,
					refpk2: this.refpk2
				}, //财务组织
				m_sKsbm: {
					display: '',
					value: null
				}, //客户
				agiotageType: {
					display: '',
					value: null
				}, //损益类型
				queryType: {
					display: '' /* 国际化处理： 汇总查询*/,
					value: 0
				}, //查询方式
				m_sDjlx: {
					display: '',
					value: null
				}, //交易类型
				m_sCurrency: {
					display: '',
					value: null
				}, //币种
				m_sDjbhBeg: {
					display: '',
					value: null
				}, //单据编号开始
				m_sDjbhEnd: {
					display: '',
					value: null
				}, //单据编号结束
				m_sDateBeg: {
					display: '',
					value: null
				}, //计算日期开始
				m_sDateEnd: {
					display: '',
					value: null
				}, //计算日期结束
				m_sMaxYbje: {
					display: '',
					value: null
				}, //最大本币差额
				m_sMinYbje: {
					display: '',
					value: null
				}, //最小本币差额
				currType: {
					display: '' /* 国际化处理： 组织本币*/,
					value: 'org'
				} //本币类型
			},
			sumQueryCons: [], //汇总查询条件
			detailTableData: {}, //获取明细表格查询数据
			summaryTableData: {}, //获取汇总表格查询数据
			cancelDetailTableData: [], //点击取消损益获取数据
			selectedClbh: [], //明细被选中pk集合
			sumJumpDetial: false //是否汇总跳转明细
		};
		this.handleCancel = this.handleCancel.bind(this);
		this.getQueryCons = this.getQueryCons.bind(this);
		this.getDetailQueryData = this.getDetailQueryData.bind(this);
		this.getSummaryQueryData = this.getSummaryQueryData.bind(this);
		this.handlechangeDetailFn = this.handlechangeDetailFn.bind(this);
		this.setSumJumpDetStatus = this.setSumJumpDetStatus.bind(this);
		this.handleSignSelectDet = this.handleSignSelectDet.bind(this);
		this.handleSignSelectSum = this.handleSignSelectSum.bind(this);
		this.handleQueryOnRef = this.handleQueryOnRef.bind(this);
		this.handleSumOnRef = this.handleSumOnRef.bind(this);
		this.handleDetOnref = this.handleDetOnref.bind(this);
		this.handleDetailData = this.handleDetailData.bind(this);
	}
	componentWillMount() {
		let { mainData } = this.state;
		let callback = (json) => {
			this.setState({ json: json }, () => {
				(mainData.queryType = {
					display: this.state.json['public-000125'] /* 国际化处理： 汇总查询*/,
					value: 0
				}), //查询方式
					(mainData.currType = {
						display: this.state.json['public-000104'] /* 国际化处理： 组织本币*/,
						value: 'org'
					}); //本币类型
				this.setState({
					mainData
				});
				isCustomer = this.props.isCustomer;
				switch (this.props.title) {
					case 'yfhdsyjl':
						title = this.state.json['public-000126']; /* 国际化处理： 应付汇兑损益记录*/
						break;
					case 'yshdsyjl':
						title = this.state.json['public-000127']; /* 国际化处理： 应收汇兑损益记录*/
						break;
				}
			});
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {
		this.props.button.setButtonVisible([ 'ExportData' ], false);
		this.props.button.setButtonDisabled('CancelProfit', true);
		let appcode = this.props.getSearchParam('c');
		if (!this.busiTypeVal) {
			getInitOrg(appcode, this.cb);
		}
	}

	cb = (pkOrg) => {
		let { mainData } = this.state;
		if (!this.props.isBack) {
			mainData.busiType = {
				display: pkOrg.display,
				value: pkOrg.value,
				refpk2: pkOrg.refpk2
			};
			this.setState({
				mainData
			});
		}
	};

	//获取表格数据
	getTableData = (url) => {
		let agiotageBzVOsData = transData.agiotageBzVOs;
		let arr = [];
		let obj = {};
		for (let i = 0, len = agiotageBzVOsData.length; i < len; i++) {
			obj = {
				m_sBzbm: agiotageBzVOsData[i].bzbm,
				m_sBzmc: agiotageBzVOsData[i].bzmc
			};
			arr.push(obj);
		}
		let oncaldata = {
			agiotageBzVOs: arr,
			date: transData.queryData.date.value,
			sfbz: this.props.sfbz,
			pk_org: transData.queryData.pk_org.value
		};
		ajax({
			url: url,
			data: oncaldata,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (url == '/nccloud/arap/agiotage/oncalculation.do') {
						let showData = data.showData;
						isGroupUse = data.isGroupUsed;
						isGlobalUse = data.isGlobalUsed;
						if (isGroupUse) {
							this.columns1.push(groupTit);
						}
						if (isGlobalUse) {
							this.columns1.push(globalTit);
						}
						showData.map((item, i) => {
							item.key = i;
							item.index = i + 1;
						});
						this.setState({
							tableData: showData,
							columns: this.columns1
						});
					} else {
						toast({
							color: 'success',
							content: this.state.json['public-000088'] /* 国际化处理： 损益成功*/
						});
					}
				}
			}
		});
	};

	//获取（明细查询/汇总查询）查询条件查询方式
	getQueryCons = (data) => {
		this.setState({
			mainData: data
		});
	};

	//获取明细查询数据
	getDetailQueryData = (data, flag, activePage) => {
		if (data.showData.length == 0) {
			if (flag == 1) {
				toast({
					color: 'success',
					title: this.state.json['public-000128'] /* 国际化处理： 刷新成功！*/
				});
			} else if (flag != 'mxqxsy') {
				toast({
					color: 'warning',
					content: this.state.json['public-000213']
				}); /* 国际化处理： 未查询出相应的数据！*/
			}
		} else {
			if (flag == 1) {
				toast({
					color: 'success',
					title: this.state.json['public-000128'] /* 国际化处理： 刷新成功！*/
				});
			} else if (flag != 'mxqxsy') {
				toast({
					color: 'success',
					content: this.state.json['public-000214'] + data.pksData.length + this.state.json['public-000215']
				}); /* 国际化处理： 已成功共几条数据！*/
			}
		}
		data.activePage = activePage;
		this.setState({
			detailTableData: data,
			sumJumpDetial: false
		});
	};

	//获取汇总查询数据
	getSummaryQueryData = (data, hisrecorddata, flag) => {
		if (data.showData.length == 0) {
			if (flag == 1) {
				toast({
					color: 'success',
					title: this.state.json['public-000128'] /* 国际化处理： 刷新成功！*/
				});
			} else if (flag != 'hzqxsy') {
				toast({
					color: 'warning',
					content: this.state.json['public-000213']
				}); /* 国际化处理： 未查询出相应的数据！*/
			}
		} else {
			if (flag == 1) {
				toast({
					color: 'success',
					title: this.state.json['public-000128'] /* 国际化处理： 刷新成功！*/
				});
			} else if (flag != 'hzqxsy') {
				toast({
					color: 'success',
					content: this.state.json['public-000214'] + data.showData.length + this.state.json['public-000215']
				}); /* 国际化处理： 已成功共几条数据！*/
			}
		}
		this.setState({
			summaryTableData: data,
			sumQueryCons: hisrecorddata,
			sumJumpDetial: false
		});
	};

	//点击取消损益表格数据是否被选中
	selectedFn = (arr) => {
		if (arr) {
			for (var i = 0, len = arr.length; i < len; i++) {
				if (arr[i].xzbz) {
					return true;
				}
			}
		}
		return false;
	};

	//取消损益按钮（汇总/明细）
	handleCancelprofit = () => {
		let { mainData } = this.state;
		let queryTypeVal = mainData.queryType.value;
		let DataSourse = queryTypeVal == 0 ? summaryDataSource : detailDataSource;
		let isSelected = this.selectedFn(DataSourse);
		if (!isSelected) {
			toast({
				color: 'warning',
				content: this.state.json['public-000130'] /* 国际化处理： 请选择一行数据！*/
			});
		} else {
			if (mainData.queryType.value == 0) {
				promptbox(
					this.state.json['public-000091'],
					this.functionSureSum, //取消损益  汇总回调函数
					'warning'
				); /* 国际化处理： 同一批次的汇兑损益记录都将被取消，是否继续？*/
			} else {
				promptbox(
					this.state.json['public-000091'],
					this.functionSureDet, //取消损益  明细回调函数
					'warning'
				); /* 国际化处理： 同一批次的汇兑损益记录都将被取消，是否继续？*/
			}
		}
	};

	//取消损益 确定按钮回调函数(汇总)
	functionSureSum = () => {
		let { mainData } = this.state;
		let agiotageVO1 = {
			//查询条件
			busiType: mainData.busiType.value,
			agiotageType: mainData.agiotageType.value,
			currType: mainData.currType.value,
			iszgAgiotage: false,
			m_HsMode: false,
			queryType: mainData.queryType.value,
			m_sDateBeg: mainData.m_sDateBeg.value,
			m_sDateEnd: mainData.m_sDateEnd.value,
			m_sDwbm: mainData.busiType.value,
			m_sDjlx: mainData.m_sDjlx.value,
			m_sMaxYbje: mainData.m_sMaxYbje.value,
			m_sMinYbje: mainData.m_sMinYbje.value,
			m_sDjbhEnd: mainData.m_sDjbhEnd.value,
			m_sDjbhBeg: mainData.m_sDjbhBeg.value
		};
		let pks = []; // 选中行的pks值
		for (var i = 0, len = summaryDataSource.length; i < len; i++) {
			if (summaryDataSource[i].xzbz) {
				pks.push(summaryDataSource[i].pks[0]);
			}
		}
		let recordsumdata = {
			sfbz: this.props.sfbz,
			pk_org: mainData.busiType.value, //财务组织
			dealnos: pks,
			agiotageVO: agiotageVO1
		};
		ajax({
			url: '/nccloud/arap/agiotage/onCancelAgiotageForGather.do',
			data: recordsumdata,
			success: (res) => {
				//此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
				let { success, data } = res;
				if (success) {
					this.setSummaryTableData('hzqxsy');
					toast({
						color: 'success',
						content: this.state.json['public-000092'] /* 国际化处理： 取消损益成功！*/
					});
				}
			},
			error: (msg) => {
				toast({
					color: 'danger',
					content: String(msg)
				});
			}
		});
	};

	//取消损益 确定按钮回调函数(明细)
	functionSureDet = () => {
		let { mainData, selectedClbh } = this.state;
		let agiotageMainVOss = [];
		for (var i = 0, len = detailDataSource.length; i < len; i++) {
			if (detailDataSource[i].xzbz) {
				agiotageMainVOss.push({
					dealno: detailDataSource[i].clbh,
					busidate: detailDataSource[i].jsrq
				});
			}
		}
		let recorddata = {
			agiotageMainVOs: agiotageMainVOss,
			sfbz: this.props.sfbz,
			pk_org: mainData.busiType.value,
			dealnoArr: selectedClbh
		};
		ajax({
			url: '/nccloud/arap/agiotage/onCancelAgiotage.do',
			data: recorddata,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					this.setDetailTableData('mxqxsy');
					toast({
						color: 'success',
						content: this.state.json['public-000092'] /* 国际化处理： 取消损益成功！*/
					});
				}
			},
			error: (msg) => {
				toast({
					color: 'danger',
					content: String(msg).substring(6)
				});
			}
		});
	};

	//点击取消损益改变汇总表格数据
	setSummaryTableData = (flag) => {
		this.child.handleQueryBtn(flag);
	};

	//点击取消损益改变明细表格数据
	setDetailTableData = (flag) => {
		let { sumJumpDetial } = this.state;
		if (sumJumpDetial) {
			//汇总跳转明细 取消损益后 查询出新数据
			let linkrecorddata = getDefData('cal-jump-query', this.props.appcode);
			linkrecorddata.agiotageVO.pageSize = detailPageSize;
			ajax({
				url: '/nccloud/arap/agiotage/onRecordLinkDetail.do',
				data: linkrecorddata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						this.setState({
							detailTableData: data,
							selectedClbh: []
						});
					}
				}
			});
		} else {
			this.child.handleQueryBtn(flag); //明细界面 取消损益后  查询出新数据
		}
	};

	//刷新按钮
	handleRefresh = () => {
		this.child.handleRefreshGetData(1);
	};

	//选择标志复选框(汇总)
	handleSignSelectSum = (index, key) => {
		let { summaryTableData } = this.state;
		summaryDataSource = summaryTableData.showData;
		if (!summaryDataSource[index]['title']) {
			return false;
		} else {
			return (value) => {
				summaryDataSource = summaryTableData.showData;
				summaryDataSource[index][key] = value;
				this.setState(
					{
						summaryDataSource
					},
					() => {
						let flag = summaryDataSource.some((item, index) => {
							return item.xzbz;
						});
						if (flag) {
							this.props.button.setButtonDisabled('CancelProfit', false);
						} else {
							this.props.button.setButtonDisabled('CancelProfit', true);
						}
					}
				);
			};
		}
	};

	//点击页签改变明细数据
	handleDetailData = (data, pageSize) => {
		detailPageSize = pageSize;
		let { selectedClbh } = this.state;
		for (let i = 0, len = data.showData.length; i < len; i++) {
			if (selectedClbh.indexOf(data.showData[i].clbh) != -1) {
				data.showData[i].xzbz = true;
			}
		}
		this.setState({
			detailTableData: data
		});
	};

	//选择标志复选框(明细)
	handleSignSelectDet = (index, key) => {
		let { detailTableData, selectedClbh } = this.state;
		return (value) => {
			detailDataSource = detailTableData.showData;
			detailDataSource[index][key] = value;
			let ind = selectedClbh.indexOf(detailDataSource[index].clbh);
			if (detailDataSource[index][key]) {
				if (ind == -1) {
					selectedClbh.push(detailDataSource[index].clbh);
				}
			} else {
				if (ind != -1) {
					selectedClbh.splice(ind, 1);
				}
			}
			for (let i = 0, len = detailDataSource.length; i < len; i++) {
				if (detailDataSource[i].clbh == detailDataSource[index].clbh) {
					detailDataSource[i].xzbz = detailDataSource[index].xzbz;
				}
			}
			this.setState(
				{
					detailDataSource,
					selectedClbh
				},
				() => {
					let flag = detailDataSource.some((item, index) => {
						return item.xzbz;
					});
					if (flag) {
						this.props.button.setButtonDisabled('CancelProfit', false);
					} else {
						this.props.button.setButtonDisabled('CancelProfit', true);
					}
				}
			);
		};
	};

	//改变detailTable的表格数据
	handlechangeDetailFn = (data) => {
		let { mainData } = this.state;
		mainData.queryType.value = 1;
		this.setState(
			{
				mainData,
				sumJumpDetial: true
			},
			() => {
				this.setState({
					detailTableData: data,
					selectedClbh: []
				});
			}
		);
	};

	// 汇总跳转明细
	setSumJumpDetStatus = () => {
		this.props.button.setButtonVisible([ 'ExportData' ], true);
		this.child.changeQueryType();
	};

	//打印
	handlePrint = () => {};

	//返回
	handleCancel = () => {
		this.props.linkTo(this.props.jumpPath, {
			Pk_org: this.props.getUrlParam('Pk_org'),
			pagecode: this.props.pagecode,
			appcode: this.props.appcode
		});
	};

	//导出
	handleExport = () => {
		let { sumJumpDetial } = this.state;
		let data1, url;
		if (sumJumpDetial) {
			data1 = JSON.parse(JSON.stringify(getDefData('cal-jump-query', this.props.appcode)));
			delete data1.agiotageVO.pageSize;
			url = '/nccloud/arap/agiotage/onRecordLinkDetail.do';
		} else {
			data1 = JSON.parse(JSON.stringify(getDefData('cal-query', this.props.appcode)));
			delete data1.agiotageVO.pageSize;
			url = '/nccloud/arap/agiotage/onhisrecord.do';
		}
		ajax({
			url: url,
			data: data1,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					var showData = JSON.stringify(data.showData);
					var titleMap = JSON.stringify(data.title);
					var exportData = {
						showData: showData
					};
					let data2 = {
						rightdata: null,
						moduleName: '',
						billType: '',
						exportDatas: exportData,
						showData: [ showData, titleMap ],
						title: title
					};
					formDownload({
						params: data2,
						url: '/nccloud/arap/arappub/gridOutputExcelAction.do',
						enctype: 2
					});
				}
			}
		});
	};

	handleQueryOnRef = (ref) => {
		this.child = ref;
	};

	handleSumOnRef = (ref) => {
		this.changeSummaryTableData = ref;
	};

	handleDetOnref = (ref) => {
		this.changeDetailTableData = ref;
	};

	render() {
		let { button, appcode, sfbz, isBack } = this.props;
		let { createButtonApp, setButtonVisible, setButtonDisabled } = button;
		let { mainData, detailTableData, summaryTableData, sumQueryCons, selectedClbh, sumJumpDetial } = this.state;
		let { queryType, busiType } = mainData;
		return (
			<div id="secondpageId">
				<div className="header">
					{!isBack && createPageIcon && createPageIcon()}
					<h2 className="title">
						{isBack && <NCBackBtn onClick={this.handleCancel} />}
						{title}
					</h2>
					<div className="btn-group">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="content">
					<QueryCondition
						getQueryCons={this.getQueryCons}
						getDetailQueryData={this.getDetailQueryData}
						getSummaryQueryData={this.getSummaryQueryData}
						sfbz={sfbz}
						isCustomer={isCustomer}
						isBack={isBack}
						setButtonVisible={setButtonVisible}
						setButtonDisabled={setButtonDisabled}
						createButtonApp={createButtonApp}
						appcode={appcode}
						onRef={this.handleQueryOnRef}
						json={this.state.json}
					/>
					<div className={'table-area'}>
						{queryType.value == 0 ? (
							<SummaryTable
								handleSignSelectSum={this.handleSignSelectSum}
								tableDatas={summaryTableData}
								sumQueryCons={sumQueryCons}
								changeDetailData={this.handlechangeDetailFn}
								setSumJumpDetStatus={this.setSumJumpDetStatus}
								setButtonDisabled={setButtonDisabled}
								onRef={this.handleSumOnRef}
								sfbz={sfbz}
								isCustomer={isCustomer}
								appcode={appcode}
							/>
						) : (
							<DetailedTable
								handleDetailData={this.handleDetailData}
								handleSignSelectDet={this.handleSignSelectDet}
								tableData={detailTableData}
								selectedClbh={selectedClbh}
								onRef={this.handleDetOnref}
								busiType={busiType}
								appcode={appcode}
								sumJumpDetial={sumJumpDetial}
							/>
						)}
					</div>
				</div>
			</div>
		);
	}
}

Secondpage.defaultProps = defaultProps12;

Secondpage = createPage({
	initTemplate: initTemplate
})(Secondpage);

export default Secondpage;
