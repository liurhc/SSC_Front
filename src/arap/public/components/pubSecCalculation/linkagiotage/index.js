import React, { Component } from 'react';
import { ajax, createPage, cacheTools, getMultiLang } from 'nc-lightapp-front';
import DetailedTable from '../detailedTable'; //明细表格
import '../index.less';
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
let detailDataSource;
let title;

class Secondpage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			detailTableData: [], //获取明细表格查询数据
			json: {}
		};
		this.handleSignSelectDet = this.handleSignSelectDet.bind(this);
	}

	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				let pks = [];
				let dataArr = [];
				let linkurl = '';
				let data = {};
				switch (this.props.title) {
					case 'yfhdsyjl':
						title = this.state.json['public-000126']; /* 国际化处理： 应付汇兑损益记录*/
						break;
					case 'yshdsyjl':
						title = this.state.json['public-000127']; /* 国际化处理： 应收汇兑损益记录*/
						break;
				}
				dataArr = cacheTools.get('checkedData');
				linkurl = '/nccloud/arap/arappub/queryGridByPK.do';
				if (dataArr) {
					data = {
						fipLink: dataArr,
						pageId: this.props.getSearchParam('p')
					};
				}
				ajax({
					url: linkurl,
					data: data,
					success: function(res) {
						//此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
						let { success, data } = res;
						if (success) {
							this.setState({
								detailTableData: data
							});
						}
					}.bind(this)
				});
				this.setState({});
			});
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	//选择标志复选框(明细查询)
	handleSignSelectDet = (index, key) => {
		let { detailTableData } = this.state;
		return (value) => {
			if (value) {
				this.props.button.setButtonDisabled('CancelProfit', false);
			} else {
				this.props.button.setButtonDisabled('CancelProfit', true);
			}
			detailDataSource = detailTableData.showData;
			detailDataSource[index][key] = value;
			for (let i = 0, len = detailDataSource.length; i < len; i++) {
				if (detailDataSource[i].clbh == detailDataSource[index].clbh) {
					detailDataSource[i].xzbz = detailDataSource[index].xzbz;
				}
			}
			this.setState({
				detailDataSource
			});
		};
	};

	//改变detailTable的表格数据
	handlechangeDetailFn = (data) => {
		this.setState(() => {
			this.setState({
				detailTableData: data
			});
		});
	};

	render() {
		let { detailTableData } = this.state;
		return (
			<div id="secondpageId">
				<div className="header">
					<h2 className="title">{title}</h2>
				</div>
				<div className="content">
					<DetailedTable
						linkagiotage
						handleSignSelectDet={this.handleSignSelectDet}
						tableData={detailTableData}
						onRef={(ref) => {
							this.changeDetailTableData = ref;
						}}
					/>
				</div>
			</div>
		);
	}
}

Secondpage.defaultProps = defaultProps12;

Secondpage = createPage({})(Secondpage);

export default Secondpage;
