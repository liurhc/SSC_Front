import { promptBox,getMultiLang } from 'nc-lightapp-front';

export default function promptBoxFn(content, callback_OK, color, title) {

	let callback = (json) => {
		promptBox({
			color: color, // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
			title: title,
			content: content, // 提示内容,非必输
			noFooter: false, // 是否显示底部按钮(确定、取消),默认显示(false),非必输
			noCancelBtn: false, // 是否显示取消按钮,，默认显示(false),非必输
			beSureBtnName: json['public-000076'], // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/this.state.json['public-000076']
			cancelBtnName: json['public-000077'], // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/this.state.json['public-000077']
			beSureBtnClick: functionSure.bind(this, callback_OK) // 确定按钮点击调用函数,非必输
		});
	};
	getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
}

function functionSure(callback_OK) {
	if (callback_OK && typeof callback_OK == 'function') callback_OK();
}
