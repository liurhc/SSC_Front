import { ajax, cacheTools } from 'nc-lightapp-front';
/**
 * 联查凭证   包含凭证预览   
 * @author xiejhk
 * @param {*} vo 选中的第一行数据
 * @param {*} pk_bill 单据主键
 * @param {*} billType 单据类型
 * @param {*} appcode 小应用编码
 */
export default function linkvouchar(that, props, vo, pk_bill, appcode) {
	let pk_org;
	let pk_group;
	let pk_tradetype;
	let billdate;
	//区分卡片和列表数据格式的不同，采用不同方式获取相应的值
	if (vo.data) {
		pk_org = vo.data.values.pk_org.value;
		pk_group = vo.data.values.pk_group.value;
		pk_tradetype = vo.data.values.pk_tradetype.value;
		billdate = vo.data.values.billdate.value;
	} else {
		pk_org = vo.rows[0].values.pk_org.value;
		pk_group = vo.rows[0].values.pk_group.value;
		pk_tradetype = vo.rows[0].values.pk_tradetype.value;
		billdate = vo.rows[0].values.billdate.value;
	}
	let pk_system = appcode.substring(0, 4) == '2006' ? 'AR' : 'AP';
	//拼接联查数据
	let querydata = [
		{
			pk_group: pk_group,
			pk_org: pk_org,
			relationID: pk_bill,
			pk_billtype: pk_tradetype
		}
	];
	//拼接预览数据
	var viewData = {
		messagevo: [
			{
				pk_system: pk_system, //系统编码
				relationid: pk_bill, //单据主键
				busidate: billdate, //单据日期
				pk_org: pk_org, //组织主键
				pk_group: pk_group, //集团主键
				pk_billtype: pk_tradetype //交易类型
			}
		], //支持多笔预览
		desbilltype: [ 'C0' ], //目标单据类型,C0为总账凭证,目前只支持总张凭证
		srcbilltype: pk_tradetype //业务组交易类型
	};
	ajax({
		url: '/nccloud/arap/arappub/linkvouchar.do',
		data: querydata,
		async: false,
		success: (res) => {
			if (res.success) {
				let srcCode = res.data.src;
				if ('_LinkVouchar2019' == srcCode) {
					//走联查
					if (res.data.des) {
						//跳转到凭证界面
						if (res.data.pklist) {
							if (res.data.pklist.length == 1) {
								//单笔联查
								props.openTo(res.data.url, {
									status: 'browse',
									appcode: res.data.appcode,
									pagecode: res.data.pagecode,
									id: res.data.pklist[0],
									n: that.state.json['public-000231'], //'联查凭证'
									backflag: 'noback'
								});
								return;
							} else {
								//多笔联查
								cacheTools.set('checkedData', res.data.pklist);
								props.openTo(res.data.url, {
									status: 'browse',
									appcode: res.data.appcode,
									pagecode: res.data.pagecode,
									n: that.state.json['public-000231'] //'联查凭证'
								});
								return;
							}
						}
					} else {
						//跳转到会计平台
						cacheTools.set(appcode + srcCode, res.data.pklist);
					}
				} else if ('_Preview2019' == srcCode) {
					//走预览
					cacheTools.set(appcode + srcCode, viewData);
				}
				//打开凭证节点
				props.openTo(res.data.url, {
					status: 'browse',
					appcode: res.data.appcode,
					pagecode: res.data.pagecode,
					scene: appcode + srcCode,
					n: that.state.json['public-000232'] // '凭证预览' 凭证使用这个参数,会计平台不用
				});
			}
		}
	});
}
