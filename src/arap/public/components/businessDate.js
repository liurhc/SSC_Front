import { getBusinessInfo } from 'nc-lightapp-front';
//获取当前业务日期 "2018-08-00 17:47:18"
function businessDateH() {
	let businessDateH;
	if (typeof getBusinessInfo == 'function') {
		businessDateH = getBusinessInfo() && getBusinessInfo().businessDate;
	}
	return businessDateH;
}

//获取当前业务日期 "2018-08-00"
function businessDate() {
	let businessDate, businessDates;
	if (typeof getBusinessInfo == 'function') {
		businessDate = getBusinessInfo() && getBusinessInfo().businessDate;
		businessDates = businessDate.substring(0, 10);
	}
	return businessDates;
}

function getDefaultBusinessDate() {
	let businessDate;
	return new Promise((resolve, reject) => {
		if (typeof getBusinessInfo == 'function') {
			businessDate = getBusinessInfo();
			if (businessDate) {
				resolve(businessDate);
			}
		}
	});
}

//获取当前时间的时分秒 ' 15:57:50'
function getHMS() {
	var date = new Date();
	var seperator2 = ':';
	var hour = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();
	if (hour >= 0 && hour <= 9) {
		hour = '0' + hour;
	}
	if (minutes >= 0 && minutes <= 9) {
		minutes = '0' + minutes;
	}
	if (seconds >= 0 && seconds <= 9) {
		seconds = '0' + seconds;
	}
	var currentdate = ' ' + hour + seperator2 + minutes + seperator2 + seconds;
	return currentdate;
}

//获取业务日期的前一个月 '2018-07-20'
function getPreMonth(date) {
	var arr = date.split('-');
	var year = arr[0]; //获取当前日期的年份
	var month = arr[1]; //获取当前日期的月份
	var day = arr[2]; //获取当前日期的日
	var days = new Date(year, month, 0);
	days = days.getDate(); //获取当前日期中月的天数
	var year2 = year;
	var month2 = parseInt(month) - 1;
	if (month2 == 0) {
		year2 = parseInt(year2) - 1;
		month2 = 12;
	}
	var day2 = day;
	var days2 = new Date(year2, month2, 0);
	days2 = days2.getDate();
	if (day2 > days2) {
		day2 = days2;
	}
	if (month2 < 10) {
		month2 = '0' + month2;
	}
	var t2 = year2 + '-' + month2 + '-' + day2;
	return t2;
}

export { businessDateH, businessDate, getHMS, getPreMonth, getDefaultBusinessDate };
