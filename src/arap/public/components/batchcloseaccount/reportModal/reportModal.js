import React, { Component } from 'react';
import { high, base, ajax, getMultiLang } from 'nc-lightapp-front';
const { NCTable, NCModal, NCButtonGroup, NCButton, NCTabs, NCRow, NCCol, NCNavbar, NCMenu } = base;
import './index.less';
let columns_report = [
	{
		title: '' /* 国际化处理： 序号*/,
		dataIndex: 'serial',
		key: 'serial'
	},
	{
		title: '' /* 国际化处理： 月检查不合格单据*/,
		dataIndex: 'notQualified',
		key: 'notQualified'
	},
	{
		title: '' /* 国际化处理： 检查结果*/,
		dataIndex: 'result',
		key: 'result'
	}
];
let emptyFunc = () => <span />; /* 国际化处理： 没有结账报告*/

const { NCTabPane } = NCTabs;
const NCSubMenu = NCMenu.NCSubMenu;
const NCMenuItemGroup = NCMenu.NCItemGroup;
const NCMenuToggle = NCMenu.NCMenuToggle;
const NCSideContainer = NCMenu.NCSideContainer;

export default class ReportModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			current: '',
			tableData: [],
			reportTableData: [],
			json: {}
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				columns_report = [
					{
						title: this.state.json['public-000028'] /* 国际化处理： 序号*/,
						dataIndex: 'serial',
						key: 'serial'
					},
					{
						title: this.state.json['public-000029'] /* 国际化处理： 月检查不合格单据*/,
						dataIndex: 'notQualified',
						key: 'notQualified'
					},
					{
						title: this.state.json['public-000030'] /* 国际化处理： 检查结果*/,
						dataIndex: 'result',
						key: 'result'
					}
				];
				emptyFunc = () => <span>{this.state.json['public-000031']}</span>;
			});
		};
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentWillReceiveProps(nextProp) {
		if (nextProp.reportTableData) {
			if (nextProp.reportTableData != this.state.reportTableData) {
				this.setState({
					reportTableData: nextProp.reportTableData
				});
			}
		}
	}

	handleClick = (e) => {
		let reportTableData = this.state.reportTableData;
		if (reportTableData[e.key]) {
			let tableData = [];
			if (reportTableData[e.key].listReportVO) {
				tableData = reportTableData[e.key].listReportVO;
			}
			this.setState({
				current: e.key,
				tableData: tableData
			});
		}
	};

	initmodalBody = (checkedDatas) => {
		return (
			<div className="report-content">
				<div className="content-left">
					<NCMenu
						onClick={this.handleClick}
						style={{ width: 167 }}
						defaultOpenKeys={[ 'sub1' ]}
						selectedKeys={[ this.state.current ]}
						mode="inline"
					>
						<NCSubMenu
							key="sub1"
							title={
								<span>
									<span>{this.state.json['public-000024']}</span>
									{/* 国际化处理： 财务组织*/}
								</span>
							}
						>
							{checkedDatas.map(function(item) {
								return <NCMenu.Item key={item.pk_org}>{item.pk_orgName}</NCMenu.Item>;
							})}
						</NCSubMenu>
					</NCMenu>
				</div>
				<div className="content-right">
					<NCTable
						columns={columns_report}
						bordered
						isDrag
						data={this.state.tableData}
						scroll={{
							x: columns_report.length > 8 ? 100 + (columns_report.length - 8) * 15 + '%' : '100%',
							y: 320
						}}
					/>
				</div>
			</div>
		);
	};

	render() {
		let { showReportModal, handleOpenOrClose, checkedDatas, reportTableData, batchCloseAccount } = this.props;
		return (
			<div id="finalTreatment_report">
				<NCModal className="msg-modal" show={showReportModal} onHide={handleOpenOrClose}>
					<NCModal.Header closeButton>
						<NCModal.Title>{this.state.json['public-000032']}</NCModal.Title>
						{/* 国际化处理： 结账报告*/}
					</NCModal.Header>
					<NCModal.Body>{this.initmodalBody(checkedDatas)}</NCModal.Body>
					<NCModal.Footer>
						<NCButton colors="primary" onClick={batchCloseAccount}>
							{this.state.json['public-000033']}
							{/* 国际化处理： 完成结账*/}
						</NCButton>
					</NCModal.Footer>
				</NCModal>
			</div>
		);
	}
}
