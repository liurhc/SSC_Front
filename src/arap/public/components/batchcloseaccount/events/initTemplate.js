import { ajax, base } from 'nc-lightapp-front';

export default function(props) {

	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do',
		data: {
			pagecode: props.getSearchParam('p'),//页面id
			appcode: props.getSearchParam('c')//注册按钮的id
		},
		success: (res) => {
			if(res.data){
				let button = res.data;
				props.button.setButtons(button);
			}
		}
	});
}





