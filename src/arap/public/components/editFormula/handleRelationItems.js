import { ajax, base } from 'nc-lightapp-front';
export default function handleRelationItems(afterData,billinfo) {
	//编辑关联项---编辑公式
		// 编辑后参数
		const { 
			type ,  //编辑是表单还是表格
			 areaCode,  //区域编码
			key,    //编辑的字段的主键
			value, //编辑字段的新值
			changedrows, //编辑字段的旧值
			 index ,//若编辑的是表格，为表格的行数 从0 开始
			callback, //编辑后
			
		} = afterData;
		if (billinfo) {
			let { billtype, pagecode, headcode, bodycode } = billinfo;

			let url, data, success;
			//单个表单 或 单个表格
			if (billtype === 'form' || billtype === 'grid') {
				if (type === 'form') {
					url = '/nccloud/platform/pub/formafteredit.do';
					data = this.props.createFormAfterEventData(pagecode, areaCode, areaCode, key, value);
				} else if (type === 'table') {
					url = '/nccloud/platform/pub/gridafteredit.do';
					data = this.props.createGridAfterEventData(pagecode, areaCode, areaCode, key, changedrows, index);
				}
			}
			//单据
			if (billtype === 'card' || billtype === 'extcard') {
				let tablecode;
				if (Object.prototype.toString.call(bodycode) === "[object Object]") {
					tablecode = Object.keys(bodycode);
					if (tablecode.length == 1) {
						tablecode = tablecode[0];
					}
				} else {
					tablecode = bodycode;
				}
				if (type === 'form') {
					url = '/nccloud/platform/pub/cardheadafteredit.do';
					if (billtype === 'extcard') {
						url = '/nccloud/platform/pub/extcardheadafteredit.do';
					}
					data = this.props.createHeadAfterEventData(pagecode, headcode, tablecode, areaCode, key, value);
				} else if (type === 'table') {
					url = '/nccloud/platform/pub/cardbodyafteredit.do';
					if (billtype === 'extcard') {
						url = '/nccloud/platform/pub/extcardbodyafteredit.do';
					}
					data = this.props.createBodyAfterEventData(
						pagecode,
						headcode,
						tablecode,
						areaCode,
						key,
						changedrows,
						index
					);
				}
			}
			//请求成功的回调函数
			success = (res) => {
				let tableTypeObj = {};
				//单个表单
				if (billtype === 'form' && type === 'form') {
					if (res.data && res.data && res.data[areaCode]) {
						this.props.form.setAllFormValue({ [areaCode]: res.data[areaCode] }, true, false, key);
					}
				}
				//单个表格
				if (billtype === 'grid' && type === 'table') {
					if (res.data && res.data && res.data[areaCode]) {
						this.props.editTable.setTableData(areaCode, res.data[areaCode], false);
						tableTypeObj[areaCode] = 'editTable';
					}
				}
				//单据
				if (billtype === 'card' || billtype === 'extcard') {
					//表头赋值
					if (res.data && res.data.head && res.data.head[headcode]) {
						this.props.form.setAllFormValue({ [headcode]: res.data.head[headcode] }, true, false, key);
					}
					//表体赋值
					if (typeof bodycode === 'string') {
						tableTypeObj[bodycode] = 'cardTable';
						if (res.data && res.data.body && res.data.body[bodycode]) {
							this.props.cardTable.setTableData(bodycode, res.data.body[bodycode], false);
						}
					} else if (bodycode instanceof Array) {
						bodycode.forEach((code) => {
							tableTypeObj[code] = 'cardTable';
							if (res.data && res.data.bodys && res.data.bodys[code]) {
								this.props.cardTable.setTableData(code, res.data.bodys[code], false);
							}
						});
					} else if (Object.prototype.toString.call(bodycode) === "[object Object]") {
						if (Object.keys(bodycode).length > 1) {
							Object.keys(bodycode).forEach((code) => {
								tableTypeObj[code] = 'cardTable';
								if (res.data && res.data.bodys && res.data.bodys[code]) {
									this.props.cardTable.setTableData(code, res.data.bodys[code], false);
								}
							})
						} else if (Object.keys(bodycode).length == 1) {
							let code = Object.keys(bodycode)[0];
							tableTypeObj[code] = 'cardTable';
							if (res.data && res.data.body && res.data.body[code]) {
								this.props.cardTable.setTableData(code, res.data.body[code], false);
							}
						}
					}
				}
				if (res.formulamsg && res.formulamsg instanceof Array && res.formulamsg.length > 0) {
					this.props.dealFormulamsg(res.formulamsg, tableTypeObj);
				}
				callback();
			};
			//发送查询关联项事件
			if (url && data && success) {
				ajax({
					url: url,
					method: 'post',
					async:false,
					data: data,
					success: success
				});
			}
		} else {
			callback();
		}
	};
