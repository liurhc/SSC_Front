import React, { Component } from 'react';
import { base, getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../ReferLoader';
const { NCSelect } = base;
const NCOption = NCSelect.NCOption;
let NCOptionData = [];
let contactO = {};
let width = '';
let billType;
let defaultDisplay;

class DropList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			linkData: {
				contactObj: { "display": "", "value": "" },
				objName: { "display": "", "value": "" }     //对象名称
			},
			json: {}
		};
	}

	componentWillMount() {

		let callback = (json) => {
			this.setState({ json: json }, () => {
				let { linkData } = this.state;
				let { contactObj } = linkData;
				billType = this.props.billType;
				if (billType == 'F1') {
					contactObj.display = this.state.json['public-000063'];/* 国际化处理： 供应商*/
					contactObj.value = '1';
					this.setState({
						linkData
					}, () => {
						contactO = this.state.linkData.contactObj;
					})
				} else {
					contactObj.display = this.state.json['public-000064'];/* 国际化处理： 客户*/
					contactObj.value = '0';
					this.setState({
						linkData
					}, () => {
						contactO = this.state.linkData.contactObj;
					})
				}
			});
		}
		getMultiLang({ moduleId: 'public', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	componentDidMount() {
		NCOptionData = this.props.NCOptionData;
		width = Number(this.props.width);
		defaultDisplay = this.props.defaultDisplay;
	}

	render() {
		let { linkData } = this.state;
		let { contactObj, objName } = linkData;
		const NCOptions = NCOptionData.map(province => (
			<NCOption value={province}>{province.display}</NCOption>
		));
		
		return (
			<div className='pub-droplist'>
				<span className='lable'>{this.props.tit}</span>
				<div className="pub-droplist-cont">
					<NCSelect
						defaultValue={contactObj.display && contactObj.display}
						style={{ width: width, marginRight: 6 }}
						onChange={(v) => {
							contactObj.display = v.display;
							contactObj.value = v.value;
							objName = { display: null, value: null };
							this.setState({
								linkData
							}, () => {
								this.props.acceptData(linkData);
								contactObj = linkData.contactObj;
							})
						}}
					>
						{NCOptions}
					</NCSelect>
				</div>
			</div>
		)
	}
}


class DifferentRefer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			linkData: {
				objName: { "display": "", "value": "" }     //对象名称
			}
		};
	}

	componentDidMount() {
		;
		let { linkData } = this.state;
		this.state.linkData.objName = this.props.objName;
		this.setState({
			linkData
		});
	}

	//参照展示
	getRefer = (tag, refcode, key, mainData, name, showUnitFlg) => {
		let WholeVerifyDatas = this.props.WholeVerifyDatas;
		return (
			<Referloader
				tag={tag}
				refcode={refcode}
				value={{
					refname: key.display,
					refpk: key.value,
				}}
				onChange={(v) => {
					key.value = v.refpk;
					key.display = v.refname;
					this.setState({
						mainData
					}, () => {
						this.props.acceptData(mainData);
					})
				}}
				placeholder={key.display || name}
				isShowUnit={showUnitFlg}
				//单元组织的条件过滤
				unitCondition={() => {
					if (tag == "DeptTreeRef") {
						return {
							'pkOrgs': WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value,
							'TreeRefActionExt': 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						}
					} else if (tag == "PsndocTreeGridRef") {
						return {
							'pkOrgs': WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value,
							'TreeRefActionExt': 'nccloud.web.arap.ref.before.OrgSqlBuilder'
						}
					}
				}}
				queryCondition={() => {
					if (tag == "SupplierRefTreeGridRef") {
						return {
							'DataPowerOperationCode': 'fi',//使用权组
							'isDataPowerEnable': 'Y',
							'pk_org': WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value
						}
					} else if (tag == "DeptTreeRef") {
						return {
							'busifuncode': 'all',
							'DataPowerOperationCode': 'fi',//使用权组
							'isDataPowerEnable': 'Y',
							'pk_org': WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value
						}
					} else if (tag == "PsndocTreeGridRef") {
						return {
							'busifuncode': 'all',
							'DataPowerOperationCode': 'fi',//使用权组
							'isDataPowerEnable': 'Y',
							'pk_org': WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value
						}
					} else if (tag == "CustomerDefaultTreeGridRef") {
						return {
							'DataPowerOperationCode': 'fi',//使用权组
							'isDataPowerEnable': 'Y',
							'pk_org': WholeVerifyDatas.pk_org == null ? null : WholeVerifyDatas.pk_org.value
						}
					}
				}}
			/>
		)
	}

	//对象名称显示不同参照
	getDifferentRefer = () => {
		let { linkData } = this.state;
		let { objName } = linkData;
		
		switch (contactO.value) {
			case '0':
				return (
					this.getRefer('CustomerDefaultTreeGridRef', 'uapbd/refer/customer/CustomerDefaultTreeGridRef/index.js', objName, linkData, this.state.json['public-000064'], false)/* 国际化处理： 客户*/
				)
				break;
			case '1':
				return (
					this.getRefer('SupplierRefTreeGridRef', 'uapbd/refer/supplier/SupplierRefTreeGridRef/index.js', objName, linkData, this.state.json['public-000063'], false)/* 国际化处理： 供应商*/
				)
				break;
			case '2':
				return (
					this.getRefer('DeptTreeRef', 'uapbd/refer/org/DeptTreeRef/index.js', objName, linkData, this.state.json['public-000065'], true)/* 国际化处理： 部门*/
				)
				break;
			case '3':
				return (
					this.getRefer('PsndocTreeGridRef', 'uapbd/refer/psninfo/PsndocTreeGridRef/index.js', objName, linkData, this.state.json['public-000066'], true)/* 国际化处理： 业务员*/
				)
				break;
			default:
				break;
		}
	}

	render() {
		return (
			<div className="pub-dreder">
				<span className='lable'>{this.props.tit}</span>
				<div className="pub-dreder-cont">
					{this.getDifferentRefer()}
				</div>
			</div>
		)
	}
}

export { DropList, DifferentRefer }
