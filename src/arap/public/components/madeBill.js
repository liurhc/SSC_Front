import { ajax, cacheTools } from 'nc-lightapp-front';

/**
 * 制单   
 * @author xiejhk
 * @param {*} madeData 制单条件 
 * @param {*} pk_bill  主键编码   
 * @param {*} appcode 小应用编码
 * @param {*} list 是否列表  批量
 */
export default function madeBill(that,props, madeData,pk_bill, appcode,list) {
  let madeDatas = [];
  if (list) {
    madeData.forEach((madeBillData) => {
      madeDatas.push({
        pk_bill: madeBillData.data.values[pk_bill].value,
        billType: madeBillData.data.values.pk_billtype.value,
        tradeType: madeBillData.data.values.pk_tradetype.value
      });
    });
  }else{
    madeDatas = madeData;
  }
  ajax({
    url: '/nccloud/arap/arappub/madebill.do',
    data: madeDatas,
    success: (res) => {
      if (res.success) {
        //打开结算制单节点
        cacheTools.set(appcode + '_MadeBill2019', res.data.pklist);
        props.openTo(res.data.url,
          {
            status: 'edit',
            n:that.state.json['public-000233'],//'凭证生成'
            appcode: res.data.appcode,
            pagecode: res.data.pagecode,
            scene: appcode + '_MadeBill2019'
          });
      }
    }
  });
}
