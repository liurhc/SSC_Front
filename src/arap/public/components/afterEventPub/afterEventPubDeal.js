import { ajax, base ,toast} from 'nc-lightapp-front';
import { calculateHeadMoney, refreshChildVO2HeadVO } from '../../components/pubUtils/billPubUtil.js';

//编辑公式提示信息框框
let formulamsgHint = function(that, res) {
	let tableTypeObj = {};
	tableTypeObj[that.tableId] = 'cardTable';
	if (res.formulamsg && res.formulamsg instanceof Array && res.formulamsg.length > 0) {
		that.props.dealFormulamsg(res.formulamsg, tableTypeObj);
	}
};

//渲染界面数据
let renderData = function(that, res) {
	if (res.data) {
		if (res.data.head) {
			that.props.form.setAllFormValue({ [that.formId]: res.data.head[that.formId] });
		}
		if (res.data.body) {
			that.props.cardTable.updateDataByRowId(that.tableId, res.data.body[that.tableId]);
		}
	}
};

/**
 * 表头字段编辑后事件，渲染整个表头和改动的表体字段
 * @param {*} that 
 * @param {*} res 
 */
let headAfterEventRenderData = function (that, res){
	if (res.data) {
		if (res.data.head) {
			that.props.form.setAllFormValue({ [that.formId]: res.data.head[that.formId] });
		}
		if (res.data.body) {
			that.props.cardTable.updateDataByRowId(that.tableId, res.data.body[that.tableId]);
		}
		if(res.data.body){
			refreshChildVO2HeadVO(that.props, that.formId, that.tableId)
		}
		calculateHeadMoney(that)
	}
}

/**
 * 表体字段编辑后事件渲染数据
 * @param {*} that 
 * @param {*} res 
 */
let bodyAfterEventRenderData = function (that, res){
	if (res.data) {
		if (res.data.head) {
			that.props.form.setAllFormValue({ [that.formId]: res.data.head[that.formId] });
		}
		if (res.data.body) {
			that.props.cardTable.updateDataByRowId(that.tableId, res.data.body[that.tableId]);
		}
		calculateHeadMoney(that)
	}
}

//错误处理信息
let errorDeal = function(that, res,changedrows) {
	let str = String(res);
	let content = str.substring(6, str.length);
	that.props.form.setFormItemsValue(that.formId, { pk_org: changedrows });
	toast({ color: 'danger', content: content });
};

export { formulamsgHint, renderData, errorDeal, headAfterEventRenderData, bodyAfterEventRenderData };
