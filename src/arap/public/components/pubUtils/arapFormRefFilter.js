
import { getBusinessInfo, ajax } from "nc-lightapp-front";


/**
 * 卡片态表头字段过滤 以及 代码控制可编辑性
 * @param {*} moduleId 区域id
 * @param {*} props 当前props
 * @param {*} key 操作的键
 * @param {*} data 当前表单所有值
 */
function formBeforeEvent(props, moduleId, key, value) {
    let flag = true; //用来控制单元格是否可操作
    let meta = props.meta.getMeta();
    let tradeType = props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? props.form.getFormItemsValue(this.formId, 'pk_tradetype').value : null;

    //支持网专项问题修改begin
    let formRelation = meta.formrelation[moduleId];
    let areas = [];
    if (formRelation.length > 0) {
        for (let j = 0; j < formRelation.length; j++) {
            if (formRelation[j] && formRelation[j].substr(0, 4) == 'head') {
                areas.push(formRelation[j]);
            }
        }
    }
    areas.push(moduleId);
    //支持网专项问题修改end
    let formObject = {
        model: props.form.getAllFormValue(this.formId),
        pageid: this.pageId
    }
    let allVisibleRows = props.cardTable.getVisibleRows(this.tableId);
    var tableObject = {
        model: {
            areaType: "table",
            areacode: null,
            rows: [allVisibleRows[0]]
        },
        pageid: this.pageId
    }
    let formData = JSON.stringify(formObject);
    let tableData = JSON.stringify(tableObject);
    var config = {
        itemKey: key,
        isHead: true,
        crossRuleConditionsVO: formData,//表单的内容
        crossRuleTableConditionsVO: tableData,//表体第一行的数据
        tradeType: tradeType,//交易类型
        DataPowerOperationCode: '',//使用权
        isDataPowerEnable: 'Y',//使用权限
        pk_org: '',
        busifuncode: '',//部门参照参数
        AppCode: '',
        orgType: '',//成本中心
        refnodename: '',//参照显示名称
        accclass: '',//供应商或客户标识
        pk_cust: '',//供应商或客户信息
        pk_currtype: '',//货币类型
        pkOrgs: '',
        parentbilltype: '',
        datestr: '',//业务日期
        pk_accountingbook: '',//会计主账簿的id
        GridRefActionExt: "",
        TreeRefActionExt: "",
        isadj: '',//开始摊销期间
        pk_psndoc: '',//个人银行账户
    }
    var billType = props.form.getFormItemsValue(this.formId, 'pk_billtype') ? props.form.getFormItemsValue(this.formId, 'pk_billtype').value : null;
    for (let m = 0; m < areas.length; m++) {
        meta[areas[m]].items.map((item) => {
            item.isShowUnit = false;
            item.isShowDisabledData = false;

            var attrcode = item.attrcode;
            //应付类型和付款类型 往来对象去除客户
            if (attrcode == 'objtype') {
                var index = -1;
                for (var i = 0; i < item.options.length; i++) {
                    if (item.options[i].value == '') {
                        index = i;
                        break;
                    }
                }
                if (index > -1) {
                    item.options.splice(index, 1);
                }
            }
            if (attrcode == 'objtype' && attrcode == key && (billType == 'F1' || billType == 'F3' || billType == "23E1")) {
                if (value.value == "0") {
                    flag = false;
                    props.form.setFormItemsDisabled(moduleId, {
                        'objtype': true
                    });
                    return flag
                } else {
                    var index = -1;
                    for (var i = 0; i < item.options.length; i++) {
                        if (item.options[i].value == '0') {
                            index = i;
                            break;
                        }
                    }
                    if (index > -1) {
                        item.options.splice(index, 1);
                    }
                }
            } else if (attrcode == 'objtype' && attrcode == key && (billType == 'F0' || billType == 'F2' || billType == "23E0")) {
                if (value.value == "1") {
                    flag = false;
                    props.form.setFormItemsDisabled(moduleId, {
                        'objtype': true
                    });
                    return flag
                } else {
                    var index = -1;
                    for (var i = 0; i < item.options.length; i++) {
                        if (item.options[i].value == '1') {
                            index = i;
                            break;
                        }
                    }
                    if (index > -1) {
                        item.options.splice(index, 1);
                    }
                }
            }
            if (item.itemtype == 'refer') {
                item.isMultiSelectedEnabled = false;
                item.unitValueIsNeeded = false; //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
            }
            if (attrcode == key) {
                switch (attrcode) {
                    case 'pk_org'://财务组织(根据集团过滤)
                        item.queryCondition = (p) => {
                            item.isShowUnit = false;
                            config.DataPowerOperationCode = 'fi';
                            config.AppCode = props.getSearchParam('c');
                            config.data = '';
                            config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                            return config;
                        }
                        break;
                    case 'pk_org_v'://财务组织版本(根据集团过滤)
                        item.isShowUnit = false;
                        item.queryCondition = (p) => {
                            item.isShowUnit = false;

                            config.data = '';
                            config.crossRuleConditionsVO = '';
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                                return config;
                            } else {
                                config.DataPowerOperationCode = 'fi';
                                return config;
                            }
                        }
                        break;
                    case 'supplier'://供应商
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.DataPowerOperationCode = 'fi';
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            //付款单和应付单
                            return config;
                        }
                        break;
                    case 'customer'://客户
                        item.queryCondition = (p) => {
                            //收款单和应收单
                            if (p) {
                                if (p.refType == 'grid') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.tradeType = tradeType;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null
                            return config;
                        }
                        break;
                    case 'pk_deptid'://部门(根据财务组织过滤)
                        item.isShowUnit = true
                        item.unitCondition = () => {
                            return {
                                pkOrgs: props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null,
                                TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                            }
                        }
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.DataPowerOperationCode = 'fi';
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.busifuncode = 'all';
                            return config;
                        }
                        break;
                    case 'pk_deptid_v'://部门版本(根据财务组织过滤)
                        item.isShowUnit = true
                        item.unitCondition = () => {
                            return {
                                isDataPowerEnable: 'N',
                                pkOrgs: props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null,
                                TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                            }
                        }
                        item.queryCondition = (p) => {
                            config.busifuncode = 'all';
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                            if (p) {
                                if (p.refType == 'grid') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleDepSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleDepSqlBuilder';
                                } else if (p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleDepSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleDepSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'pk_psndoc'://业务员(根据部门+组织过滤)
                        item.isShowUnit = true
                        item.unitCondition = () => {
                            return {
                                pkOrgs: props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null,
                                TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                            }
                        }
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.busifuncode = 'all';
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            return config
                        }
                        break;
                    case 'pk_fiorg'://废弃财务组织(根据集团)
                        item.queryCondition = (p) => {
                            item.isShowUnit = false;
                            config.DataPowerOperationCode = 'fi';
                            config.AppCode = props.getSearchParam('c');
                            config.data = '';
                            config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                            return config;
                        }
                        break;
                    case 'pk_fiorg_v'://废弃财务组织版本(根据集团)
                        item.isShowUnit = false;
                        item.queryCondition = (p) => {
                            item.isShowUnit = false;
                            config.AppCode = props.getSearchParam('c');
                            config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                            config.data = '';
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                                return config;
                            } else {
                                config.DataPowerOperationCode = 'fi';
                                return config;
                            }
                        }
                        break;
                    case 'sett_org'://结算财务组织(根据集团) 且根据财务组织联动
                        item.queryCondition = (p) => {
                            item.isShowUnit = false;
                            config.DataPowerOperationCode = 'fi';
                            config.AppCode = props.getSearchParam('c');
                            config.data = '';
                            config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                            return config;
                        }
                        break;
                    case 'sett_org_v'://结算财务组织版本(根据集团)且根据财务组织联动
                        item.isShowUnit = false;
                        item.queryCondition = (p) => {
                            item.isShowUnit = false;
                            config.AppCode = props.getSearchParam('c');
                            config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                            config.data = '';
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                                return config;
                            } else {
                                config.DataPowerOperationCode = 'fi';
                                return config;
                            }
                        }
                        break;
                    case 'pu_deptid'://业务部门(根据组织)
                        item.isShowUnit = false;
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.busifuncode = 'all';
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pu_org') ? props.form.getFormItemsValue(this.formId, 'pu_org').value : null;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1") {
                                //付款单和应付单
                                config.DataPowerOperationCode = 'fi';
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'pu_deptid_v'://业务部门版本(根据组织)
                        item.isShowUnit = false;
                        item.queryCondition = (p) => {
                            config.busifuncode = 'all';
                            if (p) {
                                if (p.refType == 'grid') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pu_org') ? props.form.getFormItemsValue(this.formId, 'pu_org').value : null;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1") {
                                //付款单和应付单
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }

                        }
                        break;
                    case 'pu_org_v'://业务单元版本(财务组织委托)
                        item.queryCondition = (p) => {
                            item.isShowUnit = false;
                            config.pkOrgs = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            }
                            config.itemKey = key;
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'pu_psndoc'://业务人员(根据组织\业务部门)@auto zhangygw 
                        item.isShowUnit = false
                        item.queryCondition = (p) => {
                            config.busifuncode = 'all';
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pu_org') ? props.form.getFormItemsValue(this.formId, 'pu_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                //付款单和应付单
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'checkelement'://责任核算要素
                        var pk_pcorg = props.form.getFormItemsValue(this.formId, 'pk_pcorg') ? props.form.getFormItemsValue(this.formId, 'pk_pcorg').value : null;
                        if (!pk_pcorg) {
                            flag = false;
                            break;
                        }
                        item.queryCondition = (p) => {
                            let pk_pcorg = props.form.getFormItemsValue(this.formId, 'pk_pcorg') ? props.form.getFormItemsValue(this.formId, 'pk_pcorg').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (!pk_pcorg) {
                                //设置责任核算要素不可编辑
                                props.form.setFormItemsDisabled(this.formId, { 'checkelement': true });
                            } else {
                                //设置责任核算要素可编辑
                                props.form.setFormItemsDisabled(this.formId, { 'checkelement': false });
                                config.DataPowerOperationCode = 'fi';
                                config.pk_org = pk_pcorg;
                                return config;
                            }
                        }
                        break;
                    case 'cashitem'://现金流量项目(根据组织)
                        item.queryCondition = (p) => {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            return config;
                        }
                        break;
                    case 'pk_subjcode'://收支项目(根据组织)
                        item.queryCondition = (p) => {
                            config.DataPowerOperationCode = 'fi',//使用权组
                                config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'costcenter'://成本中心(根据利润中心) 
                        item.queryCondition = (p) => {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_pcorg') ? props.form.getFormItemsValue(this.formId, 'pk_pcorg').value : null;
                            config.orgType = 'pk_profitcenter';
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CostCenterSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CostCenterSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CostCenterSqlBuilder';
                            }
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'payaccount'://付款银行账户
                        var objtypeValue = props.form.getFormItemsValue(this.formId, 'objtype').value;
                        if (billType == 'F0' || billType == 'F2' || billType == "23E0") {
                            if (objtypeValue == "1") {
                                item.itemType = 'refer'
                                item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                                item.refName = this.state.json['public-000147']/* 国际化处理： 供应商银行账户*/
                            } else if (objtypeValue == "3") {
                                item.itemType = 'refer'
                                item.refcode = 'uapbd/refer/pubinfo/PsnbankaccGridRef/index'
                                item.refName = this.state.json['public-000148']/* 国际化处理： 个人银行账户*/
                            } else {
                                item.itemType = 'refer'
                                item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                                item.refName = this.state.json['public-000149']/* 国际化处理： 客户银行账户*/
                            }
                        }

                        item.queryCondition = (p) => {
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "23E1") {
                                //付款单和应付单
                                config.DataPowerOperationCode = 'fi';//使用权组
                                config.refnodename = this.state.json['public-000150'];/* 国际化处理： 使用权参照*/
                                config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                                config.pk_billtype = props.form.getFormItemsValue(this.formId, 'pk_billtype') ? props.form.getFormItemsValue(this.formId, 'pk_billtype').value : null;
                                config.pk_currtype = props.form.getFormItemsValue(this.formId, 'pk_currtype') ? props.form.getFormItemsValue(this.formId, 'pk_currtype').value : null;
                                if (p) {
                                    if (p.refType == 'grid' || p.refType == 'gridTree') {
                                        config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                                    } else if (p.refType == 'tree') {
                                        config.TreeRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                                    }
                                } else {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                                }
                                config.itemKey = key;
                                return config;
                            } else {
                                let supplier = props.form.getFormItemsValue(this.formId, 'supplier') ? props.form.getFormItemsValue(this.formId, 'supplier').value : null;
                                let customer = props.form.getFormItemsValue(this.formId, 'customer') ? props.form.getFormItemsValue(this.formId, 'customer').value : null;
                                let psndoc = props.form.getFormItemsValue(this.formId, 'pk_psndoc') ? props.form.getFormItemsValue(this.formId, 'pk_psndoc').value :
                                    getBusinessInfo().userId;
                                let pk_cust = null;
                                let accclass = null;
                                if (supplier) {
                                    pk_cust = supplier;
                                    accclass = 3;
                                } else if (customer) {
                                    pk_cust = customer;
                                    accclass = 1;
                                } else if (psndoc) {
                                    config.pk_psndoc = psndoc;
                                }
                                config.DataPowerOperationCode = 'fi';//使用权组
                                config.accclass = accclass;//3代表供应商
                                config.pk_cust = pk_cust;
                                config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                                config.pk_currtype = props.form.getFormItemsValue(this.formId, 'pk_currtype') ? props.form.getFormItemsValue(this.formId, 'pk_currtype').value : null;
                                var objtypeValue = props.form.getFormItemsValue(this.formId, 'objtype').value;
                                if (objtypeValue == "3") {
                                    config.pk_psndoc = psndoc;
                                    config.pk_cust = null;
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccPersonSqlBuilder';
                                } else if (p) {
                                    if (p.refType == 'grid' || p.refType == 'gridTree') {
                                        config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                    } else if (p.refType == 'tree') {
                                        config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                    }
                                } else {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                                config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                config.itemKey = key;
                                return config;
                            }
                        }
                        break;
                    case 'recaccount'://收款银行账户(1代表客户，2代表客商，3代表供应商) 客商占定 @auto zhangyg --- ok
                        if (billType == 'F1' || billType == 'F3' || billType == "23E1") {
                            let objtypeValue = props.form.getFormItemsValue(this.formId, 'objtype').value;
                            if (objtypeValue == "1" || objtypeValue == "2") {
                                item.itemType = 'refer'
                                item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                                item.refName = this.state.json['public-000147']/* 国际化处理： 供应商银行账户*/
                            } else if (objtypeValue == "3") {
                                item.itemType = 'refer'
                                item.refcode = 'uapbd/refer/pubinfo/PsnbankaccGridRef/index'
                                item.refName = this.state.json['public-000148']/* 国际化处理： 个人银行账户*/
                            } else {
                                item.itemType = 'refer'
                                item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                                item.refName = this.state.json['public-000149']/* 国际化处理： 客户银行账户*/
                            }
                        }
                        item.queryCondition = (p) => {
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                //收款单和应收单
                                config.DataPowerOperationCode = 'fi';//使用权组
                                config.refnodename = this.state.json['public-000150'];/* 国际化处理： 使用权参照*/
                                config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                                config.pk_billtype = props.form.getFormItemsValue(this.formId, 'pk_billtype') ? props.form.getFormItemsValue(this.formId, 'pk_billtype').value : null;
                                config.pk_currtype = props.form.getFormItemsValue(this.formId, 'pk_currtype') ? props.form.getFormItemsValue(this.formId, 'pk_currtype').value : null;
                                if (p) {
                                    if (p.refType == 'grid' || p.refType == 'gridTree') {
                                        config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                                    } else if (p.refType == 'tree') {
                                        config.TreeRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                                    }
                                } else {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                                }
                                config.itemKey = key;
                                return config;
                            } else {

                                let supplier = props.form.getFormItemsValue(this.formId, 'supplier') ? props.form.getFormItemsValue(this.formId, 'supplier').value : null;
                                let customer = props.form.getFormItemsValue(this.formId, 'customer') ? props.form.getFormItemsValue(this.formId, 'customer').value : null;
                                let psndoc = props.form.getFormItemsValue(this.formId, 'pk_psndoc') ? props.form.getFormItemsValue(this.formId, 'pk_psndoc').value :
                                    getBusinessInfo().userId;
                                let pk_cust = null;
                                let accclass = null;
                                if (supplier) {
                                    pk_cust = supplier;
                                    accclass = 3;
                                } else if (customer) {
                                    pk_cust = customer;
                                    accclass = 1;
                                } else if (psndoc) {
                                    config.pk_psndoc = psndoc;
                                }
                                config.DataPowerOperationCode = 'fi';//使用权组
                                config.accclass = accclass;//3代表供应商
                                config.pk_cust = pk_cust;
                                config.pk_currtype = props.form.getFormItemsValue(this.formId, 'pk_currtype') ? props.form.getFormItemsValue(this.formId, 'pk_currtype').value : null;
                                config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                                var objtypeValue = props.form.getFormItemsValue(this.formId, 'objtype').value;
                                if (objtypeValue == "3") {
                                    config.pk_psndoc = psndoc;
                                    config.pk_cust = null;
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccPersonSqlBuilder';
                                } else if (p) {
                                    if (p.refType == 'grid' || p.refType == 'gridTree') {
                                        config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                    } else if (p.refType == 'tree') {
                                        config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                    }
                                } else {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                                config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                config.itemKey = key;
                                
                                // 付款单-收款银行账户参照过滤条件中去掉币种 -20191118
                                if(props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3"){
                                    delete config.pk_currtype;
                                }
                                
                                return config;
                            }
                        }
                        break;
                    case 'cashaccount'://现金账户
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CashBankSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CashBankSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CashBankSqlBuilder';
                            }
                            config.itemKey = key;
                            config.pk_currtype = props.form.getFormItemsValue(this.formId, 'pk_currtype') ? props.form.getFormItemsValue(this.formId, 'pk_currtype').value : null;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                                //付款单
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'rate'://本币汇率
                    case 'grouprate'://集团本币汇率
                    case 'globalrate'://全局本币汇率
                        ajax({
                            url: '/nccloud/arap/ref/ratecontrol.do',
                            async: false,
                            data: {
                                pk_org: props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null,
                                pk_currtype: props.form.getFormItemsValue(this.formId, 'pk_currtype') ? props.form.getFormItemsValue(this.formId, 'pk_currtype').value : null,
                                ratekey: key
                            },
                            success: (res) => {
                                if (res.success) {
                                    flag = res.data;
                                    setTimeout(() => {
                                        props.form.setFormItemsDisabled(this.formId, {
                                            key: !res.data
                                        });
                                    }, 0);
                                }
                            }
                        });
                        break;
                    case 'pk_balatype'://结算方式(根据pk_billtype)
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.pk_billtype = props.form.getFormItemsValue(this.formId, 'pk_billtype') ? props.form.getFormItemsValue(this.formId, 'pk_billtype').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.BalatypeSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.BalatypeSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.BalatypeSqlBuilder';
                            }
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'bankrollprojet'://资金计划项目 
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'pu_org'://业务组织(业务单元（财务组织委托）) @atuo:zhangygw ---- ok
                        item.queryCondition = (p) => {
                            config.pkOrgs = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            }
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'pk_currtype'://币种
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        }
                        break;
                    case 'pk_busitype'://业务流程
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        }
                        break;
                    case 'pk_pcorg'://利润中心
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'pk_pcorg_v'://利润中心版本
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'so_org'://业务组织(业务单元（财务组织委托）) @atuo:zhangygw ---- ok
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            }
                            config.itemKey = key;
                            config.pkOrgs = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'so_org_v'://业务单元版本(财务组织委托)
                        item.queryCondition = (p) => {
                            item.isShowUnit = false;
                            config.pkOrgs = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            }
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'so_deptid'://业务部门(根据组织)
                        item.isShowUnit = false;
                        item.queryCondition = (p) => {
                            item.isShowUnit = true;
                            config.busifuncode = 'all';
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'so_org') ? props.form.getFormItemsValue(this.formId, 'so_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                //收款单和应收单
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'so_deptid_v'://业务部门版本(根据组织) --- ok
                        item.isShowUnit = false
                        item.queryCondition = (p) => {
                            item.isShowUnit = true;
                            config.busifuncode = 'all';
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'so_org') ? props.form.getFormItemsValue(this.formId, 'so_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                //收款单和应收单
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'so_psndoc'://业务员(根据部门+组织过滤)
                        item.isShowUnit = false;
                        item.queryCondition = (p) => {
                            item.isShowUnit = true;
                            config.busifuncode = 'all';
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'so_org') ? props.form.getFormItemsValue(this.formId, 'so_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'rececountryid'://收货国
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'sendcountryid'://发货国
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'rececountryid'://收货国
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'taxcountryid'://报税国
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'approver'://审核人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'billmaker'://制单人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'confirmuser'://单据确认人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'creator'://创建人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'consignagreement'://托收协议号
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'effectuser'://生效人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'lastadjustuser'://最终调整人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'lastapproveid'://最终审批人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'modifier'://最后修改人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'officialprintuser'://正式打印人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'ordercubasdoc'://010
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'outbusitype'://外系统业务类型
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'payman'://支付人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'pk_deptid_res'://责任部门
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'pk_rescenter'://责任中心
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'sddreversaler'://直接借记退回人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'signuser'://签字人
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                        break;
                    case 'pk_tradetypeid'://交易类型
                        item.refName = this.state.json['public-000151']/* 国际化处理： 交易类型参照*/
                        item.queryCondition = (p) => {
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.parentbilltype = props.form.getFormItemsValue(this.formId, 'pk_billtype').value;
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'subjcode'://科目
                        item.queryCondition = (p) => {
                            let pkAccountingbook = null;
                            ajax({
                                url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
                                data: {
                                    pk_org: props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null,
                                },
                                async: false,
                                success: (res) => {
                                    if (res.success) {
                                        pkAccountingbook = res.data;
                                    }
                                }
                            })
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.pk_accountingbook = pkAccountingbook;
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    case 'objtype'://往来对象
                        break;
                    case 'start_period'://开始摊销期间
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.isadj = 'N';
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.itemKey = key;
                            return config;
                        }
                        break;
                    default:
                        item.isShowUnit = true
                        item.unitCondition = () => {
                            return {
                                pkOrgs: props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null,
                                TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                            }
                        }
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            return config;
                        }
                        break;
                }
            }

        });
    }
    props.meta.setMeta(meta);
    return flag; //默认单元格都可操作
}


export { formBeforeEvent }
