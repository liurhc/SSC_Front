
let billstatus = {//单据状态
    //9=未确认，-1=保存，1=审批通过，2=审批中，-99=暂存，8=签字，
    UNCOMFIRM: 9,
    SAVE: -1,
    AUDIT: 1,
    AUDITING: 2,
    TEMPEORARY: -99,
    SIGN: 8
};
let approvestatus = {//审批状态
    //-1=自由态，0=未通过态，1=通过态，2=进行中态，3=提交态， 
    NOSTATE: -1,
    NOPASS: 0,
    PASSING: 1,
    GOINGON: 2,
    COMMIT: 3
};
let effectstatus = {//生效状态
    //0=未生效，10=已生效
    EXECUTED: 10,
    INVALID: 0
}

let coordflag = {//确认标志
    //0=非确认，1=确认
    VERIFY: 1,
    NOVERIFY: 0

}

//根据状态和数据确认按钮的可见性
//status 当前页面状态，列表态传入空值
//data 当前的主表数据
//id 按钮id

let trueBtn = ['Add_blank', 'Add', 'Head_g1', 'adcode', 'paca', 'F0', 'F1', 'F2', 'F3', 'FCT1',
'FCT2', '36D1', '4A18', '4A23', '4A49', '4A24']


let listTrueBtn = ['Add_blank', 'Add', 'Head_g1', 'adcode', 'paca', 'F0', 'F1', 'F2', 'F3', 'FCT1',
'FCT2', '36D1', '4A18', '4A23', '4A49', '4A24','More','InitBuild','CancelInitBuild','ImportData','Refresh','ConnectSettleInfo']

/**
 * 
 * @param {*} status 浏览态还是编辑态，默认浏览态
 * @param {*} data 数据
 * @param {*} id 按钮id
 * @param {*} type  list 表示列表控制按钮
 */
let buttonVisible = function (status, data, id, type, that) {
    if (!data ||!(data.pk_tradetype ? data.pk_tradetype.value : null)) {
        if(type == 'list'){
            if (listTrueBtn.indexOf(id) != -1) {
                return true;
            } else {
                return false;
            }
        }else{
            if(that.props.getUrlParam('status')){
                if (trueBtn.indexOf(id) != -1) {
                    return true;
                } else {
                    return false;
                }
            }else{
                return false;
            }
           
        }
    }
    if (!status) {
        status = 'browse'//默认status为browse
    }
    let flag = true;//按钮的状态，默认为true
    let coordflagInfo = data.coordflag ? data.coordflag.value : null;//确认标志信息
    let approvestatusInfo = data.approvestatus ? data.approvestatus.value : null; //审批状态信息
    let billstatusInfo = data.billstatus ? data.billstatus.value : null;//单据状态信息
    let effectstatusInfo = data.effectstatus ? data.effectstatus.value : null;  //生效状态信息
    let isinitInfo = data.isinit ? data.isinit.value : null;//期初信息 
    let pk_org = data.pk_org ? data.pk_org.value : null;//组织信息
    let isforce = data.isforce ? data.isforce.value : false;//是否承付
    let scene;//场景信息
    switch (id) {
        /**
         * 头部按钮
         */
        case 'More':
            if (status != 'browse') {
                flag = false;
            }
            break;
        case 'CancelConfirm'://取消确认
            if (status != 'browse' || coordflagInfo == coordflag.NOVERIFY) {
                flag = false;
            }
            break;
        case 'Confirm'://确认
            if (coordflagInfo == coordflag.VERIFY) {
                flag = false;
            }
            break;
        case 'Add'://新增
            if (status != 'browse') {
                flag = false;
            }
            break;
        case 'Edit'://修改
            //单据状态为暂存和保存态可以修改
            if (status != 'browse' || !(isinitInfo == true || billstatusInfo == billstatus.TEMPEORARY || billstatusInfo == billstatus.SAVE)) {
                flag = false;
            }
            break;
        case 'Save'://保存
            if (status == 'browse') {
                flag = false;
            }
            break;
        case 'TempSave'://暂存
            //暂存态单据可暂存
            if (status == 'browse' || (status == 'edit' && billstatusInfo != billstatus.TEMPEORARY)) {
                flag = false;
            }
            break;
        case 'SaveAndCommit'://保存提交
            //浏览态或审批中不能有保存提交按钮
            if (status == 'browse' || approvestatusInfo != approvestatus.NOSTATE) {
                flag = false;
            }
            //承付单据,不等于承付中不可用
            if(type == 'card' && isforce && that.props.cardTable.getValByKeyAndIndex(that.tableId, 0, 'commpaystatus')
                && that.props.cardTable.getValByKeyAndIndex(that.tableId, 0, 'commpaystatus').value != '1'){
                flag = false
            }
            break;
        case 'Delete'://删除
            //浏览态 审批状态为自由态 
            if (status != 'browse' || !(isinitInfo == true || approvestatusInfo == approvestatus.NOSTATE)) {
                flag = false;
            }
            break;
        case 'Refresh'://刷新
            if (status != 'browse') {
                flag = false;
            }
            break;
        case 'Cancel'://取消
            if (status == 'browse') {
                flag = false;
            }
            break;
        case 'Copy'://复制
            if (status != 'browse') {
                flag = false;
            }
            break;
        case 'Commit'://提交
            //自由态单据且单据状态为保存态单据
            if (status != 'browse' || !(approvestatusInfo == approvestatus.NOSTATE && billstatusInfo == billstatus.SAVE)) {
                flag = false;
            }
            //承付单据提交按钮判断
            // if(type == 'card' && isforce && that.props.cardTable.getValByKeyAndIndex(that.tableId, 0, 'commpaystatus')
            //     && that.props.cardTable.getValByKeyAndIndex(that.tableId, 0, 'commpaystatus').value == '0'){
            //     flag = false
            // }
            break;
        case 'Uncommit'://收回
            if (that) {
                scene = that.props.getUrlParam('scene')
            }
            //所有场景提交或者已经生效有收回，默认的审批通过也有收回,报账场景走默认
            if (scene && scene!='bz') {
                if (status != 'browse' || !(approvestatusInfo == approvestatus.COMMIT || effectstatusInfo == effectstatus.EXECUTED)) {
                    flag = false;
                }
            } else {
                if (status != 'browse' || !(approvestatusInfo == approvestatus.COMMIT ||
                    approvestatusInfo == approvestatus.PASSING || effectstatusInfo == effectstatus.EXECUTED)) {
                    flag = false;
                }
            }
            break;
        case 'Approve'://审批
            if (status != 'browse' || effectstatusInfo == effectstatus.EXECUTED) {
                flag = false;
            }
            break;
        case 'UnApprove'://取消审批取消审批
            if (status != 'browse' || effectstatusInfo == effectstatus.INVALID) {
                flag = false;
            }
            break;
        case 'MadeBill'://制单
            //生效状态为已生效
            if (status != 'browse' || !(effectstatusInfo == effectstatus.EXECUTED)) {
                flag = false;
            }
            break;
        case 'BodyVerify'://按表体核销
            //单据状态为暂存无法核销
            if (status != 'browse' || billstatusInfo == billstatus.TEMPEORARY) {
                flag = false;
            }

            break;
        case 'WholeVerify'://按整单核销
            //单据状态为暂存无法核销
            if (status != 'browse' || billstatusInfo == billstatus.TEMPEORARY) {
                flag = false;
            }
            break;
        case 'RedBack'://红冲
            //生效状态为已生效
            if (status != 'browse' || !(effectstatusInfo == effectstatus.EXECUTED)) {
                flag = false;
            }
            break;
        case 'AttachManage'://附件管理
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'CommisionPay'://承付
            if (status != 'browse' || !isforce) {
                flag = false
            }
            break
        case 'CancelCommisionPay'://取消承付
            if (status != 'browse' || !isforce) {
                flag = false
            }
            break
        case 'Receipt'://影像管理
            if (status != 'browse') {
                flag = false;
            }
            break;
        case 'ReceiptCheck'://影像查看
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'ReceiptScan'://影像扫描  
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'InvoiceUploader'://上传电子发票 
            if (status != 'browse'|| approvestatusInfo == approvestatus.PASSING) {
                flag = false
            }
            break;
        case 'BillLinkQuery'://联查单据
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'LinkBal'://联查余额表
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'LinkDeal'://联查处理情况
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'LinkVouchar'://联查凭证
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'LinkConfer'://联查协同该单据
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'LinkTerm'://联查收付款协议
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'LinkTbb'://联查计划预算
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'LinkInformer'://到账通知
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'LinkSettleInfo'://结算信息
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'LinkAprv'://审批详情 
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'ImportData'://导入
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'ExportData'://导出
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'Print'://打印
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'Output'://输出
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'OfficalPrint'://正式打印
            if (status != 'browse') {
                flag = false
            }
            break;
        case 'CancelPrint'://取消正式打印
            if (status != 'browse') {
                flag = false
            }
            break;

        /**
         * 以下是卡片界面的肩部按钮
         */

        case 'Pausetrans'://挂起
            //生效状态为已生效的才可以过去和取消挂起
            if (status != 'browse' || !(effectstatusInfo == effectstatus.EXECUTED)) {
                flag = false;
            }
        case 'Cancelpause'://取消挂起
            if (status != 'browse' || !(effectstatusInfo == effectstatus.EXECUTED)) {
                flag = false;
            }
            break;
        case 'PrePay'://预收付
            //生效状态为已生效的才可以预收付
            if (status != 'browse' || !(effectstatusInfo == effectstatus.EXECUTED)) {
                flag = false;
            }
            break;
        case 'AddLine'://增行
            if (status == 'browse' ) {
                flag = false;
            }
            break;
        case 'DelLine'://删除行
            if (status == 'browse' ) {
                flag = false;
            }
            break;
        case 'CopyLine'://复制行
            if (status == 'browse' ) {
                flag = false;
            }
            break;
        case 'PasteToEndLine'://粘行到末尾
           // if (status == 'browse' ) {
                flag = false;
            //}
            break;
        case 'CancelLine'://肩部取消
           // if (status == 'browse') {
                flag = false;
            //}
            break;
        /**
         * 表体行按钮
         */
        case 'Approve_inner'://审批
            if (status != 'browse' || effectstatusInfo == effectstatus.EXECUTED) {
                flag = false;
            }
            break;
        case 'UnApprove_inner'://取消审批取消审批
            if (status != 'browse' || effectstatusInfo == effectstatus.INVALID) {
                flag = false;
            }
            break;
        case 'MadeBill_inner'://制单
            //生效状态为已生效
            if (status != 'browse' || !(effectstatusInfo == effectstatus.EXECUTED)) {
                flag = false;
            }
            break;
        case 'Edit_inner': //修改行按钮
            //单据状态为暂存和保存态可以修改
            if (status != 'browse' || !(isinitInfo == true || billstatusInfo == billstatus.TEMPEORARY || billstatusInfo == billstatus.SAVE)) {
                flag = false;
            }
            break;
        case 'Delete_inner': //删除行按钮
            //浏览态 审批状态为自由态
            if (status != 'browse' || !(isinitInfo == true || approvestatusInfo == approvestatus.NOSTATE)) {
                flag = false;
            }
            break;
        case 'Copy_inner'://复制行按钮
            if (status != 'browse') {
                flag = false;
            }
            break;
        case 'CancelConfirm_inner'://取消确认行按钮
            if (coordflagInfo == coordflag.NOVERIFY) {
                flag = false;
            }
            break;
        case 'Confirm_inner'://确认行按钮
            if (coordflagInfo == coordflag.VERIFY) {
                flag = false;
            }
            break;
        case 'Commit_inner'://提交
            //自由态单据且单据状态为保存态单据
            if (status != 'browse' || !(approvestatusInfo == approvestatus.NOSTATE && billstatusInfo == billstatus.SAVE)) {
                flag = false;
            }
            break;
        case 'Uncommit_inner'://收回
            if(that){
                scene = that.props.getUrlParam('scene')
            }
            //所有场景提交或者已经生效有收回，默认的审批通过也有收回
            if(scene && scene!='bz' ){
                if(status != 'browse' || !(approvestatusInfo == approvestatus.COMMIT ||effectstatusInfo == effectstatus.EXECUTED)){
                    flag = false;
                }
            }else{
                if(status != 'browse' || !(approvestatusInfo == approvestatus.COMMIT || 
                    approvestatusInfo == approvestatus.PASSING ||effectstatusInfo == effectstatus.EXECUTED)){
                    flag = false;
                }
            }
            break;
        case 'open_browse'://切换视图
            if (status != 'browse') {
                flag = false;
            }
            break;
        case 'open_edit'://弹窗编辑
            if (status == 'browse') {
                flag = false;
            }
            break;

        default:
            break;


    }
    return flag;
}

//获取到所有的btn
//inBtn 传入的按钮
//outBtenKey 所有的不属于行按钮的key
let getButtonsKey = function (inBtn, outBtnKey) {
    for (let i = 0; i < inBtn.length; i++) {
        if (inBtn[i].area != 'list_inner' && inBtn[i].area != 'card_inner') {
            if (inBtn[i].children.length == 0) {
                outBtnKey.push(inBtn[i].key);
            } else {
                outBtnKey.push(inBtn[i].key);
                getButtonsKey(inBtn[i].children, outBtnKey);
            }
        }
    }
    return outBtnKey;
}

//获取所有的inner
let getInnerButtonkey = function (buttons) {
    let innerButtons = [];
    for (let i = 0; i < buttons.length; i++) {
        if (buttons[i].area == 'list_inner' || buttons[i].area == 'card_inner') {
            innerButtons.push(buttons[i].key)
        }
    }
    return innerButtons;

}

//获取卡片界面的所有的肩部按钮和行按钮用于控制卡片行和肩部按钮的可见性
let cardGetbodyAndInnerbutton = function (buttons) {
    let bodyAndInnerbutton = [];
    for (let i = 0; i < buttons.length; i++) {
        if (buttons[i].area == 'card_body' || buttons[i].area == 'card_inner') {
            bodyAndInnerbutton.push(buttons[i].key)
        }
    }
    return bodyAndInnerbutton;

}

/**单据列表对按钮的控制 */
let onListButtonControl = function(that){
    let selectedData = that.props.table.getCheckedRows(that.tableId);
    if (selectedData.length == 1) {
        dealButtons(selectedData[0].data.values,that);
    } else if (selectedData.length > 1) {
        that.props.button.setButtonDisabled(that.Info.allButtonsKey, false);
    } else {
        dealButtons(null,that);
    }
}

let dealButtons = (record,that) => {
    let trueBtn = []; //可见的按钮
    let falseBtn = []; //不可见的按钮
    for (let i = 0; i < that.Info.allButtonsKey.length; i++) {
        let flag = buttonVisible(null, record, that.Info.allButtonsKey[i],'list',that);
        if (flag) {
            trueBtn.push(that.Info.allButtonsKey[i]);
        } else {
            falseBtn.push(that.Info.allButtonsKey[i]);
        }
    }
    that.props.button.setButtonDisabled(trueBtn, false);
    that.props.button.setButtonDisabled(falseBtn, true);
};


/**
 * 卡片点击行
 * 浏览态控制按钮状态,主要控制挂起，取消挂起和预收付按钮
 * 编辑态控制按钮状态
 */
let onSelectedCardBodyEditControl = function(that){
    let status = that.props.getUrlParam('status');
    if (!status) {
        status = 'browse';
    }
    if (status == 'browse') {//浏览态
        let selectedData = that.props.cardTable.getCheckedRows(that.tableId);
        if(selectedData.length == 1){
            let pausetransact = selectedData[0].data.values.pausetransact?selectedData[0].data.values.pausetransact.value:null;//挂起标志 1是  0 否
            let prepay = selectedData[0].data.values.prepay ?selectedData[0].data.values.prepay.value:null;//预收付标志0=应付款，1=预付款
            let trueBtn =[];
            let falseBtn = [];
            if(pausetransact == '0' || !pausetransact){
                trueBtn.push('Pausetrans');
                falseBtn.push('Cancelpause');
            }else if(pausetransact){
                trueBtn.push('Cancelpause');
                falseBtn.push('Pausetrans');
            }
            if(prepay == '0' || !prepay){
                trueBtn.push('PrePay');
            }else{
                falseBtn.push('PrePay');
            }
            that.props.button.setButtonDisabled(trueBtn, false);
            that.props.button.setButtonDisabled(falseBtn, true);
        }else if(selectedData.length > 1) {
            that.props.button.setButtonDisabled(['Pausetrans', 'Cancelpause','PrePay'], false);
        }else{
            that.props.button.setButtonDisabled(['Pausetrans', 'Cancelpause','PrePay'], true);
        }
    } else {//编辑态
        let head = that.props.createMasterChildData(that.props.getUrlParam('pagecode'), that.formId, that.tableId)
            .head[that.formId];
        let cardhead = head.rows[0].values;
        let pk_org = cardhead.pk_org ? cardhead.pk_org.value : null;
        if (pk_org) {
            let selectedData = that.props.cardTable.getCheckedRows(that.tableId);
            if (selectedData.length > 0) {
                that.props.button.setButtonDisabled(['CopyLine', 'DelLine'], false);
            } else {
                that.props.button.setButtonDisabled(['CopyLine', 'DelLine'], true);
            }
        } else {
            //pk_org不存在的时候，不做行按钮的处理
        }
    }
}

/**初始化卡片行的控制，主要是挂起等按钮的控制 */
let initCardBodybrowseControl = function(that){
    that.props.button.setButtonDisabled(['Pausetrans', 'Cancelpause','PrePay'], true);
}

/**初始化卡片行的控制，主要是增行等按钮的控制 */
let initCardBodyEditControl = function (props , pk_org){
    if(!pk_org){
        props.button.setButtonDisabled(['AddLine','CopyLine', 'DelLine'], true);
    }else{
        props.button.setButtonDisabled(['AddLine'], false);
        props.button.setButtonDisabled(['CopyLine', 'DelLine'], true);
    }
}

/**
 * 卡片表体行按钮的控制
 * @param {*} props 
 * @param {*} buttonfalg 
 */
let cardBodyControl = function (props, buttonfalg) {
    props.button.setButtonVisible(['PasteToEndLine', 'CancelLine'], !buttonfalg);
    if(props.getUrlParam('type') === 'transfer'){
        props.button.setButtonVisible(['CopyLine', 'DelLine'], buttonfalg);
    }else{
        props.button.setButtonVisible(['CopyLine', 'DelLine', 'AddLine'], buttonfalg);
    }
}

/**
 * 
 * @param {*} status 当前表格状态
 * @param {*} buttonfalg 当前点击按钮之后改变的状态，用于判断显示那些按钮
 */
let cardBodyAndInnerButtonVisible = function (that, buttonfalg) {
    let tableStatus = that.props.cardTable.getStatus(that.tableId);
    if (tableStatus == 'browse') {
        return ['open_browse'];//浏览态只有弹窗编辑
    } else if (tableStatus == 'edit') {
        if (buttonfalg == true) {//true
            if(that.props.getUrlParam('type') === 'transfer'){
                return ['open_edit', 'Copy_inner', 'Delete_inner']
            } else {
                return ['open_edit', 'Copy_inner', 'Insert_inner', 'Delete_inner']
            }
        } else if (buttonfalg == false) {//false
            return ['Paste_inner']
        } else {
            return [];
        }
    } else {
        return [];
    }

}



//转单场景下，部分按钮不可见
export function dealTranferBtns(falseBtn) {
    if (this.props.getUrlParam('type') === 'transfer') {
        this.setState({ tpflag: false });//设置交易类型不可见
        falseBtn.push('Add')
        falseBtn.push('TempSave')
        falseBtn.push('Copy')
        falseBtn.push('Verify')
        falseBtn.push('RedBack')
        // falseBtn.push('ConnectSettleInfo')
        // falseBtn.push('LinkInformer')
        // falseBtn.push('LinkConfer')
        // falseBtn.push('LinkAprv')
        // falseBtn.push('ImportData')
        falseBtn.push('Pause_g1')
        falseBtn.push('PrePay')
        falseBtn.push('AddLine')
        falseBtn.push('Insert_inner')
        falseBtn.push('Refresh')
    }

}

function removeArray(arr, key) {
    var index = arr.indexOf(key);
    if (index >= 0) {
        arr.splice(index, 1);
    }
}


export {
    buttonVisible, getButtonsKey, getInnerButtonkey, cardGetbodyAndInnerbutton,
    cardBodyAndInnerButtonVisible, cardBodyControl, initCardBodyEditControl, onSelectedCardBodyEditControl, initCardBodybrowseControl,
    onListButtonControl
}
