let updatePaCFV = function(that, route) {
	let data = that.props.ViewModel.getData('WholeVerifyDatas');
	let pkBill = data.pkBill;
	let prev_pc, nowv_pc, ac;
	switch (pkBill) {
		case 'pk_payablebill':
		case 'pk_paybill':
			prev_pc = '20080PREV';
			nowv_pc = '20080NOWV';
			ac = '20082002';
			break;
		case 'pk_recbill':
		case 'pk_gatherbill':
			prev_pc = '20060PREV';
			nowv_pc = '20060NOWV';
			ac = '20062002';
			break;
	}
	let tmppagecode = nowv_pc;
	if (route == '#/prev') {
		tmppagecode = prev_pc;
	}
	let param = window.parent.location.hash.split('?')[1];
	let Arr = param && param.split('&');
	if (Arr && Arr instanceof Array && Arr.length > 0) {
		let newArr = Arr.map((item) => {
			if (item.indexOf('=') != -1 && item.split('=') && item.split('=') instanceof Array) {
				if (item.split('=')[0] === 'ifr') {
					let oldIfr = decodeURIComponent(decodeURIComponent(item.split('=')[1]));
					let newIfr = oldIfr.split('#')[0] + route;
					item = `ifr=${encodeURIComponent(encodeURIComponent(newIfr))}`;
				}
				if (item.split('=')[0] === 'p') {
					item = `p=${tmppagecode}`;
				}
				if (item.split('=')[0] === 'c') {
					item = `c=${ac}`;
				}
				return item;
			}
		});
		let newParams = newArr.join('&');
		//修改工作台路径
		window.parent.location.hash = `#/ifr?${newParams}`;
	}
};

export { updatePaCFV };
