import { getBusinessInfo, ajax } from "nc-lightapp-front";

/**
 * 卡片态表头字段过滤 以及 代码控制可编辑性
 * @param {*} props 
 * @param {*} searchId 当前查询区的moduleId
 * @param {*} meta meta
 * @param {*} billType 交易类型
 * @param {*} pkOrg 财务组织主键
 */
function modifierSearchMetas(searchId, props, meta, billType, pkOrg,that) {
    
    let falg = true; //用来控制单元格是否可操作
    //let meta = props.meta.getMeta();
    meta[searchId].items.map((item) => {
        item.isShowUnit = false;
        if(item.itemtype == 'refer'){
			item.isMultiSelectedEnabled = true;
		}else{
            return true;//非参照不过滤
        }
        let attrcode = item.attrcode;
        switch (attrcode) {
            
            case 'pk_tradetype'://交易类型
                item.refName = that.state.json['public-000151']/* 国际化处理： 交易类型参照*/
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: "F0,F1,F2,F3"
                    }
                }
                break;
            case 'customer'://客户
                item.queryCondition = () => {
                    //收款单和应收单
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrg
                    };
                }
                break;
            case 'supplier'://供应商
                item.isShowDisabledData = false;
                item.queryCondition = () => {
                    //付款单和应付单
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrg
                    };

                }
                break;
            case 'pk_deptid'://部门
                item.isShowUnit = true
                item.unitCondition = () => {
                    return {
                        pkOrgs: pkOrg,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrg
                    };
                }
                break;
            case 'pk_psndoc'://业务员
                item.isShowUnit = true
                item.unitCondition = () => {
                    return {
                        pkOrgs: pkOrg,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrg,
                    };
                }
                break;
            case 'pk_costsubj'://收支项目
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrg
                    };
                }
                break;
            case 'material'://物料
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrg,
                        matcustcode: props.search.getSearchValByField(searchId, 'bodys.matcustcode') ? props.search.getSearchValByField(searchId, 'bodys.matcustcode').value.firstvalue : null,
                        GridRefActionExt: 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder'
                    };
                }
                break;
            case 'ordercubasdoc'://订单客户
                item.queryCondition = () => {
                    if (billType == "F0") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrg
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrg
                        };
                    }
                }
                break;
            case 'pk_balatype'://结算方式(根据pk_billtype)
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_billtype: billType,
                        GridRefActionExt: 'nccloud.web.arap.ref.before.BalatypeSqlBuilder',
                    };
                }
                break;
            case 'sett_org'://结算财务组织(根据集团) 且根据财务组织联动
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            // case 'productline'://产品线
            //     item.queryCondition = () => {
            //         return {
            //             DataPowerOperationCode: 'fi',//使用权组
            //             isDataPowerEnable: 'Y',
            //         };
            //     }
            //     break;
            case 'so_ordertype'://销售交易类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: billType,
                    }
                }
                break;
            case 'so_transtype'://销售渠道类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    }
                }
                break;
            case 'so_org'://业务组织(业务单元（财务组织委托）) @atuo:zhangygw ---- ok
                item.queryCondition = () => {
                    if (billType == "F0" || billType == "F2") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pkOrgs: pkOrg,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pkOrgs: pkOrg,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        };
                    }
                }
                break;
            case 'so_deptid'://业务部门(根据组织)
                item.isShowUnit = true
                item.unitCondition = () => {
                    return {
                        pkOrgs: pkOrg,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    if (billType == "F0" || billType == "F2") {
                        //收款单和应收单
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrg
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrg
                        };
                    }
                }
                break;
            case 'so_psndoc'://业务员(根据部门+组织过滤)
                item.isShowUnit = true
                item.unitCondition = () => {
                    return {
                        pkOrgs: pkOrg,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    if (billType == "F0" || billType == "F2") {
                        return {
                            busifuncode: 'all',
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrg,
                        };
                    } else {
                        return {
                            busifuncode: 'all',
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrg,
                        };
                    }
                }
                break;
            case 'subjcode'://科目
                item.queryCondition = () => {
                    let pkAccountingbook = null;
                    ajax({
                        url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
                        data: {
                            pk_org: pkOrg,
                        },
                        async: false,
                        success: (res) => {
                            if (res.success) {
                                pkAccountingbook = res.data;
                            }
                        }
                    })
                    return {
                        isDataPowerEnable: 'Y',
                        pk_accountingbook: pkAccountingbook,
                        DataPowerOperationCode: 'fi',//使用权组
                        datestr: getBusinessInfo().businessDate
                    };
                }
                break;
            case 'pk_pcorg'://利润中心
                item.queryCondition = () => {
                    if (billType == "F0" || billType == "F2") {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                        };
                    } else {
                        return {
                            isDataPowerEnable: 'Y',
                        };
                    }
                }
                break;
            case 'pu_org'://业务组织(业务单元（财务组织委托）) @atuo:zhangygw ---- ok
                item.queryCondition = () => {
                    return {
                        isDataPowerEnable: 'Y',
                        pkOrgs: pkOrg,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            case 'pu_org_v'://业务单元版本(财务组织委托)@auto zhangygw --- ok 
                item.isShowUnit = false;
                item.queryCondition = () => {
                    return {
                        isDataPowerEnable: 'Y',
                        pkOrgs: pkOrg,
                        VersionStartDate:getBusinessInfo().businessDate,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            case 'pu_deptid'://业务部门
                item.isShowUnit = true
                item.unitCondition = () => {
                    return {
                        pkOrgs: pkOrg,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    if (billType == "F1" || billType == "F3") {
                        return {
                            busifuncode: 'all',
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrg
                        };
                    } else {
                        return {
                            busifuncode: 'all',
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrg
                        };
                    }
                }
                break;
            case 'pu_psndoc'://业务人员
                item.isShowUnit = true
                item.unitCondition = () => {
                    return {
                        pkOrgs: pkOrg,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    if (billType == "F3" || billType == "F1") {
                        return {
                            busifuncode: 'all',
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrg,
                        };
                    } else {
                        return {
                            busifuncode: 'all',
                            isDataPowerEnable: 'Y',
                            pk_org: pkOrg,
                        };
                    } s
                }
                break;
            case 'pk_areacl'://地区
                item.queryCondition = () => {
                    return {
                        isDataPowerEnable: 'Y',
                    }
                }
                break;
            case 'project'://项目
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrg
                    };
                }
                break;
            case 'project_task'://项目任务
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrg,
                        project: props.search.getSearchValByField(searchId, 'project') ? props.search.getSearchValByField(searchId, 'project').value.firstvalue : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.ProjectTaskSqlBuilder'
                    };
                }
                break;
            case 'costcenter'://成本中心
                item.queryCondition = () => {
                    let tmporg =  props.search.getSearchValByField(searchId, 'pk_pcorg') ? props.search.getSearchValByField(searchId, 'pk_pcorg').value.firstvalue : null;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_pcorg: tmporg,
                        pk_org: tmporg,
                        orgType:"pk_profitcenter",
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.CostCenterSqlBuilder'
                    };
                }
                break;
            default:
                item.queryCondition = () => {
                    return {
                        pk_org: pkOrg,
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
        }
    })
    props.meta.setMeta(meta);
    return falg; //默认单元格都可操作

}

export { modifierSearchMetas }
