/**
 * 记录复制中一些固定值
 */
let ArapConstant ={
    ARAP_POWER_CODE : "fi",
	ARAP_AR_BILLCLASS : "ys",
	ARAP_AP_BILLCLASS : "yf",
	ARAP_SK_BILLCLASS : "sk",
	ARAP_FK_BILLCLASS : "fk",
	ARAP_ZS_BILLCLASS : "zs",
    ARAP_ZF_BILLCLASS : "zf",
    ASSO_OBJ_CUSTOMER : '0', // 客户
	ASSO_OBJ_SUPPLIER : '1', // 供应商
	ASSO_OBJ_DEPT : '2',// 部门
	ASSO_OBJ_BUSIMAN : '3', // 业务员
	ASSO_OBJ_ALL : '9', // 所有
}

let IBillFieldGet ={
    YS : "ys",
	YF : "yf",
	SK : "sk",
	FK : "fk",
	ZS : "zs", 
    ZF : "zf",
    //单据大类
    F0 : "F0",// 应收
	F1 : "F1",// 应付
	F2 : "F2", // 收款
	F3 : "F3", // 付款
	DE : "DE", // 收款转预收
	DF : "DF", // 付款转预付
	F0S : "23F0",// 供应商应收
	F1C : "23F1", // 客户应收
	F2S : "23F2", // 供应商收款单
	F3C : "23F3",// 客户付款单
	E0 : "23E0",// 暂估应收单
	E1 : "23E1",// 暂估应付单
}

let ArapBillVOConsts ={
    m_intJSZXZF_NetBank : '5',//网银支付
	m_intJSZXZF_NONE : '0',//无结算方式
	m_intJSZXZF_HAND : '3',//手工支付
    m_intJSZXZF_BCenter : '10',//结算中心
    m_intDJZzzt_Default : '0',//默认
	m_intDJZzzt_Succeed : '1',//转账成功
	m_intDJZzzt_Fail : '-1',//转账失败
	m_intDJZzzt_Ongoing : '2',//转账中
    m_intDJZzzt_partly : '11',//部分
}

let FromSystem ={
    AR : '0', // 应收系统
	AP : '1', // 应付系统
	CMP : '2', // 现金管理
	SO : '3', // 销售系统
	PO : '4', // 采购系统
	FTS : '5', // 资金结算
	NET : '6', // 网上银行----dap_dapsystem中没有记录,
	CFBM : '8', // 集中票据管理
	XTDJ : '9', // 协同单据----dap_dapsystem中没有记录
	CDMA : '10', // 银行信贷管理
	XBGL : '12', // 项目管理----dap_dapsystem中没有记录
	TB : '13', // 全面预算
	TO : '16', // 内部交易
	WBJHPT : '17', // 外部交换平台----dap_dapsystem中没有记录
	ZJJX : '18', // 资金计息----dap_dapsystem中没有记录
	IC : '19', // 库存管理
	CT : '20', // 合同管理
	YS : '24', // 运输----dap_dapsystem中没有记录
	PS : '7', // 付款排程
	AM : '104', // 资产管理
	WSBX : '105',// 费用管理（网上报销）----dap_dapsystem中没有记录
	CR09 : '109'
}

let billstatus = {//单据状态
    //9=未确认，-1=保存，1=审批通过，2=审批中，-99=暂存，8=签字，
    UNCOMFIRM: '9',
    SAVE: '-1',
    AUDIT: '1',
    AUDITING: '2',
    TEMPEORARY: '-99',
    SIGN: '8'
};
let effectstatus = {//生效状态
    //0=未生效，10=已生效
    EXECUTED: '10',
    INVALID: '0'
}
let approvestatus = {//审批状态
    //-1=自由态，0=未通过态，1=通过态，2=进行中态，3=提交态， 
    NOSTATE: '-1',
    NOPASS: '0',
    PASSING: '1',
    GOINGON: '2',
    COMMIT: '3'
};

let INDEX_PK = "index_0";

//复制过程表头需要清空的值
let parentItemKey = ["isreded", "outbusitype", "payman", "paydate", "isonlinepay", "officialprintuser", "officialprintdate", "settlenum", "approvenote",
"modifiedtime", "creationtime", "creator", "modifier", "isflowbill", "confirmuser", "billno", "billdate", "billstatus", "operator", "approver", "approvestatus", "approvedate",
"lastapproveid", "isnetpayready", "lastadjustuser", "signuser", "signyear", "signperiod", "settleflag", "settletype", "effectstatus", "effectuser", "effectdate", "src_syscode",
"approvedate", "signuser", "busidate", "signdate", "isforce", "commpayenddate", "commpaybegindate", "isfromindependent", "invoiceno", "sddreversalflag", "reversalreason", "payreason",
"failurereason", "sddreversaler", "sddreversaldate", "creditrefstd", "ts" ,"coordflag"];

//不能清空rowno , 这样黏贴的时候就没有rowno了 ,特别是对结算页签表体行对应的时候, 要做虚拟id, rowno作为map的key
let childrenItemKey = ["payflag", "top_tradetype", "src_tradetype", "checkdirection", "transferdate", "src_itemid", "rowtype", "top_itemid", "top_billid",
"billno", "coordflag", "ispaytermmodified", "pk_ssitem", "top_billtype", "transferflag", "src_billid", "src_billtype", "dstlsubcs", "isverifyfinished", "pausetransact", "billdate",
"payman", "paydate", "verifyfinisheddate", "innerorderno", "purchaseorder", "invoiceno", "occupationmny", "busidate", "contractno", "commpaystatus", "commpaytype", "refuse_reason",
"paystaus", "commpaytype", "commpaystatus", "refuse_reason", "commpayer", "isrefused", "confernum", "checkno","checkno_display", "checktype", "bankrelated_code", "payreason", "batchcode", "pk_batchcode",
"settlemoney", "settlecurr", "ts","transerial"];


//计算金额表头需要传到后台的值
let headkey = ["pk_org","pk_group","billdate","pk_currtype","billclass"];
//计算金额表体需要传到后台的值(公共)
let bodykey = ["pk_org","pk_group","buysellflag","top_billtype","pk_currtype","taxprice","direction","taxrate"
		,"occupationmny","money_bal","local_money_bal"];

//贷方 - 应付单和收款单
let crBodykey = ["globalcrebit","globalnotax_cre","globaltax_cre","groupcrebit","groupnotax_cre","grouptax_cre",
	"local_money_cr","local_notax_cr","local_tax_cr","money_cr","notax_cr","quantity_cr"];

//借方 - 应收单和付款单
let deBodykey = ["globaldebit","globalnotax_de","globaltax_de","groupdebit","groupnotax_de","grouptax_de",
	"local_money_de","local_notax_de","local_tax_de","money_de","notax_de","quantity_de"];


export {ArapConstant,parentItemKey,childrenItemKey,IBillFieldGet,FromSystem,ArapBillVOConsts,
	billstatus,effectstatus,approvestatus,INDEX_PK,headkey,bodykey,crBodykey,deBodykey}