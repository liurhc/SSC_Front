import { ajax } from 'nc-lightapp-front';

//获取个性化中心中设置的默认值
function getInitOrg(appcode, cb) {
	let data = {
		appcode: appcode
	};
	ajax({
		url: ' /nccloud/platform/appregister/queryappcontext.do',
		data: data,
		async: false,
		success: (res) => {
			let { data } = res;
			let { org_Name, pk_org } = data;
			let pkOrg = {
				display: org_Name,
				value: pk_org,
				refpk2: pk_org
			};
			if (pk_org) {
				cb(pkOrg);
			}
		}
	});
}

export default getInitOrg;
