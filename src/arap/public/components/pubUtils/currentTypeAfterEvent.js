import { ajax } from 'nc-lightapp-front';

/**
 * 
 * @formId    表单id
 * @props    
 * @key    编辑后事件的key
 */
function currentTypeAfterFormEvents(formId, props, key) {
	ajax({
		url: '/nccloud/arap/ref/ratecontrol.do',
		async:false,
		data: {
			pk_org: props.form.getFormItemsValue(formId, 'pk_org') ? props.form.getFormItemsValue(formId, 'pk_org').value : null,
			pk_currtype: props.form.getFormItemsValue(formId, 'pk_currtype') ? props.form.getFormItemsValue(formId, 'pk_currtype').value : null
		},
		success: (res) => {
			if (res.success) {
				props.form.setFormItemsDisabled(formId, {
					'rate': !res.data
				});
			}
		}
	});
}


export { currentTypeAfterFormEvents };

