//获取个性化中心中设置的默认值
function getByStrLen(str) {
	var len = 0;
	var pattern = new RegExp('[\u4E00-\u9FA5]+'); //中文校验
	var pattern2 = new RegExp('[A-Za-z]+'); //英文校验
	if (pattern.test(str)) {
		len = str.length;
		if (len > 16) {
			return true;
		}
	} else if (pattern2.test(str)) {
		str = str.split('');
		len = str.length;
		if (len > 35) {
			return true;
		}
	}
	return false;
}

export default getByStrLen;
