


let dealCardData = (that,cardData) =>{
    let newCardData = JSON.parse(JSON.stringify(cardData));
    let head = newCardData.head[that.formId].rows[0];
    let bodys =  newCardData.body[that.tableId].rows;
    dealHead(head ,newCardData);
    dealbodys(bodys ,newCardData);

    return newCardData;

}

let dealHead = (head) =>{
    let values = head.values;
    Object.keys(values).map((key,index) => {
        //清空value和display
        let value = values[key].value;
        let display = values[key].display;
        let scale = values[key].scale;
        if((!value || value == '') && (!display || display =='') && scale == '-1'){
            delete values[key];
        }
    });
}

let dealbodys = (bodys) =>{
    let length = bodys.length;
    for(let i = 0 ;i < length ; i ++){
        let body = bodys[i];
        let values = body.values;
        Object.keys(values).map((key) => {
            //清空value和display
            let value = values[key].value;
            let display = values[key].display;
            let scale = values[key].scale;
            if((!value || value == '') && (!display || display =='') && scale == '-1'){
                delete values[key];
            }
        });
    }
}

export { dealCardData };