import { ajax } from "nc-lightapp-front";


/**
 * 金额、税、汇率相关字段
 */
let moneyAndRateFields = ["money_bal", "money_de", "money_cr", "notax_de", "notax_cr", "occupationmny", "settlemoney", "nosubtax", "nosubtaxrate","local_money_bal", "local_money_de", "local_money_cr",
	"local_notax_de", "local_notax_cr", "local_tax_de", "local_tax_cr", "caltaxmny", "groupbalance", "groupdebit", "groupcrebit", "groupnotax_de", "groupnotax_cre", "groupagentreceivelocal",
	"grouptax_de", "grouptax_cre", "globalbalance", "globaldebit", "globalcrebit", "globalnotax_de", "globalnotax_cre", "globalagentreceivelocal", "globaltax_de", "globaltax_cre",
	"price", "taxprice", "postprice", "postpricenotax", "local_price", "local_taxprice", "rate", 'taxcodeid', 'taxrate', 'taxtype', "grouprate", "globalrate", "quantity_bal"]

/**
 * 非元数据字段票据号checkno_display编辑后特殊处理
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} index 
 */
function checknoDisplayAfterEvent(props, tableId, key, value, index) {
	if (key != 'checkno_display') {
		return
	}
	//将checkno_display字段的值赋给checkno
	if (value && value.refpk) {
		//参照
		props.cardTable.setValByKeyAndIndex(tableId, index, 'checkno', { value: value.refpk, display: value.refname })
		props.cardTable.setValByKeyAndIndex(tableId, index, 'checkno_display', { value: value.refname, display: value.refname })
	} else {
		if (value instanceof Object) {
			//清空
			props.cardTable.setValByKeyAndIndex(tableId, index, 'checkno', { value: null, display: null })
		} else {
			//字符串
			props.cardTable.setValByKeyAndIndex(tableId, index, 'checkno', { value: value, display: value })
		}
	}

}

/**
 * 根据票据类型动态修改票据号字段类型
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} pk_org 
 * @param {*} checktype 
 */
function modifyChecknoItemtype(props, tableId, pk_org, checktype) {
	if (!checktype) {
		return
	}
	let billclass = props.cardTable.getValByKeyAndIndex(tableId, 0, 'billclass').value;
	let meta = props.meta.getMeta()
	var destEditAreaCode = meta['gridrelation']['bodys'].destEditAreaCode;
	meta[destEditAreaCode].items.map((item) => {
		if (item.attrcode == 'checkno_display') {
			let needref = isChecknoRef(checktype, pk_org);
			if (needref && item.itemtype != 'refer') {
				item.itemtype = 'refer';
				if (billclass == 'fk') {
					item.refcode = 'fbm/refer/fbm/Bill4CmpPayGridRef/index.js';
				} else if (billclass == 'sk') {
					item.refcode = 'fbm/refer/fbm/Bill4CmpReceiveGridRef/index.js';
				}
			} else if (needref && item.itemtype == 'refer') {
			} else {
				item.itemtype = 'input';
				item.refcode = null;
			}
		}
		return item;
	})
	props.meta.setMeta(meta);
}

/**
 * 票据类型checktype编辑后特殊处理
 * @param {*} props 
 * @param {*} formId 
 * @param {*} tableId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} index 
 */
function checktypeAfterEvent(props, tableId, key, value, index) {
	if (key != 'checktype' || !value.refpk) {
		return
	}
	//编辑票据类型清空票据号
	props.cardTable.setValByKeyAndIndex(tableId, index, 'checkno_display', { value: null, display: null })
	props.cardTable.setEditableByIndex(tableId, index, 'checkno_display', true);
	//根据票据类型动态修改票据号字段类型
	let pk_org = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_org').value
	modifyChecknoItemtype(props, tableId, pk_org, value.refpk)
}

/**
 * 根据票据类型和财务组织判断票据号是否参照类型
 * @param {*} checktype 
 * @param {*} pk_org 
 */
function isChecknoRef(checktype, pk_org) {
	let editflag = false;
	ajax({
		url: "/nccloud/arap/arappub/ischecknoref.do",
		data: {
			checktype: checktype,
			pk_org: pk_org
		},
		async: false,
		success: (res) => {
			editflag = res.data;
		}
	})
	return editflag;
}

function reWriteCheckno(props, tableId) {
	let checknoValues = props.cardTable.getColValue(tableId, 'checkno_display', false, false)
	for (var i = 0; i < checknoValues.length; i++) {
		let checkno_display = checknoValues[i]
		if (!checkno_display.display) {
			props.cardTable.setValByKeyAndIndex(tableId, i, 'checkno_display', { value: checkno_display.value, display: checkno_display.value })
		}
	}
}


export { checknoDisplayAfterEvent, modifyChecknoItemtype, checktypeAfterEvent, isChecknoRef, reWriteCheckno, moneyAndRateFields };
