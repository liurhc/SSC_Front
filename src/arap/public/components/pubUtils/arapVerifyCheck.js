import { toast, getBusinessInfo } from 'nc-lightapp-front';
import { accAdd } from '../method';

let prev_pc, //预核销 pageCode
	nowv_pc, // 即时核销 pageCode
	ac; // 预核销 即时核销 appCode

/**
 * 按表体核销校验
 */

function BodyVerify(props, formId, tableId, pkBill, pkItem, that) {
	let businessDates = getBusinessInfo().businessDate;
	let getCheckedRowDatas = props.cardTable.getCheckedRows(tableId); //获取选中行的数据
	if (getCheckedRowDatas.length == 0) {
		toast({
			color: 'warning',
			content: that.state.json['public-000152'] /* 国际化处理： 请选择一行进行核销！*/
		});
		return;
	} else if (getCheckedRowDatas.length == 1) {
		let allFormDatas = props.form.getAllFormValue(formId).rows[0].values;
		let checkedRowDatas = getCheckedRowDatas[0].data.values;
		// 1、操作日期早于单据日期 校验
		let isDate = true;
		let billDate = props.form.getFormItemsValue(formId, 'billdate').value.substring(0, 10);
		let billDateB = billDate; //获取整单单据日期
		let operationDateB = businessDates; //操作日期
		if (operationDateB < billDateB) {
			isDate = false;
		}
		if (!isDate) {
			toast({
				color: 'warning',
				content: that.state.json['public-000153'] /* 国际化处理： 操作日期早于单据日期，不能进行核销!*/
			});
			return;
		}

		let isOccupationmnyNow = true; //预占用余额 校验
		let isHangUpB = getCheckedRowDatas[0].data.values.pausetransact.value; // 单据是否挂起
		let effectstatus = props.form.getFormItemsValue(formId, 'effectstatus').value;
		let occupationmnyB = getCheckedRowDatas[0].data.values.occupationmny.value * 1;
		if (effectstatus == 10) {
			// 2、选中行表体余额
			if (occupationmnyB == 0) {
				isOccupationmnyNow = false;
			}
			if (effectstatus == 10 && !isOccupationmnyNow) {
				toast({
					color: 'warning',
					content: that.state.json['public-000154'] /* 国际化处理： 表体余额为零，不能进行核销!*/
				});
				return;
			}

			// 3、挂起校验
			if (isHangUpB) {
				toast({
					color: 'warning',
					content: that.state.json['public-000155'] /* 国际化处理： 单据行已经挂起，不能进行核销操作!*/
				});
				return;
			}
		}

		let objTypeVal = null; //核销对象的值
		switch (checkedRowDatas.objtype.value) {
			case '0':
				objTypeVal = checkedRowDatas.customer;
				break;
			case '1':
				objTypeVal = checkedRowDatas.supplier;
				break;
			case '2':
				objTypeVal = checkedRowDatas.pk_deptid_v;
				break;
			case '3':
				objTypeVal = checkedRowDatas.pk_psndoc;
				break;
		}
		let checkedData = {
			pk_bill: allFormDatas[pkBill], //单据主键
			pkBill: pkBill,
			pk_org: allFormDatas.pk_org, //财务组织
			pk_item: getCheckedRowDatas[0].data.values[pkItem].value, //按表体核销时，设置行主键
			billType: allFormDatas.pk_billtype, //单据类型
			ts: allFormDatas.ts, //表头ts
			objtype: checkedRowDatas.objtype, //往来对象
			objTypeValue: objTypeVal, //核销对象的值
			customer: allFormDatas.customer, //核销对象对应的值(客户)
			supplier: allFormDatas.supplier, //核销对象对应的值(供应商)
			pk_deptid: allFormDatas.pk_deptid_v, //核销对象对应的值(部门)
			pk_psndoc: allFormDatas.pk_psndoc, //核销对象对应的值(业务员)
			occupationmnySum: occupationmnyB, //表体选中行的预占用余额
			pk_tradetype: allFormDatas.pk_tradetype, //交易类型即页面编码
			pageId: props.getSearchParam('p'), //页面编码
			appcode: props.getSearchParam('c'), //小应用编码
			effectstatus: effectstatus,
			scene: props.getUrlParam('scene')
		};
		switch (pkBill) {
			case 'pk_payablebill':
			case 'pk_paybill':
				prev_pc = '20080PREV';
				nowv_pc = '20080NOWV';
				ac = '20082002';
				break;
			case 'pk_recbill':
			case 'pk_gatherbill':
				prev_pc = '20060PREV';
				nowv_pc = '20060NOWV';
				ac = '20062002';
				break;
		}
		let tmppagecode = nowv_pc;
		let tmpcomp = '/nowv';
		if (effectstatus == 0) {
			tmppagecode = prev_pc;
			tmpcomp = '/prev';
		}
		let scene = props.getUrlParam('scene');
		let parameter =
			'appcode=' +
			ac +
			'&pagecode=' +
			tmppagecode +
			'&c=' +
			ac +
			'&p=' +
			tmppagecode +
			'&scene=' +
			scene +
			'&transAppCode=' +
			props.getUrlParam('appcode') +
			'&transPageCode=' +
			props.getUrlParam('pagecode') +
			'&Id=' +
			JSON.stringify(allFormDatas[pkBill]);
		if (isDate && isOccupationmnyNow && !isHangUpB) {
			props.ViewModel.setData('WholeVerifyDatas', checkedData);
			if (window.top === window.parent) {
				props.pushTo(tmpcomp, {
					appcode: ac,
					pagecode: tmppagecode,
					transAppCode: props.getSearchParam('c'),
					transPageCode: props.getSearchParam('p'),
					scene: scene,
					Id: allFormDatas[pkBill]
				});
			} else {
				window.location.hash = tmpcomp + '?' + parameter;
				window.parent.location.hash = tmpcomp + '?' + parameter;
			}
		}
	} else {
		toast({
			color: 'warning',
			content: that.state.json['public-000156'] /* 国际化处理： 只能选择一行进行核销！*/
		});
		return;
	}
}

/**
 * 按整单核销校验
 */

function WholeVerify(props, formId, tableId, pkBill, that) {
	let businessDates = getBusinessInfo().businessDate;
	let allFormData = props.form.getAllFormValue(formId).rows[0].values;
	let effectstatus = props.form.getFormItemsValue(formId, 'effectstatus').value;
	let cartTableDatas = props.cardTable.getAllRows(tableId, false);

	// 1、操作日期早于单据日期
	let isDateTrue = true; //操作日期早于单据日期 校验
	let billDates = props.form.getFormItemsValue(formId, 'billdate').value.substring(0, 10);
	let billDate = Date.parse(billDates); //获取整单单据日期
	let operationDate = Date.parse(businessDates); //操作日期
	if (operationDate < billDate) {
		isDateTrue = false;
	}
	if (!isDateTrue) {
		toast({
			color: 'warning',
			content: that.state.json['public-000157'] /* 国际化处理： 操作日期早于单据日期，不能核销!*/
		});
		return;
	}

	// 2、表体往来对象值相同
	let objIstrue = true;
	if (cartTableDatas.length > 0) {
		var temp = cartTableDatas[0].values.objtype.value;
	}
	let contactObj = 'customer';
	switch (temp) {
		case '0':
			contactObj = 'customer';
			break;
		case '1':
			contactObj = 'supplier';
			break;
		case '2':
			contactObj = 'pk_deptid_v';
			break;
		case '3':
			contactObj = 'pk_psndoc';
			break;
	}
	for (var i = 0; i < cartTableDatas.length; i++) {
		if (cartTableDatas[i].values.objtype.value != temp) {
			objIstrue = false;
			break;
		}
	}
	if (!objIstrue) {
		toast({
			color: 'warning',
			content: that.state.json['public-000158'] /* 国际化处理： 表体往来对象值必须相同!*/
		});
		return;
	}

	let objtypeIsTrue = true; //表体往来对象值相同 校验
	if (cartTableDatas.length > 0) {
		var tem = cartTableDatas[0].values[contactObj].value;
	}
	if (objIstrue) {
		for (var i = 0; i < cartTableDatas.length; i++) {
			if (cartTableDatas[i].values[contactObj].value != tem) {
				objtypeIsTrue = false;
				break;
			}
		}
	}
	if (!objtypeIsTrue) {
		toast({
			color: 'warning',
			content: that.state.json['public-000158'] /* 国际化处理： 表体往来对象值必须相同!*/
		});
		return;
	}

	// 3、表体币种校验
	let isCurrencyTrue = true; //表体币种 校验
	if (cartTableDatas.length > 0) {
		var temp = cartTableDatas[0].values.pk_currtype.value;
	}
	for (var i = 0; i < cartTableDatas.length; i++) {
		if (cartTableDatas[i].values.pk_currtype.value != temp) {
			isCurrencyTrue = false;
			break;
		}
	}
	if (!isCurrencyTrue) {
		toast({
			color: 'warning',
			content: that.state.json['public-000159'] /* 国际化处理： 表体币种必须相同!*/
		});
		return;
	}

	let occupationmnySum = 0; //表体预占用余额的汇总和
	if (cartTableDatas.length > 0) {
		for (var i = 0; i < cartTableDatas.length; i++) {
			occupationmnySum = accAdd(occupationmnySum, cartTableDatas[i].values.occupationmny.value * 1);
		}
	}

	let isHangUp = true; //"单据是否挂起
	let isOccupationmnyNow = true;
	if (effectstatus == 10) {
		// 4、预占用余额 校验
		if (occupationmnySum == 0) {
			isOccupationmnyNow = false;
			toast({
				color: 'warning',
				content: that.state.json['public-000160'] /* 国际化处理： 单据余额为零，不能核销!*/
			});
			return;
		}

		// 5、单据是否挂起  pausetransact  value false
		for (var i = 0; i < cartTableDatas.length; i++) {
			if (cartTableDatas[i].values.pausetransact.value == true) {
				isHangUp = false;
				break;
			}
		}
		if (!isHangUp) {
			toast({
				color: 'warning',
				content: that.state.json['public-000161'] /* 国际化处理： 单据存在已经挂起的行,不能核销!*/
			});
			return;
		}
	}

	let objTypeValue = null; //核销对象的值
	switch (allFormData.objtype.value) {
		case '0':
			objTypeValue = allFormData.customer;
			break;
		case '1':
			objTypeValue = allFormData.supplier;
			break;
		case '2':
			objTypeValue = allFormData.pk_deptid_v;
			break;
		case '3':
			objTypeValue = allFormData.pk_psndoc;
			break;
	}

	let formData = {
		pk_bill: allFormData[pkBill], //单据主键
		pkBill: pkBill,
		pk_org: allFormData.pk_org, //财务组织
		pk_item: null, //按表体核销时，设置行主键
		billType: allFormData.pk_billtype, //单据类型
		ts: allFormData.ts, //表头ts
		objtype: allFormData.objtype, //往来对象
		objTypeValue: objTypeValue, //核销对象的值
		customer: allFormData.customer, //核销对象对应的值(客户)
		supplier: allFormData.supplier, //核销对象对应的值(供应商)
		pk_deptid: allFormData.pk_deptid_v, //核销对象对应的值(部门)
		pk_psndoc: allFormData.pk_psndoc, //核销对象对应的值(业务员)
		occupationmnySum: occupationmnySum, //表体预占用余额的汇总和
		pk_tradetype: allFormData.pk_tradetype, //交易类型即页面编码
		pageId: props.getSearchParam('p'), //页面编码
		appcode: props.getSearchParam('c'), //小应用编码
		effectstatus: effectstatus,
		scene: props.getUrlParam('scene')
	};
	switch (pkBill) {
		case 'pk_payablebill':
		case 'pk_paybill':
			prev_pc = '20080PREV';
			nowv_pc = '20080NOWV';
			ac = '20082002';
			break;
		case 'pk_recbill':
		case 'pk_gatherbill':
			prev_pc = '20060PREV';
			nowv_pc = '20060NOWV';
			ac = '20062002';
			break;
	}
	let tmppagecode = nowv_pc;
	let tmpcomp = '/nowv';
	if (effectstatus == 0) {
		tmppagecode = prev_pc;
		tmpcomp = '/prev';
	}
	let scene = props.getUrlParam('scene');
	let parameter =
		'appcode=' +
		ac +
		'&pagecode=' +
		tmppagecode +
		'&c=' +
		ac +
		'&p=' +
		tmppagecode +
		'&scene=' +
		scene +
		'&transAppCode=' +
		props.getUrlParam('appcode') +
		'&transPageCode=' +
		props.getUrlParam('pagecode') +
		'&Id=' +
		JSON.stringify(allFormData[pkBill]);
	if (objIstrue && objtypeIsTrue && isDateTrue && isCurrencyTrue && isHangUp && isOccupationmnyNow) {
		props.ViewModel.setData('WholeVerifyDatas', formData);
		if (window.top === window.parent) {
			props.pushTo(tmpcomp, {
				appcode: ac,
				pagecode: tmppagecode,
				transVerifyDatas: JSON.stringify(formData),
				transAppCode: props.getSearchParam('c'),
				transPageCode: props.getSearchParam('p'),
				scene: scene,
				Id: allFormData[pkBill]
			});
		} else {
			window.location.hash = tmpcomp + '?' + parameter;
			window.parent.location.hash = tmpcomp + '?' + parameter;
		}
	}
}

export { BodyVerify, WholeVerify };
