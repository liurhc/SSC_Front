
export function contactButton(button, btns) {
    let buttons = button;
    let AddBtn = undefined; //新增
    let AddBlankBtn = undefined; //新增 下拉按钮 的一个区域
    //循环得到 AddBtn
    AddBtn = getAddButton(AddBtn, buttons)
    if (AddBtn) {
        if (AddBtn.children && AddBtn.children.length > 0) {
            AddBlankBtn = AddBtn.children[0];
        }
        if (AddBlankBtn) {
            if (btns && btns.length > 0) {
                AddBlankBtn.children = AddBlankBtn.children.concat(btns);
            } 
        }
    }
}



/**
 * 获取拉单按钮所对应的流程信息
 * @e   this对象本身
 * @key 拉单按钮key  
 */
function getTransferInfo(that, key) {
    let pullBillInfoVOAry = that.Info.pullBillInfoVOAry;
    let transferInfo = undefined;
    if (pullBillInfoVOAry) {
        for (let i = 0; i < pullBillInfoVOAry.length; i++) {
            let info = pullBillInfoVOAry[i];
            if (info['src_billtype'] === key) {
                transferInfo = info;
                break;
            }
        }
    }
    return transferInfo;
}

function getAddButton(AddBtn, buttons) {

    //循环得到 AddBtn
    if (AddBtn) {
        return AddBtn;
    }
    for (let btn of buttons) {
        if (btn.key == 'Add') {
            AddBtn = btn
        } else if (btn.children.length == 0) {
            if (btn.key == 'Add') {
                AddBtn = btn
                break;
            }
        } else {
            AddBtn = getAddButton(AddBtn, btn.children);
            if (AddBtn) {
                break;
            }
        }
    }
    return AddBtn;
}


export { getTransferInfo };

