

/**
 * 编辑单据时，动态控制往来对象的编辑性，不再通过编辑前事件控制
 * @param {*} props 
 * @param {*} formId 
 * @param {*} tableId 
 */
function objtypeEditable(props, formId, tableId) {
    var billtype = props.form.getFormItemsValue(formId, 'pk_billtype') ? props.form.getFormItemsValue(formId, 'pk_billtype').value : null;
    //往来对象 0=客户，1=供应商，2=部门，3=业务员
    var objtype = props.form.getFormItemsValue(formId, 'objtype') ? props.form.getFormItemsValue(formId, 'objtype').value : null;
    if (billtype && (billtype == 'F1' || billtype == 'F3' || billtype == "23E1")) {
        //应付单、付款单往来对象是客户，控制往来对象、币种不可编辑
        if (objtype && objtype == "0") {
            props.form.setFormItemsDisabled(formId, { 'objtype': true });
            props.form.setFormItemsDisabled(formId, { 'pk_currtype': true });
            props.cardTable.setColEditableByKey(tableId, ['objtype', 'pk_currtype'], true)
        }
    } else if (billtype && (billtype == 'F0' || billtype == 'F2' || billtype == "23E0")) {
        //应收单、收款单往来对象是供应商，控制往来对象、币种不可编辑
        if (objtype && objtype == "0") {
            props.form.setFormItemsDisabled(formId, { 'objtype': true });
            props.form.setFormItemsDisabled(formId, { 'pk_currtype': true });
            props.cardTable.setColEditableByKey(tableId, ['objtype', 'pk_currtype'], true)
        }
    }
}

/**
 * 编辑单据时，动态控制承付、关联结算信息相关字段的编辑性
 * @param {*} props 
 * @param {*} formId 
 * @param {*} tableId 
 */
function dealCommisionPayField(props, formId, tableId) {
    //是否承付
    var isforce = props.form.getFormItemsValue(formId, 'isforce') ? props.form.getFormItemsValue(formId, 'isforce').value : false;
    var billtype = props.form.getFormItemsValue(formId, 'pk_billtype') ? props.form.getFormItemsValue(formId, 'pk_billtype').value : null;
    let supplier = props.form.getFormItemsValue(formId, 'supplier') ? props.form.getFormItemsValue(formId, 'supplier').value : null
    let customer = props.form.getFormItemsValue(formId, 'customer') ? props.form.getFormItemsValue(formId, 'customer').value : null
    if (billtype != 'F2' && billtype != 'F3') {
        return
    }
    //关联结算信息
    let src = props.getUrlParam('src') == 'settlement';
    if (isforce || src) {
        props.form.setFormItemsDisabled(formId, {
            pk_org: true,
            objtype: true,
            supplier: true,
            customer: true,
            pk_currtype: true,
            rate: true,
            payaccount: true,
            recaccount: true,
            money: true,
            local_money: true,
            grouplocal: true,
            grouprate: true,
            globallocal: true,
            globalrate: true,
            commpaybegindate: true,
            commpayenddate: true,
            isforce: true
        });
        props.cardTable.setColEditableByKey(
            tableId,
            [
                'objtype',
                'supplier',
                'customer',
                'pk_currtype',
                'rate',
                'payaccount',
                'recaccount',
                'money_de',
                'notax_de',
                'local_money_de',
                'local_notax_de',
                'local_tax_de',
                'groupdebit',
                'groupnotax_de',
                'globaldebit',
                'globalnotax_de',
                'commpayer',
                'commpaystatus',
                'commpaytype'
            ],
            true
        );
    }
    if (!supplier && billtype == 'F3') {
        props.form.setFormItemsDisabled(formId, { supplier: false });
        props.cardTable.setColEditableByKey(tableId, ['supplier'], false);
    }
    if (!customer && billtype == 'F2') {
        props.form.setFormItemsDisabled(formId, { "customer": false })
        props.cardTable.setColEditableByKey(tableId, ["customer"], false)
    }
}

/**
 * 付款单拉红字应收特殊字段编辑性
 * @param {*} props 
 * @param {*} formId 
 * @param {*} tableId 
 */
function F0ToF3FieldsEditable(props, formId, tableId) {
    props.form.setFormItemsDisabled(formId, { objtype: true, pk_currtype: true });
    props.cardTable.setColEditableByKey(tableId, ['objtype', 'pk_currtype'], true)
}

/**
 * 付款单拉付款申请特殊字段编辑性
 * @param {*} props 
 * @param {*} formId 
 * @param {*} tableId 
 */
function applyToF3FieldsEditable(props, formId, tableId) {
    props.form.setFormItemsDisabled(formId, { pk_org: true, pk_currtype: true, billdate: true, supplier: true });
    props.cardTable.setColEditableByKey(tableId,
        ['pk_currtype', 'billdate', 'supplier', 'prepay', 'purchaseorder', 'invoiceno', 'contractno', 'quantity_de', 'pu_org', 'pu_deptid', 'pu_psndoc'],
        true)
    //付款银行账户如果不为空，则不可编辑
    var payaccount = props.form.getFormItemsValue(formId, 'payaccount') ? props.form.getFormItemsValue(formId, 'payaccount').value : null;
    if (payaccount) {
        props.form.setFormItemsDisabled(formId, { payaccount: true });
        props.cardTable.setColEditableByKey(tableId, ['payaccount'], true)
    }
}

/**
 * 收付款合同推应收应付字段编辑性控制
 * @param {*} props 
 * @param {*} formId 
 * @param {*} tableId 
 */
function fctToArapFieldEditable(props, formId, tableId){
    let srcbilltype = props.getUrlParam('srcbilltype')
    if (srcbilltype && (srcbilltype == 'FCT1' || srcbilltype == 'FCT2')){
        props.form.setFormItemsDisabled(formId, { pk_org: true });
    }
}


export { objtypeEditable, dealCommisionPayField, F0ToF3FieldsEditable, applyToF3FieldsEditable, fctToArapFieldEditable }