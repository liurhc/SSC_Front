import { ajax, base, toast, cacheTools, cardCache, deepClone } from 'nc-lightapp-front';
let { getDefData, setDefData } = cardCache;
//复制行
let copyLine = function (e, dataSource) {
	let copyData = e.props.cardTable.getCheckedRows(e.tableId);
	if (copyData.length == 0) {
		toast({ color: 'warning', content: this.state.json['public-000042'] });/* 国际化处理： 请至少选择一行数据进行复制！*/
		return false;
	}
	let i = 0;
	let arr = [];
	copyData.forEach((val) => {
		if (val.data.status != '3') {
			arr.push(copyData[i].data);
			i++;
		}

	});
	setDefData("CopyLine", dataSource, arr);
	return true;
};


//粘贴行到末行
let pasteToEndLine = function (e, dataSource) {
	let rowNum = e.props.cardTable.getNumberOfRows(e.tableId);
	// let pasteData = cacheTools.get('copyData');
	let pasteData = getDefData("CopyLine", dataSource);
	e.props.cardTable.insertRowsAfterIndex(e.tableId, pasteData, rowNum - 1);
	let i = 0;
	for (; i < pasteData.length; i++) {
		//清空表体主键
		resetBodyPk(e, rowNum + i);
		// 清空top_bill, src_bill
		// clearTopInfos(e, rowNum + i);
	}

	//计算单据表头金额
	calculateHeadMoney(e);
	if (e.props.getUrlParam('type') == 'transfer') {
		e.synTransferData();
	}
};

//删除行
let delLine = function (e) {
	let delData = e.props.cardTable.getCheckedRows(e.tableId);
	if (delData.length == 0) {
		toast({ color: 'warning', content: this.state.json['public-000041'] });/* 国际化处理： 请至少选择一行数据！*/
		return;
	}
	let delIndexs = [];
	delData.forEach((val) => {
		if (val.data.status != '3') {
			delIndexs.push(val.index);
		}
	});
	e.props.cardTable.delRowsByIndex(e.tableId, delIndexs);
	//应收单合并计算税
	if(e.billType == 'F0'){
		F0DeleteAfterEditCalculate(e,delData);
	}
	//计算单据表头金额
	calculateHeadMoney(e);
	if (e.props.getUrlParam('type') == 'transfer') {
		e.synTransferData();
	}
};

//应收单删行的合并计算税功能
let F0DeleteAfterEditCalculate =function(that,delData){
	if(that.billType == 'F0'){//应收单合并计算税额
		let flag = false;
		let tradeType =that.props.form.getFormItemsValue(that.formId, 'pk_tradetype') ? (
			that.props.form.getFormItemsValue(that.formId, 'pk_tradetype').value
		) : null;
		ajax({
			url: '/nccloud/arap/arappub/isConsolidatedTax.do',
			data: {
				tradeType:tradeType
			},
			async: false,
			success: (res) => {
				flag = res.data;
			}
		});
		if(!flag){
			return;
		}
		let GROUP_KEYS_LIST =['customer','taxcodeid','taxrate','taxtype','buysellflag','money_de',
		'local_money_de','notax_de','local_notax_de','local_tax_de',];
		let colValues =getColvalues(that.props, that.tableId, GROUP_KEYS_LIST);
		//对整单中客户、税码、税率、扣税类别、购销类型相同的，作为一组，合计计算税基，再计算税额。
		let bodys = [];//找到当前删除行的同一组的最后一行customer, taxcodeid, taxrate, taxtype, buysellflag
		let allRows = that.props.cardTable.getAllRows(that.tableId);//获取所有显示的行
		let rowidIndex ={};//每一行对应的实际行，主键为rowId ,value为index
		//key = 对整单中客户、税码、税率、扣税类别、购销类型 字段拼接而成，value =当前行数据
		let groupRow ={}//保存当前分组的最后一行,如果可以一样，前面的将会被后面的覆盖
		allRows.forEach((val,index) => {
			if(val.status != '3'){
				let key = val.values.customer.value+"_"+
				val.values.taxcodeid.value+"_"+
				val.values.taxrate.value+"_"+
				val.values.taxtype.value+"_"+
				val.values.buysellflag.value;
				groupRow[key] = val;
				rowidIndex[val.rowid] = index;
			}
		});
		let delDataKey =[];//删除行对应的key
		delData.forEach((val) => {
			//if (val.data.status != '3') {
				let key = val.data.values.customer.value+"_"+
				val.data.values.taxcodeid.value+"_"+
				val.data.values.taxrate.value+"_"+
				val.data.values.taxtype.value+"_"+
				val.data.values.buysellflag.value;
				delDataKey.push(key);
			//}
		});
		for(let i =0; i< delDataKey.length ;i++){
			if(groupRow[delDataKey[i]]){
				bodys.push(groupRow[delDataKey[i]]);
			}
		}
		if(bodys.length == 0){//如果不存在合并计算税的表体行，直接return;
			return;
		}
		let cardData = that.props.createMasterChildData(that.getPagecode(), that.formId, that.tableId);
        //清空cardData的表体，直流表头
		cardData.body[that.tableId].rows = bodys;
		ajax({
			url: '/nccloud/arap/arappub/deleteAfterEditCalculate.do',
			data: {
				cardData:cardData,
				colValues:colValues,
				rowidIndex:rowidIndex
			},
			success: (res) => {
				that.props.cardTable.updateDataByRowId(that.tableId, res.data.body[that.tableId]);//只需要更新表体行
			},
			
		});


	}
}



//表体行复制
let copyInner = function (record, dataSource, that) {

	let copyIndexs = [];
	copyIndexs.push(record);
	setDefData("CopyLine", dataSource, copyIndexs);
	if (copyIndexs.length >= 1) {
		toast({ color: 'success', content: that.state.json['public-000162'] });/* 国际化处理： 复制成功！*/
		return;
	}
}

//粘贴至此
let pasteInner = function (that, props, dataSource, tableId, index) {

	let pasteData = getDefData("CopyLine", dataSource);

	props.cardTable.insertRowsAfterIndex(tableId, pasteData, index);
	let i = 0;
	for (; i < pasteData.length; i++) {
		//清空表体主键
		resetBodyPk(that, index + 1 + i);
		// 清空top_bill, src_bill
		// clearTopInfos(that, index + 1 + i);

	}
	//计算单据表头金额
	calculateHeadMoney(that);
	if (that.props.getUrlParam('type') == 'transfer') {
		that.synTransferData();
	}
};

//删行
let deleteInner = function (that, props, tableId, index) {

	props.cardTable.delRowsByIndex(tableId, index);
	if(that.billType == 'F0'){
		let delData = that.props.cardTable.getRowsByIndexs(that.tableId,[index]);
		//删行之后进行合并计算税
		let newdata =[{
			data : delData[0],
			index: index
		}];
		F0DeleteAfterEditCalculate(that,newdata);
	}
	//计算单据表头金额
	calculateHeadMoney(that);
	if (that.props.getUrlParam('type') == 'transfer') {
		that.synTransferData();
	}
};


//计算单据表头金额
let calculateHeadMoney = function (e) {
	let billclass = e.props.form.getFormItemsValue(e.formId, 'billclass').value;
	let data = e.props.createMasterChildData(e.props.pageId, e.formId, e.tableId);
	let money = null;
	let local_money = null;
	let grouplocal = null;
	let globallocal = null;
	if (billclass == 'yf' || billclass == 'sk') {
		money = getBodyAmountValue(e.formId, e.tableId, data, 'money_cr');
		local_money = getBodyAmountValue(e.formId, e.tableId, data, 'local_money_cr');
		grouplocal = getBodyAmountValue(e.formId, e.tableId, data, 'groupcrebit');
		globallocal = getBodyAmountValue(e.formId, e.tableId, data, 'globalcrebit');
	} else if (billclass == 'ys' || billclass == 'fk') {
		money = getBodyAmountValue(e.formId, e.tableId, data, 'money_de');
		local_money = getBodyAmountValue(e.formId, e.tableId, data, 'local_money_de');
		grouplocal = getBodyAmountValue(e.formId, e.tableId, data, 'groupdebit');
		globallocal = getBodyAmountValue(e.formId, e.tableId, data, 'globaldebit');
	}
	e.props.form.setFormItemsValue(e.formId, { money: { value: money }, local_money: { value: local_money }, grouplocal: { value: grouplocal }, globallocal: { value: globallocal } });
};

//获取单据表体行某个字段金额合计值
let getBodyAmountValue = function (formId, tableId, data, fieldcode) {
	if (data && data.head[formId] && data.body[tableId]) {
		var amount = 0;
		let scale = 0;
		for (let i = 0; i < data.body[tableId].rows.length; i++) {
			if (data.body[tableId].rows[i].status != "3") {
				let val = data.body[tableId].rows[i].values[fieldcode].value;
				scale = data.body[tableId].rows[i].values[fieldcode].scale * 1;
				val = parseFloat(val);
				if (!isNaN(val)) {
					amount = amount + val;
				}
			}
		}
		amount = amount.toFixed(scale);
		return amount;
	}
};

//清空表体主键pk
let resetBodyPk = function (e, index) {
	let billclass = e.props.form.getFormItemsValue(e.formId, 'billclass').value;
	if (billclass == null) {
		return;
	}
	if (billclass == 'ys') {
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'pk_recitem', { value: null });
	} else if (billclass == 'sk') {
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'pk_gatheritem', { value: null });
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'checktype', { value: null, display: null });
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'checkno', { value: null, display: null });
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'checkno_display', { value: null, display: null });
	} else if (billclass == 'yf') {
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'pk_payableitem', { value: null });
	} else if (billclass == 'fk') {
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'pk_payitem', { value: null });
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'checktype', { value: null, display: null });
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'checkno', { value: null, display: null });
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'checkno_display', { value: null, display: null });
	}
	e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'rowno', { value: null });
};

//清空top_bill, src_bill
let clearTopInfos = function (e, index) {
	let billType = e.props.cardTable.getValByKeyAndIndex(e.tableId, index, 'top_billtype');
	let topBillType = billType.value;
	if (topBillType == 'F0' || topBillType == 'F1' || topBillType == 'F2' || topBillType == 'F3') {
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'src_billid', { value: null });
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'src_billtype', { value: null });
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'src_itemid', { value: null });
		e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'src_tradetype', { value: null });
	} else {
		/*付款申请因为要分行录入付款单的金额，所以不清空来源单据的信息，以便回写上游单据的合同付款金额*/
		// 36D7 计划执行 ；收付款合同： FCT2 收款合同 FCT1 付款合同
		if (
			topBillType != null &&
			(topBillType == '36D1' || topBillType == '36D7' || topBillType == 'FCT2' || topBillType == 'FCT1')
		) {
		} else {
			e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'src_billid', { value: null });
			e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'src_billtype', { value: null });
			e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'src_itemid', { value: null });
			e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'src_tradetype', { value: null });
			e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'top_billid', { value: null });
			e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'top_billtype', { value: null });
			e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'top_itemid', { value: null });
			e.props.cardTable.setValByKeyAndIndex(e.tableId, index, 'top_tradetype', { value: null });
		}
	}
};

//收付单据联查时解析路径参数工具类
let getLinkconferQuery = function (query) {
	let theRequest = {};
	if (query.indexOf('#') != -1) {
		let strArr = query.split('#');
		if (strArr && strArr.length > 0) {
			let str = strArr[1];
			if (str.indexOf('&') != -1) {
				let strs = str.split('&');
				for (let i = 0; i < strs.length; i++) {
					theRequest[strs[i].split('=')[0]] = strs[i].split('=')[1];
				}
			} else {
				theRequest[str.split('=')[0]] = str.split('=')[1];
			}
		}

	}
	return theRequest;
};

/**
 * 设置整个表单区域编辑性
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} flag 
 */
function setFormEditable(props, mainId, moduleId, flag) {
	// if (props.form.getAllFormValue(moduleId)) {
	let meta = props.meta.getMeta();
	let items = meta[moduleId].items;
	for (let i = 0; i < items.length; i++) {
		let item = items[i];
		props.form.setFormItemsDisabled(mainId, { [item.attrcode]: !flag });
	}
	// }
}

//单据删除最后一行空行
function delBlankLine(that, tableId, billType, cardData,modelIndex) {
	let rows = cardData.body[tableId].rows;
	if (!rows || rows.length == 0) {
		toast({ color: 'danger', content: that.state.json['public-000163'] });/* 国际化处理： 子表行不能为空*/
	}
	if (rows.length == 1) {
		return
	}
	let deleteIndex = [];
	for (let i = 0; i < rows.length; i++) {
		let row = rows[i];
		if (row.status == '3') {
			continue;
		}
		if ('F3' == billType) {
			//金额承付场景可以为0
			let commpaytype = '0';
			let valueAt = row.values.commpaytype ? row.values.commpaytype.value : null;
			if (valueAt && valueAt == commpaytype) {
				return;
			}
		}
		let money;
		let money2;
		if ('F3' == billType || 'F0' == billType) {
			money = row.values.money_de.value;
			money2 = row.values.local_tax_de.value;
		} else {
			money = row.values.money_cr.value;
			money2 = row.values.local_tax_cr.value;
		}
		if ((Number(money) == Number(0)) && (Number(money2) == Number(0))) {
			deleteIndex.push(i);

		}
	}
	if (deleteIndex.length > 0) {
		that.props.cardTable.delRowsByIndex(tableId, deleteIndex);
		//删除行之后如果表体为空了关闭模态框
		if(modelIndex && deleteIndex.indexOf(modelIndex)!=-1 && that.Info.isModelSave){
			that.Info.isModelSave = false;
			that.props.cardTable.closeModel(that.tableId);
		}
		// let allVisibleRows=that.props.cardTable.getVisibleRows(that.tableId);
		// if((!allVisibleRows || allVisibleRows.length == 0) && that.Info.isModelSave){
		// 	that.Info.isModelSave = false;
		// 	that.props.cardTable.closeModel(that.tableId);
		// }
	}

}

/**
 * 获取表格fields列的数据
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} fields 
 */
function getColvalues(props, tableId, fields) {
	let col = {}
	for (var i = 0; i < fields.length; i++) {
		let key = fields[i]
		let value = props.cardTable.getColValue(tableId, key, false, false)
		if (value) {
			col[key] = value
		}
	}
	return col
}

/**
 * 获取选中行指定字段的值
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} fields 
 */
function getCheckedRowColvalues(props, tableId, fields) {
	let col = {}
	let rows = props.cardTable.getCheckedRows(tableId)
	for (var i = 0; i < fields.length; i++) {
		let key = fields[i]
		let value = []
		for (var j = 0; j < rows.length; j++) {
			value.push(rows[j].data.values[key])
		}
		if (value) {
			col[key] = value
		}
	}
	return col
}

/**
 * 获取表格所有行的rowid
 * @param {*} props 
 * @param {*} tableId 
 */
function getRowIds(props, tableId) {
	let rows = props.cardTable.getVisibleRows(tableId)
	let ids = []
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].rowid)
	}
	return ids
}


/**
 * 设置表体第一行的一些field数值， 到表头显示
 * @param {*} props 
 * @param {*} formId 
 * @param {*} tableId 
 */
function refreshChildVO2HeadVO(props, formId, tableId) {
	let arr = ["pk_tradetype", "billdate", "pk_group", "pk_fiorg", "pk_pcorg", "sett_org_v", "sett_org", "pk_billtype", "pk_tradetype", "billclass",
		"rate", "grouprate", "globalrate", "payaccount", "recaccount", "cashaccount", "objtype", "payman", "pk_rescenter", "pk_group", "checkelement", "pk_deptid",
		"pk_deptid_v", "pk_psndoc", "customer", "supplier", "pu_org", "pu_org_v", "pk_balatype", "pk_org_v", "pk_fiorg_v", "pk_pcorg_v", "so_org", "so_org_v", "cashitem", "bankrollprojet",
		"pk_subjcode", "so_psndoc", "pu_psndoc", "so_deptid", "pu_deptid", "so_deptid_v", "pu_deptid_v", "busidate", "ordercubasdoc", "costcenter", "payreason", "invoiceno", "pk_currtype"]

	let row = props.cardTable.getRowsByIndexs(tableId, 0)
	if (row) {
		let col = {}
		for (var i = 0; i < arr.length; i++) {
			let key = arr[i]
			col[key] = row[0].values[key]
		}
		props.form.setFormItemsValue(formId, col)
	}
}


export {
	setFormEditable,
	calculateHeadMoney,
	getBodyAmountValue,
	clearTopInfos,
	resetBodyPk,
	getLinkconferQuery,
	delLine,
	copyLine,
	pasteToEndLine,
	copyInner,
	deleteInner,
	pasteInner,
	delBlankLine,
	getColvalues,
	getCheckedRowColvalues,
	getRowIds,
	refreshChildVO2HeadVO,
	F0DeleteAfterEditCalculate
};
