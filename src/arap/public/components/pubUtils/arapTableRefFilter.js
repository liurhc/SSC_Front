import { getBusinessInfo, ajax } from "nc-lightapp-front";
import { isChecknoRef, reWriteCheckno } from '../pubUtils/specialFieldAfterEvent';

/**
 * 卡片态表头字段过滤 以及 代码控制可编辑性
 * @param {*} props 
 * @param {*} moduleId 当前表格的moduleId
 * @param {*} key  当前的key
 * @param {*} value 当前的value
 * @param {*} index 当前第几行
 * @param {*} record 当前行信息
 */
function bodyBeforeEvent(props, moduleId, key, value, index, record, type) {

    //增加一些不需要走编辑前过滤的字段，提高点击反应效率
    var noFilterFields = ["money_bal", "money_de", "money_cr", "notax_de", "notax_cr", "occupationmny", "nosubtax", "nosubtaxrate", "local_money_bal", "local_money_de", "local_money_cr",
        "local_notax_de", "local_notax_cr", "local_tax_de", "local_tax_cr", "groupbalance", "groupdebit", "groupcrebit", "groupnotax_de", "groupnotax_cre", "groupagentreceivelocal",
        "grouptax_de", "grouptax_cre", "globalbalance", "globaldebit", "globalcrebit", "globalnotax_de", "globalnotax_cre", "globalagentreceivelocal", "globaltax_de", "globaltax_cre",
        "price", "taxprice", "postprice", "postpricenotax", "local_price", "local_taxprice", 'taxrate', 'taxtype', "quantity_bal"]
    if (contains(noFilterFields, key)) {
        return true
    }
    if (type == 'model') {
        moduleId = "bodys_edit"
    }
    var flag = true; //用来控制单元格是否可操作
    var meta = props.meta.getMeta();
    var tradeType = record.values.pk_tradetype ? record.values.pk_tradetype.value : null;
    var billType = props.form.getFormItemsValue(this.formId, 'pk_billtype') ? props.form.getFormItemsValue(this.formId, 'pk_billtype').value : null;
    var tableObject = {
        model: {
            areaType: "table",
            areacode: null,
            rows: [record]
        },
        pageid: this.pageId
    }
    let formObject = {
        model: props.form.getAllFormValue(this.formId),
        pageid: this.pageId
    }
    let formData = JSON.stringify(formObject);
    let tableData = JSON.stringify(tableObject);
    var config = {
        itemKey: '',
        isHead: false,
        crossRuleConditionsVO: formData,//表单的内容
        crossRuleTableConditionsVO: tableData,//表格的内容
        tradeType: tradeType,//交易类型
        DataPowerOperationCode: '',//使用权
        isDataPowerEnable: 'Y',//使用权限
        pk_org: '',
        busifuncode: '',
        AppCode: '',
        orgType: '',
        refnodename: '',//参照显示名称
        accclass: '',
        pk_cust: '',
        pk_currtype: '',//货币类型
        pkOrgs: '',
        parentbilltype: '',
        datestr: '',//业务日期
        pk_accountingbook: '',//会计主账簿的id
        dateStr: '',
        GridRefActionExt: "",
        TreeRefActionExt: "",
        supplier: '',
        customer: '',
        pretype: '',//收付款性质 
        customSupplier: '',//散户
        pk_psndoc: '',//个人银行账户
    }
    meta[moduleId].items.map((item) => {
        item.isShowUnit = false;
        item.isShowDisabledData = false;
        var attrcode = item.attrcode;
        if (item.itemtype == 'refer') {
            item.isMultiSelectedEnabled = false;
            item.unitValueIsNeeded = false; //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
        }
        if (attrcode == key) {
            switch (attrcode) {
                case 'objtype'://往来对象
                    if (attrcode == 'objtype') {
                        var index = -1;
                        for (var i = 0; i < item.options.length; i++) {
                            if (item.options[i].value == '') {
                                index = i;
                                break;
                            }
                        }
                        if (index > -1) {
                            item.options.splice(index, 1);
                        }
                    }
                    if (attrcode == 'objtype' && billType == "23E1") {
                        if (value.value == "0") {
                            flag = false;
                            props.cardTable.setEditableByIndex(moduleId, index, 'objtype', false);
                        } else {
                            var index = -1;
                            for (var i = 0; i < item.options.length; i++) {
                                if (item.options[i].value == '0') {
                                    index = i;
                                    break;
                                }
                            }
                            if (index > -1) {
                                item.options.splice(index, 1);
                            }
                        }
                    } else if (attrcode == 'objtype' && billType == "23E0") {
                        if (value.value == "1") {
                            flag = false;
                            props.cardTable.setEditableByIndex(moduleId, index, 'objtype', false);
                        } else {
                            var index = -1;
                            for (var i = 0; i < item.options.length; i++) {
                                if (item.options[i].value == '1') {
                                    index = i;
                                    break;
                                }
                            }
                            if (index > -1) {
                                item.options.splice(index, 1);
                            }
                        }
                    }
                    break;
                case 'pk_org'://财务组织
                    item.queryCondition = (p) => {
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.AppCode = props.getSearchParam('c');
                        config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                        return config;
                    }
                    break;
                case 'pk_org_v'://财务组织版本
                    item.queryCondition = (p) => {
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.AppCode = props.getSearchParam('c');
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                        return config;
                    }
                    break;
                case 'pk_fiorg'://废弃财务组织
                    item.queryCondition = (p) => {
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.AppCode = props.getSearchParam('c');
                        config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                        return config;
                    }
                    break;
                case 'pk_fiorg_v'://废弃财务组织
                    item.queryCondition = (p) => {
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.AppCode = props.getSearchParam('c');
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                        return config;
                    }
                    break;
                case 'sett_org'://结算财务组织
                    item.queryCondition = (p) => {
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.AppCode = props.getSearchParam('c');
                        config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                        return config;
                    }
                    break;
                case 'sett_org_v'://结算财务组织
                    item.queryCondition = (p) => {
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.AppCode = props.getSearchParam('c');
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        config.TreeRefActionExt = 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder';
                        return config;
                    }
                    break;
                case 'scomment'://摘要
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'material'://物料
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder';
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        config.matcustcode = record.values.matcustcode ? record.values.matcustcode.value : null;//物料客户码，应收模块需要传值
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        return config;
                    }
                    break;
                case 'matcustcode'://物料
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder';
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        config.pk_customer = record.values.customer ? record.values.customer.value : null;//客户
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        return config;
                    }
                    break;
                case 'pk_payterm'://付款协议(根据财务组织)
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        return config;
                    }
                    break;
                case 'supplier'://供应商
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        //付款单和应付单
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'customer'://客户
                    item.queryCondition = (p) => {
                        //收款单和应收单
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'pk_deptid'://部门
                    item.isShowUnit = true
                    item.unitCondition = () => {
                        return {
                            pkOrgs: record.values.pk_org.value,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        }
                    }
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.busifuncode = 'all';
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'pk_deptid_v'://部门版本
                    item.isShowUnit = true
                    item.unitCondition = () => {
                        return {
                            pkOrgs: record.values.pk_org.value,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        }
                    }
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.busifuncode = 'all';
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        return config;
                    }
                    break;
                case 'pk_psndoc'://业务员
                    item.isShowUnit = true
                    item.unitCondition = () => {
                        return {
                            pkOrgs: record.values.pk_org.value,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        }
                    }
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.busifuncode = 'all';
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'taxcodeid'://税码(根据报税国和购销类型)
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.TaxcodeIdSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.TaxcodeIdSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.TaxcodeIdSqlBuilder';
                        }
                        config.itemKey = key;
                        config.taxcountryid = props.form.getFormItemsValue(this.formId, 'taxcountryid') ? props.form.getFormItemsValue(this.formId, 'taxcountryid').value : null;//报税国
                        config.buysellflag = record.values.buysellflag ? record.values.buysellflag.value : null;//购销类型
                        config.pk_org = record.values.pk_org.value;
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'pu_deptid'://业务部门
                    item.isShowUnit = false
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.busifuncode = 'all';
                        config.pk_org = record.values.pu_org ? record.values.pu_org.value : null;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'pu_deptid_v'://业务部门
                    item.isShowUnit = false
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.busifuncode = 'all';
                        config.pk_org = record.values.pu_org ? record.values.pu_org.value : null;
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                            config.DataPowerOperationCode = 'fi';//使用权组                               
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'pu_psndoc'://业务人员
                    item.isShowUnit = false
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.busifuncode = 'all';
                        config.pk_org = record.values.pu_org ? record.values.pu_org.value : null;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'cashitem'://现金流量项目
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org ? record.values.pk_org.value : null;
                        return config;
                    }
                    break;
                case 'pk_subjcode'://收支项目
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org ? record.values.pk_org.value : null;
                        return config;
                    }
                    break;
                case 'payaccount'://付款银行账户
                    if (billType == 'F0' || billType == 'F2' || billType == "23E0") {
                        var objtypeValue = record.values.objtype;
                        if (objtypeValue.value == "1") {
                            item.itemType = 'refer'
                            item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                            item.refName = this.state.json['public-000147']/* 国际化处理： 供应商银行账户*/
                        } else if (objtypeValue.value == "3") {
                            item.itemType = 'refer'
                            item.refcode = 'uapbd/refer/pubinfo/PsnbankaccGridRef/index'
                            item.refName = this.state.json['public-000148']/* 国际化处理： 个人银行账户*/
                        } else {
                            item.itemType = 'refer'
                            item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                            item.refName = this.state.json['public-000149']/* 国际化处理： 客户银行账户*/
                        }
                    }
                    item.queryCondition = (p) => {
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "23E1") {
                            //付款单和应付单
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                            }

                            config.itemKey = key;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.refnodename = this.state.json['public-000150'];/* 国际化处理： 使用权参照*/
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.pk_currtype = record.values.pk_currtype ? record.values.pk_currtype.value : null;
                            return config;
                        } else {
                            var objtypeValue = record.values.objtype.value;
                            var supplier_1 = record.values.supplier ? record.values.supplier.value : null;
                            var customer_1 = record.values.customer ? record.values.customer.value : null;
                            let psndoc = record.values.pk_psndoc ? record.values.pk_psndoc.value : getBusinessInfo().userId;
                            var pk_cust = null;
                            var accclass = null;
                            if (supplier_1 && objtypeValue == "1") {
                                pk_cust = supplier_1;
                                accclass = 3;
                            } else if (customer_1 && (objtypeValue == "0" || objtypeValue == "2")) {
                                pk_cust = customer_1;
                                accclass = 1;
                            } else if (psndoc) {
                                config.pk_psndoc = psndoc;
                            }
                            if (objtypeValue == "3") {
                                config.pk_psndoc = psndoc;
                                config.pk_cust = null;
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccPersonSqlBuilder';
                            } else if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.accclass = accclass;//3代表供应商
                            config.pk_currtype = record.values.pk_currtype ? record.values.pk_currtype.value : null;
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.pk_cust = pk_cust;
                            return config;
                        }
                    }
                    break;
                case 'recaccount'://收款银行账户
                    if (billType == 'F1' || billType == 'F3' || billType == "23E1") {
                        var objtypeValue = record.values.objtype;
                        if (objtypeValue.value == "1" || objtypeValue.value == "2") {
                            item.itemType = 'refer'
                            item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                            item.refName = this.state.json['public-000147']/* 国际化处理： 供应商银行账户*/
                        } else if (objtypeValue.value == "3") {
                            item.itemType = 'refer'
                            item.refcode = 'uapbd/refer/pubinfo/PsnbankaccGridRef/index'
                            item.refName = this.state.json['public-000148']/* 国际化处理： 个人银行账户*/
                        } else {
                            item.itemType = 'refer'
                            item.refcode = 'uapbd/refer/pub/CustBankAccGridRef/index'
                            item.refName = this.state.json['public-000149']/* 国际化处理： 客户银行账户*/
                        }
                    }
                    item.queryCondition = (p) => {
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                            //收款单和应收单
                            //付款单和应付单
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder';
                            }
                            config.itemKey = key;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.refnodename = this.state.json['public-000150'];/* 国际化处理： 使用权参照*/
                            config.pk_org = props.form.getFormItemsValue(this.formId, 'pk_org') ? props.form.getFormItemsValue(this.formId, 'pk_org').value : null;
                            config.pk_currtype = record.values.pk_currtype ? record.values.pk_currtype.value : null;
                            return config;
                        } else {
                            var objtypeValue = record.values.objtype.value;
                            var supplier_1 = record.values.supplier ? record.values.supplier.value : null;
                            var customer_1 = record.values.customer ? record.values.customer.value : null;
                            let psndoc = record.values.pk_psndoc ? record.values.pk_psndoc.value : getBusinessInfo().userId;
                            var pk_cust = null;
                            var accclass = null;
                            if (supplier_1 && (objtypeValue == "1" || objtypeValue == "2")) {
                                pk_cust = supplier_1;
                                accclass = 3;
                            } else if (customer_1 && objtypeValue == "0") {
                                pk_cust = customer_1;
                                accclass = 1;
                            } else if (psndoc) {
                                config.pk_psndoc = psndoc;
                            }
                            if (objtypeValue == "3") {
                                config.pk_psndoc = psndoc;
                                config.pk_cust = null;
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankaccPersonSqlBuilder';
                            } else if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.pk_cust = pk_cust;
                            config.itemKey = key;
                            config.DataPowerOperationCode = 'fi';//使用权组
                            config.accclass = accclass;//3代表供应商
                            config.pk_currtype = record.values.pk_currtype ? record.values.pk_currtype.value : null;
                            
                            // 付款单-收款银行账户参照过滤条件中去掉币种 -20191118
                            if(props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3"){
                                delete config.pk_currtype;
                            }

                            return config;
                        }
                    }
                    break;
                case 'ordercubasdoc'://订单供应商(和供应商联动)
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'freecust'://散户  根据供应商和客户判断可编辑性，根据供应商和客户过滤
                    var supplier_2 = record.values.supplier ? record.values.supplier.value : null;
                    var customer_2 = record.values.customer ? record.values.customer.value : null;
                    if (supplier_2 != null || customer_2 != null) {
                        ajax({
                            url: '/nccloud/arap/ref/freecustcontrol.do',
                            async: false,
                            data: {
                                supplier: supplier_2,
                                customer: customer_2
                            },
                            success: (res) => {
                                if (res.success) {
                                    flag = res.data;
                                }
                            }
                        })
                    }
                    if (flag) {
                        item.queryCondition = (p) => {
                            if (flag) {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.FreeCustSqlBuilder';
                                config.itemKey = key;
                                config.pk_org = record.values.pk_org.value;
                                if (supplier_2 != null) {
                                    config.customSupplier = supplier_2;
                                } else {
                                    config.customSupplier = customer_2;
                                }
                                config.supplier = config.customSupplier;
                                config.customer = config.customSupplier;
                                config.crossRuleTableConditionsVO = '';
                                return config;
                            }
                        }
                    }
                    break;
                case 'checkelement'://责任核算要素(pk_pcorg如果利润中心不存在就不可编辑，如果利润中心存在，设置pk_org为利润中心)
                    var pk_pcorg = record.values.pk_pcorg ? record.values.pk_pcorg.value : null;
                    if (!pk_pcorg) {
                        flag = false;
                    }
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = pk_pcorg;
                        return config;
                    }
                    break;
                case 'project_task'://项目任务
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.ProjectTaskSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.ProjectTaskSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.ProjectTaskSqlBuilder';
                        }
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        config.project = record.values.project ? record.values.project.value : null;
                        return config;
                    }
                    break;
                case 'costcenter'://成本中心
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CostCenterSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CostCenterSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CostCenterSqlBuilder';
                        }
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_pcorg ? record.values.pk_pcorg.value : null;
                        return config;
                    }
                    break;
                case 'project'://项目
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'material_src'://原始物料(同物料)
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_org = record.values.pk_org.value;
                        config.matcustcode = record.values.matcustcode ? record.values.matcustcode.value : null;//物料客户码，应收模块需要传值
                        return config;
                    }
                    break;
                case 'checktype'://票据类型
                    //根据是否期初判断可编辑性
                    var isinit_2 = props.form.getFormItemsValue(this.formId, 'isinit') ? props.form.getFormItemsValue(this.formId, 'isinit').value : null;
                    if (isinit_2) { //如果是期初
                        flag = false;
                        break
                    }
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.pk_curr = record.values.pk_currtype.value;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'checkno'://票据号
                    // checkno不可编辑,并隐藏，编辑checkno_display
                    flag = false;
                    break;
                case 'checkno_display'://票据号
                    //根据是否期初判断可编辑性
                    var isinit_1 = props.form.getFormItemsValue(this.formId, 'isinit') ? props.form.getFormItemsValue(this.formId, 'isinit').value : null;
                    if (isinit_1) { //如果是期初
                        flag = false;
                        break
                    }
                    //如果票据类型未输入，票据号不可编辑
                    var checktype = record.values.checktype.value
                    if (!checktype) {
                        flag = false;
                        break
                    }
                    var billclass = props.form.getFormItemsValue(this.formId, 'billclass') ? props.form.getFormItemsValue(this.formId, 'billclass').value : null;
                    let needref = isChecknoRef(checktype, record.values.pk_org.value);
                    if (needref && item.itemtype != 'refer') {
                        item.itemtype = 'refer';
                        if (billclass == 'fk') {
                            item.refcode = 'fbm/refer/fbm/Bill4CmpPayGridRef/index.js';
                        } else if (billclass == 'sk') {
                            item.refcode = 'fbm/refer/fbm/Bill4CmpReceiveGridRef/index.js';
                        }
                        reWriteCheckno(props, moduleId)
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.pk_org = record.values.pk_org.value;
                            config.pk_curr = record.values.pk_currtype.value;
                            config.fbmbilltype = record.values.checktype.value;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                    } else if (needref && item.itemtype == 'refer') {
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            } else {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.pk_org = record.values.pk_org.value;
                            config.pk_curr = record.values.pk_currtype.value;
                            config.fbmbilltype = record.values.checktype.value;
                            if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                                config.DataPowerOperationCode = 'fi';//使用权组
                                return config;
                            } else {
                                return config;
                            }
                        }
                    } else {
                        item.itemtype = 'input';
                        item.refcode = null;
                    }
                    break;
                case 'cbs'://CBS
                    var project = record.values.project ? record.values.project.value : null;//项目
                    if (!project) {//如果项目不存在，cbs不可编辑
                        flag = false;
                    } else {//根据pk_org和项目去过滤(未完成)
                        flag = true;
                        item.queryCondition = (p) => {
                            if (p) {
                                if (p.refType == 'grid' || p.refType == 'gridTree') {
                                    config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                } else if (p.refType == 'tree') {
                                    config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                }
                            }
                            config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.itemKey = key;
                            config.pk_project = record.values.project.value;
                            config.pk_org = record.values.pk_org.value;
                            return config;
                        }
                    }
                    break;
                case 'rate'://本币汇率
                    ajax({
                        url: '/nccloud/arap/ref/ratecontrol.do',
                        data: {
                            pk_org: record.values.pk_org ? record.values.pk_org.value : null,
                            pk_currtype: record.values.pk_currtype ? record.values.pk_currtype.value : null,
                            ratekey: key
                        },
                        async: false,
                        success: (res) => {
                            if (res.success) {
                                flag = res.data;
                                setTimeout(() => {
                                    props.cardTable.setEditableByIndex(moduleId, index, 'rate', flag);
                                }, 0);
                            }
                        }
                    })
                    break;
                case 'grouprate'://集团本币汇率
                    ajax({
                        url: '/nccloud/arap/ref/ratecontrol.do',
                        data: {
                            pk_org: record.values.pk_org ? record.values.pk_org.value : null,
                            pk_currtype: record.values.pk_currtype ? record.values.pk_currtype.value : null,
                            ratekey: key
                        },
                        async: false,
                        success: (res) => {
                            if (res.success) {
                                flag = res.data;
                                setTimeout(() => {
                                    props.cardTable.setEditableByIndex(moduleId, index, 'rate', flag);
                                }, 0);
                            }
                        }
                    })
                    break;
                case 'globalrate'://全局本币汇率
                    ajax({
                        url: '/nccloud/arap/ref/ratecontrol.do',
                        data: {
                            pk_org: record.values.pk_org ? record.values.pk_org.value : null,
                            pk_currtype: record.values.pk_currtype ? record.values.pk_currtype.value : null,
                            ratekey: key
                        },
                        async: false,
                        success: (res) => {
                            if (res.success) {
                                flag = res.data;
                                setTimeout(() => {
                                    props.cardTable.setEditableByIndex(moduleId, index, 'rate', flag);
                                }, 0);
                            }
                        }
                    })
                    break;
                case 'settlemoney'://拉单金额编辑前控制
                    var settlecurr = record.values.settlecurr ? record.values.settlecurr.value : null;//拉单上游币种
                    var pk_billtype = record.values.pk_billtype ? record.values.pk_billtype.value : null;//单据类型
                    var top_billtype = record.values.top_billtype ? record.values.top_billtype.value : null;//上层交易类型
                    if (settlecurr && pk_billtype && top_billtype) {//如果都存在就可编辑
                        flag = true;
                    } else {
                        flag = false;
                    }
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'refuse_reason'://拒付理由的可编辑性
                    var commpaytype = record.values.commpaytype ? record.values.commpaytype.value : null;
                    if (commpaytype == 0) {// 全额承付
                        flag = false;
                    } else if (commpaytype == 1) {// 全额拒付
                        flag = false;
                    } else if (commpaytype == 2) {// 部分承付
                        flag = false;
                    }
                    //修改“付款单承付时，多表体只要有1个全额拒付，就会导致所有行金额字段置灰不可编辑
                    //该条需求目前不知如何做
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'caltaxmny':
                    var buysellflag = record.values.buysellflag ? record.values.buysellflag.value : null; //购销类型
                    if (buysellflag) {
                        if (buysellflag == 4 || buysellflag == 3) {//4代表进口采购 3代表出口销售 
                            flag = true;
                        } else {
                            flag = false;
                        }
                    }
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'so_deptid'://业务部门(根据销售组织)   
                    item.isShowUnit = false
                    item.unitCondition = () => {
                        return {
                            pkOrgs: record.values.so_org.value,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        }
                    }
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.so_org ? record.values.so_org.value : null;
                        config.busifuncode = 'all';
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                            //收款单和应收单
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'so_deptid_v'://业务部门(根据销售组织)    
                    item.isShowUnit = false
                    item.unitCondition = () => {
                        return {
                            pkOrgs: record.values.so_org.value,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        }
                    }
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.so_org ? record.values.so_org.value : null;
                        config.busifuncode = 'all';
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                            //收款单和应收单
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'so_psndoc'://业务人员(根据销售组织)
                    item.isShowUnit = false
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.so_org ? record.values.so_org.value : null;
                        config.busifuncode = 'all';
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                            //收款单和应收单
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'pk_balatype'://结算方式(根据pk_billtype)
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.BalatypeSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.BalatypeSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.BalatypeSqlBuilder';
                        }

                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.pk_billtype = record.values.pk_billtype ? record.values.pk_billtype.value : null;
                        return config;
                    }
                    break;
                case 'pu_org'://业务组织(业务单元（财务组织委托）) 
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                        }
                        config.itemKey = key;
                        config.pkOrgs = record.values.pk_org.value;
                        return config;
                    }
                    break;
                case 'pu_org_v'://业务单元版本(财务组织委托)@auto zhangygw --- ok 

                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                        }

                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        return config;
                    }
                    break;
                case 'bankrollprojet'://资金计划项目
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.BankRollProjetGridSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.BankRollProjetGridSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.BankRollProjetGridSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        return config;
                    }
                    break;
                case 'so_deptid'://业务部门
                    item.isShowUnit = false
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.busifuncode = 'all';
                        config.pk_org = record.values.so_org ? record.values.so_org.value : null;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'so_deptid_v'://业务部门
                    item.isShowUnit = false
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.busifuncode = 'all';
                        config.pk_org = record.values.so_org ? record.values.so_org.value : null;
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'so_org'://业务组织(业务单元（财务组织委托）) 
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                        }

                        config.itemKey = key;
                        config.pkOrgs = record.values.pk_org.value;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'so_org_v'://业务单元版本(财务组织委托)

                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.OrgSqlBuilder';
                        }

                        config.itemKey = key;
                        config.pkOrgs = record.values.pk_org.value;
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'sendcountryid'://发货国
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'rececountryid'://收货国
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F0") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'pk_pcorg'://利润中心
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        return config;
                    }
                    break;
                case 'pk_pcorg_v'://利润中心版本
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.VersionStartDate = props.form.getFormItemsValue(this.formId, 'billdate') ? props.form.getFormItemsValue(this.formId, 'billdate').value : null;
                        return config;
                    }
                    break;
                case 'productline'://产品线
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        return config;
                    }
                    break;
                case 'pk_currtype'://币种
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        return config;
                    }
                    break;
                case 'cashaccount'://现金账户
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CashBankSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CashBankSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CashBankSqlBuilder';
                        }

                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.pk_currtype = record.values.pk_currtype ? record.values.pk_currtype.value : null;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'commpayer'://承付人
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'facard'://固定资产卡片号
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';

                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F1" || props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F3") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config;
                        }
                    }
                    break;
                case 'payman'://支付人
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        if (props.form.getFormItemsValue(this.formId, 'pk_billtype').value == "F2") {
                            config.DataPowerOperationCode = 'fi';//使用权组
                            return config;
                        } else {
                            return config
                        }
                    }
                    break;
                case 'pk_tradetypeid'://交易类型
                    item.refName = this.state.json['public-000151']/* 国际化处理： 交易类型参照*/
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.parentbilltype = props.form.getFormItemsValue(this.formId, 'pk_billtype').value;
                        return config
                    }
                    break;
                case 'so_ordertype'://销售订单类型
                    item.refName = this.state.json['public-000151']/* 国际化处理： 交易类型参照*/
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        } else {
                            config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        config.parentbilltype = "30,32,38,4310,4331";
                        return config
                    }
                    break;
                case 'subjcode'://科目
                    item.queryCondition = (p) => {
                        var pkAccountingbook = null;
                        ajax({
                            url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
                            data: {
                                pk_org: record.values.pk_org.value,
                            },
                            async: false,
                            success: (res) => {
                                if (res.success) {
                                    pkAccountingbook = res.data;
                                }
                            }
                        })
                        config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        config.pk_accountingbook = pkAccountingbook;
                        config.dateStr = props.form.getFormItemsValue(this.formId, 'billdate') ? (props.form.getFormItemsValue(this.formId, 'billdate').value).substring(0,10) : null;
                        config.DataPowerOperationCode = 'fi';//使用权组
                        return config;
                    }
                    break;
                default:
                    item.isShowUnit = true
                    item.unitCondition = () => {
                        return {
                            pkOrgs: record.values.pk_org.value,
                            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                        }
                    }
                    item.queryCondition = (p) => {
                        if (p) {
                            if (p.refType == 'grid' || p.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            } else if (p.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                            }
                        }
                        config.UsualGridRefActionExt = 'nccloud.web.arap.ref.before.CrossRuleSqlBuilder';
                        config.itemKey = key;
                        config.pk_org = record.values.pk_org.value;
                        return config;
                    }
                    break;
            }
        }
    })
    props.meta.setMeta(meta);
    props.renderItem("table", moduleId, 'checkno', null);
    return flag; //默认单元格都可操作
}
/**
 * 是否子集
 * @param {*} array 
 * @param {*} item 
 */
function contains(array, item) {
    return array.indexOf(item) > -1;
}


export { bodyBeforeEvent }
