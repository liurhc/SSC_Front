let updatePandC = function(that) {
	let data = that.props.ViewModel.getData('WholeVerifyDatas');
	let param = window.parent.location.hash.split('?')[1];
	let Arr = param && param.split('&');
	if (Arr && Arr instanceof Array && Arr.length > 0) {
		let newArr = Arr.map((item) => {
			if (item.indexOf('=') != -1 && item.split('=') && item.split('=') instanceof Array) {
				if (item.split('=')[0] === 'ifr') {
					let oldIfr = decodeURIComponent(decodeURIComponent(item.split('=')[1]));
					let newIfr = oldIfr.split('#')[0] + '#/card';
					item = `ifr=${encodeURIComponent(encodeURIComponent(newIfr))}`;
				}
				if (item.split('=')[0] === 'p') {
					item = `p=${data.pageId}`;
				}
				if (item.split('=')[0] === 'c') {
					item = `c=${data.appcode}`;
				}
				return item;
			}
		});
		let newParams = newArr.join('&');
		//修改工作台路径
		window.parent.location.hash = `#/ifr?${newParams}`;
		that.props.setUrlParam({ id: data.pk_bill.value });
	}
};

export { updatePandC };
