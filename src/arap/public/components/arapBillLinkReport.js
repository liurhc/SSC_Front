import { ajax } from 'nc-lightapp-front';
/**
 * 单据联查余额表
 * @author  xiejhk
 * @param {*} pk_bill 单据主键
 * @param {*} billType  单据类型
 * @param {*} objtype 往来对象
 */
let appcode;
export default function arapLinkReport(props, pk_bill, billType, objtype) {
	if (pk_bill) {
		ajax({
			url: '/nccloud/arap/arappub/arapBillLinkReport.do',
			data: {
				pk_bill: pk_bill,
				billType: billType,
				objType: objtype
			},
			async: false,
			success: (res) => {
				if (res.success) {
					let finalLinkData = transData(res.data);
					sessionStorage.setItem('LinkReport', JSON.stringify(finalLinkData.queryInfo));
					sessionStorage.setItem('sessionCustcondition', JSON.stringify(finalLinkData.custconditions));
					props.openTo('/nccloud/resources/arap/accountquery/report/balanceData/index.html', {
						appcode: appcode,
						pagecode: appcode + 'page'
					});
				}
			}
		});
	}
}

/**
 * 解析并构建单据联查余额表查询参数
 */
function transData(queryVO) {
	let queryInfo = {
		logic: 'and',
		conditions: [
			{
				field: 'not_search',
				oprtype: '=',
				value: { firstvalue: '', secondvalue: '' }
			}
		],
		oprtype: '=',
		display: null
	};
	let custconditions = {
		logic: 'and',
		conditions: []
	};
	for (let key in queryVO) {
		let value = queryVO[key];
		switch (key) {
			//个别字段特殊处理
			case 'pk_orgs':
				queryInfo.conditions.push({
					field: key,
					datatype: '204',
					isIncludeSub: false,
					oprtype: '=',
					value: { firstvalue: value[0], secondvalue: null }
				});
				break;
			case 'repInitContext':
				appcode = value.m_headVo.node_code;
				break;
			case 'showFrontN':
			case 'userObject':
			case 'pk_group':
			case 'syscode':
			case 'alarmDay':
				break;

			case 'qryObjs':
				let vals = value[0];
				let objs = vals.values.join(',');
				let val = [ vals.billFieldName, objs.replace(/,/g, '@'), false, false ];
				custconditions.conditions.push({
					field: 'queryObjsVal',
					oprtype: '=',
					value: { firstvalue: val.join(','), secondvalue: null }
				});
				break;
			case 'beginDate':
			case 'endDate':
				queryInfo.conditions.push({
					field: key,
					datatype: '204',
					isIncludeSub: false,
					oprtype: '=',
					value: { firstvalue: value, secondvalue: null }
				});
				break;
			case 'nooccur_noshow':
			case 'nobal_nooccur_noshow':
				queryInfo.conditions.push({
					field: key,
					datatype: '32',
					isIncludeSub: false,
					oprtype: '=',
					value: { firstvalue: value, secondvalue: null }
				});
				break;
			default:
				queryInfo.conditions.push({
					field: key,
					datatype: '203',
					isIncludeSub: false,
					oprtype: '=',
					value: { firstvalue: value, secondvalue: null }
				});
				break;
		}
	}

	let finalData = {
		queryInfo: queryInfo,
		custconditions: custconditions
	};
	//return JSON.stringify(finalData);

	return finalData;
}
