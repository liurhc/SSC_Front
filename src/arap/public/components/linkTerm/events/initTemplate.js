import tableButtonClick from './tableButtonClick';
export default function(props) {
	let self = this;
	if (props.moduleId) {
		let appcode = null;
		let pagecode = null;
		let moduleId = props.moduleId;
		if (moduleId == '2008') {
			appcode = '20082004';
			pagecode = '20082004_LIST';
		} else if (moduleId == '2006') {
			appcode = '20062004';
			pagecode = '20062004_LIST';
		} else {
			return;
		}
		props.createUIDom(
			{
				pagecode: pagecode, //页面id
				appcode: appcode //注册按钮的id
			},
			function(data) {
				if (data) {
					if (data.template) {
						let meta = data.template;
						meta = modifierMeta(props, meta, self.tableId, self);
						props.meta.setMeta(meta);
					}
					if (data.button) {
						let button = data.button;
						props.button.setButtons(button);
					}
				}
			}
		);
	} else {
	}
}

function modifierMeta(props, meta, tableId, self) {
	//添加操作列
	meta[tableId].items.push({
		label: self.state.json['public-000044'] /* 国际化处理： 操作*/,
		itemtype: 'customer',
		attrcode: 'opr',
		width: '80px',
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let status = props.editTable.getStatus(tableId);
			let buttonAry = status === 'browse' ? [] : [ 'Delete_inner' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index)
			});
		}
	});
	return meta;
}
