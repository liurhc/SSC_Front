import { ajax,base ,toast } from 'nc-lightapp-front';
let { Message } = base;

export default function buttonClick(props, id) {
	switch (id) {
		case 'Edit'://修改
			props.editTable.setStatus(this.tableId,'edit');
			this.toggleShow();
			break;
		case 'Cancel'://取消
			this.cancel();
			this.toggleShow();
			break;
		case 'AddLine'://增行
			let selectPk = props.seletedPks;
			let pk_items = null;
			if(selectPk && selectPk.length > 0 ){
				pk_items = selectPk[0];
			}
			ajax({
                url: '/nccloud/arap/arappub/termaddrow.do',
                data:{
					moduleId : props.moduleId,
					billType : props.billType,
					pk_item : pk_items,
					pk_bill : props.pk_bill
				},
                success: (res) => {
                    if (res.data) {
                        props.editTable.addRow(this.tableId, undefined,false, res.data[this.tableId].rows[0].values);
                    } else {
                        props.editTable.addRow(this.tableId,undefined,false,undefined);
                    }
                }
            });
			break;
		case 'Close'://关闭
			this.closeModel();
			break;
		case 'Save'://保存
			let model = props.editTable.getAllData(this.tableId);
			if(model.rows.length == 0 ){
				toast({ color: 'warning', content: this.state.json['public-000067'] });/* 国际化处理： 收付款协议的行不能为空!*/
				return;
			}
			//selectPks是选中子表行的pk ，列表选中不了，所以selectPks未定义，选中多行的时候不可能点击保存，
			//所以只有选中一行的时候可以修改保存，所以取值selectPks[0]，
			//当表体只有一行的时候，也可以修改保存，这个时候后台取值子表的pk_item就可以
			let selectPks = props.seletedPks;
			let pk_item = null;
			if(selectPks && selectPks.length > 0 ){
				pk_item = selectPks[0];
			}
			let data ={
				model : props.editTable.getAllData(this.tableId),
				moduleId : props.moduleId,
				billType : props.billType,
				pk_item : pk_item,
				pk_bill : props.pk_bill
			}
			ajax({
				url: '/nccloud/arap/arappub/linktermsave.do',
				data: data,
				success: (res) => {
					if(res.data){
						this.props.setFormTsVal(res.data.ts)
						this.setState({
							tableData : res.data.grid
						}, () => {
							props.editTable.setTableData(this.tableId, res.data.grid[this.tableId]);
							props.editTable.setStatus(this.tableId,'browse');
							this.toggleShow();
							toast({ color: 'success', content: this.state.json['public-000068'] });/* 国际化处理： 保存数据成功*/
						})
					}
				}
			});
			break;
		default:
			break;
		
	}
}
