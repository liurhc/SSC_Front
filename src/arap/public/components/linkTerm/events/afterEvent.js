import { ajax } from 'nc-lightapp-front';

export default function afterEvent(props, moduleId, key, value, changedrows, index, lineInfo, g) {
	if(key == 'accRate' || key == 'prepay' || key == 'insurance' || key == 'innerctldeferdays'){//百分数,预收付标志,质保金,内控到期日
		return;
	}
	if(!isNaN(changedrows[0].oldvalue.value)){
		if(Number(changedrows[0].newvalue.value) == Number(changedrows[0].oldvalue.value)){
			return;
		}
	}else{
		if(changedrows[0].newvalue.value == changedrows[0].oldvalue.value){
			return;
		}
	}
	
	//selectPks是选中子表行的pk ，列表选中不了，所以selectPks未定义，选中多行的时候不可能点击保存，
	//所以只有选中一行的时候可以修改保存，所以取值selectPks[0]，
	//当表体只有一行的时候，也可以修改保存，这个时候后台取值子表的pk_item就可以
	let seletedPks = props.seletedPks;
	let pk_item = null;
	if (seletedPks && seletedPks.length > 0) {
		pk_item = seletedPks[0];
	}
	let data = {
		key : key,
		model:props.editTable.getAllData(this.tableId),
		index : index,
		moduleId: props.moduleId,
		billType: props.billType,
		pk_item: pk_item,
		pk_bill: props.pk_bill
	}
	ajax({
		url: '/nccloud/arap/arappub/termafterevent.do',
		data: data,
		success: (res) => {
			if (res.data) {
				props.editTable.setTableData(this.tableId, res.data[this.tableId],false);
			}
		}
	});



}
