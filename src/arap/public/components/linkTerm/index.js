// 单表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,getMultiLang } from 'nc-lightapp-front';
let { NCButton } = base;
const { NCBreadcrumb, NCModal } = base;
const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
import { buttonClick, initTemplate, afterEvent } from './events';
import { tableId } from './constants';
import './index.less';

class LinkTerm extends Component {
	constructor(props) {
		super(props);
		this.tableId = tableId;
		this.state = {
			allButtonsKey: [ 'Edit', 'Cancel', 'AddLine', 'Save' ],
			tableData: [], //用于保存表格数据
			allData: null, //用于保存父组件传递的数据
			json: {}
		};
		this.Info = {
			isEdit: false //表格是否允许修改，默认false
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({json:json},() => {
				initTemplate.call(this, this.props);
			});
		}
		getMultiLang({moduleId:'public',domainName :'arap',currentLocale:'simpchn',callback});
	}
	componentDidMount() {
		
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.linkTermData.grid && nextProps.show) {
			this.Info.isEdit = nextProps.linkTermData.isEdit;
			//父组件传入的数据改变，渲染   子组件的表格数据改变 也渲染
			if (this.state.tableData.length == 0 && nextProps.linkTermData != this.state.allData) {
				this.setState({
					tableData: nextProps.linkTermData.grid,
					allData: nextProps.linkTermData
				},()=>{
					this.props.editTable.setTableData(this.tableId, nextProps.linkTermData.grid[this.tableId]);
					if (!nextProps.linkTermData.isEdit) {
						this.props.button.setButtonVisible(this.state.allButtonsKey, false); //设置不可修改
					} else if (this.props.editTable.getStatus(this.tableId) == 'edit') {
						this.props.editTable.setStatus(this.tableId, 'browse');
						this.toggleShow();
					} else {
						this.toggleShow();
					}
				});
				
			}
		}
	}

	//切换表格状态改变按钮
	toggleShow = () => {
		let status = this.props.editTable.getStatus(this.tableId);
		let trueBtn = []; //可见的按钮
		let falseBtn = []; //不可见的按钮
		for (let i = 0; i < this.state.allButtonsKey.length; i++) {
			let key = this.state.allButtonsKey[i];
			switch (key) {
				case 'Edit': //修改
					if (status == 'browse') {
						trueBtn.push(key);
					} else {
						falseBtn.push(key);
					}
					break;
				case 'Save': //保存
					if (status == 'edit') {
						trueBtn.push(key);
					} else {
						falseBtn.push(key);
					}
					break;
				case 'Cancel': //取消
					if (status == 'edit') {
						trueBtn.push(key);
					} else {
						falseBtn.push(key);
					}
					break;
				case 'AddLine': //增行
					if (status == 'edit') {
						trueBtn.push(key);
					} else {
						falseBtn.push(key);
					}
					break;
			}
		}
		this.props.button.setButtonVisible(trueBtn, true);
		this.props.button.setButtonVisible(falseBtn, false);
	};

	//取消修改
	cancel = () => {
		//重新渲染数据
		//this.props.editTable.setTableData(this.tableId, this.state.tableData[this.tableId]);
		
		this.props.editTable.cancelEdit(this.tableId);
		//设置表格状态为浏览态
		//this.props.editTable.setStatus(this.tableId, 'browse');
	};

	closeModel = () => {
		//清空数据
		this.setState({
			tableData: [],
			allData:null
		});
		this.props.handleModel(); //关闭模态框
	};

	render() {
		let { show, editTable, button, linkTermData } = this.props;
		let { createEditTable } = editTable;
		let { createButtonApp } = button;
		return (
			<NCModal id="linkTerm-area" show={show} size={'lg'} onHide={this.closeModel} backdrop={'static'}>
				<NCModal.Header closeButton>
					<NCModal.Title>
						<h2 className="title-search-detail">{this.state.json['public-000069']}</h2>{/* 国际化处理： 收付款协议*/}
					</NCModal.Title>
				</NCModal.Header>
				<NCModal.Body>
					{this.Info.isEdit && (
						<div className="table-operation-btn">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					)}
					<div className="table-area">
						<div className={this.Info.isEdit ? '' : 'operation-table'}>
							{createEditTable(this.tableId, {
								showIndex: true,
								onAfterEvent: afterEvent.bind(this)
								//params: linkTermData.grid
							})}
						</div>
					</div>
				</NCModal.Body>
				<NCModal.Footer>
					<NCButton onClick={this.closeModel}> {this.state.json['public-000070']} </NCButton>{/* 国际化处理： 关闭*/}
				</NCModal.Footer>
			</NCModal>
		);
	}
}

LinkTerm = createPage(
	{
		//initTemplate: initTemplate
	}
)(LinkTerm);

export default LinkTerm;
