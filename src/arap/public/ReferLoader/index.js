import { Component } from 'react';
import createScript from '../components/uapRefer';
import './index.less';

/**
 * 参照加载组件
 */
export default class ReferLoader extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	componentWillReceiveProps(nextProps) {
		// 参照类型改变时，清空已有参照，否则会走react更新的生命周期，不会走构造函数
		if (this.props.tag !== nextProps.tag) {
			this.setState({
				[nextProps.tag]: null
			});
		}
	}

	render() {
		let props = this.props;
		let state = this.state;
		let refer = null;
		if (!state[props.tag]) {
			createScript.call(this, props.refcode, props.tag);
		} else {
			refer = state[props.tag] ? state[props.tag](props) : <div />;
		}
		return (
			<div className="refer-wrapper">
				{props.showStar ? <span className="required-star">*</span> : null}
				{refer}
			</div>
		);
	}
}
