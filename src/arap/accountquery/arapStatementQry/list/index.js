import { base, createPage, ajax, print, getMultiLang,createPageIcon,getBusinessInfo } from 'nc-lightapp-front';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { searchId, headTableCode, bodyTableCode, nodekey, queryFormCode } from './constants';
import { afterEvent, buttonClick, initTemplate, searchBtnClick } from './events';
import { headButton } from '../../../public/components/pubUtils/buttonName';
import './index.less';

class List extends Component {
	constructor(props) {
		super(props);
		this.searchId = searchId;
		this.queryFormCode = queryFormCode;
		this.headTableCode = headTableCode;
		this.bodyTableCode = bodyTableCode;
		let { search } = this.props;
		let { setSearchValByField, getAllSearchData } = search;
		this.setSearchValByField = setSearchValByField; //设置查询区某个字段值
		this.getAllSearchData = getAllSearchData; //获取查询区所有字段数据
		this.printData = {
			funcode: props.getSearchParam('c'),
			billtype: '', //单据类型
			appcode: props.getSearchParam('c'), //功能节点编码，即模板编码
			nodekey: nodekey, //模板节点标识
			oids: [], // 功能节点的数据主键
			userjson: ''
		};
		this.state = {
			alldata: [],
			checkedDatas: [],
			json: {}
		};
	}
	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				this.props.button.setButtonDisabled(headButton.Print, true);
			});
		}
		getMultiLang({ moduleId: '20061905', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	//主表行点击方法
	RowClick = (props, moduleId, record, index) => {
		let backData = {
			pageid: props.getSearchParam('p'),
			model: {
				areaType: 'table',
				pageinfo: null,
				areacode:'',
				rows: []
			}
		};
		backData.model.rows = this.state.alldata[this.bodyTableCode][index][this.bodyTableCode].rows;
		backData.model.areacode = this.state.alldata[this.bodyTableCode][index][this.bodyTableCode].areacode;
		ajax({
			url: '/nccloud/arap/arappub/arapStatementTranslateChildGrid.do',
			data: backData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						this.props.cardTable.setTableData(this.bodyTableCode, res.data[this.bodyTableCode]);
						//储存所有子表主键，用于打印
						this.state.checkedDatas = res.data[this.bodyTableCode].rows;
						let pks = {
							pageid: props.getSearchParam('p'),
							model: {
								areaType: 'table',
								pageinfo: null,
								rows: []
							}
						};
						//去除非法字段
						delete record.key;

						//带入打印数据
						pks.model.rows = [
							{
								rowid: null,
								status: '0',
								values: record.values
							}
						];
						this.printData.userjson = JSON.stringify(pks);
					}
				}
			}
		});


		this.props.button.setButtonDisabled(headButton.Print, false);

	};

	//点击高级面板中的查询方案事件 ,返回查询方案信息，用于业务组为自定义查询区赋值
	clickPlanEve = (data) => {
		let formData = data.conditionobj4web.nonpublic.rows[0].values;
		this.props.form.setFormItemsValue(this.queryFormCode, formData);
	};
	//保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
	saveSearchPlan = () => {
		return this.props.form.getAllFormValue(this.queryFormCode);
	};

	//打印
	onPrint = () => {
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/arap/report/printData.do', //后台服务url
			this.printData,
			false
		);
	};
	// 添加查询区form
	addAdvBody = () => {
		let { createForm } = this.props.form;
		return (
			<div>
				{createForm(queryFormCode, {
					onAfterEvent: afterEvent.bind(this)
				})}
			</div>
		);
	};
	//高级查询框内清空值按钮
	clearSearchEve = () => {
		let props = this.props;
		this.props.form.EmptyAllFormValue(queryFormCode);
		let appcodenumber = props.getSearchParam('c').substring(0,4);
            //给下拉框赋默认值
            if('2006' == appcodenumber){
                props.form.setFormItemsValue(queryFormCode, { 'assoObj': { value: '0',display: this.state.json['20061905-000000'] } });/* 国际化处理： 客户*/
                props.form.setFormItemsValue(queryFormCode, { 'anaDate': { value: 'billdate',display: this.state.json['20061905-000001'] } });/* 国际化处理： 单据日期*/
                props.form.setFormItemsValue(queryFormCode, { 'anaPattern': { value: 'final',display: this.state.json['20061905-000002'] } });/* 国际化处理： 最终余额*/
                props.form.setFormItemsValue(queryFormCode, { 'qryScope': { value: 'allrec',display: this.state.json['20061905-000003'] } });/* 国际化处理： 全部应收*/
                props.form.setFormItemsValue(queryFormCode, { 'billState': { value: 'all',display: this.state.json['20061905-000004'] } });/* 国际化处理： 全部*/
            }else{
                props.form.setFormItemsValue(queryFormCode, { 'assoObj': { value: '1',display: this.state.json['20061905-000005'] } });/* 国际化处理： 供应商*/
                props.form.setFormItemsValue(queryFormCode, { 'anaDate': { value: 'billdate',display: this.state.json['20061905-000001'] } });/* 国际化处理： 单据日期*/
                props.form.setFormItemsValue(queryFormCode, { 'anaPattern': { value: 'final',display: this.state.json['20061905-000002'] } });/* 国际化处理： 最终余额*/
                props.form.setFormItemsValue(queryFormCode, { 'qryScope': { value: 'allpay',display: this.state.json['20061905-000006'] } });/* 国际化处理： 全部应付*/
                props.form.setFormItemsValue(queryFormCode, { 'billState': { value: 'all',display: this.state.json['20061905-000004'] } });/* 国际化处理： 全部*/
			}
			let beginTime = props.form.getFormItemsValue(queryFormCode, 'beginDate');
			let endTime = props.form.getFormItemsValue(queryFormCode, 'endDate');
			let date = getBusinessInfo().businessDate.substring(0, 10)
			let beginDate = date.substring(0, 7) + '-01 00:00:00'
			let endDate = date + ' 23:59:59'
            if (beginTime.value ==null) {
                props.form.setFormItemsValue(queryFormCode, { 'beginDate': { value: beginDate,display: beginDate } });
            }
            if (endTime.value ==null) {
                props.form.setFormItemsValue(queryFormCode, { 'endDate': { value: endDate,display: endDate } });
            }
            let dateline = props.form.getFormItemsValue(queryFormCode, 'dateline');
            if (dateline.value ==null) {
                props.form.setFormItemsValue(queryFormCode, { 'dateline': { value: endDate,display: endDate } });
			}
			props.form.setFormItemsValue(queryFormCode, { 'isShowMatCustCode': { value: false} });//
	}

	render() {
		let appcodenumber = this.props.getSearchParam('c').substring(0, 4);
		let appname = appcodenumber == '2006' ? this.state.json['20061905-000012'] : this.state.json['20061905-000013'];/* 国际化处理： 应收对账单,应付对账单*/
		let { cardTable, search } = this.props;
		let { createCardTable } = cardTable;
		let { NCCreateSearch } = search;
		return (
			<div className="nc-bill-list arap-statement-qry">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon ? createPageIcon() : null}
						<h2 className="title-search-detail">{appname}</h2>
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area" style={{ margin: 0 }}>
					{NCCreateSearch(
						this.searchId, //模块id
						{
							onlyShowSuperBtn: true, // 只显示高级按钮
							clickSearchBtn: searchBtnClick.bind(this), //   点击按钮事件
							showAdvBtn: true, //  显示高级按钮
							clickPlanEve: this.clickPlanEve, // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
							addAdvBody: this.addAdvBody, // 添加高级查询区自定义查询条件Dom (fun) , return Dom
							hideBtnArea: true, // 隐藏查询按钮区域,可以使用其他按钮调用props.search.openAdvSearch(searchId, true)显示查询框
							saveSearchPlan: this.saveSearchPlan, // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
							advSearchClearEve: this.clearSearchEve.bind(this)//高级查询面板中的清空按钮 点击事件钩子，用于业务组清除自定义查询条件值
						}
					)}
				</div>

				<div className="nc-bill-table-area">
					{createCardTable(this.headTableCode, {
						// showCheck: true,
						showIndex: true,
						onRowClick: this.RowClick.bind(this),
						pageSize: 10
					})}
				</div>
				<div className="nc-bill-table-area">
					{createCardTable(this.bodyTableCode, {
						// showCheck: true,
						showIndex: true,
						pageSize: 10

					})}
				</div>
			</div>
		);
	}
}

List = createPage({
	initTemplate: initTemplate,
	mutiLangCode: '2052'
})(List);

ReactDOM.render(<List />, document.querySelector('#app'));

ReactDOM.render(<List />, document.querySelector('#app'));
