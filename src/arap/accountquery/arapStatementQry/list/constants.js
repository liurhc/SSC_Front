
/**
 * 查询区域
 */
export const searchId = '1905_query';

/**
 * 查询区查询条件编码
 */
export const queryFormCode = 'queryFormArea';

/**
 * 主表编码
 */
export const headTableCode = '1905_head';

/**
 * 子表编码
 */
export const bodyTableCode = '1905_body';

/**
 * 打印模板节点标识
 */
export const nodekey = 'card';





