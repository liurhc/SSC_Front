import { ajax, toast } from 'nc-lightapp-front';
import { searchId, headTableCode, bodyTableCode, queryFormCode } from '../constants';


export default function afterEvent(props, moduleId, key, value, oldValue) {
	// props, moduleId(区域id), key(操作的键), value（当前值），oldValue(旧值)
	if ('anaPattern' == key) {
		if ('final' == value || 'final' == value.value) {
			props.form.setFormItemsDisabled(queryFormCode, { 'dateline': true });
			props.form.setFormItemsRequired(queryFormCode, { 'dateline': false });
		} else if ('point' == value || 'point' == value.value) {
			props.form.setFormItemsDisabled(queryFormCode, { 'dateline': false });
			props.form.setFormItemsRequired(queryFormCode, { 'dateline': true });
		}
	}

}
