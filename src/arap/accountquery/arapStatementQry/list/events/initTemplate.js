import { createPage, ajax, base, toast } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;
import { searchId, headTableCode, bodyTableCode, queryFormCode } from '../constants';
import arapStatementRefFilter from './arapStatementRefFilter.js'
export default function (props) {
	props.createUIDom(
		{
			pagecode: props.getSearchParam('c') + "_LIST",//页面编码
			appcode: props.getSearchParam('c')//注册按钮的编码
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta);
					meta = formRefFilter(props, meta, queryFormCode);
					props.meta.setMeta(meta);
					arapStatementRefFilter(searchId, props, meta);
					
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				//设置用户个性化设置默认财务组织
				if(data.context && data.context.pk_org && data.context.pk_org.length != 0){
					let pk_orgsVal = props.form.getFormItemsValue(queryFormCode, 'pk_orgs');
					if(pk_orgsVal.value == null || pk_orgsVal.value.length == 0){
						props.form.setFormItemsValue(queryFormCode, { 'pk_orgs': { value: data.context.pk_org,display: data.context.org_Name} });
					}
				}
			}
		}
	)
}

function seperateDate(date) {
	if (typeof date !== 'string') return;
	let result = date.split(' ') && date.split(' ')[0];
	return result;
}

function modifierMeta(props, meta) {
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		//item.col = '3';
		
		return item;
	})

	
	//添加联查单据超链接
	meta[bodyTableCode].items = meta[bodyTableCode].items.map((item, key) => {
		//item.width = 150;
		if (item.attrcode == 'billno') {
			//item.width = 200;
			item.renderStatus = 'browse';
			item.render = (a, text, value) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							//billLink(record, props)
							drillToBill(props, text);
						}}
					>
						{text.values.billno && text.values.billno.value}
					</a>
				)
			}
		}
		return item;
	})
	meta[queryFormCode].status = "edit";

	return meta;
}

function formRefFilter(props, meta, moduleId) {
	meta[moduleId].items.map((item) => {
		item.isShowUnit = false;
		item.isShowDisabledData = true;
		let key = item.attrcode;
		item.unitValueIsNeeded = false; //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
		switch (key) {
			case 'pk_orgs'://财务组织(根据集团过滤)
				item.isMultiSelectedEnabled = true;
				item.queryCondition = () => {
					item.isShowUnit = false;
					return {
						DataPowerOperationCode: 'fi',//使用权组
						AppCode: props.getSearchParam('c'),
						TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
					};
				}
				break;
			//地区分类
			case 'areapk':
				isShowUnitControl(item, props, moduleId);
				item.queryCondition = () => {
					let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: pkOrgValue
					};
				}
				break;
			//客户
			case 'customer':
				isShowUnitControl(item, props, moduleId);
				item.queryCondition = () => {
					let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: pkOrgValue
					};
				}

				break;
			//部门
			case 'dept':
				isShowUnitControl(item, props, moduleId);
				item.queryCondition = () => {
					let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
					return {
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: pkOrgValue,
						busifuncode:"all"
					};
				}
				break;
			//业务员
			case 'psndoc':
				isShowUnitControl(item, props, moduleId)
				item.queryCondition = () => {
					let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
					return {
						busifuncode: 'all',
						DataPowerOperationCode: 'fi',//使用权组
						isDataPowerEnable: 'Y',
						pk_org: pkOrgValue,
					}
				}

				break;
			//币种
			case 'pk_currency':
				item.queryCondition = () => {
					return {
						DataPowerOperationCode: 'fi',//使用权组
					};
				}
			case 'supplier':
				isShowUnitControl(item, props, moduleId)
				item.isShowDisabledData = false;
				item.queryCondition = () => {
					let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
					return {
						DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
					};
				}
			
				break;
			default:
				break;
		}
	});
	return meta;
}
//获取选中的业务组织的第一个
function getFirstOrgValue(orgValues) {
	let pkOrgValue = '';
	if (orgValues != null) {
		let orgArray = orgValues.split(',');
		if (orgArray != null && orgArray.length > 0) {
			pkOrgValue = orgArray[0];
		}
	}
	return pkOrgValue;
}
//控制显示多级管控（业务单元切换）
function isShowUnitControl(item, props, moduleId) {
	item.isShowUnit = true
	item.unitCondition = () => {
		let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
		return {
			pkOrgs: pkOrgValue,
			TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSearchAreaSqlBuilder'
		}
	}
}
//联查单据
function drillToBill(props, data, callback){
	let defaultCallBack = (res) => {
		if (res.success) {
			props.openTo(res.data.url, {
				...res.data,
				status: 'browse',
				scene: 'linksce'
			});
		}
	};
	ajax({
		url: '/nccloud/arap/arappub/arapStatementBillLink.do',
		data: {
			pk_billtype:data.values.pk_billtype.value,
			pk_bill:data.values.pk_bill.value,
			pk_tradetype:data.values.pk_tradetypeid.value
		},
		method: 'post',
		success: callback || defaultCallBack
	});
};
