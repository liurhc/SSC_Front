import { getBusinessInfo, ajax } from "nc-lightapp-front";

/**
 * 查询区字段过滤
 * @param {*} searchId 查询区id
 * @param {*} props 当前props
 * @param {*} meta meta
 */
export default function arapStatementRefFilter(searchId, props, meta) {
    let moduleId = 'queryFormArea';
    meta[searchId].items = meta[searchId].items.map((item, key) => {
        let attrcode = item.attrcode;
        item.isShowUnit = false;
        item.isShowDisabledData = true;
        item.unitValueIsNeeded = false; //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
        switch (attrcode) {
            case 'pk_org'://财务组织
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        AppCode: props.getSearchParam('c'),
                        TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
                    };
                }
                break;
            case 'bodys.pk_org'://财务组织
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        AppCode: props.getSearchParam('c'),
                        TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
                    };
                }
                break;
            case 'pk_deptid'://部门
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        busifuncode:"all"
                    };
                }
                break;
            case 'pk_deptid_v'://部门版本
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        VersionStartDate: getBusinessInfo().businessDate,
                        busifuncode:"all"
                    };
                }
                break;
            case 'bodys.pk_deptid'://部门
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        busifuncode:"all"
                    };
                }
                break;
            case 'bodys.pk_deptid_v'://部门版本
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        busifuncode:"all"
                    };
                }
                break;
            case 'supplier'://供应商
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {

                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'customer'://客户
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {

                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.supplier'://单行供应商
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {

                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.customer'://单行客户
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {

                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //单据类型
            case 'pk_billtype':
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pk_billtypecode = props.getSearchParam('c').substring(0, 4) == '2006' ? 'F0,F2,23E0' : 'F1,F3,23E1';
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_billtypecode: pk_billtypecode,
                        pk_org: props.form.getFormItemsValue(moduleId, 'pk_orgs') ? props.form.getFormItemsValue(moduleId, 'pk_orgs').value : null
                    };
                }
                break;
            case 'pk_tradetypeid'://交易类型
                item.queryCondition = () => {
                    let parentbilltype = props.getSearchParam('c').substring(0, 4) == '2006' ? 'F0,F2,23E0' : 'F1,F3,23E1';
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',//
                        parentbilltype: parentbilltype,
                    }
                }

                break;
            case 'bodys.pk_tradetypeid'://交易类型
                item.queryCondition = () => {
                    let parentbilltype = props.getSearchParam('c').substring(0, 4) == '2006' ? 'F0,F2,23E0' : 'F1,F3,23E1';
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',//
                        parentbilltype: parentbilltype,
                    }
                }
                break;
            case 'pk_fiorg_v'://废弃财务组织版本(根据集团)
                item.queryCondition = () => {
                    item.isShowUnit = false;

                    return {
                        isDataPowerEnable: 'Y',
                    };

                }
                break;
            case 'bodys.pk_fiorg_v'://废弃财务组织版本(根据集团)
                item.queryCondition = () => {
                    item.isShowUnit = false;

                    return {
                        isDataPowerEnable: 'Y',
                    };

                }
                break;
            case 'pk_fiorg'://废弃财务组织(根据集团)
                item.queryCondition = () => {
                    item.isShowUnit = false;

                    return {
                        isDataPowerEnable: 'Y',
                    };


                }
                break;
            case 'bodys.pk_fiorg'://废弃财务组织(根据集团)
                item.queryCondition = () => {
                    item.isShowUnit = false;

                    return {
                        isDataPowerEnable: 'Y',
                    };


                }
                break;
            case 'pk_pcorg'://利润中心
                item.queryCondition = () => {

                    return {
                        isDataPowerEnable: 'Y',
                    };

                }
                break;
            case 'sett_org'://结算财务组织(根据集团) 且根据财务组织联动
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            case 'sett_org_v'://结算财务组织版本(根据集团)且根据财务组织联动
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            case 'creator'://创建人
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            case 'subjcode'://科目
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    let pkAccountingbook = '0000000000';
                    ajax({
                        url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
                        data: {
                            pk_org: pkOrgValue,
                        },
                        async: false,
                        success: (res) => {
                            if (res.success) {
                                if(res.data != null && res.data.length > 0){
                                    pkAccountingbook = res.data;
                                }
                            }
                        }
                    })
                    return {
                        isDataPowerEnable: 'Y',
                        pk_accountingbook: pkAccountingbook,
                        DataPowerOperationCode: 'fi',//使用权组
                        datestr: getBusinessInfo().businessDate
                    };
                }
                break;
            case 'bodys.subjcode'://科目
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    let pkAccountingbook = '0000000000';
                    ajax({
                        url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
                        data: {
                            pk_org: pkOrgValue,
                        },
                        async: false,
                        success: (res) => {
                            if (res.success) {
                                if(res.data != null && res.data.length > 0){
                                    pkAccountingbook = res.data;
                                }
                            }
                        }
                    })
                    return {
                        isDataPowerEnable: 'Y',
                        pk_accountingbook: pkAccountingbook,
                        DataPowerOperationCode: 'fi',//使用权组
                        datestr: getBusinessInfo().businessDate
                    };
                }
                break;
            case 'ordercubasdoc'://订单客户
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {

                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };

                }
                break;
            case 'bodys.ordercubasdoc'://订单客户
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {

                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };

                }
                break;
            case 'pk_balatype'://结算方式(根据pk_billtype)
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_billtype: props.search.getSearchValByField(searchId, 'pk_billtype') ? props.search.getSearchValByField(searchId, 'pk_billtype').value.firstvalue : null,
                        GridRefActionExt: 'nccloud.web.arap.ref.before.BalatypeSqlBuilder',
                    };
                }
                break;
            case 'costcenter'://成本中心
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.form.getFormItemsValue(moduleId, 'pk_pcorg') ? props.form.getFormItemsValue(moduleId, 'pk_pcorg').value : null,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.CostCenterSqlBuilder'
                    };
                }
                break;
            case 'pk_psndoc'://业务员(根据部门+组织过滤)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                    }
                }
                break;
            case 'bodys.pk_psndoc'://业务员(根据部门+组织过滤)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                    }
                }
                break;
            case 'pk_subjcode'://收支项目(根据组织)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.pk_subjcode'://收支项目(根据组织)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'pk_currtype'://币种
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                    };
                }
                break;
            case 'pu_org'://业务组织(业务单元（财务组织委托）)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        isDataPowerEnable: 'Y',
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            case 'pu_org_v'://业务单元版本(财务组织委托)
                item.isShowUnit = false;
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        isDataPowerEnable: 'Y',
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            case 'so_org'://业务组织(业务单元（财务组织委托）)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        isDataPowerEnable: 'Y',
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };


                }
                break;
            case 'so_org_v'://业务单元版本(财务组织委托)
                item.isShowUnit = false;
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        isDataPowerEnable: 'Y',
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };


                }
                break;
            case 'pu_deptid'://业务部门(根据组织)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };


                }
                break;
            case 'bodys.pu_deptid'://业务部门(根据组织)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };


                }
                break;
            case 'pu_deptid_v'://业务部门版本(根据组织) --- ok
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };


                }
                break;
            case 'bodys.pu_deptid_v'://业务部门版本(根据组织) --- ok
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };


                }
                break;
            case 'pu_psndoc'://业务人员(根据组织\业务部门)@auto zhangygw 
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        busifuncode: 'all',
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };

                }
                break;
            case 'bodys.pu_psndoc'://业务人员(根据组织\业务部门)@auto zhangygw 
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        busifuncode: 'all',
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };

                }
                break;
            case 'so_deptid'://业务部门(根据组织)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };


                }
                break;
            case 'bodys.so_deptid'://业务部门(根据组织)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };


                }
                break;
            case 'so_deptid_v'://业务部门版本(根据组织) --- ok
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };

                }
                break;
            case 'bodys.so_deptid_v'://业务部门版本(根据组织) --- ok
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);

                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };

                }
                break;
            case 'bodys.so_psndoc'://业务员(根据部门+组织过滤)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    item.isShowUnit = true;

                    return {
                        busifuncode: 'all',
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                    };

                }
                break;
            case 'so_psndoc'://业务员(根据部门+组织过滤)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    item.isShowUnit = true;

                    return {
                        busifuncode: 'all',
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                    };

                }
                break;
            case 'bodys.so_ordertype'://销售交易类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: props.search.getSearchValByField(searchId, 'pk_billtype') ? props.search.getSearchValByField(searchId, 'pk_billtype').value.firstvalue : null,
                    }
                }
                break;
            case 'bodys.so_transtype'://销售渠道类型
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    }
                }
                break;
            case 'material'://物料
            case 'bodys.material'://单行物料
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        matcustcode: props.form.getFormItemsValue(moduleId, 'bodys.matcustcode') ? props.form.getFormItemsValue(moduleId, 'bodys.matcustcode').value : null,//物料客户码，应收模块需要传值
                        GridRefActionExt: 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder'
                    };
                }
                break;
            case 'bodys.productline'://产品线
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            case 'bodys.pk_payterm'://付款协议(根据财务组织)
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.freecust'://散户  根据供应商和客户判断可编辑性，根据供应商和客户过滤
                item.queryCondition = () => {
                    let supplier_2 = props.form.getFormItemsValue(moduleId, 'bodys.supplier') ? props.form.getFormItemsValue(moduleId, 'bodys.supplier').value : null;
                    let customer_2 = props.form.getFormItemsValue(moduleId, 'bodys.customer') ? props.form.getFormItemsValue(moduleId, 'bodys.customer').value : null;
                    ajax({
                        url: '/nccloud/arap/ref/freecustcontrol.do',
                        data: {
                            supplier: supplier_2,
                            customer: customer_2
                        },
                        success: (res) => {
                            if (res.success) {
                                falg = res.data;
                            }
                        }
                    });
                    if (flag) {
                        return {
                            isDataPowerEnable: 'Y',
                            supplier: supplier_2,
                            customer: customer_2,
                            GridRefActionExt: 'nccloud.web.arap.ref.before.FreeCustSqlBuilder'
                        };
                    }
                }
                break;
            case 'bodys.taxcodeid'://单行税码(根据报税国和购销类型)
                item.queryCondition = () => {

                    return {
                        isDataPowerEnable: 'Y',
                        taxcountryid: props.search.getSearchValByField(searchId, 'taxcountryid') ? props.search.getSearchValByField(searchId, 'taxcountryid').value.firstvalue : null,//报税国
                        buysellflag: props.search.getSearchValByField(searchId, 'bodys.buysellflag') ? props.search.getSearchValByField(searchId, 'bodys.buysellflag').value.firstvalue : null,//购销类型
                        GridRefActionExt: 'nccloud.web.arap.ref.before.TaxcodeIdSqlBuilder'
                    };


                }
                break;
            case 'bodys.project'://项目
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            case 'bodys.project_task'://项目任务
                isShowUnitControl(item, props, moduleId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        project: props.form.getFormItemsValue(moduleId, 'bodys.project') ? props.form.getFormItemsValue(moduleId, 'bodys.project').value : null,//报税国
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.ProjectTaskSqlBuilder'
                    };
                }
                break;
        }
        return item;
    })
    return meta;
}

//获取选中的业务组织的第一个
function getFirstOrgValue(orgValues) {
    let pkOrgValue = '';
    if (orgValues != null) {
        let orgArray = orgValues.split(',');
        if (orgArray != null && orgArray.length > 0) {
            pkOrgValue = orgArray[0];
        }
    }
    return pkOrgValue;
}
//控制显示多级管控（业务单元切换）
function isShowUnitControl(item, props, moduleId) {
    item.isShowUnit = true
    item.unitCondition = () => {
        let pkOrgValue = getFirstOrgValue((props.form.getFormItemsValue(moduleId, 'pk_orgs') || {}).value);
        return {
            pkOrgs: pkOrgValue,
            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSearchAreaSqlBuilder'
        }
    }
}
