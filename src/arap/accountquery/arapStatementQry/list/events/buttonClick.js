import { ajax, base, toast, cacheTools, print } from 'nc-lightapp-front';
import { searchId, headTableCode, bodyTableCode, nodekey, queryFormCode } from '../constants';
import { headButton } from '../../../../public/components/pubUtils/buttonName';

export default function buttonClick(props, id) {
    switch (id) {
        case headButton.Query:
            props.search.openAdvSearch(searchId, true);
            let appcodenumber = props.getSearchParam('c').substring(0,4);
            //给下拉框赋默认值
            if('2006' == appcodenumber){
                props.form.setFormItemsValue(queryFormCode, { 'assoObj': { value: '0',display: this.state.json['20061905-000000'] } });/* 国际化处理： 客户*/
                props.form.setFormItemsValue(queryFormCode, { 'anaDate': { value: 'billdate',display: this.state.json['20061905-000001'] } });/* 国际化处理： 单据日期*/
                props.form.setFormItemsValue(queryFormCode, { 'anaPattern': { value: 'final',display: this.state.json['20061905-000002'] } });/* 国际化处理： 最终余额*/
                props.form.setFormItemsValue(queryFormCode, { 'qryScope': { value: 'allrec',display: this.state.json['20061905-000003'] } });/* 国际化处理： 全部应收*/
                props.form.setFormItemsValue(queryFormCode, { 'billState': { value: 'all',display: this.state.json['20061905-000004'] } });/* 国际化处理： 全部*/
            }else{
                props.form.setFormItemsValue(queryFormCode, { 'assoObj': { value: '1',display: this.state.json['20061905-000005'] } });/* 国际化处理： 供应商*/
                props.form.setFormItemsValue(queryFormCode, { 'anaDate': { value: 'billdate',display: this.state.json['20061905-000001'] } });/* 国际化处理： 单据日期*/
                props.form.setFormItemsValue(queryFormCode, { 'anaPattern': { value: 'final',display: this.state.json['20061905-000002'] } });/* 国际化处理： 最终余额*/
                props.form.setFormItemsValue(queryFormCode, { 'qryScope': { value: 'allpay',display: this.state.json['20061905-000006'] } });/* 国际化处理： 全部应付*/
                props.form.setFormItemsValue(queryFormCode, { 'billState': { value: 'all',display: this.state.json['20061905-000004'] } });/* 国际化处理： 全部*/
            }
            
            //给时间赋默认值
            //获取当前时间，格式为YY-MM-DD
            let date = new Date();
            let seperator1 = "-";
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            let currentdate = year + seperator1 + month + seperator1 + strDate;
            let currentmonthbegin = year + seperator1 + month + seperator1 + '01';
            //给时间字段赋默认值querydate
            
            let beginTime = props.form.getFormItemsValue(queryFormCode, 'beginDate');
            let endTime = props.form.getFormItemsValue(queryFormCode, 'endDate');
            if (beginTime.value ==null) {
                props.form.setFormItemsValue(queryFormCode, { 'beginDate': { value: currentmonthbegin+ ' ' +'00:00:00',display: currentmonthbegin } });
            }
            if (endTime.value ==null) {
                props.form.setFormItemsValue(queryFormCode, { 'endDate': { value: currentdate+ ' ' +'23:59:59',display: currentdate } });
            }
            let dateline = props.form.getFormItemsValue(queryFormCode, 'dateline');
            if (dateline.value ==null) {
                props.form.setFormItemsValue(queryFormCode, { 'dateline': { value: currentdate+ ' ' +'23:59:59',display: currentdate } });
            }

            break;
        case headButton.Print:
            if (this.state.checkedDatas.length == 0) {
                toast({ content: this.state.json['20061905-000007'], color: 'warning' });/* 国际化处理： 请先选择数据*/
                return;
            }
            let pks = {
                pageid: props.getSearchParam("p"),
                model: {
                    areaType: "table",
                    pageinfo: null,
                    rows: []
                }
            };
            pks.model.rows = this.state.checkedDatas;
            let printOids = JSON.stringify(pks);
            this.printData.oids = [printOids];
            this.onPrint();
            break;
    }
}
