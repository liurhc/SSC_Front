import { ajax,toast } from 'nc-lightapp-front';
import { searchId, headTableCode, bodyTableCode, queryFormCode } from '../constants';

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, queryInfo) {
    
    queryInfo = props.search.getQueryInfo(searchId)
    let formdatas = props.form.getAllFormValue(queryFormCode);
    let formdata = formdatas.rows[0].values;
    //必输项效验
    let mustChoseFlag = false;
    let contentMessage = '';
    if(formdata.pk_orgs.value == null || formdata.pk_orgs.value.length == 0){
        contentMessage += this.state.json['20061905-000014']+','
        mustChoseFlag = true;
    }
    if(formdata.beginDate.value == null || formdata.beginDate.value.length == 0){
        contentMessage += this.state.json['20061905-000015']+','
        mustChoseFlag = true;
    }
    if(formdata.endDate.value == null || formdata.endDate.value.length == 0){
        contentMessage += this.state.json['20061905-000016']+','
        mustChoseFlag = true;
    }
    if(formdata.dateline.value == null || formdata.dateline.value.length == 0){
        contentMessage += this.state.json['20061905-000017']+','
        mustChoseFlag = true;
    }
    if(mustChoseFlag){
        toast({ color: 'warning', content: contentMessage.substring(0,contentMessage.length-1) });
        return true;
    }
    let querydata = {
        queryVO: queryInfo,
        arapStatement: {
            model: formdatas,
            pageid: props.getSearchParam('c')+"_LIST"
        },
        headTableCode: headTableCode,
        bodyTableCode: bodyTableCode,
        pageId: props.getSearchParam('c')+"_LIST"
    };
    if(props.getSearchParam('c').substr(0,4) == "2006"){
        querydata.arapStatement.model.rows[0].values.syscode = {display:"",value:3};
    }else{
        querydata.arapStatement.model.rows[0].values.syscode = {display:"",value:4};
    }
    querydata.arapStatement.model.rows[0].values.appcode = {display:"",value:props.getSearchParam('c')};
    let beginDate = querydata.arapStatement.model.rows[0].values.beginDate.value.split('-');
    let endDate = querydata.arapStatement.model.rows[0].values.endDate.value.split('-');
    let flag = false;
    //比较年份
    if (beginDate[0] > endDate[0]) {
        flag = true;
        //比较月份
    } else if (beginDate[0] == endDate[0]){
        if( beginDate[1] > endDate[1]) {
            flag = true;
            //比较日期
        } else if (beginDate.length == 3){
            if ( beginDate[1] == endDate[1] && beginDate[2].substring(0,2) > endDate[2].substring(0,2)) {
                flag = true;
            }
        }
    }
    if(flag){
        toast({ color: 'warning', content: this.state.json['20061905-000009'] });/* 国际化处理： 查询开始时间不能晚于结束时间!*/
        return true;
    }
    ajax({
        url: '/nccloud/arap/arappub/arapStatementQry.do',
        data: querydata,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                if (data) {
                    this.state.alldata = res.data;
                    this.state.querydata = querydata;
                    if(querydata.arapStatement.model.rows[0].values.isShowMatCustCode.value){
                        let meta = props.meta.getMeta();
                        meta[bodyTableCode].items.map((item, key) => {
                            if (item.attrcode == 'matcustcode') {
                                item.visible = true;
                                return;
                            }
                        });
                        props.meta.setMeta(meta);
                    }
                    props.cardTable.setTableData(headTableCode, data[headTableCode][headTableCode]);
                    props.cardTable.setTableData(bodyTableCode, data[bodyTableCode][0][bodyTableCode]);
                    toast({ color: 'success', content: this.state.json['20061905-000010']+data[headTableCode][headTableCode].rows.length+this.state.json['20061905-000011'] });/* 国际化处理： 查询成功，共查询到,条数据!*/
                }else{
                    props.cardTable.setTableData(headTableCode, {rows: []});
                    props.cardTable.setTableData(bodyTableCode, {rows: []});
                    toast({ color: 'success', content: this.state.json['20061905-000018']});
                }
            }
        }
    });


};
