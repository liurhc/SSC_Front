
/**
 * 期间大于六个月，请选择查询对象具体值查询
 */
export default function queryObjCheck(items, props,data) {
    if(data.length == 0 || data[0][1] == null || data[0][1].length == 0){
        let beginDate = props.search.getSearchValByField('light_report', 'beginDate').value.firstvalue.split('-');
        let endDate = props.search.getSearchValByField('light_report', 'endDate').value.firstvalue.split('-');
        let flag = false;
        //判断查询期间是否大于六个月
        if(beginDate[0] == endDate[0]){
            if(endDate[1]*1-beginDate[1]*1 >= 6){
                flag = true;
            }
        }else{
            if((12-beginDate[1]*1)+endDate[1]*1 >= 6){
                flag = true;
            }
        }
        return flag;
    }
    
}
