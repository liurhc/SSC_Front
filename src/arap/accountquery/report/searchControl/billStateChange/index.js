import { toast } from 'nc-lightapp-front';
/**
 * 单据状态值效验控制
 * 
 */
export default function billStateChange(items, props,content) {
    let flag = false;
    let back = false;
    items.conditions.forEach((item) => {
        if('billState' == item.field){
            if('all' == item.value.firstvalue || 'save' == item.value.firstvalue){
                flag = true;
            }
        }
    });
    if(flag){
        items.conditions.forEach((item) => {
            if('anaDate' == item.field){
                if('auditdate' == item.value.firstvalue || 'effectdate' == item.value.firstvalue){
                    back = true;
                    toast({ color: 'warning', content: content });/* 国际化处理： 当单据状态为“全部”或“已保存”时，分析日期不允许选择“审批日期”或“生效日期”!*/
                }
            }
        });
    }
    return back;
}
