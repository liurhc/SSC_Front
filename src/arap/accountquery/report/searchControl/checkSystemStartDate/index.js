import {  ajax } from "nc-lightapp-front";
/**
 * 开始时间不能早于系统启用时间
 */
export default function checkSystemStartDate(items, props) {
    let beginDate = props.search.getSearchValByField('light_report', 'beginDate').value.firstvalue;
    let moduleId = props.getSearchParam('c').substring(0,4);
    let pk_orgs = props.search.getSearchValByField('light_report', 'pk_orgs').value.firstvalue.split(',');
    let message = '';
    ajax({
        url: '/nccloud/arap/report/checkSystemStartDate.do',
        data: {
            queryBeginDate: beginDate,
            moduleId: moduleId,
            pk_orgs: pk_orgs
        },
        async: false,
        success: (res) => {
            if (res.success) {
                message = res.data;
            }
        }
    })
    return message;
}
