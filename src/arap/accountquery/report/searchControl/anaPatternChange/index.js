
/**
 * 分析方式控制截止日期和分析日期可编辑性
 */
export default function anaPatternChange(props, searchId, field, val) {
    let meta = props.meta.getMeta();
    let items = meta['light_report'].items;
    if('anaPattern' == field){
         if('final' == val){
            items.find(e=>e.attrcode === 'dateline').disabled = true
            items.find(e=>e.attrcode === 'dateline').required = false;
            //
            let anaDate =  items.find(e=>e.attrcode === 'anaDate');
            if(anaDate){
                anaDate.disabled = true;
                anaDate.required = false;
            }
        }else if('point' == val){
            items.find(e=>e.attrcode === 'dateline').disabled = false;
            items.find(e=>e.attrcode === 'dateline').required = true;
            let anaDate =  items.find(e=>e.attrcode === 'anaDate');
            if(anaDate){
                anaDate.disabled = false;
                anaDate.required = true;
            }
        }
        props.meta.setMeta(meta);
    }
}
