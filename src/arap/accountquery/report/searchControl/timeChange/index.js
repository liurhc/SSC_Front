
/**
 * 根据查询方式选择使用月份还是日期
 */
export default function timeChange(props, searchId, field, val) {
    let date = new Date();
    let seperator1 = "-";
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    let currentdate = year + seperator1 + month + seperator1 + strDate;
    let currentmonth = year + seperator1 + month;
    let finalmonth = year + seperator1 + month;
    if ("04" == month || "06" == month
        || "09" == month || "11" == month) {
        finalmonth += "-30 23:59:59";
    } else if ("02" == month) {
        if (year % 100 == 0 || year % 4 != 0) {
            finalmonth += "28 23:59:59";
        } else {
            finalmonth += "29 23:59:59";
        }

    } else {
        finalmonth += "-31 23:59:59";
    }
    //修改月份和日期
    if ('qryMode' == field) {
        let meta = props.meta.getMeta();
        let items = meta['light_report'].items;
        if ('byDate' == val) {
            items.forEach((item) => {
                if (item.attrcode == 'beginDate') {
                    item.itemtype = 'NCTZDatePickerStart';
                    item.refcode = undefined;
                } else if (item.attrcode == 'endDate') {
                    item.itemtype = 'NCTZDatePickerEnd';
                    item.refcode = undefined;
                }
            });
            props.search.setSearchValByField(searchId, 'beginDate', { value: currentdate + ' ' + '00:00:00', display: currentdate }, '=');
            props.search.setSearchValByField(searchId, 'endDate', { value: currentdate + ' ' + '23:59:59', display: currentdate }, '=');
        } else if ('byMonth' == val) {
            items.forEach((item) => {
                if (item.attrcode == 'beginDate') {
                    item.itemtype = 'refer';
                    item.refcode = 'uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index';
                } else if (item.attrcode == 'endDate') {
                    item.itemtype = 'refer';
                    item.refcode = 'uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index';
                }
            });
            // props.search.setSearchValByField(searchId, 'beginDate', { value: currentmonth+'-01 00:00:00', display: currentmonth }, '=');
            // props.search.setSearchValByField(searchId, 'endDate', { value: finalmonth, display: currentmonth }, '=');
            props.search.setSearchValByField(searchId, 'beginDate', { value: currentmonth, display: currentmonth }, '=');
            props.search.setSearchValByField(searchId, 'endDate', { value: currentmonth, display: currentmonth }, '=');
        }
        props.meta.setMeta(meta);
    }
    if ('pk_orgs' == field) {
        let beginTime = props.search.getSearchValByField('light_report', 'beginDate');
        let endTime = props.search.getSearchValByField('light_report', 'endDate');
        if (beginTime.value.firstvalue.length == 0) {
            //props.search.setSearchValByField(searchId, 'beginDate', { value: currentmonth+'-01 00:00:00', display: currentmonth }, '=');
            props.search.setSearchValByField(searchId, 'beginDate', { value: currentmonth, display: currentmonth }, '=');
        }
        if (endTime.value.firstvalue.length == 0) {
            //props.search.setSearchValByField(searchId, 'endDate', { value: finalmonth, display: currentmonth }, '=');
            props.search.setSearchValByField(searchId, 'endDate', { value: currentmonth, display: currentmonth }, '=');
        }
    }
}
