
/**
 * 往来对象控制必输项   客户=0,部门=2,业务员=3       供应商=1,部门=2,业务员=3
 */
export default function assoObjChange(props, searchId, field, val) {
    let meta = props.meta.getMeta();
    let items = meta['light_report'].items;
    if('assoObj'==field){
        if('0' == val){
            items.find(e=>e.attrcode === 'customer').required = true
            items.find(e=>e.attrcode === 'dept').required = false
            items.find(e=>e.attrcode === 'psndoc').required = false
        }else if('1' == val){
            items.find(e=>e.attrcode === 'supplier').required = true
            items.find(e=>e.attrcode === 'dept').required = false
            items.find(e=>e.attrcode === 'psndoc').required = false
        }else if('2' == val){
            let customer = items.find(e=>e.attrcode === 'customer');
            let supplier = items.find(e=>e.attrcode === 'supplier');
            if(customer){
                customer.required = false
            }
            if(supplier){
                supplier.required = false
            }
            items.find(e=>e.attrcode === 'dept').required = true
            items.find(e=>e.attrcode === 'psndoc').required = false
        }else if('3' == val){
            let customer = items.find(e=>e.attrcode === 'customer');
            let supplier = items.find(e=>e.attrcode === 'supplier');
            if(customer){
                customer.required = false
            }
            if(supplier){
                supplier.required = false
            }
            items.find(e=>e.attrcode === 'dept').required = false
            items.find(e=>e.attrcode === 'psndoc').required = true
        }
        props.meta.setMeta(meta);
    }
}
