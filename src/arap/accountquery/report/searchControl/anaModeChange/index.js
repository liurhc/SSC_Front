
/**
 * 分析方式控制截止日期可编辑性
 */
export default function anaModeChange(props, searchId, field, val) {
    let meta = props.meta.getMeta();
    let items = meta['light_report'].items;
    if ('anaMode' == field) {
        if ('byAccAge' == val) {
            let anaPattern = items.find(e => e.attrcode === 'anaPattern');
            if (anaPattern) {
                anaPattern.disabled = false;
                anaPattern.required = true;
            }
            let anaType = items.find(e => e.attrcode === 'anaType');
            if (anaType) {
                anaType.disabled = false;
                anaType.required = true;
            }
            items.find(e => e.attrcode === 'accAgePlan').disabled = false;
            items.find(e => e.attrcode === 'accAgePlan').required = true;
            let anaDate = items.find(e => e.attrcode === 'anaDate');
            if (anaDate) {
                anaDate.disabled = false;
                let newOptions = [{display:anaDate.initialvalue.display,value:anaDate.initialvalue.value}];
                anaDate.options.map((option) => {
                    if(option.value != 'expiredate'){
                        newOptions.push(option);
                    }
                });
                anaDate.options = newOptions;
                //将分析日期的值设为 到期日 expiredate
                props.search.setSearchValByField('light_report', 'anaDate', { value: 'expiredate', display: '' });
            }
        } else if ('byDate' == val) {
            items.find(e => e.attrcode === 'dateline').disabled = true;
            items.find(e => e.attrcode === 'accAgePlan').disabled = true;
            items.find(e => e.attrcode === 'dateline').required = false;
            items.find(e => e.attrcode === 'accAgePlan').required = false;
            let anaPattern = items.find(e => e.attrcode === 'anaPattern');
            if (anaPattern) {
                anaPattern.disabled = true;
                anaPattern.required = false;
                //将分析模式的值设为最终余额 final
                props.search.setSearchValByField('light_report', 'anaPattern', { value: 'final', display: '' });
            }
            let anaType = items.find(e => e.attrcode === 'anaType');
            if (anaType) {
                anaType.disabled = true;
                anaType.required = false;
                //将分析类型的值设为 结算日期 settle
                props.search.setSearchValByField('light_report', 'anaType', { value: 'settle', display: '' });
            }
            let anaDate = items.find(e => e.attrcode === 'anaDate');
            if (anaDate) {
                anaDate.disabled = false;
                //将分析日期的值设为 单据日期 billdate
                props.search.setSearchValByField('light_report', 'anaDate', { value: 'billdate', display: '' });
                let newOptions = [];
                anaDate.options.map((option) => {
                    if(option.value != 'expiredate'){
                        newOptions.push(option);
                    }
                });
                anaDate.options = newOptions;
            }

        }
        props.meta.setMeta(meta);
    }
}
