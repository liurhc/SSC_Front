import { toast } from 'nc-lightapp-front';
/**
 * 加入时间框联动效验
 */
export default function anaModeDatasControl(custconditions,transferDateCons,items,flag,content) {
    //分析模式为按时间时，时间框为必输项
    let back = false;
    let contentMessage = '';
    let contentMessage0 = '';
    let isnull = false;
    //加入时间框
     let firstvalueD = [];
     for (var i = 0; i < transferDateCons.length; i++) {
         let brr = [];
        if(transferDateCons[i].dateEnd == null && transferDateCons[i].dateBeg == null){
            isnull = true;
            contentMessage0 += content.json['report-000049']+transferDateCons[i].number.toString()+content.json['report-000059']+'，';
        }
        if(transferDateCons[i].dateEnd < transferDateCons[i].dateBeg){
            back = true;
            contentMessage += content.json['report-000049']+transferDateCons[i].number.toString()+content.json['report-000050']+'，';
        }
        
         brr.push(transferDateCons[i].number.toString(), transferDateCons[i].dateBeg,
             transferDateCons[i].dateEnd , transferDateCons[i].showName == "" ? "noval" : transferDateCons[i].showName);
         firstvalueD.push(brr);
     }
     
     if(back){
        toast({ color: 'warning', content: contentMessage.substring(0,contentMessage.length-1) });/* 国际化处理： 结束日期不能小于开始日期!*/
     }
     if(isnull){
         back = true;
        toast({ color: 'warning', content: contentMessage0.substring(0,contentMessage0.length-1) });/* 国际化处理： 结束日期和开始日期不能同时为空!*/
     }
     let obj1 = {
         //obj对象内oprtype为between时firstvalue,secondvalue都有值，其他情况只有firstvalue有值
         field: 'datas',
         datatype:"1",
         isIncludeSub: false,
         oprtype: '=',
         value: { firstvalue: firstvalueD.join(','), secondvalue: null },
         //notSearch: true //不把此条件放在condition内
     };
     custconditions.conditions.push(obj1);
    
    items.conditions.forEach((item) => {
         if('anaMode'==item.field){
             if('byDate'==item.value.firstvalue){
                if(obj1.value.firstvalue == null || obj1.value.firstvalue.length == 0){
                    back = true;
                    toast({ color: 'warning', content: content.json['report-000041'] });/* 国际化处理： 查询条件不完整，没有设置日期期间!*/
                }
             }
         }
    });
    if(flag){
        if(obj1.value.firstvalue == null || obj1.value.firstvalue.length == 0){
            back = true;
            toast({ color: 'warning', content: content.json['report-000041'] });/* 国际化处理： 查询条件不完整，没有设置日期期间!*/
        }
    }
    return back;
}
