
/**
 * 分析方式控制截止日期可编辑性
 */
export default function anaTypeChange(props, searchId, field, val) {
    let meta = props.meta.getMeta();
    let items = meta['light_report'].items;
    let dateline = items.find(e=>e.attrcode === 'dateline');
    if('anaType' == field){
         if('settle' == val){
            dateline.disabled = true
            dateline.required = false;
        }else if('deadline' == val){
            dateline.disabled = false;
            dateline.required = true;
        }
        props.meta.setMeta(meta);
    }
}
