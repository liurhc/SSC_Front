
/**
 * 给日期类型字段赋默认值
 */
export default function defaultTime(searchId, props,type) {
    //获取当前时间，格式为YY-MM-DD
    let date = new Date();
    let seperator1 = "-";
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    let currentdate = year + seperator1 + month + seperator1 + strDate ;
    let currentmonth = year + seperator1 + month;
    //给时间字段赋默认值querydate
    let beginTime = props.search.getSearchValByField('light_report', 'beginDate');
    let endTime = props.search.getSearchValByField('light_report', 'endDate');
    if('byMonth' == type){
        if (beginTime.value.firstvalue.length == 0) {
            props.search.setSearchValByField(searchId, 'beginDate', { value: currentmonth, display: currentmonth },'normal');
            props.search.setSearchValByField(searchId, 'beginDate', { value: currentmonth, display: currentmonth },'super');
        }
        if (endTime.value.firstvalue.length == 0) {
            props.search.setSearchValByField(searchId, 'endDate', { value: currentmonth, display: currentmonth },'normal');
            props.search.setSearchValByField(searchId, 'endDate', { value: currentmonth, display: currentmonth },'super');
        }
    }else{
        if (beginTime.value.firstvalue.length == 0) {
            props.search.setSearchValByField(searchId, 'beginDate', { value: currentdate+ ' ' +'00:00:00',display: currentdate},'normal');
            props.search.setSearchValByField(searchId, 'beginDate', { value: currentdate+ ' ' +'00:00:00',display: currentdate},'super');
        }
        if (endTime.value.firstvalue.length == 0) {
            props.search.setSearchValByField(searchId, 'endDate', { value: currentdate+ ' ' +'23:59:59',display: currentdate },'normal');
            props.search.setSearchValByField(searchId, 'endDate', { value: currentdate+ ' ' +'23:59:59',display: currentdate },'super');
        }
    }
    
    let dateline = props.search.getSearchValByField('light_report', 'dateline');
    if (dateline.value.firstvalue.length == 0) {
        props.search.setSearchValByField(searchId, 'dateline', { value: currentdate+ ' ' +'23:59:59',display: currentdate },'normal');
        props.search.setSearchValByField(searchId, 'dateline', { value: currentdate+ ' ' +'23:59:59',display: currentdate },'super');
    }
    let querydate = props.search.getSearchValByField('light_report', 'querydate');
    if (querydate.value.firstvalue.length == 0) {
        props.search.setSearchValByField(searchId, 'querydate', { value: currentdate+ ' ' +'23:59:59',display: currentdate },'normal');
        props.search.setSearchValByField(searchId, 'querydate', { value: currentdate+ ' ' +'23:59:59',display: currentdate },'super');
    }
    
}
