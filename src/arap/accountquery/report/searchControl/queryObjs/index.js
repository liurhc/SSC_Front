/**
 * 加入查询对象框
 */
export default function queryObjs(custconditions, transferDatas) {
    let Datas = [];
    let dataobj;
    let firstvalueArr = [];
    for (var i = 0; i < transferDatas.length; i++) {
        if (transferDatas[i].dsp_objname != null) {
            transferDatas[i].dataobj = {};
            dataobj = '';
            if (transferDatas[i].queryObjsVal.length > 0) {
                transferDatas[i].queryObjsVal.forEach((obj) => {
                    dataobj += obj.refpk + '@';
                });
            }
            transferDatas[i].dataobj = dataobj;
            Datas.push(transferDatas[i]);
        }
    }
    if (Datas.length > 0) {
        for (var i = 0; i < Datas.length; i++) {
            let arr = [];
            arr.push(
                Datas[i].billfieldname,
                Datas[i].dataobj.substring(0, Datas[i].dataobj.length - 1),
                Datas[i].lowlevel,
                Datas[i].showSubtotal
            );
            firstvalueArr.push(arr);
        }
        custconditions.conditions.push({
            //obj对象内oprtype为between时firstvalue,secondvalue都有值，其他情况只有firstvalue有值
            field: 'queryObjsVal',
            oprtype: '=',
            value: { firstvalue: firstvalueArr.join(','), secondvalue: null }
            //notSearch: true //不把此条件放在condition内
        });
    }
    return firstvalueArr;
}
