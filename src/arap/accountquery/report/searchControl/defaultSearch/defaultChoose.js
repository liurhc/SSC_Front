/**
 * 给查询区赋默认值（个性化）
 */
export default function defaultChoose(props, searchId, tempedata,type) {
    // let keyVal = 'appTempletStorage_'+props.getSearchParam('c')+'_'+props.getSearchParam('p');
    // let localMessage = (JSON.parse(localStorage.getItem(keyVal)));
    // let context = JSON.parse(localMessage.v).context;
    let context = tempedata.context;
    let pk_org = '';
    let orgName = '';
    let pk_orgs = props.search.getSearchValByField(searchId, 'pk_orgs');
    if(context && context.pk_org){
        pk_org = context.pk_org;
        orgName = context.org_Name;
    }
    if (pk_org.length != 0 && pk_orgs.value.firstvalue.length == 0) {
        //设置个性化默认财务组织
        props.search.setSearchValByField(searchId, 'pk_orgs', { value: pk_org, display: orgName });
        props.search.setSearchValByField(searchId, 'pk_orgs', { value: pk_org, display: orgName },'normal');
        props.search.setSearchValByField(searchId, 'pk_orgs', { value: pk_org, display: orgName },'super');
        //带出默认查询区间
        let beginTime = props.search.getSearchValByField('light_report', 'beginDate');
        let endTime = props.search.getSearchValByField('light_report', 'endDate');
        let date = new Date();
        let seperator1 = "-";
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        let currentdate = year + seperator1 + month + seperator1 + strDate;
        let currentmonth = year + seperator1 + month;
        if('byMonth' == type){
            if (beginTime.value.firstvalue.length == 0) {
                props.search.setSearchValByField(searchId, 'beginDate', { value: currentmonth, display: currentmonth },'normal');
                props.search.setSearchValByField(searchId, 'beginDate', { value: currentmonth, display: currentmonth },'super');
            }
            if (endTime.value.firstvalue.length == 0) {
                props.search.setSearchValByField(searchId, 'endDate', { value: currentmonth, display: currentmonth },'normal');
                props.search.setSearchValByField(searchId, 'endDate', { value: currentmonth, display: currentmonth },'super');
            }
        }else if('byDate' == type){
            if (beginTime.value.firstvalue.length == 0) {
                props.search.setSearchValByField(searchId, 'beginDate', { value: currentdate+ ' ' +'00:00:00',display: currentdate},'normal');
                props.search.setSearchValByField(searchId, 'beginDate', { value: currentdate+ ' ' +'00:00:00',display: currentdate},'super');
            }
            if (endTime.value.firstvalue.length == 0) {
                props.search.setSearchValByField(searchId, 'endDate', { value: currentdate+ ' ' +'23:59:59',display: currentdate },'normal');
                props.search.setSearchValByField(searchId, 'endDate', { value: currentdate+ ' ' +'23:59:59',display: currentdate },'super');
            }
        }
        


    }

}
