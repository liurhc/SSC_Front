
/**
 * 开始时间不能晚于结束时间
 */
export default function timeCheck(items, props,content) {
    let beginDate = props.search.getSearchValByField('light_report', 'beginDate').value.firstvalue.split('-');
    let endDate = props.search.getSearchValByField('light_report', 'endDate').value.firstvalue.split('-');
    let flag = false;
    //比较年份
    if (beginDate[0] > endDate[0]) {
        flag = true;
        //比较月份
    } else if (beginDate[0] == endDate[0]){
        if( beginDate[1] > endDate[1]) {
            flag = true;
            //比较日期
        } else if (beginDate.length == 3){
            if ( beginDate[1] == endDate[1] && beginDate[2].substring(0,2) > endDate[2].substring(0,2)) {
                flag = true;
            }
        }
    }
    if(flag){
        return content;/* 国际化处理： 查询开始时间不能晚于结束时间 */
    }else{
        return "OK";
    };

}
