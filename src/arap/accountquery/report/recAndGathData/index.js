import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';
import { base, ajax,cacheTools,toast,getMultiLang } from 'nc-lightapp-front';
const { NCButton } = base;
import drillToBill from '../linkControl/linkBill';//引入联查单据
import LinkVouchar from '../linkControl/LinkVouchar';//引入联查凭证
import disposePkorg from '../disposePkorg';//引入主组织及权限过滤
import timeChange from '../searchControl/timeChange';//引入月份日期切换
import defaultTime from '../searchControl/defaultTime';//时间默认值
import assoObjChange from '../searchControl/assoObjChange';//往来对象控制
import timeCheck from '../searchControl/timeCheck';//引入时间效验
import checkSystemStartDate from '../searchControl/checkSystemStartDate';//引入时间效验
import defaultChoose from '../searchControl/defaultSearch/defaultChoose';//给查询区赋个性化设置的值


/**
 * 应收收款
 * 
 * @author xiejhk
 */

let type;
export default class GeneralData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mainData: {

            },
            linkData: {},
            json:{}
        }
    }

    disposeSearch(meta, props) {
        meta['light_report'].items.forEach((item)=>{
            if('qryMode' == item.attrcode){
                type = item.initialvalue.value;
                return;
            }
        });
       // 参照过滤
       return disposePkorg(meta,props,'no',this.state); // 处理后的过滤参照返回给查询区模板
    }
    componentDidMount() {
		let callback = (json) => {
			this.setState({json:json});
		}
		getMultiLang({moduleId:'report',domainName :'arap',currentLocale:'simpchn',callback});
	}
    /**
      * 
      * items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
    */
    expandSearchVal(items, props) {
        // 变量赋值拓展
        if (items && items.conditions.length > 0) {
            //扩展编辑
            let finalDept = '';
            items.conditions.forEach((item) => {
                if (item.field == 'dept' || item.field == 'psndoc' || item.field == 'customer') {
                    // 需要进行拓展的变量
                    let vals = item.value.firstvalue.split(',');
                    let finalval = '';

                    vals.forEach((v) => {
                        finalval += '\'' + v + '\'' + ',';
                    })
                    item.value.firstvalue = finalval.substring(0, finalval.length - 1);
                }
            });
            if(timeCheck(items, props,this.state.json['report-000043']) != "OK"){
                toast({ color: 'warning', content: timeCheck(items, props,this.state.json['report-000043']) });
                return true;
            }
            if(checkSystemStartDate(items, props) != "OK"){
                toast({ color: 'warning', content: checkSystemStartDate(items, props) });
                return true;
            }
            return items;
        }
    }
    

    /**
     * 非模板查询条件
     */
    custconditions = () => {
        let custconditions = {};
        custconditions = {
            logic: 'and',
            conditions: [
                {
                    field: 'qryDirect',
                    oprtype: '=',
                    value: { firstvalue: 'rec', secondvalue: null },
                }
            ]

        };
        return custconditions;
    }


    /**
      * searchId: 查询区需要的searchId参数
      * 'vname': 需要附默认值的字段
      * {value: '111'}: 显示值，区间为[]，具体可以对照平台查询区修改
      * 'like': 为oprtype字段值
    */
   clickAdvBtnEve = (props, searchId,context) => {
        //查询区默认显示字段值
       
        defaultTime(searchId, props,type);
        
    }
    setDefaultVal(searchId, props , islinkuse,tempedata) {
        //查询区默认显示字段值
        defaultChoose(props, searchId,tempedata,type);
    }
    /**
      * props: props
      * searchId: 查询区需要的searchId参数
      * field: 编辑后的key
      * val: 编辑后的value
    */
    onAfterEvent(props, searchId, field, val) {
        //查询区编辑后事件
        //修改月份和日期
        timeChange(props, searchId, field, val);
        //往来对象控制必输项   客户=0,部门=2,业务员=3
        assoObjChange(props, searchId, field, val);
    }

    /**
      * isRange: 查询后数据区域内点击单元格按钮可操作。如需要像升降序按钮那样，则设置 disabled={isRange}属性
      * data: 表格数据
      * coords: 选中的单元格
    */
    CreateNewSearchArea(isRange, data, coords) {
        // button区域业务端自定义的新增按钮
        //return <NCButton disabled={isRange} className="btn" shape="border">新增按钮</NCButton>
    }
    changeSearchByBusi = (searchData) => {
        searchData.forEach((data) => {
            if(data && data.drillCode == '002'){
				data.drillName = this.state.json['report-000054'];
            }else if(data && data.drillCode == '004'){
				data.drillName = this.state.json['report-000053'];
            }
        });
        return searchData;
    }
    /**
      * transSaveObject: transSaveObject参数
      * obj: 点击的联查item信息
      * data: 联查需要的参数
      * props: 平台props
      * url: 平台openTo第一个参数
      * urlParams: 平台openTo第二个参数
      * sessonKey: sessionStorage的key
    */
    setConnectionSearch(transSaveObject, obj, data, props, url, urlParams, sessonKey) {
        sessionStorage.setItem(sessonKey, transSaveObject); //处理好的transSaveObject放到浏览器内存中，必须，跳转页面会用到
        let appcode = props.getSearchParam('c');
        if (obj.key === '002') {
            //联查单据
            drillToBill(props, transSaveObject, data, 'pk_bill', 'pk_tradetype', null);
        } else if (obj.key === '004') {
            LinkVouchar(props, transSaveObject, data, appcode, this);
        } else {
            props.openTo(url, urlParams) //业务端处理完成后执行跳转新页面
        }
    }

    /**
      * nodekey: 通过nodekey业务组判断走哪一个接口
    */
    printUrlChange = (nodekey) => {
        // if (nodekey == '10300004') {
        //     return this.state.json['report-000000']/* 国际化处理： /nccloud/report/widget/业务拓展接口.do*/
        // }
    }

    // 改变state里的状态
    setDatasFn = (data) => {
        this.setState({
            mainData: data
        })
    }
    //在查询条件页签内添加自己的内容
    addAdvBody = () => {
        let { mainData } = this.state;
        return (
           
            <div>

            </div>

        )
    }

    render() {
        return (
            <div className="table">
                <SimpleReport
                    showAdvBtn={true}
                    addAdvBody={this.addAdvBody}
                    showHighSearchBtn = {true}
                    clickAdvBtnEve = {this.clickAdvBtnEve}
                    expandSearchVal={this.expandSearchVal.bind(this)}
                    disposeSearch={this.disposeSearch.bind(this)}
                    onAfterEvent={this.onAfterEvent.bind(this)}
                    CreateNewSearchArea={this.CreateNewSearchArea.bind(this)}
                    setConnectionSearch={this.setConnectionSearch.bind(this)}
                    showSortAndFilter={false}
                    custconditions={this.custconditions}
                    setDefaultVal={this.setDefaultVal.bind(this)}
                    changeSearchByBusi={this.changeSearchByBusi.bind(this)}
                />
            </div>
        );
    }
}

ReactDOM.render(<GeneralData />, document.getElementById('app'));
