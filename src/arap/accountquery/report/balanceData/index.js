import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';
import { toast, getMultiLang } from 'nc-lightapp-front';
import Tables from '../table'; //引入表格
import disposePkorg from '../disposePkorg'; //引入主组织及权限过滤
import timeChange from '../searchControl/timeChange'; //引入月份日期切换
import timeCheck from '../searchControl/timeCheck'; //引入时间效验
import checkSystemStartDate from '../searchControl/checkSystemStartDate'; //引入时间效验
import defaultChoose from '../searchControl/defaultSearch/defaultChoose'; //给查询区赋个性化设置的值
import queryObjs from '../searchControl/queryObjs'; //获取查询对象框

/**
 * 余额表
 * 
 * @author xiejhk
 */
let type;
export default class GeneralData extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pkOrg: null,
			json: {}
		};
	}

	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		getMultiLang({ moduleId: 'report', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	disposeSearch(meta, props) {
		meta['light_report'].items.forEach((item) => {
			if ('qryMode' == item.attrcode) {
				type = item.initialvalue.value;
				return;
			}
		});
		// 参照过滤
		meta = disposePkorg(meta, props, 'no', this.state); // 处理后的过滤参照返回给查询区模板
		return meta; // 处理后的过滤参照返回给查询区模板
	}
	/**
      * 
      * items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
    */
	expandSearchVal(items, props) {
		// 变量赋值拓展

		if (items && items.conditions.length > 0) {
			//扩展编辑

			if (timeCheck(items, props, this.state.json['report-000043']) != 'OK') {
				toast({ color: 'warning', content: timeCheck(items, props, this.state.json['report-000043']) });
				return true;
			}
			if (checkSystemStartDate(items, props) != 'OK') {
				toast({ color: 'warning', content: checkSystemStartDate(items, props) });
				return true;
			}

			return items;
		}
	}

	/**
     * 非模板查询条件
     */
	custconditions = () => {
		let custconditions = {};
		let transferDatas = this.child.getTableCons();
		custconditions = {
			logic: 'and',
			conditions: [
				{
					//obj对象内oprtype为between时firstvalue,secondvalue都有值，其他情况只有firstvalue有值
					field: 'flag',
					oprtype: '=',
					value: { firstvalue: 'balance', secondvalue: null }
					//notSearch: true //不把此条件放在condition内
				}
			]
		};
		queryObjs(custconditions, transferDatas);
		return custconditions;
	};

	onRef = (ref) => {
		this.child = ref;
	};

	setDefaultVal(searchId, props, islinkuse, tempedata) {
		//查询区默认显示字段值
		defaultChoose(props, searchId, tempedata, type);
		this.state.pkOrg = props.search.getSearchValByField(searchId, 'pk_orgs').value.firstvalue.split(',')[0];
	}

	/**
      * props: props
      * searchId: 查询区需要的searchId参数
      * field: 编辑后的key
      * val: 编辑后的value
    */
	onAfterEvent(props, searchId, field, val) {
		//查询区编辑后事件
		//修改月份和日期
		timeChange(props, searchId, field, val);
		if ('pk_orgs' == field) {
			if (val && val.length > 0) {
				this.setState({
					pkOrg: val[0].refpk
				});
			}
		}
	}

	/**
     * 为应对平台联查按钮有改动必须重抽多语，在前端设置多语
     */
	changeSearchByBusi = (searchData) => {
		searchData.forEach((data) => {
			if (data && data.drillCode == '001') {
				data.drillName = this.state.json['report-000051'];
				if (data.targetcode && data.targetcode == '20061903') {
					data.targetname = this.state.json['report-000056'];
				} else if (data.targetcode && data.targetcode == '20081903') {
					data.targetname = this.state.json['report-000058'];
				}
			}
		});
		return searchData;
	};

	//在查询条件页签内添加自己的内容
	addAdvBody = () => {
		let { pkOrg } = this.state;
		return (
			<div>
				<Tables reqUrl={'/nccloud/arap/arappub/getQuertObjs.do'} onRef={this.onRef} pkOrg={pkOrg} />
			</div>
		);
	};

	//高级查询框内清空值按钮
	advSearchClearEve = () => {
		this.child.clearTableCons(); //清空表格数据
	};

	render() {
		return (
			<div className="table">
				<SimpleReport
					showAdvBtn={true}
					addAdvBody={this.addAdvBody}
					showHighSearchBtn={true}
					disposeSearch={this.disposeSearch.bind(this)}
					advSearchClearEve={this.advSearchClearEve.bind(this)}
					expandSearchVal={this.expandSearchVal.bind(this)}
					onAfterEvent={this.onAfterEvent.bind(this)}
					showSortAndFilter={false}
					custconditions={this.custconditions}
					setDefaultVal={this.setDefaultVal.bind(this)}
					changeSearchByBusi={this.changeSearchByBusi.bind(this)}
				/>
			</div>
		);
	}
}

ReactDOM.render(<GeneralData />, document.getElementById('app'));
