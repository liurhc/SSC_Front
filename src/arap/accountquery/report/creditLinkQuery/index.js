import React, { Component } from 'react';
import { ajax, base, createPage,getMultiLang } from 'nc-lightapp-front';
import promptbox from '../../../public/components/promptbox';
const { NCModal, NCTable } = base;
import './index.less';
let data;
let meta;
//获取并初始化模板
let initTemplate = (props) => {
	props.createUIDom(
		{
			pagecode: props.getSearchParam('p'), //页面id
			appcode: props.getSearchParam('c') //注册按钮的id
		},
		function(data) {
			if (data) {
				if (data.template) {
					meta = data.template;
					props.meta.setMeta(meta);
				}
			}
		}
	);
};

export default class CreditLinkQuery extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: [],
			json:{}
		};
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.data != this.props.data) {
			let callback = (json) => {
				this.setState({json:json},() => {
					data = nextProps.data;
					this.getTableData(data);
				});
			}
			getMultiLang({moduleId:'report',domainName :'arap',currentLocale:'simpchn',callback});
		}
	}

	//给表格赋值
	getTableData = (data) => {
		let linkCode = this.props.getSearchParam('c').substring(0,4) + '1910_link';
		if (data) {
			this.props.meta.setMeta(meta);
			this.props.table.setAllTableData(linkCode, data[linkCode]);
		}
	};
	callback_OK = () => {
		this.props.handleModel();
	};
	render() {
		let { show, table } = this.props;
		let { tableData } = this.state;
		let linkCode = this.props.getSearchParam('c').substring(0,4) + '1910_link';
		let { createSimpleTable } = table;
		return (
			<div>
				<NCModal id="ModalId" className="query-modal credit-link-query" show={show} size={'lg'} backdrop = {true} onHide={() => {	promptbox(this.state.json['report-000027'], this.callback_OK, 'warning')}}>
					<NCModal.Header closeButton>
						<NCModal.Title >
								<span >{this.state.json['report-000028']}</span>{/* 国际化处理： 联查信用额度*/}
						</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>
						<div className="steps-content">
							{createSimpleTable(linkCode, {
								showIndex: true
							})}
						</div>
					</NCModal.Body>
				</NCModal>
			</div>
		);
	}
}
CreditLinkQuery = createPage({
	initTemplate: initTemplate
})(CreditLinkQuery);

ReactDOM.render(<CreditLinkQuery />, document.querySelector('#app'));
