import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';
import { base, createPage, getMultiLang } from 'nc-lightapp-front';
const { NCButton } = base;
import disposePkorg from './disposePkorg';//引入主组织及权限过滤


/**
 * 应收执行查询
 * 
 * @author  xiejhk
 */
export default class GeneralData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mainData: {

            },
            json: {}
        }
    }

    disposeSearch(meta, props) {
        // 参照过滤
        return disposePkorg(meta, props, 'F0,F2', this.state); // 处理后的过滤参照返回给查询区模板
    }
    componentDidMount() {
        let callback = (json) => {
            this.setState({ json: json });
        }
        getMultiLang({ moduleId: 'report', domainName: 'arap', currentLocale: 'simpchn', callback });
    }
    /**
      * 
      * items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
    */
    expandSearchVal(items, props) {
        // 变量赋值拓展
        if (items && items.conditions.length > 0) {
            //扩展编辑

            
            return items;
        }
    }


    /**
     * 非模板查询条件
     */
    custconditions = () => {
        let custconditions = {
            logic: 'and',
            conditions: [
                {
                    field: 'flag',
                    oprtype: '=',
                    value: { firstvalue: 'recivableExeQry', secondvalue: null },
                }
            ]

        };
        return custconditions;
    }


    /**
      * searchId: 查询区需要的searchId参数
      * 'vname': 需要附默认值的字段
      * {value: '111'}: 显示值，区间为[]，具体可以对照平台查询区修改
      * 'like': 为oprtype字段值
    */
    clickAdvBtnEve = (props, searchId) => {
        //查询区默认显示字段值
        props.search.setSearchValByField(searchId, 'vname', { value: '111' });
    }
    setDefaultVal(searchId, props, islinkuse, tempedata) {
        //查询区默认显示字段值bodys.pk_org
        let context = tempedata.context;
        let pk_org = '';
        let orgName = '';
        let pk_orgs = props.search.getSearchValByField(searchId, 'bodys.pk_org');
        if (context && context.pk_org) {
            pk_org = context.pk_org;
            orgName = context.org_Name;
        }
        if (pk_org.length != 0 && pk_orgs.value.firstvalue.length == 0) {
            //设置个性化默认财务组织
            props.search.setSearchValByField(searchId, 'bodys.pk_org', { value: pk_org, display: orgName });
            props.search.setSearchValByField(searchId, 'bodys.pk_org', { value: pk_org, display: orgName }, 'normal');
            props.search.setSearchValByField(searchId, 'bodys.pk_org', { value: pk_org, display: orgName }, 'super');
        }
    }
    /**
      * props: props
      * searchId: 查询区需要的searchId参数
      * field: 编辑后的key
      * val: 编辑后的value
    */
    onAfterEvent(props, searchId, field, val) {
        //查询区编辑后事件

    }

    /**
      * isRange: 查询后数据区域内点击单元格按钮可操作。如需要像升降序按钮那样，则设置 disabled={isRange}属性
      * data: 表格数据
      * coords: 选中的单元格
    */
    CreateNewSearchArea(isRange, data, coords) {
        // button区域业务端自定义的新增按钮
        //return <NCButton disabled={isRange} className="btn" shape="border">新增按钮</NCButton>
    }

    /**
      * transSaveObject: transSaveObject参数
      * obj: 点击的联查item信息
      * data: 联查需要的参数
      * props: 平台props
      * url: 平台openTo第一个参数
      * urlParams: 平台openTo第二个参数
      * sessonKey: sessionStorage的key
    */
    setConnectionSearch(transSaveObject, obj, data, props, url, urlParams, sessonKey) {
        sessionStorage.setItem(sessonKey, transSaveObject); //处理好的transSaveObject放到浏览器内存中，必须，跳转页面会用到
        props.openTo(url, urlParams) //业务端处理完成后执行跳转新页面
    }

    /**
      * nodekey: 通过nodekey业务组判断走哪一个接口
    */
    printUrlChange = (nodekey) => {
        // if (nodekey == '10300004') {
        //     return this.state.json['report-000000']/* 国际化处理： /nccloud/report/widget/业务拓展接口.do*/
        // }
    }

    // 改变state里的状态
    setDatasFn = (data) => {
        this.setState({
            mainData: data
        })
    }
    //在查询条件页签内添加自己的内容
    addAdvBody = () => {
        let { mainData } = this.state;
        return (

            <div></div>

        )
    }

    render() {
        return (
            <div className="table">
                <SimpleReport
                    showAdvBtn={true}
                    addAdvBody={this.addAdvBody}
                    clickAdvBtnEve={this.clickAdvBtnEve}
                    expandSearchVal={this.expandSearchVal.bind(this)}
                    disposeSearch={this.disposeSearch.bind(this)}
                    onAfterEvent={this.onAfterEvent.bind(this)}
                    CreateNewSearchArea={this.CreateNewSearchArea.bind(this)}
                    setConnectionSearch={this.setConnectionSearch.bind(this)}
                    custconditions={this.custconditions}
                    setDefaultVal={this.setDefaultVal.bind(this)}
                />
            </div>
        );
    }
}



ReactDOM.render(<GeneralData />, document.getElementById('app'));
