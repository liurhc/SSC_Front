import React, { Component } from 'react';
import { ajax, base, createPage,getMultiLang } from 'nc-lightapp-front';
import promptbox from '../../../public/components/promptbox';
const { NCModal, NCTable } = base;
import './index.less';
let data;
let meta;
//获取并初始化模板
let initTemplate = (props) => {
	props.createUIDom(
		{
			pagecode: props.getSearchParam('p'), //页面id
			appcode: props.getSearchParam('c').substring(0,4) + '1903' //注册按钮的id
		},
		function(data) {
			if (data) {
				if (data.template) {
					meta = data.template;
					props.meta.setMeta(meta);
				}
			}
		}
	);
};

export default class DetailLinkQuery extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: [],
			json:{}
		};
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.data != this.props.data) {
			let callback = (json) => {
				this.setState({json:json},() => {
					data = nextProps.data;
					this.getTableData(data);
				});
			}
			getMultiLang({moduleId:'report',domainName :'arap',currentLocale:'simpchn',callback});
		}
	}
	//给表格赋值
	getTableData = (data) => {
		let linkCode = this.props.getSearchParam('c').substring(0,4) + '1903_link';
		if (data) {
			meta[linkCode].items.map((item) => {
				if (
					item.attrcode == 'def6' ||
					item.attrcode == 'def10' ||
					item.attrcode == 'def3' ||
					item.attrcode == 'def5'
				) {
					item.visible = true;
				}
			});
			let flagnum = data[linkCode].rows[0].values.def11.value;
			if (flagnum == '41') {
				meta[linkCode].items.map((item) => {
					if (item.attrcode == 'def3' || item.attrcode == 'def5') {
						item.visible = false;
					}
				});
			} else if (flagnum == '11' || flagnum == '12' || flagnum == '13') {
				meta[linkCode].items.map((item) => {
					if (item.attrcode == 'def6' || item.attrcode == 'def10') {
						item.visible = false;
					}
				});
			} else if (flagnum == '31' || flagnum == '32') {
				meta[linkCode].items.map((item) => {
					if (
						item.attrcode == 'def6' ||
						item.attrcode == 'def10' ||
						item.attrcode == 'def3' ||
						item.attrcode == 'def5'
					) {
						item.visible = false;
					}
				});
			} else if (flagnum == '21') {
				meta[linkCode].items.map((item) => {
					if (
						item.attrcode == 'def6' ||
						item.attrcode == 'def10' ||
						item.attrcode == 'def3' ||
						item.attrcode == 'def5' ||
						item.attrcode == 'objtype'
					) {
						item.visible = false;
					}
				});
			}
			this.props.meta.setMeta(meta);
			this.props.table.setAllTableData(linkCode, data[linkCode]);
		}
	};
	//取消
	handleCancel = () => {
		promptbox(this.state.json['report-000027'], this.callback_OK, 'warning');/* 国际化处理： 是否确认要关闭？*/
	}
	callback_OK = () => {
		this.props.handleModel();
	};
	componentDidMount() {}

	render() {
		let { show, table } = this.props;
		let { tableData } = this.state;
		let  title  = this.state.json['report-000039'];
		let linkCode = this.props.getSearchParam('c').substring(0,4) + '1903_link';
		let { createSimpleTable } = table;
		return (
			<div>
				<NCModal
					id = "supplemodalId"
					className = "query-modal"
					show = {show}
					onHide={this.handleCancel}
					backdrop = {true}
				>
				
					<NCModal.Header closeButton>
						<NCModal.Title > {title} </NCModal.Title>{/* 国际化处理： 联查处理情况*/}
					</NCModal.Header>

					<NCModal.Body>
						<div className="steps-content">
							{createSimpleTable(linkCode, {
								//pkname: 'pk_busidata',
								showIndex: true,
								showTotal: true,
								noTotalCol: [ 'def1' ]
							})}
						</div>
					</NCModal.Body>
				</NCModal>
			</div>
		);
	}
}
DetailLinkQuery = createPage({
	initTemplate: initTemplate
})(DetailLinkQuery);

ReactDOM.render(<DetailLinkQuery />, document.querySelector('#app'));
