import React, { Component } from 'react';
import { base, toast, getMultiLang } from 'nc-lightapp-front';
const { NCButton, NCFormControl, NCTable, NCDatePicker } = base;
import './index.less';
const format = 'YYYY-MM-DD';
let checkedIndex = ''; //表格点击哪一行的下表

function strlen(str) {
	var len = 0;
	for (var i = 0; i < str.length; i++) {
		var c = str.charCodeAt(i);
		//单字节加1
		if ((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f)) {
			len++;
		} else {
			len += 2;
		}
	}
	return len;
}

class DateComponent extends Component {
	constructor(props) {
		super(props);
		(this.selectedData = {}), //cart表格选中行的数据
			(this.columns = []),
			(this.state = {
				tableData: [],
				isClickBtn: true, //初始化页面的按钮显示状态
				count: '', //行数
				isModifyShow: true, //修改按钮是否修改
				json: {}
			});
	}

	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				this.columns = [
					{
						title: this.state.json['report-000029'] /* 国际化处理： 序号*/,
						dataIndex: 'number',
						key: 'number',
						width: 100,
						fixed: 'left'
					},
					{
						title: this.state.json['report-000030'] /* 国际化处理： 起始日期*/,
						dataIndex: 'dateBeg',
						key: 'dateBeg',
						width: 130,
						render: (text, record, index) => {
							return (
								<NCDatePicker
									value={record.dateBeg || null}
									className={'range-fixed'}
									format={format}
									onChange={(d) => {
										let { tableData } = this.state;
										if (record.dateBeg) {
											if (index == 0) {
												tableData[index].dateBeg = d;
											} else {
												tableData[index].dateBeg = record.dateBeg;
											}
										} else {
											tableData[index].dateBeg = d;
										}
										this.setState({
											tableData
										});
									}}
									placeholder={this.state.json['report-000031']} /* 国际化处理： 请选择日期*/
								/>
							);
						}
					},
					{
						title: this.state.json['report-000032'] /* 国际化处理： 截止日期*/,
						dataIndex: 'dateEnd',
						key: 'dateEnd',
						width: 130,
						render: (text, record, index) => {
							return (
								<NCDatePicker
									value={record.dateEnd || null}
									className={'range-fixed'}
									format={format}
									onChange={(d) => {
										let { tableData } = this.state;
										if (d < tableData[index].dateBeg) {
											toast({
												color: 'warning',
												content: this.state.json['report-000048']
											}); /* 国际化处理： 结束日期需要大于开始日期，请重新选择*/
										} else {
											tableData[index].dateEnd = d;
											this.setState(
												{
													tableData
												},
												() => {
													if (tableData.length > index + 1) {
														tableData[index + 1].dateBeg = this.addDate(d, 1);
														this.setState({
															tableData
														});
													}
												}
											);
										}
									}}
									placeholder={this.state.json['report-000031']} /* 国际化处理： 请选择日期*/
								/>
							);
						}
					},
					{
						title: this.state.json['report-000033'] /* 国际化处理： 显示名称*/,
						dataIndex: 'showName',
						key: 'showName',
						width: 100,
						render: (text, record, index) => {
							let { tableData } = this.state;
							return (
								<NCFormControl
									value={record.showName || ''}
									onChange={(v) => {
										let vLen = strlen(v);
										if (vLen <= 50) {
											tableData[index].showName = v;
											this.setState({
												tableData
											});
										}
									}}
								/>
							);
						}
					}
				];
				this.props.onRef(this);
			});
		};
		getMultiLang({ moduleId: 'report', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	//在原有的日期上加1天
	addDate = (date, days) => {
		if (days == undefined || days == '') {
			days = 1;
		}
		var date = new Date(date);
		date.setDate(date.getDate() + days);
		var month = date.getMonth() + 1;
		var day = date.getDate();
		return date.getFullYear() + '-' + this.getFormatDate(month) + '-' + this.getFormatDate(day);
	};

	// 日期月份/天的显示，如果是1位数，则在前面加上'0'
	getFormatDate = (arg) => {
		if (arg == undefined || arg == '') {
			return '';
		}
		var re = arg + '';
		if (re.length < 2) {
			re = '0' + re;
		}
		return re;
	};

	// 增行按钮
	handleAdd = () => {
		const { count, tableData } = this.state;
		let number = 0;
		let newData;
		if (tableData.length == 0) {
			number = 1;
			newData = {
				key: count,
				number: number,
				dateBeg: null,
				dateEnd: null,
				showName: ''
			};
			this.setState({
				tableData: [ ...tableData, newData ],
				count: count + 1
			});
		} else if (tableData.length == 1) {
			if (tableData[0].dateEnd) {
				number = tableData.length + 1;
				newData = {
					key: count,
					number: number,
					dateBeg: this.addDate(this.state.tableData[number - 2].dateEnd, 1),
					dateEnd: null,
					showName: ''
				};
				this.setState({
					tableData: [ ...tableData, newData ],
					count: count + 1
				});
			}
		} else {
			if (tableData[tableData.length - 1].dateEnd >= tableData[tableData.length - 1].dateBeg) {
				if (tableData[tableData.length - 1].dateEnd) {
					number = tableData.length + 1;
					newData = {
						key: count,
						number: number,
						dateBeg: this.addDate(this.state.tableData[number - 2].dateEnd, 1),
						dateEnd: null,
						showName: ''
					};
					this.setState({
						tableData: [ ...tableData, newData ],
						count: count + 1
					});
				}
			} else {
				toast({ color: 'warning', content: this.state.json['report-000048'] }); /* 国际化处理： 结束日期需要大于开始日期，请重新选择*/
			}
		}
	};

	//点击删除按钮 数据的编号重新排列
	getArrayIndex = (tableData) => {
		for (var i = 0; i < tableData.length; i++) {
			tableData[i].number = i + 1;
		}
		return tableData;
	};

	// 删行按钮
	handleDelete = () => {
		let { tableData } = this.state;
		if (checkedIndex === '') {
			toast({ color: 'success', content: this.state.json['report-000034'] }); /* 国际化处理： 请选择一行数据*/
		} else {
			tableData.splice(checkedIndex, 1);
			if (tableData.length) {
				tableData = this.getArrayIndex(tableData);
			}
			this.setState({
				tableData
			});
		}
	};

	// 获取点击哪一行的index
	getOnRowClick = (record, index, event) => {
		checkedIndex = index;
	};

	//获取表格数据
	getDateCons = () => {
		let { tableData } = this.state;
		return tableData;
	};

	//清除表格数据
	clearTableDatas = () => {
		this.setState({
			tableData: []
		});
	};

	render() {
		let { tableData } = this.state;
		return (
			<div className="content">
				<div className="btn-area">
					<NCButton onClick={this.handleDelete.bind(this)}>{this.state.json['report-000035']}</NCButton>
					{/* 国际化处理： 删除*/}
					<NCButton onClick={this.handleAdd.bind(this)}>{this.state.json['report-000036']}</NCButton>
					{/* 国际化处理： 插入*/}
					<NCButton onClick={this.handleAdd.bind(this)}>{this.state.json['report-000037']}</NCButton>
					{/* 国际化处理： 增加*/}
				</div>
				<NCTable
					columns={this.columns}
					bordered
					isDrag
					data={tableData}
					style={{ marginRight: '20px', minHeight: '100px' }}
					onRowClick={this.getOnRowClick.bind(this)}
					scroll={{
						x: this.columns.length > 6 ? 100 + (this.columns.length - 6) * 10 + '%' : '100%',
						y: 320
					}}
					loading={false}
				/>
			</div>
		);
	}
}

export default DateComponent;
