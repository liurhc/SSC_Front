import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';
import { base, getMultiLang } from 'nc-lightapp-front';
const { NCButton } = base;
import Tables from '../table'; //引入表格
import DateComponent from '../dateComponent'; //引入日期选择表格
import disposePkorg from '../disposePkorg'; //引入主组织及权限过滤
import defaultTime from '../searchControl/defaultTime'; //引入截止日期默认值
import defaultChoose from '../searchControl/defaultSearch/defaultChoose'; //给查询区赋个性化设置的值
import queryObjs from '../searchControl/queryObjs'; //获取查询对象框
import anaModeDatasControl from '../searchControl/anaModeDatasControl'; //时间框效验

/**
 * 客户收款预测
 * 
 * @author  xiejhk
 */
export default class GeneralData extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pkOrg: null,
			mainData: {},
			json: {}
		};
	}

	disposeSearch(meta, props) {
		// 参照过滤
		return disposePkorg(meta, props, 'no', this.state); // 处理后的过滤参照返回给查询区模板
	}
	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		getMultiLang({ moduleId: 'report', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	/**
      * 
      * items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
    */
	expandSearchVal(items, props) {
		// 变量赋值拓展
		if (items && items.conditions.length > 0) {
			//扩展编辑

			return items;
		}
	}

	/**
     * 非模板查询条件
     */
	custconditions = (items) => {
		let custconditions = {};
		let transferDatas = this.child.getTableCons();
		let transferDateCons = this.child2.getDateCons(); //获取时间表格数据
		custconditions = {
			logic: 'and',
			conditions: [
				{
					field: 'flag',
					oprtype: '=',
					value: { firstvalue: 'forecastData', secondvalue: null }
				}
			]
		};
		queryObjs(custconditions, transferDatas);

		let flag = anaModeDatasControl(custconditions, transferDateCons, items, true, this.state);
		if (flag) {
			return true;
		}
		return custconditions;
	};

	/**
      * searchId: 查询区需要的searchId参数
      * 'vname': 需要附默认值的字段
      * {value: '111'}: 显示值，区间为[]，具体可以对照平台查询区修改
      * 'like': 为oprtype字段值
    */
	clickAdvBtnEve = (props, searchId, context) => {
		//查询区默认显示字段值
		//截止日期默认值

		defaultTime(searchId, props);
	};
	setDefaultVal(searchId, props, islinkuse, tempedata) {
		//查询区默认显示字段值
		defaultChoose(props, searchId, tempedata);
		this.state.pkOrg = props.search.getSearchValByField(searchId, 'pk_orgs').value.firstvalue.split(',')[0];
	}
	/**
      * props: props
      * searchId: 查询区需要的searchId参数
      * field: 编辑后的key
      * val: 编辑后的value
    */
	onAfterEvent(props, searchId, field, val) {
		//查询区编辑后事件
		if ('pk_orgs' == field) {
			if (val && val.length > 0) {
				this.setState({
					pkOrg: val[0].refpk
				});
			}
		}
	}

	/**
      * isRange: 查询后数据区域内点击单元格按钮可操作。如需要像升降序按钮那样，则设置 disabled={isRange}属性
      * data: 表格数据
      * coords: 选中的单元格
    */
	CreateNewSearchArea(isRange, data, coords) {
		// button区域业务端自定义的新增按钮
		//return <NCButton disabled={isRange} className="btn" shape="border">新增按钮</NCButton>
	}

	/**
      * transSaveObject: transSaveObject参数
      * obj: 点击的联查item信息
      * data: 联查需要的参数
      * props: 平台props
      * url: 平台openTo第一个参数
      * urlParams: 平台openTo第二个参数
      * sessonKey: sessionStorage的key
    */
	setConnectionSearch(transSaveObject, obj, data, props, url, urlParams, sessonKey) {
		sessionStorage.setItem(sessonKey, transSaveObject); //处理好的transSaveObject放到浏览器内存中，必须，跳转页面会用到
		props.openTo(url, urlParams); //业务端处理完成后执行跳转新页面
	}

	/**
      * nodekey: 通过nodekey业务组判断走哪一个接口
    */
	printUrlChange = (nodekey) => {
		// if (nodekey == '10300004') {
		//     return this.state.json['report-000000']/* 国际化处理： /nccloud/report/widget/业务拓展接口.do*/
		// }
	};

	// 改变state里的状态
	setDatasFn = (data) => {
		this.setState({
			mainData: data
		});
	};
	//在查询条件页签内添加自己的内容
	addAdvBody = () => {
		let { mainData, pkOrg } = this.state;
		return (
			<div>
				<Tables
					reqUrl={'/nccloud/arap/arappub/getQuertObjs.do'}
					onRef={(ref) => {
						this.child = ref;
					}}
					pkOrg={pkOrg}
				/>
				<DateComponent
					onRef={(ref) => {
						this.child2 = ref;
					}}
				/>
			</div>
		);
	};

	//高级查询框内清空值按钮
	advSearchClearEve = () => {
		this.child.clearTableCons(); //清空表格数据
		this.child2.clearTableDatas(); //清空日期表格数据
	};

	render() {
		return (
			<div className="table">
				<SimpleReport
					showAdvBtn={true}
					addAdvBody={this.addAdvBody}
					showHighSearchBtn={true}
					clickAdvBtnEve={this.clickAdvBtnEve}
					expandSearchVal={this.expandSearchVal.bind(this)}
					advSearchClearEve={this.advSearchClearEve.bind(this)}
					disposeSearch={this.disposeSearch.bind(this)}
					onAfterEvent={this.onAfterEvent.bind(this)}
					CreateNewSearchArea={this.CreateNewSearchArea.bind(this)}
					setConnectionSearch={this.setConnectionSearch.bind(this)}
					showSortAndFilter={false}
					custconditions={this.custconditions}
					setDefaultVal={this.setDefaultVal.bind(this)}
				/>
			</div>
		);
	}
}

ReactDOM.render(<GeneralData />, document.getElementById('app'));
