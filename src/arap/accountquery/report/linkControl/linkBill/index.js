
import { ajax } from 'nc-lightapp-front';
/**
 * 报表联查到单据方法
 * @param {*} data 报表数据，报表平台提供
 * @param {*} pkCode 报表选中数据行上主键对应的code
 * @param {*} billTypeCode 表体单据类型所在列对应的code
 * @param {*} callback 请求成功的回调函数，用于对返回的url进行特殊操作的情况，可以不传    drillToBill(props, transSaveObject, data, PKCODE, CONDITIONS.cbilltypecode_ic);
 */
export default function drillToBill(props, transSaveObject, data, pkCode, billTypeCode, callback) {
    data = {
        ...data,
        transSaveObject,
        pkCode,
        billTypeCode
    };
    //默认回调
    let defaultCallBack = (res) => {
        if (res.success) {
            //平台提供的跳转页面方法
            props.openTo(res.data.url, {
                ...res.data,
                status: 'browse',
                scene: 'linksce'
            });
        }
    };
    ajax({
        url: '/nccloud/arap/arappub/reportLinkBill.do',
        data: data,
        method: 'post',
        success: callback || defaultCallBack
    });
}
