
import { ajax,toast } from 'nc-lightapp-front';
/**
 * 报表联查凭证
 */
export default function LinkVouchar(props, transSaveObject, data, appcode, that) {
    let linkDatas = {
        ...data,
        transSaveObject,
        appcode,
    }
    ajax({
        url: '/nccloud/arap/report/reportLinkVouchar.do',
        data: linkDatas,
        success: (res) => {
            if (res.data.des) {//跳转到凭证界面
                if (res.data.pklist) {
                    props.openTo(res.data.url, {
                        status: 'browse',
                        appcode: res.data.appcode,
                        pagecode: res.data.pagecode,
                        id: res.data.pklist[0],
                        n: that.state.json['report-000053'],//'联查凭证'
                        pagekey: 'link',
                        backflag: 'noback'
                    });
                }
            }else{
                toast({ color: 'warning', content: that.state.json['report-000060'] });
            } 
        }
    });
}
