import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';
import { base, ajax, cacheTools, toast, getMultiLang } from 'nc-lightapp-front';
const { NCButton } = base;
import Tables from '../table'; //引入表格
import drillToBill from '../linkControl/linkBill'; //引入联查单据
import LinkVouchar from '../linkControl/LinkVouchar'; //引入联查凭证
import disposePkorg from '../disposePkorg'; //引入主组织及权限过滤
import timeChange from '../searchControl/timeChange'; //引入月份日期切换
import DetailLinkQuery from '../detailLinkQuery'; //引入联查处理情况
import timeCheck from '../searchControl/timeCheck'; //引入时间效验
import queryObjCheck from '../searchControl/queryObjCheck'; //引入时间效验
import checkSystemStartDate from '../searchControl/checkSystemStartDate'; //引入时间效验
import defaultChoose from '../searchControl/defaultSearch/defaultChoose'; //给查询区赋个性化设置的值
import queryObjs from '../searchControl/queryObjs'; //获取查询对象框

/**
 * 明细账
 * 
 * @author xiejhk
 * 
 */

let type;
export default class GeneralData extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pkOrg: null,
			mainData: {},
			isDetailLinkQueryModelShow: false, //控制联查处理情况弹窗
			linkData: {},
			json: {}
		};
	}
	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		getMultiLang({ moduleId: 'report', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	disposeSearch(meta, props) {
		meta['light_report'].items.forEach((item) => {
			if ('qryMode' == item.attrcode) {
				type = item.initialvalue.value;
				return;
			}
		});
		// 参照过滤
		meta = disposePkorg(meta, props, 'no', this.state);
		return meta; // 处理后的过滤参照返回给查询区模板
	}

	/**
      * 
      * items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
    */
	expandSearchVal(items, props) {
		// 变量赋值拓展

		if (items && items.conditions.length > 0) {
			//扩展编辑

			if (timeCheck(items, props, this.state.json['report-000043']) != 'OK') {
				toast({ color: 'warning', content: timeCheck(items, props, this.state.json['report-000043']) });
				return true;
			}
			if (checkSystemStartDate(items, props) != 'OK') {
				toast({ color: 'warning', content: checkSystemStartDate(items, props) });
				return true;
			}

			return items;
		}
	}

	/**
     * 非模板查询条件
     */
	custconditions = (items, props) => {
		let custconditions = {};
		let transferDatas = this.child.getTableCons();
		custconditions = {
			logic: 'and',
			conditions: [
				{
					//obj对象内oprtype为between时firstvalue,secondvalue都有值，其他情况只有firstvalue有值
					field: 'flag',
					oprtype: '=',
					value: { firstvalue: 'detail', secondvalue: null }
					//notSearch: true //不把此条件放在condition内
				}
			]
		};
		let firstvalueArr = queryObjs(custconditions, transferDatas);
		if (queryObjCheck(items, props, firstvalueArr)) {
			toast({ color: 'warning', content: this.state.json['report-000038'] }); /* 国际化处理： 期间大于六个月，请选择查询对象具体值查询!*/
			return true;
		}
		return custconditions;
	};
	setDefaultVal(searchId, props, islinkuse, tempedata) {
		//查询区默认显示字段值
		defaultChoose(props, searchId, tempedata, type);
		this.state.pkOrg = props.search.getSearchValByField(searchId, 'pk_orgs').value.firstvalue.split(',')[0];
	}

	/**
      * props: props
      * searchId: 查询区需要的searchId参数
      * field: 编辑后的key
      * val: 编辑后的value
    */
	onAfterEvent(props, searchId, field, val) {
		//查询区编辑后事件

		//修改月份和日期
		timeChange(props, searchId, field, val);
		if ('pk_orgs' == field) {
			if (val && val.length > 0) {
				this.setState({
					pkOrg: val[0].refpk
				});
			}
		}
	}

	/**
      * isRange: 查询后数据区域内点击单元格按钮可操作。如需要像升降序按钮那样，则设置 disabled={isRange}属性
      * data: 表格数据
      * coords: 选中的单元格
    */
	CreateNewSearchArea(isRange, data, coords) {
		// button区域业务端自定义的新增按钮
		//return <NCButton disabled={isRange} className="btn" shape="border">新增按钮</NCButton>
	}
	/**
     * 为应对平台联查按钮有改动必须重抽多语，在前端设置多语
     */
	changeSearchByBusi = (searchData) => {
		searchData.forEach((data) => {
			if (data && data.drillCode == '001') {
				data.drillName = this.state.json['report-000052'];
				if (data.targetcode && data.targetcode == '20061902') {
					data.targetname = this.state.json['report-000055'];
				} else if (data.targetcode && data.targetcode == '20081902') {
					data.targetname = this.state.json['report-000057'];
				}
			} else if (data && data.drillCode == '002') {
				data.drillName = this.state.json['report-000054'];
			} else if (data && data.drillCode == '003') {
				data.drillName = this.state.json['report-000039'];
			} else if (data && data.drillCode == '004') {
				data.drillName = this.state.json['report-000053'];
			}
		});
		return searchData;
	};
	/**
      * transSaveObject: transSaveObject参数
      * obj: 点击的联查item信息
      * data: 联查需要的参数
      * props: 平台props
      * url: 平台openTo第一个参数
      * urlParams: 平台openTo第二个参数
      * sessonKey: sessionStorage的key
    */
	setConnectionSearch(transSaveObject, obj, data, props, url, urlParams, sessonKey) {
		sessionStorage.setItem(sessonKey, transSaveObject); //处理好的transSaveObject放到浏览器内存中，必须，跳转页面会用到
		let appcode = props.getSearchParam('c');
		let linkDatas = {
			...data,
			transSaveObject,
			appcode
		};
		if (obj.key === '002') {
			//联查单据
			drillToBill(props, transSaveObject, data, 'pk_bill', 'pk_tradetype', null);
		} else if (obj.key === '003') {
			//联查处理情况
			ajax({
				url: '/nccloud/arap/report/linkQueryProcessOperator.do',
				data: linkDatas,
				method: 'post',
				success: (res) => {
					//打开处理情况模态框
					let data = res.data;
					data[props.getSearchParam('c').substring(0, 4) + '1903_link'].pageInfo = {
						pageSize: '10',
						pageIndex: '1'
					};
					this.setState({
						linkData: data
					});
					this.handleisDetailLinkQueryModel();
				}
			});
		} else if (obj.key === '004') {
			LinkVouchar(props, transSaveObject, data, appcode, this);
		} else {
			props.openTo(url, urlParams); //业务端处理完成后执行跳转新页面
		}
	}
	/**
     * 联查处理情况弹窗状态
     * 
     */
	handleisDetailLinkQueryModel = () => {
		let { isDetailLinkQueryModelShow } = this.state;
		this.setState({
			isDetailLinkQueryModelShow: !this.state.isDetailLinkQueryModelShow
		});
	};
	/**
      * nodekey: 通过nodekey业务组判断走哪一个接口
    */
	printUrlChange = (nodekey) => {
		// if (nodekey == '10300004') {
		//     return this.state.json['report-000000']/* 国际化处理： /nccloud/report/widget/业务拓展接口.do*/
		// }
	};

	// 改变state里的状态
	setDatasFn = (data) => {
		this.setState({
			mainData: data
		});
	};
	//在查询条件页签内添加自己的内容
	addAdvBody = () => {
		let { mainData, pkOrg } = this.state;
		return (
			<div>
				<Tables reqUrl={'/nccloud/arap/arappub/getQuertObjs.do'} onRef={this.onRef} pkOrg={pkOrg} />
			</div>
		);
	};
	onRef = (ref) => {
		this.child = ref;
	};

	//高级查询框内清空值按钮
	advSearchClearEve = () => {
		this.child.clearTableCons(); //清空表格数据
	};

	render() {
		let { linkData } = this.state;
		return (
			<div>
				<div className="table" style={{ height: '100%' }}>
					<SimpleReport
						showAdvBtn={true}
						addAdvBody={this.addAdvBody}
						showHighSearchBtn={true}
						expandSearchVal={this.expandSearchVal.bind(this)}
						disposeSearch={this.disposeSearch.bind(this)}
                        onAfterEvent={this.onAfterEvent.bind(this)}
                        advSearchClearEve={this.advSearchClearEve.bind(this)}
						CreateNewSearchArea={this.CreateNewSearchArea.bind(this)}
						setConnectionSearch={this.setConnectionSearch.bind(this)}
						showSortAndFilter={false}
						custconditions={this.custconditions}
						setDefaultVal={this.setDefaultVal.bind(this)}
						changeSearchByBusi={this.changeSearchByBusi.bind(this)}
					/>
				</div>
				<div>
					{linkData && (
						<DetailLinkQuery
							show={this.state.isDetailLinkQueryModelShow}
							handleModel={this.handleisDetailLinkQueryModel.bind(this)}
							data={this.state.linkData}
						/>
					)}
				</div>
			</div>
		);
	}
}

ReactDOM.render(<GeneralData />, document.getElementById('app'));
