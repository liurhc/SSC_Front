import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { high, base, ajax, createPage, getBusinessInfo, getMultiLang } from 'nc-lightapp-front';
import createScript from '../../../public/components/uapRefer.js';
const {
	NCFormControl,
	NCDatePicker,
	NCButton,
	NCRadio,
	NCBreadcrumb,
	NCRow,
	NCCol,
	NCTree,
	NCMessage,
	NCTable,
	NCSelect,
	NCCheckbox,
	NCNumber,
	NCForm,
	NCButtonGroup
} = base;
import './index.less';

class Tables extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {},
			columns: [],
			dataSource: [
				{
					dsp_objname: null,
					queryObjsVal: [],
					billfieldname: null,
					referurl: null,
					lowlevel: false,
					showSubtotal: false,
					key: 0
				},
				{
					dsp_objname: null,
					queryObjsVal: [],
					billfieldname: null,
					referurl: null,
					lowlevel: false,
					showSubtotal: false,
					key: 1
				},
				{
					dsp_objname: null,
					queryObjsVal: [],
					billfieldname: null,
					referurl: null,
					lowlevel: false,
					showSubtotal: false,
					key: 2
				},
				{
					dsp_objname: null,
					queryObjsVal: [],
					billfieldname: null,
					referurl: null,
					lowlevel: false,
					showSubtotal: false,
					key: 3
				},
				{
					dsp_objname: null,
					queryObjsVal: [],
					billfieldname: null,
					referurl: null,
					lowlevel: false,
					showSubtotal: false,
					key: 4
				}
			]
		};
		this.renderRefPathEle = this.renderRefPathEle.bind(this);
	}

	componentDidMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				this.state.columns = [
					{
						title: this.state.json['report-000044'] /* 国际化处理： 查询对象*/,
						dataIndex: 'dsp_objname',
						key: 'dsp_objname',
						render: (text, record, index) => {
							return <div>{text}</div>;
						}
					},
					{
						title: this.state.json['report-000045'] /* 国际化处理： 查询对象的值*/,
						dataIndex: 'queryObjsVal',
						key: 'queryObjsVal',
						render: (text, record, index) => {
							let renderEle = this.renderRefPathEle(record, index);
							return renderEle;
						}
					},
					{
						title: this.state.json['report-000046'] /* 国际化处理： 包含下级*/,
						dataIndex: 'lowlevel',
						key: 'lowlevel',
						render: (text, record, index) => {
							let { dataSource } = this.state;
							return (
								<NCCheckbox
									checked={record.lowlevel}
									onChange={(d) => {
										dataSource[index].lowlevel = d;
										this.setState({
											dataSource
										});
									}}
								/>
							);
						}
					},
					{
						title: this.state.json['report-000047'] /* 国际化处理： 计算小计*/,
						dataIndex: 'showSubtotal',
						key: 'showSubtotal',
						render: (text, record, index) => {
							let { dataSource } = this.state;
							return (
								<NCCheckbox
									checked={record.showSubtotal}
									onChange={(d) => {
										dataSource[index].showSubtotal = d;
										this.setState({
											dataSource
										});
									}}
								/>
							);
						}
					}
				];
				this.getTableData();
				this.props.onRef(this);
			});
		};
		getMultiLang({ moduleId: 'report', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	//获取表格数据
	getTableData = () => {
		let url = this.props.reqUrl;
		let funcode = this.props.getSearchParam('c');
		let { dataSource } = this.state;
		ajax({
			url: url,
			data: {
				funCode: funcode
			},
			success: function(res) {
				//此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
				let { success, data } = res;
				if (success) {
					let datas = data.m_itemVos;

					for (var i = 0; i < datas.length; i++) {
						dataSource[i].dsp_objname = datas[i].dsp_objname;
						dataSource[i].referurl = datas[i].refurl;
						dataSource[i].billfieldname = datas[i].billfieldname;
					}
					this.setState({
						dataSource
					});
				}
			}.bind(this)
		});
	};

	//根据传进来的url渲染对应的参照组件
	renderRefPathEle = (record, index) => {
		let { dataSource } = this.state;
		let mybook = null;
		let objKey = record.referurl + record.key; //后端返回的路径
		if (record.referurl) {
			if (!this.state[objKey]) {
				//undefined
				{
					createScript.call(this, record.referurl + '.js', objKey);
				}
			} else if (record.billfieldname == 'subjcode') {
				mybook = (
					<div>
						{this.state[objKey] ? (
							this.state[objKey]({
								value: record.queryObjsVal,
								isMultiSelectedEnabled: true,
								onChange: (v) => {
									dataSource[index].queryObjsVal = v;
									this.setState({
										dataSource
									});
								},
								queryCondition: () => {
									let pkOrgValue = this.props.pkOrg;
									let pkAccountingbook = '0000000000';
									if (pkOrgValue) {
										ajax({
											url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
											data: {
												pk_org: pkOrgValue
											},
											async: false,
											success: (res) => {
												if (res.success) {
													if (res.data != null && res.data.length > 0) {
														pkAccountingbook = res.data;
													}
												}
											}
										});
									}
									return {
										isDataPowerEnable: 'Y',
										pk_accountingbook: pkAccountingbook,
										DataPowerOperationCode: 'fi', //使用权组
										datestr: getBusinessInfo().businessDate
									};
								}
							})
						) : (
							<div />
						)}
					</div>
				);
			} else if (record.billfieldname == 'so_org') {
				mybook = (
					<div>
						{this.state[objKey] ? (
							this.state[objKey]({
								value: record.queryObjsVal,
								isMultiSelectedEnabled: true,
								onChange: (v) => {
									dataSource[index].queryObjsVal = v;
									this.setState({
										dataSource
									});
								},
								queryCondition: () => {
									return {
										isDataPowerEnable: 'Y',
										pkOrgs: this.props.pkOrg,
										TreeRefActionExt:
											'nccloud.web.arap.ref.before.BusinessUnitVersionTreeRefSqlBuilder'
									};
								}
							})
						) : (
							<div />
						)}
					</div>
				);
			} else if (
				record.billfieldname == 'pk_deptid' ||
				record.billfieldname == 'pk_psndoc' ||
				record.billfieldname == 'so_deptid'
			) {
				mybook = (
					<div>
						{this.state[objKey] ? (
							this.state[objKey]({
								value: record.queryObjsVal,
								isMultiSelectedEnabled: true,
								onChange: (v) => {
									dataSource[index].queryObjsVal = v;
									this.setState({
										dataSource
									});
								},
								isShowUnit: true,
								unitCondition: () => {
									return {
										pkOrgs: this.props.pkOrg,
										TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSearchAreaSqlBuilder'
									};
								},
								queryCondition: () => {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										pk_org: this.props.pkOrg,
										busifuncode: 'all'
									};
								}
							})
						) : (
							<div />
						)}
					</div>
				);
			} else if (record.billfieldname.indexOf('_v') != -1) {
				mybook = (
					<div>
						{this.state[objKey] ? (
							this.state[objKey]({
								value: record.queryObjsVal,
								isMultiSelectedEnabled: true,
								onChange: (v) => {
									dataSource[index].queryObjsVal = v;
									this.setState({
										dataSource
									});
								},
								isShowUnit: true,
								unitCondition: () => {
									return {
										pkOrgs: this.props.pkOrg,
										TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSearchAreaSqlBuilder'
									};
								},
								queryCondition: () => {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										VersionStartDate: getBusinessInfo().businessDate,
										pk_org: this.props.pkOrg
									};
								}
							})
						) : (
							<div />
						)}
					</div>
				);
			} else {
				mybook = (
					<div>
						{this.state[objKey] ? (
							this.state[objKey]({
								value: record.queryObjsVal,
								isMultiSelectedEnabled: true,
								onChange: (v) => {
									dataSource[index].queryObjsVal = v;
									this.setState({
										dataSource
									});
								},
								isShowUnit: true,
								unitCondition: () => {
									return {
										pkOrgs: this.props.pkOrg,
										TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSearchAreaSqlBuilder'
									};
								},
								queryCondition: () => {
									return {
										DataPowerOperationCode: 'fi', //使用权组
										isDataPowerEnable: 'Y',
										pk_org: this.props.pkOrg
									};
								}
							})
						) : (
							<div />
						)}
					</div>
				);
			}
		}
		return mybook;
	};

	//获取表格数据
	getTableCons = () => {
		let { dataSource } = this.state;
		return dataSource;
	};

	//清空表格数据
	clearTableCons = () => {
		let { dataSource } = this.state;
		for (let i = 0, len = dataSource.length; i < len; i++) {
			if (dataSource[i].queryObjsVal.length > 0) {
				dataSource[i].queryObjsVal = [];
			}
		}
		this.setState({
			dataSource: dataSource
		});
	};

	render() {
		return (
			<div className="table-area-box">
				<NCTable data={this.state.dataSource} columns={this.state.columns} isDrag />
			</div>
		);
	}
}

Tables = createPage({})(Tables);

export default Tables;
