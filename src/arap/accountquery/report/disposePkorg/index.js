import { getBusinessInfo, ajax } from "nc-lightapp-front";
/**
 * 报表主组织过滤及权限过滤方法
 */
export default function disposePkorg(meta, props, flag,refState) {
    let items = meta['light_report'].items;
    let searchId = 'light_report';
    items.forEach((item) => {
        let attrcode = item.attrcode;
        item.isShowUnit = false;
        item.isShowDisabledData = true;
        item.unitValueIsNeeded = false; //参照item.unitValueIsNeeded为true时：只有选业务单元才会发请求
        switch (attrcode) {
            case 'pk_orgs'://财务组织(根据集团过滤)
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    let istotal = flag == 'total' ? false : true;
                    if (istotal) {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            AppCode: props.getSearchParam('c'),
                            TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
                        };
                    } else {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                        };
                    }

                }
                break;
            case 'beginDate'://会计期间档案参照
                item.queryCondition = () => {
                    return {
                        isadj: 'N'
                    }
                };


                break;
            case 'endDate'://会计期间档案参照
                item.queryCondition = () => {
                    return {
                        isadj: 'N'
                    }
                };
                break;
            //账龄方案
            case 'accAgePlan':
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        pk_org: pkOrgValue
                    };
                }
                break;
            //   摘要
            case 'brief':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: props.search.getSearchValByField('light_report', 'pk_orgs') ? props.search.getSearchValByField('light_report', 'pk_orgs').value.firstvalue : null
                    };
                }
                break;
            //客户
            case 'customer':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //物料
            case 'material':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        matcustcode: props.search.getSearchValByField(searchId, 'allinfo.matcustcode') ? props.search.getSearchValByField(searchId, 'allinfo.matcustcode').value.firstvalue : null,
                        GridRefActionExt: 'nccloud.web.arap.ref.before.MaterialGridSqlBuilder'
                    };
                }
                break;
            //订单客户
            case 'ordercubasdoc':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //单据类型
            case 'pk_billtype':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pk_billtypecode = props.getSearchParam('c').substring(0, 4) == '2006' ? 'F0,F2,23E0' : 'F1,F3,23E1';
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_billtypecode: pk_billtypecode,
                        pk_org: props.search.getSearchValByField('light_report', 'pk_orgs') ? props.search.getSearchValByField('light_report', 'pk_orgs').value.firstvalue : null
                    };
                }
                break;
            //币种
            case 'pk_currtype':
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                    };
                }
                break;
            //部门
            case 'dept':
            case 'pk_deptid':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        busifuncode:"all"
                    };
                }
                break;
            //集团
            case 'pk_group':
                break;
            //财务组织
            case 'pk_org':
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    let istotal = props.getSearchParam('c') == '20061901' || props.getSearchParam('c') == '20081901' ? false : true;
                    if (istotal) {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            AppCode: props.getSearchParam('c'),
                            TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
                        };
                    } else {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                        };
                    }

                }
                break;
            //财务组织版本
            case 'pk_org_v':
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    let istotal = props.getSearchParam('c') == '20061901' || props.getSearchParam('c') == '20081901' ? false : true;
                    if (istotal) {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            AppCode: props.getSearchParam('c'),
                            TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
                        };
                    } else {
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                        };
                    }

                }
                break;
            //业务员
            case 'psndoc':
            case 'pk_psndoc':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        busifuncode: 'all',
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                    }
                }
                break;
            //收支项目
            case 'pk_subjcode':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //交易类型ID
            case 'tradetype':
            case 'pk_tradetypeid':
                item.queryCondition = () => {
                    let parentbilltype = props.getSearchParam('c').substring(0, 4) == '2006' ? 'F0,F2,23E0' : 'F1,F3,23E1';
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',//
                        parentbilltype: parentbilltype,
                    }
                }
                break;
            //会计科目（表体）
            case 'subjcode':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    let pkAccountingbook = '0000000000';
                    if (pkOrgValue) {
                        ajax({
                            url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
                            data: {
                                pk_org: pkOrgValue,
                            },
                            async: false,
                            success: (res) => {
                                if (res.success) {
                                    if(res.data != null && res.data.length > 0){
                                        pkAccountingbook = res.data;
                                    }
                                }
                            }
                        })
                    }
                    return {
                        isDataPowerEnable: 'Y',
                        pk_accountingbook: pkAccountingbook,
                        DataPowerOperationCode: 'fi',//使用权组
                        datestr: getBusinessInfo().businessDate
                    };
                }
                break;
            //供应商
            case 'supplier':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;

            //非常用字段
            //资金计划项目
            case 'allinfo.bankrollprojet':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //现金账户
            case 'allinfo.cashaccount':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //现金流量项目
            case 'allinfo.cashitem':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //责任核算要素
            case 'allinfo.checkelement':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pk_pcorg = props.search.getSearchValByField(searchId, 'allinfo.pk_pcorg') ? props.search.getSearchValByField(searchId, 'allinfo.pk_pcorg').value.firstvalue : null;
                    if (!pk_pcorg) {
                        //设置责任核算要素不可编辑
                        props.search.setDisabledByField(searchId, 'allinfo.checkelement', true);
                    } else {
                        //设置责任核算要素可编辑
                        props.search.setDisabledByField(searchId, 'allinfo.checkelement', false);
                        return {
                            DataPowerOperationCode: 'fi',//使用权组
                            isDataPowerEnable: 'Y',
                            pk_org: pk_pcorg
                        };
                    }
                }
                break;
            //票据类型
            case 'allinfo.checktype':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //成本中心
            case 'allinfo.costcenter':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    let pk_pcorg = props.search.getSearchValByField(searchId, 'allinfo.pk_pcorg') ? props.search.getSearchValByField(searchId, 'allinfo.pk_pcorg').value.firstvalue : null;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        pk_pcorg: pk_pcorg,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.CostCenterSqlBuilder'
                    };
                }
                break;
            //散户
            case 'allinfo.freecust':
                item.queryCondition = () => {
                    let supplier_2 = props.search.getSearchValByField(searchId, 'supplier') ? props.search.getSearchValByField(searchId, 'supplier').value.firstvalue : null;
                    let customer_2 = props.search.getSearchValByField(searchId, 'customer') ? props.search.getSearchValByField(searchId, 'customer').value.firstvalue : null;
                    ajax({
                        url: '/nccloud/arap/ref/freecustcontrol.do',
                        data: {
                            supplier: supplier_2,
                            customer: customer_2
                        },
                        success: (res) => {
                            if (res.success) {
                                falg = res.data;
                            }
                        }
                    });
                    if (flag) {
                        return {
                            isDataPowerEnable: 'Y',
                            supplier: supplier_2,
                            customer: customer_2,
                            GridRefActionExt: 'nccloud.web.arap.ref.before.FreeCustSqlBuilder'
                        };
                    }
                }
                break;
            //会计科目(表头)
            case 'allinfo.h_subjcode':
                isShowUnitControl(item, props, searchId);
                
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    let pkAccountingbook = '0000000000';
                    ajax({
                        url: '/nccloud/arap/arappub/arapQueryAccountBookAction.do',
                        data: {
                            pk_org: pkOrgValue,
                        },
                        async: false,
                        success: (res) => {
                            if (res.success) {
                                if(res.data != null && res.data.length > 0){
                                    pkAccountingbook = res.data;
                                }
                            }
                        }
                    })
                    return {
                        isDataPowerEnable: 'Y',
                        pk_accountingbook: pkAccountingbook,
                        DataPowerOperationCode: 'fi',//使用权组
                        datestr: getBusinessInfo().businessDate
                    };
                }
                break;
            //客户物料码
            case 'allinfo.matcustcode':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //付款银行帐号
            case 'allinfo.payaccount':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    let supplier = props.search.getSearchValByField(searchId, 'supplier') ? props.search.getSearchValByField(searchId, 'supplier').value.firstvalue : null;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        refnodename: refState.json['report-000040'],/* 国际化处理： 使用权参照*/
                        pk_org: pkOrgValue,
                        reportaccclass: 2,
                        pk_cust: supplier,
                        pk_currtype: props.search.getSearchValByField(searchId, 'pk_currtype') ? props.search.getSearchValByField(searchId, 'pk_currtype').value.firstvalue : null,
                        GridRefActionExt: 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder'
                    };
                }
                break;
            //业务流程
            case 'allinfo.pk_busitype':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //付款协议 
            case 'allinfo.pk_payterm':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //利润中心
            case 'allinfo.pk_pcorg':
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                    };
                }
                break;
            //收款协议
            case 'allinfo.pk_recterm':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //产品线
            case 'allinfo.productline':
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            //项目
            case 'allinfo.project':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };
                }
                break;
            //项目任务
            case 'allinfo.project_task':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                        project: props.search.getSearchValByField(searchId, 'allinfo.project') ? props.search.getSearchValByField(searchId, 'allinfo.project').value.firstvalue : null,//报税国
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.ProjectTaskSqlBuilder'
                    };
                }
                break;
            //业务部门
            case 'allinfo.pu_deptid':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);

                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };

                }
                break;
            //业务组织
            case 'allinfo.pu_org':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    return {
                        isDataPowerEnable: 'Y',
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };
                }
                break;
            //业务人员
            case 'allinfo.pu_psndoc':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);

                    return {
                        busifuncode: 'all',
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };

                }
                break;
            //收款银行帐号
            case 'allinfo.recaccount':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
                    let customer = props.search.getSearchValByField(searchId, 'customer') ? props.search.getSearchValByField(searchId, 'customer').value.firstvalue : null;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        refnodename: refState.json['report-000040'],/* 国际化处理： 使用权参照*/
                        pk_org: pkOrgValue,
                        reportaccclass: 2,
                        pk_cust: customer,
                        pk_currtype: props.search.getSearchValByField(searchId, 'pk_currtype') ? props.search.getSearchValByField(searchId, 'pk_currtype').value.firstvalue : null,
                        GridRefActionExt: 'nccloud.web.arap.ref.before.BankaccSubUseSqlBuilder'
                    };
                }
                break;
            //结算财务组织
            case 'allinfo.sett_org':
                item.queryCondition = () => {
                    item.isShowUnit = false;
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    };
                }
                break;
            //业务部门(应收)
            case 'allinfo.so_deptid':
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'allinfo.so_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    item.isShowUnit = true;
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'allinfo.so_org') || {}).value.firstvalue);

                    return {
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue
                    };


                }
                break;
            //销售订单类型
            case 'allinfo.so_ordertype':
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                        parentbilltype: props.search.getSearchValByField(searchId, 'pk_billtype') ? props.search.getSearchValByField(searchId, 'pk_billtype').value.firstvalue : null,
                    }
                }
                break;
            //业务组织(应收)
            case 'allinfo.so_org':
                isShowUnitControl(item, props, searchId);
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);

                    return {
                        isDataPowerEnable: 'Y',
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    };


                }
                break;
            //业务人员(应收)
            case 'allinfo.so_psndoc':
                item.isShowUnit = true
                item.unitCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'allinfo.so_org') || {}).value.firstvalue);
                    return {
                        pkOrgs: pkOrgValue,
                        TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSqlBuilder'
                    }
                }
                item.queryCondition = () => {
                    let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'allinfo.so_org') || {}).value.firstvalue);
                    item.isShowUnit = true;

                    return {
                        busifuncode: 'all',
                        isDataPowerEnable: 'Y',
                        pk_org: pkOrgValue,
                    };

                }
                break;
            //销售渠道类型
            case 'allinfo.so_transtype':
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    }
                }
                break;
            //源头单据类型
            case 'allinfo.src_billtype':
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    }
                }
                break;
            //源头交易类型
            case 'allinfo.src_tradetype':
                item.queryCondition = () => {
                    return {
                        DataPowerOperationCode: 'fi',//使用权组
                        isDataPowerEnable: 'Y',
                    }
                }
                break;


            default:
                break;
        }
        return item;
    });
    return meta;
}

//获取选中的业务组织的第一个
function getFirstOrgValue(orgValues) {
    let pkOrgValue = '';
    if (orgValues != null) {
        let orgArray = orgValues.split(',');
        if (orgArray != null && orgArray.length > 0) {
            pkOrgValue = orgArray[0];
        }
    }
    return pkOrgValue;
}

//控制显示多级管控（业务单元切换）
function isShowUnitControl(item, props, searchId) {
    item.isShowUnit = true
    item.unitCondition = () => {
        let pkOrgValue = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_orgs') || {}).value.firstvalue);
        return {
            pkOrgs: pkOrgValue,
            TreeRefActionExt: 'nccloud.web.arap.ref.before.OrgSearchAreaSqlBuilder'
        }
    }
}
