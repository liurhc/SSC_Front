import React, { Component } from 'react';
import { createPage, ajax, base, toast, getMultiLang, getLangCode } from 'nc-lightapp-front';
import { createTree } from '../../../../public/components/createTree.js';
import promptbox from '../../../../public/components/promptbox';
import './reportmodal.less';
import '../../../../public/less/radio.less';
import { initTemplate, buttonClick } from '../events';
const {
	NCRadio,
	NCModal,
	NCStep,
	NCSelect: Select,
	NCTable: Table,
	NCCheckbox,
	NCTree,
	NCFormControl,
	NCRow,
	NCCol
} = base;

const NCSteps = NCStep.NCSteps;
const Option = Select.NCOption;
let optionsData = [];
let optionsDataClone = [];
let changeDataSource = [ { name: '' }, { name: '' }, { name: '' }, { name: '' }, { name: '' } ],
	changeselectedData = [
		{
			dsp_objname: '',
			dsp_order: ''
		},
		{
			dsp_objname: '',
			dsp_order: ''
		},
		{
			dsp_objname: '',
			dsp_order: ''
		},
		{
			dsp_objname: '',
			dsp_order: ''
		},
		{
			dsp_objname: '',
			dsp_order: ''
		}
	]; //需要先赋初始值  存储第二步查询对象选择完值
let requestData = [];
let langCode; //获取多语语种
//树形控件
const NCTreeNode = NCTree.NCTreeNode;

class ReportModal extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.selectedDataClone = [];
		this.state = {
			json: {},
			//树形控件
			expandedKeys: [],
			autoExpandParent: true,
			selectedKeys: [],
			selectedKeys2: [],
			appregData: {},
			menuitemregData: {},
			appTreeData: [],
			menuTreeData: [],
			current: 0,
			selectedValue: '',
			secondSelectedValue: '',
			fiveSelectedVal: '',
			steps: [],
			dataSource: [ { name: '' }, { name: '' }, { name: '' }, { name: '' }, { name: '' } ],
			selectedData: [
				{
					dsp_objname: '',
					dsp_order: ''
				},
				{
					dsp_objname: '',
					dsp_order: ''
				},
				{
					dsp_objname: '',
					dsp_order: ''
				},
				{
					dsp_objname: '',
					dsp_order: ''
				},
				{
					dsp_objname: '',
					dsp_order: ''
				}
			],
			index: '',
			isMultiunitshowmode: 1,
			isPublishtype: 'query',
			data: {}
		};
		this.onExpand = this.onExpand.bind(this);
		this.onSelect = this.onSelect.bind(this);
		this.onSelect2 = this.onSelect2.bind(this);
		this.columns = [];
	}

	componentDidMount() {
		langCode = getLangCode();
		let url = '/nccloud/arap/reportnodeinit/getData.do';
		optionsData.push('');
		optionsDataClone.push('');
		ajax({
			url: url,
			success: (res) => {
				requestData = res.data;
				for (var i = 0, len = res.data.length; i < len; i++) {
					optionsData.push(res.data[i].dsp_objname);
					optionsDataClone.push(res.data[i].dsp_objname);
				}
			}
		});
		this.getTreeData('/nccloud/platform/appregister/appregref.do'); //获取第三步内容
		this.getTreeData('/nccloud/platform/appregister/menuitemregref.do'); //获取第四步内容
	}

	componentWillReceiveProps(nextProps) {
		if (this.state.json == nextProps.json) return;
		this.setState(
			{
				json: nextProps.json
			},
			() => {
				this.madeTable();
			}
		);
	}

	madeTable = () => {
		this.setState(
			{
				selectedValue: this.state.json['initbill-000010'] /* 国际化处理： 总账表*/,
				secondSelectedValue: this.state.json['initbill-000011'] /* 国际化处理： 按单位显示*/,
				steps: [
					{
						title: this.state.json['initbill-000012'] /* 国际化处理： 选择账表类型*/
					},
					{
						title: this.state.json['initbill-000013'] /* 国际化处理： 选择查询对象*/
					},
					{
						title: this.state.json['initbill-000014'] /* 国际化处理： 发布为功能节点*/
					},
					{
						title: this.state.json['initbill-000015'] /* 国际化处理： 发布为菜单*/
					},
					{
						title: this.state.json['initbill-000016'] /* 国际化处理： 系统设置*/
					}
				],
				data: {
					m_billField: null,
					m_headVo: {
						reporttype: 'general', // 账表类型
						reportformat: { display: this.state.json['initbill-000017'], value: 'foreign' }, //账页格式/* 国际化处理： 外币金额式*/
						showformat: { display: this.state.json['initbill-000018'], value: 'name' }, //显示格式/* 国际化处理： 名称*/
						rec_pay_direct: '', //收付方向    应收为rec 应付为pay
						publishtype: '', //发布类型  rep_query_node
						isshowquickqrypane: '', //是否显示快
						isshowvoucher: false, //是否显示凭证号
						multiunitshowmode: 0, //多单位显示 多单位合并
						ownmodule: 'arap', //所属模块
						node_code: '', //节点编码
						node_name: '', //节点名称
						createtable: 0, //是否创建物...
						dr: 0,
						appRegisterVO: {
							pk_appregister: ''
						}, //应用注册VO
						appMenuItemVO: {
							pk_menuitem: '',
							parentcode: ''
						} //菜单注册VO
					},
					m_itemVos: []
				}
			},
			() => {
				this.columns = [
					{
						title: (
							<div style={{ position: 'relative' }}>
								<span className="starStyle">*</span>
								{this.state.json['initbill-000034']}
								{/* 国际化处理： 查询对象*/}
							</div>
						),
						dataIndex: 'address',
						key: 'address',
						width: '100px',
						render: (text, record, index) => {
							const dataOptions = optionsData.map((options) => (
								<Option value={options}>{options}</Option>
							));
							return (
								<Select
									showSearch
									style={{ width: 200 }}
									value={this.state.selectedData[index].dsp_objname}
									optionFilterProp="children"
									onChange={this.handleChangeV.bind(this, index)}
								>
									{dataOptions}
								</Select>
							);
						}
					},
					{
						title: this.state.json['initbill-000019'] /* 国际化处理： 查询对象的次序*/,
						dataIndex: 'name',
						key: 'name',
						width: '100px'
					}
				];
			}
		);
	};

	//树形控件事件
	onExpand(expandedKeys) {
		this.setState({
			expandedKeys,
			autoExpandParent: false
		});
	}
	//第三步选择事件
	onSelect(selectedKeys, info) {
		let checkappregData = info.selectedNodes[0].props.refData;
		this.setState({
			selectedKeys,
			appregData: checkappregData
		});
	}

	//创建新的菜单项编码
	getDisorder(numbers, startnum) {
		let index = 0;
		numbers.sort();
		if (numbers == null || numbers == undefined || numbers.length == 0) {
			index = startnum + '01'; //创建新的编码
			return index;
		} else {
			index = (startnum + '01') * 1;
			numbers.forEach((number) => {
				if (index == number) {
					index += 1;
				} else {
					return;
				}
			});
		}
		return index;
	}

	//第四部选择事件
	onSelect2 = (selectedKeys2, info) => {
		let { data } = this.state;
		let checkmenuiData = info.selectedNodes[0].props.refData;
		this.state.data.m_headVo.appMenuItemVO.parentcode = checkmenuiData.children[0].pid;
		let childrenData = checkmenuiData.children;
		let numbers = [];
		if (childrenData && childrenData != null && childrenData.length != 0) {
			childrenData.forEach((child) => {
				numbers.push(child.refcode * 1);
			});
		}
		checkmenuiData.refcode = this.getDisorder(numbers, checkmenuiData.children[0].pid);
		this.state.data.m_headVo.node_code = checkmenuiData.refcode;
		this.setState({
			selectedKeys2,
			menuitemregData: checkmenuiData,
			data
		});
	};

	//查询对象次序重新排序
	handleSort = () => {
		let selectedDatas = this.selectedDataClone;
		let index = 0;
		for (var i = 0, len = selectedDatas.length; i < len; i++) {
			if (selectedDatas[i].dsp_objname == '') {
				selectedDatas[i].dsp_order = '';
			}
			if (selectedDatas[i].dsp_objname != '') {
				index++;
				selectedDatas[i].dsp_order = index;
			}
		}
		return selectedDatas;
	};

	handleChangeV = (value, index) => {
		let { dataSource, selectedData } = this.state;
		selectedData[value].dsp_objname = index;
		selectedData[value].dsp_order = value + 1;
		this.selectedDataClone = selectedData;
		selectedData = this.handleSort();
		for (var i = 0, len = selectedData.length; i < len; i++) {
			dataSource[i].name = selectedData[i].dsp_order;
			if (index == '') {
				dataSource[`${value}`].name = '';
			}
		}
		this.setState({
			dataSource,
			selectedData
		});
	};

	//下一步
	next() {
		let { selectedData, data } = this.state;
		let { m_headVo } = data;
		let { node_code, node_name } = m_headVo;
		const current = this.state.current + 1;
		//这是第一步按钮
		if (current == 1) {
			if (data.m_headVo.reporttype != '') {
				this.setState({
					current
				});
				if (
					data.m_headVo.reporttype == 'apalarm' ||
					data.m_headVo.reporttype == 'payqry' ||
					data.m_headVo.reporttype == 'aralarm' ||
					data.m_headVo.reporttype == 'recqry'
				) {
					optionsData = [];
					let initDataSource = [ { name: '' }, { name: '' }, { name: '' }, { name: '' }, { name: '' } ],
						initSelectedData = [
							{
								dsp_objname: '',
								dsp_order: ''
							},
							{
								dsp_objname: '',
								dsp_order: ''
							},
							{
								dsp_objname: '',
								dsp_order: ''
							},
							{
								dsp_objname: '',
								dsp_order: ''
							},
							{
								dsp_objname: '',
								dsp_order: ''
							}
						];
					this.setState({
						dataSource: initDataSource,
						selectedData: initSelectedData
					});
				} else {
					optionsData = optionsDataClone;
					this.setState({
						dataSource: changeDataSource,
						selectedData: changeselectedData
					});
				}
			} else {
				toast({ content: this.state.json['initbill-000020'], color: 'warning' }); /* 国际化处理： 请选择账表类型*/
			}
		}
		//这是第二步的按钮
		if (current == 2) {
			let self = this;
			let flag = false;
			data.m_itemVos = [];
			let selectedDataClone = JSON.parse(JSON.stringify(selectedData));
			for (var i = 0; i < selectedDataClone.length; i++) {
				if (selectedDataClone[i].dsp_objname == '') {
					selectedDataClone.splice(i, 1);
				}
			}
			for (var i = 0; i < selectedDataClone.length; i++) {
				for (var j = 0; j < requestData.length; j++) {
					if (selectedDataClone[i].dsp_objname == requestData[j].dsp_objname) {
						requestData[j].dsp_order = selectedDataClone[i].dsp_order;
						data.m_itemVos.push(requestData[j]);
					}
				}
			}
			this.setState(
				{
					data
				},
				() => {
					if (
						data.m_itemVos.length == 0 &&
						data.m_headVo.reporttype != 'aralarm' &&
						data.m_headVo.reporttype != 'apalarm' &&
						data.m_headVo.reporttype != 'recqry' &&
						data.m_headVo.reporttype != 'payqry'
					) {
						toast({
							content: this.state.json['initbill-000021'] /* 国际化处理： 请选择至少一个查询对象*/,
							color: 'warning'
						});
					} else if (data.m_itemVos.length > 0) {
						let arr = data.m_itemVos;
						for (var i = 0, leng = arr.length; i < leng; i++) {
							for (var j = i + 1, leng = arr.length; j < leng; j++) {
								if (arr[i].dsp_objname == arr[j].dsp_objname) {
									flag = true;
								}
							}
						}
					}
					if (
						data.m_itemVos.length > 0 ||
						data.m_headVo.reporttype == 'aralarm' ||
						data.m_headVo.reporttype == 'apalarm' ||
						data.m_headVo.reporttype == 'recqry' ||
						data.m_headVo.reporttype == 'payqry'
					) {
						if (flag) {
							toast({
								content: this.state.json['initbill-000022'] /* 国际化处理： 查询对象不能重复*/,
								color: 'warning'
							});
						} else {
							self.setState({
								current
							});
						}
					}
				}
			);
		}

		//这是第三步按钮
		if (current == 3) {
			if (!this.state.appregData) {
				toast({
					content: this.state.json['initbill-000023'] /* 国际化处理： 请选择要发布小应用位置*/,
					color: 'warning'
				});
			} else if (this.state.appregData.refpk.length < 5) {
				toast({
					content: this.state.json['initbill-000024'] /* 国际化处理： 请选择到小应用级目录下*/,
					color: 'warning'
				});
			} else {
				this.setState({
					current
				});
			}
		}

		//这是第四步按钮
		if (current == 4) {
			if (!node_code) {
				toast({
					content: this.state.json['initbill-000025'] /* 国际化处理： 请您先选择菜单项编码*/,
					color: 'warning'
				});
				return;
			} else if ((this.state.menuitemregData.refcode + '').length < 7) {
				toast({
					content: this.state.json['initbill-000026'] /* 国际化处理： 请选择小应用级菜单项*/,
					color: 'warning'
				});
				return;
			} else if (!node_name) {
				toast({
					content: this.state.json['initbill-000027'] /* 国际化处理： 请选择菜单项名称*/,
					color: 'warning'
				});
				return;
			} else if (node_code && node_name) {
				this.setState({
					current
				});
			}
		}
	}

	//上一步
	prev() {
		let { data } = this.state;
		const current = this.state.current - 1;
		this.setState({ current }, () => {
			if (
				current == 1 &&
				(data.m_headVo.reporttype == 'aralarm' ||
					data.m_headVo.reporttype == 'apalarm' ||
					data.m_headVo.reporttype == 'recqry' ||
					data.m_headVo.reporttype == 'payqry')
			) {
				changeDataSource = JSON.parse(JSON.stringify(this.state.dataSource));
				changeselectedData = JSON.parse(JSON.stringify(this.state.selectedData));
			}
		});
	}

	//第一步的内容
	getFirstContent() {
		let { data } = this.state;
		if (!this.state.data.m_headVo) {
			return;
		}
		if ('2006' == this.props.getSearchParam('p').substr(0, 4)) {
			//设置查询方向     应收 rec  应付 pay
			this.state.data.m_headVo.rec_pay_direct = 'rec';
			return (
				<div className="first-content">
					<NCRadio.NCRadioGroup
						name=""
						selectedValue={data.m_headVo.reporttype}
						onChange={(v) => {
							data.m_headVo.reporttype = v;
							this.setState({
								data
							});
						}}
					>
						<div className="first-page-radio">
							<div>{this.state.json['initbill-000035']}:</div>
							{/* 国际化处理： 往来账表*/}
							<div className={langCode == 'english' ? 'en-radios' : 'radios'}>
								<NCRadio value="general">{this.state.json['initbill-000010']}</NCRadio>
								{/* 国际化处理： 总账表*/}
								<NCRadio value="balance">{this.state.json['initbill-000036']}</NCRadio>
								{/* 国际化处理： 余额表*/}
								<NCRadio value="detail">{this.state.json['initbill-000037']}</NCRadio>
								{/* 国际化处理： 明细账*/}
							</div>
						</div>

						<div className="first-page-radio">
							<div>{this.state.json['initbill-000038']}:</div>
							{/* 国际化处理： 分析报表*/}
							<div className={langCode == 'english' ? 'en-radios' : 'radios'}>
								<NCRadio value="yszlfx">{this.state.json['initbill-000039']}</NCRadio>
								{/* 国际化处理： 应收账龄分析*/}
								<NCRadio value="skzlfx">{this.state.json['initbill-000040']}</NCRadio>
								{/* 国际化处理： 收款账龄分析*/}
							</div>
						</div>

						<div className="first-page-radio">
							<div>{this.state.json['initbill-000041']}:</div>
							{/* 国际化处理： 管理报表*/}
							<div className={langCode == 'english' ? 'en-radios' : 'radios'}>
								<NCRadio value="ysqkfx">{this.state.json['initbill-000042']}</NCRadio>
								{/* 国际化处理： 应收欠账分析*/}
								<NCRadio value="skfx">{this.state.json['initbill-000043']}</NCRadio>
								{/* 国际化处理： 收款分析*/}
								<NCRadio value="skyc">{this.state.json['initbill-000044']}</NCRadio>
								{/* 国际化处理： 收款预测*/}
							</div>
						</div>

						<div className="first-page-radio last">
							<div>{this.state.json['initbill-000045']}:</div>
							{/* 国际化处理： 其他报表*/}
							<div className={langCode == 'english' ? 'en-radios' : 'radios'}>
								<NCRadio value="aralarm">{this.state.json['initbill-000046']}</NCRadio>
								{/* 国际化处理： 应收报警单*/}
								<NCRadio value="recqry">{this.state.json['initbill-000047']}</NCRadio>
								{/* 国际化处理： 应收收款情况查询*/}
							</div>
						</div>
					</NCRadio.NCRadioGroup>
				</div>
			);
		} else if ('2008' == this.props.getSearchParam('p').substr(0, 4)) {
			this.state.data.m_headVo.rec_pay_direct = 'pay';
			return (
				<div className="first-content">
					<NCRadio.NCRadioGroup
						name=""
						selectedValue={data.m_headVo.reporttype}
						onChange={(v) => {
							data.m_headVo.reporttype = v;
							this.setState({
								data
							});
						}}
					>
						<div className="first-page-radio">
							<div>{this.state.json['initbill-000035']}:</div>
							{/* 国际化处理： 往来账表*/}
							<div className={langCode == 'english' ? 'en-radios' : 'radios'}>
								<NCRadio value="general">{this.state.json['initbill-000010']}</NCRadio>
								{/* 国际化处理： 总账表*/}
								<NCRadio value="balance">{this.state.json['initbill-000036']}</NCRadio>
								{/* 国际化处理： 余额表*/}
								<NCRadio value="detail">{this.state.json['initbill-000037']}</NCRadio>
								{/* 国际化处理： 明细账*/}
							</div>
						</div>

						<div className="first-page-radio">
							<div>{this.state.json['initbill-000038']}:</div>
							{/* 国际化处理： 分析报表*/}
							<div className={langCode == 'english' ? 'en-radios' : 'radios'}>
								<NCRadio value="yfzlfx">{this.state.json['initbill-000048']}</NCRadio>
								{/* 国际化处理： 应付账龄分析*/}
								<NCRadio value="fkzlfx">{this.state.json['initbill-000049']}</NCRadio>
								{/* 国际化处理： 付款账龄分析*/}
							</div>
						</div>

						<div className="first-page-radio">
							<div>{this.state.json['initbill-000041']}:</div>
							{/* 国际化处理： 管理报表*/}
							<div className={langCode == 'english' ? 'en-radios' : 'radios'}>
								<NCRadio value="yfqkfx">{this.state.json['initbill-000050']}</NCRadio>
								{/* 国际化处理： 应付欠款分析*/}
								<NCRadio value="fkyc">{this.state.json['initbill-000051']}</NCRadio>
								{/* 国际化处理： 付款预测*/}
							</div>
						</div>

						<div className="first-page-radio last">
							<div>{this.state.json['initbill-000045']}:</div>
							{/* 国际化处理： 其他报表*/}
							<div className={langCode == 'english' ? 'en-radios' : 'radios'}>
								<NCRadio value="apalarm">{this.state.json['initbill-000052']}</NCRadio>
								{/* 国际化处理： 应付报警单*/}
								<NCRadio value="payqry">{this.state.json['initbill-000053']}</NCRadio>
								{/* 国际化处理： 应付付款情况查询*/}
							</div>
						</div>
					</NCRadio.NCRadioGroup>
				</div>
			);
		}
	}

	//第二步的内容
	getSecondContent = () => {
		let { data, isMultiunitshowmode } = this.state;
		let value = data.m_headVo.reportformat && data.m_headVo.reportformat.display;
		return (
			<div className="second-content-area">
				<Table data={this.state.dataSource} columns={this.columns} isDrag />
				<div className="secont-content-foot-area">
					<div className="secont-content-footer">
						<div>
							<span className={langCode == 'english' ? 'en-sec-tit' : ''}>
								{this.state.json['initbill-000054']}
							</span>
							{/* 国际化处理： 账页格式*/}
							<Select
								style={{ width: 150 }}
								value={value ? value : ''}
								optionFilterProp="children"
								onChange={(v) => {
									data.m_headVo.reportformat.display = v.display;
									data.m_headVo.reportformat.value = v.value;
									this.setState({
										data
									});
								}}
							>
								<Option value={{ display: this.state.json['initbill-000017'], value: 'foreign' }}>
									{this.state.json['initbill-000017']}
								</Option>
								{/* 国际化处理： 外币金额式,外币金额式*/}
								<Option value={{ display: this.state.json['initbill-000028'], value: 'local' }}>
									{this.state.json['initbill-000028']}
								</Option>
								{/* 国际化处理： 金额式,金额式*/}
							</Select>
						</div>
						<div>
							<span>{this.state.json['initbill-000055']}</span>
							{/* 国际化处理： 显示格式*/}
							<Select
								style={{ width: 150 }}
								value={data.m_headVo.showformat.display}
								optionFilterProp="children"
								onChange={(v) => {
									data.m_headVo.showformat.display = v.display;
									data.m_headVo.showformat.value = v.value;
									this.setState({
										data
									});
								}}
							>
								<Option value={{ display: this.state.json['initbill-000018'], value: 'name' }}>
									{this.state.json['initbill-000018']}
								</Option>
								{/* 国际化处理： 名称,名称*/}
								<Option value={{ display: this.state.json['initbill-000029'], value: 'code' }}>
									{this.state.json['initbill-000029']}
								</Option>
								{/* 国际化处理： 编码,编码*/}
								<Option value={{ display: this.state.json['initbill-000030'], value: 'name_code' }}>
									{this.state.json['initbill-000018']}+{this.state.json['initbill-000029']}
								</Option>
								{/* 国际化处理： 名称+编码,名称,编码*/}
							</Select>
						</div>
						{data.m_headVo.reporttype == 'detail' ? (
							<div className="second-page-radio voucher">
								<NCCheckbox
									colors="info"
									onChange={() => {
										data.m_headVo.isshowvoucher = !data.m_headVo.isshowvoucher;
										this.setState({
											data
										});
									}}
									checked={data.m_headVo.isshowvoucher}
								>
									{this.state.json['initbill-000056']}
									{/* 国际化处理： 显示凭证号*/}
								</NCCheckbox>
							</div>
						) : (
							''
						)}
						{data.m_headVo.reporttype == 'recqry' ? (
							<div className="second-page-radio voucher">
								<NCCheckbox
									colors="info"
									onChange={() => {
										data.m_headVo.isshowvoucher = !data.m_headVo.isshowvoucher;
										this.setState({
											data
										});
									}}
									checked={data.m_headVo.isshowvoucher}
								>
									{this.state.json['initbill-000056']}
									{/* 国际化处理： 显示凭证号*/}
								</NCCheckbox>
							</div>
						) : (
							''
						)}
						<div className={langCode == 'english' ? 'en-second-radios' : 'second-radios'}>
							<NCRadio.NCRadioGroup
								name=""
								selectedValue={isMultiunitshowmode}
								onChange={(v) => {
									v == 1 ? (isMultiunitshowmode = 1) : (isMultiunitshowmode = 2);
									this.setState(
										{
											isMultiunitshowmode
										},
										() => {
											data.m_headVo.multiunitshowmode = isMultiunitshowmode;
											this.setState({
												data
											});
										}
									);
								}}
							>
								<div className="second-page-radio">
									{data.m_headVo.reporttype == 'yszlfx' ||
									data.m_headVo.reporttype == 'ysqkfx' ||
									data.m_headVo.reporttype == 'skfx' ||
									data.m_headVo.reporttype == 'skyc' ||
									data.m_headVo.reporttype == 'yfzlfx' ||
									data.m_headVo.reporttype == 'yfqkfx' ||
									data.m_headVo.reporttype == 'fkyc' ? (
										<div className="radios">
											<div className="radios-box">
												<NCRadio value={1}>{this.state.json['initbill-000011']}</NCRadio>
												{/* 国际化处理： 按单位显示*/}
												<NCRadio value={2}>{this.state.json['initbill-000057']}</NCRadio>
												{/* 国际化处理： 多单位合并*/}
											</div>
										</div>
									) : (
										''
									)}
								</div>
							</NCRadio.NCRadioGroup>
						</div>
					</div>
				</div>
			</div>
		);
	};

	// 获取应用树
	getTreeData(url) {
		let appTreeData = {};
		let menuTreeData = {};
		ajax({
			url: url,
			data: '',
			success: ({ data: { rows } }) => {
				if (rows) {
					if (url.indexOf('appregref') != -1) {
						appTreeData = createTree(rows, 'refpk', 'pid');
						this.setState({ appTreeData });
					} else {
						menuTreeData = createTree(rows, 'refcode', 'pid');
						this.setState({ menuTreeData });
					}
				}
			}
		});
	}

	//第三步的内容
	getThirdContent = () => {
		const loop = (data) =>
			data.map((item) => {
				if (item.children) {
					return (
						<NCTreeNode key={item.refpk} title={item.refname} refData={item}>
							{loop(item.children)}
						</NCTreeNode>
					);
				}
				return <NCTreeNode key={item.refpk} title={item.refname} refData={item} disabled={true} />;
			});
		let appTreeData = this.state.appTreeData;
		return (
			<NCTree
				onExpand={this.onExpand}
				expandedKeys={this.state.expandedKeys}
				autoExpandParent={this.state.autoExpandParent}
				onSelect={this.onSelect}
				selectedKeys={this.state.selectedKeys}
				showLine={true}
			>
				{loop(appTreeData)}
			</NCTree>
		);
	};

	//第四步的内容
	getFourthContent = () => {
		let { menuitemregData, data } = this.state;
		let { m_headVo } = data;
		let { node_name } = m_headVo;
		const loop = (data) =>
			data.map((item) => {
				if (item.children) {
					return (
						<NCTreeNode key={item.refpk} title={item.refname} refData={item}>
							{loop(item.children)}
						</NCTreeNode>
					);
				}
				return <NCTreeNode key={item.refpk} title={item.refname} refData={item} disabled={true} />;
			});
		let menuTreeData = this.state.menuTreeData;
		return (
			<div>
				<div className="fourth-refer-area-tree" />
				<NCTree
					onExpand={this.onExpand}
					expandedKeys={this.state.expandedKeys}
					autoExpandParent={this.state.autoExpandParent}
					onSelect={this.onSelect2}
					selectedKeys={this.state.selectedKeys2}
					showLine={true}
				>
					{loop(menuTreeData)}
				</NCTree>
				<div className="fourth-refer-area">
					<div className="first-div" style={{ marginRight: '20x' }}>
						<div className="Code">
							<span className="star">*</span>
							<span>{this.state.json['initbill-000058']}</span>
							{/* 国际化处理： 菜单项编码*/}
						</div>
						<NCFormControl value={menuitemregData.refcode} disabled={true} />
					</div>
					<div className="second-div">
						<div className="name">
							<span className="star">*</span>
							<span>{this.state.json['initbill-000059']}</span>
							{/* 国际化处理： 菜单项名称*/}
						</div>
						<NCFormControl
							value={node_name}
							onChange={(v) => {
								this.state.data.m_headVo.node_name = v;
								this.setState({
									node_name
								});
							}}
						/>
					</div>
				</div>
			</div>
		);
	};

	//第五步的内容
	getFifthContent = () => {
		let { isPublishtype } = this.state;
		return (
			<div>
				<NCRadio.NCRadioGroup selectedValue={isPublishtype}>
					<div className="first-page-radio">
						<div className="radios">
							<span className="radios-tit">{this.state.json['initbill-000060']}:</span>
							{/* 国际化处理： 发布为*/}
							<NCRadio value="query">{this.state.json['initbill-000061']}</NCRadio>
							{/* 国际化处理： 查询*/}
						</div>
					</div>
				</NCRadio.NCRadioGroup>
			</div>
		);
	};

	//弹出框关闭按钮
	handleClose() {
		promptbox(
			this.state.json['initbill-000031'],
			this.callBackOK,
			'info',
			this.state.json['initbill-000032']
		); /* 国际化处理： 确定要取消吗？,取消*/
	}

	//取消按钮
	handlecancel = () => {
		promptbox(
			this.state.json['initbill-000031'],
			this.callBackOK,
			'info',
			this.state.json['initbill-000032']
		); /* 国际化处理： 确定要取消吗？,取消*/
	};

	//promptBox 点击确定的回调函数
	callBackOK = () => {
		this.props.handleInital();
		let initData = {
				m_billField: null,
				m_headVo: {
					reporttype: 'general', // 账表类型
					reportformat: { display: this.state.json['initbill-000017'], value: 'foreign' }, //账页格式/* 国际化处理： 外币金额式*/
					showformat: { display: this.state.json['initbill-000018'], value: 'name' }, //显示格式/* 国际化处理： 名称*/
					rec_pay_direct: '', //收付方向    应收为rec 应付为pay
					publishtype: '', //发布类型  rep_query_node
					isshowquickqrypane: '', //是否显示快
					isshowvoucher: false, //是否显示凭证号
					multiunitshowmode: 0, //多单位显示 多单位合并
					ownmodule: 'arap', //所属模块
					node_code: '', //节点编码
					node_name: '', //节点名称
					createtable: 0, //是否创建物...
					dr: 0,
					appRegisterVO: {
						pk_appregister: ''
					}, //应用注册VO
					appMenuItemVO: {
						pk_menuitem: '',
						parentcode: ''
					} //菜单注册VO
				},
				m_itemVos: []
			},
			initDataSource = [ { name: '' }, { name: '' }, { name: '' }, { name: '' }, { name: '' } ],
			initSelectedData = [
				{
					dsp_objname: '',
					dsp_order: ''
				},
				{
					dsp_objname: '',
					dsp_order: ''
				},
				{
					dsp_objname: '',
					dsp_order: ''
				},
				{
					dsp_objname: '',
					dsp_order: ''
				},
				{
					dsp_objname: '',
					dsp_order: ''
				}
			];
		changeDataSource = initDataSource;
		changeselectedData = initSelectedData;
		this.setState({
			dataSource: initDataSource,
			selectedData: initSelectedData,
			menuitemregData: {},
			data: initData,
			current: 0
		});
	};

	//完成
	handleDone() {
		let { data } = this.state;
		let dataNeww = data.m_headVo;
		let dataNew1 = JSON.parse(JSON.stringify(dataNeww));
		dataNew1.reportformat = data.m_headVo.reportformat.value;
		dataNew1.showformat = data.m_headVo.showformat.value;
		let appre = this.state.appregData;
		let appmenu = this.state.menuitemregData;
		dataNew1.appRegisterVO.pk_appregister = appre.refpk;
		dataNew1.appMenuItemVO.pk_menuitem = appmenu.refpk;
		let dataNew2 = data.m_itemVos;
		let dataNew = {
			reportInitializeVO: dataNew1,
			reportInitializeItemVO: dataNew2
		};
		let url = '/nccloud/arap/reportnodeinit/initreport.do';
		ajax({
			url: url,
			data: dataNew,
			success: (res) => {
				this.callBackOK();
				toast({ content: this.state.json['initbill-000033'], color: 'success' }); /* 国际化处理： 创建成功*/
				this.handleRefresh();
			}
		});
	}

	//重新请求页面函数
	handleRefresh = () => {
		setTimeout(() => {
			this.props.handleGetData();
			//刷新菜单项和小应用树数据
			this.getTreeData('/nccloud/platform/appregister/appregref.do'); //获取第三步内容
			this.getTreeData('/nccloud/platform/appregister/menuitemregref.do'); //获取第四步内容
			this.setState({
				selectedKeys: [],
				selectedKeys2: []
			});
			this.getThirdContent();
			this.getFourthContent();
		}, 1000);
	};

	render() {
		let { show, button } = this.props;
		const { current, steps } = this.state;
		let { createButtonApp } = button;
		return (
			<div>
				<NCModal id="initBillModelId" backdrop={'static'} show={show} onHide={this.handleClose.bind(this)}>
					<NCModal.Header closeButton>
						<NCModal.Title className="modal-tit">
							{this.state.json['initbill-000062']} —— {this.state.json['initbill-000063']}
							<span>{current + 1}</span>
							{this.state.json['initbill-000064']}
							{/* 国际化处理： 账表初始化向导,第,步*/}
						</NCModal.Title>
					</NCModal.Header>
					<NCModal.Body>
						<div className="step">
							<NCSteps current={current}>
								{steps.map((item) => <NCStep key={item.title} title={item.title} />)}
							</NCSteps>
						</div>
						<div className="steps-content">
							{current == 0 && this.getFirstContent()}
							{current == 1 && this.getSecondContent()}
							{current == 2 && this.getThirdContent()}
							{current == 3 && this.getFourthContent()}
							{current == 4 && this.getFifthContent()}
						</div>
					</NCModal.Body>
					<NCModal.Footer>
						{this.state.current > 0 &&
							createButtonApp({
								area: 'list_model_p',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						{this.state.current < 4 &&
							createButtonApp({
								area: 'list_model_n',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						{this.state.current > 3 &&
							createButtonApp({
								area: 'list_model_d',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						{createButtonApp({
							area: 'list_model_c',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</NCModal.Footer>
				</NCModal>
			</div>
		);
	}
}

ReportModal = createPage({
	initTemplate: initTemplate
})(ReportModal);

export default ReportModal;
