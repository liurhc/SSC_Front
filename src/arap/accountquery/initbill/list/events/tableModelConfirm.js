import { ajax } from 'nc-lightapp-front';


export default function(props, saveData, opr) {
	let appcode = props.getSearchParam('c'); //获取小应用编码
	let tableid = appcode+'_list';
	//刷新页面数据的方法
	let refreshFun = function() {
		ajax({
            url: '/nccloud/arap/reportnodeinit/query.do',
            data:{
                pageid: appcode+'_LIST'
            },
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    props.table.setAllTableData(tableid, data[tableid]);
                }
            }
        });
	};
	//保存数据
	let data = { '0001': null };
	data['0001'] = saveData[tableid];
	data['0001'].areacode = '0001';
	data.pageid = appcode+'_LIST';
	let url = '/nccloud/arap/reportnodeinit/getData.do'
	ajax({
		url: url,
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				refreshFun();
			}
		}
	});
}
