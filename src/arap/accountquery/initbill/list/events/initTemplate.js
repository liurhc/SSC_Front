import { ajax, base } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon, NCMessage } = base;
let searchid = '';
let tableid;
let appcode;
export default function(props) {
	const that = this;
	appcode = props.getSearchParam('c'); //获取小应用编码
	let pagecode = props.getSearchParam('p'); //获取页面编码
	tableid = appcode + '_list';
	props.createUIDom(
		{
			pagecode: pagecode, //页面id
			appcode: appcode //注册按钮的id
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta,that);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	);
}

function modifierMeta(props, meta,that) {
	if(!that){
		return;
	}
	//添加表格操作列
	let event = {
		label: that.state.json['initbill-000000'],/* 国际化处理： 操作*/
		attrcode: 'opr',
		itemtype: 'customer',
		visible: true,
		width: '150px',
		fixed: 'right',
		render(text, record, index) {
			return (
				<div className="currency-opr-col">
					<NCPopconfirm
						trigger="click"
						placement="top"
						content={that.state.json['initbill-000001']}/* 国际化处理： 已经发布为节点，删除会影响查询*/
						onClose={() => {
							let delObj = {
								rowId: index,
								status: '3',
								values: {
									ts: {
										display: that.state.json['initbill-000002'],/* 国际化处理： 时间戳*/
										value: record.ts.value
									},
									node_code: {
										display: that.state.json['initbill-000003'],/* 国际化处理： 节点编号*/
										value: record.node_code.value
									}
								}
							};
							let indexArr = [];
							indexArr.push(index);
							let data = {
								pageid: appcode + '_LIST',
								model: {
									areaType: 'table',
									pageinfo: null,
									rows: [ delObj ]
								}
							};
							ajax({
								url: '/nccloud/arap/reportnodeinit/delete.do',
								data: {
									funCode: record.node_code.value,
									ts: record.ts.value
								},
								success: function(res) {
									let { success, data } = res;
									if (success) {
										props.table.deleteTableRowsByIndex(tableid, indexArr);
										NCMessage.create({ content: that.state.json['initbill-000004'], color: 'success', position: 'bottom' });/* 国际化处理： 删除成功*/
										refreshFun(props);
									}
								}
							});
						}}
					>
						<a>{that.state.json['initbill-000005']}</a>{/* 国际化处理： 删除*/}
					</NCPopconfirm>
				</div>
			);
		}
	};
	meta[tableid].items.push(event);
	return meta;
}

//刷新页面数据方法
function refreshFun(props) {
	ajax({
		url: '/nccloud/arap/reportnodeinit/query.do',
		data: {
			pageId: appcode + '_LIST',
			appCode: props.getSearchParam('p').substr(0, 4)
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				props.table.setAllTableData(tableid, data[tableid]);
			}
		}
	});
}
