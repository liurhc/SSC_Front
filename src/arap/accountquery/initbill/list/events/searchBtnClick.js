import { ajax } from 'nc-lightapp-front';
//点击查询，获取查询区数据
export default function searchBtnClick(props,value) {
	let appcode = props.getSearchParam('c'); //获取小应用编码
	let tableid = appcode+'_list';
	if(value){
		let data = {
			conditions: value,
			pageInfo: {
				currentPageIndex:0,
				pageSize:10,
				total:0,
				pageCount:0
			}
		};
		ajax({
			url: '/nccloud/bd/basedoc/testquery.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if(success){
					if(data && data[tableid]){
						props.table.setAllTableData(tableid, res.data[tableid]);
					}else{
						props.table.setAllTableData(tableid, {rows:[]});
					}	
				}
			}
		});
	}else{
		ajax({
			url: '/nccloud/arap/reportnodeinit/query.do',
			data: {
				pageid: appcode+'_LIST'
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if(data && data[tableid]){
						props.table.setAllTableData(tableid, data[tableid]);
					}else{
						props.table.setAllTableData(tableid, {rows:[]});
					}
				}
			}
		});
	}
	
}

