import { modelButton, headButton } from '../../../../public/components/pubUtils/buttonName';

export default function buttonClick(props, id) {
	switch (id) {
		case modelButton.Prev://上一步
			this.prev();
			break;
		case modelButton.Next://下一步
			this.next();
			break;
		case modelButton.Done://完成
			this.handleDone();
			break;
		case modelButton.Cancel://取消
			this.handlecancel();
			break;
		case headButton.InitialReport://报表初始化按钮
			this.handleInital();
			break;
	}
}
