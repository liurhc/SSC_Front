// 单表
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, getMultiLang,createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, pageInfoClick, tableModelConfirm } from './events';
import './index.less';
import InitBillModel from './initBillModel/index';
let tableid;
let appcode;
class SingleTable extends Component {
	constructor(props) {
		appcode = props.getSearchParam('c'); //获取小应用编码
		tableid = appcode + '_list';
		super(props);
		this.props = props;
		//表单meta信息
		this.state = {
			meta9: {},
			isInitBillShow: false, //模态框
			json: {}
		};
		this.getData = this.getData.bind(this);
		this.handleInital = this.handleInital.bind(this);
	}

	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				initTemplate.call(this, this.props);
				this.setState({
					meta9: {
						title: this.state.json['initbill-000006'] /* 国际化处理： 结算方式*/,
						id: 'pk_org', //模糊搜索字段
						buttons: [
							{
								id: 'addButton',
								name: this.state.json['initbill-000007'] /* 国际化处理： 新增*/,
								onButtonClick: this.buttonClick1.bind(this, 'add'),
								colors: 'primary'
							}
						],
						content: this.others(), //其他搜索，没有则不写
						refers: {
							//refer信息,没有则不写
							attrcode: 'dept',
							label: this.state.json['initbill-000008'] /* 国际化处理： 组织*/,
							refcode: 'dept'
						}
					}
				});
				this.getData();
			});
		};
		getMultiLang({ moduleId: 'initbill', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	buttonClick1 = (id) => {
		switch (id) {
			case 'add':
				this.props.table.openModel(tableid, 'add');
				break;
		}
	};
	handleInital = () => {
		this.setState({
			isInitBillShow: !this.state.isInitBillShow
		});
	};
	//请求列表数据
	getData = () => {
		ajax({
			url: '/nccloud/arap/reportnodeinit/query.do',
			data: {
				pageId: appcode + '_LIST',
				appCode: this.props.getSearchParam('p').substr(0, 4)
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data == null) {
					} else {
						this.props.table.setAllTableData(tableid, data[tableid]);
					}
				}
			}
		});
	};
	others() {
		//其他搜索条件，如停启用等
		return <div>test</div>;
	}

	render() {
		let { table, button } = this.props;
		let { createSimpleTable } = table;
		let { createButtonApp } = button;
		return (
			<div className="nc-bill-list" id="finance-reva-pobdoc-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon ? createPageIcon() : null}
						<h2 className="title-search-detail initbill-tit">{this.state.json['initbill-000009']}</h2>
						{/* 国际化处理： 报表初始化*/}
					</div>
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableid, {
						//列表区
						onAfterEvent: afterEvent,
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm
					})}
				</div>
				<InitBillModel
					handleGetData={this.getData}
					show={this.state.isInitBillShow}
					handleInital={this.handleInital}
					json={this.state.json}
				/>
			</div>
		);
	}
}

SingleTable = createPage({})(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
