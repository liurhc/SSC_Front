import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, high, toast, getMultiLang,createPageIcon } from 'nc-lightapp-front';
let { NCMessage, NCPopconfirm, NCFormControl, NCButton } = base;
import './index.less';
import QueryModel from './queryModel/index';
import promptbox from '../../../public/components/promptbox';


let searchid = '';
let tableid = '';
let pagecode = '';
let appcode;
const urls = {
	save: '/nccloud/arap/queryobj/save.do',
	query: '/nccloud/arap/queryobj/query.do'
};
let firstCheck = false;
const isShowOffEnable = false; //是否启用“显示停用”功能
let allTableData = {};
let selectdata = {};
let meta = {};
let olddisplay = '';
//获取并初始化模板
let initTemplate = (props) => {
	appcode = props.getSearchParam('c'); //获取小应用编码
	pagecode = props.getSearchParam('p'); //获取页面编码
	tableid = appcode + '_list';
	props.createUIDom(
		{
			pagecode: pagecode, //页面id
			appcode: appcode
		},
		function(data) {
			if (data) {
				if (data.template) {
					meta = data.template;
					meta[tableid].items.map((item) => {
						if (item.attrcode == 'dsp_objname') {
							ajax({
								url: '/nccloud/arap/queryobj/getData.do',
								data: '',
								success: function(res) {
									selectdata = res.data;
									let obj = {};
									let finaloption = [];
									if (selectdata) {
										selectdata.map((back) => {
											if (appcode && appcode.substring(0, 4) == '2008') {
												if (back.billfieldname && back.billfieldname.split('_')[0] == 'so') {
													let value = back.billfieldname.split('_')[1];
													back.billfieldname = 'pu_' + value;
													if (
														back.tallyfieldname &&
														back.tallyfieldname.split('.')[1] &&
														back.tallyfieldname.split('.')[1].indexOf('so') != -1
													) {
														let value = back.tallyfieldname.split('.')[1].split('_')[1];
														back.tallyfieldname =
															back.tallyfieldname.split('.')[0] + '.pu_' + value;
													}
												}
											}
											obj = {
												display: back.dsp_objname,
												value: back.dsp_objname
											};
											finaloption.push(JSON.parse(JSON.stringify(obj)));
										});
										item.options = finaloption;
										item.itemtype = 'select';
									} else {
										firstCheck = true;
									}
								}
							});
						}
					});
					meta = modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	);
};

//对表格模板进行加工操作
function modifierMeta(props, meta) {
	meta[tableid].showindex = true; //表格显示序号
	//meta[tableid].items.push(event);
	return meta;
}

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.props.button.setButtonsVisible({
			Add: true,
			Edit: true,
			Save: false,
			Cancel: false,
			AddLine: false,
			Recon: true
		});
		this.state = {
			searchValue: '',
			searchDisable: true, //简单搜索框是否禁用	true：禁用		false：可用
			moreButton: false, //更多按钮状态
			showOffDisable: true, //显示停用复选框是否禁用	true：禁用		false：可用
			isShowOff: false, //列表是否显示停用数据
			isQueryModelShow: false, //模态框
			json: {}
		};
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				appcode = this.props.getSearchParam('c');
				this.getData(false);
			});
		};
		initTemplate(this.props);

		getMultiLang({ moduleId: 'queryObjSet', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	//请求列表数据
	getData = (showOff = false) => {
		//如果showOff为true，则显示停用数据，在请求参数data中增加显示停用参数，根据业务前后端自行处理
		ajax({
			url: urls['query'],
			data: {
				pageId: pagecode
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					data[tableid].rows.map((item) => {
						if (item.values['dsp_objname'].value) {
							item.values['dsp_objname'] = {
								display: item.values['dsp_objname'].value,
								value: item.values['dsp_objname'].value
							};
						}
						if (appcode && appcode.substring(0, 4) == '2008') {
							if (
								item.values['billfieldname'].value &&
								item.values['billfieldname'].value.split('_')[0] == 'so'
							) {
								let value = item.values['billfieldname'].value.split('_')[1];
								item.values['billfieldname'].value = 'pu_' + value;
								if (
									item.values['tallyfieldname'].value &&
									item.values['tallyfieldname'].value.split('.')[1] &&
									item.values['tallyfieldname'].value.split('.')[1].indexOf('so') != -1
								) {
									let value = item.values['tallyfieldname'].value.split('.')[1].split('_')[1];
									item.values['tallyfieldname'].value =
										item.values['tallyfieldname'].value.split('.')[0] + '.pu_' + value;
								}
							}
						}
					});

					allTableData = data[tableid];
					this.props.editTable.setTableData(tableid, data[tableid]);
				}
			}
		});
	};

	//表格编辑后事件
	onAfterEvent(props, moduleId, key, changerows, value, index, data) {
		//props, moduleId(区域id), key(操作的键), value（当前值）, changedrows（新旧值集合）, record（行数据）, index（当前index）
		//表格的编辑后事件，暂时没用到，处理表格字段编辑后业务及验证使用
		let length = this.props.editTable.getNumberOfRows(moduleId);
		//刷新页面数据的方法

		if (length - 1 === index && data.status === '2') {
			this.onButtonClick('add');
		}

		//添加下拉框值事件
		if (key == 'dsp_objname') {
			olddisplay = value[0].oldvalue.value;
			selectdata.map((back) => {
				//修改操作
				if (back.dsp_objname == value[0].newvalue.value) {
					//新值与旧值交换
					let data1 = data.values.bd_codefield.value;
					let data2 = data.values.bd_mdid.value;
					let data3 = data.values.bd_namefield.value;
					let data4 = data.values.bd_pkfield.value;
					let data5 = data.values.bd_refname.value;
					let data6 = data.values.bd_tablename.value;
					let data7 = data.values.billfieldname.value;
					let data8 = data.values.dsp_objfieldname.value;
					let data9 = data.values.dsp_objname.value;
					let data10 = data.values.dsp_objtablename.value;
					let data11 = data.values.ownmodule.value;
					let data12 = data.values.qry_objfieldname.value;
					let hasresid = 'no';
					let data13 = '';
					if (data.values.resid) {
						data13 = data.values.resid.value;
						hasresid = 'yes';
					}
					let data14 = data.values.qry_objtablename.value;
					let data15 = data.values.tallyfieldname.value;

					data.values.bd_codefield.value = back.bd_codefield;
					data.values.bd_mdid.value = back.bd_mdid;
					data.values.bd_namefield.value = back.bd_namefield;
					data.values.bd_pkfield.value = back.bd_pkfield;
					data.values.bd_refname.value = back.bd_refname;
					data.values.bd_tablename.value = back.bd_tablename;
					data.values.billfieldname.value = back.billfieldname;
					data.values.dsp_objfieldname.value = back.dsp_objfieldname;
					data.values.dsp_objname.value = back.dsp_objname;
					data.values.dsp_objtablename.value = back.dsp_objtablename;
					data.values.ownmodule.value = back.ownmodule;
					data.values.qry_objfieldname.value = back.qry_objfieldname;
					if (back.resid) {
						//为data添加属性resid
						data.values.resid = {};
						data.values.resid.value = back.resid;
					} else {
						data.values.resid = {};
						data.values.resid.value = null;
					}
					data.values.qry_objtablename.value = back.qry_objtablename;
					data.values.tallyfieldname.value = back.tallyfieldname;

					if (olddisplay) {
						back.bd_codefield = data1;
						back.bd_mdid = data2;
						back.bd_namefield = data3;
						back.bd_pkfield = data4;
						back.bd_refname = data5;
						back.bd_tablename = data6;
						back.billfieldname = data7;
						back.dsp_objfieldname = data8;
						back.dsp_objname = olddisplay;
						back.dsp_objtablename = data10;
						back.ownmodule = data11;
						back.qry_objfieldname = data12;
						if (hasresid == 'yes') {
							back.resid = data13;
						}
						back.qry_objtablename = data14;
						back.tallyfieldname = data15;
					} else {
						back.dsp_objname = 'add';
					}

					//动态修改下拉框内数据
					meta[tableid].items.map((item) => {
						if (item.attrcode == 'dsp_objname') {
							let obj2 = {};
							let finaloption2 = [];
							selectdata.map((back2) => {
								if (back2.dsp_objname != 'add') {
									obj2 = {
										display: back2.dsp_objname,
										value: back2.dsp_objname
									};
									finaloption2.push(JSON.parse(JSON.stringify(obj2)));
								}
							});

							if (finaloption2.length == 0) {
								//item.itemtype = 'input';
								//item.options = finaloption2;
								item.options = [
									{
										display: '',
										value: ''
									}
								];
								item.disabled = true;
								this.props.button.setButtonDisabled('AddLine', true);
								toast({
									color: 'warning',
									content: this.state.json['queryObjSet-000001']
								}); /* 国际化处理： 预制查询对象已用尽，不可再新增！*/
							} else {
								item.options = finaloption2;
							}
						}
					});

					meta = modifierMeta(props, meta);
					this.props.meta.setMeta(meta);
				}
			});
		}
		//开关编辑后事件
		if (key == 'iscreatebal') {
			let allRows = props.editTable.getAllRows(moduleId);
			if (data.values.isqryobj.value == false) {
				toast({ color: 'warning', content: this.state.json['queryObjSet-000002'] }); /* 国际化处理： 请先设置选中查询对象！*/
				data.values.isqryobj = { display: undefined, scale: '-1', value: false, isEdit: false };
				data.values.iscreatebal = { display: undefined, scale: '-1', value: false, isEdit: false };
				data.values.balfieldname = null;
				let retData = data;
				allRows[index] = retData;
				let allData = props.editTable.getAllData(moduleId);
				allData.rows = allRows;
				props.editTable.setTableData(moduleId, allData);
			} else {
				if (value[0].newvalue.value) {
					let allshowData = this.props.editTable.getAllRows(tableid, false);
					let obj = '';
					let finaloption = [];
					allshowData.map((back) => {
						if (back.values.balfieldname) {
							obj = back.values.balfieldname.value;
							finaloption.push(obj);
						}
					});
					let requestData = finaloption;

					ajax({
						url: '/nccloud/arap/queryobj/switch.do',
						data: requestData,
						success: (res) => {
							//此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
							let { success } = res;
							let localData = res.data;
							if (success) {
								if (res.data) {
									data.values.balfieldname = {};
									data.values.balfieldname = { value: res.data };
									let retData = data;
									allRows[index] = retData;
									let allData = props.editTable.getAllData(moduleId);
									allData.rows = allRows;
									props.editTable.setTableData(moduleId, allData);
								} else {
									toast({
										color: 'warning',
										content: this.state.json['queryObjSet-000003']
									}); /* 国际化处理： 余额表只支持不能超过六个扩展查询对象,不能再添加新的！*/
									data.values.iscreatebal = {
										display: undefined,
										scale: '-1',
										value: false,
										isEdit: false
									};
									data.values.balfieldname = null;
									let retData = data;
									allRows[index] = retData;
									let allData = props.editTable.getAllData(moduleId);
									allData.rows = allRows;
									props.editTable.setTableData(moduleId, allData);
								}
							}
						}
					});
				} else {
					data.values.balfieldname = null;
					let retData = data;
					allRows[index] = retData;
					let allData = props.editTable.getAllData(moduleId);
					allData.rows = allRows;
					props.editTable.setTableData(moduleId, allData);
				}
			}
		}
		//新建余额表事件
		if (key == 'isqryobj') {
			let allRows = props.editTable.getAllRows(moduleId);
			if (value[0].newvalue.value) {
			} else {
				data.values.iscreatebal = { display: undefined, scale: '-1', value: false, isEdit: false };
				data.values.balfieldname = null;
				let retData = data;
				allRows[index] = retData;
				let allData = props.editTable.getAllData(moduleId);
				allData.rows = allRows;
				props.editTable.setTableData(moduleId, allData);
			}
		}
	}

	//更新按钮状态
	updateButtonStatus() {
		//此处控制按钮的隐藏显示及启用状态
		let tableData = this.props.editTable.getCheckedRows(tableid);
		let length = tableData.length; //获取列表页选择数据的行数
		if (this.props.editTable.getStatus(tableid) === 'edit') {
			//编辑状态
			this.props.button.setButtonsVisible({
				Add: false,
				Edit: false,
				Save: true,
				Cancel: true,
				Recon: false,
				AddLine: true,
				moreButton: false
			});
			this.setState({
				moreButton: false,
				searchDisable: true,
				showOffDisable: false
			});
		} else {
			//浏览态
			this.props.button.setButtonsVisible({
				Add: true,
				Edit: true,
				Save: false,
				Cancel: false,
				Recon: true,
				AddLine: false,
				moreButton: true
			});
			this.setState({
				moreButton: true,
				searchDisable: false,
				showOffDisable: false
			});
		}
	}

	//显示停用数据
	showOffChange() {
		this.setState({
			isShowOff: !this.state.isShowOff
		});
		this.getData(this.state.isShowOff);
	}

	//按钮点击事件
	onButtonClick(props, id) {
		switch (id) {
			case 'Add':
				if (firstCheck) {
					toast({
						color: 'warning',
						content: this.state.json['queryObjSet-000000']
					}); /* 国际化处理： 预制查询对象已用尽，不可再新增！*/
					return;
				}
				let num = props.editTable.getNumberOfRows(tableid); //获取列表总行数
				//设置对象显示顺序
				let datas = props.editTable.getAllRows(tableid, false);
				let ordernum = datas.length == 0 ? 1 : datas[num * 1 - 1].values.dsp_order.value * 1 + 1;
				let scale = 0;
				let itemtype = Number;
				props.editTable.addRow(tableid, undefined, true, { dsp_order: { display: ordernum, value: ordernum } });
				// props.editTable.setValByKeyAndIndex(tableid, num-1, 'dsp_order', {value: ordernum,display:ordernum });
				props.editTable.hideColByKey(tableid, 'opr'); //隐藏操作列
				props.editTable.setStatus(tableid, 'edit');
				this.updateButtonStatus();
				break;
			case 'AddLine':
				if (firstCheck) {
					toast({
						color: 'warning',
						content: this.state.json['queryObjSet-000000']
					}); /* 国际化处理： 预制查询对象已用尽，不可再新增！*/
					return;
				}
				let num3 = props.editTable.getNumberOfRows(tableid); //获取列表总行数
				//设置对象显示顺序
				let datas3 = props.editTable.getAllRows(tableid, false);
				let ordernum3 = datas3.length == 0 ? 1 : datas3[num3 * 1 - 1].values.dsp_order.value * 1 + 1;
				props.editTable.addRow(tableid, undefined, true, {
					dsp_order: { display: ordernum3, value: ordernum3 }
				});
				break;
			case 'Edit':
				props.editTable.setStatus(tableid, 'edit');
				this.updateButtonStatus();
				break;
			case 'Cancel':
				promptbox(
					this.state.json['queryObjSet-000021'],
					function() {
						props.editTable.cancelEdit(tableid);
						window.location.reload();
					},
					'warning',
					this.state.json['queryObjSet-000019']
				); /* 国际化处理： ​确定要取消吗？,取消*/
				break;
			case 'Recon':
				this.handleQueryModel();
				break;
			case 'Save':
				let tableData = props.editTable.getChangedRows(tableid); //保存时，只获取改变的数据行而不是table的所有数据行，减少数据传输
				tableData.forEach((td) => {
					if (td.values.balfieldname == null) {
						td.values.balfieldname = { display: null, scale: null, value: '' };
					}
					if (appcode && appcode.substring(0, 4) == '2008') {
						if (td.values.billfieldname.value && td.values.billfieldname.value.split('_')[0] == 'pu') {
							let value = td.values.billfieldname.value.split('_')[1];
							td.values.billfieldname.value = 'so_' + value;
							if (
								td.values.tallyfieldname.value &&
								td.values.tallyfieldname.value.split('.')[1] &&
								td.values.tallyfieldname.value.split('.')[1].indexOf('pu') != -1
							) {
								let value = td.values.tallyfieldname.value.split('.')[1].split('_')[1];
								td.values.tallyfieldname.value =
									td.values.tallyfieldname.value.split('.')[0] + '.so_' + value;
							}
						}
					}
				});

				let data = {
					pageid: pagecode,
					model: {
						areaType: 'table',
						pageinfo: null,
						rows: []
					}
				};

				data.model.rows = tableData;
				ajax({
					url: urls['save'],
					data,
					success: function(res) {
						//此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
						let { success } = res;
						if (success) {
							props.editTable.setStatus(tableid, 'browse'); //设置表格状态为浏览态
							props.editTable.showColByKey(tableid, 'opr'); //显示操作列
							this.updateButtonStatus(); //更新按钮状态
							ajax({
								url: urls['query'],
								data: {
									pageId: pagecode
								},
								success: (res) => {
									let { success, data } = res;
									if (success) {
										toast({
											color: 'success',
											content: this.state.json['queryObjSet-000020']
										}); /* 国际化处理： 保存成功！*/
										data[tableid].rows.map((item) => {
											if (item.values['dsp_objname'].value) {
												item.values['dsp_objname'] = {
													display: item.values['dsp_objname'].value,
													value: item.values['dsp_objname'].value
												};
											}
										});
										allTableData = data[tableid];
										props.editTable.setTableData(tableid, data[tableid]);
									}
								}
							});
						}
					}.bind(this)
				});
				break;
		}
	}

	handleQueryModel = () => {
		const { isQueryModelShow } = this.state;
		this.setState({
			isQueryModelShow: !this.state.isQueryModelShow
		});
	};

	render() {
		let { table, button, search, editTable } = this.props;
		let { createEditTable } = editTable;
		let { NCCreateSearch } = search;
		let { createButton } = button;
		let { NCFormControl, NCCheckbox } = base;

		return (
			<div id="queryObjSetId">
				{/* 标题 title */}
				<div className="header">
					{createPageIcon ? createPageIcon() : null}
					<h2 className="title">{this.state.json['queryObjSet-000004']}</h2>
					{/* 国际化处理： 查询对象注册*/}
					{/* 显示停用数据 */}
					{isShowOffEnable ? (
						<span className="showOff">
							<NCCheckbox
								checked={this.state.isShowOff}
								onChange={this.showOffChange.bind(this)}
								disabled={this.state.showOffDisable}
							>
								{this.state.json['queryObjSet-000005']}
								{/* 国际化处理： 显示停用*/}
							</NCCheckbox>
						</span>
					) : (
						''
					)}
					{/* 按钮区  btn-group */}
					{/* {createButton('addButton', {name: '新增',style:{height:'30px','line-height':'30px'},onButtonClick: this.onButtonClick.bind(this,'add'),colors:'danger'})} */}
					<div className="btn-group">
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: this.onButtonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>

				{/* 列表区 */}
				<div id="query-model">
					<div className="table-area">
						{createEditTable(tableid, {
							//列表区
							onAfterEvent: this.onAfterEvent.bind(this), // 控件的编辑后事件
							useFixedHeader: true,
							selectedChange: this.updateButtonStatus.bind(this), // 选择框有变动的钩子函数
							statusChange: this.updateButtonStatus.bind(this) //表格状态监听
						})}
					</div>
				</div>
				{appcode && (
					<QueryModel
						show={this.state.isQueryModelShow}
						handleQueryModel={this.handleQueryModel.bind(this)}
						appcode={appcode && appcode}
						json={this.state.json}
					/>
				)}
			</div>
		);
	}
}

SingleTable = createPage(
	{
		//initTemplate: initTemplate
	}
)(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
