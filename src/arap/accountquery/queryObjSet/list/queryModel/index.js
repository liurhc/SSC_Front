import React, { Component } from 'react';
import { ajax, base, promptBox, toast, getMultiLang } from 'nc-lightapp-front';
import Referloader from '../../../../public/ReferLoader';
import AccPeriodDefaultTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef'; //时间参照
import './querymodal.less';
import '../../../../public/less/radio.less';
const { NCRadio, NCModal, NCButton, NCRow: Row, NCCol: Col, NCForm, NCHotKeys } = base;
const NCFormItem = NCForm.NCFormItem;

export default class QueryModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			MainData: {
				flag: { display: null, value: 3 }, //余额表类型
				pk_org: { display: null, value: null }, //财务组织
				begintime: { display: '', value: null }, //开始时间
				endtime: { display: '', value: null }, //结束时间
				refpks: { display: '', value: null } // 客户/供应商
			},
			appcode: '',
			json: {}
		};
	}
	componentWillReceiveProps(nextProps) {
		if (this.state.json == nextProps.json) return;
		this.setState(
			{
				json: nextProps.json
			},
			() => {
				let { appcode } = this.state;
				appcode = this.props.appcode;
				this.setState({
					appcode
				});
			}
		);
	}
	//弹出框关闭按钮
	handleClose = () => {
		this.props.handleQueryModel();
		let MainData = {
			flag: { display: null, value: 3 },
			pk_org: { display: null, value: null },
			begintime: { display: '', value: null },
			endtime: { display: '', value: null },
			refpks: { display: '', value: null }
		};
		this.setState({
			MainData: MainData
		});
	};

	//确定
	handleConfirm = () => {
		let { MainData } = this.state;
		if (MainData.pk_org.value && MainData.endtime.display) {
			let data = {
				flag: MainData.flag.value,
				pk_org: MainData.pk_org.value,
				begintime: MainData.begintime.display,
				endtime: MainData.endtime.display,
				refpks: [ MainData.refpks.value ]
			};
			ajax({
				url: '/nccloud/arap/queryobj/rebuild.do',
				data: data,
				success: (res) => {
					if (res.data == 'success') {
						toast({
							duration: 3,
							color: 'success',
							content: this.state.json['queryObjSet-000006'] /* 国际化处理： 重建余额表成功！*/
						});
						let MainData = {
							flag: { display: null, value: 3 },
							pk_org: { display: null, value: null },
							begintime: { display: '', value: null },
							endtime: { display: '', value: null },
							refpks: { display: '', value: null }
						};
						this.setState({
							MainData: MainData
						});
					}
				}
			});
			this.props.handleQueryModel();
		} else {
			if (!MainData.pk_org.value) {
				toast({
					duration: 3,
					color: 'warning',
					content: this.state.json['queryObjSet-000007'] /* 国际化处理： 请选择财务组织!*/
				});
				return;
			}
			if (!MainData.endtime.display) {
				toast({
					duration: 3,
					color: 'warning',
					content: this.state.json['queryObjSet-000008'] /* 国际化处理： 请选择结束期间!*/
				});
				return;
			}
		}
	};

	//取消
	handleCancel = () => {
		this.props.handleQueryModel();
		let MainData = {
			flag: { display: null, value: 3 },
			pk_org: { display: null, value: null },
			begintime: { display: '', value: null },
			endtime: { display: '', value: null },
			refpks: { display: '', value: null }
		};
		this.setState({
			MainData: MainData
		});
	};

	//参照展示
	getRefer2 = (Refer, schemeData, name, key) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<Refer
						value={{
							refname: key.display,
							refpk: key.value
						}}
						queryCondition={() => {
							return {
								isadj: 'N'
							};
						}}
						onChange={(v) => {
							key.value = v.refpk;
							key.display = v.refname;
							this.setState({
								schemeData
							});
						}}
						placeholder={name}
					/>
				</div>
			</div>
		);
	};

	//参照展示
	getRefer = (tag, refcode, key, mainData, flag, isShowStar, name) => {
		return (
			<div className="finance">
				<div className="finance-refer">
					<Referloader
						showStar={isShowStar}
						tag={tag}
						refcode={refcode}
						value={{
							refname: key.display,
							refpk: key.value,
							refpk2: key.refpk2
						}}
						queryCondition={() => {
							if (flag == 'cus' || flag == 'sup') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									pk_org:
										this.state.mainData && this.state.mainData.pk_org
											? this.state.mainData.pk_org.value
											: null
								};
							} else if (flag == 'pk_org') {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									AppCode: this.state.appcode,
									TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
								};
							} else {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									isDataPowerEnable: 'Y',
									isadj: 'N'
								};
							}
						}}
						onChange={(v) => {
							key.value = v.refpk;
							key.display = v.refname;
							key.refpk2 = v.refpk2;
							this.setState({
								mainData
							});
						}}
						placeholder={name}
					/>
				</div>
			</div>
		);
	};

	render() {
		let { show } = this.props;
		let { MainData } = this.state;
		let { pk_org, begintime, endtime, flag, refpks } = MainData;
		return (
			<div>
				<NCModal
					id="queryModalId"
					className="query-modal"
					backdrop={'static'} //遮罩层点击是否触发关闭
					show={show}
					onHide={this.handleClose.bind(this)}
				>
					<NCModal.Header closeButton>
						<NCModal.Title className="modal-tit">{this.state.json['queryObjSet-000014']}</NCModal.Title>
						{/* 国际化处理： 重建余额表*/}
					</NCModal.Header>
					<NCModal.Body>
						<div className="steps-content">
							<NCRadio.NCRadioGroup
								name=""
								selectedValue={flag.value}
								onChange={(v) => {
									this.state.MainData.flag.value = v;
									this.state.MainData.refpks = { display: '', value: null };
									this.setState({
										MainData
									});
								}}
							>
								<div className="first-page-radio">
									<span>{this.state.json['queryObjSet-000015']}</span>
									{/* 国际化处理： 余额表类型*/}
									<div className="radios">
										<NCRadio value={3}>{this.state.json['queryObjSet-000016']}</NCRadio>
										{/* 国际化处理： 应收余额表*/}
										<NCRadio value={4}>{this.state.json['queryObjSet-000017']}</NCRadio>
										{/* 国际化处理： 应付余额表*/}
									</div>
								</div>
							</NCRadio.NCRadioGroup>
							<NCFormItem
								showMast={true}
								labelName={this.state.json['queryObjSet-000009']}
								isRequire={true}
								inline={true}
							>
								{/* 国际化处理： 财务组织*/}
								{this.getRefer(
									'FinanceOrgTreeRef_Acc',
									'uapbd/refer/org/FinanceOrgTreeRef/index.js',
									pk_org,
									MainData,
									'pk_org',
									false,
									this.state.json['queryObjSet-000009']
								)}
							</NCFormItem>
							<NCFormItem
								showMast={false}
								labelName={this.state.json['queryObjSet-000010']}
								isRequire={true}
								inline={true}
							>
								{/* 国际化处理： 开始时间*/}
								{this.getRefer2(
									AccPeriodDefaultTreeGridRef,
									MainData,
									this.state.json['queryObjSet-000010'],
									begintime
								)}
								{/* 国际化处理： 开始时间*/}
							</NCFormItem>
							<NCFormItem
								showMast={true}
								labelName={this.state.json['queryObjSet-000011']}
								isRequire={true}
								inline={true}
							>
								{/* 国际化处理： 结束时间*/}
								{this.getRefer(
									'AccPeriodDefaultTreeGridRef_Acc_Beg',
									'uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index.js',
									endtime,
									MainData,
									'endtime',
									false,
									this.state.json['queryObjSet-000011'] /* 国际化处理： 结束时间*/
								)}
							</NCFormItem>
							<NCFormItem
								showMast={false}
								labelName={
									MainData.flag.value == 3 ? (
										this.state.json['queryObjSet-000012']
									) : (
										this.state.json['queryObjSet-000013']
									)
								} /* 国际化处理： 客户,供应商*/
								isRequire={true}
								inline={true}
							>
								{MainData.flag.value == 3 ? (
									<div>
										{this.getRefer(
											'CustomerDefaultTreeGridRef_Acc',
											'uapbd/refer/customer/CustomerDefaultTreeGridRef/index.js',
											refpks,
											MainData,
											'cus',
											false,
											this.state.json['queryObjSet-000012']
										)}
									</div>
								) : (
									<div>
										{this.getRefer(
											'SupplierRefTreeGridRef_Acc',
											'uapbd/refer/supplier/SupplierRefTreeGridRef/index.js',
											refpks,
											MainData,
											'sup',
											false,
											this.state.json['queryObjSet-000013']
										)}
									</div>
								)}
							</NCFormItem>
						</div>
					</NCModal.Body>
					<NCModal.Footer>
						<NCButton colors="primary" onClick={() => this.handleConfirm()}>
							{this.state.json['queryObjSet-000018']}
							{/* 国际化处理： 确定*/}
						</NCButton>
						<NCButton onClick={() => this.handleCancel()}>{this.state.json['queryObjSet-000019']}</NCButton>
						{/* 国际化处理： 取消*/}
					</NCModal.Footer>
				</NCModal>
			</div>
		);
	}
}
