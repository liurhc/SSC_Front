import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, pageTo, ajax, getBusinessInfo, base, getMultiLang } from 'nc-lightapp-front';
import echarts from 'echarts';
import FinanceOrgTreeRef from '../../../../uapbd/refer/org/FinanceOrgTreeRef/index.js';
import './index.less';
let { NCIcon, NCTooltip } = base;
let option = {
	tooltip: {
		trigger: 'item',
		formatter: '{b} {d}%<br/>{c}'
	},
	color: [ 'rgba(16,182,180,1)', '#2DD9CD', '#64E6DD', '#C3F7F5' ],
	series: [
		{
			name: '',
			type: 'pie',
			center: [ 96, 145 ],
			radius: [ 61, 85 ],
			animation: false,
			hoverAnimation: false,
			hoverOffset: 0,
			avoidLabelOverlap: false,
			label: {
				normal: {
					show: false,
					position: 'center'
				}
			},
			labelLine: {
				normal: {
					show: false
				}
			},
			data: []
		}
	]
};
// 非正常数据配置项
let optionNull = {
	tooltip: {
		trigger: 'none'
	},
	color: [ '#D3D9DF' ],
	series: [
		{
			name: '',
			type: 'pie',
			center: [ 96, 145 ],
			radius: [ 61, 85 ],
			animation: false,
			legendHoverLink: false,
			hoverAnimation: false,
			hoverOffset: 0,
			avoidLabelOverlap: false,
			label: {
				normal: {
					show: false,
					position: 'center'
				}
			},
			labelLine: {
				normal: {
					show: false
				}
			},
			data: [ { name: '', value: 100 } ]
		}
	]
};

class RecAccountAge extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		// 获取小部件编码
		this.appcode = this.getParam('appcode');
		this.state = {
			// 参照初始值
			accRefVal: {
				refname: '--',
				refpk: ''
			},
			legendData: [],
			pieData: [],
			// 数据状态,正常数据和100，非正常数据0,
			noDataFlag: false,
			json: {},
			recId: 'arap-widget-re-pie'
		};
		//this.getData = this.getData.bind(this);
		// echarts实例
		this.myChart = null;
		// 正常数据配置项

		// 参照默认值
		this.defaultRef = null;
		this.handleJump = this.handleJump.bind(this);
		this.handleRefresh = this.handleRefresh.bind(this);
	}
	/**
	 * @method 当前页面挂载完成时调用
	 */

	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		getMultiLang({ moduleId: 'widget', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {
		// 获取参照默认值方法
		this.props.createUIDom(
			{
				pagecode: '', //页面id
				appcode: '' //注册按钮的id
			},
			(data) => {
				// 赋值
				this.defaultRef = {
					refname: data.context.org_Name ? data.context.org_Name : '--',
					refpk: data.context.pk_org
					// refpk: ""
				};
				// 更改参状态，从新请求渲染页面
				this.setState(
					{
						accRefVal: this.defaultRef,
						recId: 'arap-widget-re-pie-' + JSON.parse(JSON.stringify(new Date().valueOf()))
					},
					() => {
						this.myChart = echarts.init(document.getElementById(this.state.recId));
						//使用刚指定的配置项和数据显示图表。
						this.myChart.setOption(option);
						this.getData();
					}
				);
			}
		);
	}

	// 请求数据
	getData() {
		// 获取系统信息
		let businessInfo = getBusinessInfo();
		ajax({
			url: '/nccloud/arap/recaccountWidget/query.do',
			data: {
				pkOrg: this.state.accRefVal.refpk,
				busidate: businessInfo.businessDate,
				pkGroup: businessInfo.groupId
			},
			success: (res) => {
				let { data } = res;
				let pieData = data.pieData;
				// 判断数据状态
				if (!data.nullFlag) {
					option.series[0].data = pieData;
					this.myChart.setOption(option);
					this.setState({
						legendData: data.legendData,
						pieData: pieData,
						noDataFlag: data.nullFlag
					});
				} else {
					this.myChart.setOption(optionNull);
					this.setState({
						legendData: data.legendData,
						pieData: pieData,
						noDataFlag: data.nullFlag
					});
				}
			}
		});
	}
	// 获取小部件参数方法
	getParam = (param) => {
		let appUrl = decodeURIComponent(window.location.href).split('?');
		if (appUrl && appUrl[1]) {
			let appPrams = appUrl[1].split('&');
			if (appPrams && appPrams instanceof Array) {
				let paramObj = {};
				appPrams.forEach((item) => {
					let key = item.split('=')[0];
					let value = item.split('=')[1];
					paramObj[key] = value;
				});
				return paramObj[param];
			}
		}
	};

	// 跳转事件
	handleJump = () => {
		this.props.openTo('', {
			pagecode: '20061906page',
			appcode: '20061906'
		});
	};

	//刷新按钮
	handleRefresh = () => {
		this.getData();
	};

	render() {
		return (
			<div id="arap-widget-rec-account-age">
				<div className="age-header">
					<span>{this.state.json['widget-000007']}</span>
					<span className="age-header-btn-area">
						<i className="se-opento iconfont icon-gengduo" onClick={this.handleJump} />
						<i className="se-refresh iconfont icon-shuaxin" onClick={this.handleRefresh} />
					</span>
				</div>
				<div className="age-body">
					<div className="body-title">
						<p>{this.state.accRefVal.refname}</p>
						<FinanceOrgTreeRef
							value={this.state.accRefVal}
							queryCondition={() => {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									AppCode: '20062101',
									TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
								};
							}}
							container={window.parent && window.parent.document.body}
							onChange={(val) => {
								this.setState(
									{
										accRefVal: val
									},
									() => {
										this.getData();
									}
								);
							}}
							clickContainer={
								<span className="re-refer-switch">{this.state.json['widget-000002']}</span>
							} /* 国际化处理： 切换*/
						/>
					</div>
					<div className="body-num">
						<div id={this.state.recId} className="arap-re-num-echarts" />
						<div className="num-info">
							{this.state.pieData.map((item) => {
								return (
									// 控制样式
									<div className={this.state.noDataFlag ? 'no-data-flag' : ''}>
										<span className="re-text">{item.name}</span>
										<span className="re-val">
											{item.formatVal == null || item.formatVal.length == 0 ? 0 : item.formatVal}
										</span>
									</div>
								);
							})}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

RecAccountAge = createPage({})(RecAccountAge);

export default RecAccountAge;
