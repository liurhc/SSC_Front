import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, toast, ajax, getBusinessInfo, cacheTools, base, getMultiLang } from 'nc-lightapp-front';
import FinanceOrgTreeRef from '../../../../uapbd/refer/org/FinanceOrgTreeRef/index.js';
import getByStrLen from '../../../public/components/pubUtils/getStrLength';
import './index.less';

const { NCMessage } = base;
class Payabletodo extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = {
			accRefVal: {
				refname: '--',
				refpk: ''
			},
			payableSaveCount: '0',
			payableNoVerifyCount: '0',
			paySaveCount: '0',
			payNoVerifyCount: '0',
			beginDate: '',
			endDate: '',
			json: {}
		};
		//初始化appcode
		this.appcode = '';
		// 参照默认值
		this.defaultRef = null;
		//查询默认pk
		this.handleRefresh = this.handleRefresh.bind(this);
		// let callback = (json) => {
		// 	this.setState({ json: json });
		// };
		// getMultiLang({ moduleId: 'widget', domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	/**
	 * @method 当前页面挂载完成时调用
	 */

	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json });
		};
		getMultiLang({ moduleId: 'widget', domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	componentDidMount() {
		this.getAppcode();
		ajax({
			url: '/nccloud/platform/appregister/queryappcontext.do',
			data: {
				appcode: this.appcode
			},
			success: (res) => {
				let { data } = res;
				this.defaultRef = {
					refname: data.org_Name ? data.org_Name : '--',
					refpk: data.pk_org
					// refpk: ""
				};
				this.setState({
					accRefVal: this.defaultRef
				});
				this.getData();
			},
			error: (res) => {}
		});
	}

	getData() {
		let businessInfo = getBusinessInfo();
		ajax({
			url: '/nccloud/arap/paytodowidget/query.do',
			data: {
				pkOrg: this.state.accRefVal.refpk,
				busiDate: businessInfo.businessDate
			},
			success: (res) => {
				let { data } = res;
				let data1 = data.data;
				let data2 = data.search;
				this.setState({
					payableSaveCount: data1.payableSaveCount,
					payableNoVerifyCount: data1.payableNoVerifyCount,
					paySaveCount: data1.paySaveCount,
					payNoVerifyCount: data1.payNoVerifyCount,
					beginDate: data2.beginDate,
					endDate: data2.endDate
				});
			},
			error: (res) => {}
		});
	}
	getAppcode = (pram) => {
		ajax({
			url: '/nccloud/arap/arappub/queryrelatedapp.do',
			data: {
				billType: sign
			},
			success: (res) => {
				let data1 = res.data;
				if (data1 != null) {
					let param = data1.appcode;
					this.appcode = param;
				}
			}
		});
	};
	skip = (url, sign, type, table, count) => {
		if (count == 0) {
			toast({ color: 'warning', content: this.state.json['widget-000000'] });
		} else {
			ajax({
				url: '/nccloud/arap/arappub/queryrelatedapp.do',
				data: {
					billType: sign
				},
				success: (res) => {
					let data1 = res.data;
					let param = data1.appcode;
					//设置缓存
					let RecToDoSearchVO = {
						pkOrg: this.state.accRefVal.refpk,
						beginDate: this.state.beginDate,
						endDate: this.state.endDate,
						status: type,
						table: table
					};
					cacheTools.set(sign + 'widget', RecToDoSearchVO);
					this.props.openTo(url, {
						appcode: param,
						src: 'widget'
					});
				},
				error: (res) => {}
			});
		}
	};

	//刷新按钮
	handleRefresh = () => {
		this.getData();
	};

	render() {
		let { accRefVal, payableSaveCount, payableNoVerifyCount, paySaveCount, payNoVerifyCount } = this.state;
		return (
			<div id="arap-widget-pay-able-todo">
				<div className="todo-header">
					<p>{this.state.json['widget-000001']}</p>
					{/* 国际化处理： 应付待办事项*/}
					<i className="se-refresh iconfont icon-shuaxin" onClick={this.handleRefresh} /> {/* 刷新按钮*/}
				</div>
				<div className="todo-body">
					<div className="body-title">
						<p className={getByStrLen(accRefVal.refname) ? 'title-style' : ''}>{accRefVal.refname}</p>
						<FinanceOrgTreeRef
							value={accRefVal}
							queryCondition={() => {
								return {
									DataPowerOperationCode: 'fi', //使用权组
									AppCode: '20082101',
									TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
								};
							}}
							container={window.parent && window.parent.document.body}
							onChange={(val) => {
								this.setState(
									{
										accRefVal: val
									},
									() => {
										this.getData();
									}
								);
							}}
							clickContainer={
								<span className="re-refer-switch">{this.state.json['widget-000002']}</span>
							} /* 国际化处理： 切换*/
						/>
					</div>
					<div className="body-payable">
						<div className="payable-title">{this.state.json['widget-000003']}</div>
						{/* 国际化处理： 应付单*/}
						<div className="payable-data">
							<div className="data-img" />
							<div className="data-num">
								<p
									onClick={this.skip.bind(
										this,
										'/nccloud/resources/arap/payablebill/payablebill/main/index.html#/list',
										'F1',
										'-1',
										'ap_payablebill',
										payableSaveCount
									)}
								>
									<span>{payableSaveCount}</span>
									<span>{this.state.json['widget-000004']}</span>
									{/* 国际化处理： 已保存*/}
								</p>
								<p
									onClick={this.skip.bind(
										this,
										'/nccloud/resources/arap/payablebill/payablebill/main/index.html#/list',
										'F1',
										'10',
										'ap_payableitem',
										payableNoVerifyCount
									)}
								>
									<span>{payableNoVerifyCount}</span>
									<span>{this.state.json['widget-000005']}</span>
									{/* 国际化处理： 未核销*/}
								</p>
							</div>
						</div>
					</div>
					<div className="body-payment">
						<div className="payment-title">{this.state.json['widget-000006']}</div>
						{/* 国际化处理： 付款单*/}
						<div className="payment-data">
							<div className="data-img" />
							<div className="data-num">
								<p
									onClick={this.skip.bind(
										this,
										'/nccloud/resources/arap/paybill/paybill/main/index.html#/list',
										'F3',
										'-1',
										'ap_paybill',
										paySaveCount
									)}
								>
									<span>{paySaveCount}</span>
									<span>{this.state.json['widget-000004']}</span>
									{/* 国际化处理： 已保存*/}
								</p>
								<p
									onClick={this.skip.bind(
										this,
										'/nccloud/resources/arap/paybill/paybill/main/index.html#/list',
										'F3',
										'10',
										'ap_payitem',
										payNoVerifyCount
									)}
								>
									<span>{payNoVerifyCount}</span>
									<span>{this.state.json['widget-000005']}</span>
									{/* 国际化处理： 未核销*/}
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

Payabletodo = createPage({})(Payabletodo);

export default Payabletodo;
