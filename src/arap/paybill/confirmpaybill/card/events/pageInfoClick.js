import {ajax ,cardCache} from 'nc-lightapp-front';
import { formId ,dataSource,pkname} from '../constants';
import initTemplate from './initTemplate';
let { getCacheById, updateCache } = cardCache;

export default function (props, pk) {
    if(!pk){//如果刷新了浏览器，那么pk将不会存在，如果pk不存在，return
        return;
    }
    let cardData = getCacheById(pk, dataSource);
    let tradeType  = this.getPagecode();
    if(cardData){
        let pagecode = cardData.head[this.formId].rows[0].values.pk_tradetype.value;
        props.setUrlParam({status:'browse',id:pk,pagecode:pagecode})
        if(tradeType !=pagecode){
            initTemplate.call(this, this.props);   
        }  
        props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
        props.cardTable.setTableData(this.tableId, cardData.body[this.tableId],null, null, true);
                        
        this.toggleShow();
        
    }else{
        let data = {
            pk_bill: pk,
        };
        ajax({
            url: '/nccloud/arap/confirmpaybill/querycard.do',
            data: data,
            success: (res) => {
                if (res.data) {
                    if (res.data.head) {
                        this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                    }
                    if (res.data.body) {
                        this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId],null, null, true);
                    }
                    updateCache(pkname,pk,res.data,this.formId,dataSource);
                    let pagecode = res.data.head[formId].rows[0].values.pk_tradetype.value;
                    props.setUrlParam({status:'browse',id:pk,pagecode:pagecode})
                    if(tradeType !=pagecode){
                        initTemplate.call(this, this.props);   
                    }
                    this.toggleShow();
                } 
            }
        });
    }
    
}
