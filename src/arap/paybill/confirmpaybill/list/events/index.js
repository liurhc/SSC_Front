import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import doubleClick from './doubleClick';
import tableButtonClick  from './tableButtonClick';
export { searchBtnClick, pageInfoClick,initTemplate,buttonClick, doubleClick,tableButtonClick};
