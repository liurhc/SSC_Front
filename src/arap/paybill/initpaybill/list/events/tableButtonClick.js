import { ajax, base, toast,cardCache } from 'nc-lightapp-front';
import { billType,tableId ,dataSource} from '../constants';
import {innerButton} from '../../../../public/components/pubUtils/buttonName.js';

export default function(that,props, key, text, record, index){
    switch (key) {
        case innerButton.Edit_inner://修改
            ajax({
                url: '/nccloud/arap/init/edit.do',
                data: {
                    pk_bill: record.pk_paybill.value,
                    billType: billType
                },
                success: (res) => {
                    if (res.success) {
                        props.pushTo('/card', {
                            status: 'edit',
                            id: record.pk_paybill.value,
                            pagecode:record.pk_tradetype.value,
                        })
                    }
                }
            });
            break;
        case innerButton.Delete_inner://删除
            ajax({
                url: '/nccloud/arap/initpaybill/delete.do',
                data: [
                    {
                        pk_bill: record.pk_paybill.value,
                        ts: record.ts.value,
                        billType: billType
                    }
                ],
                success: (res) => {
                    if (res.success) {
                        toast({color: 'success', content: that.state.json['paybill-000009']});/* 国际化处理： 删除成功*/
                        //删除当前行数据
                        props.table.deleteTableRowsByIndex(tableId, index);
                        let {deleteCacheId} = props.table;
                        //删除缓存数据
                        deleteCacheId(tableId,record.pk_paybill.value);
                        that.onSelected();
                    }
                }
            });
            break;
        case innerButton.Copy_inner://复制
            props.pushTo('/card', {
                status: 'add',
                id: record.pk_paybill.value,
                type: 'copy',
                pagecode:record.pk_tradetype.value
            })           
            break;
        default:
            break;
    }
};
