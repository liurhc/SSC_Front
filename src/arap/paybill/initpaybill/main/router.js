import {asyncComponent} from 'nc-lightapp-front';
import List from '../list';


const card = asyncComponent(() => import(/* webpackChunkName: "arap/paybill/initpaybill/card/card" */ /* webpackMode: "eager" */'../card'));

const routes = [
  {
    path: '/list',
    component: List,
    exact: true,
  },
  {
    path: '/card',//定义路由
    component: card,
  }
];

export default routes;
