import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import doubleClick from './doubleClick';
export { searchBtnClick, pageInfoClick,initTemplate,buttonClick ,doubleClick};
