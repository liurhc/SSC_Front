import { cardCache } from 'nc-lightapp-front';
let { setDefData } = cardCache;
import { searchId, searchKey, dataSource } from '../constants';
export default function doubleClick(record, index, e) {
    //表页跳转的时候，获取所有查询区条件，并放入缓存
    let searchVal = this.props.search.getAllSearchData(searchId);
    if (searchVal) {
        setDefData(searchKey, dataSource, searchVal);
    }
    if (this.props.getUrlParam("scene") == 'linksce' || this.props.getUrlParam("scene") == 'fip') {
        this.props.pushTo('/card', {
            status: 'browse',
            id: record.pk_paybill.value,
            pagecode: '20080EBM_CARD_LINK',
            scene: 'linksce'
        })
    } else {
        this.props.pushTo('/card', {
            status: 'browse',
            id: record.pk_paybill.value,
            pagecode: record.pk_tradetype.value,
        })
    }


}
