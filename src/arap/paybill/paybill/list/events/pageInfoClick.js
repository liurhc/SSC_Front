import {ajax} from 'nc-lightapp-front';
import { tableId} from '../constants';

export default function (props, config, pks) {
    let that = this;
    if(!pks || pks.length == 0){
        return ;
    }
    let data = {
        pk_bills: pks,
        pageId: props.getSearchParam('p')
    };
    ajax({
        url: '/nccloud/arap/paybill/querygridbyids.do',
        data: data,
        success: function (res) {
            let { success, data } = res;
            if (success) {
                if (data) {
                    props.table.setAllTableData(tableId, data[tableId]);
                } else {
                    props.table.setAllTableData(tableId, { rows: [] });
                }
            }
            that.onSelected();
        }
    });
}
