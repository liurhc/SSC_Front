import { ajax, toast ,promptBox} from 'nc-lightapp-front';
import { billType, tableId } from '../constants';
import { innerButton } from '../../../../public/components/pubUtils/buttonName.js';
import madeBill from '../../../../public/components/madeBill.js';
export default function (that,props, key, text, record, index) {
    switch (key) {
        case innerButton.Edit_inner:
            ajax({
                url: '/nccloud/arap/arappub/edit.do',
                data: {
                    pk_bill: record.pk_paybill.value,
                    billType: billType
                },
                success: (res) => {
                    if (res.success) {
                        if (props.getUrlParam("scene") == 'linksce' || props.getUrlParam("scene") == 'fip') {
                            props.pushTo('/card', {
                                status: 'edit',
                                id: record.pk_paybill.value,
                                pagecode:'20080EBM_CARD_LINK',
                                scene:'linksce'
                            })
                        }else{
                            props.pushTo('/card', {
                                status: 'edit',
                                id: record.pk_paybill.value,
                                pagecode:record.pk_tradetype.value,
                            })
                        }
                    }
                }
            });
            break;
        case innerButton.Delete_inner:
            that.Info.pk_bill = record.pk_paybill.value
            that.Info.ts = record.ts.value
            that.Info.index = index;
            that.delConfirm();           
            break;
        case innerButton.Copy_inner:
            if (props.getUrlParam("scene") == 'linksce' ||props.getUrlParam("scene") == 'fip') {
                props.pushTo('/card', {
                    status: 'add',
                    id: record.pk_paybill.value,
                    type: 'copy',
                    pagecode:'20080EBM_CARD_LINK',
                    scene:'linksce'
                })
            }else{
                props.pushTo('/card', {
                    status: 'add',
                    id: record.pk_paybill.value,
                    type: 'copy',
                    pagecode:record.pk_tradetype.value,
                })
            }
            break;
        case innerButton.Commit_inner:
            that.Info.tipUrl = '/nccloud/arap/arappub/commit.do';
            that.Info.record = record;
            that.commitAndUncommit();
            break;
        case innerButton.Uncommit_inner:
            that.Info.tipUrl = '/nccloud/arap/arappub/uncommit.do';
            that.Info.record = record;
            that.commitAndUncommit();
            break;
        case innerButton.MadeBill_inner://制单
            let madeData = [{
                pk_bill: record.pk_paybill.value,
                billType: billType,
                tradeType: record.pk_tradetype.value,
            }]
            madeBill(that,props,madeData,'',props.getSearchParam('c'));
            break;
        default:
            break;
    }
};
