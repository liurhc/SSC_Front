import { createPage, ajax, base, toast ,cardCache, excelImportconfig } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;
import tableButtonClick from './tableButtonClick';
import { buttonVisible, getButtonsKey ,getInnerButtonkey} from '../../../../public/components/pubUtils/buttonvisible.js';
import { tableId, searchId, billType, tradeType,dataSource, searchKey } from '../constants';
import { headButton ,innerButton} from '../../../../public/components/pubUtils/buttonName.js';
import {modifierSearchMetas} from '../../../../public/components/pubUtils/arapListSearchRefFilter';
let { setDefData, getDefData } = cardCache;
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';
import setDefOrgBilldateSrchArea from '../../../../public/components/defOrgBilldateSrchArea.js';
import {OperationColumn} from '../../../../public/components/pubUtils/arapConstant';
export default function (props,callback) {
	const that = this;
	let pagecode = that.getPagecode();
	let appcode = props.getSearchParam('c') ? props.getSearchParam('c') : props.getUrlParam('c');
	let excelimportconfig = excelImportconfig(props, "arap", billType,true,"",{"appcode":appcode,"pagecode":"20080EBM_CARD"});
	
	let tradetype = getTradeType()
	
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode,//注册按钮的id
			reqDataQueryallbtns: {
				rqUrl: '/arap/arappub/queryallbtns.do',
				rqJson: `{\n  \"pagecode\": \"${pagecode}\",\n  \"appcode\": \"${appcode}\"\n,\n  \"billtype\": \"${billType}\"\n,\n  \"tradetype\": \"${tradetype}\"\n}`,
				rqCode: 'button'
			},
			reqDataQuerypage: {
				rqUrl: '/arap/arappub/querypage.do',
                rqJson: `{\n  \"pagecode\": \"${pagecode}\",\n  \"appcode\": \"${appcode}\"\n}`,
                rqCode: 'template'
			}
		},
		function (data) {
			if (data) {
				if(!data.template[tableId]){
					return;
				}
				let lineButton = [];
				if (data.button && data.button.button) {
					let button = data.button.button;
					let pullBillInfoVOAry = data.button.pullbillinfo
					that.Info.pullBillInfoVOAry = pullBillInfoVOAry
					lineButton = getInnerButtonkey(button);
					getButtonsKey(button, that.Info.allButtonsKey);//获取所有按钮
					props.button.setButtons(button);
					props.button.setPopContent(innerButton.Delete_inner, that.state.json['paybill-000006']);/*删除信息提示框*//* 国际化处理： 确定要删除吗？*/
					props.button.setUploadConfig("ImportData", excelimportconfig);
				}
				if (data.template) {
					//高级查询设置财务组织默认值
					setDefOrgBilldateSrchArea(props, searchId, data);
					let meta = data.template;
					meta = modifierMeta(that,props, meta,lineButton);
					props.meta.setMeta(meta,() => {
						if (data.context.paramMap != null){
							
							props.search.setSearchValByField(searchId, 'pk_tradetypeid', { display: data.context.paramMap.transtype_name, value: data.context.paramMap.pk_transtype });
						}
					});
					modifierSearchMetas(searchId, props, meta, billType,data.context.paramMap?data.context.paramMap.transtype:null, that);
				}

				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
					if(getContext(loginContextKeys.transtype)){
						if (that.refs.tradetypeBtn) {
							that.refs.tradetypeBtn.setVisible(false); 
						}
					}
				}
				if(callback){
					callback()
				}
				
				
			}
		}
	)
}


function modifierMeta(that,props, meta,lineButton) {
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'billno') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							let searchVal = props.search.getAllSearchData(searchId);
							if (searchVal) {
								setDefData(searchKey, dataSource, searchVal);
							}
							if (props.getUrlParam("scene") == 'linksce' || props.getUrlParam("scene") == 'fip') {
								props.pushTo('/card', {
									status: 'browse',
									id: record.pk_paybill.value,
									pagecode:'20080EBM_CARD_LINK',
									scene:'linksce'
								})
							}else{
								props.pushTo('/card', {
									status: 'browse',
									id: record.pk_paybill.value,
									pagecode:record.pk_tradetype.value,
								})
							}
						}}
					>
						{record.billno && record.billno.value}
					</a>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableId].items.push({
		label: that.state.json['paybill-000007'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: OperationColumn,
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let buttonAry = lineButton ? lineButton : [];
			let trueBtn = [];
			for (let i = 0; i < buttonAry.length; i++) {
				let flag = buttonVisible('browse', record, buttonAry[i]);
				if (flag) {
					trueBtn.push(buttonAry[i]);
				}
			}
			return props.button.createOprationButton(trueBtn, {
				area: "list_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(that,props, key, text, record, index)
			});
		}

	});
	return meta;
}

function getTradeType(){
	let cacheTradeType = getDefData('sessionTradeType', dataSource);
	if (cacheTradeType != null && cacheTradeType != undefined) {
		return cacheTradeType;
	} else {
		return tradeType;
	}
}
