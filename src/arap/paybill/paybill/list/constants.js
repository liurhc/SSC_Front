
/**
 * 查询区域
 */
export const searchId = 'query';

/**
 * 列表区域
 */
export const tableId = 'list';


/**
 * 单据类型
 */
export const billType = 'F3';

/**
 * 默认交易类型
 */
export const tradeType = 'D3';

/**
 * 默认模板节点标识
 */
export const nodekey = "list";
/**
 * 单页应用缓存
 */
export const dataSource  = 'fi.arap.paybill.20080EBM';

/**
 * 单页应用缓存主键名字(该名字为表的主键名字)
 */
export const pkname  = 'pk_paybill';

export const searchKey = 'pk_paybill_search';



