import { ajax, promptBox, toast } from 'nc-lightapp-front';
import { currentTypeAfterFormEvents } from '../../../../public/components/pubUtils/currentTypeAfterEvent';
import { autoAddLineKeys } from '../../../../public/components/pubUtils/billPubInfo';
import { checknoDisplayAfterEvent, checktypeAfterEvent } from '../../../../public/components/pubUtils/specialFieldAfterEvent.js';
import { formulamsgHint, renderData, headAfterEventRenderData, bodyAfterEventRenderData, errorDeal } from '../../../../public/components/afterEventPub/afterEventPubDeal';
import { getColvalues, getRowIds } from '../../../../public/components/pubUtils/billPubUtil';
import { moneyAndRateFields } from '../../../../public/components/pubUtils/specialFieldAfterEvent.js';

export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
	if (changedrows instanceof Array) {
		if (changedrows[0].newvalue.value == changedrows[0].oldvalue.value) {
			return;
		}
	}
	let pagecode = this.getPagecode();
	let index = 0;
	if (moduleId == this.formId) {
		index = 0;
	} else if (moduleId == this.tableId) {
		index = i;
	}
	//表头编辑后事件
	if (moduleId == this.formId) {
		let data = null
		switch (key) {
			case 'pk_org':
				if (value.value == null || value.value == '') {
					//清空财务组织
					if (this.props.getUrlParam('type') === 'transfer') {
						promptBox({
							color: 'warning',
							title: this.state.json['paybill-000018'] /* 国际化处理： 确认修改*/,
							content: this.state.json['paybill-000053'] /* 国际化处理： 来源于上游的单据不允许清空财务组织！*/,
							beSureBtnName: this.state.json['paybill-000004'] /* 国际化处理： 确定*/,
							noCancelBtn: true,
							beSureBtnClick: () => {
								this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
							},
							closeByClickBackDrop: false
						});
					} else {
						promptBox({
							color: 'warning',
							title: this.state.json['paybill-000018'] /* 国际化处理： 确认修改*/,
							content: this.state.json['paybill-000019'] /* 国际化处理： 确定​修改组织，这样会清空您录入的信息?*/,
							beSureBtnName: this.state.json['paybill-000004'] /* 国际化处理： 确定*/,
							cancelBtnName: this.state.json['paybill-000005'] /* 国际化处理： 取消*/,
							beSureBtnClick: () => {
								this.props.form.EmptyAllFormValue(this.formId);
								this.props.cardTable.setTableData(this.tableId, { rows: [] });
								this.initAdd(true);
							},
							cancelBtnClick: () => {
								this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
							},
							closeByClickBackDrop: false
						});
					}
				} else if (changedrows.value != null && changedrows.value != '') {
					//切换组织
					promptBox({
						color: 'warning',
						title: this.state.json['paybill-000018'] /* 国际化处理： 确认修改*/,
						content: this.state.json['paybill-000019'] /* 国际化处理： 确定​修改组织，这样会清空您录入的信息?*/,
						beSureBtnName: this.state.json['paybill-000004'] /* 国际化处理： 确定*/,
						cancelBtnName: this.state.json['paybill-000005'] /* 国际化处理： 取消*/,
						beSureBtnClick: () => {
							ajax({
								url: '/nccloud/arap/paybill/cardheadafteredit.do',
								data: {
									pageId: pagecode,
									event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
									uiState: this.props.getUrlParam('status')
								},
								async: false,
								success: (res) => {
									//渲染数据
									renderData(this, res);
									//编辑公式提示
									formulamsgHint(this, res);
								},
								error: (res) => {
									errorDeal(this, res, changedrows);
								}
							});
						},
						cancelBtnClick: () => {
							this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
						},
						closeByClickBackDrop: false
					});
				} else {
					data = {
						pageId: pagecode,
						event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
						uiState: this.props.getUrlParam('status')
					}
					headFieldAfterRequest.call(this, data, key, changedrows)
				}
				break;
			case 'supplier':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'payaccount', 'pk_currtype', 'supplier', 'buysellflag',
						'objtype', "direction", "pk_billtype", "top_billtype"].concat(moneyAndRateFields)),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'payaccount':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'payaccount', 'pk_currtype', 'supplier', 'buysellflag',
						'objtype', "direction", "pk_billtype"].concat(moneyAndRateFields)),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_deptid_v':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_deptid', 'pk_deptid_v', 'pk_currtype', 'supplier', 'taxtype', 'buysellflag', 'objtype', "direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_psndoc':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_psndoc', 'pk_deptid', 'pk_deptid_v', 'isrefused', 'prepay', 'isdiscount', 'objtype', 'direction', 'agentreceivelocal']),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_currtype':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ["pk_org","pk_group","pk_currtype","pk_billtype","billdate","rate","grouprate","globalrate",
					"buysellflag","taxprice","local_taxprice","taxrate","occupationmny","money_bal","local_money_bal",
					"globaldebit","globalnotax_de","globaltax_de","groupdebit","groupnotax_de",
					"grouptax_de","local_money_de","local_notax_de","local_tax_de","money_de","notax_de","quantity_de","direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows)
				//币种事件发送完要判断汇率
				currentTypeAfterFormEvents(this.formId, props, key);
				break;
			default:
				data = {
					pageId: pagecode,
					event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
		}
	}

	//表体编辑后事件
	if (moduleId == this.tableId) {
		//票据类型
		if (key == 'checktype') {
			checktypeAfterEvent(this.props, this.tableId, key, value, i);
		}
		//非元数据字段，票据号
		if (key == 'checkno_display') {
			checknoDisplayAfterEvent(this.props, this.tableId, key, value, i);
			key = 'checkno';
		}

		ajax({
			url: '/nccloud/arap/paybill/cardbodyafteredit.do',
			data: {
				rowindex: 0,
				editindex: index,
				pageId: pagecode,
				changedrows: changedrows,
				tableId:this.tableId,
				body: props.cardTable.getDataByIndex(this.tableId, index),
				formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
				uiState: this.props.getUrlParam('status')
			},
			async: false,
			success: (res) => {
				//渲染数据
				bodyAfterEventRenderData(this, res);
				if (this.props.getUrlParam('type') == 'transfer') {
					this.synTransferData();
				}
				//表体改变表头税率编辑性
				if (i == 0 && key == 'pk_currtype') {
					currentTypeAfterFormEvents(this.formId, props, 'pk_currtype');
				}
				//编辑公式提示
				formulamsgHint(this, res);
			},
			error: (res) => {
				let str = String(res);
				let content = str.substring(6, str.length);
				if (content.substring(1, 17) == 'convertException') {
					promptBox({
						color: 'warning',
						title: this.state.json['paybill-000000'] /* 国际化处理： 折算误差*/,
						content: content.substring(17, content.length),
						closeByClickBackDrop: false,
						beSureBtnClick: () => {
							afterTableEvent(this, props, i, pagecode, moduleId, key, changedrows, 'sure', index,value);
						},
						cancelBtnClick: () => {
							afterTableEvent(this, props, i, pagecode, moduleId, key, changedrows, 'cancel', index,value);
						}
					});
				} else {
					this.props.cardTable.setValByKeyAndRowId(this.tableId, i, key, changedrows);
					toast({ color: 'danger', content: content });
				}
			}
		});

		//自动增行
		autoAddline.call(this, moduleId, pagecode, key, i)
	}
}

function afterTableEvent(that, props, i, pagecode, moduleId, key, changedrows, isCalculateConvert, index,value) {

	ajax({
		url: '/nccloud/arap/paybill/cardbodyafteredit.do',
		data: {
			rowindex: 0,
			editindex: index,
			pageId: pagecode,
			changedrows: changedrows,
			tableId:that.tableId,
			body: props.cardTable.getDataByIndex(that.tableId, index),
			formEvent: props.createFormAfterEventData(pagecode, that.formId, that.tableId, key, value),
			uiState: that.props.getUrlParam('status'),
			isCalculateConvert: isCalculateConvert
		},
		async: false,
		success: (res) => {
			//渲染数据
			bodyAfterEventRenderData(that, res);
			if (that.props.getUrlParam('type') == 'transfer') {
				that.synTransferData();
			}
			//表体改变表头税率编辑性
			if (i == 0 && key == 'pk_currtype') {
				currentTypeAfterFormEvents(that.formId, props, 'pk_currtype');
			}
			//编辑公式提示
			formulamsgHint(that, res);
		}
	});
}

export function headFieldAfterRequest(requestData, key, changedrows) {
	ajax({
		url: '/nccloud/arap/paybill/cardheadafteredit.do',
		data: requestData,
		async: false,
		success: (res) => {
			//渲染数据
			try {//F3
				if(localStorage.getItem('yhlsgz')=="bianji"){
					var record=JSON.parse(localStorage.getItem('record'));
								var  heads=res.data.head[this.formId].rows["0"].values;
								var  tables=res.data.body[this.tableId].rows["0"].values
							//	tables.top_billid =record.values.m_pk_bankreceipt;//F3表体pk_upbill后端要求
								heads.def77 =record.values.m_pk_bankreceipt;//F3表体pk_upbill后端要求
								heads.payaccount=record.values.m_pk_bank;//银行账户
								heads.recaccount=record.values.otheraccount;//银行账户
								tables.payaccount=record.values.m_pk_bank;//银行账户
								tables.recaccount=record.values.otheraccount;//银行账户
								heads.def8=record.values.checkdate;//流水时间
								heads.supplier=record.values.supplier;//对方单位
								tables.supplier=record.values.supplier;//对方单位
								record.values.num&&record.values.num.value&&(heads.num=record.values.num);//挂账凭证号
                                heads.oppunitname =record.values.oppunitname ;//对方单位
								heads.accounting_status=record.values.accounting_status;//挂账状态
								heads.pk_balatype=record.values.pk_balatype1;    //结算方式
								tables.pk_balatype=record.values.pk_balatype1;  //结算方式
								record.values.pk_currtype&&record.values.pk_currtype.value&&(heads.pk_currtype=record.values.pk_currtype)&&(tables.pk_currtype=record.values.pk_currtype);//币种
								heads.rate=tables.rate=record.values.rate;//汇率
								tables.notax_de=heads.money_de=tables.money_de=heads.def10=record.values.m_creditamount;//原币贷发生额
								tables.local_notax_de=tables.local_money_de={value:parseFloat(record.values.m_creditamount.value)*parseFloat(record.values.rate.value)};
								tables.notax_de.scale=tables.local_notax_de.scale=tables.local_money_de.scale=heads.money_de.scale=tables.money_de.scale=heads.def10.scale="2"
								if(record.values.explanation&&record.values.explanation.value){
									record.values.explanation.display=record.values.explanation.value;
									tables.scomment=record.values.explanation;//摘要
								}
					localStorage.removeItem("yhlsgz");
				}
				
				
			}
			catch(err) {
			debugger
			console.log("不需要重置")
			}
			headAfterEventRenderData(this, res);
			//编辑公式提示
			formulamsgHint(this, res);

			if (key == 'pk_org') {
				let pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
				if (pk_org) {
					this.props.resMetaAfterPkorgEdit();
					this.state.buttonfalg = true;
				} else {
					this.state.buttonfalg = null;
				}
				this.toggleShow();
			}

			if (this.props.getUrlParam('type') == 'transfer') {
				this.synTransferData();
			}
		},
		error: (res) => {
			errorDeal(this, res, changedrows);
		}
	});
}

export function autoAddline(moduleId, pagecode, key, i) {
	//自动增行
	let allRowsNumber = this.props.cardTable.getNumberOfRows(this.tableId);
	if (moduleId == this.tableId && allRowsNumber == i + 1 && autoAddLineKeys.indexOf(key) != -1) {
		let data = this.props.createMasterChildData(pagecode, this.formId, this.tableId);
		//清空cardData的表体
		data.body[this.tableId].rows = [];
		ajax({
			url: '/nccloud/arap/paybillpub/addline.do',
			data: data,
			async: false,
			success: (res) => {
				if (res.data) {
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.addRow(this.tableId, allRowsNumber, res.data.body[this.tableId].rows[0].values, false);
					}
				}
			}
		});
	}
}