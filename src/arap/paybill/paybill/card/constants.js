
/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 表体区域
 */
export const tableId = 'bodys';

/**
 * 默认页面编码
 */
export const pageId = '20080EBM_CARD';

/**
 * 默认交易类型
 */
export const tradeType = 'D3';


/**
 * 单据类型
 */
export const billType = 'F3';

/**
 * 转单左侧区域
 */
export const leftarea = 'left';

/**
 * 模板节点标识
 */
export const nodekey = "card"

/**
 * 单页应用缓存
 */
export const dataSource  = 'fi.arap.paybill.20080EBM';

/**
 * 单页应用缓存主键名字(该名字为表的主键名字)
 */
export const pkname  = 'pk_paybill';

/**
 * 列表联查页面pageId
 */
export const linkPageId = '20080EBM_LIST_LINK';
