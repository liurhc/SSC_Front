//主子表卡片
import React, { Component } from 'react';
import { createPage, ajax, base, toast, high, print, cardCache, getMultiLang ,createPageIcon} from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, pageInfoClick, transferButtonClick } from './events';
import { setValue } from './events/transferButtonClick';
import {
	buttonVisible,
	getButtonsKey,
	dealTranferBtns,
	initCardBodyEditControl,
	onSelectedCardBodyEditControl,
	initCardBodybrowseControl
} from '../../../public/components/pubUtils/buttonvisible.js';
import { tableId, formId, billType, leftarea, nodekey, dataSource, pkname, linkPageId, tradeType } from './constants';
import CombinedExaminationModel from '../../../public/components/combinedExaminationModel'; //联查处理情况
import { bodyBeforeEvent } from '../../../public/components/pubUtils/arapTableRefFilter';
import { formBeforeEvent } from '../../../public/components/pubUtils/arapFormRefFilter';
import linkSourceCard from '../../../public/components/linkSourceCard.js';
import cmpLinkArapCard from '../../../public/components/cmpLinkArapCard.js';
import { calculateHeadMoney, delBlankLine } from '../../../public/components/pubUtils/billPubUtil.js';
import { updatePandC } from '../../../public/components/pubUtils/updatePandC';
let { Inspection, BillTrack, NCUploader, PrintOutput, Refer, ApproveDetail } = high;
let { NCFormControl, NCButton, NCBackBtn,NCSelect } = base;
const NCOption = NCSelect.NCOption;
let arrList=[]
let {
	setDefData,
	getDefData,
	addCache,
	getNextId,
	deleteCacheById,
	getCacheById,
	updateCache,
	getCurrentLastId
} = cardCache;
import { loginContext, getContext, loginContextKeys } from '../../../public/components/arapInitInfo/loginContext';
import TradeTypeButton from '../../../public/components/tradetype'; //交易类型按钮
import { modifyChecknoItemtype } from '../../../public/components/pubUtils/specialFieldAfterEvent.js';
import { dealCardData } from '../../../public/components/pubUtils/dealCardData';
import {
	objtypeEditable,
	dealCommisionPayField,
	F0ToF3FieldsEditable,
	applyToF3FieldsEditable,
	fctToArapFieldEditable
} from '../../../public/components/pubUtils/billFieldEditableUtil.js';
import '../../../public/less/tradetype.less';
import InvoiceUploader from 'sscrp/rppub/components/invoice-uploader';
import InvoiceLink from 'sscrp/rppub/components/invoice-link';
const { ExcelImport, ApprovalTrans } = high;

class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isCombinedExaminationModelShow: false, //联查处理情况模态框
			isTbbLinkshow: false, //控制联查执行预算的模态框
			showUploader: false, //附件管理
			target: null,
			showBillTrack: false, //单据追溯模态框显示的状态
			showApproveDetail: false, //审批详情
			buttonfalg: null, //卡片态点击肩部按钮和表体行按钮改变该值控制按钮状态
			compositedisplay: false, //指派信息弹框
			json: {},
			sscrpInvoiceData: {} ,//电子发票上传
			ifChange:false,
		};
		this.printData = {
			billtype: billType, //单据类型
			appcode: this.props.getSearchParam('c'), //功能节点编码，即模板编码
			nodekey: props.getSearchParam('p'), //模板节点标识  =交易类型
			oids: [ this.props.getUrlParam('id') ], // 功能节点的数据主键 oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			userjson: billType //单据类型
		};
		this.outputData = {
			billtype: billType, //单据类型
			funcode: this.props.getSearchParam('c'), //功能节点编码，即模板编码
			appcode: this.props.getSearchParam('c'), //功能节点编码，即模板编码
			nodekey: props.getSearchParam('p'), //模板节点标识  =交易类型
			oids: [ this.props.getUrlParam('id') ], // 功能节点的数据主键 oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			userjson: billType, //单据类型
			outputType: 'output'
		};
		this.data = null; //推单单据数据缓存
		this.formId = formId;
		this.tableId = tableId;
		this.billType = billType;
		this.leftarea = leftarea;
		this.transferIndex = 0; //转单时左侧选择的数据序号
		this.Info = {
			allButtonsKey: [], //保存所有头部按钮和肩部按钮
			combinedExaminationData: [], //联查处理情况模态框表格数据
			tbbLinkSourceData: null, //联查执行预算的数据
			selectedPKS: [], //导出数据的主键pk
			billCode: '', // 发票号
			tipContent: null, //提示框Content
			tipUrl: null, //提示框二次交互的url
			exType: null, //异常类型，现为三种（1,2,3）
			flag: false, //提交收回异常交互参数值，默认为false
			pk_bill: null, //提示框二次交互时后台传入前台主键
			ts: null,
			billCard: null, //保存提交后，返回的保存单据
			compositedata: null, //指派信息数据
			isModelSave: false //是否是整单保存，默认为false
		};
	}

	//关闭、刷新弹窗时
	componentWillMount() {
		if (this.props.getSearchParam('c') == '20082002' && this.props.getSearchParam('scene') != 'approvesce') {
			updatePandC(this);
		}
		let callback = (json) => {
			this.setState({ json: json }, () => {
				window.onbeforeunload = () => {
					let status = this.props.getUrlParam('status');
					if (status == 'edit' || status == 'add') {
						return '';
					}
				};
				initTemplate.call(this, this.props, this.initShow);
			});
		};
		getMultiLang({ moduleId: [ 'paybill', 'public' ], domainName: 'arap', currentLocale: 'simpchn', callback });
	}

	componentWillReceiveProps(nextProps) {}
	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		});
	};
	beforeUpload(billId, fullPath, file, fileList) {
		// 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表
		const isJPG = true;
		if (!isJPG) {
			alert(this.state.json['paybill-000023']); /* 国际化处理： 只支持png格式图片*/
		}
		const isLt2M = file.size / 1024 / 1024 < 10;
		if (!isLt2M) {
			alert(this.state.json['paybill-000024']); /* 国际化处理： 上传大小小于10M*/
		}
		return isJPG && isLt2M;
		// 备注： return false 不执行上传  return true 执行上传
	}
	componentDidMount() {
		if (this.props.getUrlParam('scene')) {
			let scene = this.props.getUrlParam('scene');
			setDefData('scene', dataSource, scene);
			let pagecode = this.getPagecode();
			setDefData('pagecode', dataSource, pagecode);
		}
	}

	getPagecode = () => {
		let pagecode = this.props.getUrlParam('pagecode');
		if (!pagecode) {
			pagecode = this.props.getSearchParam('p');
		}
		if (pagecode == '20080EBM_CARD_LINK' && !this.props.getUrlParam('scene')) {
			//浏览器的刷新可能导致场景丢失，这里自己塞一次
			this.props.ViewModel.setData('nccloud-router-params', { scene: 'linksce' });
		}
		return pagecode;
	};
	getExportPageCode = () => {
		let pagecode = "20080EBM_CARD";
		if(this.props.getUrlParam('scene')){
			pagecode = "20080EBM_CARD_LINK";
		}
		if (pagecode == '20080EBM_CARD_LINK' && !this.props.getUrlParam('scene')) {
			//浏览器的刷新可能导致场景丢失，这里自己塞一次
			this.props.ViewModel.setData('nccloud-router-params', { scene: 'linksce' });
		}
		return pagecode;

	}

	//页面初始化
	initShow = () => {
		if (this.props.getUrlParam('type') === 'transfer' && this.props.getUrlParam('status') != 'browse') {
			let transferIds = this.props.transferTable.getTransferTableSelectedId();
			this.getTransferValue(transferIds);
		} else if (this.props.getUrlParam('scene') && this.props.getUrlParam('scene') == 'fip') {
			linkSourceCard(this.props, linkPageId, 'pk_paybill', this.formId, this.tableId, pkname, dataSource, this);
		} else if (
			this.props.getUrlParam('scene') &&
			this.props.getUrlParam('scene') == 'linksce' &&
			this.props.getUrlParam('flag') == 'ftsLinkArap'
		) {
			cmpLinkArapCard(
				this.props,
				this.billType,
				'pk_paybill',
				this.formId,
				this.tableId,
				pkname,
				dataSource,
				this
			);
		} else if (this.props.getUrlParam('type') === 'copy') {
			//复制的情况
			ajax({
				url: '/nccloud/arap/arappub/copy.do',
				data: {
					pk_bill: this.props.getUrlParam('id'),
					pageId: this.getPagecode(),
					billType: this.billType,
					type: 1,
					tradeType: this.getPagecode()
				},
				success: (res) => {
					this.props.beforeUpdatePage(); //打开开关
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}
					this.props.resMetaAfterPkorgEdit();
					this.state.buttonfalg = true;
					this.props.cardTable.setStatus(this.tableId, 'edit');
					this.props.form.setFormStatus(this.formId, 'edit');
					this.props.updatePage(this.formId, this.tableId); //关闭开关
					this.toggleShow();
				},
				error: (res) => {
					this.props.form.EmptyAllFormValue(this.formId);
					this.props.cardTable.setTableData(this.tableId, { rows: [] });
					this.props.setUrlParam({ status: 'browse' });
					this.props.cardTable.setStatus(this.tableId, 'browse');
					this.props.form.setFormStatus(this.formId, 'browse');
					this.toggleShow();
					let str = String(res);
					let content = str.substring(6, str.length);
					toast({ color: 'danger', content: content });
				}
			});
		} else if (this.props.getUrlParam('status') == 'edit' || this.props.getUrlParam('status') == 'browse') {
			let pk_bill = this.props.getUrlParam('id');
			if (!pk_bill) {
				this.toggleShow();
				return;
			}
			ajax({
				url: '/nccloud/arap/paybill/querycard.do',
				data: { pk_bill: pk_bill },
				success: (res) => {
					this.props.beforeUpdatePage(); //打开开关
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}
					if (this.props.getUrlParam('status') == 'edit') {
						this.props.resMetaAfterPkorgEdit();
						this.state.buttonfalg = true;
						objtypeEditable(this.props, this.formId, this.tableId);
						this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
						//处理承付字段的编辑性
						dealCommisionPayField(this.props, this.formId, this.tableId);
					} else {
						this.state.buttonfalg = null;
					}
					if (!this.props.getUrlParam('scene')) {
						this.props.setUrlParam({
							pagecode: res.data.head[this.formId].rows[0].values.pk_tradetype.value
						});
					}
				
					let status = this.props.getUrlParam('status');
					this.props.cardTable.setStatus(this.tableId, status);
					this.props.form.setFormStatus(this.formId, status);
					this.props.updatePage(this.formId, this.tableId); //关闭开关
					updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
					this.toggleShow();
				}
			});
		} else if (this.props.getUrlParam('status') == 'add' && this.props.getUrlParam('src') == 'settlement') {
			//资金，关联结算信息
			if (!this.props.getUrlParam('pk_settle')) {
				toast({ color: 'danger', content: this.state.json['paybill-000041'] }); /* 国际化处理： 数据异常，请重新操作!*/
			}
			ajax({
				url: '/nccloud/arap/arappub/associatesettinfo.do',
				data: {
					pk_bill: this.props.getUrlParam('pk_settle'),
					pageId: this.getPagecode(),
					billType: this.billType
				},
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							this.props.form.EmptyAllFormValue(this.formId);
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}
					this.props.beforeUpdatePage(); //打开开关
					this.state.buttonfalg = true;
					this.props.cardTable.setStatus(this.tableId, 'edit');
					this.props.form.setFormStatus(this.formId, 'edit');
					dealCommisionPayField(this.props, this.formId, this.tableId);
					this.props.updatePage(this.formId, this.tableId); //关闭开关
					this.toggleShow();
				}
			});
		} else if (this.props.getUrlParam('status') == 'add' && this.props.getUrlParam('srcbilltype')) {
			//推单新增时在inittemplate里处理
			this.props.beforeUpdatePage(); //打开开关
			if (this.data.head) {
				this.props.form.setAllFormValue({ [this.formId]: this.data.head[this.formId] });
			}
			if (this.data.body) {
				this.props.cardTable.setTableData(this.tableId, this.data.body[this.tableId]);
			}
			this.state.buttonfalg = true;
			this.props.cardTable.setStatus(this.tableId, 'edit');
			this.props.form.setFormStatus(this.formId, 'edit');
			fctToArapFieldEditable(this.props, this.formId, this.tableId);
			this.props.updatePage(this.formId, this.tableId); //关闭开关
			this.toggleShow();
		} else if (this.props.getUrlParam('status') == 'add') {
			//状态为add时请求显示默认值
			this.initAdd();
		} else {
			this.toggleShow();
		}
	};

	getTransferValue = (ids) => {
		if (ids) {
			let data = {
				data: ids,
				pageId: this.getPagecode(),
				destTradetype: this.getPagecode(),
				srcBilltype: this.props.getUrlParam('srcBilltype')
			};
			ajax({
				url: '/nccloud/arap/arappub/transfer.do',
				data: data,
				success: (res) => {
					if (res && res.data) {
						this.props.transferTable.setTransferListValue(this.leftarea, res.data);
						if (this.props.getUrlParam('srcBilltype') == 'F0') {
							//付款单拉红字应收单部分字段不可编辑
							F0ToF3FieldsEditable(this.props, this.formId, this.tableId);
						} else if (this.props.getUrlParam('srcBilltype') == '36D1') {
							// 付款单拉付款申请特殊字段编辑性
							applyToF3FieldsEditable(this.props, this.formId, this.tableId);
						}
					}
					this.toggleShow();
				}
			});
		} else {
			this.props.transferTable.setTransferListValue(this.leftarea, []);
		}
	};

	//同步单据信息到转单控件
	synTransferData = () => {
		//重取界面现有数据赋值到转单中
		let amount = this.props.transferTable.getTransformFormAmount(this.leftarea);
		if (amount != 1) {
			let cardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
			this.props.transferTable.setTransferListValueByIndex(this.leftarea, cardData, this.transferIndex);
		}
	};

	//单据新增
	initAdd = (isClearPKorg) => {
		if (!isClearPKorg) {
			//isClearPKorg如果未未定义等，就取值为false
			isClearPKorg = false;
		}
		ajax({
			url: '/nccloud/arap/paybill/defvalue.do',
			data: {
				isClearPKorg: isClearPKorg,
				appcode: this.props.getSearchParam('c'),
				pageId: this.getPagecode(),
				tradeType: this.getPagecode()
			},
			success: (res) => {
				if (res.data) {
					this.props.beforeUpdatePage(); //打开开关
					this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
					let pk_org = res.data.head[this.formId].rows[0].values.pk_org.value;
					if (pk_org) {
						this.props.resMetaAfterPkorgEdit();
						this.props.form.setFormItemsDisabled(this.formId, { pk_org: false });
						this.state.buttonfalg = true;
					} else {
						this.state.buttonfalg = null;
						if (
							!this.props.getUrlParam('type') &&
							!this.props.getUrlParam('srcbilltype') &&
							!this.props.getUrlParam('src')
						) {
							this.props.initMetaByPkorg();
						}
					}
                    //银行流水挂账-跳转F3
					if(window.location.hash.includes("ifturnTo=")){
						var record=JSON.parse(localStorage.getItem('record'))
						localStorage.setItem("yhlsgz", "bianji");
						afterEvent.call(this, this.props,"head","pk_org",record.values.m_pk_org,{},"",res.data.head[this.formId].rows["0"].values)
					}
					this.props.form.setFormStatus(this.formId, 'edit');
					this.props.cardTable.setStatus(this.tableId, 'edit');
					this.props.updatePage(this.formId, this.tableId); //关闭开关
				}
				this.toggleShow();
			},
			error: (res) => {//主要控制列表进卡片抛出错误
				let str = String(res);
				let content = str.substring(6, str.length);
				toast({ color: 'danger', content: content });
				let status = this.props.getUrlParam('status');
				if(status !='browse'){
					this.props.setUrlParam({ status: 'browse' });
					this.props.form.setFormStatus(this.formId, 'browse');
					this.props.cardTable.setStatus(this.tableId, 'browse');
					this.toggleShow();
				}
			}
		});
	};

	//切换页面状态
	toggleShow = (headData) => {
		let status = this.props.getUrlParam('status');
		if (!status) {
			status = 'browse';
		}
		let cardhead = {};
		if (headData) {
			cardhead = headData.rows[0].values;
		} else {
			let head = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId).head[
				this.formId
			];
			if (head) {
				cardhead = head.rows[0].values;
			}
		}
		if (status != 'browse') {
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: false });
			if (this.refs.tradetypeBtn && !getContext(loginContextKeys.transtype)) {
				this.refs.tradetypeBtn.setVisible(false); //设置交易类型是否显示
			}
		} else {
			if (!(cardhead.pk_tradetype ? cardhead.pk_tradetype.value : null)) {
				this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
			} else {
				this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);
			}
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: true });
			if (this.refs.tradetypeBtn && !getContext(loginContextKeys.transtype)) {
				this.refs.tradetypeBtn.setVisible(true); //设置交易类型是否显示
			}
		}
		let trueBtn = []; //可见的按钮
		let falseBtn = []; //不可见的按钮
		for (let i = 0; i < this.Info.allButtonsKey.length; i++) {
			let flag = buttonVisible(status, cardhead, this.Info.allButtonsKey[i], 'card', this);
			if (flag) {
				trueBtn.push(this.Info.allButtonsKey[i]);
			} else {
				falseBtn.push(this.Info.allButtonsKey[i]);
			}
		}
		//初始化肩部按钮信息增行等按钮的控制
		if (status == 'browse') {
			initCardBodybrowseControl(this);
		} else {
			initCardBodyEditControl(this.props, cardhead.pk_org ? cardhead.pk_org.value : null);
		}

		//处理转单不可见按钮
		dealTranferBtns.call(this, falseBtn);
		this.props.button.setButtonVisible(trueBtn, true);
		this.props.button.setButtonVisible(falseBtn, false);
		//联查场景，默认场景，浏览态存在返回按钮
		if(this.props.getUrlParam('scene')&&this.props.getUrlParam('scene') != 'linksce' &&
			this.props.getUrlParam('scene') != 'fip' ){
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: false });
		}
	};

	//卡片表体点击行事件
	onSelected = () => {
		onSelectedCardBodyEditControl(this);
	};

	//删除单据
	delConfirm = (extype, flag) => {
		ajax({
			url: '/nccloud/arap/arappub/delete.do',
			data: [
				{
					pk_bill: this.props.getUrlParam('id'),
					ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
					billType: this.billType,
					extype: extype,
					flag: flag
				}
			],
			success: (res) => {
				//删除单据后，卡片界面显示该单据的下一个单据
				//如果是最后一个单据，删除后返回list界面
				if (res.success) {
					if (res.data.exType == '1') {
						this.Info.tipContent = res.data.message;
						this.Info.exType = '1';
						this.Info.flag = true;
						this.props.modal.show('deleteCheck');
						return;
					} else {
						toast({ color: 'success', content: this.state.json['paybill-000009'] }); /* 国际化处理： 删除成功*/
						let id = this.props.getUrlParam('id');
						deleteCacheById(pkname, id, dataSource);
						let nextId = getNextId(id, dataSource);
						if (nextId) {
							if (this.props.getUrlParam('type') === 'transfer') {
								this.props.setUrlParam({ status: 'browse' });
							}
							this.props.setUrlParam({ id: nextId });
							this.initShow();
						} else {
							this.props.setUrlParam({ id: null });
							this.props.form.EmptyAllFormValue(this.formId);
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
							this.toggleShow();
						}
						this.clearExType();
					}
				}
			}
		});
	};

	cancel = () => {
		//取消按钮
		if (this.props.getUrlParam('type') && this.props.getUrlParam('type') != 'transfer') {
			//如果type存在就删除
			this.props.delUrlParam('type');
		}
		let pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org');
		if (!pk_org || !pk_org.value || pk_org.value == '') {
			this.props.resMetaAfterPkorgEdit();
		}
		this.props.beforeUpdatePage(); //打开开关
		this.state.buttonfalg = null;
		if (this.props.getUrlParam('status') === 'edit') {
			this.props.setUrlParam({ status: 'browse' });
			let id = this.props.getUrlParam('id');
			let cardData = getCacheById(id, dataSource);
			if (cardData) {
				this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
				this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
				this.props.cardTable.setStatus(this.tableId, 'browse');
				this.props.form.setFormStatus(this.formId, 'browse');
				this.props.updatePage(this.formId, this.tableId); //关闭开关
				this.toggleShow();
			} else {
				this.initShow();
			}
		} else if (this.props.getUrlParam('status') === 'add') {
			let id = this.props.getUrlParam('id');
			if (!id) {
				id = getCurrentLastId(dataSource);
			}
			if (id) {
				this.props.setUrlParam({ status: 'browse', id: id });
				let cardData = getCacheById(id, dataSource);
				if (cardData) {
					this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
					this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
					let pagecode = cardData.head[this.formId].rows[0].values.pk_tradetype.value;
					if (this.getPagecode() != pagecode && !this.props.getUrlParam('scene')) {
						this.props.setUrlParam({ pagecode: pagecode });
						initTemplate.call(this, this.props);
					}
					this.props.cardTable.setStatus(this.tableId, 'browse');
					this.props.form.setFormStatus(this.formId, 'browse');
					this.props.updatePage(this.formId, this.tableId); //关闭开关
					this.toggleShow();
				} else {
					this.initShow();
				}
			} else {
				this.props.form.EmptyAllFormValue(this.formId);
				this.props.cardTable.setTableData(this.tableId, { rows: [] });
				this.props.setUrlParam({ status: 'browse' });
				this.props.cardTable.setStatus(this.tableId, 'browse');
				this.props.form.setFormStatus(this.formId, 'browse');
				this.props.updatePage(this.formId, this.tableId); //关闭开关
				this.toggleShow();
			}
		}
	};

	//保存单据
	saveBill = (url, extype, flag, modelIndex) => {
		if (url != '/nccloud/arap/arappub/tempsave.do') {
			//暂存不进行校验
			//删除空白行
			let checkCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
			delBlankLine(this, this.tableId, this.billType, checkCardData, modelIndex);
			if (!this.props.form.isCheckNow(this.formId)) {
				//表单验证
				return;
			}
			if (!this.props.cardTable.checkTableRequired(this.tableId)) {
				//表格验证
				return;
			}
		}
		let cardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
		let newCardData = dealCardData(this, cardData); //去掉空值，减少压缩时间
		let datas = {
			cardData: newCardData,
			uiState: this.props.getUrlParam('status'),
			extype: extype,
			flag: flag,
			pk_settle: this.props.getUrlParam('pk_settle')
		};
		let callback = () => {
			ajax({
				url: url,
				data: datas,
				success: (res) => {
					let pk_paybill = null;
					let pk_tradetype = null;
					if (res.success) {
						if (res.data.exType == '1') {
							this.Info.tipContent = res.data.message;
							this.Info.pk_bill = res.data.pk_bill;
							this.Info.ts = res.data.ts;
							this.Info.billCard = res.data.billCard;
							this.Info.tipUrl = res.data.pk_bill == null ? url : '/nccloud/arap/arappub/commit.do';
							this.Info.exType = '1';
							this.Info.flag = true;
							if (res.data.pk_bill == null) {
								this.props.modal.show('saveCheck');
							} else {
								this.commitAndUncommit();
							}
							// this.props.modal.show(res.data.pk_bill == null ? 'saveCheck' : 'commitAndUncommit');
							return;
						} else if (res.data.exType == '2') {
							this.Info.tipContent = res.data.message;
							this.Info.pk_bill = res.data.pk_bill;
							this.Info.ts = res.data.ts;
							this.Info.billCard = res.data.billCard;
							this.Info.tipUrl = res.data.pk_bill == null ? url : '/nccloud/arap/arappub/commit.do';
							this.Info.exType = '2';
							this.Info.flag = true;
							if (res.data.pk_bill == null) {
								this.props.modal.show('saveCheck');
							} else {
								this.commitAndUncommit();
							}
							// this.props.modal.show(res.data.pk_bill == null ? 'saveCheck' : 'commitAndUncommit');
							return;
						} else if (res.data.exType == '3') {
							//普通异常要捕获到并抛出错误信息，同时要将保存后的单据进行回写
							let content = res.data.message;
							toast({ color: 'danger', content: JSON.stringify(content) });
						} else if (
							res.data.assignInfo &&
							res.data.assignInfo.workflow &&
							(res.data.assignInfo.workflow == 'approveflow' ||
								res.data.assignInfo.workflow == 'workflow')
						) {
							this.Info.compositedata = res.data.assignInfo;
							this.Info.pk_bill = res.data.pk_bill;
							this.Info.ts = res.data.ts;
							this.Info.exType = res.data.exType;
							this.Info.billCard = res.data.billCard;
							this.Info.tipUrl = '/nccloud/arap/arappub/commit.do';
							this.setState({ compositedisplay: true });
							return;
						}
						if (res.data) {
							if (this.props.getUrlParam('type') == 'transfer') {
								setValue.call(this, this.props, res);
							} else {
								if (!res.data.message) {
									toast({
										color: 'success',
										content: this.state.json['paybill-000010']
									}); /* 国际化处理： 保存成功*/
								}
								this.props.beforeUpdatePage(); //打开开关
								if (res.data.head && res.data.head[this.formId]) {
									this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
									pk_paybill = res.data.head[this.formId].rows[0].values.pk_paybill.value;
									pk_tradetype = res.data.head[this.formId].rows[0].values.pk_tradetype.value;
								}
								if (res.data.body && res.data.body[this.tableId]) {
									this.props.cardTable.updateDataByRowId(this.tableId, res.data.body[this.tableId]);
								}
								this.state.buttonfalg = null;
								this.props.cardTable.setStatus(this.tableId, 'browse');
								this.props.form.setFormStatus(this.formId, 'browse');
								this.props.updatePage(this.formId, this.tableId); //关闭开关
								let newCardData = this.props.createMasterChildData(
									this.getPagecode(),
									this.formId,
									this.tableId
								);
								if (this.props.getUrlParam('status') == 'add') {
									addCache(pk_paybill, newCardData, this.formId, dataSource); //新增
								} else {
									updateCache(pkname, pk_paybill, newCardData, this.formId, dataSource); //修改之后更新缓存
								}
								if (this.props.getUrlParam('type')) {
									this.props.delUrlParam('type');
								}
								if (this.props.getUrlParam('scene')) {
									this.props.setUrlParam({ status: 'browse', id: pk_paybill });
								} else {
									this.props.setUrlParam({
										status: 'browse',
										id: pk_paybill,
										pagecode: pk_tradetype
									});
								}
							}
						}
					}
					if (this.Info.isModelSave) {
						this.Info.isModelSave = false;
						this.props.cardTable.closeModel(this.tableId);
					}
					if (this.props.getUrlParam('type') != 'transfer') {
						this.toggleShow();
					}
					this.clearExType();
				}
			});
		};

		this.props.validateToSave(datas.cardData, callback, { table1: 'cardTable' }, 'card');
	};

	//提交和收回弹框点击取消
	clearExType = () => {
		this.Info.tipContent = '';
		this.Info.pk_bill = null;
		this.Info.ts = null;
		this.Info.billCard = null;
		this.Info.tipUrl = null;
		this.Info.exType = null;
		this.Info.flag = false;
		this.Info.compositedata = null;
	};

	cancelClick = () => {
		if (this.Info.pk_bill != null) {
			if (this.props.getUrlParam('type') && this.props.getUrlParam('type') != 'transfer') {
				this.props.delUrlParam('type');
			}
			this.props.beforeUpdatePage(); //打开开关
			this.state.buttonfalg = null;
			if (this.Info.billCard.head) {
				this.props.form.setAllFormValue({ [this.formId]: this.Info.billCard.head[this.formId] });
			}
			if (this.Info.billCard.body) {
				this.props.cardTable.updateDataByRowId(this.tableId, this.Info.billCard.body[this.tableId]);
			}
			if (this.props.getUrlParam('scene')) {
				this.props.setUrlParam({
					status: 'browse',
					id: this.Info.pk_bill == null ? this.props.getUrlParam('id') : this.Info.pk_bill
				});
			} else {
				this.props.setUrlParam({
					status: 'browse',
					id: this.Info.pk_bill == null ? this.props.getUrlParam('id') : this.Info.pk_bill,
					pagecode: this.Info.billCard.head[this.formId].rows[0].values.pk_tradetype.value
				});
			}
			this.props.cardTable.setStatus(this.tableId, 'browse');
			this.props.form.setFormStatus(this.formId, 'browse');
			this.props.updatePage(this.formId, this.tableId); //关闭开关
			let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
			addCache(pkname, newCardData, this.formId, dataSource); //新增
			this.toggleShow();
		}
		if (this.state.compositedisplay) {
			this.setState({ compositedisplay: false });
		}
		this.clearExType();
	};

	//提交and收回公共调用函数
	commitAndUncommit = () => {
		let tipUrl = this.Info.tipUrl;
		let extype = this.Info.exType;
		let flag = this.Info.flag;
		ajax({
			url: tipUrl,
			data: {
				pk_bill: this.Info.pk_bill == null ? this.props.getUrlParam('id') : this.Info.pk_bill,
				ts: this.Info.ts == null ? this.props.form.getFormItemsValue(this.formId, 'ts').value : this.Info.ts,
				pageId: this.getPagecode(),
				billType: this.billType,
				type: 2,
				extype: extype,
				flag: flag,
				assignObj: this.Info.compositedata
			},
			success: (res) => {
				if (res.data.exType == '1') {
					let content = res.data.message;
					this.Info.tipContent = content;
					this.Info.tipUrl = tipUrl;
					this.Info.exType = '1';
					this.Info.flag = true;
					this.props.modal.show('commitAndUncommit');
					return;
				} else if (
					res.data.workflow &&
					(res.data.workflow == 'approveflow' || res.data.workflow == 'workflow')
				) {
					this.Info.compositedata = res.data;
					this.Info.tipUrl = tipUrl;
					this.setState({ compositedisplay: true });
					return;
				}
				if (res.success) {
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
					}
					let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
					if (this.Info.pk_bill == null) {
						updateCache(pkname, this.props.getUrlParam('id'), newCardData, this.formId, dataSource);
					} else {
						if (this.props.getUrlParam('type') && this.props.getUrlParam('type') != 'transfer') {
							this.props.delUrlParam('type');
						}
						this.state.buttonfalg = null;
						addCache(pkname, newCardData, this.formId, dataSource); //新增
					}
					if (this.props.getUrlParam('status') != 'browse') {
						this.props.cardTable.setStatus(this.tableId, 'browse');
						this.props.form.setFormStatus(this.formId, 'browse');
					}
					this.props.setUrlParam({
						status: 'browse',
						id: this.Info.pk_bill == null ? this.props.getUrlParam('id') : this.Info.pk_bill
					});
					if (this.state.compositedisplay) {
						this.setState({ compositedisplay: false });
					}
					this.clearExType();
					this.toggleShow();
					toast({ color: 'success', content: this.state.json['paybill-000026'] }); /* 国际化处理： 操作成功*/
				}
			}
		});
	};

	//单据删除模态框确定按钮点击事件
	deleteBillSureBtnClick = () => {
		let exType = this.Info.exType;
		let flag = this.Info.flag;
		this.delConfirm(exType, flag);
	};

	//挂起和取消挂起操作
	pause = (url) => {
		let selectedData = this.props.cardTable.getCheckedRows(this.tableId);
		if (selectedData.length == 0) {
			toast({ color: 'warning', content: this.state.json['paybill-000033'] }); /* 国际化处理： 请选择表体行!*/
			return;
		}
		let pauseObj = [];
		selectedData.forEach((val) => {
			pauseObj.push(val.data.values.pk_payitem.value);
		});
		let data = {
			pk_items: pauseObj,
			pk_bill: this.props.getUrlParam('id'),
			ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
			pageId: this.getPagecode(),
			billType: this.billType
		};
		ajax({
			url: url,
			data: data,
			success: (res) => {
				if (res.data.message) {
					toast({
						duration: 'infinity',
						color: res.data.PopupWindowStyle,
						content: res.data.message
					});
				} else {
					toast({ color: 'success', content: this.state.json['paybill-000026'] }); /* 国际化处理： 操作成功*/
				}
				if (res.data.billCard) {
					if (res.data.billCard.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.billCard.head[this.formId] });
					}
					if (res.data.billCard.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.billCard.body[this.tableId]);
					}
					let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
					updateCache(pkname, this.props.getUrlParam('id'), newCardData, this.formId, dataSource);
				}
				this.onSelected();
			}
		});
	};

	//获取列表肩部信息
	getTableHead = (buttons) => {
		return (
			<span>
				{this.props.button.createButtonApp({
					area: 'card_body',
					buttonLimit: 3,
					onButtonClick: buttonClick.bind(this),
					popContainer: document.querySelector('.header-button-area')
				})}
			</span>
		);
	};

	//联查处理情况按钮
	handleCombined = () => {
		this.setState({
			isCombinedExaminationModelShow: !this.state.isCombinedExaminationModelShow
		});
	};

	//关闭联查执行预算的模态框
	tbbLinkcancel() {
		this.setState({
			isTbbLinkshow: false
		});
	}

	//审批详情模态框控制
	closeApprove = () => {
		this.setState({
			showApproveDetail: false
		});
	};

	//打印
	onPrint = () => {
		this.printData.oids = [ this.props.getUrlParam('id') ];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/arap/arappub/print.do', //后台服务url
			this.printData
			// false 去掉后支持弹出选择打印模板
		);
	};

	//打印输出
	printOutput = () => {
		this.outputData.oids = [ this.props.getUrlParam('id') ];
		this.outputData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		this.refs.printOutput.open();
	};
	//正式打印
	officalPrintOutput = () => {
		this.printData.oids = [ this.props.getUrlParam('id') ];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		ajax({
			url: '/nccloud/arap/arappub/officialPrint.do',
			data: this.printData,
			async: false,
			success: (res) => {
				if (res.success) {
					print(
						'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
						'/nccloud/arap/arappub/print.do', //后台服务url
						this.printData,
						false
					);
				}
			}
		});
	};
	//取消正式打印
	cancelPrintOutput = () => {
		this.printData.oids = [ this.props.getUrlParam('id') ];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		ajax({
			url: '/nccloud/arap/arappub/cancelPrint.do',
			data: this.printData,
			async: false,
			success: (res) => {
				if (res.success) {
					//打开协同单据节点
					toast({ color: 'success', content: res.data });
				}
			}
		});
	};

	//输出成功回调函数
	onSubmit() {}

	//返回列表
	backList = () => {
		if (this.props.getUrlParam('scene') == 'linksce' || this.props.getUrlParam('scene') == 'fip') {
			this.props.pushTo('/list', {
				pagecode: '20080EBM_LIST_LINK',
				scene: 'linksce'
			});
		} else {
			this.props.pushTo('/list', {
				pagecode: '20080EBM_LIST'
			});
		}
	};

	//返回转单页面
	backTransfer = () => {
		let dataSource = this.props.getUrlParam('dataSource');
		let src_appcode = getDefData('src_appcode', dataSource);
		let src_pagecode = getDefData('src_pagecode', dataSource);
		let url = '/' + this.props.getUrlParam('srcBilltype');
		this.props.pushTo(url, {
			src_appcode: src_appcode,
			dest_billtype: this.billType,
			dest_tradetype: this.getPagecode()
		});
	};

	//保存事件模态框确定按钮点击事件
	saveSureBtnClick = () => {
		let tipUrl = this.Info.tipUrl;
		let exType = this.Info.exType;
		let flag = this.Info.flag;
		this.saveBill(tipUrl, exType, flag);
	};
	//整单保存事件
	modelSaveClick = (a, modelIndex) => {
		this.Info.isModelSave = true;
		let saveUrl = '/nccloud/arap/arappub/save.do';
		this.saveBill(saveUrl, null, null, modelIndex);
	};

	//侧拉删行
	modelDelRow = (props, moduleId) => {
		calculateHeadMoney(this);
		let allVisibleRows = this.props.cardTable.getVisibleRows(this.tableId);
		if (!allVisibleRows || allVisibleRows.length == 0) {
			this.props.cardTable.closeModel(this.tableId);
		}
	};

	//侧拉增行
	modelAddRow = (props, moduleId, modelIndex) => {
		if (this.props.form.getFormItemsValue(this.formId, 'pk_org').value != null) {
			let allRowsNumber = this.props.cardTable.getNumberOfRows(this.tableId);
			ajax({
				url: '/nccloud/arap/paybillpub/addline.do',
				data: this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId),
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.updateDataByIndexs(this.tableId, [
								{
									index: allRowsNumber - 1,
									data: {
										status: 2,
										values: res.data.body[this.tableId].rows[0].values
									}
								}
							]);
						}
					}
				}
			});
		} else {
			toast({ color: 'warning', content: this.state.json['paybill-000027'] }); /* 国际化处理： 请先选择财务组织*/
		}
	};

	switchTemplate = () => {
		//initTemplate.call(this, this.props);
		let pagecode = this.getPagecode();
		let appcode = this.props.getSearchParam('c') ? this.props.getSearchParam('c') : this.props.getUrlParam('c');
		let tradetype = getDefData('sessionTradeType', dataSource);
		if (!tradetype) {
			tradetype = tradeType;
		}
		ajax({
			url: '/nccloud/arap/arappub/queryallbtns.do',
			data: {
				appcode: appcode,
				pagecode: pagecode,
				tradetype: tradetype,
				billtype: billType
			},
			success: (res) => {
				let data = res.data;
				if (data.button) {
					let button = data.button;
					this.Info.pullBillInfoVOAry = data.pullbillinfo;
					getButtonsKey(button, this.Info.allButtonsKey); //获取所有按钮
					this.props.button.setButtons(button);
				}
			}
		});
	};
	 gotoLink=(link)=>{
		    if(link){
			        window.open(link)
		    };
	}
	render() {
		let { cardTable, form, button, modal, cardPagination, transferTable } = this.props;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createCardTable } = cardTable;
		const { createCardPagination } = cardPagination;
		let { createButton } = button;
		let { createModal } = modal;
		let transfer = this.props.getUrlParam('type') == 'transfer';
		const { createTransferList } = transferTable;
		let { showUploader, target } = this.state;
		let self=this;
		if (transfer) {
			return (
				<div id="transferCard" className="nc-bill-transferList">
					<div className="nc-bill-header-area">
						<div>
							<NCBackBtn onClick={this.backTransfer} />
						</div>
						<div className="header-title-search-area">
							<h2 className="title-search-detail">{this.state.json['paybill-000046']}</h2>
							{/* 国际化处理： 付款单*/}
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'card_head',
								buttonLimit: 3,
								onButtonClick: transferButtonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
							{getDefData('scene', dataSource) == 'bz' ? null : (
								<NCButton colors="default" onClick={this.backList}>
									{this.state.json['paybill-000047']}
								</NCButton>
							)}
							{/* 国际化处理： 取消转单*/}
						</div>
					</div>
					<div className="nc-bill-transferList-content">
						{createTransferList({
							headcode: this.formId,
							bodycode: this.tableId,
							transferListId: this.leftarea, //转单列表id
							onTransferItemSelected: (record, status, index) => {
								//转单缩略图被选中时的钩子函数
								this.transferIndex = index;
								let isEdit = status ? 'browse' : 'edit';
								if (isEdit == 'browse') {
									let id = record.head[this.formId].rows[0].values.pk_paybill.value;
									record = id == null ? record : getCacheById(id, dataSource);
									this.props.setUrlParam({ status: 'browse' });
									this.props.setUrlParam({ id: id });
								} else {
									this.props.setUrlParam({ status: 'add' });
									this.props.delUrlParam('id');
									this.state.buttonfalg = true;
								}
								this.toggleShow(record.head[this.formId]);
								this.props.form.setFormStatus(this.formId, isEdit);
								this.props.cardTable.setStatus(this.tableId, isEdit);
								this.props.form.setAllFormValue({ [this.formId]: record.head[this.formId] });
								this.props.cardTable.setTableData(this.tableId, record.body[this.tableId]);
							}
						})}
						<div className="transferList-content-right nc-bill-card">
							<div className="nc-bill-form-area">
								{createForm(this.formId, {
									onAfterEvent: afterEvent.bind(this),
									onBeforeEvent: formBeforeEvent.bind(this)
								})}
							</div>
							<div className="nc-bill-table-area">
								{createCardTable(this.tableId, {
									tableHead: this.getTableHead.bind(this, buttons),
									modelSave: this.modelSaveClick.bind(this),
									onAfterEvent: afterEvent.bind(this),
									onBeforeEvent: bodyBeforeEvent.bind(this),
									modelAddRow: this.modelAddRow.bind(this),
									modelDelRow: this.modelDelRow.bind(this),
									onSelected: this.onSelected.bind(this),
									onSelectedAll: this.onSelected.bind(this),
									showCheck: true,
									showIndex: true,
									hideAdd: true,
									isAddRow: false
								})}
							</div>
							{/* {联查处理情况} */}
							<CombinedExaminationModel
								show={this.state.isCombinedExaminationModelShow}
								combinedExaminationData={this.Info.combinedExaminationData}
								pk_tradetypeid={
									this.props.form.getFormItemsValue(this.formId, 'pk_tradetypeid') ? (
										this.props.form.getFormItemsValue(this.formId, 'pk_tradetypeid').display
									) : null
								}
								billno={
									this.props.form.getFormItemsValue(this.formId, 'billno') ? (
										this.props.form.getFormItemsValue(this.formId, 'billno').value
									) : null
								}
								handleModel={this.handleCombined.bind(this)}
							/>

							{/* {附件管理} */}
							{showUploader && (
								<NCUploader
									billId={
										this.props.form.getFormItemsValue(this.formId, 'pk_paybill') ? (
											this.props.form.getFormItemsValue(this.formId, 'pk_paybill').value
										) : null
									}
									billNo={
										this.props.form.getFormItemsValue(this.formId, 'billno') ? (
											this.props.form.getFormItemsValue(this.formId, 'billno').value
										) : null
									}
									target={target}
									placement={'bottom'}
									disableModify={this.props.form.getFormItemsValue(this.formId, 'approvestatus')&&this.props.form.getFormItemsValue(this.formId, 'approvestatus').value!="-1"}
									beforeUpload={this.beforeUpload}
									onHide={this.onHideUploader}
									close={() => this.closeModal()}
									customize={{
										pk_billType: this.props.form.getFormItemsValue(this.formId, 'pk_billtype')
											? this.props.form.getFormItemsValue(this.formId, 'pk_billtype').value
											: null
									}}
									customInterface={{
										queryLeftTree: '/nccloud/arap/arappub/arapFileLeftTreeQuery.do',
										queryAttachments: '/nccloud/arap/arappub/transformBillEnclosureQuery.do'
									}}
								/>
							)}
							{/* 单据追溯组件 */}
							<BillTrack
								show={this.state.showBillTrack}
								close={() => {
									this.setState({ showBillTrack: false });
								}}
								pk={this.props.getUrlParam('id')} //单据id
								type={
									this.props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? (
										this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
									) : null
								} //单据类型
							/>
							{/* {打印输出} */}
							<PrintOutput
								ref="printOutput"
								url="/nccloud/arap/arappub/outPut.do"
								data={this.outputData}
								callback={this.onSubmit}
							/>
							{/* {审批详情模态框} */}
							<ApproveDetail
								show={this.state.showApproveDetail}
								close={this.closeApprove}
								billtype={
									this.props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? (
										this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
									) : null
								}
								billid={
									this.props.form.getFormItemsValue(this.formId, 'pk_paybill') ? (
										this.props.form.getFormItemsValue(this.formId, 'pk_paybill').value
									) : null
								}
							/>
							{/* {导入} */}
							{createModal('importModal', {
								noFooter: true,
								className: 'import-modal',
								hasBackDrop: false
							})}
							<ExcelImport
								{...Object.assign(this.props)}
								moduleName="arap" //模块名
								billType={this.billType} //单据类型
								pagecode={this.getExportPageCode()}
								appcode={this.props.getSearchParam('c')}
								selectedPKS={this.Info.selectedPKS}
								exportTreeUrl={"/nccloud/arap/paybill/paybillexport.do"}
								//referVO={{ignoreTemplate:true}}
							/>
							{/* {联查计划预算} */}
							<Inspection
								show={this.state.isTbbLinkshow}
								sourceData={this.Info.tbbLinkSourceData}
								cancel={this.tbbLinkcancel.bind(this)}
							/>

							<InvoiceUploader
								// {...this.props}
								{...this.state.sscrpInvoiceData}
							/>
							<InvoiceLink {...this.state.sscrpInvoiceData} table={this.props.table} />
							{/* 异常模态框 */}
							<div>
								{createModal('saveCheck', {
									title: this.state.json['paybill-000042'], //'预算异常提示信息',//'预算异常提示信息',//this.state.json['10170406-000017'],// 异常提示/* 国际化处理： 异常提示信息*/
									content: this.Info.tipContent, // '预算不通过!',//this.modalContent.call(this), //弹框内容，可以是字符串或dom
									beSureBtnClick: this.saveSureBtnClick.bind(this) //点击确定按钮事件
								})}
								{createModal('deleteCheck', {
									title: this.state.json['paybill-000042'], //'预算异常提示信息',//'预算异常提示信息',//this.state.json['10170406-000017'],// 异常提示/* 国际化处理： 异常提示信息*/
									content: this.Info.tipContent, // '预算不通过!',//this.modalContent.call(this), //弹框内容，可以是字符串或dom
									beSureBtnClick: this.deleteBillSureBtnClick.bind(this) //点击确定按钮事件
								})}
								{createModal('commitAndUncommit', {
									title: this.state.json['paybill-000042'] /* 国际化处理： 异常提示信息*/,
									content: this.Info.tipContent,
									beSureBtnClick: this.commitAndUncommit.bind(this), //点击确定按钮事件
									cancelBtnClick: this.cancelClick.bind(this) //提交和收回取消事件
								})}
								{/* 指派信息弹框 */}
								{this.state.compositedisplay ? (
									<ApprovalTrans
										title={this.state.json['paybill-000045']} /* 国际化处理： 指派*/
										data={this.Info.compositedata}
										display={this.state.compositedisplay}
										getResult={this.commitAndUncommit.bind(this)}
										cancel={this.cancelClick.bind(this)}
									/>
								) : null}
							</div>
						</div>
					</div>
				</div>
			);
		} else {
			return (
				<div className="nc-bill-card">
					<div className="nc-bill-top-area">
						<div className="nc-bill-header-area">
							{/* {联查场景，默认场景，浏览态存在返回按钮} */}
							<div className="header-title-search-area">
								{createBillHeadInfo({
									title:this.props.getSearchParam('n'),
									//title:this.state.json['paybill-000046'],//国际化处理： 付款单
									backBtnClick: () => {
										this.backList();
									}
								})}
							</div>
							{/* 国际化处理： 付款单*/}
							<div className="header-button-area">
								{/* {交易类型按钮，默认场景} */}
								{!this.props.getUrlParam('scene') && !getContext(loginContextKeys.transtype) ? (
									<div className="trade-type">
										{TradeTypeButton({
											ref: 'tradetypeBtn',
											billtype: 'F3',
											dataSource: dataSource,
											switchTemplate: this.switchTemplate.bind(this)
										})}
									</div>
								) : null}

								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{/* {分页按钮，默认场景或者联查场景} */}
							{this.props.getUrlParam('scene') == 'linksce' ||
							this.props.getUrlParam('scene') == 'fip' ||
							!this.props.getUrlParam('scene') ? (
								<div className="header-cardPagination-area" style={{ float: 'right' }}>
									{createCardPagination({
										handlePageInfoChange: pageInfoClick.bind(this),
										dataSource: dataSource
									})}
								</div>
							) : null}
						</div>
						<div className="nc-bill-form-area">
							{createForm(this.formId, {
								onAfterEvent: afterEvent.bind(this),
								onBeforeEvent: formBeforeEvent.bind(this)
							})}
						</div>
					</div>
					<div className="nc-bill-bottom-area">
						<div className="nc-bill-table-area">
							{createCardTable(this.tableId, {
								tableHead: this.getTableHead.bind(this, buttons),
								modelSave: this.modelSaveClick.bind(this),
								onAfterEvent: afterEvent.bind(this),
								onBeforeEvent: bodyBeforeEvent.bind(this),
								modelAddRow: this.modelAddRow.bind(this),
								modelDelRow: this.modelDelRow.bind(this),
								sideRowChange: (record) => {
									//动态修改票据号字段类型
									modifyChecknoItemtype(
										this.props,
										this.tableId,
										record.values.pk_org.value,
										record.values.checktype.value
									);
								},
								onSelected: this.onSelected.bind(this),
								onSelectedAll: this.onSelected.bind(this),
								showCheck: true,
								showIndex: true
							})}
						</div>
					</div>

					<InvoiceUploader
						// {...this.props}
						{...this.state.sscrpInvoiceData}
					/>
					<InvoiceLink {...this.state.sscrpInvoiceData} table={this.props.table} />
					{/* {联查处理情况} */}
					<CombinedExaminationModel
						show={this.state.isCombinedExaminationModelShow}
						combinedExaminationData={this.Info.combinedExaminationData}
						pk_tradetypeid={
							this.props.form.getFormItemsValue(this.formId, 'pk_tradetypeid') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_tradetypeid').display
							) : null
						}
						billno={
							this.props.form.getFormItemsValue(this.formId, 'billno') ? (
								this.props.form.getFormItemsValue(this.formId, 'billno').value
							) : null
						}
						handleModel={this.handleCombined.bind(this)}
					/>
					{showUploader && (
						<NCUploader
							billId={
								this.props.form.getFormItemsValue(this.formId, 'pk_paybill') ? (
									this.props.form.getFormItemsValue(this.formId, 'pk_paybill').value
								) : null
							}
							billNo={
								this.props.form.getFormItemsValue(this.formId, 'billno') ? (
									this.props.form.getFormItemsValue(this.formId, 'billno').value
								) : null
							}
							target={target}
							placement={'bottom'}
							disableModify={this.props.form.getFormItemsValue(this.formId, 'approvestatus')&&this.props.form.getFormItemsValue(this.formId, 'approvestatus').value!="-1"}
							beforeUpload={this.beforeUpload}
							onHide={this.onHideUploader}
							close={() => this.closeModal()}
							customize={{
								pk_billType: this.props.form.getFormItemsValue(this.formId, 'pk_billtype')
									? this.props.form.getFormItemsValue(this.formId, 'pk_billtype').value
									: null
							}}
							customInterface={{
								queryLeftTree: '/nccloud/arap/arappub/arapFileLeftTreeQuery.do',
								queryAttachments: '/nccloud/arap/arappub/transformBillEnclosureQuery.do'
							}}
						/>
					)}

					{/* {审批详情模态框} */}
					<ApproveDetail
						show={this.state.showApproveDetail}
						close={this.closeApprove}
						billtype={
							this.props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
							) : null
						}
						billid={
							this.props.form.getFormItemsValue(this.formId, 'pk_paybill') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_paybill').value
							) : null
						}
					/>

					{/* 单据追溯组件 */}
					<BillTrack
						show={this.state.showBillTrack}
						close={() => {
							this.setState({ showBillTrack: false });
						}}
						pk={this.props.getUrlParam('id')} //单据id
						type={
							this.props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
							) : null
						} //单据类型
					/>

					{/* {打印输出} */}
					<PrintOutput
						ref="printOutput"
						url="/nccloud/arap/arappub/outPut.do"
						data={this.outputData}
						callback={this.onSubmit}
					/>
					{/* {导入} */}
					{createModal('importModal', {
						noFooter: true,
						className: 'import-modal',
						hasBackDrop: false
					})}
					<ExcelImport
						{...Object.assign(this.props)}
						moduleName="arap" //模块名
						billType={this.billType} //单据类型
						pagecode={this.getExportPageCode()}
						appcode={this.props.getSearchParam('c')}
						selectedPKS={this.Info.selectedPKS}
						exportTreeUrl={"/nccloud/arap/paybill/paybillexport.do"}
						//referVO={{ignoreTemplate:true}}
					/>

					{/* {联查计划预算} */}
					<Inspection
						show={this.state.isTbbLinkshow}
						sourceData={this.Info.tbbLinkSourceData}
						cancel={this.tbbLinkcancel.bind(this)}
					/>

					{/* 异常模态框 */}
					<div>
						{createModal('saveCheck', {
							title: this.state.json['paybill-000043'], //'预算异常提示信息',//'预算异常提示信息',//this.state.json['10170406-000017'],// 异常提示/* 国际化处理： 预算异常提示信息*/
							content: this.Info.tipContent, // '预算不通过!',//this.modalContent.call(this), //弹框内容，可以是字符串或dom
							beSureBtnClick: this.saveSureBtnClick.bind(this) //点击确定按钮事件
						})}
						{createModal('deleteCheck', {
							title: this.state.json['paybill-000042'], //'预算异常提示信息',//'预算异常提示信息',//this.state.json['10170406-000017'],// 异常提示/* 国际化处理： 异常提示信息*/
							content: this.Info.tipContent, // '预算不通过!',//this.modalContent.call(this), //弹框内容，可以是字符串或dom
							beSureBtnClick: this.deleteBillSureBtnClick.bind(this) //点击确定按钮事件
						})}
						{createModal('commitAndUncommit', {
							title: this.state.json['paybill-000044'] /* 国际化处理： 提示信息*/,
							content: this.Info.tipContent,
							beSureBtnClick: this.commitAndUncommit.bind(this), //点击确定按钮事件
							cancelBtnClick: this.cancelClick.bind(this) //提交和收回取消事件
						})}
						{/* 指派信息弹框 */}
						{this.state.compositedisplay ? (
							<ApprovalTrans
								title={this.state.json['paybill-000045']} /* 国际化处理： 指派*/
								data={this.Info.compositedata}
								display={this.state.compositedisplay}
								getResult={this.commitAndUncommit.bind(this)}
								cancel={this.cancelClick.bind(this)}
							/>
						) : null}
					</div>
				</div>
			);
		}
	}
}

Card = createPage({})(Card);

export default Card;
