import { asyncComponent } from 'nc-lightapp-front';
import List from '../list';

//付款单卡片
const card = asyncComponent(() => import(/*webpackChunkName:"/arap/paybill/paybill/card/card"*/  /* webpackMode: "eager" */ '../card'));
//预核销
const prev = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/preverify/list/prev"*/ /* webpackMode: "eager" */ '../../../verificationsheet/preverify/list'));
//及时核销 list
const nowv = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/verifynow/list/nowv"*/ /* webpackMode: "eager" */ '../../../verificationsheet/verifynow/list'));
//及时核销card
const nowvcard = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/verifynow/nowvcard"*/ /* webpackMode: "eager" */ '../../../verificationsheet/verifynow/nowvcard'));
//应收单转单
const F0 = asyncComponent(() => import(/*webpackChunkName:"/arap/receivablebill/recbill/transfer/f0transfer"*/ /* webpackMode: "eager" */ '../../../receivablebill/recbill/transfer'));
//应付单转单
const F1 = asyncComponent(() => import(/*webpackChunkName:"/arap/payablebill/payablebill/transfer/f1transfer"*/ /* webpackMode: "eager" */ '../../../payablebill/payablebill/transfer'));
//收款单
const F2 = asyncComponent(() => import(/*webpackChunkName:"/arap/gatheringbill/gatheringbill/transfer/f2transfer"*/ /* webpackMode: "eager" */ '../../../gatheringbill/gatheringbill/transfer'));
//付款合同转单
const FCT1 = asyncComponent(() => import(/*webpackChunkName:"/fct/bill/ap/transfer/fct1transfer"*/ /* webpackMode: "eager" */ '../../../../fct/bill/ap/transfer'));
//付款申请转单
const apply = asyncComponent(() => import(/*webpackChunkName:"/cmp/apply/apply/ref23/36d1transfer"*/ /* webpackMode: "eager" */ '../../../../cmp/apply/apply/ref23'));


const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: card
	},
	{
		path: '/prev',
		component: prev
	},
	{
		path: '/nowv',
		component: nowv
	},
	{
		path: '/nowvcard',
		component: nowvcard
	},
	{
		path: '/F0',
		component: F0
	},
	{
		path: '/F1',
		component: F1
	},
	{
		path: '/F2',
		component: F2
	},
	{
		path: '/FCT1',
		component: FCT1
	},
	{
		path: '/36D1',
		component: apply
	},

];

export default routes;


