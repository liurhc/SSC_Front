import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { base, createPage, ajax, spaCache,getMultiLang } from 'nc-lightapp-front';
import { initTemplate, initTemplateFull } from './init';
import { searchBtnClick } from './btnClick';
import { pagecode, searchId, headId, bodyId, billType, dataSource, mainCode, mainPageCode } from './constants';
let { setDefData, getDefData } = spaCache;
let { NCBackBtn, NCToggleViewBtn } = base;
import afterEvent from '../../../public/components/searchAreaAfterEvent'//查询区编辑后事件


class TransferTable extends Component {
	constructor(props) {
		super(props);
		this.searchId = searchId;
		this.headId = headId;
		this.bodyId = bodyId;
		this.pagecode = pagecode;
		this.appcode = props.getSearchParam('c');
		this.state = {
			expand: true,
			json:{}
		};
	}
	componentDidMount() {
		
		let callback = (json) => {
			this.setState({json:json},() => {
				let { hasCache } = this.props.transferTable;
				if (!hasCache(dataSource)) {
					this.props.transferTable.setTransferTableValue(headId, bodyId, [], 'pk_paybill', 'pk_payitem');
				}
				initTemplate.call(this, this.props, this.initShow);
			});
		}
		getMultiLang({moduleId:['paybill','public'],domainName :'arap',currentLocale:'simpchn',callback});
	}

	//转单跳转目的小应用
	transfer = () => {
		setDefData('src_appcode', dataSource, this.props.getUrlParam('src_appcode'));
		setDefData('src_pagecode', dataSource, pagecode);
		this.props.pushTo('/card', {
			status: 'add',
			type: 'transfer',
			srcBilltype: billType,
			dataSource: dataSource,
			pagecode: this.props.getUrlParam('dest_tradetype')
		});
	};

	//返回列表
	backList = () => {
		let dest_billtype = this.props.getUrlParam('dest_billtype');
		let pagecode = null;
		if (dest_billtype) {
			if (dest_billtype == 'F0') {
				pagecode = '20060RBM_LIST'; //应收单列表
			} else if (dest_billtype == 'F1') {
				pagecode = '20080PBM_LIST'; //应付单列表
			} else if (dest_billtype == 'F2') {
				pagecode = '20060GBM_LIST'; //收款单列表
			} else if (dest_billtype == 'F3') {
				pagecode = '20080EBM_LIST'; //付款单列表
			}
		}
		this.props.pushTo('/list', {
			pagecode: pagecode
		});
	};

	render() {
		const { transferTable, button, search } = this.props;
		const { NCCreateSearch } = search;
		const { createButton } = button;
		const { createTransferTable, createTransferList, getTransformFormDisplay } = transferTable;
		return (
			<div id="paybill-transferListId" className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div>
						<NCBackBtn onClick={this.backList} />
					</div>
					<div className="header-title-search-area">
						<h2 className="title-search-detail">{this.state.json['paybill-000050']}</h2>{/* 国际化处理： 选择付款单*/}
					</div>
					<div className="header-button-area">
						<NCToggleViewBtn
							expand={this.state.expand}
							onClick={() => {
								if (!this.props.meta.getMeta()[mainCode]) {
									initTemplateFull(this.props); //加载主子拉平模板
								} else {
									initTemplate.call(this, this.props); //加载主子拉平模板
								}
								this.props.transferTable.changeViewType(headId);
								let expandtemp = !this.state.expand;
								this.setState({ expand: expandtemp });
							}}
						/>
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(
						searchId,
						{
							clickSearchBtn: searchBtnClick.bind(this),
							onAfterEvent: afterEvent.bind(this),//编辑后事件
						}
						//模块id
					)}
				</div>
				<div className="nc-bill-transferTable-area">
					{createTransferTable({
						dataSource: dataSource,
						headTableId: headId, //表格组件id
						bodyTableId: bodyId, //子表模板id
						fullTableId: mainCode, //主子拉平模板id
						//点击加号展开，设置表格数据
						transferBtnText: this.state.json['paybill-000049'], //转单按钮显示文字/* 国际化处理： 生成下游单据*/
						containerSelector: '#transferList',
						onTransferBtnClick: this.transfer
					})}
				</div>
			</div>
		);
	}
}

TransferTable = createPage({
	
})(TransferTable);

export default TransferTable;
