
/**
 * 查询区域
 */
export const searchId = '20080EBM_query';

/**
 * 表头区域
 */
export const headId = '20080EBM_head';

/**
 * 表体区域
 */
export const bodyId = '20080EBM_bodys';

export const pagecode = '20080EBM_TRANSFER';

//主子拉平展示页面编码
export const mainPageCode = '20080EBM_TRANSFERMAIN';

// 主子拉平显示区域编码
export const mainCode =  'transfermain'

/**
 * 单据类型
 */
export const billType = 'F3';

/**
 * 转单单页应用缓存,命名规范为："领域名.模块名.节点名.自定义名"。
 */
export const dataSource = 'fi.arap.recbill.20080EBM_TRANSFER';
