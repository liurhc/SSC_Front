import { base } from 'nc-lightapp-front';
import { modifierSearchMetas } from '../../../../public/components/pubUtils/arapListSearchRefFilter';
import { pagecode, bodyId, headId, searchId, billType } from '../constants';
import setDefOrgBilldateSrchArea from '../../../../public/components/defOrgBilldateSrchArea.js';

export default function (props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pagecode,
			appcode: props.getUrlParam('src_appcode'),
		},
		function (data) {
			if (data) {
				if (data.template) {
					//高级查询设置财务组织默认值
					setDefOrgBilldateSrchArea(props, searchId, data);
					let meta = data.template;
					// modifierMeta(props, meta);
					modifierSearchMetas(searchId, props, meta, billType,null,that);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	);
}

/**
 * 自定义元数据样式
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	// 设置固定宽度 撑开子表
	meta[headId].items.map((item) => {
		if (item.attrcode == 'billno') {
			item.width = 180;
		} else {
			item.width = 120;
		}
		return item;
	});
	meta[bodyId].items.map((item) => {
		if (item.attrcode == 'billno') {
			item.width = 180;
		} else {
			item.width = 120;
		}
		return item;
	});


	return meta;
}
