import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang: {
			domainName: 'arap',
			currentLocale: 'simpchn',
			moduleId: 'refer'
		},
		refType: 'grid',
		refName: 'refer-000000',//交易类型参照
		placeholder: 'refer-000001',//交易类型
        refCode: 'arap.refer.pub.ARTranstypeRef',
		queryGridUrl: '/nccloud/arap/ref/arTranstypeRef.do',
		isMultiSelectedEnabled: false,
		columnConfig: [{name: [ 'refer-000002', 'refer-000003' ],code: [ 'refcode', 'refname' ]}]
	};//交易类型编码,交易类型名称

	return <Refer {...Object.assign(conf, props)} />
}
