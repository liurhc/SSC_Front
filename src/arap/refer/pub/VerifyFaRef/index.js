import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang: {
			domainName: 'arap',
			currentLocale: 'simpchn',
			moduleId: 'refer'
		},
		refType: 'grid',
		refName: 'refer-000019',//核销方案
		refCode: 'arap.refer.pub.VerifyFaRef',
		queryGridUrl: '/nccloud/arap/ref/verifyFaRef.do',
		isMultiSelectedEnabled: true,
		columnConfig: [{
			name: ['refer-000020', 'refer-000021', 'refer-000022'],//所在系统,方案名称,组织
			code: [
				"case when arap_verify_fa.node_code = '20060VPS' then"+ 'refer-000023'+" when arap_verify_fa.node_code = '20080VPS' then "+'refer-000024'+" else "+'refer-000025'+" end node_code",
				"case when INSTR(arap_verify_fa.name, '+') <> 0 then substr(arap_verify_fa.name, 1, INSTR(arap_verify_fa.name, '+') - 1) else arap_verify_fa.name end faname",
				"org_orgs.name"
			]
		}]//应收系统,应付系统,报帐中心
	};

	return <Refer {...Object.assign(conf, props) } />
}