import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang: {
			domainName: 'arap',
			currentLocale: 'simpchn',
			moduleId: 'refer'
		},
		refType: 'grid',
		refName: 'refer-000004',//未核销收款单分录
		refCode: 'arap.refer.pub.ArapGathingBillRef',
		queryGridUrl: '/nccloud/arap/ref/arapGathingBillRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [{
            name: ["refer-000005", "refer-000006", "refer-000007", "refer-000008","refer-000009"],//单据号,单据日期,原币金额,未核销金额,币种
			code: ["billno", "billdate", "money_cr", "occupationmny","pk_currtype"]
        }]
	};

	return <Refer {...Object.assign(conf, props)} />
}