import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang: {
			domainName: 'arap',
			currentLocale: 'simpchn',
			moduleId: 'refer'
		},
		refType: 'grid',
		refName: 'refer-000016',//贷方引用信息指定字段
		refCode: 'arap.refer.pub.CreditRefInfoStdRef',
		queryGridUrl: '/nccloud/arap/ref/creditRefInfoStdRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [{
            name: ['refer-000017','refer-000018'],
            code: ["name","field"]
        }]//字段名称,显示名称
	};

	return <Refer {...Object.assign(conf, props)} />
}