import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang: {
			domainName: 'arap',
			currentLocale: 'simpchn',
			moduleId: 'refer'
		},
		refType: 'grid',
		refName: 'refer-000010',//催款语气
		refCode: 'arap.refer.pub.ArapReminderBillRef',
		queryGridUrl: '/nccloud/arap/ref/arapReminderBillRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [{
            name: ['refer-000011', 'refer-000012', 'refer-000013', 'refer-000009'],//编码,名称,类别,币种
			code: ["arap_notice.code", "arap_notice.name", "case arap_notice.type when '1' then '账龄' when '2' then '余额' else '余额' end type ", "bd_currtype.name"]
        }]//账龄,余额,余额
	};

	return <Refer {...Object.assign(conf, props)} />
}