import { toast,ajax,promptBox,cardCache } from 'nc-lightapp-front';
import { billType ,dataSource,searchId} from '../constants';
import { headButton } from '../../../../public/components/pubUtils/buttonName.js';
import arapLinkReport from "../../../../public/components/arapBillLinkReport.js";
import linkvouchar from '../../../../public/components/linkvouchar.js';
import madeBill from '../../../../public/components/madeBill.js';
let { setDefData, getDefData } = cardCache;

export default function buttonClick(props, id) {
    switch (id) {
        case headButton.MadeBill://制单
            madeBill(this,props,this.props.table.getCheckedRows(this.tableId),'pk_estipayablebill',props.getSearchParam('c'),true);
            break;
        case headButton.LinkBal://联查余额表
            arapLinkReport(this.props,getFirstCheckedData(this.props, this.tableId).data.values.pk_estipayablebill.value,billType,getFirstCheckedData(this.props, this.tableId).data.values.objtype.value);
            break;
        case headButton.LinkVouchar://联查凭证
            let voucharInfo = getFirstCheckedData(this.props, this.tableId);
	        linkvouchar(this,props, voucharInfo, voucharInfo.data.values.pk_estipayablebill.value, props.getSearchParam('c'));
	        break;
        case 'Print'://打印
            let selectedData = this.props.table.getCheckedRows(this.tableId);
            if (selectedData.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            let pk_bills = [];
            selectedData.forEach((val) => {
                pk_bills.push(val.data.values.pk_estipayablebill.value);
            });
            this.printData.oids = pk_bills;
            this.printData.nodekey = "card";
            this.onPrint();
            break;
        case headButton.PrintList://打印清单
            const selectedDatas = this.props.table.getCheckedRows(this.tableId);
            pk_bills = [];
            selectedDatas.forEach((val) => {
                pk_bills.push(val.data.values.pk_estipayablebill.value);
            });
            if (pk_bills.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            this.printData.oids = pk_bills;
            this.printData.nodekey = "list";
            this.onPrint();
            break;
        case headButton.Output://打印输出
            selectedData = this.props.table.getCheckedRows(this.tableId);
            if (selectedData.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            pk_bills = [];
            selectedData.forEach((val) => {
                pk_bills.push(val.data.values.pk_estipayablebill.value);
            });
            this.outputData.oids = pk_bills;
            this.outputData.nodekey = "card";
            this.printOutput();
            break;
        case headButton.OfficalPrint://正式打印
            selectedData = this.props.table.getCheckedRows(this.tableId);
            if (selectedData.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            pk_bills = [];
            selectedData.forEach((val) => {
                pk_bills.push(val.data.values.pk_estipayablebill.value);
            });
            this.printData.oids = pk_bills;
            this.printData.nodekey = "card";
            this.officalPrintOutput();
            break;
        case headButton.CancelPrint://取消正式打印
            selectedData = this.props.table.getCheckedRows(this.tableId);
            if (selectedData.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            pk_bills = [];
            selectedData.forEach((val) => {
                pk_bills.push(val.data.values.pk_estipayablebill.value);
            });
            this.printData.oids = pk_bills;
            this.printData.nodekey = "card";
            this.cancelPrintOutput();
            break;
        case headButton.Refresh://刷新
            let data = getDefData(searchId, dataSource);
            if (data) {
                ajax({
                    url: '/nccloud/arap/estipayablebill/queryscheme.do',
                    data: data,
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            if(data){
                                toast({ color: 'success', title: this.state.json['payablebill-000062'] });/* 国际化处理： 刷新成功*/
                                this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                            }else{
                                toast({ color: 'success', content: this.state.json['payablebill-000017'] });/* 国际化处理： 未查询到数据*/
                                this.props.table.setAllTableData(this.tableId, {rows:[]});
                            } 
                            setDefData(this.tableId, dataSource, data);//放入缓存
                            this.onSelected();
                        }
                    }
                });
            } else {
                toast({ color: 'success', title: this.state.json['payablebill-000062'] });/* 国际化处理： 刷新成功*/
                this.props.table.setAllTableData(this.tableId, { rows: [] });
            }

            break;
        case headButton.BillLinkQuery://单据追溯
            let selectInfo = getFirstCheckedData(this.props, this.tableId);
            this.Info.pk_bill = selectInfo.data.values.pk_estipayablebill.value;
            this.Info.pk_tradetype = selectInfo.data.values.pk_tradetype.value;
            this.setState({
                showBillTrack: true,
            })
            break;

        case headButton.UnApprove://取消审批
            let unApproveInfo = props.table.getCheckedRows(this.tableId);
            if(unApproveInfo.length == 0){
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            let unApprovepks=[];
            let unApproveIndexInfo = new Map();
            unApproveInfo.forEach((val) => {
                let pk = val.data.values.pk_estipayablebill.value;
                let index = val.index;
                unApprovepks.push(pk );
                unApproveIndexInfo.set(pk , index)
            });
            ajax({
                url: '/nccloud/arap/arappub/listunapprove.do',
                data: {
                    pk_bills:unApprovepks,
                    billType: billType,
                    pageId :props.getSearchParam('p'),
                    indexInfo :unApproveIndexInfo
                },
                success: (res) => {
                    let {success ,data} = res;
                    if (success) {
                       //更新当前行数据
                      if(data.grid){
                        let grid = data.grid;
                        let updateValue = [];
                        for(let key in grid){
                            updateValue.push({index :key ,data : {values : grid[key].values }})
                        }
                        props.table.updateDataByIndexs(this.tableId,updateValue);
                      }

                      if(data.message){
                        toast({
                            duration:  'infinity',          
                            color: data.PopupWindowStyle,        
                            content: data.message,   
                        })
                        
                      }
                      
                    }
                }
            });
            break;
        case headButton.Approve://审批
            let ApproveInfo = props.table.getCheckedRows(this.tableId);
            if(ApproveInfo.length == 0){
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            let Approvepks=[];
            let ApproveIndexInfo = new Map();
            ApproveInfo.forEach((val) => {
                let pk = val.data.values.pk_estipayablebill.value;
                let index = val.index;
                Approvepks.push(pk );
                ApproveIndexInfo.set(pk , index)
            });
            ajax({
                url: '/nccloud/arap/arappub/listapprove.do',
                data: {
                    pk_bills:Approvepks,
                    billType: billType,
                    pageId :props.getSearchParam('p'),
                    indexInfo :ApproveIndexInfo
                },
                success: (res) => {
                    let {success ,data} = res;
                    if (success) {
                       //更新当前行数据
                      if(data.grid){
                        let grid = data.grid;
                        let updateValue = [];
                        for(let key in grid){
                            updateValue.push({index :key ,data : {values : grid[key].values }})
                        }
                        props.table.updateDataByIndexs(this.tableId,updateValue);
                      }

                      if(data.message){
                        toast({
                            duration:  'infinity',          
                            color: data.PopupWindowStyle,         
                            content: data.message,   
                        })
                      }
                      
                    }
                }
            });
            break;
            case headButton.AttachManage:
			let AttachInfo = getFirstCheckedData(this.props, this.tableId);
			this.Info.pk_bill = AttachInfo.data.values.pk_estipayablebill.value;
			this.Info.billno = AttachInfo.data.values.billno.value;
			this.setState({
			  showUploader: true,
			  target: null
			})
			break;
        default:
            break
    }
}

//获取选中数据的第一行,选中多行的时候只取第一行数据
let getFirstCheckedData = function (props, tableId) {
    let checkedData = props.table.getCheckedRows(tableId);
    let checkedObj;
    if (checkedData.length > 0) {
        checkedObj = checkedData[0]
    } else {
        toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
        return;
    }
    return checkedObj;
}


//获取选中数据的id和billType
let getAllCheckedData = function (props, tableId, billType) {
    let checkedData = props.table.getCheckedRows(tableId);
    let checkedObj = [];
    checkedData.forEach((val) => {
        checkedObj.push({
            pk_bill: val.data.values.pk_estipayablebill.value,
            ts: val.data.values.ts.value,
            billType: billType
        }
        );
    });
    return checkedObj;
}
