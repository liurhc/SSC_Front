import { createPage, ajax, base, toast, cardCache } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;
import { buttonVisible, getButtonsKey, getInnerButtonkey } from '../../../../public/components/pubUtils/buttonvisible.js';
import tableButtonClick from './tableButtonClick';
import { tableId, searchId, billType, searchKey, dataSource } from '../constants';
import { modifierSearchMetas } from '../../../../public/components/pubUtils/arapListSearchRefFilter';
let { setDefData } = cardCache;
import setDefOrgBilldateSrchArea from '../../../../public/components/defOrgBilldateSrchArea.js';
import {OperationColumn} from '../../../../public/components/pubUtils/arapConstant';
export default function (props,callback) {
	const that = this;
	let pagecode = that.getPagecode();
	let appcode = props.getSearchParam('c') ? props.getSearchParam('c') : props.getUrlParam('c');
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode,//注册按钮的id
			reqDataQuerypage: {
				rqUrl: '/arap/arappub/querypage.do',
                rqJson: `{\n  \"pagecode\": \"${pagecode}\",\n  \"appcode\": \"${appcode}\"\n}`,
                rqCode: 'template'
			}
		},
		function (data) {
			if (data) {
				if(!data.template[tableId]){
					return;
				}
				let lineButton = [];
				if (data.button) {
					let button = data.button;
					getButtonsKey(button, that.Info.allButtonsKey);//获取所有按钮
					props.button.setButtons(button);
				}
				if (data.template) {
					//高级查询设置财务组织默认值
					setDefOrgBilldateSrchArea(props, searchId, data);
					let meta = data.template;
					lineButton = getInnerButtonkey(data.button);
					meta = modifierMeta(props, meta, lineButton,that);
					modifierSearchMetas(searchId, props, meta, billType, null, that);
					props.meta.setMeta(meta);
				}
				if(callback){
					callback()
				}

			}
		}
	)
}


function modifierMeta(props, meta, lineButton,that) {


	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'billno') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer' }}
						onClick={() => {
							//表页跳转的时候，获取所有查询区条件，并放入缓存
							let searchVal = props.search.getAllSearchData(searchId);
							if (searchVal) {
								setDefData(searchKey, dataSource, searchVal);
							}
							props.pushTo('/card', {
								status: 'browse',
								id: record.pk_estipayablebill.value,
								pagecode: record.pk_tradetype.value,
							})
						}}
					>
						{record.billno && record.billno.value}
					</a>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableId].items.push({
		label: that.state.json['payablebill-000011'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: OperationColumn,
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let buttonAry = lineButton ? lineButton : [];
			let trueBtn = [];
			for (let i = 0; i < buttonAry.length; i++) {
				let flag = buttonVisible('browse', record, buttonAry[i]);
				if (flag) {
					trueBtn.push(buttonAry[i]);
				}
			}
			return props.button.createOprationButton(trueBtn, {
				area: "list_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(that,props, key, text, record, index)
			});
		}
	});
	return meta;
}
