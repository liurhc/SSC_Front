import handleRelationItems from '../../../../../arap/public/components/editFormula/handleRelationItems.js';
import { tableId, formId, dataSource, pkname, billType } from '../constants';

export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
    if(changedrows instanceof Array){
		if(changedrows[0].newvalue.value == changedrows[0].oldvalue.value){
			return;
		}
	}
    let index = 0;
    let type = null;
    if (moduleId == this.formId) {
        index = 0;
        type = 'form'
    } else if (moduleId == this.tableId) {
        index = i;
        type = 'table'
    }
    let callback = () => { }

    //以下为公式适配
    let afterData = {
        type: type,  //编辑是表单还是表格
        areaCode: moduleId,  //区域编码
        key,    //编辑的字段的主键
        value, //编辑字段的新值
        changedrows, //编辑字段的旧值
        index: index,//若编辑的是表格，为表格的行数 从0 开始
        callback: callback, //编辑后
    }
    let billinfo = {
        billtype: 'card',
        pagecode: this.getPagecode(),
        headcode: formId,
        bodycode: tableId
    }
    handleRelationItems.call(this, afterData, billinfo);



}
