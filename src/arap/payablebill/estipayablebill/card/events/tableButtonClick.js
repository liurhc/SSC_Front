import { tableId } from '../constants';
import {innerButton} from '../../../../public/components/pubUtils/buttonName.js';


export default function (props, key, text, record, index) {
    switch (key) {
        // 表格操修改
        case innerButton.open_browse:
            props.cardTable.toggleRowView(tableId, record);
            break;
        case innerButton.open_edit:
            props.cardTable.openModel(tableId, 'edit', record, index);
            break;
        default:
            break;
    }
};
