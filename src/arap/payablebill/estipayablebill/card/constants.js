
/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 表体区域
 */
export const tableId = 'bodys';

/**
 * 单据类型
 */
export const billType = '23E1';
/**
 * 单据类型
 */
export const tradeType = '23C1';

/**
 * 默认模板节点标识
 */
export const nodekey = "card";

/**
 * 单页应用缓存
 */
export const dataSource  = 'fi.arap.estipayablebill.200805EPB';

/**
 * 单页应用缓存主键名字(该名字为表的主键名字)
 */
export const pkname  = 'pk_estipayablebill';

/**
 * 列表联查页面pageId
 */
export const linkPageId = '200805EPB_LIST_LINK';

