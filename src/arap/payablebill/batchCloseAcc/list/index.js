import React, { Component } from 'react';
import { createPage,getMultiLang } from 'nc-lightapp-front';
import ReactDOM from 'react-dom';
import Batchcloseaccbook from '../../../../uapbd/orgcloseacc/batch_closeaccbook/main';
 
/**
* 组织批量关账
*/
export default class Batchcloseaccbook_uapbd extends Component{
    constructor(props){
        super(props);
        this.state = {
            json :{}
        }
    }
    componentWillMount(){
        let callback = (json) => {
			this.setState({json:json});
		}
		getMultiLang({moduleId:'payablebill',domainName :'arap',currentLocale:'simpchn',callback});
    }
    render(){
        /**
         *  后面还要考虑 多语 的情况
         * @type {{title: string, showMoudles: Object}}
         */
        let config = {
            title : this.state.json['payablebill-000000'],/* 国际化处理： 应付批量关账*/
            appCode : this.props.getSearchParam('c'),
            showMoudles : { //传入需要显示的模块 key-value
                //'2002'  : true,//2002总账
                // '2006'  : true,//2006应收管理
                '2008'  : true,//2008应付管理
                // '2011'  : true,//2011费用管理
                // '2014'  : true,//2014存货核算
                // '2016'  : true,//2016税务管理
                // '3607'  : true,//3607现金管理
                // '3820'  : true,//3820责任会计
                // '3840'  : true,//3840项目成本会计
                // '4008'  : true//4008库存
            }
        }
        return (
            <Batchcloseaccbook {...{config:config}}/>
        )
    }
}
Batchcloseaccbook_uapbd = createPage({appAutoFocus: false})(Batchcloseaccbook_uapbd);

ReactDOM.render(<Batchcloseaccbook_uapbd />, document.querySelector('#app'));
