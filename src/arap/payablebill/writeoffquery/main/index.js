import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';

export default class Test extends Component {
	constructor(props) {
		super(props);
	}
	
	disposeSearch(meta, props) {
		// 参照过滤
		// let items = meta['light_report'].items;
		// items.forEach((item) => {
		// 	if (item.attrcode == 'user_name') {
		// 		// 如果user_name的参照需要根据前面选中参照的值进行过滤
		// 		item.queryCondition = () => {
		// 			// 添加过滤方法
		// 			let data = props.search.getSearchValByField('light_report', 'pk_org'); // 获取前面选中参照的值
		// 			data = (data && data.value && data.value.firstvalue) || ''; // 获取value
		// 			return { pk_org: data }; // 根据pk_org过滤
		// 		};
		// 	}
		// });
		return meta; // 处理后的过滤参照返回给查询区模板
	}
	
	/**
      * 
      * items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
      * props: 平台props
      * type: 是普通查询还是高级查询
      * queryInfo: 经过包装的queryCondition
    */
	expandSearchVal(items, props, type, queryInfo) {
		// 变量赋值拓展
		console.log(items);
		return items;
	}

	/**
      * searchId: 查询区需要的searchId参数
      * 'vname': 需要附默认值的字段
      * {value: '111'}: 显示值，区间为[]，具体可以对照平台查询区修改
      * 'like': 为oprtype字段值
    */
	// setDefaultVal(searchId, props) {
	// 	//查询区默认显示字段值
	// 	props.search.setSearchValByField(searchId, 'vname', { value: '111' }, 'like');
	// }
	
    /**
      * props: props
      * searchId: 查询区需要的searchId参数
      * field: 编辑后的key
      * val: 编辑后的value
    */
    onAfterEvent (props, searchId, field, val) {
      //查询区编辑后事件
      console.log(props)
      console.log(searchId)
      console.log(field)
      console.log(val)
    }
    
    /**
      * isRange: 查询后数据区域内点击单元格按钮可操作。如需要像升降序按钮那样，则设置 disabled={isRange}属性
      * data: 表格数据
      * coords: 选中的单元格
    */
    // CreateNewSearchArea (isRange, data, coords) {
    //   // button区域业务端自定义的新增按钮
    //   return <NCButton disabled={isRange} className="btn" shape="border">新增按钮</NCButton>
    // }
    
    /**
      * transSaveObject: transSaveObject参数
      * obj: 点击的联查item信息
      * data: 联查需要的参数
      * props: 平台props
      * url: 平台openTo第一个参数
      * urlParams: 平台openTo第二个参数
      * sessonKey: sessionStorage的key
    */
    setConnectionSearch (transSaveObject, obj, data, props, url, urlParams, sessonKey) {
      sessionStorage.setItem(sessonKey, transSaveObject); //处理好的transSaveObject放到浏览器内存中，必须，跳转页面会用到
      props.openTo(url, urlParams) //业务端处理完成后执行跳转新页面
    }
    
    /**
      * nodekey: 通过nodekey业务组判断走哪一个接口
    */
    // printUrlChange = (nodekey) => {
	//   if (nodekey == '10300004') {
	// 	return '/nccloud/report/widget/业务拓展接口.do'
	//   }
	// }
	
	/**
	  * props: 平台props
	  * searchId: 查询区searchId
	*/
	clickAdvBtnEve = (props, searchId) => {
	    
	}
    
	render() {
		return (
			<div className="table">
				<SimpleReport
				    // showAdvBtn={true}
					// setDefaultVal={this.setDefaultVal.bind(this)}
					expandSearchVal={this.expandSearchVal.bind(this)}
					disposeSearch={this.disposeSearch.bind(this)}
					onAfterEvent={this.onAfterEvent.bind(this)}
					// CreateNewSearchArea={this.CreateNewSearchArea.bind(this)}
					// setConnectionSearch={this.setConnectionSearch.bind(this)}
					// printUrlChange={this.printUrlChange}
				/>
			</div>
		);
	}
}

ReactDOM.render(<Test />, document.getElementById('app'));