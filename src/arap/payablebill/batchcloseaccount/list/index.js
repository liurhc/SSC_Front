import React, { Component } from 'react';
import { getMultiLang } from 'nc-lightapp-front';
import BatchCloseAccount from '../../../public/components/batchcloseaccount/index';
class BatchCloseAccount_AP extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {}
		}
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({json:json});
		}
		getMultiLang({moduleId:'payablebill',domainName :'arap',currentLocale:'simpchn',callback});
	}

	

	render() {
		return (
			<BatchCloseAccount {...{
				title:this.state.json['payablebill-000001'],/* 国际化处理： 应付集中结账*/
				moudleId: '2008',// moudleId 2008代表应付   2006代表应收
				prodId: 'AP',//prodId AR代表收 AP代表应付
				sfbz: 'yf',// 收付标志 应付yf 应收ys
			}} />
		)
		
	}
}



ReactDOM.render(<BatchCloseAccount_AP />, document.querySelector('#app'));
