import { ajax, base, spaCache, toast } from 'nc-lightapp-front';
let { setDefData, getDefData } = spaCache;
import { pagecode, searchId, headId, bodyId, billType, dataSource } from '../constants';


//点击查询，获取查询区数据
export default function () {
	let searchVal = this.props.search.getAllSearchData(searchId);
	if(searchVal){
		let data = {
			queryInfo: this.props.search.getQueryInfo(searchId),
			appCode: this.props.getUrlParam('src_appcode'),
			pageId: pagecode,
			src_billtype: billType,
			dest_tradetype: this.props.getUrlParam('dest_tradetype'),
			busitype: getDefData(billType + this.props.getUrlParam('src_tradetype'), "transfer.dataSource")
		};
	
		//得到数据渲染到页面
		ajax({
			url: '/nccloud/arap/arappub/transferquery.do',
			data: data,
			success: (res) => {
				if (res.data) {
					this.props.transferTable.setTransferTableValue(headId, bodyId, res.data, 'pk_payablebill', 'pk_payableitem');
					/*
					* key：存储数据的key
					* dataSource: 缓存数据命名空间
					* data： 存储数据
					*/
					setDefData('searchData', dataSource, res.data);
				} else {
					this.props.transferTable.setTransferTableValue(headId, bodyId, [], 'pk_payablebill', 'pk_payableitem');				
					toast({ content: this.state.json['payablebill-000059'], color: 'warning'});/* 国际化处理： 未查询到符合条件的数据*/
				}
			}
		})
	}

}
