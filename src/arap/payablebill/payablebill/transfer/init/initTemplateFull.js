import { base } from 'nc-lightapp-front';
import { mainPageCode, bodyId, headId, searchId, billType } from '../constants';

export default function (props) {
	props.createUIDom(
		{
			pagecode: mainPageCode,
			appcode: props.getUrlParam('src_appcode'),
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					// modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
			}
		}
	);
}

/**
 * 自定义元数据样式
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	// 设置固定宽度 撑开子表
	meta[headId].items.map((item) => {
		if (item.attrcode == 'billno') {
			item.width = 180;
		}else{
			item.width = 120;
		}
		return item;
	});
	meta[bodyId].items.map((item) => {
		if (item.attrcode == 'billno') {
			item.width = 180;
		}else{
			item.width = 120;
		}
		return item;
	});
	return meta;
}
