import initTemplate from './initTemplate';
import initTemplateFull from './initTemplateFull';

export {initTemplate, initTemplateFull}
