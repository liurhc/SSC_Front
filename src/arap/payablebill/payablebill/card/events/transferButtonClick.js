import { ajax, base, toast, cardCache, promptBox } from 'nc-lightapp-front';
import { tableId, formId, leftarea, dataSource, pkname } from '../constants';
import buttonClick from './buttonClick';
let { setDefData, getDefData, addCache, deleteCacheById, getCacheById, updateCache } = cardCache;

export default function transferButtonClick(props, id) {
	let that = this
	let amount = props.transferTable.getTransformFormAmount(leftarea)
	if(amount == 1){
		buttonClick.call(this, props, id);
		return
	}
	switch (id) {
		case 'Cancel':
			{
				promptBox({
					color: 'warning',
					title: this.state.json['payablebill-000047'],             /* 国际化处理： 请注意*/
					content: this.state.json['payablebill-000048'],/* 国际化处理： 是否取消？*/
					beSureBtnName: this.state.json['payablebill-000049'],/* 国际化处理： 是*/
					cancelBtnName: this.state.json['payablebill-000050'],/* 国际化处理： 否*/
					beSureBtnClick: () => {
						if(this.props.getUrlParam('id')){
							this.props.setUrlParam({ status: 'browse' });
							let id = this.props.getUrlParam('id');
							let cardData = getCacheById(id, dataSource);
							if (cardData) {
								this.props.beforeUpdatePage();//打开开关
								this.props.form.EmptyAllFormValue(this.formId);
								this.props.cardTable.setTableData(this.tableId, { rows: [] });
								this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
								this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
								this.props.cardTable.setStatus(this.tableId, 'browse');
								this.props.form.setFormStatus(this.formId, 'browse');
								this.props.updatePage(this.formId, this.tableId);//关闭开关
								this.toggleShow();
							}

						}else {
							props.transferTable.setTransformFormStatus(leftarea, {
								status: false,
								onChange: (current, next) => {
									// toast({ color: 'success', content: '取消成功' });
								}
							})
						}
					}
				})
			}
			break;
		case 'Delete':
			promptBox({
				color: 'warning',
				title: this.state.json['payablebill-000006'] /* 国际化处理： 删除*/,
				content: this.state.json['payablebill-000010'] /* 国际化处理： ​确定要删除吗？*/,
				noFooter: false,
				noCancelBtn: false,
				beSureBtnName: this.state.json['payablebill-000008'] /* 国际化处理： 确定*/,
				cancelBtnName: this.state.json['payablebill-000009'] /* 国际化处理： 取消*/,
				beSureBtnClick: () => {
					ajax({
						url: '/nccloud/arap/arappub/delete.do',
						data: [{
							pk_bill: this.props.getUrlParam('id'),
							ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
							billType: this.billType
						}],
						success: function (res) {
							if (props.transferTable.getTransformFormAmount(leftarea) == 1) {
								history.go(-1);
							} else {
								deleteCacheById(pkname, props.getUrlParam('id'), dataSource);
								props.transferTable.setTransformFormStatus(leftarea, {
									status: false,
									onChange: (current, next) => {
										toast({ color: 'success', content: that.state.json['payablebill-000013'] });/* 国际化处理： 删除成功*/
										// this.toggleShow(current.head[this.formId]);
									}
								})
							}
						}
					})
				}
			});
			break;
		case 'Copy':		
			ajax({
				url: '/nccloud/arap/arappub/copy.do',
				data: {
					pk_bill: props.getUrlParam('id'),
					ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
					pageId: this.getPagecode(),
					billType: this.billType,
					tradeType: this.getPagecode(),
				},
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							this.props.form.EmptyAllFormValue(this.formId);
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}
					this.props.setUrlParam({ status: 'add'})
					this.props.addUrlParam({ operFlag: 'copy'})	 
					this.state.buttonfalg = true;
					props.resMetaAfterPkorgEdit(); 
					this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': false });
					this.toggleShow()
				}
			});
			break;
		default:
			buttonClick.call(this, props, id);
			break;
	}
}

export function setValue(props, res) {
	this.props.beforeUpdatePage();//打开开关
	if (res.data.head && res.data.head[formId]) {
		props.form.setAllFormValue({ [formId]: res.data.head[formId] });
	}
	if (res.data.body && res.data.body[tableId]) {
		props.cardTable.updateDataByRowId(this.tableId, res.data.body[this.tableId]);
	}
	let pk_payablebill = this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value
	let newCardData = this.props.createMasterChildData(
		this.getPagecode(),
		this.formId,
		this.tableId
	);
	
	let pkvalues = [];
	let amount = this.props.cardTable.getNumberOfRows(this.tableId) * 1
	for(var i =0 ; i< amount; i++){
		pkvalues.push(this.props.cardTable.getValByKeyAndIndex(this.tableId, i, 'top_itemid').value);
	}
	/**
	 * 保存转单上游选中表体pks到转单的dataSource中
	 */
	props.transferTable.savePk(props.getUrlParam("dataSource"), pkvalues);
	/**
	 * 将新增单据数据保存到dataSource
	 */
	if (props.getUrlParam('status') == 'add') {
		addCache(pk_payablebill, newCardData, formId, dataSource)
	}else {
		updateCache(pkname, pk_payablebill, newCardData, formId, dataSource);
	}
	this.props.updatePage(this.formId, this.tableId);//关闭开关
	this.props.transferTable.setTransformFormStatus(leftarea, {
		status: true
		/** onChange: (current, next, currentIndex) => {
			
			this.transferIndex = currentIndex + 1;
			this.Info.isNeedSelect = true
			props.transferTable.setTransferListValueByIndex(
				leftarea,
				newCardData,
				currentIndex
			)
		} */
	})
}
