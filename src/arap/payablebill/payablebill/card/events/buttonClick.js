import { ajax, base, toast, cacheTools, print, cardCache, promptBox } from 'nc-lightapp-front';
import { tableId, formId, dataSource, pkname, billType } from '../constants';
import { headButton, bodyButton } from '../../../../public/components/pubUtils/buttonName';
import {
	calculateHeadMoney,
	delLine,
	copyLine,
	pasteLine,
	pasteToEndLine
} from '../../../../public/components/pubUtils/billPubUtil.js';
import { imageScan, imageView } from 'sscrp/rppub/components/image';
import { cardBodyControl } from '../../../../public/components/pubUtils/buttonvisible.js';
import { getTransferInfo } from '../../../../public/components/pubUtils/transferButtonUtil.js';
import initTemplate from './initTemplate';
import { BodyVerify, WholeVerify } from '../../../../public/components/pubUtils/arapVerifyCheck';
import arapLinkReport from '../../../../public/components/arapBillLinkReport.js';
import linkvouchar from '../../../../public/components/linkvouchar.js';
import madeBill from '../../../../public/components/madeBill.js';
import { copyBill } from '../../../../public/components/pubUtils/CopyBill/arapCopyBill';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';

let { getDefData, setDefData, updateCache, deleteCacheById } = cardCache;

export default function(props, id) {
	let code = getContext(loginContextKeys.transtype);
	switch (id) {
		case headButton.Commit: //提交
			this.Info.tipUrl = '/nccloud/arap/arappub/commit.do';
			this.commitAndUncommit();
			break;
		case headButton.Uncommit: //收回
			this.Info.tipUrl = '/nccloud/arap/arappub/uncommit.do';
			this.commitAndUncommit();
			break;
		case headButton.LinkAprv: //审批详情
			this.setState({ showApproveDetail: true });
			break;
		case headButton.Save: //保存
			this.saveBill('/nccloud/arap/arappub/save.do');
			break;
		case headButton.TempSave: //暂存
			this.saveBill('/nccloud/arap/arappub/tempsave.do');
			break;
		case headButton.SaveAndCommit:
			this.saveBill('/nccloud/arap/arappub/saveandcommit.do');
			break;
		case headButton.Add:
			//卡片新增时，将浏览态的单据主键缓存，点击取消时重新查询该主键单据
			let pagecode = getDefData('sessionTradeType', dataSource);
			if (code) {
				pagecode = code;
			} else if (!pagecode) {
				pagecode = this.getPagecode();
			}
			//缓存中的交易类型和链接中交易类型不一样的话，重新加载模板
			if (pagecode != this.getPagecode()) {
				props.setUrlParam({ status: 'add', pagecode: pagecode });
				initTemplate.call(this, this.props);
			} else {
				props.setUrlParam({ status: 'add', pagecode: pagecode });
			}
			this.initAdd();
			break;
		case headButton.Edit:
			let sceneType = 0;
			let scene = props.getUrlParam('scene');
			//获取单据编号
			let djbh = this.props.form.getFormItemsValue(this.formId, 'billno').value;
			let canEdit = true;
			//来源于审批中心
			if (scene == 'approve' || scene == 'approvesce') {
				sceneType = 1;
				//判断单据是否是当前用户待审批
				ajax({
					url: '/nccloud/riart/message/list.do',
					async: false,
					data: {
						billno: djbh,
						isread: 'N'
					},
					success: (result) => {
						if (result.data) {
							if (result.data.total < 1) {
								toast({
									content: this.state.json['payablebill-000041'],
									color: 'warning'
								}); /* 国际化处理： 当前单据已审批，不可进行修改操作!*/
								canEdit = false;
							}
						}
					}
				});
			}
			//来源于我的作业
			if (scene == 'zycl') {
				sceneType = 2;
				//判断单据是否是当前用户待处理
				ajax({
					url: '/nccloud/ssctp/sscbd/SSCTaskHandlePendingNumAction.do',
					async: false,
					data: {
						billno: djbh
					},
					success: (result) => {
						if (result.data) {
							if (result.data.total < 1) {
								toast({
									content: this.state.json['payablebill-000042'],
									color: 'warning'
								}); /* 国际化处理： 当前单据已处理，不可进行修改操作!*/
								canEdit = false;
							}
						}
					}
				});
			}
			if (!canEdit) {
				return;
			}
			let editData = {
				pk_bill: this.props.getUrlParam('id'),
				billType: this.billType,
				sence: sceneType
			};
			ajax({
				url: '/nccloud/arap/arappub/edit.do',
				data: editData,
				success: (res) => {
					if (res.success) {
						props.setUrlParam({ status: 'edit' });
						this.props.cardTable.setStatus(this.tableId, 'edit');
						this.props.form.setFormStatus(this.formId, 'edit');
						this.state.buttonfalg = true;
						this.props.resMetaAfterPkorgEdit();
						this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
						this.toggleShow();
					}
				}
			});
			break;
		case headButton.Copy:
			copyBill(this, this.getPagecode());
			// ajax({
			// 	url: '/nccloud/arap/arappub/copy.do',
			// 	data: {
			// 		pk_bill: props.getUrlParam('id'),
			// 		ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
			// 		pageId: this.getPagecode(),
			// 		billType: this.billType,
			// 		tradeType: this.getPagecode()
			// 	},
			// 	success: (res) => {
			// 		if (res.data) {
			// 			this.props.beforeUpdatePage(); //打开开关
			// 			if (res.data.head) {
			// 				this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
			// 			}
			// 			if (res.data.body) {
			// 				this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
			// 			}
			// 		}
			// 		props.setUrlParam({ status: 'add', type: 'copy' });
			// 		this.state.buttonfalg = true;
			// 		this.props.resMetaAfterPkorgEdit();
			// 		this.props.form.setFormItemsDisabled(this.formId, { pk_org: false });
			// 		this.props.cardTable.setStatus(this.tableId, 'edit');
			// 		this.props.form.setFormStatus(this.formId, 'edit');
			// 		this.props.updatePage(this.formId, this.tableId); //关闭开关
			// 		this.toggleShow();
			// 	}
			// });
			break;

		case headButton.Delete:
			promptBox({
				color: 'warning',
				title: this.state.json['payablebill-000006'] /* 国际化处理： 删除*/,
				content: this.state.json['payablebill-000010'] /* 国际化处理： ​确定要删除吗？*/,
				noFooter: false,
				noCancelBtn: false,
				beSureBtnName: this.state.json['payablebill-000008'] /* 国际化处理： 确定*/,
				cancelBtnName: this.state.json['payablebill-000009'] /* 国际化处理： 取消*/,
				beSureBtnClick: this.delConfirm
			});
			break;
		case headButton.Cancel:
			promptBox({
				color: 'warning',
				title: this.state.json['payablebill-000009'] /* 国际化处理： 取消*/,
				content: this.state.json['payablebill-000007'] /* 国际化处理： ​确定要取消吗？*/,
				noFooter: false,
				noCancelBtn: false,
				beSureBtnName: this.state.json['payablebill-000008'] /* 国际化处理： 确定*/,
				cancelBtnName: this.state.json['payablebill-000009'] /* 国际化处理： 取消*/,
				beSureBtnClick: this.cancel
			});
			break;
		case headButton.Pausetrans: //挂起
			this.pause('/nccloud/arap/arappub/pause.do');
			break;
		case headButton.Cancelpause: //取消挂起
			this.pause('/nccloud/arap/arappub/cancelpause.do');
			break;

		case headButton.PrePay: //预收付
			let prePayDatas = this.props.cardTable.getCheckedRows(this.tableId);
			if (prePayDatas.length == 0) {
				toast({ color: 'success', content: this.state.json['payablebill-000043'] }); /* 国际化处理： 请选择至少一条表体行!*/
				return;
			}
			let prePayDatasObj = [];
			prePayDatas.forEach((val) => {
				prePayDatasObj.push(val.data.values.pk_payitem.value);
			});
			let prePayDatasdata = {
				pk_items: prePayDatasObj,
				pk_bill: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
				pageId: this.getPagecode(),
				billType: this.billType
			};
			ajax({
				url: '/nccloud/arap/arappub/prepay.do',
				data: prePayDatasdata,
				success: (res) => {
					toast({ color: 'success', content: this.state.json['payablebill-000044'] }); /* 国际化处理： 预收付成功*/
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}
					let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
					updateCache(pkname, this.props.getUrlParam('id'), newCardData, this.formId, dataSource);
					this.onSelected();
				}
			});
			break;
		case headButton.RedBack: //红冲操作
			let writebackData = {
				pk_bill: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
				pageId: this.getPagecode(),
				billType: this.billType,
				tradeType: this.getPagecode()
			};
			ajax({
				url: '/nccloud/arap/arappub/redback.do',
				data: writebackData,
				success: (res) => {
					this.props.beforeUpdatePage(); //打开开关
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
					}
					props.setUrlParam({ status: 'add', id: this.props.getUrlParam('id'), type: 'redBack' });
					this.state.buttonfalg = true;
					this.props.cardTable.setStatus(this.tableId, 'edit');
					this.props.form.setFormStatus(this.formId, 'edit');
					this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
					this.props.updatePage(this.formId, this.tableId); //关闭开关
					this.toggleShow();
					this.props.button.setButtonVisible([ headButton.TempSave ], false);
				}
			});
			break;

		case headButton.BillLinkQuery: //单据追溯
			this.setState({ showBillTrack: true });
			break;
		case headButton.LinkConfer: //联查协同单据
			ajax({
				url: '/nccloud/arap/arappub/linkconfer.do',
				async: false,
				data: {
					pk_bill: this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
					billType: this.billType
				},
				success: (res) => {
					let data = res.data;
					if (data) {
						props.openTo(data.url, data.condition);
					}
				}
			});
			break;
		case headButton.LinkTbb: //联查计划预算
			ajax({
				url: '/nccloud/arap/arappub/linktbb.do',
				data: {
					pk_bill: this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
					billType: this.billType
				},
				success: (res) => {
					if (res.success) {
						this.Info.tbbLinkSourceData = res.data;
						this.setState({
							isTbbLinkshow: true
						});
					}
				}
			});
			break;
		case headButton.LinkBal: //联查余额表
			arapLinkReport(
				this.props,
				this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
				this.billType,
				this.props.form.getFormItemsValue(this.formId, 'objtype').value
			);
			break;
		case headButton.LinkTerm: //联查收付款协议
			let selectedData = this.props.cardTable.getCheckedRows(this.tableId);
			let seletedPks = [];
			if (selectedData.length != 0) {
				selectedData.forEach((val) => {
					seletedPks.push(val.data.values.pk_payableitem.value);
				});
			}
			ajax({
				url: '/nccloud/arap/arappub/linkterm.do',
				data: {
					pk_bill: this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
					billType: this.billType,
					pk_items: seletedPks,
					moduleId: '2008'
				},
				success: (res) => {
					if (res.success) {
						//打开收付款协议模态框
						let data = res.data;
						this.Info.selectedPKS = seletedPks;
						this.Info.linkTermData = data;
						this.handleLinkTerm();
					}
				}
			});
			break;
		case headButton.LinkDeal: //联查处理情况
			ajax({
				url: '/nccloud/arap/arappub/linkdeal.do',
				data: {
					pk_bill: this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
					billType: this.billType
				},
				success: (res) => {
					if (res.success) {
						//打开处理情况模态框
						let data = res.data;
						this.Info.combinedExaminationData = data;
						this.handleCombined();
					}
				}
			});
			break;
		case headButton.ReceiptCheck: //影像查看pk_tradetype
			if (props.getUrlParam('status') == 'add') {
				toast({ color: 'warning', content: this.state.json['payablebill-000045'] }); /* 国际化处理： 单据未暂存！*/
				return;
			}
			var billInfoMap = {};

			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value;
			billInfoMap.pk_billtype = this.props.form.getFormItemsValue(this.formId, 'pk_billtype').value;
			billInfoMap.pk_tradetype = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
			billInfoMap.pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
			imageView(billInfoMap, 'iweb');

			break;
		case headButton.ReceiptScan: //影像扫描
			if (props.getUrlParam('status') == 'add') {
				toast({
					color: 'warning',
					content: this.state.json['payablebill-000046']
				}); /* 国际化处理： 请先 <暂存> 单据再扫描影像！*/
				return;
			}
			let tradeType = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
			let allData = props.createMasterChildData(tradeType, formId, tableId);
			// let openbillid = props.getUrlParam('id');
			// imageScan(allData, openbillid, tradetype, 'iweb');

			var billInfoMap = {};
			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = allData.head.head.rows[0].values.pk_payablebill.value;
			billInfoMap.pk_billtype = allData.head.head.rows[0].values.pk_billtype.value;
			billInfoMap.pk_tradetype = allData.head.head.rows[0].values.pk_tradetype.value;
			billInfoMap.pk_org = allData.head.head.rows[0].values.pk_org.value;

			//影像所需 FieldMap
			billInfoMap.BillType = allData.head.head.rows[0].values.pk_tradetype.value;
			billInfoMap.BillDate = allData.head.head.rows[0].values.creationtime.value;
			billInfoMap.Busi_Serial_No = allData.head.head.rows[0].values.pk_payablebill.value;
			billInfoMap.pk_billtype = allData.head.head.rows[0].values.pk_billtype.value;
			billInfoMap.OrgNo = allData.head.head.rows[0].values.pk_org.value;
			billInfoMap.BillCode = allData.head.head.rows[0].values.billno.value;
			billInfoMap.OrgName = allData.head.head.rows[0].values.pk_org.display;
			billInfoMap.Cash = allData.head.head.rows[0].values.money.value;

			imageScan(billInfoMap, 'iweb');

			break;
		case headButton.Print: //打印
			this.onPrint();
			break;
		case headButton.Output: //输出...
			this.printOutput();
			break;
		case headButton.OfficalPrint: //正式打印
			this.officalPrintOutput();
			break;
		case headButton.CancelPrint: //取消正式打印
			this.cancelPrintOutput();
			break;
		case headButton.AttachManage:
			let flag = props.getUrlParam('status');
			if (
				flag == 'add' ||
				props.getUrlParam('copyFlag') == 'copy' ||
				props.getUrlParam('writebackFlag') == 'redBack'
			) {
				toast({ color: 'warning', content: this.state.json['payablebill-000027'] }); /* 国际化处理： 请保存后再进行上传附件！*/
				return;
			}
			this.setState({
				showUploader: true,
				target: null
			});
			break;
		case headButton.ExportData: //导出
			let outbillid = props.getUrlParam('id');
			let pk_bills = [];
			pk_bills.push(outbillid);
			this.Info.selectedPKS = pk_bills; //传递主键数组,之前nc需要导出的加主键
			this.props.modal.show('exportFileModal'); //不需要导出的只执行这行代码
			break;
		case headButton.MadeBill: //制单
			let madeData = [
				{
					pk_bill: props.form.getFormItemsValue(formId, 'pk_payablebill').value,
					billType: this.billType,
					tradeType: props.form.getFormItemsValue(formId, 'pk_tradetype').value
				}
			];
			madeBill(this, props, madeData, '', props.getSearchParam('c'));
			break;
		case headButton.LinkVouchar: //联查凭证
			linkvouchar(
				this,
				props,
				this.props.form.getAllFormValue(this.formId),
				this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,
				this.props.getSearchParam('c')
			);
			break;
		case headButton.BodyVerify: //按表体核销
			BodyVerify(props, this.formId, this.tableId, 'pk_payablebill', 'pk_payableitem', this);
			break;
		case headButton.WholeVerify: //按整单核销
			WholeVerify(props, this.formId, this.tableId, 'pk_payablebill', this);
			break;
		case headButton.Refresh:
			let pk = this.props.getUrlParam('id');
			ajax({
				url: '/nccloud/arap/arappub/cardRefresh.do',
				data: {
					pk_bill: pk,
					pageId: this.getPagecode(),
					billType: this.billType
				},
				success: (res) => {
					if (res.data) {
						toast({ color: 'success', title: this.state.json['payablebill-000062'] }); /* 国际化处理： 刷新成功*/
						updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
						this.getfpxx(pk);
					} else {
						this.props.form.EmptyAllFormValue(this.formId);
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
					}
					this.toggleShow();
				},
				error: (res) => {
					this.props.form.EmptyAllFormValue(this.formId);
					this.props.cardTable.setTableData(this.tableId, { rows: [] });
					deleteCacheById(pkname, this.props.getUrlParam('id'), dataSource);
					this.toggleShow();
					let str = String(res);
					let content = str.substring(6, str.length);
					toast({ color: 'danger', content: content });
				}
			});
			break;
		case headButton.LinkInvoice: //发票查看
			invoiceLink(this);
			break;
		case headButton.InvoiceUploader: //上传电子发票
			ajax({
				url: '/nccloud/arap/arappub/queryrelatedapp.do',
				data: {
					pk_org: 'GLOBLE00000000000000',
					funCode: 'IV0001'
				},
				success: (res) => {
					if (res.success) {
						if (res.data == '2') {
							const billId = props.getUrlParam('id');
							const billCode = this.props.form.getFormItemsValue(this.formId, 'billno').value;
							const pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
							const tradetype = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
							const random = Math.random();

							this.setState({
								sscrpInvoiceData: { billId, billCode, pk_org, tradetype, random }
							});
						} else {
							toast({
								color: 'warning',
								content: this.state.json['payablebill-000065']
							}); /* 国际化处理： 没有使用税务管理系统，该功能无法使用！*/
						}
					}
				}
			});

			break;
		//表体肩部的按钮操作
		case bodyButton.AddLine:
			if (this.props.form.getFormItemsValue(this.formId, 'pk_org').value != null) {
				let rowNum = props.cardTable.getNumberOfRows(this.tableId);
				ajax({
					url: '/nccloud/arap/payablebillpub/addline.do',
					data: this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId),
					success: (res) => {
						if (res.data) {
							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							}
							if (res.data.body) {
								this.props.cardTable.addRow(
									this.tableId,
									rowNum,
									res.data.body[this.tableId].rows[0].values
								);
							}
						}
					}
				});
			}
			break;
		case bodyButton.DelLine: //删除行
			delLine(this);
			//删行之后控制按钮
			this.onSelected();
			break;
		case bodyButton.CopyLine: //复制行
			if (copyLine(this, dataSource)) {
				this.setState(
					{
						buttonfalg: false
					},
					() => {
						cardBodyControl(props, this.state.buttonfalg);
					}
				);
			}
			break;
		case bodyButton.PasteLine: //粘贴行
			pasteLine(this);
			break;
		case bodyButton.PasteToEndLine: //粘贴行到末尾
			pasteToEndLine(this, dataSource);
			break;
		case bodyButton.CancelLine: //行取消
			this.setState(
				{
					buttonfalg: true
				},
				() => {
					cardBodyControl(props, this.state.buttonfalg);
				}
			);
			//取消之后控制肩部按钮
			this.onSelected();
			break;
			case headButton.goto_link: //采购结算应付单 联查明源
					let a=props.form.getFormItemsValue(this.formId, 'pk_payablebill').value
					let b=props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
					ajax({
						url:'/nccloud/nc/myerp/erpurl.do',
						data:{pk_bill:a ,tradeType:b }, 
						success:(result)=>{
							if(result.data.code==1){
							var arr=result.data.url
							    if(arr){
								        window.open(arr)
							    };
							}else{
								toast({color:'danger',title:result.data.msg});
							}
						},
						error:(result)=>{
						toast({color:'danger',title:result.message});
						}
					});
			break;
		case "acquisition":
			ajax({
				url:'/nccloud/nc/myerp/costaccrualamont.do',
				data:{pk_payablebill:props.form.getFormItemsValue(this.formId, 'pk_payablebill').value,pk_tradetype:props.form.getFormItemsValue(this.formId, 'pk_tradetype').value}, 
				success:(result)=>{
					toast({color:'success',title:'数据获取成功'});
					
				},
				error:(result)=>{
				toast({color:'danger',title:result.message});
				}
			});
			break;
		default:
			let transferInfo = getTransferInfo(this, id);
			if (transferInfo) {
				ajax({
					url: '/nccloud/arap/arappub/queryrelatedapp.do',
					data: {
						billType: transferInfo.src_billtype
					},
					success: (res) => {
						if (res) {
							//将业务流程放入缓存
							setDefData(
								transferInfo.src_billtype + transferInfo.transtypes[0],
								'transfer.dataSource',
								transferInfo.busitypes
							);
							let dest_tradetype = getDefData('sessionTradeType', dataSource);
							if (code) {
								dest_tradetype = code;
							} else if (!dest_tradetype) {
								dest_tradetype = this.getPagecode();
							}
							let url = '/' + transferInfo.src_billtype;
							props.pushTo(url, {
								src_appcode: res.data.appcode,
								src_tradetype: transferInfo.transtypes[0], //来源交易类型
								dest_billtype: this.billType,
								dest_tradetype: dest_tradetype
							});
						}
					}
				});
			}
			break;
	}
}
//联查发票
let invoiceLink = function(that) {
	const billId = that.props.getUrlParam('id');
	const billCode = that.props.form.getFormItemsValue(that.formId, 'billno').value;
	const pk_org = that.props.form.getFormItemsValue(that.formId, 'pk_org').value;
	const tradetype = that.props.form.getFormItemsValue(that.formId, 'pk_tradetype').value;
	const viewRandom = Math.random();

	that.setState({
		sscrpInvoiceData: { billId, billCode, pk_org, tradetype, viewRandom }
	});
};
