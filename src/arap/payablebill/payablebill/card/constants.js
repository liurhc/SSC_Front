
/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 表体区域
 */
export const tableId = 'bodys';

/**
 * 单据类型
 */
export const billType = 'F1';

export const tradeType = 'D1';

/**
 * 转单左侧区域
 */
export const leftarea = 'left';

export const headId = 'headId';

/**
 * 默认模板节点标识
 */
export const nodekey = "card";

/**
 * 单页应用缓存
 */
export const dataSource  = 'fi.arap.payablebill.20080PBM';

/**
 * 单页应用缓存主键名字(该名字为表的主键名字)
 */
export const pkname  = 'pk_payablebill';

/**
 * 列表联查页面pageId
 */
export const linkPageId = '20080PBM_LIST_LINK';

