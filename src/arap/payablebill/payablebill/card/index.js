//主子表卡片
import { ajax, base, createPage, high, toast, print, cardCache, getMultiLang,createPageIcon } from 'nc-lightapp-front';
import React, { Component } from 'react';
import CombinedExaminationModel from '../../../public/components/combinedExaminationModel'; //联查处理情况
import LinkTerm from '../../../public/components/linkTerm'; //联查收付款协议
import {
	buttonVisible,
	getButtonsKey,
	dealTranferBtns,
	initCardBodyEditControl,
	onSelectedCardBodyEditControl,
	initCardBodybrowseControl
} from '../../../public/components/pubUtils/buttonvisible.js';
import { billType, formId, leftarea, tableId, nodekey, tradeType, dataSource, pkname, linkPageId } from './constants';
import { afterEvent, buttonClick, initTemplate, pageInfoClick, transferButtonClick } from './events';
import { setValue } from './events/transferButtonClick';
import { bodyBeforeEvent } from '../../../public/components/pubUtils/arapTableRefFilter';
import { formBeforeEvent } from '../../../public/components/pubUtils/arapFormRefFilter';
import linkSourceCard from '../../../public/components/linkSourceCard.js';
import cmpLinkArapCard from '../../../public/components/cmpLinkArapCard.js';
import { calculateHeadMoney, delBlankLine } from '../../../public/components/pubUtils/billPubUtil.js';
import { updatePandC } from '../../../public/components/pubUtils/updatePandC';
let { NCFormControl, NCButton, NCBackBtn,NCSelect } = base;
const NCOption = NCSelect.NCOption;
const { BillTrack, PrintOutput, NCUploader, Refer, Inspection, ApproveDetail } = high;
var arrList=[];
let {
	setDefData,
	getDefData,
	addCache,
	getNextId,
	deleteCacheById,
	getCacheById,
	updateCache,
	getCurrentLastId
} = cardCache;
import { loginContext, getContext, loginContextKeys } from '../../../public/components/arapInitInfo/loginContext';
import { dealCardData } from '../../../public/components/pubUtils/dealCardData';
import TradeTypeButton from '../../../public/components/tradetype'; //交易类型按钮组件
import '../../../public/less/tradetype.less';
import InvoiceUploader from 'sscrp/rppub/components/invoice-uploader';
import InvoiceLink from 'sscrp/rppub/components/invoice-link';
const { ExcelImport, ApprovalTrans } = high;

class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isCombinedExaminationModelShow: false, //联查处理情况模态框
			isLinkTermModelShow: false, //联查收付款协议模态框
			isTbbLinkshow: false, //控制联查执行预算的模态框
			showBillTrack: false, //单据追溯
			showUploader: false, //附件管理
			target: null, //附件管理
			buttonfalg: null, //卡片态点击肩部按钮和表体行按钮改变该值控制按钮状态
			showApproveDetail: false, //审批详情
			compositedisplay: false, //指派信息弹框
			json: {},
			sscrpInvoiceData: {}, //电子发票上传
			ifChange:false
		};
		this.printData = {
			billtype: billType, //单据类型
			appcode: props.getSearchParam('c'),
			nodekey: props.getSearchParam('p'), //模板节点标识  =交易类型
			funcode: props.getSearchParam('c'),
			oids: [ this.props.getUrlParam('id') ], // 功能节点的数据主键
			userjson: billType //单据类型
		};
		this.outputData = {
			billtype: billType, //单据类型
			appcode: props.getSearchParam('c'),
			funcode: props.getSearchParam('c'),
			nodekey: props.getSearchParam('p'), //模板节点标识  =交易类型
			oids: [ this.props.getUrlParam('id') ], // 功能节点的数据主键
			userjson: billType, //单据类型
			outputType: 'output'
		};
		this.formId = formId;
		this.tableId = tableId;
		this.billType = billType;
		this.leftarea = leftarea; //转单使用
		this.transferIndex = 0; //转单时左侧选择的数据序号
		this.Info = {
			//保存全局变量，不涉及页面刷新的全局变量都保存在这里
			allButtonsKey: [], //保存所有的表体头部按钮
			linkTermData: [], //联查收付款协议模态框表格数据
			combinedExaminationData: [], //联查处理情况模态框表格数据
			tbbLinkSourceData: null, //联查执行预算的数据
			billCode: '', // 发票号
			selectedPKS: [], //导出数据的主键pk,收付款协议
			pullBillInfoVOAry: [], //拉单使用
			tipContent: null, //提示框Content
			tipUrl: null, //提示框二次交互的url
			exType: null, //异常类型，现为三种（1,2,3）
			flag: false, //提交收回异常交互参数值，默认为false
			pk_bill: null, //提示框二次交互时后台传入前台主键
			ts: null,
			billCard: null, //保存提交后，返回的保存单据
			compositedata: null, //指派信息数据
			isModelSave: false //是否是整单保存，默认为false
		};
	}

	//关闭、刷新弹窗时
	componentWillMount() {
		if (this.props.getSearchParam('c') == '20082002' && this.props.getSearchParam('scene') != 'approvesce') {
			updatePandC(this);
		}
		let callback = (json) => {
			this.setState({ json: json }, () => {
				window.onbeforeunload = () => {
					let status = this.props.getUrlParam('status');
					if (status == 'edit' || status == 'add') {
						return ''; /* 国际化处理： 确定要离开吗？*/
					}
				};
				initTemplate.call(this, this.props, this.initShow);
			});
		};
		getMultiLang({ moduleId: [ 'payablebill', 'public' ], domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {
		if (this.props.getUrlParam('scene')) {
			let scene = this.props.getUrlParam('scene');
			setDefData('scene', dataSource, scene);
			let pagecode = this.getPagecode();
			setDefData('pagecode', dataSource, pagecode);
		}
	}

	componentWillReceiveProps(nextProps) {}
	beforeUpload(billId, fullPath, file, fileList) {
		// 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表
		const isJPG = true;
		if (!isJPG) {
			alert(this.state.json['payablebill-000028']); /* 国际化处理： 只支持png格式图片*/
		}
		const isLt2M = file.size / 1024 / 1024 < 10;
		if (!isLt2M) {
			alert(this.state.json['payablebill-000029']); /* 国际化处理： 上传大小小于10M*/
		}
		return isJPG && isLt2M;
		// 备注： return false 不执行上传  return true 执行上传
	}
	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		});
	};

	getPagecode = () => {
		let pagecode = this.props.getUrlParam('pagecode');
		if (!pagecode) {
			pagecode = this.props.getSearchParam('p');
		}
		if (pagecode == '20080PBM_CARD_LINK' && !this.props.getUrlParam('scene')) {
			//浏览器的刷新可能导致场景丢失，这里自己塞一次
			this.props.ViewModel.setData('nccloud-router-params', { scene: 'linksce' });
		}
		return pagecode;
	};
	getExportPageCode = () => {
		let pagecode = "20080PBM_CARD";
		if(this.props.getUrlParam('scene')){
			pagecode = "20080PBM_CARD_LINK";
		}
		if (pagecode == '20080PBM_CARD_LINK' && !this.props.getUrlParam('scene')) {
			//浏览器的刷新可能导致场景丢失，这里自己塞一次
			this.props.ViewModel.setData('nccloud-router-params', { scene: 'linksce' });
		}
		return pagecode;

	}

	//页面初始化
	initShow = () => {
		let transfer = this.props.getUrlParam('type') === 'transfer';
		if (transfer && this.props.getUrlParam('status') != 'browse') {
			let transferIds = this.props.transferTable.getTransferTableSelectedId();
			this.getTransferValue(transferIds);
		} else if (this.props.getUrlParam('scene') && this.props.getUrlParam('scene') == 'fip') {
			linkSourceCard(
				this.props,
				linkPageId,
				'pk_payablebill',
				this.formId,
				this.tableId,
				pkname,
				dataSource,
				this
			);
		} else if (
			this.props.getUrlParam('scene') &&
			this.props.getUrlParam('scene') == 'linksce' &&
			this.props.getUrlParam('flag') == 'ftsLinkArap'
		) {
			cmpLinkArapCard(
				this.props,
				this.billType,
				'pk_payablebill',
				this.formId,
				this.tableId,
				pkname,
				dataSource,
				this
			);
		} else {
			if (this.props.getUrlParam('type') === 'copy') {
				//复制的情况
				ajax({
					url: '/nccloud/arap/arappub/copy.do',
					data: {
						pk_bill: this.props.getUrlParam('id'),
						pageId: this.getPagecode(),
						billType: this.billType,
						type: 1,
						tradeType: this.getPagecode()
					},
					success: (res) => {
						this.props.beforeUpdatePage(); //打开开关
						if (res.data) {
							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							}
							if (res.data.body) {
								this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
							}
						}
						this.props.resMetaAfterPkorgEdit();
						this.state.buttonfalg = true;
						this.props.cardTable.setStatus(this.tableId, 'edit');
						this.props.form.setFormStatus(this.formId, 'edit');
						this.props.updatePage(this.formId, this.tableId); //关闭开关
						this.toggleShow();
					},
					error: (res) => {
						this.props.form.EmptyAllFormValue(this.formId);
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
						this.props.setUrlParam({ status: 'browse' });
						this.props.cardTable.setStatus(this.tableId, 'browse');
						this.props.form.setFormStatus(this.formId, 'browse');
						this.toggleShow();
						let str = String(res);
						let content = str.substring(6, str.length);
						toast({ color: 'danger', content: content });
					}
				});
			} else if (this.props.getUrlParam('status') == 'edit' || this.props.getUrlParam('status') == 'browse') {
				let pk_bill = this.props.getUrlParam('id');
				if (!pk_bill) {
					this.toggleShow();
					return;
				}
				ajax({
					url: '/nccloud/arap/payablebill/querycard.do',
					data: { pk_bill: pk_bill },
					success: (res) => {
						this.props.beforeUpdatePage(); //打开开关
						if (res.data) {
							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							}
							if (res.data.body) {
								this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
							}
						}
						if (this.props.getUrlParam('status') == 'edit') {
							this.props.resMetaAfterPkorgEdit();
							this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
							this.state.buttonfalg = true;
						} else {
							this.state.buttonfalg = null;
						}
						if (!this.props.getUrlParam('scene')) {
							this.props.setUrlParam({
								pagecode: res.data.head[this.formId].rows[0].values.pk_tradetype.value
							});
						}
						let status = this.props.getUrlParam('status');
						this.props.cardTable.setStatus(this.tableId, status);
						this.props.form.setFormStatus(this.formId, status);
						this.props.updatePage(this.formId, this.tableId); //关闭开关
						updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
						this.toggleShow();
					}
				});
				this.getfpxx(pk_bill);
			} else if (this.props.getUrlParam('status') == 'add') {
				//状态为add时请求显示默认值
				this.initAdd();
			} else {
				this.toggleShow();
			}
		}
	};

	// 请求发票action
	getfpxx = (pk_bill) => {
		ajax({
			url: '/nccloud/arap/payablebill/querycardfpxx.do',
			data: { pk_bill: pk_bill },
			success: (res) => {
				this.props.beforeUpdatePage(); //打开开关
				if (res.data) {
					this.props.cardTable.setTableData('fpxx', res.data);
				}else{
					this.props.cardTable.setTableData('fpxx', {rows:[]});
				}
				this.props.updatePage(this.formId, 'fpxx'); //关闭开关
			}
		});
	}

	//整单保存事件
	modelSaveClick = (a, modelIndex) => {
		this.Info.isModelSave = true;
		let saveUrl = '/nccloud/arap/arappub/save.do';
		this.saveBill(saveUrl, null, null, modelIndex);
	};
	//查询转单数据
	getTransferValue = (ids) => {
		if (ids) {
			let data = {
				data: ids,
				pageId: this.getPagecode(),
				destTradetype: this.getPagecode(),
				srcBilltype: this.props.getUrlParam('srcBilltype')
			};
			ajax({
				url: '/nccloud/arap/arappub/transfer.do',
				data: data,
				success: (res) => {
					if (res && res.data) {
						this.props.transferTable.setTransferListValue(this.leftarea, res.data);
					}
					// this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
					this.toggleShow();
				}
			});
		} else {
			this.props.transferTable.setTransferListValue(this.leftarea, []);
		}
	};

	//同步单据信息到转单控件
	synTransferData = () => {
		//重取界面现有数据赋值到转单中
		let amount = this.props.transferTable.getTransformFormAmount(this.leftarea);
		if (amount != 1) {
			let cardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
			this.props.transferTable.setTransferListValueByIndex(this.leftarea, cardData, this.transferIndex);
		}
	};

	//新增时初始化
	initAdd = (isClearPKorg) => {
		if (!isClearPKorg) {
			//isClearPKorg如果未未定义等，就取值为false
			isClearPKorg = false;
		}
		ajax({
			url: '/nccloud/arap/payablebill/defvalue.do',
			data: {
				isClearPKorg: isClearPKorg,
				appcode: this.props.getSearchParam('c'),
				pageId: this.getPagecode(),
				tradeType: this.getPagecode()
			},
			success: (res) => {
				if (res.data) {
					this.props.beforeUpdatePage(); //打开开关
					this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
					let pk_org = res.data.head[this.formId].rows[0].values.pk_org.value;
					if (pk_org) {
						this.props.resMetaAfterPkorgEdit();
						this.props.form.setFormItemsDisabled(this.formId, { pk_org: false });
						this.state.buttonfalg = true;
					} else {
						this.state.buttonfalg = null;
						if (!this.props.getUrlParam('type')) {
							this.props.initMetaByPkorg();
						}
					}
					this.props.form.setFormStatus(this.formId, 'edit');
					this.props.cardTable.setStatus(this.tableId, 'edit');
					this.props.updatePage(this.formId, this.tableId); //关闭开关
				}
				this.toggleShow();
			},
			error: (res) => {//主要控制列表进卡片抛出错误
				let str = String(res);
				let content = str.substring(6, str.length);
				toast({ color: 'danger', content: content });
				let status = this.props.getUrlParam('status');
				if(status !='browse'){
					this.props.setUrlParam({ status: 'browse' });
					this.props.form.setFormStatus(this.formId, 'browse');
					this.props.cardTable.setStatus(this.tableId, 'browse');
					this.toggleShow();
				}
			}
		});
	};

	//切换页面状态
	toggleShow = (headData) => {
		let status = this.props.getUrlParam('status');
		if (!status) {
			status = 'browse';
		} //如果未定义，默认给值浏览态
		let cardhead = {};
		if (headData) {
			cardhead = headData.rows[0].values;
		} else {
			let head = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId).head[
				this.formId
			];
			if (head) {
				cardhead = head.rows[0].values;
			}
		}
		if (status != 'browse') {
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: false });
			if (this.refs.tradetypeBtn && !getContext(loginContextKeys.transtype)) {
				this.refs.tradetypeBtn.setVisible(false); //设置交易类型是否显示
			}
		} else {
			if (!(cardhead.pk_tradetype ? cardhead.pk_tradetype.value : null)) {
				this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
			} else {
				this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);
			}
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: true });
			if (this.refs.tradetypeBtn && !getContext(loginContextKeys.transtype)) {
				this.refs.tradetypeBtn.setVisible(true); //设置交易类型是否显示
			}
		}
		let trueBtn = []; //可见的按钮
		let falseBtn = []; //不可见的按钮
		for (let i = 0; i < this.Info.allButtonsKey.length; i++) {
			let flag = buttonVisible(status, cardhead, this.Info.allButtonsKey[i], 'card', this);
			if (flag) {
				trueBtn.push(this.Info.allButtonsKey[i]);
			} else {
				falseBtn.push(this.Info.allButtonsKey[i]);
			}
		}
		
		//处理转单不可见按钮
		dealTranferBtns.call(this, falseBtn);
		this.props.button.setButtonVisible(trueBtn, true);
		this.props.button.setButtonVisible(falseBtn, false);
		//初始化肩部按钮信息增行等按钮的控制
		if (status == 'browse') {
			initCardBodybrowseControl(this);
			if(this.props.form.getFormItemsValue(this.formId, 'approvestatus').value=='1'){
				this.props.button.setButtonDisabled('acquisition', true);
			}else{
				this.props.button.setButtonDisabled('acquisition', false);
			}
			this.props.button.setButtonVisible('acquisition', true);
		} else {
			initCardBodyEditControl(this.props, cardhead.pk_org ? cardhead.pk_org.value : null);
			this.props.button.setButtonVisible('acquisition', false);
		}
		//联查场景，默认场景，浏览态存在返回按钮
		if(this.props.getUrlParam('scene')&&this.props.getUrlParam('scene') != 'linksce' &&
			this.props.getUrlParam('scene') != 'fip' ){
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: false });
		}
		
	};

	//卡片表体点击行事件
	onSelected = () => {
		onSelectedCardBodyEditControl(this);
	};

	//删除单据
	delConfirm = (extype, flag) => {
		ajax({
			url: '/nccloud/arap/arappub/delete.do',
			data: [
				{
					pk_bill: this.props.getUrlParam('id'),
					ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
					billType: this.billType,
					extype: extype,
					flag: flag
				}
			],
			success: (res) => {
				//删除单据后，卡片界面显示该单据的下一个单据
				//如果是最后一个单据，删除后返回list界面
				if (res.success) {
					if (res.data.exType == '1') {
						this.Info.tipContent = res.data.message;
						this.Info.exType = '1';
						this.Info.flag = true;
						this.props.modal.show('deleteCheck');
						return;
					} else {
						toast({ color: 'success', content: this.state.json['payablebill-000013'] }); /* 国际化处理： 删除成功*/
						let id = this.props.getUrlParam('id');
						deleteCacheById(pkname, id, dataSource);
						let nextId = getNextId(id, dataSource);
						if (nextId) {
							if (this.props.getUrlParam('type') === 'transfer') {
								this.props.setUrlParam({ status: 'browse' });
							}
							this.props.setUrlParam({ id: nextId });
							this.initShow();
						} else {
							this.props.setUrlParam({ id: null });
							this.props.form.EmptyAllFormValue(this.formId);
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
							this.toggleShow();
						}
						this.clearExType();
					}
				}
			}
		});
	};

	cancel = () => {
		//取消按钮
		if (this.props.getUrlParam('type') && this.props.getUrlParam('type') != 'transfer') {
			//如果type存在就删除
			this.props.delUrlParam('type');
		}
		let pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org');
		if (!pk_org || !pk_org.value || pk_org.value == '') {
			this.props.resMetaAfterPkorgEdit();
		}
		this.props.beforeUpdatePage(); //打开开关
		this.state.buttonfalg = null;
		if (this.props.getUrlParam('status') === 'edit') {
			this.props.setUrlParam({ status: 'browse' });
			let id = this.props.getUrlParam('id');
			let cardData = getCacheById(id, dataSource);
			if (cardData) {
				this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
				this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
				this.props.cardTable.setStatus(this.tableId, 'browse');
				this.props.form.setFormStatus(this.formId, 'browse');
				this.props.updatePage(this.formId, this.tableId); //关闭开关
				this.toggleShow();
			} else {
				this.initShow();
			}
		} else if (this.props.getUrlParam('status') === 'add') {
			let id = this.props.getUrlParam('id');
			if (!id) {
				id = getCurrentLastId(dataSource);
			}
			if (id != undefined && (id != null) & (id != '')) {
				this.props.setUrlParam({ status: 'browse', id: id });
				let cardData = getCacheById(id, dataSource);
				if (cardData) {
					this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
					this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
					let pagecode = cardData.head[this.formId].rows[0].values.pk_tradetype.value;
					if (this.getPagecode() != pagecode && !this.props.getUrlParam('scene')) {
						this.props.setUrlParam({ pagecode: pagecode });
						initTemplate.call(this, this.props);
					}
					this.props.cardTable.setStatus(this.tableId, 'browse');
					this.props.form.setFormStatus(this.formId, 'browse');
					this.props.updatePage(this.formId, this.tableId); //关闭开关
					this.toggleShow();
				} else {
					this.initShow();
				}
			} else {
				this.props.form.EmptyAllFormValue(this.formId);
				this.props.cardTable.setTableData(this.tableId, { rows: [] });
				this.props.setUrlParam({ status: 'browse' });
				this.props.cardTable.setStatus(this.tableId, 'browse');
				this.props.form.setFormStatus(this.formId, 'browse');
				this.props.updatePage(this.formId, this.tableId); //关闭开关
				this.toggleShow();
			}
		}
	};

	//保存单据(请求地址、异常类型、boolean)
	saveBill = (url, extype, flag, modelIndex) => {
		if (url != '/nccloud/arap/arappub/tempsave.do') {
			//不等于暂存的时候开启检验
			//删除空白行
			let checkCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
			delBlankLine(this, this.tableId, this.billType, checkCardData, modelIndex);
			if (!this.props.form.isCheckNow(this.formId)) {
				//表单验证
				return;
			}
			if (!this.props.cardTable.checkTableRequired(this.tableId)) {
				//表格验证
				return;
			}
		}
		let cardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
		let newCardData = dealCardData(this, cardData); //去掉空值，减少压缩时间
		let datas = {
			cardData: newCardData,
			uiState: this.props.getUrlParam('status'),
			extype: extype,
			flag: flag
		};
		let callback = () => {
			ajax({
				url: url,
				data: datas,
				success: (res) => {
					let pk_payablebill = null;
					let pk_tradetype = null;
					if (res.success) {
						if (res.data.exType == '1') {
							this.Info.tipContent = res.data.message;
							this.Info.pk_bill = res.data.pk_bill;
							this.Info.ts = res.data.ts;
							this.Info.billCard = res.data.billCard;
							this.Info.tipUrl = res.data.pk_bill == null ? url : '/nccloud/arap/arappub/commit.do';
							this.Info.exType = '1';
							this.Info.flag = true;
							if (res.data.pk_bill == null) {
								this.props.modal.show('saveCheck');
							} else {
								this.commitAndUncommit();
							}
							// this.props.modal.show(res.data.pk_bill == null ? 'saveCheck' : 'commitAndUncommit');
							return;
						} else if (res.data.exType == '2') {
							this.Info.tipContent = res.data.message;
							this.Info.pk_bill = res.data.pk_bill;
							this.Info.ts = res.data.ts;
							this.Info.billCard = res.data.billCard;
							this.Info.tipUrl = res.data.pk_bill == null ? url : '/nccloud/arap/arappub/commit.do';
							this.Info.exType = '2';
							this.Info.flag = true;
							if (res.data.pk_bill == null) {
								this.props.modal.show('saveCheck');
							} else {
								this.commitAndUncommit();
							}
							// this.props.modal.show(res.data.pk_bill == null ? 'saveCheck' : 'commitAndUncommit');
							return;
						} else if (res.data.exType == '3') {
							//普通异常要捕获到并抛出错误信息，同时要将保存后的单据进行回写
							let content = res.data.message;
							toast({ color: 'danger', content: JSON.stringify(content) });
						} else if (
							res.data.assignInfo &&
							res.data.assignInfo.workflow &&
							(res.data.assignInfo.workflow == 'approveflow' ||
								res.data.assignInfo.workflow == 'workflow')
						) {
							this.Info.compositedata = res.data.assignInfo;
							this.Info.pk_bill = res.data.pk_bill;
							this.Info.ts = res.data.ts;
							this.Info.exType = res.data.exType;
							this.Info.billCard = res.data.billCard;
							this.Info.tipUrl = '/nccloud/arap/arappub/commit.do';
							this.setState({ compositedisplay: true });
							return;
						}
						if (res.data) {
							if (this.props.getUrlParam('type') == 'transfer') {
								setValue.call(this, this.props, res);
							} else {
								if (!res.data.message) {
									toast({
										color: 'success',
										content: this.state.json['payablebill-000014']
									}); /* 国际化处理： 保存成功*/
								}
								this.props.beforeUpdatePage(); //打开开关
								if (res.data.head && res.data.head[this.formId]) {
									this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
									pk_payablebill = res.data.head[this.formId].rows[0].values.pk_payablebill.value;
									pk_tradetype = res.data.head[this.formId].rows[0].values.pk_tradetype.value;
								}
								if (res.data.body && res.data.body[this.tableId]) {
									this.props.cardTable.updateDataByRowId(this.tableId, res.data.body[this.tableId]);
								}
								this.state.buttonfalg = null;
								this.props.cardTable.setStatus(this.tableId, 'browse');
								this.props.form.setFormStatus(this.formId, 'browse');
								this.props.updatePage(this.formId, this.tableId); //关闭开关

								let newCardData = this.props.createMasterChildData(
									this.getPagecode(),
									this.formId,
									this.tableId
								);
								if (this.props.getUrlParam('status') == 'add') {
									addCache(pk_payablebill, newCardData, this.formId, dataSource); //新增
								} else {
									updateCache(pkname, pk_payablebill, newCardData, this.formId, dataSource); //修改之后更新缓存
								}
								if (this.props.getUrlParam('type')) {
									this.props.delUrlParam('type');
								}
								if (this.props.getUrlParam('scene')) {
									this.props.setUrlParam({ status: 'browse', id: pk_payablebill });
								} else {
									this.props.setUrlParam({
										status: 'browse',
										id: pk_payablebill,
										pagecode: pk_tradetype
									});
								}
							}
						}
					}
					if (this.Info.isModelSave) {
						this.Info.isModelSave = false;
						this.props.cardTable.closeModel(this.tableId);
					}
					if (this.props.getUrlParam('type') != 'transfer') {
						this.toggleShow();
					}
					this.clearExType();
				}
			});
		};
		this.props.validateToSave(datas.cardData, callback, { table1: 'cardTable' }, 'card');
	};

	//异常弹框信息点击取消
	clearExType = () => {
		this.Info.tipContent = '';
		this.Info.pk_bill = null;
		this.Info.ts = null;
		this.Info.tipUrl = null;
		this.Info.exType = null;
		this.Info.flag = false;
		this.Info.compositedata = null;
	};

	cancelClick = () => {
		if (this.Info.pk_bill != null) {
			if (this.props.getUrlParam('type') && this.props.getUrlParam('type') != 'transfer') {
				this.props.delUrlParam('type');
			}
			this.props.beforeUpdatePage(); //打开开关
			this.state.buttonfalg = null;
			if (this.Info.billCard.head) {
				this.props.form.setAllFormValue({ [this.formId]: this.Info.billCard.head[this.formId] });
			}
			if (this.Info.billCard.body) {
				this.props.cardTable.updateDataByRowId(this.tableId, this.Info.billCard.body[this.tableId]);
			}
			if (this.props.getUrlParam('scene')) {
				this.props.setUrlParam({
					status: 'browse',
					id: this.Info.pk_bill == null ? this.props.getUrlParam('id') : this.Info.pk_bill
				});
			} else {
				this.props.setUrlParam({
					status: 'browse',
					id: this.Info.pk_bill == null ? this.props.getUrlParam('id') : this.Info.pk_bill,
					pagecode: this.Info.billCard.head[this.formId].rows[0].values.pk_tradetype.value
				});
			}
			this.props.cardTable.setStatus(this.tableId, 'browse');
			this.props.form.setFormStatus(this.formId, 'browse');
			this.props.updatePage(this.formId, this.tableId); //关闭开关
			let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
			addCache(pkname, newCardData, this.formId, dataSource); //新增
			this.toggleShow();
		}

		if (this.state.compositedisplay) {
			this.setState({ compositedisplay: false });
		}
		this.clearExType();
	};

	//提交and收回公共调用函数
	commitAndUncommit = () => {
		let tipUrl = this.Info.tipUrl;
		let extype = this.Info.exType;
		let flag = this.Info.flag;
		ajax({
			url: tipUrl,
			data: {
				pk_bill: this.Info.pk_bill == null ? this.props.getUrlParam('id') : this.Info.pk_bill,
				ts: this.Info.ts == null ? this.props.form.getFormItemsValue(this.formId, 'ts').value : this.Info.ts,
				pageId: this.getPagecode(),
				billType: this.billType,
				type: 2,
				extype: extype,
				flag: flag,
				assignObj: this.Info.compositedata
			},
			success: (res) => {
				if (res.data.exType == '1') {
					let content = res.data.message;
					this.Info.tipContent = content;
					this.Info.tipUrl = tipUrl;
					this.Info.exType = '1';
					this.Info.flag = true;

					this.props.modal.show('commitAndUncommit');
					return;
				} else if (
					res.data.workflow &&
					(res.data.workflow == 'approveflow' || res.data.workflow == 'workflow')
				) {
					this.Info.compositedata = res.data;
					this.Info.tipUrl = tipUrl;
					this.setState({ compositedisplay: true });
					return;
				}
				if (res.success) {
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
					}
					let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
					if (this.Info.pk_bill == null) {
						updateCache(pkname, this.props.getUrlParam('id'), newCardData, this.formId, dataSource);
					} else {
						if (this.props.getUrlParam('type') && this.props.getUrlParam('type') != 'transfer') {
							this.props.delUrlParam('type');
						}
						addCache(pkname, newCardData, this.formId, dataSource); //新增
					}
					if (this.props.getUrlParam('status') != 'browse') {
						this.props.cardTable.setStatus(this.tableId, 'browse');
						this.props.form.setFormStatus(this.formId, 'browse');
					}
					this.props.setUrlParam({
						status: 'browse',
						id: this.Info.pk_bill == null ? this.props.getUrlParam('id') : this.Info.pk_bill
					});
					this.state.buttonfalg = null;
					if (this.state.compositedisplay) {
						this.setState({ compositedisplay: false });
					}
					this.clearExType();
					this.toggleShow();
					toast({ color: 'success', content: this.state.json['payablebill-000031'] }); /* 国际化处理： 操作成功*/
				}
			}
		});
	};

	//挂起和取消挂起操作
	pause = (url) => {
		let selectedData = this.props.cardTable.getCheckedRows(this.tableId);
		if (selectedData.length == 0) {
			toast({ color: 'success', content: this.state.json['payablebill-000051'] }); /* 国际化处理： 请选择表体行!*/
			return;
		}
		let pauseObj = [];
		selectedData.forEach((val) => {
			pauseObj.push(val.data.values.pk_payableitem.value);
		});
		let data = {
			pk_items: pauseObj,
			pk_bill: this.props.getUrlParam('id'),
			ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
			pageId: this.getPagecode(),
			billType: this.billType
		};
		ajax({
			url: url,
			data: data,
			success: (res) => {
				if (res.data.message) {
					toast({
						duration: 'infinity',
						color: res.data.PopupWindowStyle,
						content: res.data.message
					});
				} else {
					toast({ color: 'success', content: this.state.json['payablebill-000031'] }); /* 国际化处理： 操作成功*/
				}

				if (res.data.billCard) {
					if (res.data.billCard.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.billCard.head[this.formId] });
					}
					if (res.data.billCard.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.billCard.body[this.tableId]);
					}
					let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
					updateCache(pkname, this.props.getUrlParam('id'), newCardData, this.formId, dataSource);
				}
				this.onSelected();
			}
		});
	};

	//获取列表肩部信息
	getTableHead = (buttons) => {
		return (
			<span>
				{this.props.button.createButtonApp({
					area: 'card_body',
					buttonLimit: 3,
					onButtonClick: buttonClick.bind(this),
					popContainer: document.querySelector('.header-button-area')
				})}
			</span>
		);
	};

	//联查处理情况按钮
	handleCombined = () => {
		this.setState({
			isCombinedExaminationModelShow: !this.state.isCombinedExaminationModelShow
		});
	};
	//联查收付款协议
	handleLinkTerm = () => {
		this.setState({
			isLinkTermModelShow: !this.state.isLinkTermModelShow
		});
	};
	//设置form表单ts的值
	setFormTsVal = (ts) => {
		this.props.form.setFormItemsValue(this.formId, { ts: { value: ts } });
	};

	//关闭联查执行预算的模态框
	tbbLinkcancel() {
		this.setState({
			isTbbLinkshow: false
		});
	}

	//打印
	onPrint = () => {
		this.printData.oids = [ this.props.getUrlParam('id') ];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/arap/arappub/print.do', //后台服务url
			this.printData
			// false 去掉后支持弹出选择打印模板
		);
	};

	//输出
	printOutput = () => {
		this.outputData.oids = [ this.props.getUrlParam('id') ];
		this.outputData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		this.refs.printOutput.open();
	};
	//输出成功回调函数
	onSubmit() {}

	//审批控制
	closeApprove = () => {
		this.setState({
			showApproveDetail: false
		});
	};

	//正式打印
	officalPrintOutput = () => {
		this.printData.oids = [ this.props.getUrlParam('id') ];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		ajax({
			url: '/nccloud/arap/arappub/officialPrint.do',
			data: this.printData,
			async: false,
			success: (res) => {
				if (res.success) {
					print(
						'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
						'/nccloud/arap/arappub/print.do', //后台服务url
						this.printData,
						false
					);
				}
			}
		});
	};
	//取消正式打印
	cancelPrintOutput = () => {
		this.printData.oids = [ this.props.getUrlParam('id') ];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		ajax({
			url: '/nccloud/arap/arappub/cancelPrint.do',
			data: this.printData,
			async: false,
			success: (res) => {
				if (res.success) {
					//取消正式打印提示框
					toast({ color: 'success', content: res.data });
				}
			}
		});
	};
	//返回列表
	backList = () => {
		if (this.props.getUrlParam('scene') == 'linksce' || this.props.getUrlParam('scene') == 'fip') {
			this.props.pushTo('/list', {
				pagecode: '20080PBM_LIST_LINK',
				scene: 'linksce'
			});
		} else {
			this.props.pushTo('/list', {
				pagecode: '20080PBM_LIST'
			});
		}
	};

	//返回转单页面
	backTransfer = () => {
		let dataSource = this.props.getUrlParam('dataSource');
		let src_appcode = getDefData('src_appcode', dataSource);
		let src_pagecode = getDefData('src_pagecode', dataSource);
		let url = '/' + this.props.getUrlParam('srcBilltype');
		this.props.pushTo(url, {
			src_appcode: src_appcode,
			dest_billtype: this.billType,
			dest_tradetype: this.getPagecode()
		});
	};

	//保存事件模态框确定按钮点击事件
	saveSureBtnClick = () => {
		let tipUrl = this.Info.tipUrl;
		let exType = this.Info.exType;
		let flag = this.Info.flag;
		this.saveBill(tipUrl, exType, flag);
	};

	//单据删除模态框确定按钮点击事件
	deleteBillSureBtnClick = () => {
		let exType = this.Info.exType;
		let flag = this.Info.flag;
		this.delConfirm(exType, flag);
	};

	//侧拉删行
	modelDelRow = (props, moduleId) => {
		calculateHeadMoney(this);
		let allVisibleRows = this.props.cardTable.getVisibleRows(this.tableId);
		if (!allVisibleRows || allVisibleRows.length == 0) {
			this.props.cardTable.closeModel(this.tableId);
		}
	};

	//侧拉增行
	modelAddRow = (props, moduleId, modelIndex) => {
		if (this.props.form.getFormItemsValue(this.formId, 'pk_org').value != null) {
			let allRowsNumber = this.props.cardTable.getNumberOfRows(this.tableId);
			ajax({
				url: '/nccloud/arap/payablebillpub/addline.do',
				data: this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId),
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.updateDataByIndexs(this.tableId, [
								{
									index: allRowsNumber - 1,
									data: {
										status: 2,
										values: res.data.body[this.tableId].rows[0].values
									}
								}
							]);
						}
					}
				}
			});
		} else {
			toast({ color: 'warning', content: this.state.json['payablebill-000038'] }); /* 国际化处理： 请先选择财务组织*/
		}
	};

	switchTemplate = () => {
		//initTemplate.call(this, this.props);
		let pagecode = this.getPagecode();
		let appcode = this.props.getSearchParam('c') ? this.props.getSearchParam('c') : this.props.getUrlParam('c');
		let tradetype = getDefData('sessionTradeType', dataSource);
		if (!tradetype) {
			tradetype = tradeType;
		}
		ajax({
			url: '/nccloud/arap/arappub/queryallbtns.do',
			data: {
				appcode: appcode,
				pagecode: pagecode,
				tradetype: tradetype,
				billtype: billType
			},
			success: (res) => {
				let data = res.data;
				if (data.button) {
					let button = data.button;
					this.Info.pullBillInfoVOAry = data.pullbillinfo;
					getButtonsKey(button, this.Info.allButtonsKey);
					this.props.button.setButtons(button);
				}
			}
		});
	};
	gotoLink=(link)=>{
		    if(link){
			        window.open(link)
		    };
	}
	render() {
		let self=this;
		let { cardTable, form, button, modal, cardPagination, transferTable } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		const { createCardPagination } = cardPagination;
		let { createButton } = button;
		let { createModal } = modal;
		let transfer = this.props.getUrlParam('type') == 'transfer';
		const { createTransferList } = transferTable;
		let { showUploader, target } = this.state;
		if (transfer) {
			return (
				<div id="transferCard" className="nc-bill-transferList">
					<div className="nc-bill-header-area">
						<div>
							<NCBackBtn onClick={this.backTransfer} />
						</div>
						<div className="header-title-search-area">
							<h2 className="title-search-detail">{this.state.json['payablebill-000056']}</h2>
							{/* 国际化处理： 应付单管理*/}
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'card_head',
								buttonLimit: 3,
								onButtonClick: transferButtonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
							{getDefData('scene', dataSource) == 'bz' ? null : (
								<NCButton colors="default" onClick={this.backList}>
									{this.state.json['payablebill-000057']}
								</NCButton>
							)}
							{/* 国际化处理： 取消转单*/}
						</div>
					</div>
					<div className="nc-bill-transferList-content">
						{createTransferList({
							headcode: this.formId,
							bodycode: this.tableId,
							transferListId: this.leftarea, //转单列表id
							onTransferItemSelected: (record, status, index) => {
								//转单缩略图被选中时的钩子函数
								this.transferIndex = index;
								let isEdit = status ? 'browse' : 'edit';
								if (isEdit == 'browse') {
									let id = record.head[this.formId].rows[0].values.pk_payablebill.value;
									record = id == null ? record : getCacheById(id, dataSource);
									this.props.setUrlParam({ status: 'browse' });
									this.props.setUrlParam({ id: id });
								} else {
									this.props.setUrlParam({ status: 'add' });
									this.props.delUrlParam('id');
									this.state.buttonfalg = true;
								}
								this.toggleShow(record.head[this.formId]);
								this.props.form.setFormStatus(this.formId, isEdit);
								this.props.cardTable.setStatus(this.tableId, isEdit);
								this.props.form.setAllFormValue({ [this.formId]: record.head[this.formId] });
								this.props.cardTable.setTableData(this.tableId, record.body[this.tableId]);
							}
						})}
						<div className="transferList-content-right nc-bill-card">
							<div className="nc-bill-form-area">
								{createForm(this.formId, {
									onBeforeEvent: formBeforeEvent.bind(this),
									onAfterEvent: afterEvent.bind(this)
								})}
							</div>
							<div className="nc-bill-table-area">
								{createCardTable(this.tableId, {
									tableHead: this.getTableHead.bind(this, buttons),
									modelSave: this.modelSaveClick.bind(this),
									onAfterEvent: afterEvent.bind(this),
									onBeforeEvent: bodyBeforeEvent.bind(this),
									modelAddRow: this.modelAddRow.bind(this),
									modelDelRow: this.modelDelRow.bind(this),
									onSelected: this.onSelected.bind(this),
									onSelectedAll: this.onSelected.bind(this),
									showCheck: true,
									showIndex: true,
									hideAdd: true,
									isAddRow: false
								})}
							</div>
							<div className="nc-bill-table-area">
								{createCardTable('fpxx', {
									// tableHead: this.getTableHead.bind(this, buttons),
									// modelSave: this.modelSaveClick.bind(this),
									// onAfterEvent: afterEvent.bind(this),
									// onBeforeEvent: bodyBeforeEvent.bind(this),
									// modelAddRow: this.modelAddRow.bind(this),
									// modelDelRow: this.modelDelRow.bind(this),
									// onSelected: this.onSelected.bind(this),
									// onSelectedAll: this.onSelected.bind(this),
									// showCheck: true,
									showIndex: true,
									// hideAdd: true,
									// isAddRow: false
								})}
							</div>
							{/* 单据追溯组件 */}
							<BillTrack
								show={this.state.showBillTrack}
								close={() => {
									this.setState({ showBillTrack: false });
								}}
								pk={this.props.getUrlParam('id')} //单据id
								type={
									this.props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? (
										this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
									) : null
								} //单据类型
							/>
							{/* {打印输出} */}
							<PrintOutput
								ref="printOutput"
								url="/nccloud/arap/arappub/outPut.do"
								data={this.outputData}
								callback={this.onSubmit}
							/>
							{/* {联查处理情况} */}
							<CombinedExaminationModel
								show={this.state.isCombinedExaminationModelShow}
								combinedExaminationData={this.Info.combinedExaminationData}
								pk_tradetypeid={
									this.props.form.getFormItemsValue(this.formId, 'pk_tradetypeid') ? (
										this.props.form.getFormItemsValue(this.formId, 'pk_tradetypeid').display
									) : null
								}
								billno={
									this.props.form.getFormItemsValue(this.formId, 'billno') ? (
										this.props.form.getFormItemsValue(this.formId, 'billno').value
									) : null
								}
								handleModel={this.handleCombined.bind(this)}
							/>
							{/* {联查收付款协议} */}
							<LinkTerm
								show={this.state.isLinkTermModelShow}
								linkTermData={this.Info.linkTermData}
								handleModel={this.handleLinkTerm.bind(this)}
								moduleId={'2008'}
								billType={this.billType}
								seletedPks={this.Info.selectedPKS}
								pk_bill={
									this.props.form.getFormItemsValue(this.formId, 'pk_payablebill') ? (
										this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value
									) : null
								}
								setFormTsVal={this.setFormTsVal.bind(this)}
							/>
							{/* {附件管理} */}
							{showUploader && (
								<NCUploader
									billId={
										this.props.form.getFormItemsValue(this.formId, 'pk_payablebill') ? (
											this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value
										) : null
									}
									billNo={
										this.props.form.getFormItemsValue(this.formId, 'billno') ? (
											this.props.form.getFormItemsValue(this.formId, 'billno').value
										) : null
									}
									target={target}
									placement={'bottom'}
									beforeUpload={this.beforeUpload}
									disableModify={this.props.form.getFormItemsValue(this.formId, 'approvestatus')&&this.props.form.getFormItemsValue(this.formId, 'approvestatus').value!="-1"}
									onHide={this.onHideUploader}
								/>
							)}
							<InvoiceUploader
								// {...this.props}
								{...this.state.sscrpInvoiceData}
							/>
							<InvoiceLink {...this.state.sscrpInvoiceData} table={this.props.table} />

							{/* {导入} */}
							{createModal('importModal', {
								noFooter: true,
								className: 'import-modal',
								hasBackDrop: false
							})}
							<ExcelImport
								{...Object.assign(this.props)}
								moduleName="arap" //模块名
								billType={billType} //单据类型
								pagecode={this.getExportPageCode()}
								appcode={this.props.getSearchParam('c')}
								selectedPKS={this.Info.selectedPKS}
								exportTreeUrl={"/nccloud/arap/payablebill/payablebillexport.do"}
								//referVO={{ignoreTemplate:true}}
							/>
							{/* 计划预算*/}
							<Inspection
								show={this.state.isTbbLinkshow}
								sourceData={this.Info.tbbLinkSourceData}
								cancel={this.tbbLinkcancel.bind(this)}
							/>
							{/* {审批详情模态框} */}
							<ApproveDetail
								show={this.state.showApproveDetail}
								close={this.closeApprove}
								billtype={
									this.props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? (
										this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
									) : null
								}
								billid={
									this.props.form.getFormItemsValue(this.formId, 'pk_payablebill') ? (
										this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value
									) : null
								}
							/>
							{/* 异常模态框 */}
							<div>
								{createModal('saveCheck', {
									title: this.state.json['payablebill-000052'], //'预算异常提示信息',//'预算异常提示信息',//this.state.json['10170406-000017'],// 异常提示/* 国际化处理： 异常提示信息*/
									content: this.Info.tipContent, // '预算不通过!',//this.modalContent.call(this), //弹框内容，可以是字符串或dom
									beSureBtnClick: this.saveSureBtnClick.bind(this) //点击确定按钮事件
								})}
								{createModal('deleteCheck', {
									title: this.state.json['payablebill-000052'], //'预算异常提示信息',//'预算异常提示信息',//this.state.json['10170406-000017'],// 异常提示/* 国际化处理： 异常提示信息*/
									content: this.Info.tipContent, // '预算不通过!',//this.modalContent.call(this), //弹框内容，可以是字符串或dom
									beSureBtnClick: this.deleteBillSureBtnClick.bind(this) //点击确定按钮事件
								})}
								{createModal('commitAndUncommit', {
									title: this.state.json['payablebill-000052'] /* 国际化处理： 异常提示信息*/,
									content: this.Info.tipContent,
									beSureBtnClick: this.commitAndUncommit.bind(this), //点击确定按钮事件
									cancelBtnClick: this.cancelClick.bind(this) //提交和收回取消事件
								})}
								{/* 指派信息弹框 */}
								{this.state.compositedisplay ? (
									<ApprovalTrans
										title={this.state.json['payablebill-000055']} /* 国际化处理： 指派*/
										data={this.Info.compositedata}
										display={this.state.compositedisplay}
										getResult={this.commitAndUncommit.bind(this)}
										cancel={this.cancelClick.bind(this)}
									/>
								) : null}
							</div>
						</div>
					</div>
				</div>
			);
		} else {
			return (
				<div className="nc-bill-card">
					<div className="nc-bill-top-area">
						<div className="nc-bill-header-area">
							
							{/* {联查场景，默认场景，浏览态存在返回按钮} */}
							<div className="header-title-search-area">
								{createBillHeadInfo({
									title:this.props.getSearchParam('n'),
									//title:this.state.json['payablebill-000058'],//国际化处理： 应付单
									backBtnClick: () => {
										this.backList();
									}
								})}
							</div>
							<div className="header-button-area">
								{/* {交易类型按钮，默认场景} */}
								{!this.props.getUrlParam('scene') && !getContext(loginContextKeys.transtype) ? (
									<div className="trade-type">
										{TradeTypeButton({
											ref: 'tradetypeBtn',
											billtype: 'F1',
											dataSource: dataSource,
											switchTemplate: this.switchTemplate.bind(this)
										})}
									</div>
								) : null}
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							<div>
							{(this.props.getPageCode() == 'F1-Cxx-07') && (<NCSelect  
							defaultValue="联查柯莱特审批详情" 
							style={{ width: 150, marginRight: 6 }}
							onFocus={()=>{
								var arr=this.props.form.getFormItemsValue(this.formId, 'def16')&&this.props.form.getFormItemsValue(this.formId,'def16').value;
								arrList = arr.split("]");
								self.setState({
									ifChange:!self.state.ifChange
								})
							}}
							onSelect={this.gotoLink.bind(this)}
							showSearch={true} allowClear={true}>
								{
									arrList.length>0&&arrList.map((item,index)=>{
										console.log(item)
										return (<NCOption  key={index} value={item}>{item}</NCOption>)
									})
								}
							</NCSelect>)}

							</div>
							{/* <div>
							{(this.props.getPageCode() == 'F1-Cxx-02') && (<NCButton>
							test
							</NCButton>)}

							</div> */}
							{/* {分页按钮，默认场景或者联查场景} */}
							{this.props.getUrlParam('scene') == 'linksce' ||
							this.props.getUrlParam('scene') == 'fip' ||
							!this.props.getUrlParam('scene') ? (
								<div className="header-cardPagination-area" style={{ float: 'right' }}>
									{createCardPagination({
										handlePageInfoChange: pageInfoClick.bind(this),
										dataSource: dataSource
									})}
								</div>
							) : null}
						</div>
						<div className="nc-bill-form-area">
							{createForm(this.formId, {
								onAfterEvent: afterEvent.bind(this),
								onBeforeEvent: formBeforeEvent.bind(this)
							})}
						</div>
					</div>
					<div className="nc-bill-bottom-area">
						<div className="nc-bill-table-area">
							{createCardTable(this.tableId, {
								tableHead: this.getTableHead.bind(this, buttons),
								modelSave: this.modelSaveClick.bind(this),
								onAfterEvent: afterEvent.bind(this),
								onBeforeEvent: bodyBeforeEvent.bind(this),
								modelAddRow: this.modelAddRow.bind(this),
								modelDelRow: this.modelDelRow.bind(this),
								showCheck: true,
								showIndex: true,
								onSelected: this.onSelected.bind(this),
								onSelectedAll: this.onSelected.bind(this)
							})}
						</div>
					</div>
					<div className="nc-bill-bottom-area">
						<div className="nc-bill-table-area">
							{createCardTable('fpxx', {
								// tableHead: this.getTableHead.bind(this, buttons),
								// modelSave: this.modelSaveClick.bind(this),
								// onAfterEvent: afterEvent.bind(this),
								// onBeforeEvent: bodyBeforeEvent.bind(this),
								// modelAddRow: this.modelAddRow.bind(this),
								// modelDelRow: this.modelDelRow.bind(this),
								showCheck: true,
								showIndex: true,
								// onSelected: this.onSelected.bind(this),
								// onSelectedAll: this.onSelected.bind(this)
							})}
						</div>
					</div>

					{/* 单据追溯组件 */}
					<BillTrack
						show={this.state.showBillTrack}
						close={() => {
							this.setState({ showBillTrack: false });
						}}
						pk={this.props.getUrlParam('id')} //单据id
						type={
							this.props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
							) : null
						} //单据类型
					/>
					{/* {打印输出} */}
					<PrintOutput
						ref="printOutput"
						url="/nccloud/arap/arappub/outPut.do"
						data={this.outputData}
						callback={this.onSubmit}
					/>
					{/* {联查处理情况} */}
					<CombinedExaminationModel
						show={this.state.isCombinedExaminationModelShow}
						combinedExaminationData={this.Info.combinedExaminationData}
						pk_tradetypeid={
							this.props.form.getFormItemsValue(this.formId, 'pk_tradetypeid') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_tradetypeid').display
							) : null
						}
						billno={
							this.props.form.getFormItemsValue(this.formId, 'billno') ? (
								this.props.form.getFormItemsValue(this.formId, 'billno').value
							) : null
						}
						handleModel={this.handleCombined.bind(this)}
					/>
					{/* {联查收付款协议} */}
					<LinkTerm
						show={this.state.isLinkTermModelShow}
						linkTermData={this.Info.linkTermData}
						handleModel={this.handleLinkTerm.bind(this)}
						moduleId={'2008'}
						billType={this.billType}
						seletedPks={this.Info.selectedPKS}
						pk_bill={
							this.props.form.getFormItemsValue(this.formId, 'pk_payablebill') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value
							) : null
						}
						setFormTsVal={this.setFormTsVal.bind(this)}
					/>

					{/* {导入} */}
					{createModal('importModal', {
						noFooter: true,
						className: 'import-modal',
						hasBackDrop: false
					})}
					<ExcelImport
						{...Object.assign(this.props)}
						moduleName="arap" //模块名
						billType={billType} //单据类型
						pagecode={this.getExportPageCode()}
						appcode={this.props.getSearchParam('c')}
						selectedPKS={this.Info.selectedPKS}
						exportTreeUrl={"/nccloud/arap/payablebill/payablebillexport.do"}
						//referVO={{ignoreTemplate:true}}
					/>
					{showUploader && (
						<NCUploader
							billId={
								this.props.form.getFormItemsValue(this.formId, 'pk_payablebill') ? (
									this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value
								) : null
							}
							billNo={
								this.props.form.getFormItemsValue(this.formId, 'billno') ? (
									this.props.form.getFormItemsValue(this.formId, 'billno').value
								) : null
							}
							target={target}
							placement={'bottom'}
							beforeUpload={this.beforeUpload}
							disableModify={this.props.form.getFormItemsValue(this.formId, 'approvestatus')&&this.props.form.getFormItemsValue(this.formId, 'approvestatus').value!="-1"}
							onHide={this.onHideUploader}
						/>
					)}

					<InvoiceUploader
						// {...this.props}
						{...this.state.sscrpInvoiceData}
					/>
					<InvoiceLink {...this.state.sscrpInvoiceData} table={this.props.table} />

					{/* {审批详情模态框} */}
					<ApproveDetail
						show={this.state.showApproveDetail}
						close={this.closeApprove}
						billtype={
							this.props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
							) : null
						}
						billid={
							this.props.form.getFormItemsValue(this.formId, 'pk_payablebill') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value
							) : null
						}
					/>

					{/* 计划预算*/}
					<Inspection
						show={this.state.isTbbLinkshow}
						sourceData={this.Info.tbbLinkSourceData}
						cancel={this.tbbLinkcancel.bind(this)}
					/>
					{/* 异常模态框 */}
					<div>
						{createModal('saveCheck', {
							title: this.state.json['payablebill-000053'], //'预算异常提示信息',//'预算异常提示信息',//this.state.json['10170406-000017'],// 异常提示/* 国际化处理： 预算异常提示信息*/
							content: this.Info.tipContent, // '预算不通过!',//this.modalContent.call(this), //弹框内容，可以是字符串或dom
							beSureBtnClick: this.saveSureBtnClick.bind(this) //点击确定按钮事件
						})}
						{createModal('deleteCheck', {
							title: this.state.json['payablebill-000052'], //'预算异常提示信息',//'预算异常提示信息',//this.state.json['10170406-000017'],// 异常提示/* 国际化处理： 异常提示信息*/
							content: this.Info.tipContent, // '预算不通过!',//this.modalContent.call(this), //弹框内容，可以是字符串或dom
							beSureBtnClick: this.deleteBillSureBtnClick.bind(this) //点击确定按钮事件
						})}
						{createModal('commitAndUncommit', {
							title: this.state.json['payablebill-000054'] /* 国际化处理： 提示信息*/,
							content: this.Info.tipContent,
							beSureBtnClick: this.commitAndUncommit.bind(this), //点击确定按钮事件
							cancelBtnClick: this.cancelClick.bind(this) //提交和收回取消事件
						})}
						{/* 指派信息弹框 */}
						{this.state.compositedisplay ? (
							<ApprovalTrans
								title={this.state.json['payablebill-000055']} /* 国际化处理： 指派*/
								data={this.Info.compositedata}
								display={this.state.compositedisplay}
								getResult={this.commitAndUncommit.bind(this)}
								cancel={this.cancelClick.bind(this)}
							/>
						) : null}
					</div>
				</div>
			);
		}
	}
}

Card = createPage(
	{
		// mutiLangCode: '2052'
	}
)(Card);

export default Card;
