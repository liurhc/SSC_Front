import { ajax, base, toast, print, cardCache, cacheTools, promptBox } from 'nc-lightapp-front';

import { tableId, billType, tradeType, dataSource, searchId } from '../constants';
import { headButton } from '../../../../public/components/pubUtils/buttonName';
import { getTransferInfo } from '../../../../public/components/pubUtils/transferButtonUtil.js';
// import { imageScan, imageView } from '../../../../../sscrp/public/common/components/imageMng';
import { imageScan, imageView } from 'sscrp/rppub/components/image';
import arapLinkReport from '../../../../public/components/arapBillLinkReport.js';
import linkvouchar from '../../../../public/components/linkvouchar.js';
import madeBill from '../../../../public/components/madeBill.js';
let { setDefData, getDefData } = cardCache;
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';

let selectData = null;

export default function buttonClick(props, id) {
	let code = getContext(loginContextKeys.transtype);
	let appcode = props.getSearchParam('c');
	switch (id) {
		case headButton.Add:
			/**
             * 新增---->根据交易类型----->加载模版
             */
			let pagecode = getDefData('sessionTradeType', dataSource);
			if (code) {
				pagecode = code;
			} else if (!pagecode) {
				pagecode = tradeType;
			}
			props.pushTo('/card', {
				status: 'add',
				pagecode: pagecode
			});
			break;
		case headButton.Delete:
			promptBox({
				color: 'warning',
				title: this.state.json['payablebill-000006'] /* 国际化处理： 删除*/,
				content: this.state.json['payablebill-000063'] /* 国际化处理： ​​确定要删除所选数据吗？*/,
				noFooter: false,
				noCancelBtn: false,
				beSureBtnName: this.state.json['payablebill-000008'] /* 国际化处理： 确定*/,
				cancelBtnName: this.state.json['payablebill-000009'] /* 国际化处理： 取消*/,
				beSureBtnClick: () => {
					let delObjs = getAllCheckedData(this.props, this.tableId, billType);
					if (delObjs.length == 0) {
						toast({
							color: 'warning',
							content: this.state.json['payablebill-000016']
						}); /* 国际化处理： 请选中至少一行数据!*/
						return;
					}
					if (delObjs.length == 1) {
						this.Info.pk_bill = delObjs[0].pk_bill;
						this.Info.ts = delObjs[0].ts;
						this.Info.index = delObjs[0].index;
						this.delConfirm();
					} else {
						ajax({
							url: '/nccloud/arap/arappub/delete.do',
							data: delObjs,
							success: (res) => {
								let { success, data } = res;
								if (success) {
									if (data) {
										toast({
											duration: 'infinity',
											color: data.PopupWindowStyle,
											content: data.errMsg
										});

										//删除当前行数据
										props.table.deleteTableRowsByIndex(tableId, data.successIndexs);
										//删除缓存数据
										props.table.deleteCacheId(tableId, data.successPKs);
										//删行之后控制按钮
										this.onSelected();
									}
								}
							}
						});
					}
				}
			});

			break;
		case headButton.Commit: // 批量提交
			commitOpreration(this, this.props, this.tableId, billType, headButton.Commit);
			break;
		case headButton.Uncommit: // 批量收回
			commitOpreration(this, this.props, this.tableId, billType, headButton.Uncommit);
			break;
		case headButton.LinkAprv: //审批详情
			let LinkAprvData = this.props.table.getCheckedRows(this.tableId)[0];
			if (LinkAprvData) {
				this.Info.pk_bill = LinkAprvData.data.values.pk_payablebill.value;
				this.Info.pk_tradetype = LinkAprvData.data.values.pk_tradetype.value;
				this.setState({
					showApproveDetail: true
				});
			} else {
				toast({ color: 'warning', content: this.state.json['payablebill-000025'] }); /* 国际化处理： 请选中一行数据!*/
			}
			break;

		case headButton.MadeBill: //制单
			madeBill(
				this,
				props,
				this.props.table.getCheckedRows(this.tableId),
				'pk_payablebill',
				props.getSearchParam('c'),
				true
			);
			break;
		case headButton.LinkBal: //联查余额表
			arapLinkReport(
				this.props,
				getFirstCheckedData(this.props, this.tableId).data.values.pk_payablebill.value,
				billType,
				getFirstCheckedData(this.props, this.tableId).data.values.objtype.value
			);
			break;
		case headButton.LinkVouchar: //联查凭证
			let voucharInfo = getFirstCheckedData(this.props, this.tableId);
			linkvouchar(
				this,
				props,
				voucharInfo,
				voucharInfo.data.values.pk_payablebill.value,
				props.getSearchParam('c')
			);
			break;
		case headButton.BillLinkQuery: //单据追溯
			if (props.table.getCheckedRows(tableId).length == 0) {
				toast({ color: 'warning', content: this.state.json['payablebill-000025'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			selectData = this.props.table.getCheckedRows(this.tableId)[0];
			this.Info.pk_bill = selectData.data.values.pk_payablebill.value;
			this.Info.pk_tradetype = selectData.data.values.pk_tradetype.value;
			this.setState({
				showBillTrack: true
			});
			break;
		case headButton.LinkInvoice: //发票查看
			let InvoiceInfo = getFirstCheckedData(this.props, this.tableId);
			InvoiceInfo.data.values.pk_payablebill.value;
			const billId = InvoiceInfo.data.values.pk_payablebill.value;
			const billCode = InvoiceInfo.data.values.billno.value;
			const pk_org = InvoiceInfo.data.values.pk_org.value;
			const tradetype = InvoiceInfo.data.values.pk_tradetype.value;
			const viewRandom = Math.random();

			this.setState({
				sscrpInvoiceData: { billId, billCode, pk_org, tradetype, viewRandom }
			});
			break;
		case headButton.LinkDeal: //联查处理情况
			let dealInfo = getFirstCheckedData(this.props, this.tableId);
			ajax({
				url: '/nccloud/arap/arappub/linkdeal.do',
				data: {
					pk_bill: dealInfo.data.values.pk_payablebill.value,
					billType: billType
				},
				success: (res) => {
					if (res.success) {
						//打开处理情况模态框
						let data = res.data;
						//当前选中数据的第一行的交易类型和单据号，用于联查处理情况的模态框上面的显示
						let checkedData = this.props.table.getCheckedRows(this.tableId)[0].data.values;
						this.Info.pk_tradetypeid = checkedData.pk_tradetypeid
							? checkedData.pk_tradetypeid.display
							: null;
						this.Info.billno = checkedData.billno ? checkedData.billno.value : null;
						this.Info.combinedExaminationData = data;
						this.handleCombined();
					}
				}
			});
			break;
		case headButton.LinkConfer: //联查协同单据
			let conferInfo = getFirstCheckedData(this.props, this.tableId);
			ajax({
				url: '/nccloud/arap/arappub/linkconfer.do',
				async: false,
				data: {
					pk_bill: conferInfo.data.values.pk_payablebill.value,
					billType: billType
				},
				success: (res) => {
					let data = res.data;
					if (data) {
						props.openTo(data.url, data.condition);
					}
				}
			});
			break;
		case headButton.LinkTerm: //联查收付款协议
			let linkTermInfo = getFirstCheckedData(this.props, this.tableId);
			ajax({
				url: '/nccloud/arap/arappub/linkterm.do',
				data: {
					pk_bill: linkTermInfo.data.values.pk_payablebill.value,
					billType: billType,
					moduleId: '2008'
				},
				success: (res) => {
					if (res.success) {
						//打开收付款协议模态框
						let data = res.data;
						let checkedData = this.props.table.getCheckedRows(this.tableId)[0];
						this.Info.index = checkedData.index;
						this.Info.linkTermData = data;
						this.Info.pk_bill = checkedData.data.values.pk_payablebill
							? checkedData.data.values.pk_payablebill.value
							: null;
						this.handleLinkTerm();
					}
				}
			});
			break;
		case headButton.LinkTbb: //联查计划预算
			let tbbInfo = getFirstCheckedData(this.props, this.tableId);
			ajax({
				url: '/nccloud/arap/arappub/linktbb.do',
				data: {
					pk_bill: tbbInfo.data.values.pk_payablebill.value,
					billType: billType
				},
				success: (res) => {
					this.Info.tbbLinkSourceData = res.data;
					this.setState({
						isTbbLinkshow: true
					});
				}
			});
			break;
		case 'Print': //打印
			if (props.table.getCheckedRows(tableId).length == 0) {
				toast({ color: 'warning', content: this.state.json['payablebill-000025'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			var selectedData = this.props.table.getCheckedRows(this.tableId);
			var pk_bills = [];
			selectedData.forEach((val) => {
				pk_bills.push(val.data.values.pk_payablebill.value);
			});
			this.printData.oids = pk_bills;
			this.printData.nodekey = 'card';
			this.onPrint();
			break;
		case headButton.PrintList: //打印清单
			var selectedData = this.props.table.getCheckedRows(this.tableId);
			var pk_bills = [];
			selectedData.forEach((val) => {
				pk_bills.push(val.data.values.pk_payablebill.value);
			});
			if (pk_bills.length == 0) {
				toast({ color: 'warning', content: this.state.json['payablebill-000025'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			this.printData.oids = pk_bills;
			this.printData.nodekey = 'list';
			this.onPrint();
			break;
		case headButton.Output: //输出...
			if (props.table.getCheckedRows(tableId).length == 0) {
				toast({ color: 'warning', content: this.state.json['payablebill-000025'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			selectedData = this.props.table.getCheckedRows(this.tableId);
			let pkBills = [];
			selectedData.forEach((val) => {
				pkBills.push(val.data.values.pk_payablebill.value);
			});
			this.outputData.oids = pkBills;
			this.outputData.nodekey = 'card';
			this.printOutput();
			break;
		case headButton.OfficalPrint: //正式打印
			if (props.table.getCheckedRows(tableId).length == 0) {
				toast({ color: 'warning', content: this.state.json['payablebill-000025'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			selectedData = this.props.table.getCheckedRows(this.tableId);
			pkBills = [];
			selectedData.forEach((val) => {
				pkBills.push(val.data.values.pk_payablebill.value);
			});
			this.printData.oids = pkBills;
			this.printData.nodekey = 'card';
			this.officalPrintOutput();
			break;
		case headButton.CancelPrint: //取消正式打印
			if (props.table.getCheckedRows(tableId).length == 0) {
				toast({ color: 'warning', content: this.state.json['payablebill-000025'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			selectedData = this.props.table.getCheckedRows(this.tableId);
			pkBills = [];
			selectedData.forEach((val) => {
				pkBills.push(val.data.values.pk_payablebill.value);
			});
			this.printData.oids = pkBills;
			this.printData.nodekey = 'card';
			this.cancelPrintOutput();
			break;
		case headButton.ExportData: //导出
			const ExportData = this.props.table.getCheckedRows(this.tableId);
			pk_bills = [];
			ExportData.forEach((val) => {
				pk_bills.push(val.data.values.pk_payablebill.value);
			});
			if (pk_bills.length == 0) {
				toast({ color: 'warning', content: this.state.json['payablebill-000025'] }); /* 国际化处理： 请选中一行数据!*/
				return;
			}
			this.Info.selectedPKS = pk_bills; //传递主键数组,之前nc需要导出的加主键
			this.props.modal.show('exportFileModal'); //不需要导出的只执行这行代码
			break;
		case headButton.Refresh: //刷新
			let data = getDefData(searchId, dataSource);
			if (data) {
				ajax({
					url: '/nccloud/arap/payablebill/queryscheme.do',
					data: data,
					success: (res) => {
						let { success, data } = res;
						if (success) {
							if (data) {
								toast({
									color: 'success',
									title: this.state.json['payablebill-000062']
								}); /* 国际化处理： 刷新成功*/
								this.props.table.setAllTableData(this.tableId, data[this.tableId]);
							} else {
								toast({
									color: 'success',
									content: this.state.json['payablebill-000017']
								}); /* 国际化处理： 未查询到数据*/
								this.props.table.setAllTableData(this.tableId, { rows: [] });
							}
							setDefData(this.tableId, dataSource, data); //放入缓存
							this.onSelected();
						}
					}
				});
			} else {
				toast({ color: 'success', title: this.state.json['payablebill-000062'] }); /* 国际化处理： 刷新成功*/
				this.props.table.setAllTableData(this.tableId, { rows: [] });
			}
			break;
		case headButton.ReceiptCheck: //影像查看pk_tradetype
			let CheckInfo = getFirstCheckedData(this.props, this.tableId);

			var billInfoMap = {};

			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = CheckInfo.data.values.pk_payablebill.value;
			billInfoMap.pk_billtype = CheckInfo.data.values.pk_billtype.value;
			billInfoMap.pk_tradetype = CheckInfo.data.values.pk_tradetype.value;
			billInfoMap.pk_org = CheckInfo.data.values.pk_org.value;
			imageView(billInfoMap, 'iweb');
			break;
		case headButton.ReceiptScan: //影像扫描
			let ScanInfo = getFirstCheckedData(this.props, this.tableId);

			var billInfoMap = {};

			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = ScanInfo.data.values.pk_payablebill.value;
			billInfoMap.pk_billtype = ScanInfo.data.values.pk_tradetype.value;
			billInfoMap.pk_tradetype = ScanInfo.data.values.pk_tradetype.value;
			billInfoMap.pk_org = ScanInfo.data.values.pk_org.value;

			//影像所需 FieldMap
			billInfoMap.BillType = ScanInfo.data.values.pk_tradetype.value;
			billInfoMap.BillDate = ScanInfo.data.values.creationtime.value;
			billInfoMap.Busi_Serial_No = ScanInfo.data.values.pk_payablebill.value;
			// billInfoMap.pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
			billInfoMap.OrgNo = ScanInfo.data.values.pk_org.value;
			billInfoMap.BillCode = ScanInfo.data.values.billno.value;
			billInfoMap.OrgName = ScanInfo.data.values.pk_org.display;
			billInfoMap.Cash = ScanInfo.data.values.money.value;
			imageScan(billInfoMap, 'iweb');
			break;
		case headButton.AttachManage:
			let AttachInfo = getFirstCheckedData(this.props, this.tableId);
			this.Info.pk_bill = AttachInfo.data.values.pk_payablebill.value;
			this.Info.billno = AttachInfo.data.values.billno.value;
			this.setState({
				showUploader: true,
				target: null
			});
			break;
		default:
			let transferInfo = getTransferInfo(this, id);
			if (transferInfo) {
				ajax({
					url: '/nccloud/arap/arappub/queryrelatedapp.do',
					data: {
						billType: transferInfo.src_billtype
					},
					success: (res) => {
						if (res) {
							//将业务流程放入缓存
							setDefData(
								transferInfo.src_billtype + transferInfo.transtypes[0],
								'transfer.dataSource',
								transferInfo.busitypes
							);
							let dest_tradetype = getDefData('sessionTradeType', dataSource);
							if (code) {
								dest_tradetype = code;
							} else if (!dest_tradetype) {
								dest_tradetype = tradeType;
							}
							let url = '/' + transferInfo.src_billtype;
							props.pushTo(url, {
								src_appcode: res.data.appcode,
								src_tradetype: transferInfo.transtypes[0], //来源交易类型
								busitype: transferInfo.busitypes[0], //业务流程
								dest_billtype: billType,
								dest_tradetype: dest_tradetype
							});
						}
					}
				});
			}
			break;
	}
}

//获取选中数据的第一行,选中多行的时候只取第一行数据
let getFirstCheckedData = function(props, tableId) {
	let checkedData = props.table.getCheckedRows(tableId);
	let checkedObj;
	if (checkedData.length > 0) {
		checkedObj = checkedData[0];
	} else {
		toast({ color: 'warning', content: this.state.json['payablebill-000025'] }); /* 国际化处理： 请选中一行数据!*/
		return;
	}
	return checkedObj;
};

//获取选中数据的id和billType
let getAllCheckedData = function(props, tableId, billType) {
	let checkedData = props.table.getCheckedRows(tableId);
	let checkedObj = [];
	checkedData.forEach((val) => {
		checkedObj.push({
			pk_bill: val.data.values.pk_payablebill.value,
			ts: val.data.values.ts.value,
			billType: billType,
			index: val.index,
			pageId: props.getSearchParam('p')
		});
	});
	return checkedObj;
};

let commitOpreration = function(that, props, tableId, billType, actionType) {
	let commitParams = getAllCheckedData(props, tableId, billType);
	if (commitParams.length == 0) {
		toast({ color: 'warning', content: this.state.json['payablebill-000016'] }); /* 国际化处理： 请选中至少一行数据!*/
		return;
	}
	let ajaxUrl = '';
	if (actionType == headButton.Commit) {
		ajaxUrl = '/nccloud/arap/arappub/batchcommit.do';
	} else if (actionType == headButton.Uncommit) {
		ajaxUrl = '/nccloud/arap/arappub/batchuncommit.do';
	}
	if (commitParams.length == 1 && actionType == headButton.Commit) {
		//单条的时候走单条的处理
		that.Info.tipUrl = '/nccloud/arap/arappub/commit.do';
		that.Info.record = props.table.getCheckedRows(tableId)[0].data.values;
		that.Info.record.numberindex = { scale: 0, value: commitParams[0].index + 1 };
		that.commitAndUncommit();
		return;
	}

	ajax({
		url: ajaxUrl,
		data: commitParams,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data.grid) {
					let grid = data.grid;
					let updateValue = [];
					for (let key in grid) {
						updateValue.push({ index: key, data: { values: grid[key].values } });
					}
					props.table.updateDataByIndexs(tableId, updateValue);
				}

				if (data.message) {
					toast({
						duration: 'infinity',
						color: data.PopupWindowStyle,
						content: data.message
					});
					that.onSelected();
				}
			}
		}
	});
};
