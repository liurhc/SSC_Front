import { asyncComponent } from 'nc-lightapp-front';
import List from '../list';

//应付单卡片
const card = asyncComponent(() => import(/*webpackChunkName:"/arap/payablebill/payablebill/card/card"*/  /* webpackMode: "eager" */ '../card'));
//预核销
const prev = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/preverify/list/prev"*/ /* webpackMode: "eager" */ '../../../verificationsheet/preverify/list'));
//及时核销list
const nowv = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/verifynow/list/nowv"*/ /* webpackMode: "eager" */ '../../../verificationsheet/verifynow/list'));
//及时核销card
const nowvcard = asyncComponent(() => import(/*webpackChunkName:"/arap/verificationsheet/verifynow/nowvcard"*/ /* webpackMode: "eager" */ '../../../verificationsheet/verifynow/nowvcard'));
//应收单转单
const F0 = asyncComponent(() => import(/*webpackChunkName:"/arap/receivablebill/recbill/transfer/f0transfer"*/ /* webpackMode: "eager" */ '../../../receivablebill/recbill/transfer'));
//付款单转单
const F3 = asyncComponent(() => import(/*webpackChunkName:"/arap/paybill/paybill/transfer/f3transfer"*/ /* webpackMode: "eager" */ '../../../paybill/paybill/transfer'));
//付款合同转单
const FCT1 = asyncComponent(() => import(/*webpackChunkName:"/fct/bill/ap/transfer/fct1transfer"*/ /* webpackMode: "eager" */ '../../../../fct/bill/ap/transfer'));
//资产处置 4A18
const sale = asyncComponent(() => import(/*webpackChunkName:"/aum/reduce/saletransfer/source/source"*/ /* webpackMode: "eager" */ '../../../../aum/reduce/saletransfer/source'));
//安装调试 4A24
const install = asyncComponent(() => import(/*webpackChunkName:"/aum/install/installtransfer/source/source"*/ /* webpackMode: "eager" */ '../../../../aum/install/installtransfer/source'));
//资产投保 4A23
const insurance = asyncComponent(() => import(/*webpackChunkName:"/aum/insur/insurancetransfer/source/source"*/ /* webpackMode: "eager" */ '../../../../aum/insur/insurancetransfer/source'));





const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: card
	},
	{
		path: '/prev',
		component: prev
	},
	{
		path: '/nowv',
		component: nowv
	},
	{
		path: '/nowvcard',
		component: nowvcard
	},
	{
		path: '/F0',
		component: F0
	},
	{
		path: '/F3',
		component: F3
	},
	{
		path: '/FCT1',
		component: FCT1
	},
	{
		path: '/4A18',
		component:sale
	},
	{
		path: '/4A24',
		component: install
	},
	{
		path: '/4A23',
		component: insurance
	},

];

export default routes;


