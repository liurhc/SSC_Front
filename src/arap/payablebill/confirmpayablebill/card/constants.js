
/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 表体区域
 */
export const tableId = 'bodys';


/**
 * 单据类型
 */
export const billType = 'F1';

/**
 * 默认交易类型
 */
export const tradeType = 'D1';

/**
 * 单页应用缓存
 */
export const dataSource  = 'fi.arap.confirmpayablebill.20080SPLC';

/**
 * 单页应用缓存主键名字(该名字为表的主键名字)
 */
export const pkname  = 'pk_payablebill';

