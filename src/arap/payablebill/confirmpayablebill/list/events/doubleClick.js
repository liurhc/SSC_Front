import { cardCache } from 'nc-lightapp-front';
let { setDefData } = cardCache;
import {  searchId, searchKey, dataSource } from '../constants';
export default function doubleClick(record, index, e) {
    //表页跳转的时候，获取所有查询区条件，并放入缓存
    let searchVal = this.props.search.getAllSearchData(searchId);
    if (searchVal) {
        setDefData(searchKey, dataSource, searchVal);
    }
    this.props.pushTo('/card', {
        status: 'browse',
        id: record.pk_payablebill.value,
        pagecode: record.pk_tradetype.value
    })

}
