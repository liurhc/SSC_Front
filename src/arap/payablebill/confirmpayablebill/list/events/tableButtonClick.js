import { ajax, base, toast ,cardCache} from 'nc-lightapp-front';
import { billType,tableId ,dataSource} from '../constants';
import {innerButton} from '../../../../public/components/pubUtils/buttonName.js';
let {setDefData, getDefData } = cardCache;

export default function(that,props, key, text, record, index){
    switch (key) {
        case innerButton.Confirm_inner:// 表格操确认
            ajax({
                url: '/nccloud/arap/arappub/confirm.do',
                data: {
                    pageId: record.pk_tradetype.value,
                    pk_bill: record.pk_payablebill.value,
                    ts: record.ts.value,
                    billType : billType
                },
                success: (res) => {
                    if(res.success){
                        setDefData('confirm'+record.pk_payablebill.value, dataSource, res.data);
                        props.pushTo('/card', {
                            status: 'edit',
                            id: record.pk_payablebill.value,
                            pagecode:record.pk_tradetype.value
                        })
                       

                    }
                }
            });
            break;
        case innerButton.CancelConfirm_inner:
            let pk_bill = record.pk_payablebill.value;
            ajax({
                url: '/nccloud/arap/arappub/cancelconfirm.do',
                data: {
                    pageId:props.getSearchParam("p"),
                    pk_bill: pk_bill,
                    ts: record.ts.value,
                    billType : billType,
                    type : 1 
                },
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        toast({ color: 'success', content: that.state.json['payablebill-000005'] });/* 国际化处理： 取消确认成功*/
                        //更新当前行数据
                        props.table.updateDataByIndexs(tableId, 
                            [{index:index ,data: {values :data[tableId].rows[0].values}}]);
                    }
                }
            });
            break;
        case innerButton.Delete_inner:
            ajax({
                url: '/nccloud/arap/arappub/confirmdelete.do',
                data: {
                        pk_bill: record.pk_payablebill.value,
                        ts: record.ts.value,
                        billType: billType
                    },
                success: (res) => {
                    if (res.success) {
                        toast({color: 'success', content: that.state.json['payablebill-000013']});/* 国际化处理： 删除成功*/
                        //删除当前行数据
                        props.table.deleteTableRowsByIndex(tableId, index);
                        let {deleteCacheId} =props.table;
                        //删除缓存数据
                        deleteCacheId(tableId,record.pk_payablebill.value);
                        //删除之后控制按状态
                        that.onSelected();
                    }
                }
            });
            break;
        default:
            break;
    }
};
