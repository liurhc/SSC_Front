
/**
 * 查询区域
 */
export const searchId = 'query';

/**
 * 列表区域
 */
export const tableId = 'list';


/**
 * 默认交易类型
 */
export const tradeType = 'D1';



/**
 * 单据类型
 */
export const billType = 'F1';

/**
 * 单页应用缓存
 */
export const dataSource  = 'fi.arap.confirmpayablebill.20080SPLC';

/**
 * 单页应用缓存主键名字(该名字为表的主键名字)
 */
export const pkname  = 'pk_payablebill';

	/**
 * 单页查询区缓存主键名字
 */
export const searchKey = 'pk_payablebill_search';

