import { asyncComponent } from 'nc-lightapp-front';
import List from '../list';

//应收单卡片
const his = asyncComponent(() => import(/*webpackChunkName:"/arap/receivablebill/debtransfer/hisrecord/his"*/  /* webpackMode: "eager" */ '../hisrecord'));


const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/his',
		component: his
	}
];

export default routes;


