
import { excelImportconfig } from 'nc-lightapp-front';
import tableButtonClick from './tableButtonClick';
import { buttonVisible, getButtonsKey,getInnerButtonkey ,cardBodyAndInnerButtonVisible} from '../../../../public/components/pubUtils/buttonvisible.js';
import { tableId, formId,billType} from '../constants';
import { loginContext, getContext, loginContextKeys } from '../../../../public/components/arapInitInfo/loginContext';
import {OperationColumn} from '../../../../public/components/pubUtils/arapConstant';
export default function (props,callback) {
	//从地址栏获取页面编码
	const that = this;
	let pagecode = that.getPagecode();
	let appcode = props.getSearchParam('c');
	
	let excelimportconfig = excelImportconfig(props, "arap", billType,true,'',{isinit: 'true',"appcode":appcode,"pagecode":"20060ARO_CARD"});
	
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode,//注册按钮的id
			reqDataQuerypage: {
				rqUrl: '/arap/arappub/querypage.do',
                rqJson: `{\n  \"pagecode\": \"${pagecode}\",\n  \"appcode\": \"${appcode}\"\n}`,
                rqCode: 'template'
			}
		},
		function (data) {
			if (data) {
				if(!data.template[tableId]){
					return;
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta,that);
					props.meta.setMeta(meta)
				}
				if (data.button) {
					let button = data.button;
					getButtonsKey(button, that.Info.allButtonsKey);//保存所有头部和肩部按钮
					props.button.setButtons(button);
					props.button.setUploadConfig("ImportData", excelimportconfig);
				}
				if(callback){
					callback()
				}
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
					if(getContext(loginContextKeys.transtype)){
						if (that.refs.tradetypeBtn) {
							that.refs.tradetypeBtn.setVisible(false); 
						}
					}
				}
			}
		}//,false//请求模板不走缓存
	)
}

function modifierMeta(props, meta,that) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	//添加操作列
	meta[tableId].items.push({
		label: that.state.json['payablebill-000011'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: OperationColumn,
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let trueBtn = cardBodyAndInnerButtonVisible(that, that.state.buttonfalg);
			return props.button.createOprationButton(trueBtn, {
				area: "card_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(that,props, key, text, record, index)
			});
		}
	});
	return meta;
}
