import { ajax, promptBox, toast } from 'nc-lightapp-front';
import { currentTypeAfterFormEvents } from '../../../../public/components/pubUtils/currentTypeAfterEvent';
import { formulamsgHint, renderData, headAfterEventRenderData, bodyAfterEventRenderData, errorDeal } from '../../../../public/components/afterEventPub/afterEventPubDeal';
import { autoAddLineKeys } from '../../../../public/components/pubUtils/billPubInfo';
import { getColvalues, getRowIds } from '../../../../public/components/pubUtils/billPubUtil';
import { moneyAndRateFields } from '../../../../public/components/pubUtils/specialFieldAfterEvent.js';

export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
	if (changedrows instanceof Array) {
		if (changedrows[0].newvalue.value == changedrows[0].oldvalue.value) {
			return;
		}
	}
	let pagecode = this.getPagecode();
	let index = 0;
	if (moduleId == this.formId) {
		index = 0;
	} else if (moduleId == this.tableId) {
		index = i;
	}

	//表头编辑后事件
	if (moduleId == this.formId) {
		let data = null
		switch (key) {
			case 'pk_org':
				if (value.value == null || value.value == '') {
					//清空财务组织
					promptBox({
						color: 'warning',
						title: this.state.json['payablebill-000033'] /* 国际化处理： 确认修改*/,
						content: this.state.json['payablebill-000034'] /* 国际化处理： 确定​修改组织，这样会清空您录入的信息?*/,
						beSureBtnName: this.state.json['payablebill-000008'] /* 国际化处理： 确定*/,
						cancelBtnName: this.state.json['payablebill-000009'] /* 国际化处理： 取消*/,
						beSureBtnClick: () => {
							this.props.form.EmptyAllFormValue(this.formId);
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
							this.initAdd(true);
						},
						cancelBtnClick: () => {
							this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
						}
					});
				} else if (changedrows.value != null && changedrows.value != '') {
					//切换组织
					promptBox({
						color: 'warning',
						title: this.state.json['payablebill-000033'] /* 国际化处理： 确认修改*/,
						content: this.state.json['payablebill-000034'] /* 国际化处理： 确定​修改组织，这样会清空您录入的信息?*/,
						beSureBtnName: this.state.json['payablebill-000008'] /* 国际化处理： 确定*/,
						cancelBtnName: this.state.json['payablebill-000009'] /* 国际化处理： 取消*/,
						beSureBtnClick: () => {
							ajax({
								url: '/nccloud/arap/initpayablebill/cardheadafteredit.do',
								data: {
									pageId: pagecode,
									event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
									uiState: this.props.getUrlParam('status')
								},
								async: false,
								success: (res) => {
									renderData(this, res);
									//编辑公式提示
									formulamsgHint(this, res);
								},
								error: (res) => {
									errorDeal(this, res, changedrows);
								}
							});
						},
						cancelBtnClick: () => {
							this.props.form.setFormItemsValue(this.formId, { pk_org: changedrows });
						},
						closeByClickBackDrop: false
					});
				} else {
					data = {
						pageId: pagecode,
						event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
						uiState: this.props.getUrlParam('status')
					}
					headFieldAfterRequest.call(this, data, key, changedrows)
				}
				break;
			case 'supplier':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'payaccount', 'pk_currtype', 'supplier', 'buysellflag',
						'objtype', "direction", "pk_billtype", "top_billtype"].concat(moneyAndRateFields)),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'payaccount':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'payaccount', 'pk_currtype', 'supplier', 'buysellflag',
						'objtype', "direction", "pk_billtype"].concat(moneyAndRateFields)),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_deptid_v':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_deptid', 'pk_deptid_v', 'pk_currtype', 'supplier', 'taxtype', 'buysellflag', 'objtype', "direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_psndoc':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ['pk_org', 'pk_psndoc', 'pk_deptid', 'pk_deptid_v', 'isrefused', 'pseudocolumn', 'prepay', 'isdiscount', 'objtype', 'direction', 'agentreceivelocal']),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				};
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
			case 'pk_currtype':
				data = {
					pageId: pagecode,
					formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
					colValues: getColvalues(this.props, this.tableId, ["pk_org","pk_group","pk_currtype","pk_billtype","billdate","rate","grouprate","globalrate",
					"buysellflag","taxprice","local_taxprice","taxrate","occupationmny","money_bal","local_money_bal",
					"globalcrebit","globalnotax_cre","globaltax_cre","groupcrebit","groupnotax_cre","grouptax_cre",
					"local_money_cr","local_notax_cr","local_tax_cr","money_cr","notax_cr","quantity_cr","direction"]),
					rowids: getRowIds(this.props, this.tableId),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows)
				//币种事件发送完要判断汇率
				currentTypeAfterFormEvents(this.formId, props, key);
				break;
			default:
				data = {
					pageId: pagecode,
					event: this.props.createHeadAfterEventData(pagecode, this.formId, this.tableId, moduleId, key, value),
					uiState: this.props.getUrlParam('status')
				}
				headFieldAfterRequest.call(this, data, key, changedrows)
				break;
		}
	}

	//表体编辑后事件
	if (moduleId == this.tableId) {
		ajax({
			url: '/nccloud/arap/initpayablebill/cardbodyafteredit.do',
			data: {
				rowindex: 0,
				editindex: index,
				pageId: pagecode,
				changedrows: changedrows,
				tableId:this.tableId,
				body: props.cardTable.getDataByIndex(this.tableId, index),
				formEvent: props.createFormAfterEventData(pagecode, this.formId, this.tableId, key, value),
				uiState: this.props.getUrlParam('status')
			},
			async: false,
			success: (res) => {
				//渲染数据
				bodyAfterEventRenderData(this, res);
				//表体改变表头税率编辑性
				if (i == 0 && key == 'pk_currtype') {
					currentTypeAfterFormEvents(this.formId, props, 'pk_currtype');
				}
				//编辑公式提示
				formulamsgHint(this, res);

			},
			error: (res) => {
				let str = String(res);
				let content = str.substring(6, str.length);
				if (content.substring(1, 17) == 'convertException') {
					/* 国际化处理： 折算误差*/
					promptBox({
						color: 'warning',
						title: this.state.json['payablebill-000004'] /* 国际化处理： 折算误差*/,
						content: content.substring(17, content.length),
						closeByClickBackDrop: false,
						beSureBtnClick: () => {
							afterTableEvent(this, props, i, pagecode, moduleId, key, changedrows, 'sure', index,value);
						},
						cancelBtnClick: () => {
							afterTableEvent(this, props, i, pagecode, moduleId, key, changedrows, 'cancel', index,value);
						}
					});
				} else {
					this.props.cardTable.setValByKeyAndRowId(this.tableId, i, key, changedrows);
					toast({ color: 'danger', content: content });
				}
			}
		});
		//自动增行
		autoAddline.call(this, moduleId, pagecode, key, i)
	}
}

function afterTableEvent(that, props, i, pagecode, moduleId, key, changedrows, isCalculateConvert, index,value) {
	ajax({
		url: '/nccloud/arap/initpayablebill/cardbodyafteredit.do',
		data: {
			rowindex: 0,
			editindex: index,
			pageId: pagecode,
			changedrows: changedrows,
			tableId:that.tableId,
			body: props.cardTable.getDataByIndex(that.tableId, index),
			formEvent: props.createFormAfterEventData(pagecode, that.formId, that.tableId, key, value),
			uiState: that.props.getUrlParam('status'),
			isCalculateConvert: isCalculateConvert
		},
		async: false,
		success: (res) => {
			//渲染数据
			bodyAfterEventRenderData(that, res);
			//表体改变表头税率编辑性
			if (i == 0 && key == 'pk_currtype') {
				currentTypeAfterFormEvents(that.formId, props, 'pk_currtype');
			}
			//编辑公式提示
			formulamsgHint(that, res);
		}
	});
}

export function headFieldAfterRequest(requestData, key, changedrows) {
	ajax({
		url: '/nccloud/arap/initpayablebill/cardheadafteredit.do',
		data: requestData,
		async: false,
		success: (res) => {
			//渲染数据
			headAfterEventRenderData(this, res);
			//编辑公式提示
			formulamsgHint(this, res);

			if (key == 'pk_org') {
				let pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
				if (pk_org) {
					this.props.resMetaAfterPkorgEdit();
					this.state.buttonfalg = true;
				} else {
					this.state.buttonfalg = null;
				}
				this.toggleShow();
			}

			if (this.props.getUrlParam('type') == 'transfer') {
				this.synTransferData();
			}
		},
		error: (res) => {
			let str = String(res);
			let content = str.substring(6, str.length);
			if(key=='pk_org'){
				this.props.form.setFormItemsValue(this.formId, { 'pk_org': {value:null,display:null} });
			}else{
				this.props.form.setFormItemsValue(that.formId, { pk_org: changedrows });
			}
			toast({ color: 'danger', content: content });
		}
	});
}

export function autoAddline(moduleId, pagecode, key, i) {
	//自动增行
	let allRowsNumber = this.props.cardTable.getNumberOfRows(this.tableId);
	if (moduleId == this.tableId && allRowsNumber == i + 1 && autoAddLineKeys.indexOf(key) != -1) {
		let data = this.props.createMasterChildData(pagecode, this.formId, this.tableId);
		//清空cardData的表体
		data.body[this.tableId].rows = [];
		ajax({
			url: '/nccloud/arap/payablebillpub/addline.do',
			data: data,
			async: false,
			success: (res) => {
				if (res.data) {
					if (res.data.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					}
					if (res.data.body) {
						this.props.cardTable.addRow(this.tableId, allRowsNumber, res.data.body[this.tableId].rows[0].values, false);
					}
				}
			}
		});
	}
}