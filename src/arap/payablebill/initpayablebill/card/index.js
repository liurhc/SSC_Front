//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, high, print, cardCache, getMultiLang } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, pageInfoClick } from './events';
import {
	buttonVisible,
	getButtonsKey,
	dealTranferBtns,
	initCardBodyEditControl,
	onSelectedCardBodyEditControl,
	initCardBodybrowseControl
} from '../../../public/components/pubUtils/buttonvisible.js';
import { tableId, formId, billType, nodekey, dataSource, pkname, tradeType } from './constants';
import CombinedExaminationModel from '../../../public/components/combinedExaminationModel'; //联查处理情况
import LinkTerm from '../../../public/components/linkTerm'; //联查收付款协议
import PeriodModal from '../../../public/components/periodModel';
import { bodyBeforeEvent } from '../../../public/components/pubUtils/arapTableRefFilter';
import { formBeforeEvent } from '../../../public/components/pubUtils/arapFormRefFilter';
const {ExcelImport} = high;
import { calculateHeadMoney, delBlankLine } from '../../../public/components/pubUtils/billPubUtil.js';
const { BillTrack, NCUploader, PrintOutput, Refer } = high;
import { dealCardData } from '../../../public/components/pubUtils/dealCardData';
import { loginContext, getContext, loginContextKeys } from '../../../public/components/arapInitInfo/loginContext';
let { NCFormControl, NCButton, NCBackBtn } = base;
let {
	setDefData,
	getDefData,
	addCache,
	getNextId,
	deleteCacheById,
	getCacheById,
	updateCache,
	getCurrentLastId
} = cardCache;
import TradeTypeButton from '../../../public/components/tradetype'; //交易类型按钮组件
import '../../../public/less/tradetype.less';

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = formId;
		this.tableId = tableId;
		this.billType = billType;
		this.printData = {
			billtype: billType, //单据类型
			appcode: this.props.getSearchParam('c'), //功能节点编码，即模板编码
			nodekey: props.getSearchParam('p'), //模板节点标识  =交易类型
			oids: [this.props.getUrlParam('id')], // 功能节点的数据主键 oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			userjson: billType //单据类型,billtype不是必需字段，后台没有设置接收字段，以userjson代替
		};
		this.outputData = {
			billtype: billType, //单据类型
			appcode: this.props.getSearchParam('c'),
			funcode: this.props.getSearchParam('c'),
			nodekey: props.getSearchParam('p'), //模板节点标识  =交易类型
			oids: [this.props.getUrlParam('id')], // 功能节点的数据主键
			userjson: billType, //单据类型
			outputType: 'output'
		};
		this.state = {
			isCombinedExaminationModelShow: false, //联查处理情况模态框
			isLinkTermModelShow: false, //联查收付款协议模态框
			isPeriodModelShow: false, //期初建账模态框
			showBillTrack: false, //单据追溯模态框的显示标识
			showUploader: false, //附件
			target: null,
			buttonfalg: null, //卡片态点击肩部按钮和表体行按钮改变该值控制按钮状态
			json: {}
		};
		this.Info = {
			allButtonsKey: [], //保存所有的表头按钮
			combinedExaminationData: [], //联查处理情况模态框表格数据
			linkTermData: [], //联查收付款协议模态框表格数据
			selectedPKS: [], //导出数据的主键pk，收付款协议选中子表行
			isModelSave: false //是否是整单保存，默认为false
		};
	}

	//关闭、刷新弹窗时
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				window.onbeforeunload = () => {
					let status = this.props.getUrlParam('status');
					if (status == 'edit' || status == 'add') {
						return '';
					}
				};
				initTemplate.call(this, this.props, this.initShow);
			});
		}
		getMultiLang({ moduleId: ['payablebill', 'public'], domainName: 'arap', currentLocale: 'simpchn', callback });
	}
	componentWillReceiveProps(nextProps) { }


	getPagecode = () => {
		let pagecode = this.props.getUrlParam('pagecode');
		if (!pagecode) {
			pagecode = this.props.getSearchParam('p');
		}
		return pagecode;
	}

	beforeUpload(billId, fullPath, file, fileList) {
		// 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表
		;
		const isJPG = true;
		if (!isJPG) {
			alert(this.state.json['payablebill-000028']);/* 国际化处理： 只支持png格式图片*/
		}
		const isLt2M = file.size / 1024 / 1024 < 10;
		if (!isLt2M) {
			alert(this.state.json['payablebill-000029']);/* 国际化处理： 上传大小小于10M*/
		}
		return isJPG && isLt2M;
		// 备注： return false 不执行上传  return true 执行上传
	}
	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		});
	};
	componentDidMount() {

	}
	//界面初始化显示的值
	initShow = () => {
		if (this.props.getUrlParam('type') == 'copy') {
			ajax({
				url: '/nccloud/arap/arappub/copy.do',
				data: {
					pk_bill: this.props.getUrlParam('id'),
					pageId: this.getPagecode(),
					billType: this.billType,
					type: 1,
					tradeType: this.getPagecode()
				},
				success: (res) => {
					this.props.beforeUpdatePage();//打开开关
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}
					this.props.resMetaAfterPkorgEdit();
					this.state.buttonfalg = true;
					this.props.cardTable.setStatus(this.tableId, 'edit');
					this.props.form.setFormStatus(this.formId, 'edit');
					this.props.updatePage(this.formId, this.tableId);//关闭开关
					this.toggleShow();
				},
				error: (res) => {
					this.props.form.EmptyAllFormValue(this.formId);
					this.props.cardTable.setTableData(this.tableId, { rows: [] });
					this.props.setUrlParam({ status: 'browse' });
					this.props.cardTable.setStatus(this.tableId, 'browse');
					this.props.form.setFormStatus(this.formId, 'browse');
					this.toggleShow();
					let str = String(res);
					let content = str.substring(6, str.length);
					toast({ color: 'danger', content: content });
				}
			});
		} else if (this.props.getUrlParam('status') == 'edit' || this.props.getUrlParam('status') == 'browse') {
			ajax({
				url: '/nccloud/arap/initpayablebill/querycard.do',
				data: {
					pk_bill: this.props.getUrlParam('id')
				},
				success: (res) => {
					this.props.beforeUpdatePage();//打开开关
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					}
					if (this.props.getUrlParam('status') == 'edit') {
						this.state.buttonfalg = true;
						this.props.resMetaAfterPkorgEdit();
						this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true });
					} else {
						this.state.buttonfalg = null;
					}
					let status = this.props.getUrlParam('status');
					this.props.cardTable.setStatus(this.tableId, status);
					this.props.form.setFormStatus(this.formId, status);
					this.props.updatePage(this.formId, this.tableId);//关闭开关
					updateCache(pkname, this.props.getUrlParam('id'), res.data, this.formId, dataSource);
					this.props.setUrlParam({ pagecode: res.data.head[this.formId].rows[0].values.pk_tradetype.value });
					this.toggleShow();
				}
			});
		} else if (this.props.getUrlParam('status') == 'add'){
			this.initAdd();
		}else{
			this.toggleShow();
		}
	};

	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		if (!status) { status = 'browse'; }//如果未定义，默认给值浏览态
		let cardhead = {};
		let head = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId).head[
			this.formId
		];
		if (head) {
			cardhead = head.rows[0].values;
		}
		if (status != 'browse') {
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: false });
			if (this.refs.tradetypeBtn && !getContext(loginContextKeys.transtype)) {
				this.refs.tradetypeBtn.setVisible(false); //设置交易类型是否显示
			}
		} else {
			if (!(cardhead.pk_tradetype ? cardhead.pk_tradetype.value : null)) {
				this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
			} else {
				this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);
			}
			this.props.BillHeadInfo.setBillHeadInfoVisible({ showBackBtn: true });
			if (this.refs.tradetypeBtn && !getContext(loginContextKeys.transtype)) {
				this.refs.tradetypeBtn.setVisible(true); //设置交易类型是否显示
			}
		}
		let trueBtn = []; //可见的按钮
		let falseBtn = []; //不可见的按钮
		for (let i = 0; i < this.Info.allButtonsKey.length; i++) {
			let flag = buttonVisible(status, cardhead, this.Info.allButtonsKey[i], 'card', this);
			if (flag) {
				trueBtn.push(this.Info.allButtonsKey[i]);
			} else {
				falseBtn.push(this.Info.allButtonsKey[i]);
			}
		}
		//初始化肩部按钮信息增行等按钮的控制
		if (status == 'browse') {
			initCardBodybrowseControl(this);
		} else {
			initCardBodyEditControl(this.props, cardhead.pk_org ? cardhead.pk_org.value : null);
		}
		this.props.button.setButtonVisible(trueBtn, true);
		this.props.button.setButtonVisible(falseBtn, false);
	};

	//点击新增时默认值
	initAdd = (isClearPKorg) => {
		if (!isClearPKorg) {
			//isClearPKorg如果未未定义等，就取值为false
			isClearPKorg = false;
		}
		ajax({
			url: '/nccloud/arap/initpayablebill/defvalue.do',
			data: {
				isClearPKorg: isClearPKorg,
				appcode: this.props.getSearchParam('c'),
				pageId: this.getPagecode(),
				tradeType: this.getPagecode()
			},
			success: (res) => {
				if (res.data) {
					this.props.beforeUpdatePage();//打开开关
					this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
					let pk_org = res.data.head[this.formId].rows[0].values.pk_org.value;
					if (pk_org) {
						this.props.resMetaAfterPkorgEdit();
						this.props.form.setFormItemsDisabled(this.formId, { pk_org: false });
						this.state.buttonfalg = true;
					} else {
						this.state.buttonfalg = null;
						this.props.initMetaByPkorg();
					}
					this.props.form.setFormStatus(this.formId, 'edit');
					this.props.cardTable.setStatus(this.tableId, 'edit');
					this.props.updatePage(this.formId, this.tableId);//关闭开关
				}
				this.toggleShow();
			},
			error: (res) => {//主要控制列表进卡片抛出错误
				let str = String(res);
				let content = str.substring(6, str.length);
				toast({ color: 'danger', content: content });
				let status = this.props.getUrlParam('status');
				if(status !='browse'){
					this.props.setUrlParam({ status: 'browse' });
					this.props.form.setFormStatus(this.formId, 'browse');
					this.props.cardTable.setStatus(this.tableId, 'browse');
					this.toggleShow();
				}
			}
		});
	};
	//卡片表体点击行事件
	onSelected = () => {
		onSelectedCardBodyEditControl(this);
	};

	//删除单据
	delConfirm = () => {
		ajax({
			url: '/nccloud/arap/initpayablebill/delete.do',
			data: [
				{
					pk_bill: this.props.getUrlParam('id'),
					ts: this.props.form.getFormItemsValue(this.formId, 'ts').value
				}
			],
			success: (res) => {
				//删除单据后，卡片界面显示该单据的下一个单据
				//如果是最后一个单据，删除后返回list界面
				if (res.success) {
					toast({ color: 'success', content: this.state.json['payablebill-000013'] });/* 国际化处理： 删除成功*/
					let id = this.props.getUrlParam('id');
					deleteCacheById(pkname, id, dataSource);
					let nextId = getNextId(id, dataSource);
					if (nextId) {
						this.props.setUrlParam({ id: nextId });
						this.initShow();
					} else {
						this.props.setUrlParam({ id: null });
						this.props.form.EmptyAllFormValue(this.formId);
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
						this.toggleShow()
					}
				}
			}
		});
	};
	cancel = () => {
		//取消按钮
		if (this.props.getUrlParam('type')) {
			//如果type存在就删除
			this.props.delUrlParam('type');
		}
		let pk_org = this.props.form.getFormItemsValue(this.formId, 'pk_org');
		if(!pk_org || !pk_org.value || pk_org.value==""){
			this.props.resMetaAfterPkorgEdit();
		}
		this.props.beforeUpdatePage();//打开开关
		this.state.buttonfalg = null;
		if (this.props.getUrlParam('status') === 'edit') {
			this.props.setUrlParam({ status: 'browse' });
			let id = this.props.getUrlParam('id');
			let cardData = getCacheById(id, dataSource);
			if (cardData) {
				this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
				this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
				this.props.cardTable.setStatus(this.tableId, 'browse');
				this.props.form.setFormStatus(this.formId, 'browse');
				this.props.updatePage(this.formId, this.tableId);//关闭开关
				this.toggleShow();
			} else {
				this.initShow();
			}
		} else if (this.props.getUrlParam('status') === 'add') {
			let id = this.props.getUrlParam('id');
			if (!id) {
				id = getCurrentLastId(dataSource);
			}
			if (id) {
				this.props.setUrlParam({ status: 'browse', id: id });
				let cardData = getCacheById(id, dataSource);
				if (cardData) {
					this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
					this.props.cardTable.setTableData(this.tableId, cardData.body[this.tableId]);
					let pagecode = cardData.head[this.formId].rows[0].values.pk_tradetype.value;
					if (this.getPagecode() != pagecode) {
						this.props.setUrlParam({ pagecode: pagecode });
						initTemplate.call(this, this.props);
					}
					this.props.cardTable.setStatus(this.tableId, 'browse');
					this.props.form.setFormStatus(this.formId, 'browse');
					this.props.updatePage(this.formId, this.tableId);//关闭开关
					this.toggleShow();
				} else {
					this.initShow();
				}
			} else {
				this.props.form.EmptyAllFormValue(this.formId);
				this.props.cardTable.setTableData(this.tableId, { rows: [] });
				this.props.setUrlParam({ status: 'browse' });
				this.props.cardTable.setStatus(this.tableId, 'browse');
				this.props.form.setFormStatus(this.formId, 'browse');
				this.props.updatePage(this.formId, this.tableId);//关闭开关
				this.toggleShow();
			}
		}
	};
	//挂起和取消挂起操作
	pause = (url) => {
		let selectedData = this.props.cardTable.getCheckedRows(this.tableId);
		if (selectedData.length == 0) {
			toast({ color: 'success', content: this.state.json['payablebill-000037'] });/* 国际化处理： 请选择至少一条子表数据*/
			return;
		}
		let pauseObj = [];
		selectedData.forEach((val) => {
			pauseObj.push(val.data.values.pk_payableitem.value);
		});
		let data = {
			pk_items: pauseObj,
			pk_bill: this.props.getUrlParam('id'),
			ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
			pageId: this.getPagecode(),
			billType: this.billType
		};
		ajax({
			url: url,
			data: data,
			success: (res) => {
				if (res.data.message) {
					toast({
						duration: 'infinity',
						color: res.data.PopupWindowStyle,
						content: res.data.message
					});
				} else {
					toast({ color: 'success', content: this.state.json['payablebill-000031'] });/* 国际化处理： 操作成功*/
				}
				if (res.data.billCard) {
					if (res.data.billCard.head) {
						this.props.form.setAllFormValue({ [this.formId]: res.data.billCard.head[this.formId] });
					}
					if (res.data.billCard.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.billCard.body[this.tableId]);
					}
					let newCardData = this.props.createMasterChildData(
						this.getPagecode(),
						this.formId,
						this.tableId
					);
					updateCache(pkname, this.props.getUrlParam('id'), newCardData, this.formId, dataSource);
				}
				this.onSelected();
			}
		});
	};

	//保存单据
	saveBill = (modelIndex) => {
		//删除空白行
		let checkCardData = this.props.createMasterChildData(
			this.getPagecode(),
			this.formId,
			this.tableId
		);
		delBlankLine(this, this.tableId, this.billType, checkCardData, modelIndex);
		if (!this.props.form.isCheckNow(this.formId)) {
			//表单验证
			return;
		}
		if (!this.props.cardTable.checkTableRequired(this.tableId)) {
			//表格验证
			return;
		}
		let cardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
		let newCardData  = dealCardData(this,cardData);//去掉空值，减少压缩时间
		let datas = {
			cardData: newCardData,
			uiState: this.props.getUrlParam('status')
		};
		let callback = () => {
			ajax({
				url: '/nccloud/arap/initpayablebill/save.do',
				data: datas,
				success: (res) => {
					let pk_payablebill = null;
					let pk_tradetype = null;
					if (res.success) {
						if (res.data) {
							toast({ color: 'success', content: this.state.json['payablebill-000014'] });/* 国际化处理： 保存成功*/
							this.props.beforeUpdatePage();//打开开关
							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
								pk_payablebill = res.data.head[this.formId].rows[0].values.pk_payablebill.value;
								pk_tradetype = res.data.head[this.formId].rows[0].values.pk_tradetype.value;
							}
							if (res.data.body) {
								this.props.cardTable.updateDataByRowId(this.tableId, res.data.body[this.tableId]);
							}
							this.state.buttonfalg = null;
							this.props.cardTable.setStatus(this.tableId, 'browse');
							this.props.form.setFormStatus(this.formId, 'browse');
							this.props.updatePage(this.formId, this.tableId);//关闭开关
							let newCardData = this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId);
							if (this.props.getUrlParam('status') == 'add') {
								addCache(pk_payablebill, newCardData, this.formId, dataSource); //新增
							} else {
								updateCache(pkname, pk_payablebill, newCardData, this.formId, dataSource); //修改之后更新缓存
							}
							if (this.props.getUrlParam('type')) {
								this.props.delUrlParam('type');
							}
							this.props.setUrlParam({ status: 'browse', id: pk_payablebill, pagecode: pk_tradetype });
						}
					}

					if (this.Info.isModelSave) {
						this.Info.isModelSave = false;
						this.props.cardTable.closeModel(this.tableId);
					}
					this.toggleShow();
				}
			});
		};
		this.props.validateToSave(datas.cardData, callback, { table1: 'cardTable' }, 'card');
	};

	//获取列表肩部信息
	getTableHead = (buttons) => {
		return (
			<span>
				{this.props.button.createButtonApp({
					area: 'card_body',
					buttonLimit: 3,
					onButtonClick: buttonClick.bind(this),
					popContainer: document.querySelector('.header-button-area')
				})}
			</span>
		);
	};

	//联查处理情况按钮
	handleCombined = () => {
		this.setState({
			isCombinedExaminationModelShow: !this.state.isCombinedExaminationModelShow
		});
	};
	//联查收付款协议
	handleLinkTerm = () => {
		this.setState({
			isLinkTermModelShow: !this.state.isLinkTermModelShow
		});
	};
	//设置form表单ts的值
	setFormTsVal = (ts) => {
		this.props.form.setFormItemsValue(this.formId, { ts: { value: ts } });
	};
	//期初弹窗控制
	handlePeriodInformation = () => {
		this.setState({
			isPeriodModelShow: !this.state.isPeriodModelShow
		});
	};

	//打印
	onPrint = () => {
		this.printData.oids = [this.props.getUrlParam('id')];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/arap/arappub/print.do', //后台服务url
			this.printData,
			// false 去掉后支持弹出选择打印模板
		);
	};

	//正式打印
	officalPrintOutput = () => {
		this.printData.oids = [this.props.getUrlParam('id')];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		ajax({
			url: '/nccloud/arap/arappub/officialPrint.do',
			data: this.printData,
			async: false,
			success: (res) => {
				if (res.success) {
					print(
						'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
						'/nccloud/arap/arappub/print.do', //后台服务url
						this.printData,
						false
					);
				}
			}
		});
	};
	//取消正式打印
	cancelPrintOutput = () => {
		this.printData.oids = [this.props.getUrlParam('id')];
		this.printData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		ajax({
			url: '/nccloud/arap/arappub/cancelPrint.do',
			data: this.printData,
			async: false,
			success: (res) => {
				if (res.success) {
					//取消正式打印提示框
					toast({ color: 'success', content: res.data });
				}
			}
		});
	};

	//打印输出
	printOutput = () => {
		this.outputData.oids = [this.props.getUrlParam('id')];
		this.outputData.nodekey = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
		this.refs.printOutput.open();
	};
	//输出成功回调函数
	onSubmit() {
	}


	//返回列表
	backList = () => {
		this.props.pushTo('/list', {
			pagecode: '20080APO_LIST'
		});
	};

	//侧拉删行
	modelDelRow = (props, moduleId) => {
		calculateHeadMoney(this);
		let allVisibleRows=this.props.cardTable.getVisibleRows(this.tableId);
		if(!allVisibleRows || allVisibleRows.length == 0){
			this.props.cardTable.closeModel(this.tableId);
		}
	};

	//侧拉增行
	modelAddRow = (props, moduleId, modelIndex) => {
		if (this.props.form.getFormItemsValue(this.formId, 'pk_org').value != null) {
			let allRowsNumber = this.props.cardTable.getNumberOfRows(this.tableId);
			ajax({
				url: '/nccloud/arap/payablebillpub/addline.do',
				data: this.props.createMasterChildData(this.getPagecode(), this.formId, this.tableId),
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.updateDataByIndexs(this.tableId, [
								{
									index: allRowsNumber - 1,
									data: {
										status: 2,
										values: res.data.body[this.tableId].rows[0].values
									}
								}
							]);
						}
					}
				}
			});
		} else {
			toast({ color: 'warning', content: this.state.json['payablebill-000038'] });/* 国际化处理： 请先选择财务组织*/
		}
	};
	//整单保存事件
	modelSaveClick = (a, modelIndex) => {
		this.Info.isModelSave = true;
		this.saveBill(modelIndex);
	};

	render() {
		let { cardTable, form, button, modal, cardPagination } = this.props;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		const { createCardPagination } = cardPagination;
		let { createButton } = button;
		let { createModal } = modal;
		let { showUploader, target } = this.state;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createBillHeadInfo({
							title : this.state.json['payablebill-000039'],//国际化处理： 应付期初
							backBtnClick: () => {
								this.backList();
							}
						})}
						</div>

						<div className="header-button-area">
							{!getContext(loginContextKeys.transtype) ? (
								<div className="trade-type">
									{TradeTypeButton({
										ref: 'tradetypeBtn',
										billtype: 'F1',
										dataSource: dataSource
									})}
								</div>
							) : null}

							{this.props.button.createButtonApp({
								area: 'card_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
						<div className="header-cardPagination-area" style={{ float: 'right' }}>
							{createCardPagination({
								handlePageInfoChange: pageInfoClick.bind(this),
								dataSource: dataSource
							})}
						</div>
					</div>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: formBeforeEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(this.tableId, {
							tableHead: this.getTableHead.bind(this, buttons),
							modelSave: this.modelSaveClick.bind(this),
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: bodyBeforeEvent.bind(this),
							modelAddRow: this.modelAddRow.bind(this),
							modelDelRow: this.modelDelRow.bind(this),
							onSelected: this.onSelected.bind(this),
							onSelectedAll: this.onSelected.bind(this),
							showCheck: true,
							showIndex: true
						})}
					</div>
				</div>

				{/* {联查处理情况} */}
				<CombinedExaminationModel
					show={this.state.isCombinedExaminationModelShow}
					combinedExaminationData={this.Info.combinedExaminationData}
					pk_tradetypeid={
						this.props.form.getFormItemsValue(this.formId, 'pk_tradetypeid') ? (
							this.props.form.getFormItemsValue(this.formId, 'pk_tradetypeid').display
						) : null
					}
					billno={
						this.props.form.getFormItemsValue(this.formId, 'billno') ? (
							this.props.form.getFormItemsValue(this.formId, 'billno').value
						) : null
					}
					handleModel={this.handleCombined.bind(this)}
				/>
				{/* {联查收付款协议} */}
				<LinkTerm
					show={this.state.isLinkTermModelShow}
					linkTermData={this.Info.linkTermData}
					handleModel={this.handleLinkTerm.bind(this)}
					moduleId={'2008'}
					billType={this.billType}
					seletedPks={this.Info.selectedPKS}
					pk_bill={
						this.props.form.getFormItemsValue(this.formId, 'pk_payablebill') ? (
							this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value
						) : null
					}
					setFormTsVal={this.setFormTsVal.bind(this)}
				/>
				<PeriodModal
					show={this.state.isPeriodModelShow}
					handleModel={this.handlePeriodInformation.bind(this)}
					billFieldType={'AP_INIT_PAYABLE'}
					funCode={'20080APO'}
				/>
				{/* 单据追溯组件 */}
				<BillTrack
					show={this.state.showBillTrack}
					close={() => {
						this.setState({ showBillTrack: false });
					}}
					pk={this.props.getUrlParam('id')} //单据id
					type={
						this.props.form.getFormItemsValue(this.formId, 'pk_tradetype') ? (
							this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value
						) : null
					} //单据类型
				/>

				{/* {打印输出} */}
				<PrintOutput
					ref="printOutput"
					url="/nccloud/arap/arappub/outPut.do"
					data={this.outputData}
					callback={this.onSubmit}
				/>
				{/* {导入} */}
				{
					createModal('importModal', {
						noFooter: true,
						className: 'import-modal',
						hasBackDrop: false,
					})
				}
				<ExcelImport
					{...Object.assign(this.props)}
					moduleName="arap" //模块名
					billType={this.billType} //单据类型
					pagecode={"20060ARO_CARD"}
					appcode={this.props.getSearchParam('c')}
					selectedPKS={this.Info.selectedPKS}
					exportTreeUrl={"/nccloud/arap/payablebill/payablebillexport.do"}
					//referVO={{ignoreTemplate:true}}
				/>
				{showUploader && (
					<NCUploader
						billId={
							this.props.form.getFormItemsValue(this.formId, 'pk_payablebill') ? (
								this.props.form.getFormItemsValue(this.formId, 'pk_payablebill').value
							) : null
						}
						billNo={
							this.props.form.getFormItemsValue(this.formId, 'billno') ? (
								this.props.form.getFormItemsValue(this.formId, 'billno').value
							) : null
						}
						target={target}
						placement={'bottom'}
						beforeUpload={this.beforeUpload}
						onHide={this.onHideUploader}
					/>
				)}
			</div>
		);
	}
}

Card = createPage({
	// initTemplate: initTemplate,
	mutiLangCode: '2052'
})(Card);

export default Card;
