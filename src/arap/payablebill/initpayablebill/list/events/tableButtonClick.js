import { ajax, base, toast,cardCache } from 'nc-lightapp-front';
import { billType,tableId ,dataSource} from '../constants';
import {innerButton} from '../../../../public/components/pubUtils/buttonName.js';
export default function(that,props, key, text, record, index){
    switch (key) {
        // 表格操修改
        case innerButton.Edit_inner:
            ajax({
                url: '/nccloud/arap/init/edit.do',
                data: {
                    pk_bill: record.pk_payablebill.value,
                    billType: billType
                },
                success: (res) => {
                    if (res.success) {
                        props.pushTo('/card', {
                            status: 'edit',
                            id: record.pk_payablebill.value,
                            pagecode:record.pk_tradetype.value,
                        })
                    }
                }
            });
            break;
        case innerButton.Delete_inner:
            ajax({
                url: '/nccloud/arap/initpayablebill/delete.do',
                data: [
                    {
                        pk_bill: record.pk_payablebill.value,
                        ts: record.ts.value,
                        billType: billType
                    }
                ],
                success: (res) => {
                    if (res.success) {
                        toast({color: 'success', content: that.state.json['payablebill-000013']});/* 国际化处理： 删除成功*/
                        //删除当前行数据
                        props.table.deleteTableRowsByIndex(tableId, index);
                        let {deleteCacheId} = props.table;
                        //删除缓存数据
                        deleteCacheId(tableId,record.pk_payablebill.value);
                        //删行之后控制肩部按钮
                        that.onSelected();
                    }
                }
            });
            break;
        case innerButton.Copy_inner:
            props.pushTo('/card', {
                status: 'add',
                id: record.pk_payablebill.value,
                type: 'copy',
                pagecode:record.pk_tradetype.value,
            })
            break;
        default:
            break;
    }
};
