import {ajax,toast,cardCache} from 'nc-lightapp-front';
import { tableId, searchId,dataSource} from '../constants';
let {setDefData, getDefData } = cardCache;

export default function clickSearchBtn(props,searchVal) {
    if(searchVal){
        let pageInfo = props.table.getTablePageInfo(this.tableId);
        let queryInfo = props.search.getQueryInfo(this.searchId);
        queryInfo.pageInfo = pageInfo;
        let data = {
            pageId: props.getSearchParam('p'),
            queryInfo:queryInfo
        }; 
        setDefData(searchId, dataSource, data);//放入缓存
        ajax({
            url: '/nccloud/arap/initpayablebill/queryscheme.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        toast({ color: 'success', content: this.state.json['payablebill-000019']+data[this.tableId].allpks.length+this.state.json['payablebill-000020'] });/* 国际化处理： 查询成功，共,条*/
                        this.props.table.setAllTableData(tableId, data[tableId]);
                    }else{
                        toast({ color: 'warning', content: this.state.json['payablebill-000021'] });/* 国际化处理： 未查询出符合条件的数据*/
                        this.props.table.setAllTableData(tableId, {rows:[]});
                    }
                    setDefData(this.tableId, dataSource, data);//放入缓存
                    this.onSelected()
                }
            }
        });
    }
    
};
