import { ajax, base, toast, cardCache, print,promptBox } from 'nc-lightapp-front';
import { headButton } from '../../../../public/components/pubUtils/buttonName.js';
import { billType, tradeType, dataSource, searchId, tableId } from '../constants';
import { getTitle } from '../../../../public/components/getCartTableData.js';
import arapLinkReport from "../../../../public/components/arapBillLinkReport.js";
let { setDefData, getDefData } = cardCache;
export default function buttonClick(props, id) {

    switch (id) {
        case headButton.Add:
            /**
             * 新增---->根据交易类型----->加载模版
             */
            let pagecode = getDefData('sessionTradeType', dataSource)
            if (!pagecode) {
                pagecode = tradeType;
            }
            props.pushTo('/card', {
                status: 'add',
                pagecode: pagecode
            })
            break;
        case headButton.Delete:
            promptBox({
                color: 'warning',                 
                title: this.state.json['payablebill-000006'],                  /* 国际化处理： 删除*/
                content: this.state.json['payablebill-000063'],             /* 国际化处理： ​​确定要删除所选数据吗？*/
                noFooter: false,                 
                noCancelBtn: false,              
                beSureBtnName: this.state.json['payablebill-000008'],           /* 国际化处理： 确定*/
                cancelBtnName: this.state.json['payablebill-000009'] ,          /* 国际化处理： 取消*/
                beSureBtnClick: ()=>{
                    let deleteData = getAllCheckedData(this.props, this.tableId, billType);
                    if (deleteData.length == 0) {
                        toast({ color: 'warning', content: this.state.json['payablebill-000016'] });/* 国际化处理： 请选中至少一行数据!*/
                        return;
                    }
                    ajax({
                        url: '/nccloud/arap/init/batchdelete.do',
                        data: deleteData,
                        success: (res) => {
                            let { success, data } = res;
                            if (success) {
                                if(data.message){
                                    toast({
                                        duration:  'infinity',          
                                        color: data.PopupWindowStyle,         
                                        content: data.message,   
                                    })
                                }
                                if(data.successIndexs){
                                    //删除当前行数据
                                    props.table.deleteTableRowsByIndex(tableId, data.successIndexs);
                                }
                                if(data.successPKs){
                                    //删除缓存数据
                                    props.table.deleteCacheId(tableId, data.successPKs);
                                }
                                //列表控制按钮
                                this.onSelected();
                            }
                        }
                    });
                }
            });

            
            break;
        case headButton.BillLinkQuery://联查单据
            let selectData = getFirstCheckedData(this.props, this.tableId);
            this.Info.pk_bill =  selectData.data.values.pk_payablebill.value;
            this.Info.pk_tradetype = selectData.data.values.pk_tradetype.value;
            this.setState({
                showBillTrack: true,
            })
            break;
        case headButton.LinkBal://联查余额表
            arapLinkReport(this.props,getFirstCheckedData(this.props, this.tableId).data.values.pk_payablebill.value,billType,getFirstCheckedData(this.props, this.tableId).data.values.objtype.value);
            break;
        case headButton.LinkDeal://联查处理情况
            let dealInfo = getFirstCheckedData(this.props, this.tableId);
            ajax({
                url: '/nccloud/arap/arappub/linkdeal.do',
                data: {
                    pk_bill: dealInfo.data.values.pk_payablebill.value,
                    ts: dealInfo.data.values.ts.value,
                    billType: billType
                },
                success: (res) => {
                    if (res.success) {
                        //打开处理情况模态框
                        let data = res.data;
                        //当前选中数据的第一行的交易类型和单据号，用于联查处理情况的模态框上面的显示
                        let checkedData = this.props.table.getCheckedRows(this.tableId)[0].data.values;
                        
                        this.Info.combinedExaminationData = data;
                        this.Info.pk_tradetypeid = checkedData.pk_tradetypeid ? checkedData.pk_tradetypeid.display : null;
                        this.Info.billno = checkedData.billno ? checkedData.billno.value : null;
                        this.handleCombined();
                    }
                }
            });
            break;
        case headButton.LinkTerm://联查收付款协议
            let linkTermInfo = getFirstCheckedData(this.props, this.tableId);
            ajax({
                url: '/nccloud/arap/arappub/linkterm.do',
                data: {
                    pk_bill: linkTermInfo.data.values.pk_payablebill.value,
                    billType: billType,
                    moduleId: '2008'
                },
                success: (res) => {
                    if (res.success) {
                        //打开收付款协议模态框
                        let data = res.data;
                        let checkedData = this.props.table.getCheckedRows(this.tableId)[0];

                        this.Info.index = checkedData.index;
                        this.Info.linkTermData = data;
                        this.Info.pk_bill = checkedData.data.values.pk_payablebill ? checkedData.data.values.pk_payablebill.value : null;

                        this.handleLinkTerm();
                    }
                }
            });
            break;
        case 'InitBuild':
            this.handlePeriodInformation();
            getTitle("close");
            break;
        case 'CancelInitBuild':
            this.handlePeriodInformation();
            getTitle("alerdyClose");
            break;
        case 'Print'://打印
            let selectedData = this.props.table.getCheckedRows(this.tableId);
            if (selectedData.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            let pk_bills = [];
            selectedData.forEach((val) => {
                pk_bills.push(val.data.values.pk_payablebill.value);
            });
            this.printData.oids = pk_bills;
            this.printData.nodekey = "card";
            this.onPrint();
            break;
        case headButton.PrintList://打印清单
            selectedData = this.props.table.getCheckedRows(this.tableId);
            pk_bills = [];
            selectedData.forEach((val) => {
                pk_bills.push(val.data.values.pk_payablebill.value);
            });
            if (pk_bills.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            this.printData.oids = pk_bills;
            this.printData.nodekey = "list";
            this.onPrint();
            break;
        case headButton.OfficalPrint://正式打印
            selectedData = this.props.table.getCheckedRows(this.tableId);
            if (selectedData.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            pk_bills = [];
            selectedData.forEach((val) => {
                pk_bills.push(val.data.values.pk_payablebill.value);
            });
            this.printData.oids = pk_bills;
            this.printData.nodekey = "card";
            this.officalPrintOutput();
            break;
        case headButton.CancelPrint://取消正式打印
            selectedData = this.props.table.getCheckedRows(this.tableId);
            if (selectedData.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            pk_bills = [];
            selectedData.forEach((val) => {
                pk_bills.push(val.data.values.pk_payablebill.value);
            });
            this.printData.oids = pk_bills;
            this.printData.nodekey = "card";
            this.cancelPrintOutput();
            break;
        case headButton.Output://打印输出
            selectedData = this.props.table.getCheckedRows(this.tableId);
            if (selectedData.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            pk_bills = [];
            selectedData.forEach((val) => {
                pk_bills.push(val.data.values.pk_payablebill.value);
            });
            this.outputData.oids = pk_bills;
            this.outputData.nodekey = "card";
            this.printOutput();
            break;
        case headButton.ExportData://导出
            selectedData = this.props.table.getCheckedRows(this.tableId);
            pk_bills = [];
            selectedData.forEach((val) => {
                pk_bills.push(val.data.values.pk_payablebill.value);
            });
            if (pk_bills.length == 0) {
                toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
                return;
            }
            this.Info.selectedPKS = pk_bills; //传递主键数组,之前nc需要导出的加主键
            this.props.modal.show('exportFileModal');//不需要导出的只执行这行代码
            break;
        case headButton.Refresh:
            let data = getDefData(searchId, dataSource);
            if (data) {
                ajax({
                    url: '/nccloud/arap/initpayablebill/queryscheme.do',
                    data: data,
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            if (data) {
                                toast({ color: 'success', title: this.state.json['payablebill-000062'] });/* 国际化处理： 刷新成功*/
                                this.props.table.setAllTableData(tableId, data[tableId]);
                            } else {
                                toast({ color: 'success', content: this.state.json['payablebill-000017'] });/* 国际化处理： 未查询到数据*/
                                this.props.table.setAllTableData(tableId, { rows: [] });
                            }
                            setDefData(this.tableId, dataSource, data);//放入缓存
                            this.onSelected();
                        }
                    }
                });
            } else {
                toast({ color: 'success', title: this.state.json['payablebill-000062'] });/* 国际化处理： 刷新成功*/
                this.props.table.setAllTableData(this.tableId, { rows: [] });
            }
            break;
            case headButton.AttachManage:
			let AttachInfo = getFirstCheckedData(this.props, this.tableId);
			this.Info.pk_bill = AttachInfo.data.values.pk_payablebill.value;
			this.Info.billno = AttachInfo.data.values.billno.value;
			this.setState({
			  showUploader: true,
			  target: null
			})
			break;
        default:
            break;
    }
}



//获取选中数据的第一行,选中多行的时候只取第一行数据
let getFirstCheckedData = function (props, tableId, billType) {
    let checkedData = props.table.getCheckedRows(tableId);
    let checkedObj;
    if (checkedData.length > 0) {
        checkedObj = checkedData[0]
    } else {
        toast({ color: 'warning', content: this.state.json['payablebill-000025'] });/* 国际化处理： 请选中一行数据!*/
        return;
    }
    return checkedObj;
}


//获取选中数据的id和billType
let getAllCheckedData = function (props, tableId, billType) {
    let checkedData = props.table.getCheckedRows(tableId);
    let checkedObj = [];
    checkedData.forEach((val) => {
        checkedObj.push({
            pk_bill: val.data.values.pk_payablebill.value,
            ts: val.data.values.ts.value,
            billType: billType,
            index: val.index,
        }
        );
    });
    return checkedObj;
}
