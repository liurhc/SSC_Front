
/**
 * 查询区域
 */
export const searchId = 'query';

/**
 * 列表区域
 */
export const tableId = 'list';


/**
 * 查询模板主键
 */
//export const oid = '1001Z31000000000SNF8';


/**
 * 单据类型
 */
export const billType = 'F1';
/**
 * 默认交易类型
 */
export const tradeType = 'D1';

/**
 * 默认模板节点标识
 */
export const nodekey = "list";

/**
 * 单页应用缓存
 */
export const dataSource  = 'fi.arap.initpayablebill.20080APO';

/**
 * 单页应用缓存主键名字(该名字为表的主键名字)
 */
export const pkname  = 'pk_payablebill';

	/**
 * 单页查询区缓存主键名字
 */
export const searchKey = 'pk_payablebill_search';


