import { ajax,toast,cacheTools } from 'nc-lightapp-front';
let searchId =  '20022025_query'
let field ='year'
 export default function onAfterEvent(field, val) {
    if(field =='pk_contrastrule')
	 {
		 if(val.length==0)
		 {
			 return;
		 }
        //添加到缓存
        cacheTools.set('reportcheckpk',val[0].refpk);
        //获取日期的默认值
		let self = this; 
        let pk_contrastrule = val[0].refpk;
        let sendData = {pk_contrastrule:pk_contrastrule};
		let url = '/nccloud/gl/contrast/contrastreportmakeinit.do';
		ajax({
			url:url,
			data:sendData,
			success: function(response){
				const { success } = response;
				if (success) {
					if(response.data){
						cacheTools.set('reportcheckpk_accperiodscheme',response.data.pk_accperiodscheme);
						cacheTools.set('reportcheckcontent',response.data.content);
                        //setInitData(self.props,pk_contrastrule,response.data.month,response.data.pk_accperiodmonth);
					}	
				}   
			}
		});
	}
 }

function setInitData(props, pk_contrastrule,month,pk_accperiodmonth) {
  let data = {
            display:month,value:pk_accperiodmonth
        };
    props.search.setSearchValByField(searchId,field,data)

}
