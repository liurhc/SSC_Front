import { createPage, ajax, base, toast,cacheTools } from 'nc-lightapp-front';
import clickSearchBtn from './searchBtnClick';
import tableButtonClick from './tableButtonClick';
let { NCPopconfirm, NCIcon } = base;
import { ISAUDIT } from '../config';
let pageId = '20022025_web_list';
let searchId = '20022025_query';
let tableId = '20022025_list';

export default function (props) {
	let page=this;
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	cacheTools.set('reportcheckappcode',appcode);
	props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta, page)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if(props.getUrlParam('jumpflag'))
				{
					let searchVal = cacheTools.get(searchId);
					clickSearchBtn(props,searchVal,'update');
				}
			}
		}
	)
}

function getListButtons (text, record, index, page) {
	let buttonArray = [];
	if (record.checkstatus.value === ISAUDIT.AUDIT) {
		buttonArray = ['unaudit'];
	} else {
		buttonArray = ['audit'];
	}
	return buttonArray;
}

function modifierMeta(props, meta, page) {
	//查询区
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		if (item.attrcode == 'pk_contrastrule') 
		{
			item.isMultiSelectedEnabled = true;
			item.queryCondition = ()=> 
			{
				return {'GridRefActionExt':'nccloud.web.gl.ref.ContrastRuleRefSqlBuilderForContarstReport'}
			}
		}
		else if (item.attrcode == 'accperiod')
		{
			if(cacheTools.get('reportcheckpk_accperiodscheme'))
			{
				item.queryCondition = ()=> 
				{
					return {"isadj":"N"}
				}
			}
			else
			{
				item.queryCondition = ()=> {
					return {
						"pk_accperiodscheme":cacheTools.get('reportcheckpk_accperiodscheme'),
						"isadj":"N"
					}
				}
			}
		}
		return item;
	})
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'month') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{cursor: 'pointer' }}
						onClick={() => {
							props.linkTo('/gl/contrastreportcheck/pages/card/index.html', {
								pagecode: '20022025_web_card',
								status: 'browse',
								id: record.pk_contrastreportcreate.value
							});
						}}
					>
						{record.month && record.month.value}
					</a>
				);
			};
		}
		return item;
	});
	meta[tableId].items.push({
		attrcode: 'opr',
		label: page.state.json['20022025-000009'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		className: 'table-opr',
		width:'160px',
		render: (text, record, index) => {
            return props.button.createOprationButton(getListButtons(text, record, index, page), {
                area: "contrastreportcheck_col",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index,tableId,page)
            });
        }
	});
	let multiLang = props.MutiInit.getIntl('2052');
	return meta;
}
