import {ajax,cacheTools,toast} from 'nc-lightapp-front';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal,flag) {
    if(searchVal){
        let pageId = '20022025_web_list';
        let searchId = '20022025_query';
        let tableId = '20022025_list';
        let data={
            querycondition: searchVal,
            pagecode: pageId,
            querytype:'tree',
        };
        //this.state.querydata=data;
        cacheTools.set(searchId, searchVal);
        ajax({
            url: '/nccloud/gl/contrast/contrastreportcheckquery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        if(flag=='update')
                        {

                        }
                        else if(flag=='simple')
                        {
                            let len = res.data[tableId].rows.length;
                            toast({ color: 'success', content: this.state.inlt&&this.state.inlt.get('20022025-000006',{count:len}) });/* 国际化处理： 查询成功，共,条。*/
                        }
                        props.table.setAllTableData(tableId, data[tableId]);
                    }else{
                        if(flag=='update')
                        {

                        }
                        else if(flag=='simple')
                        {
                            toast({ color: 'warning', content: this.state.json['20022025-000008'] });/* 国际化处理： 未查询出符合条件的数据！*/
                        }
                        props.table.setAllTableData(tableId, {rows:[]});
                    }
                    
                }
            }
        });
    }
    
};
