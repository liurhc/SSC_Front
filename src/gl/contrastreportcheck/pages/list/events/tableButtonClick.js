import { ajax, toast, base, cardCache, getMultiLang, cacheTools } from 'nc-lightapp-front';
let { NCMessage } = base;
let { setDefData, getDefData } = cardCache;
import { searchId, tableId } from '../config.js';
import clickSearchBtn from './searchBtnClick';
const tableButtonClick = (props, key, text, record, index, tableId, page) => {
    let data;
    let searchVal = cacheTools.get(searchId);
    switch (key) {
        // 表格操作按钮
        case 'audit' :
        if(record){
            ajax({
                url: '/nccloud/gl/contrast/contrastreportcheckaudit.do',
                data: {pk_contrastreportcreate: record.pk_contrastreportcreate.value},
                success: (res) => {
                let { success, data } = res;
                    if (success) {
                        if(data==true)
                        {
                            toast({color: 'success', content: page.state.json['20022025-000002']});/* 国际化处理： 审核成功*/
                            clickSearchBtn(props,searchVal,'update');
                        }
                        else
                        {
                            toast({color: 'warning', content: page.state.json['20022025-000003']});/* 国际化处理： 审核失败*/
                            clickSearchBtn(props,searchVal,'update');
                        } 
                    }
                }
            });
        }
        break;
    case 'unaudit' :
        if(record){
            ajax({
                url: '/nccloud/gl/contrast/contrastreportcheckunaudit.do',
                data: {pk_contrastreportcreate: record.pk_contrastreportcreate.value},
                success: (res) => {
                let { success, data } = res;
                    if (success) {
                        if(data==true)
                        {
                            toast({color: 'success', content: page.state.json['20022025-000004']});/* 国际化处理： 取消审核成功*/
                            clickSearchBtn(props,searchVal,'update');
                        }
                        else
                        {
                            toast({color: 'warning', content: page.state.json['20022025-000005']});/* 国际化处理： 取消审核失败*/
                            clickSearchBtn(props,searchVal,'update');
                        }
                    }
                }
            });
        }
        break;
        default:
            break;
    }
};
export default tableButtonClick;
