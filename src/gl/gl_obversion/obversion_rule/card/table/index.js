import React, { Component } from "react";
import { high, base, ajax, createPage, getMultiLang, promptBox, toast } from 'nc-lightapp-front';
import { getTableHeight } from '../../../../public/common/method.js';
const {
  NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button,
  NCRadio, NCBreadcrumb: Breadcrumb, NCRow: Row, NCCol: Col, NCTree: Tree,
  NCMessage: Message, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect,
  NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel, NCtabs
} = base;
const { NCTabs } = base;
const NCTabPane = NCTabs.NCTabPane;
class TableModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json: {},
      data_obj: {},
      pk_soblink: '',
      mainData: [],
      selIds: [],//选中行号
      checkedArray: [//各行选中判断

      ],
      selectArr: [],
      columns: []
    };
  }
  componentWillMount() {
    let callback = (json) => {
      this.setState({
        json: json,
        columns: [
          {
            title: (<div fieldid='rulecode' className="mergecells">{json['20020CONRL-000034']}</div>),
            dataIndex: "rulecode",
            key: "rulecode", /* 国际化处理： 规则编码*/
            render: (text, record, index) => (
              <div fieldid='rulecode'>
                {this.state.mainData[index].rulecode?this.state.mainData[index].rulecode:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid='rulename' className="mergecells">{json['20020CONRL-000035']}</div>),
            dataIndex: "rulename",
            key: "rulename",/* 国际化处理： 规则名称*/
            render: (text, record, index) => (
              <div fieldid='rulename'>
                {this.state.mainData[index].rulename?this.state.mainData[index].rulename:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid='FCConditionVO' className="mergecells">{json['20020CONRL-000036']}</div>),
            dataIndex: "FCConditionVO",
            key: "FCConditionVO",/* 国际化处理： 规则条件*/
            render: (text, record, index) => (
              <div fieldid='FCConditionVO'>
                {this.state.mainData[index].condition?this.state.mainData[index].condition:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: json['20020CONRL-000037'], dataIndex: "rulecode", key: "rulecode", fixed: 'right', width: '250px',/* 国际化处理： 操作*/
            render: (text, record, index) => (
              <div>
                <a href="javascript:;" onClick={this.handleCheck.bind(this, index)} style={{ 'margin-right': '5px' }}>{json['20020CONRL-000041']}</a>
                <a href="javascript:;" onClick={this.handleRepaire.bind(this, index)} style={{ 'margin-right': '5px' }}>{json['20020CONRL-000009']}</a>
                <a href="javascript:;" onClick={() => {
                  promptBox({
                    color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                    title: "请注意",                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                    content: '是否确认删除',             // 提示内容,非必输
                    noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                    noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                    beSureBtnName: "确定",          // 确定按钮名称, 默认为"确定",非必输
                    cancelBtnName: "取消",         // 取消按钮名称, 默认为"取消",非必输
                    hasCloseBtn: false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
                    beSureBtnClick: () => {
                      let mainData = this.state.mainData;
                      let data = { pk_convertrule: mainData[index].pk_convertrule };
                      let url = "/nccloud/gl/voucher/fcruledel.do";
                      let self = this;
                      ajax({
                        url,
                        data,
                        success: function (response) {
                          const { data, error, success } = response;
                          mainData.splice(index, 1);
                          self.setState({
                            mainData
                          })
                        }
                      });
                    },   // 确定按钮点击调用函数,非必输
                    zIndex: 200                     //遮罩的层级为zIndex，弹框默认为zIndex+1。默认为200，非比传项
                  })
                }}>{json['20020CONRL-000042']}</a>
              </div>
            )
          },
        ]
      }, () => { })
    }
    getMultiLang({ moduleId: '20020CONRL', domainName: 'gl', currentLocale: 'simpchn', callback });
  }
  handleDelModel = () => {
    promptBox({
      color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
      title: "请注意",                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
      content: '是否确认删除',             // 提示内容,非必输
      noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
      noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
      beSureBtnName: "确定",          // 确定按钮名称, 默认为"确定",非必输
      cancelBtnName: "取消",         // 取消按钮名称, 默认为"取消",非必输
      hasCloseBtn: false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
      beSureBtnClick: this.handleDel,   // 确定按钮点击调用函数,非必输
      zIndex: 200                     //遮罩的层级为zIndex，弹框默认为zIndex+1。默认为200，非比传项
    })
  }
  componentWillReceiveProps(nextProps) {
    let pk_soblink = nextProps.pk_soblink;
    if (!this.state.pk_soblink) {
      this.setState({
        pk_soblink
      })
      let self = this;
      if (pk_soblink) {
        let data = { pk_soblink: pk_soblink };
        let url = "/nccloud/gl/voucher/fcrulequery.do";
        ajax({
          url,
          data,
          success: function (response) {
            const { data, error, success } = response;
            if (success && data) {
              let checkedArray = [];
              for (let i = 0, len = data.length; i < len; i++) {
                data[i].key = i;
                checkedArray.push(false)
              }
              self.setState({
                mainData: data, checkedArray
              })
            } else {
              // toast({ content: '无请求数据', color: 'warning' });
            }
          }
        });
      }
    }
  }

  handleAdd = () => {
    if (!this.props.mainData.pk_soblink) {
      toast({ content: this.state.json['20020CONRL-000038'], color: 'warning' });/* 国际化处理： 无账簿关系,请保存*/
      return;
    }
    if (!this.props.mainData.pk_desorgbook.value) {
      toast({ content: this.state.json['20020CONRL-000039'], color: 'warning' });/* 国际化处理： 无对方账簿,请选择*/
      return;
    }
    if (!this.props.mainData.pk_srcorgbook.value) {
      toast({ content: this.state.json['20020CONRL-000040'], color: 'warning' });/* 国际化处理： 无本方账簿,请选择*/
      return;
    }
    this.props.linkTo('/gl/gl_obversion/obversion_rule/set/index.html', {
      pk_soblink: this.props.mainData.pk_soblink,
      pk_srcbook: this.props.mainData.pk_srcorgbook.value,
      pk_des: this.props.mainData.pk_desorgbook.value,
      pk_org: this.props.mainData.pk_org.value
    })
  }

  handleRepaire = (index) => {
    this.props.linkTo('/gl/gl_obversion/obversion_rule/set/index.html', {
      index: index + 1,
      pk_soblink: this.props.mainData.pk_soblink,
      pk_srcbook: this.props.mainData.pk_srcorgbook.value,
      pk_des: this.props.mainData.pk_desorgbook.value,
      pk_org: this.props.mainData.pk_org.value,
      type: 'edit'
    })
  }
  handleCheck = (index) => {
    this.props.linkTo('/gl/gl_obversion/obversion_rule/set/index.html', {
      index: index + 1,
      pk_soblink: this.props.mainData.pk_soblink,
      pk_srcbook: this.props.mainData.pk_srcorgbook.value,
      pk_des: this.props.mainData.pk_desorgbook.value,
      pk_org: this.props.mainData.pk_org.value,
      type: 'check'
    })
  }
  handleDel = () => {
    let { mainData, selectArr, checkedArray } = this.state;
    for (let i = 0, len = mainData.length; i < len; i++) {
      for (let j = 0, lenj = selectArr.length; j < lenj; j++) {
        if (selectArr[j].key == mainData[i].key) {
          let data = { pk_convertrule: selectArr[j].pk_convertrule };
          let url = "/nccloud/gl/voucher/fcruledel.do";
          let self = this;
          ajax({
            url,
            data,
            success: function (response) {
              const { data, error, success } = response;

            }
          });
          checkedArray[i] = false;
          mainData.splice(i, 1);
          len--;
          self.setState({
            mainData,
            checkedArray
          })
        }
      }
    }
    this.setState({
      // mainData,
      // checkedArray,
      selIds: []
    })
  }
  //处理多选
  onAllCheckChange = () => {//全选
    let self = this;
    let checkedArray = [];
    let listData = self.state.mainData.concat();
    let selIds = [];
    // let id = self.props.multiSelect.param;
    for (var i = 0; i < self.state.checkedArray.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
      selIds.push(i);
    }
    if (self.state.checkedAll) {
      selIds = []
    }
    self.setState({
      checkedAll: !self.state.checkedAll
    });
    // self.props.setCheckedArray(checkedArray);
    // self.props.setCheckedselIds(selIds);
    let mainData = this.state.mainData;
    let selectArr = [];

    for (let i = 0; i < selIds.length; i++) {
      selectArr.push(JSON.parse(JSON.stringify(mainData[selIds[i]])));
    }
    this.setState({
      selectArr, checkedArray, selIds
    })
    // self.props.getSelectData(selIds);
  };
  onCheckboxChange = (text, record, index) => {//单选
    let self = this;
    let allFlag = false;
    let selIds = self.state.selIds;
    // let id = self.props.postId;
    let checkedArray = self.state.checkedArray.concat();
    if (self.state.checkedArray[index]) {
      selIds.splice(record.key, 1);
      for (let i = 0, len = selIds.length; i < len; i++) {
        if (record.key == selIds[i]) {
          selIds.splice(i, 1)
          break;
        }
      }
    } else {
      selIds.push(record.key);
    }
    checkedArray[index] = !self.state.checkedArray[index];
    for (let i = 0; i < self.state.checkedArray.length; i++) {
      if (!checkedArray[i]) {
        allFlag = false;
        break;
      } else {
        allFlag = true;
      }
    }
    self.setState({
      checkedAll: allFlag
    });
    // self.props.setCheckedArray(checkedArray);
    // self.props.setCheckedselIds(selIds);
    let mainData = this.state.mainData;
    let selectArr = [];
    selIds = this.sortarr(selIds);
    for (let i = 0; i < selIds.length; i++) {
      selectArr.push(JSON.parse(JSON.stringify(mainData[selIds[i]])));
    }
    this.setState({
      selectArr, checkedArray, selIds
    })
  };
  renderColumnsMultiSelect(columns) {
    const { data, checkedArray, mainData } = this.state;
    let { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // multiSelect= "checkbox";
    // let indeterminate_bool1 = true;
    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArray.length;
      while (i--) {
        if (checkedArray[i]) {
          indeterminate_bool = true;
          break;
        }
      }
      let defaultColumns = [
        {
          title:  (<div fieldid="firstcol" className='checkbox-mergecells'>{
            <Checkbox
              className="table-checkbox"
              checked={this.state.checkedAll}
              indeterminate={indeterminate_bool && !this.state.checkedAll}
              onChange={this.onAllCheckChange}
            />
          }</div>),
          key: "checkbox",
          attrcode: "checkbox",
          dataIndex: "checkbox",
          width: "50px",
          render: (text, record, index) => {
            return (
              <div fieldid="firstcol">
              <Checkbox
                className="table-checkbox"
                checked={this.state.checkedArray[index]}
                onChange={this.onCheckboxChange.bind(this, text, record, index)}
              />
              </div>
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
  sortarr = (arr) => {//排序
    for (let i = 0; i < arr.length - 1; i++) {
      for (let j = 0; j < arr.length - 1 - i; j++) {
        if (arr[j] > arr[j + 1]) {
          var temp = arr[j];
          arr[j] = arr[j + 1];
          arr[j + 1] = temp;
        }
      }
    }
    return arr;
  }
  render() {
    let columns = this.renderColumnsMultiSelect(this.state.columns);
    let { mainData } = this.state;
    return (
      <div className='obr-table-mo'>
        <div className='header table-header table-buttons'>
          <div className='tittle nc-theme-form-label-c'>
            <h3>{this.state.json['20020CONRL-000043']}</h3>
          </div>
          <div style={{ float: 'right', 'padding-bottom': '3px', 'padding-right': '20px' }} className="btn-group table-button-type">
            <Button fieldid="handleAdd" colors="info" onClick={this.handleAdd}>{this.state.json['20020CONRL-000044']}</Button>
            <Button fieldid="handleDelModel" className='u-button  nc-button-wrapper button-primary' onClick={this.handleDelModel}>{this.state.json['20020CONRL-000042']}</Button>
          </div>
        </div>
        <div fieldid='fc_table-area'>
          <Table
            columns={columns}
            data={mainData}
            bodyStyle={{ height: getTableHeight(250) }}
            scroll={{ x: true, y: getTableHeight(250) }}
          />
        </div>
      </div>
    );
  }
}
TableModal = createPage({
  // initTemplate: initTemplate
})(TableModal);
export default TableModal;
TableModal.defaultProps = {
  prefixCls: "bee-table",
  multiSelect: {
    type: "checkbox",
    param: "key"
  }
};