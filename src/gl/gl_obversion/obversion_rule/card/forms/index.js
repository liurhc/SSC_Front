import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,getBusinessInfo,createPage,getMultiLang} from 'nc-lightapp-front';
import {InputItem} from '../../../../public/components/FormItems';
import OperatorDefaultRef from '../../../../refer/voucher/operatorDefaultRef/index';
// import AccountDefaultModelTreeRef from '../../../../../uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
import AccountBookTreeRef from '../../../../gl_cashFlows/pk_book/refer_pk_book';
import createScript from '../../../../public/components/uapRefer.js';
import { toast } from '../../../../public/components/utils.js';
import ReferLoader from '../../../../public/ReferLoader/index.js'
const { 
    NCDatePicker:DatePicker,NCButton: Button,
    NCRadio,NCBreadcrumb:Breadcrumb,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,
    NCForm, NCFormControl
    } = base;
const NCOption = NCSelect.NCOption;
const { Refer } = high;
class FormModal extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            mainData:{
                "pk_soblink":"",
                "autocreatevs":false,"initconvert":false,
                "realtimeflag":false,"combindetail":false,
                "isdifflag":false,"unequalstyle":'0',
                "pk_assessor":{"":"","value":"","scale":null},
                "pk_org":{"display":"","value":"","scale":null},
                "pk_repairsubj":{"display":"","value":"","scale":null},
                "pk_tally":{"display":"","value":"","scale":null},
                "code":"",
                "pk_prepared":{"display":"","value":"","scale":null},
                "pk_casher":{"display":null,"value":null,"scale":null},
                "pdstyle":'0',
                "pk_desorgbook":{"display":"","value":"","scale":null},
                "pk_srcorgbook":{"display":"","value":"",
                "scale":null}
            },
            isCheck:false,
            isDesPK:false,
        }
        this.businessInfo='';
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentDidMount(){
        if(getBusinessInfo()){
            if(getBusinessInfo().businessDate){
                this.businessInfo=getBusinessInfo().businessDate.split(' ')[0]
            }
        }
    }
    componentWillReceiveProps(nextProps){
        let mainData = nextProps.mainData;
        let isCheck = nextProps.isCheck;
        if(!isCheck){
            if(!mainData.pk_desorgbook.value){
                this.setState({
                    isDesPK:true
                })
            }else{
                this.setState({
                    isDesPK:false
                }) 
            }
        }
        this.setState({
            mainData,
            isCheck
        })
    }
    handleChange=()=>{}
    getParm=(parm)=>{
        let appUrl = decodeURIComponent(window.location.href).split('?');//分割查询
        if (appUrl && appUrl[1]){
            let appPrams = appUrl[1].split('/');
            if(appPrams && appPrams instanceof Array){
                let parmObj={};
                appPrams.forEach(item=>{
                    let key = item.split('=')[0];
                    let value = item.split('=')[1];
                    parmObj[key] = value;
                })
                return parmObj[parm];
            }
        }
    }
    render(){
        let {mainData,isCheck,isDesPK}=this.state;
        let mybook1;
		let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
		if(!this.state['myattrcode']){
			{createScript.call(this,referUrl,'myattrcode')}
		}else{	
            mybook1 =  (
				<Row>
					<Col xs={12} md={12}>
					{this.state['myattrcode']?(this.state['myattrcode'])(
						{   
                            fieldid:'desorgbook_ref',
							value: {'refname': mainData.pk_desorgbook.display, refpk: mainData.pk_desorgbook.value},
                            placeholder:this.state.json['20020CONRL-000010'],/* 国际化处理： 目的账簿*/
                            disabledDataShow:true,
                            queryCondition:{
                                "pk_accountingbook":mainData.pk_srcorgbook.value,
                                TreeRefActionExt:'nccloud.web.gl.fc.action.FCReblationBookRefSqlBuilder,nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                "appcode": this.props.getSearchParam('c'),
                            },
                            disabled:isCheck,
                            onChange:(v)=>{
                                // 根据选定的pk 实现过滤
                                mainData.pk_desorgbook.value=v.refpk
                                mainData.pk_desorgbook.display=v.refname
                                this.setState({
                                    mainData
                                })
                                this.props.getPk(mainData)
                                if(!v.refpk){
                                    this.setState({
                                        isDesPK:true
                                    })
                                }else{
                                    this.setState({
                                        isDesPK:false
                                    })
                                }
                            }
						}
					):<div/>}
					</Col>
				</Row>
			);	
		}
        return (
            <div style={{padding:"15px"}} className='top-form nc-theme-form-label-c'>
                <div className='top-form-books' fieldid='head_form-area'>
                    <Col className='form-item'></Col>
                    <Col className='form-item'>
                        <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000011']}</Col>
                        <Col lg={2} sm={2} xs={2} className='form-item-content-obversion' style={{'position':'relative'}}>
                            <AccountBookTreeRef 
                                fieldid='srcorgbook_ref'
                                value={ {'refname': mainData.pk_srcorgbook.display, refpk: mainData.pk_srcorgbook.value} }
                                attrcode={'attrcode1'}
                                placeholder={this.state.json['20020CONRL-000011']}/* 国际化处理： 来源账簿*/
                                disabledDataShow={true}
                                queryCondition={{
                                    "pk_accountingbook":mainData.pk_srcorgbook.value,
                                    pk_srcbook:this.getParm('pk'),
                                    "appcode": this.props.getSearchParam('c'),
                                    TreeRefActionExt:'nccloud.web.gl.fc.action.FCReblationBookRefSqlBuilder,nccloud.web.gl.ref.AccountBookRefSqlBuilder'
                                }}    
                                disabled={isCheck||isDesPK}
                                onChange={(v)=>{
                                    // 根据选定的pk 实现过滤
                                    mainData.pk_srcorgbook.value=v.refpk
                                    mainData.pk_srcorgbook.display=v.refname
                                    let self =this;
                                    ajax({
                                        url:'/nccloud/gl/glpub/queryFinanceOrg.do',
                                        data:{"pk_accountingbook":v.refpk,'needaccount':false},
                                        success: function(response) { 
                                            const { data, error, success } = response;
                                            if (success&&data) {
                                                mainData.pk_org.value = data.pk_org;
                                                mainData.pk_org.display=data.name;
                                                self.setState({
                                                    pk_org:data.pk_org
                                                })
                                            } else {
                                                
                                            }    
                                        }
                                    });
                                    this.setState({
                                        mainData
                                    })
                                    this.props.getPk(mainData)
                                }}
                            />
                            <Icon
                                className='iconfont icon-single_arrow'
                                style={{'position':'absolute',right:'-40px',top:'5px',color:'#3BBE78','font-size':'22px'}}
                            />
                            <Icon
                                className='uf uf-arrow-right-2'
                                style={{'position':'absolute',right:'-40px',top:'5px',color:'#3BBE78','font-size':'22px'}}
                            />
                        </Col>
                    </Col>
                    <Col className='form-item'>
                        <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000010']}：</Col>
                        <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                            {mybook1}
                        </Col>
                    </Col>
                </div>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'> <span style={{'color':'#E14C46'}}>*</span>{this.state.json['20020CONRL-000015']}：</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion pkvoucher-num'>
                        {/* <InputItem
                            type="customer"
                            name="scale"
                            disabled={isCheck||isDesPK}
                            value={mainData.code}
                            onChange={(v) => {
                                let testNUM = /[0-9]/
                                if(testNUM.test(v)){
                                    if(v.length<5){
                                        mainData.code=v;
                                    }
                                    else{
                                        mainData.code=v.slice(0,4);
                                        toast({ content: '请输入不超过4位的数字', color: 'warning' });
                                    }
                                }else{
                                    mainData.code='';
                                    toast({ content: '请输入数字', color: 'warning' });
                                }
                                // this.setState({
                                //     mainData
                                // })
                                this.props.getChangeData(mainData)
                            }}
                        /> */}
                        
                        <div className="explanation">
                            <NCFormControl 
                                name='explanation' 
                                type='text' 
                                fieldid='explanation'
                                disabled={isCheck||isDesPK}
                                value={mainData.code}
                                autocomplete="off"
                                onChange={(v) => {
                                    let testNUM = /[0-9]/;
                                    if(testNUM.test(v)){
                                        if(v.length<5){
                                            mainData.code=v;
                                        }
                                        else{
                                            mainData.code=v.slice(0,4);
                                            toast({ content: this.state.json['20020CONRL-000012'], color: 'warning' });/* 国际化处理： 请输入不超过4位的数字*/
                                        }
                                    }else{
                                        mainData.code='';
                                        toast({ content: this.state.json['20020CONRL-000013'], color: 'warning' });/* 国际化处理： 请输入数字*/
                                    }
                                    this.props.getChangeData(mainData)
                                }}
                            />
                        </div>
                    </Col>
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000016']}：</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                        <NCSelect fieldid='soblink'
                            name="fruit"
                            value={mainData.isdifflag?'1':'0'}
                            disabled={isCheck||isDesPK}
                            onChange={(v)=>{
                                if(v==0){
                                    mainData.isdifflag=false;
                                }else{
                                    mainData.isdifflag=true;
                                }
                                this.setState({
                                    mainData
                                })
                                this.props.getChangeData(mainData);
                            }}
                            >
                            <NCOption value="0" disabled={isCheck}>{this.state.json['20020CONRL-000017']}</NCOption>
                            <NCOption value="1" disabled={isCheck}>{this.state.json['20020CONRL-000018']}</NCOption>
                        </NCSelect>
                    </Col>
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000019']}：</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                        <NCSelect fieldid='prepareddate'
                            name="fruit"
                            value={Number(mainData.pdstyle)}
                            disabled={isCheck||isDesPK} 
                            onChange={(v)=>{
                                mainData.pdstyle=v;
                                this.setState({
                                    mainData
                                })
                                this.props.getChangeData(mainData);
                            }}
                            >
                            <NCOption value={0} disabled={isCheck||isDesPK}>{this.state.json['20020CONRL-000020']}</NCOption>
                            <NCOption value={1} disabled={isCheck||isDesPK}>{this.state.json['20020CONRL-000021']}</NCOption>
                        </NCSelect>
                    </Col>
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000198']}：</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion1'>
                        <div style={{'height':'30px'}}>
                            <NCSelect fieldid='dealmethod'
                                name="frui2t"
                                value={Number(mainData.unequalstyle)}
                                // className='form-item-select'
                                dropdownMatchSelectWidth={false}
                                disabled={isCheck||isDesPK}  
                                onChange={(v)=>{
                                    mainData.unequalstyle=v
                                    if(v==1){
                                        mainData.pk_repairsubj.value=''
                                        mainData.pk_repairsubj.display=''
                                    }
                                    this.setState({
                                        mainData
                                    })
                                    this.props.getChangeData(mainData);
                                }}
                                >
                                <NCOption value={0} disabled={isCheck||isDesPK} className='form-item-select1'>{this.state.json['20020CONRL-000014']}</NCOption>
                                <NCOption value={1} disabled={isCheck||isDesPK} className='form-item-select1'>{this.state.json['20020CONRL-000023']}</NCOption>
                            </NCSelect>
                            <ReferLoader fieldid='ceaccount_ref'
                                tag='AccountDefaultGridTreeRefCode'
                                refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
                                placeholder={this.state.json['20020CONRL-000014']}/* 国际化处理： 入补差科目*/
                                value={ {'refname': mainData.pk_repairsubj.display, refpk: mainData.pk_repairsubj.value} }
                                placeholder={this.state.json['20020CONRL-000014']}/* 国际化处理： 入补差科目*/
                                disabled={isCheck||mainData.unequalstyle==1||isDesPK} 
                                className='form-item-refer form-item-refer2'
                                onlyLeafCanSelect={true}
                                queryCondition={{"pk_accountingbook":mainData.pk_desorgbook.value,
                                    'dateStr':this.businessInfo,
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi',
                                }}
                                onChange={(v)=>{
                                    // 根据选定的pk 实现过滤
                                    mainData.pk_repairsubj.value=v.refpk
                                    mainData.pk_repairsubj.display=v.refname
                                    this.setState({
                                        mainData
                                    })
                                    this.props.getChangeData(mainData);
                                }}
                            />
                        </div>
                       
                        {/* <NCRadio.NCRadioGroup
                            name="fruit"
                            style={{'width':'250px'}}
                            selectedValue={Number(mainData.unequalstyle)}
                            onChange={(v)=>{
                                mainData.unequalstyle=v
                                if(v==1){
                                    mainData.pk_repairsubj.value=''
                                    mainData.pk_repairsubj.display=''
                                }
                                this.setState({
                                    mainData
                                })
                                this.props.getChangeData(mainData);
                            }}
                            >
                            <AccountDefaultModelTreeRef
                                    value={ {'refname': mainData.pk_repairsubj.display, refpk: mainData.pk_repairsubj.value} }
                                    placeholder="入补差科目"
                                    disabled={isCheck} 
                                    onlyLeafCanSelect={true}
                                    queryCondition={{"pk_accountingbook":mainData.pk_desorgbook.value,
                                    'dateStr':this.businessInfo
                                }}
                                    onChange={(v)=>{
                                        // 根据选定的pk 实现过滤
                                        mainData.pk_repairsubj.value=v.refpk
                                        mainData.pk_repairsubj.display=v.refname
                                        this.setState({
                                            mainData
                                        })
                                        this.props.getChangeData(mainData);
                                    }}
                            />
                            <NCRadio value={0} disabled={isCheck}>
                                
                            </NCRadio>
                            <NCRadio value={1} disabled={isCheck}>提示但不生成</NCRadio>
                        </NCRadio.NCRadioGroup> */}
                    </Col>
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000024']}：</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                        <OperatorDefaultRef fieldid='prepared_ref'
                          value={ {'refname': mainData.pk_prepared.display, refpk: mainData.pk_prepared.value} }
                          queryCondition={{
                            pk_accountingbook : mainData.pk_srcorgbook.value,
                            user_Type : '4',
                            isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',
                            appcode:'20020PREPA',
                            pk_desbook:mainData.pk_desorgbook.value,
                            GridRefActionExt:'nccloud.web.gl.ref.FCOperatorRefSqlBuilder'
                          }}
                          disabled={isCheck||isDesPK}
                          onChange={(v)=>{
                            // 根据选定的pk 实现过滤
                            mainData.pk_prepared.value=v.refpk
                            mainData.pk_prepared.display=v.refname
                            this.setState({
                                mainData
                            })
                            this.props.getChangeData(mainData);
                            }}  
                        />
                    </Col>
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000025']}：</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                        <OperatorDefaultRef fieldid='casher_ref'
                          value={ {'refname': mainData.pk_casher.display, refpk: mainData.pk_casher.value} }
                          queryCondition={{
                            pk_accountingbook : mainData.pk_srcorgbook.value,
                            user_Type : '4',
                            isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',
                            appcode:'20020VSIGN',
                            pk_desbook:mainData.pk_desorgbook.value,
                            GridRefActionExt:'nccloud.web.gl.ref.FCOperatorRefSqlBuilder'
                          }}
                          disabled={isCheck||isDesPK} 
                          onChange={(v)=>{
                            // 根据选定的pk 实现过滤
                            mainData.pk_casher.value=v.refpk
                            mainData.pk_casher.display=v.refname
                            this.setState({
                                mainData
                            })
                            this.props.getChangeData(mainData);
                            }}  
                        />
                    </Col>
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000026']}：</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                        <OperatorDefaultRef fieldid='checker_ref'
                          value={ {'refname': mainData.pk_assessor.display, refpk: mainData.pk_assessor.value} }
                          queryCondition={{
                            pk_accountingbook : mainData.pk_srcorgbook.value,
                            user_Type : '4',
                            isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',
                            appcode:'20020CHECK',
                            pk_desbook:mainData.pk_desorgbook.value,
                            GridRefActionExt:'nccloud.web.gl.ref.FCOperatorRefSqlBuilder'
                          }}
                          disabled={isCheck||isDesPK} 
                          onChange={(v)=>{
                            // 根据选定的pk 实现过滤
                            mainData.pk_assessor.value=v.refpk
                            mainData.pk_assessor.display=v.refname
                            this.setState({
                                mainData
                            })
                            this.props.getChangeData(mainData);
                            }}  
                        />
                    </Col>     
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000027']}：</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                        <OperatorDefaultRef fieldid='tallyer_ref'
                          value={ {'refname': mainData.pk_tally.display, refpk: mainData.pk_tally.value} }
                          queryCondition={{
                            pk_accountingbook : mainData.pk_srcorgbook.value,
                            user_Type : '4',
                            isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',
                            appcode:'20020TALLY',
                            pk_desbook:mainData.pk_desorgbook.value,
                            GridRefActionExt:'nccloud.web.gl.ref.FCOperatorRefSqlBuilder'
                          }}
                          disabled={isCheck||isDesPK} 
                          onChange={(v)=>{
                            // 根据选定的pk 实现过滤
                            mainData.pk_tally.value=v.refpk
                            mainData.pk_tally.display=v.refname
                            this.setState({
                                mainData
                            })
                            this.props.getChangeData(mainData);
                            }}  
                        />
                    </Col>
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000028']}:</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                        <NCSelect fieldid='realfc'
                            name="fruit"
                            value={mainData.realtimeflag}
                            disabled={isCheck||isDesPK} 
                            onChange={(v)=>{
                                mainData.realtimeflag=v
                                this.setState({
                                    mainData
                                })
                                this.props.getChangeData(mainData);
                            }}
                            >
                            <NCOption value={true} disabled={isCheck||isDesPK}>{this.state.json['20020CONRL-000029']}</NCOption>
                            <NCOption value={false} disabled={isCheck||isDesPK}>{this.state.json['20020CONRL-000030']}</NCOption>
                        </NCSelect>
                    </Col>
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000031']}:</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                        <NCSelect
                            name="fruit" fieldid='autotally'
                            value={mainData.autocreatevs}
                            disabled={isCheck||isDesPK} 
                            onChange={(v)=>{
                                mainData.autocreatevs=v
                                this.setState({
                                    mainData
                                })
                                this.props.getChangeData(mainData);
                            }}
                            >
                            <NCOption value={true} disabled={isCheck||isDesPK}>{this.state.json['20020CONRL-000029']}</NCOption>
                            <NCOption value={false} disabled={isCheck||isDesPK}>{this.state.json['20020CONRL-000030']}</NCOption>
                        </NCSelect>
                    </Col>
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000032']}:</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                        <NCSelect fieldid='initfc'
                            name="fruit"
                            value={mainData.initconvert}
                            disabled={isCheck||isDesPK} 
                            onChange={(v)=>{
                                mainData.initconvert=v
                                this.setState({
                                    mainData
                                })
                                this.props.getChangeData(mainData);
                            }}
                            >
                            <NCOption value={true} disabled={isCheck||isDesPK}>{this.state.json['20020CONRL-000029']}</NCOption>
                            <NCOption value={false} disabled={isCheck||isDesPK}>{this.state.json['20020CONRL-000030']}</NCOption>
                        </NCSelect>
                    </Col>
                </Col>
                <Col className='form-item'>
                    <Col lg={1} sm={1} xs={1} className='form-item-label-obversion'>{this.state.json['20020CONRL-000033']}:</Col>
                    <Col lg={2} sm={2} xs={2} className='form-item-content-obversion'>
                        <NCSelect fieldid='combinesame'
                            name="fruit"
                            value={mainData.combindetail}
                            disabled={isCheck||isDesPK} 
                            onChange={(v)=>{
                                mainData.combindetail=v
                                this.setState({
                                    mainData
                                })
                                this.props.getChangeData(mainData);
                            }}
                            >
                            <NCOption value={true} disabled={isCheck||isDesPK}>{this.state.json['20020CONRL-000029']}</NCOption>
                            <NCOption value={false} disabled={isCheck||isDesPK}>{this.state.json['20020CONRL-000030']}</NCOption>
                        </NCSelect>
                    </Col>
                </Col>
            </div>
        )
    }
}
FormModal = createPage({})(FormModal);
export default FormModal;
