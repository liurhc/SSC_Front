import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.less';
import { ajax, createPage, getMultiLang } from 'nc-lightapp-front';
import BooksModal from './books';
import FormModal from './forms';
import TableModal from './table';
class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {},
			pk_accountingbook: { display: '', value: '' },
			mainData: {
				"pk_soblink": "",
				"autocreatevs": false, "initconvert": false,
				"realtimeflag": false, "combindetail": false,
				"isdifflag": false, "unequalstyle": '1',
				"pk_assessor": { "": "", "value": "", "scale": null },
				"pk_org": { "display": "", "value": "", "scale": null },
				"pk_repairsubj": { "display": "", "value": "", "scale": null },
				"pk_tally": { "display": "", "value": "", "scale": null },
				"code": "",
				"pk_prepared": { "display": "", "value": "", "scale": null },
				"pk_casher": { "display": null, "value": null, "scale": null },
				"pdstyle": '0',
				"pk_desorgbook": { "display": "", "value": "", "scale": null },
				"pk_srcorgbook": {
					"display": "", "value": "",
					"scale": null
				},
				condition: ''
			},
			selectData: [],
			pk_soblink: '',
			isCheck: true//是否编辑态
		}
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {  })
		}
		getMultiLang({ moduleId: '20020CONRL', domainName: 'gl', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {
		//this.getData();
		let pk_accountingbook = this.props.getUrlParam('pk_accountingbook');
		let pk_soblink = this.props.getUrlParam('pk_soblink');
		let isCheck = this.props.getUrlParam('isCheck');
		if (pk_accountingbook) {
			let self = this;
			if (isCheck) {
				this.setState({
					isCheck: true
				})
			} else {
				this.setState({
					isCheck: false
				})
			}
			let data = { pk_accountingbook: pk_accountingbook };
			let url = "/nccloud/gl/voucher/relationquery.do";
			ajax({
				url,
				data,
				success: function (response) {
					const { data, error, success } = response;
					if (success && data) {
						let mainData;
						for (let i = 0, len = data.length; i < len; i++) {
							if (data[i].pk_soblink == pk_soblink) {
								mainData = data[i]
							}
						}
						self.setState({
							mainData,
							pk_soblink: pk_soblink
						})
					} else {
						// toast({ content: '无请求数据', color: 'warning' });
					}
				}
			});
		}
		if (this.props.getUrlParam('name')) {//新增时带入核算账簿
			let mainData = this.state.mainData;
			mainData.pk_srcorgbook = {
				display: this.props.getUrlParam('name'),
				value: this.props.getUrlParam('pk')
			}
			this.setState({
				mainData,
				isCheck: false
			})
		}
	}
	getPk = (data) => {
		if (!this.state.mainData.pk_srcorgbook.value) {
			this.setState({
				mainData: data
			})
		}
	}
	getChangeData = (data) => {
		this.setState({
			mainData: data
		})
	}
	changeIsCheck = (data) => {
		this.setState({
			isCheck: data
		})
	}
	render() {
		let { mainData, selectData, pk_soblink, isCheck } = this.state
		return (
			<div className="obversion_rule_table">
				<div className=' nc-bill-list'>
					<BooksModal
						mainData={mainData}
						getPk={this.getPk.bind(this)}
						isCheck={isCheck}
						changeIsCheck={this.changeIsCheck.bind(this)}
					/>
					<div className="nc-bill-search-area">
						<FormModal
							mainData={mainData}
							getChangeData={this.getChangeData.bind(this)}
							getPk={this.getPk.bind(this)}
							isCheck={isCheck}
						/>
					</div>
				</div>
				<div className="obversion_rule">
					<TableModal pk_soblink={pk_soblink} mainData={mainData} />
				</div>
			</div>
		);
	}
}
List = createPage({
	// initTemplate: initTemplate
})(List);
ReactDOM.render(<List />, document.querySelector('#app'));
