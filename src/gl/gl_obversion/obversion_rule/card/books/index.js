import React, {
    Component
} from 'react';
import ReactDOM from 'react-dom';
import {
    high,
    base,
    ajax,
    createPage,
    toast,
    promptBox,
    getMultiLang,
    createPageIcon
} from 'nc-lightapp-front';
const {
    NCFormControl: FormControl,
    NCDatePicker: DatePicker,
    NCButton: Button,
    NCRadio: Radio,
    NCBreadcrumb: Breadcrumb,
    NCRow: Row,
    NCCol: Col,
    NCTree: Tree,
    NCMessage: Message,
    NCIcon: Icon,
    NCLoading: Loading,
    NCTable: Table,
    NCSelect: Select,
    NCCheckbox: Checkbox,
    NCNumber,
    AutoComplete,
    NCDropdown: Dropdown,
    NCPanel: Panel
} = base;
import HeaderArea from '../../../../public/components/HeaderArea';
import AccountBookTreeRef from '../../../../gl_cashFlows/pk_book/refer_pk_book';
import createScript from '../../../../public/components/uapRefer.js';
import {
    EFAULT
} from 'constants';
const {
    Refer
} = high;
class BooksModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            mainData: {
                "pk_soblink": "",
                "autocreatevs": false,
                "initconvert": false,
                "realtimeflag": false,
                "combindetail": false,
                "isdifflag": "0",
                "unequalstyle": 1,
                "pk_assessor": {
                    "": "",
                    "value": "",
                    "scale": null
                },
                "pk_org": {
                    "display": "",
                    "value": "",
                    "scale": null
                },
                "pk_repairsubj": {
                    "display": "",
                    "value": "",
                    "scale": null
                },
                "pk_tally": {
                    "display": "",
                    "value": "",
                    "scale": null
                },
                "code": "",
                "pk_prepared": {
                    "display": "",
                    "value": "",
                    "scale": null
                },
                "pk_casher": {
                    "display": null,
                    "value": null,
                    "scale": null
                },
                "pdstyle": '0',
                "pk_desorgbook": {
                    "display": "",
                    "value": "",
                    "scale": null
                },
                "pk_srcorgbook": {
                    "display": "",
                    "value": "",
                    "scale": null
                }
            },
            isCheck: this.props.isCheck
        };

    }
    componentWillMount() {
        let callback = (json) => {   
            this.setState({
                json: json
            }, () => { })    
        }
        getMultiLang({
            moduleId: '20020CONRL',
            domainName: 'gl',
            currentLocale: 'simpchn',
            callback
        });
    }
    componentWillReceiveProps(nextProps) {
        let mainData = nextProps.mainData;
        let isCheck = nextProps.isCheck;
        this.setState({
            mainData,
            isCheck
        })
    }
    getParm = (parm) => {
        let appUrl = decodeURIComponent(window.location.href).split('?'); //分割查询
        if (appUrl && appUrl[1]) {
            let appPrams = appUrl[1].split('/');
            if (appPrams && appPrams instanceof Array) {
                let parmObj = {};
                appPrams.forEach(item => {
                    let key = item.split('=')[0];
                    let value = item.split('=')[1];
                    parmObj[key] = value;
                })
                return parmObj[parm];
            }
        }
    }
    handleSave = () => { //保存
        let {
            mainData
        } = this.state;
        let self = this;
        let data = mainData;
        // this.setState({
        //     pk_accountingbook:v.value
        // })
        if (!mainData.code) {
            toast({
                content: this.state.json['20020CONRL-000000'],
                color: 'warning'
            }); /* 国际化处理： 请输入编码*/
            return
        }
        let url = "/nccloud/gl/voucher/relationsave.do";
        ajax({
            url,
            data,
            success: function(response) {
                self.setState({
                    isLoading: false
                });
                const {
                    data,
                    error,
                    success
                } = response;
                if (success) {
                    // window.location.href="../rule_list/index.html"
                    self.props.changeIsCheck(true)
                    if (data) {
                        mainData.pk_soblink = data
                    }
                    toast({
                        color: 'success',
                        content: self.state.json['20020CONRL-000001']
                    }); /* 国际化处理： 保存成功*/
                    self.props.getPk(mainData)
                } else {
                    // toast({ content: '无请求数据', color: 'warning' });
                }
            }
        });
    }
    handleEdit = () => { //编辑
        this.props.changeIsCheck(false)
    }
    render() {
        let {
            mainData,
            isCheck
        } = this.state;
        return (
            <HeaderArea
					title={this.state.json["20020CONRL-000007"]}
                    initShowBackBtn={isCheck?true:false}
                    backBtnClick={()=>{
                        this.props.linkTo('/gl/gl_obversion/obversion_rule/list/index.html', {
                            pk_srcbook: this.props.mainData.pk_srcorgbook.value
                        })
                    }}
					btnContent={
						<div>
                    {!isCheck?<Button fieldid='save' className={isCheck?'display-none':''} colors="primary" onClick={this.handleSave.bind(this)}>{this.state.json['20020CONRL-000008']}</Button>:<span/>}
                    {isCheck?<Button fieldid='edit' className={!isCheck?'display-none':''} onClick={this.handleEdit.bind(this)}>{this.state.json['20020CONRL-000009']}</Button>:<span/>}
                    {!isCheck?<Button fieldid='cancel' className={isCheck?'display-none':''} 
                        onClick={()=>{
                            if(!isCheck){
                                promptBox({
                                    color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                    title: this.state.json['20020CONRL-000003'],                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 请注意*/
                                    content: this.state.json['20020CONRL-000004'],               // 是否显示取消按钮,，默认显示(false),非必输/* 国际化处理： 已编辑数据未保存，是否取消*/
                                    beSureBtnName: this.state.json['20020CONRL-000005'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
                                    cancelBtnName: this.state.json['20020CONRL-000006'],         // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
                                    hasCloseBtn:false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
                                    beSureBtnClick: ()=>{
                                        this.props.linkTo('/gl/gl_obversion/obversion_rule/list/index.html', {
                                            pk_srcbook: this.props.mainData.pk_srcorgbook.value
                                        })
                                    },   // 确定按钮点击调用函数,非必输
                                })
                            }else{
                                this.props.linkTo('/gl/gl_obversion/obversion_rule/list/index.html', {
                                    pk_srcbook: this.props.mainData.pk_srcorgbook.value
                                })
                            }
                        }}
                    >
                        {this.state.json['20020CONRL-000006']}
                    </Button>
                    :<span/>}
                </div>
					}
				/>
        )
    }
}
BooksModal = createPage({})(BooksModal);
export default BooksModal;