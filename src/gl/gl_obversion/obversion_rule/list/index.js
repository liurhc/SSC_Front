import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { base } from 'nc-lightapp-front';
import SerchModal from './search';
import ContentModal from './content';
import './index.less';
import {ajax,createPage,getMultiLang } from 'nc-lightapp-front';
class List extends Component {
	constructor(props) {
		super(props);
		this.state={
			json:{},
			pk_accountingbook:{display:'',value:''},
			mainData:[],
			selectData:[]
		}
	}
	componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
      }
	getPk=(v)=>{
		let self = this;
        let data = {
            pk_accountingbook: v.value
		};
		this.setState({
            pk_accountingbook:data
        })
		let url = "/nccloud/gl/voucher/relationquery.do";
        ajax({
            url,
            data,
            success: function(response) {
                self.setState({
                    isLoading: false
                });
                const { data, error, success } = response;
                if (success&&data) {
                    self.setState({
                        mainData:data
                    })
                    // self.getTableDetail(data[0])
                } else {
					self.setState({
                        mainData:[]
                    })
                    // toast({ content: '无请求数据', color: 'warning' });
                }
            }
		});
	}
	//增加删除
	getReturn=(data)=>{
		this.setState({
			mainData:data
		})
	}
	getSelect=(data)=>{
		this.setState({
			selectData:data
		})
	}
	render() {
		let {mainData,selectData,pk_accountingbook}=this.state;
		return (
            <div className='gl_obversion-list nc-bill-list'>
				<SerchModal getPk={this.getPk.bind(this)}
					getReturn={this.getReturn.bind(this)} 
					mainData={mainData} 
					selectData={selectData}
					pk_accountingbook={pk_accountingbook}
				/>
				<ContentModal mainData={mainData} getSelect={this.getSelect.bind(this)} pk_accountingbook={pk_accountingbook}/>
			</div>
		);
	}
}
List = createPage({
	// initTemplate: initTemplate
})(List);
ReactDOM.render(<List />, document.querySelector('#app'));
