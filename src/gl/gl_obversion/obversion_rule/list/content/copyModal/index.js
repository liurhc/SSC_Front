import React, { Component } from 'react';
import {high,base,ajax,toast,getMultiLang} from 'nc-lightapp-front';
const {
    NCModal:Modal,NCButton:Button,NCRow:Row,NCCol:Col,NCDiv
}=base
const { Refer } = high;
import FinanceOrgTreeRef from '../../../../../../uapbd/refer/org/FinanceOrgTreeRef';
export default class CopyModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            currtype:{display:'',value:''},//
            typeDisplay:[]
        };
        this.close = this.close.bind(this);
    }

    close() {
        this.props.getCopyShow(false);
    }
    handleConfirm=()=>{
        let currtype = this.state.currtype;
        let pk_soblinks = this.props.pk_soblink;
        let sourceorg = this.props.sourceorg;
        let desorgs = currtype.value;
        this.props.getCopyShow(false);
        let data = {
            desorgs,
            pk_soblinks,
            sourceorg
        }
        let self = this;
		let url = "/nccloud/gl/voucher/fcrulecopy.do";
        ajax({
            url,
            data,
            success: function(response) {
                const {data,success}=response;
                if(success&&data){
                    toast({
                        duration: 'infinity',          // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
                        color: 'info',     // 提示类别，默认是 "success",非必输
                        title: self.state.json['20020CONRL-000003'],      // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 请注意*/
                        content: data,   // 提示内容，批量操作要输入,非必输
                        TextArr:[self.state.json['20020CONRL-000045'],self.state.json['20020CONRL-000046'],self.state.json['20020CONRL-000047']],/* 国际化处理： 展开,收起,我知道了*/
                        groupOperationMsg:data, //数组的每一项，需要点击展开按钮显示的内容描述，非必输
                    })
                }
            }
		});
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.copyShow;
        this.setState({
            showModal
        })
    }
    render () {
        return (
        <div>
            <Modal
            show = { this.state.showModal }
            className='cashflow-small-modal-a' fieldid="query"
            onHide = { this.close } >
                <Modal.Header closeButton fieldid='header-area'>
                    <Modal.Title>{this.state.json['20020CONRL-000049']}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}>
                    <Row >
                        <Col lg={2} sm={2} xs={2} >
                            <h2>{this.state.json['20020CONRL-000050']}</h2>
                        </Col>
                        <Col lg={6} sm={6} xs={6} >
                            <FinanceOrgTreeRef fieldid='org-ref'
                                isMultiSelectedEnabled = {true}
                                value={this.state.typeDisplay}
                                queryCondition={{
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi',
                                    // GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder'
                                }}
                                onChange={(v)=>{
                                    let {currtype} = this.state;
                                    // currtype.display = v;
                                    currtype.value = [];
                                    let typeDisplay = v;
                                    for(let i=0;i<v.length;i++){
                                    currtype.value.push(v[i].refpk)
                                    }
                                    this.setState({
                                        currtype,
                                        typeDisplay
                                    }) 
                                }}
                            />
                        </Col>
                    </Row >
                  <div class="mark">
                    <div class="mark_inco"><i class="uf uf-i-c-2 mark_inco_i"></i></div>
                    <div class="footer-container">
                        <h1>{this.state.json['20020CONRL-000199']}</h1>
                        <p>{this.state.json['20020CONRL-000200']}</p>
                        <p>{this.state.json['20020CONRL-000201']}</p>
                    </div>
                  </div>
                  </NCDiv>
                </Modal.Body>

                <Modal.Footer>
                    <Button onClick={ this.handleConfirm } fieldid='confirm' colors="primary">{this.state.json['20020CONRL-000005']}</Button>
                    <Button onClick={ this.close } shape="border" fieldid='close' style={{marginRight: 6}}>{this.state.json['20020CONRL-000006']}</Button>
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
