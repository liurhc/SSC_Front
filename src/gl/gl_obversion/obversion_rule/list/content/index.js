import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,createPage,getMultiLang,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage} = viewModel;
import {getTableHeight } from '../../../../public/common/method.js';
const { 
    NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,
    NCRadio:Radio,NCBreadcrumb:Breadcrumb,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,NCDiv,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCPopconfirm:Popconfirm
    } = base;
import CopyModal from './copyModal';
const { Refer } = high;
class ContentModal extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            tableData:[],
            pk_accountingbook:{},
            copyShow:false,
            pk_soblink:[],//选中条目
            sourceorg:[],//来源账簿
            columns:[]
          }
    }
    componentWillMount(){
      let callback= (json) =>{
         this.setState({json:json,
        columns:[
          {
            title: (<div fieldid='srcbookcode' className="mergecells">{json['20020CONRL-000051']}</div>),/* 国际化处理： 来源财务核算账簿编码*/
            dataIndex: "srcbookcode",
            key: "srcbookcode",
          //   width:'6%',
            render: (text, record, index) => (
              <div fieldid='srcbookcode'>
                {this.state.tableData[index].srcbookcode?this.state.tableData[index].srcbookcode:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title:  (<div fieldid='srcbookcode' className="mergecells">{json['20020CONRL-000052']}</div>),/* 国际化处理： 来源财务核算账簿名称*/
            dataIndex: "pk_srcorgbook",
            key: "pk_srcorgbook",
          //   width:'6%',
            render: (text, record, index) => (
              <div fieldid='pk_srcorgbook'>
                {this.state.tableData[index].pk_srcorgbook.display?this.state.tableData[index].pk_srcorgbook.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid='srcbookcode' className="mergecells">{json['20020CONRL-000053']}</div>),/* 国际化处理： 目的财务核算账簿编码*/
            dataIndex: "desbookcode",
            key: "desbookcode",
          //   width:'6%',
            render: (text, record, index) => (
              <div fieldid='desbookcode'>
                {this.state.tableData[index].desbookcode?this.state.tableData[index].desbookcode:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid='srcbookcode' className="mergecells">{json['20020CONRL-000054']}</div>),/* 国际化处理： 目的财务核算账簿名称*/
            dataIndex: "pk_desorgbook",
            key: "pk_desorgbook",
          //   width:'10%',
            render: (text, record, index) => (
              <div fieldid='pk_desorgbook'>
                {this.state.tableData[index].pk_desorgbook.display?this.state.tableData[index].pk_desorgbook.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title:json['20020CONRL-000037'],/* 国际化处理： 操作*/
            dataIndex: "pk_currtype",
            key: "pk_currtype",
            width:'220px',
            fixed:'right',
            render: (text, record, index) => (
              <div>
                  <a size="sm" shape="border" colors="info" fieldid="link"
                  style={{'margin':"0",'cursor':'pointer'}}
                  onClick={
                    // this.handleRevise.bind(this,index)
                    ()=>{
                      this.props.linkTo('/gl/gl_obversion/obversion_rule/card/index.html', {
                        pk_accountingbook: this.props.pk_accountingbook.pk_accountingbook,
                        pk_soblink: this.state.tableData[index].pk_soblink,
                        index:index,
                        isCheck:true,
                        status:'browse'
                      })
                    }
                  }  
                  >{json['20020CONRL-000041']}</a>
                  <a size="sm" style={{'margin-left':'12px','cursor':'pointer'}} shape="border" colors="info" onClick={this.handleRevise.bind(this,index)} fieldid="update">{json['20020CONRL-000009']}</a>
                  <a size="sm" style={{'margin-left':'12px','cursor':'pointer'}} shape="border" colors="info" onClick={this.handleCopy.bind(this,index)} fieldid="copy">{json['20020CONRL-000049']}</a>
                  <Popconfirm content={json['20020CONRL-000055']} placement="top" id="aa" onClose={this.onDelete.bind(this,index)}>
                    <a size="sm" style={{'margin-left':'12px','cursor':'pointer'}} shape="border" colors="info" fieldid="del">{json['20020CONRL-000042']}</a>	
                  </Popconfirm>
              </div>
            )
          }
        ]
      },()=>{ })
          }
      getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let tableData = nextProps.mainData;
        let pk_accountingbook = nextProps.pk_accountingbook;
        for(let i=0,len=tableData.length;i<len;i++){
            tableData[i].key=i+1;
        }
        this.setState({
            tableData,
            pk_accountingbook
        })
    }
  //删除
    onDelete = (index) => {
      let tableData = this.state.tableData;
      let self = this;
      let url = "/nccloud/gl/voucher/relationdel.do";
      //let url='/User/Post';
      let data = {
        pk_soblink:tableData[index].pk_soblink
      };
      ajax({
        url,
        data,
        success: function(response) {
          const { data, error, success } = response;
          if (success) {
            tableData.splice(index, 1);
            self.setState({ tableData });
            // toast({ content: "删除成功", color: 'success' });
              // self.getTableDetail(data[0])
          } else {
              // toast({ content: '无请求数据', color: 'warning' });
          }
      }
      })
    };    
    getSelectedDataFunc=(data)=>{
        this.props.getSelect(data)
    }
    //修改
    handleRevise=(index)=>{
      setGlobalStorage('localStorage','rule_card_data',JSON.stringify({
        pk_accountingbook:this.props.pk_accountingbook.pk_accountingbook,
        index:index,
        isCheck:false
      }));
      // let url = `../../obversion_rule/card/index.html?pk_accountingbook=${this.state.pk_accountingbook.value}/index=${index}`
      // window.location.href= url;
      this.props.linkTo('/gl/gl_obversion/obversion_rule/card/index.html', {
        pk_accountingbook: this.props.pk_accountingbook.pk_accountingbook,
        pk_soblink: this.state.tableData[index].pk_soblink,
        index:index,
        status:'edit'
      })
    }
    //复制
    handleCopy=(index)=>{
      let tableData = this.state.tableData;
      let data = tableData[index];
      let pk_soblink = [data.pk_soblink];
      let sourceorg = data.pk_srcorgbook.value
      let copyShow = true;
      this.setState({
        copyShow,pk_soblink,sourceorg
      })
    }
    getCopyShow=(data)=>{
      this.setState({
        copyShow:data
      })
    }
    render(){
        let {tableData,copyShow,pk_soblink,sourceorg}=this.state;
        let columns = this.state.columns;
        let multiObj = {
            type: "checkbox"
        };
        return (
            <div className="cashAnalyseTable">
              <NCDiv fieldid="fcrule" areaCode={NCDiv.config.TableCom}>
                <Table
                    columns={columns}
                    data={tableData}
                    onRowClick={this.clickRow}
                    bodyStyle={{height:getTableHeight(80)}}
                    scroll={{ x: true, y: getTableHeight(80) }}
                    multiSelect={multiObj}
                    getSelectedDataFunc={this.getSelectedDataFunc}
                />
                </NCDiv>
                <CopyModal 
                  copyShow={copyShow}
                  getCopyShow={this.getCopyShow.bind(this)}
                  sourceorg={sourceorg}
                  pk_soblink={pk_soblink}
                />
            </div>
        
        );
    }
}
ContentModal = createPage({
	// initTemplate: initTemplate
})(ContentModal);
export default ContentModal;
