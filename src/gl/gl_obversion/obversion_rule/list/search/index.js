import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { high, base, ajax, createPage, getMultiLang, createPageIcon ,viewModel} from 'nc-lightapp-front';
let { setGlobalStorage} = viewModel;
import AccountBookTreeRef from '../../../../gl_cashFlows/pk_book/refer_pk_book';
const {
    NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button,
    NCRadio: Radio, NCBreadcrumb: Breadcrumb, NCRow: Row, NCCol: Col, NCTree: Tree,
    NCMessage: Message, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel
} = base;
import { toast } from '../../../../public/components/utils.js';
const { Refer } = high;
import HeaderArea from '../../../../public/components/HeaderArea';
class SeachModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            pk_accountingbook: { display: '', value: '' },
            mainData: [],
            selectData: []
        }
    }
    componentWillMount() {
        let callback = (json) => {
            this.setState({ json: json }, () => { })
        }
        getMultiLang({ moduleId: '20020CONRL', domainName: 'gl', currentLocale: 'simpchn', callback });
        //获取默认账簿
        let url = '/nccloud/platform/appregister/queryappcontext.do';
        let data = {
            appcode: this.props.getSearchParam('c')
        }
        let self = this;
        ajax({
            url,
            data,
            success: function (response) {
                const { data, error, success } = response;
                if (success && data) {
                    let pk_accountingbook = {
                        display: data.defaultAccbookName,
                        value: data.defaultAccbookPk
                    }
                    self.setState({
                        pk_accountingbook
                    })
                    self.props.getPk(pk_accountingbook);
                } else {
                }
            }
        });
    }
    componentWillReceiveProps(nextProps) {
        let mainData = nextProps.mainData;
        let selectData = nextProps.selectData;
        this.setState({
            selectData, mainData
        })
    }
    handleAdd = () => {//增加
        if (this.state.pk_accountingbook.value) {
            this.props.linkTo('/gl/gl_obversion/obversion_rule/card/index.html', {
                pk: this.state.pk_accountingbook.value,
                name: this.state.pk_accountingbook.display
            })
            setGlobalStorage('localStorage','rule_card_data', JSON.stringify({
                pk_accountingbook: this.state.pk_accountingbook.value,
                index: ''
            }));
        } else {
            toast({ content: this.state.json['20020CONRL-000056'], color: 'warning' });/* 国际化处理： 请选择账簿*/
        }
    }
    getParm = (parm) => {
        let appUrl = decodeURIComponent(window.location.href).split('?');//分割查询
        if (appUrl && appUrl[1]) {
            let appPrams = appUrl[1].split('&');
            if (appPrams && appPrams instanceof Array) {
                let parmObj = {};
                appPrams.forEach(item => {
                    let key = item.split('=')[0];
                    let value = item.split('=')[1];
                    parmObj[key] = value;
                })
                return parmObj[parm];
            }
        }
    }
    render() {
        let { pk_accountingbook } = this.state;
        return (
            <div>
                <HeaderArea
                    title={this.state.json['20020CONRL-000057']}
                    searchContent={
                        <div style={{ 'margin-left': '15px' }} >
                            <AccountBookTreeRef 
                                fieldid='accountbookref'
                                value={{ 'refname': pk_accountingbook.display, refpk: pk_accountingbook.value }}
                                disabledDataShow={true}

                                queryCondition={{
                                    "appcode": this.props.getSearchParam('c'),
                                    TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder'
                                }}
                                onChange={(v) => {
                                    // 根据选定的pk 实现过滤
                                    pk_accountingbook.value = v.refpk
                                    pk_accountingbook.display = v.refname
                                    this.setState({
                                        pk_accountingbook: pk_accountingbook
                                    })
                                    this.props.getPk(pk_accountingbook);
                                }}
                            />
                        </div>
                    }
                    btnContent={
                        <div className="header-button-area" fieldid='header-button-area'>
                            <Button colors="primary"
                                fieldid="search"
                                disabled={pk_accountingbook.value ? '' : true}
                                style={{ 'min-width': '60px', 'font-size': '13px' }}
                                onClick={this.handleAdd}>{this.state.json['20020CONRL-000058']}
                            </Button>
                        </div>
                    }
                    />
            </div>
        )
    }
}
SeachModal = createPage({})(SeachModal);
export default SeachModal;
