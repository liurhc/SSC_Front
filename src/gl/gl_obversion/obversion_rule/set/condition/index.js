import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,getBusinessInfo,getMultiLang } from 'nc-lightapp-front';
const { 
    NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,
    NCRadio:Radio,NCBreadcrumb:Breadcrumb,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel
    } = base;
import VoucherTypeDefaultGridRef from './../../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef';
// import AccountDefaultModelTreeRef from './../../../../../uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js';
// import AccountDefaultModelTreeRef from './../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import ExplanationModal from './ExplanationModal';
import CtlSysRef from '../../../../refer/voucher/CtlSysGridRef/index';
import ReferLoader from '../../../../public/ReferLoader/index.js' 
const { Refer } = high;
const NCOption = NCSelect.NCOption;
export default class ConditionComponent extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            explanationModalShow:false,//摘要
            explanation:{refname:'',refpk:''},
            pk_accountingbook:{display:'',value:''},
            mainData:{
                pk_system:{
                    operator:{display:0,value:0},
                    value:{},
                    field:'pk_system'
                },
                pk_vouchertype:{
                    operator:{display:0,value:0},
                    value:[],
                    field:'pk_vouchertype'
                },
                pk_accasoa:{
                    operator:{display:0,value:0},
                    value:[],
                    field:'pk_accasoa'
                },
                explanation:{
                    operator:{display:0,value:0},
                    value:{},
                    field:'explanation'
                }
            }
        }
        this.businessInfo='';
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentDidMount(){
        if(getBusinessInfo()){
            if(getBusinessInfo().businessDate){
                this.businessInfo=getBusinessInfo().businessDate.split(' ')[0]
            }
        }
    }
    componentWillReceiveProps(nextProps){
        // let propsTable=nextProps.mainData.conditions?nextProps.mainData.conditions:[];
        let mainData = nextProps.mainData.conditions;
        let localArray = ['pk_system','pk_vouchertype','pk_accasoa','explanation'];
        for(let i=0,len=localArray.length;i<len;i++){
            if(!mainData[localArray[i]]){
                if(localArray[i]=='pk_system'||localArray[i]=='explanation'){
                    mainData[localArray[i]]={
                        operator:{display:0,value:0},
                        value:{},
                        field:localArray[i]
                    }
                }
                if(localArray[i]=='pk_accasoa'||localArray[i]=='pk_vouchertype'){
                    mainData[localArray[i]]={
                        operator:{display:0,value:0},
                        value:[],
                        field:localArray[i]
                    }
                }
            }
        }
        if(!Array.isArray(mainData)){
            this.setState({
                mainData
            })
        }
    }
    handleShowExplanation=()=>{//摘要显示
        this.setState({
            explanationModalShow:true
        })
    }
    closeExplanationModal(data){//摘要隐藏
        this.setState({
            explanationModalShow:data
        })
    }
    getAppointData=(data)=>{//摘要
        let mainData =this.state.mainData;
        mainData.explanation.value.display=data.refname;
        mainData.explanation.value.value=data.refpk;
        this.props.getCondition(mainData);
        this.setState(
            {
                explanation:data,
                mainData
            }
        )
      }
    render(){
        let {mainData,explanationModalShow,explanation}=this.state;
        return (
            <div style={{padding:'5px'}} className="rule-condition">
                <h3>{this.state.json['20020CONRL-000065']}</h3>
                <Row>
                    <Col lg={1} sm={1} xs={1}>{this.state.json['20020CONRL-000063']}</Col>
                    <Col lg={2} sm={2} xs={2} className="rule-select">
                        <NCSelect
                            value={!isNaN(mainData.pk_system.operator.display)?Number(mainData.pk_system.operator.display):''}
                            disabled={this.props.stateStatus=='check'}
                            dropdownMatchSelectWidth={false}
                            onChange={(v)=>{
                                mainData.pk_system.operator.display=v;
                                mainData.pk_system.operator.value=v;
                                this.setState({mainData});
                                this.props.getCondition(mainData);
                            }}
                        >
                            <NCOption value={0}>{this.state.json['20020CONRL-000066']}</NCOption>
                            <NCOption value={1}>{this.state.json['20020CONRL-000067']}</NCOption>
                        </NCSelect>
                    </Col>
                    <Col lg={3} sm={3} xs={3}>
                        <CtlSysRef
                            placeholder={this.state.json['20020CONRL-000063']}/* 国际化处理： 凭证来源*/
                            value={{refname: mainData.pk_system.value.display,refpk: mainData.pk_system.value.value}}
                            queryCondition={{"pk_accountingbook":this.props.pk_srcbook,isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',}} 
                            onChange={(v)=>{
                                // 根据选定的pk 实现过滤
                                mainData.pk_system.value.value=v.refpk
                                mainData.pk_system.value.display=v.refname
                                this.setState({
                                    mainData
                                })
                                this.props.getCondition(mainData);
                            }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col lg={1} sm={1} xs={1}>{this.state.json['20020CONRL-000068']}</Col>
                    <Col lg={2} sm={2} xs={2} className="rule-select">
                        <NCSelect
                            value={(!isNaN(mainData.pk_vouchertype.operator.display))?Number(mainData.pk_vouchertype.operator.display):''}
                            disabled={this.props.stateStatus=='check'}
                            dropdownMatchSelectWidth={false}
                            onChange={(v)=>{
                                mainData.pk_vouchertype.operator.display=v;
                                mainData.pk_vouchertype.operator.value=v;
                                this.setState({mainData})
                                this.props.getCondition(mainData);
                            }}
                        >
                            <NCOption value={0}>{this.state.json['20020CONRL-000066']}</NCOption>
                            <NCOption value={1}>{this.state.json['20020CONRL-000067']}</NCOption>
                        </NCSelect>
                    </Col>
                    <Col lg={3} sm={3} xs={3}>
                        <VoucherTypeDefaultGridRef
                            value={ mainData.pk_vouchertype.value }        
                            disabled={this.props.stateStatus=='check'}    
                            isShowDisabledData={true}              
                            queryCondition={{ 
                                "pk_accountingbook":this.props.pk_srcbook,
                                pk_org:this.props.pk_srcbook,
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',
                            }}
                            isMultiSelectedEnabled={true}
                            onChange={(v)=>{
                                // 根据选定的pk 实现过滤
                                mainData.pk_vouchertype.value=v;
                                this.setState({
                                    mainData
                                })
                                this.props.getCondition(mainData);
                            }}                          
                        />   
                    </Col>
                </Row>
                <h3>{this.state.json['20020CONRL-000069']}</h3>
                <Row>
                    <Col lg={1} sm={1} xs={1}>{this.state.json['20020CONRL-000070']}</Col>
                    <Col lg={2} sm={2} xs={2} className="rule-select">
                        <NCSelect
                            value={!isNaN(mainData.pk_accasoa.operator.display)?Number(mainData.pk_accasoa.operator.display):''}
                            disabled={this.props.stateStatus=='check'}
                            dropdownMatchSelectWidth={false}
                            onChange={(v)=>{
                                mainData.pk_accasoa.operator.display=v;
                                mainData.pk_accasoa.operator.value=v;
                                this.setState({mainData});
                                this.props.getCondition(mainData);
                            }}
                        >
                            <NCOption value={0}>{this.state.json['20020CONRL-000066']}</NCOption>
                        </NCSelect>
                    </Col>
                    <Col lg={3} sm={3} xs={3}>
                        {/* <AccountDefaultModelTreeRef
                            value={ mainData.pk_accasoa.value }
                            queryCondition={{ 
                                "pk_accountingbook":this.props.pk_srcbook,
                                'dateStr':this.businessInfo,
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',
                            }}
                            disabled={this.props.stateStatus=='check'}
                            isMultiSelectedEnabled={true}
                            isShowDisabledData={true} 
                            onlyLeafCanSelect={true}
                            onChange={(v)=>{
                                // 根据选定的pk 实现过滤
                                mainData.pk_accasoa.value=v
                                this.setState({
                                    mainData
                                })
                                this.props.getCondition(mainData);
                            }}
                        />    */}
                        <ReferLoader
                            tag='AccountDefaultGridTreeRefCode'
                            refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
                            value={ mainData.pk_accasoa.value }
                            queryCondition={{ 
                                "pk_accountingbook":this.props.pk_srcbook,
                                'dateStr':this.businessInfo,
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',
                            }}
                            disabled={this.props.stateStatus=='check'}
                            isMultiSelectedEnabled={true}
                            isShowDisabledData={true} 
                            // onlyLeafCanSelect={true}
                            onChange={(v)=>{
                                // 根据选定的pk 实现过滤
                                mainData.pk_accasoa.value=v
                                this.setState({
                                    mainData
                                })
                                this.props.getCondition(mainData);
                            }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col lg={1} sm={1} xs={1}>{this.state.json['20020CONRL-000062']}</Col>
                    <Col lg={2} sm={2} xs={2} className="rule-select">
                        <NCSelect
                            value={ !isNaN(mainData.explanation.operator.display)?Number(mainData.explanation.operator.display):''}
                            // style={{ width: 200, marginRight: 6 }}
                            disabled={this.props.stateStatus=='check'}
                            dropdownMatchSelectWidth={false}
                            onChange={(v)=>{
                                mainData.explanation.operator.display=v;
                                mainData.explanation.operator.value=v;
                                this.setState({mainData});
                                this.props.getCondition(mainData);
                            }}
                        >
                            <NCOption value={0}>{this.state.json['20020CONRL-000066']}</NCOption>
                            <NCOption value={2}>{this.state.json['20020CONRL-000071']}</NCOption>
                        </NCSelect>
                    </Col>
                    <Col lg={3} sm={3} xs={3}>
                        <div className="explanation">
                            <FormControl name='explanation' type='text' value={explanation.refname}
                                autocomplete="off"
                                value = {mainData.explanation.value.display}
                                onChange={(v)=>{
                                    mainData.explanation.value.value=v;
                                    mainData.explanation.value.display=v;
                                    this.setState({
                                        mainData,
                                        explanation
                                    })
                                    this.props.getCondition(mainData);
                                }}
                            />
                            <span class="uf uf-3dot-v" style={{cursor: 'pointer'}}
                                onClick={this.handleShowExplanation.bind(this)}
                            ></span>
                            <ExplanationModal 
                                explanationModalShow={explanationModalShow}
                                closeExplanationModal={this.closeExplanationModal.bind(this)}
                                FinanceOrg={this.props.mainData.pk_org}
                                getAppointData={this.getAppointData.bind(this)}
                                pk_org={this.props.pk_orgSrc}
                            />
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}
