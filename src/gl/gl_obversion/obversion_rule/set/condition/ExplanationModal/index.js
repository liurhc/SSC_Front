import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,getMultiLang } from 'nc-lightapp-front';
// import createScript from '../../../public/components/uapRefer.js';
const { NCModal,NCButton,NCRadio,NCTable } = base;
const { Refer } = high;
import './index.less';
// import CashflowTreeRef from '../../../../uapbd/refer/fiacc/CashflowTreeRef';
export default class AppointModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            selectData:null,
            FinanceOrg:'',
            tableData:[],
            columns:[]
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json,
            columns:[
                {
                    title: (<div fieldid="index" className="mergecells">{json['20020CONRL-000060']}</div>),/* 国际化处理： 序号*/
                    dataIndex: "index",
                    key: "index",
                    width:'50px',
                    render: (text, record, index) => (
                      <div fieldid="index">{index}</div>
                    )
                },
                {
                  title: (<div fieldid="pk_debitaccasoa" className="mergecells">{json['20020CONRL-000015']}</div>),/* 国际化处理： 编码*/
                  dataIndex: "pk_debitaccasoa",
                  key: "pk_debitaccasoa",
                  width:'150px',
                  render: (text, record, index) => (
                    <div fieldid="pk_debitaccasoa">{record.refcode}</div>
                  )
                },
                {
                  title: (<div fieldid="pk_creditaccasoa" className="mergecells">{json['20020CONRL-000061']}</div>),/* 国际化处理： 名称*/
                  dataIndex: "pk_creditaccasoa",
                  key: "pk_creditaccasoa",
                  width:'150px',
                  render: (text, record, index) => (
                    <div fieldid="pk_creditaccasoa">{record.refname}</div>
                  )
                },
                {
                  title: "",
                  dataIndex: "pk_creditaccasoa",
                  key: "pk_creditaccasoa",
                  width:'50px',
                  render: (text, record, index) => (
                    <div className={this.state.tableData[index].isSelected?'':'correctHide'}>
                      <i className="uf uf-correct"></i>
                    </div>
                  )
                }
              ]
        },()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.explanationModalShow;
        let url = '/nccloud/fipub/ref/SummaryRef.do';
        let FinanceOrg = this.state.FinanceOrg;
        if(FinanceOrg!=nextProps.explanationModalShow){
            this.setState({
                FinanceOrg:nextProps.pk_org.value
            })
            let dataCount = 
			{
                    pid: "",
                    keyword: "",
                    queryCondition: {
	                    pk_org: nextProps.pk_org.value
                    },
                    pageInfo: {
                         pageSize: 10,
                         pageIndex: -1
                }
            }
			ajax({
				url: '/nccloud/fipub/ref/SummaryRef.do',
				data: dataCount,
				success: (res) => {
                    let { rows }=res.data
                    let tableData=rows;
                    for(let i=0,len=tableData.length;i<len;i++){
                        tableData[i].key=i
                        tableData[i].isSelected=false;
                    }
					this.setState({
                        tableData
                    })
				}
			});
        }
		
        this.setState({
            showModal
        })
    }
    close() {
        this.props.closeExplanationModal(false);
    }
    clickRow=(data,rowClassName)=>{
        let tableData = this.state.tableData;
        for(let i=0,len=tableData.length;i<len;i++){
          tableData[i].isSelected=false;
        }
        tableData[rowClassName].isSelected=true;
        this.setState({
          selectData:data,
          tableData
        })
        // this.props.getBusrelation(data);
    }
    handleConfirm=()=>{
        let selectData = this.state.selectData;
        this.props.getAppointData(selectData);
        this.props.closeExplanationModal(false);
    }

    render () {
        let {tableData,selectData}=this.state;
        let columns = this.state.columns;
        return (
            <div>
              <NCModal show = {
                  this.state.showModal
              }
              className='obs-set-explanModal'
			  backdrop={ false }
			  >
                <NCModal.Header>
                  <NCModal.Title>{this.state.json['20020CONRL-000062']}</NCModal.Title>
                  <i class="uf uf-close-bold" style={{
                      'position': 'absolute',
                      'right': '12px',
                      'top': '12px',
                      'color': '#777',
                      'cursor':'pointer'
                  }} onClick={this.close}></i>
                </NCModal.Header>

                <NCModal.Body >
                   <NCTable
                         columns={columns}
                         data={tableData}
                         onRowClick={this.clickRow}
                         scroll={{ x:false, y: 200 }}
                         style={{'overflow-x':false}}
                   />
                </NCModal.Body>

                <NCModal.Footer>
                 <NCButton onClick={this.handleConfirm} colors="primary">{this.state.json['20020CONRL-000005']}</NCButton>
                  <NCButton onClick={this.close}>{this.state.json['20020CONRL-000006']}</NCButton>
                </NCModal.Footer>

              </NCModal>
            </div>
        )
    }
}

