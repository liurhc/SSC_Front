import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,getMultiLang } from 'nc-lightapp-front';
const { 
    NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,
    NCRadio:Radio,NCBreadcrumb:Breadcrumb,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,
    NCButtonGroup:ButtonGroup
    } = base;
const { Refer } = high;
import { toast } from '../../../../public/components/utils.js';
import VoucherTypeDefaultGridRef from './../../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef';
import { getTableHeight} from "../../../../public/common/method.js";
export default class VoucherComponent extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            pk_accountingbook:{display:'',value:''},
            tableData:[],
            selectArr:[],//辅助核算选中项
            selIds:[],//多选需要
            checkedArray:[],//选中数组
            mainData:{
                conditions:[],
                "defaultsubj":{//默认科目
                    "display":"",
                    "value":"",
                    "scale":null
                },
            },
            columns:[],
        };
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json,
            columns : [
            { title: (<div fieldid="srcname" className="mergecells">{json['20020CONRL-000118']}</div>),
                 dataIndex: "srcname",
                  key: "srcname",
                   width:'250px',/* 国际化处理： 来源凭证类别名称*/
                    render: (text, record, index) => (
                        <div fieldid="srcname">
                        <VoucherTypeDefaultGridRef
                        fieldid="srcname"
                            value =  {{
                                refname: this.state.tableData[index].srccode?this.state.tableData[index].srccode.display:'', 
                                refpk: this.state.tableData[index].srccode?this.state.tableData[index].srccode.value:''
                            }}
                            disabled={this.props.stateStatus=='check'}
                            queryCondition={{
                                "pk_group":'',                        
                                GridRefActionExt:'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
                                pk_org:this.props.pk_srcbook,
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',
                            }}
                            onChange={(v)=>{
                                let {tableData} = this.state;
                                tableData[index].srccode={
                                    display:'',
                                    value:''
                                }
                                tableData[index].srccode.display= v.refname;	
                                tableData[index].srccode.value = v.refpk;	
                                // tableData[index].srccode.display = v.refcode;
                                let url = "/nccloud/gl/glpub/vtshortnamequery.do";
                                let self=this;
                                ajax({
                                    url,
                                    data:{
                                        pk_vouchertype:v.refpk
                                    },
                                    success: function(response) {
                                        const { data, error, success } = response;
                                        if (success&&data) {
                                            tableData[index].srcname = data;
                                            self.setState({
                                                tableData
                                            }) 
                                            self.state.mainData.vouchertyperef=tableData;
                                            self.props.getVouchertyperef(self.state.mainData);
                                        } else {
                                            // toast({ content: '无请求数据', color: 'warning' });
                                        }
                                    }
                                  });
                                                   
                            }}
                        />
                        </div>
                    )
                },
                { title: (<div fieldid="srccode" className="mergecells">{json['20020CONRL-000119']}</div>),
                 dataIndex: "srccode",
                  key: "srccode",
                  width:'250px',/* 国际化处理： 来源凭证类别简称*/
                    render: (text, record, index) => (
                        <div fieldid="srccode">
                            {this.state.tableData[index].srcname?this.state.tableData[index].srcname:<span>&nbsp;</span>}
                        </div>
                    )
                },
                { title: (<div fieldid="desname" className="mergecells">{json['20020CONRL-000120']}</div>),
                 dataIndex: "desname",
                  key: "desname",
                   width:'250px',/* 国际化处理： 目的凭证类别名称*/
                    render: (text, record, index) => (
                        <div fieldid="desname">
                        <VoucherTypeDefaultGridRef
                            fieldid="desname"
                            value =  {{
                                refname: this.state.tableData[index].descode?this.state.tableData[index].descode.display:'', 
                                refpk: this.state.tableData[index].descode?this.state.tableData[index].descode.value:''
                            }}
                            disabled={this.props.stateStatus=='check'}
                            queryCondition={{
                                "pk_group":'',                        
                                GridRefActionExt:'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
                                pk_org:this.props.pk_des,
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',
                            }}
                            onChange={(v)=>{
                                let {tableData} = this.state;
                                tableData[index].descode={
                                    display:'',
                                    value:''
                                }
                                tableData[index].descode.display= v.refname;	
                                tableData[index].descode.value = v.refpk;	
                                
                                let url = "/nccloud/gl/glpub/vtshortnamequery.do";
                                let self=this;
                                ajax({
                                    url,
                                    data:{
                                        pk_vouchertype:v.refpk
                                    },
                                    success: function(response) {
                                        const { data, error, success } = response;
                                        if (success&&data) {
                                            tableData[index].desname = data;   
                                            self.setState({
                                                tableData
                                            }) 
                                            self.state.mainData.vouchertyperef=tableData;
                                            self.props.getVouchertyperef(self.state.mainData);
                                        } else {
                                            // toast({ content: '无请求数据', color: 'warning' });
                                        }
                                    }
                                  });   
                                
                            }}  
                        />
                        </div>
                    )
                },
                { title:(<div fieldid="descode" className="mergecells">{json['20020CONRL-000121']}</div>),
                 dataIndex: "descode",
                  key: "descode",
                  width:'250px',/* 国际化处理： 目的凭证类别简称*/
                    render: (text, record, index) => (
                        <div fieldid="descode">
                            {this.state.tableData[index].desname?this.state.tableData[index].desname:<span>&nbsp;</span>}
                        </div>
                    )
                },
                { title: (<div fieldid="cz" className="mergecells">{json['20020CONRL-000037']}</div>),
                 dataIndex: "cz",
                  key: "cz",
                  width:'100px',/* 国际化处理： 操作*/
                    render: (text, record, index) => (
                        <a
                        fieldid="cz"
                        style={{'cursor':'pointer',display:this.props.stateStatus=='check'?'none':''}}
                         onClick={()=>{
                            let tableData = this.state.tableData;
                            tableData.splice(index,1);
                            this.state.mainData.vouchertyperef=tableData;
                            this.props.getVouchertyperef(this.state.mainData);
                        }}
                        disabled={this.props.stateStatus=='check'}
                        >{json['20020CONRL-000042']}</a>/* 国际化处理： 删除*/
                    )
                },
              ]    
        },()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let mainData = nextProps.mainData;
        let tableData = mainData.vouchertyperef;
        if(tableData){
            for(let i=0,len=tableData.length;i<len;i++){
                tableData[i].key=i
            }
        }
        this.setState({
            mainData,
            tableData
        })
    }
    handleAdd=()=>{
        let tableData = this.state.tableData;       
        let pk_soblink = this.props.mainData.pk_soblink;
        let obj={
            key:tableData.length+1,
            "pk_convertmap":"",
            "pk_convertrule":"",
            "pk_soblink":pk_soblink,
            "srccode":{
                "display":"",
                "value":"",
                "scale":null
            },
            "descode":{
                "display":"",
                "value":"",
                "scale":null
            },
            "srcname":"1",
            "desname":"2"
        }
        tableData.push(JSON.parse(JSON.stringify(obj)))
        this.setState({
            tableData
        })
        let mainData = this.state.mainData;
        mainData.vouchertyperef = tableData;
        this.props.getVouchertyperef(mainData);
    }
    handleImport=()=>{//导入
        let pk_soblink=this.state.mainData.pk_soblink;
        let data = {pk_srcbook:this.props.pk_srcbook,type:'0'};
        let url = "/nccloud/gl/voucher/fcrulerefimport.do";
        let self=this;
        ajax({
          url,
          data,
          success: function(response) {
              const { data, error, success } = response;
              if (success&&data) {
                self.setState({
                    tableData:data
                })
                let mainData = self.state.mainData;
                mainData.vouchertyperef = data;
                self.props.getVouchertyperef(mainData);
                toast({ content: self.state.json['20020CONRL-000108'], color: 'success' });/* 国际化处理： 导入成功*/
              } else {
                  toast({ content: self.state.json['20020CONRL-000109'], color: 'warning' });/* 国际化处理： 导入失败*/
              }
          }
        });
    }
    handleAutoMatch=()=>{//自动匹配
        let pk_soblink=this.state.mainData.pk_soblink;
        let data = {pk_desbook:this.props.pk_des,type:'0',refvos:this.state.tableData};
        let url = "/nccloud/gl/voucher/fcrulerefmatch.do";
        let self=this;
        ajax({
          url,
          data,
          success: function(response) {
              const { data, error, success } = response;
              if (success&&data) {
                self.setState({
                    tableData:data
                })
                let mainData = self.state.mainData;
                mainData.vouchertyperef = data;
                self.props.getVouchertyperef(mainData);
                toast({ content: self.state.json['20020CONRL-000110'], color: 'success' });/* 国际化处理： 自动匹配成功*/
              } else {
                  // toast({ content: '无请求数据', color: 'warning' });
                self.setState({
                    tableData:[]
                })
                let mainData = self.state.mainData;
                mainData.vouchertyperef = [];
                self.props.getVouchertyperef(mainData);
              }
          }
        });
    }
    handleDelArr=()=>{//批量删除
        let {selectArr,tableData} = this.state;
        for(let i=0,len=tableData.length;i<len;i++){
            for(let j=0,lenj=selectArr.length;j<lenj;j++){
                if(tableData[i].key==selectArr[j].key){
                    tableData.splice(i,1)
                    len--
                }
            }
        }
        for(let i=0,len=tableData.length;i<len;i++){
            tableData[i].key=i
        }
        this.setState({
            tableData,
            checkedArray:[],
            selIds:[]
        })
        toast({ content: this.state.json['20020CONRL-000111'], color: 'success' });/* 国际化处理： 删除成功*/
    }
    //处理多选
    onAllCheckChange = () => {//全选
        let self = this;
        let checkedArray = [];
        let listData = self.state.tableData.concat();
        let selIds = [];
        // let id = self.props.multiSelect.param;
        for (var i = 0; i < self.state.tableData.length; i++) {
        checkedArray[i] = !self.state.checkedAll;
        selIds.push(i);
        }
        if(self.state.checkedAll){
        selIds=[]
        }
        self.setState({
        checkedAll: !self.state.checkedAll,
        checkedArray: checkedArray,
        selIds: selIds
        });
        let tableData = this.state.tableData;
        let selectArr = [];
        
        for (let i=0;i<selIds.length;i++) {
        selectArr.push(JSON.parse(JSON.stringify(tableData[selIds[i]])));
        }
        this.setState({
            selectArr
        });
        // self.props.getSelectData(selIds);
    };
    onCheckboxChange = (text, record, index) => {//单选
        let self = this;
        let allFlag = false;
        let selIds = self.state.selIds;
        // let id = self.props.postId;
        let checkedArray = self.state.checkedArray.concat();
        if (self.state.checkedArray[index]) {
            selIds.splice(record.key,1);
        } else {
            selIds.push(record.key);
        }
        checkedArray[index] = !self.state.checkedArray[index];
        for (let i = 0; i < self.state.checkedArray.length; i++) {
            if (!checkedArray[i]) {
                allFlag = false;
                break;
            } else {
                allFlag = true;
            }
        }
        for (let i=0,len=selIds.length;i<len;i++) {
            for (let j=0;j<len-1-i;j++) {
                if(selIds[j]>selIds[j+1]){
                    let temp = selIds[j+1];
                    selIds[j+1] = selIds[j]
                    selIds[j]=temp
                }
            }
        }
        self.setState({
        checkedAll: allFlag,
        checkedArray: checkedArray,
        selIds: selIds
        });
        let tableData = this.state.tableData;
        let selectArr = [];
        for (let i=0;i<selIds.length;i++) {
        selectArr.push(JSON.parse(JSON.stringify(tableData[selIds[i]])));
        }
        this.setState({
            selectArr
        });
    };
    renderColumnsMultiSelect(columns) {
        const { data,checkedArray,tableData } = this.state;
        let { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        // multiSelect= "checkbox";
        // let indeterminate_bool1 = true;
        if (multiSelect && multiSelect.type === "checkbox") {
        let i = checkedArray.length;
        while(i--){
            if(checkedArray[i]){
                indeterminate_bool = true;
                break;
            }
        }
        let defaultColumns = [
            {
            title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
                <Checkbox
                className="table-checkbox"
                checked={this.state.checkedAll}
                indeterminate={indeterminate_bool&&!this.state.checkedAll}
                onChange={this.onAllCheckChange}
                />
            }</div>),
            key: "checkbox",
            attrcode: "checkbox",
            dataIndex: "checkbox",
            width: "30px",
            render: (text, record, index) => {
                return (
                    <div fieldid="firstcol">
                        <Checkbox
                            className="table-checkbox"
                            checked={this.state.checkedArray[index]}
                            onChange={this.onCheckboxChange.bind(this, text, record, index)}
                        />
                    </div>
                );
            }
            }
        ];
        columns = defaultColumns.concat(columns);
        }
        return columns;
    }
    render(){
        let {pk_accountingbook,tableData,mainData,selectArr}=this.state;
        let columns = this.renderColumnsMultiSelect(this.state.columns);
        return (
            <div style={{padding:'5px'}} className="rule_voucher">
                 <div style={{'text-align':'left','float':'left'}} fieldid='header-area'>
                    <h3 style={{'display':'inline-block'}} fieldid={`${this.state.json['20020CONRL-000122']}_title`}>{this.state.json['20020CONRL-000122']}：</h3>
                    <Checkbox 
                        checked={mainData.vouchertypeautomatch=='Y'?true:false}
                        disabled={this.props.stateStatus=='check'}
                        onChange={(v)=>{
                            if(v){
                                mainData.vouchertypeautomatch='Y'
                            }else{
                                mainData.vouchertypeautomatch='N'
                            }
                            this.props.getVouchertyperef(mainData);
                    }}
                    >{this.state.json['20020CONRL-000123']}</Checkbox>
                </div>
                <div style={{'text-align':'left'}}>
                    <div style={{'float':'right','padding-bottom':'5px','margin-bottom': '5px','margin-top': '-7px'}}>
                        <ButtonGroup>
                            <Button style={{'margin-right':'5px'}} colors="info" onClick={this.handleAdd} fieldid="handleAdd"
                                disabled={this.props.stateStatus=='check'}
                            >{this.state.json['20020CONRL-000114']}</Button>
                            <Button style={{'margin-right':'5px'}} disabled={this.props.stateStatus=='check'||selectArr.length==0} colors="info" onClick={this.handleDelArr} fieldid="handleDel">
                            {this.state.json['20020CONRL-000115']}
                            </Button>
                        </ButtonGroup>
                        <ButtonGroup>
                            <Button style={{'margin-right':'5px'}} colors="info" onClick={this.handleImport} fieldid="handleImport"
                                disabled={this.props.stateStatus=='check'}
                            >{this.state.json['20020CONRL-000116']}</Button>
                            <Button style={{'margin-right':'5px'}} colors="info" onClick={this.handleAutoMatch} fieldid="handleAutoMatch"
                                disabled={this.props.stateStatus=='check'}
                            >{this.state.json['20020CONRL-000117']}</Button>
                        </ButtonGroup>
                    </div>
                </div>
               
                <div>
                    <div fieldid="linkvoucher_table">
                    <Table
                        columns={columns}
                        data={tableData}
                        // onRowClick={this.clickRow}
                        scroll={{ x:false, y:getTableHeight(230)}}
                        bodyStyle={{'height':getTableHeight(230)}}
                    />
                    </div>
                </div>
            </div>
        )
    }
}
VoucherComponent.defaultProps = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
};
