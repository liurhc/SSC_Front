import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,createPage,getMultiLang,createPageIcon,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
const { 
    NCMessage:Message,NCButton: Button,NCStep 
    } = base;
const NCSteps = NCStep.NCSteps;
import './index.less';
import {toast} from '../../../public/components/utils.js';
import ConditionComponent from './condition';
import VoucherComponent from './voucher';
import SubjectComponent from './subject';
import OtherComponent from './other'
class SetComponent extends Component {
	constructor(props) {
		super(props);
        this.state = {
            json:{},
            current: 0,//tab控制
            stateStatus:'',//查看状态
            mainData:{
                "pk_convertrule":"",
                "curconvertstyle":'0',
                "curconverttime":'1',
                "defaultsubj":{//默认科目
                    "display":"",
                    "value":"",
                    "scale":null
                },
                "pk_soblink":"",
                "rulecode":null,
                "rulename":"",
                "subjcodesame":false,//科目编码一致
                "subjmatcherro":'1',
                "defaultrule":"",
                "sourcerule":null,
                "pk_org":{
                    "display":"",
                    "value":"",
                    "scale":null
                },
                "cashflow":"",
                "verify":"",
                "checkstyle":"",
                "pk_docmaptemplet":{
                    "display":"",
                    "value":"",
                    "scale":null
                },
                "vouchertypeautomatch":false,
                "condition":"",
                "conditions":{},
                "subjref":[],
                "vouchertyperef":[],
                "convertrate":null
            },
            pk_srcbook:'',
            pk_des:'',
            "pk_orgDes":{
                "display":"",
                "value":"",
                "scale":null
            },
            pk_orgSrc:{
                "display":"",
                "value":"",
                "scale":null
            },
            steps : []
        };
        
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json,
            steps:[{
                title: json['20020CONRL-000192'],description:json['20020CONRL-000196']
              }, {
                title: json['20020CONRL-000193'],description:''
              }, {
                title: json['20020CONRL-000194'],description:''
              },
              {
                title:json['20020CONRL-000195'],description:''
              }
            ]
        },()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
	componentDidMount() {
        let mainData = this.state.mainData;
        let getParm = this.props.getUrlParam;
        let self=this;
        if(getParm('index')==0||getParm('index')>0){//根据URl初始化
            let index = getParm('index')-1;
            let pk_soblink = getParm('pk_soblink');
            let data = {pk_soblink:pk_soblink};
            let stateStatus=getParm('type');
            let url = "/nccloud/gl/voucher/fcrulequery.do";
            let pk_srcbook = getParm('pk_srcbook');
            let pk_des = getParm('pk_des');
            mainData.pk_soblink = pk_soblink;
            this.setState({
                stateStatus,
                pk_srcbook,
                pk_des
            })
            ajax({
                url,
                data,
                success: function(response) {
                    const { data, error, success } = response;
                    if (success&&data) {
                        //修改回传参数
                        let mainData = data[index];
                        let conditions = mainData.conditions;
                        mainData.conditions=self.changToConditions(conditions);
                        self.setState({
                            mainData
                        })
                    } else {
                        // toast({ content: '无请求数据', color: 'warning' });
                    }
                }
            });
        }else{
            let pk_soblink = getParm('pk_soblink');
            let pk_srcbook = getParm('pk_srcbook');
            let pk_des = getParm('pk_des');
            mainData.pk_soblink = pk_soblink;
            this.setState({
                mainData,
                pk_srcbook,
                pk_des
            })
        }
        let pk_des = getParm('pk_des')
        if(pk_des){
            ajax({
                url:'/nccloud/gl/glpub/queryFinanceOrg.do',
                data:{"pk_accountingbook":pk_des,'needaccount':false},
                success: function(response) { 
                    const { data, error, success } = response;
                    if (success&&data) {
                        let pk_orgDes = self.state.pk_orgDes;
                        pk_orgDes.value=data.pk_org;
                        self.setState({
                            pk_orgDes
                        })
                    } else {
                        self.setState({
                            pk_orgDes:{value:''}
                        })
                    }    
                }
            });
        }
        let pk_srcbook = getParm('pk_srcbook');
        if(pk_srcbook){
            ajax({
                url:'/nccloud/gl/glpub/queryFinanceOrg.do',
                data:{"pk_accountingbook":pk_srcbook,'needaccount':false},
                success: function(response) { 
                    const { data, error, success } = response;
                    if (success&&data) {
                        let pk_orgSrc = self.state.pk_orgSrc;
                        pk_orgSrc.value=data.pk_org;
                        self.setState({
                            pk_orgSrc
                        })
                    } else {
                        self.setState({
                            pk_orgSrc:{value:''}
                        })
                    }    
                }
            });
        }
	}
	next() {
        const current = this.state.current + 1;
        this.setState({ current });
        let steps= this.state.steps;
        for(let i=0,len=steps.length;i<len;i++){
            if(i<current){
                steps[i].description=this.state.json['20020CONRL-000197']
            }else if(i==current){[
                steps[i].description=this.state.json['20020CONRL-000196']
            ]}else if (i>current){
                steps[i].description=''
            }
        }
        this.setState({steps})
      }
    prev() {
        const current = this.state.current - 1;
        this.setState({ current });
        let steps= this.state.steps;
        for(let i=0,len=steps.length;i<len;i++){
            if(i<current){
                steps[i].description=this.state.json['20020CONRL-000197']
            }else if(i==current){[
                steps[i].description=this.state.json['20020CONRL-000196']
            ]}else if (i>current){
                steps[i].description=''
            }
        }
        this.setState({steps})
    }
    
    alertDone() {
        Message.create({content: this.state.json['20020CONRL-000072'], color: 'info'});/* 国际化处理： 完成*/
    }
    getCondition=(data)=>{//条件
        let mainData = this.state.mainData;
        mainData.conditions = data;
        this.setState({
            mainData
        })
    }
    getVouchertyperef=(data)=>{//凭证
        this.setState({
            mainData:data
        })
    }
    getSubjref=(data)=>{//科目、规则条件
        this.setState({
            mainData:data
        })
    }
    handleSubmit=()=>{//提交
        let rule_card_data = JSON.parse(getGlobalStorage('localStorage','rule_card_data'));
        if(this.state.stateStatus=='check'){//查看时返回
            this.props.linkTo('/gl/gl_obversion/obversion_rule/card/index.html', {
                pk_soblink: this.state.mainData.pk_soblink,
                pk_accountingbook: this.state.pk_srcbook,
                isCheck:true
            })
            return;
        }
        let data = JSON.parse(JSON.stringify(this.state.mainData));
        let self = this;
        let url = "/nccloud/gl/voucher/fcrulesave.do";
        let conData=JSON.parse(JSON.stringify(data.conditions));
        let pk_vouchertypeArrDis = [],pk_vouchertypeArrVal=[],
        pk_accasoaArrDis = [],pk_accasoaArrVal=[];
        data.conditions=[];
        if(JSON.stringify(conData)!='{}'){
            if(conData.pk_vouchertype.value[0]){//修改pk_vouchertype为保存需要
                for(let i=0,len=conData.pk_vouchertype.value.length;i<len;i++){
                    pk_vouchertypeArrDis.push(conData.pk_vouchertype.value[i].refname)
                    pk_vouchertypeArrVal.push(conData.pk_vouchertype.value[i].refpk)
                }
                conData.pk_vouchertype.value={
                    value:String(pk_vouchertypeArrVal.join(',')),
                    display:String(pk_vouchertypeArrDis.join(','))
                }
                data.conditions.push(conData.pk_vouchertype);
            }else{
                conData.pk_vouchertype={
                    operator:{display:0,value:0},
                    value:{display:'',value:''},
                    field:'pk_vouchertype'
                }
                data.conditions.push(conData.pk_vouchertype);
            }
            if(conData.pk_accasoa.value[0]){//修改pk_accasoa为保存需要
                for(let i=0,len=conData.pk_accasoa.value.length;i<len;i++){
                    pk_accasoaArrDis.push(conData.pk_accasoa.value[i].refname)
                    pk_accasoaArrVal.push(conData.pk_accasoa.value[i].refpk)
                }
                conData.pk_accasoa.value={
                    value:String(pk_accasoaArrVal.join(',')),
                    display:String(pk_accasoaArrDis.join(','))
                } 
                data.conditions.push(conData.pk_accasoa);
            }else{
                conData.pk_accasoa={
                    operator:{display:0,value:0},
                    value:{display:'',value:''},
                    field:'pk_accasoa'
                }
                data.conditions.push(conData.pk_accasoa);
            }
            if(conData.explanation.value.value){//判断修改或新增
                data.conditions.push(conData.explanation);
            }
            if(conData.pk_system.value.value){//判断修改或新增
                data.conditions.push(conData.pk_system);
            }
        }
        data.pk_org.value=this.props.getUrlParam('pk_org');
        // toast({ color: 'success', content: '保存成功' }); 
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success) {
                    // let url = `../../obversion_rule/card/index.html?pk_accountingbook=${rule_card_data.pk_accountingbook}/index=${rule_card_data.index}`
                    // window.location.href= url;
                    // toast({content: '保存成功', color: 'success'});
                    toast({ color: 'success', content: self.state.json['20020CONRL-000001'] }); /* 国际化处理： 保存成功*/
                    setTimeout(
                        ()=>{
                            self.props.linkTo('/gl/gl_obversion/obversion_rule/card/index.html', {
                                pk_soblink: self.state.mainData.pk_soblink,
                                pk_accountingbook: self.state.pk_srcbook,
                                isCheck:true
                            })
                        },2000
                    )
                } else {
                    toast({ content: self.state.json['20020CONRL-000073'], color: 'warning' });/* 国际化处理： 保存失败*/
                }
            }
        });
    }
    handleBack=()=>{//取消
        this.props.linkTo('/gl/gl_obversion/obversion_rule/card/index.html', {
            pk_soblink: this.state.mainData.pk_soblink,
            pk_accountingbook: this.state.pk_srcbook,
            isCheck:true
        })
    }
    changToConditions=(data)=>{//转换获取值为条件需要
        let obj = {};
        let getArray = (data)=>{
            let len = data.value.split(',').length;
            let valueArray=data.value.split(',');
            let displayArray=data.display.split(',');
            let localobj = {};
            let newArr=[]
            for(let i=0;i<len;i++){
                localobj={
                    refpk:valueArray[i],
                    refname:displayArray[i]
                }
                newArr.push(Object.assign({},localobj))
            }
            return newArr;
        }
        for(let i=0,len=data.length;i<len;i++){
            obj[data[i].field]={
                field:data[i].field,
                operator:data[i].operator,
                value:data[i].field=='pk_vouchertype'||data[i].field=='pk_accasoa'?
                getArray(data[i].value)
                :data[i].value
            }
        }
        return obj;   
    }
	render() {
        let {current,mainData,pk_srcbook,pk_des,stateStatus,pk_orgDes,pk_orgSrc,steps}=this.state;
		return (
            <div className='obr-set-steps nc-bill-list nc-theme-area-bgc'>
                <div className="nc-bill-header-area">
                    <div className="header-title-search-area">
                        {createPageIcon()}
                        <h2 className='title-search-detail' >{this.state.json['20020CONRL-000075']}</h2>
                    </div>
                    <div className="header-button-area">
                        {
                            this.state.current > 0
                            &&
                            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()} fieldid="pre">
                            {this.state.json['20020CONRL-000076']}
                            </Button>
                        }
                        {
                            this.state.current === steps.length - 1
                            &&
                            <Button type="primary" colors='primary' onClick={this.handleSubmit} fieldid="submit">{stateStatus=='check'?this.state.json['20020CONRL-000074']:this.state.json['20020CONRL-000008']}</Button>/* 国际化处理： 返回,保存*/
                        }
                        {
                            this.state.current < steps.length - 1
                            &&
                            <Button type="primary" colors='primary' onClick={() => this.next()} fieldid="next">{this.state.json['20020CONRL-000077']}</Button>/* 国际化处理： 下一步*/
                        }
                        {
                            this.state.current !== steps.length - 1
                            &&
                            <Button type="primary" onClick={() => this.handleBack()} fieldid="back">{stateStatus=='check'?this.state.json['20020CONRL-000074']:this.state.json['20020CONRL-000006']}</Button>/* 国际化处理： 返回,取消*/
                        }
                        {
                            this.state.current === steps.length - 1
                            &&
                            <Button  fieldid="link" type="primary" colors='primary' className={stateStatus=='check'?'rule-hide':''} onClick={()=>{
                                this.props.linkTo('/gl/gl_obversion/obversion_rule/card/index.html', {
                                    pk_soblink: this.state.mainData.pk_soblink,
                                    pk_accountingbook: this.state.pk_srcbook,
                                    isCheck:true
                                })
                            }}>{this.state.json['20020CONRL-000006']}</Button>/* 国际化处理： 取消*/
                        }
                    </div>
                </div>
                <div className="rule-steps">
                    <NCSteps current={current}>
                        {steps.map(item => <NCStep key={item.title} title={item.title} description={item.description} />)}
                    </NCSteps>
                    <div className="steps-content nc-theme-form-label-c nc-theme-area-split-bc">
                        {/* {steps[this.state.current].content} */}
                        <div className={this.state.current==0?"":'rule-hide'}>
                            <OtherComponent mainData={mainData}
                                getSubjref={this.getSubjref.bind(this)} pk_srcbook={pk_srcbook}
                                stateStatus={stateStatus} pk_des={pk_des} pk_orgDes={pk_orgDes}
                            />
                        </div>
                        <div className={this.state.current==1?"":'rule-hide'}>
                            <ConditionComponent  getCondition={this.getCondition.bind(this)}
                                mainData={mainData} stateStatus={stateStatus}
                                pk_srcbook={pk_srcbook} pk_des={pk_des}
                                pk_orgDes={pk_orgDes}
                                pk_orgSrc={pk_orgSrc}
                            /> 
                        </div>
                        <div className={this.state.current==2?"":'rule-hide'}>
                            <VoucherComponent mainData={mainData} pk_srcbook={pk_srcbook}
                                getVouchertyperef={this.getVouchertyperef.bind(this)} pk_des={pk_des}
                                stateStatus={stateStatus}
                            />
                        </div>
                        <div className={this.state.current==3?"":'rule-hide'}>
                            <SubjectComponent mainData={mainData} pk_srcbook={pk_srcbook}
                                getSubjref={this.getSubjref.bind(this)} pk_des={pk_des} stateStatus={stateStatus}
                            />
                        </div>
                    </div>
                </div>
            </div>
		);
	}
}
SetComponent = createPage({
	// initTemplate: initTemplate
})(SetComponent);
ReactDOM.render(<SetComponent />, document.querySelector('#app'));
