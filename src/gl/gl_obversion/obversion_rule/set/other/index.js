import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,getMultiLang } from 'nc-lightapp-front';
import {InputItem,} from '../../../../public/components/FormItems';
import DocmapGridRef from '../../../../refer/voucher/DocmapGridRef/index';
import { toast } from '../../../../public/components/utils.js';
import RateModal from './rateModal/index';
const { 
    NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,
    NCRadio,NCBreadcrumb:Breadcrumb,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel
    } = base;
const { Refer } = high;
export default class OtherComponent extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            isRateShow:null,
            pk_desbook:'',
            codeflag:true,
            codeflag1:true,
            mainData:{
                conditions:[],
                "pk_docmaptemplet":
                {
                    "display":"",
                    "value":"",
                    "scale":null
                },
            }
        }
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let mainData = nextProps.mainData;
        this.setState({
            mainData
        })
        let pk_srcbook = nextProps.pk_srcbook;
        let pk_desbook = nextProps.pk_des;
        let data = {pk_srcbook:pk_srcbook,pk_desbook:pk_desbook};
        let url = "/nccloud/gl/voucher/islocalcurrsame.do";
        let self=this;
        if(pk_desbook==this.state.pk_desbook){
            return
        }
        ajax({
          url,
          data,
          success: function(response) {
              const { data, error, success } = response;
              if (success) {
                self.setState({
                    isRateShow:data,
                    pk_desbook:pk_desbook
                })
              } else {
                  // toast({ content: '无请求数据', color: 'warning' });
              }
          }
        });
    }
    GetLength = (s)=> //获取字节长度
    {
        let len = 0;
        for(var i=0; i<s.length; i++) 
        {
            var c = s.substr(i,1);
            var ts = escape(c);
            if(ts.substring(0,2) == "%u") 
            {
                len+=2;
            } else 
            {
                len+=1;
            }
        }
        return len;
    }
    getRateData=(data)=>{//获取返回汇率值
        let mainData=this.state.mainData;
        mainData.convertrate=data;
        this.props.getSubjref(mainData);
    }
    render(){
        let {mainData,isRateShow}=this.state;
        return (
            <div style={{padding:'5px','line-height':'35px'}} className='obr-set-other'>
                <Row>
                    <Col lg={2} sm={2} xs={2}> <span style={{'color':'#e14c46'}}>*</span>{this.state.json['20020CONRL-000034']}：</Col>
                    <Col lg={3} sm={3} xs={3} className='pkvoucher-num'>
                        <div className="explanation">
                            <FormControl name='explanation' type='text' fieldid="rulecode"
                                name="scale"
                                disabled={this.props.stateStatus=='check'}
                                value={mainData.rulecode}
                                onChange={(v) => {
                                    let testNUM = new RegExp("^[0-9]*$");
                                    if(testNUM.test(v)){
                                        if(v.length<5){
                                            mainData.rulecode=v;
                                        }else{
                                            mainData.rulecode=v.substr(0,4);
                                            if(this.state.codeflag){
                                                this.setState({
                                                    codeflag:false
                                                })
                                                toast({ content: this.state.json['20020CONRL-000012'], color: 'warning' });/* 国际化处理： 请输入不超过4位的数字*/
                                            }
                                        }
                                    }else{
                                        if(mainData.rulecode){
                                            mainData.rulecode=v.substr(0,mainData.rulecode.length);
                                        }
                                        if(v.length==1){
                                            mainData.rulecode=''
                                        }
                                        if(this.state.codeflag1){
                                            this.setState({
                                                codeflag1:false
                                            })
                                            toast({ content: this.state.json['20020CONRL-000013'], color: 'warning' });/* 国际化处理： 请输入数字*/
                                        }
                                    }
                                    this.setState({
                                        mainData
                                    })
                                    this.props.getSubjref(mainData)
                                }}
                            />
                        </div>
                    </Col>
                </Row> 
                <Row>
                    <Col lg={2} sm={2} xs={2}><span style={{'color':'#e14c46'}}>*</span>{this.state.json['20020CONRL-000035']}：</Col>
                    <Col lg={3} sm={3} xs={3}>
                    <InputItem
                        fieldid="rulename"
                        type="customer"
                        name="scale"
                        disabled={this.props.stateStatus=='check'}
                        defaultValue={mainData.rulename}
                        onChange={(v) => {
                            if(this.GetLength(v)<31){
                                mainData.rulename=v;
                            }else{
                                mainData.rulename='';
                                toast({ content: this.state.json['20020CONRL-000078'], color: 'warning' });/* 国际化处理： 请不要输入超过30字节*/
                            }
                            this.setState({
                                mainData
                            })
                            this.props.getSubjref(mainData)
						  }}
					    />
                    </Col>
                </Row> 
                <Row style={{'position':'relative'}}>
                    <Col lg={2} sm={2} xs={2}>{this.state.json['20020CONRL-000079']}：</Col>
                    <Col lg={3} sm={3} xs={3}>
                        <NCRadio.NCRadioGroup
                        name="curconvertstyle"
                        disabled={this.props.stateStatus=='check'}
                        selectedValue={mainData.curconvertstyle}
                        onChange={(v)=>{
                            mainData.curconvertstyle=v;
                            this.props.getSubjref(mainData)
                        }}
                        >
                        <NCRadio value={'0'} disabled={this.props.stateStatus=='check'}>{this.state.json['20020CONRL-000080']}</NCRadio>
                        <NCRadio value={'2'} disabled={this.props.stateStatus=='check'}>{this.state.json['20020CONRL-000081']}{this.state.json['20020CONRL-000082']}</NCRadio>
                        <NCRadio value={'1'} disabled={this.props.stateStatus=='check'}>{this.state.json['20020CONRL-000081']}{this.state.json['20020CONRL-000083']}</NCRadio>
                    </NCRadio.NCRadioGroup>         
                    </Col>
                    <Col lg={2} sm={2} xs={2} style={{'position': 'absolute','top':'35px','right':'15px'}}>
                        {isRateShow=='Y'&&mainData.curconvertstyle!='0'?<RateModal 
                            getRateData={this.getRateData.bind(this)}
                            mainData={mainData.convertrate}
                            pk_desbook = {this.props.pk_des}
                            stateStatus={this.props.stateStatus}
                            RateModal
                        />:''}
                    </Col>
                </Row> 
                <Row>
                    <Col lg={2} sm={2} xs={2}>{this.state.json['20020CONRL-000084']}：</Col>
                    <Col lg={3} sm={3} xs={3}>
                        <NCRadio.NCRadioGroup
                        name="curconverttime"
                        selectedValue={Number(mainData.curconverttime)}
                        disabled={this.props.stateStatus=='check'}
                        onChange={(v)=>{
                            mainData.curconverttime=v
                            this.props.getSubjref(mainData)
                        }}
                        >
                        <NCRadio value={1} disabled={this.props.stateStatus=='check'}>{this.state.json['20020CONRL-000019']}</NCRadio>
                        <NCRadio value={2} disabled={this.props.stateStatus=='check'}>{this.state.json['20020CONRL-000085']}</NCRadio>
                        </NCRadio.NCRadioGroup>  
                    </Col>
                </Row> 
                <Row>
                    <Col lg={2} sm={2} xs={2}>{this.state.json['20020CONRL-000086']}：</Col>
                        <Col lg={3} sm={3} xs={3}>
                            {/* <span>{mainData.pk_docmaptemplet.display}</span> */}
                            <DocmapGridRef
                                fieldid="pk_docmaptemplet"
                                value =  {{
                                    refname: mainData.pk_docmaptemplet.display, 
                                    refpk: mainData.pk_docmaptemplet.value
                                }}
                                disabled={this.props.stateStatus=='check'}
                                queryCondition={{
                                    "pk_accountingbook":this.props.pk_srcbook,
                                    pk_src_crop:this.props.mainData.pk_org.value,
                                    pk_des_crop:this.props.pk_orgDes.value,
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi',
                                }}
                                onChange={(v)=>{
                                    let {mainData} = this.state;
                                    mainData.pk_docmaptemplet.display = v.refname;	
                                    mainData.pk_docmaptemplet.value = v.refpk;	
                                    this.setState({
                                        mainData
                                    })     
                                    this.props.getSubjref(this.state.mainData);                 
                                }
                            }
                        />
                    </Col>
                </Row>
                <Row>
                    <Col lg={2} sm={2} xs={2}>{this.state.json['20020CONRL-000087']}：</Col>
                    <Col lg={3} sm={3} xs={3}>
                        <Checkbox 
                            checked={mainData.defaultrule=="Y"?true:''}
                            disabled={this.props.stateStatus=='check'}
                            onChange={(v)=>{
                                if(v){
                                    mainData.defaultrule="Y"
                                }else{
                                    mainData.defaultrule='N'
                                }
                                this.setState({
                                    mainData
                                })
                                this.props.getSubjref(mainData)
                            }}
                        >{this.state.json['20020CONRL-000088']}</Checkbox>
                        <Checkbox 
                            checked={mainData.cashflow=="Y"?true:false}
                            disabled={this.props.stateStatus=='check'}
                            onChange={(v)=>{
                                if(v){
                                    mainData.cashflow="Y"
                                }else{
                                    mainData.cashflow='N'
                                }
                                this.setState({
                                    mainData
                                })
                                this.props.getSubjref(mainData)
                            }}
                        >{this.state.json['20020CONRL-000089']}</Checkbox>
                        <Checkbox 
                            checked={mainData.checkstyle=="Y"?true:false}
                            disabled={this.props.stateStatus=='check'}
                            onChange={(v)=>{
                                if(v){
                                    mainData.checkstyle="Y"
                                }else{
                                    mainData.checkstyle='N'
                                }
                                this.setState({
                                    mainData
                                })
                                this.props.getSubjref(mainData)
                            }}
                        >{this.state.json['20020CONRL-000090']}</Checkbox>
                        <Checkbox 
                            checked={mainData.verify=="Y"?true:false}
                            disabled={this.props.stateStatus=='check'}
                            onChange={(v)=>{
                                if(v){
                                    mainData.verify="Y"
                                }else{
                                    mainData.verify='N'
                                }
                                this.setState({
                                    mainData
                                })
                                this.props.getSubjref(mainData)
                            }}
                        >{this.state.json['20020CONRL-000091']}</Checkbox>
                    </Col>
                </Row> 
                {/* <Row>
                    <Col lg={3} sm={3} xs={3}>
                        <Checkbox 
                            checked={mainData.checkstyle=="Y"?true:false}
                            disabled={this.props.stateStatus=='check'}
                            onChange={(v)=>{
                                if(v){
                                    mainData.checkstyle="Y"
                                }else{
                                    mainData.checkstyle='N'
                                }
                                this.setState({
                                    mainData
                                })
                                this.props.getSubjref(mainData)
                            }}
                        >携带现金流量</Checkbox>
                    </Col>
                    <Col lg={3} sm={3} xs={3}>
                        <Checkbox 
                            checked={mainData.cashflow=="Y"?true:false}
                            disabled={this.props.stateStatus=='check'}
                            onChange={(v)=>{
                                if(v){
                                    mainData.cashflow="Y"
                                }else{
                                    mainData.cashflow='N'
                                }
                                this.setState({
                                    mainData
                                })
                                this.props.getSubjref(mainData)
                            }}
                        >携带结算信息</Checkbox>
                    </Col>
                    <Col lg={3} sm={3} xs={3}>
                        <Checkbox 
                            checked={mainData.verify=="Y"?true:false}
                            disabled={this.props.stateStatus=='check'}
                            onChange={(v)=>{
                                if(v){
                                    mainData.verify="Y"
                                }else{
                                    mainData.verify='N'
                                }
                                this.setState({
                                    mainData
                                })
                                this.props.getSubjref(mainData)
                            }}
                        >携带核销信息</Checkbox>
                    </Col>
                </Row>  */}
            </div>
        )
    }
}
