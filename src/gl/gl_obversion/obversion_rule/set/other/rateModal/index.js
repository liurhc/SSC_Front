import React, { Component } from 'react';
import {high,base,ajax,getBusinessInfo,getMultiLang  } from 'nc-lightapp-front';
const { 
    NCMessage:Message,NCButton,NCStep,NCModal:Modal,NCTable,NCButtonGroup,NCSelect,NCCheckbox:Checkbox,
    NCNumber 
    } = base;
import {InputItem,} from '../../../../../public/components/FormItems';
// import AccountDefaultModelTreeRef from '../../../../../../uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
import AccTypeGridRef from '../../../../../../uapbd/refer/fiacc/AccTypeGridRef/index';
import ReferLoader from '../../../../../public/ReferLoader/index.js';
const NCOption = NCSelect.NCOption;
export default class RateModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            localMaindata:[],
            selectArr:[],//辅助核算选中项
            selIds:[],//多选需要
            checkedArray:[],//选中数组
            checkedAll:false,
            mainData:[
                {
                    key:0,
                    pk_convertrate:'',//
                    pk_convertrule:'',//折算汇率主键
                    subjcode:{
                        display:null,
                        value:''
                    },//科目编码
                    subjname:'',//科目名称
                    subjtype:{//科目类别
                        display:null,
                        value:''
                    },
                    ratetype:{value:'F'},//汇率类别 :: D(日汇率）,R(即时汇率), P(期间汇率), F(固定汇率)
                    rate:{display:'',value:''},//汇率值
                    isSecondEdit:false
                }
            ],
            selectedItem:null,
            columns:[]
        };
        this.businessInfo;
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
    }
    
    componentWillMount(){
        let mainData = this.props.mainData;
        let callback= (json) =>{
               this.setState({json:json,
                columns:[
                    // { title: "序号", dataIndex: "prepareddate", key: "prepareddate", 
                    // width:'10%',
                    // render: (text, record, index) => (
                    //   <div>
                    //     {this.state.mainData[index].key}
                    //   </div>
                    // )
                    // },
                { title: (<div fieldid="voucherno" className="mergecells">{json['20020CONRL-000092']}</div>),
                     dataIndex: "voucherno",
                      key: "voucherno",/* 国际化处理： 科目编码*/
                    width:'120',
                    render: (text, record, index) => (
                        <ReferLoader
                            fieldid="voucherno"
                            tag='AccountDefaultGridTreeRefCode'
                            refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
                            value={ {'refname': this.state.mainData[index].subjcode.display, refpk: this.state.mainData[index].subjcode.value} }
                            placeholder={json['20020CONRL-000092']}/* 国际化处理： 科目编码*/
                            // onlyLeafCanSelect={true}
                            disabled={this.props.stateStatus=='check'}
                            queryCondition={{
                                "pk_accountingbook":this.props.pk_desbook,
                                'dateStr':this.businessInfo,
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',
                            }}
                            onChange={(v)=>{
                                // 根据选定的pk 实现过滤
                                let mainData = this.state.mainData
                                if(!v.refpk){
                                    mainData[index].subjcode.value='';
                                    mainData[index].subjcode.display='';
                                    mainData[index].subjname='';
                                    return;
                                }
                                mainData[index].subjcode.value=v.refpk;
                                mainData[index].subjcode.display=v.code;
                                mainData[index].subjname=v.refname;
                                // mainData[index].subjtype.value=v.refpk;
                                this.setState({
                                    mainData
                                })
                                let url = '/nccloud/gl/glpub/acctypenamequery.do';
                                let self = this;
                                ajax({
                                    url,
                                    data:{
                                        pk_accountingbook:this.props.pk_desbook,
                                        pk_acctype:v.pk_acctype
                                    },
                                    success: function(response) {
                                        const { data, error, success } = response;
                                        if (success) {
                                          mainData[index].subjtype.display=data;
                                          mainData[index].subjtype.value=data;
                                          self.setState({
                                            mainData
                                          })
                                        } else {
                                            // toast({ content: '无请求数据', color: 'warning' });
                                        }
                                    }
                                  });
                                // this.props.getChangeData(mainData);
                            }}
                        />
                    )},
                    { title: (<div fieldid="explanation" className="mergecells">{json['20020CONRL-000093']}</div>), 
                    dataIndex: "explanation",
                     key: "explanation",/* 国际化处理： 科目名称*/
                    width:'200',
                      render: (text, record, index) => (
                          <div fieldid="explanation"
                            disabled={this.state.mainData[index].isSecondEdit} 
                          >
                            {this.state.mainData[index].subjname?this.state.mainData[index].subjname:<span>&nbsp;</span>}
                          </div>
                        )
                    },
                    { title:(<div fieldid="debit" className="mergecells">{ json['20020CONRL-000094']}</div>), 
                    dataIndex: "debit",
                     key: "debit",/* 国际化处理： 科目类别*/
                    width:'120',
                      render: (text, record, index) => (
                          <AccTypeGridRef
                            fieldid="debit"
                            value={ {'refname': this.state.mainData[index].subjtype.display, refpk: this.state.mainData[index].subjtype.value} }
                            placeholder={json['20020CONRL-000094']}/* 国际化处理： 科目类别*/
                            disabled={this.props.stateStatus=='check'||record.subjcode.display}
                            queryCondition={{
                                GridRefActionExt:'nccloud.web.gl.ref.AccTypeRefSqlBuilder',
                                pk_accountingbook:this.props.pk_desbook,
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',
                            }}
                            onChange={(v)=>{
                                // 根据选定的pk 实现过滤
                                let mainData = this.state.mainData
                                mainData[index].subjtype.value=v.refpk;
                                mainData[index].subjtype.display=v.refname;
                                mainData[index].isSecondEdit=true;
                                this.setState({
                                    mainData
                                })
                                // this.props.getChangeData(mainData);
                            }}
                          />
                        )
                    },
                    { title: (<div fieldid="credit" className="mergecells">{json['20020CONRL-000095']}</div>),
                     dataIndex: "credit",
                      key: "credit",/* 国际化处理： 汇率种类*/
                    width:'120',
                      render: (text, record, index) => (
                        <NCSelect
                            fieldid="credit"
                            size="lg"
                            defaultValue='F'
                            value={this.state.mainData[index].ratetype.value}
                            disabled={this.props.stateStatus=='check'}
                            style={{ width: 200, marginRight: 6 }}
                            onSelect={(value)=>{
                                let mainData = this.state.mainData;
                                mainData[index].ratetype.value=value;
                                mainData[index].rate={};
                                this.setState({
                                    mainData
                                })
                            }}
                        >
                            <NCOption value="D">{json['20020CONRL-000097']}</NCOption>
                            <NCOption value="R" >{json['20020CONRL-000098']}</NCOption>
                            <NCOption value="P">{json['20020CONRL-000099']}</NCOption>
                            <NCOption value="F">{json['20020CONRL-000100']} </NCOption>
                      </NCSelect>
                      )
                    },
                    { title: (<div fieldid="state" className="mergecells">{json['20020CONRL-000096']}</div>),
                     dataIndex: "state",
                      key: "state",/* 国际化处理： 汇率值*/
                    width:'120',
                      render: (text, record, index) => (
                          <div>
                              {this.state.mainData[index].ratetype.value=='F'?
                                <NCNumber
                                    fieldid="state"
                                    type="customer"
                                    name="scale"
                                    scale={8}
                                    disabled={this.props.stateStatus=='check'}
                                    value={this.state.mainData[index].rate.display}
                                    onChange={(v) => {
                                        let mainData = this.state.mainData;
                                        if(v.split('.')[1])
                                        mainData[index].rate.value=v
                                        mainData[index].rate.display=v
                                        this.setState({
                                            mainData
                                        })
                                    }}
                                />
                              :
                              <div>
                                {this.state.mainData[index].rate.display?this.state.mainData[index].rate.display:<span>&nbsp;</span>}
                              </div>}
                          </div>
                          
                      )
                    },
                    { title: (<div fieldid="cz" className="mergecells">{json['20020CONRL-000037']}</div>),
                     dataIndex: "cz",
                      key: "cccz",/* 国际化处理： 操作*/
                    width:'80',
                      render: (text, record, index) => (
                            <a 
                            fieldid="cz"
                            disabled={this.props.stateStatus=='check'}
                            onClick={()=>{
                                let {selectedItem,mainData} = this.state
                                mainData.splice(index,1)
                                this.setState({
                                    mainData
                                })
                            }  
                            }>{json['20020CONRL-000042']}</a>                  /* 国际化处理： 删除*/
                        )
                    },
                ]
            },()=>{ })
                }
            getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
        if(!mainData){
            return
        }
        for(let i=0,len=mainData.length;i<len;i++){
            mainData[i].key=i
        }
        if(mainData){
            this.setState({
                mainData,
                localMaindata:JSON.parse(JSON.stringify(mainData))
            })
        }
        
    }
    componentDidMount(){
        if(getBusinessInfo()){
            if(getBusinessInfo().businessDate){
                this.businessInfo=getBusinessInfo().businessDate.split(' ')[0]
            }
        }
    }
    componentWillReceiveProps(nextProps){
        let mainData = nextProps.mainData;
        if(!mainData){
            return
        }
        let checkedArray =[];
        for(let i=0,len=mainData.length;i<len;i++){
            mainData[i].key=i
            checkedArray.push(false)
        }
        this.setState({
            checkedArray
        })
        this.setState({
            mainData,
            localMaindata:JSON.parse(JSON.stringify(mainData))
        })
    }
    handleAdd=()=>{//新增
        let mainData = this.state.mainData;
        let obj={
            pk_convertrate:'',//
            pk_convertrule:'',//折算汇率主键
            subjcode:{
                display:null,
                value:''
            },//科目编码
            subjname:'',//科目名称
            subjtype:{
                display:null,
                value:''
            },//科目类别
            ratetype:{value:'F'},//汇率类别 :: D(日汇率）,R(即时汇率), P(期间汇率), F(固定汇率)
            rate:{value:''},//汇率值
            key:mainData[mainData.length-1]?mainData[mainData.length-1].key+1:mainData.length+1
        }
        mainData.push(Object.assign({},obj))
        this.setState({
            mainData
        })
    }
    handleDel=()=>{//删除
        let {selectArr,mainData} = this.state;
        for(let i=0,len=mainData.length;i<len;i++){
            for(let j=0,lenj=selectArr.length;j<lenj;j++){
                if(mainData[i].key==selectArr[j].key){
                    mainData.splice(i,1)
                    len--
                }
            }
        }
        for(let i=0,len=mainData.length;i<len;i++){
            mainData[i].key=i
        }
        this.setState({
            mainData,
            checkedArray:[],
            selIds:[]
        })
    }
    handleConfirm=()=>{//确认
        this.props.getRateData(this.state.mainData);
        this.setState({
            showModal: false,
            checkedArray:[],
        });
    }
    handleCancel=()=>{//取消
        this.setState({
            showModal: false,
            mainData:this.state.localMaindata
        });
        this.props.getRateData(this.state.localMaindata);
    }
    getSelectedItem=(data)=>{
        this.setState({
            selectedItem:data
        })
    }
    close() {
        this.setState({
            showModal: false,
            // mainData:[]
        });
    }
    open() {
        this.setState({
            showModal: true
        });
    }
    //处理多选
  onAllCheckChange = () => {//全选
    let self = this;
    let mainData = this.state.mainData;
    let checkedArray = [];
    for(let i=0,len=mainData.length;i<len;i++){
        checkedArray.push(false)
    }
    // let listData = self.state.mainData.concat();
    let selIds = [];
    // let id = self.props.multiSelect.param;
    for (let i = 0; i < checkedArray.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
      selIds.push(i);
    }
    if(self.state.checkedAll){
      selIds=[]
    }
    self.setState({
      checkedAll: !self.state.checkedAll,
      selIds: selIds,
      checkedArray:checkedArray
    });
    let selectArr = [];
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(mainData[selIds[i]])));
    }
    // this.props.getSelectArr(selectArr);
    this.setState({
        selectArr
    })
    // self.props.getSelectArr(selIds);
  };
  onCheckboxChange = (text, record, index) => {//单选
    let self = this;
    let allFlag = false;
    let selIds = self.state.selIds;
    // let id = self.props.postId;
    let checkedArray = self.state.checkedArray.concat();
    if (self.state.checkedArray[index]) {
      selIds.splice(record.key,1);
    } else {
      selIds.push(record.key);
    }
    checkedArray[index] = !self.state.checkedArray[index];
    for (let i = 0; i < self.state.checkedArray.length; i++) {
      if (!checkedArray[i]) {
        allFlag = false;
        break;
      } else {
        allFlag = true;
      }
    }
    self.setState({
      checkedAll: allFlag,
      selIds: selIds,
      checkedArray
    });
    // self.props.setCheckedArray(checkedArray)
    let mainData = this.state.mainData;
    let selectArr = [];
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(mainData[selIds[i]])));
    }
    this.setState({
        selectArr
    })
  };
  renderColumnsMultiSelect(columns) {
    const { data,checkedArray,mainData } = this.state;
    let { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // multiSelect= "checkbox";
    // let indeterminate_bool1 = true;
    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArray.length;
      while(i--){
          if(checkedArray[i]){
            indeterminate_bool = true;
            break;
          }
      }
      let defaultColumns = [
        {
          title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
            <Checkbox
              className="table-checkbox"
              checked={this.state.checkedAll}
              indeterminate={indeterminate_bool&&!this.state.checkedAll}
              onChange={this.onAllCheckChange}
            />
        }</div>),
          key: "checkbox",
          attrcode: "checkbox",
          dataIndex: "checkbox",
          width: "50px",
          render: (text, record, index) => {
            return (
                <div fieldid="firstcol">
                    <Checkbox
                        className="table-checkbox"
                        checked={this.state.checkedArray[index]}
                        onChange={this.onCheckboxChange.bind(this, text, record, index)}
                    />
                </div>
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
    render () {
        let {mainData}=this.state;
        let columns = this.renderColumnsMultiSelect(this.state.columns);
        return (
        <div>
            <a
            fieldid="open_btn"
            colors = "primary"
            size = "lg"
            className="demo-margin"
            style={{'text-decoration': 'underline','cursor':'pointer'}}
            onClick = { this.open }>
                {this.state.json['20020CONRL-000101']}
            </a>​
            <Modal
            fieldid="query"
            show = { this.state.showModal }
            size = "xlg"
            className="obversion-set-rateModal"
            onHide = { this.close } >
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20020CONRL-000101']}</Modal.Title>
                </Modal.Header>
                <Modal.Body style={{'padding-top':0}}>
                    <div style={{'overflow':'hidden','padding':'5px 0'}}>
                        <div style={{'float':'right'}}>
                            <NCButton disabled={this.props.stateStatus=='check'} onClick={this.handleAdd} fieldid="handleAdd">{this.state.json['20020CONRL-000044']}</NCButton>
                            <NCButton disabled={this.props.stateStatus=='check'} onClick={this.handleDel} fieldid="handleDel">{this.state.json['20020CONRL-000102']}</NCButton>
                        </div>
                    </div>
                    <div fieldid="rate_table">
                   <NCTable
                        columns={columns} 
                        data={mainData} 
                        onRowClick={this.getSelectedItem.bind(this)}
                        scroll={{ x:false, y: 180 }}
                        bodyStyle={{'height':'180px'}}
                   />
                   </div>
                </Modal.Body>

                <Modal.Footer fieldid="bottom_area">
                    <NCButton style={{'margin-right':'6px'}} onClick={ this.handleCancel } shape="border" fieldid="handleCancel">{this.state.json['20020CONRL-000006']}</NCButton>
                    <NCButton onClick={ this.handleConfirm } shape="border" colors="primary" fieldid="handleConfirm">{this.state.json['20020CONRL-000103']}</NCButton>
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
RateModal.defaultProps = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
};
