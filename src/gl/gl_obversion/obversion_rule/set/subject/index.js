import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,getBusinessInfo,getMultiLang  } from 'nc-lightapp-front';
const { 
    NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,
    NCRadio,NCBreadcrumb:Breadcrumb,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,
    NCButtonGroup:ButtonGroup
    } = base;
import { toast } from '../../../../public/components/utils.js';
const { Refer } = high;
// import AccountDefaultModelTreeRef from './../../../../../uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js';
// import AccountDefaultModelTreeRef from './../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef/index.js';
import ReferLoader from '../../../../public/ReferLoader/index.js';
import { getTableHeight} from "../../../../public/common/method.js";
export default class SubjectComponent extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            pk_accountingbook:{display:'',value:''},
            tableData:[],
            selectArr:[],//辅助核算选中项
            selIds:[],//多选需要
            checkedArray:[],//选中数组
            mainData:{
                conditions:[],
                "defaultsubj":{//默认科目
                    "display":"",
                    "value":"",
                    "scale":null
                },
            },
            columns:[]
        };
        this.businessInfo='';
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json,
            columns :  [
                { title: (<div fieldid="srcname" className="mergecells">{json['20020CONRL-000104']}</div>),
                 dataIndex: "srcname",
                  key: "srcname",
                   width:'250',/* 国际化处理： 来源科目编码*/
                    render: (text, record, index) => (
                        <div fieldid="srcname">
                        <ReferLoader
                            fieldid="srcname"
                            tag='AccountDefaultGridTreeRefCode'
                            refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js' 
                            value =  {{
                                refname: this.state.tableData[index].srccode?this.state.tableData[index].srccode.display:'', 
                                refpk: this.state.tableData[index].srccode?this.state.tableData[index].srccode.value:''
                            }}
                            disabled={this.props.stateStatus=='check'}
                            onlyLeafCanSelect={true}
                            queryCondition={{
                                "pk_accountingbook":this.props.pk_srcbook,
                                'dateStr':this.businessInfo,
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',
                            }}
                            onChange={(v)=>{
                                let {tableData} = this.state;
                                tableData[index].srccode={
                                    display:'',
                                    value:''
                                }
                                tableData[index].srccode.display= v.refcode;	
                                tableData[index].srccode.value = v.refpk;	
                                tableData[index].srcname = v.refname;
                                this.setState({
                                    tableData
                                })     
                                this.state.mainData.subjref=tableData;
                                this.props.getSubjref(this.state.mainData);                 
                            }}
                        />
                        </div>
                    )
                },
                { title: (<div fieldid="srccode" className="mergecells">{json['20020CONRL-000105']}</div>),
                 dataIndex: "srccode",
                  key: "srccode",
                   width:'250',/* 国际化处理： 来源科目名称*/
                    render: (text, record, index) => (
                        <div fieldid="srccode">
                            {this.state.tableData[index].srcname?this.state.tableData[index].srcname:<span>&nbsp;</span>}
                        </div>
                    )
                },
                { title: (<div fieldid="desname" className="mergecells">{json['20020CONRL-000106']}</div>),
                 dataIndex: "desname",
                  key: "desname",
                    width:'250',/* 国际化处理： 目的科目编码*/
                    render: (text, record, index) => (
                        <div fieldid="desname">
                        <ReferLoader
                            fieldid="desname"
                            tag='AccountDefaultGridTreeRefCode'
                            refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js' 
                            value =  {{
                                refname: this.state.tableData[index].descode?this.state.tableData[index].descode.display:'', 
                                refpk: this.state.tableData[index].descode?this.state.tableData[index].descode.value:''
                            }}
                            disabled={this.props.stateStatus=='check'}
                            onlyLeafCanSelect={true}
                            queryCondition={{
                                "pk_accountingbook":this.props.pk_des,
                                'dateStr':this.businessInfo,
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',                       
                            }}
                            onChange={(v)=>{
                                let {tableData} = this.state;
                                tableData[index].descode={
                                    display:'',
                                    value:''
                                }
                                tableData[index].descode.display = v.refcode;	
                                tableData[index].descode.value = v.refpk;	
                                tableData[index].desname = v.refname;         
                                this.setState({
                                    tableData
                                })
                                this.state.mainData.subjref=tableData;
                                this.props.getSubjref(this.state.mainData);
                            }}  
                        />
                        </div>
                    )
                },
                { title: (<div fieldid="descode" className="mergecells">{json['20020CONRL-000107']}</div>),
                 dataIndex: "descode", key: "descode", width:'250',/* 国际化处理： 目的科目名称*/
                    render: (text, record, index) => (
                        <div fieldid="descode">
                            {this.state.tableData[index].desname?this.state.tableData[index].desname:<span>&nbsp;</span>}
                        </div>
                    )
                },
                { title: (<div fieldid="cz" className="mergecells">{json['20020CONRL-000037']}</div>),
                 dataIndex: "cz", key: "cz",width:'100',/* 国际化处理： 操作*/
                    render: (text, record, index) => (
                        <a
                        fieldid="cz"
                        style={{'cursor':'pointer',display:this.props.stateStatus=='check'?'none':''}}
                        onClick={()=>{
                            let tableData = this.state.tableData;
                            tableData.splice(index,1);
                            this.state.mainData.subjref=tableData;
                            this.props.getSubjref(this.state.mainData);
                        }}
                        disabled={this.props.stateStatus=='check'}
                        >{json['20020CONRL-000042']}</a>/* 国际化处理： 删除*/
                    )
                },
              ]
        },()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentDidMount(){
        if(getBusinessInfo()){
            if(getBusinessInfo().businessDate){
                this.businessInfo=getBusinessInfo().businessDate.split(' ')[0]
            }
        }
    }
    componentWillReceiveProps(nextProps){
        let mainData = nextProps.mainData;
        let tableData = mainData.subjref;
        if(tableData){
            for(let i=0,len=tableData.length;i<len;i++){
                tableData[i].key=i
            }
        }
        this.setState({
            mainData,
            tableData
        })
    }
    handleAdd=()=>{
        let tableData = this.state.tableData;       
        let pk_soblink = this.props.mainData.pk_soblink;
        let obj={
            key:tableData.length,
            "pk_convertmap":"",
            "pk_convertrule":"",
            "pk_soblink":pk_soblink,
            "srccode":{
                "display":"",
                "value":"",
                "scale":null
            },
            "descode":{
                "display":"",
                "value":"",
                "scale":null
            },
            "srcname":"1",
            "desname":"2"
        }
        tableData.push(JSON.parse(JSON.stringify(obj)))
        this.setState({
            tableData
        })
        let mainData = this.state.mainData;
        mainData.subjref = tableData;
        this.props.getSubjref(mainData);
    }
    handleImport=()=>{//导入
        let pk_soblink=this.state.mainData.pk_soblink;
        let data = {pk_srcbook:this.props.pk_srcbook,type:'1'};
        let url = "/nccloud/gl/voucher/fcrulerefimport.do";
        let self=this;
        ajax({
          url,
          data,
          success: function(response) {
              const { data, error, success } = response;
              if (success&&data) {
                self.setState({
                    tableData:data
                })
                let mainData = self.state.mainData;
                mainData.subjref = data;
                self.props.getSubjref(mainData);
                toast({ content: self.state.json['20020CONRL-000108'], color: 'success' });/* 国际化处理： 导入成功*/
              } else {
                  toast({ content: self.state.json['20020CONRL-000109'], color: 'warning' });/* 国际化处理： 导入失败*/
              }
          }
        });
    }
    handleAutoMatch=()=>{//自动匹配
        let pk_soblink=this.state.mainData.pk_soblink;
        let data = {pk_desbook:this.props.pk_des,type:'1',refvos:this.state.tableData};
        let url = "/nccloud/gl/voucher/fcrulerefmatch.do";
        let self=this;
        ajax({
          url,
          data,
          success: function(response) {
              const { data, error, success } = response;
              if (success&&data) {
                self.setState({
                    tableData:data
                })
                let mainData = self.state.mainData;
                mainData.subjref = data;
                self.props.getSubjref(mainData);
                toast({ content: self.state.json['20020CONRL-000110'], color: 'success' });/* 国际化处理： 自动匹配成功*/
              } else {
                self.setState({
                    tableData:[]
                })
                let mainData = self.state.mainData;
                mainData.subjref = [];
                self.props.getSubjref(mainData);
                  // toast({ content: '无请求数据', color: 'warning' });
              }
          }
        });
    }
    handleDelArr=()=>{//批量删除
        let {selectArr,tableData} = this.state;
        for(let i=0,len=tableData.length;i<len;i++){
            for(let j=0,lenj=selectArr.length;j<lenj;j++){
                if(tableData[i].key==selectArr[j].key){
                    tableData.splice(i,1)
                    len--
                }
            }
        }
        for(let i=0,len=tableData.length;i<len;i++){
            tableData[i].key=i
        }
        this.setState({
            tableData,
            checkedArray:[],
            selIds:[]
        })
        toast({ content: this.state.json['20020CONRL-000111'], color: 'success' });/* 国际化处理： 删除成功*/
    }
   //处理多选
    onAllCheckChange = () => {//全选
        let self = this;
        let checkedArray = [];
        let listData = self.state.tableData.concat();
        let selIds = [];
        // let id = self.props.multiSelect.param;
        for (var i = 0; i < self.state.tableData.length; i++) {
            checkedArray[i] = !self.state.checkedAll;
            selIds.push(i);
        }
        if(self.state.checkedAll){
            selIds=[]
        }
        self.setState({
        checkedAll: !self.state.checkedAll,
        checkedArray: checkedArray,
        selIds: selIds
        });
        let tableData = this.state.tableData;
        let selectArr = [];
        
        for (let i=0;i<selIds.length;i++) {
        selectArr.push(JSON.parse(JSON.stringify(tableData[selIds[i]])));
        }
        this.setState({
            selectArr
        });
        // self.props.getSelectData(selIds);
    };
    onCheckboxChange = (text, record, index) => {//单选
        let self = this;
        let allFlag = false;
        let selIds = self.state.selIds;
        // let id = self.props.postId;
        let checkedArray = self.state.checkedArray.concat();
        if (self.state.checkedArray[index]) {
            selIds.splice(record.key,1);
        } else {
            selIds.push(record.key);
        }
        checkedArray[index] = !self.state.checkedArray[index];
        for (let i = 0; i < self.state.checkedArray.length; i++) {
            if (!checkedArray[i]) {
                allFlag = false;
                break;
            } else {
                allFlag = true;
            }
        }
        for (let i=0,len=selIds.length;i<len;i++) {
            for (let j=0;j<len-1-i;j++) {
                if(selIds[j]>selIds[j+1]){
                    let temp = selIds[j+1];
                    selIds[j+1] = selIds[j]
                    selIds[j]=temp
                }
            }
        }
        self.setState({
        checkedAll: allFlag,
        checkedArray: checkedArray,
        selIds: selIds
        });
        let tableData = this.state.tableData;
        let selectArr = [];
        for (let i=0;i<selIds.length;i++) {
        selectArr.push(JSON.parse(JSON.stringify(tableData[selIds[i]])));
        }
        this.setState({
            selectArr
        });
    };
    renderColumnsMultiSelect(columns) {
        const { data,checkedArray,tableData } = this.state;
        let { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        // multiSelect= "checkbox";
        // let indeterminate_bool1 = true;
        if (multiSelect && multiSelect.type === "checkbox") {
        let i = checkedArray.length;
        while(i--){
            if(checkedArray[i]){
                indeterminate_bool = true;
                break;
            }
        }
        let defaultColumns = [
            {
            title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
                <Checkbox
                    className="table-checkbox"
                    checked={this.state.checkedAll}
                    indeterminate={indeterminate_bool&&!this.state.checkedAll}
                    onChange={this.onAllCheckChange}
                    // width="30px"
                    style={{ 'white-space':'inherit'}}
                   
                />
            }</div>),
            key: "checkbox",
            attrcode: "checkbox",
            dataIndex: "checkbox",
            width: "30px",
            render: (text, record, index) => {
                return (
                    <div fieldid="firstcol">
                        <Checkbox
                            className="table-checkbox"
                            checked={this.state.checkedArray[index]}
                            onChange={this.onCheckboxChange.bind(this, text, record, index)}
                        />
                    </div>
                );
            }
            }
        ];
        columns = defaultColumns.concat(columns);
        }
        return columns;
    }
    render(){
        let {pk_accountingbook,tableData,mainData,selectArr}=this.state;
        let columns = this.renderColumnsMultiSelect(this.state.columns);
        let businessInfo = this.businessInfo;
        return (
            <div style={{padding:'5px','line-height':'30px','margin-top':'-8px'}} className='obr-set-subject'>
                <div>
                    <Row>
                        <Col lg={2} sm={2} xs={2}>{this.state.json['20020CONRL-000022']}：</Col>
                        <Col lg={4} sm={4} xs={4}>
                            <Col lg={7} sm={7} xs={7} style={{'padding':'0'}}>
                                <NCRadio.NCRadioGroup
                                 fieldid="subjmatcherro"
                                    name="fruit"
                                    disabled={this.props.stateStatus=='check'}
                                    width='50%'
                                    selectedValue={Number(mainData.subjmatcherro)}
                                    onChange={(v)=>{
                                        mainData.subjmatcherro=v
                                        if(v==1){
                                            mainData.defaultsubj.value=''
                                            mainData.defaultsubj.display=''
                                        }
                                        this.setState({
                                            mainData
                                        })
                                        this.props.getSubjref(mainData);
                                    }}
                                    >
                                    <NCRadio value={1} disabled={this.props.stateStatus=='check'}>{this.state.json['20020CONRL-000023']}</NCRadio>
                                    <NCRadio value={0} disabled={this.props.stateStatus=='check'}> </NCRadio>
                                </NCRadio.NCRadioGroup>
                            </Col>
                            <Col lg={5} sm={5} xs={5} style={{'padding':'0'}}>
                                <ReferLoader
                                fieldid="defaultsubj"
                                    tag='AccountDefaultGridTreeRefCode'
                                    width='50%'
                                    refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
                                        placeholder={this.state.json['20020CONRL-000112']}
                                        disabled={this.props.stateStatus=='check'}
                                        onlyLeafCanSelect={true}
                                        value={ {'refname': mainData.defaultsubj.display, refpk: mainData.defaultsubj.value} }
                                        queryCondition={{
                                            "pk_accountingbook":this.props.pk_des,
                                            'dateStr':businessInfo,
                                            isDataPowerEnable: 'Y',
                                            DataPowerOperationCode: 'fi',
                                    }}
                                        onChange={(v)=>{
                                        // 根据选定的pk 实现过滤
                                        mainData.defaultsubj.value=v.refpk
                                        mainData.defaultsubj.display=v.refname
                                        this.setState({
                                            mainData
                                        })
                                        this.props.getSubjref(mainData);
                                    }}
                                />
                            </Col>
                        </Col>
                        <Col lg={2} sm={2} xs={2}>
                            <Checkbox 
                                checked={mainData.subjcodesame=='Y'?true:false}
                                // =='Y'?true:false
                                disabled={this.props.stateStatus=='check'}
                                onChange={(v)=>{
                                    if(v){
                                        mainData.subjcodesame='Y'
                                    }else{
                                        mainData.subjcodesame='N'
                                    }
                                    // mainData.subjcodesame=v
                                    this.setState({
                                        mainData
                                    })
                                    this.props.getSubjref(mainData);
                            }}
                            >{this.state.json['20020CONRL-000113']}</Checkbox>
                        </Col>
                        <Col lg={6} sm={6} xs={6} style={{'width':'100%','float':'none'}}>
                            <div style={{'float':'right','padding-bottom':'9px'}}>
                                <ButtonGroup>
                                    <Button style={{'margin-right':'5px'}} colors="info"  fieldid="handleAdd"
                                        disabled={this.props.stateStatus=='check'}
                                    onClick={this.handleAdd}>{this.state.json['20020CONRL-000114']}</Button>
                                    <Button style={{'margin-right':'5px'}} disabled={this.props.stateStatus=='check'||selectArr.length==0} colors="info" onClick={this.handleDelArr} fieldid="handleDel">
                                    {this.state.json['20020CONRL-000115']}
                                    </Button>
                                </ButtonGroup>
                                <ButtonGroup>
                                    <Button style={{'margin-right':'5px'}} disabled={this.props.stateStatus=='check'} colors="info" onClick={this.handleImport} fieldid="handleImport">
                                    {this.state.json['20020CONRL-000116']}
                                    </Button>
                                    <Button style={{'margin-right':'5px'}} disabled={this.props.stateStatus=='check'} colors="info" onClick={this.handleAutoMatch} fieldid="handleAutoMatch">
                                    {this.state.json['20020CONRL-000117']}
                                    </Button>
                                </ButtonGroup>
                            </div>
                        </Col>
                    </Row>
                </div>
                
                <div fieldid="subject_table">
                    <Table
                        columns={columns}
                        data={tableData}
                        // onRowClick={this.clickRow}
                        scroll={{ x:false, y: getTableHeight(230) }}
                        bodyStyle={{'height':getTableHeight(230)}}
                    />
                </div>
            </div>
        )
    }
}
SubjectComponent.defaultProps = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
};
