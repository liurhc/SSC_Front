import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base ,ajax,cardCache,getMultiLang,createPageIcon} from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
const { 
    NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,
    NCRadio:Radio,NCBreadcrumb:Breadcrumb,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCDiv
    } = base;
import './index.less';
import HeaderArea from '../../../public/components/HeaderArea';
import { openToVoucher } from '../../../public/common/voucherUtils';
class MoreData extends Component {
	constructor(props) {
		super(props);
		this.state={
            json:{},
			pk_accountingbook:{display:'',value:''},
			mainData:[],
            selectData:[],
            columns:[],
            getrow:0
		}
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json,
            columns :  [
            { title: (<div fieldid="srcbook" className="mergecells">{json['20020CONRL-000168']}</div>),
                 dataIndex: "srcbook",
                  key: "srcbook", /* 国际化处理： 核算账簿*/
                    render: (text, record, index) => (
                        <div fieldid="srcbook">
                            {this.state.mainData[index].srcbook?this.state.mainData[index].srcbook:<span>&nbsp;</span>}
                        </div>
                    )
                },
                { title: (<div fieldid="srcvouchertype" className="mergecells">{json['20020CONRL-000068']}</div>), 
                dataIndex: "srcvouchertype",
                 key: "srcvouchertype",/* 国际化处理： 凭证类别*/
                    render: (text, record, index) => (
                    <div fieldid="srcvouchertype">
                        {this.state.mainData[index].srcvouchertype?this.state.mainData[index].srcvouchertype:<span>&nbsp;</span>}
                    </div>
                    )
                },
                { title: (<div fieldid="srcvoucherno" className="mergecells">{json['20020CONRL-000125']}</div>),
                 dataIndex: "srcvoucherno",
                  key: "srcvoucherno", /* 国际化处理： 凭证号*/
                    render: (text, record, index) => (
                        <div fieldid="srcvoucherno">
                            {this.state.mainData[index].srcvoucherno?this.state.mainData[index].srcvoucherno:<span>&nbsp;</span>}
                        </div>
                    )
                },
                { title: (<div fieldid="srcprepareddate" className="mergecells">{json['20020CONRL-000019']}</div>),
                 dataIndex: "srcprepareddate",
                  key: "srcprepareddate",/* 国际化处理： 制单日期*/
                    render: (text, record, index) => (
                    <div fieldid="srcprepareddate">
                        {this.state.mainData[index].srcprepareddate?this.state.mainData[index].srcprepareddate:<span>&nbsp;</span>}
                    </div>
                    )
                },
                { title: (<div fieldid="srcexplanation" className="mergecells">{json['20020CONRL-000062']}</div>),
                 dataIndex: "srcexplanation",
                  key: "srcexplanation",/* 国际化处理： 摘要*/
                    render: (text, record, index) => (
                    <div fieldid="srcexplanation">
                        {this.state.mainData[index].srcexplanation?this.state.mainData[index].srcexplanation:<span>&nbsp;</span>}
                    </div>
                    )
                },
                { title: (<div fieldid="desbook" className="mergecells">{json['20020CONRL-000168']}</div>),
                 dataIndex: "desbook",
                  key: "desbook", /* 国际化处理： 核算账簿*/
                    render: (text, record, index) => (
                        <div fieldid="desbook">
                            {this.state.mainData[index].desbook?this.state.mainData[index].desbook:<span>&nbsp;</span>}
                        </div>
                    )
                },
                { title: (<div fieldid="desvouchertype" className="mergecells">{json['20020CONRL-000068']}</div>),
                 dataIndex: "desvouchertype",
                  key: "desvouchertype",/* 国际化处理： 凭证类别*/
                    render: (text, record, index) => (
                    <div fieldid="desvouchertype">
                        {this.state.mainData[index].desvouchertype?this.state.mainData[index].desvouchertype:<span>&nbsp;</span>}
                    </div>
                    )
                },
                { title: (<div fieldid="desvoucherno" className="mergecells">{json['20020CONRL-000125']}</div>),
                 dataIndex: "desvoucherno",
                  key: "desvoucherno", /* 国际化处理： 凭证号*/
                    render: (text, record, index) => (
                        <div fieldid="desvoucherno">
                            {this.state.mainData[index].desvoucherno?this.state.mainData[index].desvoucherno:<span>&nbsp;</span>}
                        </div>
                    )
                },
                { title: (<div fieldid="desprepareddate" className="mergecells">{json['20020CONRL-000019']}</div>),
                 dataIndex: "desprepareddate",
                  key: "desprepareddate",/* 国际化处理： 制单日期*/
                    render: (text, record, index) => (
                    <div fieldid="desprepareddate">
                        {this.state.mainData[index].desprepareddate?this.state.mainData[index].desprepareddate:<span>&nbsp;</span>}
                    </div>
                    )
                },
                { title: (<div fieldid="desexplanation" className="mergecells">{json['20020CONRL-000062']}</div>),
                 dataIndex: "desexplanation",
                  key: "desexplanation",/* 国际化处理： 摘要*/
                    render: (text, record, index) => (
                    <div fieldid="desexplanation">
                        {this.state.mainData[index].desexplanation?this.state.mainData[index].desexplanation:<span>&nbsp;</span>}
                    </div>
                    )
                },
              ]
        },()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
	componentDidMount() {
		let localdata = getDefData(1234, 'gl_obervision.discount.moreData');
		let data = localdata;
        let url = "/nccloud/gl/voucher/fcdetailquery.do";
        let self = this;
        ajax({
            url,
            data,
            success: function(response) {
				const {data} = response;
                self.setState({
					mainData:data
				})
            }
		});
	}
   
	handleBack=()=>{//返回
		this.props.pushTo('/list', {})
	}
	clickRow=(data,record)=>{
		this.setState({
            selectData:data,
            getrow: record
		})
	}
	render() {
		let {mainData,selectData}=this.state;
        let columns = this.state.columns;
        // let voucherapp=voucherRelatedApp(voucher_gen);
		return (
            <div className='ov-moredata-main nc-bill-list'>
                <HeaderArea
                    title={this.state.json['20020CONRL-000163']}
                    initShowBackBtn = {true} //是否显示返回按钮，默认false，不不显示返回按钮
                    backBtnClick = {this.handleBack} //返回按钮点击事件
                    btnContent={
                        <div className="header-button-area">
                        <Button 
                            fieldid="linkVoucher"
                            className={selectData.srcpkvoucher?'':'disB'}
                            onClick={()=>{
                                let param = {
                                    pk_voucher: selectData.srcpkvoucher
                                }
                                openToVoucher(this,param)
                            }}>{this.state.json['20020CONRL-000169']}
                        </Button>
                        <Button 
                            fieldid="openVoucher"
                            className={selectData.srcpkvoucher?'':'disB'}
                            onClick={()=>{
                                let param = {
                                    pk_voucher: selectData.despkvoucher
                                }
                                openToVoucher(this,param)
                            //     this.props.openTo('/gl/gl_voucher/pages/main/index.html#/Welcome',
                            //     {
                            //         // link:JSON.stringify(link),
                            //         appcode:voucherapp.appcode,//'20020PREPA',
                            //         c:voucherapp.appcode,//'20020PREPA',
                            //         id:selectData.despkvoucher,
                            //         status:'browse',
                            //         pagekey:'link',
                            //         ifshowQuery:true
                            //     }
                            // )
                        }}>{this.state.json['20020CONRL-000170']}</Button>
                    </div>
                    }
                />
                {/* <div className='nc-bill-header-area' fieldid="header_area">
                    <div className="header-title-search-area">
                        <h2 className='title-search-detail' fieldid={`${this.state.json['20020CONRL-000163']}_title`}>
                            <i onClick={this.handleBack} class="iconfont icon-fanhuishangyiji back-btn"></i>
                            {createPageIcon()}
                            {this.state.json['20020CONRL-000163']}
                        </h2>
                    </div>
                    <div className="header-button-area">
                        <Button 
                            fieldid="linkVoucher"
                            className={selectData.srcpkvoucher?'':'disB'}
                            onClick={()=>{
                                let param = {
                                    pk_voucher: selectData.srcpkvoucher
                                }
                                openToVoucher(self,param)
                            }}>{this.state.json['20020CONRL-000169']}
                        </Button>
                        <Button 
                            fieldid="openVoucher"
                            className={selectData.srcpkvoucher?'':'disB'}
                            onClick={()=>{
                                this.props.openTo('/gl/gl_voucher/pages/main/index.html#/Welcome',
                                {
                                    // link:JSON.stringify(link),
                                    appcode:voucherapp.appcode,//'20020PREPA',
                                    c:voucherapp.appcode,//'20020PREPA',
                                    id:selectData.despkvoucher,
                                    status:'browse',
                                    pagekey:'link',
                                    ifshowQuery:true
                                }
                            )
                        }}>{this.state.json['20020CONRL-000170']}</Button>
                    </div> */}
                {/* </div> */}
                <NCDiv fieldid="moredata" areaCode={NCDiv.config.TableCom}>
                    <Table
                        columns={columns}
                        data={mainData}
                        bordered
                        rowClassName={(record,index,indent)=>{
							if(this.state.getrow==index){
								return 'selected'
							}else{
								return ''
							}
						}}
                        onRowClick={this.clickRow}
                        scroll={{ x:false, y: 350 }}
                    />
                </NCDiv>
			</div>
		);
	}
}
MoreData = createPage({
	// initTemplate: initTemplate
})(MoreData);
export default MoreData;
