import React, { Component } from 'react';
import {high,base,ajax,createPage,promptBox,getMultiLang } from 'nc-lightapp-front';
const { 
    NCFormControl: FormControl,NCButton: Button,
    NCRadio,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,
	NCCheckbox:Checkbox,NCModal:Modal,NCNumber,NCDiv
    } = base;
const { Refer } = high;
import createScript from '../../../public/components/uapRefer.js';

const NCOption = NCSelect.NCOption;
import TreeCom from './tree';
import { toast } from '../../../public/components/utils.js';
class CountBeginModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            pk_accountingbook:{display:'',value:''},
            realtimeRate:'',
            treeData:[],
            selectedData:[],
            selectData:[]//主表显示数据
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentDidMount(){
        //获取默认账簿
        let url = '/nccloud/platform/appregister/queryappcontext.do';
        let data={
            appcode: this.props.getSearchParam('c')
        }
        let self=this;
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                let pk_accountingbook={
                        display:data.defaultAccbookName,
                        value:data.defaultAccbookPk
                    }
                self.setState({
                    pk_accountingbook
                })
                self.getRelation()
                } else {
                }
            }
        });
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.disbeginShow;
        if(nextProps.selectData.length){//判断取关系值
            if(this.state.selectData.length){
                if(nextProps.selectData[0].key!=this.state.selectData[0].key&&
                    nextProps.selectData.length!=this.state.selectData.length){
                        this.setState({
                            selectData:Object.assign({},nextProps.selectData)
                        })
                        // this.getRelation()
                }
            }else{
                this.setState({
                    selectData:Object.assign({},nextProps.selectData)
                })
                // this.getRelation()
            }
        }
        this.setState({
            showModal
        })
    }
    close() {
        this.props.getDisbeginShow(false);
    }
    handleConfirm=()=>{
        let {selectedData,realtimeRate}=this.state;
        if(!selectedData.length){
            toast({ content: this.state.json['20020CONRL-000131'], color: 'warning' });/* 国际化处理： 请选择折算规则*/
            return;
        }
        if(selectedData.length>0){
            for(let i=0;i<selectedData.length;i++){
                selectedData[i].realtimeRate = realtimeRate;
            }
        }
        let data = {binds:selectedData};
        let url = "/nccloud/gl/voucher/convertinit.do";
        let self = this;
		// let url = "/gl_web/gl/voucher/convert.do";
        ajax({
            url,
            data,
            success: function(response) {
                const {data} = response;
                promptBox({
                    color: 'success',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                    title: self.state.json['20020CONRL-000132'],                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 提示*/
                    content: data,             // 提示内容,非必输
                    noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                    noCancelBtn: true,             // 是否显示取消按钮,，默认显示(false),非必输
                    beSureBtnName: self.state.json['20020CONRL-000005'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
                    // cancelBtnName: "取消",           // 取消按钮名称, 默认为"取消",非必输
                    beSureBtnClick: ()=>{
                        // self.setState({
                        //     showModal:false
                        // })
                        self.close();
                    } // 确定按钮点击调用函数,非必输
                    // cancelBtnClick: functionCancel  // 取消按钮点击调用函数,非必输
                })
                // promptBox({color:"success",content:"测试一下"})
            }
		});
    }
    getRelation=()=>{//查询关系
        let {pk_accountingbook,treeData} = this.state;
        let data = {pk_accountingbook: pk_accountingbook.value}
        let url = "/nccloud/gl/voucher/relationquery.do";
        let self = this;
        treeData=[]
        ajax({
            url,
            data,
            success: function(response) {
                const reData = response.data;
                let obj={};
                if(reData){
                    for(let i=0,len=reData.length;i<len;i++){
                        let data = {pk_soblink: reData[i].pk_soblink,init:'Y'};
                        let url = "/nccloud/gl/voucher/fcrulequery.do";
                        ajax({
                            url,
                            data,
                            success: function(response) {
                                const { data, error, success } = response;
                                if (success&&data) {
                                    let localArr = [];
                                    let objChild={};
                                    for(let j=0,lenj=data.length;j<lenj;j++){
                                        objChild={
                                            name:data[j].rulename,
                                            key: data[j].pk_convertrule,
                                            isLeaf: true
                                        }
                                        localArr.push(JSON.parse(JSON.stringify(objChild)))
                                    }
                                    obj = {
                                        name:reData[i].pk_desorgbook.display,
                                        key: reData[i].pk_desorgbook.value,
                                        id: reData[i].pk_soblink,
                                        children:localArr
    
                                    }
                                    treeData.push(JSON.parse(JSON.stringify(obj)))
                                    self.setState({
                                        treeData
                                    },()=>{
                                        if(self.TreeCom){
                                            self.TreeCom.clearSelectedData();
                                        }
                                        
                                    })
                                } else {
                                    self.setState({
                                        treeData
                                    },()=>{
                                        if(self.TreeCom){
                                            self.TreeCom.clearSelectedData();
                                        }
                                    })
                                    // toast({ content: '无请求数据', color: 'warning' });
                                }
                            }
                        });
                    }
                }else{
                    self.setState({
                        treeData
                    },()=>{
                        if(self.TreeCom){
                            self.TreeCom.clearSelectedData();
                        }
                    })
                }
            }
        });
    }
    getSelectedData=(data)=>{
        let {selectedData,pk_accountingbook,treeData,realtimeRate} = this.state;
        selectedData=[]
        let obj = {}
        for(let i=0,len=data.length;i<len;i++){
            obj = {
                soblink:data[i].id,
                rule:data[i].children[0].key,
                desorgbook:data[i].key,
                srcorgbook:pk_accountingbook.value,
                realtimeRate:realtimeRate
              }
              selectedData.push(JSON.parse(JSON.stringify(obj)));
        }
        this.setState({
            selectedData
        })
    }
    getParm=(parm)=>{
        let appUrl = decodeURIComponent(window.location.href).split('?');//分割查询
        if (appUrl && appUrl[1]){
            let appPrams = appUrl[1].split('&');
            if(appPrams && appPrams instanceof Array){
                let parmObj={};
                appPrams.forEach(item=>{
                    let key = item.split('=')[0];
                    let value = item.split('=')[1];
                    parmObj[key] = value;
                })
                return parmObj[parm];
            }
        }
    }
    onRef=(ref)=>{
        this.TreeCom=ref;
    }
    render () {
        let {pk_accountingbook,treeData,selectedData} = this.state;
        let mybook;
		// let self = this;
		// let referUrl= item.refcode+'/index.js';
		let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
		if(!this.state['myattrcode']){
            {createScript.call(this,referUrl,'myattrcode')}
		}else{
			mybook =  (
				<Row>
					<Col xs={12} md={12}>
					{this.state['myattrcode']?(this.state['myattrcode'])(
						{
                            fieldid:"pk_accountingbook",
							value:{'refname': pk_accountingbook.display, refpk: pk_accountingbook.value},
                            disabledDataShow:true,
                            isMultiSelectedEnabled:false,
                            queryCondition:{
                                "appcode": this.props.getSearchParam('c'),
                                TreeRefActionExt:'nccloud.web.gl.ref.AccountBookRefSqlBuilder'
                            },
                            onChange: (v)=>{
                                pk_accountingbook.value=v.refpk
                                pk_accountingbook.display=v.refname
                                this.setState({
                                    pk_accountingbook:pk_accountingbook,
                                    selectedData:[]
                                })
                                this.getRelation();
							  }
						}
					):<div/>}
					</Col>
				</Row>
			);	
		}
        return (
        <div>
            <Modal
            fieldid="query"
            show = { this.state.showModal }
            size = 'lg'
            className='obversion-small-modal-a obversion-small-modal-b'
            // closeButton={true}
            onHide = { this.close } >
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20020CONRL-000133']}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                    <Row>
                        <Col lg={2} sm={2} xs={2}>
                            {this.state.json['20020CONRL-000134']}
                        </Col>
                        <Col lg={10} sm={10} xs={10}>
                            {mybook}
                        </Col>
                    </Row>
                    <div style={{padding:'0'}}>
                        <TreeCom 
                            onRef={this.onRef}
                            treeData={treeData} 
                            getSelectedData={this.getSelectedData.bind(this)}
                        >
                        </TreeCom>
                    </div>
                    <Row>
                        <Col lg={2} sm={2} xs={2}>
                            <div className='searchTipsRate nc-theme-area-bgc nc-theme-common-font-c nc-theme-area-split-bc'>{this.state.json['20020CONRL-000098']}
                                {/* <div className='searchTips'>{this.state.json['20020CONRL-000135']}</div> */}
                            </div>
                        </Col>
                        <Col lg={10} sm={10} xs={10}>
                            <NCNumber
                                fieldid="realtimeRate"
                                scale={5}
                                value={this.state.realtimeRate}
                                onChange={(v) => {
                                    let realtimeRate=v;
                                    this.setState({
                                        realtimeRate
                                    })
                                }}
                            />
                        </Col>
                    </Row>
                    <div class="mark">
                        <div class="mark_inco"><i class="uf uf-i-c-2 mark_inco_i"></i></div>
                        <div class="footer-container nc-theme-area-bgc nc-theme-common-font-c nc-theme-area-split-bc">{this.state.json['20020CONRL-000135']}</div>
                    </div>
                </NCDiv>
                </Modal.Body>

                <Modal.Footer fieldid="bottom_area">
                    <Button onClick={ this.close } shape="border" style={{marginRight: 6}} fieldid="close">{this.state.json['20020CONRL-000136']}</Button>
                    <Button onClick={ this.handleConfirm } colors="primary" fieldid="handleconfirm">{this.state.json['20020CONRL-000103']}</Button>
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
CountBeginModal = createPage({})(CountBeginModal);
export default CountBeginModal;
