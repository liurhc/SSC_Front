import React,{Component} from 'react';
import { base,getMultiLang } from 'nc-lightapp-front';
const { NCSelect,NCRow,NCCol,NCIcon,NCDiv } = base;
const NCOption = NCSelect.NCOption;

// 2.组件中引入NCSelect（示例）
export default class Demo extends Component {
	constructor(props){
		super();
		this.state={
            json:{},
            selectData:[],
            selectedData:[]
		}
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
	componentWillReceiveProps(nextProps){
        let selectData = nextProps.treeData;
		this.setState({
			selectData
		})
    }
    componentDidMount(){
        if(this.props.onRef){
            this.props.onRef(this);
        }
        let selectData = this.props.treeData;
		this.setState({
			selectData
		})
    }
    handleChange = (index,value) => {
        let {selectedData,selectData} = this.state;
        let obj = {};
        let flag = true;
        for(let i=0,len=selectData[index].children.length;i<len;i++){
            if(value==selectData[index].children[i].key){
                obj.children=[selectData[index].children[i]];
                obj.key=selectData[index].key;
                obj.id=selectData[index].id;
                obj.name=selectData[index].name;
            }
        }
        if(selectedData.length>0){
            for(let i=0,len=selectedData.length;i<len;i++){
                if(obj.key==selectedData[i].key){
                    selectedData[i]=JSON.parse(JSON.stringify(obj))
                    flag=false;
                }
            }
            if(flag){
                selectedData.push(
                    JSON.parse(JSON.stringify(obj))
                )
            }
        }else{
            selectedData.push(
                JSON.parse(JSON.stringify(obj))
            )
        } 
        this.setState({
            selectedData
        })
        this.props.getSelectedData(selectedData);
	};
	hangdleDel=(index)=>{
        let {selectedData} = this.state;
        selectedData.splice(index,1);
        this.setState({
            selectedData
        })
        this.props.getSelectedData(selectedData);
    }	
    clearSelectedData=()=>{
        this.setState({
            selectedData:[]
        })
    }
    render () {
        let {selectData,selectedData}=this.state;
        return (
            <div>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}>
                <NCRow>
                    <NCCol lg={12} xs={12} sm={12}>
                        {
                        selectData.map((item,index)=>{
                            return (
                                <div>
                                    <NCRow>
                                        <NCCol lg={2} xs={2} sm={2}>{item.name}</NCCol>
                                        <NCCol lg={10} xs={10} sm={10}>
                                            <NCSelect
                                                defaultValue=""
                                                style={{ width: 200, marginRight: 6 }}
                                                onSelect={this.handleChange.bind(this,index)}
                                            >
                                                {item.children.map((slectOpt)=>{
                                                    return(
                                                        <NCOption title={slectOpt.id} value={slectOpt.key}>{slectOpt.name}</NCOption>
                                                    )
                                                })}
                                            </NCSelect>
                                        </NCCol>
                                    </NCRow>
                                </div>
                                    )
                            })
                        }
                    </NCCol>
                    <NCCol lg={12} xs={12} sm={12}>
                    {
                        selectedData.map((item,index)=>{
                            return (
                                <div>
                                    <NCRow>
                                        <NCCol lg={2} xs={2} sm={2}>{item.name}</NCCol>
                                        <NCCol lg={10} xs={10} sm={10}>{
                                            item.children.map((slectOpt)=>{
                                                return(
                                                    <span style={{'line-height':'28px'}} value={slectOpt.key}>{slectOpt.name} 
                                                        <NCIcon style={{'font-size':'10px','color':'red'}} type="uf-close-bold" onClick={
                                                            this.hangdleDel.bind(this,index)
                                                        }></NCIcon>
                                                    </span>
                                                )
                                            })
                                        }</NCCol>
                                    </NCRow>
                                </div>
                                    )
                            })
                        }
                    </NCCol>
                </NCRow>
                </NCDiv>
                
            </div>
        )
    }
}
