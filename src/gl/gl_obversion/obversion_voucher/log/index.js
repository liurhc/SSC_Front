import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, ajax, cardCache, getMultiLang, createPageIcon } from 'nc-lightapp-front';
let { setDefData, getDefData } = cardCache;
const {
    NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button,
    NCRadio: Radio, NCBreadcrumb: Breadcrumb, NCRow: Row, NCCol: Col, NCTree: Tree,
    NCMessage: Message, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCDiv
} = base;
import SearchModal from './searchModal';
import HeaderArea from '../../../public/components/HeaderArea';
import {getTableHeight } from '../../../public/common/method.js';
import './index.less';
class ObVourcher extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            pk_accountingbook: { display: '', value: '' },
            mainData: [],
            selectData: [],
            columns: []
        }
    }
    componentWillMount() {
        let callback = (json) => {
            this.setState({
                json: json,
                columns: [
                    {
                        title: (<div fieldid="converttime" className="mergecells">{json['20020CONRL-000084']}</div>), 
                        dataIndex: "converttime",
                         key: "converttime", /* 国际化处理： 折算日期*/
                        render: (text, record, index) => (
                            <div fieldid="converttime">
                                {this.state.mainData[index].converttime?this.state.mainData[index].converttime:<span>&nbsp;</span>}
                            </div>
                        )
                    },
                    {
                        title: (<div fieldid="prepareddate" className="mergecells">{json['20020CONRL-000124']}</div>),
                         dataIndex: "prepareddate",
                          key: "prepareddate",/* 国际化处理： 日期*/
                        render: (text, record, index) => (
                            <div fieldid="prepareddate">
                                {this.state.mainData[index].prepareddate?this.state.mainData[index].prepareddate:<span>&nbsp;</span>}
                            </div>
                        )
                    },
                    {
                        title: (<div fieldid="voucherno" className="mergecells">{json['20020CONRL-000125']}</div>),
                         dataIndex: "voucherno",
                          key: "voucherno", /* 国际化处理： 凭证号*/
                        render: (text, record, index) => (
                            <div fieldid="voucherno">
                                {this.state.mainData[index].voucherno?this.state.mainData[index].voucherno:<span>&nbsp;</span>}
                            </div>
                        )
                    },
                    {
                        title: (<div fieldid="explanation" className="mergecells">{json['20020CONRL-000126']}</div>),
                         dataIndex: "explanation",
                          key: "explanation",/* 国际化处理： 凭证摘要*/
                        render: (text, record, index) => (
                            <div fieldid="explanation">
                                {this.state.mainData[index].explanation?this.state.mainData[index].explanation:<span>&nbsp;</span>}
                            </div>
                        )
                    },
                    {
                        title: (<div fieldid="debit" className="mergecells">{json['20020CONRL-000127']}</div>),
                         dataIndex: "debit",
                          key: "debit",/* 国际化处理： 借*/
                        className: 't-a-r',
                        render: (text, record, index) => (
                            <div fieldid="debit">
                                {this.state.mainData[index].debit?this.state.mainData[index].debit:<span>&nbsp;</span>}
                            </div>
                        )
                    },
                    {
                        title: (<div fieldid="credit" className="mergecells">{json['20020CONRL-000128']}</div>),
                         dataIndex: "credit",
                          key: "credit",/* 国际化处理： 贷*/
                        className: 't-a-r',
                        render: (text, record, index) => (
                            <div style={{ 'textAlign': 'right!important' }} fieldid="credit">
                                {this.state.mainData[index].credit?this.state.mainData[index].credit:<span>&nbsp;</span>}
                            </div>
                        )
                    },
                    {
                        title: (<div fieldid="state" className="mergecells">{json['20020CONRL-000129']}</div>),
                         dataIndex: "state",
                          key: "state",/* 国际化处理： 折算状态*/
                        render: (text, record, index) => (
                            <div fieldid="state">
                                {this.state.mainData[index].state?this.state.mainData[index].state:<span>&nbsp;</span>}
                            </div>
                        )
                    },
                    {
                        title: (<div fieldid="message" className="mergecells">{json['20020CONRL-000140']}</div>),
                         dataIndex: "message",
                          key: "message",/* 国际化处理： 附件信息*/
                        render: (text, record, index) => (
                            <div fieldid="message">
                                {this.state.mainData[index].message?this.state.mainData[index].message:<span>&nbsp;</span>}
                            </div>
                        )
                    }
                ]
            }, () => { })
        }
        getMultiLang({ moduleId: '20020CONRL', domainName: 'gl', currentLocale: 'simpchn', callback });
    }
    componentDidMount() {
        //this.getData();
        let mainData = getDefData(123, 'gl_obervision.discount.logData');
        if (mainData) {
            this.setState({
                mainData
            })
        }
    }
    getSearchData = (data) => {
        let url = "/nccloud/gl/voucher/fclogquery.do";
        let self = this;
        ajax({
            url,
            data,
            success: function (response) {
                const { data } = response;
                if (data) {
                    self.setState({
                        mainData: data
                    })
                } else {
                    self.setState({
                        mainData: []
                    })
                }
            }
        });
    }
    handleBack = () => {
        this.props.pushTo('/list', {'backfrom':'log'});
    }
    render() {
        let { mainData, selectData } = this.state;
        let columns = this.state.columns;
        return (
            <div className='ov-moredata-log nc-bill-list'>
                <HeaderArea 
                    title = {this.state.json['20020CONRL-000141']}
                    initShowBackBtn={true}
                    backBtnClick={this.handleBack} //返回按点击事件
                    btnContent = {  
                        <SearchModal
                            getSearchData={this.getSearchData.bind(this)}
                        />
                    }
                />
                <NCDiv fieldid="log" areaCode={NCDiv.config.TableCom}>
                    <Table
                        columns={columns}
                        data={mainData}
                        // style={{padding:'5px'}}
                        bordered={true}
                        bodyStyle={{height:getTableHeight(80)}}
                        scroll={{ x: true, y: getTableHeight(80) }}
                    />
                </NCDiv>
            </div>
        );
    }
}
ObVourcher = createPage({
    // initTemplate: initTemplate
})(ObVourcher);
export default ObVourcher;
