import React, { Component } from 'react';
import {high,base,ajax,createPage,cardCache,toast,getMultiLang } from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
const { 
    NCFormControl: FormControl,NCButton: Button,
    NCRadio,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,
	NCCheckbox:Checkbox,NCModal:Modal,NCDatePicker
    } = base;
const { Refer } = high;
const NCOption = NCSelect.NCOption;
import AccountBookTreeRef from '../../../../gl_cashFlows/pk_book/refer_pk_book';
import './index.less'
class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            pk_srcbook:{
                refname:'',
                refpk:''
            },
            pk_desbook:{
                refname:'',
                refpk:''
            },
            selectData:[],//业务规则数据
            searchData:{
                pk_convertrule:'',
                pk_desbook:'',
                pk_srcbook:'',
                state:"0",
                begindate:'',
                enddate:'',
            }
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        
    }
    close() {
        this.setState({
            showModal:false
        })
    }
    componentDidMount(){
        //获取默认账簿
        let url = '/nccloud/platform/appregister/queryappcontext.do';
        let data={
            appcode: this.props.getSearchParam('c')
        }
        let self=this;
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                    if(!data.defaultAccbookName){
                        return
                    }
                    let pk_srcbook={
                            refname:data.defaultAccbookName,
                            refpk:data.defaultAccbookPk
                        }
                    let {searchData} = self.state;
                    searchData.pk_srcbook=pk_srcbook.refpk;
                    self.setState({
                        pk_srcbook
                    })
                    ajax({
                        url:'/nccloud/gl/glpub/queryBizDate.do',
                        data:{"pk_accountingbook":pk_srcbook.refpk,'needaccount':false},
                        success: function(response) { 
                            const { data, error, success } = response;
                            if (success&&data) {
                                if(data.bizPeriod){
                                    searchData.begindate=data.begindate;
                                    searchData.enddate=data.enddate;
                                }else{
                                    searchData.begindate='';
                                    searchData.enddate='';
                                }
                                self.setState({
                                    searchData
                                })
                            } else {
                                
                            }    
                        }
                    });
                } else {
                }
            }
        });
    }
    handleConfirm=()=>{//确认
        let searchData = this.state.searchData;
        if(!searchData.pk_srcbook){
            toast({ content: this.state.json['20020CONRL-000056'], color: 'warning' });/* 国际化处理： 请选择账簿*/
            return;
        }
        if(!searchData.pk_desbook){
            toast({ content: this.state.json['20020CONRL-000142'], color: 'warning' });/* 国际化处理： 请选择对应关系*/
            return;
        }
        this.setState({
            showModal:false
        })
        this.props.getSearchData(this.state.searchData)
    }
    getPk_soblink=(searchData)=>{//获取pk_soblink
        if(!searchData){
            searchData=this.state.searchData
        }
        let url = "/nccloud/gl/voucher/soblinkQueryByBook.do";
        let data={
            pk_srcbook:searchData.pk_srcbook,
            pk_desbook:searchData.pk_desbook
        }
        if(!data.pk_desbook||!data.pk_srcbook){
            return
        }
        let self = this;
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                    self.setState({
                        pk_soblink:data.pk_soblink
                    })
                    // resolve(data)
                    self.getRules(data)
                }
            }
		});
    }
    getRules=(data)=>{//获取业务规则
        let url = "/nccloud/gl/voucher/fcrulequery.do";
        let self = this;
        // let url = "/gl_web/gl/voucher/convert.do";
        ajax({
            url,
            data:{pk_soblink: data.pk_soblink},
            success: function(response) {
                self.setState({
                    isLoading: false
                });
                const { data, error, success } = response;
                if (success&&data) {
                    let obj = {
                        value:'',
                        display:''
                    }
                    let selectData = []
                    data.map((item,index)=>{
                        obj={
                            value:item.pk_convertrule,
                            display:item.rulename
                        }
                        selectData.push(JSON.parse(JSON.stringify(obj)))
                    })
                    self.setState({
                        selectData
                    })
                    // self.getTableDetail(data[0])
                } else {
                    // toast({ content: '无请求数据', color: 'warning' });
                }
            }
        })
    }
    render () {
        let {showModal,pk_srcbook,searchData,pk_desbook,selectData} = this.state;
        return (
        <div>
            <Button onClick={()=>{
                this.setState({
                    showModal:true
                })
            }} colors="primary" fieldid="query">{this.state.json['20020CONRL-000059']}</Button>
            <Modal
            fieldid="query"
            show = { showModal }
            // size = 'lg'
            className='log-search-modal senior'
            onHide = { this.close } >
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20020CONRL-000143']}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <div className="nc-theme-form-input-c">
                    <Row>
                        <Col lg={3} sm={3} xs={3}>
                        <span style={{'color':'#E14C46'}}>*</span>{this.state.json['20020CONRL-000134']}
                        </Col>
                        <Col lg={9} sm={9} xs={9}>
                            <AccountBookTreeRef
                                fieldid="pk_srcbook"
                                value={{'refname': pk_srcbook.refname, refpk: pk_srcbook.refpk}}
                                disabledDataShow={true}
                                queryCondition={{
                                    "appcode": this.props.getSearchParam('c'),
                                    TreeRefActionExt:'nccloud.web.gl.ref.AccountBookRefSqlBuilder'
                                }}
                                attrcode={'attrcode1'}
                                onChange= {(v)=>{
                                    pk_srcbook.refpk=v.refpk;
                                    pk_srcbook.refname=v.refname;
                                    searchData.pk_srcbook = v.refpk;
                                    this.setState({
                                        pk_srcbook,
                                        searchData
                                    })
                                    this.getPk_soblink();
                                  }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={3} sm={3} xs={3} >
                        <span style={{'color':'#E14C46'}}>*</span>{this.state.json['20020CONRL-000144']}
                        </Col>
                        <Col lg={9} sm={9} xs={9} >
                            <AccountBookTreeRef
                                fieldid="pk_desbook"
                                value={{'refname': pk_desbook.refname, refpk: pk_desbook.refpk}}
                                attrcode={'attrcode2'}
                                disabledDataShow={true}
                                queryCondition={() => {
                                    return {
                                        "appcode": this.props.getSearchParam('c'),
                                        TreeRefActionExt:'nccloud.web.gl.fc.action.FCDesBookRefSqlBuilder,nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                        // nccloud.web.gl.fc.action.FCDesBookRefSqlBuilder
                                        pk_srcbook:pk_srcbook.refpk
                                    }
                                }}
                                onChange={(v)=>{
                                    pk_desbook.refpk=v.refpk;
                                    pk_desbook.refname=v.refname;
                                    searchData.pk_desbook = v.refpk;
                                    this.setState({
                                        pk_desbook,
                                        searchData
                                    })
                                    this.getPk_soblink();
                                  }
                                }
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={3} sm={3} xs={3}>
                            <span>{this.state.json['20020CONRL-000137']}</span>
                        </Col>
                        <Col lg={9} sm={9} xs={9}>
                            <NCSelect
                                showClear={true}
                                fieldid="pk_convertrule"
                                onChange={(v)=>{
                                    searchData.pk_convertrule=v
                                    this.setState({
                                        searchData
                                    })
                                }}
                            >
                                {
                                    selectData.map((item)=>{
                                        return (
                                            <NCOption value={item.value}>{item.display}</NCOption>
                                        )
                                    })
                                }
                            </NCSelect>
                        </Col>  
                    </Row>
                    <Row>
                        <Col lg={3} sm={3} xs={3}>
                                <span>{this.state.json['20020CONRL-000145']}</span>
                            </Col>
                            <Col lg={9} sm={9} xs={9}>
                                <NCSelect
                                    showClear={false}
                                    fieldid="state"
                                    value={searchData.state}
                                    onChange={(v)=>{
                                        searchData.state=v
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                >
                                    <NCOption value={'0'}>{this.state.json['20020CONRL-000146']}</NCOption>
                                    <NCOption value={'1'}>{this.state.json['20020CONRL-000147']}</NCOption>
                                    <NCOption value={'2'}>{this.state.json['20020CONRL-000148']}</NCOption>
                                    <NCOption value={'3'}> {this.state.json['20020CONRL-000149']}</NCOption>
                                    <NCOption value={'4'}>{this.state.json['20020CONRL-000150']}</NCOption>
                                    <NCOption value={'5'}>{this.state.json['20020CONRL-000151']}</NCOption>
                                    <NCOption value={'6'}>{this.state.json['20020CONRL-000152']}</NCOption>
                                    <NCOption value={'7'}> {this.state.json['20020CONRL-000153']}</NCOption>
                                    <NCOption value={'8'}> {this.state.json['20020CONRL-000154']}</NCOption>
                                    <NCOption value={'9'}>{this.state.json['20020CONRL-000155']}</NCOption>
                                </NCSelect>
                                
                            </Col>  
                    </Row>
                    <Row>
                        <Col lg={3} sm={3} xs={3}>
                            {this.state.json['20020CONRL-000124']}
                        </Col>
                        <Col lg={4} sm={4} xs={4}>
                            <NCDatePicker
                                fieldid="begindate"
                                value={searchData.begindate}
                                onChange={(v)=>{
                                    searchData.begindate=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </Col>
                        
                        <Col lg={4} sm={4} xs={4}>
                            <NCDatePicker
                                fieldid="enddate"
                                value={searchData.enddate}
                                onChange={(v)=>{
                                    searchData.enddate=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </Col>
                    </Row>
                    </div>
                </Modal.Body>

                <Modal.Footer fieldid="bottom_area">
                     <Button onClick={ this.handleConfirm } colors="primary" fieldid="confirm">{this.state.json['20020CONRL-000059']}</Button>
                    <Button onClick={ this.close } shape="border" style={{marginRight: 6}} fieldid="close">{this.state.json['20020CONRL-000006']}</Button>
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
SearchModal = createPage({
	// initTemplate: initTemplate
})(SearchModal);
export default SearchModal;
