import React, { Component } from "react";
// import { Table } from 'tinper-bee';
// import multiSelect from "tinper-bee/lib/multiSelect.js";
import {ajax,base,getMultiLang } from 'nc-lightapp-front';
const { 
    NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,
    NCRadio,NCBreadcrumb:Breadcrumb,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,NCDiv,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCTabs:Tabs
    } = base;
const {TabPane} = Tabs;
import {getTableHeight } from '../../../public/common/method.js';
export default class ContentComponent extends Component {
  constructor(props){
    super(props);
    this.state={
      json:{},
      mainData:[],
      checkedAll:false,//是否全选
      selectData:[],//选中数据
      selIds:[],//选中行号
      checkedArray: [//各行选中判断
        
      ],
      toalPage:'1',
      columns:[],
    };
  }
  componentWillMount(){
    let callback= (json) =>{
       this.setState({json:json,
      columns : [
      { title: (<div fieldid="prepareddate" className="mergecells">{json['20020CONRL-000124']}</div>),
         dataIndex: "prepareddate",
          key: "prepareddate", /* 国际化处理： 日期*/
        width:'120px',
        render: (text, record, index) => (
          <div fieldid="prepareddate">
            {this.state.mainData[index].prepareddate?this.state.mainData[index].prepareddate:<span>&nbsp;</span>}
          </div>
        )
        },
        { title: (<div fieldid="voucherno" className="mergecells">{json['20020CONRL-000125']}</div>),
         dataIndex: "voucherno",
          key: "voucherno",
          width:'120px',/* 国际化处理： 凭证号*/
        render: (text, record, index) => (
          <div fieldid="voucherno">
            {this.state.mainData[index].voucherno?this.state.mainData[index].voucherno:<span>&nbsp;</span>}
          </div>
        )},
        { title: (<div fieldid="explanation" className="mergecells">{json['20020CONRL-000126']}</div>),
         dataIndex: "explanation",
          key: "explanation",
          width:'120px',/* 国际化处理： 凭证摘要*/
          render: (text, record, index) => (
              <div fieldid="explanation">
                {this.state.mainData[index].explanation?this.state.mainData[index].explanation:<span>&nbsp;</span>}
              </div>
            )
        },
        { title: (<div fieldid="debit" className="mergecells">{json['20020CONRL-000127']}</div>),
         dataIndex: "debit",
          key: "debit",
          width:'120px',/* 国际化处理： 借*/
          className:'t-a-r',
          render: (text, record, index) => (
              <div fieldid="debit">
                {this.state.mainData[index].debit?this.state.mainData[index].debit:<span>&nbsp;</span>}
              </div>
            )
        },
        { title: (<div fieldid="credit" className="mergecells">{json['20020CONRL-000128']}</div>),
         dataIndex: "credit",
          key: "credit",
          width:'120px',/* 国际化处理： 贷*/
          className:'t-a-r',
          render: (text, record, index) => (
              <div fieldid="credit">
                {this.state.mainData[index].credit?this.state.mainData[index].credit:<span>&nbsp;</span>}
              </div>
          )
        },
        { title: (<div fieldid="state" className="mergecells">{json['20020CONRL-000129']}</div>),
         dataIndex: "state",
          key: "state",
          width:'120px',/* 国际化处理： 折算状态*/
          render: (text, record, index) => (
              <div fieldid="state">
                {this.state.mainData[index].state?this.state.mainData[index].state:<span>&nbsp;</span>}
              </div>
          )
        },
        { title: (<div fieldid="isdifflag" className="mergecells">{json['20020CONRL-000130']}</div>),
         dataIndex: "isdifflag",
          key: "isdifflag",width:'120px',/* 国际化处理： 是否差异*/
          render: (text, record, index) => (
              <div fieldid="isdifflag">
                {this.state.mainData[index].isdifflag?this.state.mainData[index].isdifflag:<span>&nbsp;</span>}
              </div>
          )
        },
      ]
    },()=>{ })
        }
    getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
  }
  componentWillReceiveProps(nextProps){
      let mainData = nextProps.mainData;
      let checkedArray = nextProps.checkedArray;
      let selIds = nextProps.selIds;
      
      this.setState({
        checkedArray,selIds,
      })
      if(mainData.length!=this.state.mainData.length){
        for(let i=0,len=mainData.length;i<len;i++){
          mainData[i].key=i;
        }
        this.setState({
          mainData
        })
      }
  }
  // getSelectedDataFunc=(data)=>{
  //   this.props.getSelectData(data)
  // }
  //处理多选
  onAllCheckChange = () => {//全选
    let self = this;
    let checkedArray = [];
    let listData = self.state.mainData.concat();
    let selIds = [];
    // let id = self.props.multiSelect.param;
    for (var i = 0; i < self.state.checkedArray.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
      selIds.push(i);
    }
    if(self.state.checkedAll){
      selIds=[]
    }
    self.setState({
      checkedAll: !self.state.checkedAll,
      checkedArray
    });
    self.props.setCheckedArray(checkedArray);
    self.props.setCheckedselIds(selIds);
    let mainData = this.state.mainData;
    let selectArr = [];
    
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(mainData[selIds[i]])));
    }
    this.props.getSelectData(selectArr);
    // self.props.getSelectData(selIds);
  };
  onCheckboxChange = (text, record, index) => {//单选
    let self = this;
    let allFlag = false;
    let selIds = self.state.selIds;
    // let id = self.props.postId;
    let checkedArray = self.state.checkedArray.concat();
    if (self.state.checkedArray[index]) {
      selIds.splice(record.key,1);
      for(let i=0,len=selIds.length;i<len;i++){
        if(record.key==selIds[i]){
          selIds.splice(i,1)
          break;
        }
      }
    } else {
      selIds.push(record.key);
    }
    checkedArray[index] = !self.state.checkedArray[index];
    for (let i = 0; i < self.state.checkedArray.length; i++) {
      if (!checkedArray[i]) {
        allFlag = false;
        break;
      } else {
        allFlag = true;
      }
    }
    self.setState({
      checkedAll: allFlag,
      checkedArray
    });
    self.props.setCheckedArray(checkedArray);
    self.props.setCheckedselIds(selIds);
    let mainData = this.state.mainData;
    let selectArr = [];
    selIds = this.sortarr(selIds);
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(mainData[selIds[i]])));
    }
    this.props.getSelectData(selectArr);
  };
  renderColumnsMultiSelect(columns) {
    const { data,checkedArray,mainData } = this.state;
    let { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // multiSelect= "checkbox";
    // let indeterminate_bool1 = true;
    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArray.length;
      while(i--){
          if(checkedArray[i]){
            indeterminate_bool = true;
            break;
          }
      }
      let defaultColumns = [
        {
          title:  (<div fieldid="firstcol" className='checkbox-mergecells'>{
            <Checkbox
              className="table-checkbox"
              checked={this.state.checkedAll}
              indeterminate={indeterminate_bool&&!this.state.checkedAll}
              onChange={this.onAllCheckChange}
            />
          }</div>),
          key: "checkbox",
          attrcode: "checkbox",
          dataIndex: "checkbox",
          width: "50px",
          render: (text, record, index) => {
            return (
              <div fieldid="firstcol">
              <Checkbox
                className="table-checkbox"
                checked={this.state.checkedArray[index]}
                onChange={this.onCheckboxChange.bind(this, text, record, index)}
              />
              </div>
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
  sortarr=(arr)=>{//排序
    for(let i=0;i<arr.length-1;i++){
        for(let j=0;j<arr.length-1-i;j++){
            if(arr[j]>arr[j+1]){
                var temp=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
    return arr;
  }
  handleSelect(eventKey) {
    let data = this.getmainData(this.state.mainData,eventKey);
    this.setState({
      mainData: data,
      activePage: eventKey
    });
  }
  getmainData=(data,index)=>{
    return (data.slice(index*20-20,index*20))
  }
  render() {
    let columns = this.renderColumnsMultiSelect(this.state.columns);
    let {mainData,totalPage}=this.state;
    return (
		<div className='ov-table'>
			<NCDiv fieldid="obversion" areaCode={NCDiv.config.TableCom}>
				<Table
					columns={columns}
					data={mainData}
					bodyStyle={{height:getTableHeight(80)}}
          scroll={{ x: true, y: getTableHeight(80) }}
					// bordered
				/>
			</NCDiv>
		</div>
    );
  }
}
ContentComponent.defaultProps = {
  prefixCls: "bee-table",
  multiSelect: {
    type: "checkbox",
    param: "key"
  }
};
