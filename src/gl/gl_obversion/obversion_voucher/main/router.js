import { asyncComponent } from 'nc-lightapp-front';
import ObVourcher from './obversionVoucher';

const log = asyncComponent(() => import(/* webpackChunkName: "reva_demo/module/saleorder/card/card" */ /* webpackMode: "eager" */'../log/index'));
const moreData = asyncComponent(() => import(/* webpackChunkName: "reva_demo/module/saleorder/ref4C/ref4C" */ /* webpackMode: "eager" */ '../moreData/index'));

const routes = [
	{
		path: '/',
		component: ObVourcher,
		exact: true
	},
	{
		path: '/list',
		component: ObVourcher
	},
	{
		path: '/log',
		component: log
	},
	{
		path: '/moreData',
		component: moreData
	}
];

export default routes;
