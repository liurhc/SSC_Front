import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, ajax, promptBox, cardCache, getMultiLang, createPageIcon } from 'nc-lightapp-front';
let { setDefData, getDefData } = cardCache;
const {
	NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button,
	NCRadio: Radio, NCBreadcrumb: Breadcrumb, NCRow: Row, NCCol: Col, NCTree: Tree,
	NCMessage: Message, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect,
	NCCheckbox: Checkbox, NCDropdown: Dropdown, NCPanel: Panel,
	NCButtonGroup: ButtonGroup, NCMenu: Menu,NCDiv
} = base;
import ContentComponent from '../content';
import DiscountModal from '../discountModal';
import CountBeginModal from '../countBeginModal';
import SearchModal from '../searchModal';
import './index.less';
import { toast } from '../../../public/components/utils.js';
import HeaderArea from '../../../public/components/HeaderArea';
import { openToVoucher } from '../../../public/common/voucherUtils';
const {
	Item
} = Menu;
// @withShell
class ObVourcher extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {},
			mainData: [],
			selectData: [],
			discountShow: false,
			disbeginShow: false,
			searchModalShow: false,//查询框判断
			searchData: { fcstate: '0', isdifButton: 'N' },//查询数据
			checkedArray: [],//判断选中
			pk_soblink: '',//账簿关系
			selectDataRules: [],//折算规则
			selectDataRulesdefault: '',//默认选中规则
			selIds: [],//多选用
		}
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => { })
		}
		getMultiLang({ moduleId: '20020CONRL', domainName: 'gl', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {
		//this.getData();
		let state=getDefData('obversion_voucher','gl_obervision.discount.moreData');
		let data = getDefData(12345, 'gl_obervision.discount.searchData');
		if(state){
			this.state=state;
			this.setState(state);
			if((this.props.getSearchParam('backfrom')&&his.props.getSearchParam('backfrom')=='log')||(this.props.getUrlParam('backfrom')&&this.props.getUrlParam('backfrom')=='log')){//折算日志返回的重新查数据
				if (data) {
					this.getSearchData(data)
				}
			}
		}else{
			// let data = getDefData(12345, 'gl_obervision.discount.searchData');
			if (data) {
				this.getSearchData(data)
			}
		}
		this.setState({
			searchModalShow: false
		})
		let pagecode = this.props.getSearchParam('p')
		let appcode = this.props.getSearchParam('c')
		let self = this;
		ajax({
			url: '/nccloud/platform/appregister/queryallbtns.do',
			data: {
				pagecode: pagecode,
				appcode: appcode
			},
			success: (res) => {
				if (!res.data) {
					return
				}
				/*  拿到按钮数据，调用setButtons方法将数据设置到页面上，然后执行后续的按钮逻辑 */
				self.props.button.setButtons(res.data);
				if (self.state.selectData.length == 0) {
					self.props.button.setButtonDisabled({
						'convert': true, 'unconvert': true, 'diff': true,
						'link': true
					})
				}
			}
		});
	}
	setButtonDis = (searchData, selectData) => {//按钮置灰
		this.props.button.setButtonDisabled({
			'convert': false, 'unconvert': false, 'detail': false, 'diff': false, 'diff': true
		})
		if (selectData && selectData.length == 0) {
			this.props.button.setButtonDisabled({
				'link': true
			})
		} else if (selectData && selectData.length > 0) {
			this.props.button.setButtonDisabled({
				'link': false
			})
		}
		if (searchData.fcstate == '1') {
			this.props.button.setButtonDisabled({
				'convert': true,
			})
		}
		if (searchData.fcstate == '0') {
			this.props.button.setButtonDisabled({
				'unconvert': true,
			})
		}
		if (searchData.fcstate == '0') {
			this.props.button.setButtonDisabled({
				'detail': true,
			})
		}
		if (searchData.fcstate == '2') {
			this.props.button.setButtonDisabled({
				'convert': true,
			})
		}
		if (searchData.fcstate == '0') {
			this.props.button.setButtonDisabled({
				'linkopp': true,
			})
		} else {
			this.props.button.setButtonDisabled({
				'linkopp': false,
			})
		}
		if (searchData.fcstate == '0' && searchData.isdifButton == 'Y') {
			this.props.button.setButtonDisabled({
				'diff': false,
			})
		}
	}
	handleShowSearch = (v) => {
		this.setState({
			searchModalShow: true
		})
	}
	getSearchData = (data) => {
		let self = this;
		let url = "/nccloud/gl/voucher/fcdataquery.do";
		self.setState({
			searchData: data
		})
		let searchData = data;
		this.setButtonDis(data);
		ajax({
			url,
			data: data,
			success: function (response) {
				const { data, error, success } = response;
				if (success && data) {
					let checkedArray = [];
					for (let i = 0, len = data.length; i < len; i++) {
						checkedArray.push(false)
					}
					self.setState({
						mainData: [],
						checkedArray: []
					})
					self.setState({
						mainData: data,
						checkedArray,
						selectData: [],
						selIds: []
					})
					// self.getTableDetail(data[0])
				} else {
					self.setState({
						mainData: [],
						checkedArray: [],
						selectData: [],
						selIds: []
					})
					// toast({ content: '无请求数据', color: 'warning' });
				}
			}
		});
		//获取账簿关系
		ajax({
			url: "/nccloud/gl/voucher/soblinkQueryByBook.do",
			data: {
				pk_srcbook: searchData.pk_srcbook,
				pk_desbook: searchData.pk_desbook
			},
			success: function (response) {
				const { data, error, success } = response;
				if (success && data) {
					self.setState({
						pk_soblink: data.pk_soblink,
					})
					let url = "/nccloud/gl/voucher/fcrulequery.do";
					//获取业务规则
					ajax({
						url,
						data: { pk_soblink: self.state.pk_soblink },
						success: function (response) {
							self.setState({
								isLoading: false
							});
							const { data, error, success } = response;
							if (success && data) {
								let obj = {
									value: '',
									display: ''
								}
								let selectData = []
								data.map((item, index) => {
									obj = {
										value: item.pk_convertrule,
										display: item.rulename
									}
									if (item.defaultrule == 'Y') {
										let selectDataRulesdefault = item.pk_convertrule
										self.setState({
											selectDataRulesdefault
										})
									}
									selectData.push(JSON.parse(JSON.stringify(obj)))
								})
								self.setState({
									selectDataRules: selectData
								})
								// self.getTableDetail(data[0])
							} else {
								self.setState({
									selectDataRules: []
								})
							}
						}
					})
				}
			}
		});
	}
	setCheckedArray = (data) => {//获取选中
		this.setState({
			checkedArray: data
		})
	}
	setCheckedselIds = (data) => {
		this.setState({
			selIds: data
		})
	}
	handleDiscount = (data) => {
		this.setState({
			discountShow: true
		})
	}
	getsearchModalShow = (data) => {
		this.setState({
			searchModalShow: data
		})
	}
	getDisShow = (data) => {
		this.setState({
			discountShow: data
		})
	}
	getSelectData = (data) => {
		this.setButtonDis(this.state.searchData, data)
		this.setState({
			selectData: data
		})
	}
	handleDisBegin = () => {//折算期初
		promptBox({
			color: 'info',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
			title: this.state.json['20020CONRL-000003'],                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 请注意*/
			content: this.state.json['20020CONRL-000156'],             // 提示内容,非必输/* 国际化处理： 该操作会清除目的方的期初数据，是否继续？*/
			noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
			noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
			beSureBtnName: this.state.json['20020CONRL-000005'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
			cancelBtnName: this.state.json['20020CONRL-000006'],           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
			beSureBtnClick: () => {
				this.setState({
					disbeginShow: true
				})
			},   // 确定按钮点击调用函数,非必输
		})
	}
	getDisbeginShow = (data) => {
		this.setState({
			disbeginShow: data
		})
	}
	handleCancel = () => {//取消
		let selectData = this.state.selectData;
		let arrSel = [];
		selectData.map((item) => {
			arrSel.push(item.pk_srcvoucher)
		})
		let data = {
			pk_srcvoucher: arrSel,
			showvoucher: this.state.searchData.showvoucher,
			pk_desbook: this.state.searchData.pk_desbook,
			pk_srcbook: this.state.searchData.pk_srcbook,
		};
		let url = "/nccloud/gl/voucher/unconvert.do";
		let self = this;
		// let url = "/gl_web/gl/voucher/convert.do";
		ajax({
			url,
			data,
			success: function (response) {
				const { data } = response
				setDefData(123, 'gl_obervision.discount.logData', data);
				self.props.pushTo('/log', {
					pk: '',
					name: ''
				})
			}
		});
	}
	handleLog = () => {//日志
		setDefData('obversion_voucher','gl_obervision.discount.moreData',this.state);
		this.props.pushTo('/log', {})
	}
	handleMore = () => {//明细
		let mainData = this.state.mainData;
		let arrSel = [];
		mainData.map((item) => {
			arrSel.push(item.pk_srcvoucher)
		})
		setDefData(1234, 'gl_obervision.discount.moreData',
			{
				pk_desbook: this.state.searchData.pk_desbook,
				pk_srcbook: this.state.searchData.pk_srcbook,
				fcstate: this.state.searchData.fcstate,
				showvoucher: this.state.searchData.showvoucher,
				pk_srcvoucher: arrSel
			}
		);
		setDefData('obversion_voucher','gl_obervision.discount.moreData',this.state);
		this.props.pushTo('/moreData', {})
	}
	handlevoucherdiff = () => {//差异凭证
		let { selectData, mainData } = this.state;
		let arrSel = [];
		let obj = {};
		selectData.map((item) => {
			obj = {
				pk_srcvoucher: item.pk_srcvoucher,
				isdifflag: item.isdifflag
			}
			arrSel.push(Object.assign({}, obj))
		})
		let self = this;
		ajax({
			url: '/nccloud/gl/voucher/voucherdiff.do',
			data: arrSel,
			success: function (response) {
				const { data, error, success } = response;
				let flagNUM = 0;
				if (success) {
					for (let i = 0, len = mainData.length; i < len; i++) {
						for (let j = 0, lenj = selectData.length; j < lenj; j++) {
							if (mainData[i].pk_srcvoucher == selectData[j].pk_srcvoucher) {
								if (mainData[i].isdifflag) {
									mainData[i].isdifflag = ''
								} else {
									mainData[i].isdifflag = self.state.json['20020CONRL-000157']/* 国际化处理： 差异*/
								}
								flagNUM++
								if (flagNUM == lenj) {
									self.setState({
										mainData
									})
									return
								}
							}
						}
					}
				} else {
					// toast({ content: '无请求数据', color: 'warning' });
				}
			}
		});
	}
	handVoucher = (v) => {//联查
		if (this.state.selectData.length == 0) {
			toast({ content: this.state.json['20020CONRL-000158'], color: 'warning' });/* 国际化处理： 无选中行*/
			return;
		}
		let selectData = this.state.selectData;
		let idData = [];
		if (v == 1) {
			idData = selectData[0].pk_desvoucher
		} else if (v == 2) {
			idData = selectData[0].pk_srcvoucher
		}
		let param = {
			pk_voucher: idData
		}
		openToVoucher(this,param)
		// let voucherapp=voucherRelatedApp(voucher_gen);
		// this.props.openTo('/gl/gl_voucher/pages/main/index.html#/Welcome',
		// 	{
		// 		// link:JSON.stringify(link),
		// 		appcode: voucherapp.appcode,//'20020PREPA',
		// 		c: voucherapp.appcode,//'20020PREPA',
		// 		id: idData,
		// 		status: 'browse',
		// 		pagekey: 'link'
		// 	}
		// )
	}
	toLogHand = () => {
		this.props.pushTo('/log', {
			pk: '',
			name: ''
		})
	}
	onButtonClick = (props, id) => {
		let selectData = this.state.selectData;
		if (selectData.length == 0 && id != 'query' && id != 'convertinit' && id != 'detail' && id != 'convertlog' && 'id' != 'convertinit') {
			toast({ content: this.state.json['20020CONRL-000159'], color: 'warning' });/* 国际化处理： 请选择数据*/
			return
		}
		switch (id) {
			case 'query':
				this.handleShowSearch();
				break;
			case 'convert':
				this.handleDiscount();
				break;
			case 'unconvert':
				this.handleCancel();
				break;
			case 'convertinit':
				this.handleDisBegin();
				break;
			case 'convertlog':
				this.handleLog();
				break;
			case 'detail':
				this.handleMore();
				break;
			case 'diff':
				this.handlevoucherdiff();
				break;
			case 'linkopp':
				this.handVoucher(1);
				break;
			case 'linkself':
				this.handVoucher(2);
				break;
			default:
				break;
		}
	}
	render() {
		let { mainData, selectData, discountShow, disbeginShow, searchModalShow, searchData, selectDataRules,
			checkedArray, pk_soblink, selectDataRulesdefault, selIds } = this.state;
		let { createButtonApp } = this.props.button;
		return (
			<div className='discount-voucher-main nc-bill-list'>
				<HeaderArea
					title={this.state.json['20020CONRL-000160']}

					btnContent={
						createButtonApp({
							area: 'head',
							onButtonClick: this.onButtonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})
					}
				/> 
				<ContentComponent mainData={mainData}
					getSelectData={this.getSelectData.bind(this)}
					checkedArray={checkedArray}
					selIds={selIds}
					setCheckedArray={this.setCheckedArray.bind(this)}
					setCheckedselIds={this.setCheckedselIds.bind(this)}
				></ContentComponent>
				<DiscountModal discountShow={discountShow}
					getDisShow={this.getDisShow.bind(this)}
					searchData={searchData}
					selectData={selectData}
					selectDataRules={selectDataRules}
					selectDataRulesdefault={selectDataRulesdefault}
					pk_soblink={pk_soblink}
					toLogHand={this.toLogHand.bind(this)}
				></DiscountModal>
				<CountBeginModal
					disbeginShow={disbeginShow}
					getDisbeginShow={this.getDisbeginShow.bind(this)}
					selectData={selectData}
				></CountBeginModal>
				<SearchModal
					searchModalShow={searchModalShow}
					getsearchModalShow={this.getsearchModalShow.bind(this)}
					getSearchData={this.getSearchData.bind(this)}
				/>
			</div>
		);
	}
}
ObVourcher = createPage({
	// initTemplate: initTemplate
})(ObVourcher);
export default ObVourcher;
