import React, {Component} from 'react';
// import ReactDOM from 'react-dom';
import {high,base,ajax,createPage,toast,cardCache,getBusinessInfo,getMultiLang } from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
import createScript from '../../../public/components/uapRefer.js';
const { NCModal,NCButton,NCRadio,NCRow:Row,NCCol:Col,NCDatePicker,NCCheckbox,NCTable:Table,
    NCSelect,NCInputNumber,NCNumber,NCDiv,NCFormControl } = base;
import AccperiodMonthTreeGridRef from '../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index';
import VoucherTypeDefaultGridRef from '../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef';
import ReferLoader from '../../../public/ReferLoader/index.js';
import './index.less';
import {InputItem,} from '../../../public/components/FormItems';
import ExplanationModal from './ExplanationModal';
import CtlSysRef from '../../../refer/voucher/CtlSysGridRef/index';
import AccountBookTreeRef from '../../../gl_cashFlows/pk_book/refer_pk_book';
const NCOption = NCSelect.NCOption;
class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            tableData:[],
            checkedArray:[],//选中数组
            showModal: false,
            configs: {},//人员参照
            selectArr:[],//辅助核算选中项
            pk_accountingbook:'',//参照用选中核算账簿
            selIds:[],//多选需要
            explanationModalShow:false,//摘要弹框
            FinanceOrg:'0001A310000000000NN6',//组织标识
            pk_srcbook:{
                refcode:"",
                refname:"",
                refpk:""
            },
            pk_desbook:{
                refcode:"",
                refname:"",
                refpk:""
            },
            beginperiod :{
                refname:"",
                refpk:"",
                pk_accperiodscheme:''
            },
            endperiod :{
                refname:"",
                refpk:""
            },
            pk_vouchertype:{
                refcode:"",
                refname:"",
                refpk:""
            },
            pk_system:{
                refcode:"",
                refname:"",
                refpk:""
            },
            explanation:{},
            accountcode:[],
            oppositesubj:[],
            searchData:{
                pk_srcbook:'',  //核算账簿
                pk_desbook:'',
                querybyperiod:'Y', //是否按期间查询  Y/N
                begindate:'',
                enddate:'',
                beginyear:'',  //开始年
		        endyear:'', //结束年
		        beginperiod :'',//开始期间
                endperiod:'',
                explanation:'',//摘要
                pk_vouchertype:'',//凭证类别
                beginNo:'', //开始凭证号
                endNo :'',  //结束凭证号
                accountcode:'',// 科目编码   注意是编码不是主键  数组
                oppositesubj:'', // 对方科目编码  注意是编码不是主键  数组
                pk_system:'',//来源系统
                firstAmount:'',//开始金额
                secondAmount:'',  //结束金额
                direction:'0', //方向
                andOr:'and',  //and  或者 or
                fcstate:'0',//是否折算
                isdifflag:'0',//差异
                showvoucher:'0',//科目类型
                isdifButton:'',//判断差异按钮
                realtimerate:'',//即时汇率精度
            },
        };
        this.businessInfo='';
        this.close = this.close.bind(this);
        this.columns = [];
    }
    componentWillMount(){
        let searchDataState = getDefData(123456, 'gl_obervision.discount.searchDataState');
        if(searchDataState){
            searchDataState.showModal=false;
            this.state = searchDataState;
            let newPk = null;
            if(this.state.searchData.showvoucher==0){
                newPk=this.state.searchData.pk_srcbook;
            }else{
                newPk=this.state.searchData.pk_desbook;
            }
            this.getTableData(newPk);
            return;
        }
        if(getBusinessInfo()){
            if(getBusinessInfo().businessDate){
                this.businessInfo=getBusinessInfo().businessDate.split(' ')[0]
            }
        }
        //获取默认账簿
        let url = '/nccloud/platform/appregister/queryappcontext.do';
        let data={
            appcode: this.props.getSearchParam('c')
        }
        let searchData = getDefData(1111, 'gl_obervision.discount.state');
        if(searchData){
            this.state = searchData;
        }
        let self=this;
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                    if(!data.defaultAccbookName){
                        return
                    }
                    let pk_srcbook={
                            refname:data.defaultAccbookName,
                            refpk:data.defaultAccbookPk
                        }
                    let {searchData,beginperiod,endperiod} = self.state;
                    searchData.pk_srcbook=pk_srcbook.refpk;
                    self.setState({
                        pk_srcbook
                    })
                    self.changeBook(0,pk_srcbook.refpk)
                } else {
                }
            }
        });
        let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
            getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.searchModalShow;
        this.setState({
            showModal
        })
    }
    close() {
        this.props.getsearchModalShow(false);
    }
    handleConfirm=()=>{//确认时
        let {searchData,tableData,selectArr}=this.state;
        setDefData(1111, 'gl_obervision.discount.state', this.state);
        this.close();
        this.setState({
            showModal:false
        })
        let obj={};
        let localArr=[];
        if(!searchData.pk_srcbook){
            toast({color:"warning",content:this.state.json['20020CONRL-000056']})/* 国际化处理： 请选择账簿*/
            return
        }
        if(!searchData.pk_desbook){
            toast({color:"warning",content:this.state.json['20020CONRL-000171']})/* 国际化处理： 请选择账簿对应关系*/
            return
        }
        for(let i=0,len=tableData.length;i<len;i++){
            for(let j=0,lenj=selectArr.length;j<lenj;j++){
                if(tableData[i].pk_checktype==selectArr[j].pk_checktype){
                    obj={
                        pk_checktype:tableData[i].pk_checktype,
                        pk_checkvalue:tableData[i].value.length?
                            this.getPk_checkvalue(tableData[i].value)
                            :tableData[i].value.refpk
                        
                    }
                    localArr.push(Object.assign({},obj));
                }   
            }
        }
        for(let i=0,len=localArr.length;i<len;i++){
            for(let j=0,lenj=selectArr.length;j<lenj;j++){
                if(localArr[i].pk_checktype!=selectArr[j].pk_checktype){
                    localArr.splice(i,1);
                    len--;
                }   
            }
            if(selectArr.length==0){
                localArr = []
            }
        }
        searchData.assList=localArr;
        this.props.getSearchData(searchData)
        setDefData(12345, 'gl_obervision.discount.searchData', searchData);
        setDefData(123456, 'gl_obervision.discount.searchDataState', this.state);
    }
    //转换辅助核算数组数据
    getPk_checkvalue=(data)=>{
        if(data.length>0){
            let localArr = [];
            for(let i=0,len=data.length;i<len;i++){
                localArr.push(data[i].refpk)
            }
            return (localArr.join(','))
        }else{
            return ('')
        }
    }
    createCfg(id, param) {//
        var obj = {
            value: this.state.configs[id] ? this.state.configs[id].value : [],
            onChange: function (val) {
                var temp = Object.assign(this.state.configs[id], { value: val });
                this.setState(Object.assign(this.state.configs, temp));
            }.bind(this)
        }
        this.state.configs[id] = obj;
        var result_param = Object.assign(obj, param)
        return result_param;
    }
    //处理多选
  onAllCheckChange = () => {//全选
    let self = this;
    let checkedArray = [];
    let listData = self.state.tableData.concat();
    let selIds = [];
    // let id = self.props.multiSelect.param;
    for (var i = 0; i < self.state.checkedArray.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
      selIds.push(i);
    }
    if(self.state.checkedAll){
      selIds=[]
    }
    self.setState({
      checkedAll: !self.state.checkedAll,
      checkedArray: checkedArray,
      selIds: selIds
    });
    let tableData = this.state.tableData;
    let selectArr = [];
    
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(tableData[selIds[i]])));
    }
    this.setState({
        selectArr
    })
  };
  onCheckboxChange = (text, record, index) => {//单选
    let self = this;
    let allFlag = false;
    let selIds = self.state.selIds;
    // let id = self.props.postId;
    let checkedArray = self.state.checkedArray.concat();
    if (self.state.checkedArray[index]) {
        selIds.splice(record.key,1);
        for(let i=0,len=selIds.length;i<len;i++){
          if(record.key==selIds[i]){
            selIds.splice(i,1)
            break;
          }
        }
      } else {
        selIds.push(record.key);
      }
    checkedArray[index] = !self.state.checkedArray[index];
    for (let i = 0; i < self.state.checkedArray.length; i++) {
      if (!checkedArray[i]) {
        allFlag = false;
        break;
      } else {
        allFlag = true;
      }
    }
    self.setState({
      checkedAll: allFlag,
      checkedArray: checkedArray,
      selIds: selIds
    });
    
    let tableData = this.state.tableData;
    let selectArr = [];
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(tableData[selIds[i]])));
    }
    this.setState({
        selectArr
    })
  };
  renderColumnsMultiSelect(columns) {//添加多选框
    const { data,checkedArray,tableData } = this.state;
    let { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // multiSelect= "checkbox";
    // let indeterminate_bool1 = true;
    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArray.length;
      while(i--){
          if(checkedArray[i]){
            indeterminate_bool = true;
            break;
          }
      }
      let defaultColumns = [
        {
          title: (
            <NCCheckbox
              className="table-checkbox"
              checked={this.state.checkedAll}
              indeterminate={indeterminate_bool&&!this.state.checkedAll}
              onChange={this.onAllCheckChange}
            />
          ),
          key: "checkbox",
          dataIndex: "checkbox",
          width: "50",
          render: (text, record, index) => {
            return (
              <NCCheckbox
                className="table-checkbox"
                checked={this.state.checkedArray[index]}
                onChange={this.onCheckboxChange.bind(this, text, record, index)}
              />
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
  closeExplanationModal(data){
    this.setState({
        explanationModalShow:data
    })
  }
  dealTableData=(data)=>{//辅助核算表格
    let checkedArray=[];
    for(let i=0,len=data.length;i<len;i++){
        checkedArray.push(false);
        data[i].key=i;
        data[i].value={
            refpk:'',
            refname:''
        }
    }
    this.columns=[
        {
        title: (<span fieldid="name">{this.state.json['20020CONRL-000172']}</span>),/* 国际化处理： 核算类型名称*/
            dataIndex: "name",
            key: "name",
            width:'150px',
            render: (text, record, index) => (
              <span fieldid="name">
                {this.state.tableData[index].name}
              </span>
            )
          },
          {
            title: (<span fieldid="pname">{this.state.json['20020CONRL-000173']}</span>),/* 国际化处理： 核算内容*/
            dataIndex: "pname",
            key: "pname",
            width:'200px',
            render: (text, record, index) => (
              this.getContent(this.state.tableData[index], index)
            )
          }
    ]
    this.setState({
        tableData:data,
        checkedArray
    })
  }
  getContent=(data, index)=>{//辅助核算参照处理
      let flag = Number(data.datatype);
      let tableData = this.state.tableData;
      if(flag){
        if(flag==1||flag==4){
            return (
                <InputItem
                    type="customer"
                    name="scale"
                    defaultValue={data.value.refpk}
                    onChange={(v) => {
                         tableData[index].value.refpk=v
                         this.setState({
                            tableData
                         })
                    }}
                />
            )
          }else if(flag==31){
            return (
                <NCNumber
                    scale={Number(data.digit)}
                    value={data.value.refpk}
                    onChange={(v) => {
                         tableData[index].value.refpk=v
                         this.setState({
                            tableData
                         })
                    }}
                />
            )
          }
          else if(flag==32){
            return (
                <NCRadio.NCRadioGroup
                    name="123"
                    selectedValue={data.value.refpk}
                    onChange={(v)=>{
                        tableData[index].value.refpk=v
                        this.setState({
                            searchData
                        })
                    }}>
                    <NCRadio  value="Y">{this.state.json['20020CONRL-000029']}</NCRadio>
                    <NCRadio  value="N">{this.state.json['20020CONRL-000030']}</NCRadio>
                </NCRadio.NCRadioGroup>
            )
          }
          else if(flag==33||flag==34){
            return (
                <NCDatePicker
                    value={tableData[index].value.refpk}
                    showTime={true}
                    format='YYYY-MM-DD HH:mm:ss'
                    onChange={(v)=>{
                        tableData[index].value.refpk=v;
                        this.setState({
                            tableData
                         })
                    }}
                />
            )
          }
      }else {
        if(data.refpath){
            let referUrl= data.refpath+ '.js';
            if(!this.state[data.pk_checktype]){
                {createScript.call(this,referUrl,data.pk_checktype)}
            }
            else{
                if(data.classid.length==20){
                    return (
                        <div>
                            <div class='u-col-md-12 u-col-sm-12 u-col-xs-12'>
                                {this.state[data.pk_checktype] ? (this.state[data.pk_checktype])(
                                    {    
                                        isMultiSelectedEnabled: true,
                                        value:tableData[index].value,
                                        queryCondition:{pk_org:this.state.FinanceOrg,"DataPowerOperationCode" : 'fi'},
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi',
                                        isShowDisabledData:true,
                                        "pk_defdoclist":data.classid,
                                        onChange: (v) => {
                                            tableData[index].value=v;
                                            this.setState({
                                                tableData
                                            })
                                        },
                                    }
                                ) : <div />}
                            </div>
                        </div>
                    );
                }else if(data.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||data.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'){
                    return (
                        <div>
                            <div class='u-col-md-12 u-col-sm-12 u-col-xs-12'>
                                {this.state[data.pk_checktype] ? (this.state[data.pk_checktype])(
                                    {    
                                        value:tableData[index].value,
                                        unitProps:{
                                            refType: 'tree',
                                            refName: this.state.json['20020CONRL-000174'],/* 国际化处理： 业务单元*/
                                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                            rootNode:{refname:this.state.json['20020CONRL-000174'],refpk:'root'},/* 国际化处理： 业务单元*/
                                            placeholder:this.state.json['20020CONRL-000174'],/* 国际化处理： 业务单元*/
                                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                            treeConfig:{name:[this.state.json['20020CONRL-000015'], this.state.json['20020CONRL-000061']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                                            isMultiSelectedEnabled: false,
                                            //unitProps:unitConf,
                                        },
                                        unitCondition:{
                                            pk_financeorg:this.state.FinanceOrg,
                                            TreeRefActionExt:'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                                        },
                                        isShowUnit:true,
                                        isMultiSelectedEnabled:true,
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi',
                                        isShowDisabledData:true,
                                        "unitValueIsNeeded":false,
                                        "isShowDimission":true,
                                        queryCondition:{
                                            "busifuncode":"all",
                                            pk_org:this.state.FinanceOrg,
                                            "DataPowerOperationCode" : 'fi',
                                            isShowDimission:true
                                        },
                                        onChange: (v) => {
                                            tableData[index].value=v;
                                            this.setState({
                                                tableData
                                            })
                                        },
                                    }
                                ) : <div />}
                            </div>
                        </div>
                    );
                }
                else{
                    return (
                        <div>
                            <div class='u-col-md-12 u-col-sm-12 u-col-xs-12'>
                                {this.state[data.pk_checktype] ? (this.state[data.pk_checktype])(
                                    {    
                                        isMultiSelectedEnabled: true,
                                        value:tableData[index].value,
                                        queryCondition:{pk_org:this.state.FinanceOrg,"DataPowerOperationCode" : 'fi'},
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi',
                                        isShowDisabledData:true,
                                        onChange: (v) => {
                                            tableData[index].value = v;
                                            this.setState({
                                                tableData
                                            })
                                        },
                                    }
                                ) : <div />}
                            </div>
                        </div>
                    );
                }
            }
        }else{
            return <span></span>
        }
        
      }
  }
  handleShowExplanation=()=>{
    this.setState({
        explanationModalShow:true
    })
  }
  getFinanceOrg=(v)=>{//获取组织
    let data = {"pk_accountingbook":v};
    let url='/nccloud/gl/glpub/queryFinanceOrg.do';
    let self = this;
    ajax({
        url,
        data,
        success: function(response) { 
            const { data, error, success } = response;
            if (success&&data) {
                self.setState({
                    FinanceOrg:data.pk_org
                });
            } else {
                
            }    
        }
    });
  }
  getTableData=(v)=>{//获取辅助核算
    let url='/nccloud/gl/accountrep/assbalancequeryobject.do';
    let self = this;
    ajax({
        url,
        data:{"pk_accountingbook":v,'needaccount':false},
        success: function(response) { 
            const { data, error, success } = response;
            if (success&&data) {
                self.dealTableData(data);
            } else {
                
            }    
        }
    });
  }
  changeBook=(v,v1,v2)=>{
    //确定账簿值及选定的账簿；
    //查询相关值（pk_org,辅助核算等）；
    let pk_desbook = v2?v2:this.state.pk_desbook.refpk;
    let pk_srcbook = v1?v1:this.state.pk_srcbook.refpk;
    let {searchData,beginperiod,endperiod} = this.state;
    let pk_accountingbook;
    if(pk_desbook&&pk_srcbook){
        let url = "/nccloud/gl/voucher/soblinkQueryByBook.do";
        let data={
            pk_srcbook:pk_srcbook,
            pk_desbook:pk_desbook
        }
        let self = this;
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                    searchData.isdifButton=data.isdiff;
                    searchData.realtimerate=data.rtdigit;
                    self.setState({
                        searchData
                    })
                }
            }
		});
    }
    if(v===''){//选择核算账簿判断当前showvoucher类型
        v=this.state.searchData.showvoucher
    }
    if(v==0){//根据类型确定核算账簿
        pk_accountingbook = pk_srcbook;
    }else if(v==1){
        pk_accountingbook = pk_desbook;
    }
    this.setState({
        pk_accountingbook
    })
    let self=this;
    self.getFinanceOrg(pk_accountingbook);
    self.getTableData(pk_accountingbook);
    ajax({
        url:'/nccloud/gl/glpub/queryBizDate.do',
        data:{"pk_accountingbook":pk_accountingbook,'needaccount':false},
        success: function(response) { 
            const { data, error, success } = response;
            if (success&&data) {
                if(data.bizPeriod){
                    searchData.beginyear=data.bizPeriod.split('-')[0];
                    searchData.beginperiod=data.bizPeriod.split('-')[1];
                    searchData.begindate=data.begindate;
                    searchData.enddate=data.enddate;
                    beginperiod.refname=data.bizPeriod;
                    beginperiod.refpk=data.bizPeriod;
                    beginperiod.pk_accperiodscheme = data.pk_accperiodscheme;
                    searchData.endyear=data.bizPeriod.split('-')[0];
                    searchData.endperiod=data.bizPeriod.split('-')[1];
                    endperiod.refname=data.bizPeriod;
                    endperiod.refpk=data.bizPeriod;
                }else{
                    searchData.beginyear='';
                    searchData.beginperiod='';
                    beginperiod.refname='';
                    beginperiod.refpk='';
                    searchData.endyear='';
                    searchData.endperiod='';
                    endperiod.refname='';
                    endperiod.refpk='';
                }
                self.setState({
                    beginperiod,
                    searchData,
                    endperiod
                })
            } else {
                
            }    
        }
    });
  }
  getAppointData=(data)=>{//摘要
    let {searchData,explanation} =this.state;
    let localArr = searchData.explanation.split(',');
    let flag=false;
    for(let i=0,len=localArr.length;i<len;i++){
        if(localArr[i]==data.refname){
            flag=true
        }
    }
    if(!flag){
        localArr.push(data.refname)
    }
    searchData.explanation=localArr.join(',')
    explanation.refname = localArr.join(',');
    this.setState(
        {
            explanation,
            searchData
        }
    )
  }
    render () {
        let {pk_srcbook,pk_desbook,beginperiod,endperiod,searchData,pk_vouchertype,
            accountcode,oppositesubj,tableData,
            explanationModalShow,FinanceOrg,explanation,pk_system,pk_accountingbook}=this.state;
        let columns = this.renderColumnsMultiSelect(this.columns);
        let accUrl = 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js';
        let myAccbook;
        if(!this.state['myattrcode']){
			{createScript.call(this,accUrl,'myattrcode')}
		}else{
			myAccbook =  (
				<Row>
					<Col xs={12} md={12}>
					{this.state['myattrcode']?(this.state['myattrcode'])(
						{
                            fieldid:"oppositesubj",
                            value:oppositesubj,
                            isMultiSelectedEnabled:true,
                            placeholder:this.state.json['20020CONRL-000176'],/* 国际化处理： 核算账簿*/
                            queryCondition:{
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',"pk_accountingbook":pk_accountingbook,'dateStr':this.businessInfo
                            },
							onChange: (v)=>{
								oppositesubj=[];
                                    let obj = {},localArr=[];
                                    for(let i=0,len=v.length;i<len;i++){
                                        obj = {
                                            refname:v[i].refcode,
                                            refpk:v[i].refpk,
                                        }
                                        oppositesubj.push(Object.assign({},obj))
                                        localArr.push(v[i].refcode);
                                    }
                                    searchData.oppositesubj=localArr.join(',')
                                    this.setState({
                                        oppositesubj,
                                        searchData
                                    })
							  }
						}
					):<div/>}
					</Col>
				</Row>
			);	
        }
        return (
            <div>
              
              <NCModal 
              fieldid="query"
              show = {this.state.showModal}
              onHide={this.close}
              className="dnd-cancel senior searchModalCash"
              animation={true}
              >
                <NCModal.Header closeButton fieldid="header-area">
                  <NCModal.Title fieldid={`${this.state.json['20020CONRL-000143']}_title`} >{this.state.json['20020CONRL-000143']}</NCModal.Title>
                </NCModal.Header>

                <NCModal.Body>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}>
                <div className="nc-theme-form-label-c">
                    <Row>
                        <Col lg={3} sm={3} xs={3}>
                            <span style={{'color':'#E14C46'}}>*</span>{this.state.json['20020CONRL-000134']}
                        </Col>
                        <Col lg={4} sm={4} xs={4}>
                            <AccountBookTreeRef
                                fieldid="pk_srcbook"
                                value={{'refname': pk_srcbook.refname, refpk: pk_srcbook.refpk}}
                                disabledDataShow={true}
                                isMultiSelectedEnabled={false}
                                queryCondition={{
                                    "appcode": this.props.getSearchParam('c'),
                                    TreeRefActionExt:'nccloud.web.gl.ref.AccountBookRefSqlBuilder'
                                }}
                                attrcode={'attrcode1'}
                                onChange= {(v)=>{
                                    pk_srcbook.refpk=v.refpk;
                                    pk_srcbook.refname=v.refname;
                                    searchData.pk_srcbook = v.refpk;
                                    pk_desbook.refpk='';
                                    pk_desbook.refname='';
                                    searchData.pk_desbook = '';
                                    this.setState({
                                        pk_srcbook,
                                        searchData,
                                        pk_desbook
                                    })
                                    this.changeBook('',v.refpk);
                                  }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={3} sm={3} xs={3} >
                            <span style={{'color':'#E14C46'}}>*</span>{this.state.json['20020CONRL-000144']}
                        </Col>
                        <Col lg={4} sm={4} xs={4} >
                            <AccountBookTreeRef
                                fieldid="pk_desbook"
                                value={{'refname': pk_desbook.refname, refpk: pk_desbook.refpk}}
                                attrcode={'attrcode2'}
                                isMultiSelectedEnabled={false}
                                disabledDataShow={true}
                                queryCondition={() => {
                                    return {
                                        "appcode": this.props.getSearchParam('c'),
                                        TreeRefActionExt:'nccloud.web.gl.fc.action.FCDesBookRefSqlBuilder,nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                        // nccloud.web.gl.fc.action.FCDesBookRefSqlBuilder
                                        pk_srcbook:pk_srcbook.refpk
                                    }
                                }}
                                onChange={(v)=>{
                                    pk_desbook.refpk=v.refpk;
                                    pk_desbook.refname=v.refname;
                                    searchData.pk_desbook = v.refpk;
                                    this.setState({
                                        pk_desbook,
                                        searchData
                                    })
                                    this.changeBook('','',pk_desbook.refpk);
                                  }
                                }
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={3} sm={3} xs={3}>
                            {this.state.json['20020CONRL-000068']}
                        </Col>
                        <Col lg={4} sm={4} xs={4}>
                            <VoucherTypeDefaultGridRef 
                                fieldid="pk_vouchertype"
                                placeholder={this.state.json['20020CONRL-000068']}/* 国际化处理： 凭证类别*/
                                value={{refcode:pk_vouchertype.refcode,refname:pk_vouchertype.refname,
                                    refpk:pk_vouchertype.refpk}}
                                queryCondition={() => {
                                    return {
                                        GridRefActionExt:'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
                                        pk_org:pk_accountingbook,
                                        isDataPowerEnable: 'Y',
                                        DataPowerOperationCode: 'fi',
                                    }
                                }}
                                onChange= {(v)=>{
                                    pk_vouchertype.refcode=v.refcode;
                                    pk_vouchertype.refname=v.refname;
                                    pk_vouchertype.refpk=v.refpk;
                                    searchData.pk_vouchertype = v.refpk;
                                    this.setState({
                                        pk_vouchertype,
                                        searchData
                                    })
                                  }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={3} sm={3} xs={3}>
                            {this.state.json['20020CONRL-000125']}
                        </Col>
                        <Col lg={3} sm={3} xs={3} className='pkvoucher-num'>
                            <div className="explanation">
                                <NCFormControl 
                                    fieldid="beginNo"
                                    name='explanation' 
                                    type='text' value={searchData.beginNo}
                                    autocomplete="off"
                                    onChange={(value)=>{
                                        let lastItem = value.slice(value.length-1)
                                        let testNum = /[0-9]/
                                        if(value==''){
                                            searchData.beginNo=value;
                                        }else if((!testNum.test(lastItem)&&lastItem!=='')||!Number(value)){
                                            searchData.beginNo=searchData.beginNo;
                                        }else{
                                            searchData.beginNo=value;
                                        }
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </div>
                        </Col>
                        <Col lg={2} sm={2} xs={2} style={{'text-align':'center'}}>
                                --
                        </Col>
                        <Col lg={3} sm={3} xs={3} className='pkvoucher-num'>
                            <div className="explanation">
                                <NCFormControl 
                                    fieldid="beginNo"
                                    name='explanation'
                                    type='text' value={searchData.endNo}
                                    autocomplete="off"
                                    onChange={(value)=>{
                                        let lastItem = value.slice(value.length-1)
                                        let testNum = /[0-9]/
                                        if(value==''){
                                            searchData.endNo=value;
                                        }else if((!testNum.test(lastItem)&&lastItem!=='')||!Number(value)){
                                            searchData.endNo=searchData.endNo;
                                        }else{
                                            searchData.endNo=value;
                                        }
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={3} sm={3} xs={3}>
                                {this.state.json['20020CONRL-000178']}
                        </Col>
                        <Col lg={8} sm={8} xs={8}>
                            <NCRadio.NCRadioGroup
                                fieldid="fcstate"
                                name="12"
                                selectedValue={searchData.showvoucher}
                                onChange={(v)=>{
                                    if(searchData.pk_desbook){
                                        searchData.showvoucher=v;
                                        this.changeBook(v)
                                        if(v==0){
                                            searchData.fcstate='0'
                                        }else{
                                            searchData.fcstate='2'
                                        }
                                        this.setState({
                                            searchData
                                        })
                                    }else{
                                        toast({color:"warning",content:this.state.json['20020CONRL-000175']})/* 国际化处理： 请选择核算账簿*/
                                    }
                                }}>
                                <NCRadio  value="0">{this.state.json['20020CONRL-000179']}</NCRadio>
                                <NCRadio  value="1">{this.state.json['20020CONRL-000180']}</NCRadio>
                            </NCRadio.NCRadioGroup>
                        </Col>
                        
                    </Row>
                    <div className='dateContent'>
                        <div className='preriodS'>
                            <ReferLoader
                                fieldid="beginyear"
                                tag='AccPeriodDefaultTreeGridRef'
                                refcode='uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index.js' 
                                value = {{refpk:beginperiod.refpk,refname:beginperiod.refname}}
                                queryCondition={() => {
                                    return {
                                        GridRefActionExt:'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder',
                                        pk_accperiodscheme:beginperiod.pk_accperiodscheme
                                    }
                                }}
                                onChange={(v)=>{
                                    if(v.refname){
                                        searchData.beginyear=v.values.periodyear.value;
                                        searchData.beginperiod=v.values.accperiodmth.value;
                                        beginperiod.refname=v.refname;
                                        beginperiod.refpk=v.refpk;
                                        searchData.begindate=v.values.begindate.value;
                                    }else{
                                        searchData.beginyear='';
                                        searchData.beginperiod='';
                                        beginperiod.refname='';
                                        beginperiod.refpk='';
                                    }
                                    
                                    this.setState({
                                        beginperiod,
                                        searchData
                                    })
                                }}
                            />
                        </div>
                        <div className='preriodE'>
                            <AccperiodMonthTreeGridRef
                                fieldid="endyear"
                                value = {{refpk:endperiod.refpk,refname:endperiod.refname}}
                                queryCondition={() => {
                                    return {
                                        pk_accperiodscheme:beginperiod.pk_accperiodscheme
                                    }
                                }}
                                onChange={(v)=>{
                                    if(v.refname){
                                        searchData.endyear=v.values.periodyear.value;
                                        searchData.endperiod=v.values.accperiodmth.value;
                                        endperiod.refname=v.refname;
                                        endperiod.refpk=v.refpk;
                                        searchData.enddate=v.values.enddate.value;
                                        if(!v.values.enddate.value){
                                            let url='/nccloud/gl/glpub/queryDateByPeriod.do';
                                            let self = this;
                                            ajax({
                                                url,
                                                data:{
                                                    pk_accountingbook:pk_accountingbook,
                                                    period:v.refname
                                                },
                                                success: function(response) { 
                                                    const { data, error, success } = response;
                                                    if (success&&data) {
                                                        searchData.enddate=data.enddate;
                                                        self.setState({
                                                            endperiod,
                                                            searchData
                                                        });
                                                    } else {
                                                        
                                                    }    
                                                }
                                            });
                                        }
                                    }else{
                                        searchData.endyear='';
                                        searchData.endperiod='';
                                        endperiod.refname='';
                                        endperiod.refpk='';
                                    }
                                    this.setState({
                                        endperiod,
                                        searchData
                                    })
                                }}
                            />
                        </div>
                        <NCRadio.NCRadioGroup
                            fieldid="querybyperiod"
                            name="fruit"
                            selectedValue={searchData.querybyperiod}
                            style={{'width':'100%'}}
                            onChange={(v)=>{
                                searchData.querybyperiod=v;
                                this.setState({
                                    searchData
                                })
                            }}>

                            <NCRadio value="Y" className='displayB'>
                                <Row>
                                    <Col lg={2} sm={2} xs={2}>
                                        {this.state.json['20020CONRL-000181']}
                                    </Col>
                                    <Col lg={4} sm={4} xs={4}>
                                        {/* <AccperiodMonthTreeGridRef
                                            value = {{refpk:beginperiod.refpk,refname:beginperiod.refname}}
                                            queryCondition={() => {
                                                return {
                                                    GridRefActionExt:'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder',
                                                    pk_accperiodscheme:beginperiod.pk_accperiodscheme
                                                }
                                            }}
                                            onChange={(v)=>{
                                                if(v.refname){
                                                    searchData.beginyear=v.values.periodyear.value;
                                                    searchData.beginperiod=v.values.accperiodmth.value;
                                                    beginperiod.refname=v.refname;
                                                    beginperiod.refpk=v.refpk;
                                                    searchData.begindate=v.values.begindate.value;
                                                }else{
                                                    searchData.beginyear='';
                                                    searchData.beginperiod='';
                                                    beginperiod.refname='';
                                                    beginperiod.refpk='';
                                                }
                                                
                                                this.setState({
                                                    beginperiod,
                                                    searchData
                                                })
                                            }}
                                        /> */}
                                        
                                    </Col>
                                    <Col lg={4} sm={4} xs={4}>
                                        
                                    </Col>
                                </Row>
                            </NCRadio>
                            <NCRadio value="N" className='displayB'>{this.state.json['20020CONRL-000124']}</NCRadio>
                        </NCRadio.NCRadioGroup>
                        <Row className='search-data-ob'>
                            <Col lg={4} sm={4} xs={4}>
                                <NCDatePicker
                                    fieldid="begindate"
                                    value={searchData.begindate}
                                    onChange={(v)=>{
                                        searchData.begindate=v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </Col>
                            
                            <Col lg={4} sm={4} xs={4}>
                                <NCDatePicker
                                    fieldid="enddate"
                                    value={searchData.enddate}
                                    onChange={(v)=>{
                                        searchData.enddate=v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </Col>
                        </Row>
                    </div>
                    <Row>
                        <Col md={3} xs={3} sm={3}>
                            {this.state.json['20020CONRL-000062']}：
                        </Col>
                        <Col md={4} xs={4} sm={4}>
                            <div className="explanation">
                                <NCFormControl 
                                    fieldid="explanation"
                                    name='explanation' 
                                    type='text' 
                                    value={explanation.refname}
                                    autocomplete="off"
                                    onChange={(value)=>{
                                        searchData.explanation=value;
                                        explanation.refname=value;
                                        this.setState({
                                            searchData,
                                            explanation
                                        })
                                    }}
                                />
                                <span class="uf uf-3dot-v" style={{cursor: 'pointer'}}
                                    onClick={this.handleShowExplanation.bind(this)}
                                ></span>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3} xs={3} sm={3}>
                            {this.state.json['20020CONRL-000092']}：
                        </Col>
                        <Col md={4} xs={4} sm={4}>
                            <ReferLoader
                                fieldid="accountcode"
                                tag='AccountDefaultGridTreeRefCode'
                                refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js' 
                                placeholder={this.state.json['20020CONRL-000092']}/* 国际化处理： 科目编码*/
                                value={accountcode}
                                isMultiSelectedEnabled={true}
                                // onlyLeafCanSelect={true}
                                queryCondition={{"pk_accountingbook":pk_accountingbook,
                                'dateStr':this.businessInfo,isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',}} 
                                onChange= {(v)=>{
                                    accountcode=[];
                                    let obj = {},localArr=[];
                                    for(let i=0,len=v.length;i<len;i++){
                                        obj = {
                                            refname:v[i].refcode,
                                            refpk:v[i].refpk,
                                        }
                                        accountcode.push(Object.assign({},obj))
                                        localArr.push(v[i].refcode);
                                    }
                                    searchData.accountcode=localArr.join(',')
                                    this.setState({
                                        accountcode,
                                        searchData
                                    })
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3} xs={3} sm={3}>
                            {this.state.json['20020CONRL-000176']}：
                        </Col>
                        <Col md={4} xs={4} sm={4}>
                            {
                                myAccbook
                            }
                            <ReferLoader
                                fieldid="oppositesubj"
                                tag='AccountDefaultGridTreeRefCode'
                                refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js' 
                                style={{'display':'none'}}
                                placeholder={this.state.json['20020CONRL-000176']}/* 国际化处理： 对方科目*/
                                value={oppositesubj}
                                isMultiSelectedEnabled={true}
                                // onlyLeafCanSelect={true}
                                queryCondition={{isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',"pk_accountingbook":pk_accountingbook,'dateStr':this.businessInfo}} 
                                onChange= {(v)=>{
                                    oppositesubj=[];
                                    let obj = {},localArr=[];
                                    for(let i=0,len=v.length;i<len;i++){
                                        obj = {
                                            refname:v[i].refcode,
                                            refpk:v[i].refpk,
                                        }
                                        oppositesubj.push(Object.assign({},obj))
                                        localArr.push(v[i].refcode);
                                    }
                                    searchData.oppositesubj=localArr.join(',')
                                    this.setState({
                                        oppositesubj,
                                        searchData
                                    })
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col style={{width:'16%',display:'inline-block'}}>
                            <NCSelect
                                showClear={false}
                                fieldid="direction"
                                defaultValue={searchData.direction}
                                style={{ width: '100%', marginRight: 2 }}
                                onChange={(v)=>{
                                    searchData.direction=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            >
                                <NCOption value="0">{this.state.json['20020CONRL-000182']}</NCOption>
                                <NCOption value="1">{this.state.json['20020CONRL-000127']}</NCOption>
                                <NCOption value="2">{this.state.json['20020CONRL-000128']}</NCOption>
                            </NCSelect>
                        </Col>
                        <Col style={{width:'11%',display:'inline-block'}}>
                            <span>{this.state.json['20020CONRL-000183']}&gt;=</span>
                        </Col>
                        <Col style={{width:'25%',display:'inline-block',marginRight:4}}>
                            <NCNumber
                                fieldid="firstAmount"
                                scale={4}
                                value={searchData.firstAmount}
                                onChange={(v) => {
                                    searchData.firstAmount=v
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                            {/* <NCNumber
                                scale={4}
                                // placeholder="请输入数量"
                                value={searchData.secondAmount}
                                onChange={(v) => {
                                    searchData.secondAmount=v
                                    this.setState({
                                        searchData
                                    })
                                }}
                            /> */}
                        </Col>
                        <Col style={{width:'16%',display:'inline-block'}}>
                            <NCSelect
                                showClear={false}
                                fieldid="andOr"
                                defaultValue={searchData.andOr}
                                style={{ width: '100%', marginRight: 2 }}
                                onChange={(v)=>{
                                    searchData.andOr=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            >
                                <NCOption value="and">{this.state.json['20020CONRL-000184']}</NCOption>
                                <NCOption value="or">{this.state.json['20020CONRL-000185']}</NCOption>
                            </NCSelect>
                        </Col>
                        <Col style={{width:'5%',display:'inline-block'}}>
                            <span>&lt;=</span>
                        </Col>
                        <Col style={{width:'23%',display:'inline-block'}}>
                            <NCNumber
                                fieldid="secondAmount"
                                scale={4}
                                // placeholder="请输入数量"
                                value={searchData.secondAmount}
                                onChange={(v) => {
                                    searchData.secondAmount=v
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3} xs={3} sm={3}>
                            {this.state.json['20020CONRL-000177']}
                        </Col>
                        <Col md={4} xs={4} sm={4}>
                            <CtlSysRef
                                fieldid="pk_system"
                                placeholder={this.state.json['20020CONRL-000177']}/* 国际化处理： 来源系统*/
                                value={pk_system}
                                queryCondition={{"pk_accountingbook":pk_srcbook.refpk,isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',}} 
                                onChange= {(v)=>{
                                    pk_system=v;
                                    searchData.pk_system=v.refcode;
                                    this.setState({
                                        pk_system,
                                        searchData
                                    })
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3} xs={3} sm={3}>
                            {this.state.json['20020CONRL-000130']}
                        </Col>
                        <Col md={4} xs={4} sm={4}>
                            <NCSelect
                                showClear={false}
                                fieldid="isdifflag"
                                defaultValue={searchData.isdifflag}
                                // style={{ width: 200, marginRight: 6 }}
                                disabled = {searchData.showvoucher=='1'}
                                onChange={(v)=>{
                                    searchData.isdifflag=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            >
                                <NCOption value="0">{this.state.json['20020CONRL-000186']}</NCOption>
                                <NCOption value="1">{this.state.json['20020CONRL-000157']}</NCOption>
                                <NCOption value="2">{this.state.json['20020CONRL-000187']}</NCOption>
                            </NCSelect>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3} xs={3} sm={3}>
                            {this.state.json['20020CONRL-000188']}
                        </Col>
                        <Col md={4} xs={4} sm={4}>
                            <NCSelect
                                showClear={false}
                                fieldid="fcstate"
                                value={searchData.fcstate}
                                onChange={(v)=>{
                                    searchData.fcstate=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            >   
                                <NCOption value="0" className={searchData.showvoucher=='1'?'display-none':''} >{this.state.json['20020CONRL-000189']}</NCOption>
                                <NCOption value="1" className={searchData.showvoucher=='1'?'display-none':''} >{this.state.json['20020CONRL-000190']}</NCOption>
                                <NCOption value="2" className={searchData.showvoucher=='0'?'display-none':''}>{this.state.json['20020CONRL-000191']}</NCOption>
                            </NCSelect>
                        </Col>
                    </Row>
                    <Row>
                        <NCDiv fieldid="search" areaCode={NCDiv.config.TableCom}>
                            <Table
                                columns={columns}
                                data={tableData}
                                className='search-table-obversion'
                                onRowClick={this.clickRow}
                                scroll={{ x:false, y: 150 }}
                            /> 
                        </NCDiv>
                    </Row>
                    </div>
                </NCDiv>  
                </NCModal.Body>

                <NCModal.Footer fieldid="bottom_area">
                  <NCButton onClick={this.handleConfirm} colors="primary" fieldid="confirm">{this.state.json['20020CONRL-000059']}</NCButton> 
                  <NCButton onClick={this.close} fieldid="close">{this.state.json['20020CONRL-000006']}</NCButton>
                </NCModal.Footer>
                                    
              </NCModal>
              <ExplanationModal 
                explanationModalShow={explanationModalShow}
                closeExplanationModal={this.closeExplanationModal.bind(this)}
                FinanceOrg={FinanceOrg}
                getAppointData={this.getAppointData.bind(this)}
              />
            </div>
        )
    }
}
SearchModal = createPage({})(SearchModal);
export default SearchModal;
SearchModal.defaultProps = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
};
