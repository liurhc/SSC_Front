import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,getMultiLang } from 'nc-lightapp-front';
// import createScript from '../../../public/components/uapRefer.js';
const { NCModal,NCButton,NCRadio,NCTable,NCDiv } = base;
const { Refer } = high;
import './index.less';
// import CashflowTreeRef from '../../../../uapbd/refer/fiacc/CashflowTreeRef';
export default class AppointModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            selectData:null,
            FinanceOrg:'',
            tableData:[],
            columns:[]
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json,
            columns : [
                {
                    title: "",
                    dataIndex: "index",
                    key: "index",
                    width:'20%',
                    render: (text, record, index) => (
                      <span fieldid="index">{index}</span>
                    )
                },
                {
                title: (<span fieldid="pk_debitaccasoa">{json['20020CONRL-000015']}</span>),/* 国际化处理： 编码*/
                  dataIndex: "pk_debitaccasoa",
                  key: "pk_debitaccasoa",
                  width:'30%',
                  render: (text, record, index) => (
                    <span fieldid="pk_debitaccasoa">{record.refcode}</span>
                  )
                },
                {
                  title: (<span fieldid="pk_creditaccasoa">{json['20020CONRL-000061']}</span>),/* 国际化处理： 名称*/
                  dataIndex: "pk_creditaccasoa",
                  key: "pk_creditaccasoa",
                  width:'30%',
                  render: (text, record, index) => (
                    <span fieldid="pk_creditaccasoa">{record.refname}</span>
                  )
                },
                {
                  title: "",
                  dataIndex: "pk_creditaccasoa",
                  key: "pk_creditaccasoa",
                  width:'20%',
                  render: (text, record, index) => (
                    <span className={this.state.tableData[index].isSelected?'':'correctHide'} fieldid="pk_creditaccasoahide">
                      <i className="uf uf-correct"></i>
                    </span>
                  )
                }
              ]    
        },()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.explanationModalShow;
        let url = '/nccloud/fipub/ref/SummaryRef.do';
        let FinanceOrg = this.state.FinanceOrg;
        if(FinanceOrg!=nextProps.explanationModalShow){
            this.setState({
                FinanceOrg:nextProps.FinanceOrg
            })
            let dataCount = 
			{
                    pid: "",
                    keyword: "",
                    queryCondition: {
	                    pk_org: nextProps.FinanceOrg
                    },
                    pageInfo: {
                         pageSize: 10,
                         pageIndex: -1
                }
            }
			ajax({
				url: '/nccloud/fipub/ref/SummaryRef.do',
				data: dataCount,
				success: (res) => {
                    let { rows }=res.data
                    let tableData=rows;
                    for(let i=0,len=tableData.length;i<len;i++){
                        tableData[i].key=i
                        tableData[i].isSelected=false;
                    }
					this.setState({
                        tableData
                    })
				}
			});
        }
		
        this.setState({
            showModal
        })
    }
    close() {
        this.props.closeExplanationModal(false);
    }
    clickRow=(data,rowClassName)=>{
        let tableData = this.state.tableData;
        for(let i=0,len=tableData.length;i<len;i++){
          tableData[i].isSelected=false;
        }
        tableData[rowClassName].isSelected=true;
        this.setState({
          selectData:data,
          tableData
        })
        // this.props.getBusrelation(data);
    }
    handleConfirm=()=>{
        let selectData = this.state.selectData;
        this.props.getAppointData(selectData);
        this.props.closeExplanationModal(false);
    }

    render () {
        let {tableData,selectData}=this.state;
        let columns = this.state.columns;
        return (
            <div>
                <NCModal
                    fieldid="query"
                    show={this.state.showModal}
                    className='explanModal'
                    onHide={this.close}
                    backdrop={false}
                >
                    <NCModal.Header closeButton fieldid="header-area">
                        <NCModal.Title>{this.state.json['20020CONRL-000062']}</NCModal.Title>
                    </NCModal.Header>

                    <NCModal.Body >
                        <NCDiv fieldid="explanation" areaCode={NCDiv.config.TableCom}>
                            <NCTable
                                columns={columns}
                                data={tableData}
                                onRowClick={this.clickRow}
                                scroll={{ x: false, y: 200 }}
                                style={{ 'height': '200px' }}
                            />
                        </NCDiv>
                    </NCModal.Body>

                    <NCModal.Footer fieldid="bottom_area">
                        <NCButton onClick={this.close} fieldid="close">{this.state.json['20020CONRL-000136']}</NCButton>
                        <NCButton onClick={this.handleConfirm} color="primary" fieldid="confirm">{this.state.json['20020CONRL-000103']}</NCButton>
                    </NCModal.Footer>

                </NCModal>
            </div>
        )
    }
}

