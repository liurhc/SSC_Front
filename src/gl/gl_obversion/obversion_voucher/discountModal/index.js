import React, { Component } from 'react';
import {high,base,ajax,createPage,cardCache,getMultiLang } from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
const { 
    NCFormControl: FormControl,NCButton: Button,
    NCRadio,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,
	NCCheckbox:Checkbox,NCModal:Modal,NCTooltip:Tooltip,NCNumber,NCPopover:Popover,NCDiv
    } = base;
const { Refer } = high;
const NCOption = NCSelect.NCOption;
import './index.less';
class DiscountModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            currtype:'0',
            selectData:[],
            pk_soblink:null,
            saveData:{
                pk_srcvoucher:[],
                pk_soblink:'',
                pk_convertrule:'',
                realtimerate:''
            },
            searchData:{
                pk_desbook:'',
                pk_srcbook:'',
            }
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CONRL',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.discountShow;
        let searchData = nextProps.searchData;
        let pk_soblink = nextProps.pk_soblink;
        let selectData  = nextProps.selectDataRules;
        this.setState({
            showModal,
            searchData,
            pk_soblink,
            selectData
        })
    }
    close() {
        this.props.getDisShow(false);
    }
    handleConfirm=()=>{//确认
        let data = this.state.saveData;
        data.pk_soblink=this.state.pk_soblink;
        // data.pk_convertrule=saveData.pk_convertrule;
        let selectData=this.props.selectData;
        let arrSel=[];
        selectData.map((item)=>{
            arrSel.push(item.pk_srcvoucher)
        })
        data.pk_srcvoucher=arrSel;
        let url = "/nccloud/gl/voucher/convert.do";
        let self = this;
        ajax({
            url,
            data,
            success: function(response) {
                const {data} = response;
                setDefData(123, 'gl_obervision.discount.logData', data);
                self.props.getDisShow(false);
                self.props.toLogHand();
            }
		});
    }
    render () {
        let {currtype,selectData,saveData} = this.state;
        return (
        <div>
            <Modal
            fieldid="query"
            show = { this.state.showModal }
            // size = 'lg'
            className='obversion-small-modal-a'
            onHide = { this.close } >
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20020CONRL-000137']}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}  className="nc-theme-form-label-c">
                    <Row>
                        <Col lg={2} sm={2} xs={2}>
                            {this.state.json['20020CONRL-000137']}
                        </Col>
                        <Col lg={10} sm={10} xs={10}>
                            <NCRadio.NCRadioGroup
                                fieldid="currtype"
                                name="fruit"
                                selectedValue={currtype}
                                onChange={(v)=>{
                                    let currtype = v;
                                    this.setState({
                                        currtype
                                    })
                                    if(v=='1'){
                                        saveData.pk_convertrule=this.props.selectDataRulesdefault
                                    }else{
                                        saveData.pk_convertrule=''
                                    }
                                    this.setState({
                                        saveData
                                    })
                                }}
                                >
                                <NCRadio value="0" >{this.state.json['20020CONRL-000138']}</NCRadio>
                                <NCRadio value="1" >{this.state.json['20020CONRL-000139']}</NCRadio>
                            </NCRadio.NCRadioGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={2} sm={2} xs={2}>
                            <span>{this.state.json['20020CONRL-000137']}</span>
                        </Col>
                        <Col lg={10} sm={10} xs={10}>
                            <NCSelect
                                fieldid="pk_convertrule"
                                // style={{ width: 150, marginRight: 6 }}
                                defaultValue={this.props.selectDataRulesdefault}
                                onChange={(v)=>{
                                    saveData.pk_convertrule=v
                                    this.setState({
                                        saveData
                                    })
                                }}
                            >
                                {
                                    selectData.map((item)=>{
                                        return (
                                            <NCOption value={item.value}>{item.display}</NCOption>
                                        )
                                    })
                                }
                            </NCSelect>
                            
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={2} sm={2} xs={2}>
                            <div className='searchTipsRate'>{this.state.json['20020CONRL-000098']}
                                <div className='searchTips nc-theme-area-bgc nc-theme-common-font-c nc-theme-area-split-bc'>{this.state.json['20020CONRL-000135']}</div>
                            </div>
                        </Col>
                        <Col lg={10} sm={10} xs={10}>
                            <NCNumber
                                fieldid="realtimerate"
                                scale={this.state.searchData.realtimerate}
                                value={saveData.realtimerate}
                                onChange={(v) => {
                                    saveData.realtimerate=v
                                    this.setState({
                                        saveData
                                    })
                                }}
                            />
                        </Col>
                    </Row>
                    </NCDiv>
                </Modal.Body>

                <Modal.Footer>
                    <Button onClick={ this.handleConfirm } colors="primary">{this.state.json['20020CONRL-000005']}</Button>
                    <Button onClick={ this.close } shape="border" style={{marginRight: 6}}>{this.state.json['20020CONRL-000006']}</Button>
                    
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
DiscountModal = createPage({
	// initTemplate: initTemplate
})(DiscountModal);
export default DiscountModal;
