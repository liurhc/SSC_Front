import React, { Component } from 'react';
import {high,base,ajax,createPage,cacheTools,toast, getMultiLang,gzip ,cardCache,createPageIcon} from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCDiv
} = base;
const dataSource = 'gl.multiformat.';
const searchKey = 'MultisearchKey';
const tupStepdataSource = 'gl.multiformat.tupStep';
import HeaderArea from '../../../public/components/HeaderArea';



import './index.less'
import { conf } from '../../../../uapbd/refer/org/BusinessUnitTreeRef/index';
import {searchById} from "../../../manageReport/common/modules/createPureBtn";
import '../../../public/reportcss/firstpage.less'
var setDefData = cardCache.setDefData
var getDefData = cardCache.getDefData
class Reckon extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            radioValue: '',
            tableData: [
                { key: 1 },
                { key: 2 },
                { key: 3 }
            ],
            saveData: {
                debit: 'Y',
                balance: 'Y',
                credit: 'Y',
                format: '1'
            },
            jinDataTable: []
        }
        this.sendData = this.sendData.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.getData = this.getData.bind(this);
        this.debitChange = this.debitChange.bind(this);
        this.balanceChange = this.balanceChange.bind(this);
        this.creditChange = this.creditChange.bind(this);
        this.saveDataEnd = this.saveDataEnd.bind(this);
        this.cancelEnd = this.cancelEnd.bind(this);
        this.searchById = searchById.bind(this);
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'2002305005',domainName:'gl',currentLocale:'simpchn',callback});
        this.getData();
        this.searchById('2002305005PAGE','2002305005');
        this.getData()
    }

    getData(){
        let data = cacheTools.get('allData');
        //('getData>>>', data)
        //(data);
        data.debit = this.state.saveData.debit
        data.credit = this.state.saveData.credit
        data.balance = this.state.saveData.balance
        data.format = this.state.saveData.format;
        this.state.saveData.refpath = data.refpath;
        this.state.saveData = data;
        //('getData>>>', data)
        this.setState({
            saveData: this.state.saveData
        },()=>{
            this.sendData()
        })
    }
    sendData() {
        let {saveData} = this.state
        ajax({
            url:'/nccloud/gl/accountrep/multibookformatpreview.do',
            data: saveData,
            success: (response) => {
                // let { data, success} = response
                let columns = [...response.data]
                this.setState({
                    jinDataTable: columns,
                    tableData: this.state.tableData
                })
            }
            // this.state.jinDataTable,
            // this.state.tableData
        });
    }
    handleChange(value) {
        this.state.saveData.format = value;
        this.setState({saveData: this.state.saveData},()=>{
            this.sendData()
        });
    }
    debitChange(val) {
        this.state.saveData.debit = val ? 'Y':'N';
        this.setState({
            saveData: this.state.saveData
        },()=>{
            this.sendData()
        })
    }
    creditChange(val) {
        this.state.saveData.credit = val ? 'Y':'N';
        this.setState({
            saveData: this.state.saveData
        },()=>{
            this.sendData()
        })
    }
    balanceChange(val) {
        this.state.saveData.balance = val ? 'Y':'N';
        this.setState({
            saveData: this.state.saveData
        },()=>{
            this.sendData()
        })
    }
    saveDataEnd(){
        this.state.saveData.pk_accountingbook = this.props.getUrlParam('refpk');
        //(this.props.getUrlParam('pk_multicol'));
        if(this.props.getUrlParam('pk_multicol') != 'undefined') {
            this.state.saveData.pk_multicol = this.props.getUrlParam('pk_multicol');
        }
        this.setState({
            saveData: this.state.saveData
        },()=>{
            //('queding :::',this.state.saveData, this.props.getUrlParam('oldRef'));
            let url ='/nccloud/gl/accountrep/multibookformatsave.do';
            ajax({
                url,
                data: this.state.saveData,
                success: (response) => {
                    toast({color: 'success'});
                    setTimeout(() => {
                        this.props.pushTo('/',{
                            oldRef: this.props.getUrlParam('oldRef')
                        });
                    }, 1000)
                }
            });
        })
    }
    cancelEnd(){
        //('cancelEnd');
    }
    handleLoginBtn = (obj, btnName) => {
        let gziptools = new gzip();
        if(btnName === 'sure'){//1、确定
            this.saveDataEnd()
        }else if(btnName === 'cancle'){//2、取消
            this.props.pushTo('/',{
                refpk: this.props.getUrlParam('refpk'),
                oldRef: this.props.getUrlParam('oldRef')
            })
        }else if(btnName === 'tupStep'){//2、上一步
            setDefData(searchKey, tupStepdataSource, this.state.saveData)
            this.props.pushTo('/list',{
               // data: gziptools.zip(JSON.stringify(this.state.saveData )),
                refpk: this.props.getUrlParam('refpk'),
                pk_multicol : this.props.getUrlParam('pk_multicol'),
                upD: true,
                selectRef: this.props.getUrlParam('oldRef'),
                pk_org: this.props.getUrlParam('pk_org'),
               // selectedData: this.props.getUrlParam('selectedData')
            })
        }
    }
    render() {
        //('jinDataTablejinDataTable::', this.state.jinDataTable)
        let { jinDataTable, tableData } = this.state
        return (
            <div className="nc-single-table reckoning">
                <HeaderArea 
                    title = {this.state.json['2002305005-000000']} /* 国际化处理： 多栏账定义*/
                    btnContent = {this.props.button.createButtonApp({
                        area: 'threepage',
                        buttonLimit: 3,
                        onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName)
                    })}
                />
                <NCDiv fieldid="multiformat" areaCode={NCDiv.config.FORM} className="serrch-area-box nc-theme-gray-area-bgc nc-theme-area-split-bc">
                    <div className='m-dlzdyej-box'>
                        <Checkbox
                            checked={this.state.saveData.debit == 'Y'}
                            onChange={(val)=> this.debitChange(val)}
                        >
                            {this.state.json['2002305005-000001']}
                        </Checkbox>{/* 国际化处理： 借方*/}
                        <Checkbox
                            checked={this.state.saveData.credit == 'Y'}
                            onChange={(val)=> this.creditChange(val)}
                        >
                            {this.state.json['2002305005-000002']}
                        </Checkbox>{/* 国际化处理： 贷方*/}
                        <Checkbox
                            checked={this.state.saveData.balance == 'Y'}
                            onChange={(val)=> this.balanceChange(val)}
                        >
                            {this.state.json['2002305005-000003']}
                        </Checkbox>{/* 国际化处理： 余额*/}
                        <div className={"m-shuchutitile nc-theme-common-font-c"}>{this.state.json['2002305005-000004']}</div>{/* 国际化处理： 输出格式*/}
                        <div>
                            <Radio.NCRadioGroup
                                name="fruit"
                                selectedValue = {this.state.saveData.format}
                                onChange = {this.handleChange.bind(this)}
                            >
                                <Radio value="1">{this.state.json['2002305005-000005']}</Radio>{/* 国际化处理： 分析内容前置*/}
                                <Radio value="2">{this.state.json['2002305005-000006']}</Radio>{/* 国际化处理： 分析内容后置*/}
                            </Radio.NCRadioGroup>
                        </div>
                    </div>
                </NCDiv>
                <NCDiv fieldid="secondstep" areaCode={NCDiv.config.TableCom} className='nc-singleTable-table-area nc-theme-area-bgc endTableContainer'>
                    <Table
                        columns={jinDataTable}
                        data={tableData}
                    />
                </NCDiv>

            </div>
        )
    }
}

let initTemplate = (props) => {
}

Reckon = createPage({
    initTemplate: initTemplate
})(Reckon);

//ReactDOM.render(<Reckon />,document.querySelector('#app'));
export default Reckon;