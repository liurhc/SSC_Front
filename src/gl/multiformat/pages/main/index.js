import React, { Component } from 'react';
import {high,base,ajax,createPage, promptBox, cacheTools, getMultiLang,gzip, cardCache,createPageIcon } from 'nc-lightapp-front';
const {
    NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCForm,NCButtonGroup:ButtonGroup,NCAffix,
    NCPopconfirm: Popconfirm, NCDiv
} = base;
const {  NCFormItem:FormItem } = NCForm;

import createScript from '../../../public/components/uapRefer.js';
//import AccperiodMonthTreeGridRef from '../../../../uapbd/refer/org/AccountBookTreeRef';

//import {handleQueryClick,handleVerifyDetails,handleVerifySum,handleRefresh} from './events/index.js';
import './index.less'
import { conf } from '../../../../uapbd/refer/org/BusinessUnitTreeRef/index';
import {
    businessUnit,
    createReferFn,
    getCheckContent,
    getReferDetault
} from "../../../manageReport/common/modules/modules";
import {toast} from "../../../public/components/utils";
import {searchById} from "../../../manageReport/common/modules/createPureBtn";
import '../../../public/reportcss/firstpage.less';
import {handleValueChange} from "../../../manageReport/common/modules/handleValueChange";
var setDefData = cardCache.setDefData
var getDefData = cardCache.getDefData
const dataSource = 'gl.multiformat.';
const searchKey = 'MultisearchKey';
const tupStepdataSource = 'gl.multiformat.tupStep';
const nextdataSource = 'gl.multiformat.next';
const nextsearchKey = 'nextMultisearchKey';
import HeaderArea from '../../../public/components/HeaderArea';
import { getTableHeight } from '../../../public/common/method.js';
const emptyCell = <span>&nbsp;</span>
class Reckon extends Component{
    constructor(props){
        super(props);
        this.state = {
            appcode: "2002305005",
            json: {},
            period_pk:{
                refname: "",
                refpk: "",
                tableData: []
            },
            remeber:{
                refpk:""
            },
            pk_accountingbook:{
                display:'',
                value:''
            },
            loadData:{
                refpk: '',
                pk_multicol: '',
                multicolname: ''
            }
        }
        this.searchById = searchById.bind(this);
        this.handleValueChange = handleValueChange.bind(this);
    }

    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'2002305005',domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        this.searchById('2002305005PAGE', this.state.appcode)
        this.props.button.setDisabled({add: true})
        this.getParam();
        //('componentDidMount>', appceod)
    }

    getDefaultRefer = () => {
        setTimeout(() => getReferDetault(this, false, {
            businessUnit,
            getCheckContent: (self,accountingbook, boolean2) => this.getDataList(self,accountingbook, boolean2)
        }, 'multiformat'),0);
    }

    getParam = () => {
        let gziptools = new gzip();
        let data = this.props.getUrlParam && this.props.getUrlParam('oldRef');
        //('datata:::',data);
        if(data){//如果data存在，那就是返回到首页的
            this.props.button.setDisabled({add: false})
           // let newData = gziptools.unzip(data);
           let newData = JSON.parse(data);
            //('newData::', newData);
            this.handleValueChange('accountingbook', newData)
            businessUnit(this, newData, {
                getCheckContent: () => this.getDataList(this,newData, 'multiformat')
            },'multiformat')
        }else{//否则就是直接进的首页
            this.getDefaultRefer()
        }
    }

    getDataList = (self,value, boolean2) => {
        //('value:::', self,value, boolean2);
        let url ='/nccloud/gl/accountrep/multibookformatquery.do';
        ajax({
            url,
            data:{"pk_accountingbook":value.refpk},
            success: (response) => {
                //(response.data);
                if(!response.data){
                    this.setState({tableData: []});
                    return;
                }
                this.setState({tableData:response.data})
            }
        });
    }
    handleDeleted = (record) => {//删除按钮
        let url = '/nccloud/gl/accountrep/multibookformatdelete.do'
        //(record, record.pk_multicol);
        ajax({
            url,
            data: {
                pk_multicol : record.pk_multicol
            },
            success:(res)=> {
                let urlAdd ='/nccloud/gl/accountrep/multibookformatquery.do';
                ajax({
                    url: urlAdd,
                    data: {"pk_accountingbook": record.refpk},
                    success: (response) => {
                        if(!response.data) response.data = [];
                        this.state.loadData.pk_multicol = '';
                        this.state.loadData.multicolname = '';
                        this.setState({
                            tableData: response.data,
                            loadData: this.state.loadData
                        });
                        this.refreshFn();
                    }
                });
            }
        })
    }

    refreshFn = () => {
        //this.state.accountingbook
        //('accountingbook>>>',this.state.accountingbook);
        if(this.state.accountingbook){
            this.getDataList(this, this.state.accountingbook)
        }
        
        // getReferAppCode(this, 'appcode');
        // setTimeout(() => getReferDetault(this, false, {
        //     businessUnit,
        //     getCheckContent: (self,accountingbook, boolean2) => this.getDataList(self,accountingbook, boolean2)
        // }, 'multiformat'),0)
    }

    newAdd = () => {
        let gziptools = new gzip();
        if( !this.state.accountingbook.refpk ){
            toast({content: this.state.json['2002305005-000037'], color: 'warning'});/* 国际化处理： 请先选择核算账簿具体项*/
            return;
        }
        cacheTools.set('allParam', {
            refpk : this.state.accountingbook.refpk,
            pk_org: this.state.unitValueParam, //this.state.accountingbook.nodeData ? this.state.accountingbook.nodeData.pk_corp : this.state.accountingbook.pk_corp
        });
        //let catcheData = setDefData(searchKey, dataSource, data)
        setDefData(nextsearchKey, nextdataSource, {})
        this.props.pushTo('/list', {
                refpk : this.state.accountingbook.refpk,
                busiDate: this.state.busiDate,
                pk_org: this.state.unitValueParam, //this.state.accountingbook.nodeData ? this.state.accountingbook.nodeData.pk_corp : this.state.accountingbook.pk_corp,
                selectRef: JSON.stringify(this.state.accountingbook)
            }
        );
    }

    modify = (record) => {//修改
        let gziptools = new gzip();
        cacheTools.set('allParam', {
            refpk : this.state.accountingbook.refpk,
            pk_multicol : this.state.loadData.pk_multicol,
            pk_org: this.state.unitValueParam, //this.state.accountingbook.nodeData ? this.state.accountingbook.nodeData.pk_corp : this.state.accountingbook.pk_corp
        });
        this.props.pushTo('/list', {
                refpk : this.state.accountingbook.refpk,
                busiDate: this.state.busiDate,
                pk_multicol : record.pk_multicol,
                pk_org: this.state.unitValueParam,//this.state.accountingbook.nodeData ? this.state.accountingbook.nodeData.pk_corp : this.state.accountingbook.pk_corp,
                selectRef: JSON.stringify(this.state.accountingbook)
            }
        );
    }
    deleted = (record) => {//删除
        promptBox({
            color: 'warning',
            title: this.state.json['2002305005-000038'],/* 国际化处理： 注意*/
            content: `${this.state.json['2002305005-000044']}${record.multicolname}${this.state.json['2002305005-000045']}？`,             // 提示内容,非必输/* 国际化处理： 确定要删除,吗*/
            noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
            noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
            beSureBtnName: this.state.json['2002305005-000039'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
            cancelBtnName: this.state.json['2002305005-000040'],           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
            beSureBtnClick: () => this.handleDeleted(record),   // 确定按钮点击调用函数,非必输
        })
    }

    handleLoginBtn = (obj, btnName) => {
        //('handleLoginBtn>', obj, btnName);
        if(btnName === 'add'){//1、新增
            this.newAdd()
        }else if(btnName === 'edit'){//2、修改
            this.modify()
        }else if(btnName === 'delete'){//3、删除
            this.deleted()
        }else if(btnName === 'refresh'){//4、刷新
            this.refreshFn()
        }
    }

    renderCopyFef(record, fieldid){
        let { period_pk,appcode }=this.state;
        let myBtnBook;
        let { pk_accountingbook,periodData}=this.state;
        let referUrl = 'uapbd/refer/org/AccountBookTreeRef/index.js';
        if(!this.state['myBtnBookcode']){
			{createScript.call(this, referUrl, 'myBtnBookcode')}
		}else{
			myBtnBook =  (
				<Row>
					<Col xs={12} md={12}>
					{this.state['myBtnBookcode']?(this.state['myBtnBookcode'])(
						{
                            fieldid: fieldid,
                            placeholder: this.state.json['2002305005-000041'],/* 国际化处理： 复制到其他核算账簿*/
                            isMultiSelectedEnabled: true,
							// value:{'refname': pk_accountingbook.display, 'refpk': pk_accountingbook.value},
                            queryCondition: {
                                "TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                "appcode": appcode,
                                "isDataPowerEnable": 'Y',
                                "DataPowerOperationCode" : 'fi',
                            },
							onChange:(v)=>{
                                let url ='/nccloud/gl/accountrep/multibookformatcopy.do';
                                //(v);
                                let refpkArr = [];
                                if(v.lenght == 0){
                                    toast({content: this.state.json['2002305005-000042'], color: 'warning'});/* 国际化处理： 请选择核算账簿！！！*/
                                    return;
                                }
                                v.forEach((item)=>{
                                    refpkArr.push(item.refpk);
                                })
                                // if(!this.state.loadData.pk_multicol){
                                //     toast({content: this.state.json['2002305005-000037'], color: 'warning'});/* 国际化处理： 请先选择核算账簿具体项*/
                                //     return;
                                // }
                                ajax({
                                    url,
                                    data:{
                                        "pk_accountingbook": refpkArr,
                                        "pk_multicol": record.pk_multicol
                                    },
                                    success: (response) => {
                                        if (response.data.error) {
                                            toast({ content: response.data.error, color: 'danger' });
                                            return;
                                        }
                                        if (response.data.msg) {
                                            toast({ content: response.data.msg, color: 'success' });
                                        }
                                    }

                                });
                            }
						}
					):<div/>}
					</Col>
				</Row>
			);
        }
        return myBtnBook;
    }
    render(){

        let { period_pk,appcode }=this.state;
        let mybook;
        
        const columns = [
            { 
                title: (<div fieldid='multicolname'>{this.state.json['2002305005-000025']}</div>), 
                dataIndex: 'multicolname', 
                key: 'multicolname', 
                width: '40%',
                render: (text, record, index) => <div fieldid='multicolname'>{text ? text : emptyCell}</div>
            },/* 国际化处理： 名称*/
            { 
                id: '123', 
                title: (<div fieldid='remark'>{this.state.json['2002305005-000013']}</div>), 
                dataIndex: 'remark', 
                key: 'remark', 
                width: '40%',
                render: (text, record, index) => <div fieldid='remark'>{text ? text : emptyCell}</div>
            },/* 国际化处理： 备注*/
            {
                title: (<div fieldid='opr'>{this.state.json['2002305005-000018']}</div>),/* 国际化处理： 操作*/
                dataIndex: 'oparation',
                key: 'oparation',
                render: (text, record) => {
                    //(this.state.json['2002305005-000043'], text, record)/* 国际化处理： 操作：*/
                    return (
                        <div fieldid='opr' className="condition" style={{display: 'flex', alignItems: 'center'}}>
                            <span
                                style={{color:'#007ACE', cursor: 'pointer', lineHeight: '20px'}}
                                onClick={ () => this.modify(record)}
                            >
                                {this.state.json['2002305005-000046']}{/* 国际化处理： 修改*/}
                            </span>
                            <Popconfirm
                                trigger="click"
                                placement="top"
                                content={`${this.state.json['2002305005-000044']}${record.multicolname}${this.state.json['2002305005-000045']}？`}/* 国际化处理： 确定要删除,吗*/
                                onClose={() => this.handleDeleted(record)}
                            >
                                <span fieldid='del_btn' style={{color:'#007ACE', cursor: 'pointer', margin: '20px', lineHeight: '20px'}}>{this.state.json['2002305005-000029']}</span>{/* 国际化处理： 删除*/}
                            </Popconfirm>
                            <div
                                className="copyToAccount"
                                style={{color:'#007ACE', lineHeight: '20px'}}
                                // disabled={this.state.loadData.multicolname ? false : true}
                            > {this.state.json['2002305005-000041']}{/* 国际化处理： 复制到其他核算账簿*/}
                                <div
                                    id='multiCopyId'
                                    className='multiCopy'
                                    style={{
                                        width: '100%',
                                        position: 'relative',
                                        top: '-26px',
                                        left: '-10px',
                                        opacity: '0',
                                        marginLeft: '14px'
                                    }}
                                >
                                    {this.renderCopyFef(record, 'copyto')}
                                </div>
                            </div>

                        </div>
                     )
                }
            }
        ];
        //('renddndnd:::',myBtnBook , this.state, this.state.accountingbook)
        return(
            <div className="reckoning">
                <HeaderArea 
                    title = {this.state.json['2002305005-000000']} /* 国际化处理： 多栏账定义*/
                    showBorderBottom = {false}
                    searchContent = {
                        createReferFn(
                            this,
                            {
                                url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                value: this.state.accountingbook,
                                referStateKey: 'checkAccountBook',
                                referStateValue: this.state.checkAccountBook,
                                stateValueKey: 'accountingbook',
                                flag: false,
                                queryCondition: {
                                    pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi',
                                }
                            },
                            {
                                businessUnit: businessUnit,
                                getCheckContent: (self,accountingbook, boolean2) => this.getDataList(self,accountingbook, boolean2)
                            },
                            'multiformat'
                        )
                    }
                    btnContent = {this.props.button.createButtonApp({
                        area: 'btnarea',
                        buttonLimit: 3,
                        onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName)
                    })}
                />
                <NCDiv fieldid="multiformat" areaCode={NCDiv.config.TableCom} className="mainBody disthnoe-box">
                    <div style={{width: "100%",height: "89vh",background: "#fff"}}>
                        <Table
                            columns={columns}
                            data={this.state.tableData}
                            bodyStyle={{height:getTableHeight(80)}}
                            scroll={{ x: false, y: getTableHeight(80) }}
                            onRowClick = {(record,index,indent)=>{
                                this.state.loadData.pk_multicol = record.pk_multicol;
                                this.state.loadData.multicolname = record.multicolname;
                                this.setState({
                                    loadData: this.state.loadData
                                })
                            }}
                        />
                    </div>
				</NCDiv>
            </div>
        )
    }
}

// let initTemplate = (props) => {
//
// }

Reckon = createPage({
    // initTemplate: initTemplate
})(Reckon);


//ReactDOM.render(<Reckon />,document.querySelector('#app'));
export default Reckon;
