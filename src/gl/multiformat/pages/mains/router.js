import { asyncComponent } from 'nc-lightapp-front';
import main from '../main'
//const main = asyncComponent(() => import(/* webpackChunkName: "gl/multiformat/pages/main" */ /* webpackMode: "eager" */ '../main'));
const List = asyncComponent(() => import(/* webpackChunkName: "gl/multiformat/pages/list" */ /* webpackMode: "eager" */ '../list'));
const end = asyncComponent(() => import(/* webpackChunkName: "gl/multiformat/pages/end" */ /* webpackMode: "eager" */ '../end'));
const routes = [
	{
		path: '/',
		component: main,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/end',
		component: end
	}
];

export default routes;