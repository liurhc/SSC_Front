import React, { Component } from 'react';
import {high, base, ajax, createPage, cacheTools,deepClone, toast, getMultiLang,gzip,cardCache, createPageIcon } from 'nc-lightapp-front';
const {
    NCFormControl:FormControl,NCForm:Form,NCDatePicker:DatePicker,
    NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,
    NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,
    NCPanel:Panel,NCForm,NCButtonGroup:ButtonGroup,NCDiv
} = base;
const {  NCFormItem:FormItem } = Form;
const NCOption = NCSelect.NCOption;

import AccountDefaultModelTreeRef from '../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import AccperiodMonthTreeGridRef from '../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import createScript from '../../../public/components/uapRefer.js';
import './index.less'
import {searchById} from "../../../manageReport/common/modules/createPureBtn";
import {businessUnit, createReferFn} from "../../../manageReport/common/modules/modules";
import '../../../public/reportcss/firstpage.less'
var setDefData = cardCache.setDefData
var getDefData = cardCache.getDefData
const dataSource = 'gl.multiformat.';
const searchKey = 'MultisearchKey';
const tupStepdataSource = 'gl.multiformat.tupStep';
const nextdataSource = 'gl.multiformat.next';
const nextsearchKey = 'nextMultisearchKey';
import HeaderArea from '../../../public/components/HeaderArea';
import { getTableHeight } from '../../../public/common/method.js';

Array.prototype.removeRow = function(index=-1) {
    //('proptotype::,', this, this.length)
    let newArr = [];
    let numAdd = 0;
	this.map((item, ind) => {
        if(index != ind) {
            item.key = numAdd;
            newArr.push(item);
            numAdd++;
        }
	})
	return newArr;
};
let numRef = 0;
class Reckon extends Component{
    constructor(props){
        super(props);
        this.state = {
            json: {},
            selectObj: {},
            lowTableDel: -1,
            tableDel: -1,
            refUrl: 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index',//uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js
            remark: '',
            headeNameValue: '',
            NCSelectDefault: '',/* 国际化处理： 会计科目*/
            NCSelectDefaultValue: '',/* 国际化处理： 会计科目*/
            mainData:{
                refpk: '',
                pk_multicol: ''
            },
            pk_accountingbook:{
                refcode: "",
                refname: "",
                refpk: ""
            },
            period_pk: {
                refname: "",
                refpk: ""
            },
            NCSelectData: [],       //栏目类别数据
            speedyComRef: [], //快速编制的参照选值
            tableHeader: [],
            NCSelectedValue: 'apple',
            lowColumns:[],
            lowTableData: [],
            tableData: [],
            multiData:{
                NCSelectData:[]   //辅助类型名称
            },
            radioValue:'0',
            remeber:{

            },
            stat: {
                statcoltype: 'N'
            },
            speedyData: [],
            auxSetVal: false
        }
        this.analysizeItem = this.analysizeItem.bind(this);
        this.NCSelectChange = this.NCSelectChange.bind(this);
        this.renderRef = this.renderRef.bind(this);
        this.getNCSelectData = this.getNCSelectData.bind(this);
        this.multiSelecChange = this.multiSelecChange.bind(this);
        this.addCol = this.addCol.bind(this);
        this.getMainData = this.getMainData.bind(this);
        this.onHeadeChange = this.onHeadeChange.bind(this);
        this.auxiliaryAcc = this.auxiliaryAcc.bind(this);
        this.changeRemark = this.changeRemark.bind(this);
        this.tableDataName = this.tableDataName.bind(this);
        this.directionChange = this.directionChange.bind(this);
        this.renderRefMulti = this.renderRefMulti.bind(this);
        this.speedyCom = this.speedyCom.bind(this);
        this.searchById = searchById.bind(this);
    }
   componentWillMount() {
        
        let callback= (json) =>{
            this.setState({
                json:json,
                NCSelectDefault: json['2002305005-000009'],/* 国际化处理： 会计科目*/
                NCSelectDefaultValue: json['2002305005-000009'],/* 国际化处理： 会计科目*/
                tableData: [
                    {
                        key: 0,
                        analyori: {
                            display: json['2002305005-000001'],/* 国际化处理： 借方*/
                            scale: null,
                            value: "000"
                        },
                        analycolcode: {
                            display:'',
                            value:''
                        }
                    }
                ],
                tableHeader: [
                    {//'*栏目编码'
                        title: (
                            <div fieldid='code'>
                                <span style={{color: 'red'}}>*</span>
                                <span>{json['2002305005-000028']}</span>{/* 国际化处理： 栏目编码*/}
                            </div>
                        ),
                        dataIndex: 'code',
                        key: 'code',
                        width: 200,                             //param, item, index = 0, multi = false,
                        render: (text, record, index) => this.renderRef(this.state.cellKey, record.analycolcode , record.key,false, record, text, index, 'code')
                    },
                    {
                        id: '123',
                        title: (<div fieldid='name'>{json['2002305005-000016']}</div>),
                        dataIndex: 'name',
                        key: 'name',
                        render: (key, record, index) => {/* 国际化处理： 栏目名称*/
                            return (
                                <FormControl
                                    fieldid='name'
                                    className="demo-input"
                                    value={record.analycolname}
                                    onChange={(val) => {
                                        //(val, record);
                                        // this.tableDataName(val, record.key);
                                    }}
                                    size="sm"
                                />
                            )
                        }
                    },
                    {
                        title: (<div fieldid='direction'>{json['2002305005-000017']}</div>),
                        dataIndex: 'direction',
                        key: 'direction',
                        render: (text, record, index) => {/* 国际化处理： 分析方向*/
                            return (
                                <NCSelect
                                    showClear={false}
                                    fieldid='direction'
                                    // defaultValue = { record.analyori ? record.analyori.display : '借方' }
                                    value = { record.analyori.display}
                                    onChange={(val)=>{
                                        this.directionChange(val, record.key)
                                    }}
                                >
                                    <NCOption value='0' key='0'>{this.state.json['2002305005-000001']}</NCOption>{/* 国际化处理： 借方*/}
                                    <NCOption value='1' key='1'>{this.state.json['2002305005-000002']}</NCOption>{/* 国际化处理： 贷方*/}
                                    <NCOption value='-1' key='-1'>{this.state.json['2002305005-000022']}</NCOption>{/* 国际化处理： 双向*/}
                                </NCSelect>
                            )
                        }
                    },
                    {
                        title: (<div fieldid='opr'>{json['2002305005-000018']}</div>),/* 国际化处理： 操作*/
                        dataIndex: 'operation',
                        key: 'operation',
                        width: 120,
                        render:(name, record, index) => {
                            //('operation:', name, record)
                            return (
                                <div fieldid='opr'>
                                    <span
                                        style={{color:'#007ACE', cursor: 'pointer'}}
                                        onClick={()=>{
                                            // let newRow = this.state.tableData.removeRow(this.state.tableDel);
                                            if(this.state.tableData.length > 1){
                                                this.state.tableData.splice(index, 1);
                                            }

                                            let newTableData = deepClone(this.state.tableData)
                                            this.setState({
                                                tableData: newTableData,
                                                tableDel: -1
                                            })
                                        }}
                                    >
                                        {this.state.json['2002305005-000029']}{/* 国际化处理： 删除*/}
                                    </span>
                                </div>
                            )
                        }
                    }
                ],
                lowTableData: [
                    {
                        key: 0,
                        dsplocation: {
                            display: json['2002305005-000020'],/* 国际化处理： 表体*/
                            value: 'N'
                        },
                        statcolname: {
                            display: '',
                            value: ''
                        }
                    }
                ],
                lowColumns:[
                    {
                        title: (<div fieldid='multicolname'>{json['2002305005-000007']}</div>),/* 国际化处理： 辅助类型名称*/
                        dataIndex: 'multicolname',
                        key: 'multicolname',
                        width: 120 ,
                        render:(name, record, index) => {
                            return (
                                <NCSelect
                                    showClear={false}
                                    fieldid='multicolname'
                                    defaultValue={(record.statcolname && record.statcolname.display) || json['2002305005-000019']}/* 国际化处理： 会计期间*/
                                    value={ this.state.stat.statcoltype == 'Y' ? record.statcolname && record.statcolname.display : ''}
                                    // style={{width: 90, marginRight: 6}}
                                    disabled={this.state.stat.statcoltype == 'N'}
                                    onChange={(val) => {
                                        this.multiSelecChange(val, index, record.pk_checktype)
                                    }}
                                >
                                    {this.state.multiData.NCSelectData.map((item, index) => {
                                        return <NCOption value={JSON.stringify(item)} key={item.pk_checktype}>{item.name}</NCOption>
                                    })}
                                </NCSelect>
                            )
                        }
                    },
                    {
                        id: '123',
                        title: (<div fieldid='remark'>{json['2002305005-000008']}</div>),/* 国际化处理： 显示位置*/
                        dataIndex: 'remark',
                        key: 'remark',
                        width: 120,
                        render:(item, record, index) => {
                            return (
                                <NCSelect
                                    showClear={false}
                                    fieldid='remark'
                                    value={ record.dsplocation && record.dsplocation.display}
                                    onChange = {(value) => {
                                        this.changeRemark(value, index)
                                    }}
                                >
                                    <NCOption value= {json['2002305005-000020']} key='1'>{this.state.json['2002305005-000020']}</NCOption>{/* 国际化处理： 表体,表体*/}
                                    <NCOption value= {json['2002305005-000021']} key='2'>{this.state.json['2002305005-000021']}</NCOption>{/* 国际化处理： 表头,表头*/}
                                </NCSelect>
                            )
                        }
                    },
                    {
                        title: (<div fieldid='opr'>{json['2002305005-000018']}</div>),/* 国际化处理： 操作*/
                        dataIndex: 'operation',
                        key: 'operation',
                        width: 100,
                        render:(name, record, index) => {
                            //('operation:', name, record, index)
                            return (
                                <div fieldid='opr'> 
                                    <span
                                        style={{color:'#007ACE', cursor: 'pointer'}}
                                        onClick={(record)=>{
                                            //('recccordd::', record, this.state.lowTableData,this.state.lowTableData[index], index)
                                            if(this.state.lowTableData[index].statcolname.display != ''){
                                                this.state.lowTableData.splice(index, 1)
                                            }

                                            // let newRow = this.state.lowTableData.removeRow(this.state.lowTableDel);
                                            let newTalbe = deepClone(this.state.lowTableData)
                                            this.setState({
                                                lowTableData: newTalbe,
                                                lowTableDel: -1
                                            })
                                        }}
                                    >
                                        {json['2002305005-000029']}{/* 国际化处理： 删除*/}
                                </span>
                            </div>
                            )
                        }
                    }
                ],
            })
        }
        getMultiLang({moduleId:'2002305005',domainName:'gl',currentLocale:'simpchn',callback});
        this.analysizeItem();
        this.getNCSelectData();
        this.getMainData();
    }
    componentDidMount(){
        let gziptools = new gzip();
        setTimeout(() => {
            //if( getDefData(searchKey, nextdataSource)){
               // let selectedData = deepClone(gziptools.unzip(this.props.getUrlParam('selectedData')));
                let selectedData = getDefData(nextsearchKey, nextdataSource);
                let statcoltype = selectedData.statcoltype ? selectedData.statcoltype : 'N'
                this.setState({
                    ...selectedData, 
                    stat: {
                        statcoltype: statcoltype
                    }
                })
                // this.setState(selectedData)
           // }
        })
        this.searchById('2002305005PAGE','2002305005');

    }

    directionChange(val,index) {
        let dataArr = [this.state.json['2002305005-000022'], this.state.json['2002305005-000001'], this.state.json['2002305005-000002']];/* 国际化处理： 双向,借方,贷方*/
        let num = 1 + Number(val);
        this.state.tableData[index].analyori.value = val;
        this.state.tableData[index].analyori.display = dataArr[num];
        this.setState({
            tableData: this.state.tableData
        })
    }
    tableDataName(val, index) {
        this.state.tableData[index].analycolname = val;
        this.setState({
            tableData: this.state.tableData
        })
        //('tableDataName', this.state.tableData[index].analycolname)
    }

    multiSelecChange(val1, index, pk_checktype){
        let val = JSON.parse(val1)
        let length = this.state.lowTableData.length - 1;

        let jin = {};
        if(index === length){
            this.state.lowTableData[length].statcolname.display = val.name;
            this.state.lowTableData[length].statcolname.value = val.pk_checktype;
            jin = {
                key: length + 1,
                dsplocation: {
                    display: this.state.json['2002305005-000020'],/* 国际化处理： 表体*/
                    value: 'N',
                    scale: null
                },
                statcolname: {
                    display: '',
                    value: '',
                    scale: null
                }
            }
            this.state.lowTableData.push(jin)
        }else{
            this.state.lowTableData[index].statcolname.display = val.name;
            this.state.lowTableData[index].statcolname.value = val.pk_checktype;
        }

        this.setState({
            lowTableData: this.state.lowTableData
        })
        //('lowTableData>>',this.state.lowTableData);
    }
    changeRemark(val, index){
        let newData = deepClone(this.state.lowTableData);
        newData[index].dsplocation.display = val;
        newData[index].dsplocation.value = val == this.state.json['2002305005-000021']? 'Y':'N';/* 国际化处理： 表头*/
        this.setState({
            lowTableData: [...newData]
        })
    }
    getNCSelectData(){
        let url ='/nccloud/gl/accountrep/assbalancequeryobject.do';
        ajax({
            url,
            data:{
                "pk_accountingbook": this.props.getUrlParam('refpk'),
                'needaccount': false
            },
            success: (response) => {
                let config = {NCSelectData: response.data};
                //(response.data);
                this.setState({
                    multiData: config
                });
            }
        });
    }
    analysizeItem(){
        let url ='/nccloud/gl/accountrep/assbalancequeryobject.do';
        ajax({
            url,
            data:{
                "pk_accountingbook": this.props.getUrlParam('refpk'),
                'needaccount': true
            },
            success: (response) => {
                //(response.data);
                let config = {NCSelectData:response.data};
                this.setState(config);
            }
        });
    }
    NCSelectChange(param){
        //('NCSelectChange>>>', param);
        let referUrl = param.refpath;
        let objKey = param.pk_checktype;
        this.setState({
            selectObj: {...param},
            NCSelectDefault: param.name,
            NCSelectDefaultValue: param.pk_checktype,
            speedyData: [],
            tableData: [
                { 
                    key: 0,
                    analycolname: '',
                    analyori: {
                        display: this.state.json['2002305005-000001'],/* 国际化处理： 借方*/
                        scale: null,
                        value: "001"
                    },
                    analycolcode: {
                        display:'',
                        value:''
                    }
                }
            ],
            refUrl: referUrl,
            cellKey: objKey
        });
    }
    addCol(currindex) {
        let length = this.state.tableData.length - 1;
        if (length == currindex) {
            let index = this.state.tableData.length;
            let jin = { 
                key: index,
                analyori: {
                    display: this.state.json['2002305005-000001'],/* 国际化处理： 借方*/
                    scale: null,
                    value: "0"
                },
                analycolcode: {
                    display: '',
                    value: ''
                }
            }
            this.state.tableData.push(jin);
            this.setState({
                tableData: this.state.tableData,
            });
         }
    }
    renderRefMulti(){
        numRef++;
        let numName = this.state.cellKey;
        let book;
        if(!this.state[numName]) {
            setTimeout(()=>{
    			{createScript.call(this, this.state.refUrl+'.js', numName)}
            })
		} else {
            //('renderRefMulti>>>>', this.state.selectObj);
            let options = {
                fieldid: 'speedyData',
                value: this.state.speedyData,
                isMultiSelectedEnabled: true,
                "isShowDisabledData": true,
                queryCondition:() => {
                    return {
                        "dateStr": this.props.getUrlParam('busiDate'),
                        "pk_accountingbook": this.props.getUrlParam('refpk'),
                        "pk_org": this.props.getUrlParam('pk_org'),
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode": 'fi',
                        'pk_defdoclist': this.state.selectObj.classid
                    }
                },
                onChange:(v ) => {
                    //(v);
                    cacheTools.set('allData', null)
                    this.setState({
                        speedyData: v
                    })
                }
            };
            let newOptions = {};
            if(this.state.selectObj.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9' || this.state.selectObj.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'){
                newOptions = {
                    ...options,
                    isShowUnit:true,
                    "unitValueIsNeeded":false,
                    "isShowDimission":true,
                    queryCondition:() => {                     
                        return {
                            "pk_accountingbook": this.props.getUrlParam('refpk'),
                            "pk_org": this.props.getUrlParam('pk_org'),
                            "isDataPowerEnable": "Y",
                            "DataPowerOperationCode" : 'fi',
                            dateStr: this.props.getUrlParam('busiDate'),
                            "busifuncode":"all",
                            'pk_defdoclist': this.state.selectObj.classid,
                            isShowDimission:true
                        }
                    },
                    unitProps:{
                        refType: 'tree',
                        refName: this.state.json['2002305005-000023'],/* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:this.state.json['2002305005-000023'],refpk:'root'},/* 国际化处理： 业务单元*/
                        placeholder:this.state.json['2002305005-000023'],/* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[this.state.json['2002305005-000024'], this.state.json['2002305005-000025']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false,
                    },
                    unitCondition:{
                        pk_financeorg: this.props.getUrlParam('pk_org'),
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    },
                }
            }else {
                newOptions = {
                    ...options
                }
            }
			book =  (
				<Row>
					<Col xs={12} md={12}>
					{this.state[numName]?(this.state[numName])(newOptions):<div/>}
					</Col>
				</Row>
			);
        }
        return book;
    }
    renderRef(param, item, index = 0, multi = false, record, text, itemIndex, fieldid) {
        //('renderRef>>>',param, item,  record, text);
        let book;
        if(!this.state[param]) {
            setTimeout(()=>{
                {createScript.call(this, this.state.refUrl+'.js', param)}
            })
		} else {
            let refname = item ? item.display : '';
            let refpk = item ? item.value : '';
            let dataVal = {
                'refname': refname,
                'refpk': refpk
            };
            let options = {
                fieldid: fieldid,
                value: dataVal,
                isMultiSelectedEnabled: multi,
                "isShowDisabledData": true,
                queryCondition:() => {                
                    return {
                        "pk_accountingbook": this.props.getUrlParam('refpk'),
                        "pk_org": this.props.getUrlParam('pk_org'),
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode": 'fi',
                        "dateStr": this.props.getUrlParam('busiDate'),
                       'pk_defdoclist': this.state.selectObj.classid
                }},

                onChange:(v )=> {
                    //('value:::',v);

                    setTimeout(() => {
                        this.state.tableData[index].analycolcode.display = v && (v.code || v.refcode || (v.nodeData && v.nodeData.refcode)) || '';
                        // dataVal.refpk = v.refcode;
                        this.state.tableData[index].analycolcode.value = v && v.refpk || '';
                        this.state.tableData[index].analycolname = v && (this.state.auxSetVal ? (v.code || v.refcode || (v.nodeData && v.nodeData.refcode)) + v.refname : v.refname) || '';
                        this.setState({
                            tableData: this.state.tableData
                        })
                        this.addCol(record.key)
                    })

                    //(this.state.tableData);
                }
            };
            let newOptions = {};
            if(this.state.selectObj.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||this.state.selectObj.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'){//部门，人员
                newOptions = {
                    ...options,
                    isShowUnit:true,
                    "unitValueIsNeeded":false,
                    queryCondition:() => {                    
                        return {
                            "pk_accountingbook": this.props.getUrlParam('refpk'),
                            "pk_org": this.props.getUrlParam('pk_org'),
                            "isDataPowerEnable": "Y",
                            "DataPowerOperationCode" : 'fi',
                            dateStr: this.props.getUrlParam('busiDate'),
                            "busifuncode":"all",
                            'pk_defdoclist': this.state.selectObj.classid,
                            isShowDimission:true
                        }
                    },
                    unitProps:{
                        refType: 'tree',
                        refName: this.state.json['2002305005-000023'],/* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:this.state.json['2002305005-000023'],refpk:'root'},/* 国际化处理： 业务单元*/
                        placeholder:this.state.json['2002305005-000023'],/* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[this.state.json['2002305005-000024'], this.state.json['2002305005-000025']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false
                    },
                    unitCondition:{
                        pk_financeorg: this.props.getUrlParam('pk_org'),
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    },
                }
            }else if(this.state.selectObj.classid && this.state.selectObj.classid.length === 20){
                newOptions = {
                    ...options,
                    pk_defdoclist:this.state.selectObj.classid
                }
            }else{
                newOptions = {
                    ...options
                }
            }

			book =  (
				<Row>
					<Col xs={12} md={12}>
					{this.state[param]?(this.state[param])(newOptions):<div/>}
					</Col>
				</Row>
			);
        }
        return book;
    }
    handleChange(value) {
        this.setState({radioValue: value});
    }
    // 修改获取值
    getMainData() {
        let gziptools = new gzip();
        if(this.props.getUrlParam('data') ){
            let data = this.props.getUrlParam('data');
           // this.state.mainData = gziptools.unzip(data);
           this.state.mainData = getDefData(searchKey, tupStepdataSource)
        }
        this.state.mainData.refpk = this.props.getUrlParam('refpk') || cacheTools.get('allParam').refpk;
        this.state.mainData.pk_multicol = this.props.getUrlParam('pk_multicol') || cacheTools.get('allParam').pk_multicol;
        
        this.setState({
            speedyComRef: this.state.mainData.speedyComRef || [],
            radioValue: this.state.mainData.radioValue || "0",
            auxSetVal: this.state.mainData.auxSetVal || false,
        });
        let url ='/nccloud/gl/accountrep/assbalancequeryobject.do';
        this.setState({
            mainData: this.state.mainData
        })
        if(this.props.getUrlParam('data') ){
            let arrData = [];
            let data = this.state.mainData;
            data.analy.forEach((item,index)=>{
                let dataI = item;
                dataI.key = index;
                arrData.push(dataI);
            })
            this.state.lowTableData = data.stat || this.state.lowTableData;
            this.state.lowTableData.forEach((item,index)=>{
                item.key = index;
            })
            this.state.stat.statcoltype = data.statcoltype;
            this.state.cellKey;
            this.setState({
                headeNaValue: data.remark,
                headeNameValue: data.multicolname,
                NCSelectDefault: data.analycoltype.display,
                NCSelectDefaultValue: data.analycoltype.value,
                refUrl: data.refpath,
                tableData: arrData,
                stat: this.state.stat,
                cellKey: 'first'
            })
        }else{
            if((this.props.getUrlParam('refpk') && this.props.getUrlParam('pk_multicol')) || (cacheTools.get('allParam').refpk && cacheTools.get('allParam').pk_multicol)) {
                let url ='/nccloud/gl/accountrep/multibookformatquery.do';
                ajax({
                    url,
                    data: {
                        "pk_multicol": this.state.mainData.pk_multicol
                    },
                    success: (response) => {
                        let arrData = [];
                        //('response.data', response.data);
                        response.data.analy.forEach((item,index)=>{
                            let dataI = item;
                            dataI.key = index;
                            arrData.push(dataI);
                        })
                        this.state.lowTableData = response.data.stat || this.state.lowTableData;
                        this.state.lowTableData.forEach((item,index)=>{
                            item.key = index;
                        })
                        let jin = {
                            key: this.state.lowTableData.length,
                            dsplocation: {
                                display: this.state.json['2002305005-000020'],/* 国际化处理： 表体*/
                                value: 'N',
                                scale: null
                            },
                            statcolname: {
                                display: '',
                                value: '',
                                scale: null
                            }
                        }
                        if(this.state.lowTableData[0].statcolname && this.state.lowTableData[0].statcolname.value != ''){
                            this.state.lowTableData.push(jin)
                        }

                        this.setState({
                            lowTableData: this.state.lowTableData
                        })
                        this.state.stat.statcoltype = response.data.statcoltype;
                        this.state.cellKey;
                        this.setState({
                            headeNaValue: response.data.remark,
                            headeNameValue: response.data.multicolname,
                            NCSelectDefault: response.data.analycoltype.display,
                            NCSelectDefaultValue: response.data.analycoltype.value,
                            refUrl: response.data.refpath,
                            tableData: arrData,
                            stat: this.state.stat,
                            cellKey: 'first'
                        });
                        this.addCol(this.state.tableData.length - 1);
                    }
                })
                
            } else if (this.props.getUrlParam('refpk')) {
                ajax({
                    url,
                    data: {
                        'pk_accountingbook': this.state.mainData.refpk,
                        'needaccount': false
                    },
                    success: (response) => {
                        //(response.data);
                    }
                })
            }
        }
    }
    onHeadeChange(val,item){

        if(item === 'headeNameValue'){
            let exeReg = /^.{0,40}$/;
            let result = exeReg.exec(val);
            //('onHeadeChange>>', result, exeReg);
            if(result){
                this.setState({
                    [item]: result[0]
                })
            }
        }else{
            if(val.length <= 200){
                this.setState({
                    [item]: val
                })
            }else{
                toast({
                    content: this.state.json['2002305005-000026'],/* 国际化处理： 最多只能输入200个字符！*/
                    color: 'warning'
                })
            }

        }

    }
    auxiliaryAcc(val){
        //  lowTableData
        this.state.stat.statcoltype = val ? 'Y':'N';
        let nowTableData = [{
            key: 0,
            dsplocation: {
                display: this.state.json['2002305005-000020'],/* 国际化处理： 表体*/
                scale: null,
                value: 'N'
            },
            statcolname: {
                display:'',
                scale: null,
                value: ''
            }
        }];
        this.setState({
            stat: this.state.stat,
            lowTableData: nowTableData
        })
    }
    sendData(){      
        let filterArr = [];
        let gziptools = new gzip();
        this.state.tableData.map((item, index) => {
            if(item.analycolcode.value != ''){
                filterArr.push(item.analycolcode.value);
            }
        })
        let newFilterArr = [...new Set(filterArr)];
        if(newFilterArr.length < filterArr.length){
            toast({content: this.state.json['2002305005-000047'], color: 'warning'});
            return;
        }
        let allData = {
            "pk_accountingbook": this.state.mainData.refpk,
            analy: this.state.tableData,//分析项内容设置 的表格数据
            analycoltype:{
                display: this.state.NCSelectDefault,
                scale: null,
                value: this.state.NCSelectDefaultValue
            },
            multicolname: this.state.headeNameValue,
            remark: this.state.headeNaValue,
            statcoltype: this.state.stat.statcoltype,
            stat: this.state.lowTableData,
            refpath:this.state.refUrl,
            speedyComRef: this.state.speedyComRef,
            radioValue: this.state.radioValue,
            auxSetVal: this.state.auxSetVal,
            speedyData: this.state.speedyData
        }
        cacheTools.set('allData', allData);
        cacheTools.set('tableData', this.state.tableData);
        cacheTools.set('NCSelectDefault', this.state.NCSelectDefault)
       setDefData(nextsearchKey, nextdataSource, {
            headeNameValue: this.state.headeNameValue,    //多栏账名称
            headeNaValue: this.state.headeNaValue,        //备注
            statcoltype: this.state.stat.statcoltype,     //辅助核算设置 的复选框
            lowTableData: this.state.lowTableData,        //辅助核算设置 的表格数据
            NCSelectDefault: this.state.NCSelectDefault,  //栏目类别
            NCSelectDefaultValue: this.state.NCSelectDefaultValue,
            tableData: this.state.tableData,              //分析项内容设置表格数据
            auxSetVal: this.state.auxSetVal,              //栏目名称显示为编码加名称
            speedyData: this.state.speedyData,            //快速编制
            radioValue: this.state.radioValue,            //方向
        });
        this.props.pushTo('/end', {
            refpk: this.state.mainData.refpk,
            pk_multicol : this.state.mainData.pk_multicol,
            oldRef: this.props.getUrlParam('selectRef'),
            pk_org: this.props.getUrlParam('pk_org'),
            // selectedData: gziptools.zip(JSON.stringify({
            //     headeNameValue: this.state.headeNameValue,    //多栏账名称
            //     headeNaValue: this.state.headeNaValue,        //备注
            //     statcoltype: this.state.stat.statcoltype,     //辅助核算设置 的复选框
            //     lowTableData: this.state.lowTableData,        //辅助核算设置 的表格数据
            //     NCSelectDefault: this.state.NCSelectDefault,  //栏目类别
            //     NCSelectDefaultValue: this.state.NCSelectDefaultValue,
            //     tableData: this.state.tableData,              //分析项内容设置表格数据
            //     auxSetVal: this.state.auxSetVal,              //栏目名称显示为编码加名称
            //     speedyData: this.state.speedyData,            //快速编制
            //     radioValue: this.state.radioValue,            //方向
            // }))
        })
    }
    speedyCom() {
        let dataArr = [this.state.json['2002305005-000022'], this.state.json['2002305005-000001'], this.state.json['2002305005-000002']];/* 国际化处理： 双向,借方,贷方*/
        let num = 1 + Number(this.state.radioValue);
        num = Number(num);
        let analyoriDisplay = dataArr[num];
        //(analyoriDisplay)
        if(this.state.speedyData.length == '0') {
            toast({content: this.state.json['2002305005-000027'], color: 'warning'});/* 国际化处理： 请选择快速编制项*/
            return;
        }
        // newTableData=原非空的分析项+快速编制项+空行
        let newTableData = [];
        this.state.tableData.forEach((item, index) => {
            if(item.analycolcode && item.analycolcode.value) {
                newTableData.push(item);
            }
        });
        let newTableDataLength = newTableData.length;
        this.state.speedyData.forEach((item, index) => {
            let addData = {
                analycolname: item.refname,
                key: newTableDataLength + index,
                analyori: {
                    display: analyoriDisplay,
                    scale: null,
                    value: this.state.radioValue
                },
                analycolcode: {
                    display: item.code || item.refcode || (item.nodeData && item.nodeData.refcode),
                    value: item.refpk
                }
            }
            if(this.state.auxSetVal) {
                addData.analycolname = (item.code || item.refcode || (item.nodeData && item.nodeData.refcode)) + addData.analycolname;
            }
            newTableData.push(addData);
        });
        setTimeout(()=>{
            this.setState({
                tableData: newTableData
            },()=>{
                //(this.state.tableData)
                this.addCol(this.state.tableData.length - 1);
            })
        })
    }
    auxSetting(value){
        this.setState({
            auxSetVal: value
        })
    }
    handleLoginBtn = (obj, btnName) => {
        //('btnName>', btnName)
        if(btnName === 'upStep'){//1、上一步
            this.props.pushTo('/',{
                refpk: this.state.mainData.refpk,
                oldRef: this.props.getUrlParam('selectRef')
            })
        }else if(btnName === 'downStep'){//2、下一步
            this.sendData()
        }
    }
    render(){

        let { NCSelectDefault } = this.state;
        let conHeignt = getTableHeight(88)
        return (
            <div className="nc-single-table reckoning">
                <HeaderArea 
                    title = {this.state.json['2002305005-000000']} /* 国际化处理： 多栏账定义*/
                    btnContent = {this.props.button.createButtonApp({
                        area: 'secondpage',
                        buttonLimit: 3,
                        onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                    })}
                />
                <NCDiv fieldid="multiformat" areaCode={NCDiv.config.FORM} className="serrch-area-box nc-theme-gray-area-bgc nc-theme-area-split-bc">
                    <div className='nc-theme-common-font-c' style={{fontSize: '13px'}}>
                        <span style={{color: 'red'}}>*</span>
                        {this.state.json['2002305005-000012']}{/* 国际化处理： 多栏账名称*/}
                    </div>
                    <div className={"serrch-ref"}>
                        <FormControl
                            fieldid='multiname'
                            className="demo-input"
                            value={this.state.headeNameValue}
                            onChange={(val) => {
                                this.onHeadeChange(val, 'headeNameValue')
                            }
                            }
                            size="sm"
                        />
                    </div>
                    <div className={"serrch-ref reMarkContainer"}>
                        <span className="main-bottom-span nc-theme-common-font-c">{this.state.json['2002305005-000013']}</span>{/* 国际化处理： 备注*/}
                        <FormControl
                            fieldid='remarks'
                            style={{
                                minHeight: '30px',
                                overflow: 'auto'
                            }}
                            componentClass='textarea'
                            className="demo-input"
                            value={this.state.headeNaValue}
                            onChange={(val) => {
                                this.onHeadeChange(val, 'headeNaValue')
                            }
                            }
                            size="sm"
                        />
                    </div>
                </NCDiv>
                <div className="mainBody m-dlzdy-list-box nc-theme-area-bgc nc-theme-area-split-bc" style={{height: conHeignt}}>
                    {/* <div className=" serrch-area-box ">
                        <div style={{fontSize: '13px'}}>
                            <span style={{color: 'red'}}>*</span>
                            {this.state.json['2002305005-000012']}
                        </div>
                        <div className={"serrch-ref"}>
                            <FormControl
                                className="demo-input"
                                value={this.state.headeNameValue}
                                onChange={(val) => {
                                    this.onHeadeChange(val, 'headeNameValue')
                                }
                                }
                                size="sm"
                            />
                        </div>
                        <div className={"serrch-ref reMarkContainer"}>
                            <span className="main-bottom-span">{this.state.json['2002305005-000013']}</span>
                            <FormControl
                                style={{
                                    minHeight: '30px',
                                    overflow: 'auto'
                                }}
                                componentClass='textarea'
                                className="demo-input"
                                value={this.state.headeNaValue}
                                onChange={(val) => {
                                    this.onHeadeChange(val, 'headeNaValue')
                                }
                                }
                                size="sm"
                            />
                        </div>
                    </div> */}
                    <NCDiv fieldid="left" areaCode={NCDiv.config.Area} className="mainShowLeft nc-theme-area-split-bc">
                        <div className={"top-main"}>
                            <Checkbox
                                value={true}
                                checked={this.state.stat.statcoltype == 'Y'}
                                onChange={(val) => {
                                    this.auxiliaryAcc(val)
                                }}
                            >{this.state.json['2002305005-000014']}</Checkbox>{/* 国际化处理： 辅助核算设置*/}
                        </div>
                        <NCDiv fieldid="left" areaCode={NCDiv.config.TableCom} className="table0 table-border nc-theme-area-split-bc">
                            <Table
                                // style={{
                                //     borderLeft: '1px solid #d0d0d0',
                                //     borderRight: '1px solid #d0d0d0',
                                //     borderBottom: '1px solid #d0d0d0'
                                // }}
                                bodyStyle={{height:getTableHeight(180)}}
                                scroll={{ x: true, y: getTableHeight(180) }}
                                columns={this.state.lowColumns}
                                data={this.state.lowTableData}
                                onRowClick = {(record, index, indent) => {
                                    this.setState({
                                        lowTableDel: index
                                    })
                                }}
                            />
                        </NCDiv>
                    </NCDiv>
                    <NCDiv fieldid="middle" areaCode={NCDiv.config.Area} className="mainShowCenter nc-theme-area-split-bc">
                       <div className={"m-mainShowCenter-top"}>
                           <div className="title nc-theme-common-font-c">{this.state.json['2002305005-000030']}</div>{/* 国际化处理： 分析项内容设置*/}
                           <div>
                               <span className='nc-theme-common-font-c'>{this.state.json['2002305005-000031']}:</span>{/* 国际化处理： 栏目类别*/}
                               <NCSelect
                                   showClear={false}
                                   fieldid='pk_checktype'
                                   value={NCSelectDefault}
                                   style={{ width: 200, marginRight: 6 }}
                                   onChange={(v) => {
                                           //(v);
                                       this.setState({
                                           tableData: [
                                               {
                                                   key: 0,
                                                   analyori: {
                                                       display: this.state.json['2002305005-000001'],/* 国际化处理： 借方*/
                                                       scale: null,
                                                       value: "002"
                                                   },
                                                   analycolcode: {
                                                       display:'',
                                                       value:''
                                                   }
                                               }
                                           ],
                                       })
                                       this.NCSelectChange(v);
                                   }}
                               >
                                   {this.state.NCSelectData.map((item, index) => {
                                       return <NCOption value={item} key={item.pk_checktype}>{item.name}</NCOption>
                                   })}
                               </NCSelect>
                           </div>

                           <div className={"checked"}>
                               <Checkbox
                                   checked={this.state.auxSetVal}
                                   onChange={(val) => {
                                       this.auxSetting(val)
                                   }}
                               >{this.state.json['2002305005-000032']}</Checkbox>{/* 国际化处理： 栏目名称显示为编码加名称*/}
                           </div>

                       </div>
                        <NCDiv fieldid="middle" areaCode={NCDiv.config.TableCom} className='tableContainer table-border nc-theme-area-split-bc'>
                            <Table
                                // style={{
                                //     borderLeft: '1px solid #d0d0d0',
                                //     borderRight: '1px solid #d0d0d0',
                                //     borderBottom: '1px solid #d0d0d0'
                                // }}
                                bodyStyle={{height:getTableHeight(180)}}
                                scroll={{ x: true, y: getTableHeight(180) }}
                                columns={this.state.tableHeader}
                                data={this.state.tableData}
                                onRowClick = {(record, index, indent) => {
                                    this.setState({
                                        tableDel: index
                                    })
                                }}
                                // scroll={{
                                //     x: true,
                                //    y: this.getTableHeight()
                                // }}
                            />
                        </NCDiv>

                    </NCDiv>
                    <NCDiv fieldid="right" areaCode={NCDiv.config.Area} className="mainShowRight">
                        <div className={"m-mainShowRight-top-box"}>
                            <div className="title nc-theme-common-font-c">{this.state.json['2002305005-000033']}</div>{/* 国际化处理： 快速编制*/}
                            <div className="refContent">
                                {
                                    this.renderRefMulti()
                                }
                            </div>
                        </div>
                        <div className={"m-mainShowRight-top-box"}>
                            <div className="choseRadio">
                                <div className="title nc-theme-common-font-c">{this.state.json['2002305005-000034']}</div>{/* 国际化处理： 方向*/}
                                <Radio.NCRadioGroup
                                    name="fruit"
                                    selectedValue = {this.state.radioValue}
                                    onChange = {this.handleChange.bind(this)}
                                >
                                    <Radio value="0" >{this.state.json['2002305005-000035']}</Radio>{/* 国际化处理： 借*/}
                                    <Radio value="1" >{this.state.json['2002305005-000036']}</Radio>{/* 国际化处理： 贷*/}
                                    <Radio value="-1">{this.state.json['2002305005-000022']}</Radio>{/* 国际化处理： 双向*/}
                                </Radio.NCRadioGroup>
                            </div>
                        </div>

                        <div className="ksbz-box">
                            <Button fieldid='speedy' onClick={()=> this.speedyCom()}>{this.state.json['2002305005-000033']}</Button>{/* 国际化处理： 快速编制*/}
                        </div>
                    </NCDiv>
                </div>
            </div>
        )
    }
}

let initTemplate = (props) => {
}

Reckon = createPage({
    initTemplate: initTemplate
})(Reckon);

//ReactDOM.render(<Reckon />,document.querySelector('#app'));
export default Reckon;