import React, { Component } from 'react';
import {high,base,ajax } from 'nc-lightapp-front';
const { NCFormControl:FormControl,NCForm:Form,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCForm,NCButtonGroup:ButtonGroup,
} = base;
const {  NCFormItem:FormItem } = Form;

import AccountDefaultModelTreeRef from '../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import AccperiodMonthTreeGridRef from '../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import createScript from '../../../public/components/uapRefer.js';
import './index.less'

class Reckon extends Component{
    constructor(props){
        super(props);
        this.state = {
            pk_accountingbook:{
                refcode:"",
                refname:"",
                refpk:""
            },
            period_pk:{
                refname:"",
                refpk:""
            },
            searchData:{
                pk_accountingbook:'',  //核算账簿
                pk_unit:'', // 业务单元  参照多选 数组
                querybyperiod:'Y', //是否按期间查询  Y/N
                year:'',
                period:'',
                begindate:'',
                enddate:'',
                analyze:'0',  // 0未指定  1已指定 2 全部
                includeuntally:'N', //包含未记账  Y/N
                includeuncash:'N', //包含非现金凭证  Y/N
                pk_vouchertype:'',//凭证类别
                startvoucherno:'', //开始凭证号
                endvoucherno:'',  //结束凭证号
                pk_prepared:'',//制单人
                accountcode:'',// 科目编码   注意是编码不是主键  数组
                oppaccountcode:'', // 对方科目编码  注意是编码不是主键  数组
                // assvo :[{pk_checktype,pk_checkvalue},{pk_checktype,pk_checkvalue}],  //辅助核算
                //下面条件是快速分析多的2个参数
                // reurncurr:'', //返回币种  1 组织本币，2集团本币， 3全局本币
                // pk_currtype:'' //币种主键
            },
            tableData1:[],
            selectData:[
                {name: 'jin'},
                {name: 'hua'},
                {name: 'teng'}
            ]
        }
    }
    test(){
        let url ='/nccloud/gl/accountrep/assbalancequeryobject.do';

        ajax({
            url,
            data:{"pk_accountingbook": "1001A3100000000000PE",'needaccount': true},
            success: (response) => {

                // this.setState({tableData:response.data});
            }
        });
    }
    componentDidMount(){
        
    }
    componentWillMount() {
        this.test();
        this.analysizeItem();
    }
    analysizeItem(){
        let url ='/nccloud/gl/accountrep/assbalancequeryobject.do';

        ajax({
            url,
            data:{"pk_accountingbook": "1001A3100000000000PE",'needaccount': true},
            success: (response) => {
                let config = {selectData:response.data}
                this.setState(config);

                // this.setState({tableData:response.data});
                
            }
        });
    }
    render(){
        const columns = [
            { title: this.state.json['2002305005-000007'], dataIndex: 'multicolname', key: 'multicolname', width: 100 },/* 国际化处理： 辅助类型名称*/
            { id: '123', title: this.state.json['2002305005-000008'], dataIndex: 'remark', key: 'remark', width: 100 }/* 国际化处理： 显示位置*/
        ];
        let {pk_accountingbook,period_pk,searchData,pk_vouchertype,pk_prepared,accountcode,oppaccountcode,tableData} = this.state;

        return(
            <div className="reckoning">
                <div className="nc-bill-form-area">
                    <ButtonGroup style={{ margin: 10 }}>
                        <Button colors="primary">this.state.json['2002305005-000010']</Button>/* 国际化处理： 上一步*/
                        <Button colors="primary">this.state.json['2002305005-000011']</Button>/* 国际化处理： 下一步*/
                    </ButtonGroup>
				</div>
                <div className="mainBody">
                    <div>
                        <div className="accNameHead">
                            this.state.json['2002305005-000012']/* 国际化处理： 多栏账名称*/
                        </div>
                        <div className="accNameInt">
                            <FormControl
                                className="demo-input"
                                value={this.state.value}
                                onChange={this.onChange}
                                size="sm"
                            />
                        </div>
                        <div className="accNameHead">
                            this.state.json['2002305005-000013']/* 国际化处理： 备注*/
                        </div>
                        <div className="accSayInt">
                            <FormControl
                                className="demo-input"
                                value={this.state.value}
                                onChange={this.onChange}
                                size="sm"
                            />
                        </div>
                    </div>
				</div>
                <div>
                    <AccountDefaultModelTreeRef
                        onChange={(v) => {
                        }}
                    />
                </div>
                <div className="mainBody">
                    <div className="mainShowLeft">
                        <div style={{marginTop:10,marginLeft:10}}>
                            <Checkbox>this.state.json['2002305005-000014']</Checkbox>/* 国际化处理： 辅助核算设置*/
                        </div>
                        <div className="table0">
                            <Table
                                columns={columns}
                                data={this.state.tableData1}
                            />
                        </div>
                    </div>
                    <div className="mainShowRight">
                        <div>this.state.json['2002305005-000015']</div>/* 国际化处理： 分析项目内容设置*/
                        <Select
                            // defaultValue=this.state.json['2002305005-000009']/* 国际化处理： 会计科目*/
                            style={{ width: 200, marginRight: 6 }}
                            // onChange={this.handleChange}
                            >
                            {this.state.selectData.map((item, index) => {
                                    return <Option value={item.pk_checktype} key={item.pk_checktype} >{item.name}</Option>
                                } )}
                            {/* <Option value="acc">会计科目</Option>
                            <Option value="selfItem">摊销项目（自定义档案）</Option>
                            <Option value="material">物料基本分类</Option>
                            <Option value="item">项目</Option> */}
                        </Select>
                    </div>
                </div>
                <Radio.NCRadioGroup
                        name="fruit"
                        selectedValue={searchData.querybyperiod}
                        onChange={(v)=>{
                            searchData.querybyperiod=v;
                            this.setState({
                                searchData
                            })
                        }}>

                        <Radio value="Y" >
                            <AccperiodMonthTreeGridRef
                                value = {{refpk:period_pk.refpk,refname:period_pk.refname}}
                                onChange={(v)=>{

                                    searchData.year=v.values.begindate.value.split('-')[0];
                                    searchData.period=v.values.accperiodmth.value;
                                    searchData.begindate=v.values.begindate.value;
                                    searchData.enddate=v.values.enddate.value;
                                    period_pk.refname=v.refname;
                                    period_pk.refpk=v.refpk;
                                    this.setState({
                                        period_pk,
                                        searchData
                                    })
                                }}
                            />
                        </Radio>

                        <Radio value="N" className='displayB'>
                            <Row>
                                <Col lg={5} sm={5} xs={5}>
                                    <DatePicker
                                        value={searchData.begindate}
                                        onChange={(v)=>{
                                            searchData.begindate=v;
                                            this.setState({
                                                searchData
                                            })
                                        }}
                                    />
                                </Col>
                                <Col lg={2} sm={2} xs={2}>
                                        -----
                                </Col>
                                <Col lg={5} sm={5} xs={5}>
                                    <DatePicker
                                        value={searchData.enddate}
                                        onChange={(v)=>{
                                            searchData.enddate=v;
                                            this.setState({
                                                searchData
                                            })
                                        }}
                                    />
                                </Col>
                            </Row>
                        </Radio>
                    </Radio.NCRadioGroup>
            </div>
        )
    }
}
ReactDOM.render(<Reckon />,document.querySelector('#app'));
