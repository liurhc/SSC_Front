import initTemplate from './initTemplate';
import buttonClick from './buttonClick';

const appcode = '20020VYOBS';
const pagecode = '20020VYOBSPAGE';
export { initTemplate, buttonClick, appcode, pagecode };
