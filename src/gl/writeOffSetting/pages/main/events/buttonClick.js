import { base, ajax, toast, promptBox } from 'nc-lightapp-front';
export default function buttonClick(props, id, _this) {
    let { tableData, selectedRow, currentRow,searchMap } = _this.state;
    let param = _this.getSubmitFormData();
    if(id === 'add'){
        if(!_this.state.ref1.refpk){
            toast({ content: _this.state.json['20020VYOBS-000000'], color: 'warning' });/* 国际化处理： 请选择核算账簿*/
            return false;
        }
    } else if (id !== 'save' && id !== 'cancel') {
        if (!currentRow.pk_verifyobj){
            toast({ content: _this.state.json['20020VYOBS-000001'], color: 'warning' });/* 国际化处理： 没有选择记录！*/
            return false;
        }
    }
    
    switch (id) {
        //新增
        case 'add':
            toggleEditStatus('add',_this);
            let addLine = JSON.parse(JSON.stringify(_this.addLine));
            addLine.editable = true;//编辑态标识，保存时不传给后台
            selectedRow = selectedRow.map(item => item = null);
            selectedRow.push(true);//高亮新增行
            tableData = [...tableData,addLine];
            _this.setState({
                addLine,
                editRow: addLine,
                editIndex: selectedRow.length - 1,
                selectedRow: selectedRow,
                tableData: tableData,
                accountbookIsdisabled:true,//账簿不可切换
                status_new:"add",
                currentRow : addLine

            });
            break;
        //保存
        case 'save':
            let parms = _this.getSubmitFormData('edit');
            ajax({
                url: '/nccloud/gl/verify/verifyobjsave.do',
                data: parms,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        toggleEditStatus('browse',_this);
                        _this.getData(searchMap);
                        toast({ content: _this.state.json['20020VYOBS-000002'], color: 'success' });/* 国际化处理： 保存成功*/
                    }
                },
                error: function (res) {
                    toast({ content: res.message, color: 'danger' });
                }
            });
            _this.setState({
                
                status_new:"nomal"
            });
            break;
        //取消
        case 'cancel':
            promptBox({
                color:"warning",
                title: _this.state.json['20020VYOBS-000003'],/* 国际化处理： 取消？*/
                content: _this.state.json['20020VYOBS-000004'],/* 国际化处理： 确认要取消吗*/
                beSureBtnName: _this.state.json['20020VYOBS-000005'],/* 国际化处理： 确认*/
                cancelBtnName: _this.state.json['20020VYOBS-000006'] ,/* 国际化处理： 取消*/
                beSureBtnClick: cancel.bind('browse',props,_this,self)
            });
            // toggleEditStatus('browse',_this);
            // _this.getData();
            _this.setState({
                
                status_new:"nomal"
            });
            break;
    }
    function toggleEditStatus(type,self) {
        // let editable = type !== 'browse';
        
        let status = type === 'browse';
        self.setState({
            accountbookIsdisabled:!status
        })
        //先设置隐藏
        props.button.setButtonsVisible({
            add: status,
            save: !status,
            cancel: !status
        });
    }
}
function cancel(type,self){
    self.getData(self.state.searchMap)
    self.toggleEditStatus('browse');
}
