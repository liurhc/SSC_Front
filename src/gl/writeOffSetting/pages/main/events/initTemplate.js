import { ajax } from 'nc-lightapp-front';
import { appcode, pagecode } from './index';
export default function initTemplate(props) {
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do',
		data: {
			// pk_appregister: "0001Z31000000005BEGV",
			pagecode,
			appcode
		},
		success: (res) => {
			if (res.success) {
				if (res.data) {
					props.button.setButtons(res.data);
					props.button.setButtonDisabled('add',true);
					props.button.setButtonVisible(['save', 'cancel'], false);
				}
			}
		}
	});
}

