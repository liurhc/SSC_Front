
//核销对象设置

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, getBusinessInfo,getMultiLang,createPageIcon  } from 'nc-lightapp-front';
const { NCTable, NCSelect, NCSwitch, NCCheckbox, NCButton } = base;
const NCOption = NCSelect.NCOption;
import { buttonClick, initTemplate, appcode, pagecode,NCAffix } from './events';
import ReferLoader from '../../../public/ReferLoader/index.js';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import {getTableHeight } from '../../../public/common/method.js';
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
let { NCPopconfirm} = base;
const format = 'YYYY-MM-DD';
class WriteOffSetting extends Component {
	constructor(props) {
		super(props);
		this.state={
			json:{},
			accountbookIsdisabled:false,//核算账簿默认可编辑，编辑态不让编辑
			ref1: {}, //核算账簿参照
			addLine: {}, //新增行
			currentRow: {}, //当前选中行
			editRow: {}, //当前编辑行
			editIndex: undefined,//当前编辑行号
			selectedRow: [], //选中行
			searchMap: {},
			tableData: [],
			checkTableData: [], //启用校验表格数据
			canenable: 'N', //启用开关
			showBusColumn: false, //显示业务单元列
			pk_accperiodscheme: "", //期间参照参数
			status_new:"nomal"
		}
		
	}
	componentWillMount() {
		let callback= (json) =>{
			let  that=this;
			
			that.addLine = {
				//辅助核算 ,value必须，多选参照后主键按  主键1,主键2,主键3 这种格式拼装组成字符串
				"assvo": {
					"display": "",
					"value": "",
				},
				//主键 ，新增不传
				// "pk_verifyobj": "",
				//科目名称， 非必录
				"accountname": "",
				//启用期间 ，必录
				"verifyperiod": "",
				//是否严格控制 是Y，否N   必录
				"bcontrol": {
					"display": json['20020VYOBS-000007'],/* 国际化处理： 否*/
					"value": "N"
				},
				//核算账簿主键  必录，查询使用的那个主键
				"pk_accountingbook": "",
				//科目编码  必录，
				"accountcode": {
					"display": "",
					"value": ""
				},
				//核销号必录  是Y，否N  必录
				"hasverifyno": {
					"display": json['20020VYOBS-000007'],/* 国际化处理： 否*/
					"value": "N"
				},
				//启用标志  是Y，否N    必录
				"userflag": {
					"display": json['20020VYOBS-000007'],/* 国际化处理： 否*/
					"value": "N"
				},
				//启用日期
				"begindate": ""
			}
			that.checkColumns = [
				{
					title: (<div fieldid="accountname" className="mergecells">{json['20020VYOBS-000012']}</div>),/* 国际化处理： 科目名称*/
					dataIndex: "accountname",
					key: "accountname",
					className: 'center',
					width: 200,
					render: (text, record, index) => {
						return <div fieldid="accountname">{text==null?<span>&nbsp;</span>:text}</div>
					}
				},
				{
					title: (<div fieldid="assvo" className="mergecells">{json['20020VYOBS-000013']}</div>),/* 国际化处理： 辅助核算*/
					dataIndex: "assvo",
					key: "assvo",
					className: 'center',
					width: 200,
					render: (text, record, index) => {
						return <div fieldid="assvo">{text==null?<span>&nbsp;</span>:text}</div>
					}
				},
				{
					title: (<div fieldid="currtypename" className="mergecells">{json['20020VYOBS-000023']}</div>),/* 国际化处理： 币种*/
					dataIndex: "currtypename",
					key: "currtypename",
					className: 'center',
					width: 150,
					render: (text, record, index) => {
						return <div fieldid="currtypename">{text==null?<span>&nbsp;</span>:text}</div>
					}
				},
				{
					title: json['20020VYOBS-000024'],/* 国际化处理： 未核销余额*/
					className: 'center',
					children: [
						{
							title: (<div fieldid="verifyamount" className="mergecells">{json['20020VYOBS-000025']}</div>),/* 国际化处理： 原币*/
							dataIndex: "verifyamount",
							key: "verifyamount",
							width: 200,
							className: 'right',
							render: (text, record, index) => {
								return <div fieldid="verifyamount">{text==null?<span>&nbsp;</span>:text}</div>
							}
						},
						{
							title: (<div fieldid="localverifyamount" className="mergecells">{json['20020VYOBS-000026']}</div>),/* 国际化处理： 组织本币*/
							dataIndex: "localverifyamount",
							key: "localverifyamount",
							className: 'right',
							width: 200,
							render: (text, record, index) => {
								return <div fieldid="localverifyamount">{text==null?<span>&nbsp;</span>:text}</div>
							}
						},
					]
				},
				{
					title: json['20020VYOBS-000027'],/* 国际化处理： 总账余额*/
					className: 'center',
					children: [
						{
							title: (<div fieldid="generalledgeramount" className="mergecells">{json['20020VYOBS-000025']}</div>),/* 国际化处理： 原币*/
							dataIndex: "generalledgeramount",
							key: "generalledgeramount",
							className: 'right',
							width: 200,
							render: (text, record, index) => {
								return <div fieldid="generalledgeramount">{text==null?<span>&nbsp;</span>:text}</div>
							}
						},
						{
							title: (<div fieldid="localgeneralledgeramount" className="mergecells">{json['20020VYOBS-000026']}</div>),/* 国际化处理： 组织本币*/
							dataIndex: "localgeneralledgeramount",
							key: "localgeneralledgeramount",
							className: 'right',
							width: 200,
							render: (text, record, index) => {
								return <div fieldid="localgeneralledgeramount">{text==null?<span>&nbsp;</span>:text}</div>
							}
						},
					]
				},
				{
					title: (<div fieldid="ifbalance" className="mergecells">{json['20020VYOBS-000028']}</div>),/* 国际化处理： 是否平衡*/
					dataIndex: "ifbalance",
					key: "ifbalance",
					className: 'center',
					width: 120,
					render: (text, record, index) => {
						return <div fieldid="ifbalance">{text==null?<span>&nbsp;</span>:text}</div>
					}
				},
			];
			that.columns = [
				{
					title: (<div fieldid="rowKey" className="mergecells">{json['20020VYOBS-000010']}</div>),/* 国际化处理： 序号*/
					dataIndex: "rowKey",
					key: "rowKey",
					width: 80,
					className :'rowKey',
					render: (text, record, index) => {
						return <div fieldid="rowKey">{index + 1}</div>
					}
				},
				{
					title: (<div fieldid="accountcode" className="mergecells">{json['20020VYOBS-000011']}</div>),/* 国际化处理： 科目编码*/
					dataIndex: "accountcode",
					key: "accountcode",
					width: 150,
					render: (text, record, index) => {
						let {editRow,editIndex,searchMap}=this.state;
						return <div fieldid="accountcode">
							{
								editRow.editable && editIndex === index ?
									<ReferLoader
										fieldid="accountcode"
										showStar={true}
										tag='AccountDefaultGridTreeRef'
										refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
										value={{
											refname: editRow.accountcode && editRow.accountcode.display,
											refpk: editRow.accountcode && editRow.accountcode.value,
										}}
										isMultiSelectedEnabled={false}
										queryCondition={() => {
											let businessDate = getBusinessInfo().businessDate.substring(0,10);
											return {
												'pk_accountingbook': searchMap.pk_accountingbook,
												"isDataPowerEnable": 'Y',
								  "DataPowerOperationCode" : 'fi',
												'dateStr': businessDate
											}
										}}
										onChange={(value) => {
											this.handleTableItemChange('accountcode', { 
												display: value.code || value.refcode || (value.nodeData&&value.nodeData.refcode), 
												value: value.refpk,
											},value)
										}}
									/> 
								: <div fieldid="accountcode">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
							}
						</div>
					}
				},
				{
					title: (<div fieldid="accountname" className="mergecells">{json['20020VYOBS-000012']}</div>),/* 国际化处理： 科目名称*/
					dataIndex: "accountname",
					key: "accountname",
					width: 150,
					render: (text, record, index) => {
						let {editRow,editIndex,searchMap}=this.state;
						return <div fieldid="accountname">
							{
								editRow.editable && editIndex === index ? 
									this.state.editRow.accountname
								: text 
							}
						</div>
					}
				},
				{
					title: (<div fieldid="assvo" className="mergecells">{json['20020VYOBS-000013']}</div>),/* 国际化处理： 辅助核算*/
					dataIndex: "assvo",
					key: "assvo",
					width: 150,
					render: (text, record, index) => {
						let {editRow,editIndex,searchMap}=this.state;
						let defaultValue=[];
						if(editRow.assvo){                           
							editRow.assvo.display.split(",").map((item,_index)=>{
							defaultValue[_index]={ refname: item, refpk:'' };
							})
							editRow.assvo.value.split(",").map((item,_index)=>{
							defaultValue[_index].refpk=item;
							})
						}else{
							defaultValue=[{refname:'',refpk:''}];
						}
						return <div fieldid="assvo">
							{
								editRow.editable && editIndex === index ? 
									<ReferLoader
										fieldid="assvo"
										tag='AccAssItemGridRef'
										refcode='uapbd/refer/fiacc/AccAssItemGridRef/index.js'
										value={defaultValue}
										onChange={(value) =>{
											let multiDisplay = [],multiValue = [];
											value.map(item => {
												multiDisplay.push(item.refname);
												multiValue.push(item.refpk);
											});
											let refVal = {
												display: multiDisplay.join(","),
												value: multiValue.join(",")
											}
											this.handleTableItemChange('assvo', refVal)
										}}
										refType="grid"
										isMultiSelectedEnabled={true}
										queryCondition={{ 
											"pk_accasoa": editRow.accountcode.value, 
											"prepareddate": this.getNowFormatDate(),
											"GridRefActionExt": "nccloud.web.gl.ref.AccAssItemRefSqlBuilder"
										}}
									/>
								:
								<div fieldid="assvo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
							}
						</div>
					}
				},
				{
					title: (<div fieldid="bcontrol" className="mergecells">{json['20020VYOBS-000014']}</div>),/* 国际化处理： 是否严格控制*/
					dataIndex: "bcontrol",
					key: "bcontrol",
					width: 130,
					render: (text, record, index) => {
						let {editRow,editIndex,searchMap}=this.state;
						return <div fieldid="bcontrol">
							{
								editRow.editable && editIndex === index ?
									<NCCheckbox
										fieldid="bcontrol"
										checked={editRow.bcontrol && editRow.bcontrol.value === 'Y'}
										onChange={(val) => {
											let display = val ? json['20020VYOBS-000015'] : json['20020VYOBS-000007'];/* 国际化处理： 是,否*/
											let newVal = val ? 'Y' : 'N';
											this.handleTableItemChange('bcontrol', { display: display, value: newVal })
										}}
									/>
									:
									<div fieldid="bcontrol">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
							}
						</div>
					}
				},
				{
					title: (<div fieldid="verifyperiod" className="mergecells">{json['20020VYOBS-000016']}</div>),/* 国际化处理： 启用期间*/
					dataIndex: "verifyperiod",
					key: "verifyperiod",
					width: 150,
					render: (text, record, index) => {
						let {editRow,editIndex,searchMap}=this.state;
						return <div fieldid="verifyperiod">
							{
								editRow.editable && editIndex === index ?
									<ReferLoader
										fieldid="verifyperiod"
										showStar={true}
										tag='AccPeriodDefaultTreeGridRef'
										refcode='uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index.js'
										value={{
											refname: editRow.verifyperiod,
											refpk: editRow.verifyperiod,
										}}
										onChange={(value) => {
											this.handleTableItemChange('verifyperiod', value.refname, value);
										}}
										refType="gridTree"
										isMultiSelectedEnabled={false}
										queryCondition={{
											pk_accperiodscheme: this.state.pk_accperiodscheme,
											"isadj":"N"
										}}
									/> 
									:
									<div fieldid="verifyperiod">{text==null?<span>&nbsp;</span>:text}</div>
							}
						</div>
					}
				},
				{
					title: (<div fieldid="begindate" className="mergecells">{json['20020VYOBS-000017']}</div>),/* 国际化处理： 启用日期*/
					dataIndex: "begindate",
					key: "begindate",
					width: 150,
					render: (text, record, index) => {
						let {editRow,editIndex,searchMap}=this.state;
						return <div fieldid="begindate">{
							editRow.editable && editIndex === index ?
								this.state.editRow.begindate
								: 
								text
						}</div>	
					}
				},
				{
					title:(<div fieldid="userflag" className="mergecells">{ json['20020VYOBS-000018']}</div>),/* 国际化处理： 启用标志*/
					dataIndex: "userflag",
					key: "userflag",
					width: 120,
					render: (text, record, index,status) => {
						let { editRow, editIndex, searchMap } = this.state;
						let { status_new } = this.state
						return <div fieldid="userflag">
							{/* {status_new=="nomal"?} */}
							{record.userflag.value === 'N' ?
								// status_new=="nomal"?
								(status_new == "nomal" ? <NCPopconfirm
									trigger="click"
									placement="top"
									content={json['20020VYOBS-000019']}	/* 国际化处理： 是否要启用该核销对象？*/
									// 删除该核销对象将会删除您所录入的期初未达数据！是否要继续？									
									onClose={() => {
										this.props.modal.show('enableModal');
										let flag = false
										this.getVerifyCheckData(this.checkBusColumn(), flag);
									}}
									className="click_top"
								>
									<span className='delete-span-span'>
										<NCSwitch
											fieldid="userflag"
											disabled={editRow.editable && editIndex === index}
											checked={record.userflag.value === 'Y'}
											onChange={(val) => this.handleEnableSwitch(text, record, index, val)}
										/>
									</span>
								</NCPopconfirm> : <NCSwitch
										fieldid="userflag"
										disabled={editRow.editable && editIndex === index}
										checked={record.userflag.value === 'Y'}
										onChange={(val) => this.handleEnableSwitch(text, record, index, val)}
									/>) : <NCSwitch
									disabled={editRow.editable && editIndex === index}
									checked={record.userflag.value === 'Y'}
									onChange={(val) => this.handleEnableSwitch(text, record, index, val)}
								/>
							}


						</div>
						
					}
				},
				{
					title: (<div fieldid="hasverifyno" className="mergecells">{json['20020VYOBS-000020']}</div>),/* 国际化处理： 是否核销号必录*/
					dataIndex: "hasverifyno",
					key: "hasverifyno",
					width: 150,
					render: (text, record, index) => {
						let {editRow,editIndex,searchMap}=this.state;
						return <div fieldid="hasverifyno">
							{
								editRow.editable && editIndex === index ?
									<NCCheckbox
										fieldid="hasverifyno"
										checked={editRow.hasverifyno && editRow.hasverifyno.value === 'Y'}
										onChange={(val) => {
											let display = val ? json['20020VYOBS-000015'] : json['20020VYOBS-000007'];/* 国际化处理： 是,否*/
											let newVal = val ? 'Y' : 'N';
											this.handleTableItemChange('hasverifyno', { display: display, value: newVal })
										}}
									/>
									:
									<div  fieldid="hasverifyno">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
							}
						</div>
					}
				},
				{
					title: (<div fieldid="opr" className="mergecells">{json['20020VYOBS-000021']}</div>),/* 国际化处理： 操作*/
					dataIndex: "opr",
					key: "opr",
					width: 150,
					className : "opr",
					render: (text, record, index) => {
						let {editRow,editIndex,searchMap}=this.state;
						return <div className="popment" fieldid="opr">
							{
								editRow.editable && editIndex === index ? ''
								:
									(record.userflag.value=='N')?
									<span className="operate-area">
										{/* <span className="item" onClick={() => this.props.modal.show('delTip')}>删除</span> */}
										{this.props.button.createOprationButton(['delline','modify'],{
											area: 'table_body',
											buttonLimit: 4, 
											onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName,record,index),
										})}
	
									</span>
									:(<span/>)
							}
						</div>
					}
				}
			];
				
			that.setState({
				json:json,
				addLine: JSON.parse(JSON.stringify(that.addLine)), //新增行
				currentRow: JSON.parse(JSON.stringify(that.addLine)), //当前选中行
				editRow: JSON.parse(JSON.stringify(that.addLine)), //当前编辑行

			},()=>{			  
				initTemplate.call(this, this.props);
				this.props.button.setPopContent('delline',json['20020VYOBS-000022']);
			})
		}
		getMultiLang({moduleId:['20020VYOBS','publiccomponents'],domainName:'gl',currentLocale:'simpchn',callback});
		if(this.state.ref1.refpk!=""){
			this.props.button.setButtonDisabled('add',false);
		}
	}
	componentDidMount() {
		let {searchMap,json}=this.state;		
		getDefaultAccountBook(appcode,json).then((defaultAccountBook)=>{
			if(defaultAccountBook.value){
				this.props.button.setButtonDisabled('add',false);
				let accountingbook={refname:defaultAccountBook.display,refpk:defaultAccountBook.value};
				searchMap.pk_accountingbook=defaultAccountBook.value;
				this.queryBizDate(accountingbook,defaultAccountBook.value);//非自然期间
				// this.setState({
				// 	ref1: accountingbook,
				// 	searchMap: searchMap
				// }, () => {
					this.getData(searchMap,(data) => {
						this.setButtonDisabled(true);
						if(this.state.ref1.refpk!=""){
							this.props.button.setButtonDisabled('add',false);
						}
					});
					
				// });
			}
		})
	}

	//切换按钮禁用状态
	setButtonDisabled = (flag) => {
		this.props.button.setButtonDisabled({
			edit: flag,
			delete: flag,
			enable: flag,
			disable: flag
		});
	}

	//查询接口
	getData = (searchMap,callback) => {
		let {  tableData, currentRow } = this.state;
		let data = { "pk_accountingbook": searchMap.pk_accountingbook };

		ajax({
			url: '/nccloud/gl/verify/verifyobjquery.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let selectedRow = [];
					// currentRow = JSON.parse(JSON.stringify(this.addLine));
					currentRow.editable = false;
					if (data) {
						tableData = data;
						selectedRow = new Array(data.length);
					} else {
						tableData = [];
					}
					callback && callback(data);
					this.setState({
						tableData: tableData,
						currentRow,
						selectedRow,
						editIndex: undefined,
						showBusColumn: false,
						accountbookIsdisabled:false
					});
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });
			}
		});
	};

	toggleEditStatus = (type) => {
		let status = type === 'browse';
		//先设置隐藏
		this.props.button.setButtonsVisible({
			add: status,
			edit: status,
			delete: status,
			save: !status,
			cancel: !status,
			enable: status,
			disable: status
		});
	}

	formatData = (data) => {
		let result = [];
		data && data.map((item,index) => {
			item.rowKey = index;
			result.push(item);
		});
		return result;
	}
	
	handleTableItemChange = (key,val,oVal) => {
		let { currentRow } = this.state;
		currentRow[key] = val;
		//选中科目编码时给科目名称赋值
		if (key === 'accountcode'){
			currentRow.accountname = oVal.refname;
			//清空辅助核算
			if(currentRow['assvo']){
				currentRow['assvo'].display='';
				currentRow['assvo'].value='';
			}else{
				currentRow['assvo']={display:'',value:''}
			}
			
		}
		//选中启用期间给启用日期赋开始日期的值
		if (key === 'verifyperiod'){
			currentRow.begindate = oVal.values && oVal.values.begindate.value;
		}
		this.setState({
			currentRow: currentRow
		});
	}

	//保存数据格式
	getSubmitFormData = (type) => {
		let result = type === 'edit' ? JSON.parse(JSON.stringify(this.state.editRow)) : JSON.parse(JSON.stringify(this.state.currentRow));
		if (result.pk_accountingbook === '') result.pk_accountingbook = this.state.searchMap.pk_accountingbook;
		delete result.rowKey;
		delete result.editable;
		return result;
	}

	//表格行点击事件
	handleTableRowClick = (record, index, e) => {
		let { tableData,currentRow } = this.state;
		let selectedRow = new Array(tableData.length);
		selectedRow[index] = true;
		this.setState({
			currentRow: record,
			selectedRow
		});
	}

	//修改
	handleEditClick = (record,index) => {		
		let { currentRow, editRow,accountbookIsdisabled } = this.state;
		if (record.userflag.value === 'Y') {
			toast({ content: this.state.json['20020VYOBS-000008'], color: 'warning' });/* 国际化处理： 该科目已经启用，不能进行修改，取消启用后方可修改！*/
			return false;
		}
		let editIndex = index;//记录修改行号
		this.toggleEditStatus('edit');
		currentRow = record
		editRow = record;
		editRow.editable = true;
		this.setState({
			currentRow,
			editRow,
			editIndex,
			accountbookIsdisabled:true,
			status_new:"edit"
		});
	}

	//删除
	beSureDelClick = (record) => {
		let { currentRow,searchMap } = this.state;
		//let param = { pk_accountingbook: currentRow.pk_accountingbook, pk_verifyobj: currentRow.pk_verifyobj };
		let param = { pk_accountingbook: record.pk_accountingbook, pk_verifyobj: record.pk_verifyobj };
		ajax({
			url: '/nccloud/gl/verify/verifyobjdelete.do',
			data: param,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					this.getData(searchMap);
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });
			}
		});
	}
	
	//切换启用开关
	handleEnableSwitch = (text,record,index,status) => {		
		let { currentRow,searchMap } = this.state;
		currentRow = record;
		this.setState({
			currentRow
		},() => {
			//启用
			if (status) {
				//this.props.modal.show('enableTip');								
			}else{ //取消启用
				let param = this.getSubmitFormData();
				ajax({
					url: '/nccloud/gl/verify/verifyobjdisable.do',
					data: param,
					success: (res) => {
						let { success, data } = res;
						if (success) {
							this.getData(searchMap);
						}
					},
					error: function (res) {
						toast({ content: res.message, color: 'danger' });
					}
				});
			}
		});
	}
	//启用校验
	getVerifyCheckData = (callback,flag) => {
		let param = this.getSubmitFormData();
		ajax({
			url: '/nccloud/gl/verify/verifyobjcheck.do',
			data: param,
			success: (res) => {
				let { success, data } = res;
				let { checkTableData } = this.state;
				if (success) {
					if (data.checkvos){
						checkTableData = data.checkvos;
					}else{
						checkTableData = [];
					}
					this.setState({
						checkTableData,
						canenable: data.canenable
					},() => {
						callback && callback(data);
					});
					
				}
				if(flag==undefined){
					toast({ content: this.state.json['20020VYOBS-000037'], color: 'success' }); /*刷新成功*/
				}

			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });
			}
		});
	}

	//启用ajax
	verifyEnable = () => {
		let {searchMap}=this.state;
		let param = this.getSubmitFormData();
		ajax({
			url: '/nccloud/gl/verify/verifyobjenable.do',
			data: param,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					this.getData(searchMap);
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });
			}
		});
	}

	//启用检查ajax
	checkBusColumn = () => {
		ajax({
			url: '/nccloud/gl/verify/verifyobjcheckbu.do',
			data: { pk_accountingbook: this.state.searchMap.pk_accountingbook},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if(data === 'Y'){
						this.setState({
							showBusColumn: true
						});
					}
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });
			}
		});
	}

	//启用检查columns
	createCheckColumns = (columns) => {
		let { showBusColumn } = this.state;
		let busColumn = {
			title: this.state.json['20020VYOBS-000009'],/* 国际化处理： 业务单元*/
			dataIndex: "unitname",
			key: "unitname",
			width: 150,
			className: 'center'
		}
		return showBusColumn ? [busColumn,...columns] : columns;
	}
    getTableHeightPosi = () => {
	  let tableHeight=document.getElementById('app').offsetHeight-92;
        return tableHeight;
     }
	getNowFormatDate = () => {
		let date = new Date();
		let seperator1 = "-";
		let year = date.getFullYear();
		let month = date.getMonth() + 1;
		let strDate = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		let currentdate = year + seperator1 + month + seperator1 + strDate;
		return currentdate;
	}

	//非自然期间
	queryBizDate = (valueObj,pk_accountingbook) => {
		let {searchMap}=this.state;
		searchMap.pk_accountingbook = pk_accountingbook;
		ajax({
			url: '/nccloud/gl/glpub/queryBizDate.do',
			data: { pk_accountingbook },
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if(data){
						this.setState({
							ref1: valueObj,
							searchMap: searchMap,
							pk_accperiodscheme: data.pk_accperiodscheme
						});
					}
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });
			}
		});
	}
	handleLoginBtn = (obj, btnName,record,index) =>{	
		if(btnName == 'modify'){
			this.handleEditClick(record,index)
		}else if(btnName == 'delline'){
			
			this.beSureDelClick(record)
		}
	}
	// getTableHeight = () => {
    //     let tableHeight=document.getElementById('app').offsetHeight-250;
    //     return tableHeight;
    // }
	render() {
		let { table, button, search, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let { createModal } = modal;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		let { currentRow, searchMap, tableData, selectedRow, editIndex, editRow,ref1,accountbookIsdisabled,json } = this.state;
		return (
			<div className="nc-bill-list" id="writeOffSettingMain">
				<HeaderArea
					title={this.state.json['20020VYOBS-000036']}/* 国际化处理： 核销对象设置*/
					searchContent = {
						<div className="header-panel">
							<div className="content" style={{ width: 200 }}>
								<ReferLoader
									fieldid="pk_accountingbook"
									tag='AccountBookTreeRef'
									refcode='uapbd/refer/org/AccountBookTreeRef/index.js'
									value={ref1}
									isMultiSelectedEnabled={false}
									disabled={accountbookIsdisabled}
									disabledDataShow={true}
									queryCondition={{
										TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
										appcode
									}}
									onChange={(value) => {
										if(value.refpk){
											this.props.button.setButtonDisabled('add',false);
										}else{
											this.props.button.setButtonDisabled('add',true);
										}
										searchMap.pk_accountingbook = value.refpk;
										this.queryBizDate(value,value.refpk);//非自然期间
										this.setState({
											ref1: value,
											searchMap: searchMap
										}, () => {
											
											this.getData(searchMap,(data) => {
												this.setButtonDisabled(!data);
											});
										});

									}}
								/>
							</div>
						</div>
					}
					btnContent={
						this.props.button.createButtonApp({
							area: '20020VYOBS',
							buttonLimit: 4, 
							onButtonClick: (props, key) => buttonClick(props, key, this),
							popContainer: document.querySelector('.header-button-area')
						})
					}
				/> 
				<div />
				<div className="nc-bill-table-area" fieldid="table_area">
					<div fieldid="writeOff_table">
					<NCTable
						columns={this.columns?this.columns:[]}
						data={this.formatData(tableData)}
						rowKey='rowKey'
						onRowClick={this.handleTableRowClick}
						rowClassName={(record, index, indent) => {
							if (this.state.selectedRow[index]) {
								return 'nctable-selected-row';
							} else {
								return '';
							}
						}}
						bodyStyle={{height:getTableHeight(80)}}
						scroll={{
							x: true,
							y: getTableHeight(80)
						}}
					/>
					</div>
				</div>
				{createModal('delTip', {
					title: this.state.json['20020VYOBS-000029'],// 弹框表头信息/* 国际化处理： 提示*/
					content: this.state.json['20020VYOBS-000030'], //弹框内容，可以是字符串或dom/* 国际化处理： 删除该核销对象将会删除您所录入的期初未达数据！是否要继续？*/
					beSureBtnClick: this.beSureDelClick.bind(this)  //点击确定按钮事件
				})}
				{createModal('enableTip', {
					className:"modalone",
					title: this.state.json['20020VYOBS-000029'],// 弹框表头信息/* 国际化处理： 提示*/
					content: this.state.json['20020VYOBS-000019'], //弹框内容，可以是字符串或dom/* 国际化处理： 是否要启用该核销对象？*/
					beSureBtnClick: () => {
						this.props.modal.show('enableModal');
						let flag=false
						this.getVerifyCheckData(this.checkBusColumn(),flag);
					}  //点击确定按钮事件
				})}
				



				<span>
				{
					this.state.json['20020VYOBS-000032']&&
					createModal('enableModal', {
					title: this.state.json['20020VYOBS-000031'],// 弹框表头信息/* 国际化处理： 检查*/
					content: <NCTable
						columns={this.createCheckColumns(this.checkColumns)}
						data={this.formatData(this.state.checkTableData)}
						rowKey='rowKey'
						scroll={{
							x: true,
							y: '400px'
						}}
					/>, //弹框内容，可以是字符串或dom
					// className:"checkoff",
					size:'xlg',
					userControl:true, // 点确定按钮后，不自动关闭模态框
					leftBtnName: this.state.json['20020VYOBS-000032'], //左侧按钮名称， 默认确认/* 国际化处理： 刷新*/
					rightBtnName: this.state.json['20020VYOBS-000033'], //右侧按钮名称,默认关闭/* 国际化处理： 返回*/
					beSureBtnClick: this.getVerifyCheckData,  //点击确定按钮事件
					closeModalEve: () => {
						this.props.modal.close('enableModal');
						if (this.state.canenable === 'Y') this.verifyEnable();
					}, //关闭按钮事件回调
					cancelBtnClick: () => {
						this.props.modal.close('enableModal');
						if (this.state.canenable === 'Y') this.verifyEnable();
					}
				})}
				</span>
			</div>
		);
	}
}

WriteOffSetting = createPage({})(WriteOffSetting);

ReactDOM.render(<WriteOffSetting />, document.querySelector('#app'));
