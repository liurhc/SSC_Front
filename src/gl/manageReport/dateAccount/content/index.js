/**
 * Created by liqiankun on 2018/7/18.
 * 账簿查询 ---> 日记账
 */

import React, {Component} from 'react';

import {high, base, ajax, createPage, getMultiLang,createPageIcon} from 'nc-lightapp-front';
const { NCDiv } = base;
import '../../css/index.less'
import HeadCom from '../../common/headSearch/headCom';
import { SimpleTable } from 'nc-report';
import QueryModal from './components/MainSelectModal';

import {tableDefaultData} from '../../defaultTableData';
import reportPrint from '../../../public/components/reportPrint';
import { toast } from '../../../public/components/utils';
import {relevanceSearch} from "../../referUrl";
import {getDetailPort} from "../../common/modules/modules";
const { PrintOutput } = high;
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import {setData} from "../../common/withPageDataSimbletable";
import {searchById} from "../../common/modules/createBtn";
import "../../../public/reportcss/firstpage.less";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import PageButtonGroup from '../../common/modules/pageButtonGroup'
import {rowBackgroundColor} from "../../common/htRowBackground";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
class DateAccount extends Component{
    constructor(props){
        super(props);
        this.subjectList = null;
        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            disabled: true,
            visible: false,
            appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            outputData: {},
            ctemplate: '', //模板输出-模板
            nodekey: '',
            printParams:{}, //查询框参数，打印使用
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
            textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            dataWithPage: [], //存放请求回来的数据信息，用于分页操作
            firstPageDisable: true,
            lastPageDisable: true,
            paramObj: {},//存放回调的查询参数，用于翻页
            codes: [],
            accountType: 'amountcolumn',//金额式
            assistList: '',//头部科目列表
            subjectListSingleValue: '',
            'dataAccount': [],
            'dataAccount2': [],
            subjname: '',//表头科目名称
            defaultAccouontBook: {} //默认账簿
        }
        this.pageType = 'nextPage';
        this.dataPage = 0; //存放请求回来的数据椰树信息
        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.handlePage = this.handlePage.bind(this);//页面处理事件
        // this.setData= this.setData.bind(this);// 整理表格所需要的数据格式
        this.queryData = this.queryData.bind(this);//请求数据的方法；
        this.handleValueChange = handleValueChange.bind(this);
        // this.changeSelectStyle = this.changeSelectStyle.bind(this);//选择 '账簿格式'事件
        this.searchById = searchById.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                'dataAccount': [
                    {
                        title: json['20023035-000047'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20023035-000048'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023035-000049'],
                        styleClass:"m-brief"
                    },

                    {
                        title: json['20023035-000050'],
                        styleClass:"m-brief"
                    }
                ],
                'dataAccount2': [
                    json['20023035-000051'], json['20023035-000052']
                ],
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023035', 'publiccommon', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        this.setState({
            flag: true,
        });
        let appceod = this.props.getSearchParam('c');
        this.searchById('20023035PAGE', appceod).then((res) => {
            if (param && res && res.data.context) {
                let { context } =res.data
                this.state.defaultAccouontBook = {refname:context.defaultAccbookName,refpk:context.defaultAccbookPk, getflag: true}
            }
        });
        this.props.button.setDisabled({
            print: true, linkvoucher: true,
            printexcel: true, saveformat: true, printoutput: true,
            first: true, pre: true, next: true, last: true,
        })
        this.props.button.setButtonVisible(['first', 'pre', 'next', 'last'], false)
    }
    showPrintModal() {
        this.setState({
            visible: true
        })
    }
    handlePrint(data, isPreview) {
        let printUrl = '/nccloud/gl/accountrep/diarybooksprint.do'
        let { printParams, appcode } = this.state
        let { ctemplate, nodekey } = data
        printParams.currpage = this.dataPage.toString() //当前页
        printParams.queryvo = data
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
        printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
        this.handleCancel()
    }
    handleCancel() {
        this.setState({
            visible: false
        });
    }
    showOutputModal() {
        // this.refs.printOutput.open()
        let outputUrl = '/nccloud/gl/accountrep/diarybooksoutput.do'
        let { appcode, nodekey, ctemplate, printParams } = this.state
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)
        // let outputData = mouldOutput(appcode, nodekey, ctemplate, printParams)
        this.setState({
            outputData: outputData
        })
    }
    handleOutput() {

    }

    /**
     * param: 'firstPage, nextPage, prePage, lastPage;
     * dataWithPage: 总数据
     * paramObj
     * */
    handlePage(param, dataWithPage){//页面控制事件
        let {paramObj, codes} = this.state;
        let dataLength = codes.length;
        let renderData = [];
        let obj = {};
        let stringPage = '1';
        switch(param){
            case 'firstPage':
                this.dataPage = 0;
                this.setState({
                    paramObj,
                    firstPageDisable: true,
                    lastPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    first: true, pre: true,
                    next: false, last: false,
                })
                break;
            case 'nextPage':
                this.dataPage += 1;
                if(this.dataPage === dataLength){
                    return;
                }

                this.setState({
                    paramObj,
                    firstPageDisable: false,//可以点击
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    first: false, pre: false
                })
                if(this.dataPage === dataLength-1){
                    this.setState({
                        lastPageDisable: true
                    })
                    this.props.button.setDisabled({
                        next: true, last: true,
                    })
                }
                break;
            case 'prePage':
                this.dataPage -=1;
                if(this.dataPage < 0){
                    return;
                }
                if(this.dataPage === dataLength-2){
                    this.setState({
                        lastPageDisable: false
                    })
                    this.props.button.setDisabled({
                        next: false, last: false,
                    })
                }
                if(this.dataPage === 0){
                    this.setState({
                        firstPageDisable: true
                    });
                    this.props.button.setDisabled({
                        first: true, pre: true
                    })
                }
                this.setState({
                    paramObj,
                    selectRowIndex: 0
                })
                break;
            case 'lastPage':
                this.dataPage = dataLength-1;
                this.setState({
                    paramObj,
                    lastPageDisable: true,
                    firstPageDisable: false,
                    selectRowIndex: 0
                });
                this.props.button.setDisabled({
                    first: false, pre: false,
                    next: true, last: true,
                })
            }
        paramObj.pageindex = String(this.dataPage);
        paramObj.operation = param;
        this.queryData(this.state.paramObj);
        return;
    }
    queryClick(){//点击"查询"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }

	/**
     * param:请求条件对象
     *
     * sureBtn: 判断是点击'查询'按钮进来的还是点击翻页进来的
     * 如果点击查询进来的说明又从新请求了，一切要归零
     * */
    queryData(param, sureBtn){//请求数据方法；
        let self = this;
        let url = '/nccloud/gl/accountrep/diarybooksquery.do';
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, success } = response;
                if(success && data){
                    self.props.button.setDisabled({
                        print: false, printexcel: false,
                        saveformat: false, printoutput: false,
                    })
                    if(data.data.pagedata.length>0){
                        self.setState({
                            disabled:false,//有数据时打印，输出 等按钮可用
                            typeDisabled: false,//账簿格式 默认是禁用的
                            selectRowIndex:0
                        })
                    }
                    if (self.dataPage === 0 && self.state.codes.length === 0) {
                        self.setState({
                            codes: [...data.codes]
                        })
                    }
                    self.dataPage = Number(data.accountindex);
                    let firstDis = (Number(data.accountindex) === 0);
                    let lastDis = (Number(data.accountindex) === self.state.codes.length - 1);
                    self.setState({
                        dataWithPage: data,
                        firstPageDisable: firstDis,
                        lastPageDisable: lastDis,
                        subjectListSingleValue: data.accounts[data.accountindex],
                        subjname: data.headtitle.subjname
                    })
                    self.props.button.setDisabled({
                        first: firstDis, pre: firstDis,
                        next: lastDis, last: lastDis,
                    })
                    
                    let renderFirstData = {};
                    renderFirstData.column = data.column;
                    renderFirstData.data = data.data;
                    renderFirstData.amountcolumn = data.amountcolumn;
                    renderFirstData.quantityamountcolumn = data.quantityamountcolumn;
                    setData(self, renderFirstData);

                }else{
                    toast({content: this.state.json['20023035-000001'], color: 'danger'});/* 国际化处理： 请求数据为空！*/
                }
            },
            error:function(error){
                toast({content: error.message, color: 'danger'});
                throw error
            }
        })
    }
    modalSure(param){//点击弹框"确定"事件
        this.subjectList = document.getElementsByClassName('subjectList');
        this.dataPage = 0;
        param.pageindex = String(this.dataPage);
        param.operation = 'nextPage';
        this.setState({
            paramObj: {...param},
            printParams: {...param},
            codes: []
        })
        this.queryData(param, 'sureClick');
    }

    //直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    handleSaveColwidth=()=>{
        let info=this.refs.balanceTable.getReportInfo();
        let {json}=this.state
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.appcode,info.colWidths,json, callBack);
    }
    handleSubjectList = (key, value) => {
        let {paramObj, codes} = this.state;
        let newCode = value.split(' ')[0];
        this.dataPage = codes.indexOf(newCode);
        if(this.dataPage > 0){
            this.setState({
                firstPageDisable: false
            })
            this.props.button.setDisabled({
                first: false, pre: false
            })
        }else{
            this.setState({
                firstPageDisable: true,
                lastPageDisable: false
            })
            this.props.button.setDisabled({
                first: true, pre: true,
                next: false, last: false,
            })
        }
        if(this.dataPage > codes.length - 2){
            this.setState({
                lastPageDisable: true
            })
            this.props.button.setDisabled({
                next: true, last: true,
            })
        }
        paramObj.pageindex = String(this.dataPage);
        delete paramObj.operation;
        this.queryData(paramObj);
        // this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
    }
    handleLoginBtn = (obj, btnName) => {
        if(btnName === 'print'){//1、打印
            this.showPrintModal()
        }else if(btnName === 'linkvoucher'){//5、联查凭证
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: relevanceSearch,
                key: 'relevanceSearch',
                appcode: '20023030'
            }, this.state.json)
        }else if(btnName === 'printexcel'){//4、直接输出
            this.printExcel()
        }else if(btnName === 'saveformat'){//3、保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'printoutput'){//2、模板输出
            this.showOutputModal()
        }else if(btnName === 'first'){//4首页
            this.handlePage('firstPage', this.state.dataWithPage)
        }else if(btnName === 'pre'){//5上一页
            this.pageType = 'prePage';
            this.handlePage('prePage', this.state.dataWithPage)
        }else if(btnName === 'next'){//6下一页
            this.pageType = 'nextPage';
            this.handlePage('nextPage', this.state.dataWithPage)
        }else if(btnName === 'last') {//7末页
            this.handlePage('lastPage', this.state.dataWithPage)
        }else if(btnName === 'refresh'){//刷新
            if(Object.keys(this.state.paramObj).length>0){
                this.modalSure(this.state.paramObj);
            }
        }
    }
    firstCallback = () => {//首页
        this.handlePage('firstPage', this.state.dataWithPage)
    }
    preCallBack = () => {//上一页
        this.pageType = 'prePage';
        this.handlePage('prePage', this.state.dataWithPage)
    }
    nextCallBack = () => {//下一页
        this.pageType = 'nextPage';
        this.handlePage('nextPage', this.state.dataWithPage)
    }
    lastCallBack = () => {//末页
        this.handlePage('lastPage', this.state.dataWithPage)
    }

    render(){
        const { cardPagination } = this.props;
        const { createCardPagination } = cardPagination;
        let { modal } = this.props;
        const { createModal } = modal;
        return(
            <div className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['20023035-000002']} /* 国际化处理： 日记账*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <QueryModal
                                    defaultAccouontBook={this.state.defaultAccouontBook}
                                    onConfirm={(datas) => {
                                        this.modalSure(datas);
                                    }}
                                />
                            </div>
                            {//按钮集合
                                this.props.button.createButtonApp({
                                    area: 'btnarea',
                                    onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                                })
                            }
                        </div>
                    }
                    pageBtnContent = {
                        <PageButtonGroup
                            first={{disabled: this.state.firstPageDisable, callBack: this.firstCallback}}
                            pre={{disabled: this.state.firstPageDisable, callBack: this.preCallBack}}
                            next={{disabled: this.state.lastPageDisable, callBack: this.nextCallBack}}
                            last={{disabled: this.state.lastPageDisable, callBack: this.lastCallBack}}
                        />
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.dataAccount.map((items) => {
                                return(
                                    <HeadCom
                                        content={this.state.dataWithPage.data}
                                        lastThre = {this.state.dataWithPage && this.state.dataWithPage.headtitle}
                                        subjectListSingleValue={this.state.subjname}
                                        labels={items.title}
                                        key={items.title}
                                        isLong = {items.styleClass}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                        {
                            this.state.dataAccount2.map((items) => {
                                return(
                                    <HeadCom
                                        ref='HeadCom'
                                        className='subjectList'
                                        content={this.state.dataWithPage.data}
                                        lastThre = {this.state.dataWithPage && this.state.dataWithPage.headtitle}
                                        labels={items}
                                        key={items}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
                                        }
                                        handleSubjectList = {
                                            (key, value) => {
                                                this.handleSubjectList(key, value);
                                            }
                                        }
                                        accountType = {this.state.accountType}
                                        assistList={this.state.assistList}
                                        dataPage = {this.dataPage}
                                        subjectListSingleValue={this.state.subjectListSingleValue}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data = {this.state.dataout}
                        onCellMouseDown = {(e, coords, td) => {
                            this.whetherDetail('link', 'multiAccountSearch');
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
                </div>

                <RepPrintModal
                    appcode={this.state.appcode}
                    visible={this.state.visible}
                    showScopeall={true}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={this.handleCancel.bind(this)}
                />
                <PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/accountrep/diarybooksoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
                {createModal('printService', {
                    className: 'print-service'
                })}
                <Iframe />
            </div>
        )
    }
}


DateAccount = createPage({})(DateAccount)

ReactDOM.render(<DateAccount />,document.querySelector('#app'));
