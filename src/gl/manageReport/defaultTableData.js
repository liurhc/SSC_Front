/**
 *   Created by Liqiankun on 2018/7/19
 */

export let tableDefaultData = {data:{
    checkTableHeadNum:'0',
        cells : [
            [
                {title: "", key: "acccode", align: "center", bodyAlign: "left", style: "head"},
                {title: "", key: "accname", align: "center", bodyAlign: "left", style: "head"},
                {title: "", key: "currtype", align: "center", bodyAlign: "left", style: "head"},
                {title: "", key: "initorint", align: "center", bodyAlign: "left", style: "head"},

                {title: "", key: undefined, align: "center", bodyAlign: "left", style: "head"},

                {title: "", key: undefined, align: "center", bodyAlign: "left", style: "head"},

                {title: "", key: undefined, align: "center", bodyAlign: "left", style: "head"},

                {title: "", key: undefined, align: "center", bodyAlign: "left", style: "head"},

                {title: "", key: undefined, align: "center", bodyAlign: "left", style: "head"},

                {title: "", key: "endorint", align: "center", bodyAlign: "left", style: "head"},

                {title: "", key: undefined, align: "center", bodyAlign: "left", style: "head"}
            ]
        ],
        mergeInfo:[
            [0, 0, 0, 0],
            [0, 1, 0, 1],
            [0, 2, 0, 2],
            [0, 3, 0, 3],
            [0, 4, 0, 4],
            [0, 5, 0, 5],
            [0, 6, 0, 6],
            [0, 7, 0, 7],
            [0, 8, 0, 8],
            [0, 9, 0, 9],
            [0,10, 0, 10]
        ],
        cell : [],
        cellsFn:{},
        rowHeaders: true,
        manualColumnResize:true,
        readOnly:true,
        columnSorting: true, // 排序功能
        wordWrap:false,
        colHeaders:true,
        autoColumnSize:false,
        autoRowSize:false,
        // minCols:50,
    }}