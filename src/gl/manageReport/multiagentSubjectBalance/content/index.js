/**
 * Created by liqiankun on 2018/6/22.
 * 管理报表 ---> 多主题科目余额表
 */
import React, {Component} from 'react';
import { base, ajax, createPage, getMultiLang, gzip } from 'nc-lightapp-front';
const { NCDiv } = base;
import '../../css/index.less'
import { SimpleTable } from 'nc-report';

import QueryModal from './MainSelectModal';

import {tableDefaultData} from '../../defaultTableData';
import reportPrint from '../../../public/components/reportPrint';
import {toast} from "../../../public/components/utils";
import HeadCom from '../../common/headSearch/headCom';
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import{searchById} from "../../common/modules/createBtn";
import {handleSimpleTableClick} from "../../common/modules/simpleTableClick";
import "../../../public/reportcss/firstpage.less";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import {rowBackgroundColor} from "../../common/htRowBackground";
import HeaderArea from '../../../public/components/HeaderArea';
import { queryRelatedAppcode, originAppcode } from '../../../public/common/queryRelatedAppcode.js';
class MultiagentSubjectBalance extends Component{
    constructor(props){
        super(props);
        this.state = {
            json: {},
            inlt:null,
            typeDisabled: true,//账簿格式 默认是禁用的
            appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            showAddBtn: false, //联查按钮是否禁止；true:禁止
            showTable: false,//控制是否显示表格
            flow: false,//控制表头是否分级
			showModal: false, //控制弹框显示
			textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            paramObj: {},
            accountType: 'amountcolumn',//金额式
            selectRow: [],
            "fourLables": []
        }

        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.changeFormat = this.changeFormat.bind(this);//账簿格式:选择事件
        this.handleValueChange = handleValueChange.bind(this);
        this.searchById = searchById.bind(this);
        this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
    }

    componentWillMount() {
        let callback= (json,status,inlt) =>{
            this.setState({
                json:json,
                inlt,
                "fourLables": [
                    {
                        title: json['20028002-000049'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20028002-000050'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20028002-000051'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20028002-000052'],
                        styleClass:"m-brief"
                    }
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20028002', 'publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        this.setState({
          flag: true,
        });
        let appceod = this.props.getSearchParam('c');
        this.searchById('20028002PAGE',appceod);
        this.props.button.setDisabled({
            print:true, directprint:true,
            assbal:true,
            detailbook:true, saveformat:true
        });
    }

    queryClick = () => {//点击查询, 弹框的"关闭"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    changeFormat(value){//账簿格式:选择事件
    	//('value>', value);
    }

    modalSure(params){//点击弹框"确定"的事件
        //('params::', params);
        this.setState({
            paramObj: {...params}
		})
	    this.ajaxData(params)
    }
    ajaxData = (params) => {
        this.props.button.setDisabled({
            print:false, directprint:false,
            saveformat:false,
        });
        let url = '/nccloud/gl/accountrep/accbalquery.do';
        let self = this;
        ajax({
            url:url,
            data:params,
            success:function(response){
                toast({
                    //content: `查询成功，共${response.data.data.length}条数据`, color: 'success'
                   content: self.state.inlt && self.state.inlt.get('20028002-000054',{len : response.data.data.length}), color: "success" 
                })
                let { data, success } = response;
                let num = 1;
                let numArr = [];
                //('sourceData:', data);
                // self.recursionFn(data.column);
                self.setState({
                    showAddBtn: false,
                    typeDisabled: false,//账簿格式 默认是禁用的
                    dataWidthPage: data,
                    selectRowIndex:0
                })
                if(success){
                    self.setData(data)
                }
            },
            error:function(error){
                //('uuuu:', self, this,  error);
                toast({ content: error.message, color: 'warning' });
            }
        })
	}

    setData = (data) => {

        //('resDARA:', data);
        let rows = [];   //表格显示的数据合集
        let columHead = []; //第一层表头
        let parent = []; //存放一级表头，二级表头需要补上
        let parent1 = []; //存放二级表头，三级表头需要补上
        let child1 = []; //第三层表头
        let child2 = []; //第二层表头
        let colKeys = [];  //列key值，用来匹配表体数据
        let colAligns=[];  //每列对齐方式
        let colWidths=[];  //每列宽度
        let oldWidths=[];
        let textAlginArr = this.state.textAlginArr.concat([]);      //对其方式
        data[this.state.accountType].forEach( (value) => {//column:[]原始数据表头信息
            //('>>>>////', value, this);
            let valueCell = {};
            valueCell.title = value.title;
            valueCell.key = value.key;
            valueCell.align = 'center';
            valueCell.bodyAlign = 'left';
            valueCell.style = 'head';
            if(value.children){
                this.setState({
                    flow: true
                })
                if(parent.length>0){
                    child1.push(...parent);
                }
                for(let i=0;i<value.children.length;i++){
                    if(value.children[i].children){
                        child2.push(...parent1);
                        for(let k=0;k<value.children[i].children.length;k++){
                            let child1Cell = {};
                            child1Cell.title = value.children[i].title;
                            child1Cell.key = value.children[i].key;
                            child1Cell.align = 'center';
                            child1Cell.bodyAlign = 'left';
                            child1Cell.style = 'head';
                            let child2Cell = {};
                            child2Cell.title = value.children[i].children[k].title;
                            child2Cell.key = value.children[i].children[k].key;
                            child2Cell.align = 'center';
                            child2Cell.bodyAlign = 'left';
                            child2Cell.style = 'head';
                            columHead.push(valueCell);
                            child1.push(child1Cell);
                            child2.push(child2Cell);
                            colKeys.push(value.children[i].children[k].key);
                            colAligns.push(value.children[i].children[k].align);
                            colWidths.push({'colname':value.children[i].children[k].key,'colwidth':value.children[i].children[k].width});
                        }
                        parent1 = [];
                        //('jjjjjjj:', colWidths)
                    }else {
                        let innerCell2 = {};
                        innerCell2.title = value.children[i].title;
                        innerCell2.key = value.children[i].key;
                        innerCell2.align = 'center';
                        innerCell2.bodyAlign = 'left';
                        innerCell2.style = 'head';
                        columHead.push(valueCell);
                        child1.push(innerCell2);
                        parent1.push(innerCell2);
                        colKeys.push(value.children[i].key);
                        colAligns.push(value.children[i].align);
                        colWidths.push({'colname':value.children[i].key,'colwidth':value.children[i].width});
                    }
                }
                parent = [];
            }else{
                let cellObj = {};
                cellObj.title = value.title;
                cellObj.key = value.key;
                cellObj.align = 'center';
                cellObj.bodyAlign = 'left';
                cellObj.style = 'head';
                columHead.push(valueCell);
                parent1.push(cellObj);
                parent.push(cellObj);
                colKeys.push(value.key);
                colAligns.push(value.align);
                colWidths.push({'colname':value.key,'colwidth':value.width});
                //('elses::', parent, parent1)
            }
        });
        //('>>>>?????',parent1, parent, child1, child2)
        let columheadrow = 0; //表头开始行
        // if (data.headtitle){
        //     columheadrow = 1;
        //     let headtitle = [];
        //     data.headtitle.forEach(function (value) {
        //         headtitle.push(value[0]);
        //         headtitle.push(value[1]);
        //     });
        //     for(let i=headtitle.length;i<columHead.length;i++){
        //         headtitle.push('');
        //     }
        //     rows.push(headtitle);
        // }

        let mergeCells = [];
        let row,col,rowspan,colspan;
        let headcount = 1; //表头层数
        if(child1.length>0){
            headcount++;
        }
        if(child2.length>0){
            headcount++;
        }
        let currentCol = 0;
        //('length::',rows.length, data.data.length)
        //计算表头合并格
        for(let i=0;i<data[this.state.accountType].length;i++) {
            let value = data[this.state.accountType][i];
            //*********
            if(value.children){//有子元素的
                let childLeng = value.children.length;
                mergeCells.push([columheadrow, currentCol, columheadrow, currentCol + childLeng -1 ]);
                //('chuldLeng', childLeng, mergeCells)

                let headCol = currentCol;
                for(let i=0;i<value.children.length;i++){

                    //('ccchildhdh', value.children[i].children);
                    let childlen = 0;
                    if(value.children[i].children){

                        let childlen = value.children[i].children.length;
                        currentCol = currentCol+childlen;
                        for(let k=0;k<value.children[i].children.length;k++){
                            textAlginArr.push(value.children[i].children[k].align);
                        }
                    }else if(childlen > 0){//子元素里面没有子元素的
                        //('childlen<<', childlen)
                        mergeCells.push([columheadrow+1, currentCol, columheadrow+2, currentCol]);//cell
                        currentCol++;
                        //('chuldLengif', childLeng, mergeCells)
                    }else {
                        currentCol++;
                        textAlginArr.push(value.children[i].align);
                    }
                }
            }else{//没有子元素对象
                mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
                currentCol++;
                textAlginArr.push(value.align);
                //('currentCol', currentCol, mergeCells)
            }
        }
        //('mergeCells>>',this, mergeCells)
        if(parent.length>0){
            child1.push(...parent);
        }
        if(child2.length>0&&parent1.length>0){
            child2.push(...parent1);
        }

        rows.push(columHead);
        //('rrrrrrcolumHead:', rows, child1, child2,this.state.flow);
        if(child1.length>0 && this.state.flow){
            rows.push(child1);
        }
        if(child2.length>0 && this.state.flow){
            rows.push(child2);
        }
        //('rows>>>', rows);
        if(data!=null && data.data.length>0){
            //('colKeys>>>', colKeys);
            let rowdata = [];
            let flag = 0;
            for(let i=0;i<data.data.length;i++){
                flag++;
                for(let j=0;j<colKeys.length;j++){
                    //('data.data[i][colKeys[j]>', data.data[i], data.data[i][colKeys[j]]);
                    // {
                    //     "title":"11163",
                    //     "key":"pk_org",
                    //     "align":"left",
                    //     "style":"body"
                    // }
                    let itemObj = {
                        align: 'right',
                        style: 'body'
                    };
                    itemObj.title = data.data[i][colKeys[j]];
                    itemObj.key = colKeys[j] + flag;
                    itemObj.link = data.data[i].link;
                    itemObj.linkmsg = data.data[i].linkmsg;
                    itemObj.align = colAligns[j];
                    if (data.data[i].hasAss) {
                        itemObj.style = 'glRowColor';
                        if (data.data[i].hasAss === 'true') {
                            itemObj.color = '#1C7ED9'
                        }
                    }
                    // rowdata.push(data.data[i][colKeys[j]])
                    rowdata.push(itemObj)
                }
                rows.push(rowdata);
                rowdata = [];
            }
        }
        //('rowdata>>>', rows);
        let rowhighs = [];
        for (let i=0;i<rows.length;i++){
            rowhighs.push(23);
        }
        this.state.dataout.data.cells= rows;//存放表体数据
        //('settings.cells', this.state.dataout.data.cells)
        this.state.dataout.data.widths =colWidths;
        oldWidths.push(...colWidths);
        this.state.dataout.data.oldWidths = oldWidths;
        this.state.dataout.data.rowHeights=rowhighs;
        let frozenCol;
        for(let i=0;i<colAligns.length;i++){
            if(colAligns[i]=='center'){
                frozenCol = i;
                break;
            }
        }
        // this.state.dataout.data.fixedColumnsLeft = 0; //冻结
        // this.state.dataout.data.fixedRowsTop = 0; //冻结
        this.state.dataout.data.fixedColumnsLeft = 0; //冻结
        this.state.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结
        this.state.dataout.data.freezing = {//固定表头
            "isFreeze" : true,
            "row": this.state.dataout.data.fixedRowsTop,
            "col" : this.state.dataout.data.fixedColumnsLeft
        }
        //('>>>', this.state.dataout.data.fixedRowsTop)
        let quatyIndex = [];
        for (let i=0;i<colKeys.length;i++){
            if(colKeys[i].indexOf('quantity')>0){
                quatyIndex.push(i);
            }
        }


        if(mergeCells.length>0){
            this.state.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
        }
        //('settings.mergeInfo', this.state.dataout.data.mergeInfo);
        let headAligns=[];

        for(let i=0;i<headcount+columheadrow;i++){
            for(let j=0;j<columHead.length;j++){
                headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
            }
        }
        this.state.dataout.data.cell= headAligns,
            this.state.dataout.data.cellsFn=function(row, col, prop) {
                let cellProperties = {};
                if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
                    cellProperties.align = colAligns[col];
                    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
                }
                return cellProperties;
            }
        //('>>>>result', this.state.dataout.data);
        this.setState({
            dataout:this.state.dataout,
            showTable: true,
            showModal: false,
            textAlginArr
        });
	}
    onSelect= (key) => {
        //(`${key} selected`);
        switch (key){
            case '1':
                this.getDetailPort({
                    url: '/nccloud/gl/accountrep/triaccquery.do',
                    jumpTo: '/gl/threedetail/pages/main/index.html',
                    key: 'detail',
                    appcode: queryRelatedAppcode(originAppcode.detailbook)
                })
                break;
            case "2":
                this.getDetailPort({
                    url: '/nccloud/gl/accountrep/triaccquery.do',
                    jumpTo: '/gl/threeall/pages/main/index.html',//'/gl/accbalance/relevanceSearch/totalAccount/index.html',
                    key: 'totalAccount',
                    appcode: queryRelatedAppcode(originAppcode.triaccbook),
                })
                break;
            case "3":
                let selectRow = this.getSelectRowData()[0].link;
                if (!selectRow.hasAss) {
                    //toast({ content: `${selectRow.acccode}科目无辅助项，不能联查辅助`, color: 'warning' })
                    toast({ content: this.state.inlt && this.state.inlt.get('20028002-000055',{code : selectRow.acccode}), color: 'warning' })
                    return;
                }
                this.getDetailPort({
					url: '/nccloud/gl/accountrep/assbalancequery.do',
					jumpTo: '/gl/manageReport/assistBalance/content/index.html',
					key: 'assist',
					appcode: queryRelatedAppcode(originAppcode.assbalance)
                });
                break;
        }
    }
    // Deprecated 通过数据行的hasAss判断是否有辅助项，无须发请求。
    relevanceAssist = (response, param) => {//科目余额表的：联查辅助
        let { success, data } = response;
        //('ssssss:', data);
        if (success) {
            //('relevanceAssist', response, self, data);
            if(data){

				//('llllll::', data[0].pk_accassitem);
				let cellArr = [];
                data.map((item) => {
                    cellArr.push(item.pk_accassitem);
				})
				this.setState({
					selectRow: [...cellArr]
				},this.getDetailPort({
					url: '/nccloud/gl/accountrep/assbalancequery.do',
					jumpTo: '/gl/manageReport/assistBalance/content/index.html',//'/gl/accbalance/relevanceSearch/assist/index.html',
					key: 'assist',
					appcode: '20023055',
				}))
            }else {
                // toast({content: `${param.acccode}科目无辅助项，不能联查辅助`, color: 'warning'})
                return;
            }
        }
    }
    getDetailPort(paramObj){//联查"明细"
        let url = paramObj.url; //'/nccloud/gl/accountrep/triaccquery.do';
        let data = this.getDataType(paramObj);
        if(Object.keys(data.link).length > 0 ){
            this.jumpToDetail(data, paramObj);
        }else {
            toast({content: data.linkmsg, color: 'waining'})
        }

        //('getDetailPort>>', data);
    }
    getDataType = (param) => {
        let selectRow = this.getSelectRowData()[0].link;
        let linkmsg = this.getSelectRowData()[0].linkmsg;
        //('selecttt:::12', this)
        let result = '';
        if(param.key === 'detail'){
            result = {
                ...this.state.paramObj,
                link: {...selectRow},
                linkmsg: linkmsg && linkmsg[0],
                "class": "nccloud.pubimpl.gl.account.MrBalbooksLinkTridetailParamTransfer"
            }
        }else if(param.key === 'totalAccount'){
            result = {
                ...this.state.paramObj,
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.account.AccbalLinkTriaccbookParamTransfer"
            }
        }else{//联查"辅助"
            selectRow['key'] = 'relevanceAssist';
            //('selecttt:::32s', this)
            let selectDataParam = [...this.state.selectRow];
            //('selecttt:::', selectDataParam, this)
            result = {
                ...this.state.paramObj,
                link: selectRow,
                "pk_accassitems": selectDataParam,
                "class": "nccloud.pubimpl.gl.account.MrBalbooksLinkAssbalParamTransfer",
                "from": "accbal"
            }
        }
        //('getDataType>', result, selectRow);
        return result;
    }
    jumpToDetail = (data, paramObj) => {
        let gziptools = new gzip();
        this.props.openTo(
            paramObj.jumpTo,
            {appcode: paramObj.appcode, status: gziptools.zip(JSON.stringify(data)),id: '12wew24'}
        );
        this.setState({
            selectRow: []
        })
    }
    //获取handsonTable当前选中行数据
    getSelectRowData=()=>{
        let selectRowData=this.refs.balanceTable.getRowRecord();
        return selectRowData;
    }

    getSelectRow = (param) => {
        this.setState({
            selectRow:[...this.state.selectRow.concat(param)]
        })
    }

	//直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    handleSaveColwidth=()=>{
        let {json} = this.state;
        let info=this.refs.balanceTable.getReportInfo();
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.appcode,info.colWidths, json, callBack);
    }
    handleLoginBtn = (obj, btnName) => {
        //('handleLoginBtn>>>', obj, btnName)
        if(btnName === 'search'){//1、查询
            this.queryClick()
        }else if(btnName === 'print'){//2、打印
            // this.showPrintModal()
        }else if(btnName === 'directprint'){//3、直接输出
            this.printExcel()
        }else if(btnName === 'assbal'){//4、辅助
            this.onSelect('3')
        }else if(btnName === 'detailbook'){//5、明细
            this.onSelect('1')
        }else if(btnName === 'saveformat'){//6、保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'refresh'){
            if (Object.keys(this.state.paramObj).length > 0) {
                this.modalSure(this.state.paramObj)
            }
        }
    }
    render(){
        //('renderrr::', this.state);

        return (
            <div className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['20028002-000018']} /* 国际化处理： 多主体科目余额表*/
                    btnContent = {
                        this.props.button.createButtonApp({
                            area: 'btnarea',
                            buttonLimit: 3,
                            onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                            // popContainer: document.querySelector('.header-button-area')
                        })
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.fourLables.map((items) => {
                                return(
                                    <HeadCom
                                        lastThre = {this.state.dataWidthPage && this.state.dataWidthPage.headtitle}
                                        labels={items.title}
                                        key={items.title}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => this.setData(this.state.dataWidthPage))
                                        }
                                        accountType = {this.state.accountType}
                                        isLong = {items.styleClass}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                   <SimpleTable
                        ref="balanceTable"
                        data = {this.state.dataout}
                        // onCellMouseDown = {(e, coord,td) => this.handleSimpleTableClick(e, coord,td, 'multiagentSubjectBalance')}
                        onCellMouseDown = {(e, coords, td) => {
                            //('recorddd:::',coords, this.refs.balanceTable.getRowRecord());
                            this.whetherDetail('link', 'multiagentSubjectBalance');
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
                </div>
                <div className="modalContainer">
                    <QueryModal
                        show={this.state.showModal}

                        ref='MainSelectModal'
                        title= {this.state.json['20028002-000053']} //查询条件
                        icon=""
                        onConfirm={(datas) => {
                            this.modalSure(datas);
                        }}

                        onCancel={() => {
                            this.queryClick()
                        }}
                    />
                </div>
            </div>
        )
    }
}

MultiagentSubjectBalance = createPage({})(MultiagentSubjectBalance)
ReactDOM.render(<MultiagentSubjectBalance />,document.querySelector('#app'));
