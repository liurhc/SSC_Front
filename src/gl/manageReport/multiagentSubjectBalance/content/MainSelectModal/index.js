import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax, deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import '../../../css/searchModal/index.less';


const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,
} = base;

import { toast } from '../../../../public/components/utils';

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';
// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import AccPeriodDefaultTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../uapbd/refer/org/BusinessUnitTreeRef';

import AccountDefaultModelTreeRef from '../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../public/components/uapRefer.js';
import {
    renderMoneyType,
    renderRefer,
    getSubjectVersion,
    createReferFn,
    getCheckContent, businessUnit, renderLevelOptions, returnMoneyType, getReferDetault
} from '../../../common/modules/modules';
import { subjectRefer } from '../../../referUrl';
import SubjectVersion from "../../../common/modules/subjectVersion"
import DateGroup from '../../../common/modules/dateGroup'
import SubjectLevel from '../../../common/modules/subjectAndLevel'
import {clearTransterData, handleChange, handleSelectChange} from "../../../common/modules/transferFn";
import CheckBoxCells from '../../../common/modules/checkBox';
import {handleValueChange} from '../../../common/modules/handleValueChange'
import FourCheckBox from '../../../common/modules/fourCheckBox'
import FormLabel from '../../../../public/components/FormLabel';

class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
			json: {},
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            selectedKeys: [],
            targetKeys: [],
            changeParam: true, //false:点击条件时不触发接口，,
            rightSelectItem: [], //科目的"条件"按钮中存放右侧数据的pk值


            showRadio: false,//控制是否显示"会计期间"的但选框；false: 不显示
            appcode: '20028002',
            versiondate: '', //存放选择的版本号
            versionDateArr: [], //存放请求账簿对应的版本数组
            level:0, //返回的最大级次，起初时前一个和后一个公用
			laterLevel: 0, //级次后一个的值
            isShowUnit:false,//是否显示业务单元
            busiDate: '', //业务日期

            start: {},  //开始期间
            end: {}, //结束期间
            /*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
            startyear: '', //开始年
            startperiod: '',  //开始期间
            endyear: '',  //结束年
            endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示


            pk_accperiodscheme: '',//会计期间参数
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: {        //二级单元
				refname:'',
				refpk: '',
			},
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表
			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次
			startcode: {
				refname:'',
				refpk: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
			},    //结束科目编码

            selectVersiondate: '',//选择的科目版本日期值
            buiDate: '', //业务日期
			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			showzerooccur: false, //无发生不显示
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: false, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
            selfAgentSubjectSearch: false, //按个主体科目查询是否选择了'
            selfAgentSubjectSearchEdit: true, //按个主体科目查询是否禁止编辑；false：不禁止

			isversiondate: false,

			
		}
		this.handleCurrtypeChange = this.handleCurrtypeChange.bind(this);
		this.renderRefer = renderRefer.bind(this);
		this.handleValueChange = handleValueChange.bind(this);
	}

	static defaultProps = {
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		// accAssItems: [],
	};


    componentWillMount() {
        let callback= (json) =>{
            this.setState({
				json:json,
                currtype: json['20028002-000000'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['20028002-000000'],  //选择的币种名称/* 国际化处理： 本币*/
			},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20028002', 'publiccommon', 'dategroup', 'checkbox', 'fourcheckbox', 'subjectandlevel', 'subjectversion', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
    }

	componentDidMount() {
        setTimeout(() => getReferDetault(this, true, {
            businessUnit
		}),0)
	}

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}




	getCurrtypeList = () => {//获取"币种"接口
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {"localType":"1", "showAllCurr":"1"};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
                    let newData = data.filter((item, index) => {
                        return item.name !== self.state.json['20028002-000003']/* 国际化处理： 所有币种*/
                    });
                    //('newData::', newData);
                    self.setState({
                        currtypeList: newData,
                    })

		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


    handleCurrtypeChange = (keyParam, value) => {
        this.setState({
            [keyParam]: value,
        });
    }


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}

    getSubjectVersion = (v) => {//获取"启用科目版本"数据
        //gl.glpub.accountinfoquery
        let url = '/nccloud/gl/glpub/accountinfoquery.do';
        let data = {
            pk_accountingbook: v[0].refpk,//""
        };
        ajax({
            url,
            data,
            success: (response) => {
                let { data } = response;
                //('getSubjectVersion>>', response, data.versiondate, data.bizDate)
                this.setState({
                    pk_accperiodscheme: data.pk_accperiodscheme,//"会计期间"参照需要的参数，
                    versiondate: data.versiondate,
                    buiDate: data.bizDate
                }, this.getReferInfo(v))
            },
            error: (error) => {
                toast({ content: error.message, color: 'warning' })
            }
        })
    }
    getReferInfo = (v) => {
        //('getReferInfo>>', v, this.state);
        let self = this;
        let newUrl = '/nccloud/gl/accountrep/assbalancequeryobject.do';//辅助余额查询对象
        let newData = {
            "pk_accountingbook":v[0].refpk,
            "versiondate":this.state.buiDate,
            "needaccount":false
        };
        //('newData>>', newData);
        ajax({
            url:newUrl,
            data:newData,
            success: function(response){
                const { success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if(response.data){
                        //('response:::>>', response.data);
                        self.setState({
                            tableData: response.data
                        })
                    }
                }
            }
        });
    }
	
	//根据核算账簿获取相关信息
	getInfoByBook(type) {
		// //(this.state.accountingbook)

		// if (!this.state.accountingbook || !this.state.accountingbook[0]) {
		// 	//如果是清空了核算账簿，应该把相关的东西都还原
		// 	return;
		// }
		
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
                getSubjectVersion(self);
		    	//(res)
		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	

		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook
		        	})
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		//(value)
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		//(value)
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    //(e);
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化
	onChangeShowzerooccur(e, key) {//value, 'showzerooccur'
		this.setState({
			[key]: e
		});
		if(key === 'showzerooccur'){
            if(e){
                this.setState({
                    showzerobalanceoccurEdit: true
                });
                if(!this.state.showzerobalanceoccur){
                    this.setState({
                        showzerobalanceoccur: true
                    })
                }
            }else{
                this.setState({
                    showzerobalanceoccurEdit: false
                })
            }
		}
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}

	handleGTypeChange =() =>{
		
	};

    handleSelect = (value) => {//
        //('handleSelect', value);

        this.setState({
            rightSelectItem: [...value]
        })
    }
    handleDateChange(value){//日期: 范围选择触发事件
        //('handleDateChange:', value);
        this.setState({
            begindate: value[0],
            enddate: value[1],
            rangeDate: value
        })
    }

	render() {
		let { show, title, isButtonShow}= this.props;
		return <Modal
			fieldid='query'
            show={ show }
            onHide={()=>this.props.onCancel(true)}
            className='multiagentSubjectBalance'
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    {title}
                </Modal.Title>
            </Modal.Header >
			<Modal.Body id="modalOuter">
                <div className='right_query noserchmatter'>
                    <div className='query_body1'>
                        <div className='query_form'>
				<Row className="myrow">
					<Col md={2} sm={2}>
						<FormLabel isRequire={true} labelname={this.state.json['20028002-000009']}/>{/* 国际化处理： 核算账簿*/}
					</Col>
					<Col md={10} sm={10}>
						<div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: true,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit
                                    },
									'multiagentSubjectBalance'
                                )
                            }
						</div>
					</Col>
				</Row>
                {/*启用科目版本：*/}
                <SubjectVersion
                    checked = {this.state.isversiondate}//复选框是否选中
                    data={this.state.versionDateArr}//
                    value={this.state.versiondate}
                    disabled={!this.state.isversiondate}
                    handleValueChange = {(key, value) => this.handleValueChange(key, value, 'SubjectVersion')}
                />

                {/*会计期间*/}
                <DateGroup
                    selectionState = {this.state.selectionState}
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook[0]}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                />

				{/*忽略日期选项*/}

				{/*科目，级次：*/}
                <SubjectLevel
                    parent = {this}
                    accountingbook = {this.state.accountingbook}
                    isMultiSelectedEnabled = {false}//控制多选单选

                    selectedKeys = {this.state.selectedKeys}
                    targetKeys = {this.state.targetKeys}
                    changeParam = {this.state.changeParam}
                    handleSelect = {this.handleSelect}
                    handleValueChange = {this.handleValueChange}

                    handleSelectChange = {(sourceSelectedKeys, targetSelectedKeys) => handleSelectChange(this, sourceSelectedKeys, targetSelectedKeys)}
                    handleChange = {(nextTargetKeys, direction, moveKeys) => handleChange(this,nextTargetKeys, direction, moveKeys) }
                    clearTransterData = {() => clearTransterData(this)}
                />

                <FourCheckBox
                    totalTitle= {this.state.json['20028002-000004']} ///* 国际化处理： 显示属性：*/
                    renderCells={
                        [
                            {
                                title: this.state.json['20028002-000005'],/* 国际化处理： 按各主体科目查询*/
                                checked: this.state.selfAgentSubjectSearch,
                                disabled: this.state.selfAgentSubjectSearchEdit,
                                id: 'selfAgentSubjectSearch',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'selfAgentSubjectSearch')
                            }
                        ]
                    }
                />

                {/*包含凭证：*/}
                <CheckBoxCells
                    paramObj = {{
                        "noChargeVoucher": {//未记账凭证
                            show: true,
                            disabled: this.state.disabled,
                            checked: this.state.includeuntally,
                            onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                        },
                        "errorVoucher": {//错误凭证
                            show: true,
                            checked: this.state.includeerror,
                            onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                        },
                        "harmToBenefitVoucher": {//损益结转凭证
                            show: true,
                            checked: this.state.includeplcf,
                            onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                        },
                        "againClassifyVoucher": {//重分类凭证
                            show: true,
                            checked: this.state.includerc,
                            onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                        },
                    }}
                />

				{/*币种*/}
				{renderMoneyType(this.state.currtypeList,this.state.currtypeName, this.handleCurrtypeChange, this.state.json)}

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
							gloableEdit: this.state.globalCurrency
                        },
						this.state.json
                    )
                }

                <FourCheckBox
                    totalTitle= {this.state.json['20028002-000004']} ///* 国际化处理： 显示属性：*/
                    renderCells={
                        [
                            {
                                title: this.state.json['20028002-000006'],/* 国际化处理： 无发生不显示*/
                                checked: this.state.showzerooccur,
                                disabled: this.state.showzerooccurEdit,
                                id: 'showzerooccur',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'showzerooccur')
                            },
                            {
                                title: this.state.json['20028002-000007'],/* 国际化处理： 无余额无发生不显示*/
                                checked: this.state.showzerobalanceoccur,
                                disabled: this.state.showzerobalanceoccurEdit,
                                id: 'showzerobalanceoccur',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'showzerobalanceoccur')
                            },
                            {
                                title: this.state.json['20028002-000008'],/* 国际化处理： 显示科目类型小计*/
                                checked: this.state.sumbysubjtype,
                                disabled: this.state.disSumbysubjtype,
                                id: 'sumbysubjtype',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'sumbysubjtype')
                            }
                        ]
                    }
                />

				<Row className="myrow">
					<Col md={2} sm={2}>
						<FormLabel labelname={this.state.json['20028002-000010']}/>{/* 国际化处理： 余额方向*/}
					</Col>
					<Col md={2} sm={2}>

						<div className="book-ref">
							<Select
								showClear={false}
								fieldid='balanceori'
							   	value={this.state.balanceori}
							   	onChange={this.handleBalanceoriChange.bind(this)}							   	
							>							 	
							 	<NCOption value={'-1'} key={'-1'} >{this.state.json['20028002-000011']}</NCOption>{/* 国际化处理： 双向*/}
							 	<NCOption value={'0'} key={'0'} >{this.state.json['20028002-000012']}</NCOption>{/* 国际化处理： 借*/}
							 	<NCOption value={'1'} key={'1'} >{this.state.json['20028002-000013']}</NCOption>{/* 国际化处理： 贷*/}
							 	<NCOption value={'3'} key={'3'} >{this.state.json['20028002-000014']}</NCOption>{/* 国际化处理： 平*/}

							</Select>

						</div>
											
					</Col>
					<Col md={3} sm={3}>
						<Checkbox
							colors="dark"
							className="mycheck"
							checked={this.state.twowaybalance}
							disabled={this.state.twowaybalanceEdit}
							onChange={this.onChangeTwowaybalance.bind(this)}
						>
							{this.state.json['20028002-000015']} {/* 国际化处理： 按借贷方显示余额*/}
						</Checkbox>
					</Col>					
				</Row>
						</div>
					</div>
				</div>
			</Modal.Body>
			{isButtonShow &&
				<Modal.Footer>
					<Button
						fieldid='query'
                        className= "button-primary"
						onClick = {() => {
								let pk_accountingbook = [];
								let pk_unit = [];
								
								if (Array.isArray(this.state.accountingbook)) {
									this.state.accountingbook.forEach(function (item) {
										pk_accountingbook.push(item.refpk)
									})
								}

								if (Array.isArray(this.state.buSecond)) {
									this.state.buSecond.forEach(function (item) {
										pk_unit.push(item.refpk)
									})
								}
								let data = {
									pk_accountingbook: pk_accountingbook,
									pk_unit,
									multbusi: this.state.multbusi,
									versiondate: this.state.isversiondate ? this.state.versiondate : this.state.busiDate,//科目版本,//科目版本
									// pk_accasoa://条件框选择科目
									startcode: this.state.startcode.refcode,  //开始科目编码
									endcode: this.state.endcode.refcode,    //结束科目编码
									startlvl: String(this.state.startlvl),
									endlvl: String(this.state.endlvl),
									isleave: this.state.isleave ? 'Y' : 'N',
									isoutacc: this.state.isoutacc ? 'Y' : 'N',
									startyear: this.state.startyear,
									endyear: this.state.endyear,
									startperiod: this.state.startperiod,
									endperiod:   this.state.endperiod,

									//未记账、错误、损益结转、重分类  Y/N  注意：‘包含凭证’是个标题，后面的才是选项
									includeuntally: this.state.includeuntally ? 'Y' : 'N',
									includeerror: this.state.includeerror ? 'Y' : 'N',
									includeplcf: this.state.includeplcf ? 'Y' : 'N',
									includerc: this.state.includerc ? 'Y' : 'N',
									pk_currtype: this.state.currtype,
									returncurr: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局
									mutibook: this.state.mutibook,
									showzerooccur: this.state.showzerooccur ? 'Y' : 'N',
									showzerobalanceoccur: this.state.showzerobalanceoccur ? 'Y' : 'N',
									currplusacc: this.state.currplusacc,
									sumbysubjtype: this.state.sumbysubjtype ? 'Y' : 'N',
									balanceori: this.state.balanceori,
									twowaybalance: this.state.twowaybalance ? 'Y' : 'N',
									istree: 'Y',
                                    pk_accasoa: this.state.rightSelectItem,//'条件'
									qrybybaseorgsubj:this.state.selfAgentSubjectSearch,//按各主体科目查询
									querybyperiod: this.state.selectionState,//会计期间/日期选项
								}
								let url = '/nccloud/gl/accountrep/checkparam.do'
								let flagShowOrHide;
								ajax({
									url,
									data,
									async:false,
									success:function(response){
										flagShowOrHide = true
									},
									error:function(error){
										flagShowOrHide = false
										toast({content: error.message, color: 'warning'});
									}
								})
								if(flagShowOrHide == true){
									this.props.onConfirm(data)
								}else if(flagShowOrHide == false){
									return true;
								}
							}
						}
					>
						{this.state.json['20028002-000001']}
					</Button>


					<Button 
						fieldid='cancel'
						className= 'btn-2 btn-cancel'
						onClick={() => {this.props.onCancel(false)}}
					>{this.state.json['20028002-000002']}</Button>
				</Modal.Footer>
			}
			<Loading fullScreen showBackDrop={true} show={this.state.isLoading} />



		</Modal>;
	}
}
CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal;
