/**
 * Created by liqiankun on 2018/7/13.
 * 对外渲染 辅助余额表
           科目余额表
           序时账
    其中的一个
 */

import React, {Component} from 'react';

import {high, base, ajax, createPage,getMultiLang} from 'nc-lightapp-front';
const {NCMessage:Message} = base;
import SubjectBalance from '../../../accbalance/container/Accbalance';//科目余额表
import AssistBalance from '../../assistBalance/content/content';//辅助余额表
import Journal from '../../journal/content/content';//序时账
import '../../../public/reportcss/firstpage.less'
import {searchById} from "../../common/modules/createBtn";
class LinkReport extends Component{
    constructor(props){
        super(props);
        this.state = {
            type: '',
            sourceData: {},
            json:{}
        }

        this.queryAjax = this.queryAjax.bind(this);
        this.renderComponent = this.renderComponent.bind(this);//根据请求接口返回的type来确定渲染哪一个组件
       // this.searchById = searchById.bind(this);
    }

    componentDidMount(){
        this.queryAjax()
       // this.searchById('20023040link',20023040);
    }
    queryAjax = () => {
        let self = this;
        let url = '/nccloud/gl/accountrep/budgetlink.do';
        let param = {pk_ntbparadimvo: this.props.getUrlParam('pk_ntbparadimvo')}
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, type, success } = response;
                if(success && data){

                    self.setState({
                        type: data.type,
                        sourceData: {...data.data}
                    })
                }else{
                    Message.create({content: self.state.json['20023040-000088'], color: 'danger'});
                }
            },
            error:function(error){
                Message.create({content: self.state.json['20023040-000089'], color: 'danger'});
                throw error
            }
        })
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023040'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    renderComponent(param){//根据请求接口返回的type来确定渲染哪一个组件
        // 说明：type报表类型 balance:科目余额表 assbalance:辅助余额表 sequence:序时账
        switch (param){
            case 'balance':
                return <SubjectBalance dataout = {this.state.sourceData}/>;
            case 'assbalance':
                return <AssistBalance dataout = {this.state.sourceData}/>;
            case 'sequence':
                return <Journal dataout = {this.state.sourceData}/>;
            default:
                return <div></div>
        }
    }
    render(){

        return(
            <div>
                {this.renderComponent(this.state.type)}
            </div>
        )
    }
}
LinkReport = createPage({})(LinkReport);

ReactDOM.render(<LinkReport />,document.querySelector('#app'));




// 页面路径 gl/manageReport/linkreport/budgetlink/index.js
// 请求：/nccloud/gl/accountrep/budgetlink.do
// 参数：{pk_ntbparadimvo: this.props.getUrlParam('pk_ntbparadimvo')}
// 返回：{
//     type: "balance",
//     data:{} //报表查询出的数据
// }
// 说明：type报表类型 balance:科目余额表 assbalance:辅助余额表 sequence:序时账






