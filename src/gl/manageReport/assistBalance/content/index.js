/**
 * Created by liqiankun on 2018/6/26.
 * 管理报表 ---> 辅助余额表
 */

import React, {Component} from 'react';
import Content from './content.js';
import {high, base, ajax,print } from 'nc-lightapp-front';

import {tableDefaultData} from '../../defaultTableData';
import "../../../public/reportcss/firstpage.less";
class ContentBox extends Component{
    constructor(props){
        super(props);
        this.state = {
            dataout: tableDefaultData
        }
    }

    render(){
        return (
            <Content dataout={this.state.dataout}/>
        )
    }
}

ReactDOM.render(<ContentBox />,document.querySelector('#app'));
