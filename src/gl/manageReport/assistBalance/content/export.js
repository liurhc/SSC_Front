/**
 * Created by liqiankun on 2018/6/26.
 * 管理报表 ---> 辅助余额表
 */

import React, {Component} from 'react';
import {high, base, ajax,print, getMultiLang } from 'nc-lightapp-front';
const { NCModal: Modal} = base;
import Content from './content.js';
class ContentBox extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{}
        }
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'20023055',domainName:'gl',currentLocale:'simpchn',callback});
    }
    render(){
        let {show, clickhandle} = this.props;
        return (
            <div style={{
                position: 'absolute',
                width: '100%',
                top: '0px',
                background: 'rgba(155,155,155,0.8)',
                zIndex: '999',
                display: show?'': 'none'}}>
                <div
                    onClick={() => clickhandle()}
                    style={{position:'relative', top: '0px', left: '0px'}}
                >
                    {this.state.json['20023055-000000']}/* 国际化处理： 关闭*/
                </div>
                <Content dataout = {this.props.dataout} style={{background: '#ccc'}}/>
            </div>
        )
    }
}

export default ContentBox






// <Modal
// show={ show }
//     >
//     <Modal.Header>
//     <Modal.Title>
//     <span className='bd-title-1'>可能</span>
// </Modal.Title>
// <Modal.Body id="modalOuter">
//     <Content dataout = {this.props.dataout} />
//     </Modal.Body>
// </Modal.Header >
// </Modal>
