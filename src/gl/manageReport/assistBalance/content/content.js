/**
 * Created by liqiankun on 2018/6/26.
 * 管理报表 ---> 辅助余额表
 */

import React, {Component} from 'react';

import {high, base, ajax, createPage, deepClone, getMultiLang, gzip,createPageIcon } from 'nc-lightapp-front';
const { NCDiv } = base;
import '../../css/index.less'
import HeadCom from '../../common/headSearch/headCom';
// import {titleLabels } from '../../common/headSearch/titledata';
import { SimpleTable } from 'nc-report';
import QueryModal from './components/MainSelectModal';
import reportPrint from '../../../public/components/reportPrint';
import { toast } from '../../../public/components/utils.js';
const { PrintOutput } = high;
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import {searchById} from "../../common/modules/createBtn";
import {handleSimpleTableClick} from "../../common/modules/simpleTableClick";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import PageButtonGroup from '../../common/modules/pageButtonGroup'
import {rowBackgroundColor} from "../../common/htRowBackground";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
import { queryRelatedAppcode, originAppcode } from '../../../public/common/queryRelatedAppcode.js';
class AssistPropertyBalance extends Component{
    constructor(props){
        super(props);

        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            disabled: true,
            visible: false,
            appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            outputData: {},
            ctemplate: '', //模板输出-模板
            nodekey: '',
            printParams:{}, //查询框参数，打印使用
            showAddBtn: false, //"联查明细"按钮是否禁止；false:不禁止，true:禁止
            textAlginArr:[],//对其方式
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
            dataWithPage: {}, //存放请求回来的数据信息，用于分页操作
            firstPageDisable: true,  //true:不可点击；false: 可点击
            lastPageDisable: true,     //true:不可点击；false: 可点击
            paramObj: {},
            showPageBtn: true,//是否显示分页按钮；true: 显示；false: 不显示
            accountType: 'amountcolumn',//金额式
            "fiveLables": [],
            hideBtnArea : false, // 是否隐藏高级查询按钮
            defaultAccouontBook: {} //默认账簿
        }
        this.switchCount = 0 ;// 转换列名下标计数
        this.dataPage = 0; //存放请求回来的数据椰树信息,查询后的页码
        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.handlePage = this.handlePage.bind(this);//页面处理事件
        this.setData= this.setData.bind(this);// 整理表格所需要的数据格式
        this.handleValueChange = handleValueChange.bind(this)
        this.renderFirstData = {};
        this.searchById = searchById.bind(this);
        this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
    }

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                "fiveLables": [
                    {
                        title: json['20023055-000049'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20023055-000050'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023055-000051'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023055-000052'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023055-000053'],
                        styleClass:"m-brief"
                    }
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023055','publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        this.setState({
            flag: true,
        })
        this.getParam();
        let appceod = this.props.getSearchParam('c');
        this.searchById('20023055PAGE', appceod).then((res) => {
            // searchById返回当前登录环境的数据源信息。
            // 如果联查的数据源与当前数据源不一致，则隐藏页面的所有按钮。
            let param = this.props.getUrlParam && this.props.getUrlParam('status');
            if (param && res && res.data.context) {
                let { context } = res.data
                let unzipParam = new gzip().unzip(param)
                if (unzipParam.env) {
                    let envs = unzipParam.env.replace(/\[/g, "").replace(/\]/g, "").replace(/\"/g, "").split(",");
                    if (envs[0] && envs[0] != res.data.context.dataSource) {
                        this.props.button.setButtons([]);
                        this.setState({ hideBtnArea: true });
                    }
                }
                this.state.defaultAccouontBook = {refname:context.defaultAccbookName,refpk:context.defaultAccbookPk, getflag: true}
            }
        });
        //linkdetail switch print templateOutput directprint saveformat refresh first pre next last
        this.props.button.setDisabled({
            linkdetail:true, switch:true, print:true,
            templateOutput:true, directprint:true,
            saveformat:true, first:true,
            pre:true, next:true, last:true
        });
        this.props.button.setButtonVisible(['first', 'pre', 'next', 'last'], false)
    }

    showPrintModal() {
        this.setState({
            visible: true
        })
    }
    handlePrint(data, isPreview) {
        let printUrl = '/nccloud/gl/accountrep/assbalanceprint.do'
        let { printParams, appcode } = this.state
        let { ctemplate, nodekey } = data
        printParams.currpage = this.dataPage.toString() //当前页
        printParams.queryvo = data
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
        printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
        this.handleCancel()
    }
    handleCancel() {
        this.setState({
            visible: false
        });
    }
    showOutputModal() {
        // this.refs.printOutput.open()
        let outputUrl = '/nccloud/gl/accountrep/assbalanceoutput.do'
        let { appcode, nodekey, ctemplate, printParams } = this.state
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)
        // let outputData = mouldOutput(appcode, nodekey, ctemplate, printParams)
        // this.setState({
        //     outputData: outputData
        // })
    }
    handleOutput() {

    }

    getParam = () => {
        let urlParam;
        // 联查参数，需解压
        let data = this.props.getUrlParam && this.props.getUrlParam('status');
        if (data) {
            urlParam = new gzip().unzip(data)
        }
        // 小友智能查账，不需解压
        let param = this.props.getSearchParam('param');
        if (param) {
            urlParam = param;
        }
        // 查询
        if (urlParam) {
            this.setState({
                printParams: { ...urlParam }
            })
            this.modalSure(urlParam)
        }
    }
    handlePage(param, dataWithPage){//页面控制事件

        let dataLength = dataWithPage.data.length;
        let renderData = [];
        let obj = {};
        switch(param){
            case 'firstPage':
                this.dataPage = 0;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                obj['amountcolumn'] = dataWithPage.amountcolumn;

                this.setState({
                    firstPageDisable: true,
                    lastPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                   first:true, pre:true,
                    next:false, last:false
                });
                this.setData(obj);
                return;
            case 'nextPage':
                this.dataPage += 1;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                this.setState({
                    firstPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                   first:false, pre:false
                });
                if(this.dataPage === dataLength - 1){
                    this.setState({
                        lastPageDisable: true
                    })
                    this.props.button.setDisabled({
                        next:true, last:true
                    });
                }

                this.setData(obj);
                return;
            case 'prePage':
                this.dataPage -=1;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                if(this.dataPage === dataLength - 2){
                    this.setState({
                        lastPageDisable: false,
                        selectRowIndex: 0
                    })
                    this.props.button.setDisabled({
                        next:false, last:false
                    });
                }
                if(this.dataPage === 0){
                    this.setState({
                        firstPageDisable: true,
                        selectRowIndex: 0
                    })
                    this.props.button.setDisabled({
                        first:true,
                        pre:true,
                    });
                }
                this.setData(obj);
                return;
            case 'lastPage':
                this.dataPage = dataLength - 1;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                this.setState({
                    lastPageDisable: true,
                    firstPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    next:true, last:true,
                    first:false, pre:false
                });
                this.setData(obj);
                return;
        }
    }
    handleSwitchColumn(dataWithPage) {// 转换事件
        this.switchCount = ( this.switchCount + 1 ) % 3;
        let renderData = [];
        let obj = {};
        renderData = dataWithPage.data[this.dataPage];
        obj.column = dataWithPage.column;
        obj.data = renderData;
        obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
        obj['amountcolumn'] = dataWithPage.amountcolumn;
        this.setData(obj);
        return;
    }
    queryClick(){//点击"查询"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    modalSure(param){//点击弹框"确定"事件

        // 页面控制的参数恢复初始值
        this.dataPage = 0;
        this.ajaxData(param)
    }
    ajaxData = (param) => {
        // 发送请求
        let url = '/nccloud/gl/accountrep/assbalancequery.do';
        let self = this;
        let {showPageBtn,firstPageDisable,disabled,selectRowIndex,lastPageDisable} = this.state;
        ajax({
            url:url,
            data:param,
            //async:false,
            success:function(response){
                let { data, success } = response;
                if(success && data){
                    let buttonObj={
                        switch:false, 
                        print:false,
                        templateOutput:false, 
                        directprint:false,
                        saveformat:false, 
                        refresh:false,
                        first:true, 
                        pre:true
                    };
                    // 首页和上一页不可用
                    firstPageDisable=true;
                    //下一页和末页根据数据量判断是否可用
                    if(data.data.length>0){
                        disabled=false;
                        selectRowIndex=0;
                        if(data.data.length > 1){
                            lastPageDisable=false;
                            buttonObj.next=false;
                            buttonObj.last=false;

                        } else {
                            lastPageDisable=true;
                            buttonObj.next=true;
                            buttonObj.last=true;
                        }
                    }
                    if(Object.keys(data.data[0]).length == 1){//如果返回的数据不带pageinfo说明是单页的，不显示按钮组件
                        showPageBtn=false;
                    }else {
                        showPageBtn=true;
                    }
                    self.setState({
                        paramObj: {...param},
                        printParams: {...param},
                        accountType: 'amountcolumn',
                        showPageBtn,firstPageDisable,
                        dataWithPage: data,//deepClone(data),
                        showAddBtn: false,
                        typeDisabled: false,
                        disabled,selectRowIndex,lastPageDisable
                    })
                    self.props.button.setDisabled(buttonObj);

                    self.renderFirstData.column = data.column;
                    self.renderFirstData.data = data.data[0];
                    self.renderFirstData.amountcolumn = data.amountcolumn;
                    self.renderFirstData.quantityamountcolumn = data.quantityamountcolumn;
                    let datalength = data.showpagelength ? data.data.length : 0;//deepClone(data);
                    self.setData(self.renderFirstData, datalength);
                }else{
                    self.setState({
                        paramObj: {...param},
                        printParams: {...param},
                        accountType: 'amountcolumn',
                    })
                    toast({content: self.state.json['20023055-000043'], color: 'warning'});/* 国际化处理： 请求数据为空！*/
                }
            },
            error:function(error){
                self.setState({
                    paramObj: {...param},
                    printParams: {...param},
                    accountType: 'amountcolumn',
                })
                toast({content: error.message, color: 'warning'});
            }

        })
        
    }
    setData = (data, datalength) => {//整理表格数据
        if(datalength > 0){
            toast({
                content: `${this.state.json['20023055-000044']}，${this.state.json['20023055-000045']}${datalength}${this.state.json['20023055-000046']}`,/* 国际化处理： 查询成功,共,页*/
                color: 'success',
                duration: 6
            })
        }else{
            toast({
                color: 'success'
            })
        }

        let rows = [];   //表格显示的数据合集
        let columHead = []; //第一层表头
        let parent = []; //存放一级表头，二级表头需要补上
        let parent1 = []; //存放二级表头，三级表头需要补上
        let child1 = []; //第三层表头
        let child2 = []; //第二层表头
        let colKeys = [];  //列key值，用来匹配表体数据
        let colAligns=[];  //每列对齐方式
        let colWidths=[];  //每列宽度
        let oldWidths=[];
        let textAlginArr = this.state.textAlginArr.concat([])
        let flagArr = 'N';
        data[this.state.accountType][this.switchCount].forEach((value) => {//column:[]原始数据表头信息
            let valueCell = {};

            valueCell.title = value.title;
            valueCell.key = value.key;
            valueCell.align = 'center';
            valueCell.bodyAlign = 'left';
            valueCell.style = 'head';
            if(value.children){
                flagArr = 'Y';
                if(parent.length>0){
                    child1.push(...parent);
                }
                for(let i=0;i<value.children.length;i++){
                    if(value.children[i].children){
                        child2.push(...parent1);
                        for(let k=0;k<value.children[i].children.length;k++){
                            let child1Cell = {};
                            child1Cell.title = value.children[i].title;
                            child1Cell.key = value.children[i].key;
                            child1Cell.align = 'center';
                            child1Cell.bodyAlign = 'left';
                            child1Cell.style = 'head';
                            let child2Cell = {};
                            child2Cell.title = value.children[i].children[k].title;
                            child2Cell.key = value.children[i].children[k].key;
                            child2Cell.align = 'center';
                            child2Cell.bodyAlign = 'left';
                            child2Cell.style = 'head';
                            columHead.push(valueCell);
                            child1.push(child1Cell);
                            child2.push(child2Cell);
                            colKeys.push(value.children[i].children[k].key);
                            colAligns.push(value.children[i].children[k].align);
                            colWidths.push({'colname':value.children[i].children[k].key,'colwidth':value.children[i].children[k].width});

                        }
                        parent1 = [];
                    }else {
                        let innerCell2 = {};
                        innerCell2.title = value.children[i].title;
                        innerCell2.key = value.children[i].key;
                        innerCell2.align = 'center';
                        innerCell2.bodyAlign = 'left';
                        innerCell2.style = 'head';
                        columHead.push(valueCell);
                        child1.push(innerCell2);
                        parent1.push(innerCell2);
                        colKeys.push(value.children[i].key);
                        colAligns.push(value.children[i].align);
                        colWidths.push({'colname':value.children[i].key,'colwidth':value.children[i].width});
                    }
                }
                parent = [];
            }else{
                let cellObj = {};
                cellObj.title = value.title;
                cellObj.key = value.key;
                cellObj.align = 'center';
                cellObj.bodyAlign = 'left';
                cellObj.style = 'head';
                columHead.push(valueCell);
                parent1.push(cellObj);
                parent.push(cellObj);
                colKeys.push(value.key);
                colAligns.push(value.align);
                colWidths.push({'colname':value.key,'colwidth':value.width});
            }
        });
        let columheadrow = 0; //表头开始行
        if (data.headtitle){
           columheadrow = 1;
           let headtitle = [];
           data.headtitle.forEach(function (value) {
            headtitle.push(value[0]);
            headtitle.push(value[1]);
           });
           for(let i=headtitle.length;i<columHead.length;i++){
            headtitle.push('');
           }
           rows.push(headtitle);
        }

        let mergeCells = [];
        let row,col,rowspan,colspan;
        let headcount = 1; //表头层数
        if(child1.length>0){
            headcount++;
        }
        if(child2.length>0){
            headcount++;
        }
        let currentCol = 0;
        //计算表头合并格
        for(let i=0;i<data[this.state.accountType][this.switchCount].length;i++) {
            let value = data[this.state.accountType][this.switchCount][i];
            //*********
            if(value.children){//有子元素的
                let childLeng = value.children.length;
                mergeCells.push([columheadrow, currentCol, columheadrow, currentCol + childLeng -1 ]);

                let headCol = currentCol;
                for(let i=0;i<value.children.length;i++){

                    let childlen = 0;
                    if(value.children[i].children){

                        let childlen = value.children[i].children.length;
                        currentCol = currentCol+childlen;
                        for(let k=0;k<value.children[i].children.length;k++){
                            textAlginArr.push(value.children[i].children[k].align);
                        }
                    }else if(childlen > 0){//子元素里面没有子元素的
                        mergeCells.push([columheadrow+1, currentCol, columheadrow+2, currentCol]);//cell
                        currentCol++;
                    }else {
                        currentCol++;
                        textAlginArr.push(value.children[i].align);
                    }
                }
            }else{//没有子元素对象
                mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
                currentCol++;
                textAlginArr.push(value.align);
            }
        }
        if(parent.length>0){
            child1.push(...parent);
        }
        if(child2.length>0&&parent1.length>0){
            child2.push(...parent1);
        }

        rows.push(columHead);

        if(child1.length>0 && flagArr === 'Y'){
            rows.push(child1);
        }
        if(child2.length>0 && flagArr === 'Y'){
            rows.push(child2);
        }

        if(data!=null && data.data.pagedata.length>0){

            let rowdata = [];
            let flag = 0;
            for(let i=0;i<data.data.pagedata.length;i++){
                flag++;
                for(let j=0;j<colKeys.length;j++){
                   // {
                    //     "title":"11163",
                    //     "key":"pk_org",
                    //     "align":"left",
                    //     "style":"body"
                    // }
                    let itemObj = {
                        align: 'right',
                        style: 'body'
                    };
                    itemObj.title = data.data.pagedata[i][colKeys[j]];
                    itemObj.key = colKeys[j] + flag;
                    itemObj.link = data.data.pagedata[i].link;
                    itemObj.align = colAligns[j];
                    // rowdata.push(data.data[0].pagedata[i][colKeys[j]])
                    rowdata.push(itemObj)
                }
                rows.push(rowdata);
                rowdata = [];
            }
        }
        let rowhighs = [];
        for (let i=0;i<rows.length;i++){
            rowhighs.push(23);
        }
        this.props.dataout.data.cells= rows;//存放表体数据
        this.props.dataout.data.widths=colWidths;
        oldWidths.push(...colWidths);
        this.props.dataout.data.oldWidths = oldWidths;
        this.props.dataout.data.rowHeights=rowhighs;
        let frozenCol;
        for(let i=0;i<colAligns.length;i++){
            if(colAligns[i]=='center'){
                frozenCol = i;
                break;
            }
        }
        this.props.dataout.data.fixedColumnsLeft = 0; //冻结
        this.props.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结
        this.props.dataout.data.freezing = {//固定表头
            "isFreeze" : true,
            "row": this.props.dataout.data.fixedRowsTop,
            "col" : this.props.dataout.data.fixedColumnsLeft
        }
        let quatyIndex = [];
        for (let i=0;i<colKeys.length;i++){
            if(colKeys[i].indexOf('quantity')>0){
                quatyIndex.push(i);
            }
        }


        if(mergeCells.length>0){
            this.props.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
        }
        let headAligns=[];

        for(let i=0;i<headcount+columheadrow;i++){
            for(let j=0;j<columHead.length;j++){
                headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
            }
        }
        this.props.dataout.data.cell= headAligns,
            this.props.dataout.data.cellsFn=function(row, col, prop) {
                let cellProperties = {};
                if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
                    cellProperties.align = colAligns[col];
                    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
                }
                return cellProperties;
            }
        this.props.dataout.data.checkTableHeadNum=(headcount-1).toString();
        this.setState({
            dataout:this.props.dataout,
            showModal: false,
            textAlginArr
        });
    }
    //直接输出
    printExcel=()=>{
        let { textAlginArr } = this.state;
        let { dataout } = this.props;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    getDetailPort = (paramObj) => {//联查"明细"
        let url = paramObj.url; //'/nccloud/gl/accountrep/triaccquery.do';
        let selectRow = this.getSelectRowData()[0];
        if(selectRow.link){
            let data = {
                ...this.state.paramObj,
                link: {...selectRow.link},
                pageindex: String(this.dataPage + 1),
                "class": "nccloud.pubimpl.gl.assbalance.AssbalLinkDetailParamTransfer", // 接口实现类路径
            };
            if (this.state.paramObj && this.state.paramObj.class) {// paramObj里有class，说明是联查进来的，需要封装原参数为origparam
                data = {
                    origparam : {...this.state.paramObj},
                    link: {...selectRow.link},
                    pageindex: String(this.dataPage + 1),
                    "class": "nccloud.pubimpl.gl.assbalance.AssbalLinkDetailParamTransfer", // 接口实现类路径
                };
            }
            this.jumpToDetail(data, paramObj);
        }
    }
    getSelectRowData=()=>{
        let selectRowData=this.refs.balanceTable.getRowRecord();
        return selectRowData;
    }
    jumpToDetail = (data, paramObj) => {
        let gziptools = new gzip();
        this.props.openTo(
            paramObj.jumpTo,
            {appcode: paramObj.appcode, status: gziptools.zip(JSON.stringify(data)),id: '12wew24', from: 'assistBalance'}
        )
    }
    handleSaveColwidth = ()=>{
        let {json}=this.state
        let info=this.refs.balanceTable.getReportInfo();
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.appcode,info.colWidths,json, callBack);
        
    }

    handleLoginBtn = (obj, btnName) => {
        if(btnName === 'linkdetail'){//联查明细
            this.getDetailPort({
                url: '/nccloud/gl/accountrep/assdetailquery.do',//src/gl/manageReport/assistDetailAccount/content
                jumpTo: '/gl/manageReport/assistDetailAccount/content/index.html',
                key: 'detail',
                appcode: queryRelatedAppcode(originAppcode.assdetail)
            })
        }else if(btnName === 'switch'){//转换
            this.handleSwitchColumn(this.state.dataWithPage)
        }else if(btnName === 'print'){//打印
            this.showPrintModal()
        }else if(btnName === 'templateOutput'){//模板输出
            this.showOutputModal()
        }else if(btnName === 'directprint'){//直接输出
            this.printExcel()
        }else if(btnName === 'saveformat'){//保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'refresh'){//刷新
            if (Object.keys(this.state.paramObj).length > 0) {
                this.modalSure(this.state.paramObj)
            }
        }else if(btnName === 'first'){//首页
            this.handlePage('firstPage', this.state.dataWithPage)
        }else if(btnName === 'pre'){//上一页
            this.pageType = 'prePage';
            this.handlePage('prePage', this.state.dataWithPage)
        }else if(btnName === 'next'){//下一页
            this.pageType = 'nextPage';
            this.handlePage('nextPage', this.state.dataWithPage)
        }else if(btnName === 'last'){//末页
            this.pageType = 'prePage';
            this.handlePage('lastPage', this.state.dataWithPage)
        }
    }
    firstCallback = () => {//首页
        this.handlePage('firstPage', this.state.dataWithPage)
    }
    preCallBack = () => {//上一页
        this.pageType = 'prePage';
        this.handlePage('prePage', this.state.dataWithPage)
    }
    nextCallBack = () => {//下一页
        this.pageType = 'nextPage';
        this.handlePage('nextPage', this.state.dataWithPage)
    }
    lastCallBack = () => {//末页
        this.pageType = 'prePage';
        this.handlePage('lastPage', this.state.dataWithPage)
    }
    render(){
        var a = 3;
        let { modal } = this.props;
        const { createModal } = modal;
        return(
            <div className="manageReportContainer" style={this.props.style}>
                <HeaderArea 
                    title = {this.state.json['20023055-000047']} /* 国际化处理： 辅助余额表*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <QueryModal
                                    hideBtnArea={this.state.hideBtnArea}
                                    defaultAccouontBook={this.state.defaultAccouontBook}
                                    onConfirm={(datas) => {
                                        this.modalSure(datas);
                                    }}
                                />
                            </div>
                            {//按钮集合
                                 this.props.button.createButtonApp({
                                    area: 'btnarea',
                                    buttonLimit: 3,
                                    onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName)
                                })
                            }
                        </div>
                    }
                    pageBtnContent = {
                        <PageButtonGroup
                            display={this.state.showPageBtn ? '' : 'none'}
                            first={{disabled: this.state.firstPageDisable, callBack: this.firstCallback}}
                            pre={{disabled: this.state.firstPageDisable, callBack: this.preCallBack}}
                            next={{disabled: this.state.lastPageDisable, callBack: this.nextCallBack}}
                            last={{disabled: this.state.lastPageDisable, callBack: this.lastCallBack}}
                        />
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.fiveLables.map((items) => {
                                return(
                                    <HeadCom
                                        labels={items.title}
                                        key={items.title}
                                        content = {this.state.dataWithPage.data && this.state.dataWithPage.data[this.dataPage]}
                                        lastThre = {this.state.dataWithPage.headtitle}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => this.setData(this.renderFirstData))
                                        }
                                        accountType = {this.state.accountType}
                                        isLong = {items.styleClass}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data = {this.props.dataout}
                        onCellMouseDown = {(e, coords, td) => {
                            this.whetherDetail('link');
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
                    {/* <div style={{
                        border: '1px solid rgb(204, 204, 204)',
                        borderRadius: '3px',
                        textAlign: 'center',
                        position: 'relative',
                        background: 'white',
                        height: '30px',
                        lineHeight: '30px',
                    }}>
                        {this.state.showPageBtn && this.state.dataWithPage.data && this.state.dataWithPage.data.length > 1 
                            ? `第${this.dataPage+1}页，共有数据${this.state.dataWithPage.data ? this.state.dataWithPage.data.length: 0}页` 
                            : ''}
                    </div> */}
                </div>
                <RepPrintModal
                    noCheckBox={true}
                    showScopeall={true}
                    appcode={this.state.appcode}
                    visible={this.state.visible}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={this.handleCancel.bind(this)}
                />
                <PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/accountrep/assbalanceoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
                {createModal('printService', {
                    className: 'print-service'
                })}
                <Iframe />
            </div>
        )
    }
}
AssistPropertyBalance = createPage({})(AssistPropertyBalance)
export default AssistPropertyBalance;
