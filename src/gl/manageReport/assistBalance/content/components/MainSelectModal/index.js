import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
//import moment from 'moment';

import {high,base,ajax, deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import '../../../../css/searchModal/index.less';
import FormLabel from '../../../../../public/components/FormLabel';


const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input, NCTooltip: Tooltip,
	NCRangePicker: RangePicker
} = base;

import { toast } from '../../../../../public/components/utils';

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';
const Timeformat = "YYYY-MM-DD HH:mm:ss";
// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import AccPeriodDefaultTreeGridRef from '../../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';
import NewModal from '../modal'
import AccountDefaultModelTreeRef from '../../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../../public/components/uapRefer.js';
import {
    renderMoneyType,
    getSubjectVersion,
    createReferFn,
    getCheckContent, delKeyOfObj,
    businessUnit, returnMoneyType, getReferDetault
} from '../../../../common/modules/modules';
import {handleNumberInput} from "../../../../common/modules/numberInputFn";
import {getAdjustTime} from '../../../../../public/hansonTableSetData/commonFn'
import SubjectVersion from "../../../../common/modules/subjectVersion";
import DateGroup from '../../../../common/modules/dateGroup';
import CheckBoxCells from '../../../../common/modules/checkBox';
import {handleValueChange} from '../../../../common/modules/handleValueChange'
import FourCheckBox from '../../../../common/modules/fourCheckBox'
import initTemplate from '../../../../modalFiles/initTemplate';
import {getRecover} from "../../../../common/modules/recoverData";
import { FICheckbox } from '../../../../../public/components/base';

class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state=this.getAllState()

		this.balanceMoney = this.balanceMoney.bind(this);//余额范围输入触发事件

        this.sureHandle = this.sureHandle.bind(this);
        this.closeNewModal = this.closeNewModal.bind(this);
        this.outInputChange = this.outInputChange.bind(this);

        this.renderTableFirstData = this.renderTableFirstData.bind(this);// table渲染初始的数据
        this.handleSearchObj = this.handleSearchObj.bind(this);//查询对象 选择触发事件
		this.findByKey = this.findByKey.bind(this);
		this.renderSearchRange = this.renderSearchRange.bind(this);//根据'查询对象'的选择来对应渲染'查询范围'的组件
		this.renderRefPathEle = this.renderRefPathEle.bind(this);//根据传进来的url渲染对应的参照组件
		this.handleCheckbox = this.handleCheckbox.bind(this);//计算小计	包含下级复选框的点击事件
		this.secondModalSelect = this.secondModalSelect.bind(this);//第二层弹框的选择框
		this.renderNewRefPath = this.renderNewRefPath.bind(this);//渲染第二层弹框内容的参照

		this.handleDateChange = this.handleDateChange.bind(this);//日期: 范围选择触发事件
		this.onChangeShowzerooccur = this.onChangeShowzerooccur.bind(this);//无发生不显示;本期无发生无余额不显示
		this.handleSortOrShowStyle = this.handleSortOrShowStyle.bind(this);//多核算账簿排序方式/多核算账簿显示方式
		this.handleDateRadio = this.handleDateRadio.bind(this);//会计期间/日期的单选框事件

		this.handleValueChange = handleValueChange.bind(this);
        this.handleNumberInput = handleNumberInput.bind(this);
        this.getRecover = getRecover.bind(this);
        this.searchId = '20023055query';
        this.index = '' //账簿打印行号
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                dateInputPlaceholder: json['20023055-000001'], ///* 国际化处理： 选择日期*/
                currtype: json['20023055-000002'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['20023055-000002'],  //选择的币种名称/* 国际化处理： 本币*/
            })
        }
        getMultiLang({moduleId:['20023055', 'dategroup', 'checkbox', 'fourcheckbox', 'subjectversion', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        if(!this.props.hideBtnArea){
			initTemplate.call(this,this.props, this.state.appcode)
        }
        this.getCurrtypeList();
        this.renderTableFirstData(10);
    }

    componentDidMount() {
        setTimeout(() => getReferDetault(this, true, {
            businessUnit,
            getCheckContent
        }, 'assistBalance',this.props.defaultAccouontBook),0)
        // setTimeout(() => initTemplate.call(this,this.props,this.state.appcode), 0)
        this.props.MainSelectRef && this.props.MainSelectRef(this)
    }
    //获取初始化state
	getInitState = () => {
		let initState = {
            defaultAccouontBook:this.props.defaultAccouontBook,
            appcode: '20023055',
		    // json: {},
            dateInputPlaceholder: '', ///* 国际化处理： 选择日期*/
            assistDateValue: '', //辅助参照中的日期的state值
            assistDateTimeValue: '', //辅助参照中的日期+时分秒的state值
            stringRefValue: '', //辅助参照中的字符的state值
            numberRefValue: '',//辅助参照中的数值的state值
            numberRefValueInteger: '',//辅助参照中整数值
            booleanValue: '', //辅助参照中布尔值
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            showRadio: true,//控制是否显示"会计期间"的但选框；false: 不显示
            versiondate: '', //存放选择的版本号
            versionDateArr: [], //存放请求账簿对应的版本数组
			showNewModal: false,
            isShowUnit:false,//是否显示业务单元
            busiDate: '', //业务日期

            renderData: [], //我的弹框里数据
			innerSelectName: [], //"查询范围"里存放的数据
            innerSelectData: [], //最里层选择数据的项
            rangefrom: '', //余额范围起始值
            rangeto: '', //余额范围结束值

            listItem:{},//业务单元中模板数据对应值

			selectedSort: 'true',//排序方式
			selectedShowType: 'false', //多主题显示方式
			doubleCell: false, // 多业务单元合并
			allLast: true, // 所有末级是否选中
			outProject: false, //表外科目是否选中
			multiProjectMerge: true, //多主题合并
			subjectShow: true, //按主题列示
			balanceMoneyLeft: '', //余额范围左边输入框
            balanceMoneyRight: '', //余额范围右边输入框
			accountingbook: this.props.defaultAccouontBook&&this.props.defaultAccouontBook.refpk?
            {refname:this.props.defaultAccouontBook.refname,refpk:this.props.defaultAccouontBook.refpk}
            :{
				refname: '',
				refpk: '',
			},  //核算账簿
			accountingUnit: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: [],//业务单元
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表

			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次
			startcode: {
				refname:'',
				refpk: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
			},    //结束科目编码

			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目

			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类

			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			/***无发生不显示;本期无发生无余额不显示***/
			showzerooccur: false, //无发生不显示
            ckNotShowZeroOccurZeroBalance: false, //本期无发生无余额不显示
			/***多核算账簿显示方式***/
            defaultShowStyle: 'false',
            /***多核算账簿排序方式***/
            defaultSortStyle: 'false',
            cbCorpSubjDspBase: false,//按各核算账簿显示科目名称
            cbQryByCorpAss: false,//按各核算账簿查询辅助核算

			upLevelSubject: false, //显示上级科目
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: false, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			start: {},  //开始期间
			end: {}, //结束期间

			/*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
			startyear: '', //开始年
            startperiod: '',  //开始期间
			endyear: '',  //结束年
			endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示

			isversiondate: false,
            pk_accperiodscheme: '',//会计期间参数
            indexArr: [], //存放"查询对象"已经选择的索引
			lastAssvos: [],//辅助核算对象整理后的数据
			//tabel组件头部

			//table的数据
			tableSourceData: [],
			//第二层弹框里的table

            secondData: [],//第二层表格的数据

            selectRow: [],//存放所选择的不是参照类型的数据
            page: 0, //第二个弹框中要显示的行内容索引
		}
		return initState
	}
	//state中添加json
	getAllState = () => {
		let initState = this.getInitState()
		initState.json={}
		return initState
	}
	//初始化state
    setInitState = (querystate) => {
		if(JSON.stringify(querystate) !=='{}') {
			this.setState(querystate)
		} else {
            let initState = this.getInitState()
            delKeyOfObj(initState, ['dateInputPlaceholder', 'currtype', 'currtypeName', 'currtypeList', 'tableSourceData'])
            this.setState(initState);
            getReferDetault(this, true, {
                businessUnit,
                getCheckContent
            }, 'assistBalance',this.props.defaultAccouontBook)
        }   
	}
    //打开高级查询面板
	openAdvSearch = (index, querystate) => {
        initTemplate.call(this,this.props, this.state.appcode).then((data)=>{
			this.props.search.openAdvSearch(this.searchId, true)
			this.index = index;
			this.setInitState(querystate);
		})
    }
    //获取返回父组件的值
    getConfirmData = (tempState) => {
        let pk_accountingbook = [];
        let pk_unit = [];
        let moneyLeft = Number(tempState.balanceMoneyLeft);
        let moneyRight = Number(tempState.balanceMoneyRight);
        // if(moneyLeft > moneyRight){
        //     Message.create({
        //         content: tempState.json['20023055-000024'],/* 国际化处理： 余额范围查询条件信息输入有误(起始值大于终止值或是为负值)！*/
        //         color: 'warning'
        //     });
        //     return;
        // }
        let lastAssvos = [];
        tempState.tableSourceData.forEach((item)=>{
            let resultObj = {};
            if(item.selectCell && !item.selectCellAttr){
                resultObj.name = item.selectCell.name;
                resultObj.pk_checktype = item.selectCell.pk_checktype;
                resultObj.pk_checkvalue = Array.isArray(item.selectRange) ? item.selectRange.join(',') : item.selectRange;
                resultObj.headEle = item.showLocal ? 'Y' : 'N';
                resultObj.accEle = item.accele ? 'Y': 'N';
                resultObj.includeSub = item.includesub ? 'Y' : 'N';
                lastAssvos.push(resultObj)
            } else if(item.selectCell && item.selectCellAttr){
                resultObj.name = item.selectCell.name;
                resultObj.pk_checktype = item.selectCell.pk_checktype;
                resultObj.attr = [];
                resultObj.headEle = item.showLocal ? 'Y' : 'N';
                resultObj.accEle = item.accele ? 'Y' : 'N';
                resultObj.includeSub = item.includesub ? 'Y' : 'N';
                item.selectCellAttr.map((itemCell) => {
                    if(itemCell.select){
                        resultObj.attr.push(itemCell);
                    }
                })
                lastAssvos.push(resultObj)
            }
        })
        if (Array.isArray(tempState.accountingbook)) {
            tempState.accountingbook.forEach(function (item) {
                pk_accountingbook.push(item.refpk)
            })
        }

        if (Array.isArray(tempState.buSecond)) {
            tempState.buSecond.forEach(function (item) {
                pk_unit.push(item.refpk)
            })
        }
        let confirmData = {
            /*******核算账簿****/
            pk_glorgbooks: pk_accountingbook,
            /*******业务单元****/
            pk_orgs: pk_unit,
            multiBusi: tempState.multbusi,//复选框是否勾选

            /*******科目版本****/
            useSubjVersion: tempState.isversiondate,//是否勾选
            subjVersion: tempState.isversiondate ? tempState.versiondate : tempState.busiDate, //tempState.versiondate,//科目版本

            /*******查询对象 table框的内容****/
            qryObjs: lastAssvos,

            /*******会计期间参数****/
            selectionState: tempState.selectionState,
            beginyear: tempState.startyear,
            beginperiod: tempState.startperiod,
            endyear: tempState.endyear,
            endperiod:   tempState.endperiod,
            /*******日期参数****/
            begindate: tempState.begindate,
            enddate: tempState.enddate,

            /*******包含凭证****/
            includeUnTallyed: tempState.includeuntally ? 'Y' : 'N',//未记账凭证
            includeError: tempState.includeerror ? 'Y' : 'N',// 错误凭证
            includeTransfer: tempState.includeplcf ? 'Y' : 'N',//损益结转凭证
            includeReClassify: tempState.includerc ? 'Y' : 'N',//重分类凭证

            /*******币种****/
            pk_currtype: tempState.currtype,//
            currTypeName: tempState.currtypeName,//

            /*******返回币种****/
            return_currtype: tempState.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局

            /*******余额范围****/
            balanceDirection: tempState.balanceori, //"双向/借/贷/平", //余额方向
            balanceRangeFrom: tempState.balanceMoneyLeft,
            balanceRangeTo: tempState.balanceMoneyRight,

            /*******无发生不显示;本期无发生无余额不显示****/
            ckShowZeroAmount: tempState.showzerooccur ,//无发生不显示
            ckNotShowZeroOccurZeroBalance: tempState.ckNotShowZeroOccurZeroBalance, //本期无发生无余额不显示true

            /*******多核算账簿显示方式;多核算账簿排序方式****/
            "rbCorpJun": tempState.mutibook, //多核算账簿显示方式：多核算账簿合并(true )

            "qryObjPlusCorp": tempState.defaultSortStyle, //多核算账簿排序方式：//查询对象+单位(true)
            "cbCorpSubjDspBase": tempState.cbCorpSubjDspBase, //按各核算账簿显示科目名称true
            "cbQryByCorpAss": tempState.cbQryByCorpAss, //按各核算账簿查询辅助核算true



            // pk_accasoa://条件框选择科目
            startcode: tempState.startcode.refpk,  //开始科目编码
            endcode: tempState.endcode.refpk,    //结束科目编码
            startlvl: tempState.startlvl,
            endlvl: tempState.endlvl,
            isleave: tempState.allLast ? 'Y' : 'N',
            isoutacc: tempState.isoutacc ? 'Y' : 'N',
            //未记账、错误、损益结转、重分类  Y/N  注意：‘包含凭证’是个标题，后面的才是选项
            mutibook: tempState.mutibook,
            showzerobalanceoccur: tempState.showzerobalanceoccur ? 'Y' : 'N',
            currplusacc: tempState.currplusacc,
            sumbysubjtype: tempState.sumbysubjtype ? 'Y' : 'N',
            twowaybalance: tempState.twowaybalance ? 'Y' : 'N',
            istree: 'Y',
            appcode: tempState.appcode,
            index: this.index
        }
        return confirmData
    }

    handleDateRadio(value, param){//会计期间/日期的单选框事件
    	this.setState({
			[param]: value
		})
	}
	handleSortOrShowStyle(value, param){//多核算账簿显示方式/多核算账簿排序方式触发事件
		this.setState({
			[param]: value
		})
		if(param === 'mutibook' && value === 'Y'){
			this.setState({
                showzerooccurEdit: true,
				ckNotShowZeroOccurZeroBalanceEdit: true,
                SortStyleTrueEdit: true,
				SortStyleFalseEdit: true,
                defaultSortStyle: 'false',
                cbCorpSubjDspBaseEdit: true,
                cbCorpSubjDspBase: false,
                showzerooccur: false,
                ckNotShowZeroOccurZeroBalance: false
			})
		}else if(param === 'mutibook' && value === 'N'){
            this.setState({
                showzerooccurEdit: false,
                ckNotShowZeroOccurZeroBalanceEdit: false,
                SortStyleTrueEdit: false,
                SortStyleFalseEdit: false,
                cbCorpSubjDspBaseEdit: false,
            })
		}
	}
    handleDateChange(value){//日期: 范围选择触发事件
    	this.setState({
            begindate: value[0],
            enddate: value[1],
			rangeDate: value
		})
	}
    handleSearchObj(value1, record){//查询对象选择具体科目
        let value = JSON.parse(value1)
    	let { tableSourceData, selectRow } = this.state;
        let originData = this.findByKey(record.key, tableSourceData);
        originData.selectCell = value;
        record.selctCells = [];
        record.selectRange = [];
        if(value.refpath ){
            originData.type = 'referpath';
            originData.searchRange = value.refpath;
		}else if(value.datatype === '1'){//字符
            originData.type = 'string';
        } else if(value.datatype === '4'){//整数
            originData.type = 'integer';
        } else if(value.datatype === '31'){//数值(小数)
            originData.type = 'number';
        } else if(value.datatype === '32'){//布尔
            originData.type = 'boolean';
        } else if(value.datatype === '33'){//日期
            originData.type = 'date'
        } else if(value.datatype === '34'){//日期+时分秒时间
            originData.type = 'dateTime'
        }else if(value.attr){
            originData.selectCellAttr = [...value.attr];
            originData.type = 'modal'
		}else {
            originData.type = ''
		}
		let newSelectRow = [...selectRow];
        newSelectRow[record.key] = record;
    	this.setState({
            tableSourceData,
			selectRow: newSelectRow
		})
	}
    handleCheckbox(key, record, value){//计算小计	包含下级复选框的点击事件
    	let {tableSourceData} = this.state;
        record[key] = value;
        this.setState({
            tableSourceData
		})

	}
    secondModalSelect(record, index){//第二层弹框的复选框
    	let {tableSourceData} = this.state;
        let newSelect = !record.select;
        record.select = newSelect;
        tableSourceData[record.itemKey].selectCellAttr[index] = record;
        this.setState({tableSourceData})
	}
    findByKey(key, rows) {
        let rt = null;
        let self = this;
        rows.forEach(function(v, i, a) {
            if (v.key == key) {
                rt = v;
            }
        });
        return rt;
    }
    dateRefChange = (key, value, record) => {
        let length = record.selectCell.inputlength;
        let exeReg = new RegExp("^.{0," + length + "}$");
        let result = exeReg.exec(value);
        if(result && key === 'stringRefValue'){
            record.selectRange = result[0]
            this.setState({
                [key]: result[0]
            })
        }else {
            record.selectRange = value
            this.setState({
                [key]: value
            })
        }
    }
    numberRefChange = (key, value, record, param) => {
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectRange = this.state[key], 0)

    }

    renderSearchRange(record, disabled, fieldid){//根据'查询对象'的选择来对应渲染'查询范围'的组件
    	switch(record.type){
			case "referpath":
				let refPathEle = this.renderRefPathEle(record, disabled, fieldid);
				return refPathEle;
			case 'date':
				return (
				    <DatePicker
                        fieldid={fieldid}
                        disabled={disabled}
                        format={format}
                        value={this.state.assistDateValue}
                        placeholder={this.state.dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateValue', value, record)}
                    />
                );
            case 'string':
                return <Input
                    fieldid={fieldid}
                    disabled={disabled}
                    value={this.state.stringRefValue}
                    onChange={(value) => this.dateRefChange('stringRefValue', value, record)}
                />;
            case 'number':
                return <Input
                    fieldid={fieldid}
                    disabled={disabled}
                    value={this.state.numberRefValue}
                    onChange={(value) => this.numberRefChange('numberRefValue', value, record, 'assistBalance') }
                />;
            case 'integer'://整数
                return <Input
                    fieldid={fieldid}
                    disabled={disabled}
                    value={this.state.numberRefValueInteger}
                    onChange={(value) => this.numberRefChange('numberRefValueInteger', value, record, 'assistBalanceInteger') }
                />;
            case 'boolean'://布尔
                return <Select
                    showClear={false}
                    fieldid={fieldid}
                    disabled={disabled}
                    value={this.state.booleanValue}
                    className='search-boolean'
                    onChange={(value)=>{
                        this.dateRefChange('booleanValue', value, record)
                    }}
                >
                    <NCOption value="Y">{this.state.json['20023055-000028']}</NCOption>{/* 国际化处理： 是*/}
                    <NCOption value="N">{this.state.json['20023055-000029']}</NCOption>{/* 国际化处理： 否*/}
                    <NCOption value="">{this.state.json['20023055-000030']}</NCOption>{/* 国际化处理： 空*/}
                </Select>;
            case 'dateTime'://日期+时分秒
                return (
                    <DatePicker
                        fieldid={fieldid}
                        disabled={disabled}
                        showTime={true}
                        format={Timeformat}
                        value={this.state.assistDateTimeValue}
                        placeholder={this.state.dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateTimeValue', value, record)}
                    />
                );
			case 'modal':
                let attrObj = record.selectCellAttr;
                let { tableSourceData } = this.state;
                let selectObj = [];
                attrObj.map((item)=>{
                	if(item){
                        item.itemKey = record.key;
					}
					if(item && item.selectName){
                        selectObj += item.selectName + ','
					}
				})
				return (
                    <div style={{display: 'flex'}}>
                        <Input
                            fieldid={fieldid}
                            disabled={disabled}
                            style={{border: 'none'}}
							placeholder={record.selectCell && record.selectCell.name}
							value={selectObj}
						/>
                        <div className='ellipsisContainer'
                             onClick={() => this.closeNewModal(record)}
                        >
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                        </div>
                    </div>
				);
			default:
				return <Input fieldid={fieldid} disabled/>;
		}
	}
    renderNewRefPath(record){
        let mybook;

        if(record.refpath){
            let objKey = record.attrclassid;
            if(!this.state[objKey]){//undefined
                {createScript.call(this,record.refpath+".js",objKey)}

            }else {
                mybook = (
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state[objKey] ? (this.state[objKey])(
                                    {
                                        value:{refname: record.selectName && record.selectName[0], refpk:  record.refpk && record.refpk[0]},
                                        isMultiSelectedEnabled: true,
                                        queryCondition: () => {
                                            return {
                                                "pk_accountingbook": this.state.accountingbook[0].pk_accountingbook ? this.state.accountingbook[0].pk_accountingbook : '',
                                                // "versiondate":currrentDate,
                                            }
                                        },
                                        onChange: (v) => {
                                            let {tableSourceData} = this.state;
                                            let refpkArr = [];
                                            let selectName = [];
                                            v.forEach((item) => {
                                                refpkArr.push(item.refpk);
                                                selectName.push(item.refname)
                                            })
                                            let pk_checkvalue = refpkArr.join(',')
                                            record.pk_checkvalue = pk_checkvalue;
                                            record.selectName = selectName;
                                            record.refpk = refpkArr;
                                            this.setState({
                                                tableSourceData
                                            })
                                        }
                                    }
                                ) : <div/>
                            }
                        </Col>
                    </Row>
                );
            }
		}

        return mybook;
	}
	renderRefPathEle(record, disabled, fieldid){//根据传进来的url渲染对应的参照组件
        //带有参照的
		let mybook;
		let objKey = record.key + record.selectCell.pk_checktype;
		if(!this.state[objKey]){//undefined
			{createScript.call(this,record.selectCell.refpath+".js",objKey)}

		}else{
			let defaultValue = [];
            let cellArr = !Array.isArray(record.selectRange) && record.selectRange.split(',');
            if(cellArr){
                cellArr.map((item, indx) => {
                	let cellObj = {refpk: ''};
                    cellObj.refpk = item;
                    defaultValue.push(cellObj)
				})
			}
            let pkOrgParam = this.state.buSecond.length > 0 ? this.state.buSecond[0].refpk : this.state.unitValueParam;
            let options = {
                fieldid: fieldid,
                value: record.selctCells,
                isMultiSelectedEnabled:true,
                disabled: disabled,
                "isShowDisabledData": true,
                queryCondition:() => {
                    return {
                        "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                        "pk_org": pkOrgParam,
                        "isDataPowerEnable": "Y",
                        "DataPowerOperationCode" : 'fi',
                        dateStr: this.state.isversiondate ? this.state.versiondate : this.state.busiDate
                    }
                },
                onChange: (v)=>{
                    let {tableSourceData} = this.state;
                    let refpkArr = [];
                    let selectCell = [];
                    v.forEach((item)=>{
                        selectCell.push(item)
                        refpkArr.push(item.refpk);
                    })
                    let pk_checkvalue = refpkArr.join(',')
                    record.selectRange = pk_checkvalue;
                    record.selctCells = selectCell;
                    this.setState({
                        tableSourceData
                    })
                }
            };
            let newOPtions = {}
            if(record.selectCell.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.selectCell.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                newOPtions = {
                    ...options,
                    isShowUnit:true,
                    disabled: disabled,
                    "unitValueIsNeeded":false,
                    "isShowDimission":true,
                    queryCondition:() => {
                        return {
                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                            "pk_org": pkOrgParam,
                            "isDataPowerEnable": "Y",
                            "DataPowerOperationCode" : 'fi',
                            "dateStr": this.state.isversiondate ? this.state.versiondate : this.state.busiDate,
                            "busifuncode":"all",
                            isShowDimission:true
                        }
                    },
                    unitProps:{
                        refType: 'tree',
                        refName: this.state.json['20023055-000014'], ///* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:this.state.json['20023055-000014'],refpk:'root'}, ///* 国际化处理： 业务单元*/
                        placeholder:this.state.json['20023055-000014'], ///* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[this.state.json['20023055-000015'], this.state.json['20023055-000016']],code: ['refcode', 'refname']}, ///* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false
                    },
                    unitCondition:{
                        pk_financeorg: pkOrgParam,
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    },
                }
            }else if( record.selectCell.classid && record.selectCell.classid.length === 20){
                newOPtions = {
                    ...options,
                    pk_defdoclist:record.selectCell.classid
                }
            }else {
                newOPtions = {
                    ...options
                }
            }
            mybook =  (
				<Row style={{
                    margin: "0px",
                    display: "flex",
                    alignItems: 'center'
                }}>
					<div>
						{
							this.state[objKey]?(this.state[objKey])(newOPtions):<div/>
						}
					</div>
				</Row>
			);
		}
		return mybook;
	}
    renderTableFirstData(length){
    	let firstData = [];
    	for(let i=0; i<length; i++){
    		let obj = {};
    		obj.searchObj = [];
            obj.searchRange = '';
            obj.showLocal = false;//显示位置
            obj.accele = false;//小计
            obj.includesub = false,//包含下级
			obj.key = i
            firstData.push(obj);
		}
		this.setState({
            tableSourceData: [...firstData]
		})
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}




	getCurrtypeList = () => {//获取"币种"接口
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {"localType":"1", "showAllCurr":"1"};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
                        currtypeList: data,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


    handleCurrtypeChange = (keyParam, value) => {
        this.setState({
            [keyParam]: value,
        });
    }


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(v) {
		if (!this.state.accountingbook || !this.state.accountingbook[0]) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}
		
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;

		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	

		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        		versiondate: '2018-5-31', //data.versiondate,
		        	},)

                    getSubjectVersion(self);

                    let newUrl = '/nccloud/gl/accountrep/assbalancequeryobject.do';//辅助余额表查询对象
                    let newData = {
                        "pk_accountingbook":v[0].refpk,
                        "versiondate": '2018-5-31',//self.state.versiondate,
                        "needaccount": true
                    };
                    ajax({
                        url:newUrl,
                        data:newData,
                        success: function(response){
                            const { success } = response;
                            //渲染已有账表数据遮罩
                            if (success) {
                                if(response.data){
                                    response.data.map((item) => {
                                    	if(item.attr){
                                            item.attr.map((cell) => {
                                                cell.select = false
											})
										}
									})

                                    let newTableFirstData = [...self.state.tableSourceData];
                                    newTableFirstData.forEach((item, index) => {
                                        item.searchObj = response.data;
									})

                                    self.setState({
                                        tableSourceData: newTableFirstData
                                    })
                                }
                            }
                        }
                    });
		        } else {
		            Message.create({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: '2018-5-31'// value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化/本期无发生无余额不显示;按各核算账簿显示科目名称/按各核算账簿查询辅助核算
	onChangeShowzerooccur(value, param) {
		this.setState({
			[param]: value
		});
    	//'showzerooccur' ckNotShowZeroOccurZeroBalance
		if(param === 'showzerooccur' && value){
			this.setState({
                ckNotShowZeroOccurZeroBalance: value,//本期无发生无余额不显示
                ckNotShowZeroOccurZeroBalanceEdit: value,
			})
		}else if(param === 'showzerooccur' && !value){
			this.setState({
                ckNotShowZeroOccurZeroBalanceEdit: value
			})
		}
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}
	//显示上级科目复选框变化
	onChangeupLevelSubject(e) {
		this.setState({upLevelSubject: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}
	handleRadioSort(param){
		this.setState({
			selectedSort: param
		})
	}
	handleShowType(param){
		this.setState({
			selectedShowType: param
		})
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
	onChangeDoubleCell(param){
		this.setState({
			doubleCell: !this.state.doubleCell
		})
	}
	changeProjectStyle(param){//所有末级   表外科目选择
		if(param === 'allLast'){
			this.setState({
				allLast: !this.state.allLast
			})
		}else{
			this.setState({
				outProject: !this.state.outProject
			})
		}
	}

	handleGTypeChange =() =>{
		
	};




    outInputChange(param){
    	let nameArr = [];
        param.forEach((item) => {
            nameArr.push(item.refname);
		})
		this.setState({
            innerSelectName: [...nameArr]
		})
	}
    sureHandle(param){
    	let SelectCell = [];
        // param.forEach((item) => {
        	// if(item.pk_checkvalue){
         //        SelectCell.push(item);
		// 	}
		// });
        this.setState({
			innerSelectData: [...SelectCell]
		})
    	this.closeNewModal(param);
	}
    closeNewModal(param){
    	this.setState({
            showNewModal: !this.state.showNewModal,
			page: param.key
		})
    }

    balanceMoney(value, param){
        let regExp = /^(\-)?\d*\.{0,1}\d{0,4}$/
        let result = regExp.exec(value)
        if(result){
            this.setState({
                [param]: result[0]
            })
        }

    }

    renderModalList = () => {
        let { status, hideBtnArea } = this.props
		let disabled = status === 'browse' ? true : false
        let index = this.state.index;
        let item = this.state.item;
        let versionArr = Array.isArray(this.state.versiondate);
        let accountbookParam = [];
        this.state.accountingbook[0] && this.state.accountingbook.map((item, indx) => {
            accountbookParam.push(item.refpk);
        })
        this.columns = [
            {
                title: (<div fieldid='searchObj'>{this.state.json['20023055-000003']}</div>),/* 国际化处理： 查询对象*/
                dataIndex: "searchObj",
                key: "searchObj",
                render: (text, record, index) => {
                    let renderArr = [];
                    text.map((item, index) => {
                        renderArr.push(<NCOption value={JSON.stringify(item)} key={index}>{item.name}</NCOption>)
                    })
                    return (
                        <Select
                            showClear={false}
                            fieldid='searchObj'
                            disabled={disabled}
                            value={record.selectCell && record.selectCell.name}
                            onChange = {(value) => this.handleSearchObj(value, record)}
                        >
                            {
                                renderArr
                            }
                        </Select>
                    )
                }
            },
            {
                title: (<div fieldid='searchRange'>{this.state.json['20023055-000004']}</div>),/* 国际化处理： 查询范围*/
                dataIndex: "searchRange",
                key: "searchRange",
                render: (text, record,index) => {
                    let renderEle = this.renderSearchRange(record, disabled,'searchRange');
                    return renderEle;
                }
            },
            {
                title: (<div fieldid='showLocal'>{this.state.json['20023055-000005']}</div>),/* 国际化处理： 显示位置*/
                dataIndex: "showLocal",
                key: "showLocal",
                width : 65,
                render: (text, record) => {
                    return (
                        <Select
                            showClear={false}
                            fieldid='showLocal'
                            disabled={disabled}
                            defaultValue = 'N'
                            value={record.showLocal ? 'Y' : 'N'}
                            onChange={(value) => {
                                record.showLocal = value == 'Y' ? true : false;
                                this.setState({})
                            }}>
                            <NCOption value='Y'>{this.state.json['20023055-000026']}</NCOption>{/* 国际化处理： 表头*/}
                            <NCOption value='N'>{this.state.json['20023055-000027']}</NCOption>{/* 国际化处理： 表体*/}
                        </Select>
                    )
                }
            },
            {
                title: (<div fieldid='accele'>{this.state.json['20023055-000006']}</div>),/* 国际化处理： 计算小计*/
                dataIndex: "accele",
                key: "accele",
                width : 65,
                render: (text, record) => {
                    return <div fieldid='accele'><Checkbox
                        checked={record.accele}
                        disabled={disabled ? disabled : record.showLocal}
                        onChange={(value) => {
                            this.handleCheckbox('accele',record, value)
                        }}
                    /></div>
                }
            },
            {
                title: (<div fieldid='includesub'>{this.state.json['20023055-000007']}</div>),/* 国际化处理： 包含下级*/
                dataIndex: "includesub",
                key: "includesub",
                width : 65,
                render: (text, record) => {
                    let tempd = (record.selectCell && record.selectCell.name === this.state.json['20023055-000008']) ? true : false/* 国际化处理： 会计科目*/
                    return <div fieldid='includesub'><Checkbox
                        checked={record.includesub}
                        disabled={disabled ? disabled : tempd}
                        onChange={(value) => {
                            this.handleCheckbox('includesub',record, value)
                        }}
                    /></div>
                }
            }
        ];
        this.secondColumn = [
            {
                title: this.state.json['20023055-000009'],/* 国际化处理： 选择*/
                dataIndex: "select",
                key: "select",
                render: (text, record, index) => {
                    return <Checkbox
                        checked={record.select}
                        onChange={() => {
                            record.index = index;
                            this.secondModalSelect(record, index);
                        }}
                    />
                }
            },
            {
                title: this.state.json['20023055-000010'],/* 国际化处理： 查询属性*/
                dataIndex: "name",
                key: "name"
            },
            {
                title: this.state.json['20023055-000011'],/* 国际化处理： 查询值*/
                dataIndex: "refpath",
                key: "refpath",
                render: (text, record) => {
                    let renderSecondEle = this.renderNewRefPath(record);
                    return renderSecondEle;
                }
            }
        ]
		return (
            <div className='right_query'>
                <div className='query_body1 assistBalance'>
                    <div className='query_form balanced'>
			<div>
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel isRequire={true} labelname={this.state.json['20023055-000031']}/>{/* 国际化处理： 核算账簿*/}
                    </Col>
                    <Col md={7} sm={7}>
                        <div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: true,
                                        disabled: disabled,
                                        showGroup: true,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                            multiGroup : 'Y'
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit ,
                                        getCheckContent: getCheckContent,
                                        renderTableFirstData: () => this.renderTableFirstData(10)
                                    },
                                    'assistBalance'
                                )
                            }
                        </div>
                    </Col>
                </Row>
                {
                    this.state.isShowUnit ?
                        <Row className="myrow">
                            <Col md={2} sm={2}>
                                <FormLabel labelname={this.state.json['20023055-000014']} />{/* 国际化处理： 业务单元*/}
                            </Col>
                            <Col md={7} sm={7}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
                                        fieldid='buSecond'
                                        value={this.state.buSecond}
                                        isMultiSelectedEnabled= {true}
                                        isShowDisabledData = {true}
                                        isHasDisabledData = {true}
                                        disabled = {disabled}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                        }}
                                        onChange={(v)=>{
                                            this.getRecover('tableSourceData', 'searchRange', 'selectCells', 'selectCell', 'type', 'accele', 'includesub', 'showLocal')
                                            //
                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if(this.state.accountingbook && this.state.accountingbook[0]) {
                                                }
                                            })
                                        }
                                        }
                                    />

                                </div>

                            </Col>
                            <Col md={3} sm={3}>
                                <FICheckbox colors="dark" className="mycheck" disabled={disabled} checked={this.state.multbusi}  onChange={this.onChangeMultbusi.bind(this)}>
                                {this.state.json['20023055-000032']} {/* 国际化处理： 多业务单元合并*/}
                                </FICheckbox>
                            </Col>
                        </Row> :
                        <div></div>
                }

                {/*启用科目版本：*/}
                <SubjectVersion
                    checked = {this.state.isversiondate}//复选框是否选中
                    data={this.state.versionDateArr}//
                    value={this.state.versiondate}
                    CheckboxDisabled = {disabled}
                    disabled={disabled ? disabled : !this.state.isversiondate}
                    renderTableFirstData = {() => this.renderTableFirstData(10)}
                    handleValueChange = {(key, value) => this.handleValueChange(key, value, 'assistBalance', getCheckContent)}
                />

                <div className="modalTable">
                    <Table
                        columns={this.columns}
                        data={this.state.tableSourceData}
                    />
                </div>



                {/*会计期间*/}
                {
                    hideBtnArea ? ''
                    :
                    <DateGroup
                        selectionState = {this.state.selectionState}
                        start = {this.state.start}
                        end = {this.state.end}
                        enddate = {this.state.enddate}
                        pk_accperiodscheme = {this.state.pk_accperiodscheme}
                        pk_accountingbook = {this.state.accountingbook[0]}
                        rangeDate = {this.state.rangeDate}
                        handleValueChange = {this.handleValueChange}
                        handleDateChange = {this.handleDateChange}
                        self = {this}
                        showRadio = {this.state.showRadio}
                    />
                }
                

                {/*包含凭证：*/}
                <CheckBoxCells
                    paramObj = {{
                        "disabled": disabled,
                        "noChargeVoucher": {//未记账凭证
                            show: true,
                            disabled: !this.state.isqueryuntallyed,
                            checked: this.state.includeuntally,
                            onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                        },
                        "errorVoucher": {//错误凭证
                            show: true,
                            checked: this.state.includeerror,
                            onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                        },
                        "harmToBenefitVoucher": {//损益结转凭证
                            show: true,
                            checked: this.state.includeplcf,
                            onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                        },
                        "againClassifyVoucher": {//重分类凭证
                            show: true,
                            checked: this.state.includerc,
                            onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                        },
                    }}
                />

                {/*币种*/}
                {renderMoneyType(this.state.currtypeList,this.state.currtypeName, this.handleCurrtypeChange, this.state.json, disabled)}

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
                        this.state.json,
                        disabled
                    )
                }

                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel labelname={this.state.json['20023055-000033']} />{/* 国际化处理： 余额范围*/}
                    </Col>
                    <Col md={3} sm={3}>

                        <div className="book-ref">
                            <Select
                                showClear={false}
                                fieldid='balanceori'
                                value={this.state.balanceori}
                                disabled={disabled}
                                // style={{ width: 80, marginRight: 6 }}
                                onChange={this.handleBalanceoriChange.bind(this)}
                            >
                                <NCOption value={'-1'} key={'-1'} >{this.state.json['20023055-000034']}</NCOption>{/* 国际化处理： 双向*/}
                                <NCOption value={'0'} key={'0'} >{this.state.json['20023055-000035']}</NCOption>{/* 国际化处理： 借*/}
                                <NCOption value={'1'} key={'1'} >{this.state.json['20023055-000036']}</NCOption>{/* 国际化处理： 贷*/}
                                <NCOption value={'3'} key={'3'} >{this.state.json['20023055-000037']}</NCOption>{/* 国际化处理： 平*/}
                            </Select>
                        </div>
                    </Col>
                    <Col md={1} sm={1}></Col>
                    <Col md={6} >
                        <Row className='balanceContainer'>
                            <Col md={5} >
                                <Input
                                    fieldid='startBalance'
                                    disabled={disabled}
                                    value={this.state.balanceMoneyLeft}
                                    onChange={(value) => this.balanceMoney(value, 'balanceMoneyLeft')}
                                />

                            </Col>
                            <Col md={2} sm={2} style={{lineHeight: '36px'}}><p className='zhi nc-theme-form-label-c'>～</p></Col>
                            <Col md={5} >
                                <Input
                                    fieldid='endBlance'
                                    disabled={disabled}
                                    value={this.state.balanceMoneyRight}
                                    onChange={(value) => this.balanceMoney(value, 'balanceMoneyRight')}
                                />
                            </Col>
                        </Row>
                    </Col>
                </Row>

                {/*忽略日期选项*/}
                <FourCheckBox
                    totalTitle= {this.state.json['20023055-000017']} ///* 国际化处理： 显示属性：*/
                    renderCells={
                        [
                            {
                                title: this.state.json['20023055-000018'], ///* 国际化处理： 无发生不显示*/
                                checked: this.state.showzerooccur,
                                disabled: disabled ? disabled : this.state.showzerooccurEdit,
                                id: 'noHappenNoShow',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'showzerooccur')
                            },
                            {
                                title: this.state.json['20023055-000019'], ///* 国际化处理： 本期无发生无余额不显示*/
                                checked:this.state.ckNotShowZeroOccurZeroBalance,
                                disabled:disabled ? disabled : this.state.ckNotShowZeroOccurZeroBalanceEdit,
                                id: 'noHappenNoBalanceNoShow',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'ckNotShowZeroOccurZeroBalance')
                            },
                        ]
                    }
                />

                {/* <div className='special'> */}
                    <Row className="myrow specialrow-lable">
                        <Col md={2} sm={2}>
                            <FormLabel labelname={this.state.json['20023055-000038']} />{/* 国际化处理： 多核算账簿显示方式*/}
                        </Col>
                        <Col md={8} sm={8}>
                            <Radio.NCRadioGroup
                                selectedValue={this.state.mutibook}
                                onChange={(value) => this.handleSortOrShowStyle(value, 'mutibook')}
                            >
                                <Radio value="Y" disabled={disabled ? disabled : this.state.disMutibook}>{this.state.json['20023055-000039']}</Radio>{/* 国际化处理： 多核算账簿合并*/}
                                <Radio value="N" disabled={disabled ? disabled : this.state.disMutibook}>{this.state.json['20023055-000040']}</Radio>{/* 国际化处理： 按核算账簿列示*/}
                            </Radio.NCRadioGroup>

                        </Col>
                    </Row>
                    <Row className="myrow specialrow-lable">
                        <Col md={2} sm={2}>
                            <FormLabel labelname={this.state.json['20023055-000041']} />{/* 国际化处理： 多核算账簿排序方式*/}
                        </Col>
                        <Col md={8} sm={8}>

                            <Radio.NCRadioGroup
                                selectedValue={this.state.defaultSortStyle}
                                onChange={(value) => this.handleSortOrShowStyle(value, 'defaultSortStyle')}
                            >
                                <Radio value="false" disabled={disabled ? disabled : this.state.SortStyleFalseEdit}>
                                    {`${this.state.json['20023055-000025']}+${this.state.json['20023055-000003']}`}
                                </Radio>{/* 国际化处理： 核算账簿,查询对象*/}

                                <Radio value="true" disabled={disabled ? disabled : this.state.SortStyleTrueEdit}>
                                    {`${this.state.json['20023055-000003']}+${this.state.json['20023055-000025']}`}
                                </Radio>{/* 国际化处理： 查询对象,核算账簿*/}
                            </Radio.NCRadioGroup>

                        </Col>
                    </Row>

                    <FourCheckBox
                        totalTitle= {this.state.json['20023055-000017']} ///* 国际化处理： 显示属性：*/
                        renderCells={
                            [
                                {
                                    title: this.state.json['20023055-000020'], ///* 国际化处理： 按各核算账簿显示科目名称*/
                                    checked: this.state.cbCorpSubjDspBase,
                                    disabled:disabled ? disabled : this.state.cbCorpSubjDspBaseEdit,
                                    id: 'cbCorpSubjDspBase',
                                    onChange: (value) => this.onChangeShowzerooccur(value, 'cbCorpSubjDspBase')
                                },
                                {
                                    title: this.state.json['20023055-000021'], ///* 国际化处理： 按各核算账簿查询辅助核算*/
                                    checked:this.state.cbQryByCorpAss,
                                    id: 'cbQryByCorpAss',
                                    disabled: disabled,
                                    onChange: (value) => this.onChangeShowzerooccur(value, 'cbQryByCorpAss')
                                },
                            ]
                        }
                    />
                {/* </div> */}

                <NewModal
                    title= {this.state.json['20023055-000022']} ///* 国际化处理： 属性选择*/
                    record={this.state.selectRow[this.state.page]}
                    column={this.secondColumn}
                    showNewModal = {this.state.showNewModal}//控制设否显示
                    closeNewModal = {this.closeNewModal}
                    sureHandle = {this.sureHandle}
                    sureText = {this.state.json['20023055-000023']} ///* 国际化处理： 确定 */
                    cancleText = {this.state.json['20023055-000013']} ///* 国际化处理： 取消*/
                />
			</div>
                    </div>
                </div>
            </div>
		)
    }
    clickPlanEve = (value) => {
        let { status } = this.props;
        if (status !== 'browse') {
            this.state = deepClone(value.conditionobj4web.nonpublic);
            this.setState(this.state)
        }
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
        return {...this.state}
    }
    // clickSearchBtn = (data) => {//点击查询框的"查询"按钮事件
    //     let { hideBtnArea } = this.props
    //     if(hideBtnArea){
    //         this.props.onConfirm(data, this.state)
    //     } else {
    //         this.props.onConfirm(data)
    //     }

    // }
    //点击查询框的"查询"按钮事件
    clickSearchBtn = () =>{   
        let { hideBtnArea, status } = this.props
        // let pk_accountingbook = [];
        // let pk_unit = [];
        // let moneyLeft = Number(this.state.balanceMoneyLeft);
        // let moneyRight = Number(this.state.balanceMoneyRight);
        // if(moneyLeft > moneyRight){
        //     Message.create({
        //         content: this.state.json['20023055-000024'],/* 国际化处理： 余额范围查询条件信息输入有误(起始值大于终止值或是为负值)！*/
        //         color: 'warning'
        //     });
        //     return;
        // }
        // let lastAssvos = [];
        // this.state.tableSourceData.forEach((item)=>{
        //     let resultObj = {};
        //     if(item.selectCell && !item.selectCellAttr){
        //         resultObj.name = item.selectCell.name;
        //         resultObj.pk_checktype = item.selectCell.pk_checktype;
        //         resultObj.pk_checkvalue = Array.isArray(item.selectRange) ? item.selectRange.join(',') : item.selectRange;
        //         resultObj.headEle = item.showLocal ? 'Y' : 'N';
        //         resultObj.accEle = item.accele ? 'Y': 'N';
        //         resultObj.includeSub = item.includesub ? 'Y' : 'N';
        //         lastAssvos.push(resultObj)
        //     } else if(item.selectCell && item.selectCellAttr){
        //         resultObj.name = item.selectCell.name;
        //         resultObj.pk_checktype = item.selectCell.pk_checktype;
        //         resultObj.attr = [];
        //         resultObj.headEle = item.showLocal ? 'Y' : 'N';
        //         resultObj.accEle = item.accele ? 'Y' : 'N';
        //         resultObj.includeSub = item.includesub ? 'Y' : 'N';
        //         item.selectCellAttr.map((itemCell) => {
        //             if(itemCell.select){
        //                 resultObj.attr.push(itemCell);
        //             }
        //         })
        //         lastAssvos.push(resultObj)
        //     }
        // })
        // if (Array.isArray(this.state.accountingbook)) {
        //     this.state.accountingbook.forEach(function (item) {
        //         pk_accountingbook.push(item.refpk)
        //     })
        // }

        // if (Array.isArray(this.state.buSecond)) {
        //     this.state.buSecond.forEach(function (item) {
        //         pk_unit.push(item.refpk)
        //     })
        // }
        // let data = {
        //     /*******核算账簿****/
        //     pk_glorgbooks: pk_accountingbook,
        //     /*******业务单元****/
        //     pk_orgs: pk_unit,
        //     multiBusi: this.state.multbusi,//复选框是否勾选

        //     /*******科目版本****/
        //     useSubjVersion: this.state.isversiondate,//是否勾选
        //     subjVersion: this.state.isversiondate ? this.state.versiondate : this.state.busiDate, //this.state.versiondate,//科目版本

        //     /*******查询对象 table框的内容****/
        //     qryObjs: lastAssvos,

        //     /*******会计期间参数****/
        //     selectionState: this.state.selectionState,
        //     beginyear: this.state.startyear,
        //     beginperiod: this.state.startperiod,
        //     endyear: this.state.endyear,
        //     endperiod:   this.state.endperiod,
        //     /*******日期参数****/
        //     begindate: this.state.begindate,
        //     enddate: this.state.enddate,

        //     /*******包含凭证****/
        //     includeUnTallyed: this.state.includeuntally ? 'Y' : 'N',//未记账凭证
        //     includeError: this.state.includeerror ? 'Y' : 'N',// 错误凭证
        //     includeTransfer: this.state.includeplcf ? 'Y' : 'N',//损益结转凭证
        //     includeReClassify: this.state.includerc ? 'Y' : 'N',//重分类凭证

        //     /*******币种****/
        //     pk_currtype: this.state.currtype,//
        //     currTypeName: this.state.currtypeName,//

        //     /*******返回币种****/
        //     return_currtype: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局

        //     /*******余额范围****/
        //     balanceDirection: this.state.balanceori, //"双向/借/贷/平", //余额方向
        //     balanceRangeFrom: this.state.balanceMoneyLeft,
        //     balanceRangeTo: this.state.balanceMoneyRight,

        //     /*******无发生不显示;本期无发生无余额不显示****/
        //     ckShowZeroAmount: this.state.showzerooccur ,//无发生不显示
        //     ckNotShowZeroOccurZeroBalance: this.state.ckNotShowZeroOccurZeroBalance, //本期无发生无余额不显示true

        //     /*******多核算账簿显示方式;多核算账簿排序方式****/
        //     "rbCorpJun": this.state.mutibook, //多核算账簿显示方式：多核算账簿合并(true )

        //     "qryObjPlusCorp": this.state.defaultSortStyle, //多核算账簿排序方式：//查询对象+单位(true)
        //     "cbCorpSubjDspBase": this.state.cbCorpSubjDspBase, //按各核算账簿显示科目名称true
        //     "cbQryByCorpAss": this.state.cbQryByCorpAss, //按各核算账簿查询辅助核算true



        //     // pk_accasoa://条件框选择科目
        //     startcode: this.state.startcode.refpk,  //开始科目编码
        //     endcode: this.state.endcode.refpk,    //结束科目编码
        //     startlvl: this.state.startlvl,
        //     endlvl: this.state.endlvl,
        //     isleave: this.state.allLast ? 'Y' : 'N',
        //     isoutacc: this.state.isoutacc ? 'Y' : 'N',
        //     //未记账、错误、损益结转、重分类  Y/N  注意：‘包含凭证’是个标题，后面的才是选项
        //     mutibook: this.state.mutibook,
        //     showzerobalanceoccur: this.state.showzerobalanceoccur ? 'Y' : 'N',
        //     currplusacc: this.state.currplusacc,
        //     sumbysubjtype: this.state.sumbysubjtype ? 'Y' : 'N',
        //     twowaybalance: this.state.twowaybalance ? 'Y' : 'N',
        //     istree: 'Y',
        //     appcode: this.state.appcode,
        //     index: this.index
        // }
        if(status !== 'browse'){
            let data = this.getConfirmData(this.state)
            let url = '/nccloud/gl/accountrep/checkparam.do'
            let flagShowOrHide;
            ajax({
                url,
                data,
                async:false,
                success:function(response){
                    flagShowOrHide = true
                },
                error:function(error){
                    flagShowOrHide = false
                    toast({content: error.message, color: 'warning'});
                }
            })
            if(flagShowOrHide == true){
                if(hideBtnArea){
                    this.props.onConfirm(data, this.state)
                } else {
                    this.props.onConfirm(data)
                }
                // this.clickSearchBtn(data)
            }else if(flagShowOrHide == false){
                return true
            } 
        } else {
            return
        }
            
    }
	render() {
		let { search, hideBtnArea, status } = this.props;
        let { NCCreateSearch } = search;
        let isHideBtnArea = typeof(hideBtnArea) === 'boolean' && hideBtnArea ? hideBtnArea : false
		return <div>
            {
                NCCreateSearch(this.searchId, {
                    onlyShowSuperBtn:true,                       // 只显示高级按钮
                    replaceSuperBtn:this.state.json['20023055-000012'],                      // 自定义替换高级按钮名称/* 国际化处理： 查询*/
                    replaceRightBody:this.renderModalList,
                    hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
                    clickPlanEve:this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                    saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
                    oid:this.props.meta.oid,
                    hideBtnArea: isHideBtnArea,
                    clickSearchBtn: this.clickSearchBtn,  // 点击按钮事件
                    isSynInitAdvSearch: true,//渲染右边面板自定义区域
                    // showAdvSearchPlanBtn: status==='browse' ? false : true, // 高级面板中是否显示保存方案按钮
                    searchBtnName: isHideBtnArea ? this.state.json['20023055-000023'] : this.state.json['20023055-000012'], /* 国际化处理： 确定*//* 国际化处理： 查询*/
                })}
		</div>
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal);
export default CentralConstructorModal
