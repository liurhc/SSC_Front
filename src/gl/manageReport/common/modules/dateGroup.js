/**
 * Created by liqiankun on 2018/7/31.
 * 会计期间；日期的组件
 */


import React, {Component} from 'react';
import {high,base,ajax, deepClone, createPage, getMultiLang} from 'nc-lightapp-front';
import AccPeriodDefaultTreeGridRef from '../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';

const {
    NCRadio:Radio, NCRow: Row, NCCol: Col,
    NCRangePicker: RangePicker
} = base;


import {accountPeriodEnd, accountPeriodStart} from "./modules";
const format = 'YYYY-MM-DD';

import '../../css/searchModal/index.less';
import '../../../public/reportcss/searchmodalpage.less'


class DateGroup extends Component{
    constructor(props){
        super(props);
        this.state = {
            json: {}
        }
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'dategroup',domainName:'gl',currentLocale:'simpchn',callback});
    }
    render(){
        const radioStyle = {
            display: 'block'
        };
        let {
            selectionState, start, end, enddate,showRadio,
            pk_accperiodscheme, rangeDate, handleValueChange,
            handleDateChange, self, showPeriod, showDate, pk_accountingbook
        } = this.props;
        let tempColNum = showRadio ? 12 : 10
        return(
          //  <div className='dateGroupContainer'>
                <div style={{display: 'flex'}} className={ `nc-theme-area-bgc nc-theme-area-split-bc ${showPeriod ? 'dateGroupContainer1' : 'dateGroupContainer'} `}>
                <div>
                <Radio.NCRadioGroup
                    style={{display: !showRadio ? 'none' : '', width: '135px', marginTop: '6px'}}
                    // className='dateRadioGroup really'
                    selectedValue={selectionState}
                    onChange={(value) => handleValueChange('selectionState', value)}
                >
                    {/* <Row className='accountRadioRow'> */}
                        <Radio value='true' className='accountantRadio' fieldid={this.state.json['dategroup-000000']}>{this.state.json['dategroup-000000']}：</Radio>
                    {/* </Row> */}

                    {/* <Row className='dateRadioRow'> */}
                        <Radio value="false" className='dateRadio' fieldid={this.state.json['dategroup-000001']}>{this.state.json['dategroup-000001']}：</Radio>
                    {/* </Row> */}

                </Radio.NCRadioGroup>
                </div>
                <div style={{flex: 1}}>
                <Row
                    className={`myrow ${showRadio ? 'accountRow' : 'noPositionDateRow'}` }
                    style={{display: !showPeriod ? '' : 'none'}}
                >
                    <Col md={2} sm={2} style={{display: !showRadio ? '' : 'none'}}>
                        <span className='nc-theme-form-label-c'>{this.state.json['dategroup-000000']}：</span>{/* 国际化处理： 会计期间*/}
                    </Col>
                    <Col md={tempColNum} sm={tempColNum}>
                    
                        <Col md={5} sm={5}>

                            <AccPeriodDefaultTreeGridRef
                                fieldid='begindate'
                                disabled = {selectionState === 'true' ? false : true}
                                value={start}
                                queryCondition = {{
                                    // "GridRefActionExt": "nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder",
                                    "pk_accperiodscheme": pk_accperiodscheme,
                                    "GridRefActionExt": "nccloud.web.gl.ref.AccperiodGridRefSqlBuilder",
                                    "TreeRefActionExt": "nccloud.web.gl.ref.AccperiodTreeRefSqlBuilder",
                                    "pk_accountingbook": pk_accountingbook && (pk_accountingbook.pk_accountingbook || pk_accountingbook.refpk)
                                }}
                                onChange={(v)=>{
                                    accountPeriodStart(self, v)
                                }}
                            />
                        </Col>
                        <Col md={1} sm={1}><p className='zhi nc-theme-form-label-c'>~</p> </Col>
                        <Col md={5} sm={5}>
                            <AccPeriodDefaultTreeGridRef
                                fieldid='enddate'
                                disabled = {selectionState === 'true' ? false : true}
                                value={end}
                                queryCondition = {{
                                    "pk_accperiodscheme": pk_accperiodscheme,
                                    "includeAdj": "Y",
                                    "GridRefActionExt": "nccloud.web.gl.ref.AccperiodGridRefSqlBuilder",
                                    "TreeRefActionExt": "nccloud.web.gl.ref.AccperiodTreeRefSqlBuilder",
                                    "pk_accountingbook": pk_accountingbook && (pk_accountingbook.pk_accountingbook || pk_accountingbook.refpk)
                                }}
                                onChange={(v)=>{

                                    accountPeriodEnd(self, v)
                                }}
                            />
                        </Col>
                    </Col>
                </Row>

                <Row
                    className = {`myrow ${showRadio ? 'dateRow' : 'noPositionDateRowDate'}`}
                    style={{display: !showDate ? '' : 'none'}}
                >
                    <Col md={2} sm={2} style={{display: !showRadio ? '' : 'none'}}>
                        <span className='nc-theme-form-label-c'>{this.state.json['dategroup-000001']}：</span>{/* 国际化处理： 日期*/}
                    </Col>
                    <Col md={tempColNum} sm={tempColNum}>
                        {/* <div style={{height: '32px'}}> */}
                        <Col md={11} sm={11}>
                            <RangePicker
                                disabled = {selectionState === 'true' ? true : false}
                                fieldid='rangeDate'
                                format={format}
                                showOk={true}
                                className="dateRangeDiv"
                                value={rangeDate}
                                onChange={ (value) => handleDateChange(value)}
                            />
                        </Col>
                        {/* </div> */}
                        
                    </Col>
                    {/* <div  className={(!showRadio || selectionState === 'true') && !showPeriod ? 'emptyDiv' : ''}></div> */}
                </Row>
                </div>
            </div>
        )
    }
}

export default DateGroup







{/*<Radio.NCRadioGroup*/}
{/*style={{*/}
{/*display: !showRadio ? 'none' : 'flex',*/}
{/*border: 'none',*/}
{/*// display: 'flex',*/}
{/*flexDirection: 'column',*/}
{/*width: '6%',*/}
{/*height: '60px',*/}
{/*justifyContent: 'space-around'*/}
{/*}}*/}
{/*// className='dateRadioGroup'*/}
{/*selectedValue={selectionState}*/}
{/*onChange={(value) => handleValueChange('selectionState', value)}*/}
{/*>*/}
{/*<Radio type={radioStyle} style={{*/}
{/*marginTop: '-10px',marginBottom: '10px'*/}
{/*}} value='true'></Radio>*/}
{/*<Radio type={radioStyle} value="false"></Radio>*/}
{/*</Radio.NCRadioGroup>*/}
{/*<div style={{*/}
{/*display: 'flex',*/}
{/*flexDirection: 'column',*/}
{/*border: '1px solid blue',*/}
{/*width: '94%'*/}
{/*}}>*/}
{/*<div style={{*/}
{/*display: 'flex',*/}
{/*flexDirection: 'row',*/}
{/*border: '1px solid yellow',*/}
{/*width: '100%',*/}
{/*alignItems: 'center'}}>*/}
{/*<span>{this.state.json['dategroup-000000']}：</span>/!* 国际化处理： 会计期间*!/*/}
{/*<div style={{*/}
{/*display: 'flex',*/}
{/*border: '1px solid red'}}>*/}
{/*<AccPeriodDefaultTreeGridRef*/}
{/*disabled = {selectionState === 'true' ? false : true}*/}
{/*value={start}*/}
{/*queryCondition = {{*/}
{/*// "GridRefActionExt": "nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder",*/}
{/*"pk_accperiodscheme": pk_accperiodscheme*/}
{/*}}*/}
{/*onChange={(v)=>{*/}
{/*accountPeriodStart(self, v)*/}
{/*}}*/}
{/*/>*/}
{/*<span style={{*/}
{/*width: '10%',*/}
{/*lineHeight: '30px'*/}
{/*}}>--</span>*/}
{/*<AccPeriodDefaultTreeGridRef*/}
{/*disabled = {selectionState === 'true' ? false : true}*/}
{/*value={end}*/}
{/*queryCondition = {{*/}
{/*"pk_accperiodscheme": pk_accperiodscheme,*/}
{/*"includeAdj": "Y"*/}
{/*}}*/}
{/*onChange={(v)=>{*/}

{/*accountPeriodEnd(self, v)*/}
{/*}}*/}
{/*/>*/}
{/*</div>*/}
{/*</div>*/}
{/*<div style={{*/}
{/*display: 'flex',*/}
{/*width: '100%',*/}
{/*alignItems: 'center'*/}
{/*}}>*/}
{/*<span>{this.state.json['dategroup-000001']}：</span>/!* 国际化处理： 日期*!/*/}
{/*<div style={{*/}
{/*width: '80%',*/}
{/*border: '1px solid green',*/}
{/*marginLeft: '28px'*/}
{/*}}>*/}
{/*<RangePicker*/}
{/*format={format}*/}
{/*showOk={true}*/}
{/*className="dateRangeDiv"*/}
{/*value={rangeDate}*/}
{/*onChange={ (value) => handleDateChange(value)}*/}
{/*/>*/}
{/*</div>*/}
{/*<div  className={(!showRadio || selectionState === 'true') && !showPeriod ? 'emptyDiv' : ''}></div>*/}
{/*</div>*/}
{/*</div>*/}