/**
 * Created by liqiankun on 2018/8/03.
 * 两框之间的元素迁移
 */

import React, {Component} from 'react';
import {high,base,ajax, deepClone, createPage, getMultiLang} from 'nc-lightapp-front';
const { Transfer } = high;
import '../../css/searchModal/index.less';
import '../../../public/reportcss/searchmodalpage.less'
const {
    NCRadio:Radio, NCRow: Row, NCCol: Col, NCModal: Modal,
    NCRangePicker: RangePicker, NCButton: Button
} = base;


class TransferCom extends Component{
    constructor(props){
        super(props);
        this.state = {
            json: {},
            targetKeys:[],
            selectedKeys: [],
            modalSize: ''
        };
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'transfer',domainName:'gl',currentLocale:'simpchn',callback});
    }

    handleScroll = (direction, e) => {


    }

    render(){
        let {targetKeys, selectedKeys, modalSize} = this.state;
        let {showModal, close, sure, data} = this.props;

        return (
            <Modal
                show = { showModal }
                onHide = { () => sure() }
            >
                <Modal.Header closeButton>
                    <Modal.Title>{this.state.json['transfer-000000']}</Modal.Title>{/* 国际化处理： 条件框*/}
                </Modal.Header>
                <Modal.Body>
                    <div className = 'myOwnTransfer'>
                        <Transfer
                            dataSource={ data }
                            targetKeys={this.props.targetKeys}
                            selectedKeys={this.props.selectedKeys}
                            onChange={this.props.handleChange}
                            onSelectChange={this.props.handleSelectChange}
                            onTargetKeysChange={this.props.handleChange}
                            onScroll={this.handleScroll}
                            showMoveBtn={true}
                            showSearch={true}
                            lazy = {{container: 'modal' }}
                            listRender={({ key, title }) => title}
                            notFoundContent={['', '']}
                            listStyle={{ width: '42%' }}
                            searchPlaceholder = {this.state.json['transfer-000002']}/* 国际化处理： 搜索会计科目*/
                        />
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        onClick={
                            () => sure()
                        }
                        colors="primary"
                    >
                        {this.state.json['transfer-000001']}{/* 国际化处理： 确认*/}
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}


export  default TransferCom
