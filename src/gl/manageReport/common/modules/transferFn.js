import {high,base,ajax, deepClone, createPage} from 'nc-lightapp-front';
import {toast} from "../../../public/components/utils";
import {transferUrl} from "../../referUrl";


export let ajaxData = (parent, self, param, disabled=false) => {//获取科目组件中"条件"的数据方法

    let accountingBook = Array.isArray(parent.state.accountingbook) ? parent.state.accountingbook[0].refpk : parent.state.accountingbook.refpk
    let data = {
        "pk_accountingbook": accountingBook,
        "startcode": parent.state.startcode.refcode,//"",//parent.state
        "endcode": parent.state.endcode.refcode, //"",
        "startlvl": String(parent.state.startlvl),//"1",
        "endlvl": String(parent.state.endlvl),//"1",
        "isleave": parent.state.isleave ? 'Y' : 'N',//"N",
        "isoutacc": parent.state.isoutacc ? 'Y' : 'N', //"N",
        versiondate: parent.state.isversiondate ? parent.state.versiondate : parent.state.busiDate,//科目版本
    };
    let dataParam = param === '' ? data : {...data, cashtype: param}

    ajax({
        url: transferUrl,
        data: dataParam,
        success: (response) => {
            let {success, data} = response;


            let paramArr = [];
            data.map((item, indx) => {
                let paramObj = {};
                paramObj.key = item.pk;
                paramObj.title = item.name;
                paramObj.code = item.code;
                paramObj.disabled = disabled;
                paramArr.push(paramObj);
            })

            if(success){
                self.setState({
                    transferDataSource: [...paramArr]
                })
            }
        },
        error: (error) => {

            toast({content: error.message, color: 'warning'})
        }
    })
}

export let clearTransterData = (self) => {

    self.setState({
        selectedKeys: [],
        targetKeys: []
    })
}
export let handleChange = (self, nextTargetKeys, direction, moveKeys) => {

    self.setState({ targetKeys: nextTargetKeys });
    self.handleSelect(nextTargetKeys)
}

export let handleSelectChange = (self, sourceSelectedKeys, targetSelectedKeys) => {

    self.setState({ selectedKeys: [...sourceSelectedKeys, ...targetSelectedKeys] });


}
