
import {high,base,ajax,deepClone, createPage } from 'nc-lightapp-front';

export function searchById(pagecode, appcode){

    let url ='/nccloud/platform/pub/mergerequest.do';
    let rqJson= {pagecode,appcode}
    let data=[
        {
            "rqCode": "template",
            "rqJson":JSON.stringify(rqJson),
            "rqUrl": "/platform/templet/querypage.do"
        },
        {
            "rqCode": "button",
            "rqJson": JSON.stringify(rqJson),
            "rqUrl": "/platform/appregister/queryallbtns.do"
        },
        {
            "rqCode": "context",
            "rqJson": JSON.stringify(rqJson),
            "rqUrl": "/platform/appregister/queryappcontext.do"
        }
    ]
    let p1=new Promise((resolve,reject)=>{
        ajax({
            url,
            data,
            success: (response) => {
                //核算账簿赋默认值
                let { data} =response
                if(data.button){
                    let button = data.button;
                    this.props.button.setButtons(button);
                }
                resolve({ 'data': data })
            }
        });
    })
    return p1
}