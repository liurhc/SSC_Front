/**
 * Created by liqiankun on 2018/7/31.
 * 会计期间；日期的组件
 */


import React, {Component} from 'react';
import {high,base,ajax, deepClone, createPage, getMultiLang} from 'nc-lightapp-front';
import '../../css/searchModal/index.less';
import '../../../public/reportcss/searchmodalpage.less'
const {
   NCRow: Row, NCCol: Col, NCCheckbox: Checkbox
} = base;

import { FICheckbox } from '../../../public/components/base';

class CheckBoxCelss extends Component{
    constructor(props){
        super(props);
        this.state = {
            json: {}
        }
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json
            })
        }
        getMultiLang({moduleId:'checkbox',domainName:'gl',currentLocale:'simpchn',callback});
    }

    render(){
        let {
            disabled,
            noChargeVoucher,  //未记账凭证
            errorVoucher, //错误凭证
            harmToBenefitVoucher, //损益结转凭证
            againClassifyVoucher  //重分类凭证
        } = this.props.paramObj;
        return (
            <Row>
                <Col md={2} sm={2} className="nc-theme-form-label-c">
                    {this.props.title ? this.props.title : this.state.json['checkbox-000000']} {/* 国际化处理： 包含凭证：*/}
                </Col>
                <Col md={10} sm={10}>
                    <Col md={3} sm={3}>
                        <FICheckbox
                            colors="dark"
                            // id='noAccountVoucher'
                            disabled={disabled}
                            checked={noChargeVoucher.checked}
                            onChange={noChargeVoucher.onChange}
                        >
                        {noChargeVoucher.title ? noChargeVoucher.title : this.state.json['checkbox-000001']} {/* 国际化处理： 未记账凭证*/}
                        </FICheckbox>
                    </Col>

                    <Col md={3} sm={3}>
                        <FICheckbox
                            colors="dark"
                            // id='errorVoucher'
                            disabled={disabled ? disabled : !noChargeVoucher.checked}
                            checked={errorVoucher.checked}
                            onChange={ errorVoucher.onChange}
                        >
                        {this.state.json['checkbox-000002']} {/* 国际化处理： 错误凭证*/}
                        </FICheckbox>
                    </Col>
                    <Col md={3} sm={3} style={{display:harmToBenefitVoucher.show ? '' : 'none'}}>
                        <FICheckbox
                            colors="dark"
                            // id='harmBenefitVoucher'
                            disabled={disabled}
                            checked={harmToBenefitVoucher.checked}
                            onChange={harmToBenefitVoucher.onChange}
                    >
                        {this.state.json['checkbox-000003']} {/* 国际化处理： 损益结转凭证*/}
                        </FICheckbox>
                    </Col>

                    <Col md={3} sm={3} style={{display: againClassifyVoucher.show ? '' : 'none'}}>
                        <FICheckbox
                            colors="dark"
                            // id='againVoucher'
                            disabled={disabled}
                            checked={againClassifyVoucher.checked}
                            onChange={againClassifyVoucher.onChange}
                        >
                        {this.state.json['checkbox-000004']} {/* 国际化处理： 重分类凭证*/}
                        </FICheckbox>
                    </Col>
                </Col>
            </Row>
        )
    }
}


export default CheckBoxCelss;
