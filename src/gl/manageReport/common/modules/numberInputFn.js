/**
 *   Created by Liqiankun on 2018/08/11
 *   只能输入数字的方法
 *   param：标记是哪个节点的
 */

export function handleNumberInput(key, value, param, record){

    let re = new RegExp('');
    if(param === 'assistBalance'){//小数

        let intLength = record.inputlength || record.selectCell.inputlength;
        let doubleLength = record.digit || record.selectCell.digit;
        re = new RegExp("^(\\d{0," + intLength + "})(\\.\\d{0," + doubleLength + "})?$");
    } else if(param === 'assistBalanceInteger'){//整数

        let intLength = record.inputlength || record.selectCell.inputlength;
        re = new RegExp("^(\\d{0," + intLength + "})");
    }else if(param === 'journalNumber'){//序时账的数值小数

        let intLength = record.inputlength;
        let doubleLength = record.digits;
        re = new RegExp("^(\\d{0," + intLength + "})(\\.\\d{0," + doubleLength + "})?$");
    }else if(param === 'journalInteger'){//序时账的数值整数

        let intLength = record.inputlength;
        re = new RegExp("^(\\d{0," + intLength + "})");
    }else if(param === 'journal'){
        re = /^(\-)?\d*\.{0,1}\d*$/
    }else{
        re = /^\d*\.{0,1}\d*$/
    }

    let result = re.exec(value);
    if(re.exec(value)){
        this.setState({
            [key]: result[0]//数组变为字符串
        })
    }
}