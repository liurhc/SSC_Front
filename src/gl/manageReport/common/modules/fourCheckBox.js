/**
 * Created by liqiankun on 2018/8/08.
 * 复选框群组件
 */


import React, {Component} from 'react';
import {high,base,ajax, deepClone, createPage, getMultiLang, getLangCode} from 'nc-lightapp-front';
import '../../css/searchModal/index.less';
import '../../../public/reportcss/searchmodalpage.less'
const {
    NCRow: Row, NCCol: Col, NCCheckbox: Checkbox
} = base;
import { FICheckbox } from '../../../public/components/base';

class FourCheckBox extends Component{
    constructor(props){
        super(props);
        this.state = {
            json: {}
        }
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'fourcheckbox',domainName:'gl',currentLocale:'simpchn',callback});
    }

    render(){
        let {renderCells, totalTitle} = this.props;
        let cellsLength = renderCells.length
        let num = cellsLength > 0 ?  12/cellsLength : 6;
        let langcode = getLangCode();
        let showTip = langcode == 'english' && cellsLength >= 3 ? true : (cellsLength <= 3 ? false : true)
        return (
            <Row className="includeVoucher">
                <Col md={2} sm={2} className='showattr' style={{display: totalTitle ? '' : this.state.json['fourcheckbox-000000']}} >{/* 国际化处理： 显示属性：*/}
                    <span className='nc-theme-form-label-c'>{totalTitle}</span>
                </Col>
                <Col md={10}>
                {
                    renderCells.map((cell, index) => {
                        return <Col md={num} sm={num}>
                            <FICheckbox
                                key={index}
                                colors="dark"
                                // id={cell.id}
                                showTip={showTip}
                                disabled={cell.disabled}
                                checked={cell.checked}
                                onChange={cell.onChange}
                            >
                            {cell.title}
                            </FICheckbox>
                        </Col>
                    })
                }
                </Col>
            </Row>
        )
    }
}


export default FourCheckBox
