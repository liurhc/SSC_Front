/**
 * 复原数据方法
 *sourceKey: 数据源的state的key值
 * param： 要操作的属性的集合
 *
 * */

export function getRecover(sourceKey, ...param){
    this.state[sourceKey].map((item, index) => {
        param.map((deleKey, deleIndx) => {
            Reflect.deleteProperty(item, deleKey)
        })
    });
    this.setState({
        [sourceKey]: [...this.state[sourceKey]]
    })
}