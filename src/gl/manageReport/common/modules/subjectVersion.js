/**
 * Created by liqiankun on 2018/7/30.
 * 启用科目版本组件
 */

import React, {Component} from 'react';

import {high,base,ajax, deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import '../../css/searchModal/index.less';
import '../../../public/reportcss/searchmodalpage.less'
import {handleValueChange} from "./modules";


const {
    NCRow:Row,
    NCCol:Col,
    NCSelect:Select,
    NCCheckbox:Checkbox,NCDiv
} = base;



class SubjectVersion extends Component{
    constructor(props){
        super(props);
        this.state = { json: {}}
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'subjectversion',domainName:'gl',currentLocale:'simpchn',callback});
    }

    render(){
        let {
            checked, data,value,top,
            disabled,handleValueChange,
            renderTableFirstData, CheckboxDisabled } = this.props;

        return (
            // <NCDiv fieldid="versiondate" areaCode={NCDiv.config.Area}>
                <Row className="myrow">
                    <Col md={2}>
                        <label for="isversiondate" className='nc-theme-form-label-c'>{this.state.json['subjectversion-000000']}：</label>{/* 国际化处理： 启用科目版本*/}
                    </Col>
                    
                    <Col md={1}>
                        <Checkbox
                            colors="dark"
                            fieldid={this.state.json['subjectversion-000000']}
                            disabled={CheckboxDisabled}
                            checked={checked}
                            onChange={
                                (value) => {
                                    handleValueChange('isversiondate', value)
                                    renderTableFirstData && renderTableFirstData()
                                }
                            }
                        >
                        </Checkbox>
                    </Col>
                    <Col md={4} >
                        <Select
                            showClear={false}
                            fieldid='versiondate'
                            value={value}
                            style={{marginRight: 6 }}
                            onChange={
                                (value) => {
                                    handleValueChange('versiondate', value);
                                    renderTableFirstData && renderTableFirstData()
                                }
                            }
                            disabled={disabled}
                        >
                            {data && data.map((item) => {
                                return (
                                    <Select.NCOption
                                        value={item}
                                        key={item}
                                    >
                                        {item}
                                    </Select.NCOption>
                                )
                            })}
                        </Select>
                    </Col>
                </Row>
            // </NCDiv>
        )
    }
}

export default SubjectVersion
