/**
 * Created by liqiankun on 2018/8/01.
 * 科目，级次组件
 */

import React, {Component} from 'react';
import {high,base,ajax, deepClone, createPage, getMultiLang} from 'nc-lightapp-front';
import '../../css/searchModal/index.less';
import {renderLevelOptions, renderLaterLevelOptions} from "./modules";
import TransferCom from './transfer';
import {subjectRefer, transferUrl} from "../../referUrl";
import '../../../public/reportcss/searchmodalpage.less'
const {
    NCRadio:Radio, NCRow: Row, NCCol: Col,NCSelect:Select,
    NCRangePicker: RangePicker, NCCheckbox: Checkbox, NCButton: Button
} = base;
import { toast } from '../../../public/components/utils';
import {ajaxData} from './transferFn';
import FourCheckBox from './fourCheckBox'

class SubjectLevel extends Component{
    constructor(props){
        super(props);
        this.state = {
            json: {},
            showTransferModal: false,//控制穿梭框的弹框是否显示；false:不显示
            transferDataSource: [],//穿梭框的数据源
        };

    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'subjectandlevel',domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount() {
        this.props.TransferRef && this.props.TransferRef(this)
    }
    //初始化state
	setTransferState = (transferDataSource, disabled) => {
        if(transferDataSource && transferDataSource.length > 0){
            transferDataSource.map((item, index)=>{
                item.disabled = disabled
            })
            this.setState({
                transferDataSource
            })
        }   
	}
    handleValueChange = (key, value) => {
        this.setState({
            [key]: value
        })
    }
    transferSure = () => {
        let { transferSure } = this.props
        transferSure && transferSure(this.state.transferDataSource)
        this.handleValueChange('showTransferModal', false);
    }


    render(){
        let {parent,accountingbook, isMultiSelectedEnabled, showLevel, queryConditionParam, showStar , disabled} = this.props;
        let accountingbookParam = accountingbook && (Array.isArray(accountingbook) ? accountingbook[0] && accountingbook[0].refpk : accountingbook && accountingbook.refpk);
        let queryCondition = {
                "pk_accountingbook": accountingbookParam,     //多选，选一个也算
                "dateStr": parent.state.isversiondate ? parent.state.versiondate : parent.state.busiDate,
                "isDataPowerEnable": 'Y',
                "DataPowerOperationCode" : 'fi'
        };
        let queryConditionDate = queryConditionParam ? {...queryCondition, ...queryConditionParam} : queryCondition;

        let conditionParam = queryConditionParam ? (queryConditionParam.cashtype ? queryConditionParam.cashtype : '') : '';//queryConditionParam.cashtype ? queryConditionParam.cashtype : ''
        return (
            //  <div className='dateGroupContainer'>
            <div className={ `nc-theme-area-bgc nc-theme-area-split-bc ${this.props.showLevel ? 'dateGroupContainer1' : 'dateGroupContainer'} `}>
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['subjectandlevel-000002']}：</span>{/* 国际化处理： 科目*/}
                    </Col>   
                    <Col md={4} sm={4}>
                        <div className="book-ref">
                            {
                                parent.renderRefer(
                                    subjectRefer,
                                    parent.state.startcode,
                                    'subjectReferStateKey',
                                    parent.state['subjectReferStateKey'],
                                    'startcode',
                                    isMultiSelectedEnabled,
                                    queryConditionDate,
                                    parent.handleValueChange,
                                    "accbalance",
                                    disabled
                                )
                            }
                        </div>
                    </Col>
                    <Col md={1} sm={1}>
                        <p className='zhi nc-theme-form-label-c'>{this.state.json['subjectandlevel-000003']}</p>{/* 国际化处理： 至*/}
                    </Col>
                    <Col md={4} sm={4}>
                        <div className="book-ref">
                            {
                                parent.renderRefer(
                                    subjectRefer,
                                    parent.state.endcode,
                                    'subjectReferStateKeyEnd',
                                    parent.state['subjectReferStateKeyEnd'],
                                    'endcode',
                                    isMultiSelectedEnabled,
                                    queryConditionDate,
                                    parent.handleValueChange,
                                    'accbalance', //过滤参照参数，fixbug118515
                                    disabled
                                )
                            }
                        </div>
                    </Col>
                    <Col md={1} sm={1}>
                        <span  className='condition1'
                                fieldid='condition'
                               style={{color:'#007ACE', cursor: 'pointer'}}
                            onClick = {() => {
                                if(this.props.changeParam){
                                    ajaxData(parent, this,  conditionParam, disabled)
                                    this.props.handleValueChange('changeParam', false)
                                    this.props.clearTransterData()
                                }
                                this.handleValueChange('showTransferModal', true);
                            }}
                        >
                            {this.state.json['subjectandlevel-000004']}{/* 国际化处理： 条件*/}
                        </span>
                    </Col>
                </Row>


                <Row className="myrow" style={{display: this.props.showLevel ? 'none' : ''}}>
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['subjectandlevel-000005']}：</span>{/* 国际化处理： 级次*/}
                    </Col>
                    {/*级次下拉*/}
                    <Col md={5} sm={5}>            
                        <Col md={5} sm={5}>
                            {/* <div className="book-ref"> */}
                                <Select
                                    showClear={false}
                                    fieldid='startNum'
                                    value={parent.state.startlvl}
                                    // style={{ width: 60, marginRight: 6 ,height:30}}
                                    onChange={
                                        (value) => {
                                            parent.handleValueChange('startlvl', value, 'accbalance')
                                            parent.handleValueChange('endlvl', value, 'accbalance')
                                        }
                                    }
                                    disabled={disabled ? disabled : parent.state.isleave}
                                >
                                    {renderLevelOptions(parent.state.level)}
                                </Select>
                            {/* </div> */}
                        </Col>
                        <Col md={2} sm={2} >
                            <p className='zhi nc-theme-form-label-c'>{this.state.json['subjectandlevel-000003']}</p>{/* 国际化处理： 至*/}
                        </Col>
                        <Col md={5} sm={5} >
                            {/* <div className="book-ref"> */}
                                <Select
                                    showClear={false}
                                    fieldid='endNum'
                                    value={parent.state.endlvl}
                                    // style={{ width: 60, marginRight: 6 ,height:30}}
                                    onChange={
                                        (value) => parent.handleValueChange('endlvl', value, 'accbalance')
                                    }
                                    disabled={disabled ? disabled : parent.state.isleave}
                                >
                                    {renderLaterLevelOptions( parent.state.startlvl -1, parent.state.level)}
                                </Select>
                            {/* </div> */}
                        </Col>
                    </Col>
                    <Col md={5} sm={5}>
                        <FourCheckBox
                            renderCells={
                                [
                                    {
                                        title: this.state.json['subjectandlevel-000000'],// /* 国际化处理： 所有末级*/
                                        checked: parent.state.isleave,
                                        id: 'isleave',
                                        disabled: disabled,
                                        onChange: (value) => parent.handleValueChange('isleave', value, 'accbalance')
                                    },
                                    {
                                        title: this.state.json['subjectandlevel-000001'], // /* 国际化处理： 表外科目*/
                                        checked: parent.state.isoutacc,
                                        id: 'isoutacc',
                                        disabled: disabled,
                                        onChange: (value) => parent.handleValueChange('isoutacc', value, 'accbalance')
                                    },
                                ]
                            }
                        />
                    </Col>
                </Row>

                <TransferCom
                    showModal = {this.state.showTransferModal}
                    // close = {this.handleValueChange}
                    sure = {this.transferSure}
                    data = {this.state.transferDataSource}
                    selectedKeys = {this.props.selectedKeys}
                    targetKeys = {this.props.targetKeys}
                    handleChange = {this.props.handleChange}
                    handleSelectChange = {this.props.handleSelectChange}
                />

            </div>
        )
    }
}


export default SubjectLevel
