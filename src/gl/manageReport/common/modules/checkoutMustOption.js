/**
 * Created by liqiankun on 2018/9/27.
 *
 * 校验必填项
 * option:
 *
 * flag1: 业务单元是否显示,必传
 *
 * callBack: 回调函数
 *
 * 注意: 业务单元的key必须为：pk_units
 *  pk_accountingbook:{key: this.state.accountingbook.refpk, content: '核算账簿'} ,
    pk_units: {key: pk_unit, content: '业务单元'},
    startcode: {key:this.state.startcode.code, content: '科目起始值'},  //开始科目编码
    endcode: {key: this.state.endcode.code, content: '科目终止值'},    //结束科目编码
 */
import {toast} from 'nc-lightapp-front'

export default function checkoutMustOptions(option, flag1, callBack, showText, flag2) {
    let tipsArr = [];
    let optionKeysArr = Object.keys(option);
    optionKeysArr.map((item, index) => {
        if(Array.isArray(option[item].key)){
            if(option[item].key.length === 0 || !option[item].key[0]){
                if(item === 'pk_units'){//业务单元为必填项切业务单元显示出来了
                    if(flag1){
                        tipsArr.push(option[item].content);
                    }
                }else{
                    tipsArr.push(option[item].content);
                }

            }
        }else{
            if (!option[item].key) {
                if (item != 'pk_units' || flag1) {
                    tipsArr.push(option[item].content);
                }
            }
        }
    })
    if(tipsArr.length > 0){
        toast({content: `${tipsArr}${showText}！`, color: 'warning', duration: 6 });
        flag2 && (flag2[0] = true);
    }else{
        callBack && callBack();
        flag2 && (flag2[0] = false);
    }
}