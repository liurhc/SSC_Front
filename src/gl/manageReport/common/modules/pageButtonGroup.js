/**
 * Created by liqiankun on 2018/10/09.
 *first: {disabled: value, callBack: fn},
 * pre: {disabled: value, callBack: fn},
 * next: {disabled: value, callBack: fn},
 * last: {disabled: value, callBack: fn},
 */


import React, {Component} from 'react';
import { base } from 'nc-lightapp-front';
const { NCButton: Button, NCButtonGroup: ButtonGroup } = base;
class PageButtonGroup extends Component{
    constructor(props){
        super(props);
    }

    render(){
        let {first, pre, next, last, display} = this.props;
        return (
            <div style={{display: display}}>
                <ButtonGroup className='btn-group'>
                    <Button shape="border" disabled={first.disabled} onClick={first.callBack}>
                        <i className="iconfont icon-shangyiye" />
                    </Button>
                    <Button shape="border" disabled={pre.disabled} onClick={pre.callBack}>
                        <i className="iconfont icon-jiantouzuo" />
                    </Button>
                    <Button shape="border" disabled={next.disabled} onClick={next.callBack}>
                        <i className="iconfont icon-jiantouyou" />
                    </Button>
                    <Button shape="border" disabled={last.disabled} onClick={last.callBack}>
                        <i className="iconfont icon-xiayiye" />
                    </Button>
                </ButtonGroup>
            </div>
            
            
            // <div className="show  cardPagination-lightapp-component" style={{display: display}}>
            //     <button
            //         type="button"
            //         className="u-button first-item cardPaginationBtn nc-button-wrapper button-undefined "
            //         disabled={first.disabled}
            //         onClick={() => first.callBack()}
            //     >
            //         <span className="icon iconfont icon-shangyiye"></span>
            //     </button>
            //     <button
            //         type="button"
            //         className="u-button item cardPaginationBtn nc-button-wrapper button-undefined "
            //         disabled={pre.disabled}
            //         onClick={() => pre.callBack()}
            //     >
            //         <span className="icon iconfont icon-jiantouzuo"></span>
            //     </button>
            //     <button
            //         type="button"
            //         className="u-button item cardPaginationBtn nc-button-wrapper button-undefined "
            //         disabled={next.disabled}
            //         onClick={() => next.callBack()}
            //     >
            //         <span className="icon iconfont icon-jiantouyou"></span>
            //     </button>
            //     <button
            //         type="button"
            //         className="u-button last-item cardPaginationBtn nc-button-wrapper button-undefined "
            //         disabled={last.disabled}
            //         onClick={() => last.callBack()}
            //     >
            //         <span className="icon iconfont icon-xiayiye"></span>
            //     </button>
            // </div>
        )
    }
}

export default PageButtonGroup