

/**
 * param： 用于区分来之哪个节点
 *
 * 科目版本变化触发回调事件
 *
 * callBack: 选科目版本时触发请求新辅助参照的接口
 *
 * */

export function handleValueChange (key, value, param, callBack){//'showzerooccurValue', value, 'showzerooccurValue'

    this.setState({
        [key]: value
    }, () => {
        if(key === 'versiondate' || key === 'isversiondate'){

            callBack && callBack(this, this.state.accountingbook, param)//self, self.state.accountingbook, boolean2
        }
        if(param === 'assistAnalyzSearch' && key === 'accountType'){

            callBack && callBack()
        }

    })

    if(key === 'includeuntally'){
        this.setState({
            includeerror: value
        })
    };
    if(param === 'SubjectVersion' || param === 'accbalance') {
        this.setState({
            changeParam: true,
            rightSelectItem: [],
            laterLevel: value
        })
    };
    if(param === 'showzerooccurValue'){//科目辅助余额表中

        if(value){
            this.setState({
                showzerobalanceoccurValue: value,
                showzerobalanceoccurValueEdit: value
            })
        }else{
            this.setState({
                showzerobalanceoccurValueEdit: value
            })
        }
        this.setState({
            showzerobalanceoccur: value
        })
    }else if(param === 'showSubjectTotal'){
        this.setState({
            disCurrplusacc: value
        })
    }else if(key === 'mutibook' && param ==='multiAgentShow'){//mutibook', value, 'multiAgentShow'
        this.setState({
            /**无余额无发生不显示*/
            showzerobalanceoccurValueEdit: false, //控制'无余额无发生不显示'是否可编辑
        })
        if(value === 'Y'){
            this.setState({
                debitAndCredit:true,
                showzerooccurValueEdit: true,
                /**按借贷方显示余额*/
                twowaybalance: false, //借贷方显示余额
                debitAndCredit: true,//按借贷显示余额是否禁止
                /**无发生不显示*/
                showzerooccurValue: false,//无发生不显示的选择状态
                showzerooccur: true, //控制 无发生不显示 是否可编辑
            })
        }else{
            this.setState({
                debitAndCredit:false,
                showzerooccurValueEdit: false
            })
        }

    }
}

// 注意区分
// export  let handleValueChange = () => {}
// export function handleValueChange() {}
// 之间的区别