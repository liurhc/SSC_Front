


import {high,base,ajax,deepClone, createPage } from 'nc-lightapp-front';

export function searchById(pagecode, appcode){

    let url ='/nccloud/platform/appregister/queryallbtns.do';
    let rqJson= {pagecode,appcode}
    let data={
        pagecode,
        appcode//小应用id
    }
    let p1=new Promise((resolve,reject)=>{
        ajax({
            url,
            data,
            success: (response) => {

                //核算账簿赋默认值
                let { data} =response
                if(data){
                    let button = data.button;
                    this.props.button.setButtons(data);
                }
            }
        });
    })
    return p1
}