/**
 *   Created by Liqiankun on 2018/7/21
 */
import { hashHistory, Redirect, Link } from 'react-router';
//import moment from 'moment';
import {high,base,ajax, deepClone, gzip } from 'nc-lightapp-front';
import './modules.less';
const { Refer } = high;
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,
} = base;

import { toast } from '../../../public/components/utils';
import createScript from "../../../public/components/uapRefer";
import {getAdjustTime, getReferCells} from "../../../public/hansonTableSetData/commonFn";
import {
    checkAccountReferUrl,
    assistReferYOrNUrl,
    multiassistreferUrl,
    assistPropertyBalanceUrl, journalReferUrl
} from '../../referUrl'
import {voucherRelatedApp} from "../../../public/components/oftenApi.js";
import { openToVoucher } from '../../../public/common/voucherUtils';
import {voucher_link} from "../../../public/components/constJSON";

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';


/**
 *1、 币种组件：
 * currtypeList: 请求返回的币种类型,
 * currtype: 当前选择的币种,
 * handleCurrtypeChange: 选择币种的方法
 * */

export let renderMoneyType = (currtypeList, currtypeName, handleCurrtypeChange, json, disabled=false) => {

    return(
        <Row className="myrow">
            <Col md={2} sm={2}>
                <span className='nc-theme-form-label-c'>{json['childmodules-000005']}：</span>{/* 国际化处理： 币种*/}
            </Col>
            <Col md={4} sm={4}>
                <div className="book-ref">
                    <Select
                        showClear={false}
                        fieldid='currtype'
                        value={currtypeName}
                        disabled={disabled}
                        onChange={
                            (value) => {
                                let values=JSON.parse(value)
                                handleCurrtypeChange('currtype',values.pk_currtype);
                                handleCurrtypeChange('currtypeName',values.name)
                            }
                        }
                    >
                        {currtypeList.map((item, index) => {
                            return <NCOption value={JSON.stringify(item)} key={item.pk_currtype} >{item.name}</NCOption>
                        } )}
                    </Select>
                </div>
            </Col>
        </Row>
    )
}


/**
 *2、参照的组件：
 * * url: 参照地址;
 * * referStateKey: 渲染参照的方法的state的key字符串
 * * referStateValue: 渲染参照的方法的state的key所对应的值
 * * stateValueKey: 参照选择值时保存选择值所对应的state的key值
 * flag: 是否多选 true/false
 * queryCondition: {} 过滤条件
 * changeFn: 选择的回调方法
 * */
export function renderRefer(url,value,referStateKey,referStateValue,stateValueKey, flag, queryCondition, changeFn, fileParam, disabled=false){//渲染参照的方法

    if(!referStateValue){//!this.state[objKey]: undefined
        {createScript.call(this,url,referStateKey)}
        // {createScript.call(this,url,referStateKey)}
    }else{
        return (
            <div>
                {
                    referStateValue?(referStateValue)(
                        {
                            fieldid: stateValueKey,
                            value: value,
                            isMultiSelectedEnabled:flag,
                            disabled: disabled,
                            queryCondition:() => {
                                return {
                                    dateStr: this.state.isversiondate ? this.state.versiondate : this.state.busiDate,
                                    ...queryCondition
                                }
                            },
                            "isShowDisabledData": true,
                            onChange: (v)=>{
                                changeFn(stateValueKey, v, fileParam, v.refpk, this.state.versiondate);

                            }
                        }
                    ):<div/>
                }
            </div>
        );
    }
}
 /**
  * 序时账中使用
  * 专门用于渲染币种的参照
  * */
export function renderCurrtyRefer(url,value,referStateKey,referStateValue,stateValueKey, flag, queryCondition, changeFn, fileParam){//渲染参照的方法

    if(!referStateValue){//!this.state[objKey]: undefined
       {createScript.call(this,url,referStateKey)}
        // {createScript.call(this,url,referStateKey)}
    }else{
        return (
            <div>
                {
                    referStateValue?(referStateValue)(
                        {
                            fieldid: stateValueKey,
                            value: value,
                            isMultiSelectedEnabled:flag,
                            queryCondition:() => {
                                return {
                                    dateStr: this.state.isversiondate ? this.state.versiondate : this.state.busiDate,
                                    ...queryCondition
                                }
                            },
                            onChange: (v)=>{
                                changeFn(stateValueKey, v, fileParam, v.pk_accasoa, this.state.versiondate);

                            }
                        }
                    ):<div/>
                }
            </div>
        );
    }
}

/**
 * 3、核算账簿参照渲染公共方法模版
 * self: 使用该方法的组件对象
 * paramObj:{
 *      url: 参照地址；这个可以不传递了
 *      referStateKey: state中"渲染参照的方法"所对应的key
 *      referStateValue: state中"渲染参照的方法"的方法；从referStateKey中取
 *      stateValueKey: 参照选择值的时候"保存"所选择的值所对应的state的key值
 *      flag: 是否多选 true/false
 *      queryCondition: {} 过滤条件
 *      changeFn: 选择的回调方法
 * }
 *
 * boolean: 来源于哪个节点
 * */

export function createReferFn(self, paramObj, callBackObj, boolean){//渲染参照的方法
    let { url, value,referStateKey,referStateValue,stateValueKey, flag, queryCondition, showGroup, disabled } = paramObj;
    if(!referStateValue){//!this.state[objKey]: undefined
        {createScript.call(self,checkAccountReferUrl,referStateKey)}
    }else{
        return (
            <div>
                 {
                    referStateValue?(referStateValue)(
                        {
                            fieldid: stateValueKey,
                            value: value,
                            isMultiSelectedEnabled:flag,
                            showInCludeChildren:true,
                            isShowDisabledData: true,
                            showGroup:showGroup,
                            disabled: disabled,
                            disabledDataShow: true,
                            queryCondition:() => {
                                return {
                                    TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    appcode: self.props.getSearchParam('c'),
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi',
                                    ...queryCondition
                                }
                            },
                            onChange: (value)=>{
                                handleValueChange(self, stateValueKey, value)//选择参照的值，更新state
                                if(boolean === 'multiformat'){
                                    self.props.button.setDisabled({add: false})
                                }
                                if( boolean === 'multiAccountSearch'){
                                    if(value&&value.pk_accountingbook==undefined){
                                        self.setState({
                                            mutiAccountName: [],
                                            selectMutiAccount: '',
                                            showTableSubject: false
                                        });
                                    }
                                    
                                }
                                if(boolean === 'accountrep'){
                                    self.props.button.setDisabled({
                                        add: false
                                    })
                                }
                                if(boolean === 'assistAnalyzSearch'){
                                    self.setState({
                                        pk_report: {
                                            refname: '',
                                            refpk: ''
                                        }
                                    })
                                }
                                if(boolean === 'AnalysisNode'){
                                    self.showSelectArr = [];
                                    self.setState({
                                        checkboxTable: {
                                            cash: true,    //现金流量项目
                                            cashSum: false,
                                            accasoa: false,  //科目
                                            accasoaSum: false,
                                            ass: false,    //辅助核算
                                            assSum: false,
                                            inner: false,  //内部单位
                                            innerSum: false,
                                            cashflow: [],//现金流量项目
                                        },
                                        accAssItems: []
                                    })
                                }
                                if(boolean === 'journal'){
                                    self.setState({
                                        voucherRefer: {refname: '', refpk: ''}, //凭证类别的参照数据
                                        madeBillPeople: {}, //制单人选择的参照数据`
                                        cashierPeople: {},  //出纳人选择的参照数据
                                        pk_system: {}, //制单系统
                                        currtype: [],      //选择的币种
                                        checkPeople: [],    //审核人选择的参照数据
                                        chargePeople: [],   //记账人选择的参照数据
                                        oppositeSubjectState: [],//对方科目
                                        subjectCodingState: [],//科目编码
                                        abstractState:{}
                                    })
                                }
                                self.setState({
                                    changeParam: ++self.state.changeParam,
                                    selectedKeys: [],
                                    targetKeys: [],
                                    rightSelectItem: [],
                                    buSecond: {},
                                    multbusi: false
                                });
                                callBackObj.renderTableFirstData && callBackObj.renderTableFirstData(value);
                                if(boolean === 'multiAnalyTable' && value.length > 1){
                                    self.setState({
                                        voucherRefer:{
                                            refname: '',
                                            refpk: ''
                                        },
                                        voucherStatus: true
                                    })
                                }
                                if(boolean === 'subjectAssistBalance'){//科目辅助余额表中选多个"核算账簿"时【显示科目类型小计，排序方式】禁止，【多主体显示方式】打开
                                    if(value.length > 1){
                                        self.setState({
                                            sumbysubjtypeEdit: true,
                                            disCurrplusacc: true,
                                            disMutibook: false
                                        });
                                    }else{
                                        self.setState({
                                            sumbysubjtypeEdit: false,
                                            disCurrplusacc: false,
                                            disMutibook: false,
                                            debitAndCredit: false
                                        })
                                    }
                                }
                                if(boolean === 'CashQuery'){
                                    if(value.length > 1){
                                        self.setState({
                                            searchStyle: false,
                                        })
                                    }else{
                                        self.setState({
                                            searchStyle: true,
                                            selectedQueryValue: '0',
                                            mainAttachedDisabled: false
                                        })
                                    }

                                }

                                if(boolean === 'multiagentSubjectBalance'){
                                    if(Array.isArray(value) && value.length > 1){//多主题科目余额表中
                                        self.setState({
                                            showzerooccurEdit: true,
                                            showzerobalanceoccurEdit: true,
                                            disSumbysubjtype: true,
                                            twowaybalanceEdit: true,
                                            selfAgentSubjectSearchEdit: false,

                                            showzerooccur: false,//无发生不显示
                                            showzerobalanceoccur: true,//无余额无发生不显示
                                            sumbysubjtype: false,//显示科目类型小计
                                            twowaybalance: false//按借贷方显示余额
                                        })
                                    }else{
                                        self.setState({
                                            showzerooccurEdit: false,
                                            showzerobalanceoccurEdit: false,
                                            disSumbysubjtype: false,
                                            twowaybalanceEdit: false,
                                            selfAgentSubjectSearchEdit: true,
                                            selfAgentSubjectSearch: false
                                        })
                                    }
                                }

                                if(
                                    boolean === 'accbalance' || boolean === 'assistBalance' || boolean === 'threeAll'
                                    || boolean === 'subjectAssistBalance' || boolean === 'threedetail' || boolean === 'dateReport'||
                                    boolean === 'capitalReport'
                                ){
                                    self.setState({
                                        sumbysubjtype: false,//显示科目类型小计
                                        startcode: {},
                                        endcode: {}
                                    })
                                    if(value.length >1){
                                        self.setState({
                                            disMutibook: false
                                        })
                                    }else {
                                        self.setState({
                                            disMutibook: true,
                                            mutibook: 'N',
                                            mutibookAlone: false,
                                            debitAndCredit: false
                                        })
                                    }
                                }                                                    //self, value, callBackObj, boolean2： 来源于哪个级节点
                                callBackObj.businessUnit && callBackObj.businessUnit(self, value, callBackObj,boolean);
                                // callBackObj.getCheckContent && callBackObj.getCheckContent(self, value, boolean, callBackObj.renderTableFirstData);
                                // callBackObj.getReferInfo && callBackObj.getReferInfo(self, {'pk_accountingbook': value.refpk, "versiondate": self.state.busiDate});
                                // callBackObj.selfFunction && callBackObj.selfFunction()
                                // businessUnit(self, value)//业务单元是否显示；级次数据；科目版本信息；业务日期
                                // getCheckContent(self, value)//根据所选择的核算账簿获取辅助核算内容
                            }
                        }
                    ):<div/>
                }
            </div>
        );
    }
}
function getCashFlowDetailSearch(self, data){//现金流量明细查询获取是否显示业务单元；这个和其他的不一样
    let pkParam = Array.isArray(data) ? data[0].refpk : data.refpk
    let url1 = "/nccloud/gl/cashflow/isbustartflag.do";
    ajax({
        url: url1,
        data: {"pk_accountingbook": pkParam},
        success: (response) => {
            let {data, success} = response;
            if(success){
                self.setState({
                    isShowUnit: data
                })
            }
        },
        error: (error) => {
            toast({content: error.message, color: 'error'})
        }
    })
}
export let getReferAppCode = (self, stateKey) => {//从URL上获取核算账簿的code值
    let codeValue = self.props.getSearchParam('c')
    handleValueChange(self, stateKey, codeValue);
}
/**
 * 获取默认核算账簿值；并请求默认核算账簿下掉其他值
 * boolean: 核算账簿是多选还是单选；
 * callBackObj: 得到默认核算账簿后请求对应的其他请求接口
 * boolean2: 是否过滤"会计科目"；  来源于哪个节点
 *
 * */
export let getReferDetault = (self, boolean, callBackObj, boolean2,defaultAccouontBook=null) => {//获取核算账簿的默认值
    
    if(defaultAccouontBook && defaultAccouontBook.getflag){ //请求过了账簿信息就不需要再发请求了
        if(defaultAccouontBook.refpk){
            getAccountbookInfo(self, boolean, callBackObj, boolean2,defaultAccouontBook);
        }
    }else{
        let url = '/nccloud/platform/appregister/queryappcontext.do';
        let data = {
            appcode: self.state.appcode
        };
        ajax({
            url,
            data,
            success: (response) => {
                let {success, data} = response;
                if(success&& data.defaultAccbookPk){
                    let accountbook={refpk:data.defaultAccbookPk,refname:data.defaultAccbookName};
                    getAccountbookInfo(self, boolean, callBackObj, boolean2,accountbook);
                }
                
            },
            error: (error) => {
                toast({content: error.message, color: 'warning'})
            }
        })
    }
}
function getAccountbookInfo(self, boolean, callBackObj, boolean2,data){
    let accountBookParam;
            if(boolean){
                accountBookParam = [{
                    refpk : data.refpk,
                    refname: data.refname
                }]
            }else{
                accountBookParam = {
                    refpk : data.refpk,
                    refname: data.refname
                }
            }

            // if(success && data.defaultAccbookPk){
                if(boolean2 === 'multiformat'){
                    self.props.button.setDisabled({add: false})
                }
                if(boolean2 === 'accountrep'){
                    self.props.button.setDisabled({
                        add: false
                    })
                }
                self.setState({
                    accountingbook: accountBookParam
                }, () => {                                                             //self, value, callBackObj, boolean2： 来源于哪个节点
                    callBackObj &&  callBackObj.businessUnit && callBackObj.businessUnit(self, self.state.accountingbook, callBackObj, boolean2);
                    // callBackObj &&  callBackObj.getCheckContent && callBackObj.getCheckContent(self, self.state.accountingbook, boolean2);
                    callBackObj &&  callBackObj.getInfoByBook && callBackObj.getInfoByBook(self, self.state.accountingbook);//多栏账查询中获取"多栏账名称"
                    callBackObj &&  callBackObj.selfFunction && callBackObj.selfFunction();
                    // callBackObj && callBackObj.getReferInfo && callBackObj.getReferInfo(self, {'pk_accountingbook': self.state.accountingbook.refpk})

                })
            // }
}
/**
 * 业务单元是否显示；级次数据；科目版本信息；业务日期
 *
 * boolean2: 来源于哪个节点
 * */
export let businessUnit = (self, value, callBackObj, boolean2) => {//业务单元是否启用；级次
    let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
    let dataParam = Array.isArray(value) ? value[0] && value[0].refpk :  value && value.refpk;
    let pk_accpont = {
        "pk_accountingbook": dataParam
    };
    ajax({
        url:url,
        data:pk_accpont,
        success: function(response){
            const { success, data } = response;
            if (success) {
                let {rangeDate} = self.state;
                if(response.data){
                    if(data.NC001){// 不是false的话接可以选择'集团本币'，解除禁止
                        self.setState({
                            groupCurrency:false
                        })
                    }else{
                        self.setState({
                            groupCurrency:true
                        })
                    }
                    if(data.NC002){// 不是false的话接可以选择'全局本币'，解除禁止
                        self.setState({
                            globalCurrency: false
                        })
                    }else{
                        self.setState({
                            globalCurrency: true
                        })
                    }
                    let periodObj = {}
                    periodObj.refname = data.bizPeriod;
                    periodObj.refpk = data.bizPeriod;
                    if(boolean2 === 'capitalReport' || boolean2 === 'dateReport' || boolean2 === 'CashQuery' || boolean2 === 'AnalysisNode' || boolean2 === 'DetailNode'){
                        rangeDate && (rangeDate[0] = data.bizDate);
                        rangeDate && (rangeDate[1] = data.bizDate);
                        
                    }else{
                        rangeDate && (rangeDate[0] = data.begindate);
                        rangeDate && (rangeDate[1] = data.enddate);
                    }

                    let dateArr = data.bizPeriod.split('-');
                    self.setState({
                        level: data.accountlvl,//渲染级次最大值
                        versionDateArr: [...response.data.versiondate],//科目版本数组
                        versiondate: data.versiondate[data.versiondate.length-1],
                        busiDate: data.bizDate,
                        pk_accperiodscheme: data.pk_accperiodscheme,
                        start: {...periodObj},
                        end: {...periodObj},
                        startyear: dateArr[0],
                        endyear: dateArr[0],
                        startperiod: dateArr[1],
                        endperiod:   dateArr[1],
                        begindate: data.begindate,
                        enddate: data.enddate,
                        rangeDate: rangeDate && [...rangeDate],
                        globalCurrinfo: data.globalCurrinfo,
                        groupCurrinfo: data.groupCurrinfo,
                        isqueryuntallyed: data.isqueryuntallyed,
                        unitValueParam: data.unit.value,//pk_org
                    }, () => {
                        callBackObj &&  callBackObj.getCheckContent && callBackObj.getCheckContent(self, self.state.accountingbook, boolean2);
                        callBackObj &&  callBackObj.selfFunction && callBackObj.selfFunction();
                        
                        callBackObj &&  callBackObj.getReferInfo && callBackObj.getReferInfo(self, {'pk_accountingbook': value.refpk, "versiondate": self.state.busiDate, needaccount: false});
                        if(boolean2 === 'CashQuery'){
                            callBackObj && callBackObj.getBackData && callBackObj.getBackData()
                        }
                    } )
                    // setTimeout(
                    //     () => {
                    //         callBackObj &&  callBackObj.getCheckContent && callBackObj.getCheckContent(self, self.state.accountingbook, boolean2);
                    //         callBackObj &&  callBackObj.selfFunction && callBackObj.selfFunction();
                    //         callBackObj &&  callBackObj.getReferInfo && callBackObj.getReferInfo(self, {'pk_accountingbook': value.refpk, "versiondate": self.state.busiDate, needaccount: false});
                    //     } ,
                    //     0
                    // )
                    if(Array.isArray(value) && value.length > 1){//核算账簿多选时，不显示二级业务单元
                        self.setState({
                            isShowUnit: false,
                            disCurrplusacc: true,
                            editSubjectTotal: true,
                            currplusacc: 'Y',
                            sumbysubjtype: false,
                            disMutibook: false
                        });
                    }else{
                        if(data.isShowUnit) {
                            //科目辅助余额表中显示"业务单元"时【多主体显示方式, 排序方式,显示科目类型小计】禁止
                            self.setState({
                                sumbysubjtypeEdit: true,//显示科目类型小计
                                disCurrplusacc: true,//排序方式
                                disMutibook: true//多主体显示方式
                            })
                        }else{
                            self.setState({
                                sumbysubjtypeEdit: false,
                                disCurrplusacc: false,
                                disMutibook: false
                            })
                        }
                        self.setState({
                            isShowUnit: data.isShowUnit,
                        });
                        if(!data.isShowUnit){
                            self.setState({
                                disCurrplusacc: false,
                                editSubjectTotal: false
                            })
                        }else{
                            self.setState({
                                disCurrplusacc: true,
                                editSubjectTotal: true
                            })
                        }
                        self.setState({
                            disMutibook: true
                        })
                        if(boolean2 === 'DetailNode' || boolean2 ==='CashQuery' || boolean2 === 'AnalysisNode'){
                            getCashFlowDetailSearch(self, value)
                        }
                    }

                }
            }
        },
    });
}

/**
 * 根据所选择的核算账簿获取<辅助核算>内容
 * boolean: 是否过滤掉"会计科目"
 *
 * */
export let getCheckContent = (self, value, boolean, getFirstTableTableCallBack) => {
    let dataParam = Array.isArray(value) ? (value[0] && value[0].refpk) : (value && value.refpk);
    let url = '';
    let Param = {"pk_accountingbook": dataParam};
    if(boolean === 'assistBalance' || boolean === 'assistDetailAccount'){
        url = assistReferYOrNUrl;
        Param.versiondate = self.state.isversiondate ? self.state.versiondate : self.state.busiDate;
        Param.needaccount = true
    } else if(boolean === 'cashAccount' || boolean === 'bankAccount' || boolean === 'subjectAssistBalance'){
        url = assistReferYOrNUrl;
        Param.needaccount = false;
        Param.versiondate = self.state.isversiondate ? self.state.versiondate : self.state.busiDate;
        if(boolean === 'cashAccount' || boolean === 'bankAccount'){
            Param.isOnlyRefDoc = true
        }
    } else if(boolean === 'assistPropertyBalance'){//辅助属性余额比表
        url = assistPropertyBalanceUrl;
        Param.versiondate = self.state.isversiondate ? self.state.versiondate : self.state.busiDate;
    } else if(boolean === 'multiAnalyTable'){//多维分析表
        url = multiassistreferUrl;
        Param.versiondate = self.state.isversiondate ? self.state.versiondate : self.state.busiDate;
    }
    // let url = '/nccloud/gl/accountrep/multianalysisqueryobject.do';//多维分析表专用：
    ajax({
        url,
        data: Param,
        success: (response) => {
            let { success, data} = response;
            let emptyObj = { name: ""}
            data.unshift(emptyObj);
            setAssistCheck(self, data, getFirstTableTableCallBack)
        },
        error: (error) => {
            toast({content: error.message, color: 'warning'})
        }
    })
}

/***
 * 整理请求回来的辅助核算的内容
 *
 */
export let setAssistCheck = (self, value, getFirstTableTableCallBack) => {
    getFirstTableTableCallBack && getFirstTableTableCallBack()
    value.map((item) => {
        if(item.attr){
            item.attr.map((cell) => {
                cell.select = false
            })
        }
    })

    let newTableFirstData = [...self.state.tableSourceData];
    newTableFirstData.forEach((item, index) => {
        item.searchObj = value;
    })
   self.setState({
        tableSourceData: newTableFirstData
    })
}

/**
 * 级次内容渲染
 * */
export let renderLevelOptions = (num) => {
    let optionArr = [];
    for(let i=0; i<num; i++){
        optionArr.push(<NCOption value={i+1} key={i}>{i+1}</NCOption>);
    }
    return optionArr;
}

export let renderLaterLevelOptions = (minNum = 0, maxNum) => {
    let optionArr = [];
    for(let i=minNum; i<maxNum; i++){
        optionArr.push(<NCOption value={i+1} key={i}>{i+1}</NCOption>);
    }
    return optionArr;
}

/**
 * 核算账簿触发的接口：
 * 1、gl/voucher/queryBookCombineInfo.do：//判断是否: 1、集团本币可点击；2、起用业务单元；3、返回币种是否可选
 *      accountlvl:"5"                             //###级次
 *      NC001:"local_convert"                      //###判断是否: 1、集团本币可点击
 *      isShowUnit:false                           //###判断是否: 2、起用业务单元
 *      bizDate:"2018-07-27"                       //###业务日期
 *      versiondate:["0000-00-00", "2017-09-13"]   //###科目版本
 *      NC002:"raw_convert"
 *      bizPeriod:"2018-07"                         //业务期间
 *      currinfo:{display: "人民币", value: "1002Z0100000000001K1", scale: null}
 *      excrate2:1
 *      excrate3:1
 *      excrate4:"1.00000000"
 *      globalCurrinfo:{display: "人民币", value: "1002Z0100000000001K1", scale: null}
 *      globalscale:"2"
 *      groupCurrinfo:{display: "人民币", value: "1002Z0100000000001K1", scale: null}
 *      groupscale:"2"
 *      isAttachmentMust:false
 *      isDisplayNullFree:true
 *      isEditVoucherNO:false
 *      isFreevalueDefault:false
 *      isInstantPrint:false
 *      num:"1"
 *      orgscale:"2"
 *      pk_accperiodscheme:"0001Z000000000000001"
 *      pk_vouchertype:{value: "1001Z31000000001Q11I", display: "集团类别"}
 *      scale:"2"
 *      unit:{display: "用友股份", value: "0001A310000000000NN6", scale: null}
 *
 * 2、gl/accountrep/multianalysisqueryobject.do';//获取辅助核算内容
 *      [
 *          {refpath: "uapbd/refer/fiacc/AccountDefaultGridTreeRef/index", name: "会计科目"},
 *          {refpath: "uapbd/refer/sminfo/BankaccSubGridTreeRef/index", pk_checktype: "0001Z010000000001N98",…},
 *          {refpath: null, pk_checktype: "0001Z0100000000005CV", name: "客商"},
 *          {digit: null, pk_checktype: "1001Z5100000000061LS", name: "字符", datatype: "1", inputlength: "100"},
 *          {refpath: "uapbd/refer/address/AreaclTreeRef/index", pk_checktype: "0001Z0100000000005CU",…},
 *          {refpath: "uapbd/refer/customer/CustClassDefaultTreeRef/index", pk_checktype: "1001Z01000000000HQW9",…},
 *          {digit: null, pk_checktype: "1001Z3100000000003DV", name: "日期", datatype: "33", inputlength: null},
 *          {refpath: "uapbd/refer/org/DeptTreeRef/index", pk_checktype: "0001Z0100000000005CS", name: "部门"},
 *          {refpath: "uapbd/refer/supplier/InnerCustSupplierGridTreeRef/index",…},
 *          {digit: "2", pk_checktype: "1001Z3100000000003DW", name: "数值", datatype: "31", inputlength: "10"},
 *          {refpath: "uapbd/refer/fiacc/AccountDefaultGridTreeRef/index", name: "对方科目"}
 *      ]
 *
 ** gl/glpub/accountinfoquery.do：也废弃了，字段都移到queryBookCombineInfo接口中了   级次；业务日期；科目版本
 *      accountlvl:"5"                           //级次
 *      bizDate :"2018-07-27"                    //业务日期
 *      bizPeriod:"2018-07"
 *      pk_accperiodscheme:"0001Z000000000000001"
 *      versiondate:["0000-00-00", "2017-09-13"] //科目版本
 *
 * gl/voucher/queryAccountingBook.do：不需要了与queryBookCombineInfo重复了
 *      NC001:"local_convert"
 *      NC002:"raw_convert"
 *      defaultBUOrg:{pk_org: "0001A310000000000NN6", orgCode: "yonyou01", orgName: "用友股份"}
 *      defaultCurrtype:{pk_currtype: "组织本币", name: "组织本币"}
 *      globalAmountCtrl:"1"
 *      globalCurrType:{pk_currtype: "1002Z0100000000001K1", name: "人民币"}
 *      groupAmountCtrl:"1"
 *      groupCurrType:{pk_currtype: "1002Z0100000000001K1", name: "人民币"}
 *      isBUBalanceCheck:false
 *      isInitBuild:true
 *      isStartBUSecond:false
 *      isYearStart:true
 *      localCurrType:{pk_currtype: "1002Z0100000000001K1", name: "人民币"}
 *      period:"01"
 *      pk_group:"0001A3100000000002FJ"
 *      pk_org:"0001A310000000000NN6"
 *      versiondate:"2017-01-01"
 *      year:"2017"
 * */

/**
 * 判断是否起用业务单元开始
 *
 * 问题1：vaersiondate必须得传递吗，刚选完核算账簿如何拿到vaersiondate
 * 问题2：选择核算账簿得整个流程，每个节点得核算账簿得选择都是一样的流程吗
 *
 * */

/**
 * 判断是否起用业务单元结束
 * */








export let getSubjectVersion = (that) => {//获取"启用科目版本"数据,"科目版本"的数据
    //gl.glpub.accountinfoquery
    let url = '/nccloud/gl/glpub/accountinfoquery.do';
    let param = Array.isArray(that.state.accountingbook) ? that.state.accountingbook[0].refpk : that.state.accountingbook.refpk;
    let data = {
        pk_accountingbook: param,//""
    };
    ajax({
        url,
        data,
        success: (response) => {
            let { data } = response;
            that.setState({
                versionDateArr: [...response.data.versiondate],
                level: response.data.accountlvl,
                pk_accperiodscheme: response.data.pk_accperiodscheme//"会计期间"参照需要的参数
            })
        },
        error: (error) => {
            toast({ content: error.message, color: 'warning' })
        }
    })
}


/**
 * 联查方法群开始：
 * */
export let onSelect= ({key}) => {//提供接口出去：用于下拉按钮使用
    switch (key){
        case '1':
            getDetailPort({
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: '/gl/threedetail/pages/main/index.html',
                key: 'detail',
                appcode: '20023030'
            })
            break;
        case "2":
            getDetailPort({
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: '/gl/threeall/pages/main/index.html',//'/gl/accbalance/relevanceSearch/totalAccount/index.html',
                key: 'totalAccount',
                appcode: '20023010',
            })
            break;
        case "3":

            let selectRow = getSelectRowData()[0].link;
            selectRow['key'] = 'relevanceAssist'
            getReferCells(this,selectRow, relevanceAssist)//查找所选择的数据有几个参照

    }
}

export let getDetailPort = (self, paramObj, json) => {//提供接口出去：用于单个按钮使用

    let selectRowData = '';
    if(!paramObj.selectRowData){
        selectRowData = getSelectRowData(self);
    }else{
        selectRowData = paramObj.selectRowData
    }

    let url = paramObj.url; //'/nccloud/gl/accountrep/triaccquery.do';

    if(selectRowData != ''){

        if(paramObj.flagParam === 'cashFlowDetailSearch' && !selectRowData[0].pk_voucher){
            toast({content: json['childmodules-000000'], color: 'warning'})/* 国际化处理： 当前行不支持联查！*/
            return;
        }

        if(paramObj.selectRowData){//现金流量查询错误分析里的l联查凭证 过来的
            let data = getDataType(self,paramObj, json);

            jumpToDetail(self, data, paramObj);
        }else if(paramObj.key === 'cashFlowDetailSearch'){//现金流量查询 / 现金流量分析表的"明细"按钮
            let data = getDataType(self,paramObj, json);

            jumpToDetail(self, data, paramObj);
        }else{
            let selectRow = getSelectRowData(self)[0];
            if(selectRow.link){
                let data = getDataType(self,paramObj, json);
                jumpToDetail(self, data, paramObj);
            }else if(selectRow.pk_voucher && paramObj.flagParam){
                let data = getDataType(self,paramObj, json);
                jumpToDetail(self, data, paramObj);
            }else {
                if(paramObj.filterParam){
                    toast({content: json['childmodules-000001'], color: 'warning'})/* 国际化处理： 当前行不支持过滤！*/
                }else{
                    toast({content: json['childmodules-000000'], color: 'warning'})/* 国际化处理： 当前行不支持联查！*/
                }

            }
        }
    }else {
        toast({content: json['childmodules-000002'], color: 'warning'})/* 国际化处理： 请进行选择数据！*/
    }


}

let relevanceAssist = (response, param) => {//科目余额表的：联查辅助
    let { success, data } = response;

    if (success) {

        if(data){
            if(data.length > 1){
                setState({
                    referLength: data.length,
                    referContent: [...data],
                    referModalShow:true
                })
            }else if(data.length ===1 ){
                setState({
                    selectRow: [data[0].pk_accassitem]
                },getDetailPort({
                    url: '/nccloud/gl/accountrep/assbalancequery.do',
                    jumpTo: '/gl/manageReport/assistBalance/content/index.html',//'/gl/accbalance/relevanceSearch/assist/index.html',
                    key: 'assist',
                    appcode: '20023055',
                }))
            }

        }else {
            toast({content: `${param.acccode}${this.state.json['childmodules-000006']}，${this.state.json['childmodules-000007']}`, color: 'warning'})/* 国际化处理： 科目无辅助项,不能联查辅助*/
            return;
        }
    }
}


let referModalSure = () => {
    self.setState({
        referModalShow: false
    })
    getDetailPort({
        url: '/nccloud/gl/accountrep/assbalancequery.do',
        jumpTo: '/gl/manageReport/assistBalance/content/index.html',//'/gl/accbalance/relevanceSearch/assist/index.html',
        key: 'assist',
        appcode: '20023055'
    })
}

let getDataType = (self, param, json) => {
    let selectRow = '';//getSelectRowData(self)[0].link
    if(param.key === 'cashFlowSearch'){
        selectRow = param.selectRowData
    }else if(param.key === 'cashFlowDetailSearch'){//现金流量查询的"明细"按钮
        selectRow = getSelectRowData(self)[0];
    }else {
        if(param.flagParam){
            selectRow = getSelectRowData(self)[0]
        }else{
            selectRow = getSelectRowData(self)[0].link
        }
    }

    let result = '';
    if(param.key === 'detail'){
        result = {
            ...self.state.queryDatas,
            link: {...selectRow},
            "class": "nccloud.pubimpl.gl.account.AccbalLinkTridetailParamTransfer"//
        }
    }else if(param.key === 'totalAccount'){
        result = {
            ...self.state.queryDatas,
            link: {...selectRow},
            "class": "nccloud.pubimpl.gl.account.AccbalLinkTriaccbookParamTransfer"
        }
    }else if(param.key === 'assist'){//联查"辅助"
        selectRow['key'] = 'relevanceAssist';
        result = {
            ...self.state.queryDatas,
            link: {...selectRow},
            "pk_accassitems": [...self.state.selectRow],
            "class": "nccloud.pubimpl.gl.account.AccbalLinkAssbalParamTransfer",
            "from": "accbal" // 联查来源：(多主体)科目余额表accbal
        }
    }else if(param.key === 'multiAccountShow'){//多核算账簿列示联查
        result = {
            ...self.state.queryDatas,
            link: {...selectRow},
            "class": "nccloud.pubimpl.gl.account.AccbalLinkMultiorgParamTransfer",
            "from": "multicorp"
        }
    }else if(param.key === 'relevanceSearch' || param.key === 'cashFlowSearch'){
        let voucherApp=voucherRelatedApp(voucher_link);
        result = {
            link: {...selectRow},
            pagekey: 'link',
            n: json['childmodules-000003'],/* 国际化处理： 联查凭证*/
            c: voucherApp.appcode/*'20020PREPA'*/,
            id: selectRow.pk_voucher,
            pk_voucher: selectRow.pk_voucher,
            status: 'browse',
            backflag: 'noback',
            appcode:  voucherApp.appcode,
            ifshowQuery: true
        }
    }else if(param.key === 'assistDetail'){//跳转到辅助明细账
        let gziptools = new gzip();
        result = {
            ...self.state.queryDatas,
            pageindex: String(self.dataPage + 1),
            link: {...selectRow},
            "class": "nccloud.pubimpl.gl.account.assattbalance.AssAttrLinkAssdetailParamTransfer"//
        }
        result = gziptools.zip(JSON.stringify(result))
    }else if(param.key === 'threeDetailToJournal'){//三栏式明细账中"过滤"的跳转参数
        result = {
            origparam: {...self.state.queryDatas},
            link: {...selectRow},
            "class": "nccloud.pubimpl.gl.account.DetailbookLinkSequenceParamTransfer"
        }
    }else if(param.key === 'detailToJournal'){
        result = {
            ...self.state.paramObj,
            link: {...selectRow},
            "class": "nccloud.pubimpl.gl.lightgl.assdetail.AssDetailLinkSequenceParamTransfer"
        }
    }else if(param.key === 'cashFlowDetailSearch'){
        if(param.nodeParam === 'analysisNode'){
            result = {
                ...param.searchParam,
                pagecode: param.pagecode,
                innercorp: selectRow.innercorp,
                pk_detail: selectRow.pk_detail ,
                pk_cashflow: selectRow.pk_cashflow,
            }
        }else{
            result = {
                pagecode: param.pagecode,
                pk_cashflow: selectRow.pk_cashflow,
                ...param.searchParam
            }
        }

    }
    return result;
}
let jumpToDetail = (self, data, paramObj) => {
    let gziptools = new gzip();
    if(paramObj.key === 'relevanceSearch' || paramObj.key === 'cashFlowSearch'){
        openToVoucher(self,data);
        // if(paramObj.key === 'relevanceSearch'){
        //     openToVoucher(self,data);
        //     // self.props.openTo(
        //     //     paramObj.jumpTo,
        //     //     {
        //     //         appcode: paramObj.appcode,
        //     //         ...data
        //     //     }
        //     // );
        // }else{
        //     self.props.openTo(
        //         paramObj.jumpTo,
        //         {
        //             appcode: paramObj.appcode,
        //             // pagecode: `${paramObj.appcode}PAGE`,
        //             ...data
        //         }
        //     );
        // }

    }else if(paramObj.key === 'cashFlowDetailSearch'){
        self.props.openTo(
            paramObj.jumpTo,
            {
                appcode: paramObj.appcode,
                pagecode: paramObj.pagecode,
                status: gziptools.zip(JSON.stringify(data))
            }
        );
    }else{
        self.props.openTo(
            paramObj.jumpTo,
            {
                appcode: paramObj.appcode,
                pagecode: paramObj.pagecode,
                status: JSON.stringify(data),
                id: '12wew24'
            }
        );
        self.setState({
            selectRow: []
        })
    }

}
//获取handsonTable当前选中行数据
let getSelectRowData=(self)=>{
    let selectRowData=self.refs.balanceTable.getRowRecord();
    return selectRowData;
}

let getSelectRow = (param) => {
    self.setState({
        selectRow:[...self.state.selectRow.concat(param)]
    })
}

/**
 * 联查方法群结束：
 * */



/**
 * "返回币种"组件：
 * self: 对应对组件对象；
 * paramObj: {
 *      key: 'key值',
 *      value: '对应值',
 *      edit: true/false,是否可编辑
 * }
 * */
export let returnMoneyType = (self, paramObj, json, disabled=false) => {
    let title = paramObj.title ? paramObj.title : json['childmodules-000004'];/* 国际化处理： 返回币种*/
    return (
        <Row className="myrow">
            <Col md={2} sm={2}>
                <span className="nc-theme-form-label-c">{`${title}：`}</span>
            </Col>
            <Col md={10} sm={10} >
               <Radio.NCRadioGroup
                    selectedValue={paramObj.value}
                    onChange={(value) => {
                        handleValueChange(self, paramObj.key, value)
                    }}
               >
                    <Radio value="1" id='firstRadio' disabled={disabled}>
                        {json['childmodules-000008']} {/* 国际化处理： 组织本币*/}
                    </Radio>
                    <Radio value="2" id='secondRadio'disabled={disabled ? disabled : paramObj.groupEdit}>
                        {json['childmodules-000009']} {/* 国际化处理： 集团本币*/}
                    </Radio>
                    <Radio value="3" id='threeRadio' disabled={disabled ? disabled : paramObj.gloableEdit}>
                        {json['childmodules-000010']} {/* 国际化处理： 全局本币*/}
                    </Radio>
                </Radio.NCRadioGroup>
            </Col>
        </Row>
    )
}
/**
 * "返回币种"组件结束
 * */
let handleValueChange = (self, key, value) => {//state键值对改变方法

    self.setState({
        [key]: value
    })
}

/**
 * 获取当前日期
 * param: boolean; true:显示时分秒
 * */
export let getNowFormatDate = (param) => {
    let date = new Date();
    let seperator1 = "-";
    let seperator2 = ":";
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    let currentdate;
    if(param){
        currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
            + " " + date.getHours() + seperator2 + date.getMinutes()
            + seperator2 + date.getSeconds();
    }else{
        currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
    }

    return currentdate;
}


/**
 *会计期间；日期的公共函数
 *
 * */


export let accountPeriodStart = (self, value) => {

    let {rangeDate} = self.state;
    if(value.values && Object.keys(self.state.end).length !== 0 ){
        rangeDate[0] = value.values && value.values.begindate.value;
        rangeDate[1] = self.state.enddate.refname !== '' && self.state.enddate
    }else {
        rangeDate = [];
    }

    self.setState({
        start: value,
        startyear: value.values ? value.values.periodyear.value : '',
        startperiod: value.values ? value.values.accperiodmth.value : '',
        begindate: value.values ? value.values.begindate.value : '',
        rangeDate: [...rangeDate],
    })

}

export let accountPeriodEnd = (self, value) => {

    let accountingBook;
    if (Array.isArray(self.state.accountingbook)) {
        if (self.state.accountingbook[0]) {
            accountingBook = self.state.accountingbook[0].refpk
        }
    } else {
        if (self.state.accountingbook) {
            accountingBook = self.state.accountingbook.refpk;
        }
    }
    if (accountingBook && value.values && (Object.keys(value.values.enddate).length === 0 || !value.values.enddate.value) ){

        getAdjustTime(self, accountingBook, value)
    }
    let {rangeDate} = self.state;
    if(value.values && Object.keys(self.state.start).length !== 0){
        if (self.state.start.values) {
            rangeDate[0] = self.state.start.values && self.state.start.values.begindate.value;
        }
        rangeDate[1] = value.values && value.values.enddate.value;
    }else {
        rangeDate = [];
    }

    // rangeDate.push(v.values.enddate.value)
    self.setState({
        end: value,
        endyear: value.values ? value.values.periodyear.value : '',
        endperiod: value.values ?  value.values.accperiodmth.value : '',
        enddate:  value.values ? value.values.enddate.value : '',
        rangeDate: [...rangeDate]
    })
}

//删除对象指定属性
export const delKeyOfObj = (obj, keys) => {
    keys.map((key, index)=>{
        delete obj[key]
    })
    return obj
}
