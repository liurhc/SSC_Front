/**
 *   Created by Liqiankun on 2018/09/11
 *   handsontable的点击事件
 *   param: 标记来源哪个节点
 */

export function handleSimpleTableClick (e, coords, td, param){
    if(td.innerHTML != ''){
        this.props.button.setDisabled({linkdetail: false});
        if(param === 'multiagentSubjectBalance'){//多主体科目余额表中的"联查明细" 和 "联查辅助"
            this.props.button.setDisabled({assbal: false, detailbook: false});
        }
        if(
            param === 'threedetail' || param === 'journal'
            || param === 'assistDetailAccount' || param === 'cashAccount'
            || param === 'bankAccount' || param === 'assistPropertyBalance'
            || param === 'capitalReport'
        ){//三栏式明细账的过滤和联查凭证 / 序时账的联查凭证
            this.props.button.setDisabled({ filter: false, linkvoucher: false});
        }
        if(param === 'accbalance'){
            //detailbook:true, accbook:true, assbal:true, multiorg:true
            this.props.button.setDisabled({
                detailbook:false, accbook:false,
                assbal:false, multiorg:false
            });
        }
    }else if(td.innerHTML === ''){
        this.props.button.setDisabled({linkdetail: true});
        if(param === 'multiagentSubjectBalance'){//多主体科目余额表中的"联查明细" 和 "联查辅助"
            this.props.button.setDisabled({assbal: true, detailbook: true});
        }
        if(
            param === 'threedetail' || param === 'journal'
            || param === 'assistDetailAccount' || param === 'cashAccount'
            || param === 'bankAccount' || param === 'assistPropertyBalance'
            || param === 'capitalReport'
        ){//三栏式明细账的过滤和联查凭证 / 序时账的联查凭证
            this.props.button.setDisabled({ filter: true, linkvoucher: true});
        }
        if(param === 'accbalance'){
            //detailbook:true, accbook:true, assbal:true, multiorg:true
            this.props.button.setDisabled({
                detailbook:true, accbook:true,
                assbal:true, multiorg:true
            });
        }
    }
}

/**
 * option: 依据的字段
 * param: 来自哪个节点
* */
export function whetherDetail(option, param) {
    let selectCell = this.refs.balanceTable.getRowRecord();
    if(param === 'multiagentSubjectBalance'){
        this.props.button.setDisabled({
            detailbook: false, assbal:false
        })
    }
    if(param === 'Accbalance'){
        if(selectCell && this.state.queryDatas.mutibook === 'Y'){
            this.props.button.setDisabled({multiorg: false})
        }else{
            this.props.button.setDisabled({multiorg: true})
        }
    }
    let optionParam = selectCell[0] && selectCell[0][option];
    if(param === 'CashQuery'){
        optionParam = selectCell[0] && selectCell[0][option] === 'Y' ? true : false
    }
    if(optionParam){
        this.props.button.setDisabled({
            linkdetail: false
        });
        if(param === 'Accbalance'){
            this.props.button.setDisabled({
                detailbook: false, accbook: false, assbal:false
            })
        }
        if(param === 'DetailNode'){//现金流量明细查询
            this.props.button.setDisabled({
                linkvou: false
            });
        }
        if(param === 'CashQuery'){
            this.props.button.setDisabled({
                linkcf: false
            });
        }
        if(param === 'multiAccountSearch'){
            this.props.button.setDisabled({
                linkvoucher: false,
                filter: false
            });

        }
        if(param === 'assistDetailAccount'){
            this.props.button.setDisabled({
                linkvoucher: false
            });
        }
    }else{
        this.props.button.setDisabled({
            linkdetail: true//现金流量分析表的明细
        })
        if(param === 'Accbalance'){
            this.props.button.setDisabled({
                detailbook: true, accbook: true, assbal:true
            })
        }
        if(param === 'DetailNode'){//现金流量明细查询
            this.props.button.setDisabled({
                linkvou: true
            });
        }
        if(param === 'CashQuery'){
            this.props.button.setDisabled({
                linkcf: true
            });
        }
        if(param === 'multiAccountSearch'){
            this.props.button.setDisabled({
                linkvoucher: true,
                filter: true
            });

        }
        if(param === 'assistDetailAccount'){
            this.props.button.setDisabled({
                linkvoucher: true
            });
        }
    }
}