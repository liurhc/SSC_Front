import {deepClone} from 'nc-lightapp-front';

export function rowBackgroundColor(e, coords, td) {
    let flag = -1;
    let arrNum = 0;
    let sourceData = this.state.dataout.data.cells;
    sourceData.map((item) => {
        if(item[0] != null){
            arrNum++;
        }
    })
    for(let headIndex=0; headIndex<arrNum; headIndex++){
        if(sourceData[headIndex][0].style == "head"){
            flag++;
        }
    }
    if(coords.row == this.state.selectRowIndex || coords.row <= flag) return;
    let sourceSelectRow = this.refs.balanceTable.getRowRecord();

    let selectRow = deepClone(sourceSelectRow);
    if(this.state.selectRowIndex){//存在的话说明已经选择过了，要把选择过的取消
        let preSelectRow = this.state.dataout.data.cells[this.state.selectRowIndex];
        for(let i=0; i<preSelectRow.length; i++){
            if(preSelectRow[i] && this.state.selectOringColor != '#F9E2D0'){
                   //未记账浅绿色，记账灰色
                if(preSelectRow[i].lightgreen=='lightgreen'){
                    preSelectRow[i].bgColor = '#DCFADC';
                }else if(preSelectRow[i].lightgray=='lightgray'){
                    preSelectRow[i].bgColor = '#E6E6E6';
                } else{
                    preSelectRow[i].bgColor = this.state.selectOringColor;  
                }         
            }else if(preSelectRow[i]){
                    preSelectRow[i].bgColor = 'white';
                }              
            }
    }
    for(let rowIndex=0; rowIndex<sourceSelectRow.length; rowIndex++){
       // 给未记账行添加标记
        if(sourceSelectRow[rowIndex] && sourceSelectRow[rowIndex].bgColor=='#DCFADC'){
            sourceSelectRow[rowIndex].lightgreen='lightgreen'
        }
        // 给记账行添加标记
        if(sourceSelectRow[rowIndex] && sourceSelectRow[rowIndex].bgColor=='#E6E6E6'){
            sourceSelectRow[rowIndex].lightgray='lightgray'
        }
        if(sourceSelectRow[rowIndex]){
            sourceSelectRow[rowIndex].bgColor = '#F9E2D0';
            sourceSelectRow[rowIndex].style = "glRowColor"
        }
    }
    this.setState({
        selectRowIndex: coords.row,
       // selectOringColor: sourceSelectRow[0].bgColor
    },()=>{
        this.refs.balanceTable.updateSettings()
    });

    // return;
    // this.tableTrs = document.getElementsByTagName('Tbody')[0].getElementsByTagName('tr');
    // for(let i=1; i<this.tableTrs[coords.row].children.length; i++){
    //     this.tableTrs[coords.row].children[i].style.background = '#F9E2D0';
    // }
}