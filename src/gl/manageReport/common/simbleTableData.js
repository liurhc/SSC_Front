// import {toast} from "../../public/components/utils";
import {ajax,deepClone,toast} from 'nc-lightapp-front';
import { tableDefaultData } from "../defaultTableData";
/**
 *   Created by Liqiankun on 2018/8/18
 *   整理handsonTable的数据格式
 */

export let setData = (self,sourceData, flag) => {
    let dataout=tableDefaultData;
    if(!flag){
        toast({
            color: 'success'
        })
    }
    let data;
    if(typeof sourceData === 'string'){
        data = JSON.parse(sourceData)
    }else{
        data = sourceData;
    }

    if (data) {
        let num = 1;
        let numArr = [];
        let rows = [];   //表格显示的数据合集
        let columHead = []; //第一层表头
        let parent = []; //存放一级表头，二级表头需要补上
        let parent1 = []; //存放二级表头，三级表头需要补上
        let child1 = []; //第三层表头
        let child2 = []; //第二层表头
        let colKeys = [];  //列key值，用来匹配表体数据
        let colAligns=[];  //每列对齐方式
        let colWidths=[];  //每列宽度
        let oldWidths=[];
        let checkTableHeadNum='0'//表头层数
        let flagParam = 'N';
        let columnData = self.switchCount != null ? data[self.state.accountType][self.switchCount] : data[self.state.accountType];
        if(columnData){
        //1、column:[]原始数据表头信息， 整理表头数据，为合并提供数据
        columnData.forEach(function (value) {
            let valueCell = {};
            valueCell.title = value.title;
            valueCell.key = value.key;
            valueCell.align = 'center';
            valueCell.bodyAlign = 'left';
            valueCell.style = 'head';
            if(value.children){//有子元素的
                flagParam = 'Y';
                if(parent.length>0){
                    child1.push(...parent);
                }
                for(let i=0;i<value.children.length;i++){//第一层子元素
                    if(value.children[i].children){
                        checkTableHeadNum='2';
                        child2.push(...parent1);
                        //第二层子元素
                        for(let k=0;k<value.children[i].children.length;k++){
                            let child1Cell = {};
                            child1Cell.title = value.children[i].title;
                            child1Cell.key = value.children[i].key;
                            child1Cell.align = 'center';
                            child1Cell.bodyAlign = 'left';
                            child1Cell.style = 'head';
                            let child2Cell = {};
                            child2Cell.title = value.children[i].children[k].title;
                            child2Cell.key = value.children[i].children[k].key;
                            child2Cell.align = 'center';
                            child2Cell.bodyAlign = 'left';
                            child2Cell.style = 'head';
                            columHead.push(valueCell);
                            child1.push(child1Cell);
                            child2.push(child2Cell);
                            colKeys.push(value.children[i].children[k].key);
                            colAligns.push(value.children[i].children[k].align);
                            colWidths.push({'colname':value.children[i].children[k].key,'colwidth':value.children[i].children[k].width});
                        }
                        parent1 = [];
                    }else {
                        checkTableHeadNum='1';
                        let innerCell2 = {};
                        innerCell2.title = value.children[i].title;
                        innerCell2.key = value.children[i].key;
                        innerCell2.align = 'center';
                        innerCell2.bodyAlign = 'left';
                        innerCell2.style = 'head';
                        columHead.push(valueCell);
                        child1.push(innerCell2);
                        parent1.push(innerCell2);
                        colKeys.push(value.children[i].key);
                        colAligns.push(value.children[i].align);
                        colWidths.push({'colname':value.children[i].key,'colwidth':value.children[i].width});
                    }
                }
                parent = [];
            }else{//无子元素的
                checkTableHeadNum='0';
                let cellObj = {};
                cellObj.title = value.title;
                cellObj.key = value.key;
                cellObj.align = 'center';
                cellObj.bodyAlign = 'left';
                cellObj.style = 'head';
                columHead.push(valueCell);//存放第一层表头
                parent1.push(cellObj);//存放第二次表头，这里是无子元素的第二层，理解为先占位，马上给合并掉
                parent.push(cellObj);
                colKeys.push(value.key);
                colAligns.push(value.align);
                colWidths.push({'colname':value.key,'colwidth':value.width});
            }
        });
        let columheadrow = 0; //表头开始行

        let mergeCells = [];
        let row,col,rowspan,colspan;
        let headcount = 1; //表头层数
        if(child1.length>0){
            headcount++;
        }
        if(child2.length>0){
            headcount++;
        }
        let currentCol = 0;//当前第几列

        //2、计算合并信息 (后续优化成递归算法)
        for(let i=0;i<columnData.length;i++) {
            let value =columnData[i];
            //*********
            if(value.children){//有子元素的
                let childLeng = value.children.length;
                let headCol = currentCol;
                for(let j=0;j<childLeng;j++){
                    if(value.children[j].children){
                        let value2=value.children[j].children;
                        let childlen = value2.length;
                        let headCol2 = currentCol;
                        for(let k=0;k<childlen;k++){
                            if(value2[k].children){

                            }else{
                                mergeCells.push([2,currentCol,2,currentCol]);
                                currentCol++;
                            }
                        }
                        mergeCells.push([1,headCol2,1,currentCol-1]);
                        // currentCol = currentCol+childlen;

                    }else {
                        mergeCells.push([columheadrow+1,currentCol,headcount-1,currentCol]);
                        currentCol++;
                    }
                }
                mergeCells.push([columheadrow,headCol,columheadrow,currentCol-1]);
            }else{//没有子元素对象
                mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
                currentCol++;
            }
        }

        if(parent.length>0){
            child1.push(...parent);
        }
        if(child2.length>0&&parent1.length>0){
            child2.push(...parent1);
        }

        rows.push(columHead);
        if(child1.length>0 && flagParam === 'Y'){
            rows.push(child1);
        }
        if(child2.length>0 && flagParam === 'Y'){
            rows.push(child2);
        }
        //3、表体数据整理
        if(data!=null && data.data.length>0){
            let rowdata = [];
            let flag = 0;
            for(let i=0;i<data.data.length;i++){
                flag++;
                for(let j=0;j<colKeys.length;j++){

                    let itemObj = {
                        align: 'left',
                        style: 'body'
                    };
                    itemObj.title = data.data[i][colKeys[j]];
                    itemObj.key = colKeys[j] + flag;
                    itemObj.link = data.data[i].link;
                    itemObj.linkmsg = data.data[i].linkmsg;// 联查的提示信息
                    itemObj.align = colAligns[j];
                    if(data.data[i].tallyed){
                        itemObj.style = 'glRowColor';
                        if(data.data[i].tallyed === 'true'){
                            itemObj.bgColor = '#E6E6E6'
                            itemObj.gray="gray"
                        }else if(data.data[i].tallyed === 'false'){
                            itemObj.bgColor = '#DCFADC'
                            itemObj.green="green"
                        }

                    }
                    if(data.data[i].hasAss){
                        itemObj.style = 'glRowColor';
                        if(data.data[i].hasAss === 'true'){
                            itemObj.color = '#1C7ED9'
                        }
                    }
                    // rowdata.push(data.data[i][colKeys[j]])
                    rowdata.push(itemObj)
                }
                rows.push(rowdata);
                rowdata = [];
            }
        }

        let rowhighs = [];
        for (let i=0;i<rows.length;i++){
            rowhighs.push(23);
        }
        self.state.dataout.data.cells= rows;//存放表体数据
        self.state.dataout.data.widths=colWidths;
        oldWidths.push(...colWidths);
        self.state.dataout.data.oldWidths = oldWidths;
        self.state.dataout.data.rowHeights=rowhighs;
        let frozenCol;
        for(let i=0;i<colAligns.length;i++){
            if(colAligns[i]=='center'){
                frozenCol = i;
                break;
            }
        }

        self.state.dataout.data.fixedColumnsLeft = 0; //冻结
        self.state.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结

        let quatyIndex = [];
        for (let i=0;i<colKeys.length;i++){
            if(colKeys[i].indexOf('quantity')>0){
                quatyIndex.push(i);
            }
        }


        if(mergeCells.length>0){
            self.state.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
        }

        let headAligns=[];

        for(let i=0;i<headcount+columheadrow;i++){
            for(let j=0;j<columHead.length;j++){
                headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
            }
        }
        self.state.dataout.data.cell= headAligns,
            self.state.dataout.data.cellsFn=function(row, col, prop) {
                let cellProperties = {};
                if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
                    cellProperties.align = colAligns[col];
                    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
                }
                return cellProperties;
            }
        self.state.dataout.data.checkTableHeadNum=(headcount-1).toString();
        self.state.dataout.data.freezing = {//固定表头
            "isFreeze" : true,
            "row": self.state.dataout.data.fixedRowsTop,
            "col" : self.state.dataout.data.fixedColumnsLeft
        }
        self.setState({
            dataout:self.state.dataout,
            showTable: true,
            showModal: false,
            MainSelectModalShow: false,
            textAlginArr: colAligns
        });
    }else{
        self.setState({
            dataout:dataout,
            showTable: true,
            showModal: false,
            MainSelectModalShow: false,
            textAlginArr: colAligns
        });
    }
    }
}