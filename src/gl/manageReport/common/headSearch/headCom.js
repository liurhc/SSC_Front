/**
 * Created by liqiankun on 2018/6/23.
 */
import React, {Component} from 'react';
import {high, base, getMultiLang} from 'nc-lightapp-front';
import './index.less'
import SeachModal from "../../../gl_obversion/obversion_rule/list/search";
const {
  NCInput: Input,
  NCSelect:Select,
  NCTooltip:Tooltip,
    NCButton:Button
} = base;
class HeadCom extends Component{
  constructor(props){
    super(props);
    this.state = {
        json: {}
    }
    this.renderInput = this.renderInput.bind(this);
  }

    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'headcom',domainName:'gl',currentLocale:'simpchn',callback});
    }

  renderInput(type, fn, handleSubjectList){
    let {
        content, lastThre, accountType,
        disabled, assistList, threeallSubject,
        dataPage, subjectListSingleValue, typeDisabled
    } = this.props;
    let renderContent = {};
    if(content && content.pageinfo || lastThre && lastThre.queryobjname){
        renderContent.queryobjname = content && content.pageinfo || lastThre && lastThre.queryobjname
    }else{
        renderContent.queryobjname = ''
    }

    switch (type){
        case this.state.json['headcom-000000']: // /* 国际化处理： 账簿格式*/
        return <Select
            showClear={false}
            fieldid='accountType'
            disabled={typeDisabled}
            onChange={(value) => fn('accountType', value)}
            value = {accountType}
        >
          <Select.NCOption value="quantityamountcolumn">{this.state.json['headcom-000017']}</Select.NCOption>{/* 国际化处理： 数量金额式*/}
          <Select.NCOption value="amountcolumn">{this.state.json['headcom-000018']}</Select.NCOption>{/* 国际化处理： 金额式*/}
        </Select>;
        case this.state.json['headcom-000001']: // /* 国际化处理： 对象*/
        case this.state.json['headcom-000002']: // /* 国际化处理： 表头查询对象*/
        case this.state.json['headcom-000003']: // /* 国际化处理： 查询对象*/
        case this.state.json['headcom-000004']: // /* 国际化处理： 统计项*/
        return (
            <Tooltip
                inverse
                placement='top'
                overlay={renderContent && renderContent.queryobjname}
            >

                <Input fieldid='queryobjname' disabled value={renderContent && renderContent.queryobjname}/>
            </Tooltip>
        );
        case this.state.json['headcom-000005']:/* 国际化处理： 币种*/
            return (
                <Tooltip
                    inverse
                    placement='top'
                    overlay={lastThre && lastThre.currtype}>
                    <Input fieldid='currtype' disabled value={lastThre && lastThre.currtype}/>
                </Tooltip>
            );
        case this.state.json['headcom-000006']:/* 国际化处理： 期间*/
        case this.state.json['headcom-000007']:/* 国际化处理： 日期范围*/
        case this.state.json['headcom-000008']:/* 国际化处理： 会计期间*/
            return (
                <Tooltip
                    inverse
                    placement='top'
                    overlay={lastThre && lastThre.period}>
                    <Input fieldid='period' disabled value={lastThre && lastThre.period}/>
                </Tooltip>
            );
        case this.state.json['headcom-000009']:/* 国际化处理： 核算账簿*/
            return (
                <Tooltip
                    inverse
                    placement='top'
                    overlay={lastThre && lastThre.accbook}>
                    <Input fieldid='accbook' disabled value={lastThre && lastThre.accbook}/>
                </Tooltip>
            );
        case this.state.json['headcom-000010']:/* 国际化处理： 计量单位*/
            return (
                <Tooltip
                    inverse
                    placement='top'
                    overlay={lastThre && lastThre.unit}>
                    <Input fieldid='unit' disabled value={lastThre && lastThre.unit}/>
                </Tooltip>
            );
        case this.state.json['headcom-000011']:/* 国际化处理： 凭证范围*/
            return (
                <Tooltip
                    inverse
                    placement='top'
                    overlay={lastThre && lastThre.voucherno}>
                    <Input fieldid='voucherno' disabled value={lastThre && lastThre.voucherno}/>
                </Tooltip>
            )
        case this.state.json['headcom-000012']:/* 国际化处理： 辅助分析表名称*/
            return (
                <Tooltip
                    inverse
                    placement='top'
                    overlay={lastThre && lastThre.reportname}>
                    <Input fieldid='reportname' disabled value={lastThre && lastThre.reportname}/>
                </Tooltip>
            )
        case this.state.json['headcom-000013']:/* 国际化处理： 科目*/
            return (
                <Tooltip
                    inverse
                    placement='top'
                    overlay={subjectListSingleValue || (lastThre && lastThre.account)}>
                    <Input fieldid='account' disabled value={ subjectListSingleValue || (lastThre && lastThre.account)}/>
                </Tooltip>
            )
        case '':
            return (
                <Tooltip
                    inverse
                    placement='top'
                    overlay={lastThre && lastThre.otherinfo}>
                    <Input fieldid='otherinfo' disabled value={lastThre && lastThre.otherinfo}/>
                </Tooltip>
            )
        case this.state.json['headcom-000014']:/* 国际化处理： 科目列表*/
            return(
                <Select
                    showClear={false}
                    fieldid='subjectList'
                    disabled={disabled}
                    onChange={(value) => handleSubjectList('subjectListSingleValue', value)}
                    value = {subjectListSingleValue}
                >
                    {
                        lastThre && lastThre.accounts.map((item, index) => {
                            return  <Select.NCOption value={item} key={item} title={item}>{item}</Select.NCOption>
                            
                        })
                    }
                </Select>
            )
        case this.state.json['headcom-000015']:/* 国际化处理： 辅助项*/
            return (
                <Tooltip
                    inverse
                    placement='top'
                    overlay={lastThre && lastThre.queryobjname}>
                    <Input fieldid='queryobjname' disabled value={lastThre && lastThre.queryobjname}/>
                </Tooltip>
            )
        case this.state.json['headcom-000016']:/* 国际化处理： 本币类型*/
            return (
                <Tooltip
                    inverse
                    placement='top'
                    overlay={lastThre && lastThre.loccurrtype}>
                    <Input fieldid='loccurrtype' disabled value={lastThre && lastThre.loccurrtype}/>
                </Tooltip>
            )
    }
  }

  render(){
    let { labels, key, changeSelectStyle, handleSubjectList} = this.props;
    return(
      <div className={"cellContainer " +this.props.isLong} key={key}>
        <div className="labelName nc-theme-common-font-c">
          <label>{labels}</label>
        </div>
        <div>
          {this.renderInput(labels, changeSelectStyle, handleSubjectList)}
        </div>
      </div>
    )
  }

}

export default HeadCom
