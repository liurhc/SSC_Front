/**
 *   Created by Liqiankun on 2018/7/19
 *   参照表格
 */
import React, {Component} from 'react';
import {high,base,ajax } from 'nc-lightapp-front';
import './index.less';

const { Refer } = high;

const {
    NCFormControl: FormControl,
    NCDatePicker:DatePicker,
    NCButton: Button,NCRadio:Radio,
    NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,
    NCLoading:Loading,
    NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,
    AutoComplete,NCDropdown:Dropdown,
    NCModal: Modal,NCInput: Input,
    NCTooltip: Tooltip,
    NCRangePicker: RangePicker
} = base;
/**
 * columns:渲染的表头
 * data: 渲染的数据
 * defaultNum: 默认渲染的行数；
 *
 * */
class ReferTable extends Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }
    componentDidMount(){
        let { defaultNum } = this.props;

        this.renderTableFirstData(defaultNum);
    }

    renderTableFirstData = (length) => {//渲染默认(多少)行数据
        let {updateReferTableData} = this.props;
        let firstData = [];
        for(let i=0; i<length; i++){
            let obj = {};
            obj.searchObj = [];
            obj.searchRange = '';
            obj.showLocal = 'N';//显示位置
            obj.accele = 'N';//小计
            obj.includesub = 'N',//包含下级
                obj.key = i
            firstData.push(obj);
        }
        updateReferTableData(firstData);//1、更新初始渲染数据
    }

    render(){
        let {columns, data} = this.props;
        return(
            <Table
                columns={columns}
                data={data}
            />
        )
    }
}

export default ReferTable