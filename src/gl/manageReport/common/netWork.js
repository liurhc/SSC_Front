/**
 *   Created by Liqiankun on 2018/7/20
 */


/**
 * ajax请求封装，
 * */
export let ajaxPromise = (url, data, successCallback, errorCallback) => {

    return new Promise((resolve, reject) => {
        ajax({
            loading: true,
            url,
            data,
            success: function (res) {
                successCallback && successCallback(res);
                resolve(res);
            },
            error: function (error) {
                errorCallback && errorCallback(error);
                reject(error);
            }
        })
    });
}