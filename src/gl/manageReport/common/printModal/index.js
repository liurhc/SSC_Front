import { base, toast, getMultiLang} from 'nc-lightapp-front';
const { NCButton: Button, NCNumber, NCModal: Modal, NCForm } = base;
import PrintModal from '../../../public/components/printModal'
// 打印模态框，继承自PrintModal
export default class RepPrintModal extends PrintModal {
    constructor(props) {
        super(props);
    }
    // 覆盖handlePrint，增加参数isPreview：1、不预览，2、预览，3、直接打印
    handlePrint(isPreview) {
        let { handlePrint } = this.props
        let { listItem } = this.state
        let baseData = {
            scopeall: '',
            ctemplate: '',
            orderunify: '',
            prepage: '',
            downpage: '',
            attach: ''
        }
        for (const key in listItem) {
            if (listItem.hasOwnProperty(key)) {
                baseData[key] = listItem[key].value
                if(key==='ctemplate'){
                    baseData.nodekey = listItem.ctemplate.code
                }
            }
        }
        if (handlePrint) {
            if(baseData.ctemplate){
                handlePrint(baseData, isPreview)
            } else {
                toast({ content: this.state.json['publiccomponents-000128'], color: 'warning' });/* 国际化处理： 请选择打印模板*/
            }
            
        }
    }
    // 覆盖render，增加预览按钮
    render() {
        let { visible, appcode } = this.props
        let { listItem, json } = this.state

        return (
            <div className='printModal'>
                <Modal style={{ width: '520px',minHeight:'268px',maxHeight:'525px' }} show={visible} onHide={this.handleCancel.bind(this)} id="printsun">
                    <Modal.Header>
                        <Modal.Title >{json['publiccomponents-000131']}</Modal.Title>{/* 国际化处理： 打印*/}
                    </Modal.Header >
                    <Modal.Body className='print-modal-body'>
                        <NCForm className='printform'
                            useRow={true}
                            submitAreaClassName='classArea'
                            showSubmit={false}　>
                            {this.renderForm(listItem)}
                        </NCForm>
                    </Modal.Body>
                    <Modal.Footer >
                        <Button size="sm" fieldid='preview' onClick={this.handlePrint.bind(this, 1)}>{json['publiccomponents-000149']}</Button>{/* 国际化处理： 预览*/}
                        <Button size="sm" fieldid='print' onClick={this.handlePrint.bind(this, 2)}>{json['publiccomponents-000131']}</Button>{/* 国际化处理： 打印*/}
                        <Button fieldid='cancel' onClick={this.handleCancel.bind(this)}>{json['publiccomponents-000010']}</Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div >

        )
    }
}