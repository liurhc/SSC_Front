import poc from "../../../../../uap/common/components/printOnClient";
const { printPreview, printOnClient, printerView } = poc;

/**
 * 模板打印
 * @param {*} props
 * @param {*} url
 * @param {*} appcode 
 * @param {*} nodekey 
 * @param {*} ctemplate 
 * @param {*} paramObj
 * @param {*} isPreview 1、预览，2、不预览，3、直接打印
 */
export default function printRequire(props,url,appcode,nodekey,ctemplate,paramObj,isPreview){
    if (isPreview == 1) {
        // 预览
        // printerView(
        printPreview(
            props,
            url, //后台服务url
            {
                billtype: 'C0',
                appcode: appcode,
                nodekey: '',
                printTemplateID: ctemplate,
                oids: [],
                userjson: JSON.stringify(paramObj),
                type: '1',
                realData: "true",
                totalPage:5
            },
            false
        );
    } else if (isPreview == 2) {
        // 打印
        // printOnClient(
        printerView(
            props,
            url, //后台服务url
            {
                billtype: 'C0',
                appcode: appcode,
                nodekey: '',     //模板节点标识
                printTemplateID: ctemplate,
                oids: [],
                userjson: JSON.stringify(paramObj),
                type: '1',
                realData: "true",
                totalPage:5
            },
            false
        );
    } else if (isPreview == 3) {
        // 直接打印
        printOnClient(
            props,
            url, //后台服务url
            {
                funcode: appcode,
                appcode: appcode,      //小应用编码
                nodekey: '',     //模板节点标识
                oids: [],    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印
                // download: true ? "fileStream" : null,//"fileStream"代表文件打印，默认为null,非必传
                suffx:['pdf'],//默认为null,非必传
                type: "1",// 默认为"1",非必传，为"3"时代表文件打印,
                //需要调用的服务端地址，非必填,测试功能使用时格式必须是protocol+ip+port
                userjson: JSON.stringify(paramObj),
                // socketIp: "10.11.117.43"
            },
            true
        );
    }
}
