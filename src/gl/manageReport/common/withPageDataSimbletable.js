import {toast} from "../../public/components/utils";

export let setData = (self, data) => {//整理表格数据
    toast({
        color: 'success',
        duration: 1,
    })
    let rows = [];   //表格显示的数据合集
    let columHead = []; //第一层表头
    let parent = []; //存放一级表头，二级表头需要补上
    let parent1 = []; //存放二级表头，三级表头需要补上
    let child1 = []; //第三层表头
    let child2 = []; //第二层表头
    let colKeys = [];  //列key值，用来匹配表体数据
    let colAligns=[];  //每列对齐方式
    let colWidths=[];  //每列宽度
    let oldWidths=[];
    let textAlginArr = self.state.textAlginArr.concat([]);      //对其方式
    let columnData = self.switchCount != null ? data[self.state.accountType][self.switchCount] : data[self.state.accountType];
    columnData.forEach((value) => {//column:[]原始数据表头信息
        let valueCell = {};
        valueCell.title = value.title;
        valueCell.key = value.key;
        valueCell.align = 'center';
        valueCell.bodyAlign = 'left';
        valueCell.style = 'head';
        if(value.children){
            self.setState({
                flow: true
            })
            if(parent.length>0){
                child1.push(...parent);
            }
            for(let i=0;i<value.children.length;i++){
                if(value.children[i].children){
                    child2.push(...parent1);
                    for(let k=0;k<value.children[i].children.length;k++){
                        let child1Cell = {};
                        child1Cell.title = value.children[i].title;
                        child1Cell.key = value.children[i].key;
                        child1Cell.align = 'center';
                        child1Cell.bodyAlign = 'left';
                        child1Cell.style = 'head';
                        let child2Cell = {};
                        child2Cell.title = value.children[i].children[k].title;
                        child2Cell.key = value.children[i].children[k].key;
                        child2Cell.align = 'center';
                        child2Cell.bodyAlign = 'left';
                        child2Cell.style = 'head';
                        columHead.push(valueCell);
                        child1.push(child1Cell);
                        child2.push(child2Cell);
                        colKeys.push(value.children[i].children[k].key);
                        colAligns.push(value.children[i].children[k].align);
                        colWidths.push({'colname':value.children[i].children[k].key,'colwidth':value.children[i].children[k].width});
                    }
                    parent1 = [];
                }else {
                    let innerCell2 = {};
                    innerCell2.title = value.children[i].title;
                    innerCell2.key = value.children[i].key;
                    innerCell2.align = 'center';
                    innerCell2.bodyAlign = 'left';
                    innerCell2.style = 'head';
                    columHead.push(valueCell);
                    child1.push(innerCell2);
                    parent1.push(innerCell2);
                    colKeys.push(value.children[i].key);
                    colAligns.push(value.children[i].align);
                    colWidths.push({'colname':value.children[i].key,'colwidth':value.children[i].width});
                }
            }
            parent = [];
        }else{
            let cellObj = {};
            cellObj.title = value.title;
            cellObj.key = value.key;
            cellObj.align = 'center';
            cellObj.bodyAlign = 'left';
            cellObj.style = 'head';
            columHead.push(valueCell);
            parent1.push(cellObj);
            parent.push(cellObj);
            colKeys.push(value.key);
            colAligns.push(value.align);
            colWidths.push({'colname':value.key,'colwidth':value.width});
        }
    });
    let columheadrow = 0; //表头开始行
    // if (data.headtitle){
    //    columheadrow = 1;
    //    let headtitle = [];
    //    data.headtitle.forEach(function (value) {
    //     headtitle.push(value[0]);
    //     headtitle.push(value[1]);
    //    });
    //    for(let i=headtitle.length;i<columHead.length;i++){
    //     headtitle.push('');
    //    }
    //    rows.push(headtitle);
    // }

    let mergeCells = [];
    let row,col,rowspan,colspan;
    let headcount = 1; //表头层数
    if(child1.length>0){
        headcount++;
    }
    if(child2.length>0){
        headcount++;
    }
    let currentCol = 0;
    //计算表头合并格
    for(let i=0;i<columnData.length;i++) {
        let value = columnData[i];
        //*********
        if(value.children){//有子元素的
            let childLeng = value.children.length;
            mergeCells.push([columheadrow, currentCol, columheadrow, currentCol + childLeng -1 ]);

            let headCol = currentCol;
            for(let i=0;i<value.children.length;i++){

                let childlen = 0;
                if(value.children[i].children){

                    let childlen = value.children[i].children.length;
                    currentCol = currentCol+childlen;
                    for(let k=0;k<value.children[i].children.length;k++){
                        textAlginArr.push(value.children[i].children[k].align);
                    }
                }else if(childlen > 0){//子元素里面没有子元素的

                    mergeCells.push([columheadrow+1, currentCol, columheadrow+2, currentCol]);//cell
                    currentCol++;
                }else {
                    currentCol++;
                    textAlginArr.push(value.children[i].align);
                }
            }
        }else{//没有子元素对象
            mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
            currentCol++;
            textAlginArr.push(value.align);
        }
    }
    if(parent.length>0){
        child1.push(...parent);
    }
    if(child2.length>0&&parent1.length>0){
        child2.push(...parent1);
    }

    rows.push(columHead);

    if(child1.length>0 && self.state.flow){
        rows.push(child1);
    }
    if(child2.length>0 && self.state.flow){
        rows.push(child2);
    }

    if(data!=null && data.data.pagedata.length>0){

        let rowdata = [];
        let flag = 0;
        for(let i=0;i<data.data.pagedata.length;i++){
            flag++;
            for(let j=0;j<colKeys.length;j++){
                // {
                //     "title":"11163",
                //     "key":"pk_org",
                //     "align":"left",
                //     "style":"body"
                // }
                let itemObj = {
                    align: 'right',
                    style: 'body'
                };
                itemObj.title = data.data.pagedata[i][colKeys[j]];
                itemObj.key = colKeys[j] + flag;
                itemObj.link = data.data.pagedata[i].link;
                itemObj.align = colAligns[j]
                if(data.data.pagedata[i].tallyed){
                    itemObj.style = 'glRowColor';
                    if(data.data.pagedata[i].tallyed === 'true'){
                        itemObj.bgColor = '#E6E6E6'
                    }else if(data.data.pagedata[i].tallyed === 'false'){
                        itemObj.bgColor = '#DCFADC'
                    }

                }
                if(data.data.pagedata[i].hasAss){
                    itemObj.style = 'glRowColor';
                    if(data.data.pagedata[i].hasAss === 'true'){
                        itemObj.color = '#1C7ED9'
                    }
                }
                // rowdata.push(data.data[0].pagedata[i][colKeys[j]])
                rowdata.push(itemObj)
            }
            rows.push(rowdata);
            rowdata = [];
        }
    }

    let rowhighs = [];
    for (let i=0;i<rows.length;i++){
        rowhighs.push(23);
    }
    self.state.dataout.data.cells= rows;//存放表体数据

    self.state.dataout.data.widths=colWidths;
    oldWidths.push(...colWidths);
    self.state.dataout.data.oldWidths = oldWidths;
    self.state.dataout.data.rowHeights=rowhighs;
    let frozenCol;
    for(let i=0;i<colAligns.length;i++){
        if(colAligns[i]=='center'){
            frozenCol = i;
            break;
        }
    }
    self.state.dataout.data.fixedColumnsLeft = 0; //冻结
    self.state.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结

    let quatyIndex = [];
    for (let i=0;i<colKeys.length;i++){
        if(colKeys[i].indexOf('quantity')>0){
            quatyIndex.push(i);
        }
    }


    if(mergeCells.length>0){
        self.state.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
    }

    let headAligns=[];

    for(let i=0;i<headcount+columheadrow;i++){
        for(let j=0;j<columHead.length;j++){
            headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
        }
    }
    self.state.dataout.data.cell= headAligns,
        self.state.dataout.data.cellsFn=function(row, col, prop) {
            let cellProperties = {};
            if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
                cellProperties.align = colAligns[col];
                cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
            }
            return cellProperties;
        }
    self.state.dataout.data.freezing = {//固定表头
        "isFreeze" : true,
        "row": self.state.dataout.data.fixedRowsTop,
        "col" : self.state.dataout.data.fixedColumnsLeft
    }
    self.setState({
        dataout:self.state.dataout,
        showModal: false,
        textAlginArr: colAligns
    });
}