import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
//import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import '../../../../css/searchModal/index.less';



const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input, NCTooltip: Tooltip,
	NCRangePicker: RangePicker
} = base;

import { toast } from '../../../../../public/components/utils';
import {handleNumberInput} from "../../../../common/modules/numberInputFn";


const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';

// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';

import AccPeriodDefaultTreeGridRef from '../../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';
import NewModal from '../modal'
import AccountDefaultModelTreeRef from '../../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../../public/components/uapRefer.js';

import ReferEle from '../../../../../../gl/refer/voucher/OperatorDefaultRef/index.js';
import VoucherRefer from '../../../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef/index';//凭证参照
import {subjectRefer} from '../../../../referUrl'

import {
    renderMoneyType,
    getSubjectVersion,
    createReferFn,
    getCheckContent,
    businessUnit, returnMoneyType, getReferDetault
} from "../../../../common/modules/modules";
import {getAdjustTime} from "../../../../../public/hansonTableSetData/commonFn";
import DateGroup from '../../../../common/modules/dateGroup';
import CheckBoxCells from '../../../../common/modules/checkBox';
import {handleValueChange} from '../../../../common/modules/handleValueChange'
import initTemplate from '../../../../modalFiles/initTemplate';
import {getRecover} from "../../../../common/modules/recoverData";
import { FICheckbox } from '../../../../../public/components/base';

class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
		    json: {},
            dateInputPlaceholder: '',/* 国际化处理： 选择日期*/
            stringRefValue: '',
            numberRefValue: '', //数值参照的值
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            showRadio: false,//控制是否显示"会计期间"的但选框；false: 不显示
            appcode: '2002305010',
            busiDate: '', //业务日期
            isShowUnit:false,//是否显示业务单元


            pk_accperiodscheme: '',//会计期间参数
			showNewModal: false,
            renderData: [], //我的弹框里数据
			innerSelectName: [], //"查询范围"里存放的数据
            innerSelectData: [], //最里层选择数据的项
            rangefrom: '', //余额范围起始值
            rangeto: '', //余额范围结束值


            listItem:{},//业务单元中模板数据对应值
			selectedSort: 'true',//排序方式
			selectedShowType: 'false', //多主题显示方式
			doubleCell: false, // 多业务单元合并
			allLast: true, // 所有末级是否选中
			outProject: false, //表外科目是否选中
			multiProjectMerge: true, //多主题合并
			subjectShow: true, //按主题列示
			balanceMoneyLeft: '', //余额范围左边输入框
            balanceMoneyRight: '', //余额范围右边输入框
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accountingUnit: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: [],//业务单元
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表

			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向

			/***现金日计记账/多主题科目余额表：级次***/
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次

            /***现金日计记账/多主题科目余额表：科目***/
			startcode: {
				refname:'',
				refpk: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
			},    //结束科目编码

			versiondate: '', //存放选择的版本号
			versionDateArr: [], //存放请求账簿对应的版本数组
			level:0, //返回的最大级次
			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			/***无发生不显示;本期无发生无余额不显示***/
			showzerooccur: false, //无发生不显示
            ckNotShowZeroOccurZeroBalance: false, //本期无发生无余额不显示
			/***多核算账簿显示方式***/
            defaultShowStyle: 'false',
            /***多核算账簿排序方式***/
            defaultSortStyle: '1',
            cbCorpSubjDspBase: false,//按各核算账簿显示科目名称
            cbQryByCorpAss: false,//按各核算账簿查询辅助核算

			upLevelSubject: false, //显示上级科目
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: false, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			start: {},  //开始期间
			end: {}, //结束期间

			/*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
			startyear: '', //开始年
            startperiod: '',  //开始期间
			endyear: '',  //结束年
			endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示


            searchDateStyle: '0',//现金日记账 按制单/签字查询
			isversiondate: false,

            indexArr: [], //存放"查询对象"已经选择的索引



			//tabel组件头部

			//table的数据
			tableSourceData: [],





			//第二层弹框里的table
            secondData: [],//第二层表格的数据

            selectRow: [],//存放所选择的不是参照类型的数据
            page: 0, //第二个弹框中要显示的行内容索引

			madeBillPeople: {refname: '', refpk: ''}, //制单人选择的参照数据
            cashierPeople: {refname: '', refpk: ''},  //出纳人选择的参照数据
            checkPeople: {refname: '', refpk: ''},    //审核人选择的参照数据
            chargePeople: {refname: '', refpk: ''},   //记账人选择的参照数据
            abstract: '', //摘要输入值
            oppositeSide: false, //对方科目取小值
			chooseoppositeSide: true, //对方科目取小值 是否可选 true:不可选  false: 可选
			voucherRefer: {refname: '', refpk: ''}, //凭证类别的参照数据
            vocherInputLeft: '', //凭证号左侧输入框
            vocherInputRight: '', //凭证号右侧侧输入框


			mutiAccountName: [], //多栏账查询 -> 多栏账名称：
            selectMutiAccount: '', //多栏账查询 -> 多栏账名称,选中的值
			showAccountantSubject: false, //是否显示会计科目        true:显示
			showTableSubject: false, //是否显示表格参照       true: 显示

            sorttype: '0',  //排序方式//0制单日期  1凭证号
            showcolwithoutdata: true, //显示无数据栏目 Y/N
		}

		this.handleTableSelect = this.handleTableSelect.bind(this);//辅助核算表格中的⌚️
		this.balanceMoney = this.balanceMoney.bind(this);//余额范围输入触发事件

        this.sureHandle = this.sureHandle.bind(this);
        this.closeNewModal = this.closeNewModal.bind(this);
        this.outInputChange = this.outInputChange.bind(this);



        this.renderTableFirstData = this.renderTableFirstData.bind(this);// table渲染初始的数据
        this.handleSearchObj = this.handleSearchObj.bind(this);//查询对象 选择触发事件
		this.findByKey = this.findByKey.bind(this);
		this.renderSearchRange = this.renderSearchRange.bind(this);//根据'查询对象'的选择来对应渲染'查询范围'的组件
		this.renderRefPathEle = this.renderRefPathEle.bind(this);//根据传进来的url渲染对应的参照组件
		this.handleCheckbox = this.handleCheckbox.bind(this);//计算小计	包含下级复选框的点击事件
		this.secondModalSelect = this.secondModalSelect.bind(this);//第二层弹框的选择框
		this.renderNewRefPath = this.renderNewRefPath.bind(this);//渲染第二层弹框内容的参照



		this.handleDateChange = this.handleDateChange.bind(this);//日期: 范围选择触发事件
		this.onChangeShowzerooccur = this.onChangeShowzerooccur.bind(this);//无发生不显示;本期无发生无余额不显示
		this.handleSortOrShowStyle = this.handleSortOrShowStyle.bind(this);//多核算账簿排序方式/多核算账簿显示方式
		this.handleDateRadio = this.handleDateRadio.bind(this);//会计期间/日期的单选框事件


		this.handleSelectChange = this.handleSelectChange.bind(this);//<Select>的onChange事件统一触发事件
		this.renderLevelOptions = this.renderLevelOptions.bind(this);//渲染级次的<NCOption>
		this.renderReferEle = this.renderReferEle.bind(this);//渲染 制单人；出纳人；审核人；记账人
		this.handleVocherChange = this.handleVocherChange.bind(this);// 凭证号输入；不能输入小数



		this.querySubjectObj = this.querySubjectObj.bind(this);//选择"多栏账名称"时请求接口判断是否显示科目
		this.setNewTableData = this.setNewTableData.bind(this);// 出现参照table时整理数据

		this.renderPathEle = this.renderPathEle.bind(this);
		this.renderPureRefer = this.renderPureRefer.bind(this);
		this.handleValueChange = handleValueChange.bind(this);
		this.searchId = '2002305010query';
        this.handleNumberInput = handleNumberInput.bind(this);
        this.getRecover = getRecover.bind(this);
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                dateInputPlaceholder: json['20023050-000001'],/* 国际化处理： 选择日期*/
                currtype: json['20023050-000002'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['20023050-000002'],  //选择的币种名称/* 国际化处理： 本币*/
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023050', 'dategroup', 'checkbox', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
    }
    componentDidMount() {
        setTimeout(() => getReferDetault(this, false, {
            businessUnit: businessUnit,
            getInfoByBook: this.getInfoByBook
        }),0);
        setTimeout(() => initTemplate.call(this,this.props), 0)
    }

    querySubjectObj(param){//多栏账查询 -> 选择"多栏账名称"时请求接口判断是否显示组织的定义以及组织定义的数据
    	let self = this;
    	let url = '/nccloud/gl/accountrep/multibookformatquery.do';
        ajax({//请求'多栏账查询'的多栏账名称的内容
            url:url,
            data: {pk_multicol: param},
            success: function(response){
                const { success } = response;
                let flagArr = [];
                if (success) {
                    if(response.data){
                    	let paramFlag = response.data.analycoltype.display;//为'会计科目'时不显示
						let tableFlag = response.data.statcoltype //Y 显示辅助核算表格。N隐藏
                        if(paramFlag !== self.state.json['20023050-000011']){/* 国际化处理： 会计科目*/
                            self.setState({
                                showAccountantSubject: true
                            })
						}else{
                            self.setState({
                                showAccountantSubject: false
                            })
						}
                        if(tableFlag === 'Y'){
                            self.setNewTableData(response.data.stat);
                            self.setState({
                                showTableSubject: true
                            })
						}else{
                            self.setState({
                                showTableSubject: false
                            })
						}
						if(!response.data.stat){//stat不存在时直接请求数据，就是不分页请求
                            flagArr.push('N');
						}

                        response.data.stat && response.data.stat.map((item) => {
                        	if(item.dsplocation.value === 'Y'){//有一个中的为Y就分页请求
                                flagArr.push('Y')
							}
						})

						if(flagArr.includes('Y')){
                            self.props.changeQueryPageData(true, 'queryPageData');
						}

                    }
                }else {
                    toast({ content: error.message, color: 'error' });
                }
            },
            error: function (error) {
                toast({ content: error.message, color: 'error' });
            }
        });
	}
    setNewTableData(param){
    	let resultArr = [];
        param.map((item, index) => {
            let itemObj = {};
            itemObj = {...item}
            itemObj.searchObj = item.statcolname.display;
            itemObj.searchRange = item.refpath;
            itemObj.includesub = false;
            itemObj.key = index;
            resultArr.push(itemObj)
		});
        this.setState({
            tableSourceData: [...resultArr]
		})

	}
    renderPureRefer(param){//渲染参照的方法
        /**
         * param:{url: '', key: '', stateKey: ''}
         * **/
        let referEle;
        let objKey = param.key;
        if(!this.state[objKey]){//undefined
            {createScript.call(this,param.url,objKey)}

        }else{
            referEle =  (
                <Row>
                    <Col xs={12} md={12}>
                        {
                            this.state[objKey]?(this.state[objKey])(
                                {
                                    fieldid: objKey,
                                    value: this.state[param.stateKey],
                                    isMultiSelectedEnabled: param.flag,
                                    isShowDisabledData: true,
                                    queryCondition:() => {
                                        return {
                                            "pk_accountingbook": this.state.accountingbook.refpk ? this.state.accountingbook.refpk:'',
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            dateStr: this.state.isversiondate ? this.state.versiondate : this.state.busiDate
                                        }
                                    },
                                    onChange: (v)=>{
                                        this.setState({
                                            [param.stateKey]: v
                                        })
                                    }
                                }
                            ):<div/>
                        }
                    </Col>
                </Row>
            );
        }
        return referEle;
    }
    dateRefChange = (key, value, record) => {
        //let length = record.selectCell.inputlength;
        let length = value.length
        let exeReg = new RegExp("^.{0," + length + "}$");
        let result = exeReg.exec(value);
        if(result && key === 'stringRefValue'){
            record.selectRange = result[0]
            this.setState({
                [key]: result[0]
            })
        }else {
            record.selectRange = value
            this.setState({
                [key]: value
            })
        }
    }
    numberRefChange = (key, value, record, param) => {
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectRange = this.state[key], 0)

    }
	renderPathEle(record, fieldid){
        //带有参照的
        if(record.refpath){
            let mybook;
            let objKey = record.key + record.pk_multicol;
            let showArr = [];
            record.selectRange && record.selectRange.map((item) => {
                let showObj = {};
                showObj.refpk = item.pk_checkvalue;
                showObj.refname = item.checkvaluename;
                showArr.push(showObj);
            })
            if(!this.state[objKey]){//undefined
                {createScript.call(this,record.searchRange+".js",objKey)}

            }else{
                let pkOrgParam = this.state.buSecond.length > 0 ? this.state.buSecond[0].refpk : this.state.unitValueParam;
                let options = {
                    fieldid: fieldid,
                    value: showArr,
                    isMultiSelectedEnabled:true,
                    "isShowDisabledData": true,
                    queryCondition:() => {
                        return {
                            "pk_accountingbook": this.state.accountingbook.refpk ? this.state.accountingbook.refpk : '',
                            "pk_org":pkOrgParam,
                            "isDataPowerEnable": 'Y',
                            "DataPowerOperationCode" : 'fi',
                            dateStr: this.state.isversiondate ? this.state.versiondate : this.state.busiDate
                        }
                    },
                    onChange: (v)=>{
                        let {tableSourceData} = this.state;
                        let refpkArr = [];
                        v.forEach((item)=>{
                            let cellObj = {};
                            cellObj.pk_checkvalue = item.refpk;
                            cellObj.checkvaluecode = item.refcode;
                            cellObj.checkvaluename = item.refname;
                            refpkArr.push(cellObj);
                        })
                        let pk_checkvalue = [...refpkArr];
                        record.selectRange = pk_checkvalue;
                        this.setState({
                            tableSourceData
                        })
                    }
                };
                let newOPtions = {}
                if(record.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                    newOPtions = {
                        ...options,
                        isShowUnit:true,
                        "unitValueIsNeeded":false,
                        "isShowDimission":true,
                        queryCondition:() => {
                            return {
                                "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                                "pk_org": pkOrgParam,
                                "isDataPowerEnable": "Y",
                                "DataPowerOperationCode" : 'fi',
                                dateStr: this.state.isversiondate ? this.state.versiondate : this.state.busiDate,
                                "busifuncode":"all",
                                isShowDimission:true
                            }
                        },
                        unitProps:{
                            refType: 'tree',
                            refName: this.state.json['20023050-000012'],/* 国际化处理： 业务单元*/
                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                            rootNode:{refname:this.state.json['20023050-000012'],refpk:'root'},/* 国际化处理： 业务单元*/
                            placeholder:this.state.json['20023050-000012'],/* 国际化处理： 业务单元*/
                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                            treeConfig:{name:[this.state.json['20023050-000013'], this.state.json['20023050-000014']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                            isMultiSelectedEnabled: false,
                            //unitProps:unitConf,
                            isShowUnit:false
                        },
                        unitCondition:{
                            pk_financeorg: pkOrgParam,
                            'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                        },
                    }
                }else if(record.classid.length === 20){
                    newOPtions = {
                        ...options,
                        pk_defdoclist: record.classid
                    }
                }else {
                    newOPtions = {
                        ...options
                    }
                }
                mybook =  (
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state[objKey]?(this.state[objKey])(newOPtions):<div/>
                            }
                        </Col>
                    </Row>
                );
            }
            return mybook;
        }else if(record.datatype === '1'){//字符
            return <Input
                fieldid={fieldid}
                value={this.state.stringRefValue}
                onChange={(value) => this.dateRefChange('stringRefValue', value, record)}
            />;
        }else if(record.datatype === '31'){//数值
            return <Input
                fieldid={fieldid}
                value={this.state.numberRefValue}
                onChange={(value) => this.numberRefChange(value, record) }
            />;
        }else if(record.datatype === '33'){//日期
            return (
                <DatePicker
                    fieldid={fieldid}
                    format={format}
                    value={this.state.assistDateValue}
                    placeholder={dateInputPlaceholder}
                    onChange={(value) => this.dateRefChange('assistDateValue', value, record)}
                />
            );
        }
	}
    handleVocherChange(value, param){// 凭证号输入；不能输入小数
        let result = value;
        let arrResult = result.split('');//输入值变为数组
        let code = arrResult[arrResult.length - 1];//最后一次输入的元素
        let codeValue = code && code.toString().charCodeAt(0);
        if(codeValue < 48 || codeValue > 57) {//不是数字的输入
            arrResult.splice(arrResult.length - 1);
        }
        this.setState({
            [param]: arrResult.join('')
        });
	}
    renderLevelOptions(num){
    	let optionArr = [];
    	for(let i=0; i<num; i++){
            optionArr.push(<NCOption value={i+1} key={i}>{i+1}</NCOption>);
		}
        return optionArr;
	}
    handleSelectChange(value, param){//<Select>的onChange事件统一触发事件
    	this.setState({
			[param]: String(value)
		})
	}
    handleDateRadio(value, param){//会计期间/日期的单选框事件
    	this.setState({
			[param]: value
		})
	}
	handleSortOrShowStyle(value, param){//多核算账簿显示方式/多核算账簿排序方式触发事件
		this.setState({
			[param]: value
		})
	}
    handleDateChange(value){//日期: 范围选择触发事件
    	this.setState({
            begindate: value[0],
            enddate: value[1],
			rangeDate: value
		})
	}
    handleSearchObj(value, record){//查询对象选择具体科目
    	let { tableSourceData, selectRow } = this.state;
        let originData = this.findByKey(record.key, tableSourceData);
        originData.selectCell = value;

        if(value.refpath ){
            originData.type = 'referpath';
            originData.searchRange = value.refpath;
		}else if(value.datatype === '33'){
            originData.type = 'date'
		} else if(value.attr){
            originData.selectCellAttr = [...value.attr];
            originData.type = 'modal'
		}
		let newSelectRow = [...selectRow];
        newSelectRow[record.key] = record;
    	this.setState({
            tableSourceData,
			selectRow: newSelectRow
		})
	}
    handleCheckbox(key, record, value){//计算小计	包含下级复选框的点击事件
    	let {tableSourceData} = this.state;
        record[key] = value;
        this.setState({
            tableSourceData
		})

	}
    secondModalSelect(record, index){//第二层弹框的复选框
    	let {tableSourceData} = this.state;
        let newSelect = !record.select;
        record.select = newSelect;
        tableSourceData[record.itemKey].selectCellAttr[index] = record;
        this.setState({tableSourceData})
	}
    findByKey(key, rows) {
        let rt = null;
        let self = this;
        rows.forEach(function(v, i, a) {
            if (v.key == key) {
                rt = v;
            }
        });
        return rt;
    }
    renderSearchRange(record){//根据'查询对象'的选择来对应渲染'查询范围'的组件
    	switch(record.type){
			case "referpath":
				let refPathEle = this.renderRefPathEle(record);
				return refPathEle;
			case 'date':
				return;
			case 'modal':
                let attrObj = record.selectCellAttr;
                let { tableSourceData } = this.state;
                let selectObj = [];
                attrObj.map((item)=>{
                	if(item){
                        item.itemKey = record.key;
					}
					if(item && item.selectName){
                        selectObj += item.selectName + ','
					}
				})
				return (
                    <div style={{display: 'flex'}}>
                        <Input
                            style={{border: 'none'}}
							placeholder={record.selectCell && record.selectCell.name}
							value={selectObj}
						/>
                        <div className='ellipsisContainer'
                             onClick={() => this.closeNewModal(record)}
                        >
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                        </div>
                    </div>
				);
			default:
				return <Input disabled/>;
		}
	}
    renderNewRefPath(record){
        let mybook;

        if(record.refpath){
            let objKey = record.attrclassid;
            if(!this.state[objKey]){//undefined
                {createScript.call(this,record.refpath+".js",objKey)}

            }else {
                mybook = (
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state[objKey] ? (this.state[objKey])(
                                    {
                                        value:{refname: record.selectName && record.selectName[0], refpk:  record.refpk && record.refpk[0]},
                                        isMultiSelectedEnabled: true,
                                        queryCondition: () => {
                                            return {
                                                "pk_accountingbook": this.state.accountingbook[0].pk_accountingbook ? this.state.accountingbook[0].pk_accountingbook : '',
                                                // "versiondate":currrentDate,
                                            }
                                        },
                                        onChange: (v) => {
                                            let {tableSourceData} = this.state;
                                            let refpkArr = [];
                                            let selectName = [];
                                            v.forEach((item) => {
                                                refpkArr.push(item.refpk);
                                                selectName.push(item.refname)
                                            })
                                            let pk_checkvalue = refpkArr.join(',')
                                            record.pk_checkvalue = pk_checkvalue;
                                            record.selectName = selectName;
                                            record.refpk = refpkArr;
                                            this.setState({
                                                tableSourceData
                                            })
                                        }
                                    }
                                ) : <div/>
                            }
                        </Col>
                    </Row>
                );
            }
		}

        return mybook;
	}
	renderRefPathEle(record){//根据传进来的url渲染对应的参照组件
        //带有参照的
		let mybook;
		let objKey = record.key + record.selectCell.pk_checktype;
		if(!this.state[objKey]){//undefined
			{createScript.call(this,record.selectCell.refpath+".js",objKey)}

		}else{
			mybook =  (
				<Row>
					<Col xs={12} md={12}>
						{
							this.state[objKey]?(this.state[objKey])(
								{
									isMultiSelectedEnabled:true,
									queryCondition:() => {
										return {
											"pk_accountingbook": this.state.accountingbook[0].pk_accountingbook? this.state.accountingbook[0].pk_accountingbook:'',
											// "versiondate":currrentDate,
										}
									},
									onChange: (v)=>{
										let {tableSourceData} = this.state;
										let refpkArr = [];
										v.forEach((item)=>{
											let cellObj = {};
                                            cellObj.pk_checkvalue = item.refpk;
                                            cellObj.checkvaluecode = item.refcode;
                                            cellObj.checkvaluename = item.refname;
											refpkArr.push(cellObj);
										})
										let pk_checkvalue = [...refpkArr];
                                        record.selectRange = pk_checkvalue;
										this.setState({
                                            tableSourceData
										})
									}
								}
							):<div/>
						}
					</Col>
				</Row>
			);
		}
		return mybook;
	}
    renderTableFirstData(length){
    	let firstData = [];
    	for(let i=0; i<length; i++){
    		let obj = {};
    		obj.searchObj = [];
            obj.searchRange = '';
            obj.showLocal = 'N';//显示位置
            obj.accele = 'N';//小计
            obj.includesub = false,//包含下级
			obj.key = i
            firstData.push(obj);
		}
		this.setState({
            tableSourceData: [...firstData]
		})
	}

	handleTableSelect(){
	}

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}




	getCurrtypeList = () => {//获取"币种"接口
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {"localType":"1", "showAllCurr":"1"};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


    handleCurrtypeChange = (keyParam, value) => {
        this.setState({
            [keyParam]: value,
        });
    }


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取"多栏账名称"的数据：
	getInfoByBook(self, v) {
        let multiAcount = '/nccloud/gl/accountrep/multibookformatquery.do';
        ajax({//请求'多栏账查询'的多栏账名称的内容
            url:multiAcount,
            data: {pk_accountingbook: v.refpk || v.pk_accountingbook},
            success: function(response){
                const { success } = response;
                if (success) {
                    if(response.data){
                        self.setState({
                            mutiAccountName: [...response.data],
                            selectMutiAccount: response.data[0].pk_multicol,
						}, () => self.querySubjectObj(self.state.selectMutiAccount));
                        // setTimeout(() => self.querySubjectObj(self.state.selectMutiAccount), 0)
                    }else{
                        self.setState({
                            mutiAccountName: [],
                            selectMutiAccount: '',
                            showTableSubject: false
                        });
                    }
                }else {
                    toast({ content: error.message, color: 'error' });
				}
            },
            error: function (error) {
                toast({ content: error.message, color: 'error' });
            }
        });
	}

	//开始级次变化
	handleStartlvlChange(value) {
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: '2018-5-31'// value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化/本期无发生无余额不显示;按各核算账簿显示科目名称/按各核算账簿查询辅助核算
	onChangeShowzerooccur(value, param) {
		this.setState({
			[param]: value
		});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}
	//显示上级科目复选框变化
	onChangeupLevelSubject(e) {
		this.setState({upLevelSubject: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}
	handleRadioSort(param){
		this.setState({
			selectedSort: param
		})
	}
	handleShowType(param){
		this.setState({
			selectedShowType: param
		})
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
	onChangeDoubleCell(param){
		this.setState({
			doubleCell: !this.state.doubleCell
		})
	}
	changeProjectStyle(param){//所有末级   表外科目选择
		if(param === 'allLast'){
			this.setState({
				allLast: !this.state.allLast
			})
		}else{
			this.setState({
				outProject: !this.state.outProject
			})
		}
	}

	handleGTypeChange =() =>{
		
	};

    shouldComponentUpdate(nextProps, nextState){
		return true;
	}



    outInputChange(param){
    	let nameArr = [];
        param.forEach((item) => {
            nameArr.push(item.refname);
		})
		this.setState({
            innerSelectName: [...nameArr]
		})
	}
    sureHandle(param){
    	let SelectCell = [];
        // param.forEach((item) => {
        	// if(item.pk_checkvalue){
         //        SelectCell.push(item);
		// 	}
		// });
        this.setState({
			innerSelectData: [...SelectCell]
		})
    	this.closeNewModal(param);
	}
    closeNewModal(param){
		this.setState({
            showNewModal: !this.state.showNewModal,
			page: param.key
		})
    }
    balanceMoney(value, param){
        let arrResult = value.split('');//输入值变为数组
        let code = arrResult[arrResult.length - 1];//最后一次输入的元素
        let codeValue = code && code.toString().charCodeAt(0);
        if (codeValue < 48 || codeValue > 57) {//不是数字的输入给剪掉
            arrResult.splice(arrResult.length - 1);
        }
    	this.setState({
			[param]: arrResult.join('')//数组变为字符串
		})
	}

	/****
	 * param: String
	 * 根据传入的参数渲染对应的参照组件
	 * ***/
	renderReferEle(param){
    	let referUrl = "gl/refer/voucher/OperatorDefaultRef/index.js" //
        if(!this.state[param]){//undefined
            {createScript.call(this,referUrl,param)}
        }else{
            return (
                <Row>
                    <Col xs={12} md={12}>
                        {
                            this.state[param]?(this.state[param])(
                                {
                                    value:{refname: this.state.accountingbook[0] && this.state.accountingbook[0].refname, refpk: this.state.accountingbook[0] && this.state.accountingbook[0].refpk},
                                    queryCondition:() => {
                                        // return {
                                        // 	"pk_accountingbook": self.state.pk_accountingbook.value
                                        // }
                                    },
                                    onChange: (v)=>{
                                        let self = this;
                                    }

                                }
                            ):<div/>
                        }
                    </Col>
                </Row>
			)
        }
	}
	renderModalList = () => {
        this.columns = [
            {
                title: (<span fieldid='searchObj'>{this.state.json['20023050-000003']}</span>),/* 国际化处理： 核算类型*/
                dataIndex: "searchObj",
                key: "searchObj",
                render: (text, record, index) => {
                    return (
                        <span fieldid='searchObj'>{text}</span>
                    )
                }
            },
            {
                title: (<span fieldid='searchRange'>{this.state.json['20023050-000004']}</span>),/* 国际化处理： 核算内容*/
                dataIndex: "searchRange",
                key: "searchRange",
                render: (text, record,index) => {
                    let renderEle = this.renderPathEle(record, 'searchRange');
                    return renderEle;
                }
            },
            {
                title: (<span fieldid='includesub'>{this.state.json['20023050-000005']}</span>),/* 国际化处理： 包含下级*/
                dataIndex: "includesub",
                key: "includesub",
                render: (text, record) => {
                    return <Checkbox
                        checked={record.includesub}
                        onChange={(param) => {

                            if(record.selectRange && record.selectRange.length > 0){
                                this.handleCheckbox('includesub',record, param)
                            }else{
                                if(param){
                                    toast({content: this.state.json['20023050-000039'], color: 'warning'})
                                }
                            }
                        }}
                    />
                }
            }
        ];
        this.secondColumn = [
            {
                title: this.state.json['20023050-000006'],/* 国际化处理： 选择*/
                dataIndex: "select",
                key: "select",
                render: (text, record, index) => {
                    return <Checkbox
                        checked={record.select}
                        onChange={() => {
                            record.index = index;
                            this.secondModalSelect(record, index);
                        }}
                    />
                }
            },
            {
                title: this.state.json['20023050-000007'],/* 国际化处理： 查询属性*/
                dataIndex: "name",
                key: "name"
            },
            {
                title: this.state.json['20023050-000008'],/* 国际化处理： 查询值*/
                dataIndex: "refpath",
                key: "refpath",
                render: (text, record) => {
                    let renderSecondEle = this.renderNewRefPath(record);
                    return renderSecondEle;
                }
            }
        ];
        return (
            <div className='right_query'>
                <div className='query_body1'>
                    <div className='query_form multiAccountSearch'>
                        <div>
                            <Row className="myrow">
                                <Col md={2} sm={2}>
                                    <span style={{color: 'red'}}>*</span>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023050-000021']}：</span>{/* 国际化处理： 核算账簿*/}
                                </Col>
                                <Col md={7} sm={7}>
                                    <div className="book-ref">
                                        {
                                            createReferFn(
                                                this,
                                                {
                                                    url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                                    value: this.state.accountingbook,
                                                    referStateKey: 'checkAccountBook',
                                                    referStateValue: this.state.checkAccountBook,
                                                    stateValueKey: 'accountingbook',
                                                    flag: false,
                                                    queryCondition: {
                                                        pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk
                                                    }
                                                },
                                                {
                                                    businessUnit: businessUnit,
                                                    getReferInfo: this.getInfoByBook
                                                },
                                                'multiAccountSearch'
                                            )
                                        }
                                    </div>
                                </Col>
                            </Row>
                            {
                                this.state.isShowUnit ?
                                    <Row className="myrow">
                                        <Col md={2} sm={2}>
                                            <span className='nc-theme-form-label-c'>{this.state.json['20023050-000012']}：</span>{/* 国际化处理： 业务单元*/}
                                        </Col>
                                        <Col md={7} sm={7}>
                                            <div className="book-ref">
                                                <BusinessUnitTreeRef
                                                    fieldid='buSecond'
                                                    value={this.state.buSecond}
                                                    isMultiSelectedEnabled= {true}
                                                    isShowDisabledData = {true}
                                                    isHasDisabledData = {true}
                                                    queryCondition = {{
                                                        "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                                                        "isDataPowerEnable": 'Y',
                                                        "DataPowerOperationCode" : 'fi',
                                                        "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                                    }}
                                                    onChange={(v)=>{
                                                        this.getRecover('tableSourceData','includesub', 'selectRange')//
                                                        this.setState({
                                                            buSecond: v,
                                                        }, () => {
                                                            if(this.state.accountingbook && this.state.accountingbook[0]) {
                                                            }
                                                        })
                                                    }
                                                    }
                                                />

                                            </div>

                                        </Col>
                                        <Col md={3} sm={3}>
                                            <FICheckbox colors="dark" className="mycheck"  id='Multbusi' checked={this.state.multbusi}  onChange={this.onChangeMultbusi.bind(this)}>
                                                {this.state.json['20023050-000022']} {/* 国际化处理： 多业务单元合并*/}
                                            </FICheckbox>
                                        </Col>
                                    </Row> :
                                    <div></div>
                            }
                            <Row className="myrow">
                                <Col md={2} sm={2}>
                                    <span style={{color: 'red'}}>*</span>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023050-000020']}：</span>{/* 国际化处理： 多栏账名称*/}
                                </Col>
                                <Col md={4} sm={4}>
                                    <Select
                                        showClear={false}
                                        fieldid='MutiAccount'
                                        value={this.state.selectMutiAccount}
                                        onChange={(value) => {
                                            this.handleDateRadio(value, 'selectMutiAccount');
                                            this.querySubjectObj(value);
                                        }}
                                    >
                                        {
                                            this.state.mutiAccountName.map((item, indx) => {
                                                return <NCOption value={item.pk_multicol} key={indx}>{item.multicolname}</NCOption>
                                            })
                                        }
                                    </Select>
                                </Col>
                            </Row>

                            <Row className="myrow" style={{display: this.state.showAccountantSubject ? '' : 'none'}}>
                                <Col md={2} sm={2}>
                                    <span style={{color: 'red'}}>*</span>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023050-000023']}：</span>{/* 国际化处理： 科目*/}
                                </Col>
                                <Col md={4} sm={4} style={{marginLeft:-30}}>
                                    <div className="book-ref">
                                        {
                                            this.renderPureRefer(
                                                {
                                                    url: subjectRefer,
                                                    key: 'startcodeSubject',
                                                    stateKey: 'startcode',
                                                    falg: false
                                                }
                                            )
                                        }
                                    </div>
                                </Col>
                            </Row>

                            <div className="modalTable" style={{display: this.state.showTableSubject ? '' : 'none'}}>
                                <Table
                                    columns={this.columns}
                                    data={this.state.tableSourceData}
                                />
                            </div>

                            {/*会计期间*/}
                            <DateGroup
                                selectionState = {this.state.selectionState}
                                start = {this.state.start}
                                end = {this.state.end}
                                enddate = {this.state.enddate}
                                pk_accperiodscheme = {this.state.pk_accperiodscheme}
                                pk_accountingbook = {this.state.accountingbook}
                                rangeDate = {this.state.rangeDate}
                                handleValueChange = {this.handleValueChange}
                                handleDateChange = {this.handleDateChange}
                                self = {this}
                                showRadio = {this.state.showRadio}
                            />

                            {/*包含凭证：*/}
                            <CheckBoxCells
                                paramObj = {{
                                    "noChargeVoucher": {//未记账凭证
                                        show: true,
                                        disabled: this.state.disabled,
                                        checked: this.state.includeuntally,
                                        onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                                    },
                                    "errorVoucher": {//错误凭证
                                        show: true,
                                        checked: this.state.includeerror,
                                        onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                                    },
                                    "harmToBenefitVoucher": {//损益结转凭证
                                        show: false,
                                        checked: this.state.includeplcf,
                                        onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                                    },
                                    "againClassifyVoucher": {//重分类凭证
                                        show: false,
                                        checked: this.state.includerc,
                                        onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                                    },
                                }}
                            />

                            <Row className="myrow currtype-area" >
                                {/*币种*/}
                                {renderMoneyType(this.state.currtypeList,this.state.currtypeName, this.handleCurrtypeChange, this.state.json)}
                                <Col md={2} sm={2} className='currtype-checkbox'>
                                    <Checkbox
                                        colors="dark"
                                        id='ckNotShowZeroOccurZeroBalance'
                                        checked={this.state.ckNotShowZeroOccurZeroBalance}
                                        onChange={(value) => this.onChangeShowzerooccur(value, 'ckNotShowZeroOccurZeroBalance')}
                                    >
                                        {this.state.json['20023050-000024']} {/* 国际化处理： 合并同凭证分录*/}
                                    </Checkbox>
                                </Col>
                            </Row>

                            {/*返回币种：*/}
                            {
                                returnMoneyType(
                                    this,
                                    {
                                        key: 'selectedCurrValue',
                                        value: this.state.selectedCurrValue,
                                        groupEdit: this.state.groupCurrency,
                                        gloableEdit: this.state.globalCurrency
                                    },
                                    this.state.json
                                )
                            }

                            {/*分析方式*/}
                            <Row className="myrow">
                                <Col md={2} sm={2}>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023050-000025']}：</span>{/* 国际化处理： 分析方式*/}
                                </Col>
                                <Col md={9} sm={9}>
                                    <Radio.NCRadioGroup
                                        selectedValue={this.state.defaultSortStyle}
                                        onChange={(value) => this.handleSortOrShowStyle(value, 'defaultSortStyle')}
                                    >
                                        <Radio value="1" >{this.state.json['20023050-000026']}</Radio>{/* 国际化处理： 按金额分析*/}

                                        <Radio value="2" >{this.state.json['20023050-000027']}</Radio>{/* 国际化处理： 按余额分析*/}
                                    </Radio.NCRadioGroup>

                                </Col>
                            </Row>
                            <Row className="myrow">
                                <Col md={2} sm={2}>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023050-000028']}：</span>{/* 国际化处理： 排序方式*/}
                                </Col>
                                <Col md={7} sm={7}>

                                    <Radio.NCRadioGroup
                                        selectedValue={this.state.sorttype}
                                        onChange={(value) => this.handleSortOrShowStyle(value, 'sorttype')}
                                    >
                                        <Radio value="0" >{this.state.json['20023050-000029']}</Radio>{/* 国际化处理： 制单日期*/}
                                        <Radio value="1" >{this.state.json['20023050-000030']}</Radio>{/* 国际化处理： 凭证号*/}
                                    </Radio.NCRadioGroup>

                                </Col>
                                <Col md={2} xs={2} sm={2} style={{marginLeft:-50}}>
                                    <Checkbox
                                        colors="dark"
                                        className="mycheck"
                                        checked={this.state.showcolwithoutdata}
                                        onChange={(value) => this.handleSortOrShowStyle(value, 'showcolwithoutdata')}
                                    >
                                        {this.state.json['20023050-000031']} {/* 国际化处理： 显示无数据栏目*/}
                                    </Checkbox>
                                </Col>
                            </Row>

                            <NewModal
                                title= {this.state.json['20023050-000016']} ///* 国际化处理： 属性选择*/
                                record={this.state.selectRow[this.state.page]}
                                column={this.secondColumn}
                                showNewModal = {this.state.showNewModal}//控制设否显示
                                closeNewModal = {this.closeNewModal}
                                sureHandle = {this.sureHandle}
                                sureText = {this.state.json['20023050-000017']} ///* 国际化处理： 确定 */
                                cancleText = {this.state.json['20023050-000010']}/* 国际化处理： 取消*/
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    clickPlanEve = (value) => {
        this.state = deepClone(value.conditionobj4web.nonpublic);
        this.setState(this.state)
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
        //('saveSearchPlan>>', this.state)
        return {...this.state}
    }
    clickSearchBtn = () => {//点击查询框的"查询"按钮事件
	    let pk_accountingbook = [];
        let pk_unit = [];
        let moneyLeft = Number(this.state.balanceMoneyLeft);
        let moneyRight = Number(this.state.balanceMoneyRight);
        //('moneyyyy::', moneyLeft,  moneyRight, moneyLeft < moneyRight)
        if(moneyLeft > moneyRight){
            toast({
                content: this.state.json['20023050-000018'],/* 国际化处理： 余额范围查询条件信息输入有误(起始值大于终止值或是为负值)！*/
                color: 'warning'
            });
            return;
        }
        let lastAssvos = [];
        this.state.showTableSubject && this.state.tableSourceData.forEach((item)=>{
            //('assvos>>>',item);
            let resultObj = {};
            let cerrArr = [];
            resultObj.pk_checktype = item.statcolname.value;
            item.selectRange && Array.isArray(item.selectRange) && item.selectRange.map((item) => {
                cerrArr.push(item.pk_checkvalue);
            })
            resultObj.pk_checkvalue = Array.isArray(item.selectRange) ? cerrArr.join(',') : item.selectRange;
            resultObj.includeSub = item.includesub ? 'Y' : 'N';
            lastAssvos.push(resultObj)
        })
        //('lastAssvos>>', lastAssvos)
        if (Array.isArray(this.state.accountingbook)) {
            this.state.accountingbook.forEach(function (item) {
                pk_accountingbook.push(item.refpk)
            })
        }

        if (Array.isArray(this.state.buSecond)) {
            this.state.buSecond.forEach(function (item) {
                pk_unit.push(item.refpk)
            })
        }
        //('>>>>>/////???', pk_unit, this.state.accountingbook);
        //
        let data = {
            /*******核算账簿****/
            pk_accountingbook: this.state.accountingbook.refpk?this.state.accountingbook.refpk:'',
            /*******业务单元****/
            pk_unit: pk_unit,
            multibusi: this.state.multbusi,//复选框是否勾选

            /***多栏账名称：   多栏账主键***/
            pk_multicol:this.state.selectMutiAccount,

            /*******会计期间参数****/
            // querybyperiod: this.state.selectionState,
            startyear: this.state.startyear,
            startperiod: this.state.startperiod,
            endyear: this.state.endyear,
            endperiod:   this.state.endperiod,

            /*******包含凭证****/
            includeuntally: this.state.includeuntally ? 'Y' : 'N',//未记账凭证
            includeerror: this.state.includeerror ? 'Y' : 'N',// 错误凭证
            // includetransfer: this.state.includeplcf ? 'Y' : 'N',//损益结转凭证
            // includereClassify: this.state.includerc ? 'Y' : 'N',//重分类凭证

            /*******币种****/
            pk_currtype: this.state.currtype,//
            // currtypename: this.state.currtypeName,//

            /*******返回币种****/
            returncurr: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局

            /***分析方式：***/
            analymode: this.state.defaultSortStyle,//按金额1  按余额2

            /***排序方式：显示无数据栏目***/
            sorttype: this.state.sorttype,  //排序方式//0制单日期  1凭证号
            showcolwithoutdata: this.state.showcolwithoutdata, //显示无数据栏目 Y/N

            /***辅助核算表格：***/
            assvo: lastAssvos, //辅助核算， {pk_checktype,pk_checkvalue（多选逗号隔开）,includesub(Y/N)}

            /*******科目****/
            pk_accasoa: this.state.startcode.refpk,  //开始科目编码
            // endcode: this.state.endcode.refcode,    //结束科目编码
            /***合并同凭证分录***/
            combinedetail: this.state.ckNotShowZeroOccurZeroBalance, //本期无发生无余额不显示true
        }
        
        let url = '/nccloud/gl/accountrep/checkparam.do'
        let flagShowOrHide;
        ajax({
            url,
            data,
            async: false,
            success: function (response) {
                flagShowOrHide = true
            },
            error: function (error) {
                flagShowOrHide = false
                toast({ content: error.message, color: 'warning' });
            }
        })
        if (flagShowOrHide == true) {
            this.props.onConfirm(data)
        } else if (flagShowOrHide == false) {
            return true;
        }
	}
	render() {
//('timerddd::',this.state, this.state.tableSourceData,this.state.level,  this.state.begindate,'..', this.state.enddate,'>>>>', this.state.rangeDate)
        let { search } = this.props;
        let { NCCreateSearch } = search;
        return <div>
            {
                NCCreateSearch(this.searchId, {
                    onlyShowSuperBtn:true,                       // 只显示高级按钮
                    replaceSuperBtn:this.state.json['20023050-000009'],                      // 自定义替换高级按钮名称/* 国际化处理： 查询*/
                    replaceRightBody:this.renderModalList,
                    hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
                    clickPlanEve:this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                    saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
                    oid:this.props.meta.oid,
                    clickSearchBtn: this.clickSearchBtn,  // 点击按钮事件
                    isSynInitAdvSearch: true,//渲染右边面板自定义区域
                })}
        </div>
	}
}
CentralConstructorModal = createPage({})(CentralConstructorModal)

export default CentralConstructorModal;
