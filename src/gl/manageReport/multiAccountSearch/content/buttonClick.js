import { ajax, base, toast } from 'nc-lightapp-front';
import { HotObservable } from 'rxjs/testing/HotObservable';

export default function(page,id) {
  switch (id) {
    
    case 'query':
      page.state.condition =  {
          "analymode":"1",
          "assvo":[
              {"pk_checktype":"0001Z0100000000005CS","pk_checkvalue":""},
              {"pk_checktype":"1001Z01000000000HQW9","pk_checkvalue":""}
          ],
          "combinedetail":"N",
          "endperiod":"05",
          "endyear":"2018",
          "includeuntally":"Y",
          "pk_accountingbook":["1001A3100000000000PE"],
          "pk_currtype":this.state.json['20023050-000032'],/* 国际化处理： 所有币种*/
          "pk_multicol":"1001Z31000000000KBHK",
          "returncurr":"1",
          "showcolwithoutdata":"Y",
          "sorttype":"0",
          "startperiod":"04",
          "startyear":"2018",
      }
      ajax({
        url: '/nccloud/gl/accountrep/multibookpage.do',
        data: page.state.condition,
        success: (res) => {
          if (res.data) {
            page.state.pageinfo = res.data;
            delete page.state.condition.assvo,
            page.state.condition.assvo = [];
            page.state.pageindex = [];
            for(let i=0;i<page.state.pageinfo.length;i++){
              page.state.pageindex.push(0);
            }
            for(let i=0;i<page.state.pageinfo.length;i++){
              page.state.condition.assvo.push(page.state.pageinfo[i][page.state.pageindex[i]]);
            }
            query(page)
          }
        }
      }); 
    break;
    case 'first':
      delete page.state.condition.assvo,
      page.state.condition.assvo = [];
      for(let i=0;i<page.state.pageinfo.length;i++){
          page.state.pageindex[i] = 0;
      }
      for(let i=0;i<page.state.pageinfo.length;i++){
        page.state.condition.assvo.push(page.state.pageinfo[i][page.state.pageindex[i]]);
      }
      query(page)
      break;
    case 'previous':
      delete page.state.condition.assvo,
      page.state.condition.assvo = [];
      for(let i=0;i<page.state.pageinfo.length;i++){
          if(page.state.pageindex[i]>0){
            page.state.pageindex[i] = page.state.pageindex[i]-1;
            break;
          }else{
            for(let j=0;i<i;j++){
              page.state.pageindex[j] = page.state.pageinfo[j].length-1;
            }
            page.state.pageindex[i] = 0;
          }
      }
      for(let i=0;i<page.state.pageinfo.length;i++){
        page.state.condition.assvo.push(page.state.pageinfo[i][page.state.pageindex[i]]);
      }

    query(page)
      break;
    case 'next':
      delete page.state.condition.assvo,
      page.state.condition.assvo = [];
      for(let i=0;i<page.state.pageinfo.length;i++){
          if(page.state.pageinfo[i].length > page.state.pageindex[i]+1){
            page.state.pageindex[i] = page.state.pageindex[i]+1;
            break;
          }else{
            for(let j=0;j<i;j++){
              page.state.pageindex[j]=0;
            }
            page.state.pageindex[i] = page.state.pageindex[i]+1;
            break;
          }
      }
      for(let i=0;i<page.state.pageinfo.length;i++){
        page.state.condition.assvo.push(page.state.pageinfo[i][page.state.pageindex[i]]);
      }
      query(page)
      break;
    case 'last':
      delete page.state.condition.assvo,
      page.state.condition.assvo = [];
      for(let i=0;i<page.state.pageinfo.length;i++){
        page.state.pageindex[i] = page.state.pageinfo[i].length-1;
      }
      for(let i=0;i<page.state.pageinfo.length;i++){
        page.state.condition.assvo.push(page.state.pageinfo[i][page.state.pageindex[i]]);
      }
      query(page)
      break;
  }
}

function query(page){
  ajax({
    url: '/nccloud/gl/accountrep/multibookquery.do',
    data: page.state.condition,
    success: (res) => { 
      if (res.data) {
      let rows = [];   //表格显示的数据合集
      let columHead = []; //第一层表头
      let parent = []; //存放一级表头，二级表头需要补上
      let parent1 = []; //存放二级表头，三级表头需要补上
      let child1 = []; //第三层表头
      let child2 = []; //第二层表头
      let colKeys = [];  //列key值，用来匹配表体数据
      let colAligns=[];  //每列对齐方式
      let colWidths=[];  //每列宽度
      let oldWidths=[];
      res.data.column.forEach(function (value) {
          if(value.children){
            if(parent.length>0){
              child1.push(...parent);
            }
            for(let i=0;i<value.children.length;i++){
              if(value.children[i].children){
                child2.push(...parent1);
                for(let k=0;k<value.children[i].children.length;k++){
                  columHead.push(value.title); 
                  child1.push(value.children[i].title);
                  child2.push(value.children[i].children[k].title);
                  colKeys.push(value.children[i].children[k].key);
                  colAligns.push(value.children[i].children[k].align);
                  colWidths.push(value.children[i].children[k].width);
                }
                parent1 = [];
              }else {
                columHead.push(value.title); 
                child1.push(value.children[i].title);
                parent1.push(value.children[i].title);
                colKeys.push(value.children[i].key);
                colAligns.push(value.children[i].align);
                colWidths.push(value.children[i].width);
              }
            }
          parent = [];
          }else{
          columHead.push(value.title);
          parent1.push(value.title);
          parent.push(value.title);
          colKeys.push(value.key);
          colAligns.push(value.align);
          colWidths.push(value.width);
          }
      });
      let columheadrow = 0; //表头开始行
      if (res.data.headtitle){
        columheadrow = 1;
        let headtitle = [];
        res.data.headtitle.forEach(function (value) {
          headtitle.push(value[0]);
          headtitle.push(value[1]);
        });
        for(let i=headtitle.length;i<columHead.length;i++){
          headtitle.push('');
        }
        rows.push(headtitle);
      }

      let mergeCells = [];
      let row,col,rowspan,colspan;
      let headcount = 1; //表头层数
      if(child1.length>0){
        headcount++;
      }
      if(child2.length>0){
        headcount++;
      }
      let currentCol = 0; 
      for(let i=0;i<res.data.column.length;i++) {
        let value = res.data.column[i];
        if(value.children){
          let headCol = currentCol;
          for(let i=0;i<value.children.length;i++){
              if(value.children[i].children){
                let childlen = value.children[i].children.length;
                mergeCells.push({row:columheadrow+1,col:currentCol,rowspan:1,colspan:childlen});
                currentCol = currentCol+childlen;
              }else {
              mergeCells.push({row:columheadrow+1,col:currentCol,rowspan:headcount-1,colspan:1});
              currentCol++;
              }
          }
          mergeCells.push({row:columheadrow,col:headCol,rowspan:1,colspan:currentCol-headCol});
        }else{
          mergeCells.push({row:columheadrow,col:currentCol,rowspan:headcount,colspan:1});
          currentCol++;
        } 
      }

      if(parent.length>0){
        child1.push(...parent);
      }
      if(child2.length>0&&parent1.length>0){
        child2.push(...parent1);
      }

      rows.push(columHead);
      if(child1.length>0){
        rows.push(child1);
      }
      if(child2.length>0){
        rows.push(child2);
      }

      if(res.data!=null && res.data.data.length>0){
        let rowdata = [];
        for(let i=0;i<res.data.data.length;i++){
          for(let j=0;j<colKeys.length;j++){
            rowdata.push(res.data.data[i][colKeys[j]])
          }
          rows.push(rowdata);
          rowdata = [];
        }
      }
      let rowhighs = [];
      for (let i=0;i<rows.length;i++){
        rowhighs.push(23);
      }
      page.state.settings.data= rows;
      page.state.settings.colWidths=colWidths;

      page.state.settings.fixedRowsTop = 3; //冻结
      let quatyIndex = [];
      for (let i=0;i<colKeys.length;i++){
        if(colKeys[i].indexOf('quantity')>0){
          quatyIndex.push(i);
        }
      }

      if(mergeCells.length>0){
        page.state.settings.mergeCells= mergeCells;
      }

      page.state.settings.cells=function(row, col, prop) {  
        let cellProperties = {};  
      return cellProperties;  
    }
      page.setState({settings:page.state.settings});
    }
  }
})
}
