/**
 * Created by liqiankun on 2018/7/09.
 * 账簿查询 ---> 多栏账查询
 */

import React, {Component} from 'react';

import { base, ajax, deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
const { NCLoading:Loading, NCDiv } = base;
import '../../css/index.less'
import HeadCom from '../../common/headSearch/headCom';
import { SimpleTable } from 'nc-report';
import QueryModal from './components/MainSelectModal';
import {tableDefaultData} from '../../defaultTableData';
import reportPrint from '../../../public/components/reportPrint';
import { toast } from '../../../public/components/utils';
import {relevanceSearch} from "../../referUrl";
import {getDetailPort} from "../../common/modules/modules";
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import "../../../public/reportcss/firstpage.less";
import {rowBackgroundColor} from "../../common/htRowBackground";
import {setData} from "../../common/simbleTableData";
import {searchById} from "../../common/modules/createBtn";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import PageButtonGroup from '../../common/modules/pageButtonGroup'
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
class AssistPropertyBalance extends Component{
    constructor(props){
        super(props);

        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            disabled: true,
            visible: false,
            appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            ctemplate: '', //模板输出-模板
            nodekey: '',
            printParams:{}, //查询框参数，打印使用
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
            textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            dataWithPage: [], //存放请求回来的数据信息，用于分页操作
            firstPageDisable: true, //true: 禁止
            lastPageDisable: true,  //true: 禁止
            paramObj: {},//存放回调的查询参数，用于翻页
            pageArr: [], //存放返回的页数信息
            totalPage: 0, //存放总页面数

            //true：需要分页查询
            queryPageData: false, //选择'多栏账名称'的内容后返回的内容中的stat中的数据中有一个dsplocation中的value为'Y'时需要分页请求，否则直接请求数据
            accountType: 'amountcolumn',//金额式
            'multiAccountSearch': [],
            showRotate: false // 是否显示loadinig(查询时显示loading，直到查到数据或页码到达边界)
        }
        this.switchCount = 0 ;// 转换列名下标计数
        this.dataPage = 0; //存放请求回来的数据椰树信息
        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.handlePage = this.handlePage.bind(this);//页面处理事件
        // this.setData= this.setData.bind(this);// 整理表格所需要的数据格式
        this.queryData = this.queryData.bind(this);//请求数据的方法；
        // this.changeSelectStyle = this.changeSelectStyle.bind(this);//选择 '账簿格式'事件

        this.getPageInfo = this.getPageInfo.bind(this);//多栏账查询请求数据前先请求分页数据
        this.handlePageData = this.handlePageData.bind(this);

        this.changeQueryPageData = this.changeQueryPageData.bind(this);
        this.handleValueChange = handleValueChange.bind(this);
        this.searchById = searchById.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
    }

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                'multiAccountSearch': [
                    json['20023050-000035'],
                    json['20023050-000036'],
                    json['20023050-000037'],
                    json['20023050-000038']
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023050', 'publiccommon', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        this.setState({
            flag: true,
        });
        let appceod = this.props.getSearchParam('c');
        this.searchById('2002305010PAGE',appceod);
        this.props.button.setDisabled({
            print:true, linkvoucher: true,
            templateOutput:true, directprint:true,
            saveformat:true, switch:true, first:true,
            pre: true, next:true, last:true
        })
        this.props.button.setButtonVisible(['first', 'pre', 'next', 'last'], false)
    }

    showPrintModal() {
        this.setState({
            visible: true
        })
    }
    consAsspage(printParams ,totalPage, pageArr){
        let asspage = []
        if(pageArr.length>0 && totalPage>0){
            let num = this.getPages(totalPage, pageArr)
            // let num = this.getAllPage(pageArr)

            asspage = num                       
        }else{
            let tempArr = []
            let tempage = deepClone(printParams.assvo)
            tempArr.push(tempage)
            asspage = tempArr
            asspage.map((page)=>{
                page.map((item)=>{
                    delete item.includeSub
                })
            })
        } 
        return asspage
    }
    // 打印框 确认
    handlePrint(data, isPreview) {
        let printUrl = '/nccloud/gl/accountrep/multibookprint.do'
        let { printParams, appcode, totalPage, pageArr } = this.state
        let { ctemplate, nodekey } = data
        printParams.currpage = this.dataPage.toString() //当前页
        printParams.queryvo = data
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
        let asspage = this.consAsspage(printParams ,totalPage, pageArr)
        printParams.asspage = asspage
        printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
        this.handleCancel()
    }
    handleCancel() {
        this.setState({
            visible: false
        });
    }
    showOutputModal() {
        // this.refs.printOutput.open()
        let outputUrl = '/nccloud/gl/accountrep/multibookoutput.do'
        let { appcode, nodekey, ctemplate, printParams,totalPage,pageArr } = this.state
        let asspage = this.consAsspage(printParams ,totalPage, pageArr)
        printParams.asspage = asspage 
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)
    }

//pageArr = [
// [{pk_checktype: "", pk_checkvalue: ""}],
// [{pk_checktype: "", pk_checkvalue: ""}],
// [{pk_checktype: "", pk_checkvalue: ""}]
// ]
    getAllPage(pageArr){
        let arr=[]
        for(let a=0 ;a<pageArr[0].length;a++){
            if(pageArr[1] && pageArr[1].length>0){
                for(let b=0 ;b<pageArr[1].length;b++){
                    arr.push([pageArr[0][a],pageArr[1][b]])
                }
            }
        }
        return arr
    }
    
    getPages(totalPage,pageArr){
        let asspage = []
        for(let pageIndex=0;pageIndex<totalPage;pageIndex++){
            let pageindexcurr = pageIndex
            let rst = [];
            for (let i=pageArr.length-1;i>=0;i--) {
                if (pageArr[i].length > 0) {
                    rst[i]=(pageindexcurr % pageArr[i].length );
                    pageindexcurr= Math.floor(pageindexcurr / pageArr[i].length) ;
                }
            }
            let pageEvery = []
            rst.map((item, index) => {
                pageEvery.push(pageArr[index][item])
            })
            asspage.push(pageEvery)
        }
        return asspage
    }

    changeQueryPageData(value, param){
        this.setState({
            [param]: value
        })
    }

    /**
     * paramPage:当前页
     * pageInfo: [ //分页信息
     *      [
     *          {pk_checktype: '', pk_checkvalue: ''},
     *          {pk_checktype: '', pk_checkvalue: ''},
     *          {pk_checktype: '', pk_checkvalue: ''},
     *          {pk_checktype: '', pk_checkvalue: ''}
     *      ],
     *      [
     *          {pk_checktype: '', pk_checkvalue: ''},
     *          {pk_checktype: '', pk_checkvalue: ''},
     *      ]
     * ]
     * **/
    handlePageData(paramPage, pageInfo){
        let rst = [];
        let pageindexcurr = paramPage;//3
        let searchParam = this.state.paramObj;//查询的总参数
        for (let i=pageInfo.length-1;i>=0;i--) {

            if (pageInfo[i].length > 0) {
                rst[i]=(pageindexcurr % pageInfo[i].length );
                pageindexcurr= Math.floor(pageindexcurr / pageInfo[i].length) ;
            }

        }
        //rst:[0, 1][0, 2][0, 3]

        searchParam.assvo.length>0 && rst.map((item, index) => {

            searchParam.assvo[index].pk_checkvalue = pageInfo[index][item].pk_checkvalue
        })

        this.setState({
            paramObj: {...searchParam}
        })
    }
    handlePage(param, pageInfo){//页面控制事件

        let {paramObj} = this.state;//查询参数
        let dataLength = Number(pageInfo.totalpage);
        let renderData = [];
        let obj = {};
        let stringPage = '1';
        switch(param){
            case 'firstPage':
                this.dataPage = 0;
                this.handlePageData(this.dataPage, pageInfo);
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;

                this.setState({
                    paramObj,
                    firstPageDisable: true,
                    lastPageDisable: false,
                    selectRowIndex: 0
                });
                break;
            case 'nextPage':
                if (this.dataPage >= this.state.totalPage - 1) {
                    return;
                }
                this.dataPage += 1;
                this.handlePageData(this.dataPage, pageInfo)
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;

                this.setState({
                    paramObj,
                    firstPageDisable: false,
                    selectRowIndex: 0
                })
                if(this.dataPage >= this.state.totalPage-1){//当点击当叶数超过总页数时禁止"下一页"和"末页"
                    this.setState({
                        lastPageDisable: true
                    })
                }
                break;
            case 'prePage':
                if (this.dataPage <= 0) {
                    return;
                }
                this.dataPage -=1;
                this.handlePageData(this.dataPage, pageInfo)
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                if(this.dataPage === this.state.totalPage-1){
                    this.setState({
                        lastPageDisable: false
                    })
                }
                if(this.dataPage === 0){
                    this.setState({
                        firstPageDisable: true
                    })
                }
                this.setState({
                    paramObj,
                    selectRowIndex: 0
                })

                break;
            case 'lastPage':
                this.dataPage = this.state.totalPage - 1;
                this.handlePageData(this.dataPage, pageInfo)
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                this.setState({
                    paramObj,
                    lastPageDisable: true,
                    firstPageDisable: false,
                    selectRowIndex: 0
                });
                break;
        }
        this.queryData(this.state.paramObj, param);
    }
    handleSwitchColumn() {// 转换事件
        this.switchCount = ( this.switchCount + 1 ) % 3;
        let data = this.state.dataWithPage;
        let renderFirstData = {};
        renderFirstData.column = data.column;
        renderFirstData.data = data.data;
        renderFirstData.amountcolumn = data.amountcolumn;
        renderFirstData.quantityamountcolumn = data.quantityamountcolumn;
        setData(this, renderFirstData);
        return;
    }
    queryClick(){//点击"查询"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    modalSure(param){//点击弹框"确定"事件
        // this.setState({
        //     paramObj: {...param},
        //     printParams: {...param}
        // })
        this.dataPage = 0;
        this.state.paramObj = param
        this.state.printParams = param
        this.setState({
            firstPageDisable: true,
            lastPageDisable: true
        })
        if(this.state.queryPageData){//为true的话请求分页；false：直接请求数据
            this.getPageInfo(param);
        }else {
            this.pureQueryData(param);
        }
    }
    getPageInfo(param){//多栏账查询请求数据前先请求分页数据
        let self = this;
        let url = '/nccloud/gl/accountrep/multibookpage.do';
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, success } = response;
                if(data){
                    self.setState({
                        pageArr: [...response.data]
                    })
                }else{
                    self.setState({
                        lastPageDisable: true,
                        firstPageDisable: true,
                        showModal: false

                    })
                }

                self.queryData(self.state.paramObj);

            },
            error:function(error){
                toast({content: error.message, color: 'danger'});
                self.setState({
                    showModal: false
                });
                throw error
            }
        })
    }
    pureQueryData(param){//不分页请求数据方法；
        let self = this;
        //gl.accountrep.assanalysisquery
        let url = '/nccloud/gl/accountrep/multibookquery.do';
        ajax({
            url:url,
            data:param,
            success:function(response){
                self.dataPage = 0;//TODO:check
                let { data, success,error } = response;
                if(success && data){
                    self.props.button.setDisabled({
                        print:false,
                        templateOutput:false, directprint:false,
                        saveformat:false, switch:false
                    })
                    self.setState({
                        disabled:false,
                        dataWithPage: data,
                        headtitle: data.headtitle,
                        typeDisabled: false,//账簿格式 默认是禁用的
                        selectRowIndex: 0
                    })
                    if(data === null){
                        data = {
                            data: []
                        }
                    }
                    let renderFirstData = {};
                    let allData  = data
                    renderFirstData.column = data.column;
                    renderFirstData.data = data.data;
                    renderFirstData.amountcolumn = data.amountcolumn;
                    renderFirstData.quantityamountcolumn = data.quantityamountcolumn;
                    setData(self, renderFirstData);
                }else{
                    toast({content: error.message, color: 'danger'});
                }
            },
            error:function(error){
                toast({content: error.message, color: 'danger'});
                throw error
            }
        })
    }
    queryData(param, pagetype){//请求数据方法；

        this.handlePageData(this.dataPage, this.state.pageArr);
        let pageInfo = this.state.pageArr
        let pageArr = [];

        let totalPage = 1;
        for(let item of pageInfo){
            pageArr.push(item.length);
        };

        pageArr.length > 0 && pageArr.map((item,index) => {
            totalPage *= item
        })
        this.setState({
            totalPage: totalPage,
            showRotate: true
        })

        let pageinfo = this.state.pageArr;
        let self = this;
        //gl.accountrep.multibookformatquery
        let url = '/nccloud/gl/accountrep/multibookquery.do';
        ajax({
            loading: false,
            url:url,
            data:this.state.paramObj,
            success:function(response){
                let { data, success } = response;
                if(success){
                    if(data&&data.data.length>0){
                        self.setState({
                            disabled:false,
                        })
                        self.props.button.setDisabled({
                            print:false,
                            templateOutput:false, directprint:false,
                            saveformat:false, switch:false
                        })
                    }
                    if(self.state.totalPage  > 1 && self.dataPage < self.state.totalPage - 1){
                        self.setState({
                            lastPageDisable: false
                        })
                    }

                    // 跳过空白页（若当前data.data为空，则在页码范围内继续翻页）
                    if (!pagetype 
                        || (pagetype == 'prePage' && self.dataPage > 0)
                        || (pagetype == 'nextPage' && self.dataPage < self.state.totalPage - 1)
                        || pagetype == 'firstPage'
                        || pagetype == 'lastPage') {
                        if (data.data == null || data.data.length == 0) {
                            if (pagetype == 'firstPage') {
                                pagetype = 'nextPage';
                            }
                            if (pagetype == 'lastPage') {
                                prePage = 'prePage';
                            }
                            if (!pagetype) {
                                pagetype = 'firstPage';
                            }
                            self.handlePage(pagetype, self.state.pageArr);
                            return;
                        }
                    }
                    
                    self.setState({
                        dataWithPage: data,
                        headtitle: data.headtitle,
                        typeDisabled: false,
                        selectRowIndex:0,
                        showRotate: false
                    })
                    if (data.data === null) {
                        data.data = [];
                    }
                    let renderFirstData = {};
                    renderFirstData.column = data.column;
                    renderFirstData.data = data.data;
                    renderFirstData.amountcolumn = data.amountcolumn;
                    renderFirstData.quantityamountcolumn = data.quantityamountcolumn;
                    setData(self, renderFirstData);
                }else{
                    toast({content: this.state.json['20023050-000000'], color: 'danger'});/* 国际化处理： 请求数据为空！*/
                }
            },
            error:function(error){
                toast({content: error.message, color: 'danger'});
                throw error
            }
        })
    }
    setData = (data) => {//整理表格数据
        let rows = [];   //表格显示的数据合集
        let columHead = []; //第一层表头
        let parent = []; //存放一级表头，二级表头需要补上
        let parent1 = []; //存放二级表头，三级表头需要补上
        let child1 = []; //第三层表头
        let child2 = []; //第二层表头
        let colKeys = [];  //列key值，用来匹配表体数据
        let colAligns=[];  //每列对齐方式
        let colWidths=[];  //每列宽度
        let oldWidths=[];
        let textAlginArr = this.state.textAlginArr.concat([]);      //对其方式
        data[this.state.accountType].forEach((value) => {//column:[]原始数据表头信息
            let valueCell = {};
            valueCell.title = value.title;
            valueCell.key = value.key;
            valueCell.align = 'center';
            valueCell.bodyAlign = 'left';
            valueCell.style = 'head';
            if(value.children){
                this.setState({
                    flow: true
                })
                if(parent.length>0){
                    child1.push(...parent);
                }
                for(let i=0;i<value.children.length;i++){
                    if(value.children[i].children){
                        child2.push(...parent1);
                        for(let k=0;k<value.children[i].children.length;k++){
                            let child1Cell = {};
                            child1Cell.title = value.children[i].title;
                            child1Cell.key = value.children[i].key;
                            child1Cell.align = 'center';
                            child1Cell.bodyAlign = 'left';
                            child1Cell.style = 'head';
                            let child2Cell = {};
                            child2Cell.title = value.children[i].children[k].title;
                            child2Cell.key = value.children[i].children[k].key;
                            child2Cell.align = 'center';
                            child2Cell.bodyAlign = 'left';
                            child2Cell.style = 'head';
                            columHead.push(valueCell);
                            child1.push(child1Cell);
                            child2.push(child2Cell);
                            colKeys.push(value.children[i].children[k].key);
                            colAligns.push(value.children[i].children[k].align);
                            colWidths.push({'colname':value.children[i].children[k].key,'colwidth':value.children[i].children[k].width});
                        }
                        parent1 = [];
                    }else {
                        let innerCell2 = {};
                        innerCell2.title = value.children[i].title;
                        innerCell2.key = value.children[i].key;
                        innerCell2.align = 'center';
                        innerCell2.bodyAlign = 'left';
                        innerCell2.style = 'head';
                        columHead.push(valueCell);
                        child1.push(innerCell2);
                        parent1.push(innerCell2);
                        colKeys.push(value.children[i].key);
                        colAligns.push(value.children[i].align);
                        colWidths.push({'colname':value.children[i].key,'colwidth':value.children[i].width});
                    }
                }
                parent = [];
            }else{
                let cellObj = {};
                cellObj.title = value.title;
                cellObj.key = value.key;
                cellObj.align = 'center';
                cellObj.bodyAlign = 'left';
                cellObj.style = 'head';
                columHead.push(valueCell);
                parent1.push(cellObj);
                parent.push(cellObj);
                colKeys.push(value.key);
                colAligns.push(value.align);
                colWidths.push({'colname':value.key,'colwidth':value.width});
            }
        });
        let columheadrow = 0; //表头开始行
        // if (data.headtitle){
        //    columheadrow = 1;
        //    let headtitle = [];
        //    data.headtitle.forEach(function (value) {
        //     headtitle.push(value[0]);
        //     headtitle.push(value[1]);
        //    });
        //    for(let i=headtitle.length;i<columHead.length;i++){
        //     headtitle.push('');
        //    }
        //    rows.push(headtitle);
        // }

        let mergeCells = [];
        let row,col,rowspan,colspan;
        let headcount = 1; //表头层数
        if(child1.length>0){
            headcount++;
        }
        if(child2.length>0){
            headcount++;
        }
        let currentCol = 0;
        //计算表头合并格
        for(let i=0;i<data[this.state.accountType].length;i++) {
            let value = data[this.state.accountType][i];
            //*********
            if(value.children){//有子元素的
                let childLeng = value.children.length;
                mergeCells.push([columheadrow, currentCol, columheadrow, currentCol + childLeng -1 ]);

                let headCol = currentCol;
                for(let i=0;i<value.children.length;i++){

                    let childlen = 0;
                    if(value.children[i].children){

                        let childlen = value.children[i].children.length;
                        currentCol = currentCol+childlen;
                        for(let k=0;k<value.children[i].children.length;k++){
                            textAlginArr.push(value.children[i].children[k].align);
                        }
                    }else if(childlen > 0){//子元素里面没有子元素的

                        mergeCells.push([columheadrow+1, currentCol, columheadrow+2, currentCol]);//cell
                        currentCol++;
                    }else {
                        currentCol++;
                        textAlginArr.push(value.children[i].align);
                    }
                }
            }else{//没有子元素对象
                mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
                currentCol++;
                textAlginArr.push(value.align);
            }
        }
        if(parent.length>0){
            child1.push(...parent);
        }
        if(child2.length>0&&parent1.length>0){
            child2.push(...parent1);
        }

        rows.push(columHead);

        if(child1.length>0 && this.state.flow){
            rows.push(child1);
        }
        if(child2.length>0 && this.state.flow){
            rows.push(child2);
        }
        if(data!=null && data.data.length>0){

            let rowdata = [];
            let flag = 0;
            for(let i=0;i<data.data.length;i++){
                flag++;
                for(let j=0;j<colKeys.length;j++){
                    // {
                    //     "title":"11163",
                    //     "key":"pk_org",
                    //     "align":"left",
                    //     "style":"body"
                    // }
                    let itemObj = {
                        align: 'right',
                        style: 'body'
                    };
                    itemObj.title = data.data[i][colKeys[j]];
                    itemObj.key = colKeys[j] + flag;
                    itemObj.link = data.data[i].link
                    // rowdata.push(data.data[0].pagedata[i][colKeys[j]])
                    rowdata.push(itemObj)
                }
                rows.push(rowdata);
                rowdata = [];
            }
        }
        let rowhighs = [];
        for (let i=0;i<rows.length;i++){
            rowhighs.push(23);
        }
        this.state.dataout.data.cells= rows;//存放表体数据
        this.state.dataout.data.widths=colWidths;
        oldWidths.push(...colWidths);
        this.state.dataout.data.oldWidths = oldWidths;
        this.state.dataout.data.rowHeights=rowhighs;
        let frozenCol;
        for(let i=0;i<colAligns.length;i++){
            if(colAligns[i]=='center'){
                frozenCol = i;
                break;
            }
        }
        this.state.dataout.data.fixedColumnsLeft = 0; //冻结
        this.state.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结
        let quatyIndex = [];
        for (let i=0;i<colKeys.length;i++){
            if(colKeys[i].indexOf('quantity')>0){
                quatyIndex.push(i);
            }
        }


        if(mergeCells.length>0){
            this.state.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
        }
        let headAligns=[];

        for(let i=0;i<headcount+columheadrow;i++){
            for(let j=0;j<columHead.length;j++){
                headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
            }
        }
        this.state.dataout.data.cell= headAligns,
            this.state.dataout.data.cellsFn=function(row, col, prop) {
                let cellProperties = {};
                if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
                    cellProperties.align = colAligns[col];
                    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
                }
                return cellProperties;
            }
        this.setState({
            dataout:this.state.dataout,
            showModal: false,
            textAlginArr
        });
    }
    //直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    handleSaveColwidth=()=>{
        let {json} = this.state;
        let info=this.refs.balanceTable.getReportInfo();
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.paramObj.pk_multicol,info.colWidths, json, callBack);
    }
    handleLoginBtn = (obj, btnName) => {
        if(btnName === 'print'){//1、打印
            this.showPrintModal()
        }else if(btnName === 'linkvoucher'){//4、联查凭证
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: relevanceSearch,
                key: 'relevanceSearch',
                appcode: '20023030'
            }, this.state.json)
        }else if(btnName === 'templateOutput'){//3、模板输出
            this.showOutputModal()
        }else if(btnName === 'directprint'){//2、直接输出
            this.printExcel()
        }else if(btnName === 'saveformat'){//5、保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'switch'){//6、转换
            this.handleSwitchColumn()
        }else if(btnName === 'first'){//首页
            this.handlePage('firstPage', this.state.pageArr)
        }else if(btnName === 'pre'){//上一页
            this.handlePage('prePage', this.state.pageArr)
        }else if(btnName === 'next'){//下一页
            this.handlePage('nextPage', this.state.pageArr)
        }else if(btnName === 'last'){//末页
            this.handlePage('lastPage', this.state.pageArr)
        }else if( btnName === 'refresh'){
            if (Object.keys(this.state.paramObj).length > 0) {
                this.modalSure(this.state.paramObj);
            }
        }
    }
    firstCallback = () => {//首页
        this.handlePage('firstPage', this.state.pageArr)
    }
    preCallBack = () => {//上一页
        this.handlePage('prePage', this.state.pageArr)
    }
    nextCallBack = () => {//下一页
        this.handlePage('nextPage', this.state.pageArr)
    }
    lastCallBack = () => {//末页
        this.handlePage('lastPage', this.state.pageArr)
    }
    render(){
        let { modal } = this.props;
        const { createModal } = modal;
        return(
            <div className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['20023050-000033']} /* 国际化处理： 多栏账查询*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <QueryModal
                                    changeQueryPageData={this.changeQueryPageData}
                                    onConfirm={(datas) => {
                                        this.modalSure(datas);
                                    }}
                                />
                            </div>
                            {//按钮集合
                                this.props.button.createButtonApp({
                                    area: 'btnarea',
                                    buttonLimit: 3,
                                    onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                                })
                            }
                        </div>
                    }
                    pageBtnContent = {
                        <PageButtonGroup
                            first={{disabled: this.state.firstPageDisable, callBack: this.firstCallback}}
                            pre={{disabled: this.state.firstPageDisable, callBack: this.preCallBack}}
                            next={{disabled: this.state.lastPageDisable, callBack: this.nextCallBack}}
                            last={{disabled: this.state.lastPageDisable, callBack: this.lastCallBack}}
                        />
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.multiAccountSearch.map((items) => {
                                return(
                                    <HeadCom
                                        labels={items}
                                        key={items}
                                        content={this.state.dataWithPage.headtitle}
                                        lastThre = {this.state.headtitle && this.state.headtitle}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
                                        }
                                        accountType = {this.state.accountType}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data = {this.state.dataout}
                        onCellMouseDown = {(e, coords, td) => {
                            this.whetherDetail('link','multiAccountSearch');
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
                </div>

                <RepPrintModal
                    noCheckBox={true}
                    showNum={true}
                    showScopeall={true}
                    appcode={this.state.appcode}
                    visible={this.state.visible}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={this.handleCancel.bind(this)}
                />

                <Loading
                    fullScreen
                    showBackDrop={true}
                    show={this.state.showRotate} />
                {createModal('printService', {
                    className: 'print-service'
                })}
                <Iframe />
            </div>
        )
    }
}

AssistPropertyBalance = createPage({})(AssistPropertyBalance)

ReactDOM.render(<AssistPropertyBalance />,document.querySelector('#app'));
