import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
//import moment from 'moment';

import {high,base,ajax, deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import '../../../../css/searchModal/index.less';



const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input, NCTooltip: Tooltip,
	NCRangePicker: RangePicker
} = base;

import { toast } from '../../../../../public/components/utils';

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';
import {getRecover} from "../../../../common/modules/recoverData";
// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import AccPeriodDefaultTreeGridRef from '../../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';
import NewModal from '../modal'
import AccountDefaultModelTreeRef from '../../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../../public/components/uapRefer.js';
import {
    renderMoneyType,
    renderRefer,
    getSubjectVersion,
    renderLevelOptions,
    createReferFn, getCheckContent, businessUnit, returnMoneyType, getReferDetault
} from '../../../../common/modules/modules';

import {getAdjustTime} from '../../../../../public/hansonTableSetData/commonFn'
import {subjectRefer} from '../../../../referUrl'
import DateGroup from '../../../../common/modules/dateGroup'
import SubjectLevel from '../../../../common/modules/subjectAndLevel'
import {clearTransterData, handleChange, handleSelectChange} from "../../../../common/modules/transferFn";
import CheckBoxCells from '../../../../common/modules/checkBox';
import {handleValueChange} from '../../../../common/modules/handleValueChange'
import FourCheckBox from '../../../../common/modules/fourCheckBox'
import initTemplate from '../../../../modalFiles/initTemplate';
import {handleNumberInput} from "../../../../common/modules/numberInputFn";
import FormLabel from '../../../../../public/components/FormLabel';
import { FICheckbox } from '../../../../../public/components/base';
class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
		    json: {},
            dateInputPlaceholder: '', ///* 国际化处理： 选择日期*/
            numberRefValue: '',
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            selectedKeys: [],
            targetKeys: [],
            changeParam: true, //false:点击条件时不触发接口，,
            rightSelectItem: [], //科目的"条件"按钮中存放右侧数据的pk值

            showRadio: true,//控制是否显示"会计期间"的但选框；false: 不显示
            appcode: '20023081',
            level:0, //返回的最大级次
            isShowUnit:false,//是否显示业务单元
            busiDate: '', //业务日期

            pk_accperiodscheme: '',//会计期间参数
			showNewModal: false,
            renderData: [], //我的弹框里数据
			innerSelectName: [], //"查询范围"里存放的数据
            innerSelectData: [], //最里层选择数据的项
            rangefrom: '', //余额范围起始值
            rangeto: '', //余额范围结束值
            listItem:{},//业务单元中模板数据对应值
			selectedSort: 'true',//排序方式
			selectedShowType: 'false', //多主题显示方式
			doubleCell: false, // 多业务单元合并
			allLast: true, // 所有末级是否选中
			outProject: false, //表外科目是否选中
			multiProjectMerge: true, //多主题合并
			subjectShow: true, //按主题列示
			balanceMoneyLeft: '', //余额范围左边输入框
            balanceMoneyRight: '', //余额范围右边输入框
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accountingUnit: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: [],//业务单元
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表

			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向

			/***现金日计记账/多主题科目余额表：级次***/
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次

            /***现金日计记账/多主题科目余额表：科目***/
			startcode: {
				refname:'',
				refpk: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
			},    //结束科目编码

			versiondate: '', //制单日期，后续要用下拉选

			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			/***无发生不显示;本期无发生无余额不显示***/
			showzerooccur: false, //无发生不显示
            ckNotShowZeroOccurZeroBalance: false, //本期无发生无余额不显示
			/***多核算账簿显示方式***/
            defaultShowStyle: 'false',
            /***多核算账簿排序方式***/
            defaultSortStyle: 'false',
            cbCorpSubjDspBase: false,//按各核算账簿显示科目名称
            cbQryByCorpAss: false,//按各核算账簿查询辅助核算

			upLevelSubject: false, //显示上级科目
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: false, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			start: {},  //开始期间
			end: {}, //结束期间

			/*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
			startyear: '', //开始年
            startperiod: '',  //开始期间
			endyear: '',  //结束年
			endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示


            searchDateStyle: 'N',//现金日记账 按制单/签字查询
			isversiondate: false,

            indexArr: [], //存放"查询对象"已经选择的索引



			//tabel组件头部

			//table的数据
			tableSourceData: [],

			//第二层弹框里的table
            secondData: [],//第二层表格的数据

            selectRow: [],//存放所选择的不是参照类型的数据
            page: 0//第二个弹框中要显示的行内容索引
		}

		this.balanceMoney = this.balanceMoney.bind(this);//余额范围输入触发事件

        this.sureHandle = this.sureHandle.bind(this);
        this.closeNewModal = this.closeNewModal.bind(this);
        this.outInputChange = this.outInputChange.bind(this);



        this.renderTableFirstData = this.renderTableFirstData.bind(this);// table渲染初始的数据
        this.handleSearchObj = this.handleSearchObj.bind(this);//查询对象 选择触发事件
		this.findByKey = this.findByKey.bind(this);
		this.renderSearchRange = this.renderSearchRange.bind(this);//根据'查询对象'的选择来对应渲染'查询范围'的组件
		this.renderRefPathEle = this.renderRefPathEle.bind(this);//根据传进来的url渲染对应的参照组件
		this.handleCheckbox = this.handleCheckbox.bind(this);//计算小计	包含下级复选框的点击事件
		this.secondModalSelect = this.secondModalSelect.bind(this);//第二层弹框的选择框
		this.renderNewRefPath = this.renderNewRefPath.bind(this);//渲染第二层弹框内容的参照



		this.handleDateChange = this.handleDateChange.bind(this);//日期: 范围选择触发事件
		this.onChangeShowzerooccur = this.onChangeShowzerooccur.bind(this);//无发生不显示;本期无发生无余额不显示
		this.handleSortOrShowStyle = this.handleSortOrShowStyle.bind(this);//多核算账簿排序方式/多核算账簿显示方式
		this.handleDateRadio = this.handleDateRadio.bind(this);//会计期间/日期的单选框事件


		this.handleSelectChange = this.handleSelectChange.bind(this);//<Select>的onChange事件统一触发事件
		this.renderLevelOptions = this.renderLevelOptions.bind(this);//渲染级次的<NCOption>
		this.renderRefer = renderRefer.bind(this);
		this.handleValueChange = handleValueChange.bind(this);
		this.searchId = '20023081query';
        this.handleNumberInput = handleNumberInput.bind(this);
        this.getRecover = getRecover.bind(this);
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                dateInputPlaceholder: json['20023081-000041'], ///* 国际化处理： 选择日期*/
                currtype: json['20023081-000003'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['20023081-000003'],  //选择的币种名称/* 国际化处理： 本币*/
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023081', 'dategroup', 'checkbox', 'fourcheckbox', 'subjectandlevel', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
        this.renderTableFirstData(9);
    }

    componentDidMount() {
        setTimeout(() => getReferDetault(this, false, {
            businessUnit,
            getCheckContent
        }, 'cashAccount'),0);
        setTimeout(() => initTemplate.call(this,this.props), 0)
    }
    renderLevelOptions(num){
    	let optionArr = [];
    	for(let i=0; i<num; i++){
            optionArr.push(<NCOption value={i+1} key={i}>{i+1}</NCOption>);
		}
        return optionArr;
	}
    handleSelectChange(value, param){//<Select>的onChange事件统一触发事件
    	this.setState({
			[param]: value
		})
	}
    handleDateRadio(value, param){//会计期间/日期的单选框事件
    	this.setState({
			[param]: value
		})
	}
	handleSortOrShowStyle(value, param){//多核算账簿显示方式/多核算账簿排序方式触发事件
		this.setState({
			[param]: value
		})
	}
    handleDateChange(value){//日期: 范围选择触发事件
    	this.setState({
            begindate: value[0],
            enddate: value[1],
			rangeDate: value
		})
	}
    handleSearchObj(value1, record){//查询对象选择具体科目
        let value = JSON.parse(value1)
    	let { tableSourceData, selectRow } = this.state;
        let originData = this.findByKey(record.key, tableSourceData);
        originData.selectCell = value;
        record.selectCells = [];
        record.selectRange = [];
        if(value.refpath ){
            originData.type = 'referpath';
            originData.searchRange = value.refpath;
		}else if(value.datatype === '33'){
            originData.type = 'date'
		}else if(value.datatype === '31'){//数值
            originData.type = 'number';
        } else if(value.datatype === '1'){//字符
            originData.type = 'string';
        }   else if(value.attr){
            originData.selectCellAttr = [...value.attr];
            originData.type = 'modal'
		} else{
            originData.type = ''
		}
		let newSelectRow = [...selectRow];
        newSelectRow[record.key] = record;
    	this.setState({
            tableSourceData,
			selectRow: newSelectRow
		})
	}
    handleCheckbox(key, record){//计算小计	包含下级复选框的点击事件
    	let {tableSourceData} = this.state;
        record[key] = record[key] === 'N' ? 'Y' : 'N';
        this.setState({
            tableSourceData
		})

	}
    secondModalSelect(record, index){//第二层弹框的复选框
    	let {tableSourceData} = this.state;
        let newSelect = !record.select;
        record.select = newSelect;
        tableSourceData[record.itemKey].selectCellAttr[index] = record;
        this.setState({tableSourceData})
	}
    findByKey(key, rows) {
        let rt = null;
        let self = this;
        rows.forEach(function(v, i, a) {
            if (v.key == key) {
                rt = v;
            }
        });
        return rt;
    }
    dateRefChange = (key, value, record) => {
        let length = record.selectCell.inputlength;
        let exeReg = new RegExp("^.{0," + length + "}$");
        let result = exeReg.exec(value);
        if(result && key === 'stringRefValue'){
            record.selectRange = result[0]
            this.setState({
                [key]: result[0]
            })
        }else {
            record.selectRange = value
            this.setState({
                [key]: value
            })
        }
    }
    numberRefChange = (key, value, record, param) => {
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectRange = this.state[key], 0)

    }
    renderSearchRange(record, fieldid){//根据'查询对象'的选择来对应渲染'查询范围'的组件
    	switch(record.type){
			case "referpath":
				let refPathEle = this.renderRefPathEle(record, fieldid);
				return refPathEle;
            case 'date':
                return (
                    <DatePicker
                        fieldid={fieldid}
                        format={format}
                        value={this.state.assistDateValue}
                        placeholder={dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateValue', value, record)}
                    />
                );
            case 'string':
                return <Input
                    fieldid={fieldid}
                    value={this.state.stringRefValue}
                    onChange={(value) => this.dateRefChange('stringRefValue', value, record)}
                />;
            case 'number':
                return <Input
                    fieldid={fieldid}
                    value={this.state.numberRefValue}
                    onChange={(value) => this.numberRefChange(value, record) }
                />;
			case 'modal':
                let attrObj = record.selectCellAttr;
                let { tableSourceData } = this.state;
                let selectObj = [];
                attrObj.map((item)=>{
                	if(item){
                        item.itemKey = record.key;
					}
					if(item && item.selectName){
                        selectObj += item.selectName + ','
					}
				})
				return (
                    <div style={{display: 'flex'}}>
                        <Input
                            fieldid={fieldid}
                            style={{border: 'none'}}
							placeholder={record.selectCell && record.selectCell.name}
							value={selectObj}
						/>
                        <div className='ellipsisContainer'
                             onClick={() => this.closeNewModal(record)}
                        >
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                        </div>
                    </div>
				);
			default:
				return <Input fieldid={fieldid} disabled/>;
		}
	}
    renderNewRefPath(record){
        let mybook;

        if(record.refpath){
            let objKey = record.attrclassid;
            if(!this.state[objKey]){//undefined
                {createScript.call(this,record.refpath+".js",objKey)}

            }else {
                mybook = (
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state[objKey] ? (this.state[objKey])(
                                    {
                                        value:{refname: record.selectName && record.selectName[0], refpk:  record.refpk && record.refpk[0]},
                                        isMultiSelectedEnabled: true,
                                        queryCondition: () => {
                                            return {
                                                "pk_accountingbook": this.state.accountingbook.pk_accountingbook ? this.state.accountingbook.pk_accountingbook : '',
                                                // "versiondate":currrentDate,
                                            }
                                        },
                                        onChange: (v) => {
                                            let {tableSourceData} = this.state;
                                            let refpkArr = [];
                                            let selectName = [];
                                            v.forEach((item) => {
                                                refpkArr.push(item.refpk);
                                                selectName.push(item.refname)
                                            })
                                            let pk_checkvalue = refpkArr.join(',')
                                            record.pk_checkvalue = pk_checkvalue;
                                            record.selectName = selectName;
                                            record.refpk = refpkArr;
                                            this.setState({
                                                tableSourceData
                                            })
                                        }
                                    }
                                ) : <div/>
                            }
                        </Col>
                    </Row>
                );
            }
		}

        return mybook;
	}
	renderRefPathEle(record, fieldid){//根据传进来的url渲染对应的参照组件
        //带有参照的
		let mybook;
		let objKey = record.key + record.selectCell.pk_checktype;
		if(!this.state[objKey]){//undefined
			{createScript.call(this,record.selectCell.refpath+".js",objKey)}

		}else{
            let pkOrgParam = this.state.buSecond.length > 0 ? this.state.buSecond[0].refpk : this.state.unitValueParam;
            let options = {
                fieldid: fieldid,
                value: record.selectCells,
                isMultiSelectedEnabled:true,
                "isShowDisabledData": true,
                queryCondition:() => {
                    return {
                        "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                        "pk_org": pkOrgParam,
                        "isDataPowerEnable": "Y",
                        "DataPowerOperationCode" : 'fi'
                    }
                },
                onChange: (v)=>{
                    let {tableSourceData} = this.state;
                    let refpkArr = [];
                    let selectCell = [];
                    v.forEach((item)=>{
                        let cellObj = {};
                        cellObj.pk_checkvalue = item.refpk;
                        cellObj.checkvaluecode = item.refcode;
                        cellObj.checkvaluename = item.refname;
                        refpkArr.push(cellObj);
                        selectCell.push(item);
                    })
                    let pk_checkvalue = [...refpkArr];
                    record.selectRange = pk_checkvalue;
                    record.selectCells = selectCell;
                    this.setState({
                        tableSourceData
                    })
                }
            };
            let newOPtions = {}
            if(record.selectCell.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.selectCell.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                newOPtions = {
                    ...options,
                    "unitValueIsNeeded":false,
                    "isShowDimission":true,
                    queryCondition:() => {
                        return {
                            "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                            "pk_org": pkOrgParam,
                            "isDataPowerEnable": "Y",
                            "DataPowerOperationCode" : 'fi',
                            "busifuncode":"all",
                            isShowDimission:true
                        }
                    },
                    isShowUnit:true,
                    unitProps:{
                        refType: 'tree',
                        refName: this.state.json['20023081-000023'],/* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:this.state.json['20023081-000023'],refpk:'root'},/* 国际化处理： 业务单元*/
                        placeholder:this.state.json['20023081-000023'],/* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[this.state.json['20023081-000042'], this.state.json['20023081-000043']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false
                    },
                    unitCondition:{
                        pk_financeorg: pkOrgParam,
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    },
                }
            }else if(record.selectCell.classid && record.selectCell.classid.length === 20){
                newOPtions = {
                    ...options,
                    pk_defdoclist: record.selectCell.classid
                }
            } else{
                newOPtions = {
                    ...options
                }
            }
            mybook =  (
				<Row className="tableselectItem">
					<div>
						{
							this.state[objKey]?(this.state[objKey])(newOPtions):<div/>
						}
					</div>
				</Row>
			);
		}
		return mybook;
	}
    renderTableFirstData(length){
    	let firstData = [];
    	for(let i=0; i<length; i++){
    		let obj = {};
    		obj.searchObj = [];
            obj.searchRange = '';
            obj.showLocal = false;//显示位置
            obj.accele = false;//小计
            obj.includesub = 'N',//包含下级
			obj.key = i
            firstData.push(obj);
		}
		this.setState({
            tableSourceData: [...firstData]
		})
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}




	getCurrtypeList = () => {//获取"币种"接口
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {"localType":"1", "showAllCurr":"1"};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {

		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


    handleCurrtypeChange = (keyParam, value) => {
        this.setState({
            [keyParam]: value,
        });
    }


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(v) {

		if (!this.state.accountingbook || !this.state.accountingbook) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}
		
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	

		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        		versiondate: '2018-5-31', //data.versiondate,
		        	},)



                    getSubjectVersion(self);

                    let newUrl = '/nccloud/gl/accountrep/assbalancequeryobject.do';//现金日记账
                    let newData = {
                        "pk_accountingbook":v.refpk,
                        "versiondate": '2018-5-31',//self.state.versiondate,
                        "needaccount": false
                    };
                    ajax({
                        url:newUrl,
                        data:newData,
                        success: function(response){
                            const { success } = response;
                            //渲染已有账表数据遮罩
                            if (success) {
                                if(response.data){
                                    response.data.map((item) => {
                                    	if(item.attr){
                                            item.attr.map((cell) => {
                                                cell.select = false
											})
										}
									})

                                    let newTableFirstData = [...self.state.tableSourceData];
                                    newTableFirstData.forEach((item, index) => {
                                        item.searchObj = response.data;
									})

                                    self.setState({
                                        tableSourceData: newTableFirstData
                                    })
                                }
                            }
                        }
                    });
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: '2018-5-31'// value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化/本期无发生无余额不显示;按各核算账簿显示科目名称/按各核算账簿查询辅助核算
	onChangeShowzerooccur(value, param) {
		this.setState({
			[param]: value
		});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}
	//显示上级科目复选框变化
	onChangeupLevelSubject(e) {
		this.setState({upLevelSubject: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}
	handleRadioSort(param){
		this.setState({
			selectedSort: param
		})
	}
	handleShowType(param){
		this.setState({
			selectedShowType: param
		})
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
	onChangeDoubleCell(param){
		this.setState({
			doubleCell: !this.state.doubleCell
		})
	}
	changeProjectStyle(param){//所有末级   表外科目选择
		if(param === 'allLast'){
			this.setState({
				allLast: !this.state.allLast
			})
		}else{
			this.setState({
				outProject: !this.state.outProject
			})
		}
	}

	handleGTypeChange =() =>{
		
	};

    shouldComponentUpdate(nextProps, nextState){
		return true;
	}



    outInputChange(param){
    	let nameArr = [];
        param.forEach((item) => {
            nameArr.push(item.refname);
		})
		this.setState({
            innerSelectName: [...nameArr]
		})
	}
    sureHandle(param){

    	let SelectCell = [];
        this.setState({
			innerSelectData: [...SelectCell]
		})
    	this.closeNewModal(param);
	}
    closeNewModal(param){
		this.setState({
            showNewModal: !this.state.showNewModal,
			page: param.key
		})
    }
    balanceMoney(value, param){

        let arrResult = value.split('');//输入值变为数组
        let code = arrResult[arrResult.length - 1];//最后一次输入的元素
        let codeValue = code && code.toString().charCodeAt(0);
        if (codeValue < 48 || codeValue > 57) {//不是数字的输入给剪掉
            arrResult.splice(arrResult.length - 1);
        }
    	this.setState({
			[param]: arrResult.join('')//数组变为字符串
		})
	}

    handleSelect = (value) => {//

        this.setState({
            rightSelectItem: [...value]
        })
    }
    renderModalList = () => {
        this.columns = [
            {
                title: (<div fieldid='searchObj'>{this.state.json['20023081-000004']}</div>),/* 国际化处理： 核算类型*/
                dataIndex: "searchObj",
                key: "searchObj",
                render: (text, record, index) => {
                    let renderArr = [];
                    text.map((item, index) => {
                        renderArr.push(<NCOption value={JSON.stringify(item)} key={index}>{item.name}</NCOption>)
                    })
                    return (
                        <Select
                            showClear={false}
                            fieldid='searchObj'
                            value={record.selectCell && record.selectCell.name}
                            onChange = {(value) => this.handleSearchObj(value, record)}
                        >
                            {
                                renderArr
                            }
                        </Select>
                    )
                }
            },
            {
                title: (<div fieldid='searchRange'>{this.state.json['20023081-000005']}</div>),/* 国际化处理： 核算内容*/
                dataIndex: "searchRange",
                key: "searchRange",
                render: (text, record,index) => {
                    let renderEle = this.renderSearchRange(record, 'searchRange');
                    return renderEle;
                }
            },
            {
                title: (<div fieldid='showLocal'>{this.state.json['20023081-000006']}</div>),/* 国际化处理： 显示表头*/
                dataIndex: "showLocal",
                key: "showLocal",
                render: (text, record) => {
                    return (
                        <Select
                            showClear={false}
                            fieldid='showLocal'
                            defaultValue = 'N'
                            value={record.showLocal ? 'Y' : 'N'}
                            onChange={(value) => {
                                record.showLocal = value == 'Y' ? true : false;
                                this.setState({})
                            }}>
                            <NCOption value='Y'>{this.state.json['20023081-000020']}</NCOption>{/* 国际化处理： 表头*/}
                            <NCOption value='N'></NCOption>
                        </Select>
                    )
                }
            }
        ];
        this.secondColumn = [
            {
                title: this.state.json['20023081-000007'],/* 国际化处理： 选择*/
                dataIndex: "select",
                key: "select",
                render: (text, record, index) => {
                    return <Checkbox
                        checked={record.select}
                        onChange={() => {
                            record.index = index;
                            this.secondModalSelect(record, index);
                        }}
                    />
                }
            },
            {
                title: this.state.json['20023081-000008'],/* 国际化处理： 查询属性*/
                dataIndex: "name",
                key: "name"
            },
            {
                title: this.state.json['20023081-000009'],/* 国际化处理： 查询值*/
                dataIndex: "refpath",
                key: "refpath",
                render: (text, record) => {
                    let renderSecondEle = this.renderNewRefPath(record);
                    return renderSecondEle;
                }
            }
        ];
        return (
            <div className='right_query'>
                <div className='query_body1'>
                    <div className='query_form cashAccount'>
    		<div>
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel isRequire={true} labelname={this.state.json['20023081-000019']}/>{/* 国际化处理： 核算账簿*/}
                    </Col>
                    <Col md={7} sm={7}>
                        <div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: false,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit ,
                                        getCheckContent: getCheckContent,
                                        renderTableFirstData: () => this.renderTableFirstData(9)
                                    },
                                    'cashAccount'
                                )
                            }
                        </div>
                    </Col>
                </Row>
                {
                    this.state.isShowUnit ?
                        <Row className="myrow">
                            <Col md={2} sm={2}>
                                <FormLabel labelname={this.state.json['20023081-000023']}/>{/* 国际化处理： 业务单元*/}
                            </Col>
                            <Col md={7} sm={7}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
                                        fieldid='buSecond'
                                        value={this.state.buSecond}
                                        isMultiSelectedEnabled= {true}
                                        isShowDisabledData = {true}
                                        isHasDisabledData = {true}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                        }}
                                        onChange={(v)=>{
                                            this.getRecover('tableSourceData', 'searchRange', 'selectCells', 'selectCell', 'type', 'showLocal')
                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if(this.state.accountingbook && this.state.accountingbook) {
                                                }
                                            })
                                        }
                                        }
                                    />

                                </div>

                            </Col>
                            <Col md={3} sm={3}>
                                <FICheckbox colors="dark" className="mycheck" checked={this.state.multbusi}  onChange={this.onChangeMultbusi.bind(this)}>
                                    {this.state.json['20023081-000024']} {/* 国际化处理： 多业务单元合并*/}
                                </FICheckbox>
                            </Col>
                        </Row> :
                        <div></div>
                }

                {/*科目，级次：*/}
                <SubjectLevel
                    parent = {this}
                    accountingbook = {this.state.accountingbook}
                    isMultiSelectedEnabled = {false}//控制多选单选
                    queryConditionParam = {{
                        cashtype: '1',
                        TreeRefActionExt: 'nccloud.web.gl.accountrep.action.CashTypeAccountRefSqlBuilder',
                    }}
                    selectedKeys = {this.state.selectedKeys}
                    targetKeys = {this.state.targetKeys}
                    changeParam = {this.state.changeParam}
                    handleSelect = {this.handleSelect}
                    handleValueChange = {this.handleValueChange}

                    handleSelectChange = {(sourceSelectedKeys, targetSelectedKeys) => handleSelectChange(this, sourceSelectedKeys, targetSelectedKeys)}
                    handleChange = {(nextTargetKeys, direction, moveKeys) => handleChange(this,nextTargetKeys, direction, moveKeys) }
                    clearTransterData = {() => clearTransterData(this)}
                />

                <div className="modalTable">
                    <Table
                        columns={this.columns}
                        data={this.state.tableSourceData}
                    />
                </div>

                {/*会计期间*/}
                <DateGroup
                    selectionState = {this.state.selectionState}
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                />

                {/*包含凭证：*/}
                <CheckBoxCells
                    totalTitle= {this.state.json['20023081-000012']} ///* 国际化处理： 显示属性：*/
                    paramObj = {{
                        "noChargeVoucher": {//未记账凭证
                            show: true,
                            disabled: this.state.disabled,
                            checked: this.state.includeuntally,
                            onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                        },
                        "errorVoucher": {//错误凭证
                            show: true,
                            checked: this.state.includeerror,
                            onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                        },
                        "harmToBenefitVoucher": {//损益结转凭证
                            show: false,
                            checked: this.state.includeplcf,
                            onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                        },
                        "againClassifyVoucher": {//重分类凭证
                            show: false,
                            checked: this.state.includerc,
                            onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                        },
                    }}
                />

                {/*币种*/}
                {renderMoneyType(this.state.currtypeList,this.state.currtypeName, this.handleCurrtypeChange, this.state.json)}

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
                        this.state.json
                    )
                }

                <FourCheckBox
                    totalTitle= {this.state.json['20023081-000012']} ///* 国际化处理： 显示属性：*/
                    renderCells={
                        [
                            {
                                title: this.state.json['20023081-000044'],/* 国际化处理： 无余额无发生不显示*/
                                checked: this.state.showzerooccur,
                                id: 'noBalanceNoHappenNoShow',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'showzerooccur')
                            },
                            {
                                title: this.state.json['20023081-000045'],/* 国际化处理： 显示结算信息*/
                                checked: this.state.ckNotShowZeroOccurZeroBalance,
                                id: 'showAccountInfo',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'ckNotShowZeroOccurZeroBalance')
                            },
                        ]
                    }
                />

                <div className='searchDateSelect1 nc-theme-gray-area-bgc nc-theme-area-split-bc'>
                    <div className='searchDateStyle'>
                        <span className='an nc-theme-form-label-c'>{this.state.json['20023081-000037']}</span> {/* 国际化处理： 按*/}
                        <Select
                            showClear={false}
                            fieldid='searchDateStyle'
                            className='searchDateSelect'
                            value={this.state.searchDateStyle}
                            onChange={(value) => {
                                this.handleSelectChange(value, 'searchDateStyle');
                            }}
                        >
                            <NCOption value='N'>{this.state.json['20023081-000038']}</NCOption>{/* 国际化处理： 制单日期*/}
                            <NCOption value='Y'>{this.state.json['20023081-000039']}</NCOption>{/* 国际化处理： 签字日期*/}
                        </Select>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023081-000010']}</span>{/* 国际化处理： 查询*/}
                    </div>
                </div>

                <NewModal
                    title= {this.state.json['20023081-000016']} ///* 国际化处理： 属性选择*/
                    record={this.state.selectRow[this.state.page]}
                    column={this.secondColumn}
                    showNewModal = {this.state.showNewModal}//控制设否显示
                    closeNewModal = {this.closeNewModal}
                    sureHandle = {this.sureHandle}
                    sureText = {this.state.json['20023081-000017']} ///* 国际化处理： 确定 */
                    cancleText = {this.state.json['20023081-000011']} ///* 国际化处理： 取消*/
                />
			</div>
                    </div>
                </div>
            </div>
		)
	}
    clickPlanEve = (value) => {

        this.state = deepClone(value.conditionobj4web.nonpublic);
        this.setState(this.state)
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
        return {...this.state}
    }
    clickSearchBtn = () => {//点击查询框的"查询"按钮事件
    	let pk_accountingbook = [];
		let pk_unit = [];
		let moneyLeft = Number(this.state.balanceMoneyLeft);
		let moneyRight = Number(this.state.balanceMoneyRight);
		if(moneyLeft > moneyRight){
			toast({
				content: this.state.json['20023081-000018'],/* 国际化处理： 余额范围查询条件信息输入有误(起始值大于终止值或是为负值)！*/
				color: 'warning'
			});
			return;
		}
		let lastAssvos = [];
		this.state.tableSourceData.forEach((item)=>{
			let resultObj = {};
			if(item.selectCell && !item.selectCellAttr){
				resultObj.checktypename = item.selectCell.name;
				resultObj.pk_checktype = item.selectCell.pk_checktype;
				resultObj.checkvalue = item.selectRange;
				resultObj.headEle = item.showLocal ? 'Y' : 'N';
				resultObj.accEle = item.accele ? 'Y' : 'N';
				resultObj.includeSub = item.includesub;
				lastAssvos.push(resultObj)
			} else if(item.selectCell && item.selectCellAttr){
				resultObj.name = item.selectCell.name;
				resultObj.pk_checktype = item.selectCell.pk_checktype;
				resultObj.attr = [];
				resultObj.headEle = item.showLocal ? 'Y' : 'N';
				resultObj.accEle = item.accele ? 'Y' : 'N';
				resultObj.includeSub = item.includesub;
				item.selectCellAttr.map((itemCell) => {
					if(itemCell.select){
						resultObj.attr.push(itemCell);
					}
				})
				lastAssvos.push(resultObj)
			}
		})

		if (Array.isArray(this.state.accountingbook)) {
			this.state.accountingbook.forEach(function (item) {
				pk_accountingbook.push(item.refpk)
			})
		}

		if (Array.isArray(this.state.buSecond)) {
			this.state.buSecond.forEach(function (item) {
				pk_unit.push(item.refpk)
			})
        }
        let data = {
            /*******核算账簿****/
            pk_accountingbook: this.state.accountingbook.refpk,
            /*******业务单元****/
            pk_units: pk_unit,
            multibusi: this.state.multbusi,//复选框是否勾选

            /*******科目版本****/
            useSubjVersion: this.state.isversiondate,//是否勾选
            subjVersion: this.state.busiDate, //this.state.versiondate,//科目版本

            /*******科目****/
            startcode: this.state.startcode.code,  //开始科目编码
            endcode: this.state.endcode.code,    //结束科目编码

            /*******级次****/
            "startlvl": this.state.startlvl, // 开始级次
            "endlvl": this.state.endlvl, // 结束级次
            isleave: this.state.isleave ? 'Y' : 'N',// 所有末级true
            isoutacc: this.state.isoutacc ? 'Y' : 'N',// 表外科目true

            /*******查询对象 table框的内容****/
            qryObjs: lastAssvos,

            /*******会计期间参数****/
            querybyperiod: this.state.selectionState,
            beginyear: this.state.startyear,
            beginperiod: this.state.startperiod,
            endyear: this.state.endyear,
            endperiod:   this.state.endperiod,
            /*******日期参数****/
            begindate: this.state.begindate,
            enddate: this.state.enddate,

            /*******包含凭证****/
            includeUnTallyed: this.state.includeuntally ? 'Y' : 'N',//未记账凭证
            includeError: this.state.includeerror ? 'Y' : 'N',// 错误凭证
            includeTransfer: this.state.includeplcf ? 'Y' : 'N',//损益结转凭证
            includeReClassify: this.state.includerc ? 'Y' : 'N',//重分类凭证

            /*******币种****/
            pk_currtype: this.state.currtype,//
            currtypename: this.state.currtypeName,//

            /*******返回币种****/
            returncurrstype: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局

            /*******余额范围****/
            balanceDirection: this.state.balanceori, //"双向/借/贷/平", //余额方向
            balanceRangeFrom: this.state.balanceMoneyLeft,
            balanceRangeTo: this.state.balanceMoneyRight,

            /*******无发生不显示;本期无发生无余额不显示****/
            showzeroamount: this.state.showzerooccur ,//无发生不显示
            needCheckData: this.state.ckNotShowZeroOccurZeroBalance, //本期无发生无余额不显示true

            /*******多核算账簿显示方式;多核算账簿排序方式****/
            "rbCorpJun": this.state.defaultShowStyle, //多核算账簿显示方式：多核算账簿合并(true )
            "qryObjPlusCorp": this.state.defaultSortStyle, //多核算账簿排序方式：//查询对象+单位(true)
            "cbCorpSubjDspBase": this.state.cbCorpSubjDspBase, //按各核算账簿显示科目名称true
            "cbQryByCorpAss": this.state.cbQryByCorpAss, //按各核算账簿查询辅助核算true

            /*****按签字日期查询/按制单日期查询******/
            "isQueryBySignatureDate": this.state.searchDateStyle,// 按签字日期查询true
            // pk_accasoa://条件框选择科目
            pageindex: '1',//this.pageindex
            cashtype: '1',

            //未记账、错误、损益结转、重分类  Y/N  注意：‘包含凭证’是个标题，后面的才是选项
            mutibook: this.state.mutibook,
            showzerobalanceoccur: this.state.showzerooccur ? 'Y' : 'N',
            currplusacc: this.state.currplusacc,
            sumbysubjtype: this.state.sumbysubjtype ? 'Y' : 'N',
            twowaybalance: this.state.twowaybalance ? 'Y' : 'N',
            istree: 'Y',
            pk_accasoa: this.state.rightSelectItem,//'条件'
        }

        let url = '/nccloud/gl/accountrep/checkparam.do'
        let flagShowOrHide;
        ajax({
            url,
            data,
            async: false,
            success: function (response) {
                flagShowOrHide = true
            },
            error: function (error) {
                flagShowOrHide = false
                toast({ content: error.message, color: 'warning' });
            }
        })
        if (flagShowOrHide == true) {
            this.props.onConfirm(data)
        } else if (flagShowOrHide == false) {
            return true;
        }
    }
	render() {
        let { search } = this.props;
        let { NCCreateSearch } = search;
        return <div>
            {
                NCCreateSearch(this.searchId, {
                    onlyShowSuperBtn:true,                       // 只显示高级按钮
                    replaceSuperBtn:this.state.json['20023081-000010'],                      // 自定义替换高级按钮名称/* 国际化处理： 查询*/
                    replaceRightBody:this.renderModalList,
                    hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
                    clickPlanEve:this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                    saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
                    oid:this.props.meta.oid,
                    clickSearchBtn: this.clickSearchBtn,  // 点击按钮事件
                    isSynInitAdvSearch: true,//渲染右边面板自定义区域
                    showAdvBtn:true
                })}
        </div>
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)

export default CentralConstructorModal;
