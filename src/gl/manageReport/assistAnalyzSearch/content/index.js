/**
 * Created by liqiankun on 2018/7/09.
 * 账簿查询 ---> 辅助分析查询
 */

import React, {Component} from 'react';

import {high, base, ajax, createPage, getMultiLang, createPageIcon} from 'nc-lightapp-front';
const { NCMessage:Message, NCDiv } = base;
import '../../css/index.less'
import {searchById} from "../../common/modules/createBtn";
import HeadCom from '../../common/headSearch/headCom';
// import {titleLabels } from '../../common/headSearch/titledata';
import { SimpleTable } from 'nc-report';
import QueryModal from './components/MainSelectModal';
import reportPrint from '../../../public/components/reportPrint';
import { toast } from '../../../public/components/utils.js';
import {tableDefaultData} from '../../defaultTableData'
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import {setData} from '../../../manageReport/common/simbleTableData.js'
import HeaderArea from '../../../public/components/HeaderArea';
import "../../../public/reportcss/firstpage.less";
class AssistPropertyBalance extends Component{
    constructor(props){
        super(props);

        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
            textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            dataWithPage: [], //存放请求回来的数据信息，用于分页操作
            firstPageDisable: true,
            lastPageDisable: true,
            paramObj: {},//存放回调的查询参数，用于翻页
            accountType: 'amountcolumn',//金额式
            appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            'assistAnalyzSearch': []
        }
        this.dataPage = 1; //存放请求回来的数据椰树信息
        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.handlePage = this.handlePage.bind(this);//页面处理事件
        // this.setData= this.setData.bind(this);// 整理表格所需要的数据格式
        this.queryData = this.queryData.bind(this);//请求数据的方法；
        this.handleValueChange = handleValueChange.bind(this)
        this.searchById = searchById.bind(this);
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                'assistAnalyzSearch': [
                    {
                        title: json['2002308010-000028'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['2002308010-000029'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['2002308010-000030'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['2002308010-000031'],
                        styleClass:"m-brief"
                    }
                ]
        },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['2002308010','publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        this.setState({
            flag: true,
        })
        let appceod = this.props.getSearchParam('c');
        this.searchById('2002308010PAGE',appceod);
        // directprint templateOutput saveformat
        this.props.button.setDisabled({ directprint: true, saveformat: true });
    }
    handlePage(param, dataWithPage){//页面控制事件
        let {paramObj} = this.state;
        let dataLength = Number(dataWithPage.totalpage);
        let renderData = [];
        let obj = {};
        let stringPage = '1';
        switch(param){
            case 'firstPage':
                this.dataPage = 1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;

                this.setState({
                    paramObj,
                    firstPageDisable: true,
                    lastPageDisable: false
                })
                this.queryData(this.state.paramObj);
                return;
            case 'nextPage':
                this.dataPage += 1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;

                this.setState({
                    paramObj,
                    firstPageDisable: false
                })
                if(this.dataPage === dataLength){
                    this.setState({
                        lastPageDisable: true
                    })
                }
                this.queryData(this.state.paramObj);
                return;
            case 'prePage':
                this.dataPage -=1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                if(this.dataPage === dataLength-1){
                    this.setState({
                        lastPageDisable: false
                    })
                }
                if(this.dataPage === 1){
                    this.setState({
                        firstPageDisable: true
                    })
                }
                this.setState({
                    paramObj
                })

                this.queryData(this.state.paramObj);
                return;
            case 'lastPage':
                this.dataPage = dataLength;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                this.setState({
                    paramObj,
                    lastPageDisable: true,
                    firstPageDisable: false
                });
                this.queryData(this.state.paramObj);
                return;
        }
    }
    queryClick(){//点击"查询"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    queryData(param){//请求数据方法；
        let self = this;
        let url = '/nccloud/gl/accountrep/assanalysisquery.do';

        ajax({
            url:url,
            data:param,
            success:function(response){
                self.props.button.setDisabled({ directprint: false, saveformat: false });
                let { data, success,error } = response;
                if(success && data){
                    if(param.pageindex === '1'){
                        self.setState({
                            lastPageDisable: false
                        })
                    }
                    self.setState({
                        dataWithPage: data,
                        typeDisabled: false,
                    })
                    let renderFirstData = {};
                    renderFirstData.column = data.column;
                    renderFirstData.data = data.data;
                    renderFirstData.amountcolumn = data.amountcolumn;
                    renderFirstData.quantityamountcolumn = data.quantityamountcolumn;

                    setData(self, renderFirstData);
                }else{
                    Message.create({content: error.message, color: 'danger'});
                }
            },
            error:function(error){
                toast({content: error.message, color: 'danger'});
                throw error
            }
        })
    }
    modalSure(param){//点击弹框"确定"事件
        this.setState({
            paramObj: {...param},
            accountType: 'amountcolumn'
        })

        this.queryData(param);
    }
    //直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    handleSaveColwidth = ()=>{
        let {json}=this.state
        let info=this.refs.balanceTable.getReportInfo();
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.paramObj.pk_report,info.colWidths,json, callBack);
    }
    handleLoginBtn = (obj, btnName)=> {
        if(btnName === 'search'){//查询
            this.queryClick()
        }else if(btnName === 'directprint'){//直接输出
            this.printExcel()
        }else if(btnName === 'saveformat'){//保存格式
            this.handleSaveColwidth()
        }else if(btnName === 'refresh'){//刷新
            if (Object.keys(this.state.paramObj).length > 0) {
                this.modalSure(this.state.paramObj);
            }
        }
    }
    render(){
        return(
            <div className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['2002308010-000001']} /* 国际化处理： 辅助分析查询*/
                    btnContent = {this.props.button.createButtonApp({
                        area: 'btnarea',
                        buttonLimit: 3,
                        onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName)
                    })}
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.assistAnalyzSearch.map((items) => {
                                return(
                                    <HeadCom
                                        labels={items.title}
                                        key={items.title}
                                        lastThre = {this.state.dataWithPage && this.state.dataWithPage.headtitle}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
                                        }
                                        accountType = {this.state.accountType}
                                        isLong = {items.styleClass}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data = {this.state.dataout}
                    />
                </div>
                <div className="modalContainer">
                    <QueryModal
                        show={this.state.showModal}
                        ref='MainSelectModal'
                        title={this.state.json['2002308010-000000']} //{/* 国际化处理： 查询条件*/}
                        icon=""
                        onConfirm={(datas) => {
                            this.modalSure(datas);
                        }}
                        onCancel={() => {
                            this.queryClick()
                        }}
                    />
                </div>
            </div>
        )
    }
}

AssistPropertyBalance = createPage({})(AssistPropertyBalance)


ReactDOM.render(<AssistPropertyBalance />,document.querySelector('#app'));
