import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
//import moment from 'moment';

import {high,base,ajax, deepClone, createPage, getMultiLang} from 'nc-lightapp-front';
import '../../../../css/searchModal/index.less';
import FormLabel from '../../../../../public/components/FormLabel';


const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input, NCTooltip: Tooltip,
	NCRangePicker: RangePicker
} = base;

import { toast } from '../../../../../public/components/utils';

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';
// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';

//src/gl/refer/voucher/ExtendReportGridRef
import AccPeriodDefaultTreeGridRef from '../../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../../uapbd/refer/org/BusinessUnitTreeRef';
import NewModal from '../modal'
import AccountDefaultModelTreeRef from '../../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../../public/components/uapRefer.js';
import AssistReport from '../../../../../refer/voucher/ExtendReportGridRef'

import ReferEle from '../../../../../../gl/refer/voucher/OperatorDefaultRef/index.js';
import VoucherRefer from '../../../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef/index';//凭证参照

import {
    renderMoneyType,
    returnMoneyType,
    getSubjectVersion,
    createReferFn,
    businessUnit,
    getReferDetault,
    accountPeriodStart, accountPeriodEnd
} from '../../../../common/modules/modules';
import {getAdjustTime} from "../../../../../public/hansonTableSetData/commonFn";
import DateGroup from '../../../../common/modules/dateGroup'
import CheckBoxCells from '../../../../common/modules/checkBox';
import {handleValueChange} from '../../../../common/modules/handleValueChange'
import FourCheckBox from '../../../../common/modules/fourCheckBox'

class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
			json: {},
            isqueryuntallyed: true, //是否禁止设置查询包含未记账凭证;true: 禁止
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            showRadio: false,//控制是否显示"会计期间"的但选框；false: 不显示
            appcode: '2002308010',
            showNewModal: false,
            renderData: [], //我的弹框里数据
            innerSelectName: [], //"查询范围"里存放的数据
            innerSelectData: [], //最里层选择数据的项
            rangefrom: '', //余额范围起始值
            rangeto: '', //余额范围结束值


            listItem:{},//业务单元中模板数据对应值
			isShowUnit:false,//是否显示业务单元
			selectedSort: 'true',//排序方式
			selectedShowType: 'false', //多主题显示方式
			doubleCell: false, // 多业务单元合并
			allLast: true, // 所有末级是否选中
			outProject: false, //表外科目是否选中
			multiProjectMerge: true, //多主题合并
			subjectShow: true, //按主题列示
			balanceMoneyLeft: '', //余额范围左边输入框
            balanceMoneyRight: '', //余额范围右边输入框
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accountingUnit: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: {        //二级单元
				refname:'',
				refpk: '',
			},
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表

			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向

			/***现金日计记账/多主题科目余额表：级次***/
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次

            /***现金日计记账/多主题科目余额表：科目***/
			startcode: {
				refname:'',
				refpk: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
			},    //结束科目编码

			versiondate: '', //存放选择的版本号
			versionDateArr: [], //存放请求账簿对应的版本数组
			level:0, //返回的最大级次
			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			/***无发生不显示;本期无发生无余额不显示***/
			showzerooccur: false, //无发生不显示
            ckNotShowZeroOccurZeroBalance: false, //本期无发生无余额不显示
			/***多核算账簿显示方式***/
            defaultShowStyle: 'false',
            /***多核算账簿排序方式***/
            defaultSortStyle: 'false',
            cbCorpSubjDspBase: false,//按各核算账簿显示科目名称
            cbQryByCorpAss: false,//按各核算账簿查询辅助核算

			upLevelSubject: false, //显示上级科目
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: false, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			start: {},  //开始期间
			end: {}, //结束期间

			/*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
			startyear: '', //开始年
            startperiod: '',  //开始期间
			endyear: '',  //结束年
			endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示


            searchDateStyle: '0',//现金日记账 按制单/签字查询
			isversiondate: false,

            indexArr: [], //存放"查询对象"已经选择的索引



			//tabel组件头部
            columns:[],
			//table的数据
			tableSourceData: [],

			//第二层弹框里的table
            secondColumn:[],
            secondData: [],//第二层表格的数据

            selectRow: [],//存放所选择的不是参照类型的数据
            page: 0, //第二个弹框中要显示的行内容索引

			madeBillPeople: {refname: '', refpk: ''}, //制单人选择的参照数据
            cashierPeople: {refname: '', refpk: ''},  //出纳人选择的参照数据
            checkPeople: {refname: '', refpk: ''},    //审核人选择的参照数据
            chargePeople: {refname: '', refpk: ''},   //记账人选择的参照数据
            abstract: '', //摘要输入值
            oppositeSide: false, //对方科目取小值
			chooseoppositeSide: true, //对方科目取小值 是否可选 true:不可选  false: 可选
			voucherRefer: {refname: '', refpk: ''}, //凭证类别的参照数据
            vocherInputLeft: '', //凭证号左侧输入框
            vocherInputRight: '', //凭证号右侧侧输入框
			//统计方式
            init: true,   //期初  Y/N
            debit: true,   //借方
            credit: true,  //贷方
            cellEnd: true,     //期末
            //辅助分析表名称：
            pk_report: {
            	refname: '',
				refpk: ''
			},//报表主键
            pk_accperiodscheme: '',//会计期间参数
		}

		this.balanceMoney = this.balanceMoney.bind(this);//余额范围输入触发事件

        this.sureHandle = this.sureHandle.bind(this);
        this.closeNewModal = this.closeNewModal.bind(this);
        this.outInputChange = this.outInputChange.bind(this);



        this.renderTableFirstData = this.renderTableFirstData.bind(this);// table渲染初始的数据
        this.handleSearchObj = this.handleSearchObj.bind(this);//查询对象 选择触发事件
		this.findByKey = this.findByKey.bind(this);
		this.renderSearchRange = this.renderSearchRange.bind(this);//根据'查询对象'的选择来对应渲染'查询范围'的组件
		this.renderRefPathEle = this.renderRefPathEle.bind(this);//根据传进来的url渲染对应的参照组件
		this.handleCheckbox = this.handleCheckbox.bind(this);//计算小计	包含下级复选框的点击事件
		this.secondModalSelect = this.secondModalSelect.bind(this);//第二层弹框的选择框
		this.renderNewRefPath = this.renderNewRefPath.bind(this);//渲染第二层弹框内容的参照



		this.handleDateChange = this.handleDateChange.bind(this);//日期: 范围选择触发事件
		this.onChangeShowzerooccur = this.onChangeShowzerooccur.bind(this);//无发生不显示;本期无发生无余额不显示
		this.handleSortOrShowStyle = this.handleSortOrShowStyle.bind(this);//多核算账簿排序方式/多核算账簿显示方式
		this.handleDateRadio = this.handleDateRadio.bind(this);//会计期间/日期的单选框事件


		this.handleSelectChange = this.handleSelectChange.bind(this);//<Select>的onChange事件统一触发事件
		this.renderLevelOptions = this.renderLevelOptions.bind(this);//渲染级次的<NCOption>
		this.renderReferEle = this.renderReferEle.bind(this);//渲染 制单人；出纳人；审核人；记账人
		this.handleVocherChange = this.handleVocherChange.bind(this);// 凭证号输入；不能输入小数
		this.getSubjectVersion = getSubjectVersion;

        this.handleValueChange = handleValueChange.bind(this);
	}

	static defaultProps = {
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		// accAssItems: [],
	};

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
				json:json,
                currtype: json['2002308010-000002'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['2002308010-000002'],  //选择的币种名称/* 国际化处理： 本币*/
                columns:[
                    {
                        title: json['2002308010-000003'],/* 国际化处理： 核算类型*/
                        dataIndex: "searchObj",
                        key: "searchObj",
                        render: (text, record, index) => {
                            return (
                                <Select
                                    showClear={false}
                                    defaultValue={record.selectCell && record.selectCell.name}
                                    onChange = {(value) => this.handleSearchObj(value, record)}
                                >
                                    {
                                        text.map((item, index) => {
                                            return <NCOption value={item} key={index}>{item.name}</NCOption>
                                        })
                                    }
                                </Select>
                            )
                        }
                    },
                    {
                        title: json['2002308010-000004'],/* 国际化处理： 核算内容*/
                        dataIndex: "searchRange",
                        key: "searchRange",
                        render: (text, record,index) => {
                            let renderEle = this.renderSearchRange(record);
                            return renderEle;
                        }
                    },
                    {
                        title: json['2002308010-000005'],/* 国际化处理： 显示表头*/
                        dataIndex: "showLocal",
                        key: "showLocal",
                        render: (text, record) => {
                            return (
                                <Select
                                    showClear={false}
                                    defaultValue = 'N'
                                    value={record.showLocal}
                                    onChange={(value) => {
                                        record.showLocal = value;
                                        this.setState({})
                                    }}>
                                    <NCOption value='Y'>{this.state.json['2002308010-000023']}</NCOption>{/* 国际化处理： 表头*/}
                                    <NCOption value='N'>{this.state.json['2002308010-000024']}</NCOption>{/* 国际化处理： 表体*/}
                                </Select>
                            )
                        }
                    },
                    {
                        title: json['2002308010-000006'],/* 国际化处理： 计算小计*/
                        dataIndex: "accele",
                        key: "accele",
                        render: (text, record) => {
                            return <Checkbox
                                checked={record.accele === 'N' ? false : true}
                                onChange={() => {
                                    this.handleCheckbox('accele',record)
                                }}
                            />
                        }
                    },
                    {
                        title: json['2002308010-000007'],/* 国际化处理： 包含下级*/
                        dataIndex: "includesub",
                        key: "includesub",
                        render: (text, record) => {
                            return <Checkbox
                                checked={record.includesub === 'N' ? false : true}
                                onChange={() => {
                                    this.handleCheckbox('includesub',record)
                                }}
                            />
                        }
                    }
                ],
                secondColumn:[
                    {
                        title: json['2002308010-000008'],/* 国际化处理： 选择*/
                        dataIndex: "select",
                        key: "select",
                        render: (text, record, index) => {
                            return <Checkbox
                                checked={record.select}
                                onChange={() => {
                                    record.index = index;
                                    this.secondModalSelect(record, index);
                                }}
                            />
                        }
                    },
                    {
                        title: json['2002308010-000009'],/* 国际化处理： 查询属性*/
                        dataIndex: "name",
                        key: "name"
                    },
                    {
                        title: json['2002308010-000010'],/* 国际化处理： 查询值*/
                        dataIndex: "refpath",
                        key: "refpath",
                        render: (text, record) => {
                            let renderSecondEle = this.renderNewRefPath(record);
                            return renderSecondEle;
                        }
                    }
                ],
			},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['2002308010', 'dategroup', 'checkbox', 'fourcheckbox', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
        this.renderTableFirstData(10);
    }

    componentDidMount() {
        setTimeout(() => getReferDetault(this, false, {
            businessUnit: businessUnit ,
        }),0)
    }

    handleVocherChange(value, param){// 凭证号输入；不能输入小数
        let result = value;
        let arrResult = result.split('');//输入值变为数组
        let code = arrResult[arrResult.length - 1];//最后一次输入的元素
        let codeValue = code && code.toString().charCodeAt(0);
        if(codeValue < 48 || codeValue > 57) {//不是数字的输入
            arrResult.splice(arrResult.length - 1);
        }
        this.setState({
            [param]: arrResult.join('')
        });
	}
    renderLevelOptions(num){
    	let optionArr = [];
    	for(let i=0; i<num; i++){
            optionArr.push(<NCOption value={i+1} key={i}>{i+1}</NCOption>);
		}
        return optionArr;
	}
    handleSelectChange(value, param){//<Select>的onChange事件统一触发事件
    	this.setState({
			[param]: String(value)
		})
	}
    handleDateRadio(value, param){//会计期间/日期的单选框事件
    	this.setState({
			[param]: value
		})
	}
	handleSortOrShowStyle(value, param){//多核算账簿显示方式/多核算账簿排序方式触发事件
		this.setState({
			[param]: value
		})
	}
    handleDateChange(value){//日期: 范围选择触发事件
    	this.setState({
            begindate: value[0],
            enddate: value[1],
			rangeDate: value
		})
	}
    handleSearchObj(value, record){//查询对象选择具体科目
    	let { tableSourceData, selectRow } = this.state;
        let originData = this.findByKey(record.key, tableSourceData);
        originData.selectCell = value;

        if(value.refpath ){
            originData.type = 'referpath';
            originData.searchRange = value.refpath;
		}else if(value.datatype === '33'){
            originData.type = 'date'
		} else if(value.attr){
            originData.selectCellAttr = [...value.attr];
            originData.type = 'modal'
		}
		let newSelectRow = [...selectRow];
        newSelectRow[record.key] = record;
    	this.setState({
            tableSourceData,
			selectRow: newSelectRow
		},(preState)=>{
		})
	}
    handleCheckbox(key, record){//计算小计	包含下级复选框的点击事件
    	let {tableSourceData} = this.state;
        record[key] = record[key] === 'N' ? 'Y' : 'N';
        this.setState({
            tableSourceData
		})

	}
    secondModalSelect(record, index){//第二层弹框的复选框
    	let {tableSourceData} = this.state;
        let newSelect = !record.select;
        record.select = newSelect;
        tableSourceData[record.itemKey].selectCellAttr[index] = record;
        this.setState({tableSourceData},() => {
		})
	}
    findByKey(key, rows) {
        let rt = null;
        let self = this;
        rows.forEach(function(v, i, a) {
            if (v.key == key) {
                rt = v;
            }
        });
        return rt;
    }
    renderSearchRange(record){//根据'查询对象'的选择来对应渲染'查询范围'的组件
    	switch(record.type){
			case "referpath":
				let refPathEle = this.renderRefPathEle(record);
				return refPathEle;
			case 'date':
				return;
			case 'modal':
                let attrObj = record.selectCellAttr;
                let { tableSourceData } = this.state;
                let selectObj = [];
                attrObj.map((item)=>{
                	if(item){
                        item.itemKey = record.key;
					}
					if(item && item.selectName){
                        selectObj += item.selectName + ','
					}
				})
				return (
                    <div style={{display: 'flex'}}>
                        <Input
                            style={{border: 'none'}}
							placeholder={record.selectCell && record.selectCell.name}
							value={selectObj}
						/>
                        <div className='ellipsisContainer'
                             onClick={() => this.closeNewModal(record)}
                        >
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                        </div>
                    </div>
				);
			default:
				return <Input disabled/>;
		}
	}
    renderNewRefPath(record){
        let mybook;

        if(record.refpath){
            let objKey = record.attrclassid;
            if(!this.state[objKey]){//undefined
                {createScript.call(this,record.refpath+".js",objKey)}

            }else {
                mybook = (
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state[objKey] ? (this.state[objKey])(
                                    {
                                        value:{refname: record.selectName && record.selectName[0], refpk:  record.refpk && record.refpk[0]},
                                        isMultiSelectedEnabled: true,
                                        queryCondition: () => {
                                            return {
                                                "pk_accountingbook": this.state.accountingbook.pk_accountingbook ? this.state.accountingbook.pk_accountingbook : '',
                                                // "versiondate":currrentDate,
                                            }
                                        },
                                        onChange: (v) => {
                                            let {tableSourceData} = this.state;
                                            let refpkArr = [];
                                            let selectName = [];
                                            v.forEach((item) => {
                                                refpkArr.push(item.refpk);
                                                selectName.push(item.refname)
                                            })
                                            let pk_checkvalue = refpkArr.join(',')
                                            record.pk_checkvalue = pk_checkvalue;
                                            record.selectName = selectName;
                                            record.refpk = refpkArr;
                                            this.setState({
                                                tableSourceData
                                            })
                                        }
                                    }
                                ) : <div/>
                            }
                        </Col>
                    </Row>
                );
            }
		}

        return mybook;
	}
	renderRefPathEle(record){//根据传进来的url渲染对应的参照组件
        //带有参照的
		let mybook;
		let objKey = record.key + record.selectCell.pk_checktype;
		if(!this.state[objKey]){//undefined
			{createScript.call(this,record.selectCell.refpath+".js",objKey)}

		}else{
			mybook =  (
				<Row>
					<Col xs={12} md={12}>
						{
							this.state[objKey]?(this.state[objKey])(
								{
									isMultiSelectedEnabled:true,
									queryCondition:() => {
										return {
											"pk_accountingbook": this.state.accountingbook.pk_accountingbook? this.state.accountingbook.pk_accountingbook:'',
											// "versiondate":currrentDate,
										}
									},
									onChange: (v)=>{
										let {tableSourceData} = this.state;
										let refpkArr = [];
										v.forEach((item)=>{
											let cellObj = {};
                                            cellObj.pk_checkvalue = item.refpk;
                                            cellObj.checkvaluecode = item.refcode;
                                            cellObj.checkvaluename = item.refname;
											refpkArr.push(cellObj);
										})
										let pk_checkvalue = [...refpkArr];
                                        record.selectRange = pk_checkvalue;
										this.setState({
                                            tableSourceData
										})
									}
								}
							):<div/>
						}
					</Col>
				</Row>
			);
		}
		return mybook;
	}
    renderTableFirstData(length){
    	let firstData = [];
    	for(let i=0; i<length; i++){
    		let obj = {};
    		obj.searchObj = [];
            obj.searchRange = '';
            obj.showLocal = 'N';//显示位置
            obj.accele = 'N';//小计
            obj.includesub = 'N',//包含下级
			obj.key = i
            firstData.push(obj);
		}
		this.setState({
            tableSourceData: [...firstData]
		})
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}




	getCurrtypeList = () => {//获取"币种"接口
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {"localType":"1", "showAllCurr":"1"};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					let newData = data.filter((item, index) => {
						return item.name !== self.state.json['2002308010-000013']/* 国际化处理： 所有币种*/
					});
					self.setState({
						currtypeList: newData,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


	handleCurrtypeChange = (keyParam, value) => {
		this.setState({
			[keyParam]: value,
		});
	}


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(v) {
		if (!this.state.accountingbook || !this.state.accountingbook) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,	
			year: '',		
		};
		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        		// versiondate: '2018-5-31', //data.versiondate,
		        	},)

                    self.getSubjectVersion(self);
                    let newUrl = '/nccloud/gl/accountrep/assbalancequeryobject.do';//现金日记账
                    let newData = {
                        "pk_accountingbook":v.refpk,
                        "versiondate": '2018-5-31',//self.state.versiondate,
                        "needaccount": false
                    };
                    ajax({
                        url:newUrl,
                        data:newData,
                        success: function(response){
                            const { success } = response;
                            //渲染已有账表数据遮罩
                            if (success) {
                                if(response.data){
                                    response.data.map((item) => {
                                    	if(item.attr){
                                            item.attr.map((cell) => {
                                                cell.select = false
											})
										}
									})
                                    let newTableFirstData = [...self.state.tableSourceData];
                                    newTableFirstData.forEach((item, index) => {
                                        item.searchObj = response.data;
									})

                                    self.setState({
                                        tableSourceData: newTableFirstData
                                    })
                                }
                            }
                        }
                    });
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: '2018-5-31'// value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally = (e) => {
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化/本期无发生无余额不显示;按各核算账簿显示科目名称/按各核算账簿查询辅助核算
	onChangeShowzerooccur(value, param) {
		this.setState({
			[param]: value
		});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}
	//显示上级科目复选框变化
	onChangeupLevelSubject(e) {
		this.setState({upLevelSubject: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}
	handleRadioSort(param){
		this.setState({
			selectedSort: param
		})
	}
	handleShowType(param){
		this.setState({
			selectedShowType: param
		})
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
	onChangeDoubleCell(param){
		this.setState({
			doubleCell: !this.state.doubleCell
		})
	}
	changeProjectStyle(param){//所有末级   表外科目选择
		if(param === 'allLast'){
			this.setState({
				allLast: !this.state.allLast
			})
		}else{
			this.setState({
				outProject: !this.state.outProject
			})
		}
	}

	handleGTypeChange =() =>{
		
	};

    shouldComponentUpdate(nextProps, nextState){
		return true;
	}



    outInputChange(param){
    	let nameArr = [];
        param.forEach((item) => {
            nameArr.push(item.refname);
		})
		this.setState({
            innerSelectName: [...nameArr]
		})
	}
    sureHandle(param){
    	let SelectCell = [];
        // param.forEach((item) => {
        	// if(item.pk_checkvalue){
         //        SelectCell.push(item);
		// 	}
		// });
        this.setState({
			innerSelectData: [...SelectCell]
		})
    	this.closeNewModal(param);
	}
    closeNewModal(param){
		this.setState({
            showNewModal: !this.state.showNewModal,
			page: param.key
		})
    }
    balanceMoney(value, param){
        let arrResult = value.split('');//输入值变为数组
        let code = arrResult[arrResult.length - 1];//最后一次输入的元素
        let codeValue = code && code.toString().charCodeAt(0);
        if (codeValue < 48 || codeValue > 57) {//不是数字的输入给剪掉
            arrResult.splice(arrResult.length - 1);
        }
    	this.setState({
			[param]: arrResult.join('')//数组变为字符串
		})
	}

	/****
	 * param: String
	 * 根据传入的参数渲染对应的参照组件
	 * ***/
	renderReferEle(param){
    	let referUrl = "gl/refer/voucher/OperatorDefaultRef/index.js" //
        if(!this.state[param]){//undefined
            {createScript.call(this,referUrl,param)}
        }else{
            return (
                <Row>
                    <Col xs={12} md={12}>
                        {
                            this.state[param]?(this.state[param])(
                                {
                                    value:{refname: this.state.accountingbook && this.state.accountingbook.refname, refpk: this.state.accountingbook && this.state.accountingbook.refpk},
                                    queryCondition:() => {
                                        // return {
                                        // 	"pk_accountingbook": self.state.pk_accountingbook.value
                                        // }
                                    },
                                    onChange: (v)=>{
                                        let self = this;
                                    }

                                }
                            ):<div/>
                        }
                    </Col>
                </Row>
			)
        }
	}
    render() {
        let { show, title, isButtonShow}= this.props;


        return<Modal
            fieldid='query'
            show={ show }
            onHide={()=>this.props.onCancel(true)}
            className='cashQuery'
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    {title}
                </Modal.Title>
            </Modal.Header >
            <Modal.Body id="modalOuter">
                <div className='right_query noserchmatter'>
                    <div className='query_body1'>
                        <div className='query_form assistAnalyzSearch'>
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel isRequire={true} labelname={this.state.json['2002308010-000025']} />{/* 国际化处理： 核算账簿*/}
                    </Col>
                    <Col md={10} sm={10}>
                        <div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: false,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit ,
                                    },
                                    "assistAnalyzSearch"
                                )
                            }
                        </div>
                    </Col>
                </Row>

                <Row className="myrow specialrow-lable">
                    <Col md={2} sm={2}>
                        <FormLabel labelname={this.state.json['2002308010-000026']} />{/* 国际化处理： 辅助分析表名称*/}
                    </Col>
                    <Col md={8} sm={8}>
                        <AssistReport
                            fieldid='pk_report'
                            value={this.state.pk_report}
                            queryCondition = {() => {
                                let condition = {
                                    pk_accountingbook: this.state.accountingbook.refpk,
                                    GridRefActionExt: 'nccloud.web.gl.ref.ExtendreportRefSqlBuilder'
                                }
                                return condition
                            }}
                            onChange={(value) => {
                                this.setState({
                                    pk_report: {...value}
                                })
                            }}
                        />
                    </Col>
                </Row>



                {/*会计期间*/}
                <DateGroup
                    selectionState = {this.state.selectionState}
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                />

                {/*包含凭证*/}
                <CheckBoxCells
                    paramObj = {{
                        "noChargeVoucher": {//未记账凭证
                            show: true,
                            checked: this.state.includeuntally,
                            onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                        },
                        "errorVoucher": {//错误凭证
                            show: true,
                            checked: this.state.includeerror,
                            onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                        },
                        "harmToBenefitVoucher": {//损益结转凭证
                            show: false,
                            checked: this.state.includeplcf,
                            onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                        },
                        "againClassifyVoucher": {//重分类凭证
                            show: false,
                            checked: this.state.includerc,
                            onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                        },
                    }}
                />

                {/*币种：*/}
                {renderMoneyType(this.state.currtypeList,this.state.currtypeName, this.handleCurrtypeChange, this.state.json)}

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
                        this.state.json
                    )
                }

                <FourCheckBox
                    totalTitle= {this.state.json['2002308010-000014']} ///* 国际化处理： 统计方式：*/
                    renderCells={
                        [
                            {
                                title: this.state.json['2002308010-000015'],/* 国际化处理： 初期*/
                                checked: this.state.init,
                                id: 'init',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'init')
                            },
                            {
                                title: this.state.json['2002308010-000016'],/* 国际化处理： 借方*/
                                checked: this.state.debit,
                                id: 'debit',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'debit')
                            },
                            {
                                title: this.state.json['2002308010-000017'],/* 国际化处理： 贷方*/
                                checked: this.state.credit,
                                id: 'credit',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'credit')
                            },
                            {
                                title: this.state.json['2002308010-000018'],/* 国际化处理： 期末*/
                                checked: this.state.cellEnd,
                                id: 'cellEnd',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'cellEnd')
                            }
                        ]
                    }
                />

                <NewModal
                    title= {this.state.json['2002308010-000019']} ///* 国际化处理： 属性选择*/
                    record={this.state.selectRow[this.state.page]}
                    column={this.state.secondColumn}
                    showNewModal = {this.state.showNewModal}//控制设否显示
                    closeNewModal = {this.closeNewModal}
                    sureHandle = {this.sureHandle}
                    sureText = {this.state.json['2002308010-000020']} ///* 国际化处理： 确定 */
                    cancleText = {this.state.json['2002308010-000012']} ///* 国际化处理： 取消*/
                />
						</div>
					</div>
				</div>
            </Modal.Body>
            {isButtonShow &&
            <Modal.Footer>
                <Button
                    className= "button-primary"
                    fieldid='query'
                    onClick = {() => {
                        let pk_accountingbook = [];
                        let pk_unit = [];
                        let moneyLeft = Number(this.state.balanceMoneyLeft);
                        let moneyRight = Number(this.state.balanceMoneyRight);
                        if(moneyLeft > moneyRight){
                            Message.create({
                                content: this.state.json['2002308010-000021'],/* 国际化处理： 余额范围查询条件信息输入有误(起始值大于终止值或是为负值)！*/
                                color: 'warning'
                            });
                            return;
                        }
                        let lastAssvos = [];
                        this.state.tableSourceData.forEach((item)=>{
                            let resultObj = {};
                            if(item.selectCell && !item.selectCellAttr){
                                resultObj.checktypename = item.selectCell.name;
                                resultObj.pk_checktype = item.selectCell.pk_checktype;
                                resultObj.checkvalue = item.selectRange;
                                resultObj.headEle = item.showLocal;
                                resultObj.accEle = item.accele;
                                resultObj.includeSub = item.includesub;
                                lastAssvos.push(resultObj)
                            } else if(item.selectCell && item.selectCellAttr){
                                resultObj.name = item.selectCell.name;
                                resultObj.pk_checktype = item.selectCell.pk_checktype;
                                resultObj.attr = [];
                                resultObj.headEle = item.showLocal;
                                resultObj.accEle = item.accele;
                                resultObj.includeSub = item.includesub;
                                item.selectCellAttr.map((itemCell) => {
                                    if(itemCell.select){
                                        resultObj.attr.push(itemCell);
                                    }
                                })
                                lastAssvos.push(resultObj)
                            }
                        })
                        if (Array.isArray(this.state.accountingbook)) {
                            this.state.accountingbook.forEach(function (item) {
                                pk_accountingbook.push(item.refpk)
                            })
                        }

                        if (Array.isArray(this.state.buSecond)) {
                            this.state.buSecond.forEach(function (item) {
                                pk_unit.push(item.refpk)
                            })
                        }
                        let data = {
                            /*******核算账簿****/
                            pk_accountingbook: this.state.accountingbook.refpk,

                            /***辅助分析表名称：***/
                            pk_report: this.state.pk_report.refpk,  //报表主键

                            /*******会计期间参数****/
                            endyear: this.state.endyear,
                            startyear: this.state.startyear,
                            // querybyperiod: this.state.selectionState,
                            // startyear: this.state.startyear,
                            startperiod: this.state.startperiod,
                            // endyear: this.state.endyear,
                            endperiod:   this.state.endperiod,

                            /*******包含凭证****/
                            includeuntally: this.state.includeuntally ? 'Y' : 'N',//未记账凭证
                            includeerror: this.state.includeerror ? 'Y' : 'N',// 错误凭证
                            // includetransfer: this.state.includeplcf ? 'Y' : 'N',//损益结转凭证
                            // includereClassify: this.state.includerc ? 'Y' : 'N',//重分类凭证

                            /*******币种****/
                            pk_currtype: this.state.currtype,//
                            // currtypename: this.state.currtypeName,//

                            /*******返回币种****/
                            returncurr: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局

                            //统计方式：
                            init: this.state.init,   //期初  Y/N
                            debit: this.state.debit,   //借方
                            credit: this.state.credit,  //贷方
                            end: this.state.cellEnd,     //期末
                        }
                        
                        let url = '/nccloud/gl/accountrep/checkparam.do'
                        let flagShowOrHide;
                        ajax({
                            url,
                            data,
                            async: false,
                            success: function (response) {
                                flagShowOrHide = true
                            },
                            error: function (error) {
                                flagShowOrHide = false
                                toast({ content: error.message, color: 'warning' });
                            }
                        })
                        if (flagShowOrHide == true) {
                            this.props.onConfirm(data)
                        } else if (flagShowOrHide == false) {
                            return true;
                        }
                    }}
                >
                    {this.state.json['2002308010-000011']}{/* 国际化处理： 查询*/}
                </Button>


                <Button
                    fieldid='cancel'
                    className= 'btn-2 btn-cancel'
                    onClick={() => {
                        this.props.onCancel(false);
                    }}
                >{this.state.json['2002308010-000012']}</Button>{/* 国际化处理： 取消*/}
            </Modal.Footer>
            }
            <Loading fullScreen showBackDrop={true} show={this.state.isLoading} />



        </Modal>;
    }
}
CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal
