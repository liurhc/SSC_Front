/**
 * Created by liqiankun on 2018/7/04.
 * 账簿查询 ---> 资金日报表
 */

import React, {Component} from 'react';

import {high, base, ajax, createPage, getMultiLang, gzip} from 'nc-lightapp-front';
const { NCDiv } = base;
import '../../css/index.less'
import {toast} from "../../../public/components/utils";
import HeadCom from '../../common/headSearch/headCom';
import { SimpleTable } from 'nc-report';
import QueryModal from './components/MainSelectModal';
import {tableDefaultData} from '../../defaultTableData'
import reportPrint from '../../../public/components/reportPrint';
const { PrintOutput } = high;
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import {setData} from "../../common/simbleTableData";
import {searchById} from "../../common/modules/createBtn";
import "../../../public/reportcss/firstpage.less";
import {handleSimpleTableClick} from "../../common/modules/simpleTableClick";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import {rowBackgroundColor} from "../../common/htRowBackground";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
import { queryRelatedAppcode, originAppcode } from '../../../public/common/queryRelatedAppcode.js';
class AssistPropertyBalance extends Component{
    constructor(props){
        super(props);

        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            disabled: true,
			visible: false,
            appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            outputData: {},
            ctemplate: '', //模板输出-模板
            nodekey: '',
            printParams:{}, //查询框参数，打印使用
            showAddBtn: true,
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
            textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            dataWithPage: [], //存放请求回来的数据信息，用于分页操作
            firstPageDisable: true,
            lastPageDisable: true,
            paramObj: {},//存放回调的查询参数，用于翻页
            accountType: 'amountcolumn',//金额式
            'captialReport': []
        }
        this.dataPage = 1; //存放请求回来的数据椰树信息
        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.handlePage = this.handlePage.bind(this);//页面处理事件
        // this.setData= this.setData.bind(this);// 整理表格所需要的数据格式
        this.queryData = this.queryData.bind(this);//请求数据的方法；
        this.handleValueChange = handleValueChange.bind(this);
        // this.changeSelectStyle = this.changeSelectStyle.bind(this);//选择 '账簿格式'事件
        this.searchById = searchById.bind(this);
        this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                'captialReport': [
                    {
                        title: json['20023083-000041'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20023083-000042'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023083-000043'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023083-000044'],
                        styleClass:"m-brief"
                    }

                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023083','publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }

    componentDidMount(){
        this.setState({
            flag: true,
        });
        let appceod = this.props.getSearchParam('c');
        this.searchById('20023083PAGE',appceod);
        this.props.button.setDisabled({
            print:true, directprint:true,
            linkvoucher:true, saveformat:true
        })
    }

    showPrintModal = () => {
		this.setState({
			visible: true
		})
	}
	handlePrint(data, isPreview) {
        let printUrl = '/nccloud/gl/accountrep/capitalreportprint.do'
        let { printParams } = this.state
        let { ctemplate, nodekey } = data
        let appcode = '20023015' 
        printParams.queryvo = data
        printParams.startyear = printParams.beginyear
        printParams.startperiod = printParams.beginperiod
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
		printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
		this.handleCancel()
	}
	handleCancel() {
		this.setState({
			visible: false
		});
    }
    showOutputModal = () => {
        // this.refs.printOutput.open()
        let outputUrl = '/nccloud/gl/accountrep/capitalreportoutput.do'
        let { nodekey, ctemplate, printParams } = this.state
        let appcode = '20023015' 
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)
    }
    handleOutput() {

    }

    handlePage(param, dataWithPage){//页面控制事件

        let {paramObj} = this.state;
        let dataLength = Number(dataWithPage.totalpage);
        let renderData = [];
        let obj = {};
        let stringPage = '1';
        switch(param){
            case 'firstPage':
                this.dataPage = 1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;

                this.setState({
                    paramObj,
                    firstPageDisable: true,
                    lastPageDisable: false
                })

                this.queryData(this.state.paramObj);
                return;
            case 'nextPage':
                this.dataPage += 1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;

                this.setState({
                    paramObj,
                    firstPageDisable: false
                })
                if(this.dataPage === dataLength){
                    this.setState({
                        lastPageDisable: true
                    })
                }
                this.queryData(this.state.paramObj);
                return;
            case 'prePage':
                this.dataPage -=1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                if(this.dataPage === dataLength-1){
                    this.setState({
                        lastPageDisable: false
                    })
                }
                if(this.dataPage === 1){
                    this.setState({
                        firstPageDisable: true
                    })
                }
                this.setState({
                    paramObj
                })

                this.queryData(this.state.paramObj);
                return;
            case 'lastPage':
                this.dataPage = dataLength;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                this.setState({
                    paramObj,
                    lastPageDisable: true,
                    firstPageDisable: false
                });
                this.queryData(this.state.paramObj);
                return;
        }
    }
    queryClick(){//点击"查询"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    queryData(param){//请求数据方法；
        let self = this;
        let url = '/nccloud/gl/accountrep/dailyreportquery.do';
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, success } = response;
                self.setState({
                    showAddBtn: false,
                })
                if(success && data){
                    if(data.data.length>0){
                        self.setState({
                            disabled:false,
                            typeDisabled: false,//账簿格式 默认是禁用的
                            selectRowIndex:0
                        })
                        self.props.button.setDisabled({
                            print:false, directprint:false,
                            saveformat:false, refresh:false
                        })
                    }
                    if(param.pageindex === '1'){
                        self.setState({
                            lastPageDisable: false
                        })
                    }
                    self.setState({
                        dataWithPage: data,
                    })
                    let renderFirstData = {};
                    renderFirstData.column = data.column;
                    renderFirstData.data = data.data;
                    renderFirstData.amountcolumn = data.amountcolumn;
                    renderFirstData.quantityamountcolumn = data.quantityamountcolumn;
                    setData(self, renderFirstData);
                }else{
                    toast({content: this.state.json['20023083-000001'], color: 'danger'});/* 国际化处理： 请求数据为空！*/
                }
            },
            error:function(error){
                toast({content: error.message, color: 'danger'});
                throw error
            }
        })
    }
    modalSure(param){//点击弹框"确定"事件
        this.setState({
            paramObj: {...param},
            printParams: {...param}
        })

        this.queryData(param);
    }
    //直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    getDetailPort(paramObj){//联查"明细"
        let url = paramObj.url; //'/nccloud/gl/accountrep/triaccquery.do';
        let data = this.getDataType(paramObj);
        this.jumpToDetail(data, paramObj);

    }
    getDataType = (param) => {
        let selectRow = this.getSelectRowData()[0].link;

        let result = '';
        if(param.key === 'detail'){
            result = {
                origparam: {...this.state.paramObj},
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.account.DailyReportLinkTridetailParamTransfer"
            }
        }else if(param.key === 'totalAccount'){
            result = {
                ...this.state.paramObj,
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.account.AccbalLinkTriaccbookParamTransfer"
            }
        }else if(param.key === 'assist'){//联查"辅助"
            selectRow['key'] = 'relevanceAssist';
            result = {
                ...this.state.paramObj,
                link: {...selectRow},
                "pk_accassitems": [...this.state.selectRow],
                "class": "nccloud.pubimpl.gl.account.AccbalLinkAssbalParamTransfer",
                "from": "accbal" // 联查来源：(多主体)科目余额表accbal
            }
        }else if(param.key === 'multiAccountShow'){
            result = {
                ...this.state.paramObj,
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.account.AccbalLinkMultiorgParamTransfer",
                "from": "multicorp"
            }
        }
        return result;
    }
    jumpToDetail = (data, paramObj) => {
        let gziptools = new gzip();
        this.props.openTo(
            paramObj.jumpTo,
            {appcode: paramObj.appcode, status: gziptools.zip(JSON.stringify(data)),id: '12wew24'}
        );
        this.setState({
            selectRow: []
        })
    }
    //获取handsonTable当前选中行数据
    getSelectRowData=()=>{
        let selectRowData=this.refs.balanceTable.getRowRecord();
        return selectRowData;
    }

    getSelectRow = (param) => {
        this.setState({
            selectRow:[...this.state.selectRow.concat(param)]
        })
    }
    handleSaveColwidth=()=>{
        let info=this.refs.balanceTable.getReportInfo();
        let {json}=this.state
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.appcode,info.colWidths,json, callBack);
    }
    handleLoginBtn = (obj, btnName) => {
        if(btnName === 'print'){//打印
            this.showPrintModal()
        }else if(btnName === 'directprint'){//直接输出
            this.printExcel()
        }else if(btnName === 'linkvoucher'){//联查明细
            this.getDetailPort({
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: '/gl/threedetail/pages/main/index.html',
                key: 'detail',
                appcode: queryRelatedAppcode(originAppcode.detailbook)
            })
        }else if(btnName === 'saveformat'){//保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'refresh'){
            if (Object.keys(this.state.paramObj).length > 0) {
                this.modalSure(this.state.paramObj);
            }
        }
    }
    render(){
        let { modal } = this.props;
        const { createModal } = modal;
        return(
            <div className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['20023083-000002']} /* 国际化处理： 资金日报表*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <QueryModal
                                    onConfirm={(datas) => {
                                        this.modalSure(datas);
                                    }}
                                />
                            </div>
                            {//按钮集合
                                this.props.button.createButtonApp({
                                    area: 'btnarea',
                                    onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                                })
                            }
                        </div>
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.captialReport.map((items) => {
                                return(
                                    <HeadCom
                                        labels={items.title}
                                        key={items.title}
                                        content={this.state.dataWithPage.data}
                                        lastThre = {this.state.dataWithPage && this.state.dataWithPage.headtitle}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
                                        }
                                        accountType = {this.state.accountType}
                                        isLong = {items.styleClass}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data = {this.state.dataout}
                        // onCellMouseDown = {(e, coord, td) => this.handleSimpleTableClick(e, coord, td, 'capitalReport')}
                        onCellMouseDown = {(e, coords, td) => {
                            this.whetherDetail('link','multiAccountSearch');
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
                </div>

                <RepPrintModal
                    noRadio={true}
					// scopeGray={true}
					noCheckBox={true}
					appcode = '20023015'  //日报表appcode,不可从路径中读取
					visible={this.state.visible}
					handlePrint={this.handlePrint.bind(this)}
					handleCancel={this.handleCancel.bind(this)}
				/>
                <PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/accountrep/capitalreportoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
                {createModal('printService', {
                    className: 'print-service'
                })}
                <Iframe />
            </div>
        )
    }
}


AssistPropertyBalance = createPage({})(AssistPropertyBalance)

ReactDOM.render(<AssistPropertyBalance />,document.querySelector('#app'));
