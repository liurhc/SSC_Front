import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
//import moment from 'moment';

import {high,base,ajax, deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import '../../../../css/searchModal/index.less';



const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input, NCTooltip: Tooltip,
	NCRangePicker: RangePicker
} = base;

import { toast } from '../../../../../public/components/utils';

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';
const Timeformat = "YYYY-MM-DD HH:mm:ss";

// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import AccPeriodDefaultTreeGridRef from '../../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';
import NewModal from '../modal'
import AccountDefaultModelTreeRef from '../../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../../public/components/uapRefer.js';

import ReferEle from '../../../../../../gl/refer/voucher/OperatorDefaultRef/index.js';
import VoucherRefer from '../../../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef/index';//凭证参照
import CtlSysRef from '../../../../../refer/voucher/CtlSysGridRef/index';//来源系统参照
import {
    renderMoneyType,
    getSubjectVersion,
    returnMoneyType,
    createReferFn,
    renderLevelOptions,
    businessUnit, getReferDetault,
    getCheckContent,//账簿变化取辅助参照，科目版本变化取辅助参照
} from '../../../../common/modules/modules';
import SubjectVersion from "../../../../common/modules/subjectVersion"
import DateGroup from '../../../../common/modules/dateGroup'
import CheckBoxCells from '../../../../common/modules/checkBox';
import {handleValueChange} from '../../../../common/modules/handleValueChange'
import initTemplate from '../../../../modalFiles/initTemplate';
import {handleNumberInput} from "../../../../common/modules/numberInputFn";
import FormLabel from '../../../../../public/components/FormLabel';
class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
            appcode: "20028005",
            json: {},
            dateInputPlaceholder: '',/* 国际化处理： 选择日期*/
            voucherStatus: false,
            assistDateValue: '', //辅助参照中的日期的state值
            assistDateTimeValue: '', //辅助参照中的日期+时分秒的state值
            stringRefValue: '', //辅助参照中的字符的state值
            numberRefValue: '',//辅助参照中的数值的state值
            numberRefValueInteger: '',//辅助参照中整数值
            booleanValue: '', //辅助参照中布尔值
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            showRadio: true,//控制是否显示"会计期间"的但选框；false: 不显示
            versiondate: '', //存放选择的版本号
            versionDateArr: [], //存放请求账簿对应的版本数组
            level:0, //返回的最大级次
            isShowUnit:false,//是否显示业务单元r
			busiDate: '', //业务日期



            pk_accperiodscheme: '',//会计期间参数
			showNewModal: false,
            renderData: [], //我的弹框里数据
			innerSelectName: [], //"查询范围"里存放的数据
            innerSelectData: [], //最里层选择数据的项
            rangefrom: '', //余额范围起始值
            rangeto: '', //余额范围结束值


            listItem:{},//业务单元中模板数据对应值

			selectedSort: 'true',//排序方式
			selectedShowType: 'false', //多主题显示方式
			doubleCell: false, // 多业务单元合并
			allLast: true, // 所有末级是否选中
			outProject: false, //表外科目是否选中
			multiProjectMerge: true, //多主题合并
			subjectShow: true, //按主题列示
			balanceMoneyLeft: '', //余额范围左边输入框
            balanceMoneyRight: '', //余额范围右边输入框
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accountingUnit: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: [],//业务单元
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表

			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向

			/***现金日计记账/多主题科目余额表：级次***/
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次

            /***现金日计记账/多主题科目余额表：科目***/
			startcode: {
				refname:'',
				refpk: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
			},    //结束科目编码


			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			/***无发生不显示;本期无发生无余额不显示***/
			showzerooccur: false, //无发生不显示
            ckNotShowZeroOccurZeroBalance: false, //本期无发生无余额不显示
			/***多核算账簿显示方式***/
            defaultShowStyle: 'false',
            /***多核算账簿排序方式***/
            defaultSortStyle: 'false',
            cbCorpSubjDspBase: false,//按各核算账簿显示科目名称
            cbQryByCorpAss: false,//按各核算账簿查询辅助核算

			upLevelSubject: false, //显示上级科目
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: false, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			start: {},  //开始期间
			end: {}, //结束期间

			/*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
			startyear: '', //开始年
            startperiod: '',  //开始期间
			endyear: '',  //结束年
			endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示


            searchDateStyle: '0',//现金日记账 按制单/签字查询
			isversiondate: false,

            indexArr: [], //存放"查询对象"已经选择的索引



			//tabel组件头部

			//table的数据
			tableSourceData: [],





			//第二层弹框里的table
            secondData: [],//第二层表格的数据

            selectRow: [],//存放所选择的不是参照类型的数据
            page: 0, //第二个弹框中要显示的行内容索引

			madeBillPeople: [], //制单人选择的参照数据
            cashierPeople: [],  //出纳人选择的参照数据
            checkPeople: [],    //审核人选择的参照数据
            chargePeople: [],   //记账人选择的参照数据
            sourceSystem: [],   //来源系统的参照数据
            abstract: '', //摘要输入值
            oppositeSide: false, //对方科目取小值
			chooseoppositeSide: true, //对方科目取小值 是否可选 true:不可选  false: 可选
			voucherRefer: {refname: '', refpk: ''}, //凭证类别的参照数据
            vocherInputLeft: '', //凭证号左侧输入框
            vocherInputRight: '', //凭证号右侧侧输入框
		}
        
		this.handleTableSelect = this.handleTableSelect.bind(this);//辅助核算表格中的⌚️
		this.balanceMoney = this.balanceMoney.bind(this);//余额范围输入触发事件

        this.sureHandle = this.sureHandle.bind(this);
        this.closeNewModal = this.closeNewModal.bind(this);
        this.outInputChange = this.outInputChange.bind(this);

        this.renderTableFirstData = this.renderTableFirstData.bind(this);// table渲染初始的数据
        this.handleSearchObj = this.handleSearchObj.bind(this);//查询对象 选择触发事件
		this.findByKey = this.findByKey.bind(this);
		this.renderSearchRange = this.renderSearchRange.bind(this);//根据'查询对象'的选择来对应渲染'查询范围'的组件
		this.renderRefPathEle = this.renderRefPathEle.bind(this);//根据传进来的url渲染对应的参照组件
		this.handleCheckbox = this.handleCheckbox.bind(this);//计算小计	包含下级复选框的点击事件
		this.secondModalSelect = this.secondModalSelect.bind(this);//第二层弹框的选择框
		this.renderNewRefPath = this.renderNewRefPath.bind(this);//渲染第二层弹框内容的参照



		this.handleDateChange = this.handleDateChange.bind(this);//日期: 范围选择触发事件
		this.onChangeShowzerooccur = this.onChangeShowzerooccur.bind(this);//无发生不显示;本期无发生无余额不显示
		this.handleSortOrShowStyle = this.handleSortOrShowStyle.bind(this);//多核算账簿排序方式/多核算账簿显示方式
		this.handleDateRadio = this.handleDateRadio.bind(this);//会计期间/日期的单选框事件


		this.handleSelectChange = this.handleSelectChange.bind(this);//<Select>的onChange事件统一触发事件
		this.renderLevelOptions = this.renderLevelOptions.bind(this);//渲染级次的<NCOption>
		this.renderReferEle = this.renderReferEle.bind(this);//渲染 制单人；出纳人；审核人；记账人
		this.handleVocherChange = this.handleVocherChange.bind(this);// 凭证号输入；不能输入小数
		this.handleValueChange = handleValueChange.bind(this);
		this.searchId = '20028005query';
        this.handleNumberInput = handleNumberInput.bind(this);
	}

	static defaultProps = {
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		// accAssItems: [],
    };
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                dateInputPlaceholder: json['20028005-000000'],/* 国际化处理： 选择日期*/
                currtype: json['20028005-000001'],      //选择的币种/* 国际化处理： 本币*/
			    currtypeName: json['20028005-000001'],  //选择的币种名称/* 国际化处理： 本币*/
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20028005', 'dategroup', 'checkbox', 'subjectversion', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
        this.renderTableFirstData(10);
    }
    handleVocherChange(value, param){// 凭证号输入；不能输入小数

        let result = value;
        let arrResult = result.split('');//输入值变为数组
        let code = arrResult[arrResult.length - 1];//最后一次输入的元素
        let codeValue = code && code.toString().charCodeAt(0);
        if(codeValue < 48 || codeValue > 57) {//不是数字的输入
            arrResult.splice(arrResult.length - 1);
        }

        this.setState({
            [param]: arrResult.join('')
        });
	}
    renderLevelOptions(num){

    	let optionArr = [];
    	for(let i=0; i<num; i++){
            optionArr.push(<NCOption value={i+1} key={i}>{i+1}</NCOption>);
		}
        return optionArr;
	}
    handleSelectChange(value, param){//<Select>的onChange事件统一触发事件

    	this.setState({
			[param]: String(value)
		})
	}
    handleDateRadio(value, param){//会计期间/日期的单选框事件

    	this.setState({
			[param]: value
		})
	}
	handleSortOrShowStyle(value, param){//多核算账簿显示方式/多核算账簿排序方式触发事件

		this.setState({
			[param]: value
		})
	}
    handleDateChange(value){//日期: 范围选择触发事件

    	this.setState({
            begindate: value[0],
            enddate: value[1],
			rangeDate: value
		})
	}
	handleOppositeSubject = () => {//对方科目取小值的处理方法
        let { tableSourceData } = this.state;
        let paramFlag = []
        tableSourceData.map((item, indx) => {
            if(item.selectCell && item.selectCell.name === this.state.json['20028005-000008']){/* 国际化处理： 对方科目*/
                paramFlag.push('Y')
            }
        });
        if(paramFlag.includes('Y')){
            this.setState({
                chooseoppositeSide: false
            })
		}else{
            this.setState({
                oppositeSide: false,
                chooseoppositeSide: true
            })
		}
	}
    handleSearchObj(values, record){//查询对象选择具体科目
        let value = JSON.parse(values)
        // accele:"Y"小记
        // includesub:"N"包含下级
        // showLocal:"N"显示表头
    	let { tableSourceData, selectRow } = this.state;
        let originData = this.findByKey(record.key, tableSourceData);
        originData.selectCell = value;
        record.selectCells = [];
        record.selectRange = [];
        record.accele = 'N';
        record.includesub = 'N';
        record.showLocal = 'N';
		this.handleOppositeSubject();
        if(value.refpath ){
            originData.type = 'referpath';
            originData.searchRange = value.refpath;
		}else if(value.datatype === '1'){//字符
            originData.type = 'string';
        } else if(value.datatype === '4'){//整数
            originData.type = 'integer';
        } else if(value.datatype === '31'){//数值(小数)
            originData.type = 'number';
        } else if(value.datatype === '32'){//布尔
            originData.type = 'boolean';
        } else if(value.datatype === '33'){//日期
            originData.type = 'date'
        } else if(value.datatype === '34'){//日期+时分秒时间
            originData.type = 'dateTime'
        } else if(value.attr){
            originData.selectCellAttr = [...value.attr];
            originData.type = 'modal'
		} else{
            originData.type = ''
		}
		let newSelectRow = [...selectRow];
        newSelectRow[record.key] = record;
    	this.setState({
            tableSourceData,
			selectRow: newSelectRow
		})
	}
    handleCheckbox(key, record){//计算小计	包含下级复选框的点击事件
    	let {tableSourceData} = this.state;
        record[key] = record[key] === 'N' ? 'Y' : 'N';
        this.setState({
            tableSourceData
		})

	}
    secondModalSelect(record, index){//第二层弹框的复选框

    	let {tableSourceData} = this.state;
        let newSelect = !record.select;
        record.select = newSelect;
        tableSourceData[record.itemKey].selectCellAttr[index] = record;
        this.setState({tableSourceData})
	}
    findByKey(key, rows) {
        let rt = null;
        let self = this;
        rows.forEach(function(v, i, a) {
            if (v.key == key) {
                rt = v;
            }
        });
        return rt;
    }
    dateRefChange = (key, value, record) => {
        let length = record.selectCell.inputlength;
        let exeReg = new RegExp("^.{0," + length + "}$");
        let result = exeReg.exec(value);
        if(result && key === 'stringRefValue'){
            record.selectRange = result[0]
            this.setState({
                [key]: result[0]
            })
        }else {
            record.selectRange = value
            this.setState({
                [key]: value
            })
        }
    }
    numberRefChange = (key, value, record, param) => {
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectRange = this.state[key], 0)

    }
    renderSearchRange(record){//根据'查询对象'的选择来对应渲染'查询范围'的组件
    	switch(record.type){
			case "referpath":
				let refPathEle = this.renderRefPathEle(record);
				return refPathEle;
            case 'date':
                return (
                    <DatePicker
                        format={format}
                        value={this.state.assistDateValue}
                        placeholder={dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateValue', value, record)}
                    />
                );
            case 'string':
                return <Input
                    value={this.state.stringRefValue}
                    onChange={(value) => this.dateRefChange('stringRefValue', value, record)}
                />;
            case 'number':
                return <Input
                    value={this.state.numberRefValue}
                    onChange={(value) => this.numberRefChange('numberRefValue', value, record, 'assistBalance') }
                />;
            case 'integer'://整数
                return <Input
                    value={this.state.numberRefValueInteger}
                    onChange={(value) => this.numberRefChange('numberRefValueInteger', value, record, 'assistBalanceInteger') }
                />;
            case 'boolean'://布尔
                return <Select
                    showClear={false}
                    value={this.state.booleanValue}
                    className='search-boolean'
                    onChange={(value)=>{
                        this.dateRefChange('booleanValue', value, record)
                    }}
                >
                    <NCOption value="Y">{this.state.json['20028005-000027']}</NCOption>{/* 国际化处理： 是*/}
                    <NCOption value="N">{this.state.json['20028005-000028']}</NCOption>{/* 国际化处理： 否*/}
                    <NCOption value="">{this.state.json['20028005-000029']}</NCOption>{/* 国际化处理： 空*/}
                </Select>;
            case 'dateTime'://日期+时分秒
                return (
                    <DatePicker
                        showTime={true}
                        format={Timeformat}
                        value={this.state.assistDateTimeValue}
                        placeholder={dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateTimeValue', value, record)}
                    />
                );
			case 'modal':
                let attrObj = record.selectCellAttr;
                let { tableSourceData } = this.state;
                let selectObj = [];
                attrObj.map((item)=>{
                	if(item){
                        item.itemKey = record.key;
					}
					if(item && item.selectName){
                        selectObj += item.selectName + ','
					}
				})
				return (
                    <div style={{display: 'flex'}}>
                        <Input
                            style={{border: 'none'}}
							placeholder={record.selectCell && record.selectCell.name}
							value={selectObj}
						/>
                        <div className='ellipsisContainer'
                             onClick={() => this.closeNewModal(record)}
                        >
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                        </div>
                    </div>
				);
			default:
				return <Input disabled/>;
		}
	}
    renderNewRefPath(record){
        let mybook;

        if(record.refpath){
            let objKey = record.attrclassid;
            if(!this.state[objKey]){//undefined
                {createScript.call(this,record.refpath+".js",objKey)}

            }else {
                mybook = (
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state[objKey] ? (this.state[objKey])(
                                    {
                                        value:{refname: record.selectName && record.selectName[0], refpk:  record.refpk && record.refpk[0]},
                                        isMultiSelectedEnabled: true,
                                        queryCondition: () => {
                                            return {
                                                "pk_accountingbook": this.state.accountingbook[0].pk_accountingbook ? this.state.accountingbook[0].pk_accountingbook : '',
                                                // "versiondate":currrentDate,
                                            }
                                        },
                                        onChange: (v) => {
                                            let {tableSourceData} = this.state;
                                            let refpkArr = [];
                                            let selectName = [];
                                            v.forEach((item) => {
                                                refpkArr.push(item.refpk);
                                                selectName.push(item.refname)
                                            })
                                            let pk_checkvalue = refpkArr.join(',')
                                            record.pk_checkvalue = pk_checkvalue;
                                            record.selectName = selectName;
                                            record.refpk = refpkArr;
                                            this.setState({
                                                tableSourceData
                                            })
                                        }
                                    }
                                ) : <div/>
                            }
                        </Col>
                    </Row>
                );
            }
		}

        return mybook;
	}
	renderRefPathEle(record){//根据传进来的url渲染对应的参照组件
    	//带有参照的
		let mybook;
		let objKey = record.key + record.selectCell.pk_checktype;
		if(!this.state[objKey]){//undefined
			{createScript.call(this,record.selectCell.refpath+".js",objKey)}

		}else{
			let pkOrgParam = this.state.unitValueParam; // this.state.accountingbook[0].nodeData ? this.state.accountingbook[0].nodeData.pk_corp : this.state.accountingbook[0].pk_corp;//this.state.unitValueParam;
            let options = {
                value: record.selectCells,
                isMultiSelectedEnabled:true,
                "isShowDisabledData": true,
                queryCondition:() => {
                    return {
                        "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                        "pk_org": pkOrgParam,
                        "isDataPowerEnable": "Y",
                        "DataPowerOperationCode" : 'fi',
                        "dateStr": this.state.isversiondate ? this.state.versiondate :this.state.busiDate
                    }
                },
                onChange: (v)=>{
                    let {tableSourceData} = this.state;
                    let refpkArr = [];
                    let selectCell = [];
                    v.forEach((item)=>{
                        let cellObj = {};
                        cellObj.pk_checkvalue = item.refpk;
                        cellObj.checkvaluecode = item.refcode;
                        cellObj.checkvaluename = item.refname;
                        refpkArr.push(cellObj);
                        selectCell.push(item);
                    })
                    let pk_checkvalue = [...refpkArr];
                    record.selectRange = pk_checkvalue;
                    record.selectCells = selectCell;
                    this.setState({
                        tableSourceData
                    })
                }
            };
            let newOPtions = {}
            if(record.selectCell.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.selectCell.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                newOPtions = {
                    ...options,
                    isShowUnit:true,
                    "unitValueIsNeeded":false,
                    "isShowDimission":true,
                    queryCondition:() => {
                        return {
                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                            "pk_org": pkOrgParam,
                            "isDataPowerEnable": "Y",
                            "DataPowerOperationCode" : 'fi',
                            "dateStr": this.state.isversiondate ? this.state.versiondate :this.state.busiDate,
                            "busifuncode":"all",
                            isShowDimission:true
                        }
                    },
                    unitProps:{
                        refType: 'tree',
                        refName: this.state.json['20028005-000014'],/* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:this.state.json['20028005-000014'],refpk:'root'},/* 国际化处理： 业务单元*/
                        placeholder:this.state.json['20028005-000014'],/* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[this.state.json['20028005-000015'], this.state.json['20028005-000016']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false
                    },
                    unitCondition:{
                        pk_financeorg: pkOrgParam,
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    },
                }
            }else if(record.selectCell.classid && record.selectCell.classid.length === 20){
                newOPtions = {
                    ...options,
                    pk_defdoclist: record.selectCell.classid
                }
            }else {
                newOPtions = {
                    ...options
                }
            }
            mybook =  (
				<Row className="tableselectItem">
					<div>
						{
							this.state[objKey]?(this.state[objKey])(newOPtions):<div/>
						}
					</div>
				</Row>
			);
		}
		return mybook;
	}
    renderTableFirstData(length){
    	let firstData = [];
    	for(let i=0; i<length; i++){
    		let obj = {};
    		obj.searchObj = [];
            obj.searchRange = '';
            obj.showLocal = 'N';//显示位置
            obj.accele = 'N';//小计
            obj.includesub = 'N',//包含下级
			obj.key = i
            firstData.push(obj);
		}
		let deepCloneData = deepClone(firstData);
    	this.setState({
            tableSourceData: [...deepCloneData]
		})
	}

	handleTableSelect(){

	}
	componentDidMount() {
        setTimeout(() => getReferDetault(this, true, {
            businessUnit,
            getCheckContent
        }, 'multiAnalyTable'),0);
        setTimeout(() => initTemplate.call(this,this.props), 0)
	}

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}




	getCurrtypeList = () => {//获取"币种"接口
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {"localType":"1", "showAllCurr":"1"};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;

		        if(success){
					self.setState({
						currtypeList: data,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


    handleCurrtypeChange = (keyParam, value) => {
        this.setState({
            [keyParam]: value,
        });
    }


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(v) {

		if (!this.state.accountingbook || !this.state.accountingbook[0]) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,	
			year: '',		
		};
		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
                getSubjectVersion(self);

		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        		// versiondate: '2018-5-31', //data.versiondate,
		        	},)

                    let versionUrl = '/nccloud/gl/glpub/accountinfoquery.do';//科目版本查询;级次
                    let versionSelect = {
                        pk_accountingbook: v[0].refpk,
                    }
                    ajax({
                        url:versionUrl,
                        data:versionSelect,
                        success: function(response){
                            const { success } = response;
                            if (success) {
                                if(response.data){

                                    self.setState({
                                        versionDateArr: [...response.data.versiondate],
                                        level: response.data.accountlvl
									})
                                }
                            }
                        },
						error: function(error){
                        	toast({
								content: error.message,
								color: 'danger'
                        	})
						}
                    });

                    // gl.accountrep.multianalysisqueryobject
                    let newUrl = '/nccloud/gl/accountrep/multianalysisqueryobject.do';//获取辅助核算内容
                    let newData = {
                        "pk_accountingbook":v[0].refpk,
                        "versiondate": '2018-5-31',//self.state.versiondate,
                    };
                    ajax({
                        url:newUrl,
                        data:newData,
                        success: function(response){
                            const { success } = response;
                            //渲染已有账表数据遮罩
                            if (success) {
                                if(response.data){
                                    response.data.map((item) => {
                                    	if(item.attr){
                                            item.attr.map((cell) => {
                                                cell.select = false
											})
										}
									})

									let newTableFirstData = [...self.state.tableSourceData];
                                    newTableFirstData.forEach((item, index) => {
                                        item.searchObj = response.data;
									})

                                    self.setState({
                                        tableSourceData: newTableFirstData
                                    })
                                }
                            }
                        }
                    });
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {

		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: '2018-5-31'// value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    //(e);
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化/本期无发生无余额不显示;按各核算账簿显示科目名称/按各核算账簿查询辅助核算
	onChangeShowzerooccur(value, param) {
    	//('onChangeShowzerooccur>', value, param)
		this.setState({
			[param]: value
		});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}
	//显示上级科目复选框变化
	onChangeupLevelSubject(e) {
		this.setState({upLevelSubject: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}
	handleRadioSort(param){
		//('handleRadioSort>', param);
		this.setState({
			selectedSort: param
		})
	}
	handleShowType(param){
		this.setState({
			selectedShowType: param
		})
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
	onChangeDoubleCell(param){
		this.setState({
			doubleCell: !this.state.doubleCell
		})
	}
	changeProjectStyle(param){//所有末级   表外科目选择
		if(param === 'allLast'){
			this.setState({
				allLast: !this.state.allLast
			})
		}else{
			this.setState({
				outProject: !this.state.outProject
			})
		}
	}

	handleGTypeChange =() =>{
		
	};

    shouldComponentUpdate(nextProps, nextState){
		return true;
	}



    outInputChange(param){
    	//('outInputChange>', param);
    	let nameArr = [];
        param.forEach((item) => {
            nameArr.push(item.refname);
		})
		this.setState({
            innerSelectName: [...nameArr]
		})
	}
    sureHandle(param){
    	//('sureHandleParam:', param);
    	let SelectCell = [];
        // param.forEach((item) => {
        	// if(item.pk_checkvalue){
         //        SelectCell.push(item);
		// 	}
		// });
        //('sureHandleParam:2', SelectCell);
        this.setState({
			innerSelectData: [...SelectCell]
		})
    	this.closeNewModal(param);
	}
    closeNewModal(param){
    	//('state:::',param, this.state.showNewModal)
		this.setState({
            showNewModal: !this.state.showNewModal,
			page: param.key
		})
    }

    balanceMoney(value, param){
        let regExp = /^(\-)?\d*\.{0,1}\d{0,4}$/
        let result = regExp.exec(value)
        if(result){
            this.setState({
                [param]: result[0]
            })
        }
    }

	/****
	 * param: String
	 * 根据传入的参数渲染对应的参照组件
	 * ***/
	renderReferEle(param){
    	//('renderReferEle>', param, this.state[param], this.state.param);
    	let referUrl = "gl/refer/voucher/OperatorDefaultRef/index.js" //
        if(!this.state[param]){//undefined
            {createScript.call(this,referUrl,param)}
        }else{
            //('myattrcodeFn', this.state, this.state[param], this.state.accountingbook[0] && this.state.accountingbook[0].refname);
            return (
                <Row>
                    <Col xs={12} md={12}>
                        {
                            this.state[param]?(this.state[param])(
                                {
                                    value:{refname: this.state.accountingbook[0] && this.state.accountingbook[0].refname, refpk: this.state.accountingbook[0] && this.state.accountingbook[0].refpk},
                                    queryCondition:() => {
                                        // return {
                                        // 	"pk_accountingbook": self.state.pk_accountingbook.value
                                        // }
                                    },
                                    onChange: (v)=>{
                                        let self = this;
                                        //('render:',param, v, this);
                                    }

                                }
                            ):<div/>
                        }
                    </Col>
                </Row>
			)
        }
	}
    renderModalList = () => {
        this.columns = [
            {
                title: (<div fieldid='searchObj'>{this.state.json['20028005-000002']}</div>), ///* 国际化处理： 核算类型*/
                dataIndex: "searchObj",
                key: "searchObj",
                render: (text, record, index) => {
                    //('columns>>>>', text, record);
                    let renderArr = [];
                    text.map((item, index) => {
                        renderArr.push(<NCOption value={JSON.stringify(item)} key={index}>{item.name}</NCOption>)
                    })
                    return (
                        <Select
                            showClear={false}
                            fieldid='searchObj'
                            value={record.selectCell && record.selectCell.name}
                            onChange = {(value) => this.handleSearchObj(value, record)}
                        >
                            {
                                renderArr
                            }
                        </Select>
                    )
                }
            },
            {
                title: (<div fieldid='searchRange'>{this.state.json['20028005-000003']}</div>),/* 国际化处理： 核算内容*/
                dataIndex: "searchRange",
                key: "searchRange",
                render: (text, record,index) => {
                    //('searchRangetext:::',record, record.type);
                    let renderEle = this.renderSearchRange(record);
                    return renderEle;
                }
            },
            {
                title: (<div fieldid='showLocal'>{this.state.json['20028005-000004']}</div>),/* 国际化处理： 显示表头*/
                dataIndex: "showLocal",
                key: "showLocal",
                width : 65,
                render: (text, record) => {
                    //('showLocal>>>>', text, record)
                    return (
                        <Select
                            showClear={false}
                            fieldid='showLocal'
                            defaultValue = 'N'
                            value={record.showLocal}
                            onChange={(value) => {
                                //('showLocal:>>>',value);
                                record.showLocal = value;
                                record.accele = 'N';
                                this.setState({})
                            }}>
                            <NCOption value='Y'>{this.state.json['20028005-000025']}</NCOption>{/* 国际化处理： 表头*/}
                            <NCOption value='N'>{this.state.json['20028005-000026']}</NCOption>{/* 国际化处理： 表体*/}
                        </Select>
                    )
                }
            },
            {
                title: (<div fieldid='accele'>{this.state.json['20028005-000005']}</div>),/* 国际化处理： 计算小计*/
                dataIndex: "accele",
                key: "accele",
                width : 65,
                render: (text, record) => {
                    return <div fieldid='accele'><Checkbox
                        checked={record.accele === 'N' ? false : true}
                        disabled={record.selectCell ? (record.showLocal === 'Y' ? true : false) : true}
                        onChange={() => {
                            //('acceleChange:',record);
                            this.handleCheckbox('accele',record)
                        }}
                    /></div>
                }
            },
            {
                title: (<div fieldid='includesub'>{this.state.json['20028005-000006']}</div>),/* 国际化处理： 包含下级*/
                dataIndex: "includesub",
                key: "includesub",
                width : 65,
                render: (text, record) => {
                    return <div fieldid='includesub'><Checkbox
                        checked={record.includesub === 'N' ? false : true}
                        disabled={record.selectCell ? ((record.selectCell.name === this.state.json['20028005-000007'] || record.selectCell.name === this.state.json['20028005-000008']) ? true :false) : true}/* 国际化处理： 会计科目,对方科目*/
                        onChange={() => {
                            //('includesub:', record);
                            this.handleCheckbox('includesub',record)
                        }}
                    /></div>
                }
            }
        ];
        this.secondColumn = [
            {
                title: this.state.json['20028005-000009'],/* 国际化处理： 选择*/
                dataIndex: "select",
                key: "select",
                render: (text, record, index) => {
                    //('secondColumn>', text, record)
                    return <Checkbox
                        checked={record.select}
                        onChange={() => {
                            //('acceleChange:',record);
                            record.index = index;
                            this.secondModalSelect(record, index);
                        }}
                    />
                }
            },
            {
                title: this.state.json['20028005-000010'],/* 国际化处理： 查询属性*/
                dataIndex: "name",
                key: "name"
            },
            {
                title: this.state.json['20028005-000011'],/* 国际化处理： 查询值*/
                dataIndex: "refpath",
                key: "refpath",
                render: (text, record) => {
                    //('refpath>>>>', text, record);
                    let renderSecondEle = this.renderNewRefPath(record);
                    return renderSecondEle;
                }
            }
        ];
        let { show, title, closeButton, icon, isButtonWhite, isButtonShow}= this.props;
        return (
            <div className='right_query'>
                <div className='query_body1 multianalytablebody'>
                    <div className='query_form'>
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel isRequire={true} labelname={this.state.json['20028005-000030']}/>{/* 国际化处理： 核算账簿*/}
                    </Col>
                    <Col md={10} sm={10}>
                        <div className="book-ref">
                            {/*{mybook}*/}
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: true,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit ,
                                        getCheckContent: getCheckContent,
                                        renderTableFirstData: () => this.renderTableFirstData(10)
                                    },
                                    'multiAnalyTable'
                                )
                            }
                        </div>
                    </Col>
                </Row>

                {/*启用科目版本：*/}
                <SubjectVersion
                    checked = {this.state.isversiondate}//复选框是否选中
                    data={this.state.versionDateArr}//
                    value={this.state.versiondate}
                    disabled={!this.state.isversiondate}
                    renderTableFirstData = {() => this.renderTableFirstData(9)}
                    handleValueChange = {(key, value) => this.handleValueChange(key, value, 'multiAnalyTable', getCheckContent)}
                />

                <div className="modalTable">
                    <Table
                        columns={this.columns}
                        data={this.state.tableSourceData}
                    />
                </div>

                {/*会计期间*/}
                <DateGroup
                    selectionState = {this.state.selectionState}
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook[0]}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                />

                {/*凭证类别： 凭证号：*/}
                <Row className='rowCell'>

                    <Col md={2} >
                        <FormLabel labelname={this.state.json['20028005-000017']}/>{/* 国际化处理： 凭证类别*/}
                    </Col>
                    <Col md={4} >
                        <VoucherRefer
                            fieldid='voucherRefer'
                            value={{refname: this.state.voucherRefer.refname, refpk: this.state.voucherRefer.refpk}}
                            disabled={this.state.voucherStatus}
                            queryCondition={ () => {
                                return {
                                    pk_accountingbook: this.state.accountingbook && this.state.accountingbook[0].refpk,
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi',
                                    pk_org: this.state.accountingbook && this.state.accountingbook[0].refpk,
                                }
                            }}
                            onChange={(value) => {
                                //(this.state.json['20028005-000017'], value);/* 国际化处理： 凭证类别*/
                                // madeBillPeople: {refname: ''}, //制单人选择的参照数据
                                this.setState({
                                    voucherRefer: {...value}
                                })
                            }}
                        />
                    </Col>
                    <Col md={2}>
                        <FormLabel labelname={this.state.json['20028005-000031']}/>{/* 国际化处理： 凭证号*/}
                    </Col>
                    <Col md={4} style={{display: 'flex'}}>
                        <Input
                            fieldid='vocherInputLeft'
                            value={this.state.vocherInputLeft}
                            onChange={(value) => {this.handleVocherChange(value, 'vocherInputLeft')}}
                        />
                        <span className='nc-theme-form-label-c' style={{marginRight: '8px'}}>-</span>
                        <Input
                            fieldid='vocherInputRight'
                            value={this.state.vocherInputRight}
                            onChange={(value) => {this.handleVocherChange(value, 'vocherInputRight')}}
                        />
                    </Col>
                </Row>

                {/*制单人: 来源系统：*/}
                <Row className='rowCell'>
                    <Col md={2} >
                        <FormLabel labelname={this.state.json['20028005-000018']}/>{/* 国际化处理： 制单人*/}
                    </Col>
                     <Col md={4} >
                        <ReferEle
                            fieldid='madeBillPeople'
                            isMultiSelectedEnabled={true}
                            value={this.state.madeBillPeople}
                            queryCondition={ () => {
                                return {
                                    pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                    user_Type : '0',
                                    GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
                            onChange={(value) => {
                                //(this.state.json['20028005-000018'], value);/* 国际化处理： 制单人*/
                                // madeBillPeople: {refname: ''}, //制单人选择的参照数据
                                this.setState({
                                    madeBillPeople: [...value]
                                })
                            }}
                        />
                    </Col>
                     <Col md={2}  >
                        <FormLabel labelname={this.state.json['20028005-000032']}/>{/* 国际化处理： 来源系统*/}
                     </Col>
                     <Col md={4}  >
                         <CtlSysRef
                            fieldid='sourceSystem'
                             isMultiSelectedEnabled={true}
                             value={this.state.sourceSystem}
                             queryCondition={ () => {
                                 return {
                                     "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                                     "isDataPowerEnable": 'Y',
                                     "DataPowerOperationCode" : 'fi'
                                 }
                             }}
                             onChange={(value) => {
                                 //(this.state.json['20028005-000018'], value);/* 国际化处理： 制单人*/
                                 // checkPeople: {refname: ''},    //审核人选择的参照数据
                                 this.setState({
                                     sourceSystem: [...value]
                                 })
                             }}
                         />
                      </Col>
                </Row>

                {/*出纳人: 审核人*/}
                <Row className='rowCell'>

                    <Col md={2} >
                        <FormLabel labelname={this.state.json['20028005-000019']}/>{/* 国际化处理： 出纳人*/}
                    </Col>
                    <Col md={4} >
                        <ReferEle
                            fieldid='cashierPeople'
                            isMultiSelectedEnabled={true}
                            value={this.state.cashierPeople}
                            queryCondition={ () => {
                                return {
                                    pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                    user_Type : '2',
                                    GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
                            onChange={(value) => {
                                //(this.state.json['20028005-000019'], value);/* 国际化处理： 出纳人*/
                                // cashierPeople: {refname: ''},  //出纳人选择的参照数据
                                this.setState({
                                    cashierPeople: [...value]
                                })
                            }}
                        />
                    </Col>
                        <Col md={2} >
                            <FormLabel labelname={this.state.json['20028005-000020']}/>{/* 国际化处理： 审核人*/}
                        </Col>
                         <Col md={4} >
                            <ReferEle
                                fieldid='checkPeople'
                                isMultiSelectedEnabled={true}
                                value={this.state.checkPeople}
                                queryCondition={ () => {
                                    return {
                                        pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                        user_Type : '1',
                                        GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi'
                                    }
                                }}
                                onChange={(value) => {
                                    //(this.state.json['20028005-000020'], value);/* 国际化处理： 审核人*/
                                    // checkPeople: {refname: ''},    //审核人选择的参照数据
                                    this.setState({
                                        checkPeople: [...value]
                                    })
                                }}
                            />
                        </Col>

                </Row>

                {/*记账人:摘要:*/}
                <Row className='rowCell'>
                    <Col md={2} >
                        <FormLabel labelname={this.state.json['20028005-000021']}/>{/* 国际化处理： 记账人*/}
                    </Col>
                        <Col md={4} >
                        <ReferEle
                            fieldid='chargePeople'
                            isMultiSelectedEnabled={true}
                            value={this.state.chargePeople}
                            queryCondition={ () => {
                                return {
                                    pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                    user_Type : '3',
                                    GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
                            onChange={(value) => {
                                //(this.state.json['20028005-000021'], value);/* 国际化处理： 记账人*/
                                // chargePeople: {refname: ''},   //记账人选择的参照数据
                                this.setState({
                                    chargePeople: [...value]
                                })
                            }}
                        />
                    </Col>
                    <Col md={2} >
                        <FormLabel labelname={this.state.json['20028005-000033']}/>{/* 国际化处理： 摘要*/}
                    </Col>
                    <Col md={4} >
                        <Input
                            fieldid='abstract'
                            value={this.state.abstract}
                            onChange={(value) => {
                                this.handleSelectChange(value, 'abstract')
                            }}
                        />
                    </Col>
                </Row>

                {/*余额方向*/}
                <Row className='rowCell'>
                    <Col md={2}  >
                        <FormLabel labelname={this.state.json['20028005-000034']}/>{/* 国际化处理： 余额方向*/}
                    </Col>
                    <Col md={3}  >
                        <Select
                            showClear={false}
                            fieldid='balanceori'
                            value={this.state.balanceori}
                            // style={{ width: 80, marginRight: 6 }}
                            onChange={
                                (value) => this.handleSelectChange(value, 'balanceori')
                            }
                        >
                            <NCOption value={'-1'} key={'-1'} >{this.state.json['20028005-000035']}</NCOption>{/* 国际化处理： 双向*/}
                            <NCOption value={'0'} key={'0'} >{this.state.json['20028005-000036']}</NCOption>{/* 国际化处理： 借*/}
                            <NCOption value={'1'} key={'1'} >{this.state.json['20028005-000037']}</NCOption>{/* 国际化处理： 贷*/}
                            <NCOption value={'3'} key={'3'} >{this.state.json['20028005-000038']}</NCOption>{/* 国际化处理： 平*/}
                        </Select>
                    </Col>
                    <Col md={1}></Col>
                    <Col md={6} >
                        <Col md={5} >
                        <Input
                            fieldid='balanceMoneyLeft'
                            value={this.state.balanceMoneyLeft}
                            onChange={(value) => this.balanceMoney(value, 'balanceMoneyLeft')}
                        />
                        </Col>
                        <Col md={2} style={{lineHeight: '36px'}}><p className='zhi nc-theme-form-label-c'>~</p></Col>
                    <Col md={5}>
                    <Input
                        fieldid='balanceMoneyRight'
                        value={this.state.balanceMoneyRight}
                        onChange={(value) => this.balanceMoney(value, 'balanceMoneyRight')}
                    />
                    </Col>
                    </Col>
                </Row>


                {/*包含凭证：*/}
                <CheckBoxCells
                    paramObj = {{
                        "noChargeVoucher": {//未记账凭证
                            show: true,
                            disabled: this.state.disabled,
                            checked: this.state.includeuntally,
                            onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                        },
                        "errorVoucher": {//错误凭证
                            show: true,
                            checked: this.state.includeerror,
                            onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                        },
                        "harmToBenefitVoucher": {//损益结转凭证
                            show: true,
                            checked: this.state.includeplcf,
                            onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                        },
                        "againClassifyVoucher": {//重分类凭证
                            show: true,
                            checked: this.state.includerc,
                            onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                        },
                    }}
                />

                {/*币种*/}
                {renderMoneyType(this.state.currtypeList,this.state.currtypeName, this.handleCurrtypeChange, this.state.json)}

                {/*显示属性*/}
                <Row className='rowCell'>
                    <Col md={2}  >
                        <FormLabel labelname={this.state.json['20028005-000039']}/>{/* 国际化处理： 显示属性*/}
                    </Col>
                    <Col md={4} >
                    <Checkbox
                        colors="dark"
                        className="mycheck"
                        checked={this.state.showzerooccur}
                        onChange={(value) => this.onChangeShowzerooccur(value, 'showzerooccur')}
                    >
                        {this.state.json['20028005-000040']} {/* 国际化处理： 只查调整期凭证*/}
                    </Checkbox>
                    </Col>
                    <Col md={4}  >
                        <Checkbox
                            colors="dark"
                            className="mycheck"
                            checked={this.state.ckNotShowZeroOccurZeroBalance}
                            onChange={(value) => this.onChangeShowzerooccur(value, 'ckNotShowZeroOccurZeroBalance')}
                        >
                            {this.state.json['20028005-000041']} {/* 国际化处理： 无发生不显示*/}
                        </Checkbox>
                    </Col>
                </Row>
                <Row className='rowCell'>
                    <Col md={2} xs={2} sm={2} >
                        <FormLabel labelname={this.state.json['20028005-000039']}/>{/* 国际化处理： 显示属性*/}
                    </Col>
                    <Col md={10} xs={10} sm={10} >
                    <Checkbox
                        colors="dark"
                        className="mycheck"
                        id='oppositeSide'
                        checked={this.state.oppositeSide}
                        disabled={this.state.chooseoppositeSide}
                        onChange={
                            (value) => this.onChangeShowzerooccur(value, 'oppositeSide')}
                    >
                        {this.state.json['20028005-000042']} {/* 国际化处理： 对方科目取小值*/}
                    </Checkbox>
                    </Col>
                </Row>

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
                        this.state.json
                    )
                }

                {/*忽略日期选项*/}

                <NewModal
                    title= {this.state.json['20028005-000022']} //{/* 国际化处理： 属性选择*/}
                    record={this.state.selectRow[this.state.page]}
                    column={this.secondColumn}
                    showNewModal = {this.state.showNewModal}//控制设否显示
                    closeNewModal = {this.closeNewModal}
                    sureHandle = {this.sureHandle}
                    sureText = {this.state.json['20028005-000023']} ///* 国际化处理： 确定 *
                    cancleText = {this.state.json['20028005-000013']} ///* 国际化处理： 取消*/
                />
			</div>
            </div>
            </div>

		)
	}
    clickPlanEve = (value) => {
        //('clickPlanEve:???', 'value>',this, value, '>')
        this.state = deepClone(value.conditionobj4web.nonpublic);
        //('clickPlanEve:???12', this.state)
        this.setState(this.state)
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
        //('saveSearchPlan>>', this.state)
        return {...this.state}
    }
    clickSearchBtn = () => {//点击查询框的"查询"按钮事件
		let pk_accountingbook = [];
		let pk_unit = [];
		let moneyLeft = Number(this.state.balanceMoneyLeft);
		let moneyRight = Number(this.state.balanceMoneyRight);
		//('moneyyyy::', moneyLeft,  moneyRight, moneyLeft < moneyRight)
		if(moneyLeft > moneyRight){
			toast({
				content: this.state.json['20028005-000024'],/* 国际化处理： 余额范围查询条件信息输入有误(起始值大于终止值或是为负值)！*/
				color: 'warning'
			});
			return;
		}
		let lastAssvos = [];
		this.state.tableSourceData.forEach((item)=>{
			//('assvos>>>',item);
			let resultObj = {};
			let checkValue = [];
			if(item.selectCell && item.selectCell.name !==''  && !item.selectCellAttr){
				resultObj.name = item.selectCell.name;
				resultObj.pk_checktype = item.selectCell.pk_checktype;
				item.selectRange && Array.isArray(item.selectRange) && item.selectRange.map((item) => {
					checkValue.push(item.pk_checkvalue);
				})
				resultObj.pk_checkvalue = Array.isArray(item.selectRange) ? checkValue.join(',') : item.selectRange;
				resultObj.headele = item.showLocal;
				resultObj.accele = item.accele;
				resultObj.includesub = item.includesub;
				lastAssvos.push(resultObj)
			} else if(item.selectCell && item.selectCell.name !==''  && item.selectCellAttr){
				resultObj.name = item.selectCell.name;
				resultObj.pk_checktype = item.selectCell.pk_checktype;
				resultObj.attr = [];
				resultObj.headEle = item.showLocal;
				resultObj.accEle = item.accele;
				resultObj.includeSub = item.includesub;
				item.selectCellAttr.map((itemCell) => {
					if(itemCell.select){
						resultObj.attr.push(itemCell);
					}
				})
				lastAssvos.push(resultObj)
			}
		})
		//('lastAssvos>>', lastAssvos)
		if (Array.isArray(this.state.accountingbook)) {
			this.state.accountingbook.forEach(function (item) {
				pk_accountingbook.push(item.refpk)
			})
		}

		if (Array.isArray(this.state.buSecond)) {
			this.state.buSecond.forEach(function (item) {
				pk_unit.push(item.refpk)
			})
		}

		let madeBillPeopleArr = [];
		this.state.madeBillPeople.map((item) => {
			madeBillPeopleArr.push(item.refpk);
		})
		let cashierPeopleArr = [];
		this.state.cashierPeople.map((item) => {
			cashierPeopleArr.push(item.refpk);
		})
		let checkPeopleArr = [];
		this.state.checkPeople.map((item) => {
			checkPeopleArr.push(item.refpk);
		})
		let chargePeopleArr = [];
		this.state.chargePeople.map((item) => {
			chargePeopleArr.push(item.refpk);
		})
		//需要code：
		// pk_system: this.state.sourceSystem,  //来源系统
		let sourceSystemArr = [];
		this.state.sourceSystem.map((item) => {
			sourceSystemArr.push(item.refcode)
		})
		//('>>>>>/////???', pk_unit, madeBillPeopleArr,cashierPeopleArr,checkPeopleArr,chargePeopleArr,sourceSystemArr);
        //
        let data = {
            /*******核算账簿****/
            pk_accountingbook: pk_accountingbook,

            /*******科目版本****/
            //usesubjVersion: this.state.isversiondate,//是否勾选
            versiondate: this.state.isversiondate ? this.state.versiondate : this.state.busiDate, //this.state.versiondate,//科目版本

            /*******查询对象 table框的内容****/
            assvo: lastAssvos,

            /*******会计期间参数****/
            querybyperiod: this.state.selectionState,
            startyear: this.state.startyear,
            startperiod: this.state.startperiod,
            endyear: this.state.endyear,
            endperiod:   this.state.endperiod,
            /*******日期参数****/
            startdate: this.state.begindate,
            enddate: this.state.enddate,

            /*******凭证类别；凭证号；制单人****/
            pk_vouchertype: this.state.voucherRefer.refpk, //凭证类别
            vouchernofrom: this.state.vocherInputLeft,  //开始凭证号
            vouchernoto: this.state.vocherInputRight, //结束凭证号
            pk_prepared: madeBillPeopleArr,  //制单人 数组 》〉》〉》〉》〉》

            /*******币种；余额方向;出纳人****/
            pk_currtype: this.state.currtype,//币种
            balanceori: this.state.balanceori, //余额方向："双向/借/贷/平", //余额方向
            rangefrom: this.state.balanceMoneyLeft,
            rangeto: this.state.balanceMoneyRight,
            pk_casher: cashierPeopleArr,  //出纳人  数组

            /*******包含凭证****/
            includeuntally: this.state.includeuntally ? 'Y' : 'N',//未记账凭证
            includeerror: this.state.includeerror ? 'Y' : 'N',// 错误凭证
            includeplcf: this.state.includeplcf ? 'Y' : 'N',//损益结转凭证
            includerc: this.state.includerc ? 'Y' : 'N',//重分类凭证
            pk_checked: checkPeopleArr,  //审核人数组》〉》〉》〉》〉》〉》〉

            /*******来源系统****/
            pk_system: sourceSystemArr,  //来源系统
            onlyadjust: this.state.showzerooccur,  //只查调整期凭证  Y/N
            showzerooccur: this.state.ckNotShowZeroOccurZeroBalance, //无发生不显示 Y/N
            pk_tally: chargePeopleArr,          //记账人数组》〉》〉》〉》〉》〉》〉》
            /*******对方科目取小值；摘要 ****/
            oppsubjmin: this.state.oppositeSide ? 'Y' : 'N',  //对方科目取小值  Y/N
            explanation: this.state.abstract,  //摘要》〉》〉》〉》〉》〉》〉》〉》〉》

            /*******返回币种****/
            returncurr: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局

            appcode: this.state.appcode
        }
        
        let url = '/nccloud/gl/accountrep/checkparam.do'
        let flagShowOrHide;
        ajax({
            url,
            data,
            async: false,
            success: function (response) {
                flagShowOrHide = true
            },
            error: function (error) {
                flagShowOrHide = false
                toast({ content: error.message, color: 'warning' });
            }
        })
        if (flagShowOrHide == true) {
            this.props.onConfirm(data)
        } else if (flagShowOrHide == false) {
            return true;
        }
	}
	render() {
//('timerddd::',this.state, this.state.tableSourceData,this.state.level,  this.state.begindate,'..', this.state.enddate,'>>>>', this.state.rangeDate)
        let { search } = this.props;
        let { NCCreateSearch } = search;
        return <div>
            {
                NCCreateSearch(this.searchId, {
                    onlyShowSuperBtn:true,                       // 只显示高级按钮
                    replaceSuperBtn:this.state.json['20028005-000012'],                      // 自定义替换高级按钮名称/* 国际化处理： 查询*/
                    replaceRightBody:this.renderModalList,
                    hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
                    clickPlanEve:this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                    saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
                    oid:this.props.meta.oid,
                    clickSearchBtn: this.clickSearchBtn,  // 点击按钮事件
                    isSynInitAdvSearch: true,//渲染右边面板自定义区域
                })}
        </div>
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal;