/**
 * Created by liqiankun on 2018/7/04.
 * 管理报表 ---> 多维分析表
 */

import React, {Component} from 'react';

import { base, ajax, createPage, getMultiLang, gzip } from 'nc-lightapp-front';
const { NCDiv } = base;
import '../../css/index.less'
import HeadCom from '../../common/headSearch/headCom';
import { SimpleTable } from 'nc-report';
import QueryModal from './components/MainSelectModal';
import {tableDefaultData} from '../../defaultTableData';
import reportPrint from '../../../public/components/reportPrint';
import { toast } from '../../../public/components/utils';
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import {setData} from "../../common/withPageDataSimbletable";
import {searchById} from "../../common/modules/createBtn";
import {handleSimpleTableClick} from "../../common/modules/simpleTableClick";
import "../../../public/reportcss/firstpage.less";
import PageButtonGroup from '../../common/modules/pageButtonGroup'
import {rowBackgroundColor} from "../../common/htRowBackground";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import HeaderArea from '../../../public/components/HeaderArea';
import { queryRelatedAppcode, originAppcode } from '../../../public/common/queryRelatedAppcode.js';
class AssistPropertyBalance extends Component{
    constructor(props){
        super(props);

        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            showAddBtn:true,//控制联查按钮
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
            textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            dataWithPage: [], //存放请求回来的数据信息，用于分页操作
            firstPageDisable: true,
            lastPageDisable: true,
            paramObj: {},//存放回调的查询参数，用于翻页
            accountType: 'amountcolumn',//金额式
            "fiveLables": []
        }
        this.dataPage = 0; //存放请求回来的数据椰树信息
        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.handlePage = this.handlePage.bind(this);//页面处理事件
        // this.setData= this.setData.bind(this);// 整理表格所需要的数据格式
        this.queryData = this.queryData.bind(this);//请求数据的方法；
        this.renderFirstData = {};
        this.handleValueChange = handleValueChange.bind(this);
        // this.changeSelectStyle = this.changeSelectStyle.bind(this);//选择 '账簿格式'事件
        this.searchById = searchById.bind(this);
        this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                "fiveLables": [
                    {
                        title: json['20028005-000045'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20028005-000046'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20028005-000047'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20028005-000048'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20028005-000049'],
                        styleClass:"m-brief"
                    }
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20028005', 'publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        this.setState({
            flag: true,
        });
        let appceod = this.props.getSearchParam('c');
        this.searchById('20028005PAGE',appceod);
        this.props.button.setDisabled({
            print:true,
            linkdetail:true, directprint:true,
            saveformat:true,
            first: true, pre: true, next: true, last: true
        });
        this.props.button.setButtonVisible(['first', 'pre', 'next', 'last'], false)
    }
    handlePage(param, dataWithPage){//页面控制事件
        let dataLength = dataWithPage.data.length;
        let renderData = [];
        let obj = {};
        switch(param){
            case 'firstPage':
                this.dataPage = 0;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                //('firstPage:::', this.dataPage, obj);
                this.setState({
                    firstPageDisable: true,
                    lastPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    first: true, pre: true,
                    next: false, last: false
                });
                setData(this, obj);
                return;
            case 'nextPage':
                this.dataPage += 1;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                this.setState({
                    firstPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    first: false, pre: false,
                });
                if(this.dataPage === dataLength - 1){
                    this.setState({
                        lastPageDisable: true
                    })
                    this.props.button.setDisabled({
                        next: true, last: true
                    });
                }
                //('nextPage>>', this.dataPage, obj);
                setData(this, obj);
                return;
            case 'prePage':
                this.dataPage -=1;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;

                if(this.dataPage === dataLength - 2){
                    this.setState({
                        lastPageDisable: false,
                        selectRowIndex: 0
                    })
                    this.props.button.setDisabled({
                        next: false, last: false
                    });
                }
                if(this.dataPage === 0){
                    this.setState({
                        firstPageDisable: true,
                        selectRowIndex: 0
                    })
                    this.props.button.setDisabled({
                        first: true, pre: true
                    });
                }
                setData(this, obj);
                return;
            case 'lastPage':
                this.dataPage = dataLength - 1;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                this.setState({
                    lastPageDisable: true,
                    firstPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    first: false, pre: false,
                    next: true, last: true
                });
                setData(this, obj);
                return;
        }
    }
    queryClick(){//点击"查询"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    queryData(param){//请求数据方法；
        let self = this;
        this.props.button.setDisabled({ print:false, directprint:false, saveformat:false});
        // gl.accountrep.multianalysisquerybook
        let url = '/nccloud/gl/accountrep/multianalysisquerybook.do';
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, success } = response;
                //('succcss::', data, data.data[0].pagedata.length);
                self.setState({
                    showAddBtn: false//解除联查按钮的禁止
                });
                self.dataPage = 0;
                if(success){
                    if(data.data.length < 2){
                        self.setState({
                            dataWithPage: data,
                            lastPageDisable: true,
                            firstPageDisable: true
                        })
                        self.props.button.setDisabled({
                            first: true, pre: true,
                            next: true, last: true
                        });
                    }else{
                        self.setState({
                            lastPageDisable: false,
                            dataWithPage: data,
                        })
                        self.props.button.setDisabled({
                            next: false, last: false
                        });
                    }
                    self.setState({
                        typeDisabled: false,//账簿格式 默认是禁用的
                        selectRowIndex:0
                    })
                    self.renderFirstData.column = data.column;
                    self.renderFirstData.data = data.data[0];
                    self.renderFirstData.amountcolumn = data.amountcolumn;
                    self.renderFirstData.quantityamountcolumn = data.quantityamountcolumn;
                    setData(self, self.renderFirstData);
                }
            },
            error:function(error){
                toast({content: error.message, color: 'danger'});
            }
        })
    }
    modalSure(param){//点击弹框"确定"事件
        //('modalSure>>', param);
        this.dataPage = 0;
        this.setState({
            paramObj: {...param},
            firstPageDisable: true,
            lastPageDisable: true,
        })
        if(param.oppsubjmin === 'Y'){
            this.props.button.setButtonVisible({linkdetail: false})
        }else{
            this.props.button.setButtonVisible({linkdetail: true})
        }

        this.queryData(param);
    }

    //直接输出
    printExcel=()=> {
        let {textAlginArr, dataout} = this.state;
        let {cells, mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item, index) => {
            let emptyArr = [];
            item.map((list, _index) => {
                if (list) {
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo, dataArr, textAlginArr);
    }
    getDetailPort = (paramObj) => {//联查"明细"
        let url = paramObj.url; //'/nccloud/gl/accountrep/triaccquery.do';
        let data = this.getDataType(paramObj);
        this.jumpToDetail(data, paramObj);
        //('getDetailPort>>', data);
    }
    getDataType = (param) => {
        let selectRow = this.getSelectRowData()[0].link;

        let result = '';
        if(param.key === 'detail'){
            result = {
                ...this.state.paramObj,
                link: {...selectRow},
                pageindex: String(this.dataPage+1),
                "class": "nccloud.pubimpl.gl.account.multianalysis.MultiAnalysisBookLinkAssDetailParamTransfer"
            }
        }else if(param.key === 'totalAccount'){
            result = {
                ...this.state.paramObj,
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.account.multianalysis.MultiAnalysisBookLinkAssDetailParamTransfer"
            }
        }else{//联查"辅助"
            selectRow['key'] = 'relevanceAssist';
            result = {
                ...this.state.paramObj,
                link: {...selectRow},
                "pk_accassitems": [...this.state.selectRow],
                "class": "nccloud.pubimpl.gl.account.AccbalLinkAssbalParamTransfer",
                "from": "accbal" // 联查来源：(多主体)科目余额表accbal
            }
        }
        //('getDataType>', result, selectRow);
        return result;
    }
    jumpToDetail = (data, paramObj) => {
        let gziptools = new gzip();
        this.props.openTo(
            paramObj.jumpTo,
            {appcode: paramObj.appcode, status: gziptools.zip(JSON.stringify(data)),id: '12wew24',from:'multianaly'}
        )
    }
    //获取handsonTable当前选中行数据
    getSelectRowData=()=>{
        let selectRowData=this.refs.balanceTable.getRowRecord();
        return selectRowData;

    }
    handleSaveColwidth=()=>{
        let {json} = this.state;
        let info=this.refs.balanceTable.getReportInfo();
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.paramObj.appcode,info.colWidths,json, callBack);
    }
    handleLoginBtn = (obj, btnName) => {
        //('handleLoginBtn>>>', obj, btnName)
        if(btnName === 'refresh'){//1、刷新
            if (Object.keys(this.state.paramObj).length > 0) {
                this.modalSure(this.state.paramObj)
            }
        }else if(btnName === 'print'){//2、打印
            this.showPrintModal()
        }else if(btnName === 'linkdetail'){//3、联查明细
            this.getDetailPort({
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: '/gl/manageReport/assistDetailAccount/content/index.html',
                key: 'detail',
                appcode: queryRelatedAppcode(originAppcode.assdetail)
            })
        }else if(btnName === 'directprint'){//4、直接输出
            this.printExcel()
        }else if(btnName === 'saveformat'){//5、保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'first'){//首页
            this.handlePage('firstPage', this.state.dataWithPage)
        }else if(btnName === 'pre'){//上一页
            this.handlePage('prePage', this.state.dataWithPage)
        }else if(btnName === 'next'){//下一页
            this.handlePage('nextPage', this.state.dataWithPage)
        }else if(btnName === 'last'){//末页
            this.handlePage('lastPage', this.state.dataWithPage)
        }
    }
    firstCallback = () => {//首页
        this.handlePage('firstPage', this.state.dataWithPage)
    }
    preCallBack = () => {//上一页
        this.handlePage('prePage', this.state.dataWithPage)
    }
    nextCallBack = () => {//下一页
        this.handlePage('nextPage', this.state.dataWithPage)
    }
    lastCallBack = () => {//末页
        this.handlePage('lastPage', this.state.dataWithPage)
    }
    render(){
        //('sour:::',this.state, this.state.dataWithPage, this.state.dataout, this.state.paramObj);
        return(
            <div className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['20028005-000043']} /* 国际化处理： 多维分析表*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <QueryModal
                                    onConfirm={(datas) => {
                                        this.modalSure(datas);
                                    }}
                                />
                            </div>
                            {//按钮集合
                                this.props.button.createButtonApp({
                                    area: 'btnarea',
                                    buttonLimit: 3,
                                    onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                                })
                            }
                             
                        </div>
                    }
                    pageBtnContent = {
                        <PageButtonGroup
                            first={{disabled: this.state.firstPageDisable, callBack: this.firstCallback}}
                            pre={{disabled: this.state.firstPageDisable, callBack: this.preCallBack}}
                            next={{disabled: this.state.lastPageDisable, callBack: this.nextCallBack}}
                            last={{disabled: this.state.lastPageDisable, callBack: this.lastCallBack}}
                        />
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.fiveLables.map((items, index) => {
                                return(
                                    <HeadCom
                                        labels={items.title}
                                        key={items.title+index}
                                        content={this.state.dataWithPage.data && this.state.dataWithPage.data[this.dataPage]}
                                        lastThre = {this.state.dataWithPage.data && this.state.dataWithPage.headtitle}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.renderFirstData))
                                        }
                                        accountType = {this.state.accountType}
                                        isLong = {items.styleClass}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data = {this.state.dataout}
                        onCellMouseDown = {(e, coord, td) => {
                            this.whetherDetail('link');
                            this.rowBackgroundColor(e, coord, td)
                        }}
                    />
                </div>

            </div>
        )
    }
}


AssistPropertyBalance = createPage({})(AssistPropertyBalance)

ReactDOM.render(<AssistPropertyBalance />,document.querySelector('#app'));
