import React, { Component } from 'react';
import { base,ajax, createPage, promptBox, tableTotal, toast, getMultiLang } from 'nc-lightapp-front';
const { NCTable, NCCheckbox: Checkbox } = base;

import { initTemplate, tableButtonClick } from '../../events';
import { getTableHeight } from '../../../../../public/common/method.js'
import EditableCell from '../EditableCell'

class PrintTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkedAll: false, //是否全选
			// checkedRows: [], //选中数据
            checkedArray: [], //各行选中判断
            tableData: [],
            json: {},
            selectedRow: null
        }
    }
    componentWillMount() {
        let callback = (json) => {
			this.setState({
                json: json
            }, () => {
                initTemplate.call(this, this.props);
                this.printobjectTypes = {
                    'generalbook':this.state.json['20023085-000009'],/* 国际化处理： 三栏式总账*/
                    'detailbook':this.state.json['20023085-000010'],/* 国际化处理： 三栏式明细账*/
                    'diarybook':this.state.json['20023085-000011'],/* 国际化处理： 日记账*/
                    'assdetail':this.state.json['20023085-000012'],/* 国际化处理： 辅助明细账*/
                    'assbalance':this.state.json['20023085-000013']/* 国际化处理： 辅助余额表*/
                }
			})
		}
        getMultiLang({moduleId:'20023085', domainName:'gl', currentLocale:'simpchn', callback});
    }
    componentWillReceiveProps(nextProps) {
		if(this.props.data == nextProps.data){
			return;
		}
		let tableData = nextProps.data;
		let checkedArray = [];
		for (let i = 0, len = tableData.length; i < len; i++) {
            tableData[i].key = i;
            tableData[i].num = i + 1
			checkedArray.push(false);
		}
		this.setState({
            tableData: tableData,
            checkedAll: false,
			checkedArray
		});
	}
    componentDidMount() {
        this.props.PrintTableRef(this)
    }
    getColumns = (json) => {
        let page = this
        let props = page.props
        let { status, pk_accountingbook } = props
        let columns = [
            {
                title: (<div fieldid='num'>{json['20023085-000014']}</div>),/* 国际化处理： 序号*/
                dataIndex: "num",
                key: "num",
                width: 100,
                fixed: "left",
                render: (text, record, index) => <div class='padding_7' fieldid='num'>{text || " "}</div>
            },
            {
                title: (<div fieldid='itemcode'><i style={{color: '#E14C46'}}>*</i>{json['20023085-000015']}</div>),/* 国际化处理： 打印任务编码*/
                dataIndex: "itemcode",
                key: "itemcode",
                width: 180,
                render(text, record, index){
                    return(
                        <EditableCell 
                            cellType='input'
                            type='itemcode'
                            json={json}
                            placeholder={json['20023085-000015']}/* 国际化处理： 打印任务编码*/
                            status={status}
                            text={text}
                            record={record}
                            index={index}
                            handleChange={page.handleChange}
                        />                      
                    )
                }
            },
            {
                title: (<div fieldid='itemname'><i style={{color: '#E14C46'}}>*</i>{json['20023085-000016']}</div>),/* 国际化处理： 打印任务名称*/
                dataIndex: "itemname",
                key: "itemname",
                width: 180,
                render(text, record, index){
                    return(
                        <EditableCell 
                            cellType='input'
                            type='itemname'
                            json={json}
                            placeholder={json['20023085-000016']}/* 国际化处理： 打印任务名称*/
                            status={status}
                            text={text}
                            record={record}
                            index={index}
                            handleChange={page.handleChange}
                        /> 
                    )
                }
            },
            {
                title: (<div fieldid='printobjectcode'><i style={{color: '#E14C46'}}>*</i>{json['20023085-000017']}</div>),/* 国际化处理： 打印对象*/
                dataIndex: "printobjectcode",
                key: "printobjectcode",
                width: 180,
                render(text, record, index){
                    let name = page.printobjectTypes[text]
                    return(
                        <EditableCell 
                            cellType='select'
                            type='printobjectcode'
                            json={json}
                            placeholder={json['20023085-000017']}/* 国际化处理： 打印对象*/
                            status={status}
                            text={name}
                            record={record}
                            index={index}
                            handleChange={page.handleChange}
                        /> 
                    )
                }
            },
            {
                title: (<div fieldid='v_user'>{json['20023085-000018']}</div>),/* 国际化处理： 使用人*/
                dataIndex: "v_user",
                key: "v_user",
                width: 180,
                render(text, record, index){
                    let tempText = text && text.length>0 ? text.map(item => item.refname).join(",") : ''
                    return(
                        <EditableCell 
                            cellType='refer'
                            type='v_user'
                            json={json}
                            placeholder={json['20023085-000018']}/* 国际化处理： 使用人*/
                            pk_accountingbook = {pk_accountingbook}
                            status={status}
                            text={tempText}
                            record={record}
                            index={index}
                            handleChange={page.handleChange}
                        /> 
                    )
                }
            },
            {
                title: (<div fieldid='memo'>{json['20023085-000019']}</div>),/* 国际化处理： 备注*/
                dataIndex: "memo",
                key: "memo",
                width: 180,
                render(text, record, index){
                    return(
                        <EditableCell 
                            cellType='input'
                            type='memo'
                            json={json}
                            placeholder={json['20023085-000019']}/* 国际化处理： 备注*/
                            status={status}
                            text={text}
                            record={record}
                            index={index}
                            handleChange={page.handleChange}
                        /> 
                    )
                }
            },
            {
                title: (<div fieldid='opr'>{json['20023085-000020']}</div>),/* 国际化处理： 操作*/
                dataIndex: "operation",
                key: "operation",
                width: 200,
                fixed: "right",
                render(text, record, index){
                    // 浏览态：0; 修改，打印，更多（查询条件、打印条件、删除、复制）
                    // 编辑态: 1; 查询条件、打印条件 (未选打印对象，不显示按钮；选打印对象后显示‘查询条件’；选查询条件后，显示打印条件)
                    
                    let editBtnArr = record.printobjectcode ? ['copy_line', 'query_terms_m', 'print_terms_m'] : []  //查询条件和打印条件没有先后顺序

                    let buttonArr = status !=='browse' ? editBtnArr : ['print_line', 'delete_line', 'query_terms_m', 'print_terms_m']
                    
                    return(             
                        <div fieldid='opr'>
                            {props.button.createOprationButton(buttonArr, {
                                area: 'table_inner',
                                buttonLimit: 3,
                                onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index, page, json)
                            })}
                        </div>
                    )
                }
            }
        ]
        return columns
    }

    openDateRangeModal = (printitemPkArr, isShowPreviewBtn) => {
        this.props.openDateRangeModal(printitemPkArr, isShowPreviewBtn)
    }
    deleteRow = (deleteRow) => {
        this.props.deleteRow(deleteRow)
    } 
    copyRow = (rowData) => {
        let tempRowData = {...rowData}
        this.props.copyRow(tempRowData)
    }
    openAdvSearch = (searchId, index, querystate) => {
        this.props.openAdvSearch(searchId, index, querystate)
    }
    openPrintTermsModal = (visible, index, printmodalContent, printstate) => {
        this.props.openPrintTermsModal(visible, index, printmodalContent, printstate)
    }
    handleChange = (value, index, key) => {
        this.props.onCellChange(value, index, key)
    }
    getCheckedRows = () => {
        let { checkedArray, tableData } = this.state;
		let checkedRows = [];
		for (var i = 0; i < checkedArray.length; i++) {
			if (checkedArray[i] === true) {
				checkedRows.push(tableData[i]);
			}
        }
		return checkedRows;
    };
    getRowDataByIndex = (index) => {
        return this.state.tableData[index]
    }
    updateButtonStatus = () => {
        let {checkedArray} = this.state
        if(checkedArray.includes(true)){
            this.props.updateButtonStatus(true)
        }else{
            this.props.updateButtonStatus(false)
        }
    }
    //全选
	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		},()=>{
            this.updateButtonStatus()
        });
	};
	//单选
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}	
		self.setState({
			checkedAll: allFlag,
			checkedArray: checkedArray
		},()=>{
            this.updateButtonStatus()
        });
    };
    onRowClick = (record, index, event) => {
        this.setState({
            selectedRow: index
        })
    }
	renderColumnsMultiSelect(columns) {
		const { checkedArray } = this.state;
		const { multiSelect } = this.props;
		let indeterminate_bool = false;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
                        <div fieldid="firstcol">
                            <Checkbox
                                className="table-checkbox"
                                checked={this.state.checkedAll}
                                indeterminate={indeterminate_bool && !this.state.checkedAll}
                                onChange={this.onAllCheckChange}
                            />
                        </div>
					),
					key: 'checkbox',
                    attrcode: 'checkbox',
                    dataIndex: 'checkbox',
                    width: '60px',
                    fixed: "left",
					render: (text, record, index) => {
						return (
                            <div fieldid="firstcol">
                                <Checkbox
                                    className="table-checkbox"
                                    checked={this.state.checkedArray[index]}
                                    onChange={this.onCheckboxChange.bind(this, text, record, index)}
                                />
                            </div>
							
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}
    render() { 
        let { tableData,json, selectedRow } = this.state
        let columns = this.getColumns(json)
        let columnsldad = this.renderColumnsMultiSelect(columns);
        let iWidth = window.innerWidth
		let iHeight = window.innerHeight
        return(
            <div>
                <NCTable 
                    bordered
				    columns={columnsldad}
                    data={tableData}
                    isDrag={true}
                    rowClassName = {(record, index, indent)=>{
                        // 平台提供选中行类名nctable-selected-row, 行复制用到
                        return selectedRow === index ? 'nctable-selected-row' : ''
                    }}
                    onRowClick = {this.onRowClick}
                    // scroll={{ x: iWidth-60, y: iHeight-100 }}
                    adaptionHeight = {true}
                    // bodyStyle={{height:getTableHeight(142)}}
                    // scroll={{ x: true, y: getTableHeight(142) }}
                />
            </div>
        )
    }
}

PrintTable = createPage({
    // initTemplate: initTemplate
})(PrintTable)
export default PrintTable
PrintTable.defaultProps = {
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
