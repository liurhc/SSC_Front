import React, { Component } from 'react';
import { base,ajax, createPage, promptBox, toast, getMultiLang, createPageIcon } from 'nc-lightapp-front';
const { NCNumber, NCFormControl:FormControl, NCSelect:Select, NCTooltip:Tooltip } = base;
const Option = Select.NCOption;
import ReferEle from '../../../../../refer/voucher/OperatorDefaultRef/index.js';
import { initTemplate } from '../../events';

class EditableCell extends Component {
    constructor(props){
        super(props);
        this.appcode = props.getSearchParam('c') || props.getUrlParam('c')
        this.state = {
            
        }
        this.printobjectTypes = [
            {
                'code':'generalbook',
                'name':props.json['20023085-000009']/* 国际化处理： 三栏式总账*/
            },
            {
                'code':'detailbook',
                'name':props.json['20023085-000010']/* 国际化处理： 三栏式明细账*/
            },
            {
                'code':'diarybook',
                'name':props.json['20023085-000011']/* 国际化处理： 日记账*/
            },
            {
                'code':'assdetail',
                'name':props.json['20023085-000012']/* 国际化处理： 辅助明细账*/
            },
            {
                'code':'assbalance',
                'name':props.json['20023085-000013']/* 国际化处理： 辅助余额表*/
            }
        ]
    }
    handleChange = (val, index, type) => {
        if (this.props.handleChange) {
            this.props.handleChange(val, index, type)
        }
    }
    renderInput = (index, type, record, placeholder) => {
        return(
            <FormControl
                fieldid={type}
                value={record[type]}
                disabled={status==='browse' ? true : false}
                placeholder={placeholder}
                onChange={(val)=>{
                    this.handleChange(val, index, type)
                }}
            />
        )
    }
    renderSelect = (index, type, record, placeholder) => {
        return(
            <div className="">
                <Select 
                    fieldid={type}
                    showClear={false}
                    value={record[type]}
                    disabled={status==='browse' ? true : false}
                    placeholder={placeholder}
                    onChange={(val)=>{
                        this.handleChange(val, index, type)
                    }}
                >               
                    {
                        this.printobjectTypes.map((item, index) => {
                            return <Option key={index} value={item.code}>{item.name}</Option>
                        })
                    }
                </Select>
            </div>
        )
    }
    renderReferLoader = (index, type, record) => {
        let { pk_accountingbook } = this.props
        let defaultVal = record[type] && record[type].length>0 ? record[type] : [{refcode: '', refname: '', refpk: ''}]         
        return(
            <ReferEle
                fieldid={type}
                isMultiSelectedEnabled={true}
                value={defaultVal}
                disabled={status==='browse' ? true : false}
                queryCondition={ () => {
                    return {
                        pk_accountingbook: pk_accountingbook,
                        user_Type : this.appcode,
                        GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi',
                        "isShowDisabledData": 'N' // 不显示已停用人员
                    }
                }}
                onChange={(val) => {
                    this.handleChange(val, index, type)
                }}
            />
        )
    }
    drawingCell = (cellType, record, index, type, placeholder) => {
        switch(cellType){
            case 'input': return this.renderInput(index, type, record, placeholder)   
            case 'select': return this.renderSelect(index, type, record, placeholder)   
            case 'refer': return this.renderReferLoader(index, type, record)   
        }
    }
    
    render(){
        let { status, cellType, type, text, record, index, placeholder } = this.props    
        return(
            <div className="" fieldid={type}>
                {status !=='browse' ? (
                    <div className=""> 
                        {this.drawingCell(cellType, record, index, type, placeholder)}
                    </div>
                ) : (
                    <div className='padding_7'>
                        {text ? text : <span>&nbsp;</span>}
                    </div>
                )}
            </div>
        )
    }
}

EditableCell = createPage({
    // initTemplate: initTemplate
})(EditableCell)
export default EditableCell
