/*
 * @Author: llixxm
 * @Description: 打印期间弹框
 * @Date: 2019-04-22 13:39:24
 */
import React, { Component } from 'react';
import { base,ajax, createPage, promptBox, toast, getMultiLang } from 'nc-lightapp-front';
const { NCRow:Row, NCCol:Col, NCButton: Button } = base;
import { initTemplate } from '../../events';
import AccPeriodDefaultTreeGridRef from '../../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';

class DateRangeModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            dateRangeParams: {},
            printitemPkArr: null, //打印行数据
            isShowPreviewBtn: true, //是否显示‘预览’按钮
            json: {}
        }
    }
    componentWillMount(){
        // let callback = (json) => {
		// 	this.setState({
        //         json: json
        //     }, () => {
        //         initTemplate.call(this, this.props);
		// 	})
		// }
        // getMultiLang({moduleId:'20023085', domainName:'gl', currentLocale:'simpchn', callback});
    }
    componentWillReceiveProps(nextProps){
        let dateRangeParams = nextProps.dateRangeParams
        this.setState({
            dateRangeParams
        })
    }
    componentDidMount(){
        this.props.DateRangeModalRef(this)
    }
    // 打开弹框
    openDateRangeModal = (printitemPkArr, isShowPreviewBtn=true) => {
        this.props.modal.show('daterangemodal')
        this.setState({
            printitemPkArr,
            isShowPreviewBtn
        })
    }

    //关闭弹框
    closeDateRangeModal = () => {
        this.props.modal.close('daterangemodal')
    }

    //校验打印期间是否正确
    checkDate = (dateRangeParams) => {
        let { json } = this.props
        let { begindate, enddate } = dateRangeParams
        if(begindate.begindate && enddate.enddate){
            let beginArr = begindate.begindate.split("-")
            let endArr = enddate.enddate.split("-")
            let printParams = {
                startyear: beginArr[0],
                endyear: endArr[0],
                startperiod: beginArr[1],
                endperiod: endArr[1]
            }
            let ismonthpass = Number(beginArr[1]) <= Number(endArr[1])
            if(beginArr[0] !== endArr[0] || !ismonthpass){
                if(beginArr[0] !== endArr[0]){
                    toast({ content: json['20023085-000001'], color: 'warning' });/* 国际化处理： 请选择同一年份的期间*/
                } else if( !ismonthpass){
                    toast({ content: json['20023085-000002'], color: 'warning' })/* 国际化处理： 起始期间不能大于结束期间*/
                }
            } else {
                return printParams
            }
            
        } else {
            toast({ content: json['20023085-000003'], color: 'warning' });/* 国际化处理： 请选择打印期间*/
        }
        
    }

    //点击 浏览/打印 isPreview(true:浏览,false:打印)
    handlePrint = (isPreview) => {
        let { dateRangeParams, printitemPkArr } = this.state
        let { handleDateRangePrint } = this.props
        let printParams = this.checkDate(dateRangeParams)
        if (printParams) {
            let sendData = {
                printParams: printParams,
                printitemPkArr: printitemPkArr,
                isPreview: isPreview
            }
            handleDateRangePrint && handleDateRangePrint(sendData)
            this.closeDateRangeModal()
        }
    }

    //切换打印期间
    changeAccountPeriod = (val, type) => {
        let { dateRangeParams } = this.state
        let tempData = {
            refname: val.refname ? val.refname : '',
            refpk: val.refpk ? val.refpk : '',
            [type]: val.values ? val.values[type].value : ''
        }
        dateRangeParams[type] = tempData
        // dateRangeParams[type] = val.values
        this.setState({
            dateRangeParams
        })
    }

    /**
     * @description: 渲染期间参照
     * @param {type} : begindate: 起始期间, enddate: 结束期间
     * @return: 
     */
    renderDateRefer = (type, pk_accperiodscheme, pk_accountingbook) => {
        let {dateRangeParams} = this.state
        let value = dateRangeParams[type]
        return(
            <AccPeriodDefaultTreeGridRef
                value={value}
                fieldid={type}
                queryCondition = {{
                    "pk_accperiodscheme": pk_accperiodscheme,
                    "includeAdj": type === 'enddate' ? "Y" : 'N',
                    "GridRefActionExt": "nccloud.web.gl.ref.AccperiodGridRefSqlBuilder",
                    "TreeRefActionExt": "nccloud.web.gl.ref.AccperiodTreeRefSqlBuilder",
                    "pk_accountingbook": pk_accountingbook 
                }}
                onChange={(val)=>{
                    this.changeAccountPeriod(val, type)
                    // type === 'begindate' ? this.accountPeriodStart(val,type) : this.accountPeriodEnd(val)
                }}
            />
        )
    }
    
    //弹框内容
    modalContent = () => {
        let { pk_accperiodscheme, pk_accountingbook, json } = this.props
        let { dateRangeParams } = this.state
        let { begindate, enddate } = dateRangeParams
        let dateRange = `${begindate && begindate.begindate} ~ ${enddate && enddate.enddate}`
        return(
            <div>
                <Row fieldid='periodrange'>
                    <Col md={3} xs={3} sm={3}>
                        <span className='nc-theme-form-label-c'>{json['20023085-000005']}：</span>{/* 国际化处理： 期间范围*/}
                    </Col>
                    <Col md={4} xs={4} sm={4}>
                        {this.renderDateRefer('begindate', pk_accperiodscheme, pk_accountingbook)}             
                    </Col>
                    <Col md={1} xs={1} sm={1}> &nbsp;~ </Col>
                    <Col md={4} xs={4} sm={4}>
                        {this.renderDateRefer('enddate', pk_accperiodscheme, pk_accountingbook)}
                    </Col>
                </Row>
                <Row className='daterange' fieldid='daterange'>
                    <Col md={3} xs={3} sm={3}>
                        <span className='nc-theme-form-label-c'>{json['20023085-000006']}：</span>{/* 国际化处理： 日期范围*/}
                    </Col>
                    <Col md={8} xs={8} sm={8}>
                        <span className='nc-theme-form-label-c'>
                            {dateRange}
                        </span>
                    </Col>
                </Row>
            </div>
        )
    }
    render(){
        let { modal, json } = this.props
        let { createModal } = modal;
        let { isShowPreviewBtn } = this.state
        let isshowPre = isShowPreviewBtn ? '' : 'none'
        return(
            <div>
                {json['20023085-000007'] ? createModal('daterangemodal', {
                    title: json['20023085-000004'],// 弹框表头信息/* 国际化处理： 打印*/
                    content: this.modalContent(), //弹框内容，可以是字符串或dom
                    closeModalEve: this.closeModalEve, //关闭按钮事件回调
                    size:'lg', //  模态框大小 sm/lg/xlg/xxlg
                    className:'senior',
                    showCustomBtns:true, 
                    customBtns: <div className='foot-btn'>
                        <div style={{ display: isshowPre }}><Button fieldid='preview' onClick={this.handlePrint.bind(this, 1)}>{json['20023085-000007']}</Button></div>{/* 国际化处理： 预览*/}
                        <Button fieldid='print' colors="primary" onClick={this.handlePrint.bind(this, 2)}>{json['20023085-000004']}</Button>{/* 国际化处理： 打印*/}
                        <Button fieldid='cancel' onClick={this.closeDateRangeModal}>{json['20023085-000008']}</Button>{/* 国际化处理： 取消*/}
                    </div>,
                    zIndex:200,//遮罩的层级为zIndex，弹框默认为zIndex+1。默认为200，非比传项
                }) :''} 
            </div>
        )
    }
}

DateRangeModal = createPage({})(DateRangeModal)
export default DateRangeModal
