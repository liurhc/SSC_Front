export const actions = {
    query: '/nccloud/gl/accbookprint/query.do',
    save: '/nccloud/gl/accbookprint/save.do',
    delete: "/nccloud/gl/accbookprint/del.do",
    checkprint: "/nccloud/gl/accbookprint/checkprint.do", //预览、打印前校验数据
    jobprint: "/nccloud/gl/accbookprint/jobprint.do",
    accountinfoquery: '/nccloud/gl/glpub/accountinfoquery.do' //根据pk_accountingbook获取期间过滤条件
}
