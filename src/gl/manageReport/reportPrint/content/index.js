import React, { Component } from 'react';
import { base,ajax, createPage, promptBox, toast, getMultiLang, createPageIcon, print } from 'nc-lightapp-front';
const { NCDiv } = base;
import { buttonClick, initTemplate, getParamsByCode } from './events';
import ReferLoader from '../../../public/ReferLoader/index.js';
import { accbookRefcode } from '../../../public/ReferLoader/constants.js';
import PrintModal from '../../../public/components/printModal/index.js';
import DateRangeModal from './components/DateRangeModal/index.js';
import PrintTable from './components/PrintTable/index.js';
import ThreeAllQueryModal from '../../../threeall/container/Accbalance/MainSelectModal';
import ThreeDetailQueryModal from '../../../threedetail/container/Accbalance/MainSelectModal';
import DateAccountQueryModal from '../../dateAccount/content/components/MainSelectModal';
import AssistDetailQueryModal from '../../assistDetailAccount/content/components/MainSelectModal';
import AssistBalanceQueryModal from '../../assistBalance/content/components/MainSelectModal';
import { printRequire } from '../../common/printModal/events'
import HeaderArea from '../../../public/components/HeaderArea';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook'
import {actions} from './actions/consts.js'
import './index.less';
import poc from "../../../../uap/common/components/printOnClient";
const { printPreview } = poc;

class ReportPrint extends Component {
    constructor(props) {
        super(props);
        this.searchId = 'reportPrintSearch';
        // this.tableId = 'reportPrintTable';
        this.appcode = props.getSearchParam('c') || props.getUrlParam('c')
		this.state = this.getAllState()
        this.index = ''
        this.currentReport='';
    }
    
    componentWillMount(){
        //获取默认账簿
        getDefaultAccountBook(this.appcode).then((defaultAccouontBook)=>{
            if(defaultAccouontBook.value){
                let accountingbook = {
                    refname:defaultAccouontBook.display,
                    refpk:defaultAccouontBook.value
                }
                this.setState({
                    defaultAccouontBook:{refname:defaultAccouontBook.display,refpk:defaultAccouontBook.value,getflag:true}
                },()=>{
                    if(accountingbook.refpk){
                       this.onAccountBookChange(accountingbook); 
                    }
                    
                })
                
            }else{
                this.setState({
                    defaultAccouontBook:{refname:'',refpk:'',getflag:true}
                })
            }
        })
		let callback = (json) => {
			this.setState({
                json: json
            }, () => {
                initTemplate.call(this, this.props);
			})
		}
        getMultiLang({moduleId:'20023085', domainName:'gl', currentLocale:'simpchn', callback});
        this.updateButtonStatus()
    }
    componentDidMount() {
        
        //关闭浏览器
        window.onbeforeunload = () => {
            let { status } = this.state
            if(status !== 'browse') {
                return ''
            }
        }
    }
    // 初始state
    getInitState = () => {
        let initState = {
            defaultAccouontBook:{//获取默认账簿
                refpk: '',
                refname: '',
                getflag:false,
            },
            accountingbook: {
                refpk: '',
                refname: ''
            }, //财务核算账簿
            pk_accperiodscheme: '', //期间过滤条件
            queryParams: {}, //查询条件--弹框
            status: 'browse', //页面状态,browse/add/edit
            tableData: [], //表格数据
            visible: false, //控制打印条件弹框显隐性,true显示,false隐藏
            printmodalContent: {
                appcode: '', //打印对象对应节点的appcode
                noRadio: true, 
                noCheckBox: true, 
                showOrderunify: false
            }, //打印条件--弹框
            dateRangeParams: {
                begindate: {},
                enddate: {}
            } //打印--期间范围
        }
        return initState
    }
    //state中添加json
	getAllState = () => {
		let initState = this.getInitState()
		initState.json={}
		return initState
    }
    //初始化state
	initPageState = () => {
        this.setState({...this.getInitState()})
        this.updateButtonStatus()
	}
    //更新按钮状态, isCheckedRow: 是否有勾选行,默认false,未勾选
    updateButtonStatus = (isCheckedRow=false) => {
        let { status, accountingbook, tableData } = this.state
        let isBrowse = status==='browse' ? true : false  //是否是浏览态
        let isChooseBook = accountingbook.refpk ? true : false //是否选择了财务核算账簿
        let isTableEmpty = isChooseBook && tableData.length > 0 ? false : true
        this.props.button.setButtonVisible([ 'save', 'cancel' ], !isBrowse) 
        this.props.button.setButtonVisible([ 'query', 'edit', 'print', 'print_list', 'refresh' ], isBrowse)
        this.props.button.setButtonVisible([ 'add', 'delete' ], true) 
        this.props.button.setButtonDisabled([ 'query', 'add', 'delete', 'print', 'refresh' ], !isChooseBook);
        this.props.button.setButtonDisabled([ 'delete', 'print' ], !isCheckedRow);
        this.props.button.setMainButton('add', isBrowse)
        this.props.button.setButtonDisabled([ 'edit', 'print_list' ], isTableEmpty)
    }
    // 获取财务核算账簿pk
    getAccountingbookPk = () => {
        return this.state.accountingbook.refpk
    }
    // 获取初始数据
    getInitData = (queryParams={},callback=null) => {
        let { accountingbook } = this.state
        if(!accountingbook.refpk){
            this.initPageState()
            return
        }
        let tempData = {
            pk_accountingbook: accountingbook.refpk,
            ispublic: true
        }
        let data = {...tempData, ...queryParams}
        // let data = {...queryParams}
        ajax({
            url: actions.query,
            data: data,
            success: (res) => {
                const { success, data } = res;
                if (success) {
                    
                    if(callback){
                        callback()
                    } else {
                        if (data.length > 0) {
                            toast({ content: this.state.json['20023085-000028'], color: 'success' });     /* 国际化处理： 查询成功！*/
                        } else {
                            toast({ content: this.state.json['20023085-000029'], color: 'warning' });/* 国际化处理： 未查询到符合条件的数据！*/
                        }
                    }                   
                    this.setState({
                        status: 'browse',
                        queryParams: queryParams,
                        tableData: data
                    },()=>{
                        this.updateButtonStatus()
                    })
                }
            }
        });
    }
    //切换核算账簿
    onAccountBookChange = (val) => {
        let { accountingbook } = this.state
        accountingbook.refpk = val.refpk
        accountingbook.refname = val.refname
        this.setState({
            accountingbook
        },()=>{
            // if(!this.state.accountingbook.refpk){
            //     this.initPageState()
            //     return
            // }
            this.getInitData({},()=>{})
            this.getAccountInfo()
        }) 
    }
    //单元格编辑
    onCellChange = (value, index, key) => {
        const tableData = [...this.state.tableData];
        tableData[index][key] = value;
        
        //切换打印对象时，查询条件、打印条件置空
        if(key==='printobjectcode'){
            tableData[index]['isncc'] = true; //重量端数据变为轻量端数据
            tableData[index]['queryvo'] = {};
            tableData[index]['querystate'] = {};
            tableData[index]['printvo'] = {};
            tableData[index]['printstate'] = {};
        }
        this.setState({ tableData });
    }
    //新增（行）
    addRow = () => {
        let { tableData, accountingbook } = this.state;
        let newData = {
            isncc: true, //区分是重量端数据/轻量端数据
            key: tableData.length,
            pk_accountingbook: accountingbook.refpk,
            num: tableData.length + 1,
            itemcode: '',
            itemname: '',
            printobjectcode: '',
            v_user: [],
            memo: '',
            queryvo: {}, //查询条件
            querystate: {}, //查询条件state
            printvo: {}, //打印条件
            printstate: {}, //打印条件state
        };
        this.setState({
            status: 'add',
            tableData: [...tableData, newData]
        }, ()=>{
            this.updateButtonStatus()
        })
    }
    // 修改
    editTable = () => {
        this.setState({
            status: 'edit'
        }, ()=>{
            this.updateButtonStatus()
        })
    }
    //行删除
    deleteRow = (checkedRows) => {
        this.batchDelete(checkedRows)
    }
    //复制行
    copyRow = (rowData) => {
        let tableData = [...this.state.tableData];
        rowData.pk_printitem && delete rowData.pk_printitem //去掉行pk
        rowData.num = tableData.length + 1
        if(!rowData.isncc) rowData.isncc = true
        this.generateVo(rowData)
        this.setState({
            tableData: [...tableData, rowData]
        })
    }
    //删除后事件
    afterDeleteEvent = (checkedKey, tableData) => {
        for (let i = tableData.length - 1; i >= 0; i--) {
            if(checkedKey.includes(tableData[i].key)){
                tableData.splice(i, 1);
                continue;
            }
        }
        toast({ content: this.state.json['20023085-000030'], color: 'success' })/* 国际化处理： 删除成功！*/
        this.setState({
            tableData: tableData
        },()=>{
            this.updateButtonStatus()
        })
    }
    //批量删除
    batchDelete = (checkedRows) => {
        let tableData = [...this.state.tableData];
        // let checkedRows = this.PrintTable.getCheckedRows()
        let delpkArr = []
        let checkedKey = []
        for(let i = checkedRows.length - 1; i >= 0; i--) {
            if(checkedRows[i].pk_printitem){
                delpkArr.push(checkedRows[i].pk_printitem)
            }
            checkedKey.push(checkedRows[i].key)
        }
        if(delpkArr.length>0){
            ajax({
                url: actions.delete,
                data: delpkArr,
                success: (res) => {
                    const { success } = res;
                    if (success) {
                        this.afterDeleteEvent(checkedKey, tableData)
                    }
                }
            });
        } else {         
            this.afterDeleteEvent(checkedKey, tableData)
        }
        
    }

    //根据生成querystate,printstate生成queryvo,printvo
    generateVo = (item) => {
        let tempQueryVo = typeof(item.querystate) !== "undefined" ? (typeof(item.querystate) === 'string' ? JSON.parse(item.querystate) : item.querystate) : {}
        let tempPrintVo = typeof(item.printstate) !== "undefined" ? (typeof(item.printstate) === 'string' ? JSON.parse(item.printstate) : item.printstate) : {}
        item.queryvo = JSON.stringify(tempQueryVo) !== '{}' ? this.getConfirmData(item.printobjectcode, tempQueryVo) : {}
        item.printvo = JSON.stringify(tempQueryVo) !== '{}' ? this.PrintModal.getConfirmData(tempPrintVo) : {}
    }

    // 根据state组合成ajax需要的vo
    getConfirmData = (printobjectcode, querystate) => {
        switch(printobjectcode){
            // generalbook:三栏式总账, detailbook:三栏式明细账,diarybook:日记账,assdetail:辅助明细账,assbalance:辅助余额表
            case 'generalbook':  
                return this.ThreeAllQueryModal.getConfirmData(querystate)
            case 'detailbook': 
                return this.ThreeDetailQueryModal.getConfirmData(querystate)
            case 'diarybook':
                return this.DateAccountQueryModal.getConfirmData(querystate) 
            case 'assdetail': 
                return this.AssistDetailQueryModal.getConfirmData(querystate)
            case 'assbalance': 
                return this.AssistBalanceQueryModal.getConfirmData(querystate)
        }
    }

    //保存
    saveTable = () => {
        // let {tableData} = this.state
        let tableData = [...this.state.tableData]
        tableData.forEach((item, index) => {
            item.v_user = item.v_user && item.v_user.length > 0 ? item.v_user : []
            if(item.pk_printitem){  
                if(item.isncc){
                    // 查询返回的轻量端数据数据没有queryvo、printvo
                    this.generateVo(item)
                } 
            }         
        })
        ajax({
            url: actions.save,
            data: tableData,
            success: (res) => {
                const { success, data } = res;
                if (success) {
                    let callback = () => { 
                        toast({ content: this.state.json['20023085-000031'], color: 'success' }); /* 国际化处理： 保存成功！*/
                    }
            
                    this.reFresh(callback)
                    this.setState({
                        status: 'browse'
                    }, () => {
                        this.updateButtonStatus()
                    })
                }
            }
        });
        
    }
    //取消
    cancel = () => {
        this.reFresh(()=>{})
        this.setState({
            status: 'browse'
        }, ()=>{
            this.updateButtonStatus()
        })
    }
    //打印列表
    ptintList = () => {
        // 查询条件json
        let { queryParams } = this.state;
        let { accountingbook } = this.state;
        let tempData = {
            pk_accountingbook: accountingbook.refpk,
            ispublic: true
        }
        let data = {...tempData, ...queryParams}
        // 无模板打印
        print(
            'pdf',
            '/nccloud/gl/accbookprint/listprint.do',
            {
                appcode: '20023085',
                pagecode: '20023085PAGE',
                download: 'directPrint',
                oids: [],
                userjson: JSON.stringify(data)
            }, false
        )
    }
    //刷新
    reFresh = (callback=null) => {
        let { queryParams } = this.state
        this.getInitData(queryParams, callback)
    }
    //打印 打开弹框
    openDateRangeModal = (printitemPkArr, isShowPreviewBtn) => {       
        this.DateRangeModal.openDateRangeModal(printitemPkArr, isShowPreviewBtn)
    }
    //打印， isPreview：1、预览，2、不预览，3、直接打印
    rowPrint = (record, printParams, isPreview) => {
        printParams = {...printParams};
        printParams.pk_printitem = record.pk_printitem
        printParams.printobjectcode = record.printobjectcode
        let { ctemplate } = record.printvo
        let appcode = getParamsByCode(record.printobjectcode).appcode
        //预览、打印前校验
        ajax({
            url: actions.checkprint,
            data: [record.pk_printitem],
            success: (res) => {
                printRequire(this.props, actions.jobprint, appcode, null, ctemplate, printParams, isPreview)                     
            }
        });
    }
    //打印 弹框 预览/打印
    handleDateRangePrint = (data) => {
        let { printParams, printitemPkArr, isPreview } = data
        if (printitemPkArr instanceof Array){
            if(printitemPkArr.length>0){
                printitemPkArr.map((item, index)=>{
                    this.rowPrint(item, printParams, 3)
                })
            }     
        } else {
            this.rowPrint(printitemPkArr, printParams, isPreview)
        }
        // if(printitemPkArr.length>0){
        //     if(printitemPkArr.length>1){
        //         isPreview = 3 //3:直接打印
        //     }
        //     printitemPkArr.map((item, index)=>{
        //         printParams.pk_printitem = item.pk_printitem
        //         printParams.printobjectcode = item.printobjectcode
        //         let { ctemplate } = item.printvo
        //         let appcode = getParamsByCode(item.printobjectcode).appcode
        //         //预览、打印前校验
        //         ajax({
        //             url: actions.checkprint,
        //             data: [item.pk_printitem],
        //             success: (res) => {
        //                 printRequire(this.props, actions.jobprint, appcode, null, ctemplate, printParams, isPreview)                     
        //             }
        //         });
        //     })
        // }
    }
    //打开查询条件弹框,
    openAdvSearch = (searchId, index, querystate) => {   
        this.currentReport=searchId;   
        this.setState({},()=>{

        switch(searchId){
            // generalbook:三栏式总账, detailbook:三栏式明细账,diarybook:日记账,assdetail:辅助明细账,assbalance:辅助余额表
            case '20023010query':  
                this.ThreeAllQueryModal.openAdvSearch(index, querystate)
                // this.ThreeAllQueryModal.setInitState(querystate) //初始化查询条件弹框state
            break;
            case '20023030query': 
                this.ThreeDetailQueryModal.openAdvSearch(index, querystate)
                // this.ThreeDetailQueryModal.setInitState(querystate)
            break;
            case '20023035query':
                this.DateAccountQueryModal.openAdvSearch(index, querystate) 
                // this.DateAccountQueryModal.setInitState(querystate) 
            break;
            case '20023060query': 
                this.AssistDetailQueryModal.openAdvSearch(index, querystate)
                // this.AssistDetailQueryModal.setInitState(querystate)
            break;
            case '20023055query': 
                this.AssistBalanceQueryModal.openAdvSearch(index, querystate)
                // this.AssistBalanceQueryModal.setInitState(querystate)
            break;
        }
                    
    })
        
    }
    // 查询条件‘确定’
    queryTermsOk = (data, querystate) => {
        let tempState = {...querystate}
        delete tempState.json
        let index = data.index
        const tableData = [...this.state.tableData];
        tableData[index].queryvo = data;
        tableData[index].querystate = tempState;
        // 重量端数据修改后变为轻量端数据
        if(!tableData[index].isncc){
            tableData[index].isncc = true
        }
        this.setState({ tableData });
    }
    // 打开打印条件弹框
    openPrintTermsModal = (visible, index, printmodalContent, printstate) => {
        this.setState({
            visible: visible,
            printmodalContent: printmodalContent
        })
        this.index = index
        this.PrintModal.setInitState(printstate)
    }
    // 打印条件弹框‘确定’
    handlePrintTermsOk = (data, printstate) => {
        let tempState = {...printstate}
        delete tempState.json
        const tableData = [...this.state.tableData];
        tableData[this.index].printvo = data;
        tableData[this.index].printstate = tempState;
        // 重量端数据修改后变为轻量端数据
        if(!tableData[this.index].isncc){
            tableData[this.index].isncc = true
        }
        this.setState({ 
            tableData,
            visible: false
         });
    }

    //获取账簿信息
    getAccountInfo = () => {
        let self = this;
        let { accountingbook } = self.state
        if(!accountingbook.refpk) {
            return 
        }
		let data = {
			pk_accountingbook: accountingbook.refpk,
        };
		ajax({
		    // loading: true,
		    url: actions.accountinfoquery,
		    data,
		    success: (res) => {
		        const { data, error, success } = res;
		        if(success){	
                    let tempObj = {
                        begindate: data.begindate,
                        enddate: data.enddate,
                        refname: data.bizPeriod,
                        refpk: data.pk_accperiodmonth
                    }
                    let dateRangeParams = {
                        begindate: tempObj,
                        enddate: tempObj
                    }       	
					self.setState({
                        pk_accperiodscheme: data.pk_accperiodscheme,

                        dateRangeParams: dateRangeParams
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
    }
    // 查询
    searchBtnClick = () => {
        let defaultParams = {}
        let pk_accountingbook = this.state.accountingbook.refpk
        let queryData = this.props.search.getAllSearchData(this.searchId).conditions
        if(queryData.length > 0){
            queryData.forEach((item)=>{
                let tempFieldVal = item.field ==='v_user' ? item.value.firstvalue.split(",") : item.value.firstvalue
                defaultParams[item.field] = tempFieldVal
            })
        } else {
            defaultParams.ispublic = false
        }
        if(pk_accountingbook){
            this.getInitData(defaultParams)
        } else {
            toast({ content: this.state.json['20023085-000032'], color: 'warning' });/* 国际化处理： 请先选择财务核算账簿！*/
        }
    }
    render(){
        let { defaultAccouontBook,accountingbook, tableData, visible , printmodalContent, status, pk_accperiodscheme, dateRangeParams, json } = this.state
        let { NCCreateSearch } = this.props.search
        let isEdit = status==='browse' ? false : true
        return(
            <div className="nc-single-table reportPrint">
                <HeaderArea 
                    title = {this.props.getSearchParam('n')}
                    searchContent = {
                        <ReferLoader
                            tag = 'accountbook'
                            fieldid = 'accountbook'
                            refcode = {accbookRefcode}
                            value = {accountingbook}
                            disabled = {isEdit}
                            isMultiSelectedEnabled = {false}
                            showStar = {true}
                            showGroup = {false}
                            disabledDataShow = {true}
                            queryCondition = {() => {
                                return {
                                    "TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    "appcode": this.appcode,
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode": 'fi'
                                }
                            }}
                            onChange = {(val)=>{
                                this.onAccountBookChange(val)                               
                            }}
                        />
                    }
                    btnContent = {this.props.button.createButtonApp({
                        area: 'page_header',
                        // buttonLimit: 3,
                        onButtonClick: buttonClick.bind(this),
                        popContainer: document.querySelector('.header-button-area')
                    })}
                />
                <div className="nc-singleTable-search-area">
                    {NCCreateSearch(this.searchId, {
                        clickSearchBtn: this.searchBtnClick,
                        showAdvBtn: false, // 是否显示高级查询按钮和查询方案按钮 ,默认显示
                    })}
                </div> 

                <div className="nc-singleTable-table-area">
                    <NCDiv fieldid="reportprint" areaCode={NCDiv.config.TableCom}>
                        <PrintTable 
                            json = {json}
                            status = {status}
                            data = {tableData}
                            pk_accountingbook = {accountingbook.refpk}
                            onCellChange = {this.onCellChange}
                            deleteRow = {this.deleteRow}
                            copyRow = {this.copyRow}
                            openAdvSearch = {this.openAdvSearch}
                            openDateRangeModal = {this.openDateRangeModal}
                            openPrintTermsModal = {this.openPrintTermsModal}
                            updateButtonStatus = {this.updateButtonStatus}
                            PrintTableRef = {(init) => { this.PrintTable = init }}
                        />
                    </NCDiv>
                </div>
                {this.state.defaultAccouontBook.getflag?
                    <div>
                        {isEdit || this.currentReport=='20023010query' ?
                            <ThreeAllQueryModal 
                                defaultAccouontBook={defaultAccouontBook}
                                status = {status}
                                hideBtnArea={true}
                                onConfirm={this.queryTermsOk}
                                MainSelectRef={(init) => { this.ThreeAllQueryModal = init }}
                            />
                        :<div/>}
                        {isEdit ||  this.currentReport=='20023030query' ?
                            <ThreeDetailQueryModal 
                                defaultAccouontBook={defaultAccouontBook}
                                status = {status}
                                hideBtnArea={true}
                                onConfirm={this.queryTermsOk}
                                MainSelectRef={(init) => { this.ThreeDetailQueryModal = init }}
                            />
                        :<div/>}
                        {isEdit || this.currentReport=='20023035query' ?
                            <DateAccountQueryModal 
                                defaultAccouontBook={defaultAccouontBook}
                                status = {status}
                                hideBtnArea={true}
                                onConfirm={this.queryTermsOk}
                                MainSelectRef={(init) => { this.DateAccountQueryModal = init }}
                            />
                        :<div/>}
                        {isEdit || this.currentReport=='20023060query' ?
                            <AssistDetailQueryModal 
                                defaultAccouontBook={defaultAccouontBook}
                                status = {status}
                                hideBtnArea={true}
                                onConfirm={this.queryTermsOk}
                                MainSelectRef={(init) => { this.AssistDetailQueryModal = init }}
                            />
                        :<div/>}
                        {isEdit || this.currentReport=='20023055query' ?
                            <AssistBalanceQueryModal 
                                defaultAccouontBook={defaultAccouontBook}
                                status = {status}
                                hideBtnArea={true}
                                onConfirm={this.queryTermsOk}
                                MainSelectRef={(init) => { this.AssistBalanceQueryModal = init }}
                            />
                        :<div/>}
                    </div>
                :<div/>}
                <PrintModal
                    noRadio={printmodalContent.noRadio}
                    noCheckBox={printmodalContent.noCheckBox}
                    showOrderunify={printmodalContent.showOrderunify}
                    appcode={printmodalContent.appcode}
                    visible={visible}
                    disabled={status === 'browse' ? true : false}
                    isPrintTerms = {true} //打印条件的打印范围默认全部，其它默认当前
                    handlePrint={this.handlePrintTermsOk.bind(this)}
                    handleCancel={()=>{
                        this.setState({
                            visible: false
                        });
                    }}
                    PrintModalRef={(init) => { this.PrintModal = init }}
                />
                <DateRangeModal 
                    json = {json}
                    pk_accperiodscheme = {pk_accperiodscheme}
                    pk_accountingbook = {accountingbook.refpk}
                    dateRangeParams = {dateRangeParams}
                    handleDateRangePrint = {this.handleDateRangePrint}
                    DateRangeModalRef = {(init) => {this.DateRangeModal = init}}
                />
            </div>
        )
    }
}

ReportPrint = createPage({})(ReportPrint)
ReactDOM.render(<ReportPrint />, document.querySelector('#app'));
