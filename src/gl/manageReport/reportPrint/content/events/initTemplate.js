// import tableButtonClick  from './tableButtonClick';

// const tableId = 'reportPrintTable';

const searchId = 'reportPrintSearch'

export default function(props) {
	let page = this;
	let pagecode = props.getSearchParam('p') ? props.getSearchParam('p') : props.getUrlParam('p');
	let appcode = props.getSearchParam('c') ? props.getSearchParam('c') : props.getUrlParam('c');
	props.createUIDom(
		{
			pagecode: pagecode,
			appcode : appcode
		},
		function(data){
			if(data){
				if (data.template) {
					let template = data.template;
					let meta = initSearchMeta(page, template);
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		},
		false
 	);
}


function initSearchMeta(page, template){
	let meta = template;
	meta[searchId].items.map((item) => {
		if (item.attrcode == 'v_user') {
			item.queryCondition = () => {
				return getOperatorRefCondition(page);
			};
		}
	});
	return meta
}
function getOperatorRefCondition(page){
	let pk_accountingbook = page.getAccountingbookPk()
	let condition = {
		pk_accountingbook: pk_accountingbook,
		user_Type : page.appcode,
		GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
		"isDataPowerEnable": 'Y',
		"DataPowerOperationCode" : 'fi'
	}
	return condition;
}


// function initMeta(props, page, template){
//     let meta = template;
// 	meta[tableId].items.push({
// 		attrcode: 'opr',
// 		label: '操作',
// 		fixed: 'right',
// 		itemtype: 'customer', 
// 		visible: true,
// 		className: 'table-opr',
// 		width:'160px',
// 		render: (text, record, index) => {
// 			// 浏览态：0; 修改，打印，更多（查询条件、打印条件、删除、复制）
// 			// 编辑态: 1; 查询条件、打印条件
// 			let buttonArr = ['edit_line', 'print_line', 'more', 'query_terms_m', 'print_terms_m', 'delete_line', 'copy_line']
// 			// let buttonArr = record.state.value === '1' ? ['query_terms', 'print_terms'] : ['edit_line', 'print_line', 'more']
//             return props.button.createOprationButton(buttonArr, {
//                 area: "table_inner",
//                 buttonLimit: 3,
//                 onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index,tableId, page)
// 			});
//         }
// 	});
// 	return meta
// }
