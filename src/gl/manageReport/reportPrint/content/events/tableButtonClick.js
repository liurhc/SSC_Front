import { promptBox } from 'nc-lightapp-front';

const tableButtonClick = (props, key, text, record, index, page, json) => {
    const params = getParamsByCode(record.printobjectcode)
    switch (key) {
        // 表格操作按钮
        case 'print_line': //打印
			page.openDateRangeModal(record, true);
			break;
        case 'query_terms_m': //查询条件   
            if(record.isncc){
                let tempQuerystate = typeof(record.querystate) === 'string' ? JSON.parse(record.querystate) : record.querystate
                page.openAdvSearch(params.searchId, index, tempQuerystate)
            } else {
                page.openAdvSearch(params.searchId, index, {})
            }
			break;
        case 'print_terms_m':  //打印条件
            if(record.isncc){
                let tempPrintState = typeof(record.printstate) === 'string' ? JSON.parse(record.printstate) : record.printstate
                page.openPrintTermsModal(true, index, params, tempPrintState)
            } else {
                page.openPrintTermsModal(true, index, params, {})
            }
            
			break;
        case 'delete_line':  //删除
            promptBox({
                color:"warning",
                title: json['20023085-000024'],/* 国际化处理： 您确定要删除所选数据吗?*/
                // beSureBtnClick: API.deleteAction.bind(checkedRows, props)
                beSureBtnClick: ()=>{
                    page.deleteRow([record])
                }
            });
            // props.editTable.deleteTableRowsByIndex(tableId, index);
			break;
		case 'copy_line': //复制
            page.copyRow(record);
            // pasteRow(tableId, data, index)
			break;
        default:
            break;
    }
};
function getParamsByCode(code) {
    let printObject = {
        generalbook: {
            appcode: '20023010',
            searchId: '20023010query',
            noRadio: true, noCheckBox: true, showOrderunify: false
        },
        detailbook: {
            appcode: '20023030',
            searchId: '20023030query',
            noRadio: false, noCheckBox: false, showOrderunify: true
        },
        diarybook: {
            appcode: '20023035',
            searchId: '20023035query',
            noRadio: false, noCheckBox: false, showOrderunify: true
        },
        assdetail: {
            appcode: '20023060',
            searchId: '20023060query',
            noRadio: true, noCheckBox: false, showOrderunify: false
        },
        assbalance: {
            appcode: '20023055',
            searchId: '20023055query',
            noRadio: true, noCheckBox: true, showOrderunify: false
        }
    }
    for (const key in printObject) {
        if (printObject.hasOwnProperty(key)) {
            if(key === code){
                return printObject[key]          
            }
        }
    }
}
export { tableButtonClick, getParamsByCode };
