import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import { tableButtonClick, getParamsByCode }  from './tableButtonClick';
export { buttonClick, initTemplate, tableButtonClick, getParamsByCode };
