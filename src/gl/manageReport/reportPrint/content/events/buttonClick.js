import { promptBox, toast } from "nc-lightapp-front";
export default function buttonClick(props, id) {
	const tableId = 'reportPrintTable';
	let page = this
	let checkedRows = page.PrintTable.getCheckedRows()
	switch (id) {
		case 'add': //新增
			page.addRow()
			// props.editTable.addRow(tableId);
			break;
		case 'edit' :
			page.editTable()
        break;
		case 'delete': //删除		
			promptBox({
				color:"warning",
				title: page.state.json['20023085-000024'],/* 国际化处理： 您确定要删除所选数据吗?*/
				beSureBtnClick: ()=>{
					page.batchDelete(checkedRows)
				}
			});
			break;
		case 'save':  //保存
			page.saveTable()
			break;
		case "cancel": //取消
			promptBox({
				color:"warning",
				title: page.state.json['20023085-000008'],/* 国际化处理： 取消*/
				content: page.state.json['20023085-000025'],/* 国际化处理： 确定要取消吗?*/
				beSureBtnClick: ()=>{
					page.cancel()
				}
			});			
			break;
		case "print": //打印
			page.openDateRangeModal(checkedRows, false)
			break;
		case 'print_list': //打印列表
			page.ptintList()
			break;
		case 'refresh': //刷新
			let callback = () => {
				toast({ content: page.state.json['20023085-000026'], color: 'success' })/* 国际化处理： 刷新成功*/
			}
			page.reFresh(callback)
			break;
	}
}
