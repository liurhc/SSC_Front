/**
 * Created by liqiankun on 2018/6/26.
 * 管理报表 ---> 辅助属性余额表
 */

import React, {Component} from 'react';

import {high, base, ajax, createPage, getMultiLang, gzip, createPageIcon} from 'nc-lightapp-front';
const { NCDiv } = base;
import '../../css/index.less'
import HeadCom from '../../common/headSearch/headCom';
// import {titleLabels } from '../../common/headSearch/titledata';
import { SimpleTable } from 'nc-report';
import QueryModal from './components/MainSelectModal';
import {tableDefaultData} from '../../defaultTableData'
import reportPrint from '../../../public/components/reportPrint';
import { toast } from '../../../public/components/utils';
import {getDetailPort} from "../../common/modules/modules";
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import {setData} from "../../common/withPageDataSimbletable";
import {searchById} from "../../common/modules/createBtn";
import "../../../public/reportcss/firstpage.less";
import {handleSimpleTableClick} from "../../common/modules/simpleTableClick";
import PageButtonGroup from '../../common/modules/pageButtonGroup';
import {rowBackgroundColor} from "../../common/htRowBackground";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import HeaderArea from '../../../public/components/HeaderArea';
import { queryRelatedAppcode, originAppcode } from '../../../public/common/queryRelatedAppcode.js';
class AssistPropertyBalance extends Component{
    constructor(props){
        super(props);

        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
            textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            dataWithPage: [], //存放请求回来的数据信息，用于分页操作
            firstPageDisable: true, //true: 不能点击
            lastPageDisable: true,  //true: 不能点击
            queryDatas: {},
            accountType: 'amountcolumn',//金额式
            "fiveLables": []
        }
        this.switchCount = 0 ;// 转换列名下标计数
        this.dataPage = 0; //存放请求回来的数据椰树信息
        this.renderFirstData = {};
        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.handlePage = this.handlePage.bind(this);//页面处理事件
        // this.setData= this.setData.bind(this);// 整理表格所需要的数据格式
        this.handleValueChange = handleValueChange.bind(this)
        // this.changeSelectStyle = this.changeSelectStyle.bind(this);//选择 '账簿格式'事件
        this.searchById = searchById.bind(this);
        this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                "fiveLables": [
                    {
                        title: json['20028001-000036'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20028001-000037'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20028001-000038'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20028001-000039'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20028001-000040'],
                        styleClass:"m-brief"
                    }
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20028001','publiccommon', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        this.setState({
            flag: true,
        });
        let appceod = this.props.getSearchParam('c');
        this.searchById('20028001PAGE',appceod);
        this.props.button.setDisabled({
            print:true, linkvoucher:true,
            switch:true, directprint:true,
            saveformat:true
        })
        this.props.button.setButtonVisible(['first', 'pre', 'next', 'last'], false)
    }
    handlePage(param, dataWithPage){//页面控制事件
        let dataLength = dataWithPage.data.length;
        let renderData = [];
        let obj = {};
        switch(param){
            case 'firstPage':
                this.dataPage = 0;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                this.setState({
                    firstPageDisable: true,
                    lastPageDisable: false,
                    selectRowIndex: 0
                })
                setData(this, obj);
                return;
            case 'nextPage':
                this.dataPage += 1;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                this.setState({
                    firstPageDisable: false,
                    selectRowIndex: 0
                })
                if(this.dataPage === dataLength - 1){
                    this.setState({
                        lastPageDisable: true
                    })
                }
                setData(this, obj);
                return;
            case 'prePage':
                this.dataPage -=1;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                if(this.dataPage === dataLength - 2){
                    this.setState({
                        lastPageDisable: false,
                        selectRowIndex: 0
                    })
                }
                if(this.dataPage === 0){
                    this.setState({
                        firstPageDisable: true,
                        selectRowIndex: 0
                    })
                }
                setData(this, obj);
                return;
            case 'lastPage':
                this.dataPage = dataLength - 1;
                renderData = dataWithPage.data[this.dataPage];
                obj.column = dataWithPage.column;
                obj.data = renderData;
                obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
                obj['amountcolumn'] = dataWithPage.amountcolumn;
                this.setState({
                    lastPageDisable: true,
                    firstPageDisable: false,
                    selectRowIndex: 0
                })
                setData(this, obj);
                return;
        }
    }
    handleSwitchColumn(dataWithPage) {// 转换事件
        this.switchCount = ( this.switchCount + 1 ) % 3;
        let renderData = dataWithPage.data[this.dataPage];
        let obj = {};
        obj.column = dataWithPage.column;
        obj.data = renderData;
        obj['quantityamountcolumn'] = dataWithPage.quantityamountcolumn;
        obj['amountcolumn'] = dataWithPage.amountcolumn;
        setData(this, obj);
        return;
    }
    queryClick(){//点击"查询"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    modalSure(param){//点击弹框"确定"事件
        // 查询时控制参数恢复默认值
        this.props.button.setDisabled({
            print:false,
            switch:false, directprint:false,
            saveformat:false, refresh:false,
        })
        this.dataPage = 0; //存放请求回来的数据椰树信息
        this.setState({
            queryDatas: param,
            firstPageDisable: true,
            lastPageDisable: true
        })
        let url = '/nccloud/gl/accountrep/assattquerybalance.do';
        let self = this;
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, success } = response;
                if(success){
                    self.setState({
                        dataWithPage: {...data},
                        typeDisabled: false,//账簿格式 默认是禁用的
                        selectRowIndex:0
                    })
                    if(data.data.length > 1){
                        self.setState({
                            lastPageDisable: false
                        })
                    }
                    self.renderFirstData.column = data.column;
                    self.renderFirstData.data = data.data[0];
                    self.renderFirstData.amountcolumn = data.amountcolumn;
                    self.renderFirstData.quantityamountcolumn = data.quantityamountcolumn;
                    setData(self, self.renderFirstData);
                }else{
                    toast({content: this.state.json['20028001-000000'], color: 'warning'})/* 国际化处理： 返回数据为空！*/
                }
            },
            error:function(error){
                toast({ content: error.message, color: 'warning' });
                () => self.queryClick()
            }
        })
    }

    //直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    getDetailPort = (queryDatas) => {//联查"明细"
        let url = queryDatas.url; //'/nccloud/gl/accountrep/triaccquery.do';
        let gziptools = new gzip();
        let selectRow = this.getSelectRowData()[0].link;
        let data = {
            ...this.state.queryDatas,
            link: {...selectRow},
            pageindex: '1',
            "class": "nccloud.pubimpl.gl.assbalance.AssbalLinkDetailParamTransfer", // 接口实现类路径
        };
        this.jumpToDetail(gziptools.zip(data), queryDatas);
    }
    getSelectRowData=()=>{
        let selectRowData=this.refs.balanceTable.getRowRecord();
        return selectRowData;
    }
    jumpToDetail = (data, queryDatas) => {
        
        this.props.openTo(
            queryDatas.jumpTo,
            {appcode: queryDatas.appcode, status: JSON.stringify(data),id: '12wew24'}
        )
    }
    handleSaveColwidth=()=>{
        let info=this.refs.balanceTable.getReportInfo();
        let {json}=this.state
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.queryDatas.appcode,info.colWidths,json, callBack);
    }
    handleLoginBtn = (obj, btnName) => {
        if(btnName === 'print'){//打印

        }else if(btnName === 'linkvoucher'){//联查明细
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/assdetailquery.do',//src/gl/manageReport/assistDetailAccount/content
                jumpTo: '/gl/manageReport/assistDetailAccount/content/index.html',
                key: 'assistDetail',
                appcode: queryRelatedAppcode(originAppcode.assdetail)
            }, this.state.json)
        }else if(btnName === 'switch'){//转换
            this.handleSwitchColumn(this.state.dataWithPage)
        }else if(btnName === 'directprint'){//直接输出
            this.printExcel()
        }else if(btnName === 'saveformat'){//保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'refresh'){//刷新
            if (Object.keys(this.state.queryDatas).length > 0) {
                this.modalSure(this.state.queryDatas)
            }
        }else if(btnName === 'first'){//首页
            this.handlePage('firstPage', this.state.dataWithPage)
        }else if(btnName === 'pre'){//上一页
            this.handlePage('prePage', this.state.dataWithPage)
        }else if(btnName === 'next'){//下一页
            this.handlePage('nextPage', this.state.dataWithPage)
        }else if(btnName === 'last'){//末页
            this.handlePage('lastPage', this.state.dataWithPage)
        }
    }
    firstCallback = () => {//首页
        this.handlePage('firstPage', this.state.dataWithPage)
    }
    preCallBack = () => {//上一页
        this.handlePage('prePage', this.state.dataWithPage)
    }
    nextCallBack = () => {//下一页
        this.handlePage('nextPage', this.state.dataWithPage)
    }
    lastCallBack = () => {//末页
        this.handlePage('lastPage', this.state.dataWithPage)
    }
    render(){
        return(
            <div className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['20028001-000002']} /* 国际化处理： 辅助属性余额表*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <QueryModal
                                    onConfirm={(datas) => {
                                        this.modalSure(datas);
                                    }}
                                />
                            </div>
                            {//按钮集合
                                this.props.button.createButtonApp({
                                    area: 'btnarea',
                                    buttonLimit: 3,
                                    onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName)
                                })
                            }
                        </div>
                    }
                    pageBtnContent = {
                        <PageButtonGroup
                            first={{disabled: this.state.firstPageDisable, callBack: this.firstCallback}}
                            pre={{disabled: this.state.firstPageDisable, callBack: this.preCallBack}}
                            next={{disabled: this.state.lastPageDisable, callBack: this.nextCallBack}}
                            last={{disabled: this.state.lastPageDisable, callBack: this.lastCallBack}}
                        />
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.fiveLables.map((items, index) => {
                                return(
                                    <HeadCom
                                        labels={items.title}
                                        key={items.title+index}
                                        content={this.state.dataWithPage.data && this.state.dataWithPage.data[this.dataPage]}
                                        lastThre = {this.state.dataWithPage && this.state.dataWithPage.headtitle}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.renderFirstData))
                                        }
                                        accountType = {this.state.accountType}
                                        isLong = {items.styleClass}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data = {this.state.dataout}
                        onCellMouseDown = {(e, coord, td) => {
                            this.whetherDetail('link', 'assistDetailAccount');//assistDetailAccount
                            this.rowBackgroundColor(e, coord, td)
                        }}
                    />
                </div>
            </div>
        )
    }
}


AssistPropertyBalance = createPage({})(AssistPropertyBalance)

ReactDOM.render(<AssistPropertyBalance />,document.querySelector('#app'));
