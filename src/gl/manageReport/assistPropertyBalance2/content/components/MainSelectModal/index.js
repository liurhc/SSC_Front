import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax, deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import '../../../../css/searchModal/index.less';


const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input, NCTooltip: Tooltip
} = base;

import { toast } from '../../../../../public/components/utils';

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';
const Timeformat = "YYYY-MM-DD HH:mm:ss";

// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import AccPeriodDefaultTreeGridRef from '../../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../../uapbd/refer/org/BusinessUnitTreeRef';
import NewModal from '../modal'
import AccountDefaultModelTreeRef from '../../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../../public/components/uapRefer.js';
import {
    renderMoneyType,
    getSubjectVersion,
    returnMoneyType,
    createReferFn,
    getCheckContent, businessUnit, getReferDetault
} from '../../../../common/modules/modules';
import SubjectVersion from "../../../../common/modules/subjectVersion";
import DateGroup from '../../../../common/modules/dateGroup';
import CheckBoxCells from '../../../../common/modules/checkBox';
import {handleValueChange} from '../../../../common/modules/handleValueChange'
import FourCheckBox from '../../../../common/modules/fourCheckBox'

import initTemplate from '../../../../modalFiles/initTemplate';
import {handleNumberInput} from "../../../../common/modules/numberInputFn";
import ReferWraper from '../../../../../public/components/ReferWraper'
import FormLabel from '../../../../../public/components/FormLabel';
class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
		    json: {},
            dateInputPlaceholder: '', ///* 国际化处理： 选择日期*/
            supplierType: '', //第二层弹框中的'供应商类型'的值
            assistDateValue: '', //辅助参照中的日期的state值
            assistDateTimeValue: '', //辅助参照中的日期+时分秒的state值
            stringRefValue: '', //辅助参照中的字符的state值
            numberRefValue: '',//辅助参照中的数值的state值
            numberRefValueInteger: '',//辅助参照中整数值
            booleanValue: '', //辅助参照中布尔值
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            showRadio: true,//控制是否显示"会计期间"的但选框；false: 不显示
            appcode: '20028001',
            isShowUnit:false,//是否显示业务单元
            busiDate: '', //业务日期

            versiondate: '', //制单日期，后续要用下拉选
            versionDateArr: [], //存放请求账簿对应的版本数组


            /*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
            startyear: '', //开始年
            startperiod: '',  //开始期间
            endyear: '',  //结束年
            endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示

			showNewModal: false,
            renderData: [], //我的弹框里数据
			innerSelectName: [], //"查询范围"里存放的数据
            innerSelectData: [], //最里层选择数据的项
            rangefrom: '', //余额范围起始值
            rangeto: '', //余额范围结束值


            listItem:{},//业务单元中模板数据对应值

			selectedSort: 'true',//排序方式
			selectedShowType: 'false', //多主题显示方式
			doubleCell: false, // 多业务单元合并
			allLast: true, // 所有末级是否选中
			outProject: false, //表外科目是否选中
			multiProjectMerge: true, //多主题合并
			subjectShow: true, //按主题列示
			balanceMoneyLeft: '', //余额范围左边输入框
            balanceMoneyRight: '', //余额范围右边输入框
            pk_accperiodscheme: '',//会计期间参数
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accountingUnit: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //
				refname:'',
				refpk: '',
			},
			buSecond: [],
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表
			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次
			startcode: {
				refname:'',
				refpk: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
			},    //结束科目编码

			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			showzerooccur: false, //无发生不显示
			upLevelSubject: false, //显示上级科目
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: false, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			start: {},  //开始期间
			end: {}, //结束期间
			isversiondate: false,
            indexArr: [], //存放"查询对象"已经选择的索引


			//tabel组件头部
			//table的数据
			tableSourceData: [],

			//第二层弹框里的table
            secondData: [],//第二层表格的数据
            cloneSecondData: [],//第二层表格的数据(取消用)

            selectRow: [],//存放所选择的不是参照类型的数据
            page: 0//第二个弹框中要显示的行内容索引
		}
		this.balanceMoney = this.balanceMoney.bind(this);//余额范围输入触发事件

        this.sureHandle = this.sureHandle.bind(this);
        this.closeNewModal = this.closeNewModal.bind(this);
        this.outInputChange = this.outInputChange.bind(this);



        this.renderTableFirstData = this.renderTableFirstData.bind(this);// table渲染初始的数据
        this.handleSearchObj = this.handleSearchObj.bind(this);//查询对象 选择触发事件
		this.findByKey = this.findByKey.bind(this);
		this.renderSearchRange = this.renderSearchRange.bind(this);//根据'查询对象'的选择来对应渲染'查询范围'的组件
		this.renderRefPathEle = this.renderRefPathEle.bind(this);//根据传进来的url渲染对应的参照组件
		this.handleCheckbox = this.handleCheckbox.bind(this);//计算小计	包含下级复选框的点击事件
		this.secondModalSelect = this.secondModalSelect.bind(this);//第二层弹框的选择框
		this.renderNewRefPath = this.renderNewRefPath.bind(this);//渲染第二层弹框内容的参照

		this.handleValueChange = handleValueChange.bind(this);
		this.searchId = '20028001query';
        this.handleNumberInput = handleNumberInput.bind(this);
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                dateInputPlaceholder: json['20028001-000003'], ///* 国际化处理： 选择日期*/
                currtype: json['20028001-000004'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['20028001-000004'],  //选择的币种名称/* 国际化处理： 本币*/
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20028001', 'dategroup', 'checkbox', 'fourcheckbox', 'subjectversion', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
        this.renderTableFirstData(10);
    }

    componentDidMount() {
        setTimeout(() => getReferDetault(this, true, {
            businessUnit,
            getCheckContent
        }, 'assistPropertyBalance'),0);
        setTimeout(() => initTemplate.call(this,this.props), 0)
    }
    handleSearchObj(values, record){//查询对象选择具体科目
        let value = JSON.parse(values)
        //('handleSearchObj>>', value, record);
        let tempData = []
    	let { tableSourceData, selectRow } = this.state;
        let originData = this.findByKey(record.key, tableSourceData);
        originData.selectCell = value;
        record.selectCells = [];
        record.selectRange = [];
        if(value.refpath ){
            originData.type = 'referpath';
            originData.searchRange = value.refpath;
		}else if(value.datatype === '1'){//字符
            originData.type = 'string';
        } else if(value.datatype === '4'){//整数
            originData.type = 'integer';
        } else if(value.datatype === '31'){//数值(小数)
            originData.type = 'number';
        } else if(value.datatype === '32'){//布尔
            originData.type = 'boolean';
        } else if(value.datatype === '33'){//日期
            originData.type = 'date'
        } else if(value.datatype === '34'){//日期+时分秒时间
            originData.type = 'dateTime'
        } else if(value.attr){
            originData.selectCellAttr = [...value.attr];
            tempData = deepClone(value.attr)
            originData.type = 'modal'
		} else {
            originData.type = ''
		}
		let newSelectRow = [...selectRow];
        newSelectRow[record.key] = record;
    	this.setState({
            tableSourceData,
            selectRow: newSelectRow,
            secondData: tempData,
            cloneSecondData: tempData,
		})
	}
    handleCheckbox(key, record){//计算小计	包含下级复选框的点击事件
    	let {tableSourceData} = this.state;
        record[key] = record[key] === 'N' ? 'Y' : 'N';
        this.setState({
            tableSourceData
		})

	}
    secondModalSelect(record, index){//第二层弹框的复选框
    	//('secondModalSelect>', record);
    	let {tableSourceData} = this.state;
        let newSelect = !record.select;
        record.select = newSelect;

        // tableSourceData[record.itemKey].selectCellAttr[index] = record;
        // this.setState({tableSourceData})
	}
    findByKey(key, rows) {
        let rt = null;
        let self = this;
        rows.forEach(function(v, i, a) {
            if (v.key == key) {
                rt = v;
            }
        });
        return rt;
    }
    dateRefChange = (key, value, record) => {
        //('dateRefChange>',value, record);
        let length = record.selectCell.inputlength;
        let exeReg = new RegExp("^.{0," + length + "}$");
        let result = exeReg.exec(value);
        if(result && key === 'stringRefValue'){
            record.selectRange = result[0]
            this.setState({
                [key]: result[0]
            })
        }else {
            record.selectRange = value
            this.setState({
                [key]: value
            })
        }
    }
    numberRefChange = (key, value, record, param) => {
        //('numberRefChange>>', value, record);
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectRange = this.state[key], 0)

    }
    renderSearchRange(record, fieldid){//根据'查询对象'的选择来对应渲染'查询范围'的组件
    	//('renderSearchRange>', record);
    	switch(record.type){
			case "referpath":
				let refPathEle = this.renderRefPathEle(record, fieldid);
				//('refPathEle>>', refPathEle);
				return refPathEle;
            case 'date':
                return (
                    <DatePicker
                        fieldid={fieldid}
                        format={format}
                        value={this.state.assistDateValue}
                        placeholder={this.state.dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateValue', value, record)}
                    />
                );
            case 'string':
                return <Input
                    fieldid={fieldid}
                    value={this.state.stringRefValue}
                    onChange={(value) => this.dateRefChange('stringRefValue', value, record)}
                />;
            case 'number':
                return <Input
                    fieldid={fieldid}
                    value={this.state.numberRefValue}
                    onChange={(value) => this.numberRefChange('numberRefValue', value, record, 'assistBalance') }
                />;
            case 'integer'://整数
                return <Input
                    fieldid={fieldid}
                    value={this.state.numberRefValueInteger}
                    onChange={(value) => this.numberRefChange('numberRefValueInteger', value, record, 'assistBalanceInteger') }
                />;
            case 'boolean'://布尔
                return <Select
                    fieldid={fieldid}
                    value={this.state.booleanValue}
                    className='search-boolean'
                    onChange={(value)=>{
                        //('NCSelect>>>', value);
                        this.dateRefChange('booleanValue', value, record)
                    }}
                >
                    <NCOption value="Y">{this.state.json['20028001-000026']}</NCOption>{/* 国际化处理： 是*/}
                    <NCOption value="N">{this.state.json['20028001-000027']}</NCOption>{/* 国际化处理： 否*/}
                    <NCOption value="">{this.state.json['20028001-000028']}</NCOption>{/* 国际化处理： 空*/}
                </Select>;
            case 'dateTime'://日期+时分秒
                return (
                    <DatePicker
                        fieldid={fieldid}
                        showTime={true}
                        format={Timeformat}
                        value={this.state.assistDateTimeValue}
                        placeholder={this.state.dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateTimeValue', value, record)}
                    />
                );
			case 'modal':
                //('????', record, record.selectCell, record.selectCellAttr);
                let attrObj = record.selectCellAttr;
                let { tableSourceData } = this.state;
                let showArr = [];
                attrObj && attrObj.map((item) => {
                    if (item) {
                        item.itemKey = record.key;
                    }
                    if (item.select) {
                        let showitem = "【" + item.name + ":";
                        if (!item.selectName || item.selectName === '' || (Array.isArray(item.selectName) && item.selectName.length == 0)) { // 不选值，即为全选
                            showitem += "ALL";
                        } else if (item.enums) { // 枚举类型的，取枚举的name
                            item.enums.map((enumcell, index) => {
                                if (enumcell.value === item.selectName) {
                                    showitem += enumcell.name;
                                }
                            })
                        } else { // 参照的单选和多选
                            if (Array.isArray(item.selectName)) {
                                showitem += item.selectName.join(',');
                            } else {
                                showitem += item.selectName;
                            }
                        }
                        showitem += "】";
                        showArr.push(showitem);
                    }
                })
                //('showArr:::', showArr);
				return (
                    <ReferWraper
                        // outStyle={{width: '200px', marginLeft: '10px'}}
                        fieldid={fieldid}
                        placeholder = {record.selectCell && record.selectCell.name}
                        display={showArr.join('')}  //显示值
                        // disabled={dis}
                        onClick={() => this.setState({
                            showNewModal: !this.state.showNewModal,
                            page: record.key
                        })}
                    />
                    // <div style={{display: 'flex'}}>
                    //     <Input
                    //         style={{border: 'none'}}
					// 		placeholder={record.selectCell && record.selectCell.name}
					// 		value={showArr.join('')}
					// 	/>
                    //     <div className='ellipsisContainer'
                    //          onClick={() => this.setState({
                    //              showNewModal: !this.state.showNewModal,
                    //              page: record.key
                    //          })}
                    //     >
                    //         <span className='ellipsisCell'>.</span>
                    //         <span className='ellipsisCell'>.</span>
                    //         <span className='ellipsisCell'>.</span>
                    //     </div>
                    // </div>
				);
			default:
				return <Input fieldid={fieldid} disabled/>;
		}
	}
    renderNewRefPath(record, fieldid){
        //('renderNewRefPath>', record, record.queryname);
        let mybook;

        if(record.refpath){
            let objKey = record.attrclassid;
            if(!this.state[objKey]){//undefined
                //('createScript:', this.state);
                {createScript.call(this,record.refpath+".js",objKey)}

            }else {
                //('rendertableRight>:::', this.state, this.state[objKey]);
                let pkOrgParam = this.state.buSecond.length > 0 ? this.state.buSecond[0].refpk : this.state.unitValueParam; // this.state.accountingbook[0].nodeData ? this.state.accountingbook[0].nodeData.pk_corp : this.state.accountingbook[0].pk_corp;//;
                let options = {
                    fieldid: {fieldid},
                    value: record.selectCells,
                    isMultiSelectedEnabled: true,
                    "isShowDisabledData": true,
                    pk_defdoclist: record.classid? record.classid:"",
                    queryCondition: () => {
                        return {
                            "pk_accountingbook": this.state.accountingbook[0].pk_accountingbook ? this.state.accountingbook[0].pk_accountingbook : '',
                            "pk_org": pkOrgParam,
                            "isDataPowerEnable": 'Y',
                            "DataPowerOperationCode" : 'fi',
                            "dateStr": this.state.isversiondate ? this.state.versiondate : this.state.busiDate
                        }
                    },
                    onChange: (v) => {
                        let {tableSourceData} = this.state;
                        let refpkArr = [];
                        let selectName = [];
                        v.forEach((item) => {
                            //('vFormach::', item);
                            refpkArr.push(item.refpk);
                            selectName.push(item.refname)
                        })
                        record.selectCells = v
                        let pk_checkvalue = refpkArr.join(',')
                        record.pk_checkvalue = pk_checkvalue;
                        record.selectName = selectName;
                        record.refpk = refpkArr;
                        this.setState({
                            tableSourceData
                        })
                    }
                };
                let newOPtions = {};
                if(record.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                    //('recorddd>>???', record);
                    newOPtions = {
                        ...options,
                        "unitValueIsNeeded":false,
                        "isShowDimission":true,
                        queryCondition: () => {
                            return {
                                "pk_accountingbook": this.state.accountingbook[0].pk_accountingbook ? this.state.accountingbook[0].pk_accountingbook : '',
                                "pk_org": pkOrgParam,
                                "isDataPowerEnable": 'Y',
                                "DataPowerOperationCode" : 'fi',
                                "busifuncode":"all",
                                isShowDimission:true
                            }
                        },
                        isShowUnit:true,
                        unitProps:{
                            refType: 'tree',
                            refName: this.state.json['20028001-000015'], ///* 国际化处理： 业务单元*/
                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                            rootNode:{refname:this.state.json['20028001-000015'],refpk:'root'}, ///* 国际化处理： 业务单元*/
                            placeholder:this.state.json['20028001-000015'], ///* 国际化处理： 业务单元*/
                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                            treeConfig:{name:[this.state.json['20028001-000016'], this.state.json['20028001-000017']],code: ['refcode', 'refname']}, ///* 国际化处理： 编码,名称*/
                            isMultiSelectedEnabled: false,
                            //unitProps:unitConf,
                            isShowUnit:false
                        },
                        unitCondition:{
                            pk_financeorg: pkOrgParam,
                            'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                        },
                    }
                }else if(record.classid && record.classid.length === 20){
                    newOPtions = {
                        ...options,
                        pk_defdoclist: record.classid
                    }
                }
                // else if(record.classid &&record.classid=='eae040f4-3c88-413d-abc9-b15774463d46'){
                //     newOPtions = {
                //         ...options,
                //         pk_defdoclist: record.classid
                //     }
                // }
                else {
                    newOPtions = {
                        ...options
                    }
                }
                mybook = (
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state[objKey] ? (this.state[objKey])(newOPtions) : <div/>
                            }
                        </Col>
                    </Row>
                );
            }
		}else if(record.enums){
            return (
                <Select
                    fieldid={fieldid}
                    value={this.state.supplierType}
                    onChange={(value) => {
                        //('vvvvalue::', value);
                        record.pk_checkvalue = value;
                        record.selectName = value;
                        record.refpk = value;
                        this.setState({
                            supplierType: value
                        })
                    }}
                >
                    <NCOption value='' ></NCOption>
                    {record.enums.map((item, index) => {
                        return <NCOption value={item.value} key={index} >{item.name}</NCOption>
                    })}
                </Select>
            )
        }

        return mybook;
	}
	renderRefPathEle(record, fieldid){//根据传进来的url渲染对应的参照组件
    	//('renderRefPathEle>',record, record.selectCell);
        //带有参照的
		//('state>>>>',record, record.selectCell, record.selectCell.pk_checktype, !this.state[record.selectCell.pk_checktype]);
		let mybook;
		let objKey = record.key + record.selectCell.pk_checktype;
		if(!this.state[objKey]){//undefined
			//('createScript:', this.state);
			{createScript.call(this,record.selectCell.refpath+".js",objKey)}

		}else{
			//('rendertableRight>:::', this.state, this.state[objKey]);
            let pkOrgParam = this.state.buSecond.length > 0 ? this.state.buSecond[0].refpk : this.state.unitValueParam; //this.state.accountingbook[0].nodeData ? this.state.accountingbook[0].nodeData.pk_corp : this.state.accountingbook[0].pk_corp;
            let options = {
                fieldid: fieldid,
                value: record.selectCells,
                isMultiSelectedEnabled:true,
                "isShowDisabledData": true,
                queryCondition:() => {
                    return {
                        "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                        "pk_org": pkOrgParam,
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi',
                        "dateStr": this.state.isversiondate ? this.state.versiondate : this.state.busiDate
                    }
                },
                onChange: (v)=>{
                    //('innerChaa:', v, this.state.tableSourceData);
                    let {tableSourceData} = this.state;
                    let refpkArr = [];
                    let selectCell = [];
                    v.forEach((item)=>{
                        //('vFormach::', item);
                        refpkArr.push(item.refpk);
                        selectCell.push(item);
                    })
                    let pk_checkvalue = refpkArr.join(',')
                    record.selectRange = pk_checkvalue;
                    record.selectCells = selectCell
                    this.setState({
                        tableSourceData
                    })
                }
            };
            let newOPtions = {}
            if(record.selectCell.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.selectCell.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                //('recorddd>>???', record);
                newOPtions = {
                    ...options,
                    "unitValueIsNeeded":false,
                    queryCondition:() => {
                        return {
                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                            "pk_org": pkOrgParam,
                            "isDataPowerEnable": 'Y',
                            "DataPowerOperationCode" : 'fi',
                            "dateStr": this.state.isversiondate ? this.state.versiondate : this.state.busiDate,
                            "busifuncode":"all",
                            isShowDimission:true
                        }
                    },
                    isShowUnit:true,
                    unitProps:{
                        refType: 'tree',
                        refName: this.state.json['20028001-000015'], ///* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:this.state.json['20028001-000015'],refpk:'root'}, ///* 国际化处理： 业务单元*/
                        placeholder:this.state.json['20028001-000015'], ///* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[this.state.json['20028001-000016'], this.state.json['20028001-000017']],code: ['refcode', 'refname']}, ///* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false
                    },
                    unitCondition:{
                        pk_financeorg: pkOrgParam,
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    },
                }
            }else if(record.selectCell.classid && record.selectCell.classid.length === 20){
                newOPtions = {
                    ...options,
                    pk_defdoclist: record.selectCell.classid
                }
            }else {
                newOPtions = {
                    ...options
                }
            }
            mybook =  (
				<Row className="tableselectItem">
					<div>
						{
							this.state[objKey]?(this.state[objKey])(newOPtions):<div/>
						}
					</div>
				</Row>
			);
		}
		return mybook;
	}
    renderTableFirstData(length){
    	let firstData = [];
    	for(let i=0; i<length; i++){
    		let obj = {};
    		obj.searchObj = [];
            obj.searchRange = '';
            obj.showLocal = 'N';//显示位置
            obj.accele = 'N';//小计
            obj.includesub = 'N',//包含下级
			obj.key = i
            firstData.push(obj);
		}
		this.setState({
            tableSourceData: [...firstData]
		})
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}




	getCurrtypeList = () => {//获取"币种"接口
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {"localType":"1", "showAllCurr":"1"};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


    handleCurrtypeChange = (keyParam, value) => {
        this.setState({
            [keyParam]: value,
        });
    }


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(v) {
		// //(this.state.accountingbook)

		if (!this.state.accountingbook || !this.state.accountingbook[0]) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}
		
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
                //('reresponsese::',res)
		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	

		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        		versiondate: '2018-5-31', //data.versiondate,
		        	},)
                    //('response.data:::')


                    getSubjectVersion(self);

                    let newUrl = '/nccloud/gl/accountrep/assattqueryobject.do';//辅助属性余额表查询对象
                    let newData = {
                        "pk_accountingbook":v[0].refpk,
                        "versiondate": '2018-5-31',//self.state.versiondate,
                    };
                    //('newData>>', newData);
                    ajax({
                        url:newUrl,
                        data:newData,
                        success: function(response){
                            const { success } = response;
                            //渲染已有账表数据遮罩
                            if (success) {
                                if(response.data){
                                    //('response:::>>', response.data);
                                    response.data.map((item) => {
                                    	if(item.attr){
                                            item.attr.map((cell) => {
                                                cell.select = false
											})
										}
									})

									//('response:::>>22:', response.data);
                                    let newTableFirstData = [...self.state.tableSourceData];
                                    newTableFirstData.forEach((item, index) => {
                                        //('newTableFirstData:', item);
                                        item.searchObj = response.data;
									})

                                    self.setState({
                                        tableSourceData: newTableFirstData
                                    })
                                }
                            }
                        }
                    });
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		//(value)
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		//(value)
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
    	//('handleVersiondateChange>', value);
		this.setState({
			versiondate: '2018-5-31'// value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    //(e);
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化
	onChangeShowzerooccur = (e, key) => {
		this.setState({
			[key]: e
		});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}
	//显示上级科目复选框变化
	onChangeupLevelSubject(e) {
		this.setState({upLevelSubject: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}
	handleRadioSort(param){
		//('handleRadioSort>', param);
		this.setState({
			selectedSort: param
		})
	}
	handleShowType(param){
		this.setState({
			selectedShowType: param
		})
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
	onChangeDoubleCell(param){
		this.setState({
			doubleCell: !this.state.doubleCell
		})
	}
	changeProjectStyle(param){//所有末级   表外科目选择
		if(param === 'allLast'){
			this.setState({
				allLast: !this.state.allLast
			})
		}else{
			this.setState({
				outProject: !this.state.outProject
			})
		}
	}

	handleGTypeChange =() =>{
		
	};


    outInputChange(param){
    	//('outInputChange>', param);
    	let nameArr = [];
        param.forEach((item) => {
            nameArr.push(item.refname);
		})
		this.setState({
            innerSelectName: [...nameArr]
		})
	}
    sureHandle(param){
    	//('sureHandleParam:', param);
        let SelectCell = [];
        let { tableSourceData, secondData, page } = this.state
        let tempCloneData = deepClone(secondData)
        tableSourceData[page].selectCellAttr = secondData;
        this.setState({
			innerSelectData: [...SelectCell],
            showNewModal: !this.state.showNewModal,
            // page: param.key,
            tableSourceData,
            cloneSecondData: tempCloneData
		})
	}
    closeNewModal(param){
        let { tableSourceData, cloneSecondData, page } = this.state
        let tempData = deepClone(cloneSecondData)
        tableSourceData[page].selectCellAttr =  cloneSecondData
		this.setState({
            showNewModal: !this.state.showNewModal,
            tableSourceData,
            secondData: tempData
		})
    }
    balanceMoney(value, param){
        let regExp = /^(\-)?\d*\.{0,1}\d{0,4}$/
        let result = regExp.exec(value)
    	if(result){
            this.setState({
                [param]: result[0]
            })
        }

	}

    handleDateChange = (value) => {//日期: 范围选择触发事件
        //('handleDateChange:', value);
        this.setState({
            begindate: value[0],
            enddate: value[1],
            rangeDate: value
        })
    }

	renderModalList = () => {
        //('renderModalList>>', this.state)
        this.columns = [
            {
                title: (<div fieldid='searchObj'>{this.state.json['20028001-000005']}</div>),/* 国际化处理： 查询对象*/
                dataIndex: "searchObj",
                key: "searchObj",
                width: '120px',
                render: (text, record, index) => {
                    //('columns>>>>', text, record);
                    let renderArr = [];
                    text.map((item, index) => {
                        renderArr.push(<NCOption value={JSON.stringify(item)} key={index}>{item.name}</NCOption>)
                    })
                    return (
                        <Select
                            showClear={false}
                            fieldid='searchObj'
                            value={record.selectCell && record.selectCell.name}
                            onChange = {(value) => this.handleSearchObj(value, record)}
                        >
                            {
                                renderArr
                            }
                        </Select>
                    )
                }
            },
            {
                title: (<div fieldid='searchRange'>{this.state.json['20028001-000006']}</div>),/* 国际化处理： 查询范围*/
                dataIndex: "searchRange",
                key: "searchRange",
                width: '120px',
                render: (text, record,index) => {
                    //('searchRangetext:::',record, record.type);
                    let renderEle = this.renderSearchRange(record,'searchRange');
                    return renderEle;
                }
            },
            {
                title: (<div fieldid='showLocal'>{this.state.json['20028001-000007']}</div>),/* 国际化处理： 显示位置*/
                dataIndex: "showLocal",
                key: "showLocal",
                width : '100px',
                render: (text, record) => {
                    //('showLocal>>>>', text, record)
                    return (
                        <Select
                            showClear={false}
                            fieldid='showLocal'
                            defaultValue = 'N'
                            value={record.showLocal}
                            onChange={(value) => {
                                //('showLocal:>>>',value);
                                record.showLocal = value;
                                this.setState({})
                            }}>
                            <NCOption value='Y'>{this.state.json['20028001-000024']}</NCOption>{/* 国际化处理： 表头*/}
                            <NCOption value='N'>{this.state.json['20028001-000025']}</NCOption>{/* 国际化处理： 表体*/}
                        </Select>
                    )
                }
            },
            {
                title: (<div fieldid='accele'>{this.state.json['20028001-000008']}</div>),/* 国际化处理： 计算小计*/
                dataIndex: "accele",
                key: "accele",
                width : 60,
                render: (text, record) => {
                    return <div fieldid='accele'><Checkbox
                        checked={record.showLocal === 'Y' ?  false : (record.accele === 'N' ? false : true) }
                        disabled={ record.selectCell &&  record.selectCell.name != ''  ?
                            (record.showLocal === 'Y' ? true : false) :
                            true
                        }
                        onChange={() => {
                            //('acceleChange:',record);
                            this.handleCheckbox('accele',record)
                        }}
                    /></div>
                }
            },
            {
                title: (<div fieldid='includesub'>{this.state.json['20028001-000009']}</div>),/* 国际化处理： 包含下级*/
                dataIndex: "includesub",
                key: "includesub",
                width : 60,
                render: (text, record) => {
                    return <div fieldid='includesub'><Checkbox
                        checked={record.includesub === 'N' ? false : true}
                        disabled={
                            record.selectCell &&  record.selectCell.name != ''  ?
                                (record.searchRange === 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index' ? true : false)
                                : true
                        }
                        onChange={() => {
                            //('includesub:', record);
                            this.handleCheckbox('includesub',record)
                        }}
                    /></div>
                }
            }
        ];
        this.secondColumn = [
            {
                title: (<div fieldid='select'>{this.state.json['20028001-000010']}</div>),/* 国际化处理： 选择*/
                dataIndex: "select",
                key: "select",
                render: (text, record, index) => {
                    //('secondColumn>', text, record)
                    return  <div fieldid='select'><Checkbox
                        checked={record.select}
                        onChange={() => {
                            //('acceleChange:',record);
                            record.index = index;
                            this.secondModalSelect(record, index);
                        }}
                    /></div>
                }
            },
            {
                title: (<div fieldid='name'>{this.state.json['20028001-000011']}</div>),/* 国际化处理： 查询属性*/
                dataIndex: "name",
                key: "name",
                render: (text, record, index) => <div fieldid='name'>{text ? text : <span>&nbsp;</span>}</div>
            },
            {
                title: (<div fieldid='refpath'>{this.state.json['20028001-000012']}</div>),/* 国际化处理： 查询值*/
                dataIndex: "refpath",
                key: "refpath",
                render: (text, record) => {
                    //('refpath>>>>', text, record);
                    let renderSecondEle = this.renderNewRefPath(record, 'refpath');
                    return renderSecondEle;
                }
            }
        ];
        return (
            <div className='right_query'>
                <div className='query_body1'>
                    <div className='query_form'>
    		<div>
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel isRequire={true} labelname={this.state.json['20028001-000029']}/>{/* 国际化处理： 核算账簿*/}
                    </Col>
                    <Col md={10} sm={10}>
                        <div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: true,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit ,
                                        getCheckContent: getCheckContent,
                                        renderTableFirstData: () => this.renderTableFirstData(9)
                                    },
                                    'assistPropertyBalance'
                                )
                            }
                        </div>
                    </Col>
                </Row>

                {/*启用科目版本：*/}
                <SubjectVersion
                    checked = {this.state.isversiondate}//复选框是否选中
                    data={this.state.versionDateArr}//
                    value={this.state.versiondate}
                    disabled={!this.state.isversiondate}
                    renderTableFirstData = {() => this.renderTableFirstData(9)}
                    handleValueChange = {(key, value) => this.handleValueChange(key, value, 'assistPropertyBalance', getCheckContent)}
                />

                <div className="modalTable">
                    <Table
                        columns={this.columns}
                        data={this.state.tableSourceData}
                    />
                </div>

                {/*会计期间*/}
                <DateGroup
                    selectionState = {this.state.selectionState}
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook[0]}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                />

                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel labelname={this.state.json['20028001-000030']}/>{/* 国际化处理： 余额范围*/}
                    </Col>
                    <Col md={3} sm={3}>

                        <div className="book-ref">
                            <Select
                                showClear={false}
                                fieldid='balanceori'
                                value={this.state.balanceori}
                                // style={{ width: 180, marginRight: 6 }}
                                onChange={this.handleBalanceoriChange.bind(this)}
                            >
                                <NCOption value={'-1'} key={'-1'} >{this.state.json['20028001-000031']}</NCOption>{/* 国际化处理： 双向*/}
                                <NCOption value={'0'} key={'0'} >{this.state.json['20028001-000032']}</NCOption>{/* 国际化处理： 借*/}
                                <NCOption value={'1'} key={'1'} >{this.state.json['20028001-000033']}</NCOption>{/* 国际化处理： 贷*/}
                                <NCOption value={'3'} key={'3'} >{this.state.json['20028001-000034']}</NCOption>{/* 国际化处理： 平*/}
                            </Select>
                        </div>
                    </Col>

                    <Col md={1} sm={1}></Col>
                    <Col md={6} sm={6}>
                        <Col md={5} >
                            <Input
                                fieldid='startBalance'
                                value={this.state.balanceMoneyLeft}
                                onChange={(value) => this.balanceMoney(value, 'balanceMoneyLeft')}
                            />

                        </Col>
                        <Col md={2} style={{lineHeight: '36px'}}><p className='zhi nc-theme-form-label-c'>~</p></Col>
                        <Col md={5} >
                            <Input
                                fieldid='endBalance'
                                value={this.state.balanceMoneyRight}
                                onChange={(value) => this.balanceMoney(value, 'balanceMoneyRight')}
                            />
                        </Col>
                    </Col>
                </Row>

                {/*忽略日期选项*/}

                {/*包含凭证：*/}
                <CheckBoxCells
                    totalTitle= {this.state.json['20028001-000018']} ///* 国际化处理： 显示属性：*/
                    paramObj = {{
                        "noChargeVoucher": {//未记账凭证
                            show: true,
                            checked: this.state.includeuntally,
                            onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                        },
                        "errorVoucher": {//错误凭证
                            show: true,
                            checked: this.state.includeerror,
                            onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                        },
                        "harmToBenefitVoucher": {//损益结转凭证
                            show: true,
                            checked: this.state.includeplcf,
                            onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                        },
                        "againClassifyVoucher": {//重分类凭证
                            show: true,
                            checked: this.state.includerc,
                            onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                        },
                    }}
                />


                {/*币种*/}
                {renderMoneyType(this.state.currtypeList,this.state.currtypeName, this.handleCurrtypeChange, this.state.json)}

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
                        this.state.json
                    )
                }

                <FourCheckBox
                    totalTitle= {this.state.json['20028001-000018']} ///* 国际化处理： 显示属性：*/
                    renderCells={
                        [
                            {
                                title: this.state.json['20028001-000019'],/* 国际化处理： 无发生不显示*/
                                checked: this.state.showzerooccur,
                                id: 'showzerooccur',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'showzerooccur')
                            }
                        ]
                    }
                />

                <NewModal
                    title= {this.state.json['20028001-000020']} ///* 国际化处理： 属性选择*/
                    // record={this.state.selectRow[this.state.page]}
                    record={this.state.secondData}
                    column={this.secondColumn}
                    showNewModal = {this.state.showNewModal}//控制设否显示
                    closeNewModal = {this.closeNewModal}
                    sureHandle = {this.sureHandle}
                    sureText = {this.state.json['20028001-000021']} ///* 国际化处理： 确定 */
                    cancleText = {this.state.json['20028001-000014']} ///* 国际化处理： 取消*/
                />
			</div>
                    </div>
                </div>
            </div>
		)
	}
    clickPlanEve = (value) => {
        //('clickPlanEve:???', 'value>',this, value, '>')
        this.state = deepClone(value.conditionobj4web.nonpublic);
        //('clickPlanEve:???12', this.state)
        this.setState(this.state)
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
        //('saveSearchPlan>>', this.state)
        return {...this.state}
    }
    clickSearchBtn = () => {//点击查询框的"查询"按钮事件
    	let pk_accountingbook = [];
		let pk_unit = [];
		let moneyLeft = Number(this.state.balanceMoneyLeft);
		let moneyRight = Number(this.state.balanceMoneyRight);
		//('moneyyyy::', moneyLeft,  moneyRight, moneyLeft < moneyRight)
		if(moneyLeft > moneyRight){
			toast({
				content: this.state.json['20028001-000022'],/* 国际化处理： 余额范围查询条件信息输入有误(起始值大于终止值或是为负值)！*/
				color: 'warning'
			});
			return;
		}
		let lastAssvos = [];
		this.state.tableSourceData.forEach((item)=>{
			//('assvos>>>',item);
			let resultObj = {};
			if(item.selectCell && !item.selectCellAttr){
				resultObj.name = item.selectCell.name;
				resultObj.pk_checktype = item.selectCell.pk_checktype;
				resultObj.pk_checkvalue = Array.isArray(item.selectRange) ? item.selectRange.join(',') : item.selectRange;
				resultObj.headele = item.showLocal;
				resultObj.accele = item.accele;
				resultObj.includesub = item.includesub;
				lastAssvos.push(resultObj)
			} else if(item.selectCell && item.selectCellAttr){
				resultObj.name = item.selectCell.name;
				resultObj.pk_checktype = item.selectCell.pk_checktype;
				resultObj.attr = [];
				resultObj.headele = item.showLocal;
				resultObj.accele = item.accele;
				resultObj.includesub = item.includesub;
				item.selectCellAttr.map((itemCell) => {
					if(itemCell.select){
						resultObj.attr.push(itemCell);
					}
				})
				lastAssvos.push(resultObj)
			}
		})
		//('lastAssvos>>', lastAssvos)
		if (Array.isArray(this.state.accountingbook)) {
			this.state.accountingbook.forEach(function (item) {
				pk_accountingbook.push(item.refpk)
			})
		}

		if (Array.isArray(this.state.buSecond)) {
			this.state.buSecond.forEach(function (item) {
				pk_unit.push(item.refpk)
			})
		}
		//
        //('new-pk_accountingbook:', pk_accountingbook);
        let data = {
            pk_accountingbook: pk_accountingbook,
            pk_unit,
            multbusi: this.state.multbusi,
            usesubjversion: this.state.isversiondate + '',// 使用科目版本
            versiondate: this.state.isversiondate ? this.state.versiondate : this.state.busiDate, //this.state.versiondate,//科目版本
            // pk_accasoa://条件框选择科目
            startcode: this.state.startcode.refpk,  //开始科目编码
            endcode: this.state.endcode.refpk,    //结束科目编码
            startlvl: this.state.startlvl,
            endlvl: this.state.endlvl,
            isleave: this.state.allLast ? 'Y' : 'N',
            isoutacc: this.state.isoutacc ? 'Y' : 'N',
            startyear: this.state.startyear,
            endyear: this.state.endyear,
            startperiod: this.state.startperiod,
            endperiod:   this.state.endperiod,

            startdate: this.state.rangeDate[0],
            enddate: this.state.rangeDate[1],

            //未记账、错误、损益结转、重分类  Y/N  注意：‘包含凭证’是个标题，后面的才是选项
            includeuntally: this.state.includeuntally ? 'Y' : 'N',
            includeerror: this.state.includeerror ? 'Y' : 'N',
            includeplcf: this.state.includeplcf ? 'Y' : 'N',
            includerc: this.state.includerc ? 'Y' : 'N',
            pk_currtype: this.state.currtype,
            returncurr: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局
            mutibook: this.state.mutibook,
            showzerooccur: this.state.showzerooccur ? 'Y' : 'N',
            showzerobalanceoccur: this.state.showzerobalanceoccur ? 'Y' : 'N',
            currplusacc: this.state.currplusacc,
            sumbysubjtype: this.state.sumbysubjtype ? 'Y' : 'N',
            balanceori: this.state.balanceori,
            twowaybalance: this.state.twowaybalance ? 'Y' : 'N',
            istree: 'Y',
            ass: lastAssvos,
            rangefrom: this.state.balanceMoneyLeft,
            rangeto: this.state.balanceMoneyRight,
            querybyperiod: this.state.selectionState,//是选择会计期间还是日期
            appcode: this.state.appcode
        }
        
        let url = '/nccloud/gl/accountrep/checkparam.do'
        let flagShowOrHide;
        ajax({
            url,
            data,
            async: false,
            success: function (response) {
                flagShowOrHide = true
            },
            error: function (error) {
                flagShowOrHide = false
                toast({ content: error.message, color: 'warning' });
            }
        })
        if (flagShowOrHide == true) {
            this.props.onConfirm(data)
        } else if (flagShowOrHide == false) {
            return true;
        }
    }

	render() {
        let { search } = this.props;
        let { NCCreateSearch } = search;
        return <div>
            {
                NCCreateSearch(this.searchId, {
                    onlyShowSuperBtn:true,                       // 只显示高级按钮
                    replaceSuperBtn:this.state.json['20028001-000013'],                      // 自定义替换高级按钮名称/* 国际化处理： 查询*/
                    replaceRightBody:this.renderModalList,
                    hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
                    clickPlanEve:this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                    saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
                    oid:this.props.meta.oid,
                    clickSearchBtn: this.clickSearchBtn,  // 点击按钮事件
                    isSynInitAdvSearch: true,//渲染右边面板自定义区域
                })}
        </div>
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)

export default CentralConstructorModal;
