import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
import {high,base,ajax } from 'nc-lightapp-front';
const {
    NCModal: Modal,
    NCButton: Button,
    NCTable: Table
} = base;



import '../../../../css/referModal/index.less';
import createScript from "../../../../../public/components/uapRefer";

class NewModal extends Component{
    constructor(props){
        super(props);
        this.state = {
            tableData: [],
            indexArr: [],
            accountingbook: [],
            assvos: [],
            innerSelect: []
        }
    }

    
    render(){
        let {
            title, showNewModal,
            closeNewModal, sureHandle,
            sureText, cancleText,
            record, column
        } = this.props;
        return(
            <Modal
                fieldid='attributes'
                show = { showNewModal }
                onHide = { () => closeNewModal() } >
                <Modal.Header>
                    <Modal.Title>{title}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Table
                        columns={column} 
                        // data={record && record.selectCellAttr}
                        data={record}
                    />
                </Modal.Body>

                <Modal.Footer>
                    <Button 
                    fieldid='confirm'
                    onClick={ () => {
                        sureHandle(record);
                    }} colors="primary">
                        {sureText}
                    </Button>
                    <Button
                        fieldid='cancel'
                        className= 'btn-2 btn-cancel'
                        onClick={ () => closeNewModal(record) }
                        shape="border"
                    >
                        {cancleText}
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default NewModal;
