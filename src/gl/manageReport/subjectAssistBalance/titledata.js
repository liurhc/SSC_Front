/**
 * Created by liqiankun on 2018/6/23.
 */
export let titleLabels = {
  "fiveLables": [
    this.state.json['20028003-000000'], this.state.json['20028003-000001'], this.state.json['20028003-000002'], this.state.json['20028003-000003'], this.state.json['20028003-000004']/* 国际化处理： 期间,币种,核算账簿,账簿格式,对象*/
  ],
  "fourLables": [
      {
          title:this.state.json['20028003-000002'],/* 国际化处理： 核算账簿*/
          styleClass:"m-long"
      },
      {
          title:this.state.json['20028003-000000'],/* 国际化处理： 期间*/
          styleClass:"m-brief"
      },
      {
          title:this.state.json['20028003-000001'],/* 国际化处理： 币种*/
          styleClass:"m-brief"
      },
      {
          title:this.state.json['20028003-000003'],/* 国际化处理： 账簿格式*/
          styleClass:"m-brief"
      }
  ]
}
