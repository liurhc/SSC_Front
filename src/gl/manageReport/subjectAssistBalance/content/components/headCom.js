/**
 * Created by liqiankun on 2018/6/23.
 */
import React, {Component} from 'react';
import {high, base} from 'nc-lightapp-front';
import '../index.less'
const {
  NCInput: Input,
  NCSelect:Select,
} = base;
class HeadCom extends Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }

  renderInput(type, fn){
    switch (type){
      case this.state.json['20028003-000003']:/* 国际化处理： 账簿格式*/
        return <Select
            onChange={(value) => fn(value)}
            defaultValue="account"
        >
          <Select.NCOption value="account">{this.state.json['20028003-000037']}</Select.NCOption>{/* 国际化处理： 数量金额式*/}
          <Select.NCOption value="money">{this.state.json['20028003-000038']}</Select.NCOption>{/* 国际化处理： 金额式*/}
        </Select>;
      default:
        return <Input disabled/>;
    }
  }

  render(){
    //('>>>>', this.props.labels)
    let { labels, key, changeSelectStyle} = this.props;
    return(
      <div className="cellContainer" key={key}>
        <div className="labelName">
          <label>{labels}</label>
        </div>
        <div>
          {this.renderInput(labels, changeSelectStyle)}
        </div>
      </div>
    )
  }

}

export default HeadCom
