import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import '../../../css/searchModal/index.less';


const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input,
} = base;

import { toast } from '../../../../public/components/utils';

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';
const Timeformat = "YYYY-MM-DD HH:mm:ss";

// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import AccPeriodDefaultTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';


import AccountDefaultModelTreeRef from '../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../public/components/uapRefer.js';

import {
    businessUnit,
    createReferFn,
    getCheckContent, getReferDetault,
    renderMoneyType,
    renderRefer, returnMoneyType
} from '../../../common/modules/modules';
import { ajaxPromise } from '../../../common/netWork'
import SubjectVersion from "../../../common/modules/subjectVersion";
import DateGroup from '../../../common/modules/dateGroup'
import { subjectRefer } from '../../../referUrl'
import {ajaxData, clearTransterData, handleChange, handleSelectChange} from "../../../common/modules/transferFn";
import SubjectLevel from '../../../common/modules/subjectAndLevel'
import CheckBoxCells from '../../../common/modules/checkBox';
import {handleValueChange} from '../../../common/modules/handleValueChange'
import FourCheckBox from '../../../common/modules/fourCheckBox'
import initTemplate from '../../../modalFiles/initTemplate';
import {handleNumberInput} from "../../../common/modules/numberInputFn";
import {getRecover} from "../../../common/modules/recoverData";
import FormLabel from '../../../../public/components/FormLabel';
import { FICheckbox } from '../../../../public/components/base';
class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
            appcode: "20028003",
			json: {},
            dateInputPlaceholder: '',/* 国际化处理： 选择日期*/
            assistDateValue: '', //辅助参照中的日期的state值
            assistDateTimeValue: '', //辅助参照中的日期+时分秒的state值
            stringRefValue: '', //辅助参照中的字符的state值
            numberRefValue: '',//辅助参照中的数值的state值
            numberRefValueInteger: '',//辅助参照中整数值
            booleanValue: '', //辅助参照中布尔值
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            selectedKeys: [],
            targetKeys: [],
            changeParam: true, //false:点击条件时不触发接口，,
            rightSelectItem: [], //科目的"条件"按钮中存放右侧数据的pk值


            showRadio: false,//控制是否显示"会计期间"的但选框；false: 不显示
            versionDateArr: [], //制单日期，后续要用下拉选
            versiondate: '',//选择的科目版本日期值
            busiDate: '', //业务日期
            isShowUnit:false,//是否显示业务单元

            //table的数据
            tableSourceData: [],
            selectRow: [],//存放所选择的不是参照类型的数据
			listItem:{},//业务单元中模板数据对应值

			selectedSort: 'Y',//排序方式
			selectedShowType: 'false', //多主体显示方式
			doubleCell: false, // 多业务单元合并
			allLast: true, // 所有末级是否选中
			outProject: false, //表外科目是否选中
			multiProjectMerge: true, //多主体合并
			subjectShow: true, //按主体列示
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accountingUnit: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: [], //业务单元
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表
			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次
			startcode: {
				refname:'',
				refpk: '',
				refcode: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
				refcode: '',
			},    //结束科目编码
            buiDate: '', //业务日期
			isleave: true, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			upLevelSubject: false, //显示上级科目
			disSumbysubjtype: false, //科目类型小计是否禁用


			start: {},  //开始期间
			end: {}, //结束期间
            /*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
            startyear: '', //开始年
            startperiod: '',  //开始期间
            endyear: '',  //结束年
            endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示


			isversiondate: false,
            assvos: [],   //辅助核算对象数组 AssVO[]
			lastAssvos: [],//辅助核算对象整理后的数据
			tableData:[], //辅助核算表体左侧默认9个数据
			tableRightData: [], //辅助核算表体右侧默认9个数据
            pk_accperiodscheme: '', //会计期间参照的参数


            /**多主体显示方式*/
            mutibook: 'N', //多主体显示方式的选择值
            disMutibook: true, //多主体显示是否禁用
            /**排序方式*/
            currplusacc: 'Y', //排序方式
            disCurrplusacc: true, //排序方式是否禁用
            /**1、无发生不显示*/
            showzerooccurValue: false,//无发生不显示的选择状态

            showzerooccur: false, //控制 无发生不显示 无余额无发生不显示 是否可编辑
            /**2、无余额无发生不显示*/
            showzerobalanceoccurValue: true, //无余额无发生不显示选择的状态
            showzerobalanceoccur: false, //控制'无余额无发生不显示'是否可编辑

            /**3、显示科目类型小计*/
            sumbysubjtype: false, //显示科目类型小计
            editSubjectTotal: false, //'显示科目类型小计'是否可编辑

            /**按借贷方显示余额*/
            twowaybalance: false, //借贷方显示余额
            debitAndCredit: false,//按借贷显示余额是否禁止

            /**多业务单元合并*/
            showSecond: false, //是否显示二级业务单元
            multbusi: false, //多业务单元复选框
			
		}

		this.handleTableSelect = this.handleTableSelect.bind(this);//辅助核算表格中的⌚️
		this.renderRefer = renderRefer.bind(this);
		this.handleValueChange = handleValueChange.bind(this);
		this.searchId = '20028003query';
        this.handleNumberInput = handleNumberInput.bind(this);
        this.getRecover = getRecover.bind(this);
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
				json:json,
                dateInputPlaceholder: json['20028003-000005'],/* 国际化处理： 选择日期*/
                currtype: json['20028003-000006'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['20028003-000006'],  //选择的币种名称/* 国际化处理： 本币*/
			},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20028003', 'dategroup', 'checkbox', 'fourcheckbox', 'subjectandlevel', 'subjectversion', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
    }
    componentDidMount() {
        this.renderTableFirstData(9)
        setTimeout(() => getReferDetault(this, true, {
            businessUnit,
            getCheckContent
        }, 'subjectAssistBalance'),0);
        setTimeout(() => initTemplate.call(this,this.props), 0)
    }

    renderTableFirstData = (length) => {
        let firstData = [];
        for(let i=0; i<length; i++){
            let obj = {};
            obj.searchObj = [];
            obj.searchRange = '';
            obj.showLocal = 'N';//显示位置
            obj.accele = 'N';//小计
            obj.includesub = 'N',//包含下级
                obj.key = i
            firstData.push(obj);
        }
        this.setState({
            tableSourceData: [...firstData]
        })
    }

	handleTableSelect(){
		//('thisthis:::', this);
	}

    handleSelect = (value) => {//
        //('handleSelect', value);

        this.setState({
            rightSelectItem: [...value]
        })
    }

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}

	getCurrtypeList = () => {//获取"币种"接口
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {"localType":"1", "showAllCurr":"1"};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


    handleCurrtypeChange = (keyParam, value) => {
        this.setState({
            [keyParam]: value,
        });
    }


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}

	getSubjectVersion = (v) => {//获取"启用科目版本"数据
		//gl.glpub.accountinfoquery
        let url = '/nccloud/gl/glpub/accountinfoquery.do';
        let data = {
            pk_accountingbook: v[0].refpk,//""
        };
        ajax({
			url,
			data,
			success: (response) => {
				let { data } = response;
				//('getSubjectVersion>>', response, data.versiondate, data.bizDate)
				this.setState({
                    pk_accperiodscheme: data.pk_accperiodscheme,//"会计期间"参照需要的参数，
                    versiondate: data.versiondate,
					buiDate: data.bizDate
				}, this.getReferInfo(v))
            },
			error: (error) => {
				toast({ content: error.message, color: 'warning' })
			}
		})
	}
	getReferInfo = (self, v) => {
		//('getReferInfo>>', v, this.state);
        let newUrl = '/nccloud/gl/accountrep/assbalancequeryobject33.do';//辅助余额查询对象
        let newData = {
            "pk_accountingbook":v[0].refpk,
            "versiondate":self.state.buiDate,
            "needaccount":false
        };
        //('newData>>', newData);
        ajax({
            url:newUrl,
            data:newData,
            success: function(response){
                const { success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if(response.data){
                        //('response:::>>',self, response.data);
                        self.setState({
                            tableData: response.data
                        })
                    }
                }
            }
        });
	}
	//根据核算账簿获取相关信息r
	getInfoByBook(v) {
		// //(this.state.accountingbook)
		// if (!this.state.accountingbook || !this.state.accountingbook[0]) {
		// 	//如果是清空了核算账簿，应该把相关的东西都还原
		// 	return;
		// }
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';//
		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,
		};
		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
		    	//('versiondate>>>>',res)
		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;

		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        	})
                    //('response.data:::', self.state.tableData,self.state.versiondate)

		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		//(value)
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		//(value)
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {

		this.setState({
            selectVersiondate: value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    //(e);
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化
	onChangeShowzerooccur(e) {
		this.setState({showzerooccur: e});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}
	//显示上级科目复选框变化
	onChangeupLevelSubject(e) {
		this.setState({upLevelSubject: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
        //('onChangeMultbusi>', e)
        this.setState({
            multbusi: e,
            disCurrplusacc: !e,//排序方式
            sumbysubjtypeEdit: !e,//显示科目类型小计
            editSubjectTotal: !e
        });
	}
	handleRadioSort(param){
		//('handleRadioSort>', param);
		this.setState({
			selectedSort: param
		})
		if(param === 'N'){
			this.setState({
                showzerooccurValueEdit: true,
				showzerobalanceoccurValueEdit:true,
				sumbysubjtypeEdit: true
			})
		}else{
            this.setState({
                showzerooccurValueEdit: false,
                showzerobalanceoccurValueEdit:false,
                sumbysubjtypeEdit: false
            })
		}
	}
	handleShowType(param){
		this.setState({
			selectedShowType: param
		})
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
	onChangeDoubleCell(param){
		this.setState({
			doubleCell: !this.state.doubleCell
		})
	}
	changeProjectStyle(param){//所有末级   表外科目选择
		if(param === 'allLast'){
			this.setState({
				allLast: !this.state.allLast
			})
		}else{
			this.setState({
				outProject: !this.state.outProject
			})
		}
	}

    handleSearchObj = (values, record) => {//查询对象选择具体科目
        //('handleSearchObj>>', value, record);
        let value = JSON.parse(values)
        let { tableSourceData, selectRow } = this.state;
        let originData = this.findByKey(record.key, tableSourceData);
        originData.selectCell = value;
        record.selectCells = [];
        record.selectRange = [];
		//('originData>>', originData, selectRow)
        if(value.refpath ){
            originData.type = 'referpath';
            originData.searchRange = value.refpath;
        }else if(value.datatype === '1'){//字符
            originData.type = 'string';
        } else if(value.datatype === '4'){//整数
            originData.type = 'integer';
        } else if(value.datatype === '31'){//数值(小数)
            originData.type = 'number';
        } else if(value.datatype === '32'){//布尔
            originData.type = 'boolean';
        } else if(value.datatype === '33'){//日期
            originData.type = 'date'
        } else if(value.datatype === '34'){//日期+时分秒时间
            originData.type = 'dateTime'
        }else if(value.attr){
            originData.selectCellAttr = [...value.attr];
            originData.type = 'modal'
        } else{
            originData.type = ''
		}
        let newSelectRow = [...selectRow];
        newSelectRow[record.key] = record;
        this.setState({
            tableSourceData,
            selectRow: newSelectRow
        })
    }
    findByKey = (key, rows) => {
        let rt = null;
        let self = this;
        rows.forEach(function(v, i, a) {
            if (v.key == key) {
                rt = v;
            }
        });
        return rt;
    }
    dateRefChange = (key, value, record) => {
        //('dateRefChange>',value, record);
        let length = record.selectCell.inputlength;
        let exeReg = new RegExp("^.{0," + length + "}$");
        let result = exeReg.exec(value);
        if(result && key === 'stringRefValue'){
            record.selectRange = result[0]
            this.setState({
                [key]: result[0]
            })
        }else {
            record.selectRange = value
            this.setState({
                [key]: value
            })
        }
    }
    numberRefChange = (key, value, record, param) => {
        //('numberRefChange>>', value, record);
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectRange = this.state[key], 0)

    }
    renderSearchRange = (record, fieldid) => {//根据'查询对象'的选择来对应渲染'查询范围'的组件
        //('renderSearchRange>', record);
        switch(record.type){
            case "referpath":
                let refPathEle = this.renderRefPathEle(record, fieldid);
                //('refPathEle>>', refPathEle);
                return refPathEle;
            case 'date':
                return (
                    <DatePicker
                        fieldid={fieldid}
                        format={format}
                        value={this.state.assistDateValue}
                        placeholder={this.state.dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateValue', value, record)}
                    />
                );
            case 'string':
                return <Input
                    fieldid={fieldid}
                    value={this.state.stringRefValue}
                    onChange={(value) => this.dateRefChange('stringRefValue', value, record)}
                />;
            case 'number':
                return <Input
                    fieldid={fieldid}
                    value={this.state.numberRefValue}
                    onChange={(value) => this.numberRefChange('numberRefValue', value, record, 'assistBalance') }
                />;
            case 'integer'://整数
                return <Input
                    fieldid={fieldid}
                    value={this.state.numberRefValueInteger}
                    onChange={(value) => this.numberRefChange('numberRefValueInteger', value, record, 'assistBalanceInteger') }
                />;
            case 'boolean'://布尔
                return <Select
                    showClear={false}
                    fieldid={fieldid}
                    value={this.state.booleanValue}
                    className='search-boolean'
                    onChange={(value)=>{
                        //('NCSelect>>>', value);
                        this.dateRefChange('booleanValue', value, record)
                    }}
                >
                    <NCOption value="Y">{this.state.json['20028003-000021']}</NCOption>{/* 国际化处理： 是*/}
                    <NCOption value="N">{this.state.json['20028003-000022']}</NCOption>{/* 国际化处理： 否*/}
                    <NCOption value="">{this.state.json['20028003-000023']}</NCOption>{/* 国际化处理： 空*/}
                </Select>;
            case 'dateTime'://日期+时分秒
                return (
                    <DatePicker
                        fieldid={fieldid}
                        showTime={true}
                        format={Timeformat}
                        value={this.state.assistDateTimeValue}
                        placeholder={this.state.dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateTimeValue', value, record)}
                    />
                );
            case 'modal':
                //('????', record, record.selectCell, record.selectCellAttr);
                let attrObj = record.selectCellAttr;
                let { tableSourceData } = this.state;
                let selectObj = [];
                attrObj.map((item)=>{
                    if(item){
                        item.itemKey = record.key;
                    }
                    if(item && item.selectName){
                        selectObj += item.selectName + ','
                    }
                    //('keyyyy:::', item, record, selectObj)
                })
                return (
                    <div style={{display: 'flex'}}>
                        <Input
                            fieldid={fieldid}
                            style={{border: 'none'}}
                            placeholder={record.selectCell && record.selectCell.name}
                            value={selectObj}
                        />
                        <div className='ellipsisContainer'
                             onClick={() => this.closeNewModal(record)}
                        >
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                        </div>
                    </div>
                );
            default:
                return <Input fieldid={fieldid} disabled/>;
        }
    }
    renderRefPathEle = (record, fieldid) => {//根据传进来的url渲染对应的参照组件
        //('renderRefPathEle>',record, record.selectCell);
        //带有参照的
        //('state>>>>',record, record.selectCell, record.selectCell.pk_checktype, !this.state[record.selectCell.pk_checktype]);
        let mybook;
        let objKey = record.key + record.selectCell.pk_checktype;
        if(!this.state[objKey]){//undefined
            //('createScript:', this.state);
            {createScript.call(this,record.selectCell.refpath+".js",objKey)}

        }else{
            let pkOrgParam = this.state.buSecond[0] ? this.state.buSecond[0].refpk : this.state.unitValueParam;
            //('rendertableRight>:::',pkOrgParam, this.state, this.state[objKey]);
            let defaultValue = [];
            let cellArr = !Array.isArray(record.selectRange) && record.selectRange.split(',');
            if(cellArr){
                cellArr.map((item, indx) => {
                    let cellObj = {refpk: ''};
                    cellObj.refpk = item;
                    defaultValue.push(cellObj)
                })
            }
            let optins = {
                "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                "pk_org": pkOrgParam,
                "isDataPowerEnable": "Y",
                "DataPowerOperationCode" : 'fi',

            }
			if(record.selectCell && record.selectCell.classid.length === 20){
                optins.pk_defdoclist = record.selectCell.classid;
			}
            //('cellArr:>>',this.state.buSecond, cellArr, defaultValue);
            let options = {
                fieldid: fieldid,
                value: record.selectCells,
                isMultiSelectedEnabled:true,
                "isShowDisabledData": true,
                queryCondition:() => {
                    return optins
                },
                onChange: (v)=>{
                    //('innerChaa:', v, this.state.tableSourceData);
                    let {tableSourceData} = this.state;
                    let refpkArr = [];
                    let selectCell = [];
                    v.forEach((item)=>{
                        //('vFormach::', item);
                        refpkArr.push(item.refpk);
                        selectCell.push(item);
                    })
                    let pk_checkvalue = refpkArr.join(',')
                    record.selectRange = pk_checkvalue;
                    record.selectCells = selectCell;
                    this.setState({
                        tableSourceData
                    })
                }
            };
            let newOPtions = {}
            if(record.selectCell.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.selectCell.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                //('recorddd>>???', record);
                newOPtions = {
                    ...options,
                    "unitValueIsNeeded":false,
                    "isShowDimission":true,
                    queryCondition:() => {
                        return {
							...optins,
                            "busifuncode":"all",
                            isShowDimission:true
                        }
                    },
                    isShowUnit:true,
                    unitProps:{
                        refType: 'tree',
                        refName: this.state.json['20028003-000011'],/* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:this.state.json['20028003-000011'],refpk:'root'},/* 国际化处理： 业务单元*/
                        placeholder:this.state.json['20028003-000011'],/* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[this.state.json['20028003-000012'], this.state.json['20028003-000013']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false
                    },
                    unitCondition:{
                        pk_financeorg: pkOrgParam,
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    },
                }
            }else {
                newOPtions = {
                    ...options
                }
            }
            mybook =  (
                <Row className="tableselectItem">
                    <div>
                        {
                            this.state[objKey]?(this.state[objKey])(newOPtions):<div/>
                        }
                    </div>
                </Row>
            );
        }
        return mybook;
    }
    renderModalList = () => {
        this.columns = [
            {
                title: (<div fieldid='searchObj'>{this.state.json['20028003-000007']}</div>),/* 国际化处理： 辅助核算*/
                dataIndex: "searchObj",
                key: "searchObj",
                render: (text, record, index) => {
                    //('columns>>>>', text, record);
                    let renderArr = [];
                    text.map((item, index) => {
                        renderArr.push(<NCOption value={JSON.stringify(item)} key={index}>{item.name}</NCOption>)
                    })
                    return (
                        <Select
                            showClear={false}
                        fieldid='searchObj'
                            value={record.selectCell && record.selectCell.name}
                            onChange = {(value) => this.handleSearchObj(value, record)}
                        >
                            {
                                renderArr
                            }
                        </Select>
                    )
                }
            },
            {
                title: (<div fieldid='searchRange'>{this.state.json['20028003-000008']}</div>),/* 国际化处理： 查询范围*/
                dataIndex: "searchRange",
                key: "searchRange",
                render: (text, record,index) => {
                    //('searchRangetext:::',record, record.type);
                    let renderEle = this.renderSearchRange(record, 'searchRange');
                    return renderEle;
                }
            }
        ];
        return (
            <div className='right_query'>
                <div className='query_body1'>
                    <div className='query_form subjectAssistBalanced'>
    		<div>
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel isRequire={true} labelname={this.state.json['20028003-000002']}/>{/* 国际化处理： 核算账簿*/}
                    </Col>
                    <Col md={7} sm={7}>
                        <div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: true,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit ,
                                        getCheckContent: getCheckContent,
                                        renderTableFirstData: () => this.renderTableFirstData(9)
                                    },
                                    'subjectAssistBalance'
                                )
                            }
                        </div>
                    </Col>
                </Row>
                {
                    this.state.isShowUnit ?
                        <Row className="myrow isShowUnit" >
                            <Col md={2} sm={2}>
                                <FormLabel labelname={this.state.json['20028003-000011']}/>{/* 国际化处理： 业务单元*/}
                            </Col>
                            <Col md={7} sm={7}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
                                        fieldid='buSecond'
                                        value={this.state.buSecond}
                                        isMultiSelectedEnabled= {true}
                                        isShowDisabledData = {true}
                                        isHasDisabledData = {true}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                        }}
                                        onChange={(v)=>{
                                            //('erjidanyuan::',v);
                                            this.getRecover('tableSourceData', 'searchRange', 'selectCells', 'selectCell', 'type', 'accele', 'includesub', 'showLocal')

                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if(this.state.accountingbook && this.state.accountingbook[0]) {
                                                }
                                            })
                                        }
                                        }
                                    />

                                </div>

                            </Col>
                            <Col md={3} sm={3}>
                                <FICheckbox
                                    colors="dark"
                                    className="mycheck"
                                    id='multbusi'
                                    checked={this.state.multbusi}
                                    onChange={this.onChangeMultbusi.bind(this)}
                                >
                                    {this.state.json['20028003-000024']} {/* 国际化处理： 多业务单元合并*/}
                                </FICheckbox>
                            </Col>
                        </Row> :
                        <div></div>
                }
                {/*启用科目版本：*/}
                <SubjectVersion
                    checked = {this.state.isversiondate}//复选框是否选中
                    data={this.state.versionDateArr}//
                    value={this.state.versiondate}
                    disabled={!this.state.isversiondate}
                    renderTableFirstData = {() => this.renderTableFirstData(9)}
                    handleValueChange = {(key, value) => this.handleValueChange(key, value, 'subjectAssistBalance', getCheckContent)}
                />

                {/*会计期间*/}
                <DateGroup
                    selectionState = {this.state.selectionState}
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook[0]}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                />

                {/*忽略日期选项*/}

                {/*科目，级次：*/}
                <SubjectLevel
                    parent = {this}
                    accountingbook = {this.state.accountingbook}
                    isMultiSelectedEnabled = {false}//控制多选单选
                    selectedKeys = {this.state.selectedKeys}
                    targetKeys = {this.state.targetKeys}
                    changeParam = {this.state.changeParam}
                    handleSelect = {this.handleSelect}
                    handleValueChange = {this.handleValueChange}

                    handleSelectChange = {(sourceSelectedKeys, targetSelectedKeys) => handleSelectChange(this, sourceSelectedKeys, targetSelectedKeys)}
                    handleChange = {(nextTargetKeys, direction, moveKeys) => handleChange(this,nextTargetKeys, direction, moveKeys) }
                    clearTransterData = {() => clearTransterData(this)}
                    showLevel = {true}//控制是否显示级次
                />

                <FourCheckBox
                    totalTitle= {this.state.json['20028003-000014']} ///* 国际化处理： 显示属性：*/
                    renderCells={
                        [
                            {
                                title: this.state.json['20028003-000015'],/* 国际化处理： 所有末级*/
                                checked: true,
                                disabled: true,
                                id: 'showzerooccur',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'allLast')
                            },
                            {
                                title: this.state.json['20028003-000016'],/* 国际化处理： 表外科目*/
                                checked: this.state.outProject,
                                id: 'outProject',
                                onChange: (value) => this.handleValueChange('outProject', value, 'SubjectVersion')
                            },
                        ]
                    }
                />
                <div className="modalTable twoItemTable">
                <Table
                    columns={this.columns}
                    data={this.state.tableSourceData}
                />
				</div>
                {/*包含凭证：*/}
                <CheckBoxCells
                    totalTitle= {this.state.json['20028003-000014']} ///* 国际化处理： 显示属性：*/
                    paramObj = {{
                        "noChargeVoucher": {//未记账凭证
                            show: true,
                            checked: this.state.includeuntally,
                            onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                        },
                        "errorVoucher": {//错误凭证
                            show: true,
                            checked: this.state.includeerror,
                            onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                        },
                        "harmToBenefitVoucher": {//损益结转凭证
                            show: true,
                            checked: this.state.includeplcf,
                            onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                        },
                        "againClassifyVoucher": {//重分类凭证
                            show: true,
                            checked: this.state.includerc,
                            onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                        },
                    }}
                />

                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel labelname={this.state.json['20028003-000025']}/>{/* 国际化处理： 多主体显示方式*/}
                    </Col>
                    <Col md={10} sm={10}>
                        <Radio.NCRadioGroup
                            selectedValue={this.state.mutibook}
                            onChange={ (value) => this.handleValueChange('mutibook', value, 'multiAgentShow')}
                        >
                            <Radio disabled = {this.state.disMutibook} value="Y" >
                                {this.state.json['20028003-000026']} {/* 国际化处理： 多主体合并*/}
                            </Radio>
                            <Radio disabled = {this.state.disMutibook} id='subjectShow' value="N" >
                                {this.state.json['20028003-000027']} {/* 国际化处理： 按主体列示*/}
                            </Radio>
                        </Radio.NCRadioGroup>
                    </Col>
                </Row>

                {/*币种*/}
                {renderMoneyType(this.state.currtypeList,this.state.currtypeName, this.handleCurrtypeChange, this.state.json)}

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
						this.state.json
                    )
                }

                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel labelname={this.state.json['20028003-000028']}/>{/* 国际化处理： 排序方式*/}
                    </Col>
                    <Col md={9} sm={9}>

                        <Radio.NCRadioGroup
                            selectedValue={this.state.selectedSort}
                            onChange={this.handleRadioSort.bind(this)}
                        >
                            <Radio value="Y" disabled={this.state.disCurrplusacc} >
                                {`${this.state.json['20028003-000001']}+${this.state.json['20028003-000029']}`} {/* 国际化处理： 币种,科目*/}
                            </Radio>
                            <Radio value="N" id='subjectMoney' disabled={this.state.disCurrplusacc}  >
                                {`${this.state.json['20028003-000029']}+${this.state.json['20028003-000001']}`} {/* 国际化处理： 科目,币种*/}
                            </Radio>
                        </Radio.NCRadioGroup>

                    </Col>
                </Row>

                <FourCheckBox
                    totalTitle= {this.state.json['20028003-000014']} ///* 国际化处理： 显示属性：*/
                    renderCells={
                        [
                            {
                                title: this.state.json['20028003-000017'],/* 国际化处理： 无发生不显示*/
                                checked: this.state.showzerooccurValue,
                                disabled: this.state.showzerooccurValueEdit,
                                id: 'showzerooccurValue',
                                onChange: (value) => this.handleValueChange('showzerooccurValue', value, 'showzerooccurValue')
                            },
                            {
                                title: this.state.json['20028003-000018'],/* 国际化处理： 无余额无发生不显示*/
                                checked: this.state.showzerobalanceoccurValue,
                                disabled: this.state.showzerobalanceoccurValueEdit,
                                id: 'showzerobalanceoccurValue',
                                onChange: (value) => this.handleValueChange('showzerobalanceoccurValue', value)
                            },
                            {
                                title: this.state.json['20028003-000019'],/* 国际化处理： 显示科目类型小计*/
                                checked: this.state.sumbysubjtype,
                                disabled: this.state.sumbysubjtypeEdit,
                                id: 'sumbysubjtype',
                                onChange: (value) => this.handleValueChange('sumbysubjtype', value, 'showSubjectTotal')
                            },
                            {
                                title: this.state.json['20028003-000020'],/* 国际化处理： 显示上级科目*/
                                checked: this.state.upLevelSubject,
                                id: 'upLevelSubject',
                                onChange: (value) => this.handleValueChange('upLevelSubject', value)
                            }
                        ]
                    }
                />

                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel labelname={this.state.json['20028003-000030']}/>{/* 国际化处理： 余额方向*/}
                    </Col>
                    <Col md={2} sm={2}>

                        <div className="book-ref">
                            <Select
                                showClear={false}
                                fieldid='balanceori'
                                value={this.state.balanceori}
                                onChange={this.handleBalanceoriChange.bind(this)}
                            >
                                <NCOption value={'-1'} key={'-1'} >{this.state.json['20028003-000031']}</NCOption>{/* 国际化处理： 双向*/}
                                <NCOption value={'0'} key={'0'} >{this.state.json['20028003-000032']}</NCOption>{/* 国际化处理： 借*/}
                                <NCOption value={'1'} key={'1'} >{this.state.json['20028003-000033']}</NCOption>{/* 国际化处理： 贷*/}
                                <NCOption value={'3'} key={'3'} >{this.state.json['20028003-000034']}</NCOption>{/* 国际化处理： 平*/}
                            </Select>
                        </div>
                    </Col>
                    <Col md={3} sm={3}>
                        <Checkbox
                            colors="dark"
                            className="mycheck"
                            id='twowaybalance'
                            checked={this.state.twowaybalance}
                            disabled={this.state.debitAndCredit}
                            onChange={this.onChangeTwowaybalance.bind(this)}
                        >
                            {this.state.json['20028003-000035']}  {/* 国际化处理： 按借贷方显示余额*/}
                        </Checkbox>
                    </Col>
                </Row>
			</div>
                    </div>
                </div>
            </div>
		)
	}
    clickPlanEve = (value) => {
        //('clickPlanEve:???', 'value>',this, value, '>')
        this.state = deepClone(value.conditionobj4web.nonpublic);
        //('clickPlanEve:???12', this.state)
        this.setState(this.state)
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
        //('saveSearchPlan>>', this.state)
        return {...this.state}
    }

	render() {
        let { search } = this.props;
        let { NCCreateSearch } = search;
        return <div>
            {
                NCCreateSearch(this.searchId, {
                    onlyShowSuperBtn:true,                       // 只显示高级按钮
                    replaceSuperBtn:this.state.json['20028003-000009'],                      // 自定义替换高级按钮名称/* 国际化处理： 查询*/
                    replaceRightBody:this.renderModalList,
                    hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
                    clickPlanEve:this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                    saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
                    isSynInitAdvSearch: true,//渲染右边面板自定义区域
                    oid:this.props.meta.oid,
                    clickSearchBtn: () => {
                        let pk_accountingbook = [];
                        let pk_unit = [];
                        
                        if (Array.isArray(this.state.accountingbook)) {
                            this.state.accountingbook.forEach(function (item) {
                                pk_accountingbook.push(item.refpk)
                            })
                        }
                        
                        if (Array.isArray(this.state.buSecond)) {
                            this.state.buSecond.forEach(function (item) {
                                pk_unit.push(item.refpk)
                            })
                        }
                        
                        let lastAssvos = [];
                        this.state.tableSourceData.forEach((item)=>{
                            //('assvos>>>', item);
                            let resultObj = {};
                            if (item.selectCell && item.selectCell.name != '') {
                                if (!item.selectCellAttr) {
                                    resultObj.name = item.selectCell.name;
                                    resultObj.pk_checktype = item.selectCell.pk_checktype;
                                    resultObj.pk_checkvalue = Array.isArray(item.selectRange) ? '' : item.selectRange;
                                    resultObj.headEle = item.showLocal;
                                    resultObj.accEle = item.accele;
                                    resultObj.includeSub = item.includesub;
                                    lastAssvos.push(resultObj)
                                } else if (item.selectCellAttr) {
                                    resultObj.name = item.selectCell.name;
                                    resultObj.pk_checktype = item.selectCell.pk_checktype;
                                    resultObj.attr = [];
                                    resultObj.headEle = item.showLocal;
                                    resultObj.accEle = item.accele;
                                    resultObj.includeSub = item.includesub;
                                    item.selectCellAttr.map((itemCell) => {
                                        if (itemCell.select) {
                                            resultObj.attr.push(itemCell);
                                        }
                                    })
                                    lastAssvos.push(resultObj)
                                }
                        }
                        })
                        //('lastAssvos>>', lastAssvos)
                        //
                        let data = {
                            pk_accountingbook: pk_accountingbook,
                            pk_unit,
                            multbusi: this.state.multbusi,
                            usesubjversion: this.state.isversiondate ? 'Y' : 'N',// 是否启用科目版本
                            versiondate: this.state.isversiondate ? this.state.versiondate : this.state.busiDate,//科目版本
                            // pk_accasoa://条件框选择科目
                            startcode: this.state.startcode.refcode,  //开始科目编码
                            endcode: this.state.endcode.refcode,    //结束科目编码
                            startlvl: this.state.startlvl,
                            endlvl: this.state.endlvl,
                            isleave: true,
                            isoutacc: this.state.isoutacc ? 'Y' : 'N',
                            /**会计期间，日期*/
                            startyear: this.state.startyear,
                            endyear: this.state.endyear,
                            startperiod: this.state.startperiod,
                            endperiod:   this.state.endperiod,
                            startdate: this.state.begindate,
                            endtdate: this.state.enddate,
                    
                            //未记账、错误、损益结转、重分类  Y/N  注意：‘包含凭证’是个标体，后面的才是选项
                            includeuntally: this.state.includeuntally ? 'Y' : 'N',
                            includeerror: this.state.includeerror ? 'Y' : 'N',
                            includeplcf: this.state.includeplcf ? 'Y' : 'N',
                            includerc: this.state.includerc ? 'Y' : 'N',
                            pk_currtype: this.state.currtype,
                            returncurr: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局
                            mutibook: this.state.mutibook,
                    
                            showzerooccur: this.state.showzerooccurValue ? 'Y' : 'N',//无发生不显示  Y/N
                            showzerobalanceoccur: this.state.showzerobalanceoccurValue ? 'Y' : 'N',//无余额无发生不显示
                            sumbysubjtype: this.state.sumbysubjtype ? 'Y' : 'N',//科目类型小计  Y/N
                            showupsubj: this.state.upLevelSubject ? 'Y' : 'N', //显示上级科目 Y/N
                    
                            currplusacc: this.state.currplusacc,//
                    
                            balanceori: this.state.balanceori,
                            twowaybalance: this.state.twowaybalance ? 'Y' : 'N',
                            istree: 'Y',
                            qryObjs: lastAssvos,
                            pk_accasoa: this.state.rightSelectItem,//'条件'
                        }
                                
                        let url = '/nccloud/gl/accountrep/checkparam.do'
                        let flagShowOrHide;
                        ajax({
                            url,
                            data,
                            async: false,
                            success: function (response) {
                                flagShowOrHide = true
                            },
                            error: function (error) {
                                flagShowOrHide = false
                                toast({ content: error.message, color: 'warning' });
                            }
                        })
                        if (flagShowOrHide == true) {
                            this.props.onConfirm(data)
                        } else if (flagShowOrHide == false) {
                            return true;
                        }
					},  // 点击按钮事件
                })}
        </div>
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)

export default CentralConstructorModal;
