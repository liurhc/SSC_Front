/**
 * Created by liqiankun on 2018/6/22.
 * 管理报表 ---> 科目辅助余额表
 */
import React, {Component} from 'react';
import {high, base, ajax, createPage, getMultiLang, gzip} from 'nc-lightapp-front';
const { NCDiv } = base;
import '../../css/index.less'
import { SimpleTable } from 'nc-report';

import HeadCom from '../../common/headSearch/headCom';
import QueryModal from './MainSelectModal';
import {tableDefaultData} from '../../defaultTableData';
import reportPrint from '../../../public/components/reportPrint';
import {toast} from "../../../public/components/utils";
const { PrintOutput } = high;
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import {setData} from "../../common/simbleTableData";
import {searchById} from "../../common/modules/createBtn";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import {rowBackgroundColor} from "../../common/htRowBackground";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
import { queryRelatedAppcode, originAppcode } from '../../../public/common/queryRelatedAppcode.js';
class MultiagentSubjectBalanceContent extends Component{
    constructor(props){
        super(props);
        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            multiCheckAccountShow: true, //多核算账簿列示是否禁止；true：禁止
            showMultiCheckAccount: true, //是否显示【多核算账簿列示】
			disabled: true,
            visible: false,
			appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
			outputData: {},
			ctemplate: '', //模板输出-模板
			nodekey: '',
			printParams:{}, //查询框参数，打印使用
            showAddBtn: true,//控制联查按钮；true:禁止使用；false: 可以使用
            showTable: false,//控制是否显示表格
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
	    	selectStyle: 'account', //控制 "账簿格式" 的类型
			paramObj: {},
			textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            accountType: 'amountcolumn',//金额式
            "fourLables": []
        }

        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
		this.changeSelectStyle = this.changeSelectStyle.bind(this);//选择 '账簿格式'事件
        this.handleValueChange = handleValueChange.bind(this);
        this.searchById = searchById.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
	}
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                "fourLables": [
                    {
                        title: json['20028003-000002'],/* 国际化处理： 核算账簿*/
                        styleClass:"m-long"
                    },
                    {
                        title: json['20028003-000000'],/* 国际化处理： 期间*/
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20028003-000001'],/* 国际化处理： 币种*/
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20028003-000003'],/* 国际化处理： 账簿格式*/
                        styleClass:"m-brief"
                    }
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20028003', 'publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }

    componentDidMount(){
        this.setState({
            flag: true,
        });

        let appceod = this.props.getSearchParam('c');
        this.searchById('20028003PAGE',appceod);
        this.props.button.setDisabled({ linkdetail: true, multiorg: true, directprint: true, saveformat: true });//linkdetail  multiorg
        this.getParam();
    }

	showPrintModal() {
        this.setState({
            visible: true
        })
    }
	handlePrint(data, isPreview) {
		let printUrl = '/nccloud/gl/accountrep/subjassbalanceprint.do'
		let { printParams, appcode } = this.state
		let { ctemplate, nodekey } = data;
		printParams.queryvo = data;
		this.setState({
			ctemplate: ctemplate,
			nodekey: nodekey
        })
        printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
        this.handleCancel()
    }
    handleCancel() {
        this.setState({
            visible: false
        });
	}
	showOutputModal() {
		// this.refs.printOutput.open()
		let outputUrl = '/nccloud/gl/accountrep/subjassbalanceoutput.do'
        let { appcode, nodekey, ctemplate, printParams } = this.state
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)
        // let outputData = mouldOutput(appcode, nodekey, ctemplate, printParams)
        // this.setState({
        //     outputData: outputData
        // })
    }
    handleOutput() {

    }

    getParam = () => {
        let data = this.props.getUrlParam && this.props.getUrlParam('status');
        if(data){
            this.state.paramObj.mutibook = 'N';
            let gziptools = new gzip();
            let paramData = gziptools.unzip(data);
            this.setState({
                showMultiCheckAccount: false,
                printParams: paramData
            })
            this.modalSure(paramData)
        }
    }
    changeSelectStyle(value){// 选择 '账簿格式'事件
      this.setState({
		selectStyle: value
      })
    }
    queryClick(){//点击查询, 弹框的"关闭"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    modalSure = (params) => {//点击弹框"确定"的事件
        this.setState({
            paramObj: params,
            printParams: params
		});
        if ((params.pk_accountingbook && params.pk_accountingbook.length > 1 && params.mutibook === 'Y')
            || (params.origparam && params.origparam.pk_accountingbook.length > 1 && params.origparam.mutibook === 'Y')) {
            this.setState({
                multiCheckAccountShow: false
            })
        }else{
            this.setState({
                multiCheckAccountShow: true
            })
		};
	    this.queryData(params)
    }
    queryData = (params) => {//请求数据方法；
        let url = '/nccloud/gl/accountrep/subjassbalancebooksquery.do';
        let self = this;
        ajax({
            url:url,
            data:params,
            success:function(response){
                let { data, success } = response;
                let num = 1;
                let numArr = [];
                self.setState({
                    showAddBtn: false
                })
                // self.recursionFn(data.column);
                if(success){
                    if(data&&data.data.length>0){
                        self.setState({
                            disabled:false,
                            typeDisabled: false,//账簿格式 默认是禁用的
                        });
                        self.props.button.setDisabled({ directprint: false, saveformat: false });
                    }
                    self.setState({
                        headtitle: {...data.headtitle},
                        dataWidthPage: data,
                        selectRowIndex:0
                    })
                    setData(self, data);
                }
            },
            error:function(error){
                toast({ content: error.message, color: 'warning' });
            }
        })
    }
    setDate = (data) => {
        let rows = [];   //表格显示的数据合集
        let columHead = []; //第一层表头
        let parent = []; //存放一级表头，二级表头需要补上
        let parent1 = []; //存放二级表头，三级表头需要补上
        let child1 = []; //第三层表头
        let child2 = []; //第二层表头
        let colKeys = [];  //列key值，用来匹配表体数据
        let colAligns=[];  //每列对齐方式
        let colWidths=[];  //每列宽度
        let oldWidths=[];
        let textAlginArr = this.state.textAlginArr.concat([]);      //对其方式
        data[this.state.accountType].forEach( (value) => {//column:[]原始数据表头信息
            let valueCell = {};
            valueCell.title = value.title;
            valueCell.key = value.key;
            valueCell.align = 'center';
            valueCell.bodyAlign = 'left';
            valueCell.style = 'head';
            let paramArr = [];

            if(value.children){
                paramArr.push('Y')
                if(parent.length>0){
                    child1.push(...parent);
                }
                for(let i=0;i<value.children.length;i++){
                    if(value.children[i].children){
                        child2.push(...parent1);
                        for(let k=0;k<value.children[i].children.length;k++){
                            let child1Cell = {};
                            child1Cell.title = value.children[i].title;
                            child1Cell.key = value.children[i].key;
                            child1Cell.align = 'center';
                            child1Cell.bodyAlign = 'left';
                            child1Cell.style = 'head';
                            let child2Cell = {};
                            child2Cell.title = value.children[i].children[k].title;
                            child2Cell.key = value.children[i].children[k].key;
                            child2Cell.align = 'center';
                            child2Cell.bodyAlign = 'left';
                            child2Cell.style = 'head';
                            columHead.push(valueCell);
                            child1.push(child1Cell);
                            child2.push(child2Cell);
                            colKeys.push(value.children[i].children[k].key);
                            colAligns.push(value.children[i].children[k].align);
                            colWidths.push({'colname':value.children[i].children[k].key,'colwidth':value.children[i].children[k].width});
                        }
                        parent1 = [];
                    }else {
                        let innerCell2 = {};
                        innerCell2.title = value.children[i].title;
                        innerCell2.key = value.children[i].key;
                        innerCell2.align = 'center';
                        innerCell2.bodyAlign = 'left';
                        innerCell2.style = 'head';
                        columHead.push(valueCell);
                        child1.push(innerCell2);
                        parent1.push(innerCell2);
                        colKeys.push(value.children[i].key);
                        colAligns.push(value.children[i].align);
                        colWidths.push({'colname':value.children[i].key,'colwidth':value.children[i].width});
                    }
                }
                parent = [];
            }else{
                let cellObj = {};
                cellObj.title = value.title;
                cellObj.key = value.key;
                cellObj.align = 'center';
                cellObj.bodyAlign = 'left';
                cellObj.style = 'head';
                columHead.push(valueCell);
                parent1.push(cellObj);
                parent.push(cellObj);
                colKeys.push(value.key);
                colAligns.push(value.align);
                colWidths.push({'colname':value.key,'colwidth':value.width});
            }
            if(paramArr.includes('Y')){
                this.setState({
                    flow: true
                })
			}

        });
        let columheadrow = 0; //表头开始行
        // if (data.headtitle){
        //    columheadrow = 1;
        //    let headtitle = [];
        //    data.headtitle.forEach(function (value) {
        //     headtitle.push(value[0]);
        //     headtitle.push(value[1]);
        //    });
        //    for(let i=headtitle.length;i<columHead.length;i++){
        //     headtitle.push('');
        //    }
        //    rows.push(headtitle);
        // }

        let mergeCells = [];
        let row,col,rowspan,colspan;
        let headcount = 1; //表头层数
        if(child1.length>0){
            headcount++;
        }
        if(child2.length>0){
            headcount++;
        }
        let currentCol = 0;
        //计算表头合并格
        for(let i=0;i<data[this.state.accountType].length;i++) {
            let value = data[this.state.accountType][i];
            //*********
            if(value.children){//有子元素的
                let childLeng = value.children.length;
                mergeCells.push([columheadrow, currentCol, columheadrow, currentCol + childLeng -1 ]);
                let headCol = currentCol;
                for(let i=0;i<value.children.length;i++){

                    let childlen = 0;
                    if(value.children[i].children){

                        let childlen = value.children[i].children.length;
                        currentCol = currentCol+childlen;
                        for(let k=0;k<value.children[i].children.length;k++){
                            textAlginArr.push(value.children[i].children[k].align);
                        }
                    }else if(childlen > 0){//子元素里面没有子元素的

                        mergeCells.push([columheadrow+1, currentCol, columheadrow+2, currentCol]);//cell
                        currentCol++;

                    }else {
                        currentCol++;
                        textAlginArr.push(value.children[i].align);
                    }
                }
            }else{//没有子元素对象
                mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
                currentCol++;
                textAlginArr.push(value.align);
            }
        }
        if(parent.length>0){
            child1.push(...parent);
        }
        if(child2.length>0&&parent1.length>0){
            child2.push(...parent1);
        }

        rows.push(columHead);

        if(child1.length>0 && this.state.flow){
            rows.push(child1);
        }
        if(child2.length>0 && this.state.flow){
            rows.push(child2);
        }

        if(data!=null && data.data.length>0){

            let rowdata = [];
            let flag = 0;
            for(let i=0;i<data.data.length;i++){
                flag++;
                for(let j=0;j<colKeys.length;j++){

                    let itemObj = {
                        align: 'left',
                        style: 'body'
                    };
                    itemObj.title = data.data[i][colKeys[j]];
                    itemObj.key = colKeys[j] + flag;
                    itemObj.link = data.data[i].link
                    // rowdata.push(data.data[i][colKeys[j]])
                    rowdata.push(itemObj)
                }
                rows.push(rowdata);
                rowdata = [];
            }
        }
        let rowhighs = [];
        for (let i=0;i<rows.length;i++){
            rowhighs.push(23);
        }
        this.props.dataout.data.cells= rows;//存放表体数据
        //('settings.cells', this.props.dataout.data.cells)
        this.props.dataout.data.widths =colWidths;
        oldWidths.push(...colWidths);
        this.props.dataout.data.oldWidths = oldWidths;
        this.props.dataout.data.rowHeights=rowhighs;
        let frozenCol;
        for(let i=0;i<colAligns.length;i++){
            if(colAligns[i]=='center'){
                frozenCol = i;
                break;
            }
        }
        this.props.dataout.data.fixedColumnsLeft = 0; //冻结
        this.props.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结
        //('>>>', this.props.dataout.data.fixedRowsTop)
        let quatyIndex = [];
        for (let i=0;i<colKeys.length;i++){
            if(colKeys[i].indexOf('quantity')>0){
                quatyIndex.push(i);
            }
        }


        if(mergeCells.length>0){
            this.props.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
        }
        //('settings.mergeInfo', this.props.dataout.data.mergeInfo);
        let headAligns=[];

        for(let i=0;i<headcount+columheadrow;i++){
            for(let j=0;j<columHead.length;j++){
                headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
            }
        }
        this.props.dataout.data.cell= headAligns,
            this.props.dataout.data.cellsFn=function(row, col, prop) {
                let cellProperties = {};
                if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
                    cellProperties.align = colAligns[col];
                    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
                }
                return cellProperties;
            }
        //('>>>>result', this.props.dataout.data);
        this.setState({
            dataout:this.props.dataout,
            showTable: true,
            showModal: false,
            textAlginArr
        });
	}
	//直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    getDetailPort(paramObj){//联查"明细"
        let url = paramObj.url; //'/nccloud/gl/accountrep/triaccquery.do';
        let selectRow = this.getSelectRowData()[0].link;
        if(selectRow){
            let data = this.getDataType(paramObj);
            this.jumpToDetail(data, paramObj);
            //('getDetailPort>>', data);
		}
    }
    relevanceAssist = (response, param) => {//科目余额表的：联查辅助
        let { success, data } = response;
        //('ssssss:', data);
        if (success) {
            //('relevanceAssist', response, self, data);
            if(data){

				//('llllll::', data[0].pk_accassitem);
				let cellArr = [];

				this.setState({
					selectRow: [data[0].pk_accassitem]
				},this.getDetailPort({
					url: '/nccloud/gl/accountrep/assbalancequery.do',
					jumpTo: '/gl/manageReport/assistBalance/content/index.html',//'/gl/accbalance/relevanceSearch/assist/index.html',
					key: 'assist',
					appcode: queryRelatedAppcode(originAppcode.assbalance),
				}))

            }else {
                // toast({content: `${param.acccode}科目无辅助项，不能联查辅助`, color: 'warning'})
                return;
            }
        }
    }
    getDataType = (param) => {
        let selectRow = this.getSelectRowData()[0].link;

        let result = '';
        if(param.key === 'detail'){
            result = {
                origparam: {...this.state.paramObj},
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.lightgl.subjassbalancebooks.SubjassbalLinkTridetailParamTransfer"
            }
        }else if(param.key === 'totalAccount'){
            result = {
                ...this.state.queryDatas,
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.account.AccbalLinkTriaccbookParamTransfer"
            }
        }else if(param.key === 'assist'){//联查"辅助"
            selectRow['key'] = 'relevanceAssist';
            //('this.state.selectRow::', this.state.selectRow)
            result = {
                ...this.state.paramObj,
                link: {...selectRow},
                "pk_accassitems": [...this.state.selectRow],
                "class": "nccloud.pubimpl.gl.account.AccbalLinkAssbalParamTransfer",
                "from": "accbal" // 联查来源：(多主体)科目余额表accbal
            }
        }else if(param.key === 'multiAccountShow'){
            result = {
                ...this.state.paramObj,
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.account.AccbalLinkMultiorgParamTransfer",
                "from": "multicorp"
            }
        }else if (param.key === 'subjectAssistBalancemultiAccountShow'){//科目辅助余额表"多核算账簿列式"
            result = {
                origparam: {...this.state.paramObj},
                link: {...selectRow},
                class: "nccloud.pubimpl.gl.lightgl.subjassbalancebooks.SubjassbalLinkMultiOrgParamTransfer",
            }
		}
        //('getDataType>', result, selectRow);
        return result;
    }
    jumpToDetail = (data, paramObj) => {
        //('jumpToDetail>>',data, paramObj)
        let gziptools = new gzip();
        this.props.openTo(
            paramObj.jumpTo,
            {appcode: paramObj.appcode, status: gziptools.zip(JSON.stringify(data)),id: '12wew24'}
        );
    }
    //获取handsonTable当前选中行数据
    getSelectRowData=()=>{
        let selectRowData=this.refs.balanceTable.getRowRecord();
        return selectRowData;
    }

    getSelectRow = (param) => {
        this.setState({
            selectRow:[...this.state.selectRow.concat(param)]
        })
    }
    handleSaveColwidth=()=>{
        let {json} = this.state;
        let info=this.refs.balanceTable.getReportInfo();
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.appcode,info.colWidths, json, callBack);
    }

    handleLoginBtn = (obj, btnName) => {
        //('handleLoginBtn>>>', obj, btnName)
        if(btnName === 'print'){//1、打印
            this.showPrintModal()
        }else if(btnName === 'directprint'){//2、直接输出
            this.printExcel()
        }else if(btnName === 'templateOutput'){//3、模板输出
            this.showOutputModal()
        }else if(btnName === 'linkdetail'){//4、联查明细
            this.getDetailPort({
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: '/gl/threedetail/pages/main/index.html',
                key: 'detail',
                appcode: queryRelatedAppcode(originAppcode.detailbook)
            })
        }else if(btnName === 'multiorg'){//6、多核算账簿列式
            this.getDetailPort({
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: '/gl/manageReport/subjectAssistBalance/page/index.html',
                key: 'subjectAssistBalancemultiAccountShow',
                appcode: this.state.appcode
            })
        }else if(btnName === 'saveformat'){//7、保存列宽
            this.handleSaveColwidth()
        } else if (btnName === 'refresh') {//刷新
            if (Object.keys(this.state.paramObj).length > 0) {
                this.modalSure(this.state.paramObj)
            }
        }
    }

    handleSimpleTableClick = (value, coords, td) => {//控制联查按钮的禁止状态
        if(td.innerHTML){

            this.props.button.setDisabled({ linkdetail: false, multiorg: false });
        }
    }

    render(){
        let { modal } = this.props;
        const { createModal } = modal;
        return (
            <div className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['20028003-000039']} /* 国际化处理： 科目辅助余额表*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <QueryModal
                                    onConfirm={(datas) => {
                                        this.modalSure(datas);
                                    }}
                                />
                            </div>
                            {//按钮集合
                                this.props.button.createButtonApp({
                                    area: 'btnarea',
                                    buttonLimit: 3,
                                    onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                                })
                            }
                        </div>
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                        this.state.fourLables.map((items) => {
                            return(
                            <HeadCom
                                labels={items.title}
                                key={items.title}
                                lastThre = {this.state.headtitle}
                                changeSelectStyle={
                                    (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWidthPage))
                                }
                                accountType = {this.state.accountType}
                                isLong = {items.styleClass}
                                typeDisabled = {this.state.typeDisabled}
                            />
                            )
                        })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                   <SimpleTable
                        ref="balanceTable"
                        data = {this.props.dataout}
                        // onCellMouseDown = {(value, coords, td) => this.handleSimpleTableClick(value, coords, td)}
                        onCellMouseDown = {(e, coords, td) => {
                            //('recorddd:::', this.refs.balanceTable.getRowRecord());
                            if(this.refs.balanceTable.getRowRecord()[0].link && this.state.paramObj.mutibook === 'Y'){
                                this.props.button.setDisabled({multiorg: false})
                            }else{
                                this.props.button.setDisabled({multiorg: true})
                            }
                            this.whetherDetail('link');
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
                </div>

				<RepPrintModal
					noRadio={true}
					noCheckBox={true}
                    appcode={this.state.appcode}
                    visible={this.state.visible}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={this.handleCancel.bind(this)}
                />
				<PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/accountrep/subjassbalanceoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
                {createModal('printService', {
                    className: 'print-service'
                })}
                <Iframe />
            </div>
        )
    }
}

MultiagentSubjectBalanceContent = createPage({})(MultiagentSubjectBalanceContent)
export default MultiagentSubjectBalanceContent;
