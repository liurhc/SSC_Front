/**
 * Created by liqiankun on 2018/6/22.
 * 管理报表 ---> 科目辅助余额表
 */
import React, {Component} from 'react';
import {high, base, ajax} from 'nc-lightapp-front';
import './index.less'
const {
    NCFormControl: FormControl,
    NCDatePicker:DatePicker,
    NCButton: Button,
    NCRadio:Radio,
    NCBreadcrumb:Breadcrumb,
    NCRow:Row, NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,
    NCNumber,
    AutoComplete,
    NCDropdown:Dropdown,
    NCPanel:Panel,
    NCForm,
    NCButtonGroup:ButtonGroup,
    NCInput: Input,
    NCModal:Modal,
    NCInputGroup: InputGroup
} = base;

import MultiagentSubjectBalanceContent from '../content';
import {tableDefaultData} from '../../defaultTableData'
import "../../../public/reportcss/firstpage.less";

class MultiagentSubjectBalance extends Component{
    constructor(props){
        super(props);
        this.state = {
            dataout: tableDefaultData,
        }
    }


    render(){
        return (
            <div className="manageReportContainer">
                <MultiagentSubjectBalanceContent dataout = {this.state.dataout}/>
            </div>
        )
    }
}

ReactDOM.render(<MultiagentSubjectBalance />,document.querySelector('#app'));
