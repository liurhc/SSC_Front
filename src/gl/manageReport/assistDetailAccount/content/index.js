/**
 * Created by liqiankun on 2018/7/05.
 * 账簿查询 ---> 辅助明细账
 */

import React, {Component} from 'react';

import {high, base, ajax, createPage, getMultiLang, gzip, createPageIcon } from 'nc-lightapp-front';
const { NCDiv } = base;
import '../../css/index.less'
import HeadCom from '../../common/headSearch/headCom';
// import {titleLabels } from '../../common/headSearch/titledata';
import { SimpleTable } from 'nc-report';
import QueryModal from './components/MainSelectModal';
import reportPrint from '../../../public/components/reportPrint';
import { setData } from '../../common/simbleTableData';
import {tableDefaultData} from '../../defaultTableData'
import { toast } from '../../../public/components/utils'
import {relevanceSearch, journalAppcode, journalUrl} from "../../referUrl";
import {getDetailPort} from "../../common/modules/modules";
const { PrintOutput } = high;
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import {handleSimpleTableClick} from "../../common/modules/simpleTableClick";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import {searchById} from "../../common/modules/createBtn";
import "../../../public/reportcss/firstpage.less";
import PageButtonGroup from '../../common/modules/pageButtonGroup'
import {rowBackgroundColor} from "../../common/htRowBackground";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
class AssistPropertyBalance extends Component{
    constructor(props){
        super(props);

        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            showFiltration: true,
            disabled: true,
            visible: false,
            appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            outputData: {},
            ctemplate: '', //模板输出-模板
            nodekey: '',
            printParams:{}, //查询框参数，打印使用
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
            textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            dataWithPage: [], //存放请求回来的数据信息，用于分页操作
            firstPageDisable: true, //true 禁止翻页
            lastPageDisable: true,
            paramObj: {},//存放回调的查询参数，用于翻页
            headTitle: {},
            accountType: 'amountcolumn',//金额式
            "assistDetailAccount": [],//辅助明细账的表头
            hideBtnArea : false, // 是否隐藏高级查询按钮
            defaultAccouontBook: {} //默认账簿
        }
        this.switchCount = 0 ;// 转换列名下标计数
        this.dataPage = 1; //存放请求回来的数据椰树信息
        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.handlePage = this.handlePage.bind(this);//页面处理事件
        // this.setData= this.setData.bind(this);// 整理表格所需要的数据格式
        this.queryData = this.queryData.bind(this);//请求数据的方法；
        // this.changeSelectStyle = this.changeSelectStyle.bind(this);//选择 '账簿格式'事件
        this.handleValueChange = handleValueChange.bind(this);
        this.searchById = searchById.bind(this);
        this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                "assistDetailAccount": [//辅助明细账的表头
                    {
                        title: json['20023060-000046'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20023060-000047'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023060-000048'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023060-000049'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023060-000050'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023060-000051'],
                        styleClass:"m-brief"
                    }
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023060','publiccommon', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        this.setState({
            flag: true,
        })
        let appceod = this.props.getSearchParam('c');
        this.searchById('20023060PAGE', appceod).then((res) => {
            // searchById返回当前登录环境的数据源信息。
            // 如果联查的数据源与当前数据源不一致，则隐藏页面的所有按钮。
            let param = this.props.getUrlParam && this.props.getUrlParam('status');
            if (param && res && res.data.context) {
                let { context } =res.data
                let unzipParam = new gzip().unzip(param)
                if (unzipParam.env) {
                    let envs = unzipParam.env.replace(/\[/g, "").replace(/\]/g, "").replace(/\"/g, "").split(",");
                    if (envs[0] && envs[0] != res.data.context.dataSource) {
                        this.props.button.setButtons([]);
                        this.setState({ hideBtnArea: true });
                    }
                }
                this.state.defaultAccouontBook = {refname:context.defaultAccbookName,refpk:context.defaultAccbookPk, getflag: true}
            }
        });
        this.props.button.setDisabled({
            print:true, linkvoucher:true, filter:true,
            switch:true, templateOutput:true, directprint:true,
            saveformat:true,  first: true, pre: true, next: true, last: true
        })
        this.props.button.setButtonVisible(['first', 'pre', 'next', 'last'], false)
        this.getParam()
        // 从多维分析表联查来的，则隐藏过滤按钮
        let from = this.props.getUrlParam('from');
        if (from === 'multianaly') {
            this.props.button.setButtonVisible({ filter: false })
        }
        if(from === 'assistBalance'){
            this.props.button.setButtonVisible({ filter: false, refresh: false })
        }
    }

    showPrintModal = () => {
        this.setState({
            visible: true
        })
    }
    handlePrint(data, isPreview) {
        let printUrl = '/nccloud/gl/accountrep/assdetailprint.do'
        let { printParams, appcode } = this.state
        let { ctemplate, nodekey } = data
        printParams.currpage = this.dataPage.toString() //当前页
        printParams.queryvo = data
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
        printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
        this.handleCancel()
    }
    handleCancel() {
        this.setState({
            visible: false
        });
    }
    showOutputModal = () => {
        // this.refs.printOutput.open()
        let outputUrl = '/nccloud/gl/accountrep/assdetailoutput.do'
        let { appcode, nodekey, ctemplate, printParams } = this.state
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)
        // let outputData = mouldOutput(appcode, nodekey, ctemplate, printParams)
        // this.setState({
        //     outputData: outputData
        // })
    }
    handleOutput() {
         //(this.state.outputData,this.state.json['20023060-000000'])/* 国际化处理： -----模板输出----成功？----*/
    }

    getParam = () => {
        let urlParam;
        // 联查参数，需解压
        let data = this.props.getUrlParam && this.props.getUrlParam('status');
        if (data) {
            urlParam = new gzip().unzip(data)
        }
        // 小友智能查账，不需解压
        let param = this.props.getSearchParam('param');
        if (param) {
            urlParam = JSON.parse(param);
        }
        // 查询
        if (urlParam) {
            this.props.button.setButtonVisible({ 'filter': false })
            this.modalSure(urlParam, 'link');
        }
    }
    handlePage(param, dataWithPage, flag){//页面控制事件
         //('handlePage>>', param, dataWithPage);
        let {paramObj} = this.state;
        let dataLength = Number(dataWithPage.totalpage);
        let renderData = [];
        let obj = {};
        let stringPage = '1';
        switch(param){
            case 'firstPage':
                this.dataPage = 1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;

                this.setState({
                    paramObj,
                    firstPageDisable: true,
                    lastPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                   first: true, pre: true,
                    next: false, last: false
                })
                 //('firstPage:::', this.dataPage, obj, this.state.paramObj);
                 flag=false;
                this.queryData(this.state.paramObj,'',flag);
                return;
            case 'nextPage':
                this.dataPage += 1;
                if(this.dataPage >= dataLength){//如果增加的页数大于总页数就禁止下一页
                    this.setState({
                        lastPageDisable: true
                    })
                    this.props.button.setDisabled({
                        next: true, last: true
                    })
                    return;
                }
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;

                this.setState({
                    paramObj,
                    firstPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    first: false, pre: false
                })
                 //('lengggth:::', typeof this.dataPage,this.dataPage, typeof dataLength, dataLength);

                 //('nextPage>>', this.dataPage, obj, this.state.paramObj);
                this.queryData(this.state.paramObj,'', flag);
                return;
            case 'prePage':
                this.dataPage -=1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                if(this.dataPage === dataLength-1){
                    this.setState({
                        lastPageDisable: false
                    })
                    this.props.button.setDisabled({
                        next: false, last: false
                    })
                }
                if(this.dataPage === 1){
                    this.setState({
                        firstPageDisable: true
                    })
                    this.props.button.setDisabled({
                        first: true, pre: true
                    })
                }
                this.setState({
                    paramObj,
                    selectRowIndex: 0
                })
                 //('prePage:::', typeof this.dataPage,this.dataPage, typeof dataLength, dataLength);

                this.queryData(this.state.paramObj,'', flag);
                return;
            case 'lastPage':
                this.dataPage = dataLength;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                this.setState({
                    paramObj,
                    lastPageDisable: true,
                    firstPageDisable: false,
                    selectRowIndex: 0
                });
                this.props.button.setDisabled({
                    first: false, pre: false,
                    next: true, last: true
                })
                 //('lastPage>>>>>', this.dataPage)
                 flag=false;
                this.queryData(this.state.paramObj,'',flag);
                return;
        }
    }
    handleSwitchColumn(dataWithPage) {// 转换事件
        this.switchCount = ( this.switchCount + 1 ) % 3;
        let renderFirstData = {};
        renderFirstData.column = dataWithPage.column;
        renderFirstData.data = dataWithPage.data;
        renderFirstData.amountcolumn = dataWithPage.amountcolumn;
        renderFirstData.quantityamountcolumn = dataWithPage.quantityamountcolumn;
        setData(this, renderFirstData);
        return;
    }
    queryClick(){//点击"查询"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    modalSure(param,type){//点击弹框"确定"事件
         //('modalSure>>', param);
         let {printParams,showFiltration}=this.state;
        // 非联查来的，设置pageindex为1
        if (!param.link) {
            param.pageindex = '1';
        }
        if(type=='link'){//联查
            showFiltration=false;
        }
        this.dataPage = 1;
        this.props.button.setDisabled({
            first: true, pre: true
        })
        this.queryData(param,type,false);
    }
    queryData(param,type, flag){//请求数据方法；
        this.props.button.setDisabled({
            print:false,
            switch:false, templateOutput:false, directprint:false,
            saveformat:false
        })
        let self = this;
        let {lastPageDisable,firstPageDisable,paramObj,printParams,disabled,selectRowIndex}=self.state;
        if(type=='link'||type=='search'||type=='refresh'){
            firstPageDisable=true // 查询时首页和上一页禁用
        }
        let pageBtn={
            first: true, pre: true,
            next: true, last: true
        };
        let url = '/nccloud/gl/accountrep/assdetailquery.do';
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, success } = response;
                 //('response::::', response);
                if(success && data){
                    if(data.data.length>0){
                        disabled=false;
                        selectRowIndex=0;
                    }

                    if(Number(data.totalpage) < 2){//如果返回的页数小于2就禁止翻页
                        lastPageDisable=true; //true: 禁止点击
                        firstPageDisable=true;
                    } else if (Number(data.totalpage) > 1) {
                        if (self.dataPage <= 1) {
                            firstPageDisable = true;
                            lastPageDisable = false;
                            pageBtn.next = false;
                            pageBtn.last = false;
                        } else if (self.dataPage > 1 && self.dataPage < Number(data.totalpage)) {
                            firstPageDisable = false;
                            lastPageDisable = false;
                            pageBtn.first = false;
                            pageBtn.pre = false;
                            pageBtn.next = false;
                            pageBtn.last = false;
                        } else if (self.dataPage >= Number(data.totalpage)) {
                            firstPageDisable = false;
                            pageBtn.first = false;
                            pageBtn.pre = false;
                        }
                    }
                    self.props.button.setDisabled(pageBtn);
                    self.setState({
                        paramObj: {...param},
                        printParams: {...param},
                        disabled,
                        selectRowIndex,
                        lastPageDisable, //true: 禁止点击
                        firstPageDisable,
                        dataWithPage: data,
                        typeDisabled: false,
                        headTitle: {...data.headtitle}
                    }, () => {
                        if(data.data.length == 0 && self.dataPage <= Number(data.totalpage) && self.dataPage >= 1){
                            self.pageType = 'nextPage';
                            self.handlePage('nextPage', self.state.dataWithPage, true);
                            return;
                        }
                    })

                    let renderFirstData = {};
                    renderFirstData.column = data.column;
                    renderFirstData.data = data.data;
                    renderFirstData.amountcolumn = data.amountcolumn;
                    renderFirstData.quantityamountcolumn = data.quantityamountcolumn;                    setData(self,renderFirstData, flag);
                }else{
                    toast({content: self.state.json['20023060-000001'], color: 'danger'});/* 国际化处理： 请求数据为空！*/
                }
            },
            error:function(error){
                toast({ content: error.message, color: 'warning' });
                //toast({content: '数据请求失败！', color: 'danger'});
                throw error
            }
        })
    }

    //直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    //保存列宽
    handleSaveColwidth=()=>{
        let {json}=this.state
        let info=this.refs.balanceTable.getReportInfo();
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.appcode,info.colWidths,json, callBack);
    }
    handleLoginBtn = (obj, btnName) => {
         //('handleLoginBtn>>>', obj, btnName)
        if(btnName === 'print'){//打印
            this.showPrintModal()
        }else if(btnName === 'linkvoucher'){//联查凭证
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: relevanceSearch,
                key: 'relevanceSearch',
                appcode: '20023030'
            }, this.state.json)
        }else if(btnName === 'filter'){//过滤
            let selectRowData = this.refs.balanceTable.getRowRecord()[0];
            if (selectRowData.linkmsg) {
                toast({ content: selectRowData.linkmsg[0], color: 'warning' });
                return;
            }
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: journalUrl,
                key: 'detailToJournal',
                appcode: journalAppcode
            }, this.state.json)
        }else if(btnName === 'switch'){//转换
            this.handleSwitchColumn(this.state.dataWithPage)
        }else if(btnName === 'templateOutput'){//模板输出
            this.showOutputModal()
        }else if(btnName === 'directprint'){//直接输出
            this.printExcel()
        }else if(btnName === 'saveformat'){//保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'refresh'){//刷新
            if(Object.keys(this.state.paramObj).length > 0){
                this.modalSure(this.state.paramObj,'refresh')
            }

        }else if(btnName === 'first'){//首页
            this.handlePage('firstPage', this.state.dataWithPage)
        }else if(btnName === 'pre'){//上一页
            this.pageType = 'prePage';
            this.handlePage('prePage', this.state.dataWithPage)
        }else if(btnName === 'next'){//下一页
            this.pageType = 'nextPage';
            this.handlePage('nextPage', this.state.dataWithPage)
        }else if(btnName === 'last'){//末页
            this.pageType = 'prePage';
            this.handlePage('lastPage', this.state.dataWithPage)
        }
    }
    firstCallback = () => {//首页
        this.handlePage('firstPage', this.state.dataWithPage)
    }
    preCallBack = () => {//上一页
        this.pageType = 'prePage';
        this.handlePage('prePage', this.state.dataWithPage)
    }
    nextCallBack = () => {//下一页
        this.pageType = 'nextPage';
        this.handlePage('nextPage', this.state.dataWithPage)
    }
    lastCallBack = () => {//末页
        this.pageType = 'prePage';
        this.handlePage('lastPage', this.state.dataWithPage)
    }
    render(){
        let { modal } = this.props;
        const { createModal } = modal;
        return(
            <div className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['20023060-000002']} /* 国际化处理： 辅助明细账*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <QueryModal
                                    hideBtnArea={this.state.hideBtnArea}
                                    defaultAccouontBook={this.state.defaultAccouontBook}
                                    onConfirm={(datas) => {
                                        this.modalSure(datas, 'search');
                                    }}
                                />
                            </div>
                            {//按钮集合
                                this.props.button.createButtonApp({
                                    area: 'btnarea',
                                    buttonLimit: 3,
                                    onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName)
                                })
                            }
                        </div>
                    }
                    pageBtnContent = {
                        <PageButtonGroup
                            first={{disabled: this.state.firstPageDisable, callBack: this.firstCallback}}
                            pre={{disabled: this.state.firstPageDisable, callBack: this.preCallBack}}
                            next={{disabled: this.state.lastPageDisable, callBack: this.nextCallBack}}
                            last={{disabled: this.state.lastPageDisable, callBack: this.lastCallBack}}
                        />
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.assistDetailAccount.map((items) => {
                                return(
                                    <HeadCom
                                        labels={items.title}
                                        key={items.title}
                                        lastThre = {this.state.dataWithPage && this.state.dataWithPage.headtitle}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
                                        }
                                        accountType = {this.state.accountType}
                                        isLong = {items.styleClass}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data = {this.state.dataout}
                        onCellMouseDown = {(e, coords, td) => {
                             //('recorddd:::', this.refs.balanceTable.getRowRecord());
                            this.whetherDetail('link', 'multiAccountSearch');//multiAccountSearch
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
                </div>

                <RepPrintModal
                    appcode={this.state.appcode}
                    showScopeall={true}
                    visible={this.state.visible}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={this.handleCancel.bind(this)}
                />
                <PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/accountrep/assdetailoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
                {createModal('printService', {
                    className: 'print-service'
                })}
                <Iframe />
            </div>
        )
    }
}


AssistPropertyBalance = createPage({})(AssistPropertyBalance)

ReactDOM.render(<AssistPropertyBalance />,document.querySelector('#app'));
