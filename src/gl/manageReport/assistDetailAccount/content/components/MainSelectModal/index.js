import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
//import moment from 'moment';

import {high,base,ajax, deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import '../../../../css/searchModal/index.less';


const { Refer } = high;

const { NCDatePicker:DatePicker, NCRadio:Radio, NCRow:Row,NCCol:Col,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCInput: Input, NCDiv
} = base;

import { toast } from '../../../../../public/components/utils';
import FormLabel from '../../../../../public/components/FormLabel';

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';
const Timeformat = "YYYY-MM-DD HH:mm:ss";

// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import AccPeriodDefaultTreeGridRef from '../../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';
import NewModal from '../modal'
import AccountDefaultModelTreeRef from '../../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../../public/components/uapRefer.js';

import ReferEle from '../../../../../../gl/refer/voucher/OperatorDefaultRef/index.js';
import VoucherRefer from '../../../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef/index';//凭证参照
import DateGroup from '../../../../common/modules/dateGroup'



import {
    renderMoneyType,
    getSubjectVersion,
    createReferFn,
    getCheckContent,renderRefer, delKeyOfObj,
    businessUnit, returnMoneyType, getReferDetault
} from '../../../../common/modules/modules';
import {getAdjustTime} from '../../../../../public/hansonTableSetData/commonFn'
import SubjectVersion from "../../../../common/modules/subjectVersion";
import SubjectLevel from '../../../../common/modules/subjectAndLevel'
import {clearTransterData, handleChange, handleSelectChange} from "../../../../common/modules/transferFn";
import CheckBoxCells from '../../../../common/modules/checkBox';
import {handleValueChange} from '../../../../common/modules/handleValueChange'
import FourCheckBox from '../../../../common/modules/fourCheckBox'
import initTemplate from '../../../../modalFiles/initTemplate';

import {handleNumberInput} from "../../../../common/modules/numberInputFn";
import {getRecover} from "../../../../common/modules/recoverData";
import { FICheckbox } from '../../../../../public/components/base';

class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state=this.getAllState()

		this.handleTableSelect = this.handleTableSelect.bind(this);//辅助核算表格中的⌚️
		this.balanceMoney = this.balanceMoney.bind(this);//余额范围输入触发事件

        this.sureHandle = this.sureHandle.bind(this);
        this.closeNewModal = this.closeNewModal.bind(this);
        this.outInputChange = this.outInputChange.bind(this);

        this.renderTableFirstData = this.renderTableFirstData.bind(this);// table渲染初始的数据
        this.handleSearchObj = this.handleSearchObj.bind(this);//查询对象 选择触发事件
		this.findByKey = this.findByKey.bind(this);
		this.renderSearchRange = this.renderSearchRange.bind(this);//根据'查询对象'的选择来对应渲染'查询范围'的组件
		this.renderRefPathEle = this.renderRefPathEle.bind(this);//根据传进来的url渲染对应的参照组件
		this.handleCheckbox = this.handleCheckbox.bind(this);//计算小计	包含下级复选框的点击事件
		this.secondModalSelect = this.secondModalSelect.bind(this);//第二层弹框的选择框
		this.renderNewRefPath = this.renderNewRefPath.bind(this);//渲染第二层弹框内容的参照

		this.handleDateChange = this.handleDateChange.bind(this);//日期: 范围选择触发事件
		this.onChangeShowzerooccur = this.onChangeShowzerooccur.bind(this);//无发生不显示;本期无发生无余额不显示
		this.handleSortOrShowStyle = this.handleSortOrShowStyle.bind(this);//多核算账簿排序方式/多核算账簿显示方式
		this.handleDateRadio = this.handleDateRadio.bind(this);//会计期间/日期的单选框事件

		this.handleSelectChange = this.handleSelectChange.bind(this);//<Select>的onChange事件统一触发事件
		this.renderLevelOptions = this.renderLevelOptions.bind(this);//渲染级次的<NCOption>
		this.renderReferEle = this.renderReferEle.bind(this);//渲染 制单人；出纳人；审核人；记账人
		this.handleVocherChange = this.handleVocherChange.bind(this);// 凭证号输入；不能输入小数

		this.renderRefer = renderRefer.bind(this);
		this.handleValueChange = handleValueChange.bind(this);
        this.handleNumberInput = handleNumberInput.bind(this);
        this.getRecover = getRecover.bind(this);
        this.searchId = '20023060query';
        this.index = '' //账簿打印行号
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                dateInputPlaceholder: this.state.json['20023060-000003'], ///* 国际化处理： 选择日期*/
                currtype: json['20023060-000004'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['20023060-000004'],  //选择的币种名称/* 国际化处理： 本币*/
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023060', 'dategroup', 'checkbox', 'fourcheckbox', 'subjectandlevel', 'subjectversion', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        if(!this.props.hideBtnArea){  
            initTemplate.call(this,this.props, this.state.appcode) 
        }
        this.getCurrtypeList();
        this.renderTableFirstData(10);

    }

    componentDidMount() {
        setTimeout(() => getReferDetault(this, true, {
            businessUnit,
            getCheckContent
        }, 'assistDetailAccount',this.props.defaultAccouontBook),0);
        // setTimeout(() => initTemplate.call(this,this.props, this.state.appcode), 0)
        this.props.MainSelectRef && this.props.MainSelectRef(this)
    }
    //获取初始化state
	getInitState = () => {
		let initState = {
            defaultAccouontBook:[this.props.defaultAccouontBook],
            appcode: '20023060',
		    // json: {},
            dateInputPlaceholder: '', ///* 国际化处理： 选择日期*/
            assistDateValue: '', //辅助参照中的日期的state值
            assistDateTimeValue: '', //辅助参照中的日期+时分秒的state值
            stringRefValue: '', //辅助参照中的字符的state值
            numberRefValue: '',//辅助参照中的数值的state值
            numberRefValueInteger: '',//辅助参照中整数值
            booleanValue: '', //辅助参照中布尔值
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            selectedKeys: [],
            targetKeys: [],

            changeParam: false, //false:点击条件时不触发接口，,
            rightSelectItem: [], //科目的"条件"按钮中存放右侧数据的pk值

            showRadio: true,//控制是否显示"会计期间"的但选框；false: 不显示
            versiondate: '', //存放选择的版本号
            versionDateArr: [], //存放请求账簿对应的版本数组
            busiDate: '', //业务日期
			showNewModal: false,
            renderData: [], //我的弹框里数据
			innerSelectName: [], //"查询范围"里存放的数据
            innerSelectData: [], //最里层选择数据的项
            rangefrom: '', //余额范围起始值
            rangeto: '', //余额范围结束值

            listItem:{},//业务单元中模板数据对应值
			isShowUnit:false,//是否显示业务单元
			selectedSort: 'true',//排序方式
			selectedShowType: 'false', //多主题显示方式
			doubleCell: false, // 多业务单元合并
			allLast: true, // 所有末级是否选中
			outProject: false, //表外科目是否选中
			multiProjectMerge: true, //多主题合并
			subjectShow: true, //按主题列示
			balanceMoneyLeft: '', //余额范围左边输入框
            balanceMoneyRight: '', //余额范围右边输入框
			accountingbook: this.props.defaultAccouontBook&&this.props.defaultAccouontBook.refpk?[this.props.defaultAccouontBook]:[],  //核算账簿
			accountingUnit: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: [],//业务单元
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表

			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向

			/***现金日计记账/多主题科目余额表：级次***/
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次

            /***现金日计记账/多主题科目余额表：科目***/
			startcode: {}, //开始科目编码
			endcode: {},    //结束科目编码

			level:0, //返回的最大级次
			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			/***无发生不显示;本期无发生无余额不显示***/
			showzerooccur: false, //无发生不显示
            ckNotShowZeroOccurZeroBalance: false, //本期无发生无余额不显示
			/***多核算账簿显示方式***/
            defaultShowStyle: 'false',
            /***多核算账簿排序方式***/
            defaultSortStyle: 'false',
            cbCorpSubjDspBase: false,//按各核算账簿显示科目名称
            cbQryByCorpAss: false,//按各核算账簿查询辅助核算

			upLevelSubject: false, //显示上级科目
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: false, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			start: {},  //开始期间
			end: {}, //结束期间

			/*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
			startyear: '', //开始年
            startperiod: '',  //开始期间
			endyear: '',  //结束年
			endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示

            searchDateStyle: '0',//现金日记账 按制单/签字查询
			isversiondate: false,

            indexArr: [], //存放"查询对象"已经选择的索引
            pk_accperiodscheme: '',//会计期间需要的参数

			//tabel组件头部

			//table的数据
			tableSourceData: [],

			//第二层弹框里的table

            secondData: [],//第二层表格的数据

            selectRow: [],//存放所选择的不是参照类型的数据
            page: 0, //第二个弹框中要显示的行内容索引

			madeBillPeople: {refname: '', refpk: ''}, //制单人选择的参照数据
            cashierPeople: {refname: '', refpk: ''},  //出纳人选择的参照数据
            checkPeople: {refname: '', refpk: ''},    //审核人选择的参照数据
            chargePeople: {refname: '', refpk: ''},   //记账人选择的参照数据
            abstract: '', //摘要输入值
            oppositeSide: false, //对方科目取小值
			chooseoppositeSide: true, //对方科目取小值 是否可选 true:不可选  false: 可选
			voucherRefer: {refname: '', refpk: ''}, //凭证类别的参照数据
            vocherInputLeft: '', //凭证号左侧输入框
            vocherInputRight: '', //凭证号右侧侧输入框

            //显示本币合计；无发生不显示；期间无发生显示累计数；合并同凭证分录
            "disLocalCurySum":false, //显示本币合计；是否选中
            LocalCurySumEdit: true,//显示本币合计是否可编辑


            "ckNoAccurNoDsp": false, //无发生不显示true
            "dspAccumu": false, //期间无发生显示累计数true
            "combineVoucher": false, //合并同凭证分录true
            sortType: '0', // 排序方式。制单日期0，凭证号1
            addupByDay: false, //按日合计true
		}
		return initState
	}
	//state中添加json
	getAllState = () => {
		let initState = this.getInitState()
		initState.json={}
		return initState
    }
    
	//初始化state
    setInitState = (querystate) => {
		if(JSON.stringify(querystate) !=='{}') {
			this.setState(querystate)
		} else {
            let initState = this.getInitState()
            delKeyOfObj(initState, ['dateInputPlaceholder', 'currtype', 'currtypeName', 'currtypeList', 'tableSourceData'])
            this.setState(initState);
            getReferDetault(this, true, {
                businessUnit,
                getCheckContent
            }, 'assistDetailAccount',this.props.defaultAccouontBook)
        }
	}
    //打开高级查询面板
	openAdvSearch = (index, querystate) => {
        initTemplate.call(this,this.props, this.state.appcode).then((data)=>{
			this.props.search.openAdvSearch(this.searchId, true)
			this.index = index;
			this.setInitState(querystate);
		})
    }
    //获取返回父组件的值
    getConfirmData = (tempState) => {
        let pk_accountingbook = [];
		let pk_unit = [];
		let moneyLeft = Number(tempState.balanceMoneyLeft);
		let moneyRight = Number(tempState.balanceMoneyRight);
		//('moneyyyy::', moneyLeft,  moneyRight, moneyLeft < moneyRight)
		if(moneyLeft > moneyRight){
			toast({
				content: tempState.json['20023060-000026'],/* 国际化处理： 余额范围查询条件信息输入有误(起始值大于终止值或是为负值)！*/
				color: 'warning'
			});
			return;
		}
		let lastAssvos = [];
		tempState.tableSourceData.forEach((item)=>{
			//('assvos>>>',item);
			let resultObj = {};
			let valueArr = [];
			if(item.selectCell && !item.selectCellAttr){
				resultObj.name = item.selectCell.name;
				resultObj.pk_checktype = item.selectCell.pk_checktype;
				item.selectRange && Array.isArray(item.selectRange) && item.selectRange.map((item) => {
					valueArr.push(item.pk_checkvalue)
				})
				resultObj.pk_checkvalue = Array.isArray(item.selectRange) ? valueArr.join(',') : item.selectRange;
				resultObj.headEle = item.showLocal ? 'Y' : 'N';
				resultObj.accEle = item.accele ? 'Y' : 'N';
				resultObj.includeSub = item.includesub ? 'Y' : 'N';
				lastAssvos.push(resultObj)
			} else if(item.selectCell && item.selectCellAttr){
				resultObj.name = item.selectCell.name;
				resultObj.pk_checktype = item.selectCell.pk_checktype;
				resultObj.attr = [];
				resultObj.headEle = item.showLocal ? 'Y' : 'N';
				resultObj.accEle = item.accele? 'Y' : 'N';
				resultObj.includeSub = item.includesub ? 'Y' : 'N';
				item.selectCellAttr.map((itemCell) => {
					if(itemCell.select){
						resultObj.attr.push(itemCell);
					}
				})
				lastAssvos.push(resultObj)
			}
		})
		//('lastAssvos>>', lastAssvos)
		if (Array.isArray(tempState.accountingbook)) {
			tempState.accountingbook.forEach(function (item) {
				pk_accountingbook.push(item.refpk)
			})
		}

		if (Array.isArray(tempState.buSecond)) {
			tempState.buSecond.forEach(function (item) {
				pk_unit.push(item.refpk)
			})
		}
		//('>>>>>/////???', pk_unit);
        //
        let confirmData = {
            /*******核算账簿****/
            pk_glorgbooks: pk_accountingbook,//[""]
            /*******业务单元****/
            pk_orgs: pk_unit,
            multiBusi: tempState.multbusi,//复选框是否勾选

            /*******科目版本****/
            useSubjVersion: tempState.isversiondate,//是否勾选
            subjVersion: tempState.isversiondate ? tempState.versiondate : tempState.busiDate, //tempState.versiondate,//科目版本

            /*******查询对象 table框的内容****/
            qryObjs: lastAssvos,

            /*******会计期间参数****/
            selectionState: tempState.selectionState,
            beginyear: tempState.startyear,
            beginperiod: tempState.startperiod,
            endyear: tempState.endyear,
            endperiod:   tempState.endperiod,
            /*******日期参数****/
            begindate: tempState.begindate,
            enddate: tempState.enddate,

            /*******包含凭证****/
            includeUnTallyed: tempState.includeuntally ? 'Y' : 'N',//未记账凭证
            includeError: tempState.includeerror ? 'Y' : 'N',// 错误凭证
            includeTransfer: tempState.includeplcf ? 'Y' : 'N',//损益结转凭证
            includeReClassify: tempState.includerc ? 'Y' : 'N',//重分类凭证

            /*******币种****/
            pk_currtype: tempState.currtype,//
            currTypeName: tempState.currtypeName,//

            /*******返回币种****/
            return_currtype: tempState.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局

            /***排序方式***/
            "sortType": tempState.sortType, //排序方式。制单日期0，凭证号1
            "addupByDay": tempState.addupByDay, //按日合计true

            /***显示本币合计；无发生不显示；期间无发生显示累计数；合并同凭证分录***/
            "disLocalCurySum": tempState.disLocalCurySum, //显示本币合计true
            "ckNoAccurNoDsp": tempState.ckNoAccurNoDsp, //无发生不显示true
            "dspAccumu": tempState.dspAccumu, //期间无发生显示累计数true
            "combineVoucher": tempState.combineVoucher, //合并同凭证分录true

            /*******余额方向****/
            balanceDirection: tempState.balanceori, //"双向/借/贷/平", //余额方向
            showBusDate: tempState.ckNotShowZeroOccurZeroBalance,//显示业务日期true

            pageindex: '1',
            pk_accasoa: tempState.rightSelectItem,//'条件'

            /*******多核算账簿显示方式;多核算账簿排序方式****/
            "mutibook": tempState.defaultShowStyle, //多核算账簿显示方式：多核算账簿合并(true )
            appcode: tempState.appcode,
            index: this.index
        }
        return confirmData
    }
    
    handleVocherChange(value, param){// 凭证号输入；不能输入小数
    	let result = value;
        let arrResult = result.split('');//输入值变为数组
        let code = arrResult[arrResult.length - 1];//最后一次输入的元素
        let codeValue = code && code.toString().charCodeAt(0);
        if(codeValue < 48 || codeValue > 57) {//不是数字的输入
            arrResult.splice(arrResult.length - 1);
        }
        this.setState({
            [param]: arrResult.join('')
        });
	}
    renderLevelOptions(num){
    	let optionArr = [];
    	for(let i=0; i<num; i++){
            optionArr.push(<NCOption value={i+1} key={i}>{i+1}</NCOption>);
		}
        return optionArr;
	}
    handleSelectChange(value, param){//<Select>的onChange事件统一触发事件
    	this.setState({
			[param]: String(value)
		})
	}
    handleDateRadio(value, param){//会计期间/日期的单选框事件
    	this.setState({
			[param]: value
		})
	}
	handleSortOrShowStyle(value, param){//多核算账簿显示方式/多核算账簿排序方式触发事件
		this.setState({
			[param]: value
		})
	}
    handleDateChange(value){//日期: 范围选择触发事件
    	this.setState({
            begindate: value[0],
            enddate: value[1],
			rangeDate: value
		})
	}
    handleSearchObj(values, record){//查询对象选择具体科目
        let value = JSON.parse(values)
    	let { tableSourceData, selectRow } = this.state;
        let originData = this.findByKey(record.key, tableSourceData);
        originData.selectCell = value;
        record.selectCells = [];
        record.selectRange = [];
        if(value.refpath ){
            originData.type = 'referpath';
            originData.searchRange = value.refpath;
		}else if(value.datatype === '1'){//字符
            originData.type = 'string';
        } else if(value.datatype === '4'){//整数
            originData.type = 'integer';
        } else if(value.datatype === '31'){//数值(小数)
            originData.type = 'number';
        } else if(value.datatype === '32'){//布尔
            originData.type = 'boolean';
        } else if(value.datatype === '33'){//日期
            originData.type = 'date'
        } else if(value.datatype === '34'){//日期+时分秒时间
            originData.type = 'dateTime'
        } else if(value.attr){
            originData.selectCellAttr = [...value.attr];
            originData.type = 'modal'
		} else {
            originData.type = ''
		}
		let newSelectRow = [...selectRow];
        newSelectRow[record.key] = record;
    	this.setState({
            tableSourceData,
			selectRow: newSelectRow
		})
	}
    handleCheckbox(key, record, value){//计算小计	包含下级复选框的点击事件
    	let {tableSourceData} = this.state;
        record[key] = value;
        this.setState({
            tableSourceData
		})

	}
    secondModalSelect(record, index){//第二层弹框的复选框
    	let {tableSourceData} = this.state;
        let newSelect = !record.select;
        record.select = newSelect;
        tableSourceData[record.itemKey].selectCellAttr[index] = record;
        this.setState({tableSourceData})
	}
    findByKey(key, rows) {
        let rt = null;
        let self = this;
        rows.forEach(function(v, i, a) {
            if (v.key == key) {
                rt = v;
            }
        });
        return rt;
    }
    dateRefChange = (key, value, record) => {
        let length = record.selectCell.inputlength;
        let exeReg = new RegExp("^.{0," + length + "}$");
        let result = exeReg.exec(value);
        if(result && key === 'stringRefValue'){
            record.selectRange = result[0]
            this.setState({
                [key]: result[0]
            })
        }else {
            record.selectRange = value
            this.setState({
                [key]: value
            })
        }
    }
    numberRefChange = (key, value, record, param) => {
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectRange = this.state[key], 0)

    }
    renderSearchRange(record, disabled, fieldid){//根据'查询对象'的选择来对应渲染'查询范围'的组件
    	switch(record.type){
			case "referpath":
				let refPathEle = this.renderRefPathEle(record, disabled, fieldid);
				return refPathEle;
            case 'date':
                return (
                    <DatePicker
                        fieldid={fieldid}
                        format={format}
                        disabled={disabled}
                        value={this.state.assistDateValue}
                        placeholder={this.state.dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateValue', value, record)}
                    />
                );
            case 'string':
                return <Input
                    fieldid={fieldid}
                    disabled={disabled}
                    value={this.state.stringRefValue}
                    onChange={(value) => this.dateRefChange('stringRefValue', value, record)}
                />;
            case 'number':
                return <Input
                    fieldid={fieldid}
                    disabled={disabled}
                    value={this.state.numberRefValue}
                    onChange={(value) => this.numberRefChange('numberRefValue', value, record, 'assistBalance') }
                />;
            case 'integer'://整数
                return <Input
                    fieldid={fieldid}
                    disabled={disabled}
                    value={this.state.numberRefValueInteger}
                    onChange={(value) => this.numberRefChange('numberRefValueInteger', value, record, 'assistBalanceInteger') }
                />;
            case 'boolean'://布尔
                return <Select
                    showClear={false}
                    fieldid={fieldid}
                    disabled={disabled}
                    value={this.state.booleanValue}
                    className='search-boolean'
                    onChange={(value)=>{
                        this.dateRefChange('booleanValue', value, record)
                    }}
                >
                    <NCOption value="Y">{this.state.json['20023060-000030']}</NCOption>{/* 国际化处理： 是*/}
                    <NCOption value="N">{this.state.json['20023060-000031']}</NCOption>{/* 国际化处理： 否*/}
                    <NCOption value="">{this.state.json['20023060-000032']}</NCOption>{/* 国际化处理： 空*/}
                </Select>;
            case 'dateTime'://日期+时分秒
                return (
                    <DatePicker
                        fieldid={fieldid}
                        showTime={true}
                        format={Timeformat}
                        disabled={disabled}
                        value={this.state.assistDateTimeValue}
                        placeholder={this.state.dateInputPlaceholder}
                        onChange={(value) => this.dateRefChange('assistDateTimeValue', value, record)}
                    />
                );
			case 'modal':
                let attrObj = record.selectCellAttr;
                let { tableSourceData } = this.state;
                let selectObj = [];
                attrObj.map((item)=>{
                	if(item){
                        item.itemKey = record.key;
					}
					if(item && item.selectName){
                        selectObj += item.selectName + ','
					}
				})
				return (
                    <div style={{display: 'flex'}}>
                        <Input
                            fieldid={fieldid}
                            disabled={disabled}
                            style={{border: 'none'}}
							placeholder={record.selectCell && record.selectCell.name}
							value={selectObj}
						/>
                        <div className='ellipsisContainer'
                             onClick={() => this.closeNewModal(record)}
                        >
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                        </div>
                    </div>
				);
			default:
				return <Input fieldid={fieldid} disabled/>;
		}
	}
    renderNewRefPath(record){
        let mybook;

        if(record.refpath){
            let objKey = record.attrclassid;
            if(!this.state[objKey]){//undefined
                {createScript.call(this,record.refpath+".js",objKey)}

            }else {
                mybook = (
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state[objKey] ? (this.state[objKey])(
                                    {
                                        value:{refname: record.selectName && record.selectName[0], refpk:  record.refpk && record.refpk[0]},
                                        isMultiSelectedEnabled: true,
                                        queryCondition: () => {
                                            return {
                                                "pk_accountingbook": this.state.accountingbook[0].pk_accountingbook ? this.state.accountingbook[0].pk_accountingbook : '',
                                                // "versiondate":currrentDate,
                                            }
                                        },
                                        onChange: (v) => {
                                            let {tableSourceData} = this.state;
                                            let refpkArr = [];
                                            let selectName = [];
                                            v.forEach((item) => {
                                                refpkArr.push(item.refpk);
                                                selectName.push(item.refname)
                                            })
                                            let pk_checkvalue = refpkArr.join(',')
                                            record.pk_checkvalue = pk_checkvalue;
                                            record.selectName = selectName;
                                            record.refpk = refpkArr;
                                            this.setState({
                                                tableSourceData
                                            })
                                        }
                                    }
                                ) : <div/>
                            }
                        </Col>
                    </Row>
                );
            }
		}

        return mybook;
	}
	renderRefPathEle(record, disabled, fieldid){//根据传进来的url渲染对应的参照组件
        //带有参照的

		let mybook;
		let objKey = record.key + record.selectCell.pk_checktype;
		if(!this.state[objKey]){//undefined
			{createScript.call(this,record.selectCell.refpath+".js",objKey)}

		}else{
            let pkOrgParam = this.state.buSecond.length > 0 ? this.state.buSecond[0].refpk : this.state.unitValueParam;
            let options = {
                fieldid: fieldid,
                value: record.selectCells,
                isMultiSelectedEnabled:true,
                disabled: disabled,
                "isShowDisabledData": true,
                queryCondition:() => {
                    return {
                        "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                        "pk_org": pkOrgParam,
                        "isDataPowerEnable": "Y",
                        "DataPowerOperationCode" : 'fi',
                        "dateStr": this.state.isversiondate ? this.state.versiondate : this.state.busiDate,
                    }
                },
                onChange: (v)=>{
                    let {tableSourceData} = this.state;
                    let refpkArr = [];
                    let selectCell = []
                    v.forEach((item)=>{
                        let cellObj = {};
                        cellObj.pk_checkvalue = item.refpk;
                        cellObj.checkvaluecode = item.refcode;
                        cellObj.checkvaluename = item.refname;
                        refpkArr.push(cellObj);
                        selectCell.push(item)
                    })
                    let pk_checkvalue = [...refpkArr];
                    record.selectRange = pk_checkvalue;
                    record.selectCells = selectCell
                    this.setState({
                        tableSourceData
                    })
                }
            };
            let newOPtions = {}
            if(record.selectCell.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.selectCell.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                newOPtions = {
                    ...options,
                    "unitValueIsNeeded":false,
                    "isShowDimission":true,
                    queryCondition:() => {
                        return {
                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                            "pk_org": pkOrgParam,
                            "isDataPowerEnable": "Y",
                            "DataPowerOperationCode" : 'fi',
                            "dateStr": this.state.isversiondate ? this.state.versiondate : this.state.busiDate,
                            "busifuncode":"all",
                            isShowDimission:true
                        }
                    },
                    isShowUnit:true,
                    unitProps:{
                        refType: 'tree',
                        refName: this.state.json['20023060-000015'], ///* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:this.state.json['20023060-000015'],refpk:'root'}, ///* 国际化处理： 业务单元*/
                        placeholder:this.state.json['20023060-000015'], ///* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[this.state.json['20023060-000016'], this.state.json['20023060-000017']],code: ['refcode', 'refname']}, ///* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false
                    },
                    unitCondition:{
                        pk_financeorg: pkOrgParam,
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    },
                }
            }else if(record.selectCell.classid && record.selectCell.classid.length === 20){
                newOPtions = {
                    ...options,
                    pk_defdoclist: record.selectCell.classid
                }
            }else {
                newOPtions = {
                    ...options
                }
            }
            mybook =  (
				<Row className='tableselectItem'>
					<div>
						{
							this.state[objKey]?(this.state[objKey])(newOPtions):<div/>
						}
					</div>
				</Row>
			);
		}
		return mybook;
	}
    renderTableFirstData(length){
    	let firstData = [];
    	for(let i=0; i<length; i++){
    		let obj = {};
    		obj.searchObj = [];
            obj.searchRange = '';
            obj.showLocal = false;//显示位置
            obj.accele = false;//小计
            obj.includesub = false,//包含下级
			obj.key = i
            firstData.push(obj);
		}
		this.setState({
            tableSourceData: [...firstData]
		})
	}

	handleTableSelect(){

	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}




	getCurrtypeList = () => {//获取"币种"接口
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {"localType":"1", "showAllCurr":"1"};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
                        currtypeList: data,
                        // currtype: self.state.json['20023060-000004'],      //选择的币种/* 国际化处理： 本币*/
                        // currtypeName: self.state.json['20023060-000004'],  //选择的币种名称/* 国际化处理： 本币*/
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


    handleCurrtypeChange = (keyParam, value) => {
		if(keyParam === 'currtypeName'){//币种
    		if(value === this.state.json['20023060-000018']){/* 国际化处理： 所有币种*/
                this.setState({
                    LocalCurySumEdit: false,//显示本币合计是否可编辑
                })
			}else{
                this.setState({
                    LocalCurySumEdit: true,//显示本币合计是否可编辑
                    "disLocalCurySum":false, //显示本币合计；是否选中
                })
			}

		}
        this.setState({
            [keyParam]: value,
        });
    }


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(v) {
		if (!this.state.accountingbook || !this.state.accountingbook[0]) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,	
			year: '',		
		};
		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        		// versiondate: '2018-5-31', //data.versiondate,
		        	},)
                    getSubjectVersion(self)

                    let newUrl = '/nccloud/gl/accountrep/assbalancequeryobject.do';//现金日记账
                    let newData = {
                        "pk_accountingbook":v[0].refpk,
                        "versiondate": '2018-5-31',//self.state.versiondate,
                        "needaccount": true
                    };
                    ajax({
                        url:newUrl,
                        data:newData,
                        success: function(response){
                            const { success } = response;
                            //渲染已有账表数据遮罩
                            if (success) {
                                if(response.data){
                                    response.data.map((item) => {
                                    	if(item.attr){
                                            item.attr.map((cell) => {
                                                cell.select = false
											})
										}
									})

                                    let newTableFirstData = [...self.state.tableSourceData];
                                    newTableFirstData.forEach((item, index) => {
                                        item.searchObj = response.data;
									})

                                    self.setState({
                                        tableSourceData: newTableFirstData
                                    })
                                }
                            }
                        }
                    });
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: '2018-5-31'// value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化/本期无发生无余额不显示;按各核算账簿显示科目名称/按各核算账簿查询辅助核算
	onChangeShowzerooccur(value, param) {
		this.setState({
			[param]: value
		});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}
	//显示上级科目复选框变化
	onChangeupLevelSubject(e) {
		this.setState({upLevelSubject: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}
	handleRadioSort(param){
		this.setState({
			selectedSort: param
		})
	}
	handleShowType(param){
		this.setState({
			selectedShowType: param
		})
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
	onChangeDoubleCell(param){
		this.setState({
			doubleCell: !this.state.doubleCell
		})
	}
	changeProjectStyle(param){//所有末级   表外科目选择
		if(param === 'allLast'){
			this.setState({
				allLast: !this.state.allLast
			})
		}else{
			this.setState({
				outProject: !this.state.outProject
			})
		}
	}

	handleGTypeChange =() =>{
		
	};

    shouldComponentUpdate(nextProps, nextState){
		return true;
	}



    outInputChange(param){
    	let nameArr = [];
        param.forEach((item) => {
            nameArr.push(item.refname);
		})
		this.setState({
            innerSelectName: [...nameArr]
		})
	}
    sureHandle(param){
    	let SelectCell = [];
        // param.forEach((item) => {
        	// if(item.pk_checkvalue){
         //        SelectCell.push(item);
		// 	}
		// });
        this.setState({
			innerSelectData: [...SelectCell]
		})
    	this.closeNewModal(param);
	}
    closeNewModal(param){
		this.setState({
            showNewModal: !this.state.showNewModal,
			page: param.key
		})
    }
    balanceMoney(value, param){
        let arrResult = value.split('');//输入值变为数组
        let code = arrResult[arrResult.length - 1];//最后一次输入的元素
        let codeValue = code && code.toString().charCodeAt(0);
        if (codeValue < 48 || codeValue > 57) {//不是数字的输入给剪掉
            arrResult.splice(arrResult.length - 1);
        }
    	this.setState({
			[param]: arrResult.join('')//数组变为字符串
		})
	}

	/****
	 * param: String
	 * 根据传入的参数渲染对应的参照组件
	 * ***/
	renderReferEle(param){
    	let referUrl = "gl/refer/voucher/OperatorDefaultRef/index.js" //
        if(!this.state[param]){//undefined
            {createScript.call(this,referUrl,param)}
        }else{
            return (
                <Row>
                    <Col xs={12} md={12}>
                        {
                            this.state[param]?(this.state[param])(
                                {
                                    value:{refname: this.state.accountingbook[0] && this.state.accountingbook[0].refname, refpk: this.state.accountingbook[0] && this.state.accountingbook[0].refpk},
                                    queryCondition:() => {
                                        // return {
                                        // 	"pk_accountingbook": self.state.pk_accountingbook.value
                                        // }
                                    },
                                    onChange: (v)=>{
                                        let self = this;

                                    }

                                }
                            ):<div/>
                        }
                    </Col>
                </Row>
			)
        }
	}
    // handleValueChange = (key, value, param) => {
    //     this.setState({
    //         [key]: value
    //     })
    //     if(param === 'SubjectVersion' || param === 'accbalance') {
    //         this.setState({
    //             changeParam: true,
    //             rightSelectItem: []
    //         })
    //     }
    // }
    handleSelect = (value) => {//

        this.setState({
            rightSelectItem: [...value]
        })
    }
    
    renderModalList = () => {
        let { status, hideBtnArea } = this.props
		let disabled = status === 'browse' ? true : false
        this.columns = [
            {
                title: (<div fieldid='searchObj'>{this.state.json['20023060-000005']}</div>),/* 国际化处理： 核算类型*/
                dataIndex: "searchObj",
                key: "searchObj",
                render: (text, record, index) => {
                    //('columns>>>>', text, record);
                    let renderArr = [];
                    text.map((item, index) => {
                        renderArr.push(<NCOption value={JSON.stringify(item)} key={index}>{item.name}</NCOption>)
                    })
                    return (
                        <div fieldid='searchObj'>
                            <Select
                                showClear={false}
                                fieldid='searchObj'
                                disabled={disabled}
                                value={record.selectCell && record.selectCell.name}
                                onChange = {(value) => this.handleSearchObj(value, record)}
                            >
                                {
                                    renderArr
                                }
                            </Select>
                        </div>
                    )
                }
            },
            {
                title: (<div fieldid='searchRange'>{this.state.json['20023060-000006']}</div>),/* 国际化处理： 核算内容*/
                dataIndex: "searchRange",
                key: "searchRange",
                render: (text, record,index) => {
                    //('searchRangetext:::',record, record.type);
                    let renderEle = this.renderSearchRange(record, disabled, 'searchRange');
                    return (
                        <div fieldid='searchRange'>
                            {renderEle}
                        </div>
                    );
                }
            },
            {
                title: (<div fieldid='showLocal'>{this.state.json['20023060-000007']}</div>),/* 国际化处理： 显示表头*/
                dataIndex: "showLocal",
                key: "showLocal",
                width : 65,
                render: (text, record) => {
                    //('showLocal>>>>', text, record)
                    return (
                        <div fieldid='showLocal'>
                            <Select
                                showClear={false}
                                fieldid='showLocal'
                                defaultValue = 'N'
                                disabled={disabled}
                                value={record.showLocal ? 'Y' : 'N'}
                                onChange={(value) => {
                                    //('showLocal:>>>',value);
                                    record.showLocal = value == 'Y' ? true : false;
                                    this.setState({})
                                }}>
                                <NCOption value='Y'>{this.state.json['20023060-000028']}</NCOption>{/* 国际化处理： 表头*/}
                                <NCOption value='N'>{this.state.json['20023060-000029']}</NCOption>{/* 国际化处理： 表体*/}
                            </Select>
                        </div>
                    )
                }
            },
            {
                title: (<div fieldid='accele'>{this.state.json['20023060-000008']}</div>),/* 国际化处理： 计算小计*/
                dataIndex: "accele",
                key: "accele",
                width : 65,
                render: (text, record) => {
                    let tempdisabled = record.selectCell && record.selectCell.name != ''
                    ?
                    record.searchRange === "uapbd/refer/fiacc/AccountDefaultGridTreeRef/index" && record.showLocal
                    :
                    true
                    return <div fieldid='accele'>
                            <Checkbox
                            checked={record.accele}
                            disabled={disabled ? disabled : tempdisabled}
                            onChange={(value) => {
                                //('acceleChange:',record);
                                this.handleCheckbox('accele',record, value)
                            }}
                        />
                    </div>
                }
            },
            {
                title: (<div fieldid='includesub'>{this.state.json['20023060-000009']}</div>),/* 国际化处理： 包含下级*/
                dataIndex: "includesub",
                key: "includesub",
                width : 65,
                render: (text, record) => {
                    let tempd = record.selectCell &&  record.selectCell.name != ''  ?
                    (record.searchRange === 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index' ? true : false)
                    : true
                    return <div fieldid='accele'>
                        <Checkbox
                            checked={record.includesub}
                            
                            disabled={disabled ? disabled : tempd}
                            onChange={(value) => {
                                //('includesub:', record);
                                this.handleCheckbox('includesub',record, value)
                            }}
                        />
                    </div>
                }
            }
        ];
        this.secondColumn = [
            {
                title: this.state.json['20023060-000010'],/* 国际化处理： 选择*/
                dataIndex: "select",
                key: "select",
                render: (text, record, index) => {
                    //('secondColumn>', text, record)
                    return <Checkbox
                        checked={record.select}
                        onChange={() => {
                            //('acceleChange:',record);
                            record.index = index;
                            this.secondModalSelect(record, index);
                        }}
                    />
                }
            },
            {
                title: this.state.json['20023060-000011'],/* 国际化处理： 查询属性*/
                dataIndex: "name",
                key: "name"
            },
            {
                title: this.state.json['20023060-000012'],/* 国际化处理： 查询值*/
                dataIndex: "refpath",
                key: "refpath",
                render: (text, record) => {
                    //('refpath>>>>', text, record);
                    let renderSecondEle = this.renderNewRefPath(record);
                    return renderSecondEle;
                }
            }
        ]
       return (
            <div className='right_query'>
                <div className='query_body1'>
                    <div className='query_form detaild'>
			<div>
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <FormLabel isRequire={true} labelname={this.state.json['20023060-000033']} />{/* 国际化处理： 核算账簿*/}
                    </Col>
                    <Col md={7} sm={7}>
                        <div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: true,
                                        disabled: disabled,
                                        showGroup: true,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                            multiGroup : 'Y'
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit ,
                                        getCheckContent: getCheckContent,
                                        renderTableFirstData: () => this.renderTableFirstData(10)
                                    },
                                    'assistDetailAccount'
                                )
                            }
                        </div>
                    </Col>
                </Row >
                {
                    this.state.isShowUnit ?
                        <Row className="myrow">
                            <Col md={2} sm={2}>
                                <FormLabel labelname={this.state.json['20023060-000015']} />{/* 国际化处理： 业务单元*/}
                            </Col>
                            <Col md={7} sm={7}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
                                        fieldid='buSecond'
                                        value={this.state.buSecond}
                                        isMultiSelectedEnabled= {true}
                                        isShowDisabledData = {true}
                                        isHasDisabledData = {true}
                                        disabled = {disabled}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                        }}
                                        onChange={(v)=>{
                                            //(v);
                                            this.getRecover('tableSourceData', 'searchRange', 'selectCells', 'selectCell', 'type', 'accele', 'includesub', 'showLocal')
                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if(this.state.accountingbook && this.state.accountingbook[0]) {
                                                }
                                            })
                                        }
                                        }
                                    />
                                </div>
                            </Col>
                            <Col md={3}>
                                <FICheckbox className="mycheck" disabled={disabled} checked={this.state.multbusi}  onChange={this.onChangeMultbusi.bind(this)}>
                                    {this.state.json['20023060-000034']} {/* 国际化处理： 多业务单元合并*/}
                                </FICheckbox>
                            </Col>
                        </Row> :
                        <div></div>
                }
                {/*启用科目版本：*/}
                <SubjectVersion
                    checked = {this.state.isversiondate}//复选框是否选中
                    data={this.state.versionDateArr}//
                    value={this.state.versiondate}
                    CheckboxDisabled = {disabled}
                    disabled={disabled ? disabled : !this.state.isversiondate}
                    renderTableFirstData = {() => this.renderTableFirstData(10)}
                    handleValueChange = {(key, value) => this.handleValueChange(key, value, 'assistDetailAccount', getCheckContent)}//(key, value, 'assistPropertyBalance', getCheckContent)
                />
                <NCDiv fieldid="assid" areaCode={NCDiv.config.TableCom}>
                    <div className="modalTable">
                        <Table
                            columns={this.columns}
                            data={this.state.tableSourceData}
                        />
                    </div>
                </NCDiv>
                {/*会计期间*/}
                <NCDiv fieldid="assid" areaCode={NCDiv.config.FORM}>
                    {
                        hideBtnArea ? ''
                        :
                        <DateGroup
                            selectionState = {this.state.selectionState}
                            start = {this.state.start}
                            end = {this.state.end}
                            enddate = {this.state.enddate}
                            pk_accperiodscheme = {this.state.pk_accperiodscheme}
                            pk_accountingbook = {this.state.accountingbook[0]}
                            rangeDate = {this.state.rangeDate}
                            handleValueChange = {this.handleValueChange}
                            handleDateChange = {this.handleDateChange}
                            self = {this}
                            showRadio = {this.state.showRadio}
                        />
                    }
                    <CheckBoxCells
                        paramObj = {{
                            "disabled": disabled,
                            "noChargeVoucher": {//未记账凭证
                                show: true,
                                disabled: !this.state.isqueryuntallyed,
                                checked: this.state.includeuntally,
                                onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                            },
                            "errorVoucher": {//错误凭证
                                show: true,
                                checked: this.state.includeerror,
                                onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                            },
                            "harmToBenefitVoucher": {//损益结转凭证
                                show: true,
                                checked: this.state.includeplcf,
                                onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                            },
                            "againClassifyVoucher": {//重分类凭证
                                show: true,
                                checked: this.state.includerc,
                                onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                            },
                        }}
                    />

                    {/*币种*/}
                    {renderMoneyType(this.state.currtypeList,this.state.currtypeName, this.handleCurrtypeChange, this.state.json, disabled)}

                    {/*返回币种：*/}
                    {
                        returnMoneyType(
                            this,
                            {
                                key: 'selectedCurrValue',
                                value: this.state.selectedCurrValue,
                                groupEdit: this.state.groupCurrency,
                                gloableEdit: this.state.globalCurrency
                            },
                            this.state.json,
                            disabled
                        )
                    }

                    <Row className="myrow  sort-area">
                        <Col md={2} sm={2}>
                            <FormLabel labelname={this.state.json['20023060-000035']} /> {/* 国际化处理： 排序方式*/}
                        </Col>
                        <Col md={6} >
                            <Radio.NCRadioGroup
                                selectedValue={this.state.sortType}
                                onChange={(value) => this.onChangeShowzerooccur(value, 'sortType')}
                            >
                                <Radio value="0" disabled={disabled}>{this.state.json['20023060-000036']}</Radio>{/* 国际化处理： 制单日期*/}
                                <Radio value="1" disabled={disabled}>{this.state.json['20023060-000037']}</Radio>{/* 国际化处理： 凭证号*/}
                            </Radio.NCRadioGroup>
                        </Col>
                        <Col md={4} className='sort-checkbox'>
                            <Checkbox
                                colors="dark"
                                checked={this.state.addupByDay}
                                onChange={(value) => this.onChangeShowzerooccur(value, 'addupByDay')}
                            >
                                {this.state.json['20023060-000038']} {/* 国际化处理： 按日合计*/}
                            </Checkbox>
                            {/* <label className='dayaccount'> {this.state.json['20023060-000038']}</label> */}
                        </Col>
                    </Row>

                    <FourCheckBox
                        totalTitle= {this.state.json['20023060-000019']} ///* 国际化处理： 显示属性：*/
                        renderCells={
                            [
                                {
                                    title: this.state.json['20023060-000020'], ///* 国际化处理： 显示本币合计*/
                                    checked: this.state.disLocalCurySum,
                                    disabled: disabled ? disabled : this.state.LocalCurySumEdit,
                                    id: 'disLocalCurySum',
                                    onChange: (value) => this.onChangeShowzerooccur(value, 'disLocalCurySum')
                                },
                                {
                                    title: this.state.json['20023060-000021'], ///* 国际化处理： 无发生不显示*/
                                    checked: this.state.ckNoAccurNoDsp,
                                    id: 'ckNoAccurNoDsp',
                                    disabled: disabled,
                                    onChange: (value) => this.onChangeShowzerooccur(value, 'ckNoAccurNoDsp')
                                },
                                {
                                    title: this.state.json['20023060-000022'], ///* 国际化处理： 期间无发生显示累计*/
                                    checked: this.state.dspAccumu,
                                    id: 'dspAccumu',
                                    disabled: disabled,
                                    onChange: (value) => this.onChangeShowzerooccur(value, 'dspAccumu')
                                },
                                {
                                    title: this.state.json['20023060-000023'], ///* 国际化处理： 合并同凭证分录*/
                                    checked: this.state.combineVoucher,
                                    id: 'combineVoucher',
                                    disabled: disabled,
                                    onChange: (value) => this.onChangeShowzerooccur(value, 'combineVoucher')
                                }
                            ]
                        }
                    />

                    <Row className="myrow">
                        <Col md={2} sm={2}>
                            <FormLabel labelname={this.state.json['20023060-000039']} /> {/* 国际化处理： 余额方向*/}
                        </Col>
                        <Col md={4} sm={4}>

                            <div className="book-ref">
                                <Select
                                    showClear={false}
                                    fieldid='balanceori'
                                    value={this.state.balanceori}
                                    disabled={disabled}
                                    onChange={
                                        (value) => this.handleSelectChange(value, 'balanceori')
                                    }
                                >
                                    <NCOption value={'-1'} key={'-1'} >{this.state.json['20023060-000040']}</NCOption>{/* 国际化处理： 双向*/}
                                    <NCOption value={'0'} key={'0'} >{this.state.json['20023060-000041']}</NCOption>{/* 国际化处理： 借*/}
                                    <NCOption value={'1'} key={'1'} >{this.state.json['20023060-000042']}</NCOption>{/* 国际化处理： 贷*/}
                                    <NCOption value={'3'} key={'3'} >{this.state.json['20023060-000043']}</NCOption>{/* 国际化处理： 平*/}
                                </Select>
                            </div>
                        </Col>
                        <Col md={4} sm={4}>
                            <Checkbox
                                // colors="dark"
                                // className="mycheck"
                                disabled={disabled}
                                checked={this.state.ckNotShowZeroOccurZeroBalance}
                                onChange={(value) => this.onChangeShowzerooccur(value, 'ckNotShowZeroOccurZeroBalance')}
                            >
                                {this.state.json['20023060-000044']} {/* 国际化处理： 显示业务日期*/}
                            </Checkbox>
                            {/* <label className='showbusdate'>{this.state.json['20023060-000044']}</label> */}
                        </Col>
                    </Row>
                </NCDiv>

                <NewModal
                    title= {this.state.json['20023060-000024']} ///* 国际化处理： 属性选择*/
                    record={this.state.selectRow[this.state.page]}
                    column={this.secondColumn}
                    showNewModal = {this.state.showNewModal}//控制设否显示
                    closeNewModal = {this.closeNewModal}
                    sureHandle = {this.sureHandle}
                    sureText = {this.state.json['20023060-000025']} ///* 国际化处理： 确定 */
                    cancleText = {this.state.json['20023060-000014']} ///* 国际化处理： 取消*/
                />
			</div>
                    </div>
                </div>
            </div>
		)
	}
    clickPlanEve = (value) => {
        let { status } = this.props;
        if (status !== 'browse') {
            this.state = deepClone(value.conditionobj4web.nonpublic);
            this.setState(this.state)
        }
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
        //('saveSearchPlan>>', this.state)
        return {...this.state}
    }
    clickSearchBtn = () => {//点击查询框的"查询"按钮事件
        //('clickSearchBtn>>');
        let { hideBtnArea, status } = this.props
    	// let pk_accountingbook = [];
		// let pk_unit = [];
		// let moneyLeft = Number(this.state.balanceMoneyLeft);
		// let moneyRight = Number(this.state.balanceMoneyRight);
		// //('moneyyyy::', moneyLeft,  moneyRight, moneyLeft < moneyRight)
		// if(moneyLeft > moneyRight){
		// 	toast({
		// 		content: this.state.json['20023060-000026'],/* 国际化处理： 余额范围查询条件信息输入有误(起始值大于终止值或是为负值)！*/
		// 		color: 'warning'
		// 	});
		// 	return;
		// }
		// let lastAssvos = [];
		// this.state.tableSourceData.forEach((item)=>{
		// 	//('assvos>>>',item);
		// 	let resultObj = {};
		// 	let valueArr = [];
		// 	if(item.selectCell && !item.selectCellAttr){
		// 		resultObj.name = item.selectCell.name;
		// 		resultObj.pk_checktype = item.selectCell.pk_checktype;
		// 		item.selectRange && Array.isArray(item.selectRange) && item.selectRange.map((item) => {
		// 			valueArr.push(item.pk_checkvalue)
		// 		})
		// 		resultObj.pk_checkvalue = Array.isArray(item.selectRange) ? valueArr.join(',') : item.selectRange;
		// 		resultObj.headEle = item.showLocal ? 'Y' : 'N';
		// 		resultObj.accEle = item.accele ? 'Y' : 'N';
		// 		resultObj.includeSub = item.includesub ? 'Y' : 'N';
		// 		lastAssvos.push(resultObj)
		// 	} else if(item.selectCell && item.selectCellAttr){
		// 		resultObj.name = item.selectCell.name;
		// 		resultObj.pk_checktype = item.selectCell.pk_checktype;
		// 		resultObj.attr = [];
		// 		resultObj.headEle = item.showLocal ? 'Y' : 'N';
		// 		resultObj.accEle = item.accele? 'Y' : 'N';
		// 		resultObj.includeSub = item.includesub ? 'Y' : 'N';
		// 		item.selectCellAttr.map((itemCell) => {
		// 			if(itemCell.select){
		// 				resultObj.attr.push(itemCell);
		// 			}
		// 		})
		// 		lastAssvos.push(resultObj)
		// 	}
		// })
		// //('lastAssvos>>', lastAssvos)
		// if (Array.isArray(this.state.accountingbook)) {
		// 	this.state.accountingbook.forEach(function (item) {
		// 		pk_accountingbook.push(item.refpk)
		// 	})
		// }

		// if (Array.isArray(this.state.buSecond)) {
		// 	this.state.buSecond.forEach(function (item) {
		// 		pk_unit.push(item.refpk)
		// 	})
		// }
		// //('>>>>>/////???', pk_unit);
        // //
        // let data = {
        //     /*******核算账簿****/
        //     pk_glorgbooks: pk_accountingbook,//[""]
        //     /*******业务单元****/
        //     pk_orgs: pk_unit,
        //     multiBusi: this.state.multbusi,//复选框是否勾选

        //     /*******科目版本****/
        //     useSubjVersion: this.state.isversiondate,//是否勾选
        //     subjVersion: this.state.isversiondate ? this.state.versiondate : this.state.busiDate, //this.state.versiondate,//科目版本

        //     /*******查询对象 table框的内容****/
        //     qryObjs: lastAssvos,

        //     /*******会计期间参数****/
        //     selectionState: this.state.selectionState,
        //     beginyear: this.state.startyear,
        //     beginperiod: this.state.startperiod,
        //     endyear: this.state.endyear,
        //     endperiod:   this.state.endperiod,
        //     /*******日期参数****/
        //     begindate: this.state.begindate,
        //     enddate: this.state.enddate,

        //     /*******包含凭证****/
        //     includeUnTallyed: this.state.includeuntally ? 'Y' : 'N',//未记账凭证
        //     includeError: this.state.includeerror ? 'Y' : 'N',// 错误凭证
        //     includeTransfer: this.state.includeplcf ? 'Y' : 'N',//损益结转凭证
        //     includeReClassify: this.state.includerc ? 'Y' : 'N',//重分类凭证

        //     /*******币种****/
        //     pk_currtype: this.state.currtype,//
        //     currTypeName: this.state.currtypeName,//

        //     /*******返回币种****/
        //     return_currtype: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局

        //     /***排序方式***/
        //     "sortType": this.state.sortType, //排序方式。制单日期0，凭证号1
        //     "addupByDay": this.state.addupByDay, //按日合计true

        //     /***显示本币合计；无发生不显示；期间无发生显示累计数；合并同凭证分录***/
        //     "disLocalCurySum": this.state.disLocalCurySum, //显示本币合计true
        //     "ckNoAccurNoDsp": this.state.ckNoAccurNoDsp, //无发生不显示true
        //     "dspAccumu": this.state.dspAccumu, //期间无发生显示累计数true
        //     "combineVoucher": this.state.combineVoucher, //合并同凭证分录true

        //     /*******余额方向****/
        //     balanceDirection: this.state.balanceori, //"双向/借/贷/平", //余额方向
        //     showBusDate: this.state.ckNotShowZeroOccurZeroBalance,//显示业务日期true

        //     pageindex: '1',
        //     pk_accasoa: this.state.rightSelectItem,//'条件'

        //     /*******多核算账簿显示方式;多核算账簿排序方式****/
        //     "mutibook": this.state.defaultShowStyle, //多核算账簿显示方式：多核算账簿合并(true )
        //     appcode: this.state.appcode,
        //     index: this.index
        // }
        if(status !== 'browse'){
            let data = this.getConfirmData(this.state)
            let url = '/nccloud/gl/accountrep/checkparam.do'
            let flagShowOrHide;
            ajax({
                url,
                data,
                async: false,
                success: function (response) {
                    flagShowOrHide = true
                },
                error: function (error) {
                    flagShowOrHide = false
                    toast({ content: error.message, color: 'warning' });
                }
            })
            if (flagShowOrHide == true) {
                if(hideBtnArea){
                    this.props.onConfirm(data, this.state)
                } else {
                    this.props.onConfirm(data)
                }
            } else if (flagShowOrHide == false) {
                return true;
            } 
        } else {
            return
        }
             
	}
	render() {
//('timerddd::',this.state, this.state.tableSourceData,this.state.level,  this.state.begindate,'..', this.state.enddate,'>>>>', this.state.rangeDate)
        let { search, hideBtnArea, status } = this.props;
        let { NCCreateSearch } = search;
        let isHideBtnArea = typeof(hideBtnArea) === 'boolean' && hideBtnArea ? hideBtnArea : false
        return <div>
            {
                NCCreateSearch(this.searchId, {
                    onlyShowSuperBtn:true,                       // 只显示高级按钮
                    replaceSuperBtn:this.state.json['20023060-000013'],                      // 自定义替换高级按钮名称/* 国际化处理： 查询*/
                    replaceRightBody:this.renderModalList,
                    hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
                    clickPlanEve:this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                    saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
                    oid:this.props.meta.oid,
                    hideBtnArea: isHideBtnArea,
                    clickSearchBtn: this.clickSearchBtn,  // 点击按钮事件
                    isSynInitAdvSearch: true,//渲染右边面板自定义区域
                    // showAdvSearchPlanBtn: status==='browse' ? false : true, // 高级面板中是否显示保存方案按钮
                    searchBtnName: isHideBtnArea ? this.state.json['20023060-000025'] : this.state.json['20023060-000013'], /* 国际化处理： 确定*//* 国际化处理： 查询*/
                })}
        </div>
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal;
