/**
 * Created by liqiankun on 2018/7/04.
 * 账簿查询 ---> 序时账
 */

import React, {Component} from 'react';

import {high, base, ajax, createPage, getMultiLang,createPageIcon} from 'nc-lightapp-front';
const { NCAffix, NCDiv } = base;
import '../../css/index.less'
import HeadCom from '../../common/headSearch/headCom';
import { SimpleTable } from 'nc-report';
import QueryModal from './components/MainSelectModal';
import {tableDefaultData} from '../../defaultTableData';
import reportPrint from '../../../public/components/reportPrint';
import { toast } from '../../../public/components/utils';
import {relevanceSearch} from "../../referUrl";
import {getDetailPort} from "../../common/modules/modules";
const { PrintOutput } = high;
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";

import {setData} from "../../common/simbleTableData";
import {searchById} from "../../common/modules/createBtn";
import {handleSimpleTableClick} from "../../common/modules/simpleTableClick";
import PageButtonGroup from '../../common/modules/pageButtonGroup';
import {rowBackgroundColor} from "../../common/htRowBackground";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';

class JournalContent extends Component{
    constructor(props){
        super(props);

        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            disabled: true,
			visible: false,

            appcode: '20023040',
            outputData: {},
            ctemplate: '', //模板输出-模板
            nodekey: '',
            printParams:{}, //查询框参数，打印使用
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
            textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            dataWithPage: [], //存放请求回来的数据信息，用于分页操作
            firstPageDisable: true,
            lastPageDisable: true,
            paramObj: {},//存放回调的查询参数，用于翻页
            accountType: 'amountcolumn',//金额式
            showPageBtn: false,
            'journal': []
        }
        this.dataPage = 1; //存放请求回来的数据椰树信息
        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.handlePage = this.handlePage.bind(this);//页面处理事件
        // this.setData= this.setData.bind(this);// 整理表格所需要的数据格式
        this.queryData = this.queryData.bind(this);//请求数据的方法；
        // this.changeSelectStyle = this.changeSelectStyle.bind(this);//选择 '账簿格式'事件
        this.propsData= this.propsData.bind(this);//从props传递的展示数据进行加工处理
        this.handleValueChange = handleValueChange.bind(this);
        this.searchById = searchById.bind(this);
        this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
        // this.setData = setData.bind(this);
    }

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                'journal': [
                    {
                        title: json['20023040-000083'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20023040-000084'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023040-000085'],
                        styleClass:"m-brief"
                    },

                    {
                        title: json['20023040-000086'],
                        styleClass:"m-brief"
                    }
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023040', 'publiccommon', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }

    componentDidMount(){
        this.props.dataout && this.propsData(this.props.dataout);

        this.setState({
            flag: true,
        });

        let pk_ntbparadimvo=this.props.getUrlParam('pk_ntbparadimvo')
        if(pk_ntbparadimvo){
            this.searchById('20023040link','20023040');
            this.props.button.setDisabled({
                print: false
            });
        }else{
            let appceod = this.props.getSearchParam('c');
            this.searchById('20023040PAGE',appceod);
            this.props.button.setDisabled({
                linkvoucher: true,
                print: true, directprint: true, saveformat: true,
                first: true, pre: true, next: true, last: true,
            });

        }
        

        this.props.button.setButtonVisible(['first', 'pre', 'next', 'last'], false)
        this.getParam()
    }
    showPrintModal() {
		this.setState({
			visible: true
		})
	}
	handlePrint(data, isPreview) {
        let printUrl = '/nccloud/gl/accountrep/sequencebookprint.do'
        let { printParams, appcode } = this.state
        let { ctemplate, nodekey } = data
        printParams.queryvo = data
        let pk_ntbparadimvo=this.props.getUrlParam('pk_ntbparadimvo')
        if(pk_ntbparadimvo){
            printParams.queryvo.pk_ntbparadimvo = pk_ntbparadimvo;
        }
        printParams.pageindex = String(this.dataPage);
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
		printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
		this.handleCancel()
	}
	handleCancel() {
		this.setState({
			visible: false
		});
    }

    handleOutput() {

    }

    getParam = () => {
        let data = this.props.getUrlParam && this.props.getUrlParam('status');
        if(data && data != 'browse' && data != 'edit' && data != 'add'){ // 凭证联查序时账的status=browse/edit
            let paramData = JSON.parse(data)
            this.setState({
                printParams:paramData,
            })
            this.modalSure(paramData)
        }
    }
    propsData(propsDataParam){//从props传递的展示数据进行加工处理
        this.setState({
            dataWithPage: propsDataParam,
            lastPageDisable: true,
            firstPageDisable: true
        })
        this.props.button.setDisabled({
            linkvoucher: true
        });
        let renderFirstData = {};
        if( Object.keys(propsDataParam).length === 0 ){
            propsDataParam.column = [];
            propsDataParam.data = [];
        }
        renderFirstData.column = propsDataParam.column;
        renderFirstData.data = propsDataParam.data;
        renderFirstData.amountcolumn = propsDataParam.amountcolumn;
        renderFirstData.quantityamountcolumn = propsDataParam.quantityamountcolumn;
        setData(this, renderFirstData);

    }
    handlePage(param, dataWithPage){//页面控制事件
        let {paramObj} = this.state;
        let dataLength = Number(dataWithPage.totalpage);
        let renderData = [];
        let obj = {};
        let stringPage = '1';
        switch(param){
            case 'firstPage':
                this.dataPage = 1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;

                this.setState({
                    paramObj,
                    firstPageDisable: true,
                    lastPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    first: true, pre: true,
                    next: false, last: false,
                });
                this.queryData(this.state.paramObj);
                return;
            case 'nextPage':
                this.dataPage += 1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;

                this.setState({
                    paramObj,
                    firstPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    first: false, pre: false
                });
                if(this.dataPage === dataLength){
                    this.setState({
                        lastPageDisable: true
                    })
                    this.props.button.setDisabled({
                        next: true, last: true
                    });
                }
                this.queryData(this.state.paramObj);
                return;
            case 'prePage':
                this.dataPage -=1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                if(this.dataPage === dataLength-1){
                    this.setState({
                        lastPageDisable: false
                    })
                    this.props.button.setDisabled({
                        next: false, last: false
                    });
                }
                if(this.dataPage === 1){
                    this.setState({
                        firstPageDisable: true
                    })
                    this.props.button.setDisabled({
                        first: true, pre: true
                    });
                }
                this.setState({
                    paramObj,
                    selectRowIndex: 0
                })

                this.queryData(this.state.paramObj);
                return;
            case 'lastPage':
                this.dataPage = dataLength;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                this.setState({
                    paramObj,
                    lastPageDisable: true,
                    firstPageDisable: false
                });
                this.props.button.setDisabled({
                    first: false, pre: false,
                    next: true, last: true,
                });
                this.queryData(this.state.paramObj);
                return;
        }
    }
    queryClick = () => {//点击"查询"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    queryData(param){//请求数据方法；
        let self = this;
        //gl.accountrep.sequencebooksquery
        let url = '/nccloud/gl/accountrep/sequencebooksquery.do';
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, success } = response;
                if(success && data){
                    if(data.data.length>0){
                        self.setState({
                            disabled:false
                        })
                    }
                    if(param.pageindex === '1' && data.totalpage !=='1'){
                        self.setState({
                            lastPageDisable: false
                        })
                        self.props.button.setDisabled({
                            next: false, last: false
                        });
                    }
                    self.setState({
                        dataWithPage: data,
                        typeDisabled: false,
                        selectRowIndex:0
                    })
                    let renderFirstData = {};
                    renderFirstData.column = data.column;
                    renderFirstData.data = data.data;
                    renderFirstData.amountcolumn = data.amountcolumn;
                    renderFirstData.quantityamountcolumn = data.quantityamountcolumn;
                    setData(self, renderFirstData);
                }else{
                    toast({content: this.state.json['20023040-000080'], color: 'danger'});/* 国际化处理： 请求数据为空！*/
                }
            },
            error:function(error){
                toast({content: error.message, color: 'danger'});
                throw error
            }
        })
    }
    modalSure(param){//点击弹框"确定"事件
        this.dataPage = 1;
        this.setState({
            paramObj: {...param},
            printParams: {...param},
            firstPageDisable: true,
            lastPageDisable: true
        })
        this.props.button.setDisabled({
            first: true, pre: true,
            next: true, last: true,
        });
        this.props.button.setDisabled({
            refresh: false,print: false, directprint: false, saveformat: false
        });
        this.queryData(param);
    }
    setDatafor = (data) => {//整理表格数据
        let rows = [];   //表格显示的数据合集
        let columHead = []; //第一层表头
        let parent = []; //存放一级表头，二级表头需要补上
        let parent1 = []; //存放二级表头，三级表头需要补上
        let child1 = []; //第三层表头
        let child2 = []; //第二层表头
        let colKeys = [];  //列key值，用来匹配表体数据
        let colAligns=[];  //每列对齐方式
        let colWidths=[];  //每列宽度
        let oldWidths=[];
        let textAlginArr = this.state.textAlginArr.concat([]);      //对其方式
        data[this.state.accountType].forEach((value) => {//column:[]原始数据表头信息
            let valueCell = {};
            valueCell.title = value.title;
            valueCell.key = value.key;
            valueCell.align = 'center';
            valueCell.bodyAlign = 'left';
            valueCell.style = 'head';
            if(value.children){
                this.setState({
                    flow: true
                })
                if(parent.length>0){
                    child1.push(...parent);
                }
                for(let i=0;i<value.children.length;i++){
                    if(value.children[i].children){
                        child2.push(...parent1);
                        for(let k=0;k<value.children[i].children.length;k++){
                            let child1Cell = {};
                            child1Cell.title = value.children[i].title;
                            child1Cell.key = value.children[i].key;
                            child1Cell.align = 'center';
                            child1Cell.bodyAlign = 'left';
                            child1Cell.style = 'head';
                            let child2Cell = {};
                            child2Cell.title = value.children[i].children[k].title;
                            child2Cell.key = value.children[i].children[k].key;
                            child2Cell.align = 'center';
                            child2Cell.bodyAlign = 'left';
                            child2Cell.style = 'head';
                            columHead.push(valueCell);
                            child1.push(child1Cell);
                            child2.push(child2Cell);
                            colKeys.push(value.children[i].children[k].key);
                            colAligns.push(value.children[i].children[k].align);
                            colWidths.push({'colname':value.children[i].children[k].key,'colwidth':value.children[i].children[k].width});
                        }
                        parent1 = [];
                    }else {
                        let innerCell2 = {};
                        innerCell2.title = value.children[i].title;
                        innerCell2.key = value.children[i].key;
                        innerCell2.align = 'center';
                        innerCell2.bodyAlign = 'left';
                        innerCell2.style = 'head';
                        columHead.push(valueCell);
                        child1.push(innerCell2);
                        parent1.push(innerCell2);
                        colKeys.push(value.children[i].key);
                        colAligns.push(value.children[i].align);
                        colWidths.push({'colname':value.children[i].key,'colwidth':value.children[i].width});
                    }
                }
                parent = [];
            }else{
                let cellObj = {};
                cellObj.title = value.title;
                cellObj.key = value.key;
                cellObj.align = 'center';
                cellObj.bodyAlign = 'left';
                cellObj.style = 'head';
                columHead.push(valueCell);
                parent1.push(cellObj);
                parent.push(cellObj);
                colKeys.push(value.key);
                colAligns.push(value.align);
                colWidths.push({'colname':value.key,'colwidth':value.width});
            }
        });
        let columheadrow = 0; //表头开始行
        // if (data.headtitle){
        //    columheadrow = 1;
        //    let headtitle = [];
        //    data.headtitle.forEach(function (value) {
        //     headtitle.push(value[0]);
        //     headtitle.push(value[1]);
        //    });
        //    for(let i=headtitle.length;i<columHead.length;i++){
        //     headtitle.push('');
        //    }
        //    rows.push(headtitle);
        // }

        let mergeCells = [];
        let row,col,rowspan,colspan;
        let headcount = 1; //表头层数
        if(child1.length>0){
            headcount++;
        }
        if(child2.length>0){
            headcount++;
        }
        let currentCol = 0;
        //计算表头合并格
        for(let i=0;i<data[this.state.accountType].length;i++) {
            let value = data[this.state.accountType][i];
            //*********
            if(value.children){//有子元素的
                let childLeng = value.children.length;
                mergeCells.push([columheadrow, currentCol, columheadrow, currentCol + childLeng -1 ]);

                let headCol = currentCol;
                for(let i=0;i<value.children.length;i++){

                    let childlen = 0;
                    if(value.children[i].children){

                        let childlen = value.children[i].children.length;
                        currentCol = currentCol+childlen;
                        for(let k=0;k<value.children[i].children.length;k++){
                            textAlginArr.push(value.children[i].children[k].align);
                        }
                    }else if(childlen > 0){//子元素里面没有子元素的

                        mergeCells.push([columheadrow+1, currentCol, columheadrow+2, currentCol]);//cell
                        currentCol++;
                    }else {
                        currentCol++;
                        textAlginArr.push(value.children[i].align);
                    }
                }
            }else{//没有子元素对象
                mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
                currentCol++;
                textAlginArr.push(value.align);
            }
        }
        if(parent.length>0){
            child1.push(...parent);
        }
        if(child2.length>0&&parent1.length>0){
            child2.push(...parent1);
        }

        rows.push(columHead);

        if(child1.length>0 && this.state.flow){
            rows.push(child1);
        }
        if(child2.length>0 && this.state.flow){
            rows.push(child2);
        }

        if(data!=null && data.data.length>0){

            let rowdata = [];
            let flag = 0;
            for(let i=0;i<data.data.length;i++){
                flag++;
                for(let j=0;j<colKeys.length;j++){
                    // {
                    //     "title":"11163",
                    //     "key":"pk_org",
                    //     "align":"left",
                    //     "style":"body"
                    // }
                    let itemObj = {
                        align: 'right',
                        style: 'body'
                    };
                    itemObj.title = data.data[i][colKeys[j]];
                    itemObj.key = colKeys[j] + flag;
                    itemObj.link = data.data[i].link
                    // rowdata.push(data.data[0].pagedata[i][colKeys[j]])
                    rowdata.push(itemObj)
                }
                rows.push(rowdata);
                rowdata = [];
            }
        }

        let rowhighs = [];
        for (let i=0;i<rows.length;i++){
            rowhighs.push(23);
        }
        this.state.dataout.data.cells= rows;//存放表体数据

        this.state.dataout.data.widths=colWidths;
        oldWidths.push(...colWidths);
        this.state.dataout.data.oldWidths = oldWidths;
        this.state.dataout.data.rowHeights=rowhighs;
        let frozenCol;
        for(let i=0;i<colAligns.length;i++){
            if(colAligns[i]=='center'){
                frozenCol = i;
                break;
            }
        }
        this.state.dataout.data.fixedColumnsLeft = 0; //冻结
        this.state.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结
        let quatyIndex = [];
        for (let i=0;i<colKeys.length;i++){
            if(colKeys[i].indexOf('quantity')>0){
                quatyIndex.push(i);
            }
        }


        if(mergeCells.length>0){
            this.state.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
        }
        let headAligns=[];

        for(let i=0;i<headcount+columheadrow;i++){
            for(let j=0;j<columHead.length;j++){
                headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
            }
        }
        this.state.dataout.data.cell= headAligns,
            this.state.dataout.data.cellsFn=function(row, col, prop) {
                let cellProperties = {};
                if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
                    cellProperties.align = colAligns[col];
                    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
                }
                return cellProperties;
            }
        this.setState({
            dataout:this.state.dataout,
            showModal: false,
            textAlginArr
        });
    }

    //直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    handleSaveColwidth=()=>{
        let {json} = this.state;
        let info=this.refs.balanceTable.getReportInfo();
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.appcode,info.colWidths, json, callBack);
    }
    handleLoginBtn = (obj, btnName) => {
        if(btnName === 'search'){//1、查询
            this.queryClick()
        }else if(btnName === 'linkvoucher'){//6、联查凭证
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: relevanceSearch,
                key: 'relevanceSearch',
                appcode: '20023030'
            }, this.state.json)
        }else if(btnName === 'print'){//2、打印
            this.showPrintModal()
        }else if(btnName === 'directprint'){//3、直接输出
            this.printExcel()
        }else if(btnName === 'saveformat'){//5、保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'refresh'){
            if(Object.keys(this.state.paramObj).length > 0){
                this.state.paramObj.pageindex = '1';
                this.modalSure(this.state.paramObj)
            }

        }else if(btnName === 'first'){//首页
            this.handlePage('firstPage', this.state.dataWithPage)
        }else if(btnName === 'pre'){//上一页
            this.handlePage('prePage', this.state.dataWithPage)
        }else if(btnName === 'next'){//下一页
            this.handlePage('nextPage', this.state.dataWithPage)
        }else if(btnName === 'last'){//末页
            this.handlePage('lastPage', this.state.dataWithPage)
        }
    }
    firstCallback = () => {//首页
        this.handlePage('firstPage', this.state.dataWithPage)
    }
    preCallBack = () => {//上一页
        this.handlePage('prePage', this.state.dataWithPage)
    }
    nextCallBack = () => {//下一页
        this.handlePage('nextPage', this.state.dataWithPage)
    }
    lastCallBack = () => {//末页
        this.handlePage('lastPage', this.state.dataWithPage)
    }
    render(){
        let { callBack, flag, modal } = this.props;
        const { createModal } = modal;
        let isshowInModal = this.props.dataout && flag
        let modalStyle = isshowInModal ? { height: '460px', overflow: 'hidden'} : {}
        let accoutTypeDisabled = this.props.dataout && flag ? false : this.state.typeDisabled //账簿格式是否禁用：凭证联查序时账不禁用

        return(
            <div className='manageReportContainer'>
                {this.props.dataout && flag ? '' :
                    <HeaderArea 
                        title = {this.state.json['20023040-000081']} /* 国际化处理： 序时账*/
                        btnContent = {
                            <div>
                                <div className='account-query-btn'>
                                    <QueryModal
                                        onConfirm={(datas) => {
                                            this.modalSure(datas);
                                        }}
                                    />
                                </div>
                                {//按钮集合
                                    this.props.button.createButtonApp({
                                        area: 'btnarea',
                                        onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                                    })
                                }
                            </div>
                        }
                        pageBtnContent = {
                            <PageButtonGroup
                                display={this.props.dataout && flag? 'none' : ''}
                                first={{disabled: this.state.firstPageDisable, callBack: this.firstCallback}}
                                pre={{disabled: this.state.firstPageDisable, callBack: this.preCallBack}}
                                next={{disabled: this.state.lastPageDisable, callBack: this.nextCallBack}}
                                last={{disabled: this.state.lastPageDisable, callBack: this.lastCallBack}}
                            />
                        }
                    />
                }
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.journal.map((items) => {
                                return(
                                    <HeadCom
                                        labels={items.title}
                                        key={items.title}
                                        content={this.state.dataWithPage.headtitle}
                                        lastThre = {this.state.dataWithPage && this.state.dataWithPage.headtitle}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
                                        }
                                        accountType = {this.state.accountType}
                                        isLong = {items.styleClass}
                                        typeDisabled = {accoutTypeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className={this.props.dataout && flag ? '' : 'report-table-area'} style={modalStyle}>
                    <SimpleTable
                        ref="balanceTable"
                        data = {this.state.dataout}
                        onCellMouseDown = {(e, coords, td) => {
                            this.whetherDetail('link', 'multiAccountSearch');
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
                </div>

                <div className="modalContainer">
                    <QueryModal
                        className="journalData"
                        show={this.state.showModal}
                        ref='MainSelectModal'
                        title={this.state.json['20023040-000087']}/*查询条件*/
                        icon=""
                        onConfirm={(datas) => {
                            this.modalSure(datas);
                        }}
                        onCancel={() => {
                            this.queryClick()
                        }}
                    />
                </div>
                <RepPrintModal
                    noRadio={true}
					// scopeGray={true}
					noCheckBox={true}
					appcode={this.state.appcode}
					visible={this.state.visible}
					handlePrint={this.handlePrint.bind(this)}
					handleCancel={this.handleCancel.bind(this)}
				/>
                <PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/accountrep/sequencebookoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
                {createModal('printService', {
                    className: 'print-service'
                })}
                <Iframe />
            </div>
        )
    }
}

JournalContent = createPage({})(JournalContent)

export default JournalContent;
