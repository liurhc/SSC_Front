/**
 * Created by liqiankun on 2018/7/04.
 * 账簿查询 ---> 序时账
 */

import React, {Component} from 'react';

import {high, base, ajax} from 'nc-lightapp-front';
import "../../../public/reportcss/firstpage.less";
const {
    NCFormControl: FormControl,
    NCDatePicker:DatePicker,
    NCButton: Button,
    NCRadio:Radio,
    NCBreadcrumb:Breadcrumb,
    NCRow:Row, NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,
    NCNumber,
    AutoComplete,
    NCDropdown:Dropdown,
    NCPanel:Panel,
    NCForm,
    NCButtonGroup:ButtonGroup,
    NCInput: Input,
    NCModal:Modal,
    NCInputGroup: InputGroup
} = base;

import JournalContent from './content.js';
import {tableDefaultData} from '../../defaultTableData'

class Journal extends Component{
    constructor(props){
        super(props);

        this.state = {
            dataout: tableDefaultData,
        }
    }

    render(){
        return(
            <JournalContent />
        )
    }
}




ReactDOM.render(<Journal />,document.querySelector('#app'));
