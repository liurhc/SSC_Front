import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
//import moment from 'moment';

import {high,base,ajax, deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import '../../../../css/searchModal/index.less';



const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input, NCTooltip: Tooltip,
	NCRangePicker: RangePicker
} = base;

import { toast } from '../../../../../public/components/utils';

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';
const Timeformat = "YYYY-MM-DD HH:mm:ss";

// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import AccPeriodDefaultTreeGridRef from '../../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';
import NewModal from '../modal'
import AccountDefaultModelTreeRef from '../../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../../public/components/uapRefer.js';

import ReferEle from '../../../../../../gl/refer/voucher/OperatorDefaultRef/index.js';
import VoucherRefer from '../../../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef/index';//凭证参照
import CtlSysRef from '../../../../../refer/voucher/CtlSysGridRef/index';
import {
    renderMoneyType,
    renderRefer,
    renderCurrtyRefer,
    getSubjectVersion,
    createReferFn,
    getCheckContent, businessUnit, returnMoneyType, getReferDetault
} from "../../../../common/modules/modules";
import {
    subjectRefer, currType, abstructReferUrl
} from "../../../../referUrl";
import {handleNumberInput} from '../../../../common/modules/numberInputFn'


//来源系统参照
let referUrlObj = {
    oppositeSubject: {url: subjectRefer, key: 'oppositeSubject',stateKey: 'oppositeSubjectState'}, //对方科目
    abstractObj: {url: abstructReferUrl, key: 'abstract',stateKey: 'abstractState'},//摘要
    subjectCoding: {url: subjectRefer, key: 'subjectCoding',stateKey: 'subjectCodingState'}, //科目编码
};
import DateGroup from '../../../../common/modules/dateGroup'
import FourCheckBox from '../../../../common/modules/fourCheckBox'

class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
		    json: {},
            dateInputPlaceholder: '', /* 国际化处理： 选择日期*/
            assistDateValue: '', //辅助参照中的日期的state值
            assistDateTimeValue: '', //辅助参照中的日期+时分秒的state值
            stringRefValue: '', //辅助参照中的字符的state值
            numberRefValue: '',//辅助参照中的数值的state值
            numberRefValueInteger: '',//辅助参照中整数值
            booleanValue: '', //辅助参照中布尔值
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            showRadio: true,//控制是否显示"会计期间"的但选框；false: 不显示
            appcode: '20023040',
            isShowUnit:false,//是否显示业务单元
            busiDate: '', //业务日期



            pk_accperiodscheme: '',//会计期间参数
			showNewModal: false,
            renderData: [], //我的弹框里数据
			innerSelectName: [], //"查询范围"里存放的数据
            innerSelectData: [], //最里层选择数据的项
            rangefrom: '', //余额范围起始值
            rangeto: '', //余额范围结束值


            listItem:{},//业务单元中模板数据对应值

			selectedSort: 'true',//排序方式
			selectedShowType: 'false', //多主题显示方式
			allLast: true, // 所有末级是否选中
			outProject: false, //表外科目是否选中
			multiProjectMerge: true, //多主题合并
			subjectShow: true, //按主题列示
			balanceMoneyLeft: '', //余额范围左边输入框
            balanceMoneyRight: '', //余额范围右边输入框
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accountingUnit: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: [],//业务单元
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表


			currtypeName: '',  //选择的币种名称
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向

			/***现金日计记账/多主题科目余额表：级次***/
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次

            /***现金日计记账/多主题科目余额表：科目***/
			startcode: {
				refname:'',
				refpk: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
			},    //结束科目编码

			versiondate: '', //存放选择的版本号
			versionDateArr: [], //存放请求账簿对应的版本数组
			level:0, //返回的最大级次
			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			/***正常凭证;错误凭证***/
			showzerooccur: true, //正常凭证
            ckNotShowZeroOccurZeroBalance: true, //错误凭证
			/***多核算账簿显示方式***/
            defaultShowStyle: 'false',
            /***多核算账簿排序方式***/
            defaultSortStyle: 'false',
            cbCorpSubjDspBase: false,//按各核算账簿显示科目名称
            cbQryByCorpAss: false,//按各核算账簿查询辅助核算

			upLevelSubject: false, //显示上级科目
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: false, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			start: {},  //开始期间
			end: {}, //结束期间

			/*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
			startyear: '', //开始年
            startperiod: '',  //开始期间
			endyear: '',  //结束年
			endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示


            searchDateStyle: '0',//现金日记账 按制单/签字查询
			isversiondate: false,

            indexArr: [], //存放"查询对象"已经选择的索引


            queryCondition:{
                pk_accountingbook:{
                    value:''
                }
            },

			//tabel组件头部
			//table的数据
			tableSourceData: [],





			//第二层弹框里的table
            secondColumn:[],

			//序时账的table:
            journalColumn: [],
            journalDate: [],


            secondData: [],//第二层表格的数据

            selectRow: [],//存放所选择的不是参照类型的数据
            page: 0, //第二个弹框中要显示的行内容索引

            sourceSystem: [],   //来源系统的参照数据
			chooseoppositeSide: true, //对方科目取小值 是否可选 true:不可选  false: 可选

            vocherInputLeft: '', //凭证号左侧输入框
            vocherInputRight: '', //凭证号右侧侧输入框

            convertState: '0', //折算状态
            convertSource: '0', //折算来源
            difflag: '0', //差异凭证

			voucherStatusCell: [],
            voucherState: '0',//凭证状态


            originalMonyDirec: '0',//原币方向；
            originalMonyfirstAmount: '', //原币第一个金额；
            originalMonyStyle: 'and', ////原币的或/且；
            originalMonysecondAmount: '',//原币第二个金额；

            myMonyDirec: '0',//原币方向；
            myMonyfirstAmount: '', //原币第一个金额；
            myMonyStyle: 'and', ////原币的或/且；
            myMonysecondAmount: '',//原币第二个金额；
            showBusinessDate: false, //显示业日期
            iscombine: false, //合并查询

            voucherRefer: {refname: '', refpk: ''}, //凭证类别的参照数据
            madeBillPeople: {}, //制单人选择的参照数据`
            cashierPeople: {},  //出纳人选择的参照数据
            pk_system: {}, //制单系统
            currtype: [],      //选择的币种
            checkPeople: [],    //审核人选择的参照数据
            chargePeople: [],   //记账人选择的参照数据
            oppositeSubjectState: [],//对方科目
            subjectCodingState: [],//科目编码
            abstractState: []


		}
		this.handleTableSelect = this.handleTableSelect.bind(this);//辅助核算表格中的⌚️
		this.balanceMoney = this.balanceMoney.bind(this);//余额范围输入触发事件

        this.sureHandle = this.sureHandle.bind(this);
        this.closeNewModal = this.closeNewModal.bind(this);
        this.outInputChange = this.outInputChange.bind(this);



        this.renderTableFirstData = this.renderTableFirstData.bind(this);// table渲染初始的数据
        this.handleSearchObj = this.handleSearchObj.bind(this);//查询对象 选择触发事件
		this.findByKey = this.findByKey.bind(this);
		this.renderSearchRange = this.renderSearchRange.bind(this);//根据'查询对象'的选择来对应渲染'查询范围'的组件
		this.renderRefPathEle = this.renderRefPathEle.bind(this);//根据传进来的url渲染对应的参照组件
		this.handleCheckbox = this.handleCheckbox.bind(this);//计算小计	包含下级复选框的点击事件
		this.secondModalSelect = this.secondModalSelect.bind(this);//第二层弹框的选择框
		this.renderNewRefPath = this.renderNewRefPath.bind(this);//渲染第二层弹框内容的参照



		this.handleDateChange = this.handleDateChange.bind(this);//日期: 范围选择触发事件
		this.onChangeShowzerooccur = this.onChangeShowzerooccur.bind(this);//无发生不显示;本期无发生无余额不显示
		this.handleSortOrShowStyle = this.handleSortOrShowStyle.bind(this);//多核算账簿排序方式/多核算账簿显示方式
		this.handleDateRadio = this.handleDateRadio.bind(this);//会计期间/日期的单选框事件


		this.handleSelectChange = this.handleSelectChange.bind(this);//<Select>的onChange事件统一触发事件
		this.renderLevelOptions = this.renderLevelOptions.bind(this);//渲染级次的<NCOption>
		this.renderReferEle = this.renderReferEle.bind(this);//渲染 制单人；出纳人；审核人；记账人
		this.handleVocherChange = this.handleVocherChange.bind(this);// 凭证号输入；不能输入小数
		this.renderPureRefer = this.renderPureRefer.bind(this); //渲染参照的方法


		this.queryListAccount = this.queryListAccount.bind(this);//请求时序表的表格数据
		this.renderJournalRefer = this.renderJournalRefer.bind(this);// 根据不通的类型渲染对应的内容类型
		this.rednerPureRefer = this.rednerPureRefer.bind(this);// 渲染参照
		this.selectJournalTable = this.selectJournalTable.bind(this);//时序表表格选择
        this.renderRefer = renderRefer.bind(this);
        this.handleNumberInput = handleNumberInput.bind(this);//处理只能输入数字
        this.renderCurrtyRefer = renderCurrtyRefer.bind(this);
	}

	static defaultProps = {
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		// accAssItems: [],
	};
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                dateInputPlaceholder: this.state.json['20023040-000000'], /* 国际化处理： 选择日期*/
                voucherStatusCell: [
                    {key: json['20023040-000012'], val: '0'},/* 国际化处理： 全部凭证*/
                    {key: json['20023040-000013'], val: '1'}, {key: json['20023040-000014'], val: '2'},{key: json['20023040-000015'], val: '3'},/* 国际化处理： 已记账,待记账,未记账*/
                    {key: json['20023040-000016'], val: '4'}, {key: json['20023040-000017'], val: '5'},{key: json['20023040-000018'], val: '6'},/* 国际化处理： 已审核,待审核,未审核*/
                    {key: json['20023040-000019'], val: '7'}, {key: json['20023040-000020'], val: '8'},{key: json['20023040-000021'], val: '9'},/* 国际化处理： 已签字,待签字,未签字*/
                ],
                journalColumn: [
                    {
                        title: (<div fieldid='select'>{json['20023040-000006']}</div>), //"选择",
                        dataIndex: "select",
                        key: "select",
                        width: 50,
                        render: (text, record, index) => {
                            return <div fieldid='select'><Checkbox
                                checked={record.select}
                                onChange={() => {
                                    record.index = index;
                                    this.selectJournalTable(record, index);
                                }}
                            /></div>
                        }
                    },
                    {
                        title: (<div fieldid='searchObj'>{json['20023040-000001']}</div>), // "核算类型",
                        dataIndex: "searchObj",
                        key: "searchObj",
                        width: 200,
                        render: (text, record, index) => {
                            return (
                                <div fieldid='searchObj'>{record.name ? record.name : <span>&nbsp;</span>}</div>
                            )
                        }
                    },
                    {
                        title: (<div fieldid='searchRange'>{json['20023040-000002']}</div>), //"核算内容",
                        dataIndex: "searchRange",
                        key: "searchRange",
                        width : 200,
                        render: (text, record,index) => {
                            let renderEle = this.renderJournalRefer(record, 'searchRange');
                            return renderEle;
                        }
                    }
                ],

                secondColumn:[
                    {
                        title: json['20023040-000006'],/* 国际化处理： 选择*/
                        dataIndex: "select",
                        key: "select",
                        render: (text, record, index) => {
                            return <Checkbox
                                checked={record.select}
                                onChange={() => {
                                    record.index = index;
                                    this.secondModalSelect(record, index);
                                }}
                            />
                        }
                    },
                    {
                        title: json['20023040-000007'],/* 国际化处理： 查询属性*/
                        dataIndex: "name",
                        key: "name"
                    },
                    {
                        title: json['20023040-000008'],/* 国际化处理： 查询值*/
                        dataIndex: "refpath",
                        key: "refpath",
                        render: (text, record) => {

                            let renderSecondEle = this.renderNewRefPath(record);
                            return renderSecondEle;
                        }
                    }
                ],
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023040', 'dategroup', 'fourcheckbox', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
        this.renderTableFirstData(10);
    }
    componentDidMount() {
        setTimeout(() => getReferDetault(this, false, {
            businessUnit: businessUnit,
            getReferInfo: this.queryListAccount
        }),0)
    }

    handleVocherChange(value, param){// 凭证号输入；不能输入小数

        let result = value;
        let arrResult = result.split('');//输入值变为数组
        let code = arrResult[arrResult.length - 1];//最后一次输入的元素
        let codeValue = code && code.toString().charCodeAt(0);
        if(codeValue < 48 || codeValue > 57) {//不是数字的输入
            arrResult.splice(arrResult.length - 1);
        }

        this.setState({
            [param]: arrResult.join('')
        });
	}
    renderLevelOptions(num){

    	let optionArr = [];
    	for(let i=0; i<num; i++){
            optionArr.push(<NCOption value={i+1} key={i}>{i+1}</NCOption>);
		}
        return optionArr;
	}
    handleSelectChange(value, param){//<Select>的onChange事件统一触发事件

    	this.setState({
			[param]: String(value)
		})
	}
    handleDateRadio(value, param){//会计期间/日期的单选框事件

    	this.setState({
			[param]: value
		})
	}
	handleSortOrShowStyle(value, param){//多核算账簿显示方式/多核算账簿排序方式触发事件

		this.setState({
			[param]: value
		})
	}
    handleDateChange(value){//日期: 范围选择触发事件

    	this.setState({
            begindate: value[0],
            enddate: value[1],
			rangeDate: value
		})
	}
    handleSearchObj(value, record){//查询对象选择具体科目

    	let { tableSourceData, selectRow } = this.state;
        let originData = this.findByKey(record.key, tableSourceData);
        originData.selectCell = value;

        if(value.refpath ){
            originData.type = 'referpath';
            originData.searchRange = value.refpath;
		}else if(value.datatype === '1'){//字符
            originData.type = 'string';
        } else if(value.datatype === '4'){//整数
            originData.type = 'integer';
        } else if(value.datatype === '31'){//数值(小数)
            originData.type = 'number';
        } else if(value.datatype === '32'){//布尔
            originData.type = 'boolean';
        } else if(value.datatype === '33'){//日期
            originData.type = 'date'
        } else if(value.datatype === '34'){//日期+时分秒时间
            originData.type = 'dateTime'
        } else if(value.attr){
            originData.selectCellAttr = [...value.attr];
            originData.type = 'modal'
		}
		let newSelectRow = [...selectRow];
        newSelectRow[record.key] = record;
    	this.setState({
            tableSourceData,
			selectRow: newSelectRow
		})
	}
    handleCheckbox(key, record){//计算小计	包含下级复选框的点击事件
    	let {tableSourceData} = this.state;
        record[key] = record[key] === 'N' ? 'Y' : 'N';
        this.setState({
            tableSourceData
		})

	}
    secondModalSelect(record, index){//第二层弹框的复选框
    	let {tableSourceData} = this.state;
        let newSelect = !record.select;
        record.select = newSelect;
        tableSourceData[record.itemKey].selectCellAttr[index] = record;
        this.setState({tableSourceData})

	}
    findByKey(key, rows) {
        let rt = null;
        let self = this;
        rows.forEach(function(v, i, a) {
            if (v.key == key) {
                rt = v;
            }
        });
        return rt;
    }
    dateRefChange = (key, value, record) => {
        if(key === 'stringRefValue'){
            let length = record.inputlength;
            let exeReg = new RegExp("^.{0," + length + "}$");
            let result = exeReg.exec(value);
            if(result){
                record.selectObj = result[0]
                this.setState({
                    [key]: result[0]
                })
            }
        } else {
            record.selectObj = value
            this.setState({
                [key]: value
            })
        }
    }
    /**
     * param: 标记来之来之哪个节点
     * */
    numberRefChange = (key, value, record, param) => {
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectObj = this.state[key], 0)

    }
    renderJournalRefer(record, fieldid){//渲染时序表的表格参照内容
    	if(record.refpath){
    		return this.rednerPureRefer(record, fieldid);
		}else if(record.datatype === "1"){//"字符"
            return <Input
                fieldid={fieldid}
                value={this.state.stringRefValue}
                onChange={(value) => this.dateRefChange('stringRefValue', value, record)}
            />;
        }else if(record.datatype === "33"){//日期
            return (
                <DatePicker
                    fieldid={fieldid}
                    format={format}
                    value={this.state.assistDateValue}
                    placeholder={this.state.dateInputPlaceholder}
                    onChange={(value) => this.dateRefChange('assistDateValue', value, record)}
                />
            );
        }else if(record.datatype === '31'){//数值(小数)
            return <Input
                fieldid={fieldid}
                value={this.state.numberRefValue}
                onChange={(value) => this.numberRefChange('numberRefValue', value, record, 'journalNumber') }
            />;
        }else if(record.datatype === '4'){//整数
            return <Input
                fieldid={fieldid}
                value={this.state.numberRefValueInteger}
                onChange={(value) => this.numberRefChange('numberRefValueInteger', value, record, 'journalInteger') }
            />;
        }else if(record.datatype === '32'){//布尔
            return <Select
                showClear={false}
                fieldid={fieldid}
                value={this.state.booleanValue}
                className='search-boolean'
                onChange={(value)=>{
                    this.dateRefChange('booleanValue', value, record)
                }}
            >
                <NCOption value="Y">{this.state.json['20023040-000053']}</NCOption>{/* 国际化处理： 是*/}
                <NCOption value="N">{this.state.json['20023040-000054']}</NCOption>{/* 国际化处理： 否*/}
                <NCOption value="">{this.state.json['20023040-000055']}</NCOption>{/* 国际化处理： 空*/}
            </Select>;
        }else if(record.datatype === '34'){//日期+时分秒时间
            return (
                <DatePicker
                    fieldid={fieldid}
                    showTime={true}
                    format={Timeformat}
                    value={this.state.assistDateTimeValue}
                    placeholder={this.state.dateInputPlaceholder}
                    onChange={(value) => this.dateRefChange('assistDateTimeValue', value, record)}
                />
            );
        }
	}
	rednerPureRefer(record, fieldid){
    	let param = record.classid

        let referUrl = record.refpath + '.js' ;//
		let showName = this.state.journalDate[record.key].selectObj && this.state.journalDate[record.key].selectObj.refname;
		let showPk = this.state.journalDate[record.key].selectObj && this.state.journalDate[record.key].selectObj.refpk;
        if(!this.state[param]){//undefined
            {createScript.call(this,referUrl,param)}
        }else{
            //buSecond
            let pkOrgParam = this.state.buSecond[0] ? this.state.buSecond[0].refpk : this.state.unitValueParam;
            let options = {
                fieldid: fieldid,
                "isShowDisabledData": true,
                value:this.state.journalDate[record.key] && this.state.journalDate[record.key].selectObj,
                isMultiSelectedEnabled:true,
                queryCondition:() => {
                    return {
                        "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                        "pk_org":pkOrgParam,
                        "isDataPowerEnable": "Y",
                        "DataPowerOperationCode" : 'fi',
                        "dateStr": this.state.isversiondate ? this.state.versiondate : this.state.busiDate
                    }
                },
                onChange: (v)=>{
                    let self = this;

                    let {journalDate} = this.state;
                    let newDate = journalDate;
                    newDate[record.key].selectObj = v;
                    this.setState({
                        journalDate: [...newDate]
                    })
                }

            };
            let newOptions = {};
            if(record.refnodename === "uapbd/refer/sminfo/BankaccSubGridTreeRef/index"){
                newOptions = {
                    ...options
                }
            }else if(record.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'){
                newOptions = {
                    ...options,
                    isShowUnit:true,
                    "unitValueIsNeeded":false,
                    "isShowDisabledData": true,
                    queryCondition:() => {
                        return {
                            "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                            "pk_org": pkOrgParam,
                            "isDataPowerEnable": "Y",
                            "DataPowerOperationCode" : 'fi',
                            dateStr: this.state.isversiondate ? this.state.versiondate : this.state.busiDate,
                            "busifuncode":"all",
                            isShowDimission:true
                        }
                    },
                    unitProps:{
                        refType: 'tree',
                        refName: this.state.json['20023040-000024'],/* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:this.state.json['20023040-000024'],refpk:'root'},/* 国际化处理： 业务单元*/
                        placeholder:this.state.json['20023040-000024'],/* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[this.state.json['20023040-000025'], this.state.json['20023040-000026']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false,
                        "pk_org": pkOrgParam,
                    },
                    unitCondition:{
                        pk_financeorg: pkOrgParam,
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    },
                }
            }else if(record.classid.length === 20){
                newOptions = {
                    ...options,
                    pk_defdoclist: record.classid
                }
            }else {
                newOptions = {
                    ...options,
                    "isShowDisabledData": true,
                }
            }

            return (
                <Row>
                    <Col xs={12} md={12}>
                        {
                            this.state[param]?(this.state[param])(newOptions):<div/>
                        }
                    </Col>
                </Row>
            )
        }
	}
	getRecover(){
        let {journalDate} = this.state;
        journalDate.map((item, index) => {
            if(item.select || item.selectObj){
                item.select = false;
                Reflect.deleteProperty(item, 'selectObj')
            }
        });

        this.setState({
            journalDate: [...journalDate]
        })
    }
    selectJournalTable(record){
    	let {journalDate} = this.state;
    	let newObj = journalDate;
        newObj[record.key].select = !journalDate[record.key].select;
        this.setState({
            journalDate: [...newObj]
		})

	}
    renderSearchRange(record){//根据'查询对象'的选择来对应渲染'查询范围'的组件

    	switch(record.type || record.refnodename){
			case "referpath":
				let refPathEle = this.renderRefPathEle(record);

				return refPathEle;
			case 'date':
				return;
			case 'modal':

                let attrObj = record.selectCellAttr;
                let { tableSourceData } = this.state;
                let selectObj = [];
                attrObj.map((item)=>{
                	if(item){
                        item.itemKey = record.key;
					}
					if(item && item.selectName){
                        selectObj += item.selectName + ','
					}

				})
				return (
                    <div style={{display: 'flex'}}>
                        <Input
                            style={{border: 'none'}}
							placeholder={record.selectCell && record.selectCell.name}
							value={selectObj}
						/>
                        <div className='ellipsisContainer'
                             onClick={() => this.closeNewModal(record)}
                        >
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                            <span className='ellipsisCell'>.</span>
                        </div>

                    </div>
				);
			default:
				return <Input disabled/>;
		}
	}
    renderNewRefPath(record){
        let mybook;

        if(record.refpath){
            let objKey = record.attrclassid;
            if(!this.state[objKey]){//undefined
                {createScript.call(this,record.refpath+".js",objKey)}

            }else {
                mybook = (
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state[objKey] ? (this.state[objKey])(
                                    {
                                        value:{refname: record.selectName && record.selectName[0], refpk:  record.refpk && record.refpk[0]},
                                        isMultiSelectedEnabled: true,
                                        queryCondition: () => {
                                            return {
                                                "pk_accountingbook": this.state.accountingbook[0].pk_accountingbook ? this.state.accountingbook[0].pk_accountingbook : '',
                                                // "versiondate":currrentDate,
                                            }
                                        },
                                        onChange: (v) => {
                                            let {tableSourceData} = this.state;
                                            let refpkArr = [];
                                            let selectName = [];
                                            v.forEach((item) => {
                                                refpkArr.push(item.refpk);
                                                selectName.push(item.refname)
                                            })
                                            let pk_checkvalue = refpkArr.join(',')
                                            record.pk_checkvalue = pk_checkvalue;
                                            record.selectName = selectName;
                                            record.refpk = refpkArr;
                                            this.setState({
                                                tableSourceData
                                            })
                                        }
                                    }
                                ) : <div/>
                            }
                        </Col>
                    </Row>
                );
            }
		}

        return mybook;
	}
    renderPureRefer(param, isMultiSelectedEnabled){//渲染参照的方法
    	/**
		 * param:{url: '', key: '', stateKey: ''}
         * key: 渲染参照的方法的state的key值
         * stateKey: 参照选择值时保存选择值所对应的state的key值
		 * **/
        let referEle;
        let objKey = param.key;
        let pkOrgParam = this.state.buSecond[0] ? this.state.buSecond[0].refpk : this.state.unitValueParam;
        if(!this.state[objKey]){//undefined
            {createScript.call(this,param.url,objKey)}

        }else{
            let paramRender = param.key === "abstract" ? {
                fieldid: param.key,
                value: this.state[param.stateKey],
                isMultiSelectedEnabled:isMultiSelectedEnabled,
                queryCondition: {
                    pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
                    "isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi',
                    dateStr: this.state.busiDate,
                    "pk_org": pkOrgParam
                },
                onChange: (v)=>{
                    this.setState({
                        [param.stateKey]: v
                    })
                }
            } : {
                fieldid: param.key,
                value: this.state[param.stateKey],
                isMultiSelectedEnabled:isMultiSelectedEnabled,
                "isShowDisabledData": true,
                queryCondition: {
                    pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
                    "isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi',
                    dateStr: this.state.busiDate,
                },
                onChange: (v)=>{
                    this.setState({
                        [param.stateKey]: v
                    })
                }
            }
                
            referEle =  (
                <Row>
                    {/* <Col xs={4} md={4} > */}
                        {
                            this.state[objKey]?(this.state[objKey])({...paramRender}):<div/>
                        }
                    {/* </Col> */}
                </Row>
            );
        }
        return referEle;
	}
	renderRefPathEle(record){//根据传进来的url渲染对应的参照组件
        //带有参照的
		let mybook;
		let objKey = record.key + record.selectCell.pk_checktype;
		if(!this.state[objKey]){//undefined
			{createScript.call(this,record.selectCell.refpath+".js",objKey)}

		}else{
			mybook =  (
				<Row>
					<Col xs={12} md={12}>
						{
							this.state[objKey]?(this.state[objKey])(
								{
									isMultiSelectedEnabled:true,
									queryCondition:() => {
										return {
											"pk_accountingbook": this.state.accountingbook[0].pk_accountingbook? this.state.accountingbook[0].pk_accountingbook:'',
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi'
										}
									},
									onChange: (v)=>{
										let {tableSourceData} = this.state;
										let refpkArr = [];
										v.forEach((item)=>{
											let cellObj = {};
                                            cellObj.pk_checkvalue = item.refpk;
                                            cellObj.checkvaluecode = item.refcode;
                                            cellObj.checkvaluename = item.refname;
											refpkArr.push(cellObj);
										})
										let pk_checkvalue = [...refpkArr];
                                        record.selectRange = pk_checkvalue;
										this.setState({
                                            tableSourceData
										})
									}
								}
							):<div/>
						}
					</Col>
				</Row>
			);
		}
		return mybook;
	}
    renderTableFirstData(length){
    	let firstData = [];
    	for(let i=0; i<length; i++){
    		let obj = {};
    		obj.searchObj = [];
            obj.searchRange = '';
            obj.showLocal = 'N';//显示位置
            obj.accele = 'N';//小计
            obj.includesub = 'N',//包含下级
			obj.key = i
            firstData.push(obj);
		}
		this.setState({
            tableSourceData: [...firstData]
		})
	}

	handleTableSelect(){

	}

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}




	getCurrtypeList = () => {//获取"币种"接口
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {"localType":"1", "showAllCurr":"1"};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;

		        if(success){
					self.setState({
						currtypeList: data,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


    handleCurrtypeChange = (keyParam, value) => {
        this.setState({
            [keyParam]: value,
        });
    }


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(v) {

		if (!this.state.accountingbook || !this.state.accountingbook[0]) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,	
			year: '',		
		};
		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;

		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        		// versiondate: '2018-5-31', //data.versiondate,
		        	},)

                    getSubjectVersion(self);

                    // gl.accountrep.multianalysisqueryobject
                    let newUrl = '/nccloud/gl/accountrep/multianalysisqueryobject.do';//获取辅助核算内容
                    let newData = {
                        "pk_accountingbook":v[0].refpk,
                        "versiondate": '2018-5-31',//self.state.versiondate,
                    };

                    ajax({
                        url:newUrl,
                        data:newData,
                        success: function(response){
                            const { success } = response;
                            //渲染已有账表数据遮罩
                            if (success) {
                                if(response.data){

                                    response.data.map((item) => {
                                    	if(item.attr){
                                            item.attr.map((cell) => {
                                                cell.select = false
											})
										}
									})


                                    let newTableFirstData = [...self.state.tableSourceData];
                                    newTableFirstData.forEach((item, index) => {

                                        item.searchObj = response.data;
									})

                                    self.setState({
                                        tableSourceData: newTableFirstData
                                    })
                                }
                            }
                        }
                    });
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {

		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {

		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: '2018-5-31'// value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {

	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化/本期无发生无余额不显示;按各核算账簿显示科目名称/按各核算账簿查询辅助核算
	onChangeShowzerooccur(value, param) {

		this.setState({
			[param]: value
		});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}
	//显示上级科目复选框变化
	onChangeupLevelSubject(e) {
		this.setState({upLevelSubject: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}
	handleRadioSort(param){

		this.setState({
			selectedSort: param
		})
	}
	handleShowType(param){
		this.setState({
			selectedShowType: param
		})
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {

		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
	changeProjectStyle(param){//所有末级   表外科目选择
		if(param === 'allLast'){
			this.setState({
				allLast: !this.state.allLast
			})
		}else{
			this.setState({
				outProject: !this.state.outProject
			})
		}
	}

    outInputChange(param){

    	let nameArr = [];
        param.forEach((item) => {
            nameArr.push(item.refname);
		})
		this.setState({
            innerSelectName: [...nameArr]
		})
	}
    sureHandle(param){

    	let SelectCell = [];
        // param.forEach((item) => {
        	// if(item.pk_checkvalue){
         //        SelectCell.push(item);
		// 	}
		// });

        this.setState({
			innerSelectData: [...SelectCell]
		})
    	this.closeNewModal(param);
	}
    closeNewModal(param){

		this.setState({
            showNewModal: !this.state.showNewModal,
			page: param.key
		})
    }
    balanceMoney(value, param){

        let arrResult = value.split('');//输入值变为数组
        let code = arrResult[arrResult.length - 1];//最后一次输入的元素
        let codeValue = code && code.toString().charCodeAt(0);
        if (codeValue < 48 || codeValue > 57) {//不是数字的输入给剪掉
            arrResult.splice(arrResult.length - 1);
        }
    	this.setState({
			[param]: arrResult.join('')//数组变为字符串
		})
	}

	/****
	 * param: String
	 * 根据传入的参数渲染对应的参照组件
	 * ***/
	renderReferEle(param){

    	let referUrl = "gl/refer/voucher/OperatorDefaultRef/index.js" //
        if(!this.state[param]){//undefined
            {createScript.call(this,referUrl,param)}
        }else{
            return (
                <Row>
                    <Col xs={12} md={12}>
                        {
                            this.state[param]?(this.state[param])(
                                {
                                    value:{refname: this.state.accountingbook[0] && this.state.accountingbook[0].refname, refpk: this.state.accountingbook[0] && this.state.accountingbook[0].refpk},
                                    queryCondition:() => {
                                        // return {
                                        // 	"pk_accountingbook": self.state.pk_accountingbook.value
                                        // }
                                    },
                                    onChange: (v)=>{
                                        let self = this;

                                    }

                                }
                            ):<div/>
                        }
                    </Col>
                </Row>
			)
        }
	}



    queryListAccount=(self, dataCount)=>{//时序表的表格数据请求

        ajax({
            url: '/nccloud/gl/accountrep/assbalancequeryobject.do',
            data: dataCount,
            success: (res) => {
                let { dataRows, queryCondition } = this.state
                let { success, data } = res;
                if (success) {
                    // dataRows=data;

                    data.map((item, i) => {
                        item.key = i;
                        item.value = {
                            display: '',
                            value: ''
                        }
                        item.select = false
                    })
                    queryCondition.pk_accountingbook.value=dataCount.pk_accountingbook;

                    this.setState({
                        journalDate: [...data],
                        queryCondition
                    })
                }
            }
        });
    }
    handleValueChange = (key, value) => {
	    this.setState({
            [key]: value
        })
    }
	render() {
        let { show, title, isButtonShow}= this.props;
		return <Modal
            fieldid='query'
            show={ show }
            onHide={()=>this.props.onCancel(true)}
            className='journalquery'
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    {title}
                </Modal.Title>
            </Modal.Header >
			<Modal.Body id="modalOuter">
                <div className='right_query noserchmatter'>
                    <div className='query_body1'>
                        <div className='query_form journal'>
				<Row className="myrow">
					<Col md={2} sm={2}>
                        <span style={{color: 'red'}}>*</span>
						<span className='nc-theme-form-label-c'>{this.state.json['20023040-000050']}：</span>{/* 国际化处理： 核算账簿*/}
					</Col>
					<Col md={10} sm={10}>
						<div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: false,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit,
                                        getReferInfo: this.queryListAccount
                                    },
                                    'journal'
                                )
                            }
						</div>
					</Col>
				</Row>
				{
					this.state.isShowUnit ?
                        <Row className="myrow">
                            <Col md={2} sm={2}>
                                <span className='nc-theme-form-label-c'>{this.state.json['20023040-000024']}：</span>{/* 国际化处理： 业务单元*/}
                            </Col>
                            <Col md={10} sm={10}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
                                        fieldid='buSecond'
                                        value={this.state.buSecond}
                                        isMultiSelectedEnabled= {true}
                                        isShowDisabledData = {true}
                                        isHasDisabledData = {true}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                        }}
                                        onChange={(v)=>{
                                            this.getRecover();
                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if(this.state.accountingbook && this.state.accountingbook[0]) {
                                                }
                                            })
                                        }}
                                    />
                                </div>
                            </Col>
                        </Row> :
						<div></div>
				}

                {/*会计期间*/}
                <DateGroup
                    selectionState = {this.state.selectionState}
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                />

                 {/*折算状态 折算来源:*/}
                <Row className='rowCell'>

                        <Col md={2} >
                            <span className='nc-theme-form-label-c'>{this.state.json['20023040-000056']}</span>{/* 国际化处理： 折算状态*/}
                        </Col>
                       <Col md={4} >
                            <Select
                                showClear={false}
                                value= {this.state.convertState}
								onChange={(value) => {
									this.handleDateRadio(value, 'convertState');
								}}
							>
                                <NCOption value='0'>{this.state.json['20023040-000012']}</NCOption>{/* 国际化处理： 全部凭证*/}
								<NCOption value='1'>{this.state.json['20023040-000057']}</NCOption>{/* 国际化处理： 已折算*/}
								<NCOption value='2'>{this.state.json['20023040-000058']}</NCOption>{/* 国际化处理： 未折算*/}
							</Select>
                        </Col>
                        <Col md={2} >
                            <span className='nc-theme-form-label-c'>{this.state.json['20023040-000059']}:</span>{/* 国际化处理： 折算来源*/}
                        </Col>
                       <Col md={4} >
                            <Select
                                showClear={false}
                                fieldid='convertSource'
                                value= {this.state.convertSource}
                                onChange={(value) => {
                                    this.handleDateRadio(value, 'convertSource');
                                }}
                            >
                                <NCOption value='0'>{this.state.json['20023040-000012']}</NCOption>{/* 国际化处理： 全部凭证*/}
                                <NCOption value='1'>{this.state.json['20023040-000060']}</NCOption>{/* 国际化处理： 折算生成*/}
                                <NCOption value='2'>{this.state.json['20023040-000061']}</NCOption>{/* 国际化处理： 非折算生成*/}
                            </Select>
                        </Col>



                </Row>

                {/*凭证类别 凭证号*/}
				<Row className='rowCell'>

                        <Col md={2}>
                            <span className='nc-theme-form-label-c'>{this.state.json['20023040-000031']}：</span>{/* 国际化处理： 凭证类别*/}
                        </Col>
                       <Col md={4}>
                            <VoucherRefer
                                fieldid='voucherRefer'
                                value={{refname: this.state.voucherRefer.refname, refpk: this.state.voucherRefer.refpk}}
                                isShowDisabledData = {true}
                                queryCondition={ () => {
                                    return {
                                        pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi',
                                        "pk_org": this.state.accountingbook && this.state.accountingbook.refpk
                                    }
                                }}
                                onChange={(value) => {
                                    // madeBillPeople: {refname: ''}, //制单人选择的参照数据
                                    this.setState({
                                        voucherRefer: {...value}
                                    })
                                }}
							/>
                        </Col>
                        <Col md={2} >
                            <span className='nc-theme-form-label-c'>{this.state.json['20023040-000062']}：</span>{/* 国际化处理： 凭证号*/}
                        </Col>
                        <Col md={4} style={{display: 'flex'}}>
							<Input
                                fieldid='vocherInputLeft'
								value={this.state.vocherInputLeft}
								onChange={(value) => {this.handleVocherChange(value, 'vocherInputLeft')}}
							/>
							-
							<Input
                                fieldid='vocherInputRight'
                                value={this.state.vocherInputRight}
                                onChange={(value) => {this.handleVocherChange(value, 'vocherInputRight')}}
							/>
                        </Col>
                        </Row>

                {/*制单人: 出纳人:*/}
                <Row className='rowCell'>
                    <Col md={2}  >
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000032']}:</span>{/* 国际化处理： 制单人*/}
                    </Col>
                    <Col md={4}  >
                        <ReferEle
                            fieldid='madeBillPeople'
                            isMultiSelectedEnabled={false}
							value={this.state.madeBillPeople}
                            queryCondition={ () => {
                                return {
                                	pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
									user_Type : '0',
									GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
							onChange={(value) => {
								// madeBillPeople: {refname: ''}, //制单人选择的参照数据
								this.setState({
                                    madeBillPeople: {...value}
								})
							}}
						/>
                    </Col>

                    <Col md={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000033']}:</span>{/* 国际化处理： 出纳人*/}
                    </Col>
                    <Col md={4}>
                        <ReferEle
                            fieldid='cashierPeople'
                            isMultiSelectedEnabled={false}
                            value={this.state.cashierPeople}
                            queryCondition={ () => {
                                return {
                                    pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
                                    user_Type : '2',
                                    GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
                            onChange={(value) => {
                                // cashierPeople: {refname: ''},  //出纳人选择的参照数据
                                this.setState({
                                    cashierPeople: {...value}
                                })
                            }}
                        />
                    </Col>
				</Row>

                {/*附单据数：*/}
                <Row className='rowCell'>


                    <Col md={2} >
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000063']}：</span>{/* 国际化处理： 附单据数*/}
                    </Col>
                    <Col md={3} >
                        <Select
                        showClear={false}
                        size="lg"
                        fieldid='balanceori'
                        value={this.state.balanceori}
                        // style={{ width: 80, marginRight: 6, display: 'none' }}
                        onChange={
                            (value) => this.handleSelectChange(value, 'balanceori')
                        }
                    >
                        <NCOption value={'-1'} key={'-1'} >{this.state.json['20023040-000064']}</NCOption>{/* 国际化处理： 双向*/}
                        <NCOption value={'0'} key={'0'} >{this.state.json['20023040-000065']}</NCOption>{/* 国际化处理： 借*/}
                        <NCOption value={'1'} key={'1'} >{this.state.json['20023040-000066']}</NCOption>{/* 国际化处理： 贷*/}
                        <NCOption value={'3'} key={'3'} >{this.state.json['20023040-000067']}</NCOption>{/* 国际化处理： 平*/}
                    </Select>
                    </Col>
                    <Col md={1}></Col>
                    <Col md={6}>
                        <Col md={5} >
                            <Input
                                fieldid='startBalance'
                                value={this.state.balanceMoneyLeft}
                                onChange={(value) => this.balanceMoney(value, 'balanceMoneyLeft')}
                            />
                        </Col>
                        <Col md={2} ><p className='zhi nc-theme-form-label-c'>~</p></Col>
                        <Col md={5} >
                            <Input
                            fieldid='endBalance'
                            value={this.state.balanceMoneyRight}
                            onChange={(value) => this.balanceMoney(value, 'balanceMoneyRight')}
                        />
                        </Col>
                    </Col>

                </Row>

                {/*显示属性*/}
                <FourCheckBox
                    totalTitle= {this.state.json['20023040-000034']} ///* 国际化处理： 显示属性：*/
                    renderCells={
                        [
                            {
                                title: this.state.json['20023040-000035'],/* 国际化处理： 正常凭证*/
                                checked: this.state.showzerooccur,
                                id: 'showzerooccur',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'showzerooccur')
                            },
                            {
                                title: this.state.json['20023040-000036'],/* 国际化处理： 错误凭证*/
                                checked: this.state.ckNotShowZeroOccurZeroBalance,
                                id: 'ckNotShowZeroOccurZeroBalance',
                                onChange: (value) => this.onChangeShowzerooccur(value, 'ckNotShowZeroOccurZeroBalance')
                            },
                        ]
                    }
                />

                {/*凭证状态 差异凭证*/}
                <Row className='rowCell'>
                    <Col md={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000068']}</span>{/* 国际化处理： 凭证状态*/}
                    </Col>
                    <Col md={4}>
                        <Select
                            showClear={false}
                            fieldid='voucherState'
                            value={this.state.voucherState}
                            onChange={(value) => {
                                this.handleDateRadio(value, 'voucherState');
                            }}
                        >
                            {
                                this.state.voucherStatusCell.map((item) => {
                                    return <NCOption value={item.val} key={item}>{item.key}</NCOption>
                                })
                            }
                        </Select>
                    </Col>
                    <Col md={2} >
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000069']}</span>{/* 国际化处理： 差异凭证*/}
                    </Col>
                        <Col md={4} >
                        <Select
                            showClear={false}
                            fieldid='difflag'
                            value={this.state.difflag}
                            onChange={(value) => {
                                this.handleDateRadio(value, 'difflag');
                            }}
                        >
                            <NCOption value='0'>{this.state.json['20023040-000012']}</NCOption>{/* 国际化处理： 全部凭证*/}
                            <NCOption value='1'>{this.state.json['20023040-000069']}</NCOption>{/* 国际化处理： 差异凭证*/}
                            <NCOption value='2'>{this.state.json['20023040-000070']}</NCOption>{/* 国际化处理： 非差异凭证*/}
                        </Select>
                    </Col>
                </Row>

                {/*制单系统： 币种*/}
                <Row className='rowCell'>

                    <Col md={2} >
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000039']}：</span>{/* 国际化处理： 制单系统*/}
                    </Col>
                    <Col md={4} >
                        <CtlSysRef
                            fieldid='pk_system'
                            isMultiSelectedEnabled={false}
                            value={this.state.pk_system}
                            queryCondition={ () => {
                                return {
                                    "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
                            onChange={(value) => {
                                this.setState({
                                    pk_system: {...value}
                                })
                            }}
                        />
                    </Col>
                    <Col md={2}>
                        {/*币种*/}
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000071']}</span>{/* 国际化处理： 币种*/}
                    </Col>
                    <Col md={4}>
                        {
                            this.renderCurrtyRefer(
                                currType,
                                this.state.currtype,
                                'currTypeRefer',
                                this.state['currTypeRefer'],
                                'currtype',
                                true, //是否多选
                                {
                                    "pk_accountingbook":  this.state.accountingbook && this.state.accountingbook.refpk,   //多选，选一个也算
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                },
                                this.handleValueChange
                            )
                        }
                    </Col>
                </Row>

                {/*审核人：记账人:*/}
                <Row className='rowCell'>
                    <Col md={2} >
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000040']}：</span>{/* 国际化处理： 审核人*/}
                    </Col>
                        <Col md={4} >
                        <ReferEle
                            fieldid='checkPeople'
                            isMultiSelectedEnabled={true}
                            value={this.state.checkPeople}
                            queryCondition={ () => {
                                return {
                                    pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
                                    user_Type : '1',
                                    GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
                            onChange={(value) => {
                                // checkPeople: {refname: ''},    //审核人选择的参照数据
                                this.setState({
                                    checkPeople: [...value]
                                })
                            }}
                        />
                    </Col>
                    <Col md={2} >
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000041']}: </span>{/* 国际化处理： 记账人*/}
                    </Col>
                    <Col md={4} >
                        <ReferEle
                            fieldid='chargePeople'
                            isMultiSelectedEnabled={true}
                            value={this.state.chargePeople}
                            queryCondition={ () => {
                                return {
                                    pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
                                    user_Type : '3',
                                    GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
                            onChange={(value) => {
                                // chargePeople: {refname: ''},   //记账人选择的参照数据
                                this.setState({
                                    chargePeople: [...value]
                                })
                            }}
                        />
                    </Col>
                </Row>

                {/*对方科目 科目编码*/}
                <Row className='rowCell'>
                    <Col md={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000072']}</span> {/* 国际化处理： 对方科目*/}
                    </Col>
                   <Col md={4} className='journalrenderPureRefer'>
                        {this.renderPureRefer(referUrlObj.oppositeSubject, true)}
                    </Col>
                    <Col  md={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000073']}</span>{/* 国际化处理： 科目编码*/}
                    </Col>
                    <Col md={4} className='journalrenderPureRefer'>
                        {this.renderPureRefer(referUrlObj.subjectCoding, true)}
                    </Col>
                </Row>

                {/*摘要:*/}
                <Row className='rowCell'>
                    <Col md={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023040-000074']}:</span>{/* 国际化处理： 摘要*/}
                    </Col>
                     <Col md={4}>
                        {this.renderPureRefer(referUrlObj.abstractObj, true)}
                    </Col>

                </Row>

                {/*原币>=*/}
                <Row className='rowCell'>
                    <Col md={2} >
						<Select
                            showClear={false}
                            fieldid='originalMonyDirec'
							value={this.state.originalMonyDirec}
							onChange={(value) => {
								this.handleDateRadio(value, 'originalMonyDirec');
							}}
						>
							<NCOption value='0'>{this.state.json['20023040-000075']}</NCOption>{/* 国际化处理： 双*/}
							<NCOption value='1'>{this.state.json['20023040-000065']}</NCOption>{/* 国际化处理： 借*/}
							<NCOption value='2'>{this.state.json['20023040-000066']}</NCOption>{/* 国际化处理： 贷*/}
						</Select>
                    </Col>
                    <Col md={2}>
						<p className='zhi nc-theme-form-label-c'>{this.state.json['20023040-000076']}>=</p>{/* 国际化处理： 原币*/}
                    </Col>
                    <Col md={2} >
                        <NCNumber
                            fieldid='originalMonyfirstAmount'
                            scale={10}
                            value={this.state.originalMonyfirstAmount}
                            onChange={(value) => this.handleSortOrShowStyle(value, 'originalMonyfirstAmount')}
                        />
                        {/*<Input*/}
                            {/*value={this.state.originalMonyfirstAmount}*/}
                            {/*onChange={(value) => {*/}
                                {/*this.handleNumberInput('originalMonyfirstAmount', value, 'journal');*/}
                            {/*}}*/}
                        {/*/>*/}
                    </Col>
                    <Col md={1}></Col>
                    <Col md={2}>
						<Select
                            showClear={false}
                            fieldid='originalMonyStyle'
							value={this.state.originalMonyStyle}
							onChange={(value) => {
								this.handleDateRadio(value, 'originalMonyStyle');
							}}
						>
                            <NCOption value='and'>{this.state.json['20023040-000077']}</NCOption>{/* 国际化处理： 且*/}
							<NCOption value='or'>{this.state.json['20023040-000078']}</NCOption>{/* 国际化处理： 或*/}
						</Select>
                        
                    </Col>
                    <Col md={1}><p className='zhi'>{'<='}</p></Col>
                    <Col md={2} >
                        <NCNumber
                            fieldid='originalMonysecondAmount'
                            scale={10}
                            value={this.state.originalMonysecondAmount}
                            onChange={(value) => this.handleSortOrShowStyle(value, 'originalMonysecondAmount')}
                        />
						{/*<Input*/}
							{/*value={this.state.originalMonysecondAmount}*/}
							{/*onChange={(value) => {*/}
                                {/*this.handleNumberInput('originalMonysecondAmount', value, 'journal');*/}
							{/*}}*/}
						{/*/>*/}
                    </Col>
                </Row>

                {/*本币>=*/}
                <Row className='rowCell'>
                    <Col md={2} >
                        <Select
                            showClear={false}
                            fieldid='myMonyDirec'
							value={this.state.myMonyDirec}
                            onChange={(value) => {
                                this.handleDateRadio(value, 'myMonyDirec');
                            }}
                        >
                            <NCOption value='0'>{this.state.json['20023040-000075']}</NCOption>{/* 国际化处理： 双*/}
                            <NCOption value='1'>{this.state.json['20023040-000065']}</NCOption>{/* 国际化处理： 借*/}
                            <NCOption value='2'>{this.state.json['20023040-000066']}</NCOption>{/* 国际化处理： 贷*/}
                        </Select>
                    </Col>
                    <Col md={2}>
                        <p className='zhi nc-theme-form-label-c'>{this.state.json['20023040-000079']}>=</p>{/* 国际化处理： 本币*/}
                    </Col>
                    <Col md={2} >
                        <NCNumber
                            fieldid='myMonyfirstAmount'
                            scale={10}
                            value={this.state.myMonyfirstAmount}
                            onChange={(value) => this.handleSortOrShowStyle(value, 'myMonyfirstAmount')}
                        />
                        {/*<Input*/}
							{/*value={this.state.myMonyfirstAmount}*/}
							{/*onChange={(value) => {*/}
                                {/*this.handleNumberInput('myMonyfirstAmount', value, 'journal');*/}
							{/*}}*/}
						{/*/>*/}
                    </Col>
                    <Col md={1}></Col>
                    <Col md={2}>
                        <Select
                            showClear={false}
                            fieldid='myMonyStyle'
							value={this.state.myMonyStyle}
                            onChange={(value) => {
                                this.handleDateRadio(value, 'myMonyStyle');
                            }}
                        >
                            <NCOption value='and'>{this.state.json['20023040-000077']}</NCOption>{/* 国际化处理： 且*/}
                            <NCOption value='or'>{this.state.json['20023040-000078']}</NCOption>{/* 国际化处理： 或*/}
                        </Select>
                        
                    </Col>
                    <Col md={1}><span className='zhi'>{'<='}</span></Col>
                    <Col md={2} >
                        <NCNumber
                            fieldid='myMonysecondAmount'
                            scale={10}
                            value={this.state.myMonysecondAmount}
                            onChange={(value) => this.handleSortOrShowStyle(value, 'myMonysecondAmount')}
                        />
                        {/*<Input*/}
                            {/*value={this.state.myMonysecondAmount}*/}
                            {/*onChange={(value) => {*/}
                                {/*this.handleNumberInput('myMonysecondAmount', value, 'journal');*/}
                            {/*}}*/}
						{/*/>*/}
                    </Col>
                </Row>

                {/*本币类型：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency,
                            title: this.state.json['20023040-000044']/* 国际化处理： 本币类型*/
                        },
                        this.state.json
                    )
                }


                <div className="modalTable">
                    <Table
                        columns={this.state.journalColumn}
                        data={this.state.journalDate}
                        scrolly={100}
                    />
                </div>

                {/*显示属性*/}
                <Row className='rowCell'>
                    <FourCheckBox
                        totalTitle= {this.state.json['20023040-000034']}/* 国际化处理： 显示属性：*/
                        renderCells={
                            [
                                {
                                    title: this.state.json['20023040-000045'],/* 国际化处理： 合并查询*/
                                    checked: this.state.iscombine,
                                    id: 'iscombine',
                                    onChange: (value) => this.onChangeShowzerooccur(value, 'iscombine')
                                },
                                {
                                    title: this.state.json['20023040-000046'],/* 国际化处理： 显示业务日期*/
                                    checked: this.state.showBusinessDate,
                                    id: 'showBusinessDate',
                                    onChange: (value) => this.onChangeShowzerooccur(value, 'showBusinessDate')
                                },
                            ]
                        }
                    />
                </Row>
                <NewModal
                    title= {this.state.json['20023040-000047']} /* 国际化处理： 属性选择*/
                    record={this.state.selectRow[this.state.page]}
                    column={this.state.secondColumn}
                    showNewModal = {this.state.showNewModal}//控制设否显示
                    closeNewModal = {this.closeNewModal}
                    sureHandle = {this.sureHandle}
                    sureText = {this.state.json['20023040-000048']}/* 国际化处理： 确定 */
                    cancleText = {this.state.json['20023040-000023']}/* 国际化处理： 取消*/
                />
                        </div>
                    </div>
                </div>
			</Modal.Body>
			{isButtonShow &&
				<Modal.Footer>
					<Button
                        fieldid='query'
                        className= "button-primary"
						onClick = {() => {

                                let abstractStateArr = [];//摘要
                                let checkPeopleArr = [];
						        let chargePeopleArr = [];
								let pk_accountingbook = [];
								let pk_unit = [];
								let moneyLeft = Number(this.state.balanceMoneyLeft);
								let moneyRight = Number(this.state.balanceMoneyRight);
								this.state.abstractState.length>0 && this.state.abstractState.map((item, indx) => {
                                    abstractStateArr.push(item.refname)
                                })
								this.state.checkPeople.map((item,index) => {
                                    checkPeopleArr.push(item.refpk);
                                })
                                this.state.chargePeople.map((item, index) => {
                                    chargePeopleArr.push(item.refpk);
                                })


								if(moneyLeft > moneyRight){
                                    toast({
										content: this.state.json['20023040-000049'],/* 国际化处理： 余额范围查询条件信息输入有误(起始值大于终止值或是为负值)！*/
										color: 'warning'
                                    });
                                    return;
								}
								let lastAssvos = [];
								this.state.journalDate.map((item) => {//处理序时账的表格数据选择
									let objCell = {};
									let objArr = [];//存放选择内容的pkvalue
                                    if(item.select){
                                        objCell.m_pk_checktype = item.pk_checktype;

                                        if(item.selectObj){
                                            Array.isArray(item.selectObj) && item.selectObj.map((cell) => {
                                                objArr.push(cell.refpk)
                                            })
                                            objCell.m_pk_checkvalue = Array.isArray(item.selectObj) ? objArr.join(',') : item.selectObj
                                        }
                                        lastAssvos.push(objCell)
                                    }
								})
								if (Array.isArray(this.state.accountingbook)) {
									this.state.accountingbook.forEach(function (item) {
										pk_accountingbook.push(item.refpk)
									})
								}

								if (Array.isArray(this.state.buSecond)) {
									this.state.buSecond.forEach(function (item) {
										pk_unit.push(item.refpk)
									})
								}


								let oppositeSubjectStateArr = [];
								if(this.state.oppositeSubjectState){
                                    this.state.oppositeSubjectState.map((item) => {
                                        oppositeSubjectStateArr.push(item.refcode)
									})
								}
								let accountingcodeArr = [];
								if(this.state.subjectCodingState){
                                    this.state.subjectCodingState.map((value) => {
                                        accountingcodeArr.push(value.refcode);
									})
								}
								let explanationArr = [];
								if(this.state.explanation){
									this.state.explanation.map((item) => {
                                        explanationArr.push(item.refpk);
									})
								}
								let currTypeArr = []

                                this.state.currtype.map((item, index) => {
                                    currTypeArr.push(item.refpk);
                                })
                            let data = {
                                /*******核算账簿****/
                                pk_accountingbook: this.state.accountingbook.refpk?[this.state.accountingbook.refpk]:[],//["1001A3100000000008MT"]
                                /*******业务单元****/
                                pk_unit: pk_unit,//["0001A310000000000U50"],// 业务单元 不选不传或传null

                                /*******会计期间参数****/
                                querybyperiod: this.state.selectionState,
                                year_start: this.state.startyear,
                                period_start: this.state.startperiod,
                                year_end: this.state.endyear,
                                period_end:   this.state.endperiod,
                                /*******日期参数****/
                                prepareddate_start: this.state.begindate,
                                prepareddate_end: this.state.enddate,

                                /***折算状态,/折算来源;合并查询；显示业务日期***/
                                convertState: this.state.convertState,//折算状态
                                convertSource: this.state.convertSource,//折算来源
                                showBusinessDate: this.state.showBusinessDate, //显示业日期
                                iscombine: this.state.iscombine, //合并查询

                                /*******凭证类别；凭证号；制单人****/
                                pk_vouchertype: this.state.voucherRefer.refpk, //"",// 凭证类别
                                num_start: this.state.vocherInputLeft,  //开始凭证号
                                num_end: this.state.vocherInputRight, //结束凭证号
                                pk_prepared: this.state.madeBillPeople.refpk ? this.state.madeBillPeople.refpk: '',  //制单人：""


                                /*******币种；附单据数;出纳人****/
                                pk_currtype: currTypeArr,//币种
                                // balanceori: this.state.balanceori, //余额方向："双向/借/贷/平", //余额方向
                                attachment_start: this.state.balanceMoneyLeft, //附单据数开始
                                attachment_end: this.state.balanceMoneyRight, //附单据数结束
                                pk_casher: this.state.cashierPeople.refpk ? this.state.cashierPeople.refpk : '',  //出纳人：''


                                /**凭证状态；差异凭证；审核人**/
                                voucherState: this.state.voucherState, //凭证状态
                                difflag: this.state.difflag, //差异凭证
                                pk_checked: checkPeopleArr.join(','),  //审核人：''


                                /*******制单系统; 正常凭证; 错误凭证; 记账人:****/
                                pk_system: this.state.pk_system.refcode,  //"ALL",// 制单系统
                                normalflag: this.state.showzerooccur,  //正常凭证  Y/N
                                errorflag: this.state.ckNotShowZeroOccurZeroBalance, //错误凭证 Y/N
                                pk_manager:  chargePeopleArr.join(','),          //记账人：''

                                /*******对方科目；科目编码；摘要 ****/
                                oppositesubj: oppositeSubjectStateArr ,  //对方科目 ["1123","1221"]
                                accountingcode: accountingcodeArr, //["10020101","10020108"],//科目编码
                                explanation: abstractStateArr,  //["111","法人公司201摘要"],// 摘要

                                /***原币；本币***/
                                oriamount: {
                                    "direction":this.state.originalMonyDirec,
                                    "firstAmount":this.state.originalMonyfirstAmount,
                                    "andOr":this.state.originalMonyStyle,
                                    "secondAmount":this.state.originalMonysecondAmount,
                                },
                                amount:{
                                    "direction":this.state.myMonyDirec,//0=双向，1=借，2=贷
                                    "firstAmount":this.state.myMonyfirstAmount,
                                    "andOr":this.state.myMonyStyle,
                                    "secondAmount":this.state.myMonysecondAmount,
                                },// 本币
                                /*******本币类型****/
                                amountType: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局

                                /*******table框的内容****/
                                assvos: lastAssvos,
                                pageindex: '1'
                            }
                            
								let url = '/nccloud/gl/accountrep/checkparam.do'
								let flagShowOrHide;
								ajax({
									url,
									data,
									async:false,
									success:function(response){
										flagShowOrHide = true
									},
									error:function(error){
										flagShowOrHide = false
										toast({content: error.message, color: 'warning'});
									}
								})
								if(flagShowOrHide == true){
									this.props.onConfirm(data)
								}else if(flagShowOrHide == false){
									return true;
								}

							}
						}
					>
						{ this.state.json['20023040-000022']}{/* 国际化处理： 查询*/}
					</Button>


                    <Button 
                        fieldid='cancel'
						className= 'btn-2 btn-cancel'
						onClick={() => {
							this.props.onCancel(false);
						}}
					>{this.state.json['20023040-000023']}</Button>{/* 国际化处理： 取消*/}
				</Modal.Footer>
			}
			<Loading fullScreen showBackDrop={true} show={this.state.isLoading} />
		</Modal>;
	}
}
CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal
