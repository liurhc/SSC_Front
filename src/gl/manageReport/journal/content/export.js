/**
 * Created by liqiankun on 2018/7/04.
 * 账簿查询 ---> 序时账
 */

import React, {Component} from 'react';

import { createPage } from 'nc-lightapp-front';

import JournalContent from './content.js';
import {tableDefaultData} from "../../defaultTableData";
import '../../../public/reportcss/firstpage.less'

class ExportJournal extends Component{
    constructor(props){
        super(props);

        this.state = {
            dataout: tableDefaultData,
        }
    }
    componentDidMount(){
        this.props.ExportJournalRef(this)
    }
    modalContent = () => {
        return(
            <JournalContent
                flag={true} //true在弹框中显示
                dataout = {this.props.dataout}
            />
        )
    }
    render(){
        let { modal, title } = this.props;
        let { createModal } = modal;
        return(
            <div>
                {createModal('exportjournal', {
                    title: title, 
                    content: this.modalContent(), //弹框内容，可以是字符串或dom
                    // closeModalEve: this.closeModalEve, //关闭按钮事件回调
                    size:'xxlg', //  模态框大小 sm/lg/xlg/xxlg
                    bodyHeight: '542px',
                    noFooter : true, //是否需要底部按钮,默认有footer,有false,没有true
                    closeByClickBackDrop:false,//点击遮罩关闭提示框，true是点击关闭，false是阻止关闭,默认是false
                    hasBackDrop:true,//显示遮罩层，显示是true，不显示是false，默认是true
                    zIndex:200//遮罩的层级为zIndex，弹框默认为zIndex+1。默认为200，非比传项
                })}
            </div>
        )
    }
}

ExportJournal = createPage({})(ExportJournal)
export default ExportJournal;

//() => clickhandle()
