/**
 * Created by liqiankun on 2018/7/03.
 * 账簿查询 ---> 银行日记账
 */

import React, {Component} from 'react';

import {high, base, ajax, createPage, getMultiLang,createPageIcon} from 'nc-lightapp-front';
const { NCDiv } = base;
import '../../css/index.less'
import HeadCom from '../../common/headSearch/headCom';
// import {titleLabels } from '../../common/headSearch/titledata';
import { SimpleTable } from 'nc-report';
import QueryModal from './components/MainSelectModal';
import {tableDefaultData} from '../../defaultTableData'
import reportPrint from '../../../public/components/reportPrint';
import { toast } from '../../../public/components/utils';
import {relevanceSearch} from "../../referUrl";
import {getDetailPort} from "../../common/modules/modules";
const { PrintOutput } = high;
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import {handleValueChange} from '../../common/modules/handleValueChange'
import reportSaveWidths from "../../../public/common/reportSaveWidths";
import {setData} from "../../common/withPageDataSimbletable";
import {searchById} from "../../common/modules/createBtn";
import "../../../public/reportcss/firstpage.less";
import {handleSimpleTableClick} from "../../common/modules/simpleTableClick";
import PageButtonGroup from '../../common/modules/pageButtonGroup'
import {rowBackgroundColor} from "../../common/htRowBackground";
import {whetherDetail} from "../../common/modules/simpleTableClick";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
class AssistPropertyBalance extends Component{
    constructor(props){
        super(props);

        this.state = {
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            disabled: true,
            visible: false,
            appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            outputData: {},
            ctemplate: '', //模板输出-模板
            nodekey: '',
            printParams:{}, //查询框参数，打印使用
            flow: false,//控制表头是否分级
            showModal: false, //控制弹框显示
            textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            dataWithPage: [], //存放请求回来的数据信息，用于分页操作
            firstPageDisable: true,
            lastPageDisable: true,
            paramObj: {},//存放回调的查询参数，用于翻页
            accountType: 'amountcolumn',//金额式
            'bankAccount': []
        }
        this.dataPage = 1; //存放请求回来的数据椰树信息
        this.queryClick = this.queryClick.bind(this);//点击查询, 弹框的"关闭"事件
        this.modalSure = this.modalSure.bind(this);//点击弹框"确定"的事件
        this.handlePage = this.handlePage.bind(this);//页面处理事件
        // this.setData= this.setData.bind(this);// 整理表格所需要的数据格式
        this.queryData = this.queryData.bind(this);//请求数据的方法；
        this.handleValueChange = handleValueChange.bind(this);
        // this.changeSelectStyle = this.changeSelectStyle.bind(this);//选择 '账簿格式'事件
        this.searchById = searchById.bind(this);
        this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                'bankAccount': [
                    {
                        title: json['20023082-000035'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20023082-000036'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023082-000037'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023082-000038'],
                        styleClass:"m-brief"
                    },

                    {
                        title: json['20023082-000039'],
                        styleClass:"m-brief"
                    }
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023082','publiccommon', 'dategroup'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        this.setState({
            flag: true,
        });
        let appceod = this.props.getSearchParam('c');
        this.searchById('20023082PAGE',appceod);
        this.props.button.setDisabled({
            print: true, directprint: true,
            templateOutput: true, linkvoucher: true, saveformat: true,
        })
        this.props.button.setButtonVisible(['first', 'pre', 'next', 'last'], false)
    }
    showPrintModal = () => {
        this.setState({
            visible: true
        })
    }
    handlePrint(data, isPreview) {
        let printUrl = '/nccloud/gl/accountrep/bankdiaryprint.do'
        let { printParams } = this.state
        let { ctemplate, nodekey } = data
        let appcode ='20023035'
        printParams.currpage = this.dataPage.toString() //当前页
        printParams.queryvo = data;
        printParams.totalpage = this.state.dataWithPage.totalpage
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
        printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
        this.handleCancel()
    }
    handleCancel() {
        this.setState({
            visible: false
        });
    }
    showOutputModal = () => {
        // this.refs.printOutput.open()
        let outputUrl = '/nccloud/gl/accountrep/bankdiaryoutput.do'
        let {  nodekey, ctemplate, printParams } = this.state
        let appcode ='20023035'
        printParams.currpage = this.dataPage.toString() //当前页
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams, this.state.dataWithPage.totalpage)
        // let outputData = mouldOutput(appcode, nodekey, ctemplate, printParams)
        // this.setState({
        //     outputData: outputData
        // })
    }
    handleOutput() {

    }

    handlePage(param, dataWithPage){//页面控制事件
        let {paramObj} = this.state;
        let dataLength = Number(dataWithPage.totalpage);
        let renderData = [];
        let obj = {};
        let stringPage = '1';
        switch(param){
            case 'firstPage':
                this.dataPage = 1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                paramObj.operation = param;
                this.setState({
                    paramObj,
                    firstPageDisable: true,
                    lastPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    first: true, pre: true,
                    next: false, last: false
                })
                break;
            case 'nextPage':
                this.dataPage += 1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                paramObj.operation = param;
                this.setState({
                    paramObj,
                    firstPageDisable: false,
                    selectRowIndex: 0
                })
                this.props.button.setDisabled({
                    first: false, pre: false,
                })
                break;
            case 'prePage':
                this.dataPage -=1;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                paramObj.operation = param;
                if(this.dataPage === dataLength-1){
                    this.setState({
                        lastPageDisable: false
                    })
                    this.props.button.setDisabled({
                        next: false, last: false
                    })
                }
                this.setState({
                    paramObj,
                    selectRowIndex: 0
                });
                break;
            case 'lastPage':
                this.dataPage = dataLength;
                stringPage = String(this.dataPage);
                paramObj.pageindex = stringPage;
                paramObj.operation = param;
                this.setState({
                    paramObj,
                    lastPageDisable: true,
                    firstPageDisable: false,
                    selectRowIndex: 0
                });
                this.props.button.setDisabled({
                    first: false, pre: false,
                    next: true, last: true
                })
                break;
        }
        this.queryData(this.state.paramObj);
        return;
    }
    queryClick(){//点击"查询"事件
        this.setState({
            showModal: !this.state.showModal
        })
    }
    queryData(param){//请求数据方法；
        this.props.button.setDisabled({
            print: false, directprint: false,
            templateOutput: false, saveformat: false,
        })
        let self = this;
        let url = '/nccloud/gl/accountrep/cashdiaryquery.do';
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, success } = response;
                if(success && data){
                    if(data.data.pagedata.length>0){
                        self.setState({
                            disabled:false,
                            typeDisabled: false,//账簿格式 默认是禁用的
                            selectRowIndex:0
                        })
                    }
                    if (Number(data.totalpage) > 1) {
                        if (Number(data.pageindex) == 1) {
                            self.setState({
                                firstPageDisable: true,
                                lastPageDisable: false
                            })
                            self.props.button.setDisabled({
                                first: true, pre: true,
                                next: false, last: false
                            })
                        } else if (Number(data.pageindex) == Number(data.totalpage)) {
                            self.setState({
                                firstPageDisable: false,
                                lastPageDisable: true
                            })
                            self.props.button.setDisabled({
                                first: false, pre: false,
                                next: true, last: true
                            })
                        } else {
                            self.setState({
                                firstPageDisable: false,
                                lastPageDisable: false
                            })
                            self.props.button.setDisabled({
                                first: false, pre: false,
                                next: false, last: false
                            })
                        }
                    } else {
                        self.setState({
                            lastPageDisable: true,
                            firstPageDisable: true
                        })
                        self.props.button.setDisabled({
                            first: true, pre: true,
                            next: true, last: true
                        })
                    }
                    self.setState({
                        dataWithPage: data,
                        selectRowIndex:0
                    })
                    self.dataPage = Number(data.pageindex);
                    let renderFirstData = {};
                    renderFirstData.column = data.column;
                    renderFirstData.data = data.data;
                    renderFirstData.amountcolumn = data.amountcolumn;
                    renderFirstData.quantityamountcolumn = data.quantityamountcolumn;
                    setData(self, renderFirstData);
                }else{
                    toast({content: this.state.json['20023082-000001'], color: 'danger'});/* 国际化处理： 请求数据为空！*/
                }
            },
            error:function(error){
                toast({content: error.message, color: 'danger'});
                throw error
            }
        })
    }
    modalSure(param){//点击弹框"确定"事件
        this.dataPage = 1;
        param.pageindex = String(this.dataPage);
        param.operation = 'nextPage';
        this.setState({
            paramObj: {...param},
            printParams: {...param}
        })
        this.queryData(param);
    }

    //直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    //保存列宽
    handleSaveColwidth=()=>{
        let info=this.refs.balanceTable.getReportInfo();
        let {json}=this.state
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(this.state.appcode,info.colWidths,json, callBack);
    }

    handleLoginBtn = (obj, btnName) => {
        if(btnName === 'print'){//打印
            this.showPrintModal()
        }else if(btnName === 'directprint'){//直接输出
            this.printExcel()
        }else if(btnName === 'templateOutput'){//模板输出
            this.showOutputModal()
        }else if(btnName === 'linkvoucher'){//联查凭证
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: relevanceSearch,
                key: 'relevanceSearch',
                appcode: '20023030'
            }, 'childmodules')
        }else if(btnName === 'saveformat'){//保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'first'){//首页
            this.handlePage('firstPage', this.state.dataWithPage)
        }else if(btnName === 'pre'){//上一页
            this.handlePage('prePage', this.state.dataWithPage)
        }else if(btnName === 'next'){//下一页
            this.handlePage('nextPage', this.state.dataWithPage)
        }else if(btnName === 'last') {//末页
            this.handlePage('lastPage', this.state.dataWithPage)
        }else if(btnName === 'refresh'){//刷新
            if (Object.keys(this.state.paramObj).length > 0) {
                this.modalSure(this.state.paramObj);
            }
        }
    }
    firstCallback = () => {//首页
        this.handlePage('firstPage', this.state.dataWithPage)
    }
    preCallBack = () => {//上一页
        this.handlePage('prePage', this.state.dataWithPage)
    }
    nextCallBack = () => {//下一页
        this.handlePage('nextPage', this.state.dataWithPage)
    }
    lastCallBack = () => {//末页
        this.handlePage('lastPage', this.state.dataWithPage)
    }
    render(){
        let { modal } = this.props;
        const { createModal } = modal;
        return(
            <div className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['20023082-000002']} /* 国际化处理： 银行日记账*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <QueryModal
                                    onConfirm={(datas) => {
                                        this.modalSure(datas);
                                    }}
                                />
                            </div>
                            {//按钮集合
                                this.props.button.createButtonApp({
                                    area: 'btnarea',
                                    buttonLimit: 3,
                                    onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName)
                                })
                            }
                        </div>
                    }
                    pageBtnContent = {
                        <PageButtonGroup
                            first={{disabled: this.state.firstPageDisable, callBack: this.firstCallback}}
                            pre={{disabled: this.state.firstPageDisable, callBack: this.preCallBack}}
                            next={{disabled: this.state.lastPageDisable, callBack: this.nextCallBack}}
                            last={{disabled: this.state.lastPageDisable, callBack: this.lastCallBack}}
                        />
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.bankAccount.map((items) => {
                                return(
                                    <HeadCom
                                        content={this.state.dataWithPage.data}
                                        lastThre = {this.state.dataWithPage && this.state.dataWithPage.headtitle}
                                        labels={items.title}
                                        key={items.title}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
                                        }
                                        accountType = {this.state.accountType}
                                        isLong = {items.styleClass}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data = {this.state.dataout}
                        onCellMouseDown = {(e, coord, td) => {
                            // this.handleSimpleTableClick(e, coord, td, 'bankAccount');
                            // this.rowBackgroundColor(e, coord, td)
                            this.whetherDetail('link','multiAccountSearch');
                            this.rowBackgroundColor(e, coord, td)
                        }}
                    />
                </div>

                <RepPrintModal
                    appcode = '20023035'  //日记账appcode,不可从路径中读取
                    visible={this.state.visible}
                    showScopeall={true}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={this.handleCancel.bind(this)}
                />
                <PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/accountrep/bankdiaryoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
                {createModal('printService', {
                    className: 'print-service'
                })}
                <Iframe />
            </div>
        )
    }
}


AssistPropertyBalance = createPage({})(AssistPropertyBalance)

ReactDOM.render(<AssistPropertyBalance />,document.querySelector('#app'));
