import { ajax, toast,cacheTools} from 'nc-lightapp-front';
import Utils from '../../../uap/public/utils';
import {quickTallyMeta} from './quickTally';
import {hintItem} from './batchHint';
import {printItem} from './print';
// import {voucherRelatedApp} from "../../public/components/oftenApi.js";
// import {voucher_link} from "../../public/components/constJSON";
import { openToVoucher } from '../../public/common/voucherUtils';
let tableId = 'gl_voucher';
export default function (props, tempAppcode='') {
    let searchId = `${this.state.appcode}query`;//appcode+query
    let pageId = `${this.state.appcode}PAGE`;//appcode+PAGE
    let appcode = tempAppcode ? tempAppcode : props.getUrlParam('c')||props.getSearchParam('c');


	// //会计平台联查
	// let checkedData=cacheTools.get('checkedData');	
	// if(checkedData){
	// 	let pks=checkedData.id/*['0001Z310000000030IPM','1001Z31000000000L9FR']*/;
	// 	if(pks&&pks.length==1){
	// 		let param = {
	// 			pk_voucher: pks[0]
	// 		}
	// 		openToVoucher(self,param)
	// 		// let voucherApp=voucherRelatedApp(voucher_link);
	// 		//   props.linkTo('/gl/gl_voucher/pages/voucher/index.html',{
	// 		// 	// c:self.props.getUrlParam('c'),
	// 		// 	 c:voucherApp.appcode/*'20020PREPA'*/,
	// 		// 	 pagecode:'20021005card',
	// 		// 	 status: 'browse',
	// 		// 	 id:pks[0]
	// 		//  })
	// 		return;
	// 	}
	// }

	// let excelimportconfig = Utils.getImportConfig("gl","C0", (resultinfo) => {
	// 	props.table.setAllTableData('importLogs', resultinfo);
	// 	props.modal.show('hintimportLog');
	// });
	return new Promise((resolve,reject)=>{
		props.createUIDom(
			{
				pagecode: pageId,//页面id
				appcode: appcode//小应用id
			},
			function (data) {
				if (data) {
					if (data.template) {
						let meta = data.template;
						// meta = modifierMeta(props, meta, searchId, appcode);
						// props.meta.oid=meta[searchId].oid;
						props.meta.setMeta(meta);					
						// if(props.ViewModel.getData('gl_voucher')){//生成凭证跳转赋值
						// 	props.table.setAllTableData(tableId, props.ViewModel.getData('gl_voucher')[tableId]);
						// }
						// // setDefaultValue(props,data.context, searchId);
					}
					// if (data.button) {
					// 	let button = data.button;
					// 	props.button.setButtons(button);
					// 	//props.button.setButtonVisible('edit', false);
					// 	props.button.setUploadConfig("import",excelimportconfig);
					// 	props.button.setPopContent('delete','确认要删除吗？')
					// }
				}
				resolve({'flag':true})
			}
		)
	})


	// if(checkedData){		
	// 	let pks=checkedData.id;
	// 	if(pks&&pks.length>1){
	// 		ajax({
	// 			url: '/nccloud/gl/voucher/query.do',
	// 			data: {
	// 				pk_voucher:pks,
	// 				voucherStatus:checkedData.voucherStatus,
	// 				type:'list'
	// 			},
	// 			success: (res) => {
	// 				let { success, data } = res;
	// 				if (success) {
	// 					if(data){
	// 						props.table.setAllTableData(tableId, data[tableId]);
	// 					}else{
	// 						props.table.setAllTableData(tableId, {rows:[]});
	// 					}
						
	// 				}
	// 			}
	// 		});
	// 	}
	// }

	//从卡片跳回列表之后，根据之前的查询条件重新查询
	let data=cacheTools.get("VoucherSearchData");
	if(data){
		ajax({
			url: '/nccloud/gl/voucher/voucherQuery.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if(data){
						props.table.setAllTableData(tableId, data[tableId]);
					}else{
						props.table.setAllTableData(tableId, {rows:[]});
					}
					
				}
			}
		});
	}

}

function setDefaultValue(props, context, searchId) {
	let oldaccountingbook = props.search.getSearchValByField(searchId, 'pk_accountingbook');
	if (context.defaultAccbookPk) {
		if (!oldaccountingbook || !oldaccountingbook.value.firstvalue || oldaccountingbook.value.firstvalue == '') {
			let pk_accountingbook = {
				display: context.defaultAccbookName,
				value: context.defaultAccbookPk
			}
            props.search.setSearchValByField(searchId, 'pk_accountingbook', pk_accountingbook);
			// props.form.setFormItemsValue('quickTally', {pk_accountingbook:pk_accountingbook});
			//设置默认的会计期间
			ajax({
				url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
				data: {
					pk_accountingbook:pk_accountingbook.value
				},
				success: (res) => {					
					let { success, data } = res;
					 if (success) {
						let period=data.bizPeriod
						// props.form.setFormItemsValue('quickTally', {period:{display:period,value:period}});
					 }
				}
			});
		}
	}
}


function modifierMeta(props, meta, searchId, appcode) {
	if(props.getUrlParam("type")=='check')
		meta['scan'].status='edit';

	// let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		item.visible = true;
		item.col = '3';
		if (item.attrcode == 'pk_accountingbook') {
			//item.initialvalue={};
			item.queryCondition = () => {
				return {
					"TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
					"appcode": appcode
				}
			}
		}
		return item;
	})

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		return item;
	});

	let material_event = {
		label: '操作',
		itemtype: 'customer',
		attrcode: 'opr',
		width: '150px',
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let buttonAry = getOprButtonArray(props, record, index);
			return props.button.createOprationButton(buttonAry, {
				area: "gl_voucher_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => oprButtonClick(props, key, record, index)
			});
		}
	};
	meta[tableId].items.push(material_event);
	meta['quickTally']=quickTallyMeta(appcode);//快速记账
	meta['batchhint']=hintItem();//批量提示
	meta['print']=printItem(appcode);//批量提示
	return meta;
}

function getOprButtonArray(props, record, index) {
	//可以放在操作列上的按钮
	let btnArry =[];
	if (record.discardflag && record.discardflag.value) {
		btnArry.push('unabandon');
	} else {
		btnArry.push('edit');
		btnArry.push('abandon');
	}

	if (record.pk_casher && record.pk_casher.value && record.pk_casher.value!='~') {
		btnArry.push('unsign');
	} else {
		btnArry.push('sign');
	}

	if (record.pk_checked && record.pk_checked.value && record.pk_checked.value!='~') {
		btnArry.push('uncheck');
	} else {
		btnArry.push('check');
	}

	if (record.pk_manager && record.pk_manager.value && record.pk_manager.value!='N/A') {
		btnArry.push('untally');
	} else {
		btnArry.push('tally');
	}

	btnArry.push('delete');
	btnArry.push('printvoucher');
	return btnArry;
}

function oprButtonClick(props, key, record, index) {
	switch (key) {
		case 'edit':
			props.linkTo('/gl/gl_voucher/pages/voucher/index.html' + window.location.search, {
				status: 'edit',
				pagecode:'20021005card',
				id: record.pk_voucher.value
			});
			break;
		case 'printvoucher':
			props.ViewModel.setData('selectVoucher',[record.pk_voucher.value]);
            props.modal.show('print');
			break;
		default:
			operate(props, record, index, key);
			break;
	}
}

function operate(props, record, index, key) {
	ajax({
		url: '/nccloud/gl/voucher/' + key + '.do',
		data: {
			pk_voucher: [record.pk_voucher.value],
			ts: record.ts.value,
			type:'list'
		},
		async: true,
		success: (res) => {
			if(key!='delete'){
				if (res.success) {
					let {error,success} =res.data;
					if (error && error.length > 0) {//将错误信息返回
						toast({ content: res.data.error, color: 'danger' })
					} else {
						if (key == 'delete') {							
							props.table.deleteTableRowsByIndex(tableId, index);
						}
						dealSuccess(props,key,index,success['gl_voucher'].rows[0].values);
					}
				}
			}else{				
				props.table.deleteTableRowsByIndex(tableId, index);
				if(res.data&&res.data.warn&&res.data.warn.length > 0){
					toast({ content: res.data.warn, color: 'warning'})
				}else
					toast({ content: "删除成功", color: 'success' })
			}
		},
		error: (res) => {
			toast({ content: res.message, color: 'danger' })
			
		}
	});
}


