import { ajax,print,toast } from 'nc-lightapp-front';

let tableid = 'gl_voucher';
function printItem(props) {
    //请求打印模板  
    //let printTemp=printTemplate("20020PREPA");

    let printItem=[
        {
            itemName: '科目汇总级次', itemType: 'select', itemKey: 'printsubjlevel',
            itemChild: [
                {label: '不汇总', value: '0'}, 
                {label: '1级',value: '1'}, 
                {label: '2级',value: '2'}, 
                {label: '3级',value: '3'}, 
                {label: '4级',value: '4'}, 
                {label: '5级',value: '5'}, 
                {label: '6级',value: '6'}, 
                {label: '7级',value: '7'}, 
                {label: '8级',value: '8'}, 
                {label: '按科目设置',value: '9'}]
        },
        {itemName:'按辅助项汇总',itemType:'checkbox',itemKey:'printasslevel',
            itemChild: [{label: '按辅助项汇总',checked: false}]},
        {itemName:'代账单格式',itemType:'checkbox',itemKey:'printmode',
            itemChild: [{label: '代账单格式',checked: false}]},
        {itemName:'选择模板',itemType:'refer',itemKey:'printemp',isMultiSelectedEnabled:false,
        config:{refCode:'/gl/refer/voucher/PrintTemplateGridRef'},appcode:"20020PREPA"},
    ];
    return printItem;
}

function confirmPrint(selectIndex,listItem,selectVoucher){
    if(listItem['printmode'].value=='Y'&&selectIndex=='-1'){
        toast({ content: '请选择需要打印的分录！', color: 'danger'});
        return;
    }

    if(selectVoucher.length==0){
        toast({ content: '请选择需要打印的凭证！', color: 'danger'});
        return;
    }

        
    let printdata={
        printsubjlevel:listItem['printsubjlevel'].value, //printsubjlevel：科目汇总到第几级，不汇总是0
        printasslevel:listItem['printasslevel'].value=='Y'?'-1':'0', //printasslevel：按辅助项汇总 勾选-1 不勾选0
        printmode:listItem['printmode'].value=='Y'?'1':'0', //printmode：代账单格式 0不勾选 1勾选
        selectIndex:selectIndex //selectIndex: 选中第几行分录 从0开始
    }
   
    print(
        'html',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
        '/nccloud/gl/voucher/printvoucher.do', //后台服务url
        {
            billtype: 'C0',  //单据类型
            appcode: '20020PREPA',      //功能节点编码，即模板编码
            nodekey: listItem['printemp'].value,     //模板节点标识
            oids: selectVoucher,    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
            userjson:JSON.stringify(printdata)
            
        }
      );
}
export {printItem,confirmPrint}