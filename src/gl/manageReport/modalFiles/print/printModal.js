import React, { Component } from 'react';
import {high,base,ajax} from 'nc-lightapp-front';

import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../../public/components/FormItems';
const { Refer} = high;
import { toast } from '../../../../public/components/utils.js';
import createScript from '../../../../public/components/uapRefer.js';

const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm
} = base;
import '../index.less';
const {  NCFormItem:FormItem } = NCForm;

const dateInputPlaceholder = '选择日期';


  const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
  };


export default class PrintModal extends Component {
    constructor(props) {
        super(props);
       
        this.state = {
            showModal:false,
            isShowUnit:false,//是否显示业务单元
            assData: [//辅助核算信息
            ],
            loadData:[],//查询模板加载数据
            listItem:{},//模板数据对应值
            checkedAll:false,
            checkedArray: [
                false,
                false,
                false,
            ],
           
        };
        this.close = this.close.bind(this);
    }

    componentWillReceiveProps (nextProp) {
        let {loadData,showOrHide}=this.props;
        let self=this;
        let { listItem,showModal }=self.state
        if (nextProp.loadData !== this.state.loadData ) {
            loadData.forEach((item,i)=>{
                let  key;
                if(item.itemType=='refer'){
                    if(item.itemKey=='pk_accasoa'||item.itemKey=='pk_units'){
                        key=[{
                            display:'',
                            value:''
                        }]
                    }else{
                        key={
                            display:'',
                            value:''
                        }
                    } 
                }else if(item.itemType=='select'||item.itemType=='Dbselect'||item.itemType=='radio'){//下拉框赋初始值
                    key={
                        value:item.itemChild.length>0?item.itemChild[0].value:''
                    }
                }else{
                    key={
                        value:''
                    }
                    if(item.itemType=='date'||item.itemType=='Dbdate'){
                        key={
                        //    value:moment().format("YYYY-MM-DD")
                        value:''
                        }
                    }
                }
                if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'){
                item.itemKey.map((k,index)=>{
                    let name= k;
                    listItem[name]=key
                });
                }else{
                    let name= item.itemKey;
                    listItem[name]=key
                }
            
            })           
            self.setState({
                loadData:loadData,
                showModal:showOrHide,
                listItem
            })
        }else{
            self.setState({
                showModal:showOrHide,
            })
        }
    }

    //表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

    close() {
        this.props.handleClose();
    }

    confirm=()=>{
        let { listItem,assData } =this.state;
        listItem.ass=assData;
        this.props.onConfirm(listItem);
    }

    queryList=(data)=>{
        let self=this;
        function getNowFormatDate() {
            let date = new Date();
            let seperator1 = "-";
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            let currentdate = year + seperator1 + month + seperator1 + strDate;
            return currentdate;
        }
        let currrentDate = getNowFormatDate();
        let { listItem,isShowUnit,assData } =self.state;
			return data.length!=0?(
				data.map((item, i) => {                   
                    switch (item.itemType) {
                        case 'refer':
                            let referUrl = item.config.refCode + '/index.js';
                            let DBValue = [];
                            let defaultValue = {}
                            if (listItem[item.itemKey].length) {
                                listItem[item.itemKey].map((item, index) => {
                                    DBValue[index] = { refname: item.display, refpk: item.value };
                                })
                            } else {
                                defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };
                            }
                            if (!self.state[item.itemKey]) {
                                { createScript.call(self, referUrl, item.itemKey) }
                                return <div />
                            } else {

                                return (
                                    <FormItem
                                        inline={true}
                                        showMast={true}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={10} md={10} sm={10}
                                        labelName={item.itemName}
                                        isRequire={true}
                                        method="change"
                                    >
                                        {self.state[item.itemKey] ? (self.state[item.itemKey])(
                                            {
                                                value: defaultValue,
                                                //isMultiSelectedEnabled:true,
                                                queryCondition: () => {
                                                    return {
                                                        appcode:item.appcode
                                                        //"pk_accountingbook": self.state.pk_accountingbook.value
                                                    }
                                                },
                                                onChange: (v) => {
                                                    listItem[item.itemKey].value = v.refcode
                                                    listItem[item.itemKey].display = v.refname
                                                    this.setState({
                                                        listItem
                                                    })
                                                }
                                            }
                                        ) : <div />}
                                    </FormItem>);
                            }


                            break;

                        case 'date':
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                    method="blur"
                                    inputAlfer="%"
                                    errorMessage="输入格式错误"
                                >
                                    <DatePicker
                                        name={item.itemKey}
                                        type="customer"
                                        isRequire={true}
                                        //format={format}
                                        //disabled={isChange}
                                        //value={moment(listItem[item.itemKey].value)}
                                        //locale={zhCN}
                                        value={listItem[item.itemKey].value}
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v;
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    />
                                </FormItem>
                            );
                        case 'Dbdate':
                            return (

                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10} md={10} sm={10}
                                    labelName={item.itemName}
                                    method="blur"
                                    inputAlfer="%"
                                    errorMessage="输入格式错误"
                                    inputAfter={
                                        <Col>
                                            <span className="online">&nbsp;--&nbsp;</span>
                                            <DatePicker
                                                name={item.itemKey}
                                                type="customer"
                                                isRequire={true}

                                                value={listItem.endDate.value}
                                                onChange={(v) => {
                                                    listItem.endDate = { value: v }
                                                    this.setState({
                                                        listItem
                                                    })
                                                }}
                                                placeholder={dateInputPlaceholder}
                                            /></Col>}
                                >
                                    <DatePicker
                                        name={item.itemKey}
                                        type="customer"
                                        isRequire={true}
                                        //format={format}
                                        //disabled={isChange}
                                        // value={moment(listItem.begin_date.value)}
                                        // locale={zhCN}
                                        value={listItem.beginDate.value}
                                        onChange={(v) => {
                                            listItem.beginDate = { value: v }
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    />
                                </FormItem>
                                // </Col>
                                // </Row>
                            );
                        case 'textInput':
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                    <FormControl
                                        value={listItem[item.itemKey].value}
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            );
                        case 'DbtextInput':
                            return (
                                // <Row>
                                //     <Col xs={8}  md={8} sm={8} className="dateMargin labelMargin">
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10} md={10} sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                    inputAfter={
                                        <Col xs={12} md={12} sm={12}>
                                            <span className="online">&nbsp;--&nbsp;</span>
                                            <FormControl
                                                value={listItem[item.itemKey[1]].value}
                                                //disabled={true}
                                                // onFocus={this.focus}
                                                onChange={(v) => {
                                                    let startkey = item.itemKey[0];
                                                    let endkey = item.itemKey[1];
                                                    // listItem[item.itemKey].value = v
                                                    listItem[endkey] = { value: v }
                                                    this.setState({
                                                        listItem
                                                    })
                                                }}
                                            />
                                        </Col>}
                                >
                                    <FormControl
                                        value={listItem[item.itemKey[0]].value}
                                        //disabled={true}
                                        // onFocus={this.focus}
                                        onChange={(v) => {
                                            listItem[item.itemKey[0]] = { value: v }
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                                //     </Col>                                
                                // </Row>
                            );
                        case 'radio':
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                //  change={self.handleGTypeChange.bind(this, 'contracttype')}
                                >
                                    <RadioItem
                                        name={item.itemKey}
                                        type="customer"
                                        defaultValue={item.itemChild[0].value ? item.itemChild[0].value : ''}
                                        items={() => {
                                            return (item.itemChild)
                                        }}
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            )
                        case 'select':
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                    <SelectItem name={item.itemKey}
                                        //defaultValue={item.itemChild[0].value ? item.itemChild[0].value : ''}
                                        items={
                                            () => {
                                                return (item.itemChild)
                                            }
                                        }
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>)
                        case 'Dbselect':
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={2}
                                    md={2}
                                    sm={2}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                    <SelectItem name={item.itemKey}
                                        defaultValue={item.itemChild[0].value ? item.itemChild[0].value : ''}
                                        items={
                                            () => {
                                                return (item.itemChild)
                                            }
                                        }
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>)
                        case 'checkbox':
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    xs={12} md={12} sm={12}
                                    // labelName={item.itemName}
                                    // isRequire={true}
                                    className='checkboxStyle'
                                    method="change"
                                >
                                    <CheckboxItem name={item.itemKey}
                                        defaultValue={item.itemChild[0].value} 
                                        boxs={
                                            () => {
                                                return (item.itemChild)
                                            }
                                        }
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = (v[0].checked == true) ? 'Y' : 'N';
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            )
                        default:
                            break;
                   }
               })
			):<div />;
           
    }
 
      render() {
        let{showOrHide}=this.props;
        let { loadData,assData} =this.state;
        return (
            <div className="fl">
                <Modal
                    className={'msg-modal'}
                    show={showOrHide }
                    onHide={ this.close }
                    animation={true}
                    >
                    <Modal.Header closeButton>
                        <Modal.Title ></Modal.Title>
                    </Modal.Header >
                    <Modal.Body >
                        <NCForm useRow={true}
                            submitAreaClassName='classArea'
                            showSubmit={false}　>
                            {this.queryList(loadData)}
                        </NCForm>      
                    </Modal.Body>
                    <Modal.Footer>
                        <Button colors="primary" onClick={ this.confirm }> 确定 </Button>
                        <Button onClick={ this.close }> 关闭 </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
PrintModal.defaultProps = defaultProps12;