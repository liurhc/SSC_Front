import { print,toast } from 'nc-lightapp-front';
/**
 * 打印
 */
let areacode='print';
function printItem(appcode){
    return (
        {
            areastatus:'edit',
            code:'print',
            moduletype:'form',
            name:'打印',
            status:'edit',
            items:[
                {
                    attrcode: 'printsubjlevel',
                    colnum: '1',
                    color: '#6E6E77',
                    isDataPowerEnable: true,
                    itemtype: 'select',
                    label: '科目汇总级次',
                    maxlength: '20',
                    position: '1',
                    visible: true,
                    options: [
                        { display: '不汇总', value: '0' },
                        { display: '1级', value: '1' },
                        { display: '2级', value: '2' },
                        { display: '3级', value: '3' },
                        { display: '4级', value: '4' },
                        { display: '5级', value: '5' },
                        { display: '6级', value: '6' },
                        { display: '7级', value: '7' },
                        { display: '8级', value: '8' },
                        { display: '按科目设置', value: '9' }
                    ],
                    initialvalue:{display: '不汇总', value: '0'}
                },
                {
                    attrcode:'printasslevel',
                    colnum:'1',
                    color:'#6E6E77',
                    isnextrow:true,
                    isDataPowerEnable:true,
                    itemtype:'checkbox_switch',
                    label:'按辅助项汇总',
                    maxlength:'20',
                    position:'2',
                    visible:true
                },
                {
                    attrcode:'printmode',
                    colnum:'1',
                    color:'#6E6E77',
                    isnextrow:true,
                    isDataPowerEnable:true,
                    itemtype:'checkbox_switch',
                    label:'代账单格式',
                    maxlength:'20',
                    position:'2',
                    visible:true
                },
                {
                    attrcode:'printemp',
                    colnum:'1',
                    color:'#6E6E77',
                    isnextrow:true,
                    required:true,
                    isDataPowerEnable:true,
                    itemtype:'refer',
                    label:'选择模板',
                    maxlength:'20',
                    position:'1',
                    visible:true,
                    refcode:"gl/refer/voucher/PrintTemplateGridRef/index",
                    queryCondition: () => {
                        return {
                            appcode:appcode
                        }
                    },
                }
            ]
        }
    )
}

function createPrint(_this){
    let { form } = _this.props;
    let { createForm } = form;
    return (
        <div className="print">
            {createForm(areacode, {
                onAfterEvent: printAfterEvent.bind(this)
            })}
        </div>
    )
}

function confirmPrint(selectIndex){
    let printmode=this.props.form.getFormItemsValue(areacode,'printmode').value
    if(printmode&&selectIndex=='-1'){
        toast({ content: '请选择需要打印的分录！', color: 'warning'});
        return;
    }
    let selectVoucher=this.props.ViewModel.getData('selectVoucher');
    if(selectVoucher.length==0){
        toast({ content: '请选择需要打印的凭证！', color: 'warning'});
        return;
    }
    let printemp =this.props.form.getFormItemsValue(areacode,'printemp').value;
    if(printemp==''){
        toast({ content: '请选择模板！', color: 'warning'});
        return;
    }
    let printsubjlevel=this.props.form.getFormItemsValue(areacode,'printsubjlevel').value;   
    let printasslevel=this.props.form.getFormItemsValue(areacode,'printasslevel').value; 
    let printdata={
        printsubjlevel:printsubjlevel, //printsubjlevel：科目汇总到第几级，不汇总是0
        printasslevel:printasslevel?'-1':'0', //printasslevel：按辅助项汇总 勾选-1 不勾选0
        printmode:printmode?'1':'0', //printmode：代账单格式 0不勾选 1勾选
        selectIndex:selectIndex //selectIndex: 选中第几行分录 从0开始
    }
   
    print(
        'html',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
        '/nccloud/gl/voucher/printvoucher.do', //后台服务url
        {
            billtype: 'C0',  //单据类型
            appcode: '20020PREPA',      //功能节点编码，即模板编码
            nodekey: printemp,     //模板节点标识
            oids: selectVoucher,    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
            userjson:JSON.stringify(printdata)            
        }
      );   
      this.props.modal.close('print'); 
}

function printAfterEvent(props,areacode,key,finalvalue,oldvalue,val){
    if('printemp'==key){
        let value=val.refcode;
        let display=finalvalue.display;
        props.form.setFormItemsValue(areacode,{'printemp':{display:display,value:value}})
    }
}

export{printItem,createPrint,confirmPrint}