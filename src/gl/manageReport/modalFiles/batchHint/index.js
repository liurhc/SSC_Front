/**
 * 列表批量提示框
 */
let areacode='batchhint';

function hintItem(){
    return{
        code:areacode,
        moduletype:'table',
        name:'批量提示',
        pagination:false,
        status:'browse',
        items:[
            {
                "position": "1",
                "visible": true,
                "metadataProperty": "uap.accountingbook.name",
                "maxlength": "20",
                "refcode": "uapbd/refer/org/AccountBookTreeRef/index",
                "itemtype": "refer",
                "isDataPowerEnable": true,
                "label": "财务核算账簿",
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "pk_accountingbook"
            },
            {
                "position": "2",
                "visible": true,
                "maxlength": "20",
                "itemtype": "datepicker",
                "isDataPowerEnable": true,
                "label": "制单日期",
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "prepareddate"
            },
            {//凭证类别
                "position": "1",
                "visible": true,
                "metadataProperty": "uap.vouchertype.name",
                "maxlength": "20",
                "refcode": "uapbd/refer/fiacc/VoucherTypeDefaultGridRef/index",
                "itemtype": "refer",
                "isDataPowerEnable": true,
                "label": "凭证类别",
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "pk_vouchertype"
            },
            {
                "position": "3",
                "visible": true,
                "maxlength": "20",
                "itemtype": "number",
                "isDataPowerEnable": true,
                "label": "凭证号",
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "num"
            },
            {
                "position": "4",
                "visible": true,
                "maxlength": "20",
                "itemtype": "input",
                "isDataPowerEnable": true,
                "label": "摘要",
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "explanation"
            },
            {
                "position": "4",
                "visible": true,
                "maxlength": "20",
                "itemtype": "input",
                "isDataPowerEnable": true,
                "label": "结果",
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "result"
            },
            {
                "position": "4",
                "visible": true,
                "maxlength": "20",
                "itemtype": "input",
                "isDataPowerEnable": true,
                "label": "原因",
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "reason"
            }
        ]
    }    
}

function createHint(_this){
    let { table } = _this.props;
    let { createSimpleTable } = table;
    return(
        <div className="nc-bill-table-area">
            {createSimpleTable(areacode, {})}
        </div>
    )
}

export {hintItem,createHint}