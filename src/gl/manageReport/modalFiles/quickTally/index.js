import {ajax,toast } from 'nc-lightapp-front';

/**
 * 快速记账
 */
let areacode='quickTally';
function quickTallyMeta(appcode){
    return (
        {
            areastatus:'edit',
            code:'quickTally',
            moduletype:'form',
            name:'快速记账',
            status:'edit',
            items:[
                {
                    attrcode:'pk_accountingbook',
                    colnum:'1',
                    color:'#6E6E77',
                    isDataPowerEnable:true,
                    itemtype:'refer',
                    label:'财务核算账簿',
                    maxlength:'20',
                    position:'1',
                    visible:true,
                    refcode:"uapbd/refer/org/AccountBookTreeRef/index",
                    metadataProperty:"uap.accountingbook.name",
                    queryCondition:() => {
                        return {
                        "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                        "appcode":appcode
                        }
                    }
                },
                {
                    attrcode:'period',
                    colnum:'1',
                    color:'#6E6E77',
                    isDataPowerEnable:true,
                    itemtype:'refer',
                    label:'会计期间',
                    maxlength:'20',
                    position:'1',
                    visible:true,
                    refcode:"uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index"
                }
            ]
        }
    )
}
function quicktallyItem(_this){
    let { form } = _this.props;
    let { createForm } = form;
    return (
        <div className="quickTally">
            {createForm(areacode, {
                onAfterEvent: onAfterEvent.bind(this)
            })}
        </div>
    )
}

function onAfterEvent(props, areaId, itemId, finalValue, oldValue, val){
    if("pk_accountingbook"==itemId){
        let pk_accountingbook=finalValue.value;
        if(pk_accountingbook&&pk_accountingbook!=''){
            ajax({
				url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
				data: {
					pk_accountingbook:pk_accountingbook
				},
				success: (res) => {					
					let { success, data } = res;
					 if (success) {
						let period=data.bizPeriod
                        props.form.setFormItemsValue(areacode, {'period':{display:period,value:period}});  
                        
                        //设置会计期间参照过滤
                        let pk_accperiodscheme=data.pk_accperiodscheme;
                        let meta=props.meta.getMeta();
                        meta[areaId].items = meta[areaId].items.map((item, key) => {                            
                            if (item.attrcode == 'period') {
                                item.queryCondition = () => {
                                    return {
                                        "pk_accperiodscheme":pk_accperiodscheme,
                                        "GridRefActionExt":'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
                                    }
                                }
                            }
                        })
					}
				}
			});
        }else{
            props.form.setFormItemsValue(areacode, {'period':{display:'',value:''}});
        }
    }
}

function quicktallyComfirm(){
    let pk_accountingbook=this.props.form.getFormItemsValue(areacode,'pk_accountingbook').value;
    let yearmth=this.props.form.getFormItemsValue(areacode,'period').value;
    if(!pk_accountingbook||pk_accountingbook==''||!yearmth||yearmth==''){
        toast({ content: "请选择账簿和期间！", color: 'warning'});
        return;
    }
    ajax({
        url: '/nccloud/gl/voucher/quicktally.do',
        data: {
            "pk_accountingbook":pk_accountingbook,
            "year":yearmth.split('-')[0],
            "period":yearmth.split('-')[1]
        },
        success: (res) => {
            let { success} = res;
            if (success) {
                toast({ content: "快速记账成功！", color: 'success', position: 'bottom' });
            }
        }
    });
}

export {quicktallyItem,quicktallyComfirm,quickTallyMeta}