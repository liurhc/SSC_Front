/**
 *   Created by Liqiankun on 2018/7/21
 */


export const checkAccountReferUrl = 'uapbd/refer/org/AccountBookTreeRef/index.js';//核算账簿参照路径

export const subjectRefer  = 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js';//科目参照路径

export const relevanceSearch = '/gl/gl_voucher/pages/main/index.html#/Welcome';//联查凭证跳转地址

//【辅助余额表】【辅助明细账】(needaccount:Y)/【现金日记账】【银行日记账】【科目辅助余额表】(needaccount:N)的查询辅助参照接口
export const assistReferYOrNUrl = '/nccloud/gl/accountrep/assbalancequeryobject.do';

export const multiassistreferUrl = '/nccloud/gl/accountrep/multianalysisqueryobject.do';//【多维分析表】的辅助查询

export const assistPropertyBalanceUrl = '/nccloud/gl/accountrep/assattqueryobject.do';//辅助属性余额表的辅助查询

export const journalReferUrl = '/nccloud/gl/voucher/queryAllAssItem.do';//序时账请求辅助参照接口

export const currType = 'uapbd/refer/pubinfo/CurrtypeGridRef/index.js';//币种参照

export const abstructReferUrl = 'fipub/ref/pub/SummaryRef/index.js';//摘要参照路径

export const transferUrl = '/nccloud/gl/accountrep/queryaccount.do';//科目组件中的"连查"按钮需要的接口


//跳转时目标节点的路径
//节点对应的appcode值
export const journalUrl = '/gl/manageReport/journal/content/index.html';//序时账的路径
export const journalAppcode = '20023040';//序时账的appcode值


//现金流量明细查询
export const cashFlowDetailSearch = '/gl/cashQuery/pages/detailNode/';//src/gl/cashQuery/pages/detailNode/index.js
export const cashFlowDetailSearchCode = '20020CFDQRY';

export const cashFlowAnalyTable = '/gl/cashQuery/pages/analysisNode/';//现金流量分析表
export const cashFlowAnalyTableCode = '20021CFRPT';