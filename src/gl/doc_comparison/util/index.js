import { toast ,base, createPage, viewModel } from 'nc-lightapp-front';
const { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel

export function getParam(url, name) {
	var pattern = new RegExp('[?&]' + name + '=([^&]+)', 'g');
	var matcher = pattern.exec(url);
	var items = null;
	if (null != matcher) {
		try {
			items = decodeURIComponent(decodeURIComponent(matcher[1]));
		} catch (e) {
			try {
				items = decodeURIComponent(matcher[1]);
			} catch (e) {
				items = matcher[1];
			}
		}
	}
	return items;
}

export function linkTo(url, data) {
	setGlobalStorage('sessionStorage', 'docu_compare_card', JSON.stringify(data));	
	window.location.href = url;
}
