import React, { Component } from 'react';
import { base } from 'nc-lightapp-front';
import ReferLoader from '../../public/ReferLoader/index';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCNumber, NCForm
} = base;
const {  NCFormItem:FormItem } = NCForm;
import {SelectItem} from '../../public/components/FormItems';

/**
 * 加载辅助核算参照 科目交叉校验规则和基础档案对照专用 不进行数据权限过滤
 * @param {*} self 
 * @param {*} json 
 * @param {*} key 
 * @param {*} condition 
 * @param {*} defaultValue 
 * @param {*} props 
 */
export default function drawingRef(self, json, key, condition, defaultValue, props){
    let {refcode, pk_org, pk_defdoclist, pk_accountingbook, date, pk_accasoa, classid, isShowUnit, showDisableData } = condition;
    showDisableData = typeof showDisableData === 'boolean' ? showDisableData : false;
    isShowUnit = typeof isShowUnit === 'boolean' ? isShowUnit : false;
    if(refcode){
        let config={
            "pk_org": pk_org,
            "pk_defdoclist": pk_defdoclist,
            "pk_accountingbook": pk_accountingbook,
            "date": date,
            "pk_accasoa": pk_accasoa,
            "classid": classid,
        }

        if(classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9' /* 组织_部门 */
            ||classid==='66ed0cf6-e260-4f39-8fbb-172260efd677' /* 组织_部门版本信息 */
            ||classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'
            ){//部门，人员
            return (
                <ReferLoader
                    tag={key}
                    refcode={refcode}
                    value={defaultValue}
                    isShowUnit={isShowUnit}
                    unitProps={{
                        refType: 'tree',
                        refName: json['200019docmap-000049'],/* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:json['200019docmap-000049'],refpk:'root'},/* 国际化处理： 业务单元*/
                        placeholder:json['200019docmap-000049'],/* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[json['200019docmap-000015'], json['200019docmap-000016']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false
                    }}
                    unitCondition={{
                        pk_financeorg:pk_org,
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    }}
                    isMultiSelectedEnabled={false}
                    isShowDisabledData={showDisableData}
                    unitValueIsNeeded={false}
                    isShowDimission={true}
                    queryCondition={(obj) => {
                        /**
                         * 只有期初和凭证适用科目交叉校验规则
                         */
                        if(classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f'){
                            //显示离职人员
                            config.isShowDimission=true;
                        }
                        if(classid&&classid.length==20){//classid的长度大于20的话过滤条件再加一个pk_defdoclist
                            return config
                        }else{
                            config.busifuncode="all";
                            return config
                        }   
                    }}
                    {...props}
                    />
                )
        }else{
            return (
                <ReferLoader
                tag={key}
                refcode={refcode}
                value={defaultValue}
                isMultiSelectedEnabled={false}
                isShowDisabledData={showDisableData}
                queryCondition={(obj) => {
                    return config 
                }}
                {...props}
            />)

        }  
    }else{//不是参照的话要区分日期、字符、数值
        if(classid=='BS000010000100001033'){//日期
            return(
                <DatePicker
                    type="customer"
                    isRequire={true}
                    value={defaultValue.refname}
                    {...props}
                    />
            )
        }else if(classid=='BS000010000100001031'){//数值
            return(
                <NCNumber
                    scale={2}
                    value={defaultValue.refname}
                    placeholder={json['200019docmap-000050']}/* 国际化处理： 请输入数字*/
                    {...props}
                />
            )
        }else if(classid=='BS000010000100001032'){//布尔
            return(
                    <FormItem
                    inline={true}
                    // showMast={false}
                    labelXs={2}
                    labelSm={2}
                    labelMd={2}
                    xs={10}
                    md={10}
                    sm={10}
                    // labelName={item.itemName}
                    isRequire={true}
                    method="change"
                >
                    <SelectItem 
                        defaultValue={defaultValue.refname} 
                        items = {
                            () => {
                                return ([{
                                    label: json['200019docmap-000051'],/* 国际化处理： 是*/
                                    value: 'Y'
                                }, {
                                    label: json['200019docmap-000052'],/* 国际化处理： 否*/
                                    value: 'N'
                                }]) 
                            }
                        }
                        {...props}
                    />
                </FormItem>
            )
        }else{//字符
            return(
                <FormControl
                    value={defaultValue.refname}
                    {...props}
                />
            )
        }
        
    }
} 
