import React, { Component } from 'react';
import { createPage, ajax, toast, getMultiLang, viewModel } from 'nc-lightapp-front';
import ListHeader from './components/header';
import ListBody from './components/body';
import initTemplate from './event/initTemplate';
import './index.less';
const { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel
class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			action: '',
			srcOrg: {}, //来源组织
			destOrg: {}, //目的组织
			tableData: [], //表格数据
			srcAccountingbook: '', //来源组织核算账簿
			desAccountingbook: '', //目的组织核算账簿
			json: {}
		};
		this.appcode = this.props.getSearchParam('c');
	}
	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			},()=>{		  
				initTemplate.call(this, this.props);
			})
		}
		getMultiLang({moduleId:'200019docmap',domainName:'gl',currentLocale:'simpchn',callback});
	}
	componentDidMount() {
		const docu_compare_card = JSON.parse(getGlobalStorage('sessionStorage', 'docu_compare_card'));
		if (docu_compare_card) {
			const { srcOrg, destOrg, srcAccountingbook, desAccountingbook } = docu_compare_card;
			this.setState({ srcOrg, destOrg, srcAccountingbook, desAccountingbook });
			let data = {
				pk_desorgbook: destOrg.refpk ? destOrg.refpk : '',
				pk_srcorgbook: srcOrg.refpk ? srcOrg.refpk : ''
			};
			this.searchData(data);
		}
	}
	handleOrgData = (key, val) => {
		this.setState({ [key]: val });
		if(val && val.refpk){
			this.qryMainOrgAccBook(key, val)
		}
	};
	//查询主组织核算账簿
	qryMainOrgAccBook = (key, val) => {
		ajax({
			url: '/nccloud/gl/glpub/qryMainOrgAccBook.do',
			data: { pk_org: val.refpk },
			success: (res) => {
				let pk_accountingbook = key === 'srcOrg' ? 'srcAccountingbook' : 'desAccountingbook';
				let {success, data} = res
				if (success) {
					if (data) {	
						this.setState({ 
							[pk_accountingbook]: data.pk_accountingbook 
						});
					} else {

					}
				}
			},
			error: (res) => {
				// 覆盖平台错误信息提示
			}
		});
	}
	deleteAction = (message) => {
		this.setState({
			action: message
		});
	};
	// 清空查询区，原需求是清空Table区，后改为清空查询区，方法名未改
	clearTableData = () =>{
		this.setState({ 
			// tableData: [] 
			srcOrg: {}, //来源组织
			destOrg: {}, //目的组织
			srcAccountingbook: '', //来源组织核算账簿
			desAccountingbook: '', //目的组织核算账簿
		})
	}
	//点击search之后的回调
	searchData = (data) => {
		if (!data || data.pk_desorgbook == '' || data.pk_srcorgbook == '') {
			toast({ color: 'warning', content: this.state.json['200019docmap-000044'] });/* 国际化处理： 请选择来源组织和目的组织！*/
			return;
		}
		ajax({
			url: '/nccloud/gl/glpub/queryDocmaptemplate.do',
			data,
			type: 'POST',
			success: (res) => {
				let newData = [];
				let {success, data} = res
				if (success) {
					if (data) {
						toast({ content: `${this.state.json['200019docmap-000046']}，${this.state.json['200019docmap-000047']} ${data.length} ${this.state.json['200019docmap-000048']}！`, color: 'success'});/* 国际化处理： 查询成功,共,条*/
						newData = data.map((d, i) => {
							return Object.assign(d, { key: d.pk_docmaptemplet, idx: i + 1 });
						});
						this.setState({ tableData: newData });
					} else {
						toast({ content: this.state.json['200019docmap-000045'], color: 'warning', });/* 国际化处理： 未查询出符合条件的数据！*/
						this.setState({ tableData: [] });
					}
				}
			}
		});
	};
	handleTableData = (data) => {
		this.setState({ tableData: data });
	};
	render() {
		let { srcOrg, destOrg, action, tableData, json, srcAccountingbook, desAccountingbook } = this.state;
		return (
			<div className="nc-single-table">
				<ListHeader
					json={json}
					srcOrg={srcOrg}
					destOrg={destOrg}
					srcAccountingbook={srcAccountingbook}
					desAccountingbook={desAccountingbook}
					NCProps={this.props}
					refresh={this.searchData}
					deleteAction={this.deleteAction}
					tableData={tableData}
				/>
				<ListBody
					json={json}
					NCProps={this.props}
					action={action}
					handleOrgData={this.handleOrgData}
					srcOrg={srcOrg}
					destOrg={destOrg}
					srcAccountingbook={srcAccountingbook}
					desAccountingbook={desAccountingbook}
					tableData={tableData}
					searchData={this.searchData}
					clearTableData={this.clearTableData}
					handleTableData={this.handleTableData}
					appcode = {this.appcode}
				/>
			</div>
		);
	}
}

export default (App = createPage({ 
	// initTemplate 
})(App));
