import React, { Component } from 'react';
import { ajax, base, high, viewModel, toast } from 'nc-lightapp-front';
import ReferPannel from './body_referpannel';
import GlobalStore from '../../../../public/components/GlobalStore';
import '../index.less'
const { NCTable: Table, NCCheckbox: Checkbox, NCPopconfirm: Popconfirm, NCDiv } = base;
const { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel
const emptyCell = <span>&nbsp;</span>
export default class ListBody extends Component {
	static defaultProps = {
		prefixCls: 'bee-table',
		multiSelect: {
			type: 'checkbox',
			param: 'key',
		}
	};
	constructor(props) {
		super(props);
		this.state = {
			checkedAll: { checked: false, indeterminate: false },
			checkedArray: {}
		};
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps && nextProps.action) {
			if (nextProps.action == 'delete') {
				let { checkedArray } = this.state;
				let checkedLen = Object.keys(checkedArray).length;
				if (checkedLen > 1) {
					toast({ color: 'warning', content: this.props.json['200019docmap-000038'] });/* 国际化处理： 只能删除一条数据！*/
				} else if (checkedLen == 0) {
					toast({ color: 'warning', content: this.props.json['200019docmap-000000'] });/* 国际化处理： 请选择要删除的数据！*/
				} else {
					let keys = Object.keys(checkedArray);
					let record = checkedArray[keys[0]].record;
					this.deleteRow('', record, '');
				}
			}
		}
	}

	//点击search之后的回调
	searchData = (data) => {
		this.props.searchData(data);
	};
	//全选操作
	onAllCheckChange = (status) => {
		let { tableData } = this.props;
		let checkedArray = {};
		if (status) {
			if (tableData.length) {
				for (let i = 0; i < tableData.length; i++) {
					checkedArray[tableData[i].pk_docmaptemplet] = { checked: true, record: tableData[i] };
				}
			}
		}
		this.setState({ checkedAll: { checked: status, indeterminate: false }, checkedArray });
	};
	//单选操作
	onCheckboxChange = (text, record, index) => {
		let { checkedArray } = this.state;
		let { tableData } = this.props;
		let checkedAll = { checked: false, indeterminate: false };
		if (checkedArray[record.pk_docmaptemplet] && checkedArray[record.pk_docmaptemplet].checked) {
			delete checkedArray[record.pk_docmaptemplet];
		} else {
			checkedArray[record.pk_docmaptemplet] = { checked: true, record };
		}
		let checkedLen = Object.keys(checkedArray).length;
		if (checkedLen == tableData.length) {
			checkedAll.checked = true;
			checkedAll.indeterminate = false;
		} else if (checkedLen > 0 && checkedLen < tableData.length) {
			checkedAll.checked = false;
			checkedAll.indeterminate = true;
		}
		this.setState({ checkedArray, checkedAll });
	};
	//修改、删除按钮点击事件
	AClick = (text, record, index, type) => {
		if (type == 'del') {
			this.deleteRow(text, record, index);
		} else {
			let { srcOrg, destOrg, NCProps, tableData, srcAccountingbook, desAccountingbook } = this.props;
			let docu_compare_card = {
				pk_docmaptemplet: record.pk_docmaptemplet,
				status: 'edit',
				srcOrg,
				destOrg,
				srcAccountingbook, 
				desAccountingbook,
				listData: tableData
			};
			setGlobalStorage('sessionStorage', 'docu_compare_card', JSON.stringify(docu_compare_card));	
			NCProps.linkTo('/gl/doc_comparison/pages/card/index.html', {
				pagecode: '200019docmap_card',
				appcode: '200019docmap'
			});
		}
	};

	//删除操作
	deleteRow = (text, record, index) => {
		ajax({
			url: '/nccloud/gl/glpub/delDocmapTemp.do',
			data: { pk_docmaptemplet: record.pk_docmaptemplet },
			success: (res) => {
				if (res.success) {
					toast({ color: 'success', content: this.props.json['200019docmap-000039'] });/* 国际化处理： 删除成功！*/
					let { checkedArray } = this.state;
					let { tableData } = this.props;
					for (let i = 0; i < tableData.length; i++) {
						let delIndex = tableData.length;
						if (tableData[i].pk_docmaptemplet == record.pk_docmaptemplet) {
							delIndex = i;
							tableData.splice(i, 1);
						}
						//重新计算序号
						if (i > delIndex) {
							let idx = tableData[i].idx - 1;
							tableData[i].idx = idx;
						}
					}
					for (const key in checkedArray) {
						if (key == record.pk_docmaptemplet) {
							delete checkedArray[key];
							break;
						}
					}
					this.props.handleTableData(tableData);
					this.setState({ checkedArray });
				} else {
					toast({ color: 'warning', content: this.props.json['200019docmap-000040'] });/* 国际化处理： 删除失败！*/
				}
			}
		});
	};
	// 行的双击事件
	onRowDoubleClick = (record, index, event) => {
		let { srcOrg, destOrg, tableData, NCProps, srcAccountingbook, desAccountingbook } = this.props;
		let docu_compare_card = {
			pk_docmaptemplet: record.pk_docmaptemplet,
			status: 'browse',
			srcOrg,
			destOrg,
			srcAccountingbook, 
			desAccountingbook,
			listData: tableData
		};
		setGlobalStorage('sessionStorage', 'docu_compare_card', JSON.stringify(docu_compare_card));	
		NCProps.linkTo('/gl/doc_comparison/pages/card/index.html', {
			pagecode: '200019docmap_card',
			appcode: '200019docmap'
		});
	};
	/* emptyText = () => {
		return <span>这里没有数据！</span>;
	}; */
	getTableColumns = () => {
		let { checkedAll, checkedArray } = this.state;
		let columns = [
			{
				title: (
					<div fieldid='firstcol'>
						<Checkbox
							className="table-checkbox"
							checked={checkedAll.checked}
							indeterminate={checkedAll.indeterminate}
							onChange={this.onAllCheckChange}
						/>
					</div>
				),
				key: 'checkbox',
				attrcode: 'checkbox',
                dataIndex: 'checkbox',
				className: 'table-checkbox-class',
				visible: true,
				itemtype: 'customer',
				width: 50,
				render: (text, record, index) => {
					let rowChecked =
						checkedArray[record.pk_docmaptemplet] && checkedArray[record.pk_docmaptemplet].checked
							? true
							: false;
					return (
						<div fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={rowChecked}
								onChange={this.onCheckboxChange.bind(this, text, record, index)}
							/>
						</div>
					);
				}
			},
			{ 
				title: (<div fieldid='idx'>{this.props.json['200019docmap-000006']}</div>), 
				dataIndex: 'idx', 
				key: 'idx', 
				width: 50, 
				render: (text, record, index) => <div fieldid='idx'>{text ? text : emptyCell}</div>
			},/* 国际化处理： 序号*/
			{ 
				title: (<div fieldid='templetcode'>{this.props.json['200019docmap-000015']}</div>), 
				dataIndex: 'templetcode', 
				key: 'templetcode', 
				width: 250, 
				render: (text, record, index) => <div fieldid='templetcode'>{text ? text : emptyCell}</div>
			},/* 国际化处理： 编码*/
			{ 
				title: (<div fieldid='templetname'>{this.props.json['200019docmap-000016']}</div>), 
				dataIndex: 'templetname', 
				key: 'templetname', 
				width: 250, 
				render: (text, record, index) => <div fieldid='templetname'>{text ? text : emptyCell}</div>
			},/* 国际化处理： 名称*/
			{ 
				title: (<div fieldid='srcname'>{this.props.json['200019docmap-000023']}</div>), 
				dataIndex: 'srcname', 
				key: 'srcname', 
				width: 250, 
				render: (text, record, index) => <div fieldid='srcname'>{text ? text : emptyCell}</div>
			},/* 国际化处理： 来源组织*/
			{ 
				title: (<div fieldid='desname'>{this.props.json['200019docmap-000024']}</div>), 
				dataIndex: 'desname', 
				key: 'desname', 
				width: 250, 
				render: (text, record, index) => <div fieldid='desname'>{text ? text : emptyCell}</div>
			},/* 国际化处理： 目的组织*/
			{
				title: (<div fieldid='opr'>{this.props.json['200019docmap-000041']}</div>),/* 国际化处理： 操作*/
				dataIndex: 'operation',
				key: 'operation',
				width: 160,
				render: (text, record, index) => {
					return (
						<div className='table-inner-btn-area' fieldid='opr'>
							<a className="handle-btn" style={{marginRight: '12px',cursor:'pointer'}} onClick={this.AClick.bind(this, text, record, index, 'edit')}>
								{this.props.json['200019docmap-000043']}{/* 国际化处理： 修改*/}
							</a>
							<Popconfirm
								content={this.props.json['200019docmap-000042']}/* 国际化处理： 确定删除么？*/
								placement="top"
								onClose={this.AClick.bind(this, text, record, index, 'del')}
							>
								<a className="handle-btn" style={{cursor: 'pointer'}}>{this.props.json['200019docmap-000029']}</a>{/* 国际化处理： 删除*/}
							</Popconfirm>
						</div>
					);
				}
			}
		];
		return columns
	}
	render() {	
		let { tableData } = this.props;
		let { handleOrgData, srcOrg, destOrg, NCProps, appcode } = this.props;
		let columns = this.getTableColumns()
		return (
			<div className="body-container">
				<NCDiv areaCode={NCDiv.config.SEARCH}>
					<ReferPannel
						json={this.props.json}
						NCProps={NCProps}
						searchData={this.searchData}
						clearTableData={this.props.clearTableData}
						handleOrgData={handleOrgData}
						srcOrg={srcOrg}
						destOrg={destOrg}
						appcode={appcode}
					/>
				</NCDiv>
				<NCDiv fieldid="doclist" areaCode={NCDiv.config.TableCom}>
					<Table
						columns={columns}
						data={tableData}
						onRowDoubleClick={this.onRowDoubleClick}
					/>
				</NCDiv>
				{/* <div className="nc-singleTable-table-area">
					
				</div> */}
			</div>
		);
	}
}
