import React, { Component } from 'react';
import BusinessUnitTreeRef from '../../../../../uapbd/refer/org/BusinessUnitAllTreeRef';

export default class ReferPannel extends Component {
	constructor(props) {
		super(props);
	}
	//查询按钮
	onSearch = () => {
		let { srcOrg, destOrg } = this.props;
		let data = {
			pk_desorgbook: destOrg.refpk ? destOrg.refpk : '',
			pk_srcorgbook: srcOrg.refpk ? srcOrg.refpk : ''
		};
		this.props.searchData(data);
	};
	clearTableData = () =>{
		this.props.clearTableData()
	}
	handleClick = (props, type) => {
		switch (type) {
			case 'search':
				this.onSearch()
				break;
			case 'clear_table':
				this.clearTableData();
				break;
			default:
				break;
		}
	}
	//来源组织、目的组织改变回调
	handleReferVal = (key, val) => {
		this.props.handleOrgData(key, val);
	};
	render() {
		let { srcOrg, destOrg, NCProps, appcode, json } = this.props;
		return (
			<div className="nc-singleTable-search-area referpannel">
				<div className="referpannel-item">
					<span className="title nc-theme-common-font-c"><i className='require-true'>*</i>{json['200019docmap-000023']}：</span>{/* 国际化处理： 来源组织*/}
					<span className="refer-content">
						<BusinessUnitTreeRef
							fieldid='srcOrg'
							value={{
								refname: srcOrg.refname,
								refpk: srcOrg.refpk
							}}
							queryCondition={{ 
								funcode:appcode,
								TreeRefActionExt: 'nccloud.web.fipub.ref.BusinessUnitSqlBuilder',
							}}
							onChange={(value) => {
								this.handleReferVal('srcOrg', value);
							}}
						/>
					</span>
				</div>
				<div className="referpannel-item">
					<span className="title nc-theme-common-font-c"><i className='require-true'>*</i>{json['200019docmap-000024']}：</span>{/* 国际化处理： 目的组织*/}
					<span className="refer-content">
						<BusinessUnitTreeRef
							fieldid='destOrg'
							value={{
								refname: destOrg.refname,
								refpk: destOrg.refpk
							}}
							queryCondition={{ 
								funcode:appcode,
								TreeRefActionExt: 'nccloud.web.fipub.ref.BusinessUnitSqlBuilder',
							}}
							onChange={(value) => {
								this.handleReferVal('destOrg', value);
							}}
						/>
					</span>
				</div>
				<div className="referpannel-btn">
					{NCProps.button.createButtonApp({
						area: 'table_header',
						buttonLimit: 1,
						onButtonClick: this.handleClick
					})}
				</div>
			</div>
		);
	}
}
