import React, { Component } from 'react';
import { toast ,base, createPage, viewModel } from 'nc-lightapp-front';
import HeaderArea from '../../../../public/components/HeaderArea';
const { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel
class ListHeader extends Component {
	constructor(props) {
		super(props);
	}
	handleClick = (props, type) => {
		let { srcOrg, destOrg, tableData, srcAccountingbook, desAccountingbook } = this.props;
		switch (type) {
			case 'refresh':
				let data = {
					pk_desorgbook: destOrg.refpk ? destOrg.refpk : '',
					pk_srcorgbook: srcOrg.refpk ? srcOrg.refpk : ''
				};
				this.props.refresh(data);
				break;
			case 'delete':
				this.props.deleteAction('delete');
				break;
			case 'add':
				if (Object.keys(srcOrg).length && Object.keys(destOrg).length) {
					let docu_compare_card = {
						status: 'add',
						srcOrg,
						destOrg,
						srcAccountingbook, 
						desAccountingbook,
						listData: tableData
					};
					setGlobalStorage('sessionStorage', 'docu_compare_card', JSON.stringify(docu_compare_card));	
					props.linkTo('/gl/doc_comparison/pages/card/index.html', {
						pagecode: '200019docmap_card',
						appcode: '200019docmap'
					});
				} else {
					toast({ color: 'warning', content: this.props.json['200019docmap-000044'] });/* 国际化处理： 请选择来源组织和目的组织！*/
				}
				break;
			// case 'clear_table':
			// 	this.props.clearTableData();
			// 	break;
			default:
				break;
		}
	};

	render() {
		let { NCProps } = this.props;
		return (
			<HeaderArea 
				title = {this.props.json['200019docmap-000025']}  /* 国际化处理： 基础档案对照*/
				btnContent = {NCProps.button.createButtonApp({
					area: 'page_header',
					buttonLimit: 3,
					onButtonClick: this.handleClick
				})}
			/>	
		);
	}
}

ListHeader = createPage({})(ListHeader)
export default ListHeader
