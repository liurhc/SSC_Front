import { ajax } from 'nc-lightapp-front';
export default function initTemplate(props) {
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do',
		data: {
			pagecode: '200019docmap_list',
			appcode: '200019docmap'
		},
		success: (res) => {
			if (res.success) {
				if (res.data) {
					props.button.setButtons(res.data);
				}
			}
		}
	});
}
