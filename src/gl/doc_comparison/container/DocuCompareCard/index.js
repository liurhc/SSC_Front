import React, { Component } from 'react';
import { ajax, base, high, createPage, toast, promptBox, getMultiLang, viewModel } from 'nc-lightapp-front';
import CardHeader from './components/header';
import CardBody from './components/body';
import { linkTo } from '../../util/index';
import GlobalStore from '../../../public/components/GlobalStore';
import initTemplate from './event/initTemplate';
import './index.less';
const { NCModal: Modal, NCButton: Button } = base;
const { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel
class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			cardInfo: {
				maptemplateWebVO: { docmaps: {} },
				treeData: []
			},
			selectedTreeNode: { key: '' },
			srcOrg: {}, //新增时列表页传来的来源组织信息
			destOrg: {}, //新增时列表页传来的目的组织信息
			srcAccountingbook: '', //新增时列表页传来的来源组织核算账簿
			desAccountingbook: '',//新增时列表页传来的目的组织核算账簿
			status: 'browse', //卡片状态 edit add browse
			pk_docmaptemplet: '', //主键
			modalData: {
				srcType: {},
				destType: {}
			}, //模态框数据
			autoMatch: 'name',
			showContent: 'name', //code  name
			json: {},
			pageBtnStatus: {
				prevBtn: true,
				nextBtn: false
			},
		};
		// initTemplate.call(this, this.props);
	}
	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			},()=>{		  
				initTemplate.call(this, this.props);
			})
		}
		getMultiLang({moduleId:'200019docmap',domainName:'gl',currentLocale:'simpchn',callback});
	}
	componentDidMount() {
		const docu_compare_card = JSON.parse(getGlobalStorage('sessionStorage', 'docu_compare_card'))
		const { status, pk_docmaptemplet = '', srcOrg, destOrg, listData, srcAccountingbook, desAccountingbook } = docu_compare_card;
		let currIndex = this.getCurrIndex(listData, pk_docmaptemplet)
		this.updatePageBtnStatus(currIndex, listData)
		switch (status) {
			case 'add':
				let srcname = srcOrg.refname;
				let pk_srcorgbook = srcOrg.refpk;
				let desname = destOrg.refname;
				let pk_desorgbook = destOrg.refpk;
				
				this.state.cardInfo.maptemplateWebVO.srcname = srcname;
				this.state.cardInfo.maptemplateWebVO.pk_srcorgbook = pk_srcorgbook;

				this.state.cardInfo.maptemplateWebVO.desname = desname;
				this.state.cardInfo.maptemplateWebVO.pk_desorgbook = pk_desorgbook;

				this.state.cardInfo.maptemplateWebVO.srcAccountingbook = srcAccountingbook;
				this.state.cardInfo.maptemplateWebVO.desAccountingbook = desAccountingbook;

				this.setState({ status, pk_docmaptemplet, srcOrg, destOrg, srcAccountingbook, desAccountingbook, cardInfo: this.state.cardInfo });
				this.props.button.setButtonDisabled(['line_group','add_line','del_line','fill','arrange' ], true);
				break;
			case 'edit':
				this.setState({ status, pk_docmaptemplet, srcOrg, destOrg, srcAccountingbook, desAccountingbook },()=>{
					this.queryCardInfo(pk_docmaptemplet)
				});
				this.updataButtonStatus(true)
				break;
			case 'browse':
				this.setState({ status, pk_docmaptemplet, srcOrg, destOrg, listData, srcAccountingbook, desAccountingbook },()=>{
					this.queryCardInfo(pk_docmaptemplet)
				});
				break;
			default:
				break;
		}

		// status != 'add' && this.queryCardInfo(pk_docmaptemplet);
	}
	//
	getCurrIndex = (listData, pk_docmaptemplet) => {
		let docmaptempletPks = listData.map(item => item.pk_docmaptemplet)
		if(pk_docmaptemplet){
			return docmaptempletPks.indexOf(pk_docmaptemplet);
		}
	}
	//更新翻页按钮状态
	updatePageBtnStatus = (currIndex, listData) =>{

		let {pageBtnStatus} =this.state
		let tempStatus = pageBtnStatus
		if(listData.length <= 1){
			tempStatus = {prevBtn: true, nextBtn: true}
		} else {
			if(currIndex == 0){
				// tempStatus.prevBtn = true
				tempStatus = {prevBtn: true, nextBtn: false}
			}else if(currIndex == listData.length - 1){
				// tempStatus.nextBtn = true
				tempStatus = {prevBtn: false, nextBtn: true}
			} else {
				tempStatus = {prevBtn: false, nextBtn: false}
			}
		}
		
		this.setState({
			pageBtnStatus: tempStatus
		})
	}
	//查询卡片信息
	queryCardInfo = (pk_docmaptemplet) => {
		let { srcAccountingbook, desAccountingbook } = this.state
		ajax({
			url: '/nccloud/gl/glpub/queryDocmaps.do',
			data: { pk_docmaptemplet },
			success: (res) => {
				if (res.success) {
					if (res.data) {
						let { maptemplateWebVO = {}, nodes = {} } = res.data;
						let { treeData, tableDataKeys } = this.buildTreeData(nodes.children);
						let docmaps = this.buildTableData(maptemplateWebVO.docmaps, tableDataKeys);
						maptemplateWebVO.docmaps = docmaps;
						maptemplateWebVO.srcAccountingbook = srcAccountingbook;
						maptemplateWebVO.desAccountingbook = desAccountingbook;
						this.setState({
							selectedTreeNode: { key: Object.keys(docmaps)[0] },
							cardInfo: { maptemplateWebVO, treeData: treeData }
						});
					}
				}
			}
		});
	};

	/*
		修改卡片信息  
		area 区域    data  数据
	*/
	handleCompareInfo = (area, data) => {
		if (area == 'form') {
			let { name, val } = data;
			switch (name) {
				case 'destOrg':
					this.state.cardInfo.maptemplateWebVO.desname = val.refname;
					this.state.cardInfo.maptemplateWebVO.pk_desorgbook = val.refpk;
					break;
				case 'templetcode':
				case 'templetname':
					this.state.cardInfo.maptemplateWebVO[name] = val;
					break;
				default:
					break;
			}
			this.setState({ cardInfo: this.state.cardInfo });
		} else if (area == 'table') {
			let { name, val, index } = data;
			let currNodeKey = this.state.selectedTreeNode.key;
			let { docmaps } = this.state.cardInfo.maptemplateWebVO;
			if (!docmaps[currNodeKey][index]) {
				docmaps[index] = {};
			}
			docmaps[currNodeKey][index][name] = { display: `${val.refcode||''} ${val.refname||''}`, value: val.refpk };
			this.setState({ cardInfo: this.state.cardInfo });
		}
	};
	//操作表格数据
	handleTableData = (data) => {
		let { selectedTreeNode } = this.state;
		this.state.cardInfo.maptemplateWebVO.docmaps[selectedTreeNode.key] = data;
		this.setState({ cardInfo: this.state.cardInfo });
	};
	//保存
	saveCard = () => {
		let maptemplateWebVO = Object.assign({}, this.state.cardInfo.maptemplateWebVO);
		if (!maptemplateWebVO.templetcode || !maptemplateWebVO.templetname) {
			toast({ content: this.state.json['200019docmap-000026'], color: 'warning' });/* 国际化处理： 编码和名称不能为空！*/
			return;
		}
		let { docmaps } = maptemplateWebVO;
		let status = this.state.status;
		let sendMaps = [],
			url = '';
		for (const key in docmaps) {
			sendMaps.push(...docmaps[key]);
		}
		if (status == 'add') {
			delete maptemplateWebVO.pk_docmaptemplet;
			url = '/nccloud/gl/glpub/insertDocmapTemp.do';
		} else if (status == 'edit') {
			url = '/nccloud/gl/glpub/updateDocmapTemp.do';
		}
		ajax({
			url,
			data: { ...maptemplateWebVO, docmaps: sendMaps },
			success: (res) => {
				if (res.success) {
					const docu_compare_card = JSON.parse(getGlobalStorage('sessionStorage', 'docu_compare_card'))
					const { listData } = docu_compare_card;
					if (status == 'add') {
						this.queryCardInfo(res.data);
						listData.unshift({ pk_docmaptemplet: res.data });
					} else if (status == 'edit') {
						// let index;
						// for (let i = 0; i < listData.length; i++) {
						// 	if (listData[i].pk_docmaptemplet == maptemplateWebVO.pk_docmaptemplet) {
						// 		index = i;
						// 		break;
						// 	}
						// }
						// listData.splice(index, 1, { pk_docmaptemplet: res.data });
					}
					setGlobalStorage('sessionStorage', 'docu_compare_card', JSON.stringify(docu_compare_card));					
					toast({ content: this.state.json['200019docmap-000027'] , color: 'success' });/* 国际化处理： 保存成功！*/
					let pk_docmaptemplet = this.state.pk_docmaptemplet;
					if(res.data) pk_docmaptemplet = res.data;
					this.setState({ status: 'browse', pk_docmaptemplet});
					let currIndex = this.getCurrIndex(listData, pk_docmaptemplet)
					this.updatePageBtnStatus(currIndex, listData)
					this.props.button.setButtonsVisible([ 'add', 'delete', 'update' ], true);
					this.props.button.setButtonsVisible([ 'save', 'cancel', 'line_group', 'add_type' ], false);
				}
			}
		});
	};
	//删除当前卡片
	deleteCard = () => {
		ajax({
			url: '/nccloud/gl/glpub/delDocmapTemp.do',
			data: { pk_docmaptemplet: this.state.pk_docmaptemplet },
			success: (res) => {
				if (res.success) {
					// 编辑态，删除成功后不需要提示成功
					// toast({ content: '删除成功！', color: 'success' });
					setTimeout(() => {
						this.backAndCancel();
					}, 500);
				}
			}
		});
	};
	backAndCancel = () => {
		let { srcOrg, destOrg,srcAccountingbook, desAccountingbook } = this.state;
		let docu_compare_card = {
			srcOrg,
			destOrg,
			srcAccountingbook, 
			desAccountingbook
		};
		setGlobalStorage('sessionStorage', 'docu_compare_card', JSON.stringify(docu_compare_card));	
		this.props.linkTo('/gl/doc_comparison/pages/list/index.html', {
			pagecode: '200019docmap_list',
			appcode: '200019docmap'
		});
	};
	cancelEdit = () => {
		let maptemplateWebVO = this.state.cardInfo.maptemplateWebVO
		maptemplateWebVO.docmaps = {}
		promptBox({
			color:'warning',
			title: this.state.json['200019docmap-000022'],/* 国际化处理： 取消*/
			content: this.state.json['200019docmap-000028'],  /* 国际化处理： 确定要取消吗？*/
			beSureBtnClick: () => {
				let { pk_docmaptemplet, status } = this.state;
				if (status == 'edit') {
					this.queryCardInfo(pk_docmaptemplet);
					this.props.button.setButtonsVisible([ 'add', 'delete', 'update' ], true);
				} else if (status == 'add') {
					this.setState({
						cardInfo: {
							// maptemplateWebVO: { docmaps: {} },
							maptemplateWebVO: maptemplateWebVO,
							treeData: []
						},
						pageBtnStatus: true
					});
					this.props.button.setButtonsVisible([ 'add' ], true);
					// this.props.button.setButtonDisabled(['first','last','prev','next'], true)
				}
				this.setState({ status: 'browse' });
				this.props.button.setButtonsVisible([ 'save', 'cancel', 'line_group', 'add_type' ], false);
			},
			cancelBtnClick: () => {}
		  });
	}
	//表头按钮操作
	headerBtnsHandle = (props, type) => {
		switch (type) {
			case 'save':
				this.saveCard();
				break;
			case 'back':
				this.backAndCancel();
				break;
			case 'cancel':
				this.cancelEdit();		
				break;
			case 'update':
				this.setState({ status: 'edit' });
				props.button.setButtonsVisible([ 'add', 'delete', 'update' ], false);
				props.button.setButtonsVisible([ 'save', 'cancel', 'line_group', 'add_type' ], true);
				this.props.button.setButtonDisabled(['line_group','add_line','del_line','fill','arrange' ], true);
				break;
			case 'add':
				//this.state.cardInfo.maptemplateWebVO.docmaps = {};
				//this.state.cardInfo.treeData = [];
				let maptemplateWebVO = this.state.cardInfo.maptemplateWebVO
				maptemplateWebVO.docmaps = {}
				maptemplateWebVO.templetcode = ''
				maptemplateWebVO.templetname = ''
				this.setState({
					status: 'add',
					cardInfo: {
						// maptemplateWebVO: { docmaps: {} },
						maptemplateWebVO: maptemplateWebVO,
						treeData: []
					},
					selectedTreeNode: { key: '' },
					modalData: { srcType: {}, destType: {} }
					//autoMatch: ''
				});
				props.button.setButtonsVisible([ 'add', 'delete', 'update' ], false);
				props.button.setButtonsVisible([ 'save', 'cancel', 'line_group', 'add_type' ], true);
				this.props.button.setButtonDisabled(['line_group','add_line','del_line','fill','arrange' ], true);
				break;
			case 'delete':
				//this.props.button.setPopContent('delete','确认要删除吗？')
				promptBox({
					color:'warning',
					title: this.state.json['200019docmap-000029'],/* 国际化处理： 删除*/
					content: this.state.json['200019docmap-000030'],  /* 国际化处理： 确定要删除吗？*/
					beSureBtnClick: this.deleteCard,
					cancelBtnClick: () => {}
				  });
			default:
				break;
		}
	};
	//模态框来源类型、目标类型
	handleModalData = (type, val) => {
		this.state.modalData[type] = val;
		this.setState({ modalData: this.state.modalData });
	};
	//重置modalData
	resetModalData = () => {
		this.setState({
			modalData: {
				srcType: {},
				destType: {}
			}
		});
	};
	//模态框确定事件--处理左树数据
	modalOkClick = () => {
		let { modalData, cardInfo } = this.state;
		let { srcType, destType } = modalData;
		let oldTreeData = cardInfo.treeData.concat([]);
		if (!srcType.refpk || !destType.refpk) {
			toast({ content: this.state.json['200019docmap-000002'], color: 'warning' });/* 国际化处理： 请选择来源类型和目标类型！*/
			return false;
		}
		let isrepeat = false;
		for (let i = 0; i < oldTreeData.length; i++) {
			if (oldTreeData[i].refpk == srcType.refpk) {
				let newChildren = {
					children: null,
					innercode: null,
					nodeData: null,
					title: null,
					isleaf: true,
					parentRefPk: oldTreeData[i].refpk,
					refname: `${oldTreeData[i].refname}->${destType.refname}`,
					refpk: destType.refpk
				};
				oldTreeData[i].children.push(newChildren);
				isrepeat = true;
				break;
			}
		}
		if (!isrepeat) {
			let newNode = {
				children: [
					{
						children: null,
						innercode: null,
						nodeData: null,
						title: null,
						isleaf: true,
						parentRefPk: srcType.refpk,
						refname: `${srcType.refname}->${destType.refname}`,
						refpk: destType.refpk
					}
				],
				innercode: null,
				nodeData: null,
				title: null,
				isleaf: false,
				parentRefPk: null,
				refname: srcType.refname,
				refpk: srcType.refpk
			};
			oldTreeData.push(newNode);
		}
		this.state.cardInfo.treeData = oldTreeData;
		this.state.cardInfo.maptemplateWebVO.docmaps[`${srcType.refpk} ${destType.refpk}`] = [];
		let selectedTreeNode = { key: `${srcType.refpk} ${destType.refpk}` };
		this.setState({ cardInfo: this.state.cardInfo, selectedTreeNode });
		this.CardBody.closeModal()
		return true;
	};
	//模态框确定并导入事件--导入数据
	exportData = () => {
		let { srcOrg, modalData } = this.state;
		let { srcType, destType } = modalData;
		if (!Object.keys(srcType).length || !Object.keys(destType).length) {
			toast({ content: this.state.json['200019docmap-000002'], color: 'warning' });/* 国际化处理： 请选择来源类型和目标类型！*/
			return;
		}
		ajax({
			url: '/nccloud/gl/glpub/importSrcDoc.do',
			data: {
				pk_srcType: srcType.refpk,
				pk_srcCrop: srcOrg.refpk,
				pk_desType: destType.refpk
			},
			success: (res) => {
				if (res.success) {
					if (res.data) {
						let { docmaps, nodes } = res.data;
						docmaps = docmaps ? docmaps : [];
						let { treeData, tableDataKeys } = this.buildTreeData(nodes.children);
						let resData = this.buildTableData(docmaps, tableDataKeys);
						let oldTreeData = this.state.cardInfo.treeData.concat([]);
						//更新treeData
						let noreapeatnodes = [];
						if (treeData && treeData.length) {
							for (let i = 0; i < treeData.length; i++) {
								let norepeat = true;
								for (let j = 0; j < oldTreeData.length; j++) {
									if (oldTreeData[j].refpk == treeData[i].refpk) {
										let newchildren = oldTreeData[j].children.concat(treeData[i].children);
										oldTreeData[j].children = newchildren;
										norepeat = false;
										break;
									}
								}
								norepeat && noreapeatnodes.push(treeData[i]);
							}
						}
						this.state.cardInfo.treeData = oldTreeData.concat(noreapeatnodes);
						//更新tableData
						let oldTableData = Object.assign({}, this.state.cardInfo.maptemplateWebVO.docmaps);
						this.state.cardInfo.maptemplateWebVO.docmaps = { ...oldTableData, ...resData };
						this.setState({
							selectedTreeNode: { key: `${srcType.refpk} ${destType.refpk}` },
							cardInfo: this.state.cardInfo
						});
					}
					this.CardBody.closeModal('check');
				}
			}
		});
	};
	autoMatchSelect = (val) => {
		this.setState({ autoMatch: val });
	};
	//自动匹配操作
	autoMatchClick = () => {
		let { docmaps } = this.state.cardInfo.maptemplateWebVO;
		let tableData = this.CardBody.buildTableData(docmaps);
		if(tableData.length===0){
			toast({ content: this.state.json['200019docmap-000031'], color: 'warning' });/* 国际化处理： 数据为空，请先增加数据在进行自动匹配！*/
			return
		}
		let { selectedTreeNode } = this.state;
		if (
			Object.keys(docmaps).length == 0 ||
			!docmaps[selectedTreeNode.key] ||
			docmaps[selectedTreeNode.key].length == 0
		) {
			return;
		}
		let { srcOrg, destOrg, modalData, autoMatch } = this.state;
		//let { srcType, destType } = modalData;
		ajax({
			url: '/nccloud/gl/glpub/matchDoc.do',
			data: {
				pk_srcType: selectedTreeNode.key.split(' ')[0],
				pk_srcCrop: srcOrg.refpk,
				pk_desType: selectedTreeNode.key.split(' ')[1],
				pk_desCrop: destOrg.refpk,
				srcMatchField: autoMatch, //匹配原则 code 编码，name 名称
				desMatchField: autoMatch //匹配原则 code 编码，name 名称
			},
			success: (res) => {
				if (res.success) {
					if (res.data) {
						toast({ content: this.state.json['200019docmap-000032'], color: 'success' });/* 国际化处理： 匹配成功！*/
						let { cardInfo, selectedTreeNode } = this.state;
						let tableData = cardInfo.maptemplateWebVO.docmaps[selectedTreeNode.key] || [];
						let { docmaps, nodes } = res.data;
						docmaps = docmaps ? docmaps : [];
						let needSetState = false;
						for (let i = 0; i < tableData.length; i++) {
							for (let j = 0; j < docmaps.length; j++) {
								if (
									docmaps[j].srctype.value == tableData[i].srctype.value &&
									docmaps[j].srcvalue.value == tableData[i].srcvalue.value
								) {
									if (docmaps[j].desvalue.value) {
										tableData[i].desvalue = docmaps[j].desvalue;
										needSetState = true;
										break;
									}
								}
							}
						}
						if (needSetState) {
							this.state.cardInfo.maptemplateWebVO.docmaps[selectedTreeNode.key] = tableData;
							this.setState({
								cardInfo: this.state.cardInfo
							});
						}
					}else{
						toast({ content: this.state.json['200019docmap-000033'], color: 'warning' });/* 国际化处理： 未找到匹配项！*/
					}
				}
			}
		});
	};
	//创建treeData
	buildTreeData = (data, tableDataKeys = [], parName = '', parPk = '') => {
		if (data) {
			for (let i = 0; i < data.length; i++) {
				let refname = data[i].refname.split(' ')[1];
				let refpk = data[i].refpk;
				if (!data[i].isleaf) {
					data[i].refname = refname;
					this.buildTreeData(data[i].children, tableDataKeys, refname, refpk);
				} else {
					data[i].refname = parName + '->' + refname;
					tableDataKeys.push(`${parPk} ${data[i].refpk}`);
				}
			}
		}
		return { treeData: data, tableDataKeys };
	};
	//创建tableData
	buildTableData = (tableData = [], tableDataKeys = []) => {
		let resData = {};
		if (tableDataKeys.length) {
			for (let i = 0; i < tableDataKeys.length; i++) {
				resData[tableDataKeys[i]] = [];
				for (let j = 0; j < tableData.length; j++) {
					if (tableDataKeys[i] == `${tableData[j]['srctype'].value||'00000'} ${tableData[j]['destype'].value}`) {
						resData[tableDataKeys[i]].push(tableData[j]);
					}
				}
			}
		}
		return resData;
	};
	updataButtonStatus = (disabled) =>{
		this.props.button.setButtonDisabled(['del_line'], disabled)
	}
	//选中树节点
	selectTreeNode = (selectedKeys, e) => {
		if (selectedKeys[0].split(' ')[0] == '') {
			this.props.button.setButtonDisabled(['line_group','add_line','del_line','fill','arrange' ], true)
			return;
		}
		if(selectedKeys.length>0){
			let keyArr = selectedKeys[0].split(' ')
			if(keyArr.length === 2){
				this.props.button.setButtonDisabled(['line_group','add_line','fill','arrange' ], false);
			} else {
				this.props.button.setButtonDisabled(['line_group','add_line','del_line','fill','arrange' ], true);
			}
		}
		this.setState({ selectedTreeNode: { key: selectedKeys[0] } });
	};

	//查看列表条目（首条，下一条，上一条，末条）
	browseItem = (type) => {
		const docu_compare_card = JSON.parse(getGlobalStorage('sessionStorage', 'docu_compare_card'));
		const { listData } = docu_compare_card;
		let { pk_docmaptemplet } = this.state;
		let docmaptempletPks = listData.map(item => item.pk_docmaptemplet)
		let currIndex = docmaptempletPks.indexOf(pk_docmaptemplet);
		let queryPk = '';
		switch (type) {
			case 'first':
			case 'prev':

				if (currIndex == 0) {
					toast({ color: 'warning', content: this.state.json['20000CRVRL-000030'] });/* 国际化处理： 已经是第一条了！*/
				} else {
					queryPk = type == 'first' ? docmaptempletPks[0] : docmaptempletPks[currIndex - 1];
				}
				break;
			case 'last':
			case 'next':

				if (currIndex == docmaptempletPks.length - 1) {
					toast({ color: 'warning', content: this.state.json['20000CRVRL-000031'] });/* 国际化处理： 已经是最后一条了！*/
				} else {
					queryPk = type == 'last' ? docmaptempletPks[docmaptempletPks.length - 1] : docmaptempletPks[currIndex + 1];
				}
				break;
			default:
				break;
		}
		let pageIndex = docmaptempletPks.indexOf(queryPk);
		this.updatePageBtnStatus(pageIndex,docmaptempletPks)
		this.setState({ pk_docmaptemplet: queryPk });
		this.queryCardInfo(queryPk);
	};
	toggleContent = () => {
		let { showContent } = this.state;
		let newToggleContent = showContent == 'name' ? 'code' : 'name';
		this.setState({ showContent: newToggleContent });
	};

	render() {
		let { status, cardInfo, modalData, selectedTreeNode, autoMatch, showContent ,pageBtnStatus, json } = this.state;
		let { maptemplateWebVO, treeData } = cardInfo;
		let defaultExpandedKeys = [];
		if (selectedTreeNode.key) {
			defaultExpandedKeys = [ ` ${selectedTreeNode.key.split(' ')[0]}` ];
		}

		return (
			<div className="nc-bill-list compare-card-container">
				<CardHeader
					json={json}
					NCProps={this.props}
					status={status}
					browseItem={this.browseItem}
					headerBtnsHandle={this.headerBtnsHandle}
					pageBtnStatus={pageBtnStatus}
				/>
				<CardBody
					json={json}
					NCProps={this.props}
					status={status}
					selectedTreeNode={selectedTreeNode}
					maptemplateWebVO={maptemplateWebVO}
					treeData={treeData}
					handleCompareInfo={this.handleCompareInfo}
					handleTableData={this.handleTableData}
					handleModalData={this.handleModalData}
					exportData={this.exportData}
					modalOkClick={this.modalOkClick}
					selectTreeNode={this.selectTreeNode}
					resetModalData={this.resetModalData}
					autoMatchClick={this.autoMatchClick}
					autoMatchSelect={this.autoMatchSelect}
					toggleContent={this.toggleContent}
					showContent={showContent}
					autoMatch={autoMatch}
					modalData={modalData}
					defaultExpandedKeys={defaultExpandedKeys}
					updataButtonStatus={this.updataButtonStatus.bind(this)}
					CardBodyRef={(init) => { this.CardBody = init }}
				/>
			</div>
		);
	}
}

export default (Card = createPage({})(Card));
