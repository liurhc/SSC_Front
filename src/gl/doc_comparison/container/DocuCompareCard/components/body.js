import React, { Component } from 'react';
import { ajax, base, toast } from 'nc-lightapp-front';
import ReferPannel from './body_referpannel';
import BdmdMainentityAndEnumReffrom from '../../../../../uap/refer/riart/bdmdMainentityAndEnumRef';
import EditableCell from './editTableCell';
import {FISelect} from '../../../../public/components/base'
const {
	NCTree: Tree,
	NCTable: Table,
	NCButton: Button,
	NCIcon: Icon,
	NCModal: Modal,
	NCCheckbox: Checkbox, NCDiv
} = base;
const TreeNode = Tree.NCTreeNode;
const Select = FISelect;
const Option = Select.FIOption;
const emptyCell = <span>&nbsp;</span>
export default class CardBody extends Component {
	constructor(props) {
		super(props);
		this.state = {
			//isHoverKey: '',
			showModal: false,
			//autoMatch: '',
			checkedAll: {},
			checkedArray: {}
		};
	}
	componentWillReceiveProps() {}
	componentDidMount(){
		this.props.updataButtonStatus(true)
	}
	//来源组织、目的组织改变回调
	handleCompareInfo = (area, data) => {
		this.props.handleCompareInfo(area, data);
	};
	//全选操作
	onAllCheckChange = (status) => {
		let { maptemplateWebVO, selectedTreeNode } = this.props;
		const tableData = maptemplateWebVO.docmaps[selectedTreeNode.key];
		let currCheckedArray = {};
		if (status) {
			if (tableData && tableData.length) {
				for (let i = 0; i < tableData.length; i++) {
					currCheckedArray[tableData[i].idx] = { checked: true, record: tableData[i] };
				}
				this.props.updataButtonStatus(false)
			}
		}else{
			this.props.updataButtonStatus(true)
		}
		let checkedArray = { [selectedTreeNode.key]: currCheckedArray };
		let checkedAll = { [selectedTreeNode.key]: { checked: status, indeterminate: false } };
		this.setState({ checkedAll, checkedArray });
	};
	//单选操作
	onCheckboxChange = (text, record, index) => {
		let currTableId = `${record.srctype.value} ${record.destype.value}`;
		let { checkedArray } = this.state;
		let currTable = checkedArray[currTableId];
		if (!currTable) {
			currTable = {};
		}

		let { maptemplateWebVO, selectedTreeNode } = this.props;
		const tableData = maptemplateWebVO.docmaps[selectedTreeNode.key];
		let currCheckedAll = { checked: false, indeterminate: false };
		if (currTable[record.idx] && currTable[record.idx].checked) {
			delete currTable[record.idx];
		} else {
			currTable[record.idx] = { checked: true, record };
		}
		checkedArray[currTableId] = currTable;

		let checkedLen = Object.keys(currTable).length;
		if(checkedLen===0){
			this.props.updataButtonStatus(true)
		}else{
			this.props.updataButtonStatus(false)
		}
		if (checkedLen == tableData.length) {
			currCheckedAll.checked = true;
			currCheckedAll.indeterminate = false;
		} else if (checkedLen > 0 && checkedLen < tableData.length) {
			currCheckedAll.checked = false;
			currCheckedAll.indeterminate = true;
		}
		let checkedAll = { [selectedTreeNode.key]: currCheckedAll };
		this.setState({ 
			checkedArray, checkedAll 
		});
	};
	//创建树节点
	createTreeNodes = (data, parPk = '') => {
		if (data && data.length) {
			return data.map((item) => {
				if (item.children) {
					return (
						<TreeNode liAttr={{"fieldid": `${item.refname}_node`}} title={this.renderTreeTitle(item, parPk)} key={`${parPk} ${item.refpk}`}>
							{this.createTreeNodes(item.children, item.refpk)}
						</TreeNode>
					);
				}
				return (
					<TreeNode
						liAttr={{"fieldid": `${item.refname}_node`}}
						title={this.renderTreeTitle(item, parPk)}
						key={`${parPk} ${item.refpk}`}
						isLeaf={item.isleaf}
					/>
				);
			});
		} else {
			return [];
		}
	};
	renderTreeTitle = (item, parPk) => {
		let titleIcon, titleInfo;
		titleInfo = <span className="title-middle">{item.refname}</span>;
		//编辑图标
		/* if (this.state.isHoverKey == `${parPk} ${item.refpk}` && item.isleaf) {
			titleIcon = <Icon className="title-middle edit-icon" type="uf-pencil" />;
		} */
		return (
			<div className="title-con">
				{titleInfo}
				{/* {titleIcon} */}
			</div>
		);
	};
	//鼠标进入treeNode
	/* onMouseEnter = (e) => {
		this.setState({
			isHoverKey: e.node.props.eventKey
		});
	}; */
	//鼠标离开treeNode
	/* onMouseLeave = (e, treenode) => {
		this.setState({
			isHoverKey: ''
		});
	}; */
	//选中树节点
	selectTreeNode = (selectedKeys, e) => {
		this.props.selectTreeNode(selectedKeys, e);
	};
	//关闭模态框
	closeModal = (type = '') => {
		this.setState({ showModal: false });
		//type == '' && this.props.resetModalData();
	};
	//打开模态框
	openModal = () => {
		this.setState({ showModal: true });
		this.props.resetModalData()
	};
	//选中自动匹配
	autoMatchSelect = (val) => {
		this.props.autoMatchSelect(val);
	};
	//自动匹配操作
	autoMatchClick = () => {
		this.props.autoMatchClick();
	};
	//改造table可用数据
	buildTableData = (data) => {
		let dataKeys = Object.keys(data);
		let currDatakey = this.props.selectedTreeNode;
		if (dataKeys.length) {
			let defaultTableData = data[currDatakey.key];
			if(defaultTableData){

				return defaultTableData.map((item, index) => {
					return Object.assign(item, { key: index + 1, idx: index + 1 });
				});
			}
		}
		return [];
	};
	//删行操作
	deleteLine = () => {
		let { maptemplateWebVO, selectedTreeNode, json } = this.props;
		let { checkedArray } = this.state;
		let currCheckedArray = checkedArray[selectedTreeNode.key] || {};
		//let currCheckedAll = checkedAll[selectedTreeNode.key] || {};
		let checkedLen = Object.keys(currCheckedArray).length;
		if (checkedLen) {
			const tableData = maptemplateWebVO.docmaps[selectedTreeNode.key];
			let delLines = Object.keys(currCheckedArray);
			for (let index = 0; index < delLines.length; index++) {
				for (let i = 0; i < tableData.length; i++) {
					if (tableData[i].idx == delLines[index]) {
						tableData.splice(i, 1);
					}
				}
			}
			this.props.handleTableData(tableData);
			this.state.checkedArray[selectedTreeNode.key] = {};
			this.state.checkedAll[selectedTreeNode.key] = {};
			this.setState({ checkedArray: this.state.checkedArray, checkedAll: this.state.checkedAll });
			//编辑态，删除成功后不需要提示成功
			// toast({ content: '删除成功！', color: 'success' });
		} else {
			toast({ content: json['200019docmap-000000'], color: 'warning' });/* 国际化处理： 请选择要删除的数据！*/
		}
	};
	//增行操作
	addLine = () => {
		let { maptemplateWebVO, selectedTreeNode, modalData, json } = this.props;
		let { destType, srcType } = modalData;
		const tableData = maptemplateWebVO.docmaps[selectedTreeNode.key] || [];
		if (!maptemplateWebVO.pk_desorgbook) {
			toast({ content: json['200019docmap-000001'], color: 'warning' });/* 国际化处理： 请选择目的组织！*/
			return;
		}
		if (!tableData.length) {
			if (!srcType.refpk || !destType.refpk) {
				toast({ content: json['200019docmap-000002'], color: 'warning' });/* 国际化处理： 请选择来源类型和目标类型！*/
				return;
			}
		}

		let newLine = {};
		if (tableData && tableData.length) {
			newLine = Object.assign({}, tableData[0]);
			delete newLine.pk_docmap;
		} else {
			newLine.srctype = { value: srcType.refpk, display: srcType.refname };
			newLine.destype = { value: destType.refpk, display: destType.refname };
		}
		newLine.desvalue = { value: null, display: null };
		newLine.srcvalue = { value: null, display: null };
		newLine.idx = tableData.length + 1;
		newLine.key = tableData.length + 1;
		tableData.push(newLine);

		this.props.handleTableData(tableData);
	};
	//填充操作
	fillTable = () => {
		let { maptemplateWebVO, selectedTreeNode, json } = this.props;
		let { checkedArray } = this.state;
		let currCheckedArray = checkedArray[selectedTreeNode.key] || {};
		let checkedLen = Object.keys(currCheckedArray).length;
		if (checkedLen) {
			let fillSrc = { display: null, value: null, scale: null };
			let isRepeat = false;
			for (const key in currCheckedArray) {
				let record = currCheckedArray[key].record;
				if (record.desvalue.value && fillSrc.value) {
					if (record.desvalue.value != fillSrc.value) {
						isRepeat = true;
						break;
					}
				}
				if (record.desvalue.value) {
					fillSrc = Object.assign({}, record.desvalue);
				}
			}
			if (isRepeat) {
				toast({ content: json['200019docmap-000003'], color: 'warning' });/* 国际化处理： 选中的记录有两个以上不同的目的值，请重新选取!*/
				return;
			}
			if (!fillSrc.value) {
				toast({ content: json['200019docmap-000004'], color: 'warning' });/* 国际化处理： 选中的记录没有任何目的值可以被用来填充，请重新选取!*/
				return;
			}
			const tableData = maptemplateWebVO.docmaps[selectedTreeNode.key];
			for (const key in currCheckedArray) {
				for (let index = 0; index < tableData.length; index++) {
					if (tableData[index].idx == key) {
						tableData[index].desvalue = Object.assign({}, fillSrc);
					}
				}
			}
			this.props.handleTableData(tableData);
		} else {
			toast({ content: json['200019docmap-000005'], color: 'warning' });/* 国际化处理： 没有任何目的值可以被用来填充，请重新选取!*/
		}
	};
	//整理操作
	arrangeTableData = () => {
		let { selectedTreeNode, maptemplateWebVO } = this.props;
		let tableData = maptemplateWebVO.docmaps[selectedTreeNode.key];
		if (tableData && tableData.length) {
			let postData = [];
			for (let i = 0; i < tableData.length; i++) {
				if (tableData[i].destype.value && tableData[i].desvalue.value) {
					postData.push({ classTypeId: tableData[i].destype.value, pk_doc: tableData[i].desvalue.value });
				}
				if (tableData[i].srctype.value && tableData[i].srcvalue.value) {
					postData.push({ classTypeId: tableData[i].srctype.value, pk_doc: tableData[i].srcvalue.value });
				}
			}
			ajax({
				url: '/nccloud/fipub/ibddata/queryIBDData.do',
				data: postData,
				success: (res) => {
					if (res.success) {
						if (res.data) {
							let resData = res.data;
							for (let j = 0; j < resData.length; j++) {
								for (let i = 0; i < tableData.length; i++) {
									if (
										resData[j].classTypeId == tableData[i].destype.value &&
										resData[j].pk_doc == tableData[i].desvalue.value
									) {
										tableData[i].desvalue.display = `${resData[j].IBDData.code} ${resData[j].IBDData
											.name.text}`;
										break;
									}
									if (
										resData[j].classTypeId == tableData[i].srctype.value &&
										resData[j].pk_doc == tableData[i].srcvalue.value
									) {
										tableData[i].srcvalue.display = `${resData[j].IBDData.code} ${resData[j].IBDData
											.name.text}`;
										break;
									}
								}
							}
							this.props.handleTableData(tableData);
						}
					}
				}
			});
		}
	};
	//表格操作
	handleTable = (props, type) => {
		switch (type) {
			case 'del_line':
				this.deleteLine();
				break;
			case 'add_line':
				this.addLine();
				break;
			case 'fill':
				this.fillTable();
				break;
			case 'arrange':
				this.arrangeTableData();
				break;
			default:
				break;
		}
	};
	//模态框来源类型、目标类型
	handleModalData = (type, val) => {
		this.props.handleModalData(type, val);
	};
	//模态框确定并导入事件--导入数据
	exportData = () => {
		this.props.exportData();
		// this.closeModal('check');
	};
	//模态框确定事件--处理左树数据
	modalOkClick = () => {
		let {modalOkClick} =this.props
		if(modalOkClick){
			modalOkClick()
		}
		// let res = this.props.modalOkClick();
		// res && this.closeModal();
	};
	//点击切换按钮
	toggleContent = () => {
		this.props.toggleContent();
	};
	componentDidMount(){
		this.props.CardBodyRef(this)
	}
	render() {
		let {
			status,
			maptemplateWebVO,
			treeData,
			NCProps,
			modalData,
			autoMatch,
			showContent,
			selectedTreeNode,
			defaultExpandedKeys,
			json
		} = this.props;
		let { showModal, checkedAll, checkedArray } = this.state;
		const tableData = this.buildTableData(maptemplateWebVO.docmaps);
		let treeNodes = this.createTreeNodes(treeData);
		let currCheckedArray = checkedArray[selectedTreeNode.key] || {};
		let currCheckedAll = checkedAll[selectedTreeNode.key] || {};
		const columns = [
			{
				title: (
					<div fieldid='firstcol'>
						<Checkbox
							className="table-checkbox"
							checked={currCheckedAll.checked}
							indeterminate={currCheckedAll.indeterminate}
							onChange={this.onAllCheckChange}
						/>
					</div>
				),
				key: 'checkbox',
				attrcode: 'checkbox',
                dataIndex: 'checkbox',
				className: 'table-checkbox-class',
				visible: true,
				itemtype: 'customer',
				width: 50,
				render: (text, record, index) => {
					let rowChecked =
						currCheckedArray[record.idx] && currCheckedArray[record.idx].checked ? true : false;
					return (
						<div fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={rowChecked}
								onChange={this.onCheckboxChange.bind(this, text, record, index)}
							/>
						</div>
					);
				}
			},
			{ 
				title: (<div fieldid='idx'>{json['200019docmap-000006']}</div>), 
				dataIndex: 'idx', 
				key: 'idx', 
				width: 50,
				render: (text, record, index) => <div fieldid='idx'>{text ? text : emptyCell}</div>
			},/* 国际化处理： 序号*/
			{
				title: (<div fieldid='srctype'>{json['200019docmap-000007']}</div>),/* 国际化处理： 来源类型*/
				dataIndex: 'srctype.display',
				key: 'srctype.display',
				width: 200,
				render: (text, record, index) => <div fieldid='srctype'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='srcvalue'>{json['200019docmap-000008']}</div>),/* 国际化处理： 来源值*/
				dataIndex: 'srcvalue.display',
				key: 'srcvalue.display',
				width: 250,
				render: (text, record, index) => {
					return (
						<EditableCell
							maptemplateWebVO={maptemplateWebVO}
							status={status}
							text={text}
							record={record}
							index={index}
							showContent={showContent}
							handleCompareInfo={this.handleCompareInfo}
							type="src"
						/>
					);
				}
			},
			{
				title: (<div fieldid='destype'>{json['200019docmap-000009']}</div>),/* 国际化处理： 目标类型*/
				dataIndex: 'destype.display',
				key: 'destype.display',
				width: 200,
				render: (text, record, index) => <div fieldid='destype'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='desvalue'>{json['200019docmap-000010']}</div>),/* 国际化处理： 目标值*/
				dataIndex: 'desvalue.display',
				key: 'desvalue.display',
				width: 250,
				render: (text, record, index) => {
					return (
						<EditableCell
							maptemplateWebVO={maptemplateWebVO}
							status={status}
							text={text}
							record={record}
							index={index}
							showContent={showContent}
							handleCompareInfo={this.handleCompareInfo}
							type="des"
						/>
					);
				}
			}
		];
		let { DragWidthCom } = NCProps;
		return (
			<div>
				<NCDiv fieldid="doccard" areaCode={NCDiv.config.FORM}>
					<ReferPannel
						json={json}
						status={status}
						maptemplateWebVO={maptemplateWebVO}
						handleCompareInfo={this.handleCompareInfo}
					/>
				</NCDiv>
				{/* compare-tree-table */}
				<div className="compare-tree-table">
					<div className="tree-table">
						<DragWidthCom
							leftDom={
								<div className="compare-tree">
									<div>
										{NCProps.button.createButtonApp({
											area: 'tree_header',
											onButtonClick: this.openModal
										})}
									</div>
									{!treeNodes || !treeNodes.length ? (
										<div className="no-content-tip">
											<div>
												<p>
													<Icon className="icon-tip uf-exc-c-o" />
												</p>
												<p>{json['200019docmap-000011']}、{json['200019docmap-000012']}，{json['200019docmap-000013']}！</p>
												{/* 国际化处理： 输入来源组织,目的组织信息后,可新增类型*/}
											</div>
										</div>
									) : (
										''
									)}
									<NCDiv fieldid="doccomparison" areaCode={NCDiv.config.TreeCom}>
										<Tree 
											showLine={true}
											onSelect={this.selectTreeNode}
											//openIcon={<i className='treeicon iconfont icon-wenjianjiadakai' />}
											//closeIcon={<i className='treeicon iconfont icon-wenjianjia' />}
										>
											{treeNodes}
										</Tree>
									</NCDiv>
								</div>
							} //左侧区域dom
							rightDom={
								<div className="compare-table">
									<div className="func-btns-area">
										{status != 'browse' ? (
											<div className="match-tenet">
												<span className='nc-theme-common-font-c'>{json['200019docmap-000014']}：</span>{/* 国际化处理： 匹配原则*/}
												<Select
													disabled={status == 'browse' ? true : false}
													//defaultValue={'name'}
													fieldid='matchrule'
													value={autoMatch}
													onSelect={this.autoMatchSelect}
													className="match-tenet-content"
												>
													<Option value="code">{json['200019docmap-000015']}</Option>{/* 国际化处理： 编码*/}
													<Option value="name">{json['200019docmap-000016']}</Option>{/* 国际化处理： 名称*/}
												</Select>
												<Button fieldid='automatch' disabled={tableData.length===0 ? true : false} onClick={this.autoMatchClick}>{json['200019docmap-000017']}</Button>{/* 国际化处理： 自动匹配*/}
											</div>
										) : (
											''
										)}
										<div className="handle-compare-btns">
											{status != 'browse' ? (
												<div>
													{NCProps.button.createButtonApp({
														area: 'table_header',
														onButtonClick: this.handleTable
													})}
												</div>
											) : (
												<div className='toggleContent'>
													<Button size="sm" colors="danger" 
														fieldid='change'
														onClick={this.toggleContent}>
														{json['200019docmap-000018']}{/* 国际化处理： 切换*/}
													</Button>
												</div>
											)}
										</div>
									</div>
									<NCDiv fieldid="doccard" areaCode={NCDiv.config.TableCom}>
										<Table columns={columns} data={tableData} />
									</NCDiv>
								</div>
							} //右侧区域dom
							defLeftWid="20%" // 默认左侧区域宽度，px/百分百
						/>
					</div>

					{/* 左树部分 */}

					{/* 右表部分 */}
				</div>

				<Modal className="type-modal" show={showModal} onHide={this.closeModal} style={{ width: '520px', minHeight: '268px', maxHeigth: '420px' }}>
					<Modal.Header closeButton >
						<Modal.Title>{json['200019docmap-000019']}</Modal.Title>{/* 国际化处理： 增加档案类型*/}
					</Modal.Header>
					<Modal.Body>
						<div className='type-modal-con'>
							<div className="src-type">
								<i className='require-true'>*</i>
								<span className='nc-theme-common-font-c'>{json['200019docmap-000007']}：</span>{/* 国际化处理： 来源类型*/}
								<span className="type-info">
									<BdmdMainentityAndEnumReffrom
										fieldid='srctype'
										isMultiSelectedEnabled={false}
										queryCondition={{ pk_org: maptemplateWebVO.pk_srcorgbook }}
										value={{
											refname: modalData.srcType.refname || '',
											refpk: modalData.srcType.refpk || ''
										}}
										onChange={(val) => {
											this.handleModalData('srcType', val);
										}}
									/>
								</span>
							</div>
							<div className="dest-type">
								<i className='require-true'>*</i>
								<span className='nc-theme-common-font-c'>{json['200019docmap-000009']}：</span>{/* 国际化处理： 目标类型*/}
								<span className="type-info">
									<BdmdMainentityAndEnumReffrom
										fieldid='desttype'
										isMultiSelectedEnabled={false}
										queryCondition={{ pk_org: maptemplateWebVO.pk_desorgbook }}
										value={{
											refname: modalData.destType.refname || '',
											refpk: modalData.destType.refpk || ''
										}}
										onChange={(val) => {
											this.handleModalData('destType', val);
										}}
									/>
								</span>
							</div>
						</div>
					</Modal.Body>
					<Modal.Footer className="text-center">
						<Button fieldid='sure-export' colors="primary" onClick={this.exportData}>
							{json['200019docmap-000020']}{/* 国际化处理： 确定并导入*/}
						</Button>
						<Button fieldid='confirm' onClick={this.modalOkClick}>
							{json['200019docmap-000021']}{/* 国际化处理： 确定*/}
						</Button>
						<Button fieldid='cancel' onClick={this.closeModal} bordered>
							{json['200019docmap-000022']}{/* 国际化处理： 取消*/}
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
}
