import React, { Component } from 'react';
import { ajax, getMultiLang } from 'nc-lightapp-front';
import drawingRef from '../../../util/drawingRef';
const emptyCell = <span>&nbsp;</span>
export default class EditableCell extends Component {
	constructor(props) {
		super(props);
		this.state = {
			editable: false,
			json: {}
		};
	}
	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'200019docmap',domainName:'gl',currentLocale:'simpchn',callback});
	}
	handleCompareInfo = (area, data) => {
		this.props.handleCompareInfo(area, data);
		this.setState({
			editable: false
		})
	};
	//查询refCode
	queryRefer = (record, type) => {
		let { maptemplateWebVO } = this.props;
		let typename = '',typevalue = '';
		if (type == 'src') {
			typename = 'srctype';
			typevalue = 'srcvalue';
		} else if (type == 'des') {
			typename = 'destype';
			typevalue = 'desvalue';
		}
		ajax({
			url: '/nccloud/fipub/ref/queryRefCode.do',
			data: { beanId: record[typename].value, name: record[typename].display },
			success: (res) => {
				if (res.success) {
					if (res.data && res.data.refCode) {
						record[typevalue].refCode = res.data.refCode
						record[typevalue].pk_accountingbook = maptemplateWebVO[`${type}Accountingbook`]
						this.setState({ editable: true, refCode: res.data.refCode, beanId: record[typename].value });
					}
				}
			}
		});
	};
	//加载refer
	renderRefer = (type, index, record, refCode) => {
		let { json } = this.state
		let { maptemplateWebVO } = this.props;
		if (refCode.indexOf('.js') == -1) {
			refCode += '.js';
		}
		let refKey = `${type}_${record.idx}_${index}`;
		
		let typename = '',pk_org = '', pk_defdoclist; 
		if (type == 'src') {
			typename = 'srcvalue';
			pk_defdoclist = record['srctype'].value;
			pk_org = maptemplateWebVO.pk_srcorgbook;
		} else if (type == 'des') {
			typename = 'desvalue';
			pk_defdoclist = record.destype.value;
			pk_org = maptemplateWebVO.pk_desorgbook;
		}
		let pk_accountingbook = record[`${type}value`] ? record[`${type}value`].pk_accountingbook : ''
		let page = this;
		let condition ={
			pk_org, 
			pk_accountingbook,
			pk_defdoclist,
			refcode:refCode,
			isShowUnit:true,
			classid:pk_defdoclist
		}
		let defaultValue = {refpk:record[typename].value, refname:record[typename].display};
		let props = {
			fieldid: {type},
			onChange: (v) => {
				page.handleCompareInfo('table', { name: typename, val:v, index });
			}
		}
		return drawingRef(page, json, refKey, condition, defaultValue, props);
		
	};
	render() {
		let { status, text, record, type, index, showContent } = this.props;
		let { editable } = this.state;
		let content = '';
		if (text) {
			if (showContent == 'name') {
				content = text.split(' ')[1];
			} else if (showContent == 'code') {
				content = text.split(' ')[0];
			}
		}
		let refCode = record[`${type}value`] ? record[`${type}value`].refCode : ''
		return (
			<div style={content ? {} : { height: '100%' }}>
				{status == 'browse' ? (
					<div fieldid={type}>{content ? content : emptyCell}</div>
				) : refCode ? (
					<div fieldid={type}>{this.renderRefer(type, index, record, refCode)}</div>
				) : (
					<div
						fieldid={type}
						style={content ? {} : { height: '100%' }}
						onClick={() => {
							this.queryRefer(record, type);
						}}
					>
						{content}
					</div>
				)}
			</div>
		);
	}
}
