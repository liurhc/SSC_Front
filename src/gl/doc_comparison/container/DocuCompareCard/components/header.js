import React, { Component } from 'react';
import { ajax, base, high, createPage, createPageIcon } from 'nc-lightapp-front';
const { NCButton: Button, NCButtonGroup: ButtonGroup, NCBackBtn: BackBtn } = base;
import HeaderArea from '../../../../public/components/HeaderArea';

class CardHeader extends Component {
	constructor(props) {
		super(props);
	}
	headerBtnsHandle = (props, type) => {
		this.props.headerBtnsHandle(props, type);
	};
	browseItem = (type) => {
		this.props.browseItem(type);
	};
	render() {
		let { status, NCProps, pageBtnStatus, json } = this.props;
		return (
			<HeaderArea 
				title = {this.props.json['200019docmap-000025']}
				initShowBackBtn = {status==='browse' ? true : false}
				backBtnClick = {this.headerBtnsHandle.bind(this, '', 'back')}
				btnContent = {NCProps.button.createButtonApp({
					area: 'page_header',
					onButtonClick: this.headerBtnsHandle
				})}
				pageBtnContent = {status == 'browse' ? (
					<ButtonGroup className='btn-group'>
						<Button shape="border" disabled={pageBtnStatus.prevBtn} onClick={this.browseItem.bind(this, 'first')}>
							<i className="iconfont icon-shangyiye" />
						</Button>
						<Button shape="border" disabled={pageBtnStatus.prevBtn} onClick={this.browseItem.bind(this, 'prev')}>
							<i className="iconfont icon-jiantouzuo" />
						</Button>
						<Button shape="border" disabled={pageBtnStatus.nextBtn} onClick={this.browseItem.bind(this, 'next')}>
							<i className="iconfont icon-jiantouyou" />
						</Button>
						<Button shape="border" disabled={pageBtnStatus.nextBtn} onClick={this.browseItem.bind(this, 'last')}>
							<i className="iconfont icon-xiayiye" />
						</Button>
					</ButtonGroup>
				) : (
					''
				)}
			/>
		);
	}
}

CardHeader = createPage({})(CardHeader)
export default CardHeader