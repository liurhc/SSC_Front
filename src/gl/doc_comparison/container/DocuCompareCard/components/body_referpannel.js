import React, { Component } from 'react';
import { base } from 'nc-lightapp-front';
import BusinessUnitTreeRef from '../../../../../uapbd/refer/org/BusinessUnitTreeRef';
const { NCFormControl: FormControl } = base;

export default class ReferPannel extends Component {
	constructor(props) {
		super(props);
	}
	//来源组织、目的组织改变回调
	handleCompareInfo = (area, data) => {
		this.props.handleCompareInfo(area, data);
	};
	render() {
		let { maptemplateWebVO, status, json } = this.props;
		const { desname, pk_desorgbook, srcname, pk_srcorgbook, templetname, templetcode } = maptemplateWebVO;
		return (
			<div className="nc-bill-search-area referpannel">
				<div className="refer-info">
					<span className="title nc-theme-common-font-c"><i className='require-true'>*</i>{json['200019docmap-000023']}：</span>{/* 国际化处理： 来源组织*/}
					<span className="info-content">
						<BusinessUnitTreeRef
							fieldid='srcOrg'
							disabled={true}
							value={{
								refname: srcname,
								refpk: pk_srcorgbook
							}}
							onChange={(val) => {
								this.handleCompareInfo('form', { name: 'srcOrg', val });
							}}
						/>
					</span>
				</div>
				<div className="refer-info">
					<span className="title nc-theme-common-font-c"><i className='require-true'>*</i>{json['200019docmap-000024']}：</span>{/* 国际化处理： 目的组织*/}
					<span className="info-content">
						<BusinessUnitTreeRef
							fieldid='destOrg'
							disabled={true}
							value={{
								refname: desname || '',
								refpk: pk_desorgbook || ''
							}}
							onChange={(val) => {
								this.handleCompareInfo('form', { name: 'destOrg', val });
							}}
						/>
					</span>
				</div>
				<div className="refer-info">
					<span className="title nc-theme-common-font-c"><i className='require-true'>*</i>{json['200019docmap-000015']}：</span>{/* 国际化处理： 编码*/}
					<span className="info-content">
						<FormControl
							fieldid='templetcode'
							disabled={status == 'browse' ? true : false}
							value={templetcode}
							onChange={(val) => {
								this.handleCompareInfo('form', { name: 'templetcode', val });
							}}
						/>
					</span>
				</div>
				<div className="refer-info">
					<span className="title nc-theme-common-font-c"><i className='require-true'>*</i>{json['200019docmap-000016']}：</span>{/* 国际化处理： 名称*/}
					<span className="info-content">
						<FormControl
							fieldid='templetname'
							disabled={status == 'browse' ? true : false}
							value={templetname}
							onChange={(val) => {
								this.handleCompareInfo('form', { name: 'templetname', val });
							}}
						/>
					</span>
				</div>
			</div>
		);
	}
}
