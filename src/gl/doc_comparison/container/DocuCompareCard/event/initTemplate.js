import { ajax } from 'nc-lightapp-front';
export default function initTemplate(props) {
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do',
		data: {
			pagecode: '200019docmap_card',
			appcode: '200019docmap'
		},
		success: (res) => {
			if (res.success) {
				if (res.data) {
					props.button.setButtons(res.data);
					let { status } = this.state;
					if (status != 'browse') {
						props.button.setButtonsVisible([ 'add', 'delete', 'update' ], false);
					} else {
						props.button.setButtonsVisible([ 'save', 'cancel', 'line_group', 'add_type' ], false);
					}
					props.button.setButtonsVisible([ 'del_type' ], false);
				}
			}
		}
	});
}
