import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import DocuCompareCard from '../../container/DocuCompareCard';
import '../../../public/reportcss/firstpage.less';

ReactDOM.render(<DocuCompareCard />, document.querySelector('#app'));
