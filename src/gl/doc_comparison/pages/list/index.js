import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import DocuCompareList from '../../container/DocuCompareList';
import '../../../public/reportcss/firstpage.less';

ReactDOM.render(<DocuCompareList />, document.querySelector('#app'));
