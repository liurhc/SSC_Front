/**
 * 常用请求
 */
import {ajax,toast} from 'nc-lightapp-front';
import myAjax from '../../../fipub/public/common/ajax';

function getAccountBookCombineInfo(pk_accountingbook){
    let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
    let data = {
        pk_accountingbook:pk_accountingbook
    }
    let result = {};
    ajax({
        url,
        data,
        async:false,
        success:function(response){
            let {data, success} = response;
            if(success){
                result = data;
            }
        },
        error:function(error){
            toast({ content: '请求失败', color: 'warning' });               
        }
    });
    return result;
}

function getContext(){
    let url='/nccloud/platform/appregister/queryappcontext.do';
    let data = {};
    let result = {};
    ajax({url, data, async:false,
        success:function(response){
            let {data, success} = response;
            if(success){
                result = data;
            }    
        },
        error:function(error){
            toast({ content: '请求失败', color: 'warning' });               
        }
    });
    return result;
}


let commonApi = {
    /**
     * 查询核算账簿相关信息
     */
    queryBookCombineInfo:(opt) => {
        ajax({
            url:'/nccloud/gl/voucher/queryBookCombineInfo.do',
            data:{pk_accountingbook:opt.data},
            success:opt.success
        });
    },
    /**
     * 查询环境信息
     */
    getContext:(data, callback) => {
        ajax({
            url:'/nccloud/platform/appregister/queryappcontext.do',
            data,
            success:(res) => {
                if(callback){
                    callback(res);
                }
            }
        });
    },

    /**
     * 根据组织和账簿类型信息查询账簿
     */
    qryAccBookByOrgAndSetOfBook:(data, callback) => {
        ajax({
            url:'/nccloud/gl/glpub/qryAccBookByOrgAndSetOfBook.do',
            data,
            success:(res)=>{
                if(callback){
                    callback(res);
                }
            }
        });
    },

    /**
     * 根据核算账簿查询会计年
     */
    qryYearByAccountingbook:(data, callback)=>{
        ajax({
            url:'/nccloud/gl/voucher/yearcombo.do',
            data,
            success:(res)=>{
                if(callback){
                    callback(res);
                }
            }
        });
    },

    /**
     * 根据核算账簿会计年查询会计期间
     */
    queryPeriodByBookAndYear:(data, callback) => {
        ajax({
            url:'/nccloud/gl/voucher/periodcombo.do',
            data,
            success:(res)=>{
                if(callback){
                    callback(res);
                }
            }
        });
    },
    queryAssId:(data, callback) =>{
        let url = '/nccloud/gl/voucher/queryAssId.do';
        myAjax({url, data, successcb:callback});
    },
    queryAssTypeAndValue:(data, callback) => {
        let url = '/nccloud/gl/voucher/queryAssTypeAndValue.do';
        myAjax({url, data, successcb:callback});
    },

    /**
     * data:{prepareddate:'', pk_accasoa}
     */
    queryAssItem:(data, callback) => {
        let url = '/nccloud/gl/voucher/queryAssItem.do';
        myAjax({url, data, successcb:callback});
    },
    /* 根据核算账簿查询财务组织 */
    queryOrgByAccountBook:(data, callback) => {
        let url = '/nccloud/gl/glpub/queryFinanceOrg.do';
        myAjax({url, data, successcb:callback});        
    }
}

export {getAccountBookCombineInfo, getContext, commonApi};