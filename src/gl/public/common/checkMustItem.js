import React, { Component } from "react";
import {ajax ,toast} from "nc-lightapp-front";
export default function checkMustItem(dataObj){
    let str=''
    let flag=true;
    for(let k in dataObj){
        if(dataObj[k]&&typeof(dataObj[k])=='object'){
            if(dataObj[k].constructor===Array||(dataObj[k] instanceof Array)){//数组
                if(dataObj[k].length>0&&dataObj[k][0]){
                    if(dataObj[k][0].isMustItem){
                        if(!dataObj[k][0].value){
                            str+=dataObj[k][0].itemName+',';
                            flag=false;
                        }
                    }
                }
            }else{
                if(dataObj[k].isMustItem){
                    if(!dataObj[k].value){
                        str+=dataObj[k].itemName+',';
                        flag=false;
                    }
                }
            }
        }
    }
    let backData={'info':str,'flag':flag};
    return backData;
}