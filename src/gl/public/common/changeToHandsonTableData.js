import React, { Component } from "react";
import {ajax ,toast} from "nc-lightapp-front";
 //转换成handsontable需要的数据格式
export default function changeToHandsonTableData(self,data,columnInfo,headtitle,busirecon,index){
 let {verifyBalancesData,querycondition,resourceDatas,textAlginArr,flag,flow}=self.state;
if(flag>=2){
    flag=0;
}else{
    flag++;
}
 // resourceDatas=data;
 let rows = [];   //表格显示的数据合集
 let columHead = []; //第一层表头
 let parent = []; //存放一级表头，二级表头需要补上
 let parent1 = []; //存放二级表头，三级表头需要补上
 let child1 = []; //第三层表头
 let child2 = []; //第二层表头
 let colKeys = [];  //列key值，用来匹配表体数据
 let colAligns=[];  //每列对齐方式
 let colWidths=[];  //每列宽度
 let oldWidths=[];
 let checkTableHeadNum='0'//表头层数
 columnInfo.forEach(function (value) {//column:[]原始数据表头信息
     let valueCell = {};
     // valueCell.title = value.title;
     // valueCell.key = value.key;
     // valueCell.align = 'center';
     // valueCell.bodyAlign = 'left';
     // valueCell.style = 'head';
     if(value.children){
         // self.setState({
         //     flow: true,
         // });
         flow=true;
         if(parent.length>0){
             child1.push(...parent);
         }
         for(let i=0;i<value.children.length;i++){
            if(value.children[i].children){
                checkTableHeadNum='2';
                 child2.push(...parent1);
                 for(let k=0;k<value.children[i].children.length;k++){
                     let child1Cell = {};
                     child1Cell.title = value.children[i].children[k].title;
                     child1Cell.key = value.children[i].children[k].key;
                     child1Cell.align = 'center';
                     child1Cell.bodyAlign = 'left';
                     child1Cell.style = 'head';
                     let child2Cell = {};
                     child2Cell.title = value.children[i].children[k].title;
                     child2Cell.key = value.children[i].children[k].key;
                     child2Cell.align = 'center';
                     child2Cell.bodyAlign = 'left';
                     child2Cell.style = 'head';
                     columHead.push(child1Cell);
                     child1.push(child1Cell);
                     child2.push(child2Cell);
                     colKeys.push(value.children[i].children[k].key);
                     colAligns.push(value.children[i].children[k].align);
                     colWidths.push({'colname':value.children[i].children[k].key,'colwidth':value.children[i].children[k].width});
                 }
                 parent1 = [];
            }else {
                checkTableHeadNum='1';
                 let innerCell2 = {};
                 innerCell2.title = value.children[i].title;
                 innerCell2.key = value.children[i].key;
                 innerCell2.align = 'center';
                 innerCell2.bodyAlign = 'left';
                 innerCell2.style = 'head';
                 columHead.push(innerCell2);
                 child1.push(innerCell2);
                 parent1.push(innerCell2);
                 colKeys.push(value.children[i].key);
                 colAligns.push(value.children[i].align);
                 colWidths.push({'colname':value.children[i].key,'colwidth':value.children[i].width});
             }
         }
         parent = [];
     }else{
        checkTableHeadNum='0';
         let cellObj = {};
         cellObj.title = value.title;
         cellObj.key = value.key;
         cellObj.align = 'center';
         cellObj.bodyAlign = 'left';
         cellObj.style = 'head';
         columHead.push(cellObj);
         parent1.push(cellObj);
         parent.push(cellObj);
         colKeys.push(value.key);
         colAligns.push(value.align);
         colWidths.push({'colname':value.key,'colwidth':value.width});
     }
 });
 let columheadrow = 0; //表头开始行
//  if (headtitle&&headtitle.length>0){
//      columheadrow = 1;
//      let headtitleArr = [];
//      headtitle.forEach(function (value) {
//        headtitleArr.push(value[0]);
//          headtitleArr.push(value[1]);
//      });
//      for(let i=headtitleArr.length;i<columHead.length;i++){
//        headtitleArr.push('');
//      }
//     //  /rows.push(headtitleArr);
//  }

 let mergeCells = [];
 let row,col,rowspan,colspan;
 let headcount = 1; //表头层数
 if(child1.length>0){
     headcount++;
 }
 if(child2.length>0){
     headcount++;
 }
 let currentCol = 0;
 //计算表头合并格
 for(let i=0;i<columnInfo.length;i++) {
     let value =columnInfo[i];
     //*********
     if(value.children){//有子元素的
         let childLeng = value.children.length;
         let headCol = currentCol;
         for(let j=0;j<value.children.length;j++){
             if(value.children[j].children){
                 let childlen = value.children[j].children.length;
                 mergeCells.push([1,currentCol,1,currentCol+childlen-1]);
                 currentCol = currentCol+childlen;
                 for(let k=0;k<value.children[j].children.length;k++){
                     textAlginArr.push(value.children[j].children[k].align);
                 }
             }
             else {
                 mergeCells.push([columheadrow+1,currentCol,headcount-1,currentCol]);
                 currentCol++;
                 textAlginArr.push(value.children[j].align);
             }
         }
         mergeCells.push([columheadrow,headCol,columheadrow,currentCol-1]);
     }else{//没有子元素对象
         mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
         currentCol++;
         textAlginArr.push(value.align);
     }
 }
 if(parent.length>0){
     child1.push(...parent);
 }
 if(child2.length>0&&parent1.length>0){
     child2.push(...parent1);
 }

 rows.push(columHead);
 if(child1.length>0 && flow){
     rows.push(child1);
 }
 if(child2.length>0 &&flow){
     rows.push(child2);
 }
 if(data!=null && busirecon.length>0){
     let rowdata = [];
     let flag = 0;
     for(let i=0;i<busirecon.length;i++){
         flag++;
         for(let j=0;j<colKeys.length;j++){
             // {
             //     "title":"11163",
             //     "key":"pk_org",
             //     "align":"left",
             //     "style":"body"
             // }
             let itemObj = {
                 align: 'left',
                 style: 'body'
             };
             itemObj.title = busirecon[i][colKeys[j]];
             itemObj.key = colKeys[j] + flag;
             //增加几个属性值供联查使用
             itemObj.link=busirecon[i].link;
             rowdata.push(itemObj)
         }
         rows.push(rowdata);
         rowdata = [];
     }
 }
 let rowhighs = [];
 for (let i=0;i<rows.length;i++){
     rowhighs.push(23);
 }
 self.state.dataout.data.cells=[];
 self.state.dataout.data.cells= rows;//存放表体数据
 self.state.dataout.data.widths=[]
 self.state.dataout.data.widths=colWidths;
 oldWidths.push(...colWidths);
 self.state.dataout.data.oldWidths = oldWidths;
 self.state.dataout.data.rowHeights=rowhighs;
 let frozenCol;
 for(let i=0;i<colAligns.length;i++){
     if(colAligns[i]=='center'){
         frozenCol = i;
         break;
     }
 }
 self.state.dataout.data.fixedColumnsLeft = 0; //冻结  frozenCol
 self.state.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结
 let quatyIndex = [];
 for (let i=0;i<colKeys.length;i++){
     if(colKeys[i].indexOf('quantity')>0){
         quatyIndex.push(i);
     }
 }


 if(mergeCells.length>0){
     self.state.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
 }
 let headAligns=[];

 for(let i=0;i<headcount+columheadrow;i++){
     for(let j=0;j<columHead.length;j++){
         headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
     }
 }
 self.state.dataout.data.cell= headAligns,
     self.state.dataout.data.cellsFn=function(row, col, prop) {
         let cellProperties = {};
         if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
             cellProperties.align = colAligns[col];
             cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
         }
         return cellProperties;
     }
    //  self.state.dataout.data.pk_report='12345678901234567890';
    // self.state.dataout.data.userid='liuhuit';
    self.state.dataout.data.checkTableHeadNum=(headcount-1).toString();
    if (index == 1) {
        self.setState({
            dataout:self.state.dataout,
            showTable: true,
            headtitle: data.headtitle,
            MainSelectModalShow: false,
            isQueryShow:false,
            textAlginArr,
            flag,
            flow:false
        },()=>{
            self.props.button.setButtonVisible(['print', 'link','refresh','switch'], true);
        });
    } else {
            self.setState({
                dataout:self.state.dataout,
                showTable: true,
                gData: data.treeData ? data.treeData : [{key: '', name: '会计科目'}],
                headtitle: data.headtitle,
                MainSelectModalShow: false,
                isQueryShow:false,
                textAlginArr,
                flag,
                flow:false
            },()=>{
                self.props.button.setButtonVisible(['print', 'link','refresh','switch'], true);
            });
    }
    // self.setState({
    //     dataout:self.state.dataout,
    //     showTable: true,
    //     isQueryShow:false,
    //     textAlginArr,
    //     flag,
    //     flow:false
    // },()=>{
    //     self.props.button.setButtonVisible(['print', 'link','refresh','switch'], true);
    // });
 // self.handleQuery()
}