import {base, getBusinessInfo,toast} from 'nc-lightapp-front';
const {NCMessage} = base;

/**
 * 取得业务日期字符串（2001-01-01）
 */
let getSysDateStr = function(){ 
    // return '2018-07-26';
    let businessInfo = getBusinessInfo();
        let buziDate = businessInfo.businessDate;
        if(buziDate){
            return buziDate.split(' ')[0];
        }
    return undefined;
}
/**
 * 获取如下格式日期：（2001-01-01 hh:mm:ss）
 */
let getVersionDateStr = function(){ 
    // return '2018-07-26';
    let businessInfo = getBusinessInfo();
        let buziDate = businessInfo.businessDate;
        if(buziDate){
            return buziDate;
        }
    return undefined;
}
let getSysCode = function(props){
    return {
        appcode: props.getUrlParam('appcode') || props.getSearchParam('c'),
        pageid: props.getUrlParam('pagecode') || props.getSearchParam('p'),
        pagecode: props.getUrlParam('pagecode') || props.getSearchParam('p')
    }
}
let linkTo = function(props, url, param){
    // // TODO: 
    // let newUrl = url + '#';
    // for(let name in param){
    //     newUrl = newUrl + '&'+ name + '=' + param[name];
    // }
    // window.location.href = newUrl;
    props.linkTo(url, param);
}
let pushTo = function(props, url, param){
    // // TODO: 
    // let newUrl = url + '#';
    // for(let name in param){
    //     newUrl = newUrl + '&'+ name + '=' + param[name];
    // }
    // window.location.href = newUrl;
    props.pushTo(url, param);
}
let checkEditForTreeTable = function(props, areaId){
    let tid= props.syncTree.getSelectNode(areaId);
    if(tid == null){
        let multiLang = props.MutiInit.getIntl(2002); 
        // 2002-0005: 请先选择
        toast({color:"warning",content:multiLang && multiLang.get('2002-0005')});
        // NCMessage.create({content: multiLang && multiLang.get('2002-0005'), colork: 'warning', position: 'bottomRight'});
        return false;
    }
    return true;
}
let setSelectedRowEditable = function(props, areaId, focusRow){
    let checkedRows = props.editTable.getCheckedRows(areaId);
    let allRows = props.editTable.getAllRows(areaId, true);
    if(checkedRows != null && checkedRows.length != 0){
        allRows.map((one, index)=>{
            props.editTable.setEditableRowByRowId(areaId, one.rowid, false);        
        })

        checkedRows.map((one, index)=>{
            // props.editTable.setEditableRowByRowId(areaId, rowid, flag);        
        })
    }
    
}
let deleteDditTableRowByIndex = function(props, areaId, index){
    let listData = props.editTable.getAllData(areaId);
    listData.rows.splice(index, 1);
    props.editTable.setTableData(areaId, listData);
}
const MESSAGETYPE = {
    SAVE:'SAVE',
    DELETE:'DELETE',
    REFRESH:'REFRESH',
    WARNING:'WARNING',
}
/**
 * 显示处理结束消息
 * @param {string type} 消息类型
 */
let showMeggage = function(props, type,page){
    switch(type){
        case MESSAGETYPE.SAVE:
            // 2002-0006: 保存成功
            toast({color:"success",content:page.main.state.json['2002-0001']});
        break;
        case MESSAGETYPE.DELETE:
            // 2002-0006: 删除成功
            toast({color:"success",content: page.main.state.json['2002-0006']});
        break;
        case MESSAGETYPE.REFRESH:
            toast({color:"success",title: page.main.state.json['2002-0008']});
        break;
        case MESSAGETYPE.WARNING:
            toast({color:"warning",content: page.main.state.json['2002-0009']});
        break;
    }
}
/**
 * 取得删除行数据
 * @param {*} props 
 * @param {*} areaId 
 * @param {*} index 
 */
let getEdittableDeleteRowDataByIndex=function(props, areaId, index){
    let data = props.editTable.getAllData(areaId);
    data.rows = [data.rows[index]];
    data.rows[0].status = '3';
    return data;
}
/**
 * 取得批量删除行数据
 * @param {*} props 
 * @param {*} areaId 
 * @param {*} index 
 */
let getEdittableDeleteRowDataByIndexs=function(props, areaId, index){
    let data = props.editTable.getAllData(areaId);
    let copyData=props.editTable.getAllData(areaId);
    if(index.length>0){
        data.rows=[];
        for(let i=0;i<index.length;i++){
            data.rows[i]=copyData.rows[index[i]];
            data.rows[i].status = '3';
        }
    }
    return data;
}
/**
 * 单页模式修改URL参数
 * @param {*} props 
 * @param {*} data 
 */
let setUrlParamForHash=function(props, data){
    let hash = (location.hash.split('?')[0]).substring(1);
    let oldParams = (location.hash.split('?')[1]).split('&');
    props.pushTo(hash, data);
}
export default {pushTo, linkTo, checkEditForTreeTable, setSelectedRowEditable, 
    deleteDditTableRowByIndex, MESSAGETYPE, showMeggage, getEdittableDeleteRowDataByIndex,getEdittableDeleteRowDataByIndexs,
    getSysCode,getSysDateStr,getVersionDateStr,setUrlParamForHash
}