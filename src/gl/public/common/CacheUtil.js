const context = 'context';
class CacheUtils{
    constructor(ViewModel, dataSource){
        this.ViewModel = ViewModel;
        this.dataSource = dataSource;
    }

    setData(key, data){
        let dataPool = this.ViewModel.getData(this.dataSource);
        if(!dataPool){
            dataPool = {};
        }
        dataPool[key] = data;
        this.ViewModel.setData(this.dataSource, dataPool);
    }

    getData(key){
        let dataPool = this.ViewModel.getData(this.dataSource);
        let data;
        if(dataPool){
            data = dataPool[key];
        }
        return data;
    }

    setContext(data){
        this.setData(context, data);
    }

    getContext(){
        return this.getData(context);
    }

    clearAll(){
        this.ViewModel.setData(this.dataSource, {});
    }

}

export default CacheUtils;