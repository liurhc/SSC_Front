
 //只提取汉字  
function  GetChinese(strValue)  {
    if (strValue !=  null  &&  strValue !=  "") {
        var  reg  =  /[\u4e00-\u9fa5]/g;
        if(strValue.match(reg)){
            return  strValue.match(reg).join("");
        }else{
            return "";
        }
        
    }
    else
        return  "";
}
//去掉汉字  
function  RemoveChinese(strValue)  {
    if (strValue !=  null  &&  strValue  !=  "") {
        var  reg  =  /[\u4e00-\u9fa5]/g;
        if(strValue.match(reg)){
            return  strValue.replace(reg, "");
        }else{
            return  strValue;
        }
    }
    else
        return  "";
} 
export { GetChinese, RemoveChinese}