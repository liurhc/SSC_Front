/**
 * 字符串格式校验工具
 */


const REG = {
    NUM : {
        POS_INT : /^[1-9]+[0-9]*$/
    }
}

function regNum(pattern, str){
    return pattern.test(str);
}

const RegUtils = {
    regNum : regNum
}

export {RegUtils, REG};

