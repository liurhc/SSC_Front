//核销情况查询报表
export const gl_pkreport_verifyDetails = '20021VYQRY';
//核销余额表
export const gl_pkreport_verifyBalances = '20021VYBALPAGE';
//往来账龄分析
export const gl_pkreport_accountAgeAnalysis = '20021DLANLPAGE';
//摘要汇总表
export const gl_pkreport_summaryall = '20023025PAGE';
//科目余额表
export const gl_pkreport_accbalance = '20023005PAGE';
//科目汇总表
export const gl_pkreport_accall = '20023020';
//三栏式总账
export const gl_pkreport_threeall = '20023010';
//三栏式明细账
export const gl_pkreport_threedetail = '20023030PAGE';


