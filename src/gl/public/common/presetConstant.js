export default {
    /**
     * 页面状态
     */
    pageStatus:{
        /**
         * 新增
         */
        ADD: 'add',
        /**
         * 编辑
         */
        Edit: 'edit',
        /**
         * 浏览
         */
        BROWSE: 'browse'
    }
}