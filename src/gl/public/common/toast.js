import { toast } from "nc-lightapp-front"

function toastSuccess(content){
    let newParams = {content:content};
    let colorInfo = {color:'success'};
    baseToast(colorInfo, newParams);
}

function toastInfo(content){
    let newParams = {content:content};
    let colorInfo = {color:'info'};
    baseToast(colorInfo, newParams);
}

function toastWarning(content){
    let newParams = {content:content};
    let colorInfo = {color:'warning'};
    baseToast(colorInfo, newParams);
}

function toastDanger(content){
    let newParams = {content:content};
    let colorInfo = {color:'danger'};
    baseToast(colorInfo, newParams);
}

function baseToast(colorInfo, newParams){
    let params = {};
    if(newParams) Object.assign(params, newParams);
    if(colorInfo) Object.assign(params, colorInfo);
    toast(params);
}

export {toastSuccess, toastInfo, toastWarning, toastDanger};