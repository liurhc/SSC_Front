import { ajax } from 'nc-lightapp-front';

/* 源appcode */
const originAppcode = {
    triaccbook: '20023010', // 三栏式总账
    detailbook: '20023030', // 三栏式明细账
    assbalance: '20023055', // 辅助余额表
    assdetail:  '20023060', // 辅助明细账
    journal:    '20023040', // 序时账
};

/**
 * 查询有权限的同源appcode
 * @param originAppcode 源appcode
 * @returns 与该appcode同源的有权限的第一个appcode（包括源appcode本身和复制的）
 */
function queryRelatedAppcode(originAppcode) {
    let url = '/nccloud/gl/pub/relatedappcode.do';
    let data = {
        appcode: originAppcode
    }
    let result = '';
    ajax({
        url,
        data: data,
        async: false,
        success: function (response) {
            if (response.success) {
                result = response.data
            }
        }
    });
    return result
}
export { queryRelatedAppcode, originAppcode }