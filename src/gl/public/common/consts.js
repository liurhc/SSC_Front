const VOStatus = {
    UNCHANGED : '0',
    UPDATED : '1',
    NEW : '2',
    DELETED : '3'
}

const NULL_STR = '~';

/* 全局组织主键 */
const GLOBAL_ORG = 'GLOBLE00000000000000';

export {VOStatus, NULL_STR, GLOBAL_ORG};