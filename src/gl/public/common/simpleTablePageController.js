import {cardCache} from "nc-lightapp-front";
let {setDefData, getDefData } = cardCache;
import pubUtil from './pubUtil'
class simpleTablePageController{
    constructor(main, props, meta, initFlag) {
        this.main = main;
        this.props = props;
        this.dataSource = meta.dataSource;
        this.listPkField = meta.listPkField;
        this.listId = meta.listId;
        this.listButtonArea = meta.listButtonArea;
        this.cardUrl = meta.cardUrl;
        this.cardPageCode = meta.cardPageCode;
        // 内部参数
        this.pkList = [];
        this.cacheData = getDefData('cacheData_Key', this.dataSource) || {};
        if(initFlag!=false){
            // 页面初始化
            this.initTemplate();       
        }
    }

    initTemplate(){
        this.props.createUIDom(
            {},
            (data) => {
                let meta = data.template;
                this.initMeta(meta);
                this.initRefParam(meta);
                window.setTimeout(() => {
                    this.addListOpr(meta, this.listId, this.listButtonArea);
                    this.props.meta.setMeta(meta);
                    this.props.button.setButtons(data.button);
                    //添加按钮
                    this.props.button.setPopContent('DelLine',this.main.state.json['2002-0014']);
                    // TODO：缓存
                    // if(!this.props.table.hasCacheData(this.dataSource)){
                        this.callBackInitTemplate();
                        this.queryData(data);
                    // }
                }, 1);
            }
        )
    }
    queryData(key){
        
    }
    /**
     * 初始化模板
     * @param {*} meta 
     */
    initMeta(meta){
        
    }
    initRefParam(meta){

    }
    callBackInitTemplate(){

    }
    /**
     * 设置表格数据
     * @param {*data} 数据 
     */
    setTableData(data){
        this.pkList = [];
        if(data.data != null && data.data[this.listId] != null){
            let allpks = [];
            if(this.listPkField != null){
                data.data[this.listId].rows.map((one)=>{
                    if((((one || {}).values || {})[this.listPkField] || {}).value != null){
                        this.pkList.push(one.values[this.listPkField].value);
                    }
                })
                data.data[this.listId].rows.map((one)=>{
                    if((((one || {}).values || {})[this.listPkField] || {}).value != null){
                        allpks.push(one.values[this.listPkField].value);
                    }
                })
            }
            data.data[this.listId].allpks = allpks;
            this.props.table.setAllTableData(this.listId, data.data[this.listId]);
        }else{
            this.props.table.setAllTableData(this.listId, {rows:[]});
        }
    }
    /**
     * 添加操作列按钮
     */
    addListOpr(meta, AreaId, buttonAreaId){
        let event = {
            label: this.main.state.json['2002-0002'],
            attrcode: 'opr',
            itemtype: 'customer',
            visible: true,
            width: '150px',
            fixed: 'right',
            render: (text, record, index) =>{
                return <div onClick={(e)=>{ e.stopPropagation()}}>
                {this.props.button.createOprationButton(this.getListButtons(text, record, index), {
                    area: buttonAreaId,
                    buttonLimit: 3,
                    onButtonClick: (props, btnKey) => {
                        this.listButtonClick(AreaId, btnKey, record, index);
                    }
                })
                }
            </div>
            }
        };
        meta[AreaId].items.push(event);
    }
    /**
     * 取得表格行按钮
     * @param {*} text 
     * @param {*} record 
     * @param {*} index 
     */
    getListButtons(text, record, index){
        return ['Edit', 'DelLine'];
    }

    /**
     * 表头按钮事件
     * @param {string buttonId} 按钮ID  
     */
    headButtonClick(buttonId){
        switch(buttonId){
            case 'Add':
                this.Add();
            break;
            case 'Refresh':
                this.Refresh();
            break;
            default:
            this[buttonId]();
        }
    }
    /**
     * 设置页面间跳转缓存数据
     */
    setCacheData(data){
        this.cacheData = data;
        setDefData('cacheData_Key', this.dataSource, data);
    }
    /**
     * 设置页面间跳转缓存数据
     */
    getCacheData(){
        return null;
    }
    /**
     * 设置翻页组件allpks
     */
    setAllPks(){
        // cacheTools.set('allpks',this.pkList);
    }
    /**
     *新增按钮事件
     */
    Add(){
        let newCacheData = this.getCacheData();
        if(newCacheData){
            this.setCacheData(newCacheData);
        }
        this.setDataSource();
        pubUtil.pushTo(this.props, this.cardUrl,{
            appcode: this.props.getSearchParam('c'),
            pagecode: this.cardPageCode,
            status: 'add',
            open_status: 'add'
        });   
    }

    /**
     * 列表行双击事件
     */
    listRowDoubleClick(record, index){
        this.setAllPks();
        this.setDataSource();
        let newCacheData = this.getCacheData();
        if(newCacheData){
            this.setCacheData(newCacheData);
        }
        this.setDataSource(record, index);
        pubUtil.pushTo(this.props, this.cardUrl,{
            appcode: this.props.getSearchParam('c'),
            pagecode: this.cardPageCode,
            status: 'browse',
            open_status: 'browse',
            id: record[this.listPkField].value,
            ...this.AddListRowDoubleClickParam(record, index)
        }); 
    }
    /**
     * 双击表体行页面跳转增加参数
     */
    AddListRowDoubleClickParam(record, index){
        return {};
    }
    

    /**
     * 列表按钮事件
     */
    listButtonClick(AreaId, actionId, data, index){
        switch(actionId){
            case 'DelLine':
                this.DelLine(AreaId, data, index);
            break;
            case 'Edit':
                this.Edit(AreaId, data, index);
            break;
            default:
                this[actionId](AreaId, data, index);
        }
    }
    /**
     * 删行按钮事件
     * @param {*data} 行数据 
     * @param {*index} 行号
     */
    DelLine(AreaId, data, index){
        this.doDelLine(data, index);
    }
    /**
     * 行编辑按钮事件
     * @param {*data} 行数据 
     * @param {*index} 行号
     */
    Edit(AreaId, data, index){
        this.setAllPks();
        let newCacheData = this.getCacheData();
        if(newCacheData){
            this.setCacheData(newCacheData);
        }
        this.setDataSource(data, index);
        pubUtil.pushTo(this.props, this.cardUrl,{
            appcode: this.props.getSearchParam('c'),
            pagecode: this.cardPageCode,
            status: 'edit',
            open_status: 'edit',
            id: data[this.listPkField].value
        }); 
    }
    /**
     * 删行处理
     * @param {*} data 
     */
    doDelLine(data, index){
        
    }
    /**
     * 删除回掉方法
     * @param {*data} 返回数据
     */
    callBackDelLine(data, index, pk){
        this.props.table.deleteTableRowsByIndex(this.listId, index);
        // TODO：缓存
        // this.props.table.deleteCacheId(this.listId, pk);
        pubUtil.showMeggage(this.props, pubUtil.MESSAGETYPE.DELETE,this);
    }

    /**
     * 设置缓存数据
     */
    setDataSource(){

    }
    /**
     * 取得缓存数据
     */
    getDataSource(key){

    }
    /**
     * 刷新按钮事件
     */
    Refresh(){
        this.queryData(null);
        pubUtil.showMeggage(this.props, pubUtil.MESSAGETYPE.REFRESH,this);
    }
}
export default simpleTablePageController;