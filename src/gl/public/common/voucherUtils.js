// 用于生成凭证、联查凭证
import { cardCache,cacheTools,ajax } from 'nc-lightapp-front';
const { setDefData } = cardCache;
import { 
    dataSource,dataSourceDetail, 
    voucher_link,voucher_gen,
    listCacheKey,cardCacheKey,linkToCacheTool
} from '../components/constJSON';

// param:
//{
//     voucher:voucher, //完整的凭证vo  [vo1,vo2]/vo
//     pk_voucher: '...',//联查时，凭证id [pk1,pk2]/pk
//     titlename:'凭证生成',
//     backUrl:'/',
//     backAppCode:backAppCode,
//     backPageCode:backPageCode
// }

export function openToVoucher(_this,param){
    if (Array.isArray(param.pk_voucher)&&param.pk_voucher.length>1) {
        let voucherapp = voucherRelatedApp(voucher_link,"Y");
        let appcode = voucherapp.appcode;
        let pagecode=voucherapp.pagecode;
        //cacheTools.set('checkedData', { id: data }); 旧的
        cacheTools.set(linkToCacheTool, { id: param.pk_voucher });
        _this.props.openTo('/gl/gl_voucher/pages/main/index.html#/', {
            status: 'browse',
            appcode: appcode,
            c: appcode,
            //pagekey: 'link',
            scene : '_LinkVouchar',
            n: param.titlename,/* 国际化处理： 凭证联查*/
            ifshowQuery: true,
            pagecode:pagecode
        });
    } else {
        let voucherapp = voucherRelatedApp(voucher_link,"N");
        let appcode = voucherapp.appcode;
        let pagecode=voucherapp.pagecode;
        let pk_voucher=param.pk_voucher;
        if(Array.isArray(param.pk_voucher)){
            pk_voucher=param.pk_voucher[0]
        }
        _this.props.openTo('/gl/gl_voucher/pages/main/index.html#Welcome',
            {
                appcode: appcode,
                c: appcode,
                id: pk_voucher,
                //pagekey: 'link',
                scene : '_LinkVouchar',
                n: param.titlename,/* 国际化处理： 凭证联查*/
                status: 'browse',
                backflag: 'noback',
                pagecode:pagecode
            }
        )
    }

}

export function pushToGenVoucher(_this,param){
    if (param.voucher.gl_voucher) {
        let voucherapp = voucherRelatedApp(voucher_gen,"Y");
        let appcode = voucherapp.appcode;
        let pagecode=voucherapp.pagecode;
        //多张凭证到列表 
        setDefData(listCacheKey, dataSource, param.voucher);
        _this.props.pushTo('/list', {
            appcode: appcode,  
            pagecode:pagecode,
            ifshowQuery: true, //是否显示查询区，true不显示，false显示，默认显示
            n: param.titlename,/* 国际化处理： 凭证生成*/
            //pagekey: 'generate',
            backUrl: param.backUrl?param.backUrl:'/',
            backAppCode:param.backAppCode,
            backPageCode:param.backPageCode,
        });
    } else {
        let voucherapp = voucherRelatedApp(voucher_gen,"N");
        let appcode = voucherapp.appcode;
        let pagecode=voucherapp.pagecode;
        //单张凭证到卡片
        setDefData(cardCacheKey, dataSourceDetail, param.voucher);
        _this.props.pushTo('/Welcome', {
            status: 'edit',
            appcode: appcode,
            pagecode:pagecode,
            //pagekey: 'generate',
            ifshowQuery: true, 
            n: param.titlename,/* 国际化处理： 凭证生成*/
            backflag: 'back',
            backAppCode: param.backAppCode,
            backPageCode: param.backPageCode
        });
    }
}

export function pushToLinkVoucher(_this, param) {
    let urlParam = {
        ifshowQuery: true,
        n: param.titlename,
        //pagekey: 'link',
        scene : '_LinkVouchar',
        status: 'browse',
        backUrl: param.backUrl,
        backAppCode: param.backAppCode,
        backPageCode: param.backPageCode,
    }
    if(param.voucher){
        //预览(未保存的凭证联查)
        if(Array.isArray(param.voucher)&&param.voucher.length>1){
            let voucherapp = voucherRelatedApp(voucher_link,"Y");
            urlParam.appcode=voucherapp.appcode;
            urlParam.pagecode=voucherapp.pagecode;
            setDefData(listCacheKey, dataSource, param.voucher);
            _this.props.pushTo('/list', urlParam);
        }else{
            let voucherapp = voucherRelatedApp(voucher_link,"N");
            urlParam.appcode=voucherapp.appcode;
            urlParam.pagecode=voucherapp.pagecode;
            setDefData(cardCacheKey, dataSourceDetail, param.voucher);
            urlParam.backflag='back';
            _this.props.pushTo('/Welcome', urlParam);
        }
    }else{
        //联查
        setDefData(listCacheKey, dataSource, param.pk_voucher);
        if(Array.isArray(param.pk_voucher)&&param.pk_voucher.length>1){
            let voucherapp = voucherRelatedApp(voucher_link,"Y");
            urlParam.appcode=voucherapp.appcode;
            urlParam.pagecode=voucherapp.pagecode;
            _this.props.pushTo('/list', urlParam);
        }else{
            let voucherapp = voucherRelatedApp(voucher_link,"N");
            urlParam.appcode=voucherapp.appcode;
            urlParam.pagecode=voucherapp.pagecode;
            let pk_voucher = param.pk_voucher;
            if (Array.isArray(param.pk_voucher)) {
                pk_voucher = param.pk_voucher[0]
            }
            urlParam.backflag = 'back';
            urlParam.id = pk_voucher;
            _this.props.pushTo('/Welcome', urlParam);
        }
    }
    // setDefData(listCacheKey, dataSource, param.voucher);
    // //setDefData('checkedData', dataSource, checkedDatas); 旧的
    // if(Array.isArray(param.voucher)&&param.voucher.length>1){
    //     _this.props.pushTo('/list', {
    //         appcode: appcode,
    //         ifshowQuery: true,
    //         n: param.titlename,
    //         pagekey: 'link',
    //         status: 'browse',
    //         backUrl: param.backUrl,
    //         backAppCode: param.backAppCode,
    //         backPageCode: param.backPageCode,
    //     })
    // }else{
    //     let pk_voucher=param.voucher;
    //     if(Array.isArray(param.voucher)){
    //         pk_voucher=param.voucher[0]
    //     }
    //     _this.props.pushTo('/Welcome', {
    //         appcode: appcode,
    //         n: param.titlename,
    //         pagekey: 'link',
    //         status: 'browse',
    //         backUrl: param.backUrl,
    //         backAppCode: param.backAppCode,
    //         backPageCode: param.backPageCode,
    //         id: pk_voucher,
    //         backflag: 'back' //是否有返回按钮
    //     })
    // }

}

export function voucherRelatedApp(params,isList) {
    let url = '/nccloud/gl/voucher/voucherRelatedApp.do';
    let data = {
        sence: params,
        billtype: 'C0',
        isList:isList
    }
    let appcode = '';
    ajax({
        url,
        data: data,
        async: false,
        success: function (response) {
            if (response.success) {
                appcode = response.data
            }
        }
    });
    return appcode
}