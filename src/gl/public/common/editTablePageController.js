import pubUtil from './pubUtil'
import { promptBox,toast} from "nc-lightapp-front"
import utils from '../../../fipub/public/common/utils'
class editTablePageController{
    constructor(main, props, meta) {
        this.main = main;
        this.props = props;
        this.headRefDisabled = meta.headRefDisabled;
        this.treeId = meta.treeId;
        this.listIds = meta.listIds || [];
        this.listButtonAreas = meta.listButtonAreas || {};
        this.browseButtons = ['Add', 'Edit','Refresh', ...meta.browseButtons || []] || [];
        this.editButtons = ['Add', 'Save', 'Cancel', ...meta.editButtons || []] || [];
        let params = {
            appcode:this.getAppcode(props),
            pagecode:this.getPagecode(props)
        }
        this.initTemplate(params);
    }

    initTemplate(params){
        this.props.createUIDom(
            params,
            (data) => {
                let meta = data.template;
                this.initMeta(meta);
                this.initRefParam(meta);
                window.setTimeout(() => {
                    this.listIds.map((one)=>{
                        if(this.listButtonAreas[one] != null){
                            this.addListOpr(meta, one, this.listButtonAreas[one]);
                        }
                    })
                    this.props.meta.setMeta(meta);
                    this.props.button.setButtons(data.button);
                    this.setPageState('browse');
                    this.queryData(data);
                }, 1);
            }
        )
    }
    queryData(key){
        
    }
    /**
     * 初始化模板
     * @param {*} meta 
     */
    initMeta(meta){
        
    }
    initRefParam(meta){

    }

    setPageState(state){
        switch(state){
            case 'browse':
                this.props.button.setButtonVisible(this.editButtons, false);
                this.props.button.setButtonVisible(this.browseButtons, true);
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'browse');
                })
                this.props.button.setPopContent('DelLine',this.main.state.json['2002-0014']);
                if(this.treeId){
                    this.props.syncTree.setNodeDisable(this.treeId, false);
                }
                if(this.headRefDisabled){
                    this.main.setState({[this.headRefDisabled]:false});
                }
            break;
            case 'add':
                this.props.button.setButtonVisible(this.browseButtons, false);
                this.props.button.setButtonVisible(this.editButtons, true);
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'add');
                })
                this.props.button.setPopContent('DelLine');
                if(this.treeId){
                    this.props.syncTree.setNodeDisable(this.treeId, true);
                }
                if(this.headRefDisabled){
                    this.main.setState({[this.headRefDisabled]:true});
                }
            break;
            case 'edit':
                this.props.button.setButtonVisible(this.browseButtons, false);
                this.props.button.setButtonVisible(this.editButtons, true);
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'edit');
                })
                this.props.button.setPopContent('DelLine');
                if(this.treeId){
                    this.props.syncTree.setNodeDisable(this.treeId, true);
                }
                if(this.headRefDisabled){
                    this.main.setState({[this.headRefDisabled]:true});
                }         
            break;
        }
    }

    /**
     * 设置表格数据
     * @param {*data} 数据 
     */
    setTableData(data){
        this.listIds.map((one)=>{
            if(data.data != null && data.data[one] != null){
                this.props.editTable.setTableData(one, data.data[one]);
            }else{
                this.props.editTable.setTableData(one, {rows:[]});
            }
        })
    }
    /**
     * 添加操作列按钮
     */
    addListOpr(meta, AreaId, buttonAreaId){
        let event = {
            label: this.main.state.json['2002-0002'],
            attrcode: 'opr',
            itemtype: 'customer',
            fixed: 'right',
            visible: true,
            width: '150px',
            render: (text, record, index) =>{
                return this.props.button.createOprationButton(this.getListButtons(text, record, index), {
                    area: buttonAreaId,
                    buttonLimit: 3,
                    onButtonClick: (props, btnKey) => {
                        this.listButtonClick(AreaId, btnKey, record, index);
                    }
                });
            }
        };
        meta[AreaId].items.push(event);
    }
    /**
     * 取得表格行按钮
     * @param {*} text 
     * @param {*} record 
     * @param {*} index 
     */
    getListButtons(text, record, index){
        return ['DelLine'];
    }

    /**
     * 表头按钮事件
     * @param {string buttonId} 按钮ID  
     */
    headButtonClick(buttonId){
        switch(buttonId){
            case 'Add':
                this.Add();
            break;
            case 'Edit':
                this.Edit();
            break;
            case 'Cancel':
                this.Cancel();
            break;
            case 'Cancel':
                this.Cancel();
            break;
            case 'Save':
                this.Save();
            break;
            case 'Delete':
                this.Delete();
            break;
            case 'Refresh':
                this.Refresh();
            break;
            default:
            this[buttonId]();
        }
    }
    /**
     *新增按钮事件
     */
    Add(){
        this.listIds.map((one)=>{
            let index=this.props.editTable.getNumberOfRows(one);
            this.props.editTable.addRow(one,index,true);
        })
        this.setPageState('add');
    }
    /**
     * 修改按钮事件
     */
    Edit(){
        this.setPageState('edit');
    }
    /**
     * 取消按钮事件
     */
    Cancel(){
        promptBox({
            color:"warning",
            title: this.main.state.json['2002-0011'],
            content: this.main.state.json['2002-0012'],
            beSureBtnClick: this.doCancel.bind(this)
            });
    }
    doCancel(){
        this.listIds.map((one)=>{
            this.props.editTable.cancelEdit(one);
        })
        this.setPageState('browse');
    }
    /**
     * 保存按钮事件
     */
    Save(){
        let checkFlag = true;
        this.listIds.map((one)=>{
            if(!this.props.editTable.getAllRows(one)){
                checkFlag = false;
            }
        })
        if(checkFlag){
            let data = {};
            this.listIds.map((one)=>{
                data[one] = this.props.editTable.getAllData(one);
            })
            this.doSave(data);
        }
        
    }
    /**
     * 保存动作
     * @param {*data} 页面数据
     */
    doSave(data){

    }
    /**
     * 保存回掉方法
     * @param {*data} 返回数据
     */
    callBackSave(data){
        this.setPageState('browse');
        this.setTableData(data);
        pubUtil.showMeggage(this.props, pubUtil.MESSAGETYPE.SAVE,this);
    }

    /**
     * 列表按钮事件
     */
    listButtonClick(AreaId, actionId, data, index){
        switch(actionId){
            case 'DelLine':
                this.DelLine(AreaId, data, index);
            break;
        }
    }
    /**
     * 删行按钮事件
     * @param {*data} 行数据 
     * @param {*index} 行号
     */
    DelLine(AreaId, data, index){
        if(this.props.editTable.getStatus(AreaId) == null || this.props.editTable.getStatus(AreaId) == "browse"){
            let postData = {[AreaId]:pubUtil.getEdittableDeleteRowDataByIndex(this.props, AreaId, index)};
            this.doDelLine(AreaId, postData, index);
        }else{
            this.props.editTable.delRow(AreaId, index);
        }
    }
    /**
     * 删行处理
     * @param {*} data 
     */
    doDelLine(AreaId, data, index){
        
    }
    /**
     * 删除回掉方法
     * @param {*data} 返回数据
     */
    callBAckDelLine(AreaId, data, index){
        this.props.editTable.deleteTableRowsByIndex(AreaId, index, true);
        pubUtil.showMeggage(this.props, pubUtil.MESSAGETYPE.DELETE,this);
    }
    /**
     * 批量删行事件
     * @param {*data} 行数据 
     * @param {*index} 行号
     */
    Delete(){
        let data = {};
        let AreaId='';
        this.listIds.map((one)=>{
            data[one] = this.props.editTable.getCheckedRows(one);
            AreaId=one;
        })
        let allIndex=[];
        if(data[AreaId].length>0){
            for(let i=0;i<data[AreaId].length;i++){
                allIndex.push(data[AreaId][i].index);
            }
            if(this.props.editTable.getStatus(AreaId) == null || this.props.editTable.getStatus(AreaId) == "browse"){
                promptBox({
                    color:"info",
                    title: this.main.state.json['2002-0014'],
                    beSureBtnClick: this.deleteAction.bind(this,AreaId,allIndex)
                })
            }else{ 
                this.props.editTable.deleteTableRowsByIndex(AreaId, allIndex);
            }
        }else{
            pubUtil.showMeggage(this.props, pubUtil.MESSAGETYPE.WARNING,this);
        }
        
    }
    deleteAction(AreaId,allIndex){
        let postData = {[AreaId]:pubUtil.getEdittableDeleteRowDataByIndexs(this.props, AreaId, allIndex)};
        this.doDelete(AreaId,postData,allIndex);
    }
    /**
     * 批量删行处理
     * @param {*} data 
     */
    doDelete(AreaId, data, index){
        
    }
    /**
     * 批量删除回掉方法
     * @param {*data} 返回数据
     */
    callBAckDelete(AreaId, data, index){
        this.props.editTable.deleteTableRowsByIndex(AreaId, index, true);
        pubUtil.showMeggage(this.props, pubUtil.MESSAGETYPE.DELETE,this);
    }
    /**
     * 刷新按钮事件
     */
    Refresh(){
        this.queryData(null);
        pubUtil.showMeggage(this.props, pubUtil.MESSAGETYPE.REFRESH,this);
    }
    /**
     * 表单编辑后事件
     * @param {*} props 
     * @param {string moduleId}  区域ID
     * @param {string key} 编辑字段
     * @param {*} value 
     * @param {*} changedrows 
     * @param {*} index 
     * @param {*} record 
     */
    afterEditEvent(props, moduleId, key, value, changedrows, index, record) {

    }

    getAppcode(props){
        return this.getUrlParam(props, "c");
    }

    getPagecode(props){
        return this.getUrlParam(props, 'p');
    }

    getTitle(props){
        return this.getUrlParam(props, 'n');
    }
    
    /**
     * 从URL中获取参数
     * 优先iframe中获取，获取不到再从浏览器地址中获取
     * @param {*} paramName 
     */
    getUrlParam(props, paramName){
        let param = props.getUrlParam(paramName);
        if(!utils.exist(param)){
            param = props.getSearchParam(paramName);
        }
        return param;
    }
}
export default editTablePageController;