import { cardCache } from 'nc-lightapp-front'
const { setDefData, getDefData } = cardCache;
import { voucherRelatedApp } from '../components/oftenApi.js';
import { dataSource, dataSourceDetail, voucher_gen, voucher_link } from '../components/constJSON';

/**
 * 获取凭证类型：生成凭证/联查凭证
 */
function getVoucherType(params) {
    return params.voucherType === 'voucher_link' ? voucher_link : voucher_gen 
}

/**
 * 单页,页面跳转前缓存数据
 */
function setCacheData( params) {
    let voucherType = getVoucherType(params)
    let chacheKey = params.isToList ? (voucherType === voucher_link ? 'checkedData' : 'gl_voucher') : 'voucher_detail'
    let chacheSource = params.isToList ? dataSource : dataSourceDetail
    setDefData(chacheKey, chacheSource, params.cacheData)
}

/**
 *  获取跳转路径
 * @param {*} params 
 */
function getGotoUrl(params){
    let urlArr = [
        '/list', 
        '/Welcome', 
        '/gl/gl_voucher/pages/voucher/index.html', 
        '/gl/gl_voucher/pages/main/index.html#/Welcome'
    ]
    return params.isSinglePage ? (params.isToList ? urlArr[0] : urlArr[1]) : (params.isToList ? urlArr[2] : urlArr[3])
}

/**
 * 获取页面跳转参数
 */
function getBaseParams(props, params) {
    let voucherType = getVoucherType(params)
    let voucherapp = voucherRelatedApp(voucherType)
    let baseParams = {
        appcode: voucherapp.appcode,
        ifshowQuery: true,
        n: params.n,
        pagekey: params.pagekey,
        status: params.status ? params.status : 'edit'
    }
    let singlePageParems = {
        // backUrl: params.backUrl,
        backAppCode: props.getUrlParam('backAppCode') || props.getSearchParam('c'),
        backPageCode: props.getUrlParam('backPageCode') || props.getSearchParam('p')
    }
    let singlePageList= {
        backUrl: params.backUrl,
    }
    let singlePageCard = {
        backflag: 'back',
        pagecode: '20021005card',
    }
    let multPageParams = {
        id: params.id,
        c: voucherapp.appcode,
    }
    if(params.isSinglePage){
        if(params.isToList){
            return { ...baseParams, ...singlePageParems, ...singlePageList }
        } else {
            return { ...baseParams, ...singlePageParems, ...singlePageCard }
        }
    } else {
        return { ...baseParams, ...multPageParams }
    }
    // return params.isSinglePage ? { ...baseParams, ...singlePageParems } : { ...baseParams, ...multPageParams }    
}

/**
 * @description: 生成凭证、联查凭证公共方法
 * @param {*} props 
 * @param {object} params 
 * @param {object} privateParams 
 * params: {
        isSinglePage: true, //是否是单页面
        isToList: true,
        voucherType: 'voucher_link', //凭证类型(生成凭证:'voucher_gen',联查凭证:'voucher_link')
        gotoType: 'pushTo', //跳转页面的方式 ‘pushTo’ ‘openTo’ ‘linkTo’
        gotoUrl: '', // 页面跳转路径, 无特殊情况可不写
        pagekey: '', //
        n: '', // 标题区显示名称
        backUrl: '', // 单页返回路径
        cacheData: null, //单页跳转页面前需要缓存的数据,仅单页面需要
    }
 */
export default function gotoVoucherPage(props, params, privateParams={} ) {  
    let baseParams = getBaseParams(props, params)
    let gotoUrl = params.gotoUrl ? params.gotoUrl : getGotoUrl(params)
    switch(params.gotoType){
        case 'pushTo'://单页应用中，页面间跳转
            setCacheData(params)
            props.pushTo(gotoUrl, {
                ...baseParams,
                ...privateParams
            })
            break;
        case 'openTo'://以新页签形式打开页面
            props.openTo(gotoUrl, {
                ...baseParams,
                ...privateParams
            })
            break;
        case 'linkTo'://多页应用，在当前页签打开页面
            props.linkTo(gotoUrl, {
                ...baseParams,
                ...privateParams
            })
            break;
        default:
        break;
    }
}

// export{ gotoVoucherPage }