class paginationUtils{

    constructor(data, max_row){
        // super(data, max_row);
        this.data = data;
        this.max_row = max_row;
    }

    /**
     * 更新数据源
     */
    setData = (data) => {
        this.data = data;
    }

    /**
     * 设置最大行数
     */
    setMaxRow = (max_row) => {
        this.max_row = max_row;
    }

    /**
     * 获取分页总页数
     * @param {数据源数组} data 
     * @param {每页最大行数} max_row 
     */
    getTotalPage = () =>{
        if(this.data && this.max_row){
            return Math.ceil(this.data.length/this.max_row);
        }
        return 0;
    }

    /**
     * 根据页码获取数据
     */
    getPageData = (currPage) =>{
        let start = this.max_row * (currPage - 1);
        let end = start + this.max_row;
        return this.data.slice(start, end);
    }

    /**
     * 是否最后一页
     */
    isLastPage = (currPage) => {
        if(this.getTotalPage() == currPage){
            return true;
        }
        return false;
    }

}
export default paginationUtils
