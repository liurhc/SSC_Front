/**
 * 列表模板变更事件
 * @param {*props} props 
 * @param {*newTemplate} 新模板
 * @param {string areaId} 区域ID
 * @param {Array<String> firstExceptionField} 前部例外字段
 * @param {Array<String> lastExceptionField} 后部例外字段
 */
export default function (props, areaId, newTemplate, firstExceptionField, lastExceptionField) {
    let meta = props.meta.getMeta();
    let listTemplate = meta[areaId];
    let items = [];
    
    if(listTemplate != null && listTemplate.items != null){
        listTemplate.items.map((one)=>{
            if(firstExceptionField.indexOf(one.attrcode) != -1){
                items.push(one);
            }
        })
    }
    if(newTemplate != null && newTemplate.items != null){
        newTemplate.items.map((one)=>{
            if(one.attrcode == null){
                one.attrcode = one.code;
            }
            items.push(one);
        })
    }
    if(listTemplate != null && listTemplate.items != null){
        listTemplate.items.map((one)=>{
            if(lastExceptionField.indexOf(one.attrcode) != -1 || !one.visible){
                items.push(one);
            }
        })
    }
    listTemplate.items=items;
    props.meta.setMeta(meta);
    return meta;
}
