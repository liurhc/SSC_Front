
export  default function getLangCode (key) {
    let multiLang = this.props.MutiInit.getIntl(this.moduleId);
    return multiLang && multiLang.get(this.moduleId + '-' + key);
};