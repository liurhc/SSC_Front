import React, { Component } from "react";
import {ajax ,toast} from "nc-lightapp-front";
export default function resizeWidths(pk_report,width,json, callBack){
    // let info=handsontableObj.getReportInfo();
    // let width=info.colWidths;
    if(width.length==0){
        //打开节点，未拖拽过列宽直接保存
        toast({ content: json['publiccommon-000002'], color: 'success' })
    } else {
        let data = {
            'pk_report':pk_report,
            'isBusinessSet': 'true',
            'width':width
        };
        ajax({
            url: '/nccloud/gl/glpub/saveReportWidth.do',
            data,
            success: (res) => {
                if (res.success) {
                    toast({ content: json['publiccommon-000002'], color: 'success' });
                    if(callBack){
                        callBack()
                    }
                }
            }
        }); 
    }
    
}