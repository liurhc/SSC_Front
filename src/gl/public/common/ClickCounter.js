/**
 * 有时效性的点击计数器
 */
class ClickCounter{
    constructor(maxInterval = 1000){
        this.maxInterval = maxInterval;
        this.lastClickTime = new Date().getTime();
        this.clickTime = 0;
    }

    count = () =>{
        let page = this;
        let now = new Date().getTime();
        if(now - this.lastClickTime > page.maxInterval){
            page.clickTime = 1;
        }else{
            page.clickTime++;
        }
        page.lastClickTime = now;
    }

    getCount = () =>{
        return this.clickTime;
    }
}

export default ClickCounter;