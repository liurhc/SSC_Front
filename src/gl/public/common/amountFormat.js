/**
 * 处理数字精度
 */
const ROUND_HALF_UP='4';//四舍五入
const ROUND_UP='0';//全部进位
const ROUND_DOWN='1';//全部舍位

/**
 * 根据进舍规则处理金额精度
 * @param {金额} amount 
 * @param {精度} scale 
 * @param {进舍规则} roundtype 
 */
export default function setScale(amount,scale,roundtype){
    if(!scale)
        scale=8;
    switch(roundtype){
        case ROUND_UP:
            return allUpScale(amount,scale);
        case ROUND_DOWN:
            return allDownScale(amount,scale);
        case ROUND_HALF_UP:
        default:
            return halfUpScale(amount,scale);
    }
}

/**
 * 四舍五入
 * @param {*} amount 
 * @param {*} scale 
 */
function halfUpScale(amount,scale){
    return Number(amount-0).toFixed(scale-0);
}

/**
 * 全部进位
 * @param {*} amount 
 * @param {*} scale 
 */
function allUpScale(amount,scale){
    let amountStr=(amount||0).toString();
    let values=amountStr.split('.');
    scale=scale-0;
    let newAmount=amountStr;
    if(values.length>1){
        if(scale==0){
            newAmount= amount-0+1;
        }else{
            if(values[1].length>scale){
                newAmount= amountStr.substring(0,amountStr.indexOf('.') + scale + 1)-0+Math.pow(0.1, scale);
            }
        }
    }
    return Number(newAmount-0).toFixed(scale);
}

/**
 * 全部舍位
 * @param {*} amount 
 * @param {*} scale 
 */
function allDownScale(amount,scale){
    let amountStr=(amount||0).toString();
    let values=amountStr.split('.');
    scale=scale-0;
    let newAmount=amountStr;
    if(values.length>1){
        if(scale==0){
            newAmount=values[0];
        }else{
            if(values[1].length>scale){//如果小数位数小于精度位数补0
                newAmount=amountStr.substring(0,amountStr.indexOf('.') + scale + 1);
            }
        }
    }
    return Number(newAmount-0).toFixed(scale);
}

/**
 * 给数字添加千分符
 * @param {数字} num 
 */
export function addThousandMark(num) {
    let pointIndex, intPart, pointPart;

    if (isNaN(num)) {
        return '';
    }
    num = num + '';
    if (/^.*\..*$/.test(num)) {
        pointIndex = num.lastIndexOf('.');
        intPart = num.substring(0, pointIndex);
        pointPart = num.substring(pointIndex + 1, num.length);
        intPart = intPart + '';
        let re = /(-?\d+)(\d{3})/;
        while (re.test(intPart)) {
            intPart = intPart.replace(re, '$1,$2');
        }
        num = intPart + '.' + pointPart;
    } else {
        num = num + '';
        let re = /(-?\d+)(\d{3})/;
        while (re.test(num)) {
            num = num.replace(re, '$1,$2');
        }
    }
    return num;
}
//export{setScale}