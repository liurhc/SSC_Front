import {LocalToDongbaTime, DongbaToLocalTime} from 'nc-lightapp-front';
import moment from 'moment';

/**
 * 当地时间转换为东八区时间
 * @param {*} 当地时间，带时分秒 
 * @param {*} 转换格式 
 */
function localToEast8(time,format){
    return LocalToDongbaTime(moment(time)).format(format);
}

/**
 * 东八区时间转换为当地时间
 * @param {*} 东八区时间，带时分秒 
 * @param {*} 转换格式 
 */
function east8ToLocal(time,format){
    return DongbaToLocalTime(moment(time)).format(format);
}

export{localToEast8,east8ToLocal}