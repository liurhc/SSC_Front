//获取界面高度
function getTableHeight(height){
    let defaultHeight=0
    if(height){
        defaultHeight=height;
    }
    let tableHeight=document.getElementById('app').offsetHeight-defaultHeight+'px';
    return tableHeight;
}
//加法
function accAdd(arg1,arg2){
    if(!arg1){
        arg1='0';
    }
    if(!arg2){
        arg2='0';
    }
    var r1,r2,m;
    try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
    try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2))
    return accDiv((accMul(arg1,m)+accMul(arg2,m)),m)
    }
//减法
function Subtr(arg1,arg2){
    if(!arg1){
        arg1='0';
    }
    if(!arg2){
        arg2='0';
    }
    var r1,r2,m,n;
    try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
    try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2));
    //last modify by deeka
    //动态控制精度长度
    n=(r1>=r2)?r1:r2;
    return ((arg1*m-arg2*m)/m).toFixed(n);
}
//乘法
function accMul(arg1,arg2){
    if(!arg1){
        arg1='0';
    }
    if(!arg2){
        arg2='0';
    }
    var m=0,s1=arg1.toString(),s2=arg2.toString();
    try{m+=s1.split(".")[1].length}catch(e){}
    try{m+=s2.split(".")[1].length}catch(e){}
    return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m)
}
//除法
function accDiv(arg1,arg2){
    if(!arg1){
        arg1='0';
    }
    if(!arg2){
        arg2='0';
    }
    var t1=0,t2=0,r1,r2;
    try{t1=arg1.toString().split(".")[1].length}catch(e){}
    try{t2=arg2.toString().split(".")[1].length}catch(e){}
    // with(Math){
    r1=Number(arg1.toString().replace(".",""))
    r2=Number(arg2.toString().replace(".",""))
    return accMul((r1/r2),Math.pow(10,t2-t1));
    // }
}
export {accAdd,Subtr,accMul,accDiv,getTableHeight}