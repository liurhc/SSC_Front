import { output } from 'nc-lightapp-front';

/**
 * 模板输出
 * @param {*} url //接口地址
 * @param {*} appcode 
 * @param {*} nodekey 
 * @param {*} ctemplate 
 * @param {*} paramObj // 查询参数
 */

export default function mouldOutput(url, appcode,nodekey,ctemplate,paramObj, totalpage){
    let outputData = {
        funcode: appcode,      //功能节点编码，即模板编码
        // nodekey: '',
        // printTemplateID: '',
        oids: [],
        outputType: 'output',
        outputMode: 'excel',
        totalpage: totalpage
    }
    paramObj.queryvo = {
        scopeall: 'y',  // 打印范围
        prepage: 'n',   // 承前页
        downpage: 'n',  // 过次页
        // ctemplate: ''
    }
    outputData['userjson'] = JSON.stringify(paramObj)
    output(
        {
            url: url,
            data: outputData,
        }
    )
}
