import { print } from 'nc-lightapp-front';

/**
 * 模板打印
 * @param {*} url //打印地址
 * @param {*} appcode 
 * @param {*} nodekey 
 * @param {*} ctemplate 
 * @param {*} paramObj //查询参数
 */
export default function printRequire(url,appcode,nodekey,ctemplate,paramObj){
    print(
        'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
        url,   //后台服务url
        {
            billtype: '',  //单据类型
            appcode: appcode,      //功能节点编码，即模板编码
            // nodekey: nodekey,
            printTemplateID: ctemplate,
            oids: [],
            //outputType:'output',
            //outputMode:'excel',
            userjson: JSON.stringify(paramObj)//传入后台查询条件
        },
        false
    )
}
