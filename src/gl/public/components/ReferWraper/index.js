import React, { Component } from 'react';
import {base, deepClone } from 'nc-lightapp-front';
import './index.less';
const { NCFormControl: FormControl} =  base;

export default class ReferWraper extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
    }
    onClick = () =>{
        let {onClick,disabled} = this.props
        if(!disabled && onClick){
            onClick()
        }
    }
    onClear = () => {
        let {onClear} = this.props
        if(onClear){
            onClear()
        }
    }
    render(){
        let {display,disabled,placeholder,fieldid, ...other} = this.props
        let outStyle = this.props.outStyle ? this.props.outStyle : {}
        return(
            <div className="refer-input-wraper nc-theme-area-split-bc" style={outStyle}>
                <FormControl
                    fieldid={fieldid ? fieldid: 'assid' }
                    placeholder={placeholder ? placeholder : ''}
                    value={display}
                    disabled={disabled}
                    onChange={(v) => {}}
                    {...other}
                    // showClose={true}
                />
                <i
                    className="iconfont icon-canzhaozuixin"
                    onClick={ this.onClick }
                />
                {/* <div className='del'>
                    <i onClick={() => {this.onClear}}
                        className="iconfont icon-qingkong"
                    />
                </div> */}
            </div>
        )
    }
}
