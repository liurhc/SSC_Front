import React, { Component } from 'react';
import {ajax} from 'nc-lightapp-front';
import { toast } from './utils.js';
/**
 * 获取核销对象科目的截止日期
 * @param {Object} condition 
 */
export function getVerifyInitHeadTitle(condition){
    let resultJSON={};
    let url='/nccloud/gl/verify/verifyinitheadtitle.do';
    ajax({
        url: url,
        data: condition,
        async: false,
        success:function(response){
            let { data, success } = response;
                if( success ){
                  resultJSON.begindate = data.begindate;
                  resultJSON.asscontent = data.asscontent;
                }
        }
        // error:function(error){
        //     toast({ content: this.state.json['publiccomponents-000089'], color: 'warning' });               /* 国际化处理： 请求启动日期失败*/
        // }
    })
    return resultJSON;
}
