import React, { Component } from 'react';
import { base,getMultiLang } from 'nc-lightapp-front';
// const { NCModal } = base
const { NCButton: Button, NCRadio: Radio,
    NCRow: Row, NCCol: Col, NCLabel: Label, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, NCModal: Modal, NCForm
} = base;
const { NCFormItem: FormItem } = NCForm;

import { CheckboxItem, RadioItem, SelectItem } from '../FormItems';
import ReferLoader from '../../ReferLoader/index.js'
import './index.less'

// const impImpletData = [
//     { itemName: this.state.json['publiccomponents-000032'], itemType: 'radio', itemKey: 'importPattern', itemChild: [{ value: 'true', label: this.state.json['publiccomponents-000033'] }, { value: 'false', label: this.state.json['publiccomponents-000034'] }] },/* 国际化处理： 导入方式:,按名称,按编码*/
//     { itemName: this.state.json['publiccomponents-000035'], itemType: 'radio', itemKey: 'isroundup', itemChild: [{ value: 'true', label: this.state.json['publiccomponents-000036'] }, { value: 'false', label: this.state.json['publiccomponents-000037'] }] },/* 国际化处理： 精度处理:,四舍五入,舍位*/
//     { itemName: this.state.json['publiccomponents-000038'], itemType: 'radio', itemKey: 'savePattern', itemChild: [{ value: 'true', label: this.state.json['publiccomponents-000039'] }, { value: 'false', label: this.state.json['publiccomponents-000040'] }] }/* 国际化处理： 导入数据方式:,覆盖,增量*/
// ]
// const expImpletData = [
//     { itemName: this.state.json['publiccomponents-000041'], itemType: 'radio', itemKey: 'exportContent', itemChild: [{ value: 'true', label: this.state.json['publiccomponents-000042'] }, { value: 'false', label: this.state.json['publiccomponents-000043'] }] },/* 国际化处理： 导出内容:,样式+数据,样式*/
//     { itemName: this.state.json['publiccomponents-000044'], itemType: 'radio', itemKey: 'incloudCode', itemChild: [{ value: 'false', label: this.state.json['publiccomponents-000006'] }, { value: 'true', label: this.state.json['publiccomponents-000005'] }] },/* 国际化处理： 档案包含编码列:,否,是*/
//     { itemName: this.state.json['publiccomponents-000045'], itemType: 'radio', itemKey: 'savePattern', itemChild: [{ value: 'true', label: this.state.json['publiccomponents-000046'] }, { value: 'false', label: this.state.json['publiccomponents-000047'] }] }/* 国际化处理： 覆盖规则:,覆盖原内容,增加新页签*/
// ]

const formItemLayout = { labelXs:3, labelSm:3, labelMd:3, xs:9, md:9, sm:9 };
const formItemParam = { inline:true, showMast:false, isRequire:true, method:"change" };

export default class ExportFile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            tmpletType: this.props.tmpletType,
            listItem: {
                exportContent: { display: '', value: 'true' },
                incloudCode: { display: '', value: 'true' },
                savePattern: { display: '', value: 'true' }
            },
        };
    }
    componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json
               // columns_reportSum:this.columns_reportSum,
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
    handleExportOK(){
        let { handleExportOK } = this.props
        let { listItem } = this.state
        let baseData = {
            exportContent: '',
            incloudCode: '',
            savePattern: ''
        }
        for (const key in listItem) {
            if (listItem.hasOwnProperty(key)) {
                baseData[key] = listItem[key].value
            }
        }
        if (handleExportOK) {
            handleExportOK(baseData)
        }
    }
    handleCancel() {
        let { handleCancel } = this.props
        if (handleCancel) {
            handleCancel()
        }
    }
    
    renderRadio(listItem, item) {
        return (
            <FormItem
                {...formItemLayout}
                {...formItemParam}
                labelName={item.itemName}
            //  change={self.handleGTypeChange.bind(this, 'contracttype')}
            >
                <RadioItem
                    name={item.itemKey}
                    type="customer"
                    defaultValue={listItem[item.itemKey].value ? listItem[item.itemKey].value : 'true'}
                    items={() => {
                        return (item.itemChild)
                    }}
                    onChange={(v) => {
                        listItem[item.itemKey].value = v
                        this.setState({
                            listItem
                        })
                    }}
                />
            </FormItem>
        )
    }

    renderForm(tmpletType,listItem) {
        const impImpletData = [
            { itemName: this.state.json['publiccomponents-000032'], itemType: 'radio', itemKey: 'importPattern', itemChild: [{ value: 'true', label: this.state.json['publiccomponents-000033'] }, { value: 'false', label: this.state.json['publiccomponents-000034'] }] },/* 国际化处理： 导入方式:,按名称,按编码*/
            { itemName: this.state.json['publiccomponents-000035'], itemType: 'radio', itemKey: 'isroundup', itemChild: [{ value: 'true', label: this.state.json['publiccomponents-000036'] }, { value: 'false', label: this.state.json['publiccomponents-000037'] }] },/* 国际化处理： 精度处理:,四舍五入,舍位*/
            { itemName: this.state.json['publiccomponents-000038'], itemType: 'radio', itemKey: 'savePattern', itemChild: [{ value: 'true', label: this.state.json['publiccomponents-000039'] }, { value: 'false', label: this.state.json['publiccomponents-000040'] }] }/* 国际化处理： 导入数据方式:,覆盖,增量*/
        ]
        const expImpletData = [
            { itemName: this.state.json['publiccomponents-000041'], itemType: 'radio', itemKey: 'exportContent', itemChild: [{ value: 'true', label: this.state.json['publiccomponents-000042'] }, { value: 'false', label: this.state.json['publiccomponents-000043'] }] },/* 国际化处理： 导出内容:,样式+数据,样式*/
            { itemName: this.state.json['publiccomponents-000044'], itemType: 'radio', itemKey: 'incloudCode', itemChild: [{ value: 'false', label: this.state.json['publiccomponents-000006'] }, { value: 'true', label: this.state.json['publiccomponents-000005'] }] },/* 国际化处理： 档案包含编码列:,否,是*/
            { itemName: this.state.json['publiccomponents-000045'], itemType: 'radio', itemKey: 'savePattern', itemChild: [{ value: 'true', label: this.state.json['publiccomponents-000046'] }, { value: 'false', label: this.state.json['publiccomponents-000047'] }] }/* 国际化处理： 覆盖规则:,覆盖原内容,增加新页签*/
        ]
        let tempData = (tmpletType==='import') ? impImpletData : expImpletData
        return (
            tempData.map((item, i) => {
                switch (item.itemType) {
                    case 'radio':
                        return this.renderRadio(listItem, item)                      
                    // case 'select':
                    //     return this.renderSelect(listItem, item)
                    // case 'checkbox':
                    //     return this.renderCheckbox(listItem, item)                       
                    // case 'ncNum':
                    //     return this.renderNCNumber(listItem, item)
                    // case 'refer':
                    //     return this.renderReferLoader(listItem, item)                       
                    default:
                        break;
                }
            })
        )
    }

    render() {
        let { visible, tmpletType } = this.props
        let { listItem } = this.state
        let modalTitle = (tmpletType==='import') ? this.state.json['publiccomponents-000048'] : this.state.json['publiccomponents-000049'] /* 国际化处理： 导入EXCEL,导出EXCEL*/
        return (
            <div className='importModal'>
                <Modal style={{ width: '520px',minHeight:'268px' }} show={visible} onHide={this.handleCancel.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.json['publiccomponents-000050']}EXCEL</Modal.Title>{/* 国际化处理： 导出*/}
                    </Modal.Header >
                    <Modal.Body style={{ borderTop: '1px solid #ccc', borderBottom: '1px solid #ccc' }}>
                        <NCForm className='exportform'
                            useRow={true}
                            submitAreaClassName='classArea'
                            showSubmit={false}　>
                            {this.renderForm(tmpletType,listItem)}
                        </NCForm>
                    </Modal.Body>
                    <Modal.Footer >
                        <Button size="sm" onClick={this.handleExportOK.bind(this)}>{this.state.json['publiccomponents-000009']}</Button>{/* 国际化处理： 确定*/}
                        <Button onClick={this.handleCancel.bind(this)}>{this.state.json['publiccomponents-000010']}</Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div >

        )
    }
    
}
