export default function GetRequest(urlstr){   
    var url = decodeURIComponent(urlstr)//(window.parent.location.href); //获取url中"?"符后的字串   
    var theRequest = new Object();   
    if (url.indexOf("?") != -1) {   
        var str = url.substr(url.indexOf("?")+1);   
        var strs = str.split("&");   
        for(var i = 0; i < strs.length; i ++) {   
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);   
        }   
    }   
    return theRequest;   
}
