import React, { Component } from 'react';
import {base} from 'nc-lightapp-front';
const {NCIcon} = base;
import pubUtil from '../../common/pubUtil'

import AssVoModal from '../assVoModal';//辅助核算模态框
import './index.less'

export default class AssidModalRedner extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayName: (props.title || {}).display,
            displayTitle: (props.title || {}).display,
            isQueryShow: false,
            pretentAssData: {}
        };
    }
    componentDidMount(){
        this.props.onRef(this);
    }
    handleClose(){
        this.setState({
            isQueryShow: false, 
            pretentAssData: {
                pk_accountingbook:'',
                pk_accasoa:'',
                prepareddate:'',
                pk_org:'',
                assData:[] ,
                assid:'',
                checkboxShow:true
            }
        });
    }
    changeState(){

    }

    iconClick(){
        let pretentAssData = {prepareddate: pubUtil.getSysDateStr(),assData:[],...this.props.getPretentAssData()}
        this.setState({ pretentAssData: pretentAssData })
            window.setTimeout(()=>{
                this.setState({
                    isQueryShow: true });
            },10)
    }
    handleQueryClick(data){
        let checkvaluename = '';
        if(data.length>0){
            for(let j=0;j<data.length;j++){
                if(data[''+j]){
                    if(data[''+j].pk_Checkvalue){
                        if(data[''+j].pk_Checkvalue.length>0){
                            for(let i=0;i<data[''+j].checkvaluename.length;i++){
                                checkvaluename = checkvaluename +'【'+ data[''+j].checktypename +'：'+data[''+j].checkvaluename[i] + '】';
                            }
                        }
                    }
                }
            }
        }
        this.props.doConfirm && this.props.doConfirm(data, checkvaluename);
        this.handleClose();
    }

    clear(){
        this.setState({displayName:'', pretentAssData: {}});
    }

    render() {
        let {title} = this.props;
        let getClass=(flag)=>{
 
            if(flag == 'home'){
                if(this.props.quotePosition == 'form'){
                    return 'nc-theme-area-split-bc home';
                }else{
                    return 'nc-theme-area-split-bc home-list';
                }
            }else if(flag == 'button'){
                if(this.props.quotePosition == 'form'){
                    return 'nc-theme-area-split-bc assid-modal-button';
                }else{
                    return 'nc-theme-area-split-bc assid-modal-button-list';
                }
            }
        }
        let getDisplayName=()=>{
            if((this.state.displayName || '').length > 10){
                return this.state.displayName.substring(0, 10) + '...';
            }else{
                return this.state.displayName;
            }
            
        }
        let getDisplayTitle=()=>{
            return this.state.displayName;
        }
        return (
            <div id="assVoModalRedner">
                <div className={getClass('home') }>
                    <table>
                        <tbody>
                            <tr>
                                <td className="assid-modal-title" title={getDisplayTitle()}>
                                    {title ? title.display : getDisplayName()}
                                </td>
                                <td className={getClass('button')}>
                                    <i className="iconfont icon-canzhaozuixin"
                                        onClick={this.iconClick.bind(this)} />
                                    {/* <NCIcon type="uf-3dot-h"/> */}
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <AssVoModal 
                        pretentAssData={this.state.pretentAssData}
                        showOrHide = {this.state.isQueryShow}
                        onConfirm = {this.handleQueryClick.bind(this)} 
                        handleClose={this.handleClose.bind(this)}
                    />
                </div>
            </div>
        )
    }
}
