import React, { Component } from 'react';
import {ajax,toast} from 'nc-lightapp-front';
export default function getDefaultAccountBook(appcode,json){   
    let pk_accountingbook={};
    let url='/nccloud/platform/appregister/queryappcontext.do';
    return  new Promise((resolve, reject) => {
        ajax({
            url:url,
            data:{appcode:appcode},
            // async:false,
            success:function(response){
                let { data, success } = response;
                    if(success){
                        pk_accountingbook.display=data.defaultAccbookName?data.defaultAccbookName:'';
                        pk_accountingbook.value=data.defaultAccbookPk?data.defaultAccbookPk:'';
                        pk_accountingbook.moduleid=data.paramMap ? data.paramMap.moduleid : '';
                    }
                    resolve(pk_accountingbook)
            },
            // error:function(error){
            //     toast({ content: json['publiccomponents-000088'], color: 'warning' });  /* 国际化处理： 请求失败*/
            // }
        })
    })
    // return pk_accountingbook; 
}
