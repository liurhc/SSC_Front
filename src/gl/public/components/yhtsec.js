~(function() {

	var pubkey_xmlHttpRequest = null;
	var remote_pubkey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCQfcXZZ4qHwLFptSsFl0ID1Hf4LIaphFuwJQmD1+XMlP1Hoy1QAmuCtWxRQ2z0iHXLfH+im5LneDw6NxE8zTPep+GcnejWQv0Nzy/Gewtf9sLpHWd6as8a1OXnk4I1keDccpP9ZrgG3+nZmlOntj2VMRLh4XXK3Gdiyi0Fjt5BCwIDAQAB';
	var ukeyInit = false;
	var checkCARet = -1;

	// documentdocument.write(' <script language="javascript" src="https://idtest.yyuap.com/cas/trd/topEsa.min.js" > </script>');
	// document.write(' <script language="javascript" src="https://idtest.yyuap.com/cas/trd/jquery/jquery.2.1.4.min.js" > </script>');
	// document.write(' <script language="javascript" src="https://idtest.yyuap.com/cas/trd/crypto/core.js" > </script>');
	// document.write(' <script language="javascript" src="https://idtest.yyuap.com/cas/trd/crypto/cipher-core.js" > </script>');
	// document.write(' <script language="javascript" src="https://idtest.yyuap.com/cas/trd/crypto/aes.js" > </script>');
	// document.write(' <script language="javascript" src="https://idtest.yyuap.com/cas/trd/crypto/pad-zeropadding.js" > </script>');
	// document.write(' <script language="javascript" src="https://idtest.yyuap.com/cas/trd/crypto/jsencrypt.min.js" > </script>');
	// document.close();

	function sleep(numberMillis) {
		var now = new Date();
		var exitTime = now.getTime() + numberMillis;
		while (true) {
			now = new Date();
			if (now.getTime() > exitTime)
				return;
		}
	}

	function generateRandom(n) {
		var chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		var res = "";
		for (var i = 0; i < n; i++) {
			var id = Math.ceil(Math.random() * 35);
			res += chars[id];
		}
		return res;
	}

	function getSelectedCert() {
		try {
			var certs = CertStore.listAllCerts(); // 此处可能会有性能问题
			return certs.get(0);
		} catch (e) {
		}
	}

	function initUKey() {
		var config = {
			"license": 'MIIFawYJKoZIhvcNAQcCoIIFXDCCBVgCAQExDjAMBggqgRzPVQGDEQUAMIGxBgkqhkiG9w0BBwGggaMEgaB7Iklzc3VlciI6IigoLipPPeWkqeWogeivmuS/oea1i+ivleezu+e7ny4qKXsxfSkiLCJ2ZXJzaW9uIjoiMS4wLjAuMSIsInNvZnRWZXJzaW9uIjoiMy4xLjAuMCIsIm5vdGFmdGVyIjoiMjAyNS0wOC0wNyIsIm5vdGJlZm9yZSI6IjIwMTUtMDgtMDciLCJub0FsZXJ0IjoidHJ1ZSJ9oIIDRDCCA0AwggLloAMCAQICFF8lnNrMgrt+8wWzAHuLjsm9+bXyMAwGCCqBHM9VAYN1BQAwVTEmMCQGA1UEAwwd5aSp6K+a5a6J5L+h5rWL6K+VU00y55So5oi3Q0ExDjAMBgNVBAsMBVRPUENBMQ4wDAYDVQQKDAVUT1BDQTELMAkGA1UEBhMCQ04wHhcNMTQwOTI2MDc0NjA4WhcNMTUwOTI2MDc0NjA4WjAxMRgwFgYDVQQDDA9TaWduRVNBMjAxNDA5MjcxFTATBgNVBAoMDOWkqeivmuWuieS/oTBZMBMGByqGSM49AgEGCCqBHM9VAYItA0IABJYWeFLmIy9mTud+ai0LBeLoxhgnO6HcQGbsQhl4fveJzoVx0Cyzt/xvWY5y7l3qAwd59AbI+Im6Ftl/wAOShYmjggGzMIIBrzAJBgNVHRMEAjAAMAsGA1UdDwQEAwIGwDCBigYIKwYBBQUHAQEEfjB8MHoGCCsGAQUFBzAChm5odHRwOi8vWW91cl9TZXJ2ZXJfTmFtZTpQb3J0L1RvcENBL3VzZXJFbnJvbGwvY2FDZXJ0P2NlcnRTZXJpYWxOdW1iZXI9NUE0N0VDRjEwNTgwNEE1QzZBNUIyMjkyOUI3NURGMERGQkMwRDc5NjBXBgNVHS4EUDBOMEygSqBIhkZQb3J0L1RvcENBL3B1YmxpYy9pdHJ1c2NybD9DQT01QTQ3RUNGMTA1ODA0QTVDNkE1QjIyOTI5Qjc1REYwREZCQzBENzk2MG8GA1UdHwRoMGYwZKBioGCGXmh0dHA6Ly9Zb3VyX1NlcnZlcl9OYW1lOlBvcnQvVG9wQ0EvcHVibGljL2l0cnVzY3JsP0NBPTVBNDdFQ0YxMDU4MDRBNUM2QTVCMjI5MjlCNzVERjBERkJDMEQ3OTYwHwYDVR0jBBgwFoAUPYnGR8txhbDZO9ZIsInZ5/7v2tkwHQYDVR0OBBYEFEs77X+HgoaHoBKSsS7mACXYtREAMAwGCCqBHM9VAYN1BQADRwAwRAIgvbTXF8yNH5jsbG6r7XL5LEupJd8l8x9akz8rhO5XYYICIOg+hxn5F44N5+waqG+1Dbs6m9xiID83VkHnmptdMoR7MYIBRTCCAUECAQEwbTBVMSYwJAYDVQQDDB3lpKnor5rlronkv6HmtYvor5VTTTLnlKjmiLdDQTEOMAwGA1UECwwFVE9QQ0ExDjAMBgNVBAoMBVRPUENBMQswCQYDVQQGEwJDTgIUXyWc2syCu37zBbMAe4uOyb35tfIwDAYIKoEcz1UBgxEFAKBpMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTE1MDgwNzE3MzAyM1owLwYJKoZIhvcNAQkEMSIEIBJeyebFtFcougN8kN5ifp/xrvvpdpJHPpvMi7oR2OSzMAwGCCqBHM9VAYItBQAERjBEAiD+AQLvSOXePUd9WHU5k8G3erod8GhQodK+GkEbvHh0vgIg4LEj7wpevBgJ88qrf/5HNTAeQP482Jb33xoGPFuj2Mw=',
			"exepath": 'https://idtest.yyuap.com/cas/download/\/u5929\/u8bda\/u5b89\/u4fe1\/u6570\/u5b57\/u8bc1\/u4e66\/u52a9\/u624b.exe'
		};
		try {
			TCA.config(config);
			ukeyInit = true;
			// initCertList();
			// uKeyLogin();
			return;
		} catch (e) {}
	}


	window.YHTTool = {
		checkCACallbackFun: null,
		encrypt: function(data) {
			var randomKey = generateRandom(16);
			var AESKey = CryptoJS.enc.Utf8.parse(randomKey);

			var encrypted = CryptoJS.AES.encrypt(data, AESKey, {
				iv: AESKey,
				mode: CryptoJS.mode.CBC,
				padding: CryptoJS.pad.ZeroPadding
			});

			var encry_AESValue = encrypted.toString();
			var encry_RSAValue = null;
			if (remote_pubkey) {
				var encrypt = new JSEncrypt();
				encrypt.setPublicKey(remote_pubkey);
				encry_RSAValue = encrypt.encrypt(randomKey);
			}
			if (encry_AESValue && encry_RSAValue) {
				return encry_AESValue + encry_RSAValue;
			} else {
				return '';
			}
		},
		checkCA: function(checkCACallbackFun) {
			try {
				if (!ukeyInit) {
					initUKey();
				}
				window.YHTTool.checkCACallbackFun = checkCACallbackFun;
				var randomKey = generateRandom(32);
				var sign = window.YHTTool.sign(randomKey);
				if (sign) {
					var script = document.createElement('script');
					script.type = 'text/javascript';
					script.src = "https://idtest.yyuap.com/cas/checkCA?data=" + randomKey + "&signData=" + encodeURIComponent(sign) + "&callback=checkCACallback";
					document.body.appendChild(script);
					return (checkCARet === true);
				} else {
					window.YHTTool.checkCACallbackFun && window.YHTTool.checkCACallbackFun(false);
					return false;
				}
			} catch (e) {
				window.YHTTool.checkCACallbackFun && window.YHTTool.checkCACallbackFun(false);
				return false;
			}
		},
		sign: function(data) {
			try {
				if (!ukeyInit) {
					initUKey();
				}
				var cert = getSelectedCert();
				if (cert) {
					return cert.signMessageRaw(data);
				}
			} catch (e) {
			}
			return null;
		},
		checkCACallback: function(ret) {
			window.YHTTool.checkCACallbackFun && window.YHTTool.checkCACallbackFun(ret.status);
		}
	}
})();
