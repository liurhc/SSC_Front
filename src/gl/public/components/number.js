import React, { Component } from 'react';
//import Input from '../nc_Input';
//import FormControl from '../nc_FormControl';
import PropTypes from 'prop-types';
import { getMultiLang } from 'nc-lightapp-front';
const propTypes = {
	min: PropTypes.number,
	max: PropTypes.number,
	onBlur: PropTypes.func,
	onChange: PropTypes.func
};

const defaultReg = /^[0-9|,]*(\.\d{0,4})?$/i;
class NCNumber extends Component {
	constructor(props){
		super(props)
		this.state=({
			json:{}
		})
	}
	componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json
               // columns_reportSum:this.columns_reportSum,
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
	handleChange = (e) => {
		const {
			onChange,
			processChange,
			scale = 4,
			value,
			reg = new RegExp('^[0-9|,]*(\\.\\d{0,' + scale + '})?$', 'i')
		} = this.props;
		let allowReg = new RegExp('^[0-9|,]*(\\.\\d{1,' + scale + '})?$', 'i');

		let verify = true,
			aimValue = this.removeThousands(e || '');

		if (!reg.test(aimValue) && aimValue != '') {
			aimValue = value;
		}

		if (!allowReg.test(aimValue) && aimValue != '') {
			verify = false;
		}

		if (onChange) {
			// onChange(this.removeThousands(e || ''));
			onChange(aimValue);
		}
	};

	handleBlur = (e) => {
		const {
			onChange,
			processChange,
			scale = 4,
			value,
			reg = new RegExp('^[0-9|,]*(\\.\\d{0,' + scale + '})?$', 'i'),
			isrequired = false
		} = this.props;

		let allowReg = new RegExp('^[0-9|,]*(\\.\\d{1,' + scale + '})?$', 'i');
		// if (!reg) {
		// 	reg = /^[0-9|,]*(\.\d{0,scale})?$/i;
		// }
		let verify = true,
			aimValue = this.removeThousands(e || '');
		if (!reg.test(aimValue) && aimValue != '') {
			aimValue = value;
		}

		if (!allowReg.test(aimValue) && aimValue != '') {
			verify = false;
		}

		if (isrequired && aimValue == '') {
			verify = false;
		}

		if (onChange) {
			onChange(aimValue);
		}
	};

	//精度千分位处理
	formatAcuracy(value, len = 2) {
		if (value === null || value === undefined) {
			return value;
		}

		return this.commafy(this.formatDot(value, len));
	}

	//移除千分位
	removeThousands(val) {
		return val ? val.toString().replace(/\,/gi, '') : val;
	}

	//数字转换成千分位 格式
	commafy(num) {
		let pointIndex, intPart, pointPart;

		if (isNaN(num)) {
			return '';
		}

		num = num + '';
		if (/^.*\..*$/.test(num)) {
			pointIndex = num.lastIndexOf('.');
			intPart = num.substring(0, pointIndex);
			pointPart = num.substring(pointIndex + 1, num.length);
			intPart = intPart + '';
			let re = /(-?\d+)(\d{3})/;
			while (re.test(intPart)) {
				intPart = intPart.replace(re, '$1,$2');
			}
			num = intPart + '.' + pointPart;
		} else {
			num = num + '';
			let re = /(-?\d+)(\d{3})/;
			while (re.test(num)) {
				num = num.replace(re, '$1,$2');
			}
		}
		return num;
	}

	// 精度处理
	formatDot(value, len = 6) {
		let formatVal, dotSplit, val;

		val = (value || 0).toString();

		dotSplit = val.split('.');

		if (dotSplit.length > 2 || !value) {
			return value;
		}

		if (val.indexOf('.') > -1) {
			formatVal = val.substring(0, val.indexOf('.') + len + 1);
		} else {
			formatVal = val;
		}

		return formatVal;
	}

	render() {
		const {
			defaultValue,
			type,
			isViewMode,
			onChange,
			value,
			isrequired,
			verify = true,
			scale = 4,
			ischecknow,
			pagestatus,
			suffix,
			...others
		} = this.props;
		let suffixDecorator = '';
		let errorMsg,
			errorBorder = {};

		if (suffix) {
			suffixDecorator = <span style={{ lineHeight: '30px' }}>{suffix}</span>;
		}

		//校验信息的控制
		if (!verify) {
			errorMsg = <span className="input-error-message">{this.state.json['publiccomponents-000093']}！</span>;/* 国际化处理： 请输入合法的数据*/
			errorBorder = 'error-border';
		}

		// value = this.formatDot(value, scale);

		//页面状态区分
		return (
			<div>
				<FormControl
					className="number-formcontrol"
					autoComplete="off"
					{...others}
					value={this.commafy(this.formatDot(value, scale))}
					// className={errorBorder}
					onChange={this.handleChange}
					onBlur={this.handleBlur}
				/>
				{suffixDecorator}
			</div>
		);
	}
}

NCNumber.propTypes = propTypes;

export default NCNumber;

//函数式组件
const HelloWorld = (props) => {
	const sayHi = (event) => alert('Hello World')
	return (
	  <div onClick={sayHi}>Hello World</div>
	)
  }

  //获取dom元素

  <input ref={(clock) => this.clock = clock}/>
