import React, { Component } from 'react';
import {base} from 'nc-lightapp-front';
const {NCIcon} = base;
import pubUtil from '../../common/pubUtil'
import ObjTypeUtil from '../../utils/ObjTypeUtil'

import AssidModal from '../assidModal';//辅助核算模态框
import './index.less'

export default class AssidModalRedner extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayName: (props.value || {}).display,
            isQueryShow: false,
            pretentAssData: {}
        };
    }
    componentDidMount(){
        if(this.props.onRef&&ObjTypeUtil.isFunction(this.props.onRef))
            this.props.onRef(this);
        if(!this.props.subjrelationset){//有这个参数就不自动弹框了
            this.iconClick();
        }
        
    }
    handleClose(){
        this.setState({
            isQueryShow: false, 
            pretentAssData: {
                pk_accountingbook:'',
                pk_accasoa:'',
                prepareddate:'',
                pk_org:'',
                assData:[] ,
                assid:'',
                checkboxShow:true,
                checkedAll:true
            }
        });
    }

    iconClick(){
        let pretentAssData = {prepareddate: pubUtil.getSysDateStr(),assData:[],...this.props.getPretentAssData()}
        if(pretentAssData.checkboxShow==null){
            pretentAssData.checkboxShow=true;
        }
        pretentAssData.checkedAll=true;
        this.setState({ pretentAssData: pretentAssData })
            window.setTimeout(()=>{
                this.setState({
                    isQueryShow: true });
            },10)
    }
    handleQueryClick(data){
        let display = (this.props.changeDisplay && this.props.changeDisplay(data)) || data.assname;
        if(!display){
            let displayname='';
            data.data.map((one)=>{
                if(one){
                    if(one.pk_Checkvalue){
                        displayname =displayname+ '【'+ one.checktypename + ':'+ (one.checkvaluename?one.checkvaluename:'~')+'】';
                        // continue;
                    }
                }
            })
            display=displayname;
        }
        let showname="";
        if(display.length > 10){
            showname=display.substring(0, 10) + '...';
        }else{
            showname= display;
        }
        this.setState({displayName: showname});
        let flag=this.props.doConfirm && this.props.doConfirm(data.assid, data, display);
        // let flag=true;
        //  window.setTimeout(()=>{
        //     flag=this.props.flag;
        // },1000);
        if(flag){
            if(flag==false){
                this.handleClose();
            }
        }else{
            this.handleClose();
        }
        
    }

    
    clear(){
        this.setState({displayName:'', pretentAssData: {}});
    }

    render() {
        let getClass=(flag)=>{
            if(flag == 'home'){
                if(this.props.position == 'form'){
                    return 'home nc-theme-area-split-bc nc-theme-from-input-bgc';
                }else{
                    return 'home-list nc-theme-area-split-bc nc-theme-from-input-bgc';
                }
            }else if(flag == 'button'){
                if(this.props.position == 'form'){
                    return 'assid-modal-button nc-theme-form-label-c';
                }else{
                    return 'assid-modal-button-list nc-theme-form-label-c';
                }
            }
        }
        let getDisplayName=()=>{
            if((this.props.foolValue || {}).display == ''){
                return '';
            }else{
                let display =this.state.displayName || '';
                if(this.props.foolValue){
                    if(this.props.foolValue.display){
                        display=this.props.foolValue.display;
                    }
                }
                // if((this.state.displayName || '').length > 10){
                if(display.length > 10){
                    return display.substring(0, 10) + '...';
                }else{
                    return display;
                }   
            }
        }
        let getDisplayTitle=()=>{
            if((this.props.foolValue || {}).display == ''){
                return '';
            }else{
                return this.state.displayName;
            }
        }
        return (
            <div id="assidModalRedner">
                <div className={getClass('home')}>
                    <table>
                        <tbody>
                            <tr>
                                <td className="assid-modal-title" title={getDisplayTitle()}>
                                    {getDisplayName()}
                                </td>
                                {/* <td className={getClass('button')} onClick={this.iconClick.bind(this)}> */}
                                <td className={getClass('button')}>
                                    <i className="iconfont icon-canzhaozuixin"
                                        onClick={this.iconClick.bind(this)} />
                                    {/* <NCIcon type="uf-3dot-h"/> */}
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <AssidModal 
                        pretentAssData={this.state.pretentAssData}
                        showOrHide = {this.state.isQueryShow}
                        onConfirm = {this.handleQueryClick.bind(this)} 
                        handleClose={this.handleClose.bind(this)}
                    />
                </div>
            </div>
        )
    }
}
