import {base} from 'nc-lightapp-front'
const {NCSelect} = base;
const NCOption = NCSelect.NCOption;

function FISelect(nextProps){
    let {children, ...newProps} = nextProps;
    let props = {showClear:false};
    Object.assign(props, newProps);
    return (
        <NCSelect {...props}>
            {children}
        </NCSelect>
        );
}

function FIOption(nextProps){
    let {children, ...newProps} = nextProps;
    Object.assign(props, newProps);
    return (
        <NCOption {...props}>
            {children}
        </NCOption>
    );
}

FISelect.FIOption = FIOption;

export {FISelect};