import React, { Component } from 'react';
import {base} from 'nc-lightapp-front'
const { NCTooltip, NCCheckbox } = base;
import './index.less';
class FICheckbox extends Component { 
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    static defaultProps = {
        showTip: true, // 悬浮是否显示提示信息,默认true:显示
    };
    render() {
        let {className, showTip, children, ...other} = this.props;
        let tip =  (<div>{children}</div>);
        return (
            <div>
                {showTip ? 
                    <NCTooltip inverse placement="top" overlay={tip}>
                        <NCCheckbox className={`${className} tip-checkbox`} {...other}>
                            {children}
                        </NCCheckbox>
                    </NCTooltip>
                :
                    <NCCheckbox className={className} {...other} >
                        {children}
                    </NCCheckbox>
                }
            </div>
        )
    }
}

export {FICheckbox}