import React, {Component} from 'react';
import {createPage, base} from 'nc-lightapp-front'
const {NCUpload, NCButton:Button} = base;

class UploadActionModal extends Component{
    constructor(props){
        super(props);
        this.state = {
            show : false
        }
    } 
    
    componentWillReceiveProps(nextProps){
        let page = this;
        let {show} = this.state;
        if(nextProps.show == show){
            return;
        }
        this.setState({show:nextProps.show})
        if(nextProps.show){
            page.props.modal.show('upload');
        }else{
            page.props.modal.close('upload');
        }
    }

    modalContent(props){
		let action = props.action;
        let data = props.data;
        let handleDone = props.handleDone;
        let handleError = props.handleError;
		let propsData = {
			name: 'file',
			data:{params:JSON.stringify(data)},
			action: action,
			accept: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			headers: {
			  authorization: 'authorization-text',
			},
			NcUploadOnChange(info) {
				console.log(info);
				if (info.file.status === 'done') {
					if(info && info.fileList && info.fileList.length > 0){
                        let response = info.fileList[0].response;
                        if(response){
                            if(response.success){
                                handleDone(response);
                            }else{
                                handleError(response);
                            }
                        }
					}
				} else if (info.file.status === 'error') {
                    // toast({ content: '导入失败', color: 'danger' })/* 国际化处理： 导入失败*/
                    handleError();
				}
			}
		}
		return (<div>
            <NCUpload {...propsData}>
                <Button fieldid='upload' shape="border" colors= 'primary'>
					上传文件
                </Button>
            </NCUpload>
		</div>);        
    }

    render(){
        let page = this;
        let props = page.props;
        return (
            <div>
                {
                    props.modal.createModal('upload', {
                        title:props.title,
                        content:page.modalContent(props),
                        closeModalEve:page.props.handleClose,
                        noFooter:true
                    })
                }
            </div>
        );
    }
}

UploadActionModal = createPage({})(UploadActionModal);
export default UploadActionModal;