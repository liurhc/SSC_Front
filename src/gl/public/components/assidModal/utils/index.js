function generatePretentAssData(pretentAssData){
    let data = {
        pk_accountingbook: "", /* 核算账簿主键 */
		pk_accasoa: "", /* 科目主键 */
		prepareddate: "", /* 制单日期 */
		pk_org: "", /* 组织主键 */
		assData: [], /* 辅助核算信息 */
		checkboxShow: true, /* 是否显示选中框 */
		linenum: 0, /*  */
		checkedAll: true	/* 是否默认全选 */
    }

    if(pretentAssData){
        Object.assign(data, pretentAssData);
    }
    return data;
}

function generateAssDatas(record) {
	let assDatas = [];
	let { assItems, assValues } = record.values;
	if (assItems && assItems.value) {
		assItems.value.map((item, index) => {
			let data = {};
			data.key = index;
			data.checktypecode = item.code;
			data.checktypename = item.name;
			data.pk_Checktype = item.pk_accassitem;
			data.refnodename = item.refnodename;
			data.refCode = data.refnodename;
			data.m_classid = item.classid;
			data.classid = item.classid;
			data.pk_accassitem = item.pk_accassitem;
			data.pk_defdoclist = item.classid;
			assDatas.push(data);
		});
	}
	if (assValues && assValues.value) {
		assValues.value.map((item) => {
			if (assDatas && assDatas.length > 0) {
				assDatas.map((data) => {
					if (data.pk_Checktype == item.m_pk_checktype) {
						data.pk_Checkvalue = item.m_pk_checkvalue;
						data.checkvaluename = item.m_checkvaluename;
						data.checkvaluecode = item.m_checkvaluecode;
					}
					return data;
				});
			}
		});
	}
	return assDatas;
}


function generateAssValues(data) {
	const assValues = [];
	if (data && data.data && data.data.length > 0) {
		data.data.map((item) => {
			const assValue = {
				m_pk_checkvalue: item.pk_Checkvalue,
				m_checkvaluename: item.checkvaluename,
				m_checkvaluecode: item.checkvaluecode,
				m_pk_checktype: item.pk_Checktype,
				m_checktypename: item.checktypename
			};
			assValues.push(assValue);
		});
	}
	return assValues;
}

export {generatePretentAssData, generateAssDatas, generateAssValues};