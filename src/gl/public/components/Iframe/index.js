import React, { Component } from 'react';

export default class Iframe extends Component {
	constructor(props) {
		super(props);
		this.state = {}
    }
    static defaultProps = {
        fieldid: 'printService',
        id: 'printServiceframe',
        name: 'printServiceframe'
    };
    render(){
        let { id, name, fieldid } = this.props
        return(
            <iframe fieldid={`${fieldid}_iframe`} id={id} name={name} style={{ display: 'none' }}></iframe>
        )
    }
}
