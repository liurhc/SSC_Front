import React, { Component } from 'react';
import { base, ajax, toast,getMultiLang } from 'nc-lightapp-front';
const { NCButton: Button, NCTextArea: TextArea, NCModal: Modal,NCRow: Row, NCCol: Col, } = base;

import FormulaParam from './FormulaParam';
import DefaultFormula from './DefaultFormula';
import './index.less';

/**
 * 
 * 计算器弹框
 * 
 * 接收父级参数：visible, pk_accountingbook, pk_org
 * 
 */

export default class FormulaModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json:{},
			appcode: this.props.appcode,
			showFormulaParamModal: false, // 控制公式弹框是否显示
			showDefaultFormula: false, // 自定义公式弹框是否显示
			paramType: '',   //选中的数据
			funcName: [],
			textValue: this.props.defaultValue
		};
		this.handleFormulaOK;
	}

	// 清空
	handleClear() {
		this.setState({
			textValue: ''
		});
	}

	// 确定
	handleFormulaOK() {
		let { textValue } = this.state;
		let { handleFormulaOK } = this.props;
		if (handleFormulaOK) {
			handleFormulaOK(textValue);
			this.handleClear();
		}
	}

	// 取消
	handleCancel() {
		let { handleCancel } = this.props;
		if (handleCancel) {
			handleCancel();
		}
	}

	// 展示公式弹框
	showFormulaParamModal(item) {
		this.setState({
			showFormulaParamModal: true,
			paramType: item
		});
	}

	// 点击公式弹框确定按钮
	handleFormulaParamOK(paramData) {
		let { textValue } = this.state;
		let tempStr = textValue ? textValue + paramData : '' + paramData;
		this.setState(
			{
				showFormulaParamModal: false,
				textValue: tempStr
			});
	}

	// 展示自定义公式弹框
	showDefaultFormula = () => {
			
		this.setState({
			showDefaultFormula: true
		});
	}
	
	// 点击公式弹框确定按钮
	handleDefaultFormulaOK(paramData) {
		let { textValue } = this.state;
		let tempStr = textValue ? textValue + paramData : '' + paramData;
		this.setState(
			{
				showDefaultFormula: false,
				textValue: tempStr
			});
	}

	// 节点列表
	getFormulaName() {
		let url = '/nccloud/gl/glpub/getFuncs.do';
		let self = this;
		let sendData = {
			nodeCode: 'transfer'
		};
		ajax({
			url,
			data: sendData,
			success: function(response) {
				const { data, success } = response;
				if (success) {
					if (data) {
						self.setState({
							funcName: data
						});
					}
				} else {
					toast({ content: error.message, color: 'warning' });
				}
			},
			error: function(res) {
				toast({ content: res.message, color: 'warning' });
			}
		});
	}

	componentWillReceiveProps(nextProps) {
		let { textValue } = this.state;
		textValue = nextProps.defaultValue;
		this.setState({ textValue });
		this.props.handleFormulaOK = nextProps.handleFormulaOK;
	}

	componentDidMount() {
		this.getFormulaName();
	}

	// 列表渲染
	renderSpan(funcName) {
		return funcName.map((item, index) => {
			return <p className="nc-theme-common-font-c" fieldid={`${item.display}_item`} onClick={this.showFormulaParamModal.bind(this, item)}>{item.display}</p>;
		});
	}
	componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json
               // columns_reportSum:this.columns_reportSum,
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
	render() {
		let { visible, pk_accountingbook, pk_org } = this.props;
		let { textValue, funcName } = this.state;
		return (
			<div className="formulaModal">
				<Modal fieldid='formula' show={visible} onHide={this.handleCancel.bind(this)}>
					<Modal.Header closeButton>
						<Modal.Title>{this.state.json['publiccomponents-000083']}</Modal.Title>{/* 国际化处理： 金额公式*/}
					</Modal.Header>
					<Modal.Body>
						<div className="formulaform">
							<div className="des_title">
								<span className="nc-theme-common-font-c">{this.state.json['publiccomponents-000084']}：</span>{/* 国际化处理： 公式*/}
								<Button className="clear-btn" fieldid='clear' onClick={this.handleClear.bind(this)}>
									{this.state.json['publiccomponents-000085']}{/* 国际化处理： 清空*/}
								</Button>
							</div>
							<TextArea
								fieldid='formula_result'
								defaultValue={textValue}
								value={textValue}
								onChange={(v) => {
									this.setState({
										textValue: v
									});
								}}
								onBlur={(e) => {
									e.target.value;
								}}
							/>
							<Row>
								<Col md={12} xs={12} sm={12}>
									<div className="des_title nc-theme-common-font-c">{this.state.json['publiccomponents-000086']}：</div>{/* 国际化处理： 总账函数*/}
									{funcName ? <div className="func" fieldid='formula_list'>{this.renderSpan(funcName)}</div> : ''}
								</Col>
								{/* 国际化处理： 自定义函数*/}
								{/* <Col md={6} xs={6} sm={6}>						
									<div className="des_title">{this.state.json['publiccomponents-000069']}：</div>
									<div className="func">
									
									<p onClick={this.showDefaultFormula.bind(this)}>
										SELFDEF_FormExcel
									</p>
									</div>
								</Col> */}
							</Row>
							
						</div>
						
					</Modal.Body>
					<Modal.Footer>
						<Button className="button-primary" fieldid='confirm' onClick={this.handleFormulaOK.bind(this)}>
							{this.state.json['publiccomponents-000009']}{/* 国际化处理： 确定*/}
						</Button>
						<Button fieldid='cancel' onClick={this.handleCancel.bind(this)}>{this.state.json['publiccomponents-000010']}</Button>{/* 国际化处理： 取消*/}
					</Modal.Footer>
				</Modal>
				<FormulaParam
					appcode={this.state.appcode}
					pk_accountingbook={pk_accountingbook}
					pk_org={pk_org}
					paramType={this.state.paramType}
					visible={this.state.showFormulaParamModal}
					handleFormulaParamOK={this.handleFormulaParamOK.bind(this)}
					handleCancel={() => {
						this.setState({
							showFormulaParamModal: false
						});
					}}
				/>
				{/* <DefaultFormula 
					visible={this.state.showDefaultFormula}	
					handleDefaultFormulaOK={this.handleDefaultFormulaOK.bind(this)}	
					handleCancel={() => {
						this.setState({
							showDefaultFormula: false
						});
					}}		
				/> */}
			</div>
		);
	}
}
