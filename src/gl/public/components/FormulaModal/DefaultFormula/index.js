import React, { Component } from 'react';
import { base,getMultiLang } from 'nc-lightapp-front';
// const { NCModal } = base
const { NCButton: Button, NCRadio: Radio,
    NCRow: Row, NCCol: Col, NCLabel: Label, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, NCModal: Modal, NCForm
} = base;
const { NCFormItem: FormItem } = NCForm;
// import * as XLSX from 'xlsx';
//import { CheckboxItem, RadioItem, SelectItem } from '../../FormItems';
import './index.less'
/**
 * 
 */
// const headData = [
//     // { itemName: '打印范围:', itemType: 'radio', itemKey: 'scopeall', itemChild: [{ value: 'n', label: '当前' }, { value: 'y', label: '全部' }] },
//     { itemName: this.state.json['publiccomponents-000054'], itemType: 'ncNum', itemKey: 'tab' },/* 国际化处理： 页签:*/
//     { itemName: this.state.json['publiccomponents-000055'], itemType: 'ncNum', itemKey: 'column' },/* 国际化处理： 列:*/
//     { itemName: this.state.json['publiccomponents-000056'], itemType: 'ncNum', itemKey: 'row' }/* 国际化处理： 行:*/
// ]

const formItemLayout = { labelXs:3, labelSm:3, labelMd:3, xs:9, md:9, sm:9 };
const formItemLayoutNCNum = { labelXs:4, labelSm:4, labelMd:4, xs:8, md:8, sm:8 };
const formItemParam = { inline:true, showMast:false, isRequire:true, method:"change" };

export default class DefaultFormula extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            // appcode: this.props.appcode,    //获取模板需要
            listItem: {
                // scopeall: { display: '', value: 'n' },
                tab: { display: '', value: '' },
                column: { display: '', value: '' },
                row: { display: '', value: '' }
            },
        };
    }

    handleCancel() {
        let { handleCancel } = this.props
        if (handleCancel) {
            handleCancel()
        }
    }
    handleDefaultFormulaOK() {
        let { handleDefaultFormulaOK } = this.props
        let { listItem } = this.state
        let baseData = {
            // scopeall: '',
            tab: '',
            column: '',
            row: ''
        }
        for (const key in listItem) {
            if (listItem.hasOwnProperty(key)) {
                baseData[key] = listItem[key].value
            }
        }
        if (handleDefaultFormulaOK) {
            handleDefaultFormulaOK(baseData)
        }
    }
    
    renderNCNumber(listItem, item) {
        return (
            <FormItem
                {...formItemLayoutNCNum}
                {...formItemParam}
                labelName={item.itemName}
            >
                <NCNumber style={{ width: '80px' }}
                    scale={0}
                    // placeholder="请输入数量"
                    value={listItem[item.itemKey].value}
                    onChange={(val) => {
                        listItem[item.itemKey].value = val
                        this.setState({
                            listItem
                        })
                    }}
                />
            </FormItem>
        )
    }

    renderForm(listItem) {
        let checkArr = []
        const headData = [
            // { itemName: '打印范围:', itemType: 'radio', itemKey: 'scopeall', itemChild: [{ value: 'n', label: '当前' }, { value: 'y', label: '全部' }] },
            { itemName: this.state.json['publiccomponents-000054'], itemType: 'ncNum', itemKey: 'tab' },/* 国际化处理： 页签:*/
            { itemName: this.state.json['publiccomponents-000055'], itemType: 'ncNum', itemKey: 'column' },/* 国际化处理： 列:*/
            { itemName: this.state.json['publiccomponents-000056'], itemType: 'ncNum', itemKey: 'row' }/* 国际化处理： 行:*/
        ]
        
        for (var i = 0; i < headData.length; i++) {
            if (headData[i].itemType === 'checkbox') {
                checkArr.push(headData[i])
            }
        }
        return (
            headData.map((item, i) => {
                switch (item.itemType) {
                    // case 'radio':
                    //     return this.renderRadio(listItem, item)                      
                    // case 'select':
                    //     return this.renderSelect(listItem, item)
                    // case 'checkbox':
                    //     return this.renderCheckbox(listItem, item)                       
                    case 'ncNum':
                        return this.renderNCNumber(listItem, item)
                    // case 'refer':
                    //     return this.renderReferLoader(listItem, item)                       
                    default:
                        break;
                }
            })

        )
    }
    componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json
               // columns_reportSum:this.columns_reportSum,
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
    //FX获取文件路径方法
    readFileFirefox = (fileBrowser) => {
        try {
            netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
        } 
        catch (e) {
            alert(this.state.json['publiccomponents-000058']+','+this.state.json['publiccomponents-000059']+'。'+this.state.json['publiccomponents-000060']+'，'+this.state.json['publiccomponents-000061']+'：(1)'+this.state.json['publiccomponents-000062']+"about:config"+';(2)' +this.state.json['publiccomponents-000063'] +'New->'+Boolean+';(3)' + this.state.json['publiccomponents-000064']+"signed.applets.codebase_principal_support"+ '（'+this.state.json['publiccomponents-000065']+'）'+this.state.json['publiccomponents-000066']+';(4)'+ this.state.json['publiccomponents-000067']+OK+this.state.json['publiccomponents-000068']);/* 国际化处理： 无法访问本地文件,由于浏览器安全设置,为了克服这一点,请按照下列步骤操作,在地址栏输入,右键点击并选择,输入,不含引号,作为一个新的首选项的名称,点击,并试着重新加载文件*/
            return;
        }
        var fileName=fileBrowser.value; //这一步就能得到客户端完整路径。下面的是否判断的太复杂，还有下面得到ie的也很复杂。
        var file = Components.classes["@mozilla.org/file/local;1"]
        .createInstance(Components.interfaces.nsILocalFile);
        try {
            // Back slashes for windows
            file.initWithPath( fileName.replace(/\//g, "\\\\") );
        }
        catch(e) {
            if (e.result!=Components.results.NS_ERROR_FILE_UNRECOGNIZED_PATH) throw e;
            alert("File '" + fileName + "' cannot be loaded: relative paths are not allowed. Please provide an absolute path to this file.");
            return;
        }
        if ( file.exists() == false ) {
            alert("File '" + fileName + "' not found.");
            return;
        }  
        return file.path;
    }
    //根据不同浏览器获取路径
    getvl = () => {
        //判断浏览器
        var Sys = {}; 
        var ua = navigator.userAgent.toLowerCase(); 
        var s; 
        (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] : 
        (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] : 
        (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] : 
        (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] : 
        (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;
        var file_url="";
        if(Sys.ie<="6.0"){
            //ie5.5,ie6.0
            // file_url = obj.value;
            file_url = document.getElementById("file").value; 
        }else if(Sys.ie>="7.0"){
        //ie7,ie8
            var file = document.getElementById("file"); 
            file.select();
            // file.blur();
            file_url = document.selection.createRange().text
        }else if(Sys.firefox){
            //fx
            //file_url = document.getElementById("file").files[0].getAsDataURL();//获取的路径为FF识别的加密字符串
            file_url = readFileFirefox(document.getElementById("file")); 
        }}

    change(picId,fileId) {
        var pic = document.getElementById(picId);
        var file = document.getElementById(fileId);
        if(window.FileReader){//chrome,firefox7+,opera,IE10,IE9，IE9也可以用滤镜来实现
            oFReader = new FileReader();
            oFReader.readAsDataURL(file.files[0]);
            oFReader.onload = function (oFREvent) {pic.src = oFREvent.target.result;};  
        }
        else if (document.all) {//IE8-
            file.select();
            var reallocalpath = document.selection.createRange().text//IE下获取实际的本地文件路径
            if (window.ie6) pic.src = reallocalpath; //IE6浏览器设置img的src为本地路径可以直接显示图片
            else { //非IE6版本的IE由于安全问题直接设置img的src无法显示本地图片，但是可以通过滤镜来实现，IE10浏览器不支持滤镜，需要用FileReader来实现，所以注意判断FileReader先
                pic.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod='image',src=\"" + reallocalpath + "\")";
                pic.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';//设置img的src为base64编码的透明图片，要不会显示红xx
            }
        }
        else if (file.files) {//firefox6-
            if (file.files.item(0)) {
                url = file.files.item(0).getAsDataURL();
                pic.src = url;
            }
        }
    }

    getObjectURL(file) {  
        var url = null;  
        if (window.createObjcectURL != undefined) {  
            url = window.createOjcectURL(file);  
        } else if (window.URL != undefined) {  
            url = window.URL.createObjectURL(file);  
        } else if (window.webkitURL != undefined) {  
            url = window.webkitURL.createObjectURL(file);  
        }  
        return url;  
    }

    // onImportExcel = file => {
    //     // 获取上传的文件对象
    //     const { files } = file.target;

    //     // 通过FileReader对象读取文件
    //     const fileReader = new FileReader();
    //     fileReader.onload = event => {
    //     try {
    //         const { result } = event.target;
    //         // 以二进制流方式读取得到整份excel表格对象
    //         const workbook = XLSX.read(result, { type: 'binary' });

    //         var sheetNames = workbook.SheetNames; // 工作表名称集合
    //         sheetNames.forEach(name => {
    //             var worksheet = workbook.Sheets[name]; // 只能通过工作表名称来获取指定工作表
    //             for(var key in worksheet) {
    //                 // v是读取单元格的原始值
    //             }
    //         });

    //         let data = []; // 存储获取到的数据
    //         // 遍历每张工作表进行读取（这里默认只读取第一张表）
    //         for (const sheet in workbook.Sheets) {
    //             if (workbook.Sheets.hasOwnProperty(sheet)) {
    //                 // 利用 sheet_to_json 方法将 excel 转成 json 数据
    //                 // data = data.concat(XLSX.utils.sheet_to_json(workbook.Sheets[sheet]));
    //                 // break; // 如果只取第一张表，就取消注释这行
    //                 // trim(oSheet.Cells(row_start,colcount).value)
    //                 //wb.SheetNames[0]是获取Sheets中第一个Sheet的名字
    //                 //wb.Sheets[Sheet名]获取第一个Sheet的数据
    //                 data=data.concat( XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]) );
    //             }
    //         }
    //     } catch (e) {
    //         // 这里可以抛出文件类型错误不正确的相关提示
    //         return;
    //     }
    //     };
    //     // 以二进制方式打开文件
    //     fileReader.readAsBinaryString(files[0]);
    // }

    // readExcel(){
    //     var filePath="C:\Users\Administrator\Desktop\initAssExport.xlsx"; //要读取的xls
    //     var sheet_id=1; //读取第2个表
    //     var row_start=2; //从第3行开始读取
    //     var tempStr='';
    //     try{
    //         var oXL = new ActiveXObject("Excel.application"); //创建Excel.Application对象
    //     }catch(err)
    //     {
    //         alert(err);
    //     }
    //     var oWB = oXL.Workbooks.open(filePath);
    //     oWB.worksheets(sheet_id).select();
    //     var oSheet = oWB.ActiveSheet;
    //     // var colcount=oXL.Worksheets(sheet_id).UsedRange.Cells.Rows.Count ;
    //     var colcount=3
    //     tempStr = trim(oSheet.Cells(row_start,colcount).value)
    //     // for(var i=row_start;i<=colcount;i++){
    //     //     if (typeof(oSheet.Cells(i,8).value)=='date'){ //处理第8列部分单元格内容是日期格式时的读取问题
    //     //         d= new Date(oSheet.Cells(i,8).value);
    //     //         temp_time=d.getFullYear()+"-"+(d.getMonth() + 1)+"-"+d.getDate();
    //     //     }
    //     //     else
    //     //         temp_time=$.trim(oSheet.Cells(i,7).value.toString());
    //     //     tempStr+=($.trim(oSheet.Cells(i,2).value)+" "+$.trim(oSheet.Cells(i,4).value)+" "+$.trim(oSheet.Cells(i,6).value.toString())+" "+temp_time+"\n");
    //     //     //读取第2、4、6、8列内容
    //     // }
    
    //     return tempStr; //返回
    //     // oXL.Quit();
    //     // CollectGarbage();
    // }

    render() {
        let { visible } = this.props
        let { listItem } = this.state

        return (
            <div className='defaultformulamodal'>
                <Modal show={visible} backdrop={false} onHide={this.handleCancel.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.json['publiccomponents-000069']}</Modal.Title>{/* 国际化处理： 自定义函数*/}
                    </Modal.Header >
                    <Modal.Body style={{ borderTop: '1px solid #ccc', borderBottom: '1px solid #ccc' }}>
                        <div className='defaultformulaform'>
                            {/* <div id="text" style={{color: '#f00'}}></div> 
                            <input type="file" id="file" onChange={this.onImportExcel}/> 
                            <input name="" type="button" value="获取" onClick={this.readExcel.bind(this)} /> */}
                            {this.renderForm(listItem)}
                        </div>
                    </Modal.Body>
                    <Modal.Footer >
                        <Button size="sm" onClick={this.handleDefaultFormulaOK.bind(this)}>{this.state.json['publiccomponents-000070']}</Button>{/* 国际化处理： 确认*/}
                        <Button onClick={this.handleCancel.bind(this)}>{this.state.json['publiccomponents-000010']}</Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div >

        )
    }
}


