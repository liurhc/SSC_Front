/*
 * @Description: 表单区lable
 * @Author: llixxm
 */
import React, { Component } from 'react';

const style = {
    container: {
        display: 'flex'
    },
    require: {
        color: 'red'
    }
}
class FormLabel extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    static defaultProps = {
        isRequire: false, //是否是必输项,默认不显示*
        labelname: '' //要显示的文字
    };
    render() {
        let { isRequire, labelname, children } = this.props
        return(
            <div style={style.container}>
                {isRequire ? <span style={style.require}>*</span> : ''}
                <label className='nc-theme-form-label-c'>
                    {labelname}：
                </label>
                {children}
            </div>
        );
    }
}
export default FormLabel