import React, { Component } from 'react';
import { base, getMultiLang } from 'nc-lightapp-front';
import './index.less';
const { NCRow:Row, NCCol:Col, NCTable:Table, NCDiv } = base;

class TrialResultTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
            json:{},
            data: {},
		}
    }
    static defaultProps = {}
    componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json,
              data: this.props.data
			},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
    shouldComponentUpdate(nextProps, nextState) {
        // if(this.props.data == nextProps.data) {
        //     return false
        // }
        return true;
    }
    componentWillUpdate(nextProps, nextState) {
        if(this.props.data !== nextProps.data){         
            // this.setState({
            //     data : nextProps.data,
            // })
            this.props.data = nextProps.data
        }
    }
    componentDidUpdate(prevProps, prevState) {
    }
    getColumns = (json) => {
        const emptyCell = <span>&nbsp;</span>
        let columns= [
			{ 
                title: (<div fieldid='accountName'>{json['publiccomponents-000134']}</div>),
                dataIndex: "accountName", 
                key: "accountName", 
                width: '20%',
                render: (text, record, index) => <div fieldid='accountName'>{text ? text : emptyCell}</div>
            },/* 国际化处理： 科目类型*/
			{ 
                title: (<div fieldid='incur-dblDebit'>{json['publiccomponents-000135']}</div>),
                dataIndex: "incur.dblDebit", 
                key: "incur.dblDebit", 
                width: '20%',
                render: (text, record, index) => {
					return (
						<div className='text_r' fieldid='incur-dblDebit'>{text ? text : emptyCell}</div>
					)
				}
            },/* 国际化处理： 借方*/
			{ 
                title: (<div fieldid='incur-dblCredit'>{json['publiccomponents-000136']}</div>),
                dataIndex: "incur.dblCredit", 
                key: "incur.dblCredit", 
                width: '20%',
                render: (text, record, index) => {
					return (
						<div className='text_r' fieldid='incur-dblCredit'>{text ? text : emptyCell}</div>
					)
				} 
            },/* 国际化处理： 贷方*/
			{ 
                title: (<div fieldid='balanOrient'>{json['publiccomponents-000080']}</div>),
                dataIndex: "balanOrient.display", 
                key: "balanOrient.display", 
                width: '20%', /* 国际化处理： 方向*/
				render: (text, record, index) => {
                    return(
                            <div className={(text!==json['publiccomponents-000137']) ? 'light_red' : ''} fieldid='balanOrient'>
                                {text ? text : emptyCell}
                            </div>/* 国际化处理： 平*/
                    )
				}
			},
			{ 
                title: (<div fieldid='balancedisp'>{json['publiccomponents-000138']}</div>),
                dataIndex: "balancedisp", 
                key: "balancedisp", 
                width: '20%',/* 国际化处理： 余额*/
				render: (text, record, index) => {
					return (
						<div className='text_r' fieldid='balancedisdiv'>{text ? text : emptyCell}</div>
					)
				}
			},
        ]
        return columns
    }

    addEmptyRow(leftVOs, rightVOs){
        if(leftVOs.length == rightVOs.length) return;
        let needAddRows = leftVOs.length < rightVOs.length ? leftVOs : rightVOs;
        let notNeedRows = leftVOs.length < rightVOs.length ? rightVOs : leftVOs;
        let startIndex = needAddRows.length - 1;
        let endIndex = notNeedRows.length - 1;
        let emptyData = {
            accountName: '',
            incur: {dblDebit: null, dblCredit:null},
            balanOrient: {display: '',scale:null,value:''},
            balancedisp: ''
        }
        for(let i = startIndex; i < endIndex; i++)
            needAddRows.splice(i, 0, emptyData);
    }

    render() {
        let emptyData = {
            accountName: '',
            incur: {dblDebit: null, dblCredit:null},
            balanOrient: {display: '',scale:null,value:''},
            balancedisp: ''
        }
        let { data, showUnits, typeName } = this.props
        let { json } = this.state
        let columns = this.getColumns(json)

        //添加空白行
        let leftVOs = [...data.leftVOs]
        let rightVOs = [...data.rightVOs]
        // let endIndex = leftVOs.length - 1
        // let startIndex = rightVOs.length - 1
        // for(let i=startIndex; i<endIndex; i++){
        //     rightVOs.splice(i, 0, emptyData)
        // }
        this.addEmptyRow(leftVOs, rightVOs);

        return(
            <div className="trialResultTable">
                <NCDiv fieldid="total" areaCode={NCDiv.config.Area}>
                    <div className='container_des'>
                        {/* {showUnits ? (<div>{json['publiccomponents-000139'] + data.name}</div>) : (<div/>)}国际化处理： 业务单元： */}
                        <div fieldid='typeName' className='nc-theme-title-font-c'>{typeName}</div>
                        <div fieldid='result' className='nc-theme-title-font-c'>
                            {data.balanResult.value == '3' ? <div>{json['publiccomponents-000140']}</div>: <div className="light_red">{json['publiccomponents-000141']}</div>}{/* 国际化处理： 试算结果平衡,试算结果不平衡*/}
                        </div>
                        <div fieldid='incurDiff' className='nc-theme-title-font-c'> 
                            {json['publiccomponents-000142']}：{/* 国际化处理： 借贷发生额合计差额*/}
                            <span className='num_color'>{data.incurDiff}</span>
                        </div>
                        <div fieldid='balanDiff' className='nc-theme-title-font-c'>
                            {json['publiccomponents-000143']}：{/* 国际化处理： 借贷余额合计差额*/}
                            <span className='num_color'>{data.balanDiff}</span>
                        </div>
                    </div>					
                </NCDiv>

                <div className="container_table">
                    <Row>
                        <Col md={6} xs={6} sm={6} style={{padding:'0px'}}>
                            <NCDiv fieldid="left" areaCode={NCDiv.config.TableCom}>
                                <Table
                                    columns={columns}
                                    // bordered
                                    data={leftVOs}
                                />
                            </NCDiv>
                        </Col>
                        <Col md={6} xs={6} sm={6} style={{padding: '0px'}}>
                            <NCDiv fieldid="right" areaCode={NCDiv.config.TableCom}>
                                <Table
                                    columns={columns}
                                    // bordered
                                    data={rightVOs}
                                />
                            </NCDiv>  
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}


export default TrialResultTable
