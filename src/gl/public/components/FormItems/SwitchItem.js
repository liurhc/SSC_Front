import React, { Component } from 'react';
import {high,base } from 'nc-lightapp-front';
const { NCSwitch: Switch} = base;

export default class SwitchItem extends Component {
	state = {
		value: this.props.defaultValue,
		defaultValue: this.props.defaultValue
	}

	handleChange = (value) => {
		const { onChange } = this.props;

		this.setState({
			value: value
		});

		if (onChange) {
			onChange(value);
		}
	}



	componentWillReceiveProps(nextProp) {
		if (nextProp.defaultValue !== this.state.defaultValue) {
			this.setState({
				value: nextProp.defaultValue,																																																						
				defaultValue: nextProp.defaultValue
			});
		}
	}

	render() {

		const { defaultValue, onChange,  ...others} = this.props;

		return <Switch  { ...others }  checked={ this.state.value }  onChange={ this.handleChange } /> 
	}
}
