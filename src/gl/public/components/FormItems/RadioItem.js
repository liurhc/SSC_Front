import React, { Component } from 'react';
//import { Radio } from 'tinper-bee'
import {high,base,ajax } from 'nc-lightapp-front';
const { NCRadio
} = base;


export default class RadioItem extends Component {

	state = { 
		items: this.props.items(), 
		defaultValue: this.props.defaultValue,
		value: this.props.defaultValue
	}


	handleChange = value => {
		this.setState({
			value: value
		});
		this.props.onChange(value);
	}


	componentWillReceiveProps(nextProp) {
		if (nextProp.defaultValue !== this.state.defaultValue) {
			this.setState({
				items: nextProp.items(),
				defaultValue: nextProp.defaultValue,
				value: nextProp.defaultValue,
			});
		}
	}



	render() {
		let { items }  = this.state;
		let {value, ...others } = this.props;
		return <NCRadio.NCRadioGroup {...others}
					selectedValue={ this.state.value }
					onChange={ this.handleChange } >
				{items.map((item, i) =>
					 <NCRadio color="primary" disabled={!!item.disabled} value={item.value} key={i} >
					 {item.label}
					 </NCRadio>) }
			</NCRadio.NCRadioGroup>
			
	}

}
