import React, { Component } from 'react';
import {high,base,ajax } from 'nc-lightapp-front';
import {FISelect} from '../base'
const FIOption = FISelect.FIOption;

export default class SelectItem extends Component {

	state = { 
		items: this.props.items(),
		defaultValue: this.props.defaultValue,
		value: this.props.defaultValue
	}

	handleChange = value => {
		const { onChange } = this.props;
		this.setState({
			value: value
		});
		if (onChange) {
			onChange(value);
		}

	}

	componentWillReceiveProps(nextProp) {
		if (nextProp.defaultValue !== this.state.defaultValue || nextProp.items() != this.state.items) {
			this.setState({ 
				items: nextProp.items(),
				defaultValue: nextProp.defaultValue, 
				value: nextProp.defaultValue
			});
		}
	}


	render() {
		
		let { items }  = this.state;
		let { value, defaultValue,  ...others } = this.props;
		return  <FISelect   {...others} value={ this.state.value } onChange={ this.handleChange } >
					{ items.map((item, i) => <FIOption value={String(item.value)}  key={i} >{item.label}</FIOption>) }
                </FISelect>

	}

}
