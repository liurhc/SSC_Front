import React, { Component } from 'react';
//import Refer from 'components/Refer';
import {high } from 'nc-lightapp-front';
const { Refer } = high;

export default class ReferItem extends Component {
	state = {
		value: this.props.defaultValue || {},
		disabled: this.props.disabled,
		defaultValue: this.props.defaultValue || {}
	}


	handleChange = (value) => {
		// this.setState({
		// 	value: value
		// }); 
		this.props.onChange(value);
	}

	componentWillReceiveProps(nextProp) {

		// if (nextProp.defaultValue.refpk != this.state.defaultValue.refpk ) {
		// 	this.setState({ 
		// 		defaultValue: nextProp.defaultValue, 
		// 		value: nextProp.defaultValue,
		// 		disabled: nextProp.disabled,
		// 		clientParam: nextProp.clientParam
		// 	});
		// }


/*		if (nextProp.clitentParam && (nextProp.clientParam.parentid != this.state.clientParam.parentid)) {
			this.setState({
				defaultValue: nextProp.defaultValue,
				value: nextProp.defaultValue,
				disabled: nextProp.disabled,
				clientParam: nextProp.clientParam
			});
		}*/
		// if (nextProp.disabled != this.state.disabled) {
		// 	this.setState({
		// 		defaultValue: nextProp.defaultValue,
		// 		value: nextProp.defaultValue,
		// 		disabled: nextProp.disabled,
		// 		clientParam: nextProp.clientParam
		// 	});
		// }
		if (nextProp.defaultValue !== this.state.defaultValue ) {
			this.setState({
				value: nextProp.defaultValue,
				defaultValue: nextProp.defaultValue
			});
		}
	}
	


	render() {

		let {defaultValue, name, disabled,isViewMode, value, ...others } = this.props;

		// let curValue = {};

		// value={ this.state.value }


		return ((isViewMode ?  <span style={{ lineHeight: 2.1 }} value={this.state.value} > { defaultValue }</span> :
			<Refer     { ...others } value={ value }
			disabled={!!disabled}
			onChange={ this.handleChange } /> ))
			
	}

}
