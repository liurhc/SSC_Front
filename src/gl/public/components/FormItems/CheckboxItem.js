import React, { Component } from 'react';
import {high,base,ajax } from 'nc-lightapp-front';
const { NCCheckbox} = base;



export default class CheckboxItem  extends Component {


	state = {
		keyName: this.props.name,
		boxs: this.props.boxs(),
		disabled:false,
	}

	handleChange = (value, index) => {
		let { boxs }= this.state
		boxs[index]['checked'] = value;
		this.setState({
			boxs
		},()=>{
			this.props.onChange(this.state.boxs);
		});
	}
	// componentDidMount(){
	// 	if(this.props.disabled){
	// 		this.setState({
	// 			disabled:this.props.disabled
	// 		})
	// 	}
	// }
	componentWillReceiveProps(nextProp) {
		if(nextProp.disabled!=this.state.disabled){	
			this.setState({
				disabled:nextProp.disabled
			})
		}
	}

	render() {
		let { boxs, keyName ,disabled}  = this.state;
		return <div>
			{boxs.map((box, i) => <NCCheckbox colors="primary" className={'checkStyle'}
				checked={box.checked}
				disabled={disabled}
				onChange={(e) => { this.handleChange(e, i) }}
				key={i} name={keyName} >
				{box.label}
			</NCCheckbox>
			)}	
		</div>
	}
}
