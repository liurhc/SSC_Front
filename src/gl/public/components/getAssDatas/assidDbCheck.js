import React, { Component } from 'react';
import {high,base,ajax,toast} from 'nc-lightapp-front';
const { Refer} = high;
import createScript from '../uapRefer.js';
import ReferLoader from '../../ReferLoader';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm
} = base;
//import '../index.less';
const {  NCFormItem:FormItem } = NCForm;
const format = 'YYYY-MM-DD';
const timeFormat = 'YYYY-MM-DD HH:mm:ss';
import {SelectItem} from '../FormItems';
import {InputItem} from '../../../public/components/FormItems/index'
import {GetChinese, RemoveChinese} from '../../common/stringDeal.js';

export default function drawingAssidData(self,text, record, index,showDisableData){
    let { assData,childAssData } = self.state;
    let defaultValue = [];
    if (assData[index]["checkvaluename"]) {
        assData[index]["checkvaluename"].split(",").map((item, _index) => {
        defaultValue[_index] = { refname: item, refpk: "" };
        });
        assData[index]["pk_Checkvalue"].split(",").map((item, _index) => {
        defaultValue[_index].refpk = item;
        });
    } else {
        defaultValue = [{ refname: "", refpk: "" }];
    }
    if(record.refnodename){
        let referUrl= record.refnodename+'.js'; 
        let config={
            "pk_org": childAssData.pk_org,
            "pk_unit": childAssData.pk_org,/* 业务单元 pk 用来进行 科目交叉校验规则过滤 */
            "pk_defdoclist":record.classid,
            "pk_accountingbook": childAssData.pk_accountingbook,
            "date": childAssData.prepareddate,
            "pk_accasoa": childAssData.pk_accasoa,
            "classid": record.classid,
            "assvo": JSON.stringify(assData),
            "isDataPowerEnable": 'Y',
            "DataPowerOperationCode" : 'fi'
        }
        if(!self.state[record.pk_accassitem]){
            {createScript.call(self,referUrl,record.pk_accassitem)}
            return <div />
        }else{
            if(record.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'){//部门，人员
                return (
                    <ReferLoader
                        fieldid='checkvaluename'
                        tag={record.refnodename}
                        refcode={referUrl}
                        value={defaultValue}
                        isShowUnit={true}
                        unitProps={{
                            refType: 'tree',
                            refName: self.state.json['publiccomponents-000011'],/* 国际化处理： 业务单元*/
                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                            rootNode:{refname:self.state.json['publiccomponents-000011'],refpk:'root'},/* 国际化处理： 业务单元*/
                            placeholder:self.state.json['publiccomponents-000011'],/* 国际化处理： 业务单元*/
                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                            treeConfig:{name:[self.state.json['publiccomponents-000012'], self.state.json['publiccomponents-000013']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                            isMultiSelectedEnabled: false,
                            //unitProps:unitConf,
                            isShowUnit:false
                        }}
                        unitCondition={{
                            pk_financeorg:childAssData.pk_org,
                            'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                        }}
                        isMultiSelectedEnabled={true}
                        isShowDisabledData={showDisableData}
                        unitValueIsNeeded={false}
                        isShowDimission={true}
                        queryCondition={(obj) => {
                            if(obj){
                                if(obj.refType=='grid'){
                                    config.GridRefActionExt='nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                                }else if(obj.refType=='tree'){
                                    config.TreeRefActionExt='nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                                }else if(obj.refType=='gridTree'){
                                    config.GridRefActionExt='nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                                }
                            }
                            if(record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f'){
                                //显示离职人员
                                config.isShowDimission=true;
                            }
                            if(record.classid&&record.classid.length==20){//classid的长度大于20的话过滤条件再加一个pk_defdoclist
                                return config
                            }else{
                                config.busifuncode="all";
                                return config
                            }   
                        }}
                        onChange={(v) => {
                            let { assData } = self.state;
                            let originData = self.findByKey(record.key, assData);
                            let refnameArr = [], refpkArr = [], refcodeArr = [];
                            if (originData) {
                                v.map((arr, index) => {
                                    refnameArr.push(arr.refname);
                                    refpkArr.push(arr.refpk);
                                    refcodeArr.push(arr.refcode);

                                })
                                originData.checkvaluename = (v.length > 0) ? refnameArr.join() : null;
                                originData.pk_Checkvalue = (v.length > 0) ? refpkArr.join() : null;
                                originData.checkvaluecode = (v.length > 0) ? refcodeArr.join() : null;
                            }
                            childAssData.assData = assData;
                            self.setState({
                                assData, childAssData
                            })
                        }}
                        />
                    )
            }else{
                return (
                    <ReferLoader
                    fieldid='checkvaluename'
                    tag={record.refnodename}
                    refcode={referUrl}
                    value={defaultValue}
                    isMultiSelectedEnabled={true}
                    isShowDisabledData={showDisableData}
                    queryCondition={(obj) => {
                        if(obj){
                            if(obj.refType=='grid'){
                                config.GridRefActionExt='nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                            }else if(obj.refType=='tree'){
                                config.TreeRefActionExt='nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                            }else if(obj.refType=='gridTree'){
                                config.GridRefActionExt='nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                            }
                        }
                        return config 
                    }}
                    onChange={(v)=>{
                        let { assData } = self.state;
                        let originData = self.findByKey(record.key, assData);
                        let refnameArr = [], refpkArr = [], refcodeArr = [];
                        if (originData) {
                            v.map((arr, index) => {
                                refnameArr.push(arr.refname);
                                refpkArr.push(arr.refpk);
                                refcodeArr.push(arr.refcode);

                            })
                            originData.checkvaluename = (v.length > 0) ? refnameArr.join() : null;
                            originData.pk_Checkvalue = (v.length > 0) ? refpkArr.join() : null;
                            originData.checkvaluecode = (v.length > 0) ? refcodeArr.join() : null;
                        }
                        childAssData.assData = assData;
                        self.setState({
                            assData, childAssData
                        })
                    }}
                />)

            }  
        }
    }else{//不是参照的话要区分日期、字符、数值
        if(record.classid=='BS000010000100001033'){//日期
            return(
                <DatePicker
                    fieldid='checkvaluename'
                    type="customer"
                    value={defaultValue[0].refname}
                    onChange={(v) => {
                        let { assData } = self.state;
                          let originData = self.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = v?assArr.join():null;
                            originData.pk_Checkvalue = v?assArr.join():null;
                            originData.checkvaluecode = v?assArr.join():null;
                          }
                          childAssData.assData = assData;
                          self.setState({
                            assData, childAssData
                          })
                    }}
                    
                    />
            )
        }else if(record.classid=='BS000010000100001034'){//日期时间
            return(
                <DatePicker
                fieldid='checkvaluename'
                    showTime={true}
                    format={timeFormat}
                    type="customer"
                    value={defaultValue[0].refname}
                    onChange={(v) => {
                        let { assData } = self.state;
                          let originData = self.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = v?assArr.join():null;
                            originData.pk_Checkvalue = v?assArr.join():null;
                            originData.checkvaluecode = v?assArr.join():null;
                          }
                          childAssData.assData = assData;
                          self.setState({
                            assData, childAssData
                          })
                    }}
                    
                    />
            )
        }else if(record.classid=='BS000010000100001031' ||record.classid=='BS000010000100001004'){//数值
            return(
                <NCNumber
                fieldid='checkvaluename'
                    scale={Number(record.digits?record.digits:'0')}
                    value={defaultValue[0].refname}
                    maxlength={Number(record.inputlength)+Number(record.digits?record.digits:'0')}
                    placeholder={self.state.json['publiccomponents-000004']}/* 国际化处理： 请输入数字*/
                    onChange={(v)=>{
                        if(v.indexOf('.')!=-1){
                            if(v&&v.slice(0,v.indexOf('.')).length>Number(record.inputlength)){
                                toast({content:self.state.json['publiccomponents-000148']+record.inputlength,color:'warning'});//输入的数字长度不能超过用户设置的长度
                                v=v.slice(0,record.inputlength);
                            }
                        }else{
                            if(v&&v.length>Number(record.inputlength)){
                                toast({content:self.state.json['publiccomponents-000148']+record.inputlength,color:'warning'});//输入的数字长度不能超过用户设置的长度
                                v=v.slice(0,record.inputlength);
                            }
                        }
                        let { assData } =self.state;
                        let originData = self.findByKey(record.key, assData);
                        if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = v?assArr.join():null;
                            originData.pk_Checkvalue = v?assArr.join():null;
                            originData.checkvaluecode = v?assArr.join():null;
                          }
                          childAssData.assData = assData;
                          self.setState({
                            assData, childAssData
                          })
                    }}
                />
            )
        }else if(record.classid=='BS000010000100001032'){//布尔
            return(
                    <FormItem
                    inline={true}
                    // showMast={false}
                    labelXs={2}
                    labelSm={2}
                    labelMd={2}
                    xs={10}
                    md={10}
                    sm={10}
                    // labelName={item.itemName}
                    isRequire={true}
                    method="change"
                >
                    <SelectItem 
                        name={record.checktypecode}
                        fieldid='checkvaluename'
                        defaultValue={defaultValue[0].refname} 
                        items = {
                            () => {
                                return ([{
                                    label: self.state.json['publiccomponents-000005'],/* 国际化处理： 是*/
                                    value: 'Y'
                                }, {
                                    label: self.state.json['publiccomponents-000006'],/* 国际化处理： 否*/
                                    value: 'N'
                                }]) 
                            }
                        }
                        onChange={(v)=>{
                            let { assData } =self.state;
                            let originData = self.findByKey(record.key, assData);
                            if (originData) {
                                let assArr = [];
                                assArr.push(v);
                                originData.checkvaluename = v?assArr.join():null;
                                originData.pk_Checkvalue = v?assArr.join():null;
                                originData.checkvaluecode = v?assArr.join():null;
                              }
                              childAssData.assData = assData;
                              self.setState({
                                assData, childAssData
                              })
                        }}
                    />
                </FormItem>
            )
        }else{//字符
            return(
                // <FormControl
				// 					value={defaultValue?defaultValue.refname:defaultValue.refname}
				// 					onChange={(v) => {
                //                         if(v&&v.length>0){
                //                             if(GetChinese(v).length>0&&2*(GetChinese(v).length)>Number(record.inputlength)){
                //                                 v=v.slice(0,Number(record.inputlength/2));
                //                             }else if(GetChinese(v).length>0&&2*(GetChinese(v).length)+RemoveChinese(v).length>Number(record.inputlength)){
                //                                 toast({content:self.state.json['publiccomponents-000148']+record.inputlength,color:'warning'});//输入的数字长度不能超过用户设置的长度
                //                                 v= RemoveChinese(v);
                //                             }else if(RemoveChinese(v).length>Number(record.inputlength)){
                //                                 toast({content:self.state.json['publiccomponents-000148']+record.inputlength,color:'warning'});//输入的数字长度不能超过用户设置的长度
                //                                 v=v.slice(0,Number(record.inputlength));
                //                             }
                //                         }
				// 						let { assData } =self.state;
                //                         let originData = self.findByKey(record.key, assData);
                //                         if (originData) {
                //                             originData.checkvaluename = v;
                //                             originData.pk_Checkvalue =v;
                //                             originData.checkvaluecode=v;
                //                             }
                //                         childAssData.assData=assData; 
                //                         self.setState({
                //                         assData,childAssData
                //                         })
				// 					}}
				// 				/>
                <FormControl
                    fieldid='checkvaluename'
                    defaultValue={defaultValue.refname}
                    maxlength={record && record.inputlength || 10}
                    onChange={(v)=>{
                        let { assData } =self.state;
                        let originData = self.findByKey(record.key, assData);
                        if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = v?assArr.join():null;
                            originData.pk_Checkvalue = v?assArr.join():null;
                            originData.checkvaluecode = v?assArr.join():null;
                          }
                          childAssData.assData = assData;
                          self.setState({
                            assData, childAssData
                          })
                    }}
                />
            )
        }
        
    }
} 
