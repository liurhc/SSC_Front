import React, { Component } from 'react';
import { base, ajax, getMultiLang } from 'nc-lightapp-front';
const { NCTable: Table, NCCheckbox: Checkbox,NCDiv } = base;
import assidDbCheck from './assidDbCheck.js';
import './index.less';
const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
        type: "checkbox",
        param: "key"
    }
};
// export default class AssidModal extends Component {
class AssidModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            showModal: false,
            isShowUnit: false,//是否显示业务单元
            assData: this.props.pretentAssData.assData || [],//辅助核算信息
            childAssData: this.props.pretentAssData.childAssData||[],//接受父组件传过来的参数
            loadData: [],//查询模板加载数据
            listItem: {},//模板数据对应值
            checkedAll: this.props.pretentAssData.checkedAll || [],//默认全选中
            SelectedAssData: [],//选中的数据
            checkedArray: this.props.pretentAssData.checkedArray || [],
            assidData: {}, //key=assid,value=assData
            assitemData: {} //key=pk_accasoa+prepareddate,value=assitems
        };
    }
    componentWillMount() {}
    //确定 过滤勾选的数据
    // confirm=()=>{
    //     let self=this;
    //     let { SelectedAssData,checkedArray,listItem,assData,childAssData } =self.state;
    //     SelectedAssData=[];//清空
    //     let url = '/nccloud/gl/voucher/queryAssId.do';
    //     for(var i = 0; i < checkedArray.length; i++){
    //         if(checkedArray[i]==true){
    //             SelectedAssData.push(assData[i]);
    //         }
    //     }
    //     let sendData={};
    //     let assid='',assname='';
    //     if(SelectedAssData.length>0){
    //         let parm={"ass":assData,"pk_accountingbook":childAssData.pk_accountingbook};            
    //         ajax({
    //             url:url,
    //             data:parm,
    //             async:false,
    //             success:function(response){

    //                 let { data }=response;
    //                 if(data){
    //                     assid = data.assid;
    //                     assname=data.assname?data.assname:'';
    //                     sendData={
    //                         data:SelectedAssData,
    //                         assid:assid,
    //                         assname:assname
    //                     }            
    //                     self.props.onConfirm(sendData);
    //                 }
    //             }
    //         })
    //     } else{
    //         sendData={
    //             data:SelectedAssData,
    //             assid:assid,
    //             assname:assname
    //         }            
    //         self.props.onConfirm(sendData);
    //     }    
    // }

    //全选
    onAllCheckChange = () => {
        let { pretentAssData } = this.props;
        let { checkedAll, checkedArray } = pretentAssData.$_this.state;
        for (var i = 0; i < checkedArray.length; i++) {
            checkedArray[i] = !checkedAll;
        }
        checkedAll = !checkedAll;
        $_this.setState({
            checkedAll, checkedArray
        })

    };
    //单选
    onCheckboxChange = (text, record, index) => {
        let { pretentAssData } = this.props;
        let { checkedAll, checkedArray } = pretentAssData.$_this.state;
        let allFlag = false;
        checkedArray[index] = !checkedArray[index];
        for (var i = 0; i < checkedArray.length; i++) {
            if (!checkedArray[i]) {
                allFlag = false;
                break;
            } else {
                allFlag = true;
            }
        }
        checkedAll = allFlag;
        $_this.setState({
            checkedAll, checkedArray
        })
    };
    //添加复选框
    renderColumnsMultiSelect(columns) {
        let { pretentAssData } = this.props;
        let { checkedAll, checkedArray } = pretentAssData.$_this.state;
        const { multiSelect } = this.props;
        let indeterminate_bool = false;
        if (multiSelect && multiSelect.type === "checkbox") {
            let i = checkedArray.length;
            while (i--) {
                if (checkedArray[i]) {
                    indeterminate_bool = true;
                    break;
                }
            }
            let defaultColumns = [
                {
                    title: <div fieldid="firstcol">{(
                        <Checkbox
                            className="table-checkbox"
                            checked={checkedAll}
                            indeterminate={indeterminate_bool && !checkedAll}
                            onChange={this.onAllCheckChange}
                        />
                    )}</div>,
                    key: "checkbox",
                    attrcode: 'checkbox',
                    dataIndex: "checkbox",
                    width: "60px",
                    render: (text, record, index) => {
                        return (<div fieldid="firstcol">
                            <Checkbox
                                className="table-checkbox"
                                checked={checkedArray[index]}
                                onChange={this.onCheckboxChange.bind(this, text, record, index)}
                            />
                            </div>
                        );
                    }
                }
            ];
            columns = defaultColumns.concat(columns);
        }
        return columns;
    }

    render() {
        let{pretentAssData}=this.props;
        let {assData,$_this} =pretentAssData;
        let columnsldad=[];
        let { json } = $_this.state
        let showDisableData = true;
        if (this.props.hasOwnProperty('showDisableData')) {
            if (this.props.showDisableData != 'show') {
                showDisableData = false;
            }
        }
        let columns10 = [
            // {
            //   title: json['publiccomponents-000001'],/* 国际化处理： 核算类型编码*/
            //   dataIndex: "checktypecode",
            //   key: "checktypecode",
            //   width: '100',
            //   render: (text, record, index) => {
            // 	return <span>{record.checktypecode}</span>;
            //     }
            // },
            {
            title: (<div fieldid="checktypename">{ json['publiccomponents-000002']}</div>),/* 国际化处理： 核算类型名称*/
                dataIndex: "checktypename",
                key: "checktypename",
                width: "200",
                render: (text, record, index) => {
                    return <div fieldid="checktypename">{record.checktypename}</div>;
                }
            },
            {
                title: (<div fieldid="checkvaluename">{ json['publiccomponents-000003']}</div>),/* 国际化处理： 核算内容*/
                dataIndex: "checkvaluename",
                key: "checkvaluename",
                width: "200",
                render: (text, record, index) => {
                    return (assidDbCheck($_this, text, record, index, showDisableData, json))
                }
            }
        ];
        if (pretentAssData.checkboxShow) {//复选框显示true
            columnsldad = $_this.renderColumnsMultiSelect(columns10);
        } else {
            columnsldad = columns10;
        }

        const emptyFunc = () => <span>{json['publiccomponents-000007']}！</span>/* 国际化处理： 这里没有数据*/
        return (
            <NCDiv fieldid="assData" areaCode={NCDiv.config.TableCom} className="getAssDatas">
                <Table
                    isDrag={true}
                    columns={columnsldad}
                    data={assData}
                    emptyText={emptyFunc}
                    scroll={{
                        x: true,
                        y: 124
                    }}
                />
            </NCDiv>
        )
    }
}
AssidModal.defaultProps = defaultProps12;
export default function (props = {}) {
    var conf = {
    };

    return <AssidModal {...Object.assign(conf, props)} />
}
