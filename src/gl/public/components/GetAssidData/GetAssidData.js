import React, { Component } from 'react';
import {high,base,ajax} from 'nc-lightapp-front';
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../FormItems';
const { Refer} = high;
import { toast } from '../utils.js';
import createScript from '../uapRefer.js';

const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm
} = base;
import './getAssidData.less';
export default function GetAssidData(self,assData){
    let assLists=getDefalutData();
    if(!assData){
        assData=[]
       assLists=getDefalutData();
    }else{
        assLists = assData.map(function (item, index) {
            let defaultValue={refname: item.checkvaluename, refpk: item.pk_Checkvalue};
            if(item.refnodename){
                let referUrl= item.refnodename+'.js';  
                if(!self.state[item.pk_accassitem]){
                    {createScript.call(self,referUrl,item.pk_accassitem)}
                    return <div />
                }else{
                    return (
                        <Row className="rowStyle">
                            <Col md={5} sm={5}>
                                <div>{item.checktypename}</div>
                            </Col>
                            <Col md={5} sm={5}>
                                {self.state[item.pk_accassitem]?(self.state[item.pk_accassitem])(
                                    {	
                                        value:defaultValue,
                                        onChange : (v) => {
                                            let { assData } =self.state;
                                                let originData = self.findByKey(item.key, assData);
                                                if (originData) {
                                                originData.checkvaluename = v.refname;
                                                originData.pk_Checkvalue = v.refpk;
                                                originData.checkvaluecode=v.refcode;
                                                }
                                                self.setState({
                                                assData
                                                })
                                        },
                                        queryCondition:() => {
                                                return {
                                                    // "pk_org": self.state.pk_unit
                                                }
                                            },
                                    }
                                ):<div/>}
                            </Col>
                        </Row>
                    )
                }
            }else{
                return (
                    <Row className="rowStyle">
                    <Col md={5} sm={5}>
                        <div>{item.checktypename}</div>
                    </Col>
                    <Col md={5} sm={5}>
                        <FormControl
                            value={defaultValue.refname}
                            onChange={(v)=>{
                                let { assData } =self.state;
                                let originData = self.findByKey(item.key, assData);
                                if (originData) {
                                originData.checkvaluename = v;
                                originData.pk_Checkvalue =v;
                                }
                                self.setState({
                                assData
                                })
                            }}
                        />
                    </Col>
                    </Row>
                )
            }
        })
    }
    return (
        <div id='getAssidData'>
            {/* <Row>
                <Col md={2} sm={2}>
                    <span>辅助核算</span>
                </Col>
                <Col md={10} sm={10}><div className="line"></div></Col>
            </Row> */}
            <Row className="assList">
                <Col md={12} sm={12}>
                    <Row className='rowStyle'>
                        <Col md={5} sm={5}>
                            {/* <div>{this.state.json['publiccomponents-000087']}</div> */}
                            {/* 国际化处理： 核算类型*/}
                        </Col>
                        <Col md={5} sm={5}>
                            {/* <div>{this.state.json['publiccomponents-000003']}</div> */}
                            {/* 国际化处理： 核算内容*/}
                        </Col>
                    </Row>
                    {assLists}
                </Col>
            </Row>
        </div>
    );
}
function getDefalutData(){
    return (
        <Row className="aaa">
            <Col md={5} sm={5}>
                <div>
                    <FormControl
                    value=''
                    onChange={(v)=>{
                    }}
                    />
                </div>
            </Col>
            <Col md={5} sm={5}>
                <FormControl
                    value=''
                    onChange={(v)=>{
                    }}
                />
            </Col>
        </Row>
    )
}
