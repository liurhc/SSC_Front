import React, { Component } from 'react';
import {createPage,high,base,ajax,formDownload,toast } from 'nc-lightapp-front';
export default function reportPrint(mergeInfo,dataArr,textAlginArr){
    let url = '/nccloud/gl/pub/directprintauthority.do';
    let data;
    ajax({
        loading: true,
        url,
        data,
        success: function (res) {
            const { error, success } = res;
            if (success) {
                let data = {
                    mergeCells: mergeInfo,
                    data: dataArr,
                    align: textAlginArr
                }
                formDownload({
                    params: { param: JSON.stringify(data) },
                    url: '/nccloud/gl/accountrep/directprint.do',
                    enctype: 1
                })
            }
        },
        error: function (res) {
            toast({ content: res.message, color: 'warning' });
        }
    });
}
