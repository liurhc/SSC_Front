import { ajax, toast} from 'nc-lightapp-front';
//@Author: lijun 

export function voucherRelatedApp(params) {
  let url = '/nccloud/gl/voucher/voucherRelatedApp.do';
  let data={
    sence:params,
    billtype:'C0'
  }
  let appcode='';
    ajax({
			url,
      data: data,
      async:false,
			success: function(response) {
        if (response.success) {
          appcode=response.data
        }
			}
    });
    return appcode
}




export function  getNowFormatDate() {
  var date = new Date();
  var seperator1 = "-";
  var seperator2 = ":";
  var month = date.getMonth() + 1;
  var strDate = date.getDate();
  if (month >= 1 && month <= 9) {
      month = "0" + month;
  }
  if (strDate >= 0 && strDate <= 9) {
      strDate = "0" + strDate;
  }
  var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
          // + " " + date.getHours() + seperator2 + date.getMinutes()
          // + seperator2 + date.getSeconds();
  return currentdate;
}

export function isObj(param) {
return Object.prototype.toString.call(param).slice(8, -1) === 'Object';
}

//是否是string类型
export function isString(str) {
  return typeof str == 'string' && str.constructor == String;
}
//是否是空对象
export function isEmptyObject(obj) {
  for (var key in obj) {
    return false; //返回false，不为空对象
  }
  return true; //返回true，为空对象
}


/* 检测类型是否为数组 */
export function isArray(param) {
return Object.prototype.toString.call(param).slice(8, -1) === 'Array';
}
/* 人民币大小写切换 */
export function convertCurrency(json,money) {
  //汉字的数字
  var cnNums = new Array(json['publiccomponents-000094'], json['publiccomponents-000095'], json['publiccomponents-000096'], json['publiccomponents-000097'], json['publiccomponents-000098'], json['publiccomponents-000099'], json['publiccomponents-000100'], json['publiccomponents-000101'], json['publiccomponents-000102'], json['publiccomponents-000103']);/* 国际化处理： 零,壹,贰,叁,肆,伍,陆,柒,捌,玖*/
  //基本单位
  var cnIntRadice = new Array('', json['publiccomponents-000104'], json['publiccomponents-000105'], json['publiccomponents-000106']);/* 国际化处理： 拾,佰,仟*/
  //对应整数部分扩展单位
  var cnIntUnits = new Array('', json['publiccomponents-000107'], json['publiccomponents-000108'], json['publiccomponents-000109']);/* 国际化处理： 万,亿,兆*/
  //对应小数部分单位
  var cnDecUnits = new Array(json['publiccomponents-000110'], json['publiccomponents-000111'], json['publiccomponents-000112'], json['publiccomponents-000113']);/* 国际化处理： 角,分,毫,厘*/
  //整数金额时后面跟的字符
  var cnInteger = json['publiccomponents-000114'];/* 国际化处理： 整*/
  //整型完以后的单位
  var cnIntLast = json['publiccomponents-000115'];/* 国际化处理： 元*/
  //最大处理的数字
  var maxNum = 999999999999999.9999;
  //金额整数部分
  var integerNum;
  //金额小数部分
  var decimalNum;
  //输出的中文金额字符串
  var chineseStr = '';
  //分离金额后用的数组，预定义
  var parts;
  if (money == '') { return ''; }
  money = parseFloat(money);
  if (money >= maxNum) {
    //超出最大处理数字
    return '';
  }
  if (money == 0) {
    chineseStr = cnNums[0] + cnIntLast + cnInteger;
    return chineseStr;
  }
  //转换为字符串
  money = money.toString();
  if (money.indexOf('.') == -1) {
    integerNum = money;
    decimalNum = '';
  } else {
    parts = money.split('.');
    integerNum = parts[0];
    decimalNum = parts[1].substr(0, 4);
  }
  //获取整型部分转换
  if (parseInt(integerNum, 10) > 0) {
    var zeroCount = 0;
    var IntLen = integerNum.length;
    for (var i = 0; i < IntLen; i++) {
      var n = integerNum.substr(i, 1);
      var p = IntLen - i - 1;
      var q = p / 4;
      var m = p % 4;
      if (n == '0') {
        zeroCount++;
      } else {
        if (zeroCount > 0) {
          chineseStr += cnNums[0];
        }
        //归零
        zeroCount = 0;
        chineseStr += cnNums[parseInt(n)] + cnIntRadice[m];
      }
      if (m == 0 && zeroCount < 4) {
        chineseStr += cnIntUnits[q];
      }
    }
    chineseStr += cnIntLast;
  }
  //小数部分
  if (decimalNum != '') {
    var decLen = decimalNum.length;
    for (var i = 0; i < decLen; i++) {
      var n = decimalNum.substr(i, 1);
      if (n != '0') {
        chineseStr += cnNums[Number(n)] + cnDecUnits[i];
      }
    }
  }
  if (chineseStr == '') {
    chineseStr += cnNums[0] + cnIntLast + cnInteger;
  } else if (decimalNum == '') {
    chineseStr += cnInteger;
  }
  return chineseStr;
}
