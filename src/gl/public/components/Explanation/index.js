//@Author: lijun
import React, { Component } from 'react';
import { high, base, ajax,getMultiLang } from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCModal: Modal,
	NCForm: Form,
	NCAutoComplete: AutoComplete
} = base;
const { Refer } = high;
//import { toast } from '../../../../utils/utils.js';
//import Label from 'bee-label';
import { CheckboxItem, RadioItem, TextAreaItem, ReferItem, InputItem } from '../../../public/components/FormItems';
import './index.less';

const { NCFormItem: FormItem } = Form;
const format = 'YYYY-MM-DD';
//const dateInputPlaceholder = this.state.json['publiccomponents-000000'];/* 国际化处理： 选择日期*/
export default class ExplanationModal extends Component {
	static defaultProps = {
		// confirmText: this.state.json['publiccomponents-000009'],/* 国际化处理： 确定*/
		// cancelText: this.state.json['publiccomponents-000010'],/* 国际化处理： 取消*/
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		multiSelect: {
			type: 'checkbox',
			param: 'key'
		}
	};

	constructor(props) {
		super(props);
		this.state = {
			json:{},
			checkFormNow: false, //控制表单form回调
			startdate: {
				//制单日期
			},
			accountData: {
				rows: []
			},
			count: 4,
			checkedAll: false,
			checkedArray: [],
			contentMessage: {
				value: '',
				index: ''
			},
			pk_unit: '',
			show: false,
			explantion: ''
		};
	}
	componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json
               // columns_reportSum:this.columns_reportSum,
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
	componentWillReceiveProps(nextProps) {
		let { pk_unit } = this.state;
		if (pk_unit != nextProps.pk_unit) {
			this.setState(
				{
					pk_unit: nextProps.pk_unit
				},
				() => {
					if (this.state.pk_unit) {
						let dataCount = {
							pid: '',
							keyword: '',
							queryCondition: {
								pk_org: this.state.pk_unit
							},
							pageInfo: {
								pageSize: 10,
								pageIndex: -1
							}
						};
						ajax({
							url: '/nccloud/fipub/ref/SummaryRef.do',
							data: dataCount,
							success: (res) => {
								let { rows } = res.data;
								let { accountData, checkedArray } = this.state;
								let { success, data } = res;
								if (success) {
									// dataRows=data;
									rows.map((e, i) => {
										checkedArray.push(false);
									});
									accountData.rows = rows;
									this.setState(
										{
											accountData,
											checkedArray
										},
										() => {
										}
									);
								}
							}
						});
					}
				}
			);
		}
	}

	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		//let listData = self.state.data.concat();
		let selIds = [];
		// let id = self.props.multiSelect.param;
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}

		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		});
	};
	onCheckboxChange = (text, record, index) => {
		let self = this;
		//let allFlag = false;
		let { contentMessage } = this.state;
		contentMessage.value = record.refname;
		//contentMessage.index=record.key;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			// if (!checkedArray[i]) {
			// allFlag = false;
			// break;
			// } else {
			// allFlag = true;
			// }
			if (i != index) {
				checkedArray[i] = false;
			}
		}
		self.setState({
			//checkedAll: allFlag,
			checkedArray: checkedArray,
			contentMessage
			// selIds: selIds
		});
		// self.props.onSelIds(selIds);
	};

	renderColumnsMultiSelect(columns) {
		const { data, checkedArray } = this.state;
		const { multiSelect } = this.props;

		let select_column = {};
		let indeterminate_bool = false;
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					// <Checkbox
					//   className="table-checkbox"
					//   checked={this.state.checkedAll}
					//   indeterminate={indeterminate_bool&&!this.state.checkedAll}
					//   onChange={this.onAllCheckChange}
					// />
					title: '',
					key: 'checkbox',
					attrcode: 'checkbox',
					dataIndex: 'checkbox',
					width: '60px',
					render: (text, record, index) => {
						return (
							<Checkbox
								className="table-checkbox"
								checked={this.state.checkedArray[index]}
								onChange={this.onCheckboxChange.bind(this, text, record, index)}
							/>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}

	checkForm = (flag, obj) => {
		let edit = obj;
		this.props.onSubmit(edit, this.props.opre);
		this.setState({
			checkFormNow: false,
			open1: true,
			open2: true,
			open3: true
		});
	};

	//选定常用摘要,传至父组件
	modalMessage = () => {
		let self = this;
		let { explantion } = self.state;
		let checkedArray = [];
		let contentName = self.state.contentMessage;
		explantion = contentName.value;
		//清空true 修改为false;每次选定摘要初始化
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = false;
		}
		self.setState(
			{
				explantion,
				show: false,
				checkedAll: false,
				checkedArray: checkedArray
			},
			() => {
				self.props.value(explantion);
			}
		);
	};

	//日期控制
	handleInputChange = (obj, type, val) => {
		let { startdate } = this.state;
		if (type === 'begindate' || type === 'enddate') {
			//	val = moment(val);
		}
		startdate.value = val;
	};

	handleGTypeChange = () => {};
	//增加一行
	// addClick =() =>{
	// 	const {count,accountData}=this.state;
	// 	const newData=
	// 		{ a:`${count*10}`, b: "部门级", c: "财务", d: "操作",key:count };
	// 		this.setState({
	// 			accountData:{rows:[...accountData.rows, newData]} ,
	// 			count: count + 1
	// 		});
	// }

	loadRows = () => {
		let { checkFormNow, banks, accountData } = this.state;
		let accountRows = accountData.rows;
		let self = this;
		let accountColumns = [
			{ title: this.state.json['publiccomponents-000012'], dataIndex: 'refcode', key: 'a', width: 100 },/* 国际化处理： 编码*/
			//{ title: "组织类型", dataIndex: "b", key: "b", width: 100 },
			{ title: this.state.json['publiccomponents-000013'], dataIndex: 'refname', key: 'c', width: 200 }/* 国际化处理： 名称*/
			// {
			// 	title: "操作",
			// 	dataIndex: "d",
			// 	key: "d",
			// 	render(text, record, index) {
			// 		return (
			// 			<div className="explanationAction">
			// 			<a
			// 				href="#"
			// 				onClick={() => {
			// 					alert('这是第'+index+'列，内容为:'+text);
			// 				}}
			// 			>
			// 				选择
			// 			</a>
			// 			<a
			// 			href="#"
			// 			onClick={() => {
			// 				alert('这是第'+index+'列，内容为:'+text);
			// 			}}
			// 		>
			// 			修改
			// 		</a>
			// 		<a
			// 		href="#"
			// 		onClick={() => {
			// 			alert('这是第'+index+'列，内容为:'+text);
			// 		}}
			// 	>
			// 		删除
			// 	</a></div>
			// 		);
			// 	}
			// }
		];
		let columns = this.renderColumnsMultiSelect(accountColumns);
		return <Table bordered columns={columns} data={accountRows} />;
	};

	explanationClick = () => {
		this.setState({
			show: true
		});
	};

	render() {
		let { title, content, confirmText, cancelText, closeButton, icon, isButtonWhite, isButtonShow } = this.props;
		let { show, explantion } = this.state;
		return [
			<div className="auto-complete-wrapper">
				<AutoComplete
					value={explantion}
					//disabled={record[item.attrcode].editable ? !record[item.attrcode].editable : false}
					//options={options}
					//	placeholder={placeholder}
					onValueChange={(value) => {
						explantion = value;
						this.setState(
							{
								explantion
							},
							() => {
								this.props.value(explantion);
							}
						);
					}}
				/>
				<i className="iconfont icon-canzhao" onClick={this.explanationClick.bind(this)} />
			</div>,
			<Modal
				className={isButtonShow ? 'explanation-modal' : 'msg-modal footer-hidden'}
				show={show}
				onHide={this.close}
				animation={false}
				backdrop="static"
			>
				<Modal.Header closeButton={closeButton}>
					<div className="title">{title || this.state.json['publiccomponents-000031']}</div>{/* 国际化处理： 选择凭证摘要*/}
					<i
						className="close-icon iconfont icon-guanbi"
						onClick={() => {
							this.setState({ show: false }, () => this.props.onCancel && this.props.onCancel(true));
						}}
					/>
				</Modal.Header>
				<Modal.Body>{this.loadRows()}</Modal.Body>
				{isButtonShow && (
					<Modal.Footer>
						<Button
							className={` ${isButtonWhite ? 'button-primary' : ''}`}
							onClick={() => this.modalMessage()}
						>
							{/* {confirmText} */}
							{this.state.json['publiccomponents-000009']}
						</Button>
						<Button
							onClick={() => {
								this.setState({ show: false });
							}}
						>
							{/* {cancelText} */}
							{this.state.json['publiccomponents-000010']}
						</Button>
					</Modal.Footer>
				)}
			</Modal>
		];
	}
}
