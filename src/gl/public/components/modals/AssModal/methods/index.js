import React from 'react';
import { base, toast } from 'nc-lightapp-front';
import ReferLoader from '../../../../../../fipub/public/components/ReferLoader';
const { NCDatePicker: DatePicker, NCForm, NCSelect, NCNumber } = base;
const { NCFormItem: FormItem } = NCForm;
const format = 'YYYY-MM-DD';
const timeFormat = 'YYYY-MM-DD HH:mm:ss';
const NCOption = NCSelect.NCOption;
import { InputItem, SelectItem } from '../../../FormItems'

export default function drawingAss(self, text, record, index, showDisableData) {
    let { assData, childAssData } = self.state;
    let defaultValue = { refname: record.checkvaluename, refpk: record.pk_Checkvalue };
    let tempVal = defaultValue.refname == '~' ? '' : defaultValue.refname //日期时间、日期有时返回'~'
    if (record.refnodename) {
        let referUrl = record.refnodename + '.js';
        let config = {
            "pk_org": childAssData.pk_org,
            "pk_unit": childAssData.pk_org,/* 业务单元 pk 用来进行 科目交叉校验规则过滤 */
            "pk_defdoclist": record.classid,
            "date": childAssData.prepareddate,
            "classid": record.classid,
            "assvo": JSON.stringify(assData),
            "isDataPowerEnable": 'Y',
            "DataPowerOperationCode": 'fi'
        }
        if (record.classid == 'b26fa3cb-4087-4027-a3b6-c83ab2a086a9' || record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
            return (
                <ReferLoader
                    fieldid='checkvaluename'
                    tag={record.refnodename}
                    refcode={referUrl}
                    value={defaultValue}
                    isShowUnit={true}
                    unitProps={{
                        refType: 'tree',
                        refName: self.state.json['publiccomponents-000011'],/* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode: { refname: self.state.json['publiccomponents-000011'], refpk: 'root' },/* 国际化处理： 业务单元*/
                        placeholder: self.state.json['publiccomponents-000011'],/* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig: { name: [self.state.json['publiccomponents-000012'], self.state.json['publiccomponents-000013']], code: ['refcode', 'refname'] },/* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        isShowUnit: false
                    }}
                    unitCondition={{
                        pk_financeorg: childAssData.pk_org,
                        'TreeRefActionExt': 'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    }}
                    isMultiSelectedEnabled={false}
                    isShowDisabledData={showDisableData}
                    unitValueIsNeeded={false}
                    isShowDimission={true}
                    queryCondition={(obj) => {
                        if (obj) {
                            if (obj.refType == 'grid') {
                                config.GridRefActionExt = 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                            } else if (obj.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                            } else if (obj.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                            }
                        }
                        config.isShowDimission=true;
                        if (record.classid && record.classid.length == 20) {//classid的长度大于20的话过滤条件再加一个pk_defdoclist
                            return config
                        } else {
                            config.busifuncode = "all";
                            return config
                        }
                    }}
                    onChange={(v) => {
                        onChange(self, record, v);
                    }}
                />
            )
        } else {
            return (
                <ReferLoader
                    fieldid='checkvaluename'
                    tag={record.refnodename}
                    refcode={referUrl}
                    value={defaultValue}
                    isMultiSelectedEnabled={false}
                    isShowDisabledData={showDisableData}
                    queryCondition={(obj) => {
                        if (obj) {
                            if (obj.refType == 'grid') {
                                config.GridRefActionExt = 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                            } else if (obj.refType == 'tree') {
                                config.TreeRefActionExt = 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                            } else if (obj.refType == 'gridTree') {
                                config.GridRefActionExt = 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
                            }
                        }
                        return config
                    }}
                    onChange={(v) => {
                        onChange(self, record, v);
                    }}
                />)

        }
    } else {//不是参照的话要区分日期、字符、数值
        if (record.classid == 'BS000010000100001033') {//日期
            return (
                <DatePicker
                    fieldid='checkvaluename'
                    format={format}
                    type="customer"
                    isRequire={true}
                    value={tempVal}
                    onChange={(v) => {
                        onChange(self, record, v);
                    }}

                />
            )
        } else if (record.classid == 'BS000010000100001034') {//日期时间
            return (
                <DatePicker
                    fieldid='checkvaluename'
                    showTime={true}
                    format={timeFormat}
                    type="customer"
                    isRequire={true}
                    value={tempVal}
                    onChange={(v) => {
                        onChange(self, record, v);
                    }}

                />
            )
        } else if (record.classid == 'BS000010000100001031' || record.classid == 'BS000010000100001004') {//数值
            return (
                <NCNumber
                    fieldid='checkvaluename'
                    scale={Number(record.digits ? record.digits : '0')}
                    value={defaultValue.refname}
                    maxlength={Number(record.inputlength) + Number(record.digits ? record.digits : '0')}
                    placeholder={self.state.json['publiccomponents-000004']}/* 国际化处理： 请输入数字*/
                    onChange={(v) => {
                        if (v.indexOf('.') != -1) {
                            if (v && v.slice(0, v.indexOf('.')).length > Number(record.inputlength)) {
                                toast({ content: self.state.json['publiccomponents-000148'] + record.inputlength, color: 'warning' });//输入的数字长度不能超过用户设置的长度
                                v = v.slice(0, record.inputlength);
                            }
                        } else {
                            if (v && v.length > Number(record.inputlength)) {
                                toast({ content: self.state.json['publiccomponents-000148'] + record.inputlength, color: 'warning' });//输入的数字长度不能超过用户设置的长度
                                v = v.slice(0, record.inputlength);
                            }
                        }
                        onChange(self, record, v);
                    }}
                />
            )
        } else if (record.classid == 'BS000010000100001032') {//布尔
            return (
                <FormItem
                    inline={true}
                    labelXs={2}
                    labelSm={2}
                    labelMd={2}
                    xs={10}
                    md={10}
                    sm={10}
                    isRequire={true}
                    method="change"
                >
                    <SelectItem 
                        name={record.checktypecode}
                        fieldid='checkvaluename'
                        defaultValue={defaultValue.refname} 
                        items = {
                            () => {
                                return ([{
                                    label: self.state.json['publiccomponents-000005'],/* 国际化处理： 是*/
                                    value: 'Y'
                                }, {
                                    label: self.state.json['publiccomponents-000006'],/* 国际化处理： 否*/
                                    value: 'N'
                                }]) 
                            }
                        }
                        onChange={(v) => {
                            onChange(self, record, v);
                        }}
                        
                    />
                 </FormItem>
            )
        } else {//字符
            return (
                <InputItem
                    fieldid='checkvaluename'
                    defaultValue={defaultValue.refname}
                    maxlength={record && record.inputlength || 10}
                    onChange={(v) => {
                        onChange(self, record, v);
                    }}
                />
            )
        }

    }
}

/**
 * 
 * @param {*} self 
 * @param {*} record 
 * @param {*} newV 
 */
function onChange(self, record, newV) {
    if(typeof newV !== 'object'){
        newV = {refname:newV, refpk:newV, refcode:newV};
    }
    let { assData, childAssData } = self.state;
    let originData = self.findByKey(record.key, assData);
    if (originData) {
        originData.checkvaluename = newV.refname;
        originData.pk_Checkvalue = newV.refpk;
        originData.checkvaluecode = newV.refcode;
    }
    childAssData.assData = assData;
    self.setState({
        assData, childAssData
    })

}
