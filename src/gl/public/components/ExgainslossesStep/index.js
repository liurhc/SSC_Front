//汇兑损益结转步骤条

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,getMultiLang } from 'nc-lightapp-front';
const { NCStep } = base;
const NCSteps = NCStep.NCSteps;

export default class ExgainslossesStep extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
        }
    }
	componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json
               // columns_reportSum:this.columns_reportSum,
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
    render() {
        return <div className="exch-step-container" style={{paddingTop: '5px',paddingBottom:"5px"}}>
            <NCSteps current={this.props.step}>
                <NCStep title={this.state.json['publiccomponents-000025']} />{/* 国际化处理： 维护汇率清单*/}
                <NCStep title={this.state.json['publiccomponents-000026']} />{/* 国际化处理： 损益计算与结果*/}
                <NCStep title={this.state.json['publiccomponents-000027']} />{/* 国际化处理： 生成凭证*/}
            </NCSteps>

        </div>;
    }
}
ExgainslossesStep.defaultProps = {
    step: 0
}
