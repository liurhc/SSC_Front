import React, { Component } from 'react';
import { base,getMultiLang } from 'nc-lightapp-front';
// const { NCModal } = base
const { NCButton: Button, NCRadio: Radio,
    NCRow: Row, NCCol: Col, NCLabel: Label, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, NCModal: Modal, NCTable: Table,NCIcon,NCForm
} = base;

import './index.less'

const defaultProps = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
};
// const emptyFunc = () => <span>{this.state.json['publiccomponents-000007']}！</span>/* 国际化处理： 这里没有数据*/
export default class CopyCheckModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            checkedData: [],
            checkedAll:false,
            checkedArray: [
                // false,
                // false,
                // false,
            ],
        };
        // this.columns = [
        //     {
        //         title: this.state.json['publiccomponents-000019'],/* 国际化处理： 账簿名称*/
        //         dataIndex: "name",
        //         key: "name"
        //         // width: "40%"
        //     }
        // ]
        this.data = [
    
        ];
    }
    componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json
               // columns_reportSum:this.columns_reportSum,
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
    handleCopyCheckOK(){
        let { checkedData,checkedArray } =this.state;
        let { repeatData } = this.props
        checkedData=[]
        for(var i = 0; i < checkedArray.length; i++){
            if(checkedArray[i]==true){
                checkedData.push(repeatData[i]);
            }
        }
        let { handleCopyCheckOK } = this.props
        if(handleCopyCheckOK) {
            handleCopyCheckOK(checkedData)
        }
    }
    handleCancel(){
        let { handleCancel } = this.props
        if (handleCancel) {
            handleCancel()
        }
    }
     //全选
     onAllCheckChange = () => {
        let self = this;
        let checkedArray = [];
        let selIds = [];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
            checkedArray[i] = !self.state.checkedAll;
        }
        self.setState({
            checkedAll: !self.state.checkedAll,
            checkedArray: checkedArray,
        });
    };
    //单选
    onCheckboxChange = (text, record, index) => {
        let self = this;
        let allFlag = false;
        let checkedArray = self.state.checkedArray.concat();
        checkedArray[index] = !self.state.checkedArray[index];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
            if (!checkedArray[i]) {
                allFlag = false;
                break;
            } else {
                allFlag = true;
            }
        }
        self.setState({
            checkedAll: allFlag,
            checkedArray: checkedArray
        });
    };
    
    componentWillReceiveProps(nextProps){
        let { repeatData } = nextProps;
        // let {checkedArray} = this.state

        if(!repeatData){
            repeatData=[]
        }
        let checkArr = []
        repeatData.map((item)=>{
            checkArr.push(false)
            this.setState({
                checkedArray: checkArr
            })
        })
    }
    
    renderColumnsMultiSelect(columns) {
        const {checkedArray } = this.state;
        const { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        // let indeterminate_bool1 = true;
        if (multiSelect && multiSelect.type === "checkbox") {
            let i = checkedArray.length;
            while(i--){
                if(checkedArray[i]){
                    indeterminate_bool = true;
                    break;
                }
            }
            let defaultColumns = [
                {
                    title: (
                        <Checkbox
                        className="table-checkbox"
                        checked={this.state.checkedAll}
                        indeterminate={indeterminate_bool&&!this.state.checkedAll}
                        onChange={this.onAllCheckChange}
                        />
                    ),
                    key: "checkbox",
                    attrcode: 'checkbox',
                    dataIndex: "checkbox",
                    width: '60px',
                    render: (text, record, index) => {
                        return (
                        <Checkbox
                            className="table-checkbox"
                            checked={this.state.checkedArray[index]}
                            onChange={this.onCheckboxChange.bind(this, text, record, index)}
                        />
                        );
                    }
                }
            ];
            columns = defaultColumns.concat(columns);
        }
        return columns;
    }
    render() {
        let { visible, repeatData } = this.props  
        let columns = [
            {
                title: this.state.json['publiccomponents-000019'],/* 国际化处理： 账簿名称*/
                dataIndex: "name",
                key: "name"
                // width: "40%"
            }
        ]
        const emptyFunc = () => <span>{this.state.json['publiccomponents-000007']}！</span>/* 国际化处理： 这里没有数据*/
        let columnsldad = this.renderColumnsMultiSelect(columns);
        return (
            <div className='copycheckModal'>
                <Modal style={{ width: '40%' }} show={visible} onHide={this.handleCancel.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.json['publiccomponents-000021']}</Modal.Title>{/* 国际化处理： 复制信息校验框*/}
                    </Modal.Header >
                    <Modal.Body style={{ borderTop: '1px solid #ccc', borderBottom: '1px solid #ccc' }}>
                    <div style={{color: '#E14C46', marginBottom: '15px',textAlign:'center'}}>{this.state.json['publiccomponents-000024']}，{this.state.json['publiccomponents-000023']}</div>{/* 国际化处理： 以下账簿存在相同检查项编码的检查项,请选择需要覆盖的核算账簿*/}
                    <Table
                        columns={columnsldad}
                        data={repeatData?repeatData:[]}
                        emptyText={emptyFunc}
                    />
                    </Modal.Body>
                    <Modal.Footer >
                        <Button size="sm" onClick={this.handleCopyCheckOK.bind(this)}>{this.state.json['publiccomponents-000009']}</Button>{/* 国际化处理： 确定*/}
                        <Button onClick={this.handleCancel.bind(this)}>{this.state.json['publiccomponents-000010']}</Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div >

        )
    }
}
CopyCheckModal.defaultProps = defaultProps;
