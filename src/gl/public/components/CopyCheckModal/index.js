import React, { Component } from 'react';
import { base ,getMultiLang} from 'nc-lightapp-front';
const { NCButton: Button, NCCheckbox: Checkbox, NCModal: Modal, NCTable: Table, NCDiv } = base;
import './index.less';

/**
 * 
 * 复制到校验框
 * 展示重复数据，选择是否覆盖重复数据
 * 接收父级方法: handleCopyCheckOK(checkedData)
 * 接收父级参数：visible, repeatData
 * 
 */
const defaultProps = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
// const emptyFunc = () => <span>{this.state.json['publiccomponents-000020']}！</span>;/* 国际化处理： 暂无数据*/
export default class CopyCheckModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json:{},
			checkedData: [], // 选中数据
			checkedAll: false,
			checkedArray: [
				// false,
				// false,
				// false,
			]
		};
		// this.columns = [
		// 	{
		// 		title: this.state.json['publiccomponents-000019'],/* 国际化处理： 账簿名称*/
		// 		dataIndex: 'name',
		// 		key: 'name'
		// 		// width: "40%"
		// 	}
		// ];
		this.data = [];
	}
	componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json,
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
	//确定
	handleCopyCheckOK() {
		let { checkedData, checkedArray } = this.state;
		let { repeatData } = this.props;
		checkedData = [];
		for (var i = 0; i < checkedArray.length; i++) {
			if (checkedArray[i] == true) {
				checkedData.push(repeatData[i]);
			}
		}
		let { handleCopyCheckOK } = this.props;
		if (handleCopyCheckOK) {
			handleCopyCheckOK(checkedData);
		}
	}
	//取消
	handleCancel() {
		let { handleCancel } = this.props;
		if (handleCancel) {
			handleCancel();
		}
	}
	//全选
	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		let selIds = [];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		});
	};
	//单选
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		self.setState({
			checkedAll: allFlag,
			checkedArray: checkedArray
		});
	};

	componentWillReceiveProps(nextProps) {
		let { repeatData } = nextProps;
		// let {checkedArray} = this.state

		if (!repeatData) {
			repeatData = [];
		}
		let checkArr = [];
		repeatData.map((item) => {
			checkArr.push(false);
			this.setState({
				checkedArray: checkArr
			});
		});
	}

	renderColumnsMultiSelect(columns) {
		const { checkedArray } = this.state;
		const { multiSelect } = this.props;
		let select_column = {};
		let indeterminate_bool = false;
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
						<span fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={this.state.checkedAll}
								indeterminate={indeterminate_bool && !this.state.checkedAll}
								onChange={this.onAllCheckChange}
							/>
						</span>
					),
					key: 'checkbox',
					attrcode: 'checkbox',
					dataIndex: 'checkbox',
					width: '60px',
					render: (text, record, index) => {
						return (
							<span fieldid='firstcol'>
								<Checkbox
									className="table-checkbox"
									checked={this.state.checkedArray[index]}
									onChange={this.onCheckboxChange.bind(this, text, record, index)}
								/>
							</span>							
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}
	render() {
		let { visible, repeatData } = this.props;
		let columns = [
			{
				title: (<span fieldid='name'>{this.state.json['publiccomponents-000019']}</span>),/* 国际化处理： 账簿名称*/
				dataIndex: 'name',
				key: 'name',
				render: (text, record, index) => (<span fieldid='name'>{text}</span>)
				// width: "40%"
			}
		];
		const emptyFunc = () => <span>{this.state.json['publiccomponents-000020']}！</span>;/* 国际化处理： 暂无数据*/
		let columnsldad = this.renderColumnsMultiSelect(columns);
		return (
			<Modal fieldid='copycheck' className="simpleModal" show={visible} onHide={this.handleCancel.bind(this)}>
				<Modal.Header>
					<Modal.Title>{this.state.json['publiccomponents-000021']}</Modal.Title>{/* 国际化处理： 复制信息校验框*/}
				</Modal.Header>
				<Modal.Body>
					<NCDiv fieldid="copycheck" areaCode={NCDiv.config.TableCom}>
						<Table columns={columnsldad} data={repeatData ? repeatData : []} emptyText={emptyFunc} />
					</NCDiv>
					<p className="tip-box">
						<i className="iconfont icon-tixing" />
						<div className="tip">{this.state.json['publiccomponents-000022']}，{this.state.json['publiccomponents-000023']}</div>{/* 国际化处理： 以上账簿存在相同转账编号的自定义转账定义,请选择需要覆盖的核算账簿*/}
					</p>
				</Modal.Body>
				<Modal.Footer>
					<Button className="button-primary" fieldid='confirm' onClick={this.handleCopyCheckOK.bind(this)}>
						{this.state.json['publiccomponents-000009']}{/* 国际化处理： 确定*/}
					</Button>
					<Button fieldid='cancel' onClick={this.handleCancel.bind(this)}>{this.state.json['publiccomponents-000010']}</Button>{/* 国际化处理： 取消*/}
				</Modal.Footer>
			</Modal>
		);
	}
}
CopyCheckModal.defaultProps = defaultProps;
