/*
 * @Description: 标题区公共组件
 * @Author: llixxm
 */
import React, { Component } from 'react';
import { base, createPage } from 'nc-lightapp-front';
import './index.less'
const { NCDiv } = base;

class HeaderArea extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    static defaultProps = {
        title: '', //标题,有的页面没有标题
        searchContent: '', //标题区过滤组件
        btnContent: '', //按钮
        backBtnClick: '', //返回按点击事件
        pageBtnContent: '',  //翻页按钮
        initShowBackBtn: false, //是否显示返回按钮，true:显示,false:不显示,默认false
        showBorderBottom: true //header是否显示border-bottom,默认true:显示
    };
    //标题点击事件
    titleClick = () => {
        let { titleClick } = this.props
        titleClick && titleClick()
    }
    
    // 返回
    backBtnClick = () => {
        let { backBtnClick } = this.props
        typeof backBtnClick === "function" && backBtnClick() 
    }
    render() {
        let { initShowBackBtn, title, searchContent, btnContent, pageBtnContent, children, showBorderBottom,verifyflag } = this.props
        const { createBillHeadInfo } = this.props.BillHeadInfo
        let titleTxt = this.props.getSearchParam('n') // 为保证小应用名称与面包屑中一致，改为不接收外部title
        let outStyle = showBorderBottom ? {} : {borderBottom: 'none'} //是否显示header底部border
        return(
            <NCDiv areaCode={NCDiv.config.HEADER} className='header' style={outStyle}>
                {
                    createBillHeadInfo(
                        {
                            title: titleTxt,             //标题
                            // billCode:'12324353ASDWR242',     //单据号
                            titleClick: this.titleClick,
                            initShowBackBtn: initShowBackBtn,
                            backBtnClick: this.backBtnClick
                        }
                )}    
                {searchContent==='' ? '' : 
                    <div className='header-search-area'>
                        {searchContent}
                    </div>
                }      
                {children} 
                <div className={verifyflag&&verifyflag=='0'?'':'header-button-area'}>
                    {btnContent}
                </div>
                {pageBtnContent==='' ? '' :
                    <div className='header-pagination-area'>
                        {pageBtnContent}
                    </div>
                }
            </NCDiv>
        );
    }
}
HeaderArea = createPage({})(HeaderArea)
export default HeaderArea