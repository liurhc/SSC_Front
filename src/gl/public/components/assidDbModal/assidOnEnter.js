/**
 * @desc 监听enter键
 * @param event 事件 13 Enter 40 ArrowDown 38 ArrowUp
 * @param loadColums table中所有显示的列
 * @param attrcode 当前字段
 * @param id div的id属性 id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`}
 * @param index 行号
 */
export  function onEnterAction(event,$this,attrcode,id,index,record){
    let assidData=$this.state.assData;
    let idArr=id.split('-');
    let nextId='';
   
        if(idArr[4]<assidData.length-1){
            idArr[4]=idArr[4]-0+1;
            nextId=idArr.join('-');
            let nextColumn=document.getElementById(nextId);
            let input=nextColumn.querySelector('input:not([tabindex="-1"])');
            if(!input){
                input=nextColumn.querySelector('div[tabindex="0"]');
            }
            if($this.ViewModel){
                $this.ViewModel.shouldAutoFocus = true;//bbqin让加的
            }
            input.focus();
            if ($this.ViewModel) {
                $this.ViewModel.shouldAutoFocus = false;
            }
        }else{//最后一个的时候聚焦确定按钮
            let bottom_area=document.getElementById('assDataModal');
            let buttonConfirm=bottom_area.querySelector('button[tabindex="0"]');
            if(buttonConfirm){
                buttonConfirm.focus();
            }
            // $this.confirm();
        }
    
}