import React, { Component } from 'react';
import ReactDOM from 'react-dom';//操作dom 
import { high, base, ajax,getMultiLang,toast  } from 'nc-lightapp-front';
const { Refer } = high;
import createScript from '../uapRefer.js';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	AutoComplete,
	NCDropdown: Dropdown,
	NCPanel: Panel,
	NCModal: Modal,
	NCForm,
	NCHotKeys:HotKeys
} = base;
import {
	CheckboxItem,
	RadioItem,
	TextAreaItem,
	ReferItem,
	SelectItem,
	InputItem,
	DateTimePickerItem
} from '../FormItems';
import './index.less';
const { NCFormItem: FormItem } = NCForm;
import {GetChinese, RemoveChinese} from '../../common/stringDeal.js';
import ReferLoader from '../../../public/ReferVoucherLoader';
import {onEnterAction} from './assidOnEnter';
// const dateInputPlaceholder = this.state.json['publiccomponents-000000'];/* 国际化处理： 选择日期*/
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
const format = 'YYYY-MM-DD';
const timeFormat = 'YYYY-MM-DD HH:mm:ss';
export default class AssidModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json:{},
			isMultiSelected: true, //辅助核算档案值默认多选
			showModal: false,
			isShowUnit: false, //是否显示业务单元
			assData: [
				//辅助核算信息
			],
			childAssData: [], //接受父组件传过来的参数
			loadData: [], //查询模板加载数据
			listItem: {}, //模板数据对应值
			checkedAll: false,
			SelectedAssData: [], //选中的数据
			checkedArray: [],
			assidData: {}, //key=assid,value=assData
			assitemData: {} //key=pk_accasoa+prepareddate,value=assitems
		};
		this.close = this.close.bind(this);
		this.moduleId="assidDbModal";
		this.ViewModel=this.props.ViewModel;
	}
	componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json
				},()=>{
				
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
	//字符转换为数组
	strChangetoArr(str) {
		if (str.indexOf(',') != -1) {
			return str.split(',');
		} else {
			return [ str ];
		}
	}

	componentWillReceiveProps(nextProp) {
		/** 
            *父组件传递过来的pretentAssData里包含 
            *   pk_accountingbook,
            *    pk_accasoa,
            *    prepareddate,
            *    pk_org,assData,assid,checkboxShow
            * 
            *   pk_defdoclist,
            */

		let self = this;
		let { checkedArray, childAssData, assidData, assitemData, isMultiSelected } = self.state;
		let { assData, assid, pk_accasoa, prepareddate, pk_accountingbook, pk_org, isMultiSelectedEnabled } = nextProp;
		if (isMultiSelectedEnabled == false || isMultiSelectedEnabled == true) {
			//接收父组件传递过来的值
			isMultiSelected = isMultiSelectedEnabled;
		} else {
			isMultiSelected = true; //默认多选
		}
		if (nextProp.showOrHide&&assData.length > 0) {
			let assArr = [];
			if(self.props.voucherType=='voucherBill'){//凭证单
				assData.forEach((ass) => {
					let newass = {
						checktypecode: ass.m_checktypecode,
						checktypename: ass.m_checktypename,
						checkvaluecode: ass.m_checkvaluecode ? ass.m_checkvaluecode.split() : [],
						checkvaluename: ass.m_checkvaluename ? ass.m_checkvaluename.split() : [],
						classid: ass.m_classid,
						pk_accassitem: ass.m_pk_checktype,
						pk_Checkvalue: ass.m_pk_checkvalue ? ass.m_pk_checkvalue.split() : []
						// refCode:ass.refCode,
						// refnodename:ass.m_checktypename
					};
					assArr.push(newass);
				});
			}else{//凭证
				assData.forEach((ass) => {
					let newass = {
						checktypecode: ass[2],//ass.m_checktypecode,
						checktypename: ass[3],//ass.m_checktypename,
						checkvaluecode: ass[5]?ass[5].split():[] ,//ass.m_checkvaluecode ? ass.m_checkvaluecode.split() : [],
						checkvaluename: ass[6]?ass[6].split():[] ,//ass.m_checkvaluename ? ass.m_checkvaluename.split() : [],
						classid: ass[0],//ass.m_classid,
						pk_accassitem: ass[1],//ass.m_pk_checktype,
						pk_Checkvalue: ass[4]?ass[4].split():[] ,//ass.m_pk_checkvalue ? ass.m_pk_checkvalue.split() : []
						// refCode:ass.refCode,
						// refnodename:ass.m_checktypename
					};
					assArr.push(newass);
				});
			}
			
			assData = assArr;
		}
		let pretentAssData = {
			pk_org: pk_org ? pk_org : '',
			assData: assData,
			assid: assid,
			pk_accasoa: pk_accasoa,
			prepareddate: prepareddate,
			pk_accountingbook: pk_accountingbook
		};

		if (nextProp.showOrHide) {
			childAssData = pretentAssData;
			if (pretentAssData && pretentAssData.assid && pretentAssData.assid.length > 0) {
				//通过assid展示
				// if (assidData.hasOwnProperty(pretentAssData.assid)) {
				// 	childAssData.assData = assidData[pretentAssData.assid];
				// 	self.setState({
				// 		assData: assidData[pretentAssData.assid],
				// 		childAssData,
				// 		isMultiSelected
				// 	});
				// } else {
					let assidParam = {
						pk_accasoa: pretentAssData.pk_accasoa,
						date: pretentAssData.prepareddate,
						assid: pretentAssData.assid
					};
					ajax({
						url: '/nccloud/gl/voucher/queryAssTypeAndValue.do',
						data: assidParam,
						success: function(response) {
							const { data, success } = response;
							if (data) {
								if (data.length > 0) {
									data.map((item, index) => {
										item.key = index;
										checkedArray.push(false);
										item.m_classid = item.classid;
										item.pk_Checktype = item.pk_accassitem;
										if (item.checkvaluecode) {
											item.checkvaluecode = self.strChangetoArr(item.checkvaluecode);
										}
										if (item.checkvaluename) {
											item.checkvaluename = self.strChangetoArr(item.checkvaluename);
										}
										if (item.pk_Checkvalue) {
											item.pk_Checkvalue = self.strChangetoArr(item.pk_Checkvalue);
										}
									});
								}
							}
							childAssData.assData = data;
							assidData[pretentAssData.assid] = data;
							self.setState({
								assData: data,
								childAssData,
								assidData,
								isMultiSelected
							},()=>{
								self.autoFocus()
							});
						}						
					});
				// }
			} else if (!pretentAssData.assid || pretentAssData.assid.length == 0) {
				//没有assid，则通过会计科目和日期重新查询
				let queryData = {
					pk_accasoa: pretentAssData.pk_accasoa,
					prepareddate: pretentAssData.prepareddate
				};
				if (assitemData.hasOwnProperty(queryData.pk_accasoa + queryData.prepareddate)) {
					let assData = assitemData[queryData.pk_accasoa + queryData.prepareddate];
					childAssData.assData = assData;
					self.setState({
						assData,
						childAssData,
						isMultiSelected
					},()=>{
						self.autoFocus()
					});
				} else {
					let assData = [];
					//请求辅助核算数据
					let url = '/nccloud/gl/voucher/queryAssItem.do';

					ajax({
						url: url,
						data: queryData,
						success: function(response) {
							const { success } = response;
							//渲染已有账表数据遮罩
							if (success) {
								if (response.data && response.data.length > 0) {
									response.data.map((item, index) => {
										item.key = index;
										checkedArray.push(false);
										let obj = {
                                            digits:item.digits?item.digits:'0',
                                            inputlength:item.inputlength?item.inputlength:'',
											key: index,
											checktypecode: item.code,
											checktypename: item.name,
											pk_Checktype: item.pk_accassitem,
											refCode: item.refCode ? item.refCode : item.code,
											refnodename: item.refnodename,
											pk_accassitem: item.pk_accassitem,
											classid: item.classid,
											pk_defdoclist: item.classid,
											pk_accountingbook: childAssData.pk_accountingbook
										};
										assData.push(obj);
									});
								}
								if (childAssData.assData && childAssData.assData.length > 0) {
									//判断当前行科目辅助核算类型与上一行科目辅助核算类型是否相同
									let preLineAss = {};
									childAssData.assData.forEach((ass) => {
										preLineAss[ass.classid] = {
											pk_Checkvalue: ass.pk_Checkvalue,
											checkvaluecode: ass.checkvaluecode,
											checkvaluename: ass.checkvaluename
										};
									});
									// if (childAssData.assData.length == assData.length) {
										let flag = true;
										// for (let i = 0; i < assData.length; i++) {
										// 	if (!preLineAss.hasOwnProperty(assData[i].classid)) {
										// 		flag = false;
										// 		break;
										// 	}
										// }
										if (flag) {
											//属性全部匹配，将上一行的值赋值到本行
											assData.forEach((ass) => {
												if(preLineAss.hasOwnProperty(ass.classid)){
													ass.pk_Checkvalue = preLineAss[ass.classid].pk_Checkvalue;
													ass.checkvaluecode = preLineAss[ass.classid].checkvaluecode;
													ass.checkvaluename = preLineAss[ass.classid].checkvaluename;
												}
											});
										}
									// }
								}
								childAssData.assData = assData;
								assitemData[queryData.pk_accasoa + queryData.prepareddate] = assData;
								self.setState({
									assData,
									childAssData,
									assitemData,
									isMultiSelected
								},()=>{
									self.autoFocus();
								});
							}
						}
					});
				}
			}
		}
	}
    //请求最新的assData
    getNewassdata=()=>{
        let self = this;
        let { checkedArray, assData, childAssData } = self.state;
        //请求辅助核算数据
        let url = '/nccloud/gl/voucher/queryAssItem.do';
        let queryData = {
            pk_accasoa: childAssData.pk_accasoa,
            prepareddate: childAssData.prepareddate
        };
        checkedArray=[];
        assData=[];
        ajax({
            url: url,
            data: queryData,
            success: function(response) {
                const { success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if (response.data && response.data.length > 0) {
                        response.data.map((item, index) => {
                            item.key = index;
                            checkedArray.push(false);
                            let obj = {
                                digits:item.digits?item.digits:'0',
                                inputlength:item.inputlength?item.inputlength:'',
                                key: index,
                                checktypecode: item.code,
                                checktypename: item.name,
                                pk_Checktype: item.pk_accassitem,
                                refCode: item.refCode ? item.refCode : item.code,
                                refnodename: item.refnodename,
                                pk_accassitem: item.pk_accassitem,
                                classid: item.classid,
                                pk_defdoclist: item.classid,
                                pk_accountingbook: childAssData.pk_accountingbook
                            };
                            assData.push(obj);
                        });
                    }
                    childAssData.assData=assData;
                    self.setState({
                        assData,childAssData,checkedArray
                    },()=>{
                        self.autoFocus()
                    })
                }
            }
        })
    }
    //获取默认辅助核算值
	getDefaultAss=(paramer,assData,isMultiSelected)=>{
		let self=this;
		let url='/nccloud/gl/voucher/defaultAss.do';
		ajax({
			url:url,
			data:paramer,
			success:function(response){
				let {success,data}=response;
				if(success){
					if(data&&assData.length>0){
						assData.map((item,index)=>{
							if(item.pk_Checktype&&data[item.pk_Checktype]){
								let accasoaArr = [];
								let refnameArr = [],refpkArr = [],refcodeArr = [];
								accasoaArr.push(data[item.pk_Checktype]);
								accasoaArr.map((arr, index) => {
									refnameArr.push(arr.display);
									refpkArr.push(arr.value);
									refcodeArr.push(arr.code);
								});
								item.checkvaluename = refnameArr;
								item.pk_Checkvalue = refpkArr;
								item.checkvaluecode = refcodeArr;
							}
						})
						self.setState({
							assData
						})
					}
				}
			}
		})
	}
	//表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

	autoFocus=()=>{
		let self=this;
		if(self.props.voucherType=='voucher'){//凭证才走自动获取焦点
			setTimeout(() => {
				let modelNode= ReactDOM.findDOMNode(self);
				let inputNode=modelNode&&modelNode.querySelector('#assDataModal input:not([tabindex="-1"])');
				if(this.props.ViewModel){
					this.props.ViewModel.shouldAutoFocus = true;//bbqin让加的
				}
				if(inputNode){
					inputNode&&inputNode.focus()
				}
				if(this.props.ViewModel){
					this.props.ViewModel.shouldAutoFocus = false;
				}
			}, 500);
		}
	}

	close=()=> {
		this.setState({
			assitemData:{}
		},()=>{
			this.props.handleClose();
		}
	)
		
	}
	//确定 过滤勾选的数据
	confirm = () => {
		let self = this;
		let { SelectedAssData, checkedArray, listItem, assData, childAssData,assitemData,assidData } = self.state;
		let url = '/nccloud/gl/voucher/queryAssIds.do';
		let parm = {
			ass: assData,
			pk_accountingbook: childAssData.pk_accountingbook,
			pk_accasoa: childAssData.pk_accasoa,
			prepareddate: childAssData.prepareddate
		};
		let assid = '',
			assname = '';
		ajax({
			url: url,
			data: parm,
			async: true,
			success: function(response) {
				let { data } = response;
				if (data) {
					// data=[{
					//     data:assData,
					//     assid:assid,
					//     assname:assname
					// }]

					data.forEach((datass) => {
                        let assArr = [];
						let newass=[];
						let voucherBill_newass={};
						if(self.props.voucherType=='voucherBill'){//凭证单
							datass.assData.forEach((ass) => {
								voucherBill_newass = {
									m_checktypecode: ass.checktypecode,//3
									m_checktypename: ass.checktypename,//4
									m_checkvaluecode: ass.checkvaluecode,//6
									m_checkvaluename: ass.checkvaluename,////7
									m_classid: ass.classid,//1
									m_pk_checktype: ass.pk_accassitem,//2
									m_pk_checkvalue: ass.pk_Checkvalue,//5
									refCode: ass.refCode
								};
								assArr.push(voucherBill_newass);
							});
						}else{//voucher 总账凭证
							datass.assData.forEach((ass) => {
								newass=[
									ass.classid,
									ass.pk_accassitem,
									ass.checktypecode,
									ass.checktypename,
									ass.pk_Checkvalue,ass.checkvaluecode,ass.checkvaluename]
								assArr.push(newass);
							});
						}
						datass.assData = assArr;
                    });
					// childAssData.assid = data[0].assid;
					self.setState({
						childAssData,
						assitemData:{}
					});
					self.props.onConfirm(data);
				}else{
                    self.close();
                }
			}
		});
	};
	//全选
	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		let selIds = [];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		});
	};
	//单选
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		self.setState({
			checkedAll: allFlag,
			checkedArray: checkedArray
		});
	};

	renderColumnsMultiSelect(columns) {
		const { checkedArray } = this.state;
		const { multiSelect } = this.props;
		let select_column = {};
		let indeterminate_bool = false;
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
						<Checkbox
							className="table-checkbox"
							checked={this.state.checkedAll}
							indeterminate={indeterminate_bool && !this.state.checkedAll}
							onChange={this.onAllCheckChange}
						/>
					),
					key: 'checkbox',
					attrcode: 'checkbox',
					dataIndex: 'checkbox',
					width: '60px',
					render: (text, record, index) => {
						return (
							<Checkbox
								className="table-checkbox"
								checked={this.state.checkedArray[index]}
								onChange={this.onCheckboxChange.bind(this, text, record, index)}
							/>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}

	render() {
		let { showOrHide,hotKeyboard } = this.props;
		let { loadData, assData } = this.state;
		const dateInputPlaceholder = this.state.json['publiccomponents-000000'];/* 国际化处理： 选择日期*/
		let columns10 = [
			{
			title: (<span fieldid="checktypecode">{this.state.json['publiccomponents-000001']}</span>),/* 国际化处理： 核算类型编码*/
				dataIndex: 'checktypecode',
				key: 'checktypecode',
				width: '100',
				render: (text, record, index) => {
					return <span fieldid="checktypecode">{record.checktypecode}</span>;
				}
			},
			{
			title: (<span fieldid="checktypename">{this.state.json['publiccomponents-000002']}</span>),/* 国际化处理： 核算类型名称*/
				dataIndex: 'checktypename',
				key: 'checktypename',
				width: '200',
				render: (text, record, index) => {
					return <span fieldid="checktypename">{record.checktypename}</span>;
				}
			},
			{
			title: (<span fieldid="checkvaluename">{this.state.json['publiccomponents-000003']}</span>),/* 国际化处理： 核算内容*/
				dataIndex: 'checkvaluename',
                key: 'checkvaluename',
                width: '200',
				render: (text, record, index) => {
					let { assData, childAssData, isMultiSelected } = this.state;
					let defaultValue = [];
					if (isMultiSelected) {
						//多选
						if (assData[index]['checkvaluename']&&assData[index]['checkvaluename'].length>0) {
							assData[index]['checkvaluename'].map((item, _index) => {
								defaultValue[_index] = { refname: item, refpk: '',refcode:'' };
							});
							assData[index]['pk_Checkvalue'].map((list, _list) => {
								defaultValue[_list].refpk = list;
							});
							if(assData[index]['checkvaluecode']&&assData[index]['checkvaluecode'].length>0){
								assData[index]['checkvaluecode'].map((num,_num)=>{
									defaultValue[_num].refcode = num;
								})
							}
						} else {
							defaultValue = [ { refname: '', refpk: '',refcode:'' } ];
						}
					} else {
						//单选
						if (assData[index]['checkvaluename']) {
							assData[index]['checkvaluename'].map((item, _index) => {
								defaultValue = { refname: item, refpk: '' };
							});
							assData[index]['pk_Checkvalue'].map((list, _index) => {
								defaultValue.refpk = list;
							});
							if(assData[index]['checkvaluecode']){
								assData[index]['checkvaluecode'].map((num,_num)=>{
									defaultValue[_num].refcode = num;
								})
							}
						} else {
							defaultValue = { refname: '', refpk: '',refcode:'' };
						}
					}
					if (record.refnodename) {
						let referUrl = record.refnodename + '.js';
						let config = {
							pk_org: childAssData.pk_org,
							pk_defdoclist: record.classid,
							pk_accountingbook: childAssData.pk_accountingbook,
							date: childAssData.prepareddate,
							pk_accasoa: childAssData.pk_accasoa,
							classid: record.classid,
							assvo: JSON.stringify(assData),
							pk_unit:childAssData.pk_org,
							isDataPowerEnable: 'Y',
							DataPowerOperationCode: 'fi'
							// "GridRefActionExt": 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder',
							// "TreeRefActionExt": 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder'
						};
						// if (!this.state[record.pk_accassitem]) {
						// 	{
						// 		createScript.call(this, referUrl, record.pk_accassitem);
						// 	}
						// 	return <div />;
						// } else {
							if (
								record.classid == 'b26fa3cb-4087-4027-a3b6-c83ab2a086a9' ||
								record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f'
							) {
								//部门，人员
								return (
									<div
										onKeyDown={(event) => {
											let referPopWindowFlag=true;
											let referPopWindowArr =document.getElementsByClassName('refer-pop-window');
											for(let i=0;i<referPopWindowArr.length;i++){
												if(referPopWindowArr[i].style.display=='flex'){
													referPopWindowFlag=false;
													break;
												}
											}
											if(event.keyCode=='9'&&assData.length-1==index&&referPopWindowFlag ){
													 event.stopPropagation();
													 event.preventDefault();
													let modelNode= ReactDOM.findDOMNode(this);
													let inputNode=modelNode&&modelNode.querySelector('#assDataModal input.refer-input:first-child');
													inputNode&&inputNode.focus()
											}
										}}
										id={`hot-key-${this.moduleId}-checkvaluename-${index}`}
										>
										<ReferLoader
										// {this.state[record.pk_accassitem]({
											onEnter={(event)=>{
												onEnterAction(event,this,'checkvaluename',`hot-key-${this.moduleId}-checkvaluename-${index}`,index,record)
											}}
											fieldid='checkvaluename'
											tag={record.pk_accassitem}
											refcode={referUrl}
											value={ defaultValue}
											isShowUnit={true}
											unitCondition={{
												pk_financeorg: childAssData.pk_org,
												TreeRefActionExt: 'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
											}}
											isMultiSelectedEnabled={isMultiSelected}
											unitValueIsNeeded={false}
											isShowDimission={true}
											queryCondition={(props) => {
												if (props.refType == 'grid') {
													config.GridRefActionExt = 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
													config.UsualGridRefActionExt='nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
												} else if (props.refType == 'tree') {
													config.TreeRefActionExt = 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
												} else if (props.refType == 'gridTree') {
													config.GridRefActionExt = 'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
													config.UsualGridRefActionExt='nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
												}

												if(record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f'){
													//显示离职人员
													config.isShowDimission=true;
												}

												config.busifuncode = 'all';
												return config;
											}}
											onChange={(v) => {
												let { assData } = this.state;
												let originData = this.findByKey(record.key, assData);
												let refnameArr = [],refpkArr = [],refcodeArr = [];
												let accasoaArr = [];
												if (originData) {
													if (isMultiSelected) {
														//多选
														accasoaArr = v;
													} else {
														accasoaArr.push(v);
													}
													accasoaArr.map((arr, index) => {
														refnameArr.push(arr.refname);
														refpkArr.push(arr.refpk);
														refcodeArr.push(arr.refcode);
													});
													originData.checkvaluename = refnameArr;
													originData.pk_Checkvalue = refpkArr;
													originData.checkvaluecode = refcodeArr;
												}
												if(accasoaArr.length>0&&assData.length>1&&this.props.voucherType!='voucherBill'){//非凭证单
													config.assvo=JSON.stringify(assData);
													this.getDefaultAss(config,assData,isMultiSelected);
												}
												childAssData.assData = assData;
												this.setState({
													assData,
													childAssData
												});
											}}
										/>
									</div>
									
								) 
							} else {
								let isTreelazyLoad = record.classid == '8c6510dd-3b8a-4cfc-a5c5-323d53c6006f'; //客商参照设置懒加载
								return (
									<div 
										onKeyDown={(event) => {
											let referPopWindowFlag=true;
											let referPopWindowArr =document.getElementsByClassName('refer-pop-window');
											for(let i=0;i<referPopWindowArr.length;i++){
												if(referPopWindowArr[i].style.display=='flex'){
													referPopWindowFlag=false;
													break;
												}
											}
												if(event.keyCode=='9'&&assData.length-1==index&&referPopWindowFlag ){
													event.stopPropagation();
													event.preventDefault();
													let modelNode= ReactDOM.findDOMNode(this);
													let inputNode=modelNode&&modelNode.querySelector('#assDataModal input.refer-input:first-child');
													inputNode&&inputNode.focus()
												}
											}}
											id={`hot-key-${this.moduleId}-checkvaluename-${index}`}
										>
											<ReferLoader
									// {this.state[record.pk_accassitem]({
										onEnter={(event)=>{
											onEnterAction(event,this,'checkvaluename',`hot-key-${this.moduleId}-checkvaluename-${index}`,index,record)
										}}
										fieldid='checkvaluename'
										refcode={referUrl}
										tag={record.pk_accassitem}
										value={ defaultValue}
										isMultiSelectedEnabled={isMultiSelected}
										isTreelazyLoad={isTreelazyLoad}
										queryCondition={(props) => {
											if (props) {
												if (props.refType == 'grid') {
													config.GridRefActionExt =
														'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
													config.UsualGridRefActionExt='nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
												} else if (props.refType == 'tree') {
													config.TreeRefActionExt =
														'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
												} else if (props.refType == 'gridTree') {
													config.GridRefActionExt =
														'nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
													config.UsualGridRefActionExt='nccloud.web.gl.ref.AccAssRuleCtrlSqlBuilder';
												}
											}
											return config;
										}}
										onChange={(v) => {
											let { assData } = this.state;
											let originData = this.findByKey(record.key, assData);
											let refnameArr = [],refpkArr = [],refcodeArr = [];
                                            let accasoaArr = [];
											if (originData) {
												if (isMultiSelected) {
													//多选
													accasoaArr = v;
												} else {
													accasoaArr.push(v);
												}
												accasoaArr.map((arr, index) => {
													refnameArr.push(arr.refname);
													refpkArr.push(arr.refpk);
													refcodeArr.push(arr.refcode);
												});
												originData.checkvaluename = refnameArr;
												originData.pk_Checkvalue = refpkArr;
												originData.checkvaluecode = refcodeArr;
                                            }
                                            if(accasoaArr.length>0&&assData.length>1&&this.props.voucherType!='voucherBill'){//非凭证单
												config.assvo=JSON.stringify(assData);
												this.getDefaultAss(config,assData,isMultiSelected);
											}
											childAssData.assData = assData;
											this.setState({
												assData,
												childAssData
											});
										}}
										/>
									</div>
								)
							}
						// }
					} else {
						//不是参照的话要区分日期、字符、数值
						if (record.classid == 'BS000010000100001033') {
							//日期
							return (
								<div id={`hot-key-${this.moduleId}-checkvaluename-${index}`}>
								<DatePicker
									//name={item.itemKey}
									onEnter={(event)=>{
										onEnterAction(event,this,'checkvaluename',`hot-key-${this.moduleId}-checkvaluename-${index}`,index,record)
									}}
                                    format={format}
									type="customer"
									isRequire={true}
									placeholder={dateInputPlaceholder}
									value={defaultValue[0]?defaultValue[0].refname:defaultValue.refname}
									onChange={(v) => {
										let { assData } = this.state;
										let originData = this.findByKey(record.key, assData);
										if (originData) {
											let assArr = [];
											assArr.push(v);
											originData.checkvaluename = assArr;
											originData.pk_Checkvalue = assArr;
											originData.checkvaluecode = assArr;
										}
										childAssData.assData = assData;
										this.setState({
											assData,
											childAssData
										});
									}}
								/>
								</div>
							);
						}else if (record.classid == 'BS000010000100001034') {
							//日期时间
							return (
								<div id={`hot-key-${this.moduleId}-checkvaluename-${index}`}>
								<DatePicker
									onEnter={(event)=>{
										onEnterAction(event,this,'checkvaluename',`hot-key-${this.moduleId}-checkvaluename-${index}`,index,record)
									}}
                                    showTime={true}
                                    format={timeFormat}
									//name={item.itemKey}
									type="customer"
									isRequire={true}
									placeholder={dateInputPlaceholder}
									value={defaultValue[0]?defaultValue[0].refname:defaultValue.refname}
									onChange={(v) => {
										let { assData } = this.state;
										let originData = this.findByKey(record.key, assData);
										if (originData) {
											let assArr = [];
											assArr.push(v);
											originData.checkvaluename = assArr;
											originData.pk_Checkvalue = assArr;
											originData.checkvaluecode = assArr;
										}
										childAssData.assData = assData;
										this.setState({
											assData,
											childAssData
										});
									}}
								/>
								</div>
							);
						} else if (record.classid == 'BS000010000100001031'||record.classid=='BS000010000100001004') {//数值 整数
							return (
								<div id={`hot-key-${this.moduleId}-checkvaluename-${index}`}>
								<NCNumber
									onEnter={(event)=>{
										onEnterAction(event,this,'checkvaluename',`hot-key-${this.moduleId}-checkvaluename-${index}`,index,record)
									}}
									scale={Number(record.digits?record.digits:'0')}
									value={defaultValue[0]?defaultValue[0].refname:defaultValue.refname}
									maxlength={Number(record.inputlength)+Number(record.digits?record.digits:'0')}
									placeholder={this.state.json['publiccomponents-000004']}/* 国际化处理： 请输入数字*/
									onChange={(v) => {
                                        if(v.indexOf('.')!=-1){
                                            if(v&&v.slice(0,v.indexOf('.')).length>Number(record.inputlength)){
                                                toast({content:this.state.json['publiccomponents-000148']+record.inputlength,color:'warning'});//输入的数字长度不能超过用户设置的长度
                                                v=v.slice(0,record.inputlength);
                                            }
                                        }else{
                                            if(v&&v.length>Number(record.inputlength)){
                                                toast({content:this.state.json['publiccomponents-000148']+record.inputlength,color:'warning'});//输入的数字长度不能超过用户设置的长度
                                                v=v.slice(0,record.inputlength);
                                            }
                                        }
										let { assData } = this.state;
										let originData = this.findByKey(record.key, assData);
										if (originData) {
											let assArr = [];
											assArr.push(v);
											originData.checkvaluename = assArr;
											originData.pk_Checkvalue = assArr;
											originData.checkvaluecode = assArr;
										}
										childAssData.assData = assData;
										this.setState({
											assData,
											childAssData
										});
									}}
								/>
								</div>
							);
						} else if (record.classid == 'BS000010000100001032') {
							//布尔
							return (
								<div id={`hot-key-${this.moduleId}-checkvaluename-${index}`}>
								<SelectItem
									onEnter={(event)=>{
										onEnterAction(event,this,'checkvaluename',`hot-key-${this.moduleId}-checkvaluename-${index}`,index,record)
									}}
									name={record.checktypecode}
									defaultValue={defaultValue[0]?defaultValue[0].refname:defaultValue.refname}
									items={() => {
										return [
											{
												label: this.state.json['publiccomponents-000005'],/* 国际化处理： 是*/
												value: 'Y'
											},
											{
												label: this.state.json['publiccomponents-000006'],/* 国际化处理： 否*/
												value: 'N'
											}
										];
									}}
									onChange={(v) => {
										let { assData } = this.state;
										let originData = this.findByKey(record.key, assData);
										if (originData) {
											let assArr = [];
											assArr.push(v);
											originData.checkvaluename = assArr;
											originData.pk_Checkvalue = assArr;
											originData.checkvaluecode = assArr;
										}
										childAssData.assData = assData;
										this.setState({
											assData,
											childAssData
										});
									}}
								/>
								</div>
							);
						} else {
							//字符
							return (
								<div id={`hot-key-${this.moduleId}-checkvaluename-${index}`}>
								<FormControl
									onEnter={(event)=>{
										onEnterAction(event,this,'checkvaluename',`hot-key-${this.moduleId}-checkvaluename-${index}`,index,record)
									}}
									value={defaultValue&&defaultValue[0]?defaultValue[0].refname:defaultValue.refname}
									onChange={(v) => {
                                        if(v&&v.length>0){
                                            if(GetChinese(v).length>0&&2*(GetChinese(v).length)>Number(record.inputlength)){
                                                v=v.slice(0,Number(record.inputlength/2));
                                            }else if(GetChinese(v).length>0&&2*(GetChinese(v).length)+RemoveChinese(v).length>Number(record.inputlength)){
                                                toast({content:this.state.json['publiccomponents-000148']+record.inputlength,color:'warning'});//输入的数字长度不能超过用户设置的长度
                                                v= RemoveChinese(v);
                                            }else if(RemoveChinese(v).length>Number(record.inputlength)){
                                                toast({content:this.state.json['publiccomponents-000148']+record.inputlength,color:'warning'});//输入的数字长度不能超过用户设置的长度
                                                v=v.slice(0,Number(record.inputlength));
                                            }
                                        }
										let { assData } = this.state;
										let originData = this.findByKey(record.key, assData);
										if (originData) {
											let assArr = [];
											assArr.push(v);
											originData.checkvaluename = assArr;
											originData.pk_Checkvalue = assArr;
											originData.checkvaluecode = assArr;
										}
										childAssData.assData = assData;
										this.setState({
											assData,
											childAssData
										});
									}}
								/>
								</div>
							);
						}
					}
				}
			}
		];
		let columnsldad = this.renderColumnsMultiSelect(columns10);
		const emptyFunc = () => <span>{this.state.json['publiccomponents-000007']}！</span>;/* 国际化处理： 这里没有数据*/
		let indexInfo={'id':'assDataModal','zIndex':200};
	//	this.autoFocus()
		return (
			<Modal className="msg-modal simpleModal combine" 
				fieldid="assidDb"
				id='assDataModal'
				show={showOrHide}
				// zIndex={210}
				// indexInfo={indexInfo}
				//show={true}
				// backdrop={ this.state.modalDropup }
				onHide={this.close}
				onEntered={()=>{
					this.autoFocus()
					// setTimeout(function() {
					// 	// 方案一  延时处理
					// 	inputNode&&inputNode.focus()
					// }, 5000);
					
				}}
				ref={Modal => (this.Modal = Modal)}
			>
			{hotKeyboard&&<HotKeys
                        keyMap={{
                            sureBtnHandler: ["alt+y"],
                            cancelBtnHandler: ["alt+n"]
                        }}
                        handlers={{
                            sureBtnHandler: () => {
                                // 确定按钮的事件 增加top的判断避免所有弹窗逻辑都被触发  by bbqin
                                if (this.Modal && this.Modal.isTopModal()) {
									this.confirm()
                                }
                            },
                            cancelBtnHandler: () => {
                                // 取消按钮的事件  增加top的判断避免所有弹窗逻辑都被触发  by bbqin
                                if (this.Modal && this.Modal.isTopModal()) {
									this.close();
                                }
                            }
                        }}
                        className="hotkeys-wrapper"
                        focused={true}
                        attach={document.body}
                    />}	
				<Modal.Header closeButton fieldid="header-area">
					<Modal.Title>{this.state.json['publiccomponents-000008']}</Modal.Title>{/* 国际化处理： 辅助核算*/}
					<div className="icon-wraper">
						<i
							className="iconfont icon-shuaxin1"
							onClick={
								this.getNewassdata.bind(this)
							}
						/>
						{/* <i className="iconfont icon-guanbi" onClick={() => this.props.onCancel(true)} /> */}
					</div>
				</Modal.Header>
				<Modal.Body>
					<div style={{ padding: '10px 20px' }}>
                        <Table 
                            columns={columnsldad}  
                            data={assData} 
                            emptyText={emptyFunc}
                            scroll={{x:true, y: 150 }}
                         />
					</div>
				</Modal.Body>
				<Modal.Footer fieldid="bottom_area">
					<Button className="button-primary" onClick={this.confirm} fieldid="assidConfirm" tabindex="0">
						{this.state.json['publiccomponents-000009']}(<span class='text-decoration-underline'>Y</span>) {/* 国际化处理： 确定*/}
					</Button>
					<Button onClick={this.close} fieldid="close"> {this.state.json['publiccomponents-000010']}(<span class='text-decoration-underline'>N</span>) </Button>{/* 国际化处理： 取消*/}
				</Modal.Footer>
			</Modal>
		);
	}
}
// AssidModal.defaultProps = defaultProps12;
// export default function (props = {}) {
//     var conf = {
//     };

//     return <AssidModal {...Object.assign(conf, props)} />
// }
