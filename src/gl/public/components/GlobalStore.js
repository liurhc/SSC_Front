/**
 * 全局store, 在整个App的生命周期生存
 */
import React, { Component } from 'react';

class GlobalStore {
    //测试数据
    name = 'test';
    rootUrl=(NODE_ENV=='development')? '/gl':'/nccloud/resources/gl';
    //全局缓存对象,刷新后会失效
    cache = {}

    //设置缓存
    setCache = (key, value) => {
        this.cache[key] = value;
    }

    //获取缓存
    getCache = (key) => {
        return this.cache[key];
    }

    //判断缓存是否存在
    isCache = (key) => {
        //return !this.cache.hasOwnProperty(key)&&(key in this.cache);
        return this.cache[key] != undefined;
    }
    clearCache = (key) => {
        //return !this.cache.hasOwnProperty(key)&&(key in this.cache);
        delete this.cache[key];
    }   
}

export default new GlobalStore();
