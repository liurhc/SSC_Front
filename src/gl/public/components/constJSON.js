const formId='head';
const tableId = 'gl_voucher';

const searchId = '20021005query';

const dataSource = 'gl.gl_voucher.voucher_list.jumper';

const dataSourceCheck='gl.gl_voucher.voucher_list.checkArray';

const dataSourceTable = 'gl.gl_voucher.voucher_list.table';

const dataSourceCoord = 'gl.gl_voucher.voucher.coorder';

const dataSourceDetail = 'gl.gl_voucher.voucher.detail';

const dataSourceNormal = 'gl.gl_voucher.list.normal';

const dataSourceNormalQuery = 'gl.gl_voucher.list.normalquery';
const dataSourceHeightQuery = 'gl.gl_voucher.list.heightquery';
const dataSourceValue = 'gl.gl_voucher.list.normalValue';
const query_accountingbook='gl.gl_voucher.query_accountingbook';

const dataSourceHeight = 'gl.gl_voucher.list.height';

const dataSourceNumber = 'gl.gl_voucher.list.number';

const oldSource = 'gl.gl_voucher.voucher.pages';

const successStatus = 'gl.gl_voucher.voucher.saveStatus';

const dataSourceAppCode = 'gl.gl_voucher.voucher.appcode'; //生成凭appcpde
const dataSourcePageCode = 'gl.gl_voucher.voucher.pagecode'; //pagecode
const currInfoDataSource="amountConvert";

const dataSourceSaveNext= 'gl.gl_voucher.voucher.savenext';

const periodDataSource='gl.gl_voucher.voucher.periodVoucher';

const mutilangJson_list='gl_voucher.list.mutilangJson';//多语
const mutilangJson_card='gl_voucher.card.mutilangJson';//多语

const mergerequest_card='gl_voucher.card.mergerequest';

const dataSourcetableclick='gl_voucher.list.dataSourcetableclick';

const voucher_link='4';
const voucher_gen='6';

const listCacheKey='gl_voucher';
const linkToCacheTool='checkedData_gl';
const cardCacheKey='voucher_detail';

export {
	formId,
	tableId,
	searchId,
	dataSource,
	dataSourceTable,
	dataSourceCoord,
	dataSourceNormal,
	dataSourceNormalQuery,
	dataSourceHeightQuery,
	dataSourceValue,
	dataSourceHeight,
	dataSourceDetail,
	dataSourceSaveNext,
	dataSourceNumber,
	oldSource,
	successStatus,
	dataSourceAppCode,
	dataSourcePageCode,
	currInfoDataSource,
	dataSourceCheck,
    periodDataSource,
    mutilangJson_list,
	mutilangJson_card,
	mergerequest_card,
	dataSourcetableclick,
	voucher_link,
	voucher_gen,
	listCacheKey,
	cardCacheKey,
	linkToCacheTool,
	query_accountingbook
};
