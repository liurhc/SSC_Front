import React, { Component } from 'react';
import { base, toast, formDownload,getMultiLang } from 'nc-lightapp-front';
const { NCButton: Button, NCRadio: Radio,NCFormControl: FormControl,
    NCRow: Row, NCCol: Col, NCLabel: Label, NCSelect: Select,
    NCCheckbox: Checkbox, NCUpload, NCIcon:Icon, NCModal: Modal, NCForm
} = base;
const { NCFormItem: FormItem } = NCForm;

import { CheckboxItem, RadioItem, SelectItem } from '../FormItems';
import './index.less'

/**
 * item 数据格式
const assExportData = [
    { itemName: '导出方式:', itemType: 'radio', itemKey: 'importPattern', value:true,  itemChild: [{ value: 'true', label: '按名称' }, { value: 'false', label: '按编码' }] }
]
*/

const formItemLayout = { labelXs:4, labelSm:4, labelMd:4, xs:8, md:8, sm:8 };
const formItemParam = { inline:true, showMast:false, isRequire:true, method:"change" };

export default class ExportModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{}
        };
    }

	componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json
               // columns_reportSum:this.columns_reportSum,
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publiccomponents',domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentWillReceiveProps(nextProps){
        let {itemsData, data} = nextProps;
        this.setState({itemsData, data});
    }

    shouldComponentUpdate (nextProps, nextState){
        let {itemsData} = nextProps;
        if(!itemsData){
            return false;
        }
        return true;
    }

    handleCancel() {
        let { handleCancel } = this.props
        if (handleCancel) {
            handleCancel()
        }
    }

    generateRequestData = () =>{
        let requestData = {};
        let {data, itemsData} = this.state;
        if(itemsData){
            itemsData.map((item) => {
                requestData[item.itemKey] = item.value;
            });
        }
        Object.assign(requestData, data);
        return requestData;
    }

    downLoad() {
        let self = this
        let data = self.generateRequestData();
        if(data) {
            let action = self.props.url;
            formDownload({
                params:{param:JSON.stringify(data)},
                url: action,
                enctype:2
            });
        }
        let {handleDone} = this.props;
        if(handleDone){
            handleDone();
        }
    }

    
    renderRadio(item) {
        return (
            <FormItem
                {...formItemLayout}
                {...formItemParam}
                labelName={item.itemName}
            >
                <RadioItem
                    name={item.itemKey}
                    type="customer"
                    defaultValue={item.value}
                    items={() => {
                        return (item.itemChild)
                    }}
                    onChange={(v) => {
                        this.updateItemState(item.itemKey, v);
                    }}
                />
            </FormItem>
        )
    }
    renderSelect(item){

    }
    
    renderForm = () => {
        let itemsData = this.state.itemsData;
        if(!itemsData){
            return;
        }
        return (
            itemsData.map((item, i) => {
                switch (item.itemType) {
                    case 'radio':
                        return this.renderRadio(item);                      
                    case 'select':
                        return this.renderSelect(item);                    
                    default:
                        break;
                }
            })
        )
    }

    /**
     * 更新项目值
     */
    updateItemState = (itemKey, value) => {
        let page = this;
        let {itemsData} = page.state;
        itemsData.map((item) => {
            if(item.itemKey === itemKey){
                item.value = value;
            }
            return item;
        });
        page.setState({itemsData});
    }

    render() {
        let { visible } = this.props
        let modalTitle = this.state.json['publiccomponents-000050'];/* 国际化处理： 导出*/
        let iconClass = 'uf-upload';
        return (
            <div className='importModal'>
                <Modal style={{ width: '40%' }} show={visible} onHide={this.handleCancel.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{modalTitle}EXCEL</Modal.Title>
                    </Modal.Header >
                    <Modal.Body style={{ borderTop: '1px solid #ccc', borderBottom: '1px solid #ccc' }}>
                        <NCForm className='importform'
                            useRow={true}
                            submitAreaClassName='classArea'
                            showSubmit={false}　>
                            {this.renderForm()}
                        </NCForm>
                    </Modal.Body>
                    <Modal.Footer >
                        <div style={{display:'inline-flex'}}>
                            <Button shape="border" colors= 'primary' onClick={this.downLoad.bind(this)}>
                                <Icon type={iconClass} />{modalTitle}
                            </Button>
                        </div>
                        <Button onClick={this.handleCancel.bind(this)}>{this.state.json['publiccomponents-000010']}</Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div >

        )
    }
    
}
