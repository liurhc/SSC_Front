import {high,base,ajax,print,deepClone, createPage } from 'nc-lightapp-front';
import { toast } from '../components/utils.js';
// import moment from 'moment';

export let getReferCells = (self, param, callBack) => {
    let result;
    let dataParam = self.state.queryDatas || self.state.paramObj
    let url = '/nccloud/gl/accountrep/queryassitembyaccountpk.do';
    let queryData = {
        pk_accasoa:param.pk_accasoa,  //v[0].refpk, //queryDatas paramObj
        prepareddate: dataParam.versiondate,//moment().format('YYYY-MM-DD'),
        includeSub: true
        //this.state.isversiondate ? this.state.versiondate : null,//科目版本

    };
    let assData=[];
    let checkedArray=[];//选中的数据
    ajax({
        url:url,
        data:queryData,
        success: (response) => {
            const { success } = response;

            if(param.key === "relevanceAssist"){
                callBack && callBack(response, param)
            }

        },
        error: (error) => {
            toast({content: error.message, color: 'warning'});
        }
    });
}


/**
 *会计期间选择结束日期时当选择调整期时间时调用
 * self: 所在对象
 * pk_accountingbook： 账簿
 * value选当值
 *
 * */
export let getAdjustTime = (self, pk_accountingbook, value) => {//当选择当时间是调整期时间时调用
    if(!value.values.enddate.value){
        let url = '/nccloud/gl/glpub/queryDateByPeriod.do';
        let data = {
            pk_accountingbook:pk_accountingbook,
            period: value.refname
        }
        ajax({
            url,
            data,
            success: (response) => {
                let {success, data} = response;
                if(success){
                    let {rangeDate} = self.state;
                    if(value.values && Object.keys(self.state.start).length !== 0){
                        rangeDate[1] = data.enddate;
                    }else {
                        rangeDate = [];
                    }
                    self.setState({
                        enddate: data && data.enddate,
                        rangeDate: [...rangeDate]
                    })
                }
            },
            error: (error) => {
                toast({content: error.message, color: 'warning'})
            }
        });
    }
}

