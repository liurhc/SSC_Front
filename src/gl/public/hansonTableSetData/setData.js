/**
 *   Created by Liqiankun on 2018/7/23
 *   整理handsonTable的数据格式
 */
import changeToHandsonTableData from '../common/changeToHandsonTableData.js';
export let setData = (self,sourceData) => {
    let data;
    if(typeof sourceData === 'string'){
        data = JSON.parse(sourceData)
    }else{
        data = sourceData;
    }

    if (data) {
        let columnInfo=[],headtitle=[],balanceVO=[];
        if(data.data.length>0){
            self.setState({
                disabled:false
            })
        }
        if(data.column){
            columnInfo=data.column;
        }
        if(data.headtitle){
            headtitle=data.headtitle;
        }
        if(data.data){
            balanceVO=data.data;
        }
        changeToHandsonTableData(self, data, columnInfo, headtitle, balanceVO);
        
    }
}
