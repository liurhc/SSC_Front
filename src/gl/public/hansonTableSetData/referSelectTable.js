/**
 *   Created by Liqiankun on 2018/7/23
 */

import React, {Component} from 'react';
import {high,base,ajax,print,deepClone, createPage,getMultiLang } from 'nc-lightapp-front';
import { SimpleTable } from 'nc-report';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm
} = base;
//import moment from 'moment';

import {tableDefaultData} from '../../manageReport/defaultTableData';
import { setData } from './setData';
import createScript from "../components/uapRefer";


class ReferSelectTable extends Component{
    static defaultProps = {
        prefixCls: "bee-table",
        multiSelect: {
            type: "checkbox",
            param: "key"
        }
    }
    constructor(props){
        super(props);
        this.state = {
            json:{},
            checkedArray: [],
            data: [],
           // columns: this.columns10,
            checkedAll: false
        }
    }
    componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'publichansonTableSetData',domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
       this.initRowData(true)
    }
    initRowData = (param) => {
        let allRowPk = [];//初始进来默认全选
        let checkArr = [];
        let isCheckedAll = true;// 只有所有数据的checked都是true时才置为true
        this.props.data.map((item, indx) => {
            if(item.checked){
                allRowPk.push(item.pk_accassitem);
            }
            checkArr.push(item.checked);
        })
        if(param){
            checkArr.includes(false)? isCheckedAll = false : isCheckedAll = true;
        }
        this.props.getSelectRow( allRowPk )
        this.setState({
            checkedArray: checkArr,
            checkedAll: isCheckedAll
        })
    }
    onCheckboxChange = (text, record, index) => {
        let self = this;
        let allFlag = false;
        let checkedArray = self.state.checkedArray.concat();
        checkedArray[index] = !self.state.checkedArray[index];
        if(checkedArray[index]){//如果是选中的状态
            this.props.getSelectRow([this.props.data[index].pk_accassitem])
        }else{
            let itemIndx = this.props.selectRow.indexOf(this.props.data[index].pk_accassitem);
            this.props.selectRow.splice(itemIndx, 1)
            this.props.getSelectRow([])
        }
        for (var i = 0; i < this.props.data.length; i++) {
            if (!checkedArray[i]) {
                allFlag = false;
                break;
            } else {
                allFlag = true;
            }
        }
        self.setState({
            checkedAll: allFlag,
            checkedArray: checkedArray,
        });
    };
    onAllCheckChange = (key, value) => {
        this.setState({
            [key]: value
        })
        if(value){//选中全选框
            this.props.selectRow.splice(0, this.props.selectRow.length)
            this.checkAllData();
        }else{//取消全选框
            this.setState({
                checkedArray: [],
            })
            this.props.selectRow.splice(0, this.props.selectRow.length)
            this.props.getSelectRow([])
        }
    }
    checkAllData = (param) => {// 全选
        let allRowPk = [];
        let checkArr = [];
        let isCheckedAll = true;
        this.props.data.map((item, indx) => {
            allRowPk.push(item.pk_accassitem);
            checkArr.push(true);
        })
        this.props.getSelectRow( allRowPk )
        this.setState({
            checkedArray: checkArr,
            checkedAll: isCheckedAll
        })
    }
    renderColumnsMultiSelect = (columns) => {
        let that = this;
       
        const {checkedArray } = this.state;
        const { multiSelect } = this.props;
        let indeterminate_bool = false;
        if (multiSelect && multiSelect.type === "checkbox") {
            let i = checkedArray.length;
            while(i--){
                if(checkedArray[i]){
                    indeterminate_bool = true;
                    break;
                }
            }
            let defaultColumns = [
                {
                    title: (
                        <Checkbox
                            className="table-checkbox"
                            checked={that.state.checkedAll}
                            onChange={(e) => this.onAllCheckChange('checkedAll', e)}
                        />
                    ),
                    key: "checkbox",
                    attrcode: 'checkbox',
                    dataIndex: "checkbox",
                    width: "60px",
                    render: (text, record, index) => {
                        return (
                            <Checkbox
                                className="table-checkbox"
                                checked={this.state.checkedAll || this.state.checkedArray[index]}
                                onChange={this.onCheckboxChange.bind(this, text, record, index)}
                            />
                        );
                    }
                }
            ];
            columns = defaultColumns.concat(columns);
        }
        return columns;
    }

    render(){
        let {data} = this.props;

        let columns=[
            {
                title: this.state.json['publichansonTableSetData-000000'],/* 国际化处理： 辅助类型编码*/
                dataIndex: "code",
                key: "code",
                width: "30%",
                render: (text, record, index) => {
                    return <span>{record.code}</span>;
                }
            },
            {
                title: this.state.json['publichansonTableSetData-000001'],/* 国际化处理： 辅助类型名称*/
                dataIndex: "name",
                key: "name",
                render: (text, record, index) => {
                    return <span>{record.name}</span>
                }
            }
        ];
        let columnsldad = this.renderColumnsMultiSelect(columns);
        const emptyFunc = () => <span>{this.state.json['publichansonTableSetData-000002']}！</span>/* 国际化处理： 这里没有数据*/

        return (
            <div>
                <Table
                    columns={columnsldad}
                    data={data}
                    emptyText={emptyFunc}
                />
            </div>
        )
    }
}

export default ReferSelectTable


