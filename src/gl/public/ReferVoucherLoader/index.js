import { Component } from 'react';
import createScript from './uapRefer';
import './index.less';

/**
 * 参照加载组件
 */
export default class ReferLoader extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	componentWillReceiveProps(nextProps) {}

	render() {
		let props = {
			isMultiSelectedEnable: false
		};
		Object.assign(props, this.props);
		let state = this.state;
		let refer = null;
		if (!state[props.tag]) {
			createScript.call(this, props.refcode, props.tag);
		} else {
			refer = state[props.tag] ? state[props.tag](props) : <div />;
		}
		return (
			<div className="refer-wrapper">
				{props.showStar ? <span className="required-star">*</span> : null}
				{refer}
			</div>
		);
	}
}
