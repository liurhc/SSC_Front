/**
 * add by qinhaof
 * 判断数据类型
 */
function isArray(obj){
    return isSomeOne(obj, '[object Array]');
}

function isFunction(obj){
    return isSomeOne(obj, '[object Function]');
}

function isSomeOne(obj, typeStr){
    return Object.prototype.toString.call(obj) === typeStr;
}

const ObjTypeUtil = {
    isArray, isFunction
}
export default ObjTypeUtil;