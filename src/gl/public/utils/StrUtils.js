/**
 * add by qinhaof
 * 字符串处理工具类 
 */


/**
 * 获取字符串的字节数
 * 与数据库中保持一致汉字统一按两个字节处理
 * @param {*} str 
 */
function getStrByteNum(str){
    if(typeof str != 'string' || str.constructor != String)
        return 0;
    let length = str.length;
    let byteNum = 0;
    for(let i = 0; i < length; i++){
        byteNum += getByteNumByCode(str.charCodeAt(i));
    }1
    return byteNum;
}

/**
 * 获取字符的字节数
 * 根据code判断是否汉字
 * 不严谨
 * @param {*} charCode 
 */
function getByteNumByCode(charCode){
    if(charCode > 255)
        return 2;
    return 1;
}

/**
 * 根据最大字节数截取字符串
 * @param {*} str 
 * @param {*} byteLength 
 */
function subStrByByteLength(str, byteLength){
    let currByteNum = getStrByteNum(str);
    if(currByteNum > byteLength){
        let needTrim = Math.ceil((currByteNum - byteLength)/2.0);
        let subIndex = str.length - needTrim;
        return subStrByByteLength(str.substring(0, subIndex));
    }else{
        return str;
    }
}

const StrUtils = {
    getStrByteNum,
    getByteNumByCode,
    subStrByByteLength
};

export {StrUtils};