import React, { Component } from 'react';
import { SimpleReport } from 'nc-report';
import { ajax, pageTo, getBusinessInfo,createPage,base,viewModel,toast } from 'nc-lightapp-front';
// let{setGlobalStorage}=viewModel;
const{NCButton}=base;
const { getUrlParam } = pageTo;
const config = {
    "isDataPowerEnable": 'Y',
    "DataPowerOperationCode": 'fi'
};
class Report extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageProps:'',
            pk_report: '',
            templetid: '',
            isShowUnit: false,
            pk_accountingbook: '',//核算账簿
            pk_accperiodscheme: '',//会计期间方案
            versiondate:'',//科目版本
            startPeriod:'',
            bizPeriod:'',
            searchId:'',
            defcondition:{//设置查询条件
                pk_accountingbook:{},
                period:[],
                periodv:[],
                includeuntally:{"display":'是',"value":'Y'},
                includeerror:{"display":'是',"value":'Y'}
            },
            isshow:false,//机器人打开账表先请求数据在渲染
            unitName: '' // 组织名称
        };
        this.disposeSearch = this.disposeSearch.bind(this)
        this.onAfterEvent = this.onAfterEvent.bind(this)
        this.setDefaultVal=this.setDefaultVal.bind(this)
    }

    componentWillMount () {

    }

    componentDidMount() {
        let{defcondition,unitName}=this.state;
        if (!getUrlParam.call(this, 'templetid')) {
            //非templetid为小应用，有templetid为点击预览过来的
            ajax({
                url: '/nccloud/nccloud/report/queryappparamaction.do',
                data: {},
                success: (res) => {
                    this.setState({
                        pk_report: res.data.pk_report,
                        templetid: res.data.templetid,

                    })
                }
            })
        }
        let robotParam=this.props.getSearchParam('param');
        if(robotParam){
            let josnParam=JSON.parse(robotParam);
            let pk_org=josnParam.companychoose .pk_org;
            unitName=josnParam.companychoose .name;
            let accountingPeriod=josnParam.accountingPeriod;
            let periodStart=accountingPeriod.split('-')[0];
            periodStart=periodStart.substring(0,4)+'-'+periodStart.substring(4);
            let periodEnd=accountingPeriod.split('-')[1];
            periodEnd=periodEnd.substring(0,4)+'-'+periodEnd.substring(4);
            this.queryAccountingbook(pk_org).then((res)=>{
                let condition={
                    'logic':'and',
                    'conditions':[{
                        "field": "pk_accountingbook",
                        "value": {
                            "firstvalue": res.refpk,
                            "secondvalue": ""
                        },
                        "oprtype": "=",
                        "display": res.refname,
                        "isIncludeSub": false,
                        "refurl": "",
                        "datatype": "204",
                        "timeFunction": ""
                    }, {
                        "field": "period",
                        "value": {
                            "firstvalue": periodStart,
                            "secondvalue": periodEnd
                        },
                        "oprtype": "between",
                        "display": `${periodStart},${periodEnd}`,
                        "isIncludeSub": false,
                        "refurl": "",
                        "datatype": "5",
                        "timeFunction": ""
                    }, {
                        "field": "includeuntally",
                        "value": {
                            "firstvalue": "Y",
                            "secondvalue": ""
                        },
                        "oprtype": "=",
                        "display": "是",
                        "isIncludeSub": false,
                        "refurl": "",
                        "datatype": "32",
                        "timeFunction": ""
                    }, {
                        "field": "includeerror",
                        "value": {
                            "firstvalue": "Y",
                            "secondvalue": ""
                        },
                        "oprtype": "=",
                        "display": "是",
                        "isIncludeSub": false,
                        "refurl": "",
                        "datatype": "32",
                        "timeFunction": ""
                    }, {
                        "field": "not_search",
                        "value": {
                            "firstvalue": "",
                            "secondvalue": null
                        }
                    }]
                }
                let userObj={
                    "pk_accountingbook":res.refpk,
                    // "period":[periodStart,periodEnd],
                    // "periodv":[periodStart,periodEnd],
                    "period_start":periodStart,
                    "period_end":periodEnd,
                    'includeuntally':'Y',
                    'includeerror':'Y',
                    'includerc':'N'
                };
                let userDefObj = {};
                userDefObj.keyReportParamWeb = userObj;
                // sessionStorage.setItem('LinkReport', JSON.stringify(condition));
                // sessionStorage.setItem('LinkReportUserDefObj', JSON.stringify(userDefObj));
                setGlobalStorage('sessionStorage','LinkReport',JSON.stringify(condition));
                setGlobalStorage('sessionStorage','LinkReportUserDefObj',JSON.stringify(userDefObj));
                defcondition.pk_accountingbook={display:res.refname,value:res.refpk};
                defcondition.period=[periodStart,periodEnd];
                defcondition.periodv=[periodStart,periodEnd];
                this.setState({
                    isshow:true,
                    defcondition,
                    unitName
                });
             })
        }
    }
    //根据组织请求账簿
    queryAccountingbook = (pk_org) => {
        return new Promise((resolve) => {
            ajax({
                url: '/nccloud/gl/pub/qryAccbookByOrg.do',
                data: { 'pk_org': pk_org},
                success: (res) => {
                    resolve(res.data);
                }
            });
        });
    };
    isRenderFromDesign = () => {
        //自定义渲染初始化表格区
        const { pk_report } = this.state;
        let appcode=this.props.getSearchParam("c");
        return new Promise((resolve) => {
            ajax({
                url: '/nccloud/report/widget/lightreport_query.do',
                data: { pk_report: getUrlParam.call(this, 'pk_report') || pk_report, appcode: appcode },
                success: (res) => {
                    resolve(res);
                }
            });
        });
    };
    disposeSearch(meta, props) {
        // 参照过滤
        let businessInfo = getBusinessInfo();
        let currrentDate = businessInfo.businessDate.split(' ')[0];
        let items = meta['light_report'].items;
        let appcode=this.props.getSearchParam("c");
        let verifyrange=false;
        for(let i=0;i<items.length;i++){
            if(items[i].attrcode=='verifyrange'){
                verifyrange=true;
                break;
            }
        }
        items.forEach((item) => {
            if (item.itemtype == "refer") {
                if (item.attrcode == 'pk_accountingbook') {//账簿
                    item.queryCondition = () => {
                        return Object.assign({
                            "TreeRefActionExt":"nccloud.web.gl.ref.AccountBookRefSqlBuilder",
                            "appcode":appcode,
                        }, config)
                    };
                } else if (item.attrcode == 'period' || item.attrcode == 'periodv') {//会计期间
                    item.queryCondition = () => {
                        return {
                            "includeAdj": "Y",//需要显示调整期
                            "pk_accperiodscheme": this.state.pk_accperiodscheme
                        }
                    };
                } else if (item.attrcode == 'pk_accasoa'&&verifyrange) {//核销科目
                    item.queryCondition = () => {
                        let versiondate=props.search.getSearchValByField( this.state.searchId, 'versiondate' );
                        return Object.assign({
                            // "refName":'核销科目',
                            "pk_accountingbook": this.state.pk_accountingbook,
                            "dateStr": versiondate.display?this.state.versiondate:currrentDate,
                            "TreeRefActionExt": 'nccloud.web.gl.verify.action.VerifyObjectRefSqlBuilder'

                        }, config)
                    };
                } else if (item.attrcode == 'pk_accasoa') {//科目
                    item.queryCondition = () => {
                        let versiondate=props.search.getSearchValByField( this.state.searchId, 'versiondate' );
                        return Object.assign({
                            isMultiSelectedEnabled: true,
                            isShowHighFilter: false,
                            "pk_accountingbook": this.state.pk_accountingbook,
                            "dateStr": versiondate.display?this.state.versiondate:currrentDate,
                            versiondate:this.state.versiondate,

                        }, config)
                    };
                } else if (item.attrcode == 'oppositesubj') {//对方科目
                    item.queryCondition = () => {
                        let versiondate=props.search.getSearchValByField( this.state.searchId, 'versiondate' );
                        return Object.assign({
                            isMultiSelectedEnabled: true,
                            isShowHighFilter: false,
                            "pk_accountingbook": this.state.pk_accountingbook,
                            "dateStr": versiondate.display?this.state.versiondate:currrentDate,
                            versiondate:this.state.versiondate

                        }, config)
                    };
                } else if (item.attrcode == 'pk_unit') {//业务单元
                    item.queryCondition = () => {
                        return Object.assign({
                            "VersionStartDate": currrentDate,
                            "pk_accountingbook": this.state.pk_accountingbook,
                            "TreeRefActionExt": 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                        }, config)
                    };
                } else if (item.attrcode == 'pk_casher' || item.attrcode == 'pk_casherv') {//出纳人
                    item.queryCondition = () => {
                        return Object.assign({
                            "pk_accountingbook": this.state.pk_accountingbook,
                            "user_Type": '2',
                            "GridRefActionExt": 'nccloud.web.gl.ref.OperatorRefSqlBuilder'
                        }, config)
                    };
                } else if (item.attrcode == 'pk_checked' || item.attrcode == 'pk_checkedv') {//审核人
                    item.queryCondition = () => {
                        return Object.assign({
                            "pk_accountingbook": this.state.pk_accountingbook,
                            "user_Type": '1',
                            "GridRefActionExt": 'nccloud.web.gl.ref.OperatorRefSqlBuilder'
                        }, config)
                    };
                } else if (item.attrcode == 'pk_manager' || item.attrcode == 'pk_managerv') {//记账人
                    item.queryCondition = () => {
                        return Object.assign({
                            "pk_accountingbook": this.state.pk_accountingbook,
                            "user_Type": '3',
                            "GridRefActionExt": 'nccloud.web.gl.ref.OperatorRefSqlBuilder'
                        }, config)
                    };
                } else if (item.attrcode == 'pk_prepared' || item.attrcode == 'pk_preparedv') {//制单人
                    item.queryCondition = () => {
                        return Object.assign({
                            "pk_accountingbook": this.state.pk_accountingbook,
                            "user_Type": '0',
                            "GridRefActionExt": 'nccloud.web.gl.ref.OperatorRefSqlBuilder'
                        }, config)
                    };
                }else if (item.attrcode == 'pk_vouchertype' || item.attrcode == 'pk_vouchertypev') {//凭证类别
                    item.queryCondition = () => {
                        return Object.assign({
                            "pk_org": this.state.pk_accountingbook
                        }, config)
                    };
                } else {
                    item.queryCondition = () => {
                        return Object.assign({
                            "pk_accountingbook": this.state.pk_accountingbook,
                            "pk_org": this.state.pk_org
                        }, config)
                    }
                }
                if(item.refcode=='uapbd/refer/org/DeptNCTreeRef/index'||item.refcode=='uapbd/refer/psninfo/PsndocTreeGridRef/index'){//部门人员
                    item.isShowUnit= true;
                    item.unitValueIsNeeded = false;
                    item.unitCondition=()=> {
                        return({
                        pk_financeorg: this.state.pk_org,
                        TreeRefActionExt: 'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                        })
                    }
                    item.queryCondition = () => {
                        return Object.assign({
                            "busifuncode": "all",
                            "pk_org": this.state.pk_org
                        },config)
                      
                    }
                }
            }
        });
        return meta; // 处理后的过滤参照返回给查询区模板
    }
    /**
         * props: props
         * searchId: 查询区需要的searchId参数
         * field: 编辑后的key
         * val: 编辑后的value
        */
    onAfterEvent(props, searchId, field, val) {
        //查询区编辑后事件
        if (field == "pk_accountingbook") {
            if (val && val[0]) {
                this.queryBookCombineInfo(val,props,searchId);
            }
        }else if(field == "versiondate"){
            props.search.setSearchValByField(searchId,'pk_accasoa',{"display":'',"value":''});
            props.search.setSearchValByField(searchId,'oppositesubj',{"display":'',"value":''});
            this.setState({
                versiondate:val
            })
        }else if(field=="periodv"||field=="period"){
            let periodvValue=props.search.getSearchValByField(searchId,field);
            let firstvalue=periodvValue.value&&periodvValue.value.firstvalue;
            let secondvalue=periodvValue.value&&periodvValue.value.secondvalue;
            
            if(val.refname==firstvalue&&firstvalue>secondvalue){
                props.search.setSearchValByField(searchId,field,{"display":[firstvalue,firstvalue],"value":[firstvalue,firstvalue]});
            }
            if(val.refname==secondvalue&&firstvalue>secondvalue){
                props.search.setSearchValByField(searchId,field,{"display":[secondvalue,secondvalue],"value":[secondvalue,secondvalue]});
            }
        }
    }
    //请求账簿信息
    /**
         * props: props
         * searchId: 查询区需要的searchId参数
         * status: 区分状态， init：初始化设置默认值
         * val: 编辑后的value
        */
    queryBookCombineInfo = (val,props,searchId,status) => {
        let pk_accountingbook='';
        if(val&&val.length>0){
            pk_accountingbook=val[0].refpk||val[0].value;
        }else{
            pk_accountingbook=val.value;
        }
        let that = this;
        let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
        let pk_accpont = { "pk_accountingbook": pk_accountingbook }
        ajax({
            url: url,
            data: pk_accpont,
            success: function (response) {
                const { success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if (response.data) {
                        let isShowUnit = response.data.isShowUnit;
                        let pk_org = response.data.unit.value;
                        let unitName = response.data.unit.display;
                        let pk_accperiodscheme = response.data.pk_accperiodscheme;
                        let bizPeriod = response.data.bizPeriod;
                        let startPeriod = response.data.startPeriod;
                        let versiondate='';
                        that.linkchange(props,searchId,response.data,status);//与账簿相关的联动
                        if(response.data.versiondate){
                            versiondate=response.data.versiondate[response.data.versiondate.length-1];
                            let versiondateArr=[];
                            response.data.versiondate.map((item,index)=>{
                                let obj={display:item,value:item};
                                versiondateArr.push(obj);
                            })
                            //给科目版本设置默认值 "versiondate"
                            props.search.setSearchValByField(searchId,'versiondate',{"display":versiondate,"value":versiondate});
                            props.search.setTemlateByField(searchId,"versiondate","options",versiondateArr);
                        }
                        props.search.setSearchValByField(searchId,'period',{"display":[bizPeriod,bizPeriod],"value":[bizPeriod,bizPeriod]});
                        props.search.setSearchValByField(searchId,'periodv',{"display":[bizPeriod,bizPeriod],"value":[bizPeriod,bizPeriod]});
                        that.setState({
                            pk_accountingbook: pk_accountingbook,
                            isShowUnit: isShowUnit,
                            pk_org: pk_org,
                            pk_accperiodscheme: pk_accperiodscheme,
                            versiondate:versiondate,
                            startPeriod:startPeriod,
                            bizPeriod:bizPeriod,
                            searchId:searchId,
                            pageProps:props,
                            unitName: unitName
                        }
                        // ,()=>{
                        //     that.linkchange(props,searchId,response.data)
                        // }
                    )
                        
                    }
                }
            }
        })
    }
    //与账簿相关的联动
    linkchange=(props,searchId,data,status)=>{
        let meta= props.meta.getMeta();
               if(meta){
                    meta.light_report.items.map((item,index)=>{
                        if(item.itemtype=='refer'&&item.attrcode!='pk_accountingbook'){
                            if(!status){//初始化的时候不需要清空
                                props.search.setSearchValByField(searchId,item.attrcode,{"display":'',"value":''});
                            }
                            
                        }else if(item.itemtype=='datepicker'){
                            let beginDate=data.begindate+' 00:00:00';
                            let endDate=data.enddate+' 23:59:59';
                            props.search.setSearchValByField(searchId,item.attrcode,{"value":`${beginDate},${endDate}`,"display":`${beginDate},${endDate}`});
                        }
                    })
               }
    }
    /**设置默认值**/
    setDefaultVal=(searchId, props,flag,templedata)=>{
        let {light_report}=templedata.data;
        if(light_report.items&&light_report.items.length>0){
            light_report.items.map((item,index)=>{
                if(item.attrcode=="pk_accountingbook"&&item.initialvalue&&item.initialvalue.value){
                    let val=item.initialvalue;
                    this.queryBookCombineInfo(val,props,searchId,'init');
                }else{
                    this.setState({
                        searchId:searchId,
                        pageProps:props
                    })
                }
            })
        }else{
            this.setState({
                searchId:searchId,
                pageProps:props
            })
        }
        let robotParam=this.props.getSearchParam('param');
        let {defcondition}=this.state;
        if(robotParam){
            props.search.setSearchValByField(searchId,'period',{"display":defcondition.period,"value":defcondition.period});
            props.search.setSearchValByField(searchId,'periodv',{"display":defcondition.periodv,"value":defcondition.periodv});
           props.search.setSearchValByField(searchId,'includeuntally',defcondition.includeuntally);
           props.search.setSearchValByField(searchId,'includeerror',defcondition.includeerror);
           props.search.setSearchValByField(searchId,'pk_accountingbook',defcondition.pk_accountingbook);

        }
    }
    save=(data)=>{
        console.log('data',data);
    }
    CreateNewSearchArea (isRange, data, coords) {
        // button区域业务端自定义的新增按钮
        return (
        <div>
            <NCButton className="btn" shape="border" onClick={this.save.bind(this,data)}>保存</NCButton>
            <NCButton className="btn" shape="border" onClick={this.doSyncReportData.bind(this, data)}>同步</NCButton>
        </div>
        )
      }
     /**
     * 同步报表数据给小友
     */
    doSyncReportData = (data) => {
        let { unitName,searchId,pageProps } = this.state;
        let endPeriod = pageProps.search.getSearchValByField(searchId, 'period')//'2019-06';// 取结束期间yyyy-MM
        if (endPeriod.value && endPeriod.value.secondvalue) {
            endPeriod = endPeriod.value.secondvalue;
        } else {
            toast({ color: 'success' });
            return;
        }
        if (unitName == '' || endPeriod == undefined || endPeriod == '') {
            toast({ color: 'success' });
            return;
        }
        let year = endPeriod.split("-")[0];
        let period = endPeriod.split("-")[1];
        if (period != '06' && period != '12') {// 只有半年报/年报才发接口
            toast({ color: 'success' });
            return;
        }
        let url = '/nccloud/gl/pub/syncreportdata.do';
        let reportdata = {};
        reportdata.company = unitName;// 组织名称
        reportdata.year = year;// 年份
        reportdata.reportType = period == '06' ? 'semiannual' : 'annual';// 半年报/年报
        reportdata.cellsArray = JSON.stringify(data);// 报表数据
        ajax({
            url,
            data : reportdata,
            success: function (res) {
                const { data, error, success } = res;
                if (success) {
                    toast({ color: 'success' });
                } else {
                    toast({ content: error.message, color: 'warning' });
                }
            },
            error: function (res) {
                toast({ content: res.message, color: 'warning' });
            }
        });
    }
    render() {
        if (!getUrlParam.call(this, 'templetid') && !this.state.templetid) return null;
        let appcode=this.props.getSearchParam("c");
        let pagecode=this.props.getSearchParam("p");
        let isSearchFromDesign = {
            templetid: getUrlParam.call(this, 'templetid') || this.state.templetid,
            appcode: appcode,
            pagecode: pagecode
        };
        let paramFlag=this.props.getSearchParam('param');//机器人打开报表有这个参数
        //自定义查询区参数

        return (
            <div>
                {this.state.isshow||!paramFlag?
                <SimpleReport
                    CreateNewSearchArea={this.CreateNewSearchArea.bind(this)}
                    setDefaultVal={this.setDefaultVal}
                    isRenderFromDesign={this.isRenderFromDesign}
                    isSearchFromDesign={isSearchFromDesign}
                    disposeSearch={this.disposeSearch}
                    onAfterEvent={this.onAfterEvent}

                />
                :<div/>
                }
            </div>
        );
    }
}
Report=createPage({})(Report);
ReactDOM.render(<Report />, document.querySelector('#app'));
