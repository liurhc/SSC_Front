import React, { Component } from 'react';
import { base } from 'nc-lightapp-front';
const { NCTree } = base;
const NCTreeNode = NCTree.NCTreeNode;

export default class ModalTree extends Component {
	constructor(props) {
		super(props);
		this.state = {
			checkedKeys:[]
		};
	}
	componentWillReceiveProps(nextProps) {
		this.setState(
		 {
		  checkedKeys: []
		 },
		 () => this.props.getSelectedDataFunc([])
		);
	   }
	onCheck = (keys, nodes) => {
		let data = [];
		let checkedNodes = nodes.checkedNodes;
		checkedNodes.length > 0 && checkedNodes.map(item => {
			if (item.props.ext.children) delete item.props.ext.children
			data.push(item.props.ext)
		})
		this.setState({
			checkedKeys:keys
		})
		if (data.length > 0) this.props.getSelectedDataFunc(data)
	}
	buildTree = (data, id, pid) => {
		// 平铺转树
		var treeList = data.slice(0);
		var afun = function (ys, json) {
			var len = json.length;
			while (len > 0) {
				len--;
				let oo = json[len];
				if (ys[id] == oo[pid]) {
					ys.children = ys.children || [];
					ys.children.push(oo);
					json.splice(len, 1);
				}
			}
		};
		data.forEach(function (value) {
			afun(value, treeList);
		});
		return treeList;
	};
	render() {
		let { data } = this.props;
		let treeData = [];
		if (data && data.length > 0) treeData = JSON.parse(JSON.stringify(data));
		const loop = (data) =>
			data.map((item, index) => {
				if (item.children) {
					return (
						<NCTreeNode
							key={item.refpk}
							title={item.refname}
							ext={item}
						>
							{loop(item.children)}
						</NCTreeNode>
					);
				}

				return (
					<NCTreeNode
						key={item.refpk}
						title={item.refname}
						ext={item}
					/>
				);
			});
		return (
			<div className="design-search-tree">
				{data && data.length > 0 ? (
					<NCTree
						checkable
						checkedKeys={this.state.checkedKeys}
						onCheck={this.onCheck}
					>
						{loop(treeData)}
					</NCTree>
				) : null}
			</div>
		)
	}
}