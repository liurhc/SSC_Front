import React, { Component } from "react";
import ReactDOM from "react-dom";
import { base, high, ajax, getBusinessInfo } from 'nc-lightapp-front';
const { Refer } = high;
const { ReferLoader } = Refer;
const { NCButton, NCRow, NCCol, NCFormControl } = base;
import SearchList from './components/modalTree.js'

export default class Dimension extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pk_setofbook: '',
            pk_org: '',
            data: {},
            inputValue: ''
        }
    }

    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {
    }
    //参照的onchange事件 
    referOnchange = (record, inputValue) => {
        let $this = this;
        let businessInfo = getBusinessInfo();
        let currrentDate = businessInfo.businessDate.split(' ')[0];
        let script = document.createElement("script");
        script.type = "text/javascript";
        script.src = `../../../../${record.refpath}.js`;
        document.getElementById('app').appendChild(script);
        script.onload = function () {
            let refer = window[record.refpath].default();
            let url = refer.props.refType === 'gridTree' ? refer.props.queryTreeUrl : refer.props.queryGridUrl;
            // 参数：
            let json = {
                pid: '',
                pageInfo: {
                    pageSize: 50,
                    pageIndex: -1
                },
                keyword: inputValue ? inputValue : '',
                defineItems: [],
                queryCondition: {
                    pk_setofbook: $this.state.pk_setofbook.refpk,
                    pk_org: $this.state.pk_org.refpk,
                    datestr: currrentDate,
                    fiba: true,
                }
            }
            ajax({
                url: "/nccloud/nccloud/report/metadataqueryaction.do",
                data: {
                    domain: record.domain,
                    moduleid: record.moduleid,
                    specialdim: record.specialdim,
                    classid: record.classid,
                    refpath: record.refpath,
                    url,
                    json: JSON.stringify(json)
                },
                success: (res) => {
                    $this.setState({
                        data: res.data && res.data.rows && res.data.rows.treeData ? res.data.rows.treeData : []
                    });

                }
            })
        }
    }
    onChange = (v,record) => {
        this.setState({
            inputValue: v
        },()=>{
            if(!v){
                this.referOnchange(record, v)
            }
        })
    }
    onSearch = (record) => {
        let { inputValue } = this.state;
        this.referOnchange(record, inputValue)
    }
    render() {
        const { defaultTreeData, TreeDOM, refreshDimTreeData, record, getSelectedDataFunc, getSelectTreeNode, onMoveNode, isAddFlagPK, onFormulaIconClick } = this.props;
        /**
         * defaultTreeData: 树数据
         * TreeDOM: 右侧的dom树组件
         * refreshDimTreeData: 数组件的新增改的更新方法，必传，参数传 'busi' 标识是领域的代码
         * record: 当前点击的标签
         * getSelectedDataFunc: 公共代码获取选中的表数据，参数类型：数组，必传
         * getSelectTreeNode: 公共代码获取点击的树节点，参数类型：对象，必传
         * onMoveNode: 点击穿梭按钮，必传，参数传 'busi' 标识是领域的代码
         * isAddFlagPK: 新增维度树标识，绑定到树组件即可
         */

        return [
                <div className="design-report-modal-refer">
                    {/* //集团的，组织的是财务组织+账簿类型 */}
                    <NCRow>
                        <NCCol xs={2} md={2} sm={2}>
                            <label>业务单元</label>
                        </NCCol>
                        <NCCol xs={4} md={4} sm={4}>
                            <ReferLoader
                                refcode={"uapbd/refer/org/FinanceOrgWithGroupTreeRef/index"}
                                value={this.state.pk_org}
                                onChange={(value) => {
                                    this.setState({
                                        'pk_org': value
                                    }, () => {
                                        if (this.state.pk_setofbook) {
                                            this.referOnchange(record);
                                        }

                                    })
                                }}

                            />
                        </NCCol>
                        <NCCol xs={2} md={2} sm={2}>
                            <label>账簿类型</label>
                        </NCCol>
                        <NCCol xs={4} md={4} sm={4}>
                            <ReferLoader
                                refcode={"uapbd/refer/org/SetOfBookGridRef/index"}
                                value={this.state.pk_setofbook}
                                onChange={(value) => {
                                    this.setState({
                                        'pk_setofbook': value
                                    }, () => {
                                        if (this.state.pk_org) {
                                            this.referOnchange(record);
                                        }

                                    })

                                }}
                            //集团的，组织的是财务组织+账簿类型
                            />
                        </NCCol>
                    </NCRow>
                </div>,
                <div className="design-report-modal-main">
                    <div className="modal-left">
                        <div className="modal-left-header">
                            待选项<span class="sub-title">（双击选择）</span>
                        </div>
                        <div className="search-section">
                        <NCFormControl
                                placeholder="搜索"
                                type="search"
                                value={this.state.inputValue}
                                onSearch={this.onSearch.bind(this, record)}
                                onChange={(value)=>{this.onChange(value,record)}}
                            />
                        </div>
                        <div className="data-section">
                        <SearchList data={this.state.data} getSelectedDataFunc={getSelectedDataFunc} />
                        </div>
                    </div>
                    <div className="modal-center">
                        <span onClick={onMoveNode.bind(this, 'busi')} className="right-button icon iconfont icon-jiantouyou" />
                    </div>
                    <div className="modal-right">
                        <div className="modal-right-header">
                            <div
                                style={{
                                    flex: 'auto'
                                }}
                            >
                                公式
                            </div>
                            <div className="design-mark">
                                <span className="mark-real">
                                    实成员 <i />
                                </span>
                                <span>
                                    虚成员 <i />
                                </span>
                            </div>
                        </div>
                        {<TreeDOM isAddFlagPK={isAddFlagPK}
                            treeData={defaultTreeData}
                            getSelectTreeNode={getSelectTreeNode}
                            refreshDimTreeData={refreshDimTreeData.bind(this, 'busi')}
                            onFormulaIconClick={onFormulaIconClick}
                        >
                        </TreeDOM>}
                    </div>
                </div>

        ]
    }
}