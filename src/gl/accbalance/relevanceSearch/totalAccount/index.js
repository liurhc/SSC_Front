/**
 *   Created by Liqiankun on 2018/7/23
 */

import React, {Component} from 'react';
import {high,base,ajax,print,deepClone, createPage } from 'nc-lightapp-front';
import { SimpleTable } from 'nc-report';

import {tableDefaultData} from '../../../manageReport/defaultTableData';
import { setData } from '../../../public/hansonTableSetData/setData';


class RelevanceTotal extends Component{
    constructor(props){
        super(props);
        this.state = {
            dataout:tableDefaultData,
            showTable: false
        }
    }
    componentDidMount(){
        this.getParam();
    }
    getParam = () => {
        let data = this.props.getUrlParam('status');
        setData(this, data)
    }

    render(){
        return (
            <div>
                <SimpleTable
                    ref="balanceTable"
                    data = {this.state.dataout}
                />
            </div>
        )
    }
}


RelevanceTotal = createPage({})(RelevanceTotal)
ReactDOM.render(
    <RelevanceTotal   />,
    document.querySelector("#app")
);
