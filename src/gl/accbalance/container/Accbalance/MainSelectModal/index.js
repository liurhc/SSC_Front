/**
 * 科目余额表
 * **/
import React, { Component } from 'react';

import { base,ajax,deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import './index.less';
import  '../../../../public/reportcss/searchmodalpage.less'

const { NCRadio:Radio, NCRow:Row, NCCol:Col,NCSelect:Select, NCCheckbox:Checkbox} = base;
import SubjectVersion from '../../../../manageReport/common/modules/subjectVersion'
import { toast } from '../../../../public/components/utils';

const NCOption = Select.NCOption;
import BusinessUnitTreeRef from '../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';
import {
    businessUnit,
    createReferFn, getCheckContent, getReferDetault,
    renderLevelOptions, renderMoneyType,
    renderRefer, returnMoneyType
} from "../../../../manageReport/common/modules/modules";
import DateGroup from '../../../../manageReport/common/modules/dateGroup'
import SubjectLevel from '../../../../manageReport/common/modules/subjectAndLevel'
import {clearTransterData, handleChange, handleSelectChange} from '../../../../manageReport/common/modules/transferFn'
import CheckBoxCells from '../../../../manageReport/common/modules/checkBox';
import FourCheckBox from '../../../../manageReport/common/modules/fourCheckBox';
import initTemplate from '../../../../manageReport/modalFiles/initTemplate';
import { FICheckbox } from '../../../../public/components/base';

class CentralConstructorModal extends Component {
	constructor(props) {
		super(props);
		this.state={
			appcode: "20023005",
			json: {},
            isqueryuntallyed: true, //是否禁止设置查询包含未记账凭证;true: 禁止
            selectedKeys: [],
            targetKeys: [],

            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
			changeParam: true, //false:点击条件时不触发接口，,
            rightSelectItem: [], //科目的"条件"按钮中存放右侧数据的pk值
            showRadio: false,//控制是否显示"会计期间"的但选框；false: 不显示

            start: {},  //开始期间
            end: {}, //结束期间
            /*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
            startyear: '', //开始年
            startperiod: '',  //开始期间
            endyear: '',  //结束年
            endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示


			accountingbook: [],
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: {        //二级单元
				refname:'',
				refpk: '',
			},
			versiondateList: [], //科目版本列表
			accountlvl: '1', //设置级次1到几
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表
			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次
			startcode: {
				refname:'',
				refpk: '',
				refcode: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
				refcode: '',
			},    //结束科目编码
			versiondate: '', //制单日期，后续要用下拉选
			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			/**多主体显示方式*/
			mutibook: 'N', //多主体显示方式的选择值
			disMutibook: true, //多主体显示是否禁用
			/**排序方式*/
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			/**1、无发生不显示*/
            showzerooccurValue: false,//无发生不显示的选择状态

			showzerooccur: false, //控制 无发生不显示 无余额无发生不显示 是否可编辑
			/**2、无余额无发生不显示*/
            showzerobalanceoccurValue: true, //无余额无发生不显示选择的状态
			showzerobalanceoccur: false, //控制'无余额无发生不显示'是否可编辑

			/**3、显示科目类型小计*/
			sumbysubjtype: false, //显示科目类型小计
            editSubjectTotal: false, //'显示科目类型小计'是否可编辑

			/**按借贷方显示余额*/
			twowaybalance: false, //借贷方显示余额
            debitAndCredit: false,//按借贷显示余额是否禁止

			/**多业务单元合并*/
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框

			isversiondate: false,
			pk_accperiodscheme: '', //会计期间参照用的过滤

			
		}
        this.renderRefer = renderRefer.bind(this);
		this.searchId = '20023005query';
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
				json:json,
                currtype: json['20023005-000000'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['20023005-000000'],  //选择的币种名称/* 国际化处理： 本币*/
			})
        }
        getMultiLang({moduleId:['20023005', 'dategroup', 'checkbox', 'fourcheckbox', 'subjectandlevel', 'subjectversion', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
    }

	componentDidMount() {
        setTimeout(() => getReferDetault(this, true, {
            businessUnit
        }, 'accbalance'),0)
        setTimeout(() => initTemplate.call(this,this.props), 0)
	}

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}
	shouldComponentUpdate(nextProps, nextState){
        return true;
	}
	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
		if(value === 'N'){
			this.setState({
				sumbysubjtype: false,
				editSubjectTotal: true,
				showzerooccurValue: false,
				showzerooccur:true,
				showzerobalanceoccurValue: true,
                showzerobalanceoccurEdit:true
			})
		}else{
            this.setState({
                editSubjectTotal: false,
                showzerooccur:false,
                showzerobalanceoccurEdit:false
            })
		}
	}
	

	getlvlver() {
		let self = this;
		let url = '/nccloud/gl/glpub/accountinfoquery.do';

		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	// 默认取科目版本列表最后一个
		        const { data, error, success } = res;

		        if(success){
		        	
					self.setState({
						accountlvl: data.accountlvl,
						versiondateList: data.versiondate,
						versiondate: data.versiondate[data.versiondate.length - 1],
						pk_accperiodscheme: data.pk_accperiodscheme,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});


	}






	getCurrtypeList = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {
			"localType":"1",
			"showAllCurr":"1",
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
						// currtype: this.state.json['20023005-000000'],/* 国际化处理： 本币*/
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}
    handleCurrtypeChange = (keyParam, value) => {
        this.setState({
            [keyParam]: value,
        });
    }

	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(type) {
		if (!this.state.accountingbook || !this.state.accountingbook[0]) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}

		this.getlvlver();
		
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	

		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        	},)
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({
	    	includeuntally: e,
	    	includeerror: e,
	    });
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({
			mutibook: e,
		});
		if(e === 'Y'){
			this.setState({
				showzerooccurValue: false,
				showzerooccur: true,
				twowaybalance: false,
                debitAndCredit: true
			})
		}else{
            this.setState({
                showzerooccur: false,
                debitAndCredit: false
            })
		}
	}

	//无发生不显示复选框变化
	onChangeShowzerooccur(e) {
		this.setState({showzerooccurValue: e});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		this.setState({
			multbusi: e,
			currplusacc : 'Y',
			disCurrplusacc: !e,
			sumbysubjtype: false,
			editSubjectTotal: !e,
		});
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}

    handleValueChange = (key, value, param) => {
        this.setState({
            [key]: value
        });
        if(param === 'showzerooccurValue'){//showzerobalanceoccurValue showzerobalanceoccurEdit
        	this.setState({
                showzerobalanceoccurEdit: value
			});
        	if(value){
                this.setState({
                    showzerobalanceoccurValue: value
                });
			}
		}else if(param === 'showSubjectTotal'){
        	this.setState({
                disCurrplusacc: value,
                currplusacc: 'Y'
			})
		}else if(param === 'SubjectVersion' || param === 'accbalance') {
        	this.setState({
                changeParam: true,
                rightSelectItem: []
			})
		}
	}
    handleSelect = (value) => {//

        this.setState({
            rightSelectItem: [...value]
        })
    }

    // clearTransterData = () => {
		// this.setState({
    //         selectedKeys: [],
    //         targetKeys: []
		// })
    // }
    // handleChange = (nextTargetKeys, direction, moveKeys) => {
    //     this.setState({ targetKeys: nextTargetKeys });
    //     this.handleSelect(nextTargetKeys)
    // }
    //
    // handleSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
    //     this.setState({ selectedKeys: [...sourceSelectedKeys, ...targetSelectedKeys] });
    // }
    renderModalList = () => {
		return (
			<div className='right_query'>
				<div className='query_body1'>
					<div className='query_form accbalance'>
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <span style={{color: 'red'}}>*</span>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023005-000009']}：</span>{/* 国际化处理： 核算账簿*/}
                    </Col>
                    <Col md={7} sm={7}>
                        <div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: true,
                                        showGroup: true,
                                        queryCondition: {
											pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
											multiGroup : 'Y'
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit
                                    },
                                    'accbalance'
                                )
                            }
                        </div>

                    </Col>
                </Row>
                {
                    this.state.isShowUnit ?
                        <Row className="myrow ">
                            <Col md={2} sm={2}>
                                <span className='nc-theme-form-label-c'>{this.state.json['20023005-000010']}：</span>{/* 国际化处理： 业务单元*/}
                            </Col>
                            <Col md={7} sm={7}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
										fieldid='buSecond'
                                        value={this.state.buSecond}
                                        isShowDisabledData = {true}
                                        isHasDisabledData = {true}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder',
                                            "VersionStartDate": this.state.rangeDate[1]
                                        }}
                                        isMultiSelectedEnabled={true}
                                        onChange={(v) => {
                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if (this.state.accountingbook && this.state.accountingbook[0]) {
                                                }
                                            })
                                        }
                                        }
                                    />

                                </div>

                            </Col>
                            <Col md={3} sm={3}>
                                <FICheckbox
                                    colors="dark"
                                    className="mycheck"
									id='Multbusi'
                                    checked={this.state.multbusi}
                                    onChange={ (e) => this.onChangeMultbusi(e)}
                                >
									{this.state.json['20023005-000011']} {/* 国际化处理： 多业务单元合并*/}
                                </FICheckbox>
                            </Col>
                        </Row> : ''
                }

                {/*会计期间*/}
                <DateGroup
                    selectionState = {this.state.selectionState}
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook[0]}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                />

                {/*启用科目版本：*/}
                <SubjectVersion
                    checked = {this.state.isversiondate}//复选框是否选中
                    data={this.state.versionDateArr}//
                    value={this.state.versiondate}
                    disabled={!this.state.isversiondate}
                    handleValueChange = {(key, value) => this.handleValueChange(key, value, 'SubjectVersion')}
                />

                {/*忽略日期选项*/}


                {/*科目，级次：*/}
                <SubjectLevel
                    parent = {this}
                    accountingbook = {this.state.accountingbook}
                    isMultiSelectedEnabled = {false}//控制多选单选
                    selectedKeys = {this.state.selectedKeys}
                    targetKeys = {this.state.targetKeys}
                    changeParam = {this.state.changeParam}
                    handleSelect = {this.handleSelect}
                    handleValueChange = {this.handleValueChange}

                    handleSelectChange = {(sourceSelectedKeys, targetSelectedKeys) => handleSelectChange(this, sourceSelectedKeys, targetSelectedKeys)}
                    handleChange = {(nextTargetKeys, direction, moveKeys) => handleChange(this,nextTargetKeys, direction, moveKeys) }
                    clearTransterData = {() => clearTransterData(this)}
                />

                {/*包含凭证：*/}
                <CheckBoxCells
                    paramObj = {{
                        "noChargeVoucher": {//未记账凭证
                            show: true,
                            checked: this.state.includeuntally,
                            disabled: !this.state.isqueryuntallyed,
                            onChange: this.onChangeIncludeuntally.bind(this)
                        },
                        "errorVoucher": {//错误凭证
                            show: true,
                            checked: this.state.includeerror,
                            disabled: !this.state.includeuntally,
                            onChange: this.onChangeIncludeerror.bind(this)
                        },
                        "harmToBenefitVoucher": {//损益结转凭证
                            show: true,
                            checked: this.state.includeplcf,
                            onChange: this.onChangeIncludeplcf.bind(this)
                        },
                        "againClassifyVoucher": {//重分类凭证
                            show: true,
                            checked: this.state.includerc,
                            onChange: this.onChangeIncluderc.bind(this)
                        },
                    }}
                />

                <Row className="myrow specialrow-lable">
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023005-000003']}</span>{/* 国际化处理： 多主体显示方式：*/}
                    </Col>
                    <Col md={8} sm={8}>
                        <Radio.NCRadioGroup
                            name="fruit"
                            selectedValue={this.state.mutibook}
                            onChange={this.handleMutibookChange.bind(this)}
                        >
                            <Radio value="Y" disabled={this.state.disMutibook} >
								{this.state.json['20023005-000012']} {/* 国际化处理： 多主体合并*/}
							</Radio>
                            <Radio value="N" disabled={this.state.disMutibook} >
								{this.state.json['20023005-000013']} {/* 国际化处理： 按主体列示*/}
							</Radio>
                        </Radio.NCRadioGroup>

                    </Col>
                </Row>


                {/*币种*/}
                {renderMoneyType(this.state.currtypeList, this.state.currtypeName, this.handleCurrtypeChange, this.state.json)}

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
						this.state.json
                    )
                }

                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023005-000014']}：</span>{/* 国际化处理： 排序方式*/}
                    </Col>
                    <Col md={10} sm={10}>
                        <Radio.NCRadioGroup
                            name="fruit"
                            selectedValue={this.state.currplusacc}
                            onChange={this.handleCurrplusacc.bind(this)}
                        >
                            <Radio value="Y" disabled={this.state.disCurrplusacc}>
								{`${this.state.json['20023005-000015']}+${this.state.json['20023005-000016']}`} {/* 国际化处理： 币种,科目*/}
							</Radio>
                            <Radio value="N" disabled={this.state.disCurrplusacc}>
								{`${this.state.json['20023005-000016']}+${this.state.json['20023005-000015']}`} {/* 国际化处理： 科目,币种*/}
							</Radio>
                        </Radio.NCRadioGroup>

                    </Col>
                </Row>

                <FourCheckBox
                    totalTitle= {this.state.json['20023005-000004']}/* 国际化处理： 显示属性：*/
                    renderCells={
                        [
                            {
                                title: this.state.json['20023005-000005'],/* 国际化处理： 无发生不显示*/
                                checked: this.state.showzerooccurValue,
                                disabled: this.state.showzerooccur,
                                id: 'showzerooccurValue',
                                onChange: (value) => this.handleValueChange('showzerooccurValue', value, 'showzerooccurValue')
                            },
                            {
                                title: this.state.json['20023005-000006'],/* 国际化处理： 无余额无发生不显示*/
                                checked: this.state.showzerobalanceoccurValue,
                                disabled: this.state.showzerobalanceoccurEdit,
                                id: 'showzerobalanceoccurValue',
                                onChange: (value) => this.handleValueChange('showzerobalanceoccurValue', value)
                            },
                            {
                                title: this.state.json['20023005-000007'],/* 国际化处理： 显示科目类型小计*/
                                checked: this.state.sumbysubjtype,
                                disabled: this.state.editSubjectTotal,
                                id: 'sumbysubjtype',
                                onChange: (value) => this.handleValueChange('sumbysubjtype', value, 'showSubjectTotal')
                            },
                        ]
                    }
                />

                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023005-000017']}：</span>{/* 国际化处理： 余额方向*/}
                    </Col>
                    <Col md={2} sm={2}>

                        <div className="book-ref">
                            <Select
								showClear={false}
								fieldid='direction'
                                value={this.state.balanceori}
                                // style={{ width: 90, marginRight: 6 }}
                                onChange={this.handleBalanceoriChange.bind(this)}
                            >
                                <NCOption value={'-1'} key={'-1'} >{this.state.json['20023005-000018']}</NCOption>{/* 国际化处理： 双向*/}
                                <NCOption value={'0'} key={'0'} >{this.state.json['20023005-000019']}</NCOption>{/* 国际化处理： 借*/}
                                <NCOption value={'1'} key={'1'} >{this.state.json['20023005-000020']}</NCOption>{/* 国际化处理： 贷*/}
                                <NCOption value={'3'} key={'3'} >{this.state.json['20023005-000021']}</NCOption>{/* 国际化处理： 平*/}

                            </Select>

                        </div>

                    </Col>
                    <Col md={4} sm={4}>
                        <Checkbox
                            colors="dark"
                            className="mycheck"
                            checked={this.state.twowaybalance}
                            disabled={this.state.debitAndCredit}
                            onChange={(value) => this.handleValueChange('twowaybalance', value)}
                        >
							{this.state.json['20023005-000022']} {/* 国际化处理： 按借贷方显示余额*/}
                        </Checkbox>
                    </Col>
                </Row></div>
			</div>
			</div>
		)
	}
    clickPlanEve = (value) => {
        this.state = deepClone(value.conditionobj4web.nonpublic);
        this.setState(this.state)
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
        return {...this.state}
    }
    clickSearchBtn = () => {//点击查询框的"查询"按钮事件
		let pk_accountingbook = [];
		let pk_unit = [];
		if (Array.isArray(this.state.accountingbook)) {
			this.state.accountingbook.forEach(function (item) {
				pk_accountingbook.push(item.refpk)
			})
		}
		if (Array.isArray(this.state.buSecond)) {
			this.state.buSecond.forEach(function (item) {
				pk_unit.push(item.refpk)
			})
		}
		let newFlag = [false];
		let data = {
			pk_accountingbook: pk_accountingbook,
			pk_unit,
			multbusi: this.state.multbusi,
			usesubjversion: this.state.isversiondate ? 'Y' : 'N',// 是否启用科目版本
			versiondate:  this.state.isversiondate ? this.state.versiondate : null,//科目版本
			// pk_accasoa://条件框选择科目
			startcode: this.state.startcode.code,  //开始科目编码
			endcode: this.state.endcode.code,    //结束科目编码
			startlvl: String(this.state.startlvl),
			endlvl: String(this.state.endlvl),
			isleave: this.state.isleave ? 'Y' : 'N',
			isoutacc: this.state.isoutacc ? 'Y' : 'N',

			startyear: this.state.startyear,
			endyear: this.state.endyear,
			startperiod: this.state.startperiod,
			endperiod:   this.state.endperiod,
			startdate: this.state.begindate,
			enddate: this.state.enddate,


			//未记账、错误、损益结转、重分类  Y/N  注意：‘包含凭证’是个标题，后面的才是选项
			includeuntally: this.state.includeuntally ? 'Y' : 'N',
			includeerror: this.state.includeerror ? 'Y' : 'N',
			includeplcf: this.state.includeplcf ? 'Y' : 'N',
			includerc: this.state.includerc ? 'Y' : 'N',
			pk_currtype: this.state.currtype,
			returncurr: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局
			mutibook: this.state.mutibook,
			showzerooccur: this.state.showzerooccurValue ? 'Y' : 'N',
			showzerobalanceoccur: this.state.showzerobalanceoccurValue ? 'Y' : 'N',
			currplusacc: this.state.currplusacc,
			sumbysubjtype: this.state.sumbysubjtype ? 'Y' : 'N',
			balanceori: this.state.balanceori,
			twowaybalance: this.state.twowaybalance ? 'Y' : 'N',
			querybyperiod: this.state.selectionState,//会计期间/日期选项
			pk_accasoa: this.state.rightSelectItem,//'条件'

		}

		let url = '/nccloud/gl/accountrep/checkparam.do'
        let flagShowOrHide;
        ajax({
            url,
            data,
            async:false,
            success:function(response){
                flagShowOrHide = true
            },
            error:function(error){
                flagShowOrHide = false
                toast({content: error.message, color: 'warning'});
            }
		})
		if (flagShowOrHide == true) {
			this.props.onConfirm(data);
		} else {
			return true;
		}
	}

	render() {
		let { show, title, content, confirmText, cancelText, closeButton, icon, isButtonWhite, isButtonShow}= this.props;
		let { search, hideBtnArea } = this.props;
		let { NCCreateSearch } = search;
		let isHideBtnArea = hideBtnArea ? hideBtnArea : false;
        return <div>
            {this.state.json['20023005-000001'] &&
                NCCreateSearch(this.searchId, {
                    onlyShowSuperBtn:true,                       // 只显示高级按钮
                    replaceSuperBtn:this.state.json['20023005-000001'],                      // 自定义替换高级按钮名称/* 国际化处理： 查询*/
                    replaceRightBody:this.renderModalList,
                    hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
                    clickPlanEve:this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                    saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
                    oid:this.props.meta.oid,
					clickSearchBtn: this.clickSearchBtn,  // 点击按钮事件
					isSynInitAdvSearch: true,//渲染右边面板自定义区域
					hideBtnArea: isHideBtnArea // 隐藏高级查询按钮
                })}
        </div>
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal;


{/*<Modal.Footer>*/}

    {/*<Button*/}
        {/*className= {`btn-2 ${isButtonWhite ? '' : 'btn-cancel'}`}*/}

        {/*}*/}
    {/*>*/}
        {/*{confirmText}*/}
    {/*</Button>*/}


    {/*<Button*/}
        {/*className= 'btn-2 btn-cancel'*/}
        {/*onClick={() => {this.props.onCancel(false)}}*/}
    {/*>{cancelText}</Button>*/}
{/*</Modal.Footer>*/}
