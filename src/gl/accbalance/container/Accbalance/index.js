/**
 * 科目余额表
 * **/

import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';

// import OpenTable from './OpenTable';

import {high,base,ajax,deepClone, createPage, getMultiLang, gzip,createPageIcon } from 'nc-lightapp-front';
const { NCButton: Button, NCIcon:Icon, NCMenu: Menu, NCModal: Modal, NCDiv
} = base;

import './index.less';
import '../../../public/reportcss/firstpage.less'
import '../../../public/style/index.less'
import { toast } from '../../../public/components/utils';
import MainSelectModal from './MainSelectModal';
import { SimpleTable } from 'nc-report';
import {tableDefaultData} from '../../../manageReport/defaultTableData';
import reportPrint from '../../../public/components/reportPrint.js';
import reportSaveWidths from '../../../public/common/reportSaveWidths.js';
import { setData } from '../../../manageReport/common/simbleTableData.js';
import { getReferCells } from '../../../public/hansonTableSetData/commonFn';
import ReferSelectTable from '../../../public/hansonTableSetData/referSelectTable';
//react-demo 13.html
const { PrintOutput } = high;
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import HeadCom from '../../../manageReport/common/headSearch/headCom';
import {handleValueChange} from '../../../manageReport/common/modules/handleValueChange'
import {searchById} from "../../../manageReport/common/modules/createBtn";
import {handleSimpleTableClick} from "../../../manageReport/common/modules/simpleTableClick";
import {whetherDetail} from "../../../manageReport/common/modules/simpleTableClick";
import {rowBackgroundColor} from "../../../manageReport/common/htRowBackground";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
import { queryRelatedAppcode, originAppcode } from '../../../public/common/queryRelatedAppcode.js';
class Accbalance extends Component {
	constructor(props) {
		super(props);

		//每定义一个state 都要添加备注
		this.state = {
			json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            accountType: 'amountcolumn',//金额式
			flag:0,//切换表头用的
      		showTable: false,
      		textAlginArr:[],//对其方式
            multiCheckAccountShow: true, //多核算账簿列示是否禁止；true：禁止
            showMultiCheckAccount: true, //是否显示【多核算账簿列示】
			disabled: true, //控制‘打印’按钮是否可操作
			visible: false, //控制打印框是否显示
			appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
			outputData: {},
			ctemplate: '', //模板输出-模板
			nodekey: '',
			printParams:{}, //查询框参数，打印使用
			referModalShow: false,//控制行的参照弹框；false: 隐藏
            showAddBtn: true,//控制"联查"按钮是否可用;true:禁止，false:解除禁止
			queryDatas:{},
			data: {},
			MainSelectModalShow: false, //查询模态框
			tableAll: {
				column: [],
				data:[],
			},              //表格全体数据
			initTableAll: null,  //初始查询出的表格全体数据
			tableStyle: {}, //表格样式
			queryCond: null, //已选择的查询条件
			pk_cashflow: '', //已点击行的pk_cashflow
			datas: {
				time: '',
				bookname: '',
				currtypeName: '',
			},
            dataout: tableDefaultData,
			referLength: 0,
			referContent: {},
			selectRow: [],
			'allbalance': [],
			hideBtnArea : false // 是否隐藏高级查询按钮
		}
        this.handleValueChange = handleValueChange.bind(this);
		this.searchById = searchById.bind(this);
		this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
		this.whetherDetail = whetherDetail.bind(this);
		this.rowBackgroundColor = rowBackgroundColor.bind(this);
	}
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
				json:json,
                'allbalance': [
                    {
                        title: json['20023005-000035'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20023005-000036'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023005-000037'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023005-000038'],
                        styleClass:"m-brief"
                    },

                    {
                        title: json['20023005-000039'],
                        styleClass:"m-brief"
                    }
                ]
			})
        }
        getMultiLang({moduleId:['20023005','publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }

    componentDidMount(){
        this.setState({
            data: {}
        })
        //('>>>>>>>', this.props.getSearchParam('c'))
        let appceod = this.props.getSearchParam('c');
		this.searchById('20023005PAGE', appceod).then((res) => {
			// searchById返回当前登录环境的数据源信息。
			// 如果联查的数据源与当前数据源不一致，则隐藏页面的所有按钮。
			let param = this.props.getUrlParam && this.props.getUrlParam('status');
			if (param && res && res.data.context) {
				let unzipParam = new gzip().unzip(param)
				if (unzipParam.env) {
					let envs = unzipParam.env.replace(/\[/g, "").replace(/\]/g, "").replace(/\"/g, "").split(",");
					if (envs[0] && envs[0] != res.data.context.dataSource) {
						this.props.button.setButtons([]);
						this.setState({ hideBtnArea: true });
					}
				}
			}
		});
        this.props.button.setDisabled({
            print:true, templateOutput:true,
			directprint:true, saveformat:true,
			detailbook:true, accbook:true, assbal:true, multiorg:true,
		})
        this.getParam();
    }
	//直接输出
	print(){
		let {resourceDatas,dataout,textAlginArr}=this.state;
		let dataArr=[];
		let emptyArr=[];
		let mergeInfo=dataout.data.mergeInfo;
		dataout.data.cells.map((item,index)=>{
			emptyArr=[];
			item.map((list,_index)=>{
				//emptyArr=[];
				if(list){
					// if(list.title){
					//     list.title='';
					// }
					emptyArr.push(list.title);
				}
			})
			dataArr.push(emptyArr);
		})
		//('printtt>>>>', mergeInfo,dataArr,textAlginArr);
		reportPrint(mergeInfo,dataArr,textAlginArr);
	}
	//保存列宽
	handleSaveColwidth=()=>{
		let {json}=this.state
		let info=this.refs.balanceTable.getReportInfo();
		let callBack = this.refs.balanceTable.resetWidths
		reportSaveWidths(this.state.appcode,info.colWidths,json, callBack);
	}
	//打印
	showPrintModal = () => {
		this.setState({
			visible: true
		})
	}
	handlePrint(data, isPreview) {
		let printUrl = '/nccloud/gl/accountrep/balancebookprint.do'
		let { printParams, appcode } = this.state
		let { ctemplate, nodekey } = data
		printParams.queryvo = data
		this.setState({
			ctemplate: ctemplate,
			nodekey: nodekey
        })
		printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
		this.handleCancel()
	}
	handleCancel() {
		this.setState({
			visible: false
		});
	}
	showOutputModal = () => {
		// this.refs.printOutput.open()
		let outputUrl = '/nccloud/gl/accountrep/balancebookoutput.do'
		let { appcode, nodekey, ctemplate, printParams } = this.state
		mouldOutput(outputUrl,appcode, nodekey, ctemplate, printParams)

        // let outputData = mouldOutput(outputUrl,appcode, nodekey, ctemplate, printParams)
        // this.setState({
        //     outputData: outputData
        // })
    }
    handleOutput() {
        //(this.state.outputData)
    }

    getParam = () => {
		let urlParam;
		// 联查参数（需解压）
		let data = this.props.getUrlParam && this.props.getUrlParam('status');
		if (data) {
			urlParam = new gzip().unzip(data)
		}
		// 小友智能查账参数（不需解压）
		let param = this.props.getSearchParam('param');
		if (param) {
			urlParam = param;
		}
		// 查询
		if(urlParam) {
			this.state.queryDatas.mutibook = 'N';
			this.getDatas(urlParam, 'link');
		}
    }

	queryShow() {
		this.setState({
			MainSelectModalShow: true,
		})
	}

    test(datas) {
        this.props.button.setDisabled({
            print:false, templateOutput:false,
			directprint:false, saveformat:false,
        })
		let self = this;
		let {disabled,headtitle,dataWithPage,typeDisabled}=self.state;
        let url = '/nccloud/gl/accountrep/accbalquery.do';
        let data = datas;
        ajax({
            loading: true,
            url,
            data,
            success: function (res) {
                const { data, error, success } = res;
                if(success){
                    if(data&&data.data.length>0){
						disabled=false,
                        headtitle=data.headtitle
                    }
                    self.setState({
						disabled,
						headtitle,
                        dataWithPage: data,
						typeDisabled: false,
						selectRowIndex: 0
                    })
                    setData(self, data);
                }else {
                    toast({ content: error.message, color: 'warning' });
                }
            },
            error: function (res) {
                toast({ content: res.message, color: 'warning' });

            }
        });
    }


	getDatas = (datas,type) => {
		let {showMultiCheckAccount,multiCheckAccountShow}=this.state;
		this.test(datas);
		//联查
		if(type=='link'){
            showMultiCheckAccount=false
		}
		
		if ((datas.pk_accountingbook && datas.pk_accountingbook.length > 1 && datas.mutibook === 'Y')
			|| (datas.origparam && datas.origparam.pk_accountingbook.length > 1 && datas.origparam.mutibook === 'Y')) {
            multiCheckAccountShow=false;
		}else{
            multiCheckAccountShow=true;
		}
		this.setState({
			queryDatas: datas,
			printParams: datas,
			showAddBtn: false,
			showMultiCheckAccount,
			multiCheckAccountShow
		});
	}

    relevanceAssist = (response, param) => {//科目余额表的：联查辅助
        let { success, data } = response;
        //('ssssss:', data);
        if (success) {
            //('relevanceAssist', response, self, data);
            if(data){
            	if(data.length > 1){
                    //('ineeerrr::')
                    this.setState({
                        referLength: data.length,
                        referContent: [...data],
                        referModalShow:true
                    })
				}else if(data.length ===1 ){
            		//('llllll::', data[0].pk_accassitem)
            		this.setState({
						selectRow: [data[0].pk_accassitem]
					})
					setTimeout(() => this.getDetailPort({
                        url: '/nccloud/gl/accountrep/assbalancequery.do',
                        jumpTo: '/gl/manageReport/assistBalance/content/index.html',//'/gl/accbalance/relevanceSearch/assist/index.html',
                        key: 'assist',
                        appcode: queryRelatedAppcode(originAppcode.assbalance),
                    }), 0);
				}

            }else {
                toast({content: `${param.acccode}${this.state.json['20023005-000023']}，${this.state.json['20023005-000024']}`, color: 'warning'})/* 国际化处理： 科目无辅助项,不能联查辅助*/
                return;
            }
        }
    }

    onSelect= (key) => {
		//(`${key} selected`);
		// 联查提示信息
		let linkmsg = this.getSelectRowData()[0].linkmsg;
		if (linkmsg) {
			toast({ content: linkmsg[parseInt(key) - 1], color: 'warning' });
			return;
		}
		// 没有link不允许联查
		let link = this.getSelectRowData()[0].link;
		if (!link) {
			return;
		}
        switch (key){
            case '1':
                this.getDetailPort({
					url: '/nccloud/gl/accountrep/triaccquery.do',
					jumpTo: '/gl/threedetail/pages/main/index.html',
					key: 'detail',
					appcode: queryRelatedAppcode(originAppcode.detailbook)
				})
                break;
            case "2":
                this.getDetailPort({
                    url: '/nccloud/gl/accountrep/triaccquery.do',
                    jumpTo: '/gl/threeall/pages/main/index.html',//'/gl/accbalance/relevanceSearch/totalAccount/index.html',
                    key: 'totalAccount',
					appcode: queryRelatedAppcode(originAppcode.triaccbook),
                })
                break;
            case "3":
                let selectRow = this.getSelectRowData()[0].link;
                selectRow['key'] = 'relevanceAssist'
                getReferCells(this,selectRow, this.relevanceAssist)//查找所选择的数据有几个参照
        }
    }
    referModalSure = () => {
		if (!this.state.selectRow || this.state.selectRow.length === 0) {
			toast({ content: `${this.state.json['20023005-000025']}，${this.state.json['20023005-000026']}`, color: 'warning' });/* 国际化处理： 没有选辅助核算类型,请重新选择*/
			return;
		}
		this.setState({
			referModalShow: false
		})
        this.getDetailPort({
            url: '/nccloud/gl/accountrep/assbalancequery.do',
            jumpTo: '/gl/manageReport/assistBalance/content/index.html',//'/gl/accbalance/relevanceSearch/assist/index.html',
            key: 'assist',
            appcode: queryRelatedAppcode(originAppcode.assbalance)
        })
	}
	getDetailPort(paramObj){//联查"明细"
		
		let url = paramObj.url; //'/nccloud/gl/accountrep/triaccquery.do';
		let data = this.getDataType(paramObj);
        this.jumpToDetail(data, paramObj);
		//('getDetailPort>>', data);
	}
	getDataType = (param) => {
		let selectRow = this.getSelectRowData()[0].link;
		let result = '';
		if(param.key === 'detail'){
			result = {
				origparam: {...this.state.queryDatas},
				link: {...selectRow},
				"class": "nccloud.pubimpl.gl.account.AccbalLinkTridetailParamTransfer"
			}
        }else if(param.key === 'totalAccount'){
			result = {
				origparam: {...this.state.queryDatas},
				link: {...selectRow},
				"class": "nccloud.pubimpl.gl.account.AccbalLinkTriaccbookParamTransfer"
			}
		}else if(param.key === 'assist'){//联查"辅助"
            selectRow['key'] = 'relevanceAssist';
			//('this.state.selectRow::', this.state.selectRow)
            result = {
            	...this.state.queryDatas,
				link: {...selectRow},
                "pk_accassitems": [...this.state.selectRow],
				"class": "nccloud.pubimpl.gl.account.AccbalLinkAssbalParamTransfer",
                "from": "accbal" // 联查来源：(多主体)科目余额表accbal
            }
        }else if(param.key === 'multiAccountShow'){
            result = {
                origparam: {...this.state.queryDatas},
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.account.AccbalLinkMultiorgParamTransfer",
                "from": "multicorp"
            }
		}
        //('getDataType>', result, selectRow);
        return result;
	}
	jumpToDetail = (data, paramObj) => {
		let gziptools = new gzip();
        this.props.openTo(
            paramObj.jumpTo,
            {appcode: paramObj.appcode, status: gziptools.zip(JSON.stringify(data)),id: '12wew24'}
        );
        this.setState({
			selectRow: []
		})
	}
    //获取handsonTable当前选中行数据
    getSelectRowData=()=>{
        let selectRowData=this.refs.balanceTable.getRowRecord();
        return selectRowData;
    }

    getSelectRow = (param) => {
		//('getSelectRow>>', param)
		this.setState({
			selectRow:[...this.state.selectRow.concat(param)]
		})
	}
    handleLoginBtn = (obj, btnName) => {
		//('handleLoginBtn>>', obj, btnName)
		if(btnName === 'print'){//打印
            this.showPrintModal()
		}else if(btnName === 'templateOutput'){
            this.showOutputModal()
		}else if(btnName === 'directprint'){//直接输出
            this.print()
		} else if(btnName === 'saveformat'){
            this.handleSaveColwidth()
        }else if(btnName === 'detailbook'){//联查-> 明细
            this.onSelect('1')
        }else if(btnName === 'accbook'){//联查-> 总账
            this.onSelect('2')
        }else if(btnName === 'assbal'){//联查-> 辅助
            this.onSelect('3')
		}else if(btnName === 'multiorg'){//联查-> 多核算账簿列式
			// 没有link不允许联查
			let link = this.getSelectRowData()[0].link;
			if (!link) {
				return;
			}
            this.getDetailPort({
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: '/gl/accbalance/pages/main/index.html',
                key: 'multiAccountShow',
                appcode: this.state.appcode
            })
		} else if (btnName === 'refresh') {// 刷新
			if (Object.keys(this.state.queryDatas).length > 0) {
				this.getDatas(this.state.queryDatas,'refresh');
			}
		}
	}

	render() {
		let { modal } = this.props;
		const { createModal } = modal;
		return (
			<div className='manageReportContainer' style={this.props.style}>
				<HeaderArea 
                    title = {this.state.json['20023005-000030']} /* 国际化处理： 科目余额表*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
								<MainSelectModal
									hideBtnArea={this.state.hideBtnArea}
									onConfirm={(datas) => {
										this.getDatas(datas, 'search');
									}}
								/>
                            </div>
                            {this.props.button.createButtonApp({
                                area: 'btnarea',
                                buttonLimit: 3,
                                onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                            })}
                        </div>
                    }
                />
				<NCDiv areaCode={NCDiv.config.SEARCH}>
					<div className="searchContainer nc-theme-gray-area-bgc">
						{
							this.state.allbalance.map((items) => {
								return (
									<HeadCom
										labels={items.title}
										key={items.title}
										lastThre={this.state.headtitle && this.state.headtitle}
										changeSelectStyle={
											(key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
										}
										accountType={this.state.accountType}
										isLong = {items.styleClass}
										typeDisabled = {this.state.typeDisabled}
									/>
								)
							})
						}
					</div>
				</NCDiv>
				<div className='report-table-area'>
                    <SimpleTable
						// className='SimpleTable'
                        ref="balanceTable"
                        data = {this.state.dataout}
                        // onCellMouseDown = {(e, coord, td) => this.handleSimpleTableClick(e, coord, td, this.state.showButton)}

                        onCellMouseDown = {(e, coords, td) => {
                            //('recorddd:::', this.refs.balanceTable.getRowRecord());
                            this.whetherDetail('link', 'Accbalance');
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
				</div>

				{/*li联查辅助的弹框*/}
                <Modal
                    className='modal'
                    show={ this.state.referModalShow }

                    animation={false}
                    backdrop="static"
                    size = "xlg"
                >
                    <Modal.Header>
                        <Modal.Title>
                            <span className={`title-icon iconfont icon-tishianniuchenggong`}></span>
							<span className='bd-title-1'>{this.state.json['20023005-000031']}</span>{/* 国际化处理： 辅助核算*/}
							<span>
								<Icon
									className='close-icon iconfont icon-guanbi'
								/>
							</span>
                        </Modal.Title>
                    </Modal.Header >
                    <Modal.Body>
                        <ReferSelectTable
							data = {this.state.referContent}
                            getSelectRow={this.getSelectRow}
                            selectRow={this.state.selectRow}
						/>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className= "button-primary" onClick={() => {this.referModalSure()}}>{this.state.json['20023005-000032']}</Button>{/* 国际化处理： 确定*/}
						<Button onClick={() => this.setState({referModalShow: false})}>{this.state.json['20023005-000002']}</Button>{/* 国际化处理： 取消*/}
					</Modal.Footer>
				</Modal>

				<RepPrintModal
					noRadio={true}
					// scopeGray={true}
					noCheckBox={true}
					appcode={this.state.appcode}
					visible={this.state.visible}
					handlePrint={this.handlePrint.bind(this)}
					handleCancel={this.handleCancel.bind(this)}
				/>
				<PrintOutput					
					ref='printOutput'
					url='/nccloud/gl/accountrep/balancebookoutput.do'
					data={this.state.outputData}
					callback={this.handleOutput.bind(this)}
				/>
				{createModal('printService', {
					className: 'print-service'
				})}
				<Iframe />
			</div>
			)
	}
}

Accbalance = createPage({
	inintTemplate: {
		appcode: '123'
	}
})(Accbalance)
export default Accbalance;
