export const APPCODE = {
    ORG : "20020VTCRL",
    GROUP : "20020VTCRLGRP",
    GLOBAL : "20020VTCRLGLB"
};

export const PAGECODE = {
    ORG : "20020VTCRLPAGE",
    GROUP : "20020VTCRLGRPPAGE",
    GLOBAL : "20020VTCRLGLBPAGE"
}

export const NODETYPE = {
    ORG : "org",
    GROUP : "group",
    GLOBAL : "global"
}

export const TABLECODE = "VouchertypeRule";