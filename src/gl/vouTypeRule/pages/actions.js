import { ajax, toast } from 'nc-lightapp-front';

const requestApi = {
    update:(opt)=>{
        myajax('/nccloud/gl/voucher/saveVouTypeRules.do', opt.data, opt.success);
    },
    query:(opt) => {
        myajax('/nccloud/gl/voucher/queryVouTypeRules.do', opt.data, opt.success);
    },
}

function myajax(url, data, success, fail){
    ajax({
        url, data, 
        success:(res)=>{
            if(res.success){
                if(success){ success(res.data); }
            }else{
                toast({content:res.msg});
                if(fail){ fail(res.data);}
            }
        }
    })
}

export {requestApi};
