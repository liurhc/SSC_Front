// 单表
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, toast, getMultiLang, createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, pageInfoClick} from './events';
import './index.less';
import SetOfBookRef from '../../../../uapbd/refer/org/SetOfBookGridRef/index'
import ReferLoader from '../../../public/ReferLoader/index.js'
import {accbookRefcode} from '../../../public/ReferLoader/constants.js'
import {APPCODE, PAGECODE, NODETYPE, TABLECODE} from '../../consts';
import {requestApi} from '../actions';
import {VOStatus} from '../../../public/common/consts'
import HeaderArea from '../../../public/components/HeaderArea';

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json:{}
		};
		this.nodeType = this.getUrlParam('nodeType');
		this.appCode = this.getAppCode(this.nodeType);
		this.pageCode = this.getPageCode(this.nodeType);
		this.tableCode = TABLECODE;
		this.editable = false;
	}

	componentDidMount(){
		//关闭浏览器
        window.onbeforeunload = () => {
                let status=this.props.editTable.getStatus(this.tableCode)
            if (status == 'edit' || status == 'add') {
                return ''
            }
        }
	}

	componentWillMount(){
		let callback = (json) => {
			this.setState({json:json}, () => {
				initTemplate.call(this, this.props);
				this.toggoleShow();
			})
		}
		getMultiLang({moduleId:'20020VTCRL', domainName:'gl', callback});
	}

	//请求列表数据
	getData = (pk_accountingbook) => {
		let data = {
			"nodeType":this.nodeType,
			"pk_accountingbook":pk_accountingbook,
			"pk_setofbook":pk_accountingbook,
			"pageId":this.pageCode
		};
		requestApi.query({data, success:(data)=>{this.loadData(data)}});
	};

	loadData = (data) => {
		if (data && data[this.tableCode]) {
			this.props.editTable.setTableData(this.tableCode, data[this.tableCode]);
			this.resetTableState();
		} else {
			this.props.editTable.setTableData(this.tableCode, { rows: [] });
		}
		this.props.editTable.cancelEdit(this.tableCode);
		this.updateBthState();
	}

	/**
	 * 设置所属组织列默认值
	 */
	getOrgInfo = (pk_accountingbook) => {
		let data = {};
		let url = '/nccloud/gl/glpub/queryOrgByPk.do';
		if(this.nodeType == NODETYPE.GLOBAL){
			data = {"pk_org" : "GLOBLE00000000000000"};
		}else if(this.nodeType == NODETYPE.ORG){
			url = '/nccloud/gl/glpub/queryFinanceOrg.do';
			data = {"pk_accountingbook":pk_accountingbook};
		}
		let props = this.props;
		ajax({
			url: url,
			data: data,
			success: (res) => {
				let {success, data} = res;
				if(success){
					if(data){
						let pk_org = data.pk_org;
						let orgName = data.name;
						let meta = props.meta.getMeta();
						meta[this.tableCode].items = meta[this.tableCode].items.map((item, index) => {
							if(item.attrcode == 'pk_org'){
								item.initialvalue = {value: pk_org, display: orgName};
							}
							return item;
						});
						props.meta.setMeta(meta);
					}
				}
			}
		});

	}

	/**
	 * 获取appCode
	 */
	getAppCode = (nodeType) => {
		switch(nodeType){
			case NODETYPE.ORG: return APPCODE.ORG; break;
			case NODETYPE.GROUP: return APPCODE.GROUP; break;
			case NODETYPE.GLOBAL: return APPCODE.GLOBAL; break;
		}
	}

	getPageCode = (nodeType) => {
		switch(nodeType){
			case NODETYPE.ORG: return PAGECODE.ORG; break;
			case NODETYPE.GROUP: return PAGECODE.GROUP; break;
			case NODETYPE.GLOBAL: return PAGECODE.GLOBAL; break;
		}
	}

	toggoleShow = () => {
		let flag = this.editable;
		this.props.button.setButtonVisible(['save', 'cancel'], flag);
		this.props.button.setButtonVisible(['add', 'delete', 'update', 'refresh'], !flag);
		this.updateBthState();
	}

	/**
	 * 更新浏览态按钮状态
	 */
	updateBthState = () => {
		let page = this;
		let {pk_accountingbook} = page.state;
		let btnState = {};
		const disabled = true;
		if(pk_accountingbook){
			btnState.add = !disabled;
			btnState.update = !disabled;
			btnState.delete = !disabled;
			btnState.refresh = !disabled;
			if(page.props.editTable.getNumberOfRows(TABLECODE) <= 0){
				btnState.update = disabled;
				btnState.delete = disabled;
			}else{
				if(this.getCheckedRownum(page.props) <= 0){
					btnState.delete = disabled;
				}
			}
		}else{
			btnState.add = btnState.update = btnState.delete = btnState.refresh = disabled;
		}
		page.props.button.setButtonDisabled(btnState);
	}

	getCheckedRownum(props){
		let data = props.editTable.getCheckedRows(TABLECODE);
		let checkedRownum = 0;
		if(data){checkedRownum = data.length;};
		return checkedRownum;
	}

	refresh = () => {
		if(this.state){
			let{pk_accountingbook} = this.state;
			let data = {
				"nodeType":this.nodeType,
				"pk_accountingbook":pk_accountingbook,
				"pk_setofbook":pk_accountingbook,
				"pageId":this.pageCode
			};
			requestApi.query({data, 
				success:(data)=>{
					toast({content:this.state.json['20020VTCRL-000002']});/* 国际化处理： 刷新成功*/
					this.loadData(data);
				}
			});
		}
	}

	saveData() {
		let changedRows = this.props.editTable.getChangedRows(this.tableCode);
		if(!changedRows){ return; } /* 未通过校验 */
		let {pk_accountingbook, pk_setofbook} = this.state;
		ajax({
			url:'/nccloud/gl/voucher/saveVouTypeRules.do',
			data: changedRows,
			success: (res) => {
				let { success, data } = res;
				if(success){
					toast({content:this.state.json['20020VTCRL-000003']});/* 国际化处理： 保存成功*/
					this.getData(pk_accountingbook, pk_setofbook);
					this.props.editTable.cancelEdit(this.tableCode);
					this.editable = false;
					this.toggoleShow();
				}
			}
		});
	}


	delRows(page){
		let tableCode = page.tableCode;
		let {pk_accountingbook} = page.state;
		let selectedRows = page.props.editTable.getCheckedRows(tableCode);
		let {editTable} = page.props;
		const changedRows = [];
		selectedRows.map(function(item, index){
			item.data.status = VOStatus.DELETED;
			changedRows.push(item.data);
		});
		requestApi.update({
			data: changedRows,
			success:(res)=>{
				toast({content:this.state.json['20020VTCRL-000004']});/* 国际化处理： 删除成功*/
				page.getData(pk_accountingbook);
			}
		});

	}

	update = () => {
		this.props.editTable.setStatus(this.tableCode, 'edit');
		this.editable = true;
		this.toggoleShow();
	}

	/**
	 * 更新列参照信息
	 */
	updateColRefInfo = (props,state) => {
		let meta = props.meta.getMeta();
		meta[this.tableCode].items = meta[this.tableCode].items.map((item, key) =>{
			if(item.attrcode == 'pk_vouchertype' && this.nodeType == NODETYPE.ORG){
			}
			return item;
		});
		props.meta.setMeta(meta);
	}

	/**
	 * 更新新增行列的默认值信息
	 */
	updateColInitInfo = (props, state) => {
		let meta = props.meta.getMeta();
		meta[this.tableCode].items = meta[this.tableCode].items.map((item, key) => {
			if(this.nodeType == NODETYPE.GLOBAL || this.nodeType == NODETYPE.GROUP){
				if(item.attrcode == 'pk_setofbook'){
					item.initialvalue = {value: state.pk_accountingbook, display: state.bookname};
				}
			}else{
				if(item.attrcode == 'pk_accountingbook'){
					item.initialvalue = {value: state.pk_accountingbook, display: state.bookname};
				}
			}
			return item;
		});
		props.meta.setMeta(meta);
	}

	onBookChanged = (props, state) => {
		this.updateColRefInfo(props, state);
		this.updateColInitInfo(props, state);
		this.getOrgInfo(state.pk_accountingbook);
		this.updateBthState();
	}

	/**
	 * 设置列表的编辑性
	 */
	resetTableState(){
		let rows = this.props.editTable.getAllRows(this.tableCode);
		let props = this.props;
		rows = rows.map((item, index) => {
			if(item.values.editable.value){
				/* 很奇葩的设定 true 启用， false 禁用 */
				setTimeout(() => {
					props.editTable.setCheckboxDisabled(this.tableCode, index, true);
					props.editTable.setEditableRowByRowId(this.tableCode, item.rowid, true);
					props.editTable.setEditableRowKeyByRowId(this.tableCode, item.rowid, "pk_org", false);
					props.editTable.setEditableRowKeyByRowId(this.tableCode, item.rowid, "pk_accountingbook", false);
					props.editTable.setEditableRowKeyByRowId(this.tableCode, item.rowid, "pk_setofbook", false);
				}, 0);
			}else{
				setTimeout(() => {
					props.editTable.setCheckboxDisabled(this.tableCode, index, false);
					props.editTable.setEditableRowByRowId(this.tableCode, item.rowid, false);
				}, 0);
			}
		});
	}

	cancelChange(){
		let page = this;
		this.props.editTable.cancelEdit(this.tableCode);
		this.editable = false;
		this.toggoleShow();
		/* 延时调用，给控件完成取消编辑的时间 */
		setTimeout(() => {
			page.resetTableState();
		}, 500);
	}

	getUrlParam(pop){
		if (!pop) return;
		let result;
		let queryString = window.location.search || window.location.hash;
		queryString = queryString.substring(1);
		if (queryString.includes('?')) {
			queryString = queryString.replace(/\?/g, '&');
		}
		let params = queryString;
		if (params) {
			params = params.split('&');
			let item = params.find((e) => e.indexOf(pop) !== -1);
			item && (result = item.split('=')[1]);
		}
		return result;
	}

	renderSearchContent=()=>{
		let page = this;
		let props = page.props;
		return (
			<div className="width_200">
				{/* 根据节点判断 参照类型 */}
				{this.nodeType != NODETYPE.ORG ?
					<SetOfBookRef
						fieldid="setofbook"
						value={{ refname: this.state.bookname, refpk: this.state.pk_accountingbook, refcode: this.state.bookcode }}
						queryCondition={{ "isDataPowerEnable": 'Y', "DataPowerOperationCode": 'fi' }}
						disabled={page.editable}
						onChange={(v) => {
							let { pk_accountingbook, bookcode, bookname } = this.state;
							pk_accountingbook = v.refpk;
							bookcode = v.refcode;
							bookname = v.refname
							this.setState({ pk_accountingbook, bookcode, bookname }, () => {
								this.onBookChanged(this.props, this.state);
								this.getData(v.refpk);
							});
						}}
					/>
					:
					<ReferLoader
						fieldid="accountbook"
						tag='accbookRef'
						isMultiSelectedEnabled={false}
						disabled={page.editable}
						showGroup={false}
						showInCludeChildren={true}
						refcode={accbookRefcode}
						value={{ refname: this.state.bookname, refpk: this.state.pk_accountingbook, refcode: this.state.bookcode }}
						queryCondition={() => {
							let condition = {
								"TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
								"appcode": "",
								"isDataPowerEnable": 'Y',
								"DataPowerOperationCode": 'fi'
							}
							condition.appcode = props.getSearchParam('c');
							return condition;
						}}

						onChange={(v) => {
							let { pk_accountingbook, bookcode, bookname } = this.state;
							pk_accountingbook = v.refpk;
							bookcode = v.refcode;
							bookname = v.refname
							this.setState({ pk_accountingbook, bookcode, bookname }, () => {
								this.onBookChanged(this.props, this.state);
								this.getData(v.refpk);
							});
						}}
					/>
				}
			</div>
		)
	}

	render() {
		let { createEditTable } = this.props.editTable;
		let props = this.props;
		let page = this;
		return (
			<div className="nc-bill-list vou-type-rule">
				<HeaderArea
					title={this.props.getSearchParam('n')}
					searchContent={this.renderSearchContent()}
					btnContent={this.props.button.createButtonApp({
						area: 'page_header',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				/>
				<div className="table-area">
					{createEditTable(this.tableCode, {
						//列表区
						adaptionHeight:true,
						onAfterEvent: afterEvent,
						handlePageInfoChange: pageInfoClick,
						onSelected: page.updateBthState,
						onSelectedAll: page.updateBthState,
						showCheck: true,
						showIndex: true
					})}
				</div>
			</div>
		);
	}

}
export default (SingleTable = createPage({})(SingleTable));
ReactDOM.render(<SingleTable />, document.querySelector('#app'));
