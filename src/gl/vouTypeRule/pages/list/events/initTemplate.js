import { ajax, base, getBusinessInfo } from 'nc-lightapp-front';
import {NODETYPE} from '../../../consts';


export default function(props) {
	let page = this;
	props.createUIDom(
		{
			pagecode: page.pageCode,
			appcode : page.appCode
		},
		function(data){
			if(data){
				if(data.template){
					let meta = data.template;
					meta = modifierMeta(props, meta, page);
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
				if(data.context){
					initState(page, data.context);
				}
			}
		}
 	);
}

function modifierMeta(props, meta, page) {
	let {tableCode, nodeType} = page;
	meta[tableCode].items = meta[tableCode].items.map((item, key) =>{
		if(nodeType == NODETYPE.GLOBAL || nodeType == NODETYPE.GROUP){
			if(item.attrcode == 'ruleaccasoa'){
				item.refcode = 'uapbd/refer/fiacc/AccountGridTreeRefInGroupOrGlobal/index';
				item.isAccountRefer = false;
				item.isMultiSelectedEnabled = true;
				item.queryCondition = () => {
					return getAccountCondition(page);
				}
			}
		}else{
			if(item.attrcode == 'ruleaccasoa'){
				item.refcode = 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index';
				item.isMultiSelectedEnabled = true;
				item.queryCondition = () => {
					return getAccountCondition(page);
				}
			}
		}
		if(item.attrcode == 'pk_vouchertype'){
			item.queryCondition = () =>{
				return getVouTypeCondition(page);
			}
		}
		return item;
	})
	return meta;
}

function getAccountCondition(page){
	let {nodeType} = page;
	let {pk_accountingbook} = page.state;
	const condition = {
		pk_accountingbook:'', 
		pk_setofbook:'',
		pk_org:'',
		dateStr:'',
		"isDataPowerEnable": 'Y',
		"DataPowerOperationCode" : 'fi'
	}
	let businessInfo = getBusinessInfo();
	let buziDate = businessInfo.businessDate;
	if(nodeType == NODETYPE.ORG){
		condition.pk_accountingbook = pk_accountingbook;
	}else{
		condition.pk_setofbook = pk_accountingbook;
		if(nodeType == NODETYPE.GROUP){
			condition.pk_org = businessInfo.groupId;
		}
	}
	if(buziDate){
		condition.dateStr = buziDate.split(' ')[0];
	}
	return condition;
}

function getVouTypeCondition(page){
	let {pk_accountingbook} = page.state;
	let {nodeType} = page;
	let businessInfo = getBusinessInfo();
	
	const condition = {
		pk_group:'',
		pk_org:'',
	}
	if(nodeType === NODETYPE.ORG){
		condition.pk_org = pk_accountingbook;
	}else if(nodeType === NODETYPE.GLOBAL){
		condition.pk_org = "~";
		condition.pk_group = "~";
	}
	return condition;
}


function initState(page, context){
	const {nodeType} = page;
	let {pk_accountingbook, bookname} = page.state;
	if(nodeType === NODETYPE.ORG){
		pk_accountingbook = context.defaultAccbookPk;
		bookname = context.defaultAccbookName;
		page.getData(pk_accountingbook);
		page.getOrgInfo(pk_accountingbook);
	}
	page.setState({context, pk_accountingbook, bookname}, () => {
		page.updateColRefInfo(page.props, page.state);
		page.updateColInitInfo(page.props, page.state);
	});
}
