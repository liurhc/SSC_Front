import { ajax,base, toast, promptBox  } from 'nc-lightapp-front';
import {NODETYPE, TOAST_NO_BOOK} from '../../../consts'

export default function buttonClick(props, id) {
	switch (id) {
		case 'add':
			addRow(this);
			break;
		case "delete":
			delRows(this);
			break;
		case 'update':
			this.update();
			break;
		case 'save':
			this.saveData();
			break;
		case 'refresh':
			this.refresh();
			break;
		case 'cancel':
			cancelChange(this);
			break;
	}
}

function addRow(page){
	if(!page.state.pk_accountingbook){
		let warnStr;
		if(page.nodeType == NODETYPE.ORG){
			warnStr = page.state.json['20020VTCRL-000000']; /*请先选择财务核算账簿！*/
		}else{
			warnStr = page.state.json['20020VTCRL-000001']; /*请先选择账簿类型*/
		}
		toast({content:warnStr, color:'warning'});
		return;
	}
	let index = page.props.editTable.getNumberOfRows(page.tableCode);
	page.props.editTable.addRow(page.tableCode, index);
	page.editable = true;
	page.toggoleShow();
}

function delRows(page){
	promptBox({
		color:'warning',
		title: page.state.json['20020VTCRL-000008'],/* 国际化处理： 确认删除*/
		content: page.state.json['20020VTCRL-000009'],/* 国际化处理： 您确定要删除所选数据吗?*/
		beSureBtnName:page.state.json['20020VTCRL-000010'],/* 国际化处理： 是*/
		cancelBtnName:page.state.json['20020VTCRL-000011'],/* 国际化处理： 否*/
		beSureBtnClick: () => {
			page.delRows(page);
		}
	});	
}

function cancelChange(page){
	promptBox({
		color:'warning',
		title:page.state.json['20020VTCRL-000012'],/* 国际化处理： 取消*/
		content:page.state.json['20020VTCRL-000013'],/* 国际化处理： 确定要取消吗？*/
		beSureBtnName:page.state.json['20020VTCRL-000014'],/* 国际化处理： 确定*/
		cancelBtnName:page.state.json['20020VTCRL-000012'],/* 国际化处理： 取消*/
		beSureBtnClick:() => {page.cancelChange()}
	});
}


