export default function (props, moduleId , key, changerows, value, index, data) {
    if(key == 'ruleaccasoa'){
        dealWithAccasoa(changerows, data);
    }
}

/**
 * 处理科目信息变动
 * @param {科目参照选中值} refDatas 
 * @param {当前行数据} rowItem 
 */
function dealWithAccasoa(refDatas, rowItem){
    if(!rowItem.values.accasoalist){
        rowItem.values.accasoalist = {};
    }
    rowItem.values.accasoalist.value = [];
    if(refDatas && refDatas.length > 0){
        for(let refData of refDatas.values()){
            let accasoaItem = generalAccasoaItem(refData, rowItem);
            rowItem.values.accasoalist.value.push(accasoaItem);
        }
        rowItem.values.ruleaccasoa = generalRuleAccasoa(refDatas);
    }
}

/**
 * 生成凭证类别约束规则子表信息
 * @param {会计科目参照选中值} refData 
 * @param {当前行数据} rowItem 
 */
function generalAccasoaItem (refData, rowItem){
    let accasoa = {};
    accasoa.accountcode = refData.refcode;
    accasoa.pk_accasoa = refData.refpk;
    if(rowItem.values.pk_vouchertyperule && rowItem.values.pk_vouchertyperule.value){
        accasoa.pk_vouchertyperule = rowItem.values.pk_vouchertyperule.value;
    }
    accasoa.status = 0;
    return accasoa;
}

/**
 * 生成会计科目显示值
 * @param {科目参照选中值} refDatas 
 */
function generalRuleAccasoa(refDatas){
    let result = {};
    let ruleaccasoa = "";
    if(refDatas && refDatas.length > 0){
        for(let item of refDatas.values()){
            ruleaccasoa += item.refname + ',';
        }
    }
    if(ruleaccasoa.length > 0){
        ruleaccasoa = ruleaccasoa.substring(0, ruleaccasoa.length - 1);
    }
    result.value = ruleaccasoa;
    result.display = ruleaccasoa;
    return result;
}
