//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,cacheTools,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const { NCTabsControl,NCAffix} = base;
import { buttonClick, initTemplate, searchBtnClick,onAfterEvent } from './events';
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
import '../../../public/reportcss/firstpage.less';
class List extends Component {
	constructor(props) {
		super(props);
		this.moduleId = '2002';
		this.searchId = '20022035_query';
		this.tableId = '20022035_list';
		this.state = {
			querydata : {},
			json:{},
			inlt:null
		};
	}
	componentDidMount() {
	}
	componentWillMount(){
		let callback= (json,status,inlt) =>{
			this.setState({json:json,inlt},()=>{
				initTemplate.call(this, this.props);
			})
		}
        getMultiLang({moduleId:'20022018',domainName:'gl',currentLocale:'zh-CN',callback});
	}
	onRowDoubleClick = (record,index)=>{
		//小应用内列表跳卡片
		if(!this.props.getUrlParam('pk_periodschema'))
		{
			this.props.linkTo('/gl/contrastreportresult/pages/card/index.html',{
				pagecode:'20022035_web_card',
				status:'browse',
				id: record.pk_contrastreportcreate.value,
				begindate:cacheTools.get('reportresultbegindate'),
				enddate:cacheTools.get('reportresultenddate')
				//begindate: this.state.querydata.querycondition.conditions[3].value.firstvalue.substring(0,10),
				//enddate: this.state.querydata.querycondition.conditions[3].value.secondvalue.substring(0,10)
			})
		}
		//外部小应用跳转过来的列表跳卡片
		else
		{
			this.props.linkTo('/gl/contrastreportresult/pages/card/index.html',{
				pagecode:'20022035_web_card',
				status:'browse',
				id: record.pk_contrastreportcreate.value,
				year : this.props.getUrlParam('year'),
				month : this.props.getUrlParam('month'),
				pk_periodschema : this.props.getUrlParam('pk_periodschema')
			})
		}
	};

	render() {
		let { table, button, search } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		return (
			<div className="nc-bill-list" id="nc-bill-list-list">
				<HeaderArea
					title={this.state.json['20022035-000001']}/* 国际化处理： 对账结果查询*/
					btnContent={this.props.button.createButtonApp({
						area: 'contrastreportresult_list',
						buttonLimit: 4,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				/>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 6, //默认显示几个查询条件
						onAfterEvent: onAfterEvent.bind(this)
					})}
				</div>
				{/* <div style={{ height: '10px' }} /> */}
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						fieldid: 'reportresult',
						//showCheck: true,
						showIndex: true,
						onRowDoubleClick : this.onRowDoubleClick.bind(this)
					})}
				</div>
			</div>
		);
	}
}

List = createPage({
	//mutiLangCode: '2002'
})(List);

ReactDOM.render(<List />, document.querySelector('#app'));
