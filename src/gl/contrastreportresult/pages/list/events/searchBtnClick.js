import {ajax,cacheTools,toast} from 'nc-lightapp-front';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal,flag) {
    if(searchVal){
        let pageId = '20022035_web_list';
        let searchId = '20022035_query';
        let tableId = '20022035_list';
        let data={
            querycondition: searchVal,
            pagecode: pageId,
            querytype:'tree',
        };
        // this.setState({
        //     querydata:data
        // })
        cacheTools.set(searchId, searchVal);
        //查询区设置值
        searchVal.conditions.forEach((val)=>
        {
            if(val.field=='daterange')
            {
                cacheTools.set('reportresultbegindate',val.value.firstvalue);
                cacheTools.set('reportresultenddate',val.value.secondvalue);
            }
        })   
        ajax({
            url: '/nccloud/gl/contrast/contrastreportresultquery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        if(flag=='update')
                        {

                        }
                        else if(flag=='simple')
                        {
                            let len = res.data[tableId].rows.length;
                            toast({ color: 'success', content: this.state.inlt&&this.state.inlt.get('20022035-000002',{count:len}) });/* 国际化处理： 查询成功，共,条。*/
                        }
                        props.table.setAllTableData(tableId, data[tableId]);
                    }else{
                        if(flag=='update')
                        {

                        }
                        else if(flag=='simple')
                        {
                            toast({ color: 'warning', content: this.state.json['20022035-000004'] });/* 国际化处理： 未查询出符合条件的数据！*/
                        }
                        props.table.setAllTableData(tableId, {rows:[]});
                    }
                    
                }
            }
        });
    }
    
};
