import { ajax,toast,cacheTools } from 'nc-lightapp-front';
let searchId =  '20022035_query'
let field ='year'
 export default function onAfterEvent(field, val) {
    if(field =='pk_contrastrule')
	 {
		let self = this; 
		if(val.refpk)
		{
			//添加到缓存
			cacheTools.set('reportresultpk',val.refpk);
			//获取日期的默认值
			let pk_contrastrule = val.refpk;
			let sendData = {pk_contrastrule:pk_contrastrule};
			let url = '/nccloud/gl/contrast/contrastreportmakeinit.do';
			ajax({
				url:url,
				data:sendData,
				success: function(response){
					const { success } = response;
					if (success) {
						if(response.data){
							cacheTools.set('reportresultcontent',response.data.content);
						}	
					}   
				}
			});
		}
		else
		{
			setInitData(self.props,'pk_accountingbook',null,null);
			setInitData(self.props,'pk_oppaccountingbooks',null,null);
		}
	}
 }

function setInitData(props,field,display,value) {
  let data = {
            display:display,value:value
        };
    props.search.setSearchValByField(searchId,field,data)

}
