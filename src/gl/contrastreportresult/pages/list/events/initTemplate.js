import { createPage, ajax, base, toast,cacheTools } from 'nc-lightapp-front';
import clickSearchBtn from './searchBtnClick';
let { NCPopconfirm, NCIcon } = base;
let pageId = '20022035_web_list';
let searchId = '20022035_query';
let tableId = '20022035_list';

export default function (props) {
	let page = this;
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	cacheTools.set('reportresultappcode',appcode); 
	props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta, page)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				//小应用内跳转
				if (props.getUrlParam('jumpflag') == 'true') 
				{
					let searchVal = cacheTools.get(searchId);
					clickSearchBtn(props,searchVal,'update');
				}
				//别的小应用跳转过来
				if(props.getUrlParam('jumpflag') == 'false')
				{
					//给缓存设值，用于跳转卡片
					cacheTools.set('reportresultcontent',props.getUrlParam('content'));
					getJumpData(props,appcode);
				}
			}
		}
	)
}

function modifierMeta(props, meta, page) {
	//查询区
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		if (item.attrcode == 'pk_contrastrule') 
		{
			item.queryCondition = ()=> 
			{
				return {'GridRefActionExt':'nccloud.web.gl.ref.ContrastRuleRefSqlBuilderForContarstResult'}
			}
		}
		else if (item.attrcode == 'pk_accountingbook') 
		{
			item.isMultiSelectedEnabled = false;
			item.showGroup = false;
			item.showInCludeChildren = false;
			item.queryCondition = ()=> {
				return {
					appcode: cacheTools.get('reportresultappcode'),
					pk_contrastrule: cacheTools.get('reportresultpk'),
					isSelf:'Y',
					'TreeRefActionExt':'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule'
				}
			}
		}
		else if (item.attrcode == 'pk_oppaccountingbooks') 
		{
			item.isMultiSelectedEnabled = false;
			item.showGroup = true;
			item.showInCludeChildren = false;
			item.queryCondition = ()=> {
				return {
					appcode: cacheTools.get('reportresultappcode'),
					pk_contrastrule: cacheTools.get('reportresultpk'),
					isSelf:'N',
					'TreeRefActionExt':'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule'
				}
			}
		}
		return item;
	})
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'year') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{cursor: 'pointer' }}
						onClick={() => {
							//小应用内列表跳卡片
							if(!page.props.getUrlParam('pk_periodschema'))
							{
								page.props.linkTo('/gl/contrastreportresult/pages/card/index.html',{
									pagecode:'20022035_web_card',
									status:'browse',
									id: record.pk_contrastreportcreate.value,
									begindate:cacheTools.get('reportresultbegindate'),
									enddate:cacheTools.get('reportresultenddate')
									//begindate: this.state.querydata.querycondition.conditions[3].value.firstvalue.substring(0,10),
									//enddate: this.state.querydata.querycondition.conditions[3].value.secondvalue.substring(0,10)
								})
							}
							//外部小应用跳转过来的列表跳卡片
							else
							{
								page.props.linkTo('/gl/contrastreportresult/pages/card/index.html',{
									pagecode:'20022035_web_card',
									status:'browse',
									id: record.pk_contrastreportcreate.value,
									year : page.props.getUrlParam('year'),
									month : page.props.getUrlParam('month'),
									pk_periodschema : page.props.getUrlParam('pk_periodschema')
								})
							}
						}}
					>
						{record.year && record.year.value}
					</a>
				);
			};
		}
		return item;
	});
	return meta;
}
function getJumpData(props,appcode) {
	//总额对账查询跳转过来，初始化界面数据
	let data = {
		appcode:appcode,
		pagecode: pageId,
		pk_contrastrule: props.getUrlParam('pk_contrastrule'),
		year: props.getUrlParam('year'),
		month: props.getUrlParam('month'),
		pk_periodschema: props.getUrlParam('pk_periodschema'),
		pk_self: props.getUrlParam('pk_self'),
		pk_opp: props.getUrlParam('pk_opp')
	};

	ajax({
		url: '/nccloud/gl/corpcontrastresult/sumcontrastquerylinkqryresult.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableId, data[tableId]);
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
				}
			}
		}
	});
}
