import { base, ajax,cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm } = base;

let tableId = 'table';
let pageId = '20022035_linkcontrastdata_list';
export default function(props) {
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
				//初始化表格数据
				if (props.getUrlParam('status') == 'browse') {
				let param = {
					pk_contrastrule: props.getUrlParam('pk_contrastrule'),
					pk_self:props.getUrlParam('pk_self'),
					pk_opp: props.getUrlParam('pk_opp'),
					pk_currtype: props.getUrlParam('pk_currtype'),
					subjcode_self: props.getUrlParam('subjcode_self'),
					subjcode_opp: props.getUrlParam('subjcode_opp'),
					iscontrasted: props.getUrlParam('iscontrasted'),
					pagecode: pageId 
				};
				if (props.getUrlParam('begindate') != null) {
					param.begindate = props.getUrlParam('begindate');
					param.enddate = props.getUrlParam('enddate');
				  } else {
					param.year = props.getUrlParam('year');
					param.month = props.getUrlParam('month');
					param.pk_periodschema = props.getUrlParam('pk_periodschema');
				  }
				ajax({
					url: '/nccloud/gl/contrast/contrastreportresultlinkcontrastdataquery.do',
					data: param,
					success: (res) => {
						if (res.data) {
							props.cardTable.setTableData(tableId, res.data[tableId]);
							setQueryContent( props,cacheTools.get('reportresultcontent'))
						} else {
							props.cardTable.setTableData(tableId, { rows: [] });
						}
					}
				});
			}


			}   
		}
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[tableId].status = status;
	meta[tableId].items.forEach((item, key) => {
		return item;
		});
	return meta;
}
//设置隐藏列
function setQueryContent(props,content){
	let tableid = 'table';
	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid,['debitquantity','creditquantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['debitquantity','creditquantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid,['debitamount','creditamount']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['debitamount','creditamount']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid,['localdebitamount','localcreditamount']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['localdebitamount','localcreditamount']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid,['groupdebitamount','groupcreditamount']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['groupdebitamount','groupcreditamount']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid,['globaldebitamount','globalcreditamount']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['globaldebitamount','globalcreditamount']);
	}
}
