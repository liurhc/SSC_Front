//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast ,getMultiLang,createPageIcon} from 'nc-lightapp-front';
let { NCFormControl ,NCAffix} = base;
import { buttonClick, initTemplate } from './events';
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
import '../../../public/reportcss/firstpage.less';
class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = 'form';
		this.moduleId = '2002';
		this.tableId = 'table';
		this.pageid='20022035_web_card';
		this.back=this.back.bind(this,props);
		this.state={
			json:{}
		}
	}
	componentDidMount() {
		this.toggleShow();
		//查询单据详情
		// let data = { pk_contrastreportcreate: this.props.getUrlParam('id'), pagecode: this.pageid };
		// ajax({
		// 	url: '/nccloud/gl/contrast/contrastreportmakecard.do',
		// 	data: data,
		// 	success: (res) => {
		// 		if (res.data) {
		// 			if (res.data.head) {
		// 				this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
		// 			}
		// 			if (res.data.body) {
		// 				this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
		// 			}
		// 		} else {
		// 			this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
		// 			this.props.cardTable.setTableData(this.tableId, { rows: [] });
		// 		}
		// 	}
		// });
	}
	componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{
				initTemplate.call(this, this.props);
			})
		}
        getMultiLang({moduleId:'20022018',domainName:'gl',currentLocale:'zh-CN',callback});
	}
	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		let flag = status === 'browse' ? false : true;
		this.props.button.setButtonVisible([ 'return','refresh'], flag);
		this.props.button.setButtonVisible([ 'return', 'refresh' ], !flag);
		this.props.form.setFormStatus(this.formId, status);
		this.props.cardTable.setStatus(this.tableId, status);
	};

	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button';
		} else {
			return 'secondary - button';
		}
	};

	//获取列表肩部信息
	getTableHead = (buttons) => {
		let { createButton } = this.props.button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-search">
				</div>
				<div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(this.tableId, {
						iconArr: [ 'close', 'open', 'max' ],
						maxDestAreaId: 'finance-reva-revecontract-card'
					})}
					{buttons.map((v) => {
						if (v.btncode == 'add') {
							return createButton(v.btncode, {
								name: v.btnname,
								onButtonClick: buttonClick.bind(this),
								buttonColor: this.getButtonNames(v.btncode)
							});
						}
					})}
				</div>
			</div>
		);
	};

	back=(props)=>{
		props.linkTo('/gl/contrastreportresult/pages/list/index.html',{pagecode:'20022035_web_list',jumpflag: true})
	}

	render() {
		let { cardTable, form, button, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButton } = button;
		let { createModal } = modal;
		return (
			<div className="nc-bill-card">
				<HeaderArea
					initShowBackBtn={true}
					backBtnClick={this.back}
					title={this.state.json['20022035-000001']}/* 国际化处理： 对账结果查询*/
					btnContent={this.props.button.createButtonApp({
						area: 'contrastreportresult_card',
						buttonLimit: 10, 
						onButtonClick: buttonClick.bind(this), 
						popContainer: document.querySelector('.header-button-area')
						})}
				/>
				<div className="nc-bill-form-area">
					{createForm(this.formId, {
						fieldid: 'reportresult'
						//onAfterEvent: afterEvent.bind(this)
					})}
				</div>
				{
					<div className="nc-bill-table-area">
						{this.getTableHead(buttons)}
						{createCardTable(this.tableId, {
                        adaptionHeight:true,
							fieldid: 'reportresult',
							tableHead: this.getTableHead.bind(this, buttons),
							//showCheck: true,
							showIndex: true,
							hideSwitch:()=>{return false;}
						})}
					</div>
				}

				{createModal('delete', {
					//title: multiLang && multiLang.get('20521030-0020'),
					//content: multiLang && multiLang.get('20521030-0006'),
					beSureBtnClick: this.delConfirm
				})}
			</div>
		);
	}
}

Card = createPage({
	//mutiLangCode: '2052'
})(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
