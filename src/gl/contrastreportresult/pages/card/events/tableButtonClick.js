import { ajax, toast, base, cardCache, getMultiLang, cacheTools } from 'nc-lightapp-front';
let { NCMessage } = base;
let { setDefData, getDefData } = cardCache;
import { tableId, searchId } from '../config.js';
const tableButtonClick = (props, key, text, record, index, tableId, page) => {
    let data;
    let searchVal = cacheTools.get(searchId);
    switch (key) {
        // 表格操作按钮
        case 'linkcontrastdetail':
        if (record) {;
          let param = {
            pagecode:'20022035_linkcontrastdata_list',
            status: 'browse',
            pk_contrastrule: props.form.getFormItemsValue(page.formId,'pk_contrastrule').value,
            subjcode_self: record.values.accasoaselfcodes.value,
            subjcode_opp: record.values.accasoaothercodes.value,
            pk_self: record.values.pk_selfaccountingbook.display,
            pk_opp: record.values.pk_oppaccountingbook.display,
            pk_currtype: record.values.pk_currtype.value,
            iscontrasted: 'Y',
            id: page.props.getUrlParam('id')
          }
          if (page.props.getUrlParam('begindate') != null) {
            param.begindate = page.props.getUrlParam('begindate');
            param.enddate = page.props.getUrlParam('enddate');
          } else {
            param.year = page.props.getUrlParam('year');
            param.month = page.props.getUrlParam('month');
            param.pk_periodschema = page.props.getUrlParam('pk_periodschema');
          }
          props.openTo('/gl/contrastreportresult/pages/linkcontrastdata/index.html', param);
        }
        else
        {
          toast({color: 'warning', content: page.state.json['20022035-000000']});/* 国际化处理： 请选择一行数据*/
        }
        break
      case 'linkuncontrastdetail':
        if (record) {
          let param = {
            pagecode:'20022035_linkcontrastdata_list',
            status: 'browse',
            pk_contrastrule: props.form.getFormItemsValue(page.formId,'pk_contrastrule').value,
            subjcode_self: record.values.accasoaselfcodes.value,
            subjcode_opp: record.values.accasoaothercodes.value,
            pk_self: record.values.pk_selfaccountingbook.display,
            pk_opp: record.values.pk_oppaccountingbook.display,
            pk_currtype: record.values.pk_currtype.value,
            iscontrasted: 'N',
            id: page.props.getUrlParam('id')
          }
          if (page.props.getUrlParam('begindate') != null) {
            param.begindate = page.props.getUrlParam('begindate');
            param.enddate = page.props.getUrlParam('enddate');
          } else {
            param.year = page.props.getUrlParam('year');
            param.month = page.props.getUrlParam('month');
            param.pk_periodschema = page.props.getUrlParam('pk_periodschema');
          }
          props.openTo('/gl/contrastreportresult/pages/linkcontrastdata/index.html', param);
        }
        else
        {
          toast({color: 'warning', content: page.state.json['20022035-000000']});/* 国际化处理： 请选择一行数据*/
        }
        break
        default:
            break;
    }
};
export default tableButtonClick;
