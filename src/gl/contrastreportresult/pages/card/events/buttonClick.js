import { ajax, base, toast,cacheTools } from 'nc-lightapp-front';
export default function(props, id) {
  let rows = props.cardTable.getClickRowIndex(this.tableId);
  switch (id) {
    case 'back':
      if (this.props.getUrlParam('year') != null) {
        let param = {}
        param.year = this.props.getUrlParam('year');
        param.month = this.props.getUrlParam('month');
        param.pk_periodschema = this.props.getUrlParam('pk_periodschema');
        param.pk_self = this.props.getUrlParam('pk_self');
        param.pk_opp = this.props.getUrlParam('pk_opp');
        param.pk_contrastrule = this.props.getUrlParam('pk_contrastrule');
        props.linkTo('../../../corpcontrastreport/pages/linkresult/index.html', param);
      } else {
        props.linkTo('/gl/contrastreportresult/pages/list/index.html',{
          pagecode:'20022035_web_list',
          jumpflag:true
        });
      }
      break
    case 'refresh':
      let data = { pk_contrastreportcreate: this.props.getUrlParam('id'), pagecode: this.pageid };
      ajax({
        url: '/nccloud/gl/contrast/contrastreportmakecard.do',
        data: data,
        success: (res) => {
          if (res.data) {
            toast({ color: 'success', title:this.state.json['20022035-000003'] });/* 国际化处理： 刷新成功！*/
            if (res.data.head) {
              this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
            }
            if (res.data.body) {
              setQueryContent(this.props,cacheTools.get('reportresultcontent'));
              this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
            }
          } else {
            toast({ color: 'success', title:this.state.json['20022035-000003'] });/* 国际化处理： 刷新成功！*/
            this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
            this.props.cardTable.setTableData(this.tableId, { rows: [] });
          }
        }
      });
      break
    case 'linkcontrastdetail':
      if (rows) {
        let record = rows['record'].values;
        let param = {
          pagecode:'20022035_linkcontrastdata_list',
          status: 'browse',
          pk_contrastrule: props.form.getFormItemsValue(this.formId,'pk_contrastrule').value,
          subjcode_self: record.accasoaselfcodes.value,
          subjcode_opp: record.accasoaothercodes.value,
          pk_self: record.pk_selfaccountingbook.display,
          pk_opp: record.pk_oppaccountingbook.display,
          pk_currtype: record.pk_currtype.value,
          iscontrasted: 'Y',
          id: this.props.getUrlParam('id')
        }
        if (this.props.getUrlParam('begindate') != null) {
          param.begindate = this.props.getUrlParam('begindate');
          param.enddate = this.props.getUrlParam('enddate');
        } else {
          param.year = this.props.getUrlParam('year');
          param.month = this.props.getUrlParam('month');
          param.pk_periodschema = this.props.getUrlParam('pk_periodschema');
        }
        props.openTo('/gl/contrastreportresult/pages/linkcontrastdata/index.html', param);
      }
      else
      {
        toast({color: 'warning', content: this.state.json['20022035-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    case 'linkuncontrastdetail':
      if (rows) {
        let record = rows['record'].values;
        let param = {
          pagecode:'20022035_linkcontrastdata_list',
          status: 'browse',
          pk_contrastrule: props.form.getFormItemsValue(this.formId,'pk_contrastrule').value,
          subjcode_self: record.accasoaselfcodes.value,
          subjcode_opp: record.accasoaothercodes.value,
          pk_self: record.pk_selfaccountingbook.display,
          pk_opp: record.pk_oppaccountingbook.display,
          pk_currtype: record.pk_currtype.value,
          iscontrasted: 'N',
          id: this.props.getUrlParam('id')
        }
        if (this.props.getUrlParam('begindate') != null) {
          param.begindate = this.props.getUrlParam('begindate');
          param.enddate = this.props.getUrlParam('enddate');
        } else {
          param.year = this.props.getUrlParam('year');
          param.month = this.props.getUrlParam('month');
          param.pk_periodschema = this.props.getUrlParam('pk_periodschema');
        }
        props.openTo('/gl/contrastreportresult/pages/linkcontrastdata/index.html', param);
      }
      else
      {
        toast({color: 'warning', content: this.state.json['20022035-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    default:
      break
  }
}
//设置隐藏列
function setQueryContent(props,content){
	let tableid = 'table';
	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_quantity','selfcontrastedquantity','selfsumquantity','opp_un_quantity','oppcontrastedquantity','opp_sum_quantity','sumbalance_quantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_quantity','selfcontrastedquantity','selfsumquantity','opp_un_quantity','oppcontrastedquantity','opp_sum_quantity','sumbalance_quantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_cur','selfcontrastedcur','selfsumcur','opp_un_cur','oppcontrastedcur','opp_sum_cur','sumbalance_cur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_cur','selfcontrastedcur','selfsumcur','opp_un_cur','oppcontrastedcur','opp_sum_cur','sumbalance_cur']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_orgcur','selfcontrastedorgcur','selfsumorgcur','opp_un_orgcur','oppcontrastedorgcur','opp_sum_orgcur','sumbalance_orgcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_orgcur','selfcontrastedorgcur','selfsumorgcur','opp_un_orgcur','oppcontrastedorgcur','opp_sum_orgcur','sumbalance_orgcur']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_groupcur','self_contrastedgroupcur','selfsumgroupcur','opp_un_groupcur','oppcontrastedgroupcur','opp_sum_groupcur','sumbalance_groupcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_groupcur','self_contrastedgroupcur','selfsumgroupcur','opp_un_groupcur','oppcontrastedgroupcur','opp_sum_groupcur','sumbalance_groupcur']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_globalcur','selfcontrastedglobalcur','selfsumglobalcur','opp_un_globalcur','oppcontrastedglobalcur','opp_sum_globalcur','sumbalance_globalcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_globalcur','selfcontrastedglobalcur','selfsumglobalcur','opp_un_globalcur','oppcontrastedglobalcur','opp_sum_globalcur','sumbalance_globalcur']);
	}
}
