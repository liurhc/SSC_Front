import {Component} from 'react';
import {createPage,base,getMultiLang,createPageIcon} from 'nc-lightapp-front';
import BusinessUnitTreeRef from '../../../../uapbd/refer/org/BusinessUnitTreeRef';
import cardPageControllerChild from './events/cardPageControllerChild'
import presetVar from './presetVar'
import pubUtils from '../../../public/common/pubUtil'
import './index.less';
import {Header, HeaderButtonArea, HeaderSearchArea} from '../../../../fipub/public/components/layout/Header'
const {NCIcon: Icon, NCDiv} = base;

class Apportionrule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            headRef: {},
            headRef_disabled: false,
            copyFlag:true,
            json:{}
        };
        this.selectTreeId = '';
        
        // 页面初始化
        this.cardPageControllerChild = new cardPageControllerChild(this, props);
    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
           window.onbeforeunload = () => {
            let status=this.props.editTable.getStatus(presetVar.list);
            if (status == 'edit' || status == 'add') {
                return this.state.json['20020APPRULE-000000']/* 国际化处理： 确定要离开吗？*/
            }
        }
    }
    render() {
        const {editTable, button, DragWidthCom, syncTree, form} = this.props;
        let {createSyncTree} = syncTree;
        let { createForm } = form;
        const {createEditTable} = editTable;
        const {createButtonApp} = button;

        {/* 左树区域 tree-area*/}
        let leftDom = () => {
            let keyData=this.props.syncTree.getSyncTreeValue(presetVar.treeId);
            return(
                <div className="tree-area">
                {!keyData || keyData.length==0?
                <div className="compare-tree">
                    <div className="no-content-tip">
                        <div>
                            <p>
                                <Icon className="icon-tip uf-exc-c-o" />
                            </p>
                            <p>{this.state.json['20020APPRULE-000001']}，{this.state.json['20020APPRULE-000002']}！</p>{/* 国际化处理： 输入业务单元信息后,可新增分摊规则*/}
                        </div>
                    </div>
                </div>
                :''}
                    {createSyncTree({
                        defaultExpandAll: true,
                        treeId: presetVar.treeId,
                        needEdit: false,
                        needSearch: false,
                        onSelectEve: (data)=>{this.cardPageControllerChild.cfSelectTree(data)}
                    })}
                </div>
            )
        }
        {/* 右区域*/}
        let rightDom = () => {
            return (
                <div>
                    <div className="card-area right-input nc-theme-area-split-bc">
                        {/* 表单*/}
                        {createForm(presetVar.formId, {
                            onAfterEvent:(props, moduleId, key, value, oldValue)=>{this.cardPageControllerChild.cfFormChange(key, value, oldValue)}
                        })}
                    </div>
                    <NCDiv className="nc-single-table" areaCode={NCDiv.config.TABS}>
                        <div className="nc-singleTable-header-area">
                            <div className="header-button-area">
                                {/* 按钮*/}
                                {createButtonApp({
                                    area: presetVar.listShoulderButtonArea, 
                                    buttonLimit: 3,
                                    onButtonClick: (props, actionId)=>{this.cardPageControllerChild.headButtonClick(actionId)}
                                })}
                            </div>
                        </div>
                    </NCDiv>
                    <div className="table-area">
                        {/* 表格*/}
                        {createEditTable(presetVar.list, {
                            onAfterEvent: (props, moduleId, key, value, changedrows, index, record)=>{this.cardPageControllerChild.cfListChange(moduleId, key, value, changedrows, index, record)},                            
                            showIndex:true,
                            selectedChange:(data)=>{this.cardPageControllerChild.selectedChange(data)},
                            showCheck:true,
                            adaptionHeight: true,
                            cancelCustomRightMenu:true
                        })}
                    </div>
                </div>
            )
        }
        return (
            <div id="yxysdy" className="nc-single-table">
                <Header>
                    <HeaderSearchArea>
                        <span style={{color:"red"}} className="stara">*</span>
                        <BusinessUnitTreeRef
                            fieldid='buziunit_ref'
                            recode="uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef/index.js"
                            queryCondition={()=>{return {
                                TreeRefActionExt:'nccloud.web.org.accountingbook.action.OrgExtRef',
                                "isDataPowerEnable": 'Y'
                                // "DataPowerOperationCode" : 'fi'
                                //二级核算单位
                                // TreeRefActionExt:'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder',
                            }}}
                            disabled={this.state.headRef_disabled}
                            value={this.state.headRef}
                            onChange={(data)=>{this.cardPageControllerChild.cfHeadRefChange(data)}}
                        />
                    </HeaderSearchArea>
                    <HeaderButtonArea>
                        {createButtonApp({
                            area: presetVar.headButtonArea, 
                            buttonLimit: 3,
                            ignoreHotkeyCode:['Cancel'],
                            onButtonClick: (props, actionId)=>{this.cardPageControllerChild.headButtonClick(actionId)}
                        })}
                    </HeaderButtonArea>
                </Header>
                {/* 树表区域 tree-table*/}
                <div className="tree-table">
                    <DragWidthCom
                        leftDom={leftDom()}     //左侧区域dom
                        rightDom={rightDom()}     //右侧区域dom
                        defLeftWid='280px'      // 默认左侧区域宽度，px/百分百
                    />
                </div>

            </div>
        )
    }
}
Apportionrule = createPage({})(Apportionrule);
export default Apportionrule;
