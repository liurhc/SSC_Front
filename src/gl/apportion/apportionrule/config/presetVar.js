/**页面全局变量 */
let currentVar = {
    pageId: 'yxysdy',
    list: 'apportionlist',
    formId: 'apportion',
    treeId: 'apportiontree',
    searchArea: 'searchArea',
    headArea: 'apportion',
    bodyArea: 'apportionlist',
    headButtonArea: 'card_head',
    listButtonArea: 'card_body_inner',
    listShoulderButtonArea: 'card_body',
    appcode:'20020APPRULE',
    /**
     * 表单字段
     */
    formField:{
        /**
         * 分摊对象
         */
        pk_accassitem:'pk_accassitem',
        /**
         * 分摊方式
         */
        apportiontype:'apportiontype'
    },
    listField:{
        itempercent:'itempercent',
    }
}
window.presetVar = {
    ...currentVar
};
export default currentVar
