import {ajax } from 'nc-lightapp-front';
import pubUtils from '../../../public/common/pubUtil'
let requestApiOverwrite = {
    /**
     * 查树接口
     */
    queryTree: (opt) => {
        ajax({
            url: '/nccloud/gl/apportion/ApportionRuleTreeQueryAction.do',
            data: opt.data,
            async:false, 
            success: opt.success
        });
    },
    /**
     * 查表接口
     */
    queryData: (opt) => {
        ajax({
            url: '/nccloud/gl/apportion/ApportionRuleQueryAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    /**
     * 查模板接口
     */
    queryTemplate: (opt) => {
        ajax({
            url: '/nccloud/gl/apportion/ApportionRuleTemplateAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    /**
     * 保存接口
     */
    save: (opt) => {
        ajax({
            url: '/nccloud/gl/apportion/ApportionRuleSaveAction.do',
            data: {...opt.data, ...{['userjson']: JSON.stringify(pubUtils.getSysCode(opt.props))}},
            success: opt.success
        });
    },
    /**
     * 删除接口
     */
    delete: (opt) => {
        ajax({
            url: '/nccloud/gl/apportion/ApportionRuleDeleteAction.do',
            async:false, 
            data: {...opt.data, ...{['userjson']: JSON.stringify(pubUtils.getSysCode(opt.props))}},
            success: opt.success
        });
    }
}

export default  requestApiOverwrite;
