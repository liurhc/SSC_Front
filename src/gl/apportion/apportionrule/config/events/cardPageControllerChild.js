import {base,promptBox,deepClone,toast} from 'nc-lightapp-front';
const {NCMessage} = base;
import cardPageController from '../../../../public/common/cardPageController'
import changeListTemplate from '../../../../public/common/changeListTemplate'
import pubUtils from '../../../../public/common/pubUtil'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
import {cacheTools} from 'nc-lightapp-front';
import getDefaultAccountBook from '../../../../public/components/getDefaultAccountBook.js'
import {VOStatus} from '../../../../public/common/consts'
class cardPageControllerChild extends cardPageController{
    constructor(main, props, meta) {
        let newMeta = {
            ...meta, 
            headId: (meta || {}).headId || presetVar.headArea,
            bodyId: (meta || {}).bodyId || presetVar.bodyArea,
            formId: (meta || {}).bodyId || presetVar.formId,
            listIds: (meta || {}).listIds || [presetVar.list],
            listButtonAreas: (meta || {}).listButtonAreas || {[presetVar.list]:presetVar.listButtonArea}
        };
        super(main, props, newMeta);
    }
    /**
     * 重新初始化，页面打开固定为浏览态
     */
    initTemplateSetPageState(data){
        this.setPageState('browse');
        this.setButtonStatus(this.disabledButtons,true);
        this.setButtonStatusVisible(["CancelTable","CopyToEnd"],false);
        this.setButtonStatus(['Add','Refresh'],true);
        if(data.context.pk_org!='' && data.context.pk_org !=null && data.context.pk_org!=undefined){
            let changeData={
                refcode:data.context.org_Name,
                refname:data.context.org_Name,
                refpk:data.context.pk_org
            }
           this.cfHeadRefChange(changeData);
        }
    }

    initMeta(meta){
        meta[this.headId].items.map((item) => {
            if(item.attrcode == 'pk_accassitem'){
                item.queryCondition = () => {
                    return {GridRefActionExt:'nccloud.web.gl.ref.AccAssItemRefnodeSqlBuilder'};
                }
            }
        });
    }
    
    /**
     * 重新新增按钮事件，增加表头参照选择判断
     */
    Add(){
        if((this.main.state.headRef || {}).refpk == null){
            // 2002-0005: 请先选择
            toast({color:"warning",content:this.main.state.json['2002-0005']});
            return;
        }

        super.Add();
        this.main.props.form.setFormItemsValue(this.formId, {apportiontype:{value:"1"}});
        let meta = this.cfChangeListTemplate({items:[]});
        meta[presetVar.list].items.map((item)=>{
            if(item.attrcode == presetVar.listField.itempercent){
                item.visible = true;
            }
        });
        this.main.props.meta.setMeta(meta);
        this.main.setState({headRef_disabled: true});
        this.props.syncTree.setNodeDisable(presetVar.treeId, true);
    }
    /**
     * 重新修改按钮事件，增加树选择判断
     */
    Edit(){
        if(this.main.selectTreeId == ''){
            let multiLang = this.props.MutiInit.getIntl(2002); 
            // 2002-0005: 请先选择
            toast({color:"warning",content:this.main.state.json['2002-0005']});
        }else{
            super.Edit();
            this.main.setState({headRef_disabled: true});
            this.props.syncTree.setNodeDisable(presetVar.treeId, true);
        }
    }
    /**
     * 删除事件
     */
    Delete(){
        promptBox({
            color:"info",
            title: this.main.state.json['20020APPRULE-000003'],/* 国际化处理： 删除*/
            content: this.main.state.json['20020APPRULE-000004'],/* 国际化处理： 确定要删除？*/
            beSureBtnClick: this.doDelete.bind(this)
        });
    }
    doDelete(){
        if(this.main.selectTreeId == ''){
            let multiLang = this.props.MutiInit.getIntl(2002); 
            // 2002-0005: 请先选择
            toast({color:"warning",content:this.main.state.json['2002-0005']});  
            return;
        }
        requestApi.delete({
            props: this.props,
            data: {pk_apportionRule: this.id},
            success: (data) => {
                pubUtils.showMeggage(this.props, pubUtils.MESSAGETYPE.DELETE,this);
            }
        })
        this.cfQueryTree(this.main.state.headRef);
        let keyData=this.props.syncTree.getSyncTreeValue(presetVar.treeId);
        if(keyData[0].children){
            if(keyData[0].children.length>0){
                this.cfSelectTree(keyData[0].children[0].key); 
            }else{
                this.cfSelectTree(''); 
            }
        }else{
            this.cfSelectTree('');
        }
    }
    /**
     * 重写取消按钮事件增加表头参照和树状态控制 
     */
    Cancel(){
        super.Cancel();
        this.main.setState({headRef_disabled: false});
        this.props.syncTree.setNodeDisable(presetVar.treeId, false);
        if(this.main.selectTreeId){
            this.cfQueryData(this.main.selectTreeId);
        }
    }
    /**
     * 保存动作
     * @param {*data} 页面数据
     */
    doSave(data){
        // 校验分摊比例
        let itempercent = 0;
        let allRows = this.props.editTable.getAllRows(presetVar.list, true);
        allRows.map((one)=>{
            if(one.status != 3){
                itempercent += parseFloat(one.values.itempercent.value);
            }
        })
        if(this.props.form.getFormItemsValue(presetVar.formId, presetVar.formField.apportiontype).value != '2' && itempercent!=100){
            let multiLang = this.props.MutiInit.getIntl(2002); 
            // 2002-0005: 分摊比例合计必须为100
            toast({color:"warning",content:this.main.state.json['20020APPRULE-000008']});
            // NCMessage.create({content: this.main.state.json['20020APPRULE-000008'], color: 'warning', position: 'bottomRight'});      
            return;
        }
        data[presetVar.formId].rows[0].values.pk_unit = {value: this.main.state.headRef.refpk}
        requestApi.save({
            props: this.props,
            data: data,
            success: (data) => {
                this.callBackSave(data);
                if(data.data[presetVar.treeId] != null){
                    let newTree = this.props.syncTree.createTreeData(data.data[presetVar.treeId].nodes); 
                    this.props.syncTree.setSyncTreeData(presetVar.treeId, newTree);
                }else{
                    this.props.syncTree.setSyncTreeData(presetVar.treeId, []);
                }
                this.main.setState({headRef_disabled: false});
                this.props.syncTree.setNodeDisable(presetVar.treeId, false);
            }
        })
    }
    // 自定义函数--------------------------------------------------------------
    /**
     * 参照选择表更事件
     * @param {*data} 变更后参照数据（参照）
     */
    cfHeadRefChange(data){
        this.main.setState({headRef : data});
        this.clearPageData();
        this.cfQueryTree(data);
    }
    /**
     * 查询树
     * @param {*} queryKey 
     */
    cfQueryTree(queryKey){
        this.setButtonStatus(['Add','Refresh'],true);
        if(queryKey != null && queryKey.refpk != null){
            requestApi.queryTree({
                data: {pk_unit:queryKey.refpk},
                success: (data) => {
                    let newTree = this.props.syncTree.createTreeData(data.data.nodes); 
                    this.props.syncTree.setSyncTreeData(presetVar.treeId, newTree);
                }
            })
            this.setButtonStatus(['Add','Refresh'],false);
        }else{
            this.props.syncTree.setSyncTreeData(presetVar.treeId, []);
        }
    }
    /**
     * 树节点选择事件
     * @param {string data} 选择的节点PK
     */
    cfSelectTree(data){
        if(data!=null && data!='' && data!='~'){
            this.main.selectTreeId = data;
            this.cfQueryData(data);
            this.setButtonStatus(this.disabledButtons,false);
        }else{
            this.main.selectTreeId = '';
            this.setPageData({});
            this.setButtonStatus(this.disabledButtons,true);
        }
    }
    /**
     * 查询表
     * @param {*} data 
     */
    cfQueryData(queryKey){
        requestApi.queryData({
            data: {pk_apportionRule: queryKey,...pubUtils.getSysCode(this.props)},
            success: (data) => {
                if(data){
                    if(data.data){
                        this.cfChangeListTemplate(data.data.template[presetVar.list]);
                        this.setPageData(data);
                        this.setId(data.data.apportion.apportion.rows[0].values.pk_apportionrule.value);
                    }
                }
                if(((data.data) || {}).apportion){
                    this.cfFormChange(presetVar.formField.apportiontype, data.data.apportion.apportion.rows[0].values.apportiontype);
                }
            }
        })
    }
    /**
     * 表单变更事件
     * @param {*} props 
     * @param {*} moduleId 
     * @param {*} key 
     * @param {*} value 
     * @param {*} oldValue 
     */
    cfFormChange(key, value, oldValue){
        // 分摊对象
        if(key == presetVar.formField.pk_accassitem){
            if(value != null && value.value != null && value.value != ''){
                let pk_accassitems = value.value.split(',');
                if(pk_accassitems.length>9){
                    toast({color:"danger",content:this.main.state.json['20020APPRULE-000005']});/* 国际化处理： 辅助项目不超过9个。*/
                }else{
                    requestApi.queryTemplate({
                        data: {pk_accassitems: pk_accassitems, ...pubUtils.getSysCode(this.props)},
                        success: (data) => {
                            let meta=this.cfChangeListTemplate(data.data[presetVar.list]);
                            meta[presetVar.list].items.map((one)=>{
                                if(one.itemtype=="refer"){
                                    let pk_defdoclist='';
                                    one.fieldDisplayed='refname';
                                    for(let i=0;i<pk_accassitems.length;i++){
                                         if(value[''+i].values){
                                             if(value[''+i].refpk==one.attrcode.replace("##","")){
                                                if(value[''+i].values.classid){
                                                    pk_defdoclist=value[''+i].values.classid.value;
                                                }
                                             }
                                         }
                                    }
                                    one.isShowUnit=true;
                                    one.queryCondition = () => {
                                        return {
                                            "pk_org":this.main.state.headRef.refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "isShowUnit":true,
                                            "pk_defdoclist":pk_defdoclist,
                                            "classid":pk_defdoclist,
                                            busifuncode:'all'
                                        };
                                    },
                                    one.unitCondition = () => {
                                        return {
                                            pk_financeorg: this.main.state.headRef.refpk,
                                            'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                                        };
                                    }
                                }
                                if(one.attrcode=='opr'){
                                    one.fixed="right";
                                }
                            })
                            this.props.meta.setMeta(meta);
                        }
                    })
                }              
            }else{
                this.cfChangeListTemplate({items:[]});
            }

            this.updateTableData(this, value, oldValue);
        }else if(key == presetVar.formField.apportiontype){
            let meta = this.props.meta.getMeta();
            meta[presetVar.list].items.map((one)=>{
                if(one.attrcode == 'itempercent'){
                    // 平均分摊
                    if(value.value == '2'){
                        one.visible=false;
                    }else{
                        one.visible=true;
                    }
                }
            })
            this.props.meta.setMeta(meta);
        }
    }

    /**
     * 分摊对象改变后调用此方法更改子表表体内容
     * @param {*} value 
     * @param {*} oldValue 
     */
    updateTableData(page, value, oldValue){
        let needRmKeys = [];
        let keepKeyMap = {};
        if(value && value.value){
            value.value.split(',').map((item) => {keepKeyMap[item] = true});
        }
        if(oldValue && oldValue.value){
            oldValue.value.split(',').map((item) => {
                if(!keepKeyMap[item]) needRmKeys.push(`##${item}`);
            })
        }
        if(needRmKeys.length <= 0) return;
        let allRows = page.props.editTable.getAllRows(presetVar.list, true);
        console.log(allRows);
        if(allRows != null){
            allRows.map((row) => {
                needRmKeys.map((key) => {delete row.values[key]});
                if(row.status == VOStatus.UNCHANGED)
                    row.status = VOStatus.UPDATED;
            })
        }

        page.props.editTable.setTableData(presetVar.list, {rows:allRows});
    }


    cfListChange(moduleId, key, value, changedrows, index, record){
        if(key != 'itempercent'){
            if(value.length>1){
                let indexL = index;
                let copyData = this.props.editTable.getClickRowIndex(presetVar.list).record;
                copyData = deepClone(copyData);
                if(copyData){
                    let data=[];
                    copyData.values.pk_appdetail = {value:''};
                    value.map((one, indexR)=>{
                        if(indexR == 0){
                            // 平台问题，无法再同步事件中使用setValByKeyAndIndex方法
                            window.setTimeout(()=>{
                                this.props.editTable.setValByKeyAndIndex(presetVar.list, index, key, {
                                    value: one.refpk,
                                    display: one.refname
                                });
                            },100);
                        }else{
                            copyData.values[key]= {
                                value: one.refpk,
                                display: one.refname
                            }
                            let clone = deepClone(copyData);
                            data.push({values:clone.values});
                            // copyData.values[key]=clone;
                            // this.props.editTable.addRow(presetVar.list, indexL, false, clone.values);
                            // indexL = indexL + 1;   
                        }
                    })
                    this.props.editTable.insertRowsAfterIndex(presetVar.list, data,index);
                }
            }
        }
    }
    /**
     * 更新表体模板
     * @param {*} data 
     */
    cfChangeListTemplate(data){
        (data.items || []).map((one)=>{
            one.isMultiSelectedEnabled = true;
        })
        let pk_accassitems=data.items;
        let meta=changeListTemplate(this.props, presetVar.list, data, ['numberindex'], ['itempercent','ts','opr']);
        meta[presetVar.list].items.map((one)=>{
            if(one.itemtype=="refer"){
                let pk_defdoclist='';
                if(one.color){
                    pk_defdoclist=one.color;
                }
                one.fieldDisplayed='refname';
                one.isShowUnit=true;
                one.queryCondition = () => {
                    return {
                        "pk_org":this.main.state.headRef.refpk,
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi',
                        "isShowUnit":true,
                        "pk_defdoclist":pk_defdoclist,
                        "classid":pk_defdoclist,
                        busifuncode:'all'
                    };
                },
                one.unitValueIsNeeded = false;
                one.unitCondition = () => {
                    return {
                        pk_financeorg: this.main.state.headRef.refpk,
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    };
                }
            }
            if(one.attrcode=='opr'){
                one.fixed="right";
            }
        })
        this.props.meta.setMeta(meta);
        return meta;
    }

    /**
     * 页面状态变更处理
     * @param {*} state 
     */
    setPageState(state){
        super.setPageState(state);
        switch(state){
            case 'browse':
            this.setButtonStatusVisible(["DelLine","CancelTable"],false);
            break;
            case 'add':
            this.setButtonStatusVisible(["DelLine"],true);
            this.setButtonStatusVisible(["CancelTable","CopyToEnd"],false);
            this.setButtonStatus(['DelLine','Copy'],true);
            break;
            case 'edit':
            this.setButtonStatusVisible(["DelLine"],true);
            this.setButtonStatusVisible(["CancelTable","CopyToEnd"],false);
            this.setButtonStatus(['DelLine','Copy'],true);
            break;
        }
    }
    /**
     * 刷新
     * @param {*} data 
     */
    doRefresh(data){
       let org=this.main.state.headRef;
       if(null!=org.refpk && org.refpk!='' && org.refpk!=undefined){
            this.cfHeadRefChange(org);
       }
       let selectData=this.props.syncTree.getSelectNode(presetVar.treeId);
       if(selectData){
        this.cfSelectTree(selectData.key);
       }
    }
    /**
     * 取得当前条显示按钮
     * @param {*} listAreaId 
     * @param {*} text 
     * @param {*} record 
     * @param {*} index 
     */
    getListButtons(listAreaId, text, record, index){
        if(this.main.state.copyFlag==true){
            return ['DelLine','Copy','insertLine'];
        }else{
            return ['CopyToThis'];
        }
    }
    /**
     * 添加处理列
     * @param {*} listAreaId 
     */
    addOpr(listAreaId){
        let meta = this.props.meta.getMeta();
        let event = {
            label: this.main.state.json['20020APPRULE-000006'],/* 国际化处理： 操作*/
            attrcode: 'opr',
            itemtype: 'customer',
            visible: true,
            width: '150px',
            fixed:'right',
            render: (text, record, index) =>{
                return this.props.button.createOprationButton(this.getListButtons(), {
                    area: this.listButtonAreas[listAreaId],
                    buttonLimit: 3,
                    onButtonClick: (props, btnKey) => {
                        this.listButtonClick(listAreaId, btnKey, record, index);
                    }
                });
            }
        };
        meta[listAreaId].items.push(event);
    }

    /**
     * 行复制事件
     * @param {*} AreaId 
     * @param {*} data 
     * @param {*} index 
     */
    Copy(AreaId, data, index){
        this.props.editTable.pasteRow(AreaId, data,index);
        this.props.editTable.setValByKeyAndIndex(AreaId, index+1, "pk_appdetail", { value:'', display:'', scale })

    }

    CopyToThis(AreaId, data, index){
        let  cachedata = cacheTools.get('copyData');
        let clone = deepClone(cachedata);
        // let number=this.props.editTable.getNumberOfRows(AreaId);
        let datas=[];
        if(clone!=null){
            for(let i=0;i<clone.length;i++){
                clone[i].data.values.pk_appdetail={display:null,value:null};
                datas.push({values:clone[i].data.values});
            }
        }
        this.props.editTable.insertRowsAfterIndex(AreaId, datas,index);
    }

    /**
     *复制按钮事件
     */
    CopyAll(){
        let selectRows=this.props.editTable.getCheckedRows(presetVar.list);
        if(selectRows.length>0){
            cacheTools.set('copyData', selectRows);
            this.main.state.copyFlag=false;
            this.setButtonStatusVisible(["CopyToEnd","CancelTable"],true);
            this.setButtonStatusVisible(["AddLine","DelLine","Copy"],false);
            this.props.meta.setMeta(this.props.meta.getMeta());
        }
    }

    CancelTable(){
        this.main.state.copyFlag=true;
        this.setButtonStatusVisible(["AddLine","DelLine","Copy"],true);
        this.setButtonStatusVisible(["CopyToEnd","CancelTable"],false);
        this.props.meta.setMeta(this.props.meta.getMeta());
    }
    selectedChange(){
        let selectRows=this.props.editTable.getCheckedRows(presetVar.list);
        if(selectRows && selectRows.length>0){
            this.setButtonStatus(['DelLine','Copy'],false);
        }else{
            this.setButtonStatus(['DelLine','Copy'],true);
        }     
    }

    AddLine(){
        this.main.state.copyFlag=true;
        this.listIds.map((one)=>{
            this.props.editTable.addRow(one);
        })
    }
}export default cardPageControllerChild;
