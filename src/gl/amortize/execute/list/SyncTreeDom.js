//主子表列表

import React, { Component} from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,cardCache,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const {NCScrollElement} = base;
import {initTemplate, pageInfoClick, tableModelConfirm,afterEvent,buttonClick} from './events';
import './index.less';
let {setDefData, getDefData } = cardCache;
import HeaderArea from '../../../public/components/HeaderArea';
import AccBookRef from '../../../../uapbd/refer/org/AccountBookGridRef/index';
import AccPeriodDefaultTree from '../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index';
class List extends Component {
	constructor(props) {  
		super(props);
		this.pageCode = '2002150602_amorexe';
		this.tableId = 'amortize';
		this.formId  = 'amortizeform';
		this.state = {
			showSealDataFlag : false,
			pk_accountingbook : '',
			accountingbookname:'',
			pageData : [],
			date:'',
			value:{},
			json:{}
		}
		this.ViewModel=this.props.ViewModel;
	}
	componentDidMount() {
		
	}

	componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{
				initTemplate.call(this,this.props);
			})
		}
        getMultiLang({moduleId:'2002150602',domainName:'gl',currentLocale:'zh-CN',callback});
	}

	getData = (props) => {
		let DefData =getDefData("queryCondition", "gl.amortize.execute.pk_amortize");
		let value=this.state.pk_accountingbook;
		let pk_accountingbook = props.form.getFormItemsValue(this.formId,'pk_accountingbook');
        let date = props.form.getFormItemsValue(this.formId,'pk_group');
		let flag = props.form.getFormItemsValue(this.formId,'flag');
		let desciption = props.form.getFormItemsValue(this.formId,'desciption');
		if(flag.value==null){
			flag={diaplay:null,value:false};
		}
		if(desciption.value==null){
			desciption={diaplay:this.state.json['2002150602-000000'],value:false};/* 国际化处理： 否*/
		}
		let meta=props.meta.getMeta();
		meta[this.formId].items.map((item, key) => {
			if (item.attrcode == 'pk_group') {
				let url='/nccloud/gl/glpub/queryBizDate.do';
				let pk_accountingbook=value;
				let data={
					"pk_accountingbook":pk_accountingbook
				}
				let pk_accperiodscheme=null;
				ajax({
					url : url,
					data : data,
					async:false, 
					success : (success) => {
						if(success){
							pk_accperiodscheme=success.data.pk_accperiodscheme;
						}
					}
				});
				item.queryCondition = () => {
					return {pk_accperiodscheme:pk_accperiodscheme,
						GridRefActionExt:'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder',
						"isDataPowerEnable": 'Y',"DataPowerOperationCode" : 'fi'};
				}
			}
			return item;
		});
		props.meta.setMeta(meta);

		let data = {
			showSealDataFlag : flag.value,
			pk_accountingbook : pk_accountingbook.value,
			date:date.value,
			diaplaydate:date.display,
			desciption:desciption.value,
			pageId : this.pageCode
		};
		ajax({
			url: '/nccloud/gl/amortize/queryExecute.do',
			data: data,
			success: (res) => {
				if (res.data) {
					if (res.data.head) {
						this.props.form.setAllFormValue({[this.formId]:res.data.head[this.formId]});
					}
					if (res.data.body) {
						if(res.data.body[this.tableId])
						{
							this.props.table.setAllTableData(this.tableId, res.data.body[this.tableId]);
						}
						else
						{
							this.props.table.setAllTableData(this.tableId, { rows: [] });
						}
					}else{
						this.props.table.setAllTableData(this.tableId, { rows: [] });
					}
				}
			}
		});
	};

	onRowDoubleClick = (record, index) => {
		this.props.linkTo('../card/index.html', {
			id : record['pk_amortize'].value,
			status : 'browse'
		});
	}

	onSelected(){
		let rows = this.props.table.getCheckedRows(this.tableId);
		if(rows.length>0){
			let showOrHidden=false;
			for(let i=0;i<rows.length;i++){
				 if( rows[i].data.values.state.value!='1'){
					showOrHidden=true;
				 }
			}
			this.props.button.setDisabled({
				linkVoucher: showOrHidden,
				execute:false
			});
		}else{
			this.props.button.setDisabled({
				linkVoucher: true,
				execute:true
			});
		}
	}
	onSelectedAll(){
		let rows = this.props.table.getCheckedRows(this.tableId);
		if(rows.length>0){
			let showOrHidden=false;
			for(let i=0;i<rows.length;i++){
				 if( rows[i].data.values.state.value!='1'){
					showOrHidden=true;
				 }
			}
			this.props.button.setDisabled({
				linkVoucher: showOrHidden,
				execute:false
			});
		}else{
			this.props.button.setDisabled({
				linkVoucher: true,
				execute:true
			});
		}
	}
	render() {
		let { table, button, search ,form} = this.props;
		let { createForm } = form;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		let status="edit";
		return (
			<div className="nc-bill-list amortize-execute">	
				<HeaderArea
					title={this.state.json['2002150602-000005']}
					btnContent={
						this.props.button.createButtonApp({
							area: 'head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
				/>
				{/* <NCScrollElement name='forminfo'> */}
					<div className="nc-bill-search-area">
						<div className="nc-bill-form-area nc-theme-gray-area-bgc">
							{createForm(this.formId, {
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					</div>
				{/* </NCScrollElement> */}
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						showIndex : true,
						showCheck : true,
						// onRowDoubleClick : this.onRowDoubleClick,
						onSelected: this.onSelected.bind(this),
						onSelectedAll:this.onSelectedAll.bind(this),
					})}
				</div>
			</div>
		);
	}
}
  
List = createPage({
	// initTemplate: initTemplate,
	mutiLangCode: '2052'
})(List);


export default List
// ReactDOM.render(<List />, document.querySelector('#app'));
