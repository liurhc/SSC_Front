import {toast, ajax} from 'nc-lightapp-front';

export default function afterEvent(props, moduleId, key,value, changedrows, i, s, g) {
	if (key != 'desciption') {
		if(key=='pk_group'){
			if(!value.value){
				this.props.table.setAllTableData(this.tableId, { rows: [] });
				return;
			}
		}
		let pk_accountingbook = props.form.getFormItemsValue(this.formId,'pk_accountingbook');
        let date = props.form.getFormItemsValue(this.formId,'pk_group');
		let flag = props.form.getFormItemsValue(this.formId,'flag');
		let desciption = props.form.getFormItemsValue(this.formId,'desciption');
		if(flag.value==null){
			flag={diaplay:null,value:false};
		}
		if(desciption.value==null){
			desciption={diaplay:this.state.json['2002150602-000000'],value:false};/* 国际化处理： 否*/
		}
		if(pk_accountingbook.value==null || pk_accountingbook.value==''){
			toast({ content: this.state.json['2002150602-000001'], color: 'warning'});/* 国际化处理： 请选择核算账簿*/
		}else{
			if(key=='pk_accountingbook'){
				let meta=props.meta.getMeta();
				meta[this.formId].items.map((item, key) => {
					if (item.attrcode == 'pk_group') {
						let url='/nccloud/gl/glpub/queryBizDate.do';
						let pk_accountingbook=value.value;
						let data={
							"pk_accountingbook":pk_accountingbook
						}
						let pk_accperiodscheme=null;
						ajax({
							url : url,
							data : data,
							async:false, 
							success : (success) => {
								if(success){
									pk_accperiodscheme=success.data.pk_accperiodscheme;
								}
							}
						});
						item.queryCondition = () => {
							return {pk_accperiodscheme:pk_accperiodscheme,
								GridRefActionExt:'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder',
								"isDataPowerEnable": 'Y',"DataPowerOperationCode" : 'fi'
							};
						}
					}
					return item;
				});
				props.meta.setMeta(meta);
				date={display:"",value:""};
			}
			let data = {
				showSealDataFlag : flag.value,
				pk_accountingbook : pk_accountingbook.value,
				date:date.value,
				diaplaydate:date.display,
				desciption:desciption.value,
				pageId : this.pageCode
			};
			ajax({
				url: '/nccloud/gl/amortize/queryExecute.do',
				data: data,
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({[this.formId]:res.data.head[this.formId]});
						}
						if (res.data.body) {
							if(res.data.body[this.tableId])
							{
								this.props.table.setAllTableData(this.tableId, res.data.body[this.tableId]);
							}
							else
							{
								this.props.table.setAllTableData(this.tableId, { rows: [] });
							}
						}else{
							this.props.table.setAllTableData(this.tableId, { rows: [] });
						}
						this.props.button.setDisabled({
							execute:true,
							linkVoucher:true,
						});
					}
				}
			});
		}
	}
       
}
