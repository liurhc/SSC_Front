import {toast, ajax,cardCache} from 'nc-lightapp-front';
let {setDefData } = cardCache;
import { pushToGenVoucher,pushToLinkVoucher } from '../../../../public/common/voucherUtils';


export default function buttonClick(props, id) {
    switch (id) {
        case 'execute': execute(this); break;
        case 'linkVoucher':     linkVoucher(this); break;
        case 'refresh': refresh(this,this.props); break;
    }
}




function execute(page){
    let pk_accountingbook = page.props.form.getFormItemsValue(page.formId,'pk_accountingbook');
    let date = page.props.form.getFormItemsValue(page.formId,'pk_group');
    let flag = page.props.form.getFormItemsValue(page.formId,'flag');
    let desciption = page.props.form.getFormItemsValue(page.formId,'desciption');
    let pks = getSelecedDataId(page);
    let pagecode= page.pageCode;
    let pk="";
    if(flag.value==null){
        flag={diaplay:null,value:false};
    }
    if(desciption.value==null){
        desciption={diaplay:page.state.json['2002150602-000000'],value:false};/* 国际化处理： 否*/
    }
    if(null!=pks && pks.length>0){
        for(let i=0;i<pks.length;i++){
            pk += pks[i]+",";
        }
        pk=pk.substr(0,pk.length-1);
    }
    let data={
        "pagecode":pagecode,
        "pk_accountingbook":pk_accountingbook.value,
        "date":date.display,
        "flag":flag.value,
        "pks":pk,
        "untallied":desciption.value,
        "dateValue":date.value,
    }
    setDefData("queryCondition", "gl.amortize.execute.pk_amortize", data);
    let url = '/nccloud/gl/amortize/amortizeExecute.do';
    let backPageCode = page.props.getUrlParam('pagecode') || page.props.getSearchParam('p');
    let backAppCode = page.props.getUrlParam('appcode') || page.props.getSearchParam('c');
    ajax({
        url : url,
        data : data,
        success : (success) => {
            if(success){
                let param={
                    voucher: success.data,
                    titlename:page.state.json['2002150602-000002'],//凭证生成
                    backUrl: '/',
                    backPageCode:backPageCode,
                    backAppCode:backAppCode
                }
                pushToGenVoucher(page,param)
            }
        }
    });
}

function linkVoucher(page){
    let pagecode= page.pageCode;
    let pk_accountingbook = page.props.form.getFormItemsValue(page.formId,'pk_accountingbook');
    let date = page.props.form.getFormItemsValue(page.formId,'pk_group');
    let flag = page.props.form.getFormItemsValue(page.formId,'flag');
    let desciption = page.props.form.getFormItemsValue(page.formId,'desciption');
    if(flag.value==null){
        flag={diaplay:null,value:false};
    }
    if(desciption.value==null){
        desciption={diaplay:page.state.json['2002150602-000000'],value:false};/* 国际化处理： 否*/
    }
    let dataValue={
        "pagecode":pagecode,
        "pk_accountingbook":pk_accountingbook.value,
        "date":date.display,
        "flag":flag.value,
        "pks":pk,
        "untallied":desciption.value,
        "dateValue":date.value,
    }
    setDefData("queryCondition", "gl.amortize.execute.pk_amortize", dataValue);
    let pks = getSelecedDataId(page);
    
    let pk="";
    if(null!=pks && pks.length>0){
        for(let i=0;i<pks.length;i++){
            pk += pks[i]+",";
        }
        pk=pk.substr(0,pk.length-1);
    }
    let data={
        "pagecode":pagecode,
        "pks":pk
    }
    let url = '/nccloud/gl/amortize/linkVoucher.do';
    let backPageCode = page.props.getUrlParam('pagecode') || page.props.getSearchParam('p');
    let backAppCode = page.props.getUrlParam('appcode') || page.props.getSearchParam('c');
    ajax({
        url : url,
        data : data,
        success : (res) => {
            if(res){
                let param={
                    pk_voucher: res.data,
                    titlename:page.state.json['2002150602-000003'],//凭证联查
                    backUrl: '/',
                    backPageCode:backPageCode,
                    backAppCode:backAppCode
                }
                pushToLinkVoucher(page,param)
            }
        }
    });
}

/**
 * 获取选中行id
 * @param {*} page 
 */
function getSelecedDataId(page){
    const key = 'pk_amortize';
    const rows = page.props.table.getCheckedRows(page.tableId);
    if(!rows) return;
    const ids = [];
    rows.map((item) => {
        // let value = page.props.table.getValsByKeyAndRowId(page.tableId, item.data.rowId, key);
        let value = item.data.values[key].value;
        ids.push(value);
    });
    return ids;
}

/**
 * 跳转到卡片页面
 * @param {*} page 
 * @param {*} params 
 */
function linkToCard(page, params){
    page.props.linkTo("../card/index.html", params);
}

function refresh(page,props){
    let value=page.state.pk_accountingbook;
		let pk_accountingbook = props.form.getFormItemsValue(page.formId,'pk_accountingbook');
		if(pk_accountingbook){
			let date = props.form.getFormItemsValue(page.formId,'pk_group');
			let flag = props.form.getFormItemsValue(page.formId,'flag');
			let desciption = props.form.getFormItemsValue(page.formId,'desciption');
			if(flag.value==null){
				flag={diaplay:null,value:false};
			}
			if(desciption.value==null){
				desciption={diaplay:page.state.json['2002150602-000000'],value:false};/* 国际化处理： 否*/
			}
			let data = {
				showSealDataFlag : flag.value,
				pk_accountingbook : pk_accountingbook.value,
				date:date.value,
				diaplaydate:date.display,
				desciption:desciption.value,
				pageId : page.pageCode
			};
			ajax({
				url: '/nccloud/gl/amortize/queryExecute.do',
				data: data,
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							props.form.setAllFormValue({[page.formId]:res.data.head[page.formId]});
						}
						if (res.data.body) {
							if(res.data.body[page.tableId])
							{
								props.table.setAllTableData(page.tableId, res.data.body[page.tableId]);
							}
							else
							{
								props.table.setAllTableData(page.tableId, { rows: [] });
                            }
                            toast({color:"success",title:page.state.json['2002150602-000004']})/* 国际化处理： 刷新成功*/
						}else{
							props.table.setAllTableData(page.tableId, { rows: [] });
						}
					}
				}
			});
		}
}
