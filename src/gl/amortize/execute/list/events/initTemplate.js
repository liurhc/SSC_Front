import { createPage, ajax, base, toast ,cardCache} from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;
let {setDefData, getDefData } = cardCache;
const pageCode = '2002150602_amorexe';
const tableId = 'amortize';
const formId = 'amortizeform';
let appcode = '2002150602';
export default function (props) {
	const page = this;
	appcode=this.props.getSearchParam('c');
	props.createUIDom(
		{
			pagecode: pageCode,//页面id
			appcode: appcode//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta,page)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				props.form.setFormStatus(formId, "edit");	
				if(data.context.defaultAccbookPk){
					page.state.pk_accountingbook=data.context.defaultAccbookPk;
					page.state.accountingbookname=data.context.defaultAccbookName;
				}
			}
			if(page.state.pk_accountingbook){
				getData(page.props,page);
			}
		}
	)
}

 function getData(props,page) {
	let DefData =getDefData("queryCondition", "gl.amortize.execute.pk_amortize");
	if(DefData){
		let data = {
			showSealDataFlag : DefData.flag,
			pk_accountingbook : DefData.pk_accountingbook,
			date:DefData.dateValue,
			diaplaydate:DefData.date,
			desciption:DefData.untallied,
			pageId : DefData.pagecode
		};
		ajax({
			url: '/nccloud/gl/amortize/queryExecute.do',
			data: data,
			success: (res) => {
				if (res.data) {
					if (res.data.head) {
						props.form.setAllFormValue({[page.formId]:res.data.head[page.formId]});
					}
					if (res.data.body) {
						if(res.data.body[page.tableId])
						{
							props.table.setAllTableData(page.tableId, res.data.body[page.tableId]);
						}
						else
						{
							props.table.setAllTableData(page.tableId, { rows: [] });
						}
					}else{
						props.table.setAllTableData(page.tableId, { rows: [] });
					}
				}
			}
		});
	}else{
		let value=page.state.pk_accountingbook;
		let pk_accountingbook = props.form.getFormItemsValue(page.formId,'pk_accountingbook');
		if(pk_accountingbook){
			let date = props.form.getFormItemsValue(page.formId,'pk_group');
			let flag = props.form.getFormItemsValue(page.formId,'flag');
			let desciption = props.form.getFormItemsValue(page.formId,'desciption');
			if(flag.value==null){
				flag={diaplay:null,value:false};
			}
			if(desciption.value==null){
				desciption={diaplay:page.state.json['2002150602-000000'],value:false};/* 国际化处理： 否*/
			}
			let meta=props.meta.getMeta();
			meta[page.formId].items.map((item, key) => {
				if (item.attrcode == 'pk_group') {
					let url='/nccloud/gl/glpub/queryBizDate.do';
					let pk_accountingbook=value;
					let data={
						"pk_accountingbook":pk_accountingbook
					}
					let pk_accperiodscheme=null;
					ajax({
						url : url,
						data : data,
						async:false, 
						success : (success) => {
							if(success){
								pk_accperiodscheme=success.data.pk_accperiodscheme;
							}
						}
					});
					item.queryCondition = () => {
						return {pk_accperiodscheme:pk_accperiodscheme,
							GridRefActionExt:'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder',
							"isDataPowerEnable": 'Y',"DataPowerOperationCode" : 'fi'
						};
					}
				}
				return item;
			});
			props.meta.setMeta(meta);
		
			let data = {
				showSealDataFlag : flag.value,
				pk_accountingbook : value,
				date:date.value,
				diaplaydate:date.display,
				desciption:desciption.value,
				pageId : page.pageCode
			};
			ajax({
				url: '/nccloud/gl/amortize/queryExecute.do',
				data: data,
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							props.form.setAllFormValue({[page.formId]:res.data.head[page.formId]});
						}
						if (res.data.body) {
							if(res.data.body[page.tableId])
							{
								props.table.setAllTableData(page.tableId, res.data.body[page.tableId]);
							}
							else
							{
								props.table.setAllTableData(page.tableId, { rows: [] });
							}
						}else{
							props.table.setAllTableData(page.tableId, { rows: [] });
						}
					}
				}
			});
		}
	}
};

function modifierMeta(props, meta,page) {
	meta[formId].items = meta[formId].items.map((item, key) => {
		if (item.attrcode == 'pk_accountingbook') {
			item.isMultiSelectedEnabled=false;
			item.showGroup=false;
			item.showInCludeChildren=false;
			item.disabledDataShow=true,
			item.queryCondition = () => {
				return {appcode:appcode,
					TreeRefActionExt:'nccloud.web.gl.ref.AccountBookRefSqlBuilder'
				};
			}
		}
		return item;
	});
	props.button.setDisabled({
		linkVoucher: true
	});
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'code') {
			item.visible=false;
		}
		//创建时间
		
		return item;
	});
	props.button.setDisabled({
		execute:true
	});
	return meta;
}
