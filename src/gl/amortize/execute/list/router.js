import {asyncComponent} from 'nc-lightapp-front';
import Gltransfer from './SyncTreeDom';

import Voucher from 'gl/voucher_card';
import VoucherList from 'gl/voucher_list';

// import VoucherList from '../../gl_voucher/voucher_list/list';
// import Voucher from '../../gl_voucher/container/Welcome';
// import PeriodVoucher from '../../gl_voucher/container/PeriodVoucher/periodpage';

//const VoucherList = asyncComponent(() => import(/* webpackChunkName: "gl/amortize/voucher_list/list" */'../../../gl_voucher/voucher_list/list'));
//const Voucher = asyncComponent(() => import(/* webpackChunkName: "gl/amortize/container/Welcome" */'../../../gl_voucher/container/Welcome'));
const PeriodVoucher = asyncComponent(() => import(/* webpackChunkName: "gl/amortize/container/PeriodVoucher/periodpage" */ '../../../gl_voucher/container/PeriodVoucher/periodpage'));

// const edit11 = asyncComponent(() => import(/* webpackChunkName: "reva_demo/module/apply/card/soCard" */'../../apply/card'));

// const appHome = asyncComponent(() => import(/* webpackChunkName: "demo/module/so/js/AppHome" */'pages/app/home'));
// const appMain = asyncComponent(() => import(/* webpackChunkName: "demo/module/so/js/AppMain" */'pages/app/main'));

const routes = [
  {
    path: '/',
    component: Gltransfer,
    exact: true,
  },
  {
    path: '/list',
    component: VoucherList,
  },
  {
    path: '/Welcome',
    component: Voucher,
  },
  {
    path: '/periodpage',
    component: PeriodVoucher,
  }
];

export default routes;
