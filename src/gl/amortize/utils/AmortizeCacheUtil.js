import CacheUtils from '../../public/common/CacheUtil.js'

const dataSource = 'fi.gl.amortize.temp';
const BTN_STATE = 'btnState';

class AmortizeCacheUtil extends CacheUtils{
    constructor(ViewModel){
        super(ViewModel, dataSource);
    }

    getBtnState(){
        return this.getData(BTN_STATE);
    }

    setBtnState(btnState){
        let state = this.getBtnState();
        if(state){
            Object.assign(state, btnState);
        }else{
            state = btnState;
        }
        this.setData(BTN_STATE, state);
    }

}

export default AmortizeCacheUtil;
