import {URL, cardPageCode, listPageCode, appCode} from '../pages/consts'
import { ajax, promptBox, viewModel } from 'nc-lightapp-front'
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;

export function inStore(data){
    setGlobalStorage('sessionStorage', 'amortize_list', JSON.stringify(data), ()=>{
        //存储缓存失败的处理函数
        //联查需要处理缓存存储失败的情况，失败时，把缓存信息转后台存放
    });
    
}

export function outStore(){
    let dataStr = JSON.parse(getGlobalStorage('sessionStorage', 'amortize_list'))
    let data = JSON.parse(dataStr);
    return data;
}

export function linkToCard(props, data){
    const params = {
        pagecode:cardPageCode,
        appcode : appCode
    };
    if(data) Object.assign(params, data);
    props.pushTo(URL.cardUrl, params);
}

export function linkToList(props, data){
    const params = {
        pageCode : listPageCode,
        appcode : appCode
    };
    if(data) Object.assign(params, data);
    props.pushTo(URL.listUrl, params);
}

export {delAmortize, stopAmortize}

function delAmortize(page, amortizes, callback){
    promptBox({
        color : 'warning',
        content : page.state.json['2002150601-000000'],/* 国际化处理： 您确定要删除所选数据吗？*/
        beSureBtnClick : () => {
            let data = {
                amortizes: amortizes
            }
            let url = URL.delUrl;
            myajax(url, data, callback);
        }
    });
}

function stopAmortize(pk_amortizes, callback){
    let data = {
        pk_amortizes : pk_amortizes
    }
    let url = URL.stopUrl;
    myajax(url, data, callback);
}

function myajax(url, data, callback){
    ajax({
        url, data,
        success: (res) => {
            if(callback){
                callback(res);
            }
        }
    });
}
