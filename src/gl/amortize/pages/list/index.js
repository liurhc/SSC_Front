//主子表列表
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, cardCache, getMultiLang, createPageIcon } from 'nc-lightapp-front';
const { NCCheckbox, NCRow:Row,NCCol:Col ,NCDiv} = base;
import { buttonClick, initTemplate, pageInfoClick, tableModelConfirm, onSelect, onSelectAll } from './events';
import './index.less';
import {inStore, outStore, linkToCard} from '../../utils'
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js'

import {ListLayout, Table} from '../../../../fipub/public/components/layout/ListLayout'
import { Header, HeaderButtonArea, HeaderSearchArea } from "../../../../fipub/public/components/layout/Header";

import ReferLoader from '../../../public/ReferLoader/index.js';
import {accbookRefcode} from '../../../public/ReferLoader/constants.js';
import {DATA_SOURCE, PKNAME} from '../consts';
import AmortizeCacheUtil from '../../utils/AmortizeCacheUtil';
import {commonApi} from '../../../public/common/actions';
let {} = cardCache;
class List extends Component {
	constructor(props) {
		super(props);
		this.cacheUtil = new AmortizeCacheUtil(this.props.ViewModel);		
		this.pageCode = this.props.getSearchParam('p');
		this.tableId = 'amortize';
		let appcode = this.props.getSearchParam('c');
		this.state = {
			showSealDataFlag : false,
			pk_accountingbook : '',
			accountbook_name : '',
			json:{},
			init:true
		}
		let context = this.cacheUtil.getContext();
		if(context&&context.pk_accountingbook){
			this.state.pk_accountingbook = context.pk_accountingbook;
			this.state.accountbook_name = context.accountbook_name;
			this.state.showSealDataFlag = context.showSealDataFlag;
		}else{
			getDefaultAccountBook(appcode).then((defaultAccountBook) => {
				if(defaultAccountBook){
					let context = {pk_accountingbook:defaultAccountBook.value, accountbook_name: defaultAccountBook.display};
					this.cacheUtil.setContext(context);
					this.setState(context, () => {this.updateContextInfo(); this.getData()})
				}
			});
		}
	}

	updateContextInfo = () =>{
		let {cacheUtil, state} = this;
		let context = cacheUtil.getContext();
		if(!context){
			context = {};
		}
		context.pk_accountingbook = state.pk_accountingbook;
		context.accountbook_name = state.accountbook_name;
		context.showSealDataFlag = state.showSealDataFlag;
		if(state.pk_accountingbook){
			commonApi.queryBookCombineInfo({data:state.pk_accountingbook, success:(res) => {
				context.combineInfo = res.data;
				cacheUtil.setContext(context);
			}});
		}else{
			context.combineInfo = {};
			cacheUtil.setContext(context);
		}
		this.setState(state, () => {this.initBtnState()})
	}

	componentDidMount() {
		this.initBtnState();
	}

	componentWillMount() {
		/* 加载多语资源 */
		let callback = (json, status, inlt) => {
			if(status){
				this.setState({json, inlt}, () => {
					initTemplate.call(this, this.props);
				});
			}else{
			}
		}
		getMultiLang({moduleId:["2002150601", "publiccommon"], domainName:'gl', callback});
	}

	componentWillUpdate(){
		let state = this.state;
		let storeData = {
			showSealDataFlag : state.showSealDataFlag,
			pk_accountingbook : state.pk_accountingbook,
			accountbook_name : state.accountbook_name
		}
		inStore(storeData);
	}

	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button';
		} else {
			return 'secondary - button';
		}
	};

	/**
	 * 初始化按钮状态
	 */
	initBtnState = () =>{
		let page = this;
		let btnState = page.getBtnInitState();
		let {init} = page.state;
		if(init){
			let cachedBtnState = page.cacheUtil.getBtnState();
			if(cachedBtnState){
				Object.assign(btnState, cachedBtnState);
			}
			init = false;
			page.setState({init});
		}
		page.props.button.setButtonDisabled(btnState);
	}

	/**
	 * 获取按钮初始设置
	 */
	getBtnInitState = () => {
		let page = this;
		let {pk_accountingbook} = page.state;
		const btnState = {};
		if(pk_accountingbook){
			btnState.add = false;
		}else{
			btnState.add = true;
		}
		btnState.update = btnState.delete = btnState.copy = btnState.stop = true;
		return btnState;
	}

	/**
	 * 缓存中有 则加载缓存中数据
	 * 缓存中没有则加载请求回来的默认数据
	 */
	getData = (callback) => {
		if(!this.state.pk_accountingbook){
			this.props.table.setAllTableData(this.tableId, {rows : []});
			return;
		}
		let data = {
			showSealDataFlag : this.state.showSealDataFlag,
			pk_accountingbook : this.state.pk_accountingbook,
			pageId : this.pageCode
		}
		ajax({
			url: '/nccloud/gl/amortize/queryAmortize.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						this.props.table.setAllTableData(this.tableId, data[this.tableId]);
					} else {
						this.props.table.setAllTableData(this.tableId, { rows: [] });
					}
					if(callback && typeof callback == 'function'){
						callback(data);
					}
				}
			}
		});
	};


	/**
	 * 是否有缓存数据
	 */
	isCached = () =>{
		let {hasCacheData} = this.props.table;
		if(hasCacheData(DATA_SOURCE)){
			return true;
		}
		return false;
	}
	
	clearCache = (pk_amortize) => {
	}

	onRowDoubleClick = (record, index, props, e) => {
		linkToCard(this.props, {
			id : record['pk_amortize'].value,
			status : 'browse'
		});
	}

	/**
	 * 更新按钮状态
	 */
	updateBtnState = (btnState) => {
		this.cacheUtil.setBtnState(btnState);
		this.props.button.setButtonDisabled(btnState);
	}

	render() {
		let { table, button, search, modal } = this.props;
		let props = this.props;
		let buttons = this.props.button.getButtons();
		let { createSimpleTable} = table;
		let {createEditTable} = this.props.editTable;
		let { pk_accountingbook,accountbook_name}=this.state;
		let cacheUtil = this.cacheUtil;
		const { createBillHeadInfo } = this.props.BillHeadInfo
		return (
			<ListLayout>
				<Header showGobakc={false}>
					<HeaderSearchArea className='amortize-define'>
						<div className="width_210 margin_r10">
							<ReferLoader
								tag = 'test'
								refcode = {accbookRefcode}
								isMultiSelectedEnabled = {false}
								showGroup = {false}
								showInCludeChildren = {true}
								value={{refname: accountbook_name, refpk: pk_accountingbook}}
								disabledDataShow={true}
								queryCondition = {() => {
									let condition = {
										"TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
										"appcode":"",
										"isDataPowerEnable": 'Y',
										"DataPowerOperationCode" : 'fi'
									}
									condition.appcode = props.getSearchParam('c');
									return condition;
								}}
								onChange= {(v) => {
									let state = this.state;
									state.pk_accountingbook = v.refpk;
									state.accountbook_name = v.refname;
									this.setState(state, () => {this.updateContextInfo(); this.getData()});
								}}
							/>
						</div>
						<div className='switch-btn'>
							<NCCheckbox
								checked = {this.state.showSealDataFlag}
								onChange = {(v) => {
									let showSealDataFlag = this.state.showSealDataFlag;
									showSealDataFlag = !showSealDataFlag;
									this.setState({showSealDataFlag}, this.getData);
									/* 更新Context */
									let context = cacheUtil.getContext() || {};
									context.showSealDataFlag = showSealDataFlag;
									cacheUtil.setContext(context);
								}}
							/>
							<span>{this.state.json['2002150601-000008']}</span>{/* 国际化处理： 是否显示停用*/}
						</div>
					</HeaderSearchArea>
					<HeaderButtonArea>
						{this.props.button.createButtonApp({
							area: 'page_header',
							buttonLimit: 3, 
							onButtonClick: buttonClick.bind(this), 
							popContainer: document.querySelector('.header-button-area')
						})}
					</HeaderButtonArea>
				</Header>
				<Table>
					{createSimpleTable(this.tableId, {
						dataSource: DATA_SOURCE,
						pkname: PKNAME,
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						showIndex : true,
						showCheck : true,
						onRowDoubleClick : this.onRowDoubleClick,
						onSelected: onSelect.bind(this),
						onSelectedAll: onSelectAll.bind(this),
						componentInitFinished:() => {

						}
					})}
				</Table>
			</ListLayout>
		);
	}
}

List = createPage({})(List);

export default List;
