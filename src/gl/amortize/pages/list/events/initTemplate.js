import { createPage, ajax, base, toast } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon, NCTooltip:Tooltip } = base;
import tableButtonClick  from './tableButtonClick';
const pageCode = '2002150601_amortize_list';
const tableId = 'amortize';
const appId = '0001Z310000000049AJU';
const appcode = '2002150601';
export default function (props) {
	const page = this;
	props.createUIDom(
		{
			pagecode: pageCode,//页面id
			appcode: appcode//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let template = data.template;
					let meta = initMeta(props, page, template);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('delete_line',page.state.json['2002150601-000009']);/* 国际化处理： 确定要删除吗？*/
				}
			}
		}
	)
}
function initMeta(props, page, template){
    let meta = template;
    meta[tableId].items.map((item) => {
        if (item.attrcode == 'amortizeno') {
            item.render = (text, record, index) => {
				let tip = (<div>{record.amortizeno.value}</div>)
                if(record)
                return (
					<Tooltip trigger="hover" placement="top" inverse={false} overlay={tip}>
						<a
							style={{ textDecoration: 'none', cursor: 'pointer' }}
							onClick={() => {
								props.pushTo('/card',{
									status:'browse',
									id: record.pk_amortize.value
								})
							}}
						>
							{record.amortizeno.value}
						</a>
					</Tooltip>
                );
            };
        }
        return item;
	});
	meta[tableId].items.push({
		attrcode: 'opr',
		label: page.state.json['2002150601-000010'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		className: 'table-opr',
		width:'160px',
		render: (text, record, index) => {
			// 未执行：0; 可修改、删除，复制，停用 
			// 已执行: 1; 可复制、停用，不可修改、删除
			// 停用：2；可删除、复制，不可修改
			let buttonArr = record.state.value === '1' ? ['copy_line'] : record.state.value==='2'?['delete_line', 'copy_line']: ['update_line', 'delete_line', 'copy_line']
            return props.button.createOprationButton(buttonArr, {
                area: "table_inner",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index,tableId,page)
            });
        }
	});
	return meta
}

