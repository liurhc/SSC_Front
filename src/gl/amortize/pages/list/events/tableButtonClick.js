import { ajax, toast } from 'nc-lightapp-front';
import {inStore, linkToCard, stopAmortize, delAmortize } from '../../../utils';
const tableButtonClick = (props, key, text, record, index,tableId,page) => {
    switch (key) {
        // 表格操作按钮
        case 'update_line':
            linkToCard(props, {status:'edit', id:record.pk_amortize.value});
        break;
        case 'delete_line':
            let {deleteCacheId} = props.table;
            let amortizes = [{
                pk_amortize: record.pk_amortize.value,
                ts: record.ts.value
            }]        
            ajax({
                url: '/nccloud/gl/amortize/batDelAmortize.do',
                data: {amortizes: amortizes},
                success: (res) => { 
                    if (res.success) {
                        toast({color:"success",content:page.state.json['2002150601-000012']});/* 国际化处理： 删除成功*/
                        deleteCacheId(tableId,record.pk_amortize.value);
                        props.table.deleteTableRowsByIndex(tableId, index);
                    }
                }
            });
            break;
        case 'copy_line':
            linkToCard(props, {status:'edit', id:record.pk_amortize.value, isCopy:true});
            break;
        case 'stop_line':
            
            break;
        default:
            break;
    }
};
export default tableButtonClick;
