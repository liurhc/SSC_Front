import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import {onSelect, onSelectAll, resetSelectNum} from './selectEvent';
export { pageInfoClick,initTemplate,buttonClick, onSelect, onSelectAll, resetSelectNum };
