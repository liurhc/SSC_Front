import {AMOR_STATE, LIST_IDS} from '../../consts'

/**
 * 停用状态不允许修改和编辑
 * 已执行状态不允许修改和删除
 * 未执行状态允许所有操作
 */

let selectedStopedNum = 0;
let selectedExecedNum = 0;
let selectedUnExecNum = 0;
let selectedNum = 0;
export function onSelect(props, moduleId, record, index, status) {
    let page = this;
    resetSelectNum(page, moduleId);
    let btnState = dealWithSelectedNum(selectedStopedNum, selectedExecedNum, selectedUnExecNum);
    page.updateBtnState(btnState);
}

export function onSelectAll(props, moduleId, status, length){
    let page = this;
    resetSelectNum(page, moduleId);
    let btnState = dealWithSelectedNum(selectedStopedNum, selectedExecedNum, selectedUnExecNum);
    page.updateBtnState(btnState);
}


/**
 * 重置选中的个状态定义数目
 * @param {*} page 
 */
export function resetSelectNum(page, moduleId){
    selectedStopedNum = 0;
    selectedExecedNum = 0;
    selectedUnExecNum = 0;
    let rows = page.props.table.getCheckedRows(moduleId);
    if(rows){
        rows.map((row) => {
            if(AMOR_STATE.STOP == row.data.values.state.value) selectedStopedNum++;
            else if(AMOR_STATE.EXCUTED == row.data.values.state.value) selectedExecedNum++;
            else if(AMOR_STATE.UNEXCUTED == row.data.values.state.value) selectedUnExecNum++;
        });
    }
}

function dealWithSelectedNum(stopedNum, excutedNum, unExecNum){
    let btnDel = false, btnStop = false; //默认能用
    if(unExecNum + stopedNum + excutedNum > 0){
        if(stopedNum > 0){
            btnStop = true;
        }
        if(excutedNum > 0){
            btnDel = true;
        }
    }else{
        btnDel = btnStop = true;
    }
    let btnState = generateBtnState(btnDel, btnStop);
    return btnState;
}

function generateBtnState(del, stop){
    let btnState = {};
    btnState.delete = del;
    btnState.stop = stop;
    return btnState;
}
