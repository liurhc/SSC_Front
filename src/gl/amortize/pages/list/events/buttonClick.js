import {toast, ajax} from 'nc-lightapp-front';
import {inStore, linkToCard, stopAmortize, delAmortize } from '../../../utils';
import {DATA_SOURCE, PKNAME, LIST_IDS} from '../../consts'

export default function buttonClick(props, id) {
    switch (id) {
        case 'refresh': refresh(this); break;
        case 'add':     add(this); break;
        case 'delete':  del(this); break;
        case 'stop':    stop(this); break;
    }
}

function add(page){
    if(!page.state.pk_accountingbook){
        toast({content:page.state.json['2002150601-000011'], color:"warning"});/* 国际化处理： 请先设置核算账簿!*/
        return;
    }
    let pk_accountingbook = page.state.pk_accountingbook;
    linkToCard(page.props, {status:'add', pk_accountingbook:pk_accountingbook});
}

function del(page){
    const rows = page.props.table.getCheckedRows(page.tableId);
    const amortizes = [];
    const indexs = [];
    if(rows){
        rows.map((row) => {
            let pk_amortize = row.data.values['pk_amortize'].value;
            let ts = row.data.values['ts'].value;
            amortizes.push({pk_amortize, ts});
            indexs.push(row.index);
        });
    }
    delAmortize(page, amortizes, (res) => {
        if(res.success){
            toast({content:page.state.json['2002150601-000012']});/* 国际化处理： 删除成功*/
            let {deleteCacheId} = page.props.table;//TODO 删除缓存
            setTimeout(() => {
                amortizes.map((item) => {
                    deleteCacheId(LIST_IDS.TABLEID, item.pk_amortize);
                });
                page.props.table.deleteTableRowsByIndex(LIST_IDS.TABLEID, indexs);
                page.initBtnState();
            });
        }
    });
}

function stop(page){
    let pks = getSelecedDataId(page);
    stopAmortize(pks, (res) => {
        if(res.success){
            toast({content:page.state.json['2002150601-000006']})/* 国际化处理： 停用成功*/
            page.getData();
            page.initBtnState();
        }
    });
}

function refresh(page){
    page.initBtnState();
    page.getData(()=> toast({content:page.state.json['publiccommon-000004']}));
}

/**
 * 获取选中行id
 * @param {*} page 
 */
function getSelecedDataId(page){
    const key = 'pk_amortize';
    const rows = page.props.table.getCheckedRows(page.tableId);
    if(!rows) return;
    const ids = [];
    rows.map((item) => {
        let value = item.data.values[key].value;
        ids.push(value);
    });
    return ids;
}


