import { asyncComponent } from 'nc-lightapp-front';
import List from '../list';

const card = asyncComponent(() => import(/* webpackChunkName: "gl/amortize/pages/card/card" */ '../card'));

const routes = [
    {
        path: '/',
        component: List,
        exact: true
    },
    {
        path: '/list',
        component: List
    },
    {
        path: '/card',
        component: card
    }
];

export default routes;
