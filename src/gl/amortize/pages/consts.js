export const currPath = '/gl/amortize/pages/';
export const listPageCode = '2002150601_amortize';
export const cardPageCode = '2002150601_amortize_card';
export const appCode = '2002150601';

export const URL = {
    listUrl:"/list",
    cardUrl:"/card",
    stopUrl:"/nccloud/gl/amortize/batStopAmortize.do",
    delUrl:"/nccloud/gl/amortize/batDelAmortize.do"
}

export const AMOR_STATE = {
    UNEXCUTED : '0',
    EXCUTED : '1',
    STOP : '2'
}

/**
 * 汇率
 */
const RATE = { ORIGIN : '0', REALTIME : '1'};


const DATA_SOURCE = 'fip.gl.amortize.datasource';
const PKNAME = 'pk_amortize';

const LIST_IDS = {
    TABLEID : 'amortize',
}

const CARD_IDS = {
    HEAD_AREA_CODE:'amortize',
    AMOR_DETAIL_ID:'amordetail'
}

export {RATE, DATA_SOURCE, PKNAME, LIST_IDS, CARD_IDS};
