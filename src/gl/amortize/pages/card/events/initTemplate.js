import { pageId, headFormId, tailFormId, amorDetailId, cycleDetailId, appcode, cycleType } from '../constants';
import {appCode, cardPageCode, RATE} from '../../consts'
import { getBusinessInfo } from "nc-lightapp-front"


export default function(props) {
	let page = this;
	props.createUIDom(
		{
			pagecode: cardPageCode,//页面id
			appcode: appcode
		}, 
		function (data){
			let status = props.getUrlParam('status');
			if(data){
				if(data.template){
					let meta = data.template;
					modifierMeta(page, meta)
					props.meta.setMeta(meta);
					if(status === 'add'){
						page.initAddPage();
					}
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button, () => {
						let status = props.getUrlParam('status');
						let flag = status === 'browse' ? false : true;
						props.button.setButtonVisible(['save','savecommit', 'cancel'], flag);
						props.button.setButtonVisible([ 'add', 'edit','delete', 'drop_commit','drop_approve','drop_doaction','drop_fzgn','drop_yxgl','drop_linkquery','drop_print'], !flag);
					});
				}
			}   
		}
	)
}

function modifierMeta(page, meta) {
	let props = page.props;
	let status = props.getUrlParam('status') =='browse' ? 'browse':"edit";
	meta[headFormId].status = status;
	meta[amorDetailId].status = status;
	meta[cycleDetailId].status = status;
	meta[tailFormId].status = status;
	initAmortizeForm(page, meta);
	initAmorCycleForm(page, meta);
	initAmorDetailTable(page, meta);
	return meta;
}

function initAmortizeForm(page, meta){
	meta[headFormId].items.map((item) => {
		if(item.attrcode == 'pk_vouchertype'){
			item.queryCondition = () => {
				return getVouTypeQryCondition(page);
			}
		}else if(item.attrcode == 'speriod'){
			let context = page.cacheUtil.getContext();
			item.queryCondition = () => {
				let condition = {
					pk_accperiodscheme: context.combineInfo.pk_accperiodscheme,
					"GridRefActionExt": "nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder",
					"isDataPowerEnable": 'Y',
					"DataPowerOperationCode" : 'fi'
				}
				return condition;
			}
		}
	});
}

function initAmorCycleForm(page, meta){
	meta[tailFormId].moduletype = 'form';
}

function initAmorDetailTable(page, meta){
	meta[amorDetailId].items.push({attrcode:'assValues'});
	meta[amorDetailId].items.push({attrcode:'quantity'});
	meta[amorDetailId].items.push({attrcode:'assItems'});
	meta[amorDetailId].items.map((item) => {
		if(item.attrcode == 'pk_accasoa'){
			/* 临时切换参照，待平台bug修复后删除 */
			item.isMultiSelectedEnabled = true;
			item.isShowDisabledData = false;
			item.queryCondition = () =>{
				return getCondition(page);
			}
		}else if(item.attrcode == 'pk_currtype'){
			item.refcode = 'gl/refer/voucher/TransCurrGridRef/index';
			item.queryCondition = () => {
				return {
					"isDataPowerEnable": 'Y',
					"DataPowerOperationCode" : 'fi'
				}
			}
		}else if(item.attrcode == 'quantity' || item.attrcode == 'bindAss'){
			item.initialvalue = {value:'N'};
		}

	});
}

function getCondition(page){
	let bizeInfo = getBusinessInfo();
	let {pk_accountingbook} = page.cacheUtil.getContext();
	return {
		isDataPowerEnable : 'Y',
		DataPowerOperationCode : 'fi',
		pk_accountingbook:pk_accountingbook,
		dateStr : bizeInfo.businessDate.split(' ')[0]
	}
}

function getVouTypeQryCondition(page){
	let bizeInfo = getBusinessInfo();
	let {pk_accountingbook} = page.cacheUtil.getContext();
	let pk_group = bizeInfo.groupId;
	let condition = {
		pk_group : pk_group,
		pk_org : pk_accountingbook,
		isDataPowerEnable : 'Y',
		DataPowerOperationCode : 'fi'
	}
	return condition;
}

