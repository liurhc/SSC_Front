import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick'
import afterEventsForm from './afterEventsForm'
import beforeEvent from './beforeEvent'
export { buttonClick, afterEvent, initTemplate, 
    pageInfoClick, afterEventsForm,
    beforeEvent
 };
