import { pageId, headFormId, tailFormId, cycleDetailId, cycleType } from "../constants";
import {RegUtils, REG} from '../../../../public/common/StrReg';
import {refreshRate, getCycleType, initRate} from '../utils/index.js'

export default function afterEventsForm(props, moduleId, key, value, oldValue){
    if(moduleId == headFormId){
        if(key == 'cycle'){
            if(!RegUtils.regNum(REG.NUM.POS_INT, value.value)){
                value.value = '';
                props.form.setFormItemsValue(headFormId, {cycle:value});
                dealWithCycleNumChange(this, 0);
            }else{
                dealWithCycleNumChange(this, value.value);
            }
        }else if(key == 'billnum'){
            if(!regPosInt(value.value)){
                props.form.setFormItemsValue(headFormId, {[key]: {value: ''}});
            }
        }
    }else if(moduleId == tailFormId && key == 'cycletype'){
        dealWithCycleTypeChange(this, value.value);
    }
}

/**
 * 处理周期数变更事件
 * @param {*} page 
 * @param {*} value 
 */
function dealWithCycleNumChange(page, value){
    let cardTable = page.props.cardTable;
    let currRowNum = cardTable.getNumberOfRows(cycleDetailId, false);
    let indexs = [];
    for(let i = 0; i < currRowNum; i++){
        indexs.push(i);
    }
    setTimeout(() => {
        cardTable.delRowsByIndex(cycleDetailId, indexs);
        for(let i = 0; i < value; i++){
            let no = i + 1;
            let row = {no:{value:no,display:no}, rate:{scale:2}};
            cardTable.addRow(cycleDetailId, i, row, false);
        }
    }, 0);
    if(getCycleType(page) == cycleType.BYRATE){
        initRate(page, value);
    }
}

/**
 * 处理周期类型变更事件
 * @param {*} page 
 * @param {*} value 
 */
function dealWithCycleTypeChange(page, value){
    if(value == cycleType.BYRATE){
        let rowNum = page.props.cardTable.getNumberOfRows(cycleDetailId);
        initRate(page, rowNum);
    }
    /* 若放在更新列数据之前会造成列数据无法显示 */
    page.updateCycleType(value);
}

function regPosInt(str){
    return RegUtils.regNum(REG.NUM.POS_INT, str);
}
