import { ajax, deepClone } from 'nc-lightapp-front';
import { pageId, amorDetailId, cycleDetailId, tailFormId } from '../constants';
import {refreshRate} from '../utils/index.js'
export default function afterEvent(props, moduleId, key,value, changedrows, index, record, type, method) {
    if(moduleId == amorDetailId){
        if(key == 'pk_accasoa'){
            dealWithAccassoaChange(this,index, moduleId, value, record);
        }
    }else if(moduleId == cycleDetailId){
        if(key == 'rate'){
            if(method == 'blur'){
                refreshRate(this, index);
            }
        }
    }
}

/**
 * 处理会计科目变更事件
 * TODO 多选科目存在已选中科目是辅助核算信息保持原样
 * @param {*} page 
 * @param {*} value 
 */
function dealWithAccassoaChange(page, index, moduleId, value, record){
    let cardTable = page.props.cardTable;
    if(value){
        if(value.length > 0){
            value.map((item, i) => {
                item = item.nodeData;
                if(i == 0){
                    record.values.pk_accasoa = {display:item.dispname, value:item.refpk, scale:-1};
                    if(item.quantity == 'Y'){
                        record.values.quantity = {value:'Y'};
                    }else{
                        record.values.quantity = {value:'N'};
                    }
                    record.values[page.keyAssValues] = {};
                    record.values[page.keyAss] = {};
                    loadAssItem(record.values);   
                }else{
                    let temp = deepClone(record.values);
                    temp.pk_accasoa = {display:item.dispname, value:item.refpk, scale:-1};
                    temp.pk_amordetail = {};
                    temp.assValues = {};
                    temp.assItems = {};
                    temp.ass = {};
                    temp.amountformula = {};
                    if(item.quantity == 'Y'){
                        temp.quantity = {value:'Y'};
                    }else{
                        temp.quantity = {value:'N'};
                    }
                    loadAssItem(temp, () => {cardTable.addRow(moduleId, index, temp, false)});
                }
            });
        }
    }else{
        record.values.assItems = {};
        record.values[page.keyAssValues] = {};
        record.values[page.keyAss] = {};
    }
}


function loadAssItem(rowData, callback){
    let data = {
        pk_accasoa : rowData.pk_accasoa.value
    }
    let url = '/nccloud/gl/voucher/queryAssItem.do';
    ajax({
        url:url,
        data:data,
        success : (result) => {
            let {success, data} = result;
            if(success){
                if(rowData.assItems){
                    rowData.assItems.value = data;
                }else{
                    rowData.assItems = {value : data};
                }
                if(callback){
                    callback(rowData);
                }
            }
        } 
    });
}
