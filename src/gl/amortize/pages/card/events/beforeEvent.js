import { ajax, getBusinessInfo } from 'nc-lightapp-front';
import {CARD_IDS} from '../../consts';

export default function beforeEvent(props, moduleId, key,value, index, record) {
    let page = this;
    if(key == 'ass'){
        if(record.values.assItems && record.values.assItems.value){
            showAssistModal(page, index, record);
        }else{
            return false;
        }
    }else if(key == 'amountformula'){
        showAmountFormulaModal(page, index, key, record);
    }else if(key == 'quantityformula'){
        showAmountFormulaModal(page, index, key, record);
    }

    return true;
}


/**
 * 
 * @param {*} page 
 * @param {*} record 
 */
function showAssistModal(page, index, record){
    let {state, cacheUtil} = page;
    let assDatas = generateAssDatas(record);
    let context = cacheUtil.getContext();
    let buziInfo = getBusinessInfo();
    let pretentAssData = {
        pk_accountingbook: context.pk_accountingbook,
        pk_accasoa: record.values.pk_accasoa.value,
        prepareddate: buziInfo.businessDate.split(' ')[0],
        pk_org: context.combineInfo.unit.value,
        assData: assDatas,
        checkboxShow: true,
        linenum: index
    };

    let {assidModalState} = state;
    assidModalState = {
        show:true,
        defaultValue:pretentAssData,
        callback:(data) => {
            updateAssValue(page, index, data);
        }
    };
    page.setState({assidModalState});
}

function generateAssDatas(record){
    let assDatas = [];
    let {assItems, assValues} = record.values;
    if(assItems && assItems.value){
        assItems.value.map((item, index) => {
            let data = {};
            data.key = index;
            data.checktypecode = item.code;
            data.checktypename = item.name;
            data.pk_Checktype = item.pk_accassitem;
            data.refnodename = item.refnodename;
            data.refCode = data.refnodename;
            data.m_classid = item.classid;
            data.pk_accassitem = item.classid;
            data.pk_defdoclist = item.classid;
            data.classid = item.classid;
            data.inputlength = item.inputlength;
            data.digits = item.digits;
            assDatas.push(data);
        });
    }
    if(assValues && assValues.value){
        assValues.value.map((item) => {
            if(assDatas && assDatas.length > 0){
                assDatas.map((data) => {
                    if(data.pk_Checktype == item.m_pk_checktype){
                        data.pk_Checkvalue = item.m_pk_checkvalue;
                        data.checkvaluename = item.m_checkvaluename;
                        data.checkvaluecode = item.m_checkvaluecode;
                    }
                    return data;
                });
            }
        });
    }
    return assDatas;
}

function generateAssValues(data){
    const assValues = [];
    if(data && data.data && data.data.length > 0){
        data.data.map((item) => {
            const assValue = {
				m_pk_checkvalue : item.pk_Checkvalue,
				m_checkvaluename : item.checkvaluename,
				m_checkvaluecode : item.checkvaluecode,
				m_pk_checktype : item.pk_Checktype,
				m_checktypename : item.checktypename
            }
            assValues.push(assValue);
        });
    }
    return assValues;
}

function updateAssValue(page, index, data){
    let assValues = generateAssValues(data);
    let ass = getAss(assValues);
    let cardTable = page.props.cardTable;
    setTimeout(() => {
        cardTable.setValByKeyAndIndex(CARD_IDS.AMOR_DETAIL_ID, index, page.keyAssValues, {value: assValues});
        cardTable.setValByKeyAndIndex(CARD_IDS.AMOR_DETAIL_ID, index, page.keyAss, {value:ass, display:ass});
    });
    let {assidModalState} = page.state;
    assidModalState.show = false;
    page.setState({assidModalState});
}

/**
 * 生成辅助核算显示信息
 * @param {*} assValues 
 */
function getAss(assValues){
	let ass = '';
	if(assValues){
		assValues.map((item) => {
            ass += '[' + item.m_checktypename;
            if(item.m_checkvaluecode){
                ass += '=' + item.m_checkvaluecode 
            } 
            ass += ']';
		});
	}
	return ass;
}



function showAmountFormulaModal(page, index, key, record){
    const {formulaModalState} = page.state;
    formulaModalState.defaultValue = record.values[key].value;
    formulaModalState.show = true;
    formulaModalState.callback = (data) =>{
        let amountformula = {display:data, value:data};
        page.props.cardTable.setValByKeyAndIndex(CARD_IDS.AMOR_DETAIL_ID, index, key,  amountformula);
        formulaModalState.show = false;
        page.setState(formulaModalState);
    }
    page.setState({formulaModalState});   
}
