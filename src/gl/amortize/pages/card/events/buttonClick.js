import {cardCache,promptBox, toast} from "nc-lightapp-front";
import {linkToList, linkToCard, stopAmortize, delAmortize} from '../../../utils'
import {URL, DATA_SOURCE, CARD_IDS, PKNAME} from '../../consts'
let {getCurrentLastId, deleteCacheById } = cardCache;

export default function (props, id) {
  switch (id) {
    case 'add': add(this); break;
    case 'save': this.saveBill(); break;
    case 'update': update(this); break;
    case 'copy': copy(this); break;
    case 'stop': stop(this); break;
    case 'back': back(this); break;
    case 'cancel': cancel(this); break;
    case 'delete':  del(this); break;
    case 'add_line' : addAmorDetailLine(this); break;
    case 'del_line' : batDelAmorDetailLine(this); break;
    case 'cp_line' : copyAmorDetailLine(this); break;
    case 'refresh' : refresh(this); break;
    default:
      break
  }
}


/**
 * 取消修改
 * @param {this} page 
 */
function cancel(page){
  promptBox({
    color:'warning',
    title: page.state.json['2002150601-000004'],/* 国际化处理： 取消*/
    content: page.state.json['2002150601-000005'],  /* 国际化处理： 确定要取消吗？*/
    beSureBtnClick: () => {
      let {pk_amortize} = page.state;
      if(!pk_amortize){
        pk_amortize = getCurrentLastId(DATA_SOURCE);
      }
      page.props.pushTo(URL.cardUrl, {status:'browse', id:pk_amortize});
      page.setState({pk_amortize, status:'browse', isCopy:false}, 
          () => {
              page.getData(pk_amortize, (data) => {
                page.loadData(data), page.updateCacheData(pk_amortize, data)
              });
              if(!pk_amortize){
                page.toggleShow(true);
                page.clearPageData();
              } else {
                page.toggleShow();
              }            
          }
      );
    },
    cancelBtnClick: () => {
      
    }
  }); 
}

/**
 * 重置所有表格数据
 * @param {this} page 
 */
function resetAllTableData(page){
  const tableIds = [page.amorDetailId, page.cycleDetailId];
  tableIds.map((item) => {
    page.props.cardTable.resetTableData(item);
  })
}

function copy(page){
  page.initCopyPage();
  page.props.pushTo(URL.cardUrl, {status:'edit', isCopy:true});
  page.setState({status:'edit', isCopy:true})
  page.toggleShow();   
}

function update(page){
  let data = {
    status: 'edit',
    id: page.props.getUrlParam('id')
  };
  page.props.pushTo(URL.cardUrl, data);
  page.toggleShow();
}

function back(page){
  linkToList(page.props);
}

function add(page){
    let pk_accountingbook = page.props.form.getFormItemsValue(page.headFormId, 'pk_accountingbook').value;
    let data = {status:'add', pk_accountingbook:pk_accountingbook};
    page.props.pushTo(URL.cardUrl, data);
    page.initAddPage();
    page.toggleShow();
}

function del(page){
    let pk_amortize = page.state.pk_amortize;
    let tsObj = page.props.form.getFormItemsValue(CARD_IDS.HEAD_AREA_CODE, 'ts');
    let ts = tsObj && tsObj.value;
    let amortize = {pk_amortize, ts};
    delAmortize(page, [amortize], (res) => {
        if(res.success){
		        deleteCacheById(PKNAME, pk_amortize, DATA_SOURCE);
            back(page);
        }
    });
}

function stop(page){
    let pk_amortize = page.state.pk_amortize;
    stopAmortize([pk_amortize], (res) => {
        if(res.success){
            page.getData(pk_amortize, (data) => {
              toast({content:page.state.json['2002150601-000006']})/* 国际化处理： 停用成功*/
              page.loadData(data);
              page.updateCacheData(pk_amortize, data);    
            }, true);
        }
    });
}

function addAmorDetailLine(page){
  let index = page.props.cardTable.getNumberOfRows(page.amorDetailId, false);
  page.props.cardTable.addRow(page.amorDetailId, index, {}, false);
}

function batDelAmorDetailLine(page){
  let rows = page.props.cardTable.getCheckedRows(page.amorDetailId);
  if(rows){
    let indexs = [];
    rows.map((record) => {
      indexs.push(record.index);
    });
    page.props.cardTable.delRowsByIndex(page.amorDetailId, indexs);
    page.updateDelLineBtn();
  }
}

function copyAmorDetailLine(page){
  let rows = page.props.cardTable.getCheckedRows(page.amorDetailId);
  if(rows){
    rows.map((record) => {
      setTimeout(() => page.props.cardTable.pasteRow(page.amorDetailId, record.index), 0);
    });
  }
}

function refresh(page){
    let pk_amortize = page.state.pk_amortize;
    page.getData(pk_amortize, (data) => {
      toast({content:page.state.json['2002150601-000007']});/* 国际化处理： 刷新成功*/
      page.loadData(data);
    }, true);
}
