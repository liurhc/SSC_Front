/**
 * 表单区域
 */
export const headFormId = 'amortize';
export const tailFormId = 'amorcycle';

/**
 * 表体区域
 */
export const amorDetailId = 'amordetail';
export const cycleDetailId = 'cycledetail';

export const urls = {
    queryCard : '/nccloud/gl/amortize/queryAmortizeCard.do',
    updateCard : '/nccloud/gl/amortize/updateAmortize.do',
    insertCard : '/nccloud/gl/amortize/addAmortize.do',
    delAmortizes : '/nccloud/gl/amortize/batDelAmortize.do',
    stopAmortizes : ''
}

export const ObjStatus = {
    UNCHANGED : 0,
    UPDATED : 1,
    NEW : 2,
    DELETED :3
}

export const cycleType = {
    BYRATE : "0",
    BYAMOUNT : "1",
    AVERAGE : "2"
} 


