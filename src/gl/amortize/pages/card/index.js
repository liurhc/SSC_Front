//主子表卡片
import React, { Component } from 'react';
import { createPage, ajax, base, toast, cardCache, getMultiLang, getBusinessInfo, createPageIcon} from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, pageInfoClick, afterEventsForm, beforeEvent } from './events';
import { headFormId, tailFormId, cycleDetailId, amorDetailId, urls, ObjStatus, cycleType} from './constants';
import './index.less';
import {linkToCard, linkToList} from '../../utils'
import {cardPageCode, DATA_SOURCE, PKNAME, CARD_IDS, URL, AMOR_STATE, RATE} from '../consts'
import AmortizeCacheUtil from '../../utils/AmortizeCacheUtil';
import FormulaModal from '../../../public/components/FormulaModal';
import AssidModal from '../../../public/components/assidModal';

let { getCacheById, updateCache, addCache, deleteCacheById } = cardCache;

let { NCAnchor, NCScrollLink, NCScrollElement, NCAffix, NCBackBtn } = base;
class Card extends Component {
	constructor(props) {
		super(props)
		this.cacheUtil = new AmortizeCacheUtil(props.ViewModel);
		this.context = this.cacheUtil.getContext();
		this.pageId = cardPageCode;
		this.headFormId = headFormId;
		this.tailFormId = tailFormId;
		this.cycleDetailId = cycleDetailId;
		this.amorDetailId = amorDetailId;
		this.urls = urls;
		this.keyAssValues = 'assValues';
		this.keyAss = 'ass';
		this.state = {
			status : props.getUrlParam('status'),
			pk_amortize : props.getUrlParam('id'),
			pk_accountingbook:props.getUrlParam('pk_accountingbook'),
			isCopy : props.getUrlParam('isCopy'),
			assistModalShow:false,
			formulaModalState:{show:false, callback:null, defaultValue:''},
			assidModalState:{show:false, callback:null, defaultValue:[]},
			showCheck:false
		};
		initTemplate.call(this, this.props);
	}
	componentDidMount() {
		//关闭浏览器
        window.onbeforeunload = () => {
            let status=this.props.form.getFormStatus(this.headFormId);
            if (status == 'edit' || status == 'add') {
                return ''
            }
	    }
		this.toggleShow();
		let pk_amortize = this.state.pk_amortize;
		if(this.state.status != 'add'){
			this.getData(pk_amortize, 
				(data) => {
					this.loadData(data);
					this.updateCacheData(pk_amortize, data);
				}
			);
		}
	}

	componentWillMount(){
		/* 加载多语资源 */
		let callback = (json, status, intl) =>{
			if(status){
				this.setState({json, intl}, () => {
					initTemplate.call(this, this.props);
				});
			}else{
			}
		}

		getMultiLang({moduleId:'2002150601', domainName:'gl', callback});
	}

	initAddPage = () => {
		let page = this;
		let userInfo = getBusinessInfo() ||{};
		let context = page.cacheUtil.getContext();
		let itemsValue = {
			/* 设置核算账簿 */
			pk_accountingbook:{value:this.state.pk_accountingbook},
			//频度
			frequency:{display:"1", value:"1"},
			//时间
			time:{display:page.state.json['2002150601-000003'], value:page.state.json['2002150601-000003']},/* 国际化处理： 月,月*/
			//状态
			state:{value:"0"},
			//定义人
			pk_operator:{value: userInfo.userId, display: userInfo.userName},
			//汇率
			amortizerate:{value:RATE.ORIGIN},
			//起始期间
			speriod:{value:context.combineInfo.pk_accperiodmonth, display:context.combineInfo.bizPeriod}
		}

		let cycleItemsValue = {
			cycletype:{value:cycleType.AVERAGE}
		}
		setTimeout(() => {
			setTimeout(()=>{
				this.props.form.EmptyAllFormValue(this.headFormId);
				this.props.form.EmptyAllFormValue(this.tailFormId);
				this.props.cardTable.setTableData(this.amorDetailId, {rows:[]});
				this.props.cardTable.setTableData(this.cycleDetailId, {rows:[]});
				this.props.form.setFormItemsValue(this.headFormId, itemsValue);
				this.props.form.setFormItemsValue(this.tailFormId, cycleItemsValue);
				this.updateCycleType(cycleType.AVERAGE);
				this.props.cardTable.addRow(this.amorDetailId, 0, {}, false);
			}, 500);
		});
	}

	initCopyPage = () => {
		/* 清空各表主键 */
		this.props.form.setFormItemsValue(this.headFormId, {pk_amortize:{value:''}, 
			pk_amorcycle:{value:''}, 
			state:{value:'0'}, 
			lastexctime:{value:''},
			exctimes:{value:''}
		});
		this.props.form.setFormItemsValue(this.tailFormId, {pk_amorcycle:{value:''}});
		this.props.cardTable.setColValue(this.amorDetailId, 'pk_amordetail', {value:''});
		this.props.cardTable.setColValue(this.amorDetailId, 'pk_amortize', {value:''});
		this.props.cardTable.setColValue(this.cycleDetailId, 'pk_cycledetail', {value:''});
		this.props.cardTable.setColValue(this.cycleDetailId, 'pk_amorcycle', {value:''});
		this.props.cardTable.setColValue(this.cycleDetailId, 'pk_voucher', {value:''});
		/* 清空编号，名称 */
		// this.props.form.setFormItemsValue(this.headFormId, {amortizeno:{value:''}, amortizename:{value:''}});
		/* 设置各表数据状态为新增 */
		let tables = [this.amorDetailId, this.cycleDetailId];
		tables.map((tableId) => {
			let rowsStatus = [];
			for(let i = 0; i < this.props.cardTable.getNumberOfRows(tableId, false); i++){
				rowsStatus.push({index:i, status:ObjStatus.NEW});
			}
			setTimeout(() => {
				this.props.cardTable.setRowStatusByIndexs(tableId, rowsStatus);
			});
		});
	}

	/**
	 * 获取数据
	 * force 强制请求接口
	 */
	getData = (pk_amortize, callback, force = false) => {
		let {status} = this.state;
		if(status == 'add' || !pk_amortize)
			return;
		let cardData;
		if(!force){
			cardData = getCacheById(pk_amortize, DATA_SOURCE);
		}
		if(cardData){
			if(callback){
				callback(cardData);
			}
		}else{
			let data = {
				pk_amortize : pk_amortize,
				pageCode : this.pageId
			};
			let url = this.urls.queryCard;
			ajax({
				url: url,
				data : data,
				success : (res) => {
					if(res.success){
						if(callback){
							callback(res.data);
						}
					}
				}
			});

		}
	}

	/**
	 * 根据主键更新缓存信息
	 */
	updateCacheData = (pk, data) => {
		updateCache(PKNAME, pk, data, CARD_IDS.HEAD_AREA_CODE, DATA_SOURCE);
	}

	addCacheData = (pk, data) =>{
		addCache(pk, data, CARD_IDS.HEAD_AREA_CODE, DATA_SOURCE);
	}

	/**
	 * 将请求到的数据加载到页面
	 */
	loadData = (data) => {
		if(data.head){
			this.props.form.setAllFormValue({
				[this.headFormId]:data.head[this.headFormId]
			});
		}
		if(data.bodys){
			if(data.bodys){
				this.props.cardTable.setTableData(this.amorDetailId, data.bodys[this.amorDetailId]);
				this.props.cardTable.setTableData(this.cycleDetailId, data.bodys[this.cycleDetailId]);
				this.props.form.setAllFormValue({[this.tailFormId]:data.bodys[this.tailFormId]});
				let cycletype = this.props.form.getFormItemsValue(this.tailFormId, 'cycletype').value;
				this.updateCycleType(cycletype);
			}
		}
		if(this.state.isCopy){
			this.initCopyPage();
		}

		/* 加载数据完成后更新按钮可用状态 */
		this.updateButtonEnableState();
	}

	//切换页面状态
	// emptyAdd 区分新增status='add',
	// List页面Table为空时，emptyAdd 为true，否则为false，默认false
	toggleShow = (emptyAdd=false) => {
		let status = this.props.getUrlParam('status');
		let status1 = status === 'browse' ? 'browse' : "edit";
		this.props.form.setFormStatus(this.headFormId, status1);
		this.props.form.setFormStatus(this.tailFormId, status1);
		this.props.cardTable.setStatus(this.amorDetailId, status1);
		this.props.cardTable.setStatus(this.cycleDetailId, status1);
		this.toggleBtnShow(emptyAdd);
		this.updateButtonEnableState();
	};

	toggleBtnShow = (emptyAdd) => {
		let status = this.props.getUrlParam('status');
		let show = false;
		if(status == "edit" || status == 'add')
			show = true;
		if(emptyAdd) {
			this.props.button.setButtonVisible(['delete', 'update', 'copy', 'back', 'stop', 'refresh'], show);
		} else {
			this.props.button.setButtonVisible(['delete', 'update', 'copy', 'back', 'stop', 'refresh'], !show);
		}
		this.props.button.setButtonVisible(['save', 'cancel', 'add_line', 'del_line', 'cp_line'], show);
		this.props.button.setButtonVisible(['add'], !show);
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', !show);
		if(show){
			this.updateDelLineBtn();
		}
		this.setState({showCheck:show});
	}

	//保存单据
	saveBill = () => {
		/* 校验不通过则返回 */
		if(!this.checkDataRequired())
			return;
		let cardData = this.getDataNeedSave();
		let addState = this.props.getUrlParam('status') == 'add' || this.props.getUrlParam('isCopy');
		let url = this.urls.updateCard;
		if(addState){
			url = this.urls.insertCard;
		}
		ajax({
			url: url,
			data : cardData,
			success : (res) => {
				if(res.success){
					toast({content:this.state.json['2002150601-000001']});/* 国际化处理： 保存成功*/
					let pk_amortize = res.data.head.amortize.rows[0].values.pk_amortize.value;
					this.updatePageStatus({status:'browse', id:pk_amortize, isCopy:false});
					this.setState({pk_amortize:pk_amortize, status:'browse', isCopy:false});
					if(addState){
						addCache(pk_amortize, res.data, this.headFormId, DATA_SOURCE);
					}else{
						this.updateCacheData(pk_amortize, res.data);
					}
					this.loadData(res.data);
				}
			}
		});
	};

	updatePageStatus = (params) => {
		this.props.pushTo(URL.cardUrl, params);
		this.toggleShow();
	}

	getDataNeedSave = () => {

		let cardData = this.props.createExtCardData(this.pageId, this.headFormId, [this.amorDetailId,
			this.tailFormId, this.cycleDetailId]);
		let amorcycle = this.props.form.getAllFormValue(this.tailFormId);
		amorcycle.areaType = 'table';
		amorcycle.areacode = this.tailFormId;
		cardData.bodys.amorcycle = amorcycle;
		return cardData;
	}

	checkDataRequired = () => {
		let page= this;
		if(!page.props.form.isCheckNow([this.headFormId, this.tailFormId])){
			return false;
		}
		let details = page.props.cardTable.getVisibleRows(this.amorDetailId);
		if(!details || details.length <= 0){
			toast({content:this.state.json['2002150601-000002'], color:"warning"});/* 国际化处理： 分录不能为空！*/
			return false;
		}
		
		if(!page.props.cardTable.checkTableRequired(this.amorDetailId)){
			return false;
		}
		return true;
	}

	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button';
		} else {
			return 'secondary - button';
		}
	};


	updateCycleType = (type) => {
		switch(type){
			case cycleType.BYRATE:
				this.setCycleColEditable({no:false, rate:true, amount:false, note:true});
				break;
			case cycleType.BYAMOUNT:
				this.setCycleColEditable({no:false, rate:false, amount:true, note:true});
				break;
			case cycleType.AVERAGE:
				this.setCycleColEditable({no:false, rate:false, amount:false, note:false});
				break;
			default:
				this.setCycleColEditable({no:false, rate:false, amount:false, note:false});
				break;
		}
	}

	/**
	 * 设置周期表可编辑性
	 * @param {*} data 
	 */
	setCycleColEditable(data){
		let page = this;
		let cycleDetailId = this.cycleDetailId;
		setTimeout(() => {

			/* 模板配置不可修改不起效果，这里暂时单独处理一下 */
			page.props.cardTable.setColEditableByKey(cycleDetailId, 'no', !data.no);

			page.props.cardTable.setColEditableByKey(cycleDetailId, 'rate', !data.rate);
			page.props.cardTable.setColEditableByKey(cycleDetailId, 'amount', !data.amount);
			page.props.cardTable.setColEditableByKey(cycleDetailId, 'note', !data.note);
			if(!data.rate){
				page.props.cardTable.setColValue(cycleDetailId, 'rate', {value:''});
			}
			if(!data.amount){
				page.props.cardTable.setColValue(cycleDetailId, 'amount', {value:''});
			}
			if(!data.note){
				page.props.cardTable.setColValue(cycleDetailId, 'note', {value:''});
			}
		});
	}

	updateAssValues = (refData, index) => {
		let cardTable = this.props.cardTable;
		let newValues = this.getAssValues(refData);
		const ass = this.getAss(newValues);
		setTimeout(() => {
			cardTable.setValByKeyAndIndex(amorDetailId, index, this.keyAssValues, {value: newValues});
			cardTable.setValByKeyAndIndex(amorDetailId, index, this.keyAss, {value:ass, display:ass});
		}, 0);
	}

	/**
	 * 生成辅助核算显示信息
	 * @param {*} assValues 
	 */
	getAss = (assValues) => {
		let ass = '';
		if(assValues){
			assValues.map((item) => {
				ass += '[' + item.m_checktypename + '=' + item.m_checkvaluecode + ']';
			});
		}
		return ass;
	}

	getAssValues = (refData) => {
		const assValues = []
		if(refData){
			refData.map((item) => {
				const assValue = {
					m_pk_checkvalue : item.refpk,
					m_checkvaluename : item.refname,
					m_checkvaluecode : item.refcode,
					m_pk_checktype : item.pk_checktype,
					m_checktypename : item.checktypename
				};
				assValues.push(assValue);
			});
		}
		return assValues;
	}
	updateDelAndStopBtn = (show) => {
		this.props.button.setButtonVisible(['stop', 'update'], show);
	}

	handleClear = () =>{

	}

	clearPageData = () =>{
		let page = this;
		page.props.form.EmptyAllFormValue(headFormId);
		page.props.form.EmptyAllFormValue(tailFormId);
		page.props.cardTable.setTableData(amorDetailId, {rows:[]});
		page.props.cardTable.setTableData(cycleDetailId, {rows:[]});
	}

	/**
	 * 更新按钮可用状态
	 */
	updateButtonEnableState = () => {
		let page = this;
		let amorStateObj = page.props.form.getFormItemsValue(headFormId, 'state');
		let amortizeState = amorStateObj ? amorStateObj.value : '';
    	let btnUpdate = false, btnDel = false, btnCopy = false, btnStop = false; //默认能用
		if(amortizeState === AMOR_STATE.STOP ){
			btnStop = true; btnUpdate = true;
		}else if(amortizeState === AMOR_STATE.EXCUTED){
			btnUpdate = true; btnDel = true;
		}
		let btnState = this.generateBtnState(btnUpdate, btnDel, btnCopy, btnStop);
		page.props.button.setButtonDisabled(btnState);
	}

	
	generateBtnState(update, del, copy, stop){
		let btnState = {};
		btnState.update = update;
		btnState.delete = del;
		btnState.copy = copy;
		btnState.stop = stop;
		return btnState;
	}

	/**
	 * 清除本地缓存
	 */
	clearCache = (pk_amortize) => {
		deleteCacheById(PKNAME, pk_amortize, DATA_SOURCE);
	}

	goBack = () => {
		let page = this;
		linkToList(page.props);
	}
	//获取列表肩部信息
	getTableHead = (buttons) => {
		let { createButtonApp } = this.props.button;
		return (
			<div>
				{
					createButtonApp({
						area:"amor_detail_header",
						buttonLimit:3,
						onButtonClick:buttonClick.bind(this),
					})
				}
			</div>
		)
	};


	onCheckBoxSelect = (checked) => {
		this.updateDelLineBtn();
	}

	updateDelLineBtn = () => {
		let page = this;
		let checkedRows = page.props.cardTable.getCheckedRows(amorDetailId);
		let disabled = true;
		if(checkedRows && checkedRows.length > 0){
			disabled = false;
		}
		setTimeout(()=>{
			page.props.button.setButtonDisabled('del_line', disabled);
		}, 500);
	}

	render() {
		let page = this;
		let props = page.props;
		let { cardTable, form, button, modal, cardPagination } = props;
		let { createForm } = form;
		let buttons = this.props.button.getButtons();
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createButton, createButtonApp,createOprationButton } = button;
		const {formulaModalState, assidModalState, showCheck}=this.state;
		const context = page.cacheUtil.getContext();
		let title = page.props.getSearchParam('n');
		let status = page.props.getUrlParam('status');
		
		return (
			<div className="amortize-card nc-bill-card">
			<div className="nc-bill-top-area">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{status === 'add' || status === 'edit' ? (<div />): (
								<div className='goBack' style={{lineHeight: '32px'}}>
									<NCBackBtn onClick={this.goBack}/>
								</div>
							)}
							
							{/*页面大图标*/}
							{createPageIcon()}
							<h2 className='title-search-detail'>{title}</h2>
						</div>
						<div className="header-button-area">
							{/* 按钮适配 第三步:在页面的 dom 结构中创建按钮组，传入显示的区域，绑定按钮事件*/}
							{createButtonApp({
								area:"page_header",
								buttonLimit:3,
								onButtonClick: buttonClick.bind(this),
								popContainer:document.querySelector('.header-button-area'),
							})}
							<div style={{marginLeft: '6px'}}>
								{createCardPagination({
									handlePageInfoChange:pageInfoClick.bind(this),
									dataSource: DATA_SOURCE
								})}
							</div>
						</div>
					</div>
				</NCAffix>
				<NCScrollElement name='forminfo'>
					<div className="nc-bill-form-area">
						{createForm(this.headFormId, {
							onAfterEvent: afterEventsForm.bind(this)
						})}
					</div>
				</NCScrollElement>
				</div>
				<NCScrollElement name='businfo'>
					<div className="nc-bill-table-area" style={{ marginTop: '8px' }}>
						{createCardTable(amorDetailId, {
							tableHead: this.getTableHead.bind(this, buttons),
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: beforeEvent.bind(this),
							showCheck: showCheck,
							showIndex: true,
							onSelected:(props, moduleId, record, index, status) => {this.onCheckBoxSelect(status)},
							onSelectedAll:(props, moduleId, status, length) => {this.onCheckBoxSelect(status&&length)},
							hideSwitch:()=>{return false},
						})}
					</div>
				</NCScrollElement>
				<NCScrollElement name='detail'>
					<div className="m-nc-bill-form-box nc-theme-area-split-bc">
						<div className="m-nc-bill-form-area1">
							{createForm(this.tailFormId, {
								onAfterEvent : afterEventsForm.bind(this)
							})}
						</div>
						{createCardTable(cycleDetailId, {
							onAfterEvent: afterEvent.bind(this),
							hideSwitch:()=>{return false},
							showCheck: false,
							showIndex: false
						})}
					</div>
				</NCScrollElement>
				<FormulaModal
					appcode={props.getSearchParam('c')}
					visible={formulaModalState.show}
					handleClear={this.handleClear}
					handleFormulaOK={formulaModalState.callback}
					pk_accountingbook = {context.pk_accountingbook}
					pk_org = {context.combineInfo.unit.value}
					defaultValue = {formulaModalState.defaultValue}
					handleCancel={() => {
						formulaModalState.show = false;
						this.setState({
							formulaModalState
						})
					}}
				/>
				<AssidModal
					pretentAssData={assidModalState.defaultValue}
					showOrHide = {assidModalState.show}
					onConfirm={assidModalState.callback} 
					showDisableData = {'false'}
					handleClose={() => {
						assidModalState.show = false;
						this.setState({ 
							assidModalState
						 });
					}}
				/>
	
		</div>
		);
	}
}
Card = createPage({
	orderOfHotKey:[headFormId, amorDetailId, tailFormId, cycleDetailId]
})(Card);

export default Card;
