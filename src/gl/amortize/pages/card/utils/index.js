/**
 * 处理周期明细比例
 */
import {cycleDetailId, tailFormId} from '../constants.js'
import { accAdd, Subtr, accMul, accDiv } from '../../../../verify/verify/vrify/events/method.js';

function initRate(page, rowNum){
    const table = page.props.cardTable;
    const rateKey = "rate";
    const rates = [];
    if(rowNum && rowNum > 0){
        let aver = div(100, rowNum);
        let sum = 0.00;
        for(let i = 0; i < rowNum; i++){
            let rate = {value:'', scale:2};
            if(i != rowNum - 1){
                rate.value = aver;
                sum = add(sum, aver);
            }else{
                rate.value = sub(100.00, sum);
            }
            rates.push(rate);
        }
        setColValue(table, cycleDetailId, rateKey, rates);
    }
}

/**
 * 刷新周期比例数据
 * @param {*} page 
 * @param {*} index 
 */
function refreshRate(page, index){
    const table = page.props.cardTable;
    const rateKey = "rate";
    let sum2Index = 0;
    let sumBeforLast = 0;
    const rates = getAllVisibleRowCol(table, cycleDetailId, rateKey);
    rates.map((item, i) => {
        let rate = item.value;
        rate = rate ? rate : 0;
        if(i <= index)  sum2Index = add(sum2Index, rate);
        if(i < rates.length - 1)    sumBeforLast = add(sumBeforLast, rate);
    });
    if(sum2Index > 100){
        rates.map((item, i) => {
            const rate = item;
            if(i == index){
                rate.value = sub(100, sub(sum2Index, rate.value));
            }else if(i > index){
                rate.value = '';
            }
            return rate;
        });
    }else{
        let averSum = 0.00;
        let last = rates.length - index - 1;
        if(last != 0){
            averSum = div(sub(100, sum2Index), last);
        }
        if(index == rates.length - 1){
            rates[index].value = sub(100, sumBeforLast);
        }else{
            let temp = 0.00;
            rates.map((item, i) => {
                if(i > index){
                    if(i !=  rates.length - 1){
                        item.value = averSum;
                        temp = add(temp, averSum);
                    }else{
                        item.value = sub(sub(100, sum2Index), temp);
                    }    
                }
                return item;
            });
        }
    }
    setColValue(table, cycleDetailId, rateKey, rates);
};

/**
 * 获取所有可见行的指定列
 * @param {*} cardTable 
 * @param {*} moduleId 
 * @param {*} colKey 
 */
function getAllVisibleRowCol(cardTable, moduleId, colKey){
    const cols = [];
    const rows = cardTable.getVisibleRows(moduleId);
    if(rows){
        rows.map((item) => {
            cols.push(item.values[colKey]);
        });
    }
    return cols;
}

/**
 * 批量设置某一列的值
 * @param {*} cardTable 
 * @param {*} moduleId 
 * @param {*} colKey 
 * @param {*} values 
 */
function setColValue(cardTable, moduleId, colKey, values){
    setTimeout(() => {
        values.map((item, i) => {
            item.display = item.value;
            cardTable.setValByKeyAndIndex(moduleId, i, colKey, item);
            return item;
        });
    });
}

/**
 * 获取周期类型
 * @param {} page 
 */
function getCycleType(page){
    return page.props.form.getFormItemsValue(tailFormId, 'cycletype').value;
}


// 精度处理
function formatDot(value, scale = 6) {
    let formatVal, dotSplit, val;

    val = (value || 0).toString();

    dotSplit = val.split('.');

    if (dotSplit.length > 2 || !value) {
        return value;
    }

    if (val.indexOf('.') > -1) {
        if (scale == 0) {
            formatVal = dotSplit[0];
        } else {
            formatVal = val.substring(0, val.indexOf('.') + scale + 1);
        }
    } else {
        formatVal = val;
    }

    return formatVal;
}

let scale = 2;
function add(arg1, arg2){
    let result = accAdd(arg1, arg2);
    return formatDot(result, scale);
}

function sub(arg1, arg2){
    let result = Subtr(arg1, arg2);
    return formatDot(result, scale);
}

function mul(arg1, arg2){
    let result = accMul(arg1, arg2);
    return formatDot(result, scale);
}

function div(arg1, arg2){
    let result = accDiv(arg1, arg2);
    return formatDot(result, scale);
}

export {refreshRate, getCycleType, initRate};
