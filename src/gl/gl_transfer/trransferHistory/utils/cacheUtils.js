import { cardCache } from 'nc-lightapp-front';

const dataSource = 'gl.gl_transfer.trransferHistory.'
const searchKey = 'searchKey';
const treeKey = 'treeKey';
const selectTreeKey = 'selectTree';  //选中树节点信息
const tableKey = 'tableKey';
const historyTableKey = 'historyTableKey';

var setDefData = cardCache.setDefData
var getDefData = cardCache.getDefData

class cacheUtils {

    setSearchData(data){
        setDefData(searchKey, dataSource, data);
    }
    getSearchData(){
        return getDefData(searchKey, dataSource);
    }

    setTreeData(data){
        setDefData(treeKey, dataSource, data);
    }
    getTreeData(){
        return getDefData(treeKey, dataSource);
    }
    // selectInfo
    setSelectTreeData(data){
        setDefData(selectTreeKey, dataSource, data);
    }
    getSelectTreeData(){
        return getDefData(selectTreeKey, dataSource);
    }
    setTableData(data){
        setDefData(tableKey, dataSource, data);
    }
    getTableData(){
        return getDefData(tableKey, dataSource);
    }
    setHistoryTableData(data){
        setDefData(historyTableKey, dataSource, data);
    }
    getHistoryTableData(){
        return getDefData(historyTableKey, dataSource);
    }
}

export default new cacheUtils()
