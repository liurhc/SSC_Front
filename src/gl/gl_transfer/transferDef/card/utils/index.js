import {SYSTEMTYPE, TRANS_TIME_LIMIT, CARD_IDS} from '../../consts'

function dealSystemChange(props, moduleId, value){
    if(value.value === SYSTEMTYPE.RECLASSIFY.value){
        let result = props.MultiInit.getLangData('20020SDTRD');
        let month = {
            value:TRANS_TIME_LIMIT.month.value, 
            display:result.json && result.json[TRANS_TIME_LIMIT.displayCode]
        };
        props.form.setFormItemsValue(moduleId, 
            {
                [CARD_IDS.FORM_TRANSITMELIMIT]: month,
                [CARD_IDS.FORM_FREQUENCY]:{display:"1", value:"1"}
            }
        );
        props.form.setFormItemsDisabled(moduleId, {
            [CARD_IDS.FORM_TRANSITMELIMIT]:true,
            [CARD_IDS.FORM_FREQUENCY]:true
        })
    }else{
        props.form.setFormItemsDisabled(moduleId, {
            [CARD_IDS.FORM_TRANSITMELIMIT]:false,
            [CARD_IDS.FORM_FREQUENCY]:false
        });
    }
}

export {dealSystemChange}