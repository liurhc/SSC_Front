import {toast, promptBox, cardCache } from 'nc-lightapp-front';
import {linkToList, delFun} from '../../utils';
import {CARD_IDS, PATH, DATA_SOURCE} from '../../consts';
let {getCurrentLastId} = cardCache;

export default function(props, id) {
    let page = this;
    switch (id) {
        case 'add': add(page); break;
        case 'update': update(page); break;
        case 'delete': del(page); break;
        case 'refresh': refresh(page); break;
        case 'cp2others': cp2others(page); break;
        case 'mv2sort': mv2sort(page); break;
        case 'save': save(page); break;
        case 'cancel': cancel(page); break;
        case 'back': back(page); break;
        case 'addLine': addLine(page); break;
        case 'delLine': delLines(page); break;
        case 'cpLine': cpLine(page); break;
    }
}

function add(page){
    let data = {
        status: 'add',
        pk_accountingbook: page.pk_accountingbook,
        pk_transsort: page.pk_transsort
    }
    page.props.pushTo(PATH.CARD, data);
    page.initAddPage();
    page.toggleShow();
    page.setState({});
}

function update(page){
    let data = {
        status: 'edit', 
        id: page.props.getUrlParam('id'),
        pk_accountingbook: page.pk_accountingbook,
        pk_transsort: page.pk_transsort
    }
    page.props.pushTo(PATH.CARD, data);
    page.toggleShow();
}

function del(page){
    let id = page.pk_transfer;
    let rows = page.props.form.getAllFormValue(CARD_IDS.FORM_ID);
    delFun(page, rows, (res) => {
        if(res.success){
            if(res.data && res.data.length > 0){
                let result = res.data[0];
                if(result.result){
                    toast({content:page.state.json['20020SDTRD-000003']});/* 国际化处理： 删除成功！*/
                    page.handleDel();
                }else{
                    toast({content:result.errorMsg, color:'danger'});
                }
            }
        }
    });
    
}


function refresh(page){
	let pk_transfer = page.props.getUrlParam('id');
    page.refresh(pk_transfer, () => {
		toast({content:page.state.json['20020SDTRD-000028']})
    });
}


function cp2others(page){
    let {accbookModalShow} = page.state;
    accbookModalShow = true;
    page.setState({accbookModalShow});
}

function mv2sort(page){
    if(!getPk_transfer(page)){
        toast({content:page.state.json['20020SDTRD-000005']});/* 国际化处理： 请选择转账定义*/
        return;
    }
    page.setState({
        transsortModalShow:true
    })
}


function save(page){
    if(!page.props.form.isCheckNow(CARD_IDS.FORM_ID)){
        return;
    }
    let details = page.props.cardTable.getVisibleRows(CARD_IDS.TABLE_ID);
    if(!details || details.length <= 0){
        toast({content:page.state.json['20020SDTRD-000006'], color:"warning"});/* 国际化处理： 分录不能为空！*/
        return;
    }

    if(!page.props.cardTable.checkTableRequired(CARD_IDS.TABLE_ID)){
        return;
    }

    page.saveBill();
}

function cancel(page){
    promptBox({
        color:'warning',
        title: page.state.json['20020SDTRD-000007'],/* 国际化处理： 取消*/
        content: page.state.json['20020SDTRD-000008'],/* 国际化处理： 确定要取消吗？*/
        beSureBtnClick: () => {
            let props = page.props;
            let pk_transfer = page.pk_transfer;
            if(!pk_transfer)
                pk_transfer = getCurrentLastId(DATA_SOURCE);
            props.pushTo(PATH.CARD, {status: 'browse',id: pk_transfer});
            page.getData(pk_transfer);
            page.toggleShow();         
        },
        cancelBtnClick: () => {}
    });

}

function back(page){
    linkToList(page);
}

function addLine(page){
    page.props.cardTable.addRow(CARD_IDS.TABLE_ID);
}

function delLines(page){
    let indexs = getCheckedRowIndex(page);
    if(indexs){
        page.props.cardTable.delRowsByIndex(CARD_IDS.TABLE_ID, indexs);
        page.updateDelLineBtn();
    }
}

function cpLine(page){
    let rows = page.props.cardTable.getCheckedRows(CARD_IDS.TABLE_ID);
    if(rows){
        rows.map((record) => {
            setTimeout(() => page.props.cardTable.pasteRow(CARD_IDS.TABLE_ID, record.index), 0);
        });
    }
}

function getCheckedRowIds(page){
    let rows = page.cardTable.getCheckedRows(CARD_IDS.TABLE_ID);
    if(rows){
        let rowIds = [];
        rows.map((row) => {
            rowIds.push(row.rowID);
        });
        return rowIds;
    }
}

function getCheckedRowIndex(page){
    let rows = page.props.cardTable.getCheckedRows(CARD_IDS.TABLE_ID);
    if(rows){
        let indexs = [];
        rows.map((row) => {
            indexs.push(row.index);
        });
        return indexs;
    }
}

function getPk_transfer(page){
    return page.props.form.getFormItemsValue(CARD_IDS.FORM_ID, 'pk_transfer').value;
}

