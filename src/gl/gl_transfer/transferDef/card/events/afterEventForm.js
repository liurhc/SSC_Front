import {RegUtils, REG} from '../../../../public/common/StrReg.js';
import {CARD_IDS, SYSTEMTYPE, TRANS_TIME_LIMIT} from '../../consts.js'
import {dealSystemChange} from '../utils/index'
let cardIds = CARD_IDS;
/**
 * 表单编辑后事件
 * add by qinhaof 
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 */
export default function onAfterEventForm(props, moduleId, key, value, oldValue){
    if(moduleId == cardIds.FORM_ID){
        if(key == cardIds.FORM_BILL_NUM || key == cardIds.FORM_FREQUENCY){
            dealPosIntItem(props, moduleId, key, value);
        }else if(key == cardIds.FORM_PK_SYSTEM){
            dealSystemChange(props, moduleId, value);
        }
    }
}

/**
 * 是否正整数
 * @param {} num 
 */
function regPosInt(num){
    return RegUtils.regNum(REG.NUM.POS_INT, num);
}

/**
 * 正整数字段校验
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 */
function dealPosIntItem(props, moduleId, key, value){
    if(!regPosInt(value.value)){
        props.form.setFormItemsValue(moduleId, {[key]:''});
    }
}

