import {ajax, cardCache} from 'nc-lightapp-front';
import {DATA_SOURCE, CARD_IDS, PAGE_CODE, ACTION, PKNAME} from '../../consts'
let {getCacheById, updateCache} = cardCache;
let formId = CARD_IDS.FORM_ID;
let tableId = CARD_IDS.TABLE_ID;

export default function (props, pk) {
    let page = this;
    page.props.setUrlParam({id:pk})//动态修改地址栏中的id的值
    page.getData(pk);
}

function loadData(page, data){
    if(data){
        if(data.head){
            page.props.form.setAllFormValue({ [formId]: data.head[formId] });
        }else{
            page.props.form.setAllFormValue({ [formId]: { rows: [] } });
        }
        if(data.body){
            page.props.cardTable.setTableData(tableId, data.body[tableId]);
        }else{
            page.props.cardTable.setTableData(tableId, { rows: [] });
        }
    }else{
        page.props.form.setAllFormValue({ [formId]: { rows: [] } });
        page.props.cardTable.setTableData(tableId, { rows: [] });
    }

}
