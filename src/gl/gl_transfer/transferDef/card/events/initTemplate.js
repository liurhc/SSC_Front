import { base, ajax, getBusinessInfo } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
import {APPCODE, PAGE_CODE, CARD_IDS} from '../../consts';

const formId = CARD_IDS.FORM_ID;
const tableId = CARD_IDS.TABLE_ID;
const pageId = PAGE_CODE.CARD;
export default function(props) {
	let page = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: APPCODE
		}, 
		function (data){
			let status = props.getUrlParam('status');
			if(data){
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
				if(data.template){
					let meta = data.template;
					modifierMeta(page, meta);
					props.meta.setMeta(meta);
					if(status === 'add'){
						page.initAddPage();
					}
				}
			}   
		}
	)
}

function modifierMeta(page, meta) {
	let status = page.props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	initFormMeta(page, meta);
	initTableMeta(page, meta);
	return meta;
}


function initFormMeta(page, meta){
	let buziInfo = getBusinessInfo();
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_vouchertype') {
			item.queryCondition = () => {
				return getVouTypeQryCondition(page);
			};
		}else if(item.attrcode == 'pk_accountingbook'){
			item.initialvalue = {value:page.pk_accountingbook};
		}else if(item.attrcode == 'pk_transsort'){
			item.initialvalue = {value:page.pk_transsort};
		}
	});

}

function initTableMeta(page, meta){
	let props = page.props;
	meta[tableId].items.push({attrcode:'assValues'});
	meta[tableId].items.push({attrcode:'quantity', initialvalue:{value:'N'}});
	meta[tableId].items.push({attrcode:'assItems'});
	meta[tableId].items.map((item) => {
		if(item.attrcode == 'ass'){
			item.queryCondition = () => {
                let data = props.form.getFormItemsValue(formId, 'pk_accountingbook').value;
                return { pk_org: data, "isDataPowerEnable": 'Y',"DataPowerOperationCode" : 'fi' };
			};
		}else if(item.attrcode == 'pk_accasoa'){
			item.isAccountRefer = true;
			item.isMultiSelectedEnabled = true;
			item.isShowDisabledData = false;
			item.queryCondition = () => {
				return getAccountQryCondition(page);
			}
		}else if(item.attrcode == 'pk_currtype'){
			item.refcode = 'gl/refer/voucher/TransCurrGridRef/index';
			item.queryCondition = {"isDataPowerEnable": 'Y',"DataPowerOperationCode" : 'fi'};
		}else if(item.attrcode == 'orientation'){
			// item.options.unshift({display:"", value: ""});
		}
	});
}

function getAccountQryCondition(page){
	let bizeInfo = getBusinessInfo();
	let {pk_accountingbook} = page.cacheUtil.getListState();
	let dateStr = bizeInfo.businessDate.split(' ')[0];
	let isDataPowerEnable =  'Y';
	let DataPowerOperationCode = 'fi';
	return {pk_accountingbook, dateStr, isDataPowerEnable, DataPowerOperationCode};
}

function getVouTypeQryCondition(page){
	let bizeInfo = getBusinessInfo();
	let {pk_accountingbook} = page.cacheUtil.getListState();
	let pk_group = bizeInfo.groupId;
	let condition = {
		pk_group : pk_group,
		pk_org : pk_accountingbook,
		isDataPowerEnable : 'Y',
		DataPowerOperationCode : 'fi'
	}
	return condition;
}
