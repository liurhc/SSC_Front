import {APPCODE, PAGE_CODE, PATH, ACTION} from '../consts';
import {toast, ajax, promptBox} from 'nc-lightapp-front';
import {extractGridModelFromTableRows} from '../../../public/utils/tableUtil'

export function linkToCard(page, data){
    let props = page.props;
    page.cacheUtil.setListState(page.state);
    const params = {
        pagecode : PAGE_CODE.CARD,
        appcode : APPCODE
    }
    if(data) Object.assign(params, data);
    props.pushTo(PATH.CARD, params);
}

export function linkToList(page, data){
    let props = page.props;
    const params = {
        pagecode : PAGE_CODE.LIST,
        appcode : APPCODE
    }
    if(data) Object.assign(params, data);
    props.pushTo(PATH.LIST, params);
}

/**
 * 复制到其他核算账簿
 * @param {*} transDefs 
 * @param {*} pk_accountingbooks 
 * @param {*} callback 
 */
function cp2othersFun(page, transDefs, pk_accountingbooks, callback){
    let url = ACTION.CP2OTHERS;
    if(!transDefs || transDefs.length <= 0){toast({content:page.state.json['20020SDTRD-000005'], color:'warning'}); return;}/* 国际化处理： 请选择转账定义*/
    if(!pk_accountingbooks || pk_accountingbooks.length <= 0){toast({content:page.state.json['20020SDTRD-000009'], color:'warning'}); return}/* 国际化处理： 请选择核算账簿*/
    let data = {
        pk_transdefs: transDefs,
        pk_accountingbooks: pk_accountingbooks
    }
    ajax({
        url,
        data,
        success: function(res){
            if(callback){
                callback(res);           
            }
        }
    })
}

function mv2sortFun(page, pk_transfer, pk_transsort, callback){
    let url = ACTION.MV2SORT;
    if(!pk_transfer) { toast({content: page.state.json['20020SDTRD-000005']}); return; }/* 国际化处理： 请选择转账定义*/
    if(!pk_transsort) { toast({content: page.state.json['20020SDTRD-000010']}); return; }/* 国际化处理： 请选择转账定义分类*/
    let data = {
        pk_transfer: pk_transfer,
        pk_transsort: pk_transsort
    }
    ajax({
        url,
        data,
        success: function(res){
            if(callback){
                callback(res);
            }
        }
    })
}

function delFun(page, rows, callback){
    promptBox({
        color : 'warning',
        content : page.state.json['20020SDTRD-000011'],/* 国际化处理： 您确定要删除所选数据吗？*/
        beSureBtnClick:() => {
            let url = ACTION.DELETE;
            if(!rows) { toast({content: page.state.json['20020SDTRD-000005']}); return; }/* 国际化处理： 请选择转账定义*/
            let data = rows;
            ajax({
                url, 
                data, 
                success: function(res){
                    if(callback){
                        callback(res);
                    }
                }
            })
        }
    });

}

/**
 * 移动到核算账簿之前校验是否存在重复编码的定义
 * @param {*} pk_accountingbooks 
 * @param {*} codes 
 * @param {*} callback 
 */
function checkMultiCode(page, pk_accountingbooks, codes, callback){
	let url = '/nccloud/gl/transfer/checkMultiCode.do'
	let sendData = {
		"pk_accountingbooks": pk_accountingbooks,
		"codes": codes
	}
	ajax({
		url: url,
		data: sendData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
                if(callback){
                    callback(data);
                }
			}
		},
		error: function(res) {
			toast({content:res.message});
		}
	});
}

export {cp2othersFun, mv2sortFun, delFun, checkMultiCode}
