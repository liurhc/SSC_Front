import CacheUtils from '../../../public/common/CacheUtil.js'

const dataSource = 'gl.gl_transfer.transferDef.';
const transSortKey = 'transSort';
const selectedNodeKey = 'selectedNode';
const listStateKey = 'listState';
const context = 'context';
const BTN_STATE = 'btnState';
const SELECTEDACCBOOK = "accountingbook";

class TransdefCacheUtil extends CacheUtils{
    constructor(ViewModel){
        super(ViewModel, dataSource);
    }

    setTransSort(pk_accountingbook, data){
        this.setData(pk_accountingbook, data);
    }

    getTransSort(pk_accountingbook){
        return this.getData(pk_accountingbook);
    }

    setListState(data){
        this.setData(listStateKey, data);
    }

    getListState(){
        return this.getData(listStateKey);
    }

    setContext(data){
        this.setData(context, data);
    }

    getContext(){
        return this.getData(context);
    }

    setSelectNode(data){
        this.setData(selectedNodeKey, data);
    }

    getSelectNode(){
        return this.getData(selectedNodeKey);
    }

    setSelectAccBook(data){
        this.setData(SELECTEDACCBOOK, data);
    }

    getSelectAccBook(){
        return this.getData(SELECTEDACCBOOK);
    }

    getBtnState(){
        return this.getData(BTN_STATE);
    }

    setBtnState(btnState){
        let state = this.getBtnState();
        if(!state){
            state = {};
        }
        Object.assign(state, btnState);
        this.setData(BTN_STATE, state);
    }

}

export default TransdefCacheUtil;
