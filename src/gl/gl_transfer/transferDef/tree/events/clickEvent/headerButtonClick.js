import {base, toast} from 'nc-lightapp-front';
import {linkToCard, delFun} from '../../../utils';
import {LIST_IDS} from '../../../consts';
import {filterRowsByAttr, extractGridModelFromTableRows} from '../../../../../public/utils/tableUtil'
export default function (props, id) {
    switch (id) {
        case 'add': add(this); break;
        case 'update': update(this); break;
        case 'delete': del(this); break;
        case 'refresh': refresh(this); break;
        case 'cp2others': cp2others(this); break;
        case 'mv2sort': mv2sort(this); break;
        default:
            break;
    }
    
}


function add(page){
    let {pk_accountingbook, pk_transsort} = page.state;
    if(!pk_accountingbook){
        toast({content:page.state.json['20020SDTRD-000009'], color:"warning"});/* 国际化处理： 请选择核算账簿*/
        return;
    }
    if(!pk_transsort){
        toast({content:page.state.json['20020SDTRD-000014'], color:"warning"});/* 国际化处理： 请选择分类*/
        return;
    }
    let data = {
        status:'add', 
        pk_accountingbook:pk_accountingbook, 
        pk_transsort:pk_transsort
    };
    linkToCard(page, data);
}

function update(page){
    let {pk_accountingbook, pk_transsort} = page.state;
    let id = getCheckedIds(page.props);
    if(!id){
        toast({content:page.state.json['20020SDTRD-000005']});/* 国际化处理： 请选择转账定义*/
        return;   
    }
    if(id.length > 1){
        toast({content:'too many items ...'});
        return;
    }
    let data = {
        status: 'edit',
        id: id[0],
        pk_accountingbook: pk_accountingbook,
        pk_transsort: pk_transsort
    }
    linkToCard(page, data);
}

function del(page){
    let rows = page.props.table.getCheckedRows(LIST_IDS.TABLE_ID);
    if(!rows){
        toast({content:page.state.json['20020SDTRD-000005']});/* 国际化处理： 请选择转账定义*/
        return;   
    }
    rows = extractGridModelFromTableRows(rows);
    delFun(page, rows, (res) => {
        if(res.success){
            if(res.data){
                let result = false;
                let successPks = [];
                res.data.map((item) => {
                    if(item.result){
                        result = item.result;
                        successPks.push(item.pk);
                    }else{
                        toast({content:item.errorMsg, color:"error"});
                    }
                });
                if(result)
                    toast({content:page.state.json['20020SDTRD-000015']});/* 国际化处理： 删除成功*/
                if(successPks && successPks.length){
                    let successIndexs = filterCheckedIndexsByPks(page.props, successPks);
                    page.props.table.deleteTableRowsByIndex(LIST_IDS.TABLE_ID, successIndexs);
                    let {deleteCacheId} = page.props.table;
                    successPks.map((pk) => {
                        deleteCacheId(LIST_IDS.TABLE_ID, pk);
                    });
                }
            }
        }
    });
    
}

function refresh(page){
    let selectedAccBook = page.cacheUtil.getSelectAccBook();
    page.cacheUtil.clearAll();
    page.cacheUtil.setSelectAccBook(selectedAccBook);
    page.loadTreeData(() => toast({content:page.state.json['publiccommon-000004']}));
}

function cp2others(page){
    let {accbookModalShow} = page.state;
    accbookModalShow = true;
    page.setState({accbookModalShow});
}

function mv2sort(page){
    if(!getCheckedIds(page.props) || getCheckedIds(page.props).length <= 0){
        toast({content:page.state.json['20020SDTRD-000005']});/* 国际化处理： 请选择转账定义*/
        return;
    }
    page.setState({
        transsortModalShow:true
    })
}

/**
 * 获取选中行ID
 * @param {*} props 
 */
function getCheckedIds(props){
    let rows = props.table.getCheckedRows(LIST_IDS.TABLE_ID);
    if(rows){
        let ids = [];
        rows.map((row) => {
            ids.push(row.data.values.pk_transfer.value);
        });
        return ids;
    }
}

function filterCheckedIndexsByPks(props, pks){
    let rows = props.table.getCheckedRows(LIST_IDS.TABLE_ID);
    rows = filterRowsByAttr(rows, "pk_transfer",  pks);
    if(rows && rows.length){
        let indexs = [];
        rows.map((row) => {
            indexs.push(row.index);
        })
        return indexs;
    }
}
