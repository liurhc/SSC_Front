export default function(props, id) {
	switch (id) {
		case 'add':
			props.editTable.addRow('purchaseOrderCardTable');
			break;
		case 'save':
			props.editTable.save('purchaseOrderCardTable', function(changedRows, allRows) {
			});
			break;
		case 'edit':
			props.editTable.edit('purchaseOrderCardTable', function() {
			});
			break;
		case 'cancel':
			props.editTable.cancelEdit('purchaseOrderCardTable', function() {
			});
			break;
		case 'del':
			props.editTable.delRow('purchaseOrderCardTable', 0);
			break;
		default:
			break;
	}
}
