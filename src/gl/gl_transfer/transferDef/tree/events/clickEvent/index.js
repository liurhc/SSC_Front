
import pageInfoClick from './pageInfoClick';
import afterEvent from './afterEvent';
import buttonClick from './buttonClick';
import headerButtonClick from './headerButtonClick';


export { pageInfoClick ,afterEvent, buttonClick, headerButtonClick };
