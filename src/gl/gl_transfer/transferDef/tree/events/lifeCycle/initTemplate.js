import {ajax,base} from 'nc-lightapp-front';
import {APPCODE, PAGE_CODE, LIST_IDS} from '../../../consts';
import {linkToCard} from '../../../utils'
export default function (props) {
    let page = this;
    props.createUIDom(
        {
            appcode : APPCODE,
            pagecode : PAGE_CODE.LIST
        }, 
        function(data){
            if(data){
                if(data.button){
                    let button = data.button;
                    initBtn(page, button);
                }
                if(data.template){
                    let template = data.template;
                    initMeta(page, template);
                }
                if(data.context){
                    let context = data.context
                    page.cacheUtil.setContext(context)
                    
                }
            }
        }
    );
}

function initBtn(page, button){
    page.props.button.setButtons(button);
    let btnStateCache = page.cacheUtil.getBtnState();
    let btnState = {add:true, update:true, delete:true, cp2others:true, mv2sort:true}
    Object.assign(btnState, btnStateCache);
    page.props.button.setButtonDisabled(btnState);
}

function initMeta(page, template){
    let meta = template;
    meta[LIST_IDS.TABLE_ID].items.map((item) => {
        if (item.attrcode == 'transferno') {
            item.render = (text, record, index) => {
                if(record)
                return (
                    <a
                        style={{ textDecoration: 'underline', cursor: 'pointer' }}
                        onClick={() => {
                            linkToCard(page, {
                                status: 'browse',
                                id: record.pk_transfer.value
                            });
                        }}
                    >
                        {record.transferno.value}
                    </a>
                );
            };
        }
        return item;
    });
    page.props.meta.setMeta(meta);
}
