import { pageInfoClick,afterEvent, buttonClick, headerButtonClick } from './clickEvent';
import { initTemplate } from './lifeCycle';

export { pageInfoClick, initTemplate, afterEvent, buttonClick, headerButtonClick  };
