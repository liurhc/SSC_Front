import {RenderRouter} from 'nc-lightapp-front';
import routes from './router.js';

(function main(routers, htmlTagid){
    RenderRouter(routers, htmlTagid)
})(routes, "app");
