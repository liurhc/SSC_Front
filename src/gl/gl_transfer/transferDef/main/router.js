import {asyncComponent} from 'nc-lightapp-front';
import SyncTree from '../tree';

const card = asyncComponent(() => import(/* webpackChunkName: "gl/gl_transfer/transferDef/card/card" */ '../card'));

const routes = [
    {path:'/', component: SyncTree, exact: true},
    {path:'/list', component: SyncTree},
    {path:'/card', component:card}
];

export default routes;
