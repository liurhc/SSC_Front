import React, { Component } from 'react';
import { base, getMultiLang } from 'nc-lightapp-front';
import ReferLoader from '../../../public/ReferLoader/index.js';
import { accbookRefcode } from '../../../public/ReferLoader/constants';
const { NCModal, NCButton } = base;
import './index.less';
export default class AccountingBookModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			accountbooks: [],
			show: false,
			json: {}
		};
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'2002PUBCOMPONENTS',domainName:'gl',currentLocale:'simpchn',callback});
	}
	componentWillReceiveProps(nextProps) {
		let { show, defaultValue } = nextProps;
		let accountbooks = defaultValue;
		this.setState({ show, accountbooks });
	}

	handleConfirm = () => {
		let { accountbooks } = this.state;
		this.props.onConfirm(accountbooks);
	};

	handleClose = () => {
		this.setState({ show: false });
		this.props.onClose();
	};

	render() {
		let { accountbooks, show } = this.state;
		let { context } = this.props;
		return (
			<NCModal fieldid='account' show={show} className="simpleModal junior" onHide={this.close} id="simple">
				<NCModal.Header>
					<NCModal.Title>{this.state.json['2002PUBCOMPONENTS-000000']/* 国际化处理： 复制到*/}</NCModal.Title>
				</NCModal.Header>
				<NCModal.Body>
					<div className='accountingBookModal'>
						<span className='copy-lable'>{this.state.json['2002PUBCOMPONENTS-000001']/* 国际化处理： 财务核算账簿*/}</span>
						<div className='refer-w'>
							<ReferLoader
								tag="mv2others"
								fieldid='accountbooks'
								value={accountbooks}
								refcode={accbookRefcode}
								placeholder={this.state.json['2002PUBCOMPONENTS-000002']}/* 国际化处理： 复制到其他核算账簿*/
								isMultiSelectedEnabled={true}
								showGroup={false}
								showInCludeChildren={true}
								disabledDataShow = {true}
								queryCondition={() => {
									let condition = {
										TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
										"isDataPowerEnable": 'Y',
										"DataPowerOperationCode" : 'fi'
									};
									condition.appcode = context.appcode;
									return condition;
								}}
								onChange={(v) => {
									accountbooks = v;
									this.setState({ accountbooks });
								}}
							/>
						</div>
					</div>
				</NCModal.Body>
				<NCModal.Footer>
					<NCButton fieldid='confirm' className="button-primary" onClick={this.handleConfirm}>
						{this.state.json['2002PUBCOMPONENTS-000003']/* 国际化处理： 确定*/}
					</NCButton>
					<NCButton fieldid='cancel' onClick={this.handleClose}>{this.state.json['2002PUBCOMPONENTS-000004']/* 国际化处理： 取消*/}</NCButton>
				</NCModal.Footer>
			</NCModal>
		);
	}
}
