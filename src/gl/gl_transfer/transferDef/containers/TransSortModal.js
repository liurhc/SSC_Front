import React, { Component } from 'react';
import TransSortTreeRef from '../../../refer/voucher/TransSortTreeRef';
import { base } from 'nc-lightapp-front';
const { NCModal, NCButton } = base;

export default class TransSortModal extends Component {
	constructor(props) {
		super(props);
		let defaultValue = this.props.defaultValue;
		this.state = {
			pk_transsort: '',
			transSortName: ''
		};
		if (defaultValue) {
			(this.state.pk_transsort = defaultValue.pk_transsort),
				(this.state.transSortName = defaultValue.transSortName);
		}
		// this.setState(state);
	}

	handleTransSortChange = (v) => {
		let { pk_transsort, transSortName } = this.state;
		if (v) {
			pk_transsort = v.refpk;
			transSortName = v.refname;
		} else {
			pk_transsort = '';
			transSortName = '';
		}
		this.setState({ pk_transsort, transSortName });
	};

	handleConfirm = () => {
		let { pk_transsort, transSortName } = this.state;
		this.props.onConfirm({ pk_transsort, transSortName });
	};

	render() {
		let { pk_transsort, transSortName } = this.state;
		let {pk_accountingbook} = this.props;
		return (
			<NCModal fieldid='transsort' show={this.props.show} onHide={this.props.onClose} className="simpleModal junior">
				<NCModal.Header>
					<NCModal.Title>{this.props.json['20020SDTRD-000026']/* 国际化处理： 转移到分类*/}</NCModal.Title>
				</NCModal.Header>
				<NCModal.Body>
					<div className='accountingBookModal'>
						<span className='copy-lable'>{this.props.json['20020SDTRD-000027']/* 国际化处理： 结转档案分类*/}</span>
						<div className='refer-w'>
							<TransSortTreeRef
								fieldid='pk_transsort'
								value={{ refpk: pk_transsort, refname: transSortName }}
								onlyLeafCanSelect={true}
								queryCondition={{
									"isDataPowerEnable": 'Y',
									"DataPowerOperationCode" : 'fi',
									pk_accountingbook:pk_accountingbook
								}}
								onChange={this.handleTransSortChange}
							/>
						</div>
					</div>
				</NCModal.Body>
				<NCModal.Footer>
					<NCButton className="button-primary" fieldid='confirm' onClick={this.handleConfirm}>
						{this.props.json['20020SDTRD-000022']/* 国际化处理： 确定*/}
					</NCButton>
					<NCButton fieldid='cancel' onClick={this.props.onClose}>{this.props.json['20020SDTRD-000007']/* 国际化处理： 取消*/}</NCButton>
				</NCModal.Footer>
			</NCModal>
		);
	}
}
