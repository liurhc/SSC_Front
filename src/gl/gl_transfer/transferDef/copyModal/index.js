import React, { Component } from 'react';
import { high, ajax, base } from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	AutoComplete,
	NCDropdown: Dropdown,
	NCButtonGroup: ButtonGroup,
	NCModal: Modal
} = base;
const NCTreeNode = NCTree.NCTreeNode;
export default class ClassModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showModal: false,
			pk_accountingbook: '',
			treeData: [],
			selDatas: [],
			selectedKeys: []
		};
	}

	componentWillReceiveProps(nextProps) {
		if (this.state.pk_accountingbook != nextProps.pk_accountingbook) {
			this.loadDept(nextProps.pk_accountingbook);
		}
		let showModal = nextProps.classModalShow;
		let pk_accountingbook = nextProps.pk_accountingbook;
		let selDatas = nextProps.selectArr;
		this.setState({
			showModal,
			pk_accountingbook,
			selDatas
		});
	}
	componentDidMount() {
		this.loadDept();
	}
	handleConfirm = () => {
		this.props.getClassModalShow(false);
		let { selDatas, selectedKeys } = this.state;
		let data = { pk_transsort: selectedKeys[0] };
		this.props.onConfirm(data);
	};
	loadDept(pk_accountingbook) {
		//加载树数据
		ajax({
			loading: true,
			url: '/nccloud/gl/voucher/transSortTree.do',
			data: { pk_accountingbook: pk_accountingbook }, //参数带上选中的行政组织
			success: (res) => {
				let { success, data } = res;
				if (success && data) {
					//data.push(factorRoot);
					this.dealTreeData(data);
					this.setState({
						treeData: data.children
					});
				}
			}
		});
	}
	dealTreeData(data) {
		let deleteDataChildrenProp = function(node) {
			if (!node.children || node.children.length == 0) {
				delete node.children;
			} else {
				node.isLeaf = false;
				node.children.forEach((e) => {
					deleteDataChildrenProp(e);
				});
			}
		};
		if (data) {
			if (data.children) {
				data.children.forEach((e) => {
					deleteDataChildrenProp(e);
				});
			}
		}
		return data;
	}
	render() {
		const loop = (data) => {
			if (data && data.length > 0) {
				data.map((item) => {
					if (item.children) {
						return (
							<NCTreeNode title={item.refname} key={item.refpk}>
								{loop(item.children)}
							</NCTreeNode>
						);
					}
					return <NCTreeNode title={item.refname} key={item.refpk} isLeaf={item.isLeaf} />;
				});
			}
		};
		const treeNodes = loop(this.state.treeData);
		return (
			<div>
				<Modal
					className="simpleModal"
					show={this.state.showModal}
					// onHide = { this.close }
				>
					<Modal.Header>
						<Modal.Title>{this.state.json['20020SDTRD-000021']}</Modal.Title>{/* 国际化处理： 分类选择框*/}
					</Modal.Header>
					<Modal.Body>
						<NCTree
							onSelect={this.onSelect}
							defaultExpandedKeys={this.state.defaultExpandedKeys}
							multiple={false}
							onSelect={(selectedKeys, e) => {
								this.setState({
									selectedKeys: selectedKeys
								});
							}}
						>
							{treeNodes}
						</NCTree>
					</Modal.Body>

					<Modal.Footer>
						<Button className="button-primary" onClick={this.handleConfirm}>
							{this.state.json['20020SDTRD-000022']}{/* 国际化处理： 确定*/}
						</Button>
						<Button
							onClick={() => {
								this.props.getClassModalShow(false);
							}}
						>
							{this.state.json['20020SDTRD-000007']}{/* 国际化处理： 取消*/}
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
}
