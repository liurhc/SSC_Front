const APPCODE = '20020SDTRD';
const PAGE_CODE = {
    LIST : '20020SDTRD_LIST',
    CARD : '20020SDTRD_CARD'
};
const CARD_IDS = {
    FORM_ID : 'voucher',
    TABLE_ID : 'transfer',
    FORM_BILL_NUM : 'billnum',
    FORM_FREQUENCY : 'transfrequency',
    FORM_PK_SYSTEM : 'pk_system',
    FORM_TRANSITMELIMIT : 'transtimelimit',

};
const LIST_IDS = {
    TABLE_ID : 'pk_transfer'
};

const PATH = {
    LIST : '/list',
    CARD : '/card'
}

const ACTION = {
    CP2OTHERS : '/nccloud/gl/voucher/copyToOtherAccbook.do',
    MV2SORT : '/nccloud/gl/voucher/moveToSort.do',
    DELETE : '/nccloud/gl/voucher/deleteTransDef.do',
    QRY_TRANS_SORT : '/nccloud/gl/voucher/transSortTree.do',
    QRY_TRANS_CARD : '/nccloud/gl/voucher/transDefCard.do'
}

const SYSTEMTYPE = {
    RECLASSIFY : {display:'', value : 'GLRC'}, /* 重分类 */
    ORDINARY_TRANSFER : {display:'', value:'OT'}, /* 普通结转 */
    PROFIT_AND_LOSS_CARRIED_FORWARD : {display:'', value:'PLCF'} /* 损益结转 */
}

/**
 * 转账时间
 */
const TRANS_TIME_LIMIT = {
    month: {displayCode: '20020SDTRD-000018', value: "1"},/* 国际化处理： 月*/
    quarter: {displayCode: '20020SDTRD-000019', value: "2"},/* 国际化处理：  季*/
    year: {displayCode: '20020SDTRD-000020', value: "3"}/* 国际化处理：  年*/
}

const DATA_SOURCE = 'fip.gl.transferDef.list_card';

const PKNAME = 'pk_transfer';

export {APPCODE, PAGE_CODE, CARD_IDS, LIST_IDS, PATH, ACTION, DATA_SOURCE, PKNAME, SYSTEMTYPE, TRANS_TIME_LIMIT}
