import React, { Component } from 'react';
import { base, ajax, getMultiLang } from 'nc-lightapp-front';
const { NCTable: Table, NCCheckbox: Checkbox } = base;
import { InputItem } from '../../../../public/components/FormItems';
import { getTableHeight } from '../../../../public/common/method.js';

export default class SelfDocTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: [], //表格数据
			checkedData: [], //选中数据
			checkedAll: false, //是否全选
			checkedArray: [], //各行选中判断
			showStar: null,
			json: {}
		};
	}
	getTableColumns = (tableData, showStar) => {
		let iStyle = showStar ? { color: '#E14C46' } : { color: '#f3f3f3' }
		let columns = [
			{
				title: (<div fieldid='index'>{this.state.json['20020SDTPDPAGE-000006']}</div>),/* 国际化处理： 序号*/
				dataIndex: 'index',
				key: 'index',
				width: '160px',
				render: (text, record, index) => <div fieldid='index'>{index+1}</div>
			},
			{
				title: <div fieldid='m_transprojectcode'><i style={iStyle}>*</i>{this.state.json['20020SDTPDPAGE-000008']}</div>,/* 国际化处理： 编号*/
				dataIndex: 'm_transprojectcode',
				key: 'm_transprojectcode',
				width: '160px',
				render: (text, record, index) => (
					<InputItem
						fieldid='m_transprojectcode'
						defaultValue={tableData[index].m_transprojectcode}
						isViewMode={tableData[index].isEdit}
						maxlength={25}
						onChange={(v) => {
							tableData[index].m_transprojectcode = v;
							this.props.sendMainData(tableData);
						}}
					/>
				)
			},
			{
				title: <div fieldid='m_transprojectname'><i style={iStyle}>*</i>{this.state.json['20020SDTPDPAGE-000009']}</div>,/* 国际化处理： 名称*/
				dataIndex: 'm_transprojectname',
				key: 'm_transprojectname',
				width: '160px',
				render: (text, record, index) => (
					<InputItem
						fieldid='m_transprojectname'
						defaultValue={tableData[index].m_transprojectname}
						isViewMode={tableData[index].isEdit}
						maxlength={25}
						onChange={(v) => {
							// let tableData = this.state.tableData;
							tableData[index].m_transprojectname = v;
							this.props.sendMainData(tableData);
						}}
					/>
				)
			},
			{
				title: (<div fieldid='m_memo'>{this.state.json['20020SDTPDPAGE-000007']}</div>),/* 国际化处理： 方案说明*/
				dataIndex: 'm_memo',
				key: 'm_memo',
				width: '160px',
				render: (text, record, index) => (
					<InputItem
						fieldid='m_memo'
						defaultValue={this.state.tableData[index].m_memo}
						isViewMode={this.state.tableData[index].isEdit}
						maxlength={75}
						onChange={(v) => {
							// let maxLength = 50
							// let tempVal = (v.length <= maxLength) ? v : v.slice(0, maxLength)

							let tableData = this.state.tableData;
							tableData[index].m_memo = v;
							this.props.sendMainData(tableData);
						}}
					/>
				)
			}
		];
		return columns
	}
	deleteLine = (index) => {
		let {accountingbook,tableData} = this.state
		let data = {
			pk_accountingbook: accountingbook.refpk,
			pk_transprojs: tableData[index].m_pk_transproject //方案主键数组
		};
		let self = this;
		let url = '/nccloud/gl/voucher/delTransProject.do';
		ajax({
			url, 
			data,
			success: (res) => {
				if (res.success) {
					tableData.splice(index, 1)
					self.setState({
						tableData
					});
				}
			}
		});
	}
	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'20020SDTPDPAGE',domainName:'gl',currentLocale:'simpchn',callback});
	}
	componentWillReceiveProps(nextProps) {
		if(this.props.mainData == nextProps.mainData && this.props.showStar === nextProps.showStar){
			return;
		}
		let loCalData = nextProps.mainData;
		let accountingbook = nextProps.accountingbook;
		let showStar = nextProps.showStar;
		let checkedArray = [];
		for (let i = 0, len = loCalData.length; i < len; i++) {
			loCalData[i].key = i;
			checkedArray.push(false);
		}
		this.setState({
			tableData: loCalData,
			checkedArray,
			accountingbook,
			showStar
		});
	}
	componentDidMount() {
		this.props.SelfDocTableRef(this);
	}

	getCheckedData = () => {
		let { checkedData, checkedArray, tableData } = this.state;
		checkedData = [];
		for (var i = 0; i < checkedArray.length; i++) {
			if (checkedArray[i] === true) {
				checkedData.push(tableData[i].m_pk_transproject);
			}
		}
		this.setState({
			checkedData
		});
		return checkedData;
	};
	resetCheckedData = (tableData) => {
		let checkedArray = [];
		for (let i = 0, len = tableData.length; i < len; i++) {
			tableData[i].key = i;
			checkedArray.push(false);
		}
		this.setState({
			checkedArray,
			checkedData: []
		});
	}
	//全选
	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		}, ()=>{self.props.onRowSelected()});
	};
	//单选
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		
		self.setState({
			checkedAll: allFlag,
			checkedArray: checkedArray
		}, ()=>{
			self.props.onRowSelected()
		});
		// });
	};
	renderColumnsMultiSelect(columns) {
		const { checkedArray } = this.state;
		const { multiSelect } = this.props;
		let indeterminate_bool = false;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
						<div fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={this.state.checkedAll}
								indeterminate={indeterminate_bool && !this.state.checkedAll}
								onChange={this.onAllCheckChange}
							/>
						</div>	
					),
					key: 'checkbox',
					attrcode: 'checkbox',
                    dataIndex: 'checkbox',
					width: '60px',
					render: (text, record, index) => {
						return (
							<div fieldid='firstcol'>
								<Checkbox
									className="table-checkbox"
									checked={this.state.checkedArray[index]}
									onChange={this.onCheckboxChange.bind(this, text, record, index)}
								/>
							</div>	
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}


	render() {
		let { tableData, showStar } = this.state;
		let columnsldad = this.getTableColumns(tableData, showStar)
		let columns = this.renderColumnsMultiSelect(columnsldad);
		return (
			<Table
				columns={columns}
				data={tableData}
				bodyStyle={{height: getTableHeight(80)}}
				scroll={{ x: false, y: getTableHeight(80) }}
			/>
		);
	}
}
SelfDocTable.defaultProps = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
