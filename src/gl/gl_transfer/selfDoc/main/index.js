import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, promptBox, getMultiLang, createPageIcon } from 'nc-lightapp-front';
const { NCDiv } = base
import createScript from '../../../public/components/uapRefer.js';
import { onButtonClick, initTemplate } from '../events';
import SelfDocTable from './table';
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
class DocMain extends Component {
	constructor(props) {
		super(props);
		this.state = {
			btnState:{add:true, update:true, delete:true},
			mainData: [],
			accountingbook: {
				refname: '',
				refpk: ''
			},
			showStar: false,
			json: {}
		};
	}

	//获取默认账簿
	queryAppcontext = () => {
		let url = '/nccloud/platform/appregister/queryappcontext.do';
		let data = {
			appcode: this.props.getSearchParam('c')
		};
		let self = this;
		ajax({
			url,
			data,
			success: function(response) {
				const { data, error, success } = response;
				if (success && data) {
					let accountingbook = {
						refname: data.defaultAccbookName,
						refpk: data.defaultAccbookPk
					};
					self.setState({
						accountingbook
					});
					if (accountingbook.refpk) {
						self.getInitData(accountingbook);
					}else{
						self.props.button.setButtonDisabled(['add','update','delete'], true);
					}
				} else {
				}
			}
		});
	}
	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			},()=>{		  
				initTemplate.call(this, this.props);
				//获取默认账簿
				this.queryAppcontext()
			})
		}
		getMultiLang({moduleId:'20020SDTPDPAGE',domainName:'gl',currentLocale:'simpchn',callback});
		this.props.button.setButtonVisible([ 'save', 'cancel' ], false)
	}
	componentDidMount() {}
	handleAdd = () => {
		//新增
		let { accountingbook, mainData } = this.state;
		let item = {
			m_pk_transproject: '', //档案主键
			m_transprojectname: '', // 档案名称
			m_transprojectcode: '', // 档案编码
			m_memo: '', // 档案说明
			pk_accountingbook: accountingbook.refpk,
			dr: '',
			status: '',
			m_isDirty: ''
		};
		mainData.push(item);
		this.setState({
			mainData,
			showStar: true
		});
		this.props.button.setButtonVisible([ 'update', 'refresh' ], false)
		this.props.button.setButtonVisible([ 'add', 'delete', 'save', 'cancel' ], true)
		this.props.button.setMainButton(['add'],false)
		
	};
	sendMainData = (data) => {
		//接收数据
		this.setState({
			mainData: data
		});
	};
	//刷新
	reFresh = () => {
		this.getInitData('',()=>{
			toast({ title: this.state.json['20020SDTPDPAGE-000012'], color: 'success' });/* 国际化处理： 刷新成功*/
		})
	}
	// 修改
	handleRepair = () => {
		let {mainData} = this.state
		for (let i = 0, len = mainData.length; i < len; i++) {
			mainData[i].isEdit = false;
		}
		this.setState({
			mainData,
			showStar: true
		})
		// this.getInitData('',false)
		this.props.button.setButtonVisible([ 'update', 'refresh' ], false)
		this.props.button.setButtonVisible([ 'add', 'delete', 'save', 'cancel' ], true)
		this.props.button.setMainButton(['add'],false)
	}
	handleSave = () => {
		//保存
		let { accountingbook, mainData } = this.state;
		let url = '/nccloud/gl/voucher/saveTransProject.do';
		let data = mainData;
		let m_transprojectcodes = data.map(item => item.m_transprojectcode)
		let m_transprojectnames = data.map(item => item.m_transprojectname)
		data.map((item,index)=>{
			let{m_transprojectcode,m_transprojectname} = item
			if(!m_transprojectcode && !m_transprojectname){
				toast({ content: `${this.state.json['20020SDTPDPAGE-000002']}${index+1}${this.state.json['20020SDTPDPAGE-000003']}`, color: 'warning'});/* 国际化处理： 第,行编号和名称不能为空*/
				return false
			}else{
				if(!m_transprojectcode){
					toast({ content: `${this.state.json['20020SDTPDPAGE-000002']}${index+1}${this.state.json['20020SDTPDPAGE-000004']}`, color: 'warning'});/* 国际化处理： 第,行编号不能为空*/
					return false
				}
				if(!m_transprojectname){
					toast({ content: `${this.state.json['20020SDTPDPAGE-000002']}${index+1}${this.state.json['20020SDTPDPAGE-000005']}`, color: 'warning'});/* 国际化处理： 第,行名称不能为空*/
					return false
				}
			}
		})
		if(!m_transprojectcodes.includes("") && !m_transprojectnames.includes("")){
			let self = this;
			ajax({
				url,
				data,
				success: function(response) {
					const { data, error, success } = response;
					if (success) {
						toast({ content: self.state.json['20020SDTPDPAGE-000000'], color: 'success' });/* 国际化处理： 保存成功*/
						self.getInitData();
						self.props.button.setButtonVisible([ 'save', 'cancel' ], false)
						self.props.button.setButtonVisible([ 'add', 'update', 'delete', 'refresh' ], true)
					} else {
					}
				}
			});
		}	
	};
	myajax = (url, data, callback) =>{
		ajax({
			url, data,
			success: (res) => {
				if(callback){
					callback(res);
				}
			}
		});
	}
	
	del = (data, callback) => {
		let url = '/nccloud/gl/voucher/delTransProject.do';
		promptBox({
			color : 'warning',
			title: this.state.json['20020SDTPDPAGE-000010'], /* 国际化处理： 删除*/
			content : this.state.json['20020SDTPDPAGE-000011'],/* 国际化处理： 确定要删除所选数据吗？*/
			beSureBtnClick : () => {
				this.myajax(url, data, callback);
			}
		});
	}
	
	handleDel = () => {
		//删除
		let { accountingbook, mainData } = this.state;
		let localArr = [];
		let checkedData = this.SelfDocTable.getCheckedData();		
		let data = {
			pk_accountingbook: accountingbook.refpk,
			pk_transprojs: checkedData //方案主键数组
		};
		let self = this;
		self.del(data, (res) => {
			if (res.success) {
				toast({ content: self.state.json['20020SDTPDPAGE-000001'], color: 'success' });/* 国际化处理： 删除成功*/
				for (let i = mainData.length - 1; i >= 0; i--) {
					if(checkedData.includes(mainData[i].m_pk_transproject)){
						mainData.splice(i, 1);
						continue;
					}
				}
				self.setState({
					mainData
				},()=>{
					self.SelfDocTable.resetCheckedData(self.state.mainData);
				});
			}
		});
	};
	handleCancel = () => {
		//取消
		this.getInitData();
		this.props.button.setButtonVisible([ 'save', 'cancel' ], false)
		this.props.button.setButtonVisible([ 'add', 'update', 'delete', 'refresh' ], true)
		this.props.button.setMainButton(['add'],true)
	};
	
	getInitData = (pk,callback) => {
		let url = '/nccloud/gl/voucher/queryTransProject.do';
		let accountingbook = this.state.accountingbook;
		let pk_accountingbook = pk&&pk.refpk || accountingbook&&accountingbook.refpk;
		if(!pk_accountingbook) return;
		let data = {
			pk_accountingbook: pk ? pk.refpk : accountingbook.refpk
		};
		let { mainData } = this.state;
		let self = this;
		ajax({
			url,
			data,
			success: function(response) {
				const { data, error, success } = response;
				if (success) {
					if(data){
						for (let i = 0, len = data.length; i < len; i++) {
							data[i].isEdit = true;
						}
						self.setState({
							mainData: data,
							showStar: false
						}, () => self.updateBtnState());
					}	
					if(callback){
						callback()
					}			
				} else {
				}
			}
		});
	};

	onBookChange = (v) =>{
		let page = this;
		if(v && v.refpk){
			page.setState({accountingbook:v}, () => {this.getInitData(v)});
		}else{
			page.setState({accountingbook:v, mainData:[]}, () => {page.updateBtnState()});
		}
		//清空表体，修改按钮状态
	}

	/**
	 * 首次加载页面或切换核算账簿时调用
	 */
	updateBtnState = () => {
		let page = this;
		let {accountingbook, mainData} = page.state;
		let btnState = {add:true, update:true, delete:true};
		if(accountingbook && accountingbook.refpk){
			btnState.add = false;
		}
		if(mainData && mainData.length > 0){
			btnState.update = false;
		}
		let checkedData = this.SelfDocTable.getCheckedData();
		if(checkedData && checkedData.length > 0){
			btnState.delete = false;
		}
		page.setState({btnState});
		page.props.button.setButtonDisabled(btnState);
	}

	render() {
		let { mainData, accountingbook, showStar } = this.state;
		let mybook;
		let page = this;
		let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
		if (!this.state['myattrcode']) {
			{
				createScript.call(this, referUrl, 'myattrcode');
			}
		} else {
			mybook = this.state['myattrcode'] ? (
				<div className="main-org">
					{this.state['myattrcode']({
						value: { refname: this.state.accountingbook.refname, refpk: this.state.accountingbook.refpk },
						isMultiSelectedEnabled: false,
						fieldid: 'accountingbook',
						showGroup: false,
						showInCludeChildren: false,
						disabledDataShow: true,
						queryCondition: {
							appcode: this.props.getSearchParam('c'),
							TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder'
						},
						onChange: (v) => {
							page.onBookChange(v);
						}
					})}
				</div>
			) : null;
		}
		return (
			<div className="nc-single-table selfDoc">
				<HeaderArea 
					title = {this.props.getSearchParam('n')}
					searchContent = {mybook}
					btnContent = {this.props.button.createButtonApp({
						area: 'page_header',
						onButtonClick: onButtonClick.bind(this),
						// popContainer: document.querySelector('.header-button-area')
					})}
				/>
				<NCDiv fieldid="selfdoc" areaCode={NCDiv.config.TableCom}>
					<div className="nc-singleTable-table-area">
						<SelfDocTable
							mainData={mainData}
							showStar={showStar}
							accountingbook={accountingbook}
							sendMainData={this.sendMainData.bind(this)}
							SelfDocTableRef={(init) => {this.SelfDocTable = init}}
							onRowSelected = {()=>this.updateBtnState()}
						/>
					</div>
				</NCDiv>
			</div>
		);
	}
}
DocMain = createPage({})(DocMain);
ReactDOM.render(<DocMain />, document.querySelector('#app'));
