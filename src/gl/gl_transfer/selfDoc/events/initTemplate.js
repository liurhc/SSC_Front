import { ajax } from 'nc-lightapp-front';
export default function (props) {  
    let pageCode =props.getSearchParam('p');
    let appcode =props.getSearchParam('c');
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do', 
		data: {
			pagecode: pageCode,
			appcode: appcode//小应用id
		},
		async:false,
		success: function(res) {
			if (res.data) {
				let button = res.data;
                props.button.setButtons(button);
			}
		}
	});
}



