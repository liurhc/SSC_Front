import React, { Component } from 'react';
import {high,ajax,base,getMultiLang } from 'nc-lightapp-front';
const { NCButton: Button, NCTable:Table, NCModal:Modal, NCDiv
    } = base;
const { Refer } = high;
export default class TransResultModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            pk_accountingbook:{
                display:'',
                value:''
            },
            tabelData:[],
            json: {}
        };
        this.columns = [
            
          ];
    }
    handleCancel(){
        let {handleCancel} = this.props
        if(handleCancel){
            handleCancel()
        }
    }
    componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'20020SDTREPAGE',domainName:'gl',currentLocale:'simpchn',callback});
    }
    
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.resultModalShow;
        let tabelData = nextProps.transResult;
        this.setState({
            showModal,tabelData
        })
    }
    
    render () {
        let {pk_accountingbook,saveData}=this.state;
        let columns=this.columns;
        let {transResult}= this.props
        let batResult = transResult.batResult
        if(batResult && batResult.column){
            let col = batResult.column
            col.map((item,index)=>{
                item.width = 240
                if(item.key!=="0"){
                    item.render = (text, record, index) => {
                        /* 国际化处理： 生成凭证*/
                        if (text!==this.state.json['20020SDTREPAGE-000032']) {			
                            return <span style={{color: 'red'}}>{text}</span>
                        } else {
                            return <span>{text}</span>
                        }
                    }
                }               
            })
        }
        return (
        <div>
            <Modal
                show = { this.props.visible }
                fieldid = 'transresult'
                onHide = { this.handleCancel.bind(this) }
                size={'lg'}
            >
                <Modal.Header closeButton>
                    <Modal.Title>{this.state.json['20020SDTREPAGE-000030']}</Modal.Title>{/* 国际化处理： 批量结转结果报告*/}
                </Modal.Header>
                <Modal.Body style={{ borderTop: '1px solid #ccc', borderBottom: '1px solid #ccc' }}>
                    <NCDiv fieldid="transresult" areaCode={NCDiv.config.TableCom}>
                        <Table className='resultTable'
                            scroll={{ x: true }}
                            columns={(batResult&&batResult.column)?batResult.column:''}
                            data={(batResult&&batResult.rows)?batResult.rows:''}
                            onRowClick={this.clickRow}
                        />
                    </NCDiv>
                </Modal.Body>

                <Modal.Footer>
                    {/* <Button onClick={ this.handleConfirm } colors="primary">确认</Button> */}

                    <Button fieldid='confirm' onClick={ this.handleCancel.bind(this) } shape="border" >{this.state.json['20020SDTREPAGE-000030']}</Button>{/* 国际化处理： 关闭*/}
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
