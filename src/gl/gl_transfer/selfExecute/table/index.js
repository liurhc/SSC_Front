import React, { Component } from 'react';
import { base, createPage, getMultiLang } from 'nc-lightapp-front';
const { NCTable: Table, NCCheckbox: Checkbox, NCDiv } = base;
import './index.less';
import cacheUtils from '../../trransferHistory/utils/cacheUtils.js';
const emptyCell = <span>&nbsp;</span>
class SelfDocTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: [], //表格数据
			checkedAll: false, //是否全选
			checkedData: [], //选中数据
			checkedArray: [], //各行选中判断
			configs: {}, //人员参照
			json: {},
			transPorj: ''
		};
	}

	getCheckedData = () => {
		let { checkedData, checkedArray, tableData } = this.state;
		checkedData = [];
		for (var i = 0; i < checkedArray.length; i++) {
			if (checkedArray[i] === true) {
				checkedData.push(tableData[i]);
			}
		}
		this.setState({
			checkedData
		});
		return checkedData;
	};
	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'20020SDTREPAGE',domainName:'gl',currentLocale:'simpchn',callback});
	}
	componentDidMount() {
		this.props.SelfDocTableRef(this);
	}
	componentWillReceiveProps(nextProps) {
		let loCalData = nextProps.mainData;
		let transPorj = nextProps.transPorj;
		let checkedArray;
		if (this.state.checkedArray.length !== loCalData.length) {
			checkedArray = [];
			for (let i = 0, len = loCalData.length; i < len; i++) {
				loCalData[i].key = i;
				checkedArray.push(false);
			}
			this.setState({
				checkedArray
			});
		}
		this.setState({
			tableData: loCalData,
			transPorj: transPorj
		});
	}
	handleChangeType = (v) => {
	};
	
	//全选
	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		},()=>{
			let tempSearchData = cacheUtils.getSearchData()
			let {checkedArray} = self.state
			if(checkedArray.includes(true)){
				if(tempSearchData.selectedTrans.key){
					self.props.updateButtonStatus(false, true)
				}else{
					self.props.updateButtonStatus(false, false)
				}
			}else{
				self.props.updateButtonStatus(true, false)
			}
		});
	};
	//单选
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		
		self.setState({
			checkedAll: allFlag,
			checkedArray: checkedArray
		},()=>{
			let tempSearchData = cacheUtils.getSearchData()
			let {checkedArray} = self.state
			if(checkedArray.includes(true)){
				if(tempSearchData.selectedTrans.key){
					self.props.updateButtonStatus(false, true)
				}else{
					self.props.updateButtonStatus(false, false)
				}
			}else{
				self.props.updateButtonStatus(true,false)
			}
		});
	};
	renderColumnsMultiSelect(columns) {
		const { checkedArray } = this.state;
		const { multiSelect } = this.props;
		let indeterminate_bool = false;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
						<div fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={this.state.checkedAll}
								indeterminate={indeterminate_bool && !this.state.checkedAll}
								onChange={this.onAllCheckChange}
							/>
						</div>
					),
					key: 'checkbox',
					attrcode: 'checkbox',
                    dataIndex: 'checkbox',
					width: '60px',
					render: (text, record, index) => {
						return (
							<div fieldid='firstcol'>
								<Checkbox
									className="table-checkbox"
									checked={this.state.checkedArray[index]}
									onChange={this.onCheckboxChange.bind(this, text, record, index)}
								/>
							</div>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}
	getTableColumns = () => {
		let { transPorj, tableData, json } = this.state
		let columns = [
			{
				title: (<div fieldid='index'>{json['20020SDTREPAGE-000021']}</div>),/* 国际化处理： 序号*/
				dataIndex: 'index',
				key: 'index',
				width: '80px',
				render: (text, record, index) => <div fieldid='index'>{index+1}</div>
			},
			{
				title: (<div fieldid='transferno'>{json['20020SDTREPAGE-000022']}</div>),/* 国际化处理： 编号*/
				dataIndex: 'transferno',
				key: 'transferno',
				width: '100px',
				render: (text, record, index) => <div fieldid='transferno'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='note'>{json['20020SDTREPAGE-000023']}</div>),/* 国际化处理： 名称*/
				dataIndex: 'note',
				key: 'note',
				width: '100px',
				render: (text, record, index) => <div fieldid='note'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='vouchertype'>{json['20020SDTREPAGE-000024']}</div>),/* 国际化处理： 凭证类别*/
				dataIndex: 'vouchertype',
				key: 'vouchertype',
				width: '100px',
				render: (text, record, index) => <div fieldid='vouchertype'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='billnum'>{json['20020SDTREPAGE-000025']}</div>),/* 国际化处理： 附单据*/
				dataIndex: 'billnum',
				key: 'billnum',
				width: '100px',
				render: (text, record, index) => <div fieldid='billnum'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='operator'>{json['20020SDTREPAGE-000026']}</div>),/* 国际化处理： 定义人*/
				dataIndex: 'operator',
				key: 'operator',
				width: '100px',
				render: (text, record, index) => <div fieldid='operator'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='system'>{json['20020SDTREPAGE-000027']}</div>),/* 国际化处理： 转账属性*/
				dataIndex: 'system',
				key: 'system',
				width: '100px',
				render: (text, record, index) => <div fieldid='system'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='exectimes'>{json['20020SDTREPAGE-000028']}</div>),/* 国际化处理： 已执行次数*/
				dataIndex: 'exectimes',
				key: 'exectimes',
				width: '100px',
				render: (text, record, index) => <div fieldid='exectimes'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='lastexec'>{json['20020SDTREPAGE-000029']}</div>),/* 国际化处理： 上次执行时间*/
				dataIndex: 'lastexec',
				key: 'lastexec',
				width: '100px',
				render: (text, record, index) => <div fieldid='lastexec'>{text ? text : emptyCell}</div>
			}
		];
		let exenoColumn = {
			title: (<div fieldid='exeno'>{json['20020SDTREPAGE-000033']}</div>),/* 国际化处理： 结转序号*/
			dataIndex: 'exeno',
			key: 'exeno',
			width: '160px',
			render: (text, record, index) => <div fieldid='exeno'>{text ? text : emptyCell}</div>
		}
		if(transPorj){
			columns.splice(1, 0, exenoColumn)
		}
		return columns
	}
	render() {
		let { tableData } = this.state;
		let columns = this.getTableColumns()
		let columnsldad = this.renderColumnsMultiSelect(columns);
		let height = window.innerHeight - 130
		return(
			<NCDiv className='table-area' fieldid="selfexecute" areaCode={NCDiv.config.TableCom}>
				<Table 
					columns={columnsldad} 
					data={tableData} 
					// scroll={{ y: height }} 
					adaptionHeight={true}
				/>
			</NCDiv>
		);
	}
}

SelfDocTable = createPage({})(SelfDocTable);
export default SelfDocTable;

SelfDocTable.defaultProps = {
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
