import {asyncComponent} from 'nc-lightapp-front';
import Gltransfer from './SyncTreeDom';

import Voucher from 'gl/voucher_card';
import VoucherList from 'gl/voucher_list';


const TransferHistory = asyncComponent(() => import(/* webpackChunkName: "gl/gl_transfer/trransferHistory/main/index" */ /* webpackMode: "eager" */ '../../trransferHistory/main'));

const routes = [
  {
    path: '/',
    component: Gltransfer,
    exact: true,
  },
  {
    path: '/list',
    component: VoucherList,
  },
  {
    path: '/Welcome',
    component: Voucher,
  },
  {
    path: '/trransferHistory',
    component: TransferHistory,
  }
];

export default routes;
