import { ajax } from 'nc-lightapp-front';
export default function (props) {
    // let meta = {};
    // let that = this;
    let pageCode = props.getUrlParam('backPageCode') || props.getSearchParam('p');
    let appcode = props.getUrlParam('backAppCode') || props.getSearchParam('c');//page 从凭证页面传过来
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do', 
		data: {
			pagecode: pageCode,
			appcode: appcode//小应用id
		},
		async:false,
		success: function(res) {
			if (res.data) {
				let button = res.data;
                props.button.setButtons(button);
			}
		}
	});

    // meta = {
    //     // 树弹出框内容模板
    //     tree1: {
    //         moduletype: 'form',
    //         status:'edit',
    //         items: [
    //             {
    //                 visible:true,
    //                 label: '节点名称',
    //                 attrcode: 'name',
    //                 itemtype: 'input',
    //                 col: 12,
    //                 initialvalue: {
    //                     value: '',
    //                     display: null
    //                 }
    //             },
    //             {
    //                 visible:true,
    //                 label: '编码',
    //                 attrcode: 'code',
    //                 itemtype: 'input',
    //                 col: 12,
    //                 initialvalue: {
    //                     value: '',
    //                     display: null
    //                 }
    //             },
    //         ]
    //     },
    // };

    // props.meta.setMeta(meta)

}
