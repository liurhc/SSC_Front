import apis from '../../actions';

export default function onButtonClick(props, id) {
    switch (id) {
        case 'group1'://
            break;
        case 'gen_voucher'://生成凭证
            // this.reduceVoucher()
            // this.setState({showUploadModal:true});
            // this.props.modal.show('upload');
            genVoucher(this);
            break;
        case 'bath_gen'://批量结转
            this.showBatchTransferModal()
            break;  
        case 'history_data'://历史数据
            this.goTransferHistory()
            break; 
        case 'link2voucher'://联查凭证
            this.goVoucher()
            break; 
        case 'query'://查询
            this.handleSearch()
            break; 
        case 'clear_table'://清空列表区
            this.clearTableData()
            break; 
        default:
        break;

    }
}

function genVoucher(page){
    
    let {isProject} = page.state;
    if(isProject){
        page.reduceVoucher();
        return;
    }
    
    let pk_transfers = page.getCheckedTransferPks();
    apis.containsSelfDefFuncs({pk_transfers}, (data) => {
        if(data == true){
            openUploadModalForGenVoucher(page);
        }else{
            page.reduceVoucher();
        }
    });    
}

function openUploadModalForGenVoucher(page){
    let uploadActionInfo = page.uploadActionInfo;
    uploadActionInfo.action = '/nccloud/gl/voucher/execTransBackground.do';
    uploadActionInfo.data = page.conformData();
    uploadActionInfo.handleSuccess = page.handleGenVoucher;
    uploadActionInfo.handleError = page.handleGenVoucherError;
    page.setState({showUploadModal:true});
}