import React, { Component } from 'react';
import { base, toast, getMultiLang } from 'nc-lightapp-front';
import ReferLoader from '../../../../public/ReferLoader/index.js';
import { accbookRefcode } from '../../../../public/ReferLoader/constants';
const { NCModal, NCButton } = base;
import './index.less'

export default class BatchTransferModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			accountbooks: [],
			show: false,
			json: {}
		};
	}
	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'20020SDTREPAGE',domainName:'gl',currentLocale:'simpchn',callback});
	}

	componentWillReceiveProps(nextProps) {
		let { show, defaultValue } = nextProps;
		let accountbooks = defaultValue;
		this.setState({ show, accountbooks });
	}

	handleConfirm = () => {
		let { accountbooks } = this.state;
		if(accountbooks.length===0){
			toast({ content: this.state.json['20020SDTREPAGE-000000'], color: 'warning'});/* 国际化处理： 请选择核算账簿！*/
		} else{
			this.props.handleBatchTransferOK(accountbooks);
		}
	};

	handleClose = () => {
		this.setState({ show: false });
		this.props.onClose();
	};

	render() {
		let { accountbooks, show } = this.state;
		let { context } = this.props;
		return (
			<NCModal fieldid='batchtransfer' className="simpleModal junior batchTransferModal" show={show} onHide={this.handleClose}>
				<NCModal.Header closeButton>
					<NCModal.Title>{this.state.json['20020SDTREPAGE-000002']}</NCModal.Title>{/* 国际化处理： 批量结转*/}
				</NCModal.Header>
				<NCModal.Body>
					<div className='batchTransferModal-body' fieldid='batchExec'>
						<i className='require-true'>*</i>
						<div className='item-title'>{this.state.json['20020SDTREPAGE-000001']}:</div>
						{/* 国际化处理： 请选择核算账簿*/}
						<div className='item-refer'>
							<ReferLoader
								tag="batchExec"
								fieldid = 'accountbooks'
								placeholder={this.state.json['20020SDTREPAGE-000001']}/* 国际化处理： 请选择核算账簿*/
								refcode={accbookRefcode}
								value={accountbooks}
								isMultiSelectedEnabled={true}
								showGroup={false}
								showInCludeChildren={true}
								isHasDisabledData = {false}
								queryCondition={() => {
									let condition = {
										TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
										appcode: '',
										filterSecondBU:true
									};
									condition.appcode = context.appcode;
									return condition;
								}}
								onChange={(v) => {
									accountbooks = v;
									this.setState({ accountbooks });
								}}
							/>
						</div>
					</div>
				</NCModal.Body>
				<NCModal.Footer>
					<NCButton fieldid='confirm' className="button-primary" onClick={this.handleConfirm}>
						{this.state.json['20020SDTREPAGE-000003']}{/* 国际化处理： 确定*/}
					</NCButton>
					<NCButton fieldid='cancel' onClick={this.handleClose}>{this.state.json['20020SDTREPAGE-000004']}</NCButton>{/* 国际化处理： 取消*/}
				</NCModal.Footer>
			</NCModal>
		);
	}
}
