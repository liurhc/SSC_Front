import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { base, getMultiLang } from 'nc-lightapp-front';
const { NCDatePicker: DatePicker, NCModal: Modal, NCForm, NCButton: Button } = base;
const { NCFormItem: FormItem } = NCForm;
import './index.less'

const formItemLayout = { labelXs:3, labelSm:3, labelMd:3, xs:9, md:9, sm:9 };
const formItemParam = { inline:true, showMast:false, isRequire:true, method:"change" };
export default class DatePickerModal extends Component {
    constructor(props) {
        super(props);       
        this.state = {
            date: '',
            json: {}
        };
    }
    handleDateOK(){
        let { handleDateOK } = this.props
        let { date } = this.state
        if (handleDateOK) {
            handleDateOK(date)
        }
    }
    handleCancel() {
        let { handleCancel } = this.props
        if (handleCancel) {
            handleCancel()
        }
    }
    componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'20020SDTREPAGE',domainName:'gl',currentLocale:'simpchn',callback});
	}

    shouldComponentUpdate(nextprops){
        let period = nextprops.period
        if(!period || !period.value || !period.value.enddate) 
            return false;
        return true;
    }


    componentWillReceiveProps(nextprops){
        let period = nextprops.period
        if(!period || !period.value || !period.value.enddate) return;
        let enddate = period.value?period.value.enddate.value:''
        this.setState({
            date: enddate
        })
    }
    componentDidMount(){
        let {period} = this.props
      //  let enddate = period.value?period.value.enddate.value:''
        // this.setState({
        //     date: enddate
        // })
    }
    renderDatePicker=(enddate)=>{
        let {date} = this.state
        let dateInputPlaceholder = this.state.json['20020SDTREPAGE-000005'];/* 国际化处理： 请选择日期*/
        return(
            <FormItem
                {...formItemLayout}
                {...formItemParam}
                labelName={this.state.json['20020SDTREPAGE-000006']}/* 国际化处理： 制单日期*/

            >
                <DatePicker
                    fieldid = 'voucher'
                    name='date'
                    type="customer"
                    isRequire={true}
                    value={date}
                    onChange={(v) => {
                        date = v;
                        this.setState({
                            date
                        })
                    }}
                    placeholder={dateInputPlaceholder}
                />
            </FormItem>
        )
    }
    render() {
        let { visible, period } = this.props
        let begindate = period.value?period.value.begindate.value:''
        let enddate = period.value?period.value.enddate.value:''
        return(
            <div className='datePickerModal'>
                <Modal fieldid='datepicker' style={{ width: '40%' }} show={visible} onHide={this.handleCancel.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.json['20020SDTREPAGE-000007']}</Modal.Title>{/* 国际化处理： 日期*/}
                    </Modal.Header >
                    <Modal.Body style={{ borderTop: '1px solid #ccc', borderBottom: '1px solid #ccc' }}>
                        <NCForm className='datePickerform'
                            useRow={true}
                            submitAreaClassName='classArea'
                            showSubmit={false}　>
                            {this.renderDatePicker(enddate)}
                        </NCForm>
                        <div className='mark' fieldid='mark'>
                            {this.state.json['20020SDTREPAGE-000008']}：{this.state.json['20020SDTREPAGE-000009']}，{this.state.json['20020SDTREPAGE-000010']}，{this.state.json['20020SDTREPAGE-000011']}{begindate}--{enddate}
                            {/* 国际化处理： 提示,如果你确认将选中的定义生成凭证,请选择凭证日期,范围为*/}
                        </div>
                    </Modal.Body>
                    <Modal.Footer >
                        <Button size="sm" fieldid='confirm' onClick={this.handleDateOK.bind(this)}>{this.state.json['20020SDTREPAGE-000003']}</Button>{/* 国际化处理： 确定*/}
                        <Button fieldid='cancel' onClick={this.handleCancel.bind(this)}>{this.state.json['20020SDTREPAGE-000004']}</Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div >
        )
    }
}
