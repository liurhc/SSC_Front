import {toast} from 'nc-lightapp-front';
import myAjax from '../../../fipub/public/common/ajax';

function containsSelfDefFuncs(data, callback){
    let url = "/nccloud/gl/voucher/containsSelfDefFunc.do";
    myAjax({url, data, successcb:callback});
}

const apis = {
    containsSelfDefFuncs:containsSelfDefFuncs
}

export default apis;