import CacheUtils from '../../../public/common/CacheUtil.js'

const dataSource = 'gl.gl_transfer.selfExecute.'
const transDefKey = 'transdef';

class TransExecCacheUtil extends CacheUtils{
    constructor(ViewModel){
        super(ViewModel, dataSource);
    }

    setAllTransDef(data){
        super.setData(transDefKey, data);
    }

    getAllTransDef(){
        return super.getData(transDefKey);
    }
}

export default TransExecCacheUtil;
