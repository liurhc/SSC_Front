import { ajax, base, toast,cacheTools,print,withNav } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
let tableid = 'gl_brsetting';
// @withNav
export default function onButtonClick(props, id) {
    switch (id) {
        case 'group1'://
            break;
        case 'update'://修改
            this.handleRepair()
            break;
        case 'delete'://删除
            this.handleDel()
            break;  
        case 'select_transdef'://选择转账定义
            this.handChoose()
            break; 
        // case 'update_no'://调整结转序号
            // this.handChangeNum()
            // break; 
        case 'save'://保存
            this.handleSave()
            break; 
        case 'cancel'://取消
            this.handleCancel()
            break; 
        default:
        break;

    }
}
