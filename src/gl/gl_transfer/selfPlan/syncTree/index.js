import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { base, createPage, ajax, getMultiLang } from 'nc-lightapp-front';
const { NCDiv } = base;
import { initTemplate } from './events';
import './index.less';
import SelfDocTable from '../table';
import SelfExecute from '../search';
const treeRootId = 'selfPlan';

class SyncTree extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		let { syncTree } = this.props;
		let { setSyncTreeData } = syncTree;
		this.setSyncTreeData = setSyncTreeData; //设置树根节点数据方法
		let data = [];
		this.newTree = syncTree.createTreeData(data); //创建树 组件需要的数据结构
		(this.state = {
			pk_accountingbook: '',
			selectedValue: '', //树选中
			saveData: {},
			stateType: '1',
			tableData: [], //表格数据
			appcode: this.props.getSearchParam('c'),
			pagecode: this.props.getSearchParam('p'),
			json: {}
		}),
			(this.datapool = {
				treeData: [],
				tableData: []
			});
	}
	// componentWillMount() {
	// 	// this.getInitData()
	// 	let localurl = decodeURIComponent(window.location);
	// 	let type;
	// 	if (localurl.match(/type=(\S*)/)) {
	// 		type = localurl.match(/type=(\S*)/)[1];
	// 	}
	// }
	componentWillMount() {
		let localurl = decodeURIComponent(window.location);
		let type;
		if (localurl.match(/type=(\S*)/)) {
			type = localurl.match(/type=(\S*)/)[1];
		}
		let callback= (json) =>{
			this.setState({
				json:json,
			},()=>{		  
				initTemplate.call(this, this.props);
			})
		}
		getMultiLang({moduleId:'20020SDTPEPAGE',domainName:'gl',currentLocale:'simpchn',callback});
	}

	componentDidMount() {}
	//获取初始数据
	getInitData = (pk) => {
		let data = {
			pk_accountingbook: pk ? pk : '' //核算账簿主键，若为集团默认值为 sysinit
		};
		this.setState({
			pk_accountingbook: pk
		});
		let self = this;
		if (!pk) {
			self.datapool.treeData = [];
			self.datapool.tableData = [];
			self.refresh();
		}
		let url = '/nccloud/gl/voucher/queryTransProjTree.do';
		ajax({
			url,
			data,
			success: function(response) {
				let { data, error, success } = response;
				if (success) {
					if (!data) {
						data = [];
					}
					self.setState({
						mainData: data
					});
					self.datapool.treeData = self.dealTreeData(data);
					self.datapool.tableData = [];
					self.refresh();
				} else {
				}
			}
		});
	};

	onSelectEve(data, item, isChange) {
		let url = '/nccloud/gl/voucher/queryTransProjDef.do',
			sendData = { pk_accountingbook: this.state.pk_accountingbook, pk_transproj: data },
			self = this;
		this.setState({
			selectedValue: data
		});
		let tableData = [];
		let page = this;
		ajax({
			url,
			// sendData,
			data: sendData,
			success: function(response) {
				const { data, error, success } = response;
				if (success && data) {
					tableData = data;					
				}
				page.datapool.tableData = tableData;
				page.setState({ 
					tableData
				});
				// page.refresh();
			}
		});
	}

	getCheckedData() {
		let checkedData = this.SelfDocTable.getCheckedData();
		return checkedData;
	}
	getPK = (data) => {
		//获取选中账簿
		this.getInitData(data);
	};
	dealTreeData(data) {
		//去null
		let deleteDataChildrenProp = function(node) {
			if (!node.children || node.children.length == 0) {
				delete node.children;
			} else {
				node.isLeaf = false;
				node.children.forEach((e) => {
					deleteDataChildrenProp(e);
				});
			}
		};

		let joinCodeAndName = function(node){
			if(node){
				node.title = node.innercode + " " + node.refname;
				node.refname = node.innercode + " " + node.refname;
				if(node.children){
					node.children.map((item) => {
						joinCodeAndName(item);
						return item;
					});
				}
			}
		}
		if (data) {
			data.forEach((e) => {
				deleteDataChildrenProp(e);
				joinCodeAndName(e);
			});
		}
		return data;
	}
	getSelectDetails = (data) => {
		let { selectedValue } = this.state;
		let obj = {};
		let localArr = [];
		if (data.length > 0) {
			data.map((item, index) => {
				item.exeno = index + 1;
				item.pk_transproj = selectedValue;
				item.isEdit = true;
				localArr.push(JSON.parse(JSON.stringify(item)));
			});
		}
		this.setState({
			tableData: localArr
		});
	};
	getEditTableData = (data) => {
		this.setState({
			tableData: data
		});
	};

	refresh = () => {
		let page = this;
		let { tableData, treeData } = page.datapool;
		page.setSyncTreeData(treeRootId, treeData);
		page.setState({ tableData, treeData });
	};
	updateButtonStatus(disabled) {
		this.SelfExecute.updateButtonStatus(disabled)
	}
	render() {
		const { syncTree, DragWidthCom } = this.props;
		let { createSyncTree } = syncTree;
		// let tableColumns = this.getTableColumns()
		let { pk_accountingbook, tableData, selectedValue } = this.state;
		return (
			<div className='nc-bill-tree-table'>
				<SelfExecute
					getPK={this.getPK.bind(this)}
					getCheckedData={this.getCheckedData.bind(this)}
					selectedValue={selectedValue}
					onSelectEve={this.onSelectEve.bind(this)}
					pk_accountingbook={pk_accountingbook}
					getSelectDetails={this.getSelectDetails.bind(this)}
					tableData={tableData}
					getEditTableData={this.getEditTableData.bind(this)}
					onAccountingbookSelect={this.getInitData}
					pagecode={this.state.pagecode}
					appcode={this.state.appcode}
					SelfExecuteRef={(init) => {this.SelfExecute = init;}}
				/>
				<div className="tree-table">
					<DragWidthCom
						defLeftWid={'280px'}
						leftDom={
							<div className='tree-area'>
								{createSyncTree({
									treeId: treeRootId,
									showLine: true,
									showModal: false, //是否使用弹出式编辑
									needSearch: false, //是否需要查询框，默认为true,显示。false: 不显示
									needEdit: false,
									selectedForInit: true, //默认选中第一个子节点
									onSelectEve: this.onSelectEve.bind(this) //选择节点回调方法
								})}
							</div>
						}
						rightDom={
							<SelfDocTable
								mainData={tableData}
								// columns = {tableColumns}
								updateButtonStatus={this.updateButtonStatus.bind(this)}
								SelfDocTableRef={(init) => {
									this.SelfDocTable = init;
								}}
							/>
						}
					/>
				</div>
			</div>
		);
	}
}

SyncTree = createPage({
	// initTemplate: initTemplate
})(SyncTree);
ReactDOM.render(<SyncTree />, document.querySelector('#app'));
