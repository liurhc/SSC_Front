import React, { Component } from 'react';
import { base, getMultiLang } from 'nc-lightapp-front';
const { NCTable: Table, NCCheckbox: Checkbox, NCFormControl:FormControl, NCDiv } = base;
import './index.less';
const emptyCell = <span>&nbsp;</span>
export default class SelfDocTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: [], //表格数据
			checkedAll: false, //是否全选
			checkedData: [], //选中数据
			checkedArray: [
				//各行选中判断
			],
			configs: {}, //人员参照
			json: {}
		};
	}
	getTableColumns = () => {
		let columns = [
			{
				title: <div fieldid='exeno'><i style={{color: '#E14C46'}}>*</i>{this.state.json['20020SDTPEPAGE-000017']}</div>,/* 国际化处理： 结转序号*/
				dataIndex: 'exeno',
				key: 'exeno',
				width: '150px',
				render: (text, record, index) => {
					let isEdit = record.isEdit
					if(isEdit){
						return (
							<div fieldid='exeno'>
								<FormControl
									fieldid='exeno'
									showClose={true}
									value={record.exeno}
									maxlength={10}
									onChange={(v) => {
										var reg = /^\+?[1-9][0-9]*$/;　　//正整数
										
										if(!v || reg.test(v)){
											let tableData = this.state.tableData;
											tableData[index].exeno = v;
											this.setState({
												tableData
											});
										}
									}}
								/>
							</div>
						)
					} else {
						return (
							<div fieldid='exeno'>{text ? text : emptyCell}</div>
						)
					}
				}
			},
			{
				title: (<div fieldid='transferno'>{this.state.json['20020SDTPEPAGE-000003']}</div>),/* 国际化处理： 编号*/
				dataIndex: 'transferno',
				key: 'transferno',
				width: '150px',
				render: (text, record, index) => <div fieldid='transferno'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='note'>{this.state.json['20020SDTPEPAGE-000004']}</div>),/* 国际化处理： 转账说明*/
				dataIndex: 'note',
				key: 'note',
				width: '150px',
				render: (text, record, index) => <div fieldid='note'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='vouchertype'>{this.state.json['20020SDTPEPAGE-000005']}</div>),/* 国际化处理： 凭证类别*/
				dataIndex: 'vouchertype',
				key: 'vouchertype',
				width: '150px',
				render: (text, record, index) => <div fieldid='vouchertype'>{text ? text : emptyCell}</div>
			},
			{
				title: (<div fieldid='creater'>{this.state.json['20020SDTPEPAGE-000006']}</div>),/* 国际化处理： 定义人*/
				dataIndex: 'creater',
				key: 'creater',
				width: '150px',
				render: (text, record, index) => <div fieldid='creater'>{text ? text : emptyCell}</div>
			}
		];
		return columns
	}
	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'20020SDTPEPAGE',domainName:'gl',currentLocale:'simpchn',callback});
	}
	componentWillReceiveProps(nextProps) {
		let loCalData = nextProps.mainData;
		let checkedArray;
		if (this.state.checkedArray.length != loCalData.length) {
			checkedArray = [];
			for (let i = 0, len = loCalData.length; i < len; i++) {
				loCalData[i].key = i;
				checkedArray.push(false);
			}
			this.setState({
				checkedArray
			});
		}
		this.setState({
			tableData: loCalData
		});
	}
	componentDidMount() {
		this.props.SelfDocTableRef(this);
	}
	getCheckedData = () => {
		let { checkedData, checkedArray, tableData } = this.state;
		checkedData = [];
		for (var i = 0; i < checkedArray.length; i++) {
			if (checkedArray[i] === true) {
				checkedData.push(tableData[i]);
			}
		}
		this.setState({
			checkedData
		});
		return checkedData;
	};

	//全选
	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		}, ()=>{
			let {checkedArray} = self.state
			if(checkedArray.includes(true)){
				self.props.updateButtonStatus(false)
			}else{
				self.props.updateButtonStatus(true)
			}
		});
	};
	//单选
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		self.setState({
			checkedAll: allFlag,
			checkedArray: checkedArray
		}, ()=>{
			let {checkedArray} = self.state
			if(checkedArray.includes(true)){
				self.props.updateButtonStatus(false)
			}else{
				self.props.updateButtonStatus(true)
			}
		});
	};
	renderColumnsMultiSelect(columns) {
		const { checkedArray } = this.state;
		const { multiSelect } = this.props;
		let indeterminate_bool = false;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
						<div fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={this.state.checkedAll}
								indeterminate={indeterminate_bool && !this.state.checkedAll}
								onChange={this.onAllCheckChange}
							/>
						</div>
					),
					key: 'checkbox',
					attrcode: 'checkbox',
                    dataIndex: 'checkbox',
					width: '50px',
					render: (text, record, index) => {
						return (
							<div fieldid='firstcol'>
								<Checkbox
									className="table-checkbox"
									checked={this.state.checkedArray[index]}
									onChange={this.onCheckboxChange.bind(this, text, record, index)}
								/>
							</div>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}
	render() {
		let columns = this.getTableColumns()
		let columnsldad = this.renderColumnsMultiSelect(columns);
		let { tableData } = this.state;
		return (
			<NCDiv className="table-area" fieldid="selfplan" areaCode={NCDiv.config.TableCom}>
				<Table 
					columns={columnsldad} 
					data={tableData} 
					adaptionHeight={true}  //表格是否自适应高度，占满屏幕
				/>
			</NCDiv>
		);
	}
}
SelfDocTable.defaultProps = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
