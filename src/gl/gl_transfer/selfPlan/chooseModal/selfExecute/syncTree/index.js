import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax } from 'nc-lightapp-front';
import SelfDocModalTable from '../table';
const allTransDef = [];
class SyncTree extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		let { syncTree } = this.props;
		let { setSyncTreeData } = syncTree;
		this.setSyncTreeData = setSyncTreeData; //设置树根节点数据方法
		let data = [];
		this.state = {
			pk_accountingbook: '',
			selectedValue: '',
			mainData: {}, //主数据
			saveData: {},
			stateType: '1',
			configs: {}, //人员参照
			tableData: [], //表格数据
			orgTableData: []
		};
	}
	componentWillMount() {}
	componentWillReceiveProps(nextProps) {
		let pk_accountingbook = nextProps.pk_accountingbook;
		let orgTableData = nextProps.orgTableData;
		if (pk_accountingbook != this.state.pk_accountingbook) {
			this.getInitData(pk_accountingbook);
			this.setState({
				pk_accountingbook,
				orgTableData
			});
		}
	}
	//获取初始数据
	getInitData = (pk) => {
		let data = {
			pk_accountingbook: pk ? pk : '' //核算账簿主键，若为集团默认值为 sysinit
		};
		let self = this;
		let url = '/nccloud/gl/voucher/queryTransferType.do';
		ajax({
			url,
			data,
			success: function(response) {
				const { data, error, success } = response;
				if (success && data) {
					self.setState({
						mainData: data
					});
					let localData = self.dealTreeData(data.nodes.children);
					self.setSyncTreeData('tree1', localData);
					self.props.syncTree.openNodeByPk('tree1', localData[0].refpk)
					self.props.syncTree.setNodeSelected('tree1', localData[0].refpk)
					let selectInfo = self.props.syncTree.getSelectNode('tree1')
					setTimeout(() => {
						self.onSelectEve(null, selectInfo, null)
					}, 1000);
					
				} else {
				}
			}
		});
		if (pk) {
			this.loadAllTransDef(pk);
		}
	};

	loadAllTransDef = (pk_accountingbook) => {
		let page = this;
		let {appcode, pagecode} = page.props;
		allTransDef.length = 0;
		// let url = actions.queryTransForExec;
		let url = '/nccloud/gl/voucher/queryTransDef.do';
		let data = {
			pk_accountingbook: pk_accountingbook,
			pagecode: pagecode
		};
		ajax({
			url,
			data,
			success: function(response) {
				let { success, data, error } = response;
				if (success) {
					if (data && data.pk_transfer && data.pk_transfer.rows) {
						let rows = data.pk_transfer.rows;
						rows.map((item) => {
							allTransDef.push(item);
						});
					}
				} else {
					toast({ content: error.message, color: 'warning' });
				}
			}
		});
	};

	onSelectEve = (data, item, isChange) => {
		let page = this;
		const tableData = [];
		let tempData = allTransDef;
		const pkSortsMap = {};
		this.extractSortPkFromNode(item, pkSortsMap);
		if (tempData) {
			tempData.map((item) => {
				if (pkSortsMap[item.values.pk_transsort.value]) {
					tableData.push(item);
				}
			});
		}
		page.setState({ tableData });
	}

	extractSortPkFromNode(node, pkMap) {
		if (node) {
			pkMap[node.refpk] = node;
			if (node.children && node.children.length > 0) {
				node.children.map((item) => {
					this.extractSortPkFromNode(item, pkMap);
				});
			}
		}
	}

	getCheckedData() {
		let checkedData = this.SelfDocModalTable.getCheckedData();
		return checkedData;
	}
	componentDidMount() {
		this.props.SyncTreeRef(this);
	}
	getPK = (data) => {
		//获取选中账簿
		this.getInitData(data);
	};
	dealTreeData(data) {
		//去null
		let deleteDataChildrenProp = function(node) {
			if (!node.children || node.children.length == 0) {
				delete node.children;
			} else {
				node.isLeaf = false;
				node.children.forEach((e) => {
					deleteDataChildrenProp(e);
				});
			}
		};
		let joinCodeAndName = function(node){
			if(node){
				node.title = node.innercode + " " + node.refname;
				node.refname = node.innercode + " " + node.refname;
				if(node.children){
					node.children.map((item) => {
						joinCodeAndName(item);
						return item;
					});
				}
			}
		}
		if (data) {
			data.forEach((e) => {
				deleteDataChildrenProp(e);
				joinCodeAndName(e);
			});
		}
		return data;
	}

	render() {
		const { syncTree, DragWidthCom } = this.props;
		let { createSyncTree } = syncTree;
		let { tableData } = this.state;
		return (
			<div style={{ height: 380 }}>
				<DragWidthCom
					defLeftWid={'280px'}
					leftDom={createSyncTree({
						treeId: 'tree1',
						showLine: true,
						showModal: false, //是否使用弹出式编辑
						needSearch: false, //是否需要查询框，默认为true,显示。false: 不显示
						needEdit: false,
						onSelectEve: this.onSelectEve.bind(this) //选择节点回调方法
					})}
					rightDom={
						<SelfDocModalTable
							orgTableData={this.state.orgTableData}
							mainData={tableData}
							SelfDocModalTableRef={(init) => {
								this.SelfDocModalTable = init;
							}}
						/>
					}
				/>
			</div>
		);
	}
}

SyncTree = createPage({})(SyncTree);
export default SyncTree;
