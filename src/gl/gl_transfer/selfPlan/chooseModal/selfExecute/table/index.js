import React, { Component } from "react";
import { base, getMultiLang } from 'nc-lightapp-front';
const { NCTable:Table, NCCheckbox:Checkbox, NCDiv } = base;

export default class SelfDocModalTable extends Component {
  constructor(props){
    super(props)
    this.state={
      tableData:[],//表格数据
      checkedAll:false,//是否全选
      checkedData:[],//选中数据
      selIds:[],//选中行号
      checkedArray: [//各行选中判断
        
      ],
      configs: {},//人员参照
      selectArr:[],//选中数据
      json: {}
    }
  }
  componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'20020SDTPEPAGE',domainName:'gl',currentLocale:'simpchn',callback});
	}
  componentWillReceiveProps(nextProps){
    let loCalData = nextProps.mainData;
    let checkedArray;
    let loCalDataArr=[]
    let {orgTableData} = this.props
    checkedArray =[];
    var checkFlag;
    for(let i=0,len=loCalData.length;i<len;i++){
      let pk_transfer = loCalData[i].values.pk_transfer.value
      for(let j=0,length=orgTableData.length;j<length;j++){
        let orgpk = orgTableData[j].pk_transfer
        if(pk_transfer === orgpk){
          checkFlag = true
          break;
        }else{
          checkFlag = false
        }
      }
      loCalData[i].values.key=i;
      checkedArray.push(checkFlag)        
      loCalDataArr.push(loCalData[i].values)
    }
    this.setState({
      checkedArray
    })
    this.setState({
      tableData:loCalDataArr
    })
  }
  getCheckedData= () => {
    let { checkedData,checkedArray,tableData } =this.state;
      checkedData=[]
      for(var i = 0; i < checkedArray.length; i++){
        if(checkedArray[i]===true){
          let obj ={}
          obj.transferno = tableData[i].transferno.value
          obj.note = tableData[i].note.value
          obj.vouchertype = tableData[i].pk_vouchertype.display
          obj.creater = tableData[i].pk_operator.display
          obj.pk_transsort = tableData[i].pk_transsort.value
          obj.pk_accountingbook = tableData[i].pk_accountingbook.value
          obj.pk_transfer = tableData[i].pk_transfer.value
          checkedData.push(obj);
        }
      }
      this.setState({
        checkedData
      })
      return checkedData
  }
  componentDidMount(){
    this.props.SelfDocModalTableRef(this)
  }
  //全选
  onAllCheckChange = () => {
    let self = this;
    let checkedArray = [];
    let selIds = [];
    for (var i = 0; i < self.state.checkedArray.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
    }
    self.setState({
      checkedAll: !self.state.checkedAll,
      checkedArray: checkedArray,
    });
  };
  //单选
  onCheckboxChange = (text, record, index) => {
    let self = this;
    let allFlag = false;
    let checkedArray = self.state.checkedArray.concat();
    checkedArray[index] = !self.state.checkedArray[index];
    for (var i = 0; i < self.state.checkedArray.length; i++) {
      if (!checkedArray[i]) {
        allFlag = false;
        break;
      } else {
        allFlag = true;
      }
    }
    self.setState({
      checkedAll: allFlag,
      checkedArray: checkedArray
    });
  };
  renderColumnsMultiSelect(columns) {
    const {checkedArray } = this.state;
    const { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // let indeterminate_bool1 = true;
    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArray.length;
      while(i--){
          if(checkedArray[i]){
            indeterminate_bool = true;
            break;
          }
      }
      let defaultColumns = [
        {
          title: (
            <span fieldid='firstcol'>
              <Checkbox
                className="table-checkbox"
                checked={this.state.checkedAll}
                indeterminate={indeterminate_bool&&!this.state.checkedAll}
                onChange={this.onAllCheckChange}
              />
            </span>
          ),
          key: "checkbox",
          attrcode: 'checkbox',
          dataIndex: 'checkbox',
          width: '60px',
          render: (text, record, index) => {
            return (
              <span fieldid='firstcol'>
                <Checkbox
                  className="table-checkbox"
                  checked={this.state.checkedArray[index]}
                  onChange={this.onCheckboxChange.bind(this, text, record, index)}
                />
              </span>
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
  getTableColumns = () => {
    let columns = [
      {
        title: (<span fieldid='exeno'>{this.state.json['20020SDTPEPAGE-000003']}</span>),/* 国际化处理： 编号*/
        dataIndex: "exeno",
        key: "exeno",
        width:'160px',
        render: (text, record, index) => (
          <span fieldid='exeno'>
            {this.state.tableData[index].transferno.value}
          </span>
        )
      },
      {
        title: (<span fieldid='note'>{this.state.json['20020SDTPEPAGE-000004']}</span>),/* 国际化处理： 转账说明*/
        dataIndex: "note",
        key: "note",
        width:'160px',
        render: (text, record, index) => (
          <span fieldid='note'>
            {this.state.tableData[index].note.value}
          </span>
        )
      },
      {
        title: (<span fieldid='pk_vouchertype'>{this.state.json['20020SDTPEPAGE-000005']}</span>),/* 国际化处理： 凭证类别*/
        dataIndex: "pk_vouchertype",
        key: "pk_vouchertype",
        width:'160px',
        render: (text, record, index) => (
            <span fieldid='pk_vouchertype'>
              {this.state.tableData[index].pk_vouchertype.display}
            </span>
        )
      },
      {
        title: (<span fieldid='pk_operator'>{this.state.json['20020SDTPEPAGE-000006']}</span>),/* 国际化处理： 定义人*/
        dataIndex: "pk_operator",
        key: "pk_operator",
        width:'160px',
        render: (text, record, index) => (
          <span fieldid='pk_operator'> 
                {this.state.tableData[index].pk_operator.display}
          </span>
        )
      }
    ];
    return columns
  }
  render() {
    let {tableData}=this.state;
    let columns = this.getTableColumns()
    let columnsldad = this.renderColumnsMultiSelect(columns);
    return (
      <NCDiv fieldid="cashAnalyse" areaCode={NCDiv.config.TableCom}>
        <div className="cashAnalyseTable">
            <Table
                columns={columnsldad}
                data={tableData}
            />
        </div>
      </NCDiv>
    );
  }
}
SelfDocModalTable.defaultProps = {
  prefixCls: "bee-table",
  multiSelect: {
    type: "checkbox",
    param: "key"
  }
};
