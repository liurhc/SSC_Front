import React, { Component } from 'react';
import { base, getMultiLang } from 'nc-lightapp-front';
import './index.less';
const { NCButton: Button, NCModal: Modal } = base;
import SyncTree from './selfExecute/syncTree';

export default class ChooseModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showModal: false,
			pk_accountingbook: '',
			orgTableData: [],
			json: {}
		};
	}
	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'20020SDTPEPAGE',domainName:'gl',currentLocale:'simpchn',callback});
	}
	componentWillReceiveProps(nextProps) {
		let showModal = nextProps.chooseModalShow;
		let pk_accountingbook = nextProps.pk_accountingbook;
		let orgTableData = nextProps.orgTableData;
		this.setState({
			showModal,
			pk_accountingbook,
			orgTableData
		});
	}
	componentDidMount() {}
	handleConfirm = () => {
		let checkedData = this.SyncTree.getCheckedData();
		this.props.handleConfirm(checkedData);
	};
	handleCancel() {
		let { handleCancel } = this.props;
		if (handleCancel) {
			handleCancel();
		}
	}
	render() {
		let { pk_accountingbook } = this.state;
		let { chooseModalShow, appcode, pagecode } = this.props;
		return (
			<Modal
				fieldid='choose'
				className="left-right-modal"
				onHide={this.handleCancel.bind(this)}
				show={chooseModalShow}
				size={'xlg'}
			>
				<Modal.Header closeButton>
					<Modal.Title>{this.state.json['20020SDTPEPAGE-000000']}</Modal.Title>{/* 国际化处理： 选择转账定义*/}
				</Modal.Header>
				<Modal.Body>
					<SyncTree
						pk_accountingbook={pk_accountingbook}
						orgTableData={this.state.orgTableData}
						appcode = {appcode}
						pagecode = {pagecode}
						SyncTreeRef={(init) => {
							this.SyncTree = init;
						}}
					/>
				</Modal.Body>

				<Modal.Footer>
					<Button fieldid='confirm' onClick={this.handleConfirm} className="button-primary">
						{this.state.json['20020SDTPEPAGE-000001']}{/* 国际化处理： 确定*/}
					</Button>

					<Button fieldid='cancel' onClick={this.handleCancel.bind(this)}>{this.state.json['20020SDTPEPAGE-000002']}</Button>{/* 国际化处理： 取消*/}
				</Modal.Footer>
			</Modal>
		);
	}
}
