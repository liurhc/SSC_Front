import {ajax} from 'nc-lightapp-front';
import {ACTIONS} from './consts';

function myajax(url, data, callback){
    ajax({
        url, data, 
        success: (res) => {
            if(callback){
                callback(res);
            }
        }
    });
}

/**
 * 校验分类是否被引用，已被引用的分类，不能增加下级
 * @param {*} pk_transsort 
 * @param {*} callback 
 */
function checkSortUsed(pk_transsort, callback){
    let url = ACTIONS.CHECK_SORT_USED;
    let data = {pk_transsort};
    myajax(url, data, callback);
}

export {checkSortUsed};
