import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, high, toast, promptBox, getBusinessInfo,getMultiLang, createPageIcon } from 'nc-lightapp-front';
import { InputItem } from '../../../public/components/FormItems';
const {  NCRadio, NCDiv } = base;
import { onButtonClick, initTemplate } from './events';
import './index.less';
import PsndocTreeGridRef from '../../../../uapbd/refer/psninfo/PsndocTreeGridRef';
import AccountBookTreeRef from '../../../gl_cashFlows/pk_book/refer_pk_book';
import TransSortTreeRef from '../../../refer/voucher/TransSortTreeRef/index';
import createScript from '../../../public/components/uapRefer.js';
import SelfDocTable from '../table';
import { checkSortUsed } from '../utils/index';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook';
import ReferLoader from '../../../public/ReferLoader/index.js';
import HeaderArea from '../../../public/components/HeaderArea';
const NCTree = base.NCTree;
const { Refer } = high;
let tableid = 'pk_transfer';
let pageCode = '20020SDTRD_list';
let pk_accountingbook;

class SyncTree extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		let { form, button, table, search, syncTree } = this.props;
		let { setSyncTreeData } = syncTree;
		this.setSyncTreeData = setSyncTreeData; //设置树根节点数据方法
		// this.openNodeByPkAsync = openNodeByPkAsync; //展开单个节点或多个节点,
		let data = [];
		this.newTree = syncTree.createTreeData(data); //创建树 组件需要的数据结构
		this.state = {
			accountingbook: {
				refname: '',
				refpk: ''
			},
			selectedValue: '',
			mainData: {}, //主数据
			selectedItem: {
				//子表数据
				transcode: '',
				transname: '',
				pk_upertrans: '',
				key: '',
				transperms: [
					{
						iscorppublic: 'Y',
						pk_user: ''
					}
				],
				pk_accountingbook: 'sysinit',
				isEdit: true
			},
			user: {
				refname: '',
				refpk: ''
			},
			// 新增时，上下级均禁用;修改，根目录下的上下级可操作
			uprefStatus: true, //编辑态和浏览态，是否可编辑，分类上下级,
			addStatus: true, //编辑态，是否可操作, 权限，true不可操作
			saveData: {},
			stateType: '1',
			tabState: '',
			configs: {}, //人员参照
			tableData: [], //表格数据
			transpermsStatus: 'Y', //公有私有默认值
			json:{},
		};
		this.treeId = 'transferGroup'
	}

	//获取初始数据
	getInitData = (pk,callback) => {
		let data = {
			pk_accountingbook: pk ? pk : 'sysinit' //核算账簿主键，若为集团默认值为 sysinit
		};
		let self = this;
		let url = '/nccloud/gl/voucher/queryTransferType.do';
		let factorRoot = [
			{
				title: self.state.json['20020SDTCD-000002'],/* 国际化处理： 结转分类*/
				refname: self.state.json['20020SDTCD-000002'],/* 国际化处理： 结转分类*/
				refpk: '999',
				innercode: '',
				nodeData: {
					btnAdd: true,
					btnDelete: false,
					btnUpdate: false
				},
				children: []
			}	
		];
		ajax({
			url,
			data,
			success: function(response) {
				const { data, error, success } = response;
				if (success) {
					if(data){
						let { form, button, table, search, syncTree } = self.props;
						if(callback){
							callback()
						}
						if (!data.nodes.children[0].children) {
							data.nodes.children[0].children = [];
						}
						self.newTree = self.dealTreeData(data.nodes.children);
						factorRoot[0].children = self.newTree;
						self.setState({
							mainData: data
						});
						self.setSyncTreeData(self.treeId, factorRoot);
						self.props.syncTree.openNodeByPk(self.treeId, '999')
						
						setTimeout(() => {
							self.setTreeIconVisible(factorRoot);
						}, 1000);
					}
				} else {
				}
			}
		});
	};

	setTreeIconVisible = (treeData) => {
		let page = this;
		const iconData = [];
		if (treeData && treeData.length > 0) {
			treeData.map((item) => {
				if (item) {
					page.extractIconState(item, iconData);
				}
			});
		}
		page.props.syncTree.setIconVisible(page.treeId, iconData);
	};

	extractIconState(treeData, iconsState) {
		let { refpk, nodeData, children } = treeData;
		let iconState = {
			key: refpk,
			value: {
				addIcon: nodeData.btnAdd,
				delIcon: nodeData.btnDelete,
				editIcon: nodeData.btnUpdate
			}
		};
		iconsState.push(iconState);
		if (children && children.length > 0) {
			children.map((item) => {
				if (item) {
					this.extractIconState(item, iconsState);
				}
			});
		}
	}

	clickEditIconEve(data) {
		//编辑事件
		let { mainData } = this.state;
		let webVOs = mainData.webVOs;
		let addFlag = false;
		this.props.syncTree.setNodeDisable(this.treeId, true);
		if (webVOs.length) {
			for (let i = 0; i < webVOs.length; i++) {
				if (webVOs[i].pk_transsort == data.key) {
					webVOs[i].isEdit = false;
					this.updateButtonStatus(true);
					webVOs[i].key = data.key;
					if (!webVOs[i].pk_upertrans) {
						addFlag = false;
					} else {
						addFlag = true;
					}
					webVOs[i].upper = this.getFa(webVOs, webVOs[i].pk_upertrans);
					let transpermsStatus = webVOs[i].transperms[0].iscorppublic
					this.setState(
						{
							selectedItem: Object.assign({}, webVOs[i]),
							stateType: 1,
							uprefStatus: false,
							addStatus: addFlag,
							transpermsStatus
						},
						() => {
							let {transpermsStatus} = this.state
							transpermsStatus==='N' ?
							this.SelfDocTable.updataTableButtonStatus(!this.state.addStatus) : ''
						}
					);
				}
			}
		}
	}
	clickAddIconEve(data, item, isChange) {
		let pk_transsort = data.refpk;
		this.props.syncTree.setNodeDisable(this.treeId, true);
		checkSortUsed(pk_transsort, (res) => {
			let { success, error } = res;
			if (success) {
				if (!res.data) {
					this.addSort(data);
				} else {
					toast({ content: this.state.json['20020SDTCD-000003'], color: 'warning'});/* 国际化处理： 此分类已被引用，不能增加下级*/
					this.props.syncTree.setNodeDisable(this.treeId, false);
				}
			} else {
				toast({ content: error.msg, color: 'danger' });
				this.props.syncTree.setNodeDisable(this.treeId, false);
			}
		});
	}

	addSort(data) {
		let { mainData, accountingbook, transpermsStatus } = this.state;
		let book = accountingbook.refpk ? accountingbook.refpk : 'sysinit';
		let webVOs = mainData.webVOs;
		let addFlag = false;
		let transperms = [
			{
				iscorppublic: 'Y',
				pk_user: ''
			}
		];
		if (accountingbook.refpk) {
			if (webVOs.length) {
				for (let i = 0; i < webVOs.length; i++) {
					if (data.refpk==='999') {
						this.SelfDocTable.updataTableButtonStatus(true);
						addFlag = false;
					}
					if (webVOs[i].pk_transsort === data.refpk) {
						if (webVOs[i].transperms[0].iscorppublic === 'Y') {
							this.SelfDocTable.updataTableButtonStatus(true);
							addFlag = true;
							transpermsStatus = 'Y'
						} else {
							transperms = webVOs[i].transperms[0].user ? webVOs[i].transperms : [];
							this.SelfDocTable.updataTableButtonStatus(false);
							addFlag = true;
							transpermsStatus = 'N'
						}
					}
				}
			}
		} else {
			this.SelfDocTable.updataTableButtonStatus(true);
			addFlag = false;
		}

		let selectedItem = {
			transcode: '',
			transname: '',
			pk_upertrans: data.refpk,
			upper: {
				refname: data.refname,
				refpk: data.refpk
			},
			key: data.key,
			transperms: transperms,
			pk_accountingbook: book
		};
		this.setState({
			selectedItem,
			stateType: 2,
			uprefStatus: true,
			tableData: transperms,
			addStatus: addFlag,
			transpermsStatus
		});
		this.updateButtonStatus(true);
	}

	clickDelIconEve(data) {
		let localData = data;
		
		promptBox({
			color:'warning',
			title: this.state.json['20020SDTCD-000004'],/* 国际化处理： 删除*/
			content: this.state.json['20020SDTCD-000005'],  /* 国际化处理： 确定要删除吗？*/
			beSureBtnClick: () => {
				let self = this;
				let url = '/nccloud/gl/voucher/deleteTransSort.do';
				ajax({
					url,
					data: {
						pk_transsort: localData.key
					},
					success: function(response) {
						const { data, error, success } = response;
						if (success) {
							self.props.syncTree.delNodeSuceess(self.treeId, localData.key);
							
							self.setInitStatus()
							
							
						} else {
						}
					}
				});			        
			},
			cancelBtnClick: () => {
				
			}
		});	
		
	}
	onSelectEve(data, item, isChange) {
		let { mainData } = this.state;
		let webVOs = mainData.webVOs;
		// if(selectedItem.key==='999' || selectedItem.key === ''){
		// 	selectedItem.key = '999';
		// 	this.setInitStatus()
		// }
		if (webVOs.length) {
			for (let i = 0; i < webVOs.length; i++) {
				if (webVOs[i].pk_transsort == data) {
					webVOs[i].isEdit = true;
					this.updateButtonStatus(false);
					webVOs[i].upper = this.getFa(webVOs, webVOs[i].pk_upertrans);
					// let transperms = webVOs[i].transperms[0].user ? webVOs[i].transperms : [];
					let transperms = webVOs[i].transperms;
					let tableData = this.transPermsToTableData(transperms);
					this.setState(
						{
							selectedItem: webVOs[i],
							tableData: tableData,
							uprefStatus: true,
							addStatus: true
						},
						() => {
							this.SelfDocTable.updataTableButtonStatus(false);
						}
					);
				}
			}
		}
	}
	getFa = (nodesData, data) => {
		//获取父节点信息
		for (let i = 0; i < nodesData.length; i++) {
			if (data == nodesData[i].pk_transsort) {
				return {
					refname: nodesData[i].transname,
					refpk: nodesData[i].pk_transsort
				};
			}
		}
	};
	changButtonStates = () => {
		//修改树按钮状态
		let mainData = this.state.mainData;
		let data = this.dealTreeData(mainData.nodes.children);
		for (let i = 0, len = data.length; i < len; i++) {
			if (data[i].nodeData) {
				let obj = {
					delIcon: data[i].nodeData.btnDelete, //false:隐藏； true:显示; 默认都为true显示
					editIcon: data[i].nodeData.btnUpdate,
					addIcon: data[i].nodeData.btnAdd
				};
				this.props.syncTree.hideIcon(this.treeId, data[i].key, obj);
			}
		}
	};
	
	createCfg(id, param) {
		var obj = {
			value: this.state.configs[id] ? this.state.configs[id].value : [],
			onChange: function(val) {
				var temp = Object.assign(this.state.configs[id], { value: val });
				this.setState(Object.assign(this.state.configs, temp));
			}.bind(this)
		};
		this.state.configs[id] = obj;
		var result_param = Object.assign(obj, param);
		return result_param;
	}
	dealTreeData(data) {
		let joinCodeAndName = function(node){
			if(node){
				node.title = node.innercode + " " + node.refname;
				node.refname = node.innercode + " " + node.refname;
				if(node.children){
					node.children.map((item) => {
						joinCodeAndName(item);
						return item;
					});
				}
			}
		}
		if (data && data.length > 0) {
			data.forEach((e) => {
				joinCodeAndName(e);
			});
		}
		return data;
	}
	reFresh = (loadType='',callback) => {
		let localurl = unescape(window.location);
		let self = this;
		let type;
		if (localurl.match(/type=(\S*)/)) {
			type = localurl.match(/type=(\S*)/)[1].split('&')[0];
		}	
		if (type === 'org') {
			self.setState({
				tabState: 'org'
			});
			if(loadType === 'refresh'){
				let { accountingbook } = self.state
				if(accountingbook.refpk){
					self.getInitData(accountingbook.refpk, callback);
					//置空数据
					self.setInitStatus()
				} else {
					toast({ content: this.state.json['20020SDTCD-000028'], color: 'warning'});/* 国际化处理： 请选择财务核算账簿*/
				}
			} else {
				let appcode = this.props.getSearchParam('c');
				getDefaultAccountBook(appcode).then((defaultAccouontBook) => {
					if (defaultAccouontBook.value) {
						let accountingbook = {
							refname: defaultAccouontBook.display,
							refpk: defaultAccouontBook.value
						};
						self.setState({ accountingbook });
						self.getInitData(accountingbook.refpk, callback);
					}
				});
			}
		} else {
			self.getInitData('', callback);
			self.setState({
				tabState: ''
			});
		}
	};
	setInitStatus = () => {
		let selectedItem = {
			//子表数据
			transcode: '',
			transname: '',
			pk_upertrans: '',
			key: '',
			transperms: [
				{
					iscorppublic: 'Y',
					pk_user: ''
				}
			],
			pk_accountingbook: 'sysinit'
			// isEdit: true
		};
		selectedItem.isEdit = true;
		this.updateButtonStatus(false);
		this.setState(
			{
				selectedItem,
				uprefStatus: true,
				addStatus: true,
				transpermsStatus: 'Y',
			},
			() => {
				this.SelfDocTable.updataTableButtonStatus(false);
			}
		);
	}
	handleSave = () => {
		//保存
		let { selectedItem, user, stateType, tableData, tabState, accountingbook,transpermsStatus } = this.state;
		if(!selectedItem.upper || selectedItem.upper.refname===this.state.json['20020SDTCD-000002']){/* 国际化处理： 结转分类*/
			selectedItem.upper = {};
			selectedItem.upper.refpk = '';
			selectedItem.key = ''
		}
		selectedItem.pk_upertrans = selectedItem.upper.refpk
		this.props.syncTree.setNodeDisable(this.treeId, false);
		let mainData = JSON.parse(JSON.stringify(selectedItem));
		let self = this;
		if(!mainData.transcode && !mainData.transname){
			toast({ content: this.state.json['20020SDTCD-000006'], color: 'warning'});/* 国际化处理： 分类编码和分类名称不能为空*/
		} else {
			if(!mainData.transcode){
				toast({ content: this.state.json['20020SDTCD-000007'], color: 'warning'});/* 国际化处理： 分类编码不能为空*/
			}
			if(!mainData.transname){
				toast({ content: this.state.json['20020SDTCD-000008'], color: 'warning'});/* 国际化处理： 分类名称不能为空*/
			}
		}
		if(mainData.transcode && mainData.transname){
			if (tabState == 'org') {
				if(transpermsStatus==='Y'){
					mainData.transperms = [
						{
							iscorppublic: 'Y',
							pk_user: ''
						}
					];
				} else {
					if(tableData.length > 0){
						mainData.transperms = tableData;
					} else {
						mainData.transperms = [
							{
								iscorppublic: 'N',
								pk_user: ''
							}
						];
						toast({ content: this.state.json['20020SDTCD-000009'], color: 'warning'});/* 国际化处理： 请选择操作人*/
						return false
					}
				}
				mainData.pk_accountingbook = accountingbook.refpk;
			} else {
				mainData.transperms = [
					{
						iscorppublic: 'Y',
						pk_user: ''
					}
				];
			}
			let data = mainData;
			let url;
			if (stateType == 1) {
				url = '/nccloud/gl/voucher/updateTransSort.do';
			} else if (stateType == 2) {
				url = '/nccloud/gl/voucher/insertTransSort.do';
			}
			ajax({
				url,
				data,
				success: function(response) {
					const { data, error, success } = response;
					if (success) {
						self.getInitData(accountingbook.refpk, ()=>{
							toast({ title: self.state.json['20020SDTCD-000027'], color: 'success' });/* 国际化处理： 保存成功*/
						});
						self.setInitStatus()
					} else {
					}
				}
			});
			
		}
		
	};
	
	//取消
	handleCancel = () => {
		promptBox({
			color:'warning',
			title: this.state.json['20020SDTCD-000010'],/* 国际化处理： 取消*/
			content: this.state.json['20020SDTCD-000011'],  /* 国际化处理： 确定要取消吗？*/
			beSureBtnClick: () => {
				let { mainData, selectedItem } = this.state;
				let webVOs = mainData.webVOs;
				this.props.syncTree.setNodeDisable(this.treeId, false);
				if (webVOs.length) {
					for (let i = 0; i < webVOs.length; i++) {
						if(selectedItem.key==='999' || selectedItem.key === ''){
							selectedItem.key = '999';
							this.setInitStatus()
						}
						if (webVOs[i].pk_transsort == selectedItem.key) {
							webVOs[i].isEdit = false;
							selectedItem.isEdit = false;
							this.updateButtonStatus(false);
							this.SelfDocTable.updataTableButtonStatus(false);
							webVOs[i].upper = this.getFa(webVOs, webVOs[i].pk_upertrans);
							let transperms = webVOs[i].transperms;
							let tableData = this.transPermsToTableData(transperms);
							this.setState({
								selectedItem: Object.assign({}, webVOs[i]),
								tableData: tableData,
								uprefStatus: true,
								addStatus: true
							});
						}
					}
				}
			},
			cancelBtnClick: () => {
				
			}
		});
		
	};
	updateButtonStatus = (visible) => {
		this.props.button.setButtonVisible([ 'save', 'cancel' ], visible);
		this.props.button.setButtonVisible([ 'refresh' ], !visible);
	};


	componentWillMount() {
		this.props.button.setButtonVisible([ 'save', 'cancel' ], false);
		let callback= (json) =>{
			this.setState({
				json:json,
			},()=>{		  
				initTemplate.call(this, this.props);
				this.reFresh();
			})
		}
		getMultiLang({moduleId:'20020SDTCD',domainName:'gl',currentLocale:'simpchn',callback});
	}

	componentDidMount() {}
	//修改名称
	handleTableDataChange = (index, v) => {
		let {tableData} = this.state;
		let users = []
		// 名称去重
		tableData.map((item,editIndex)=>{	
			if(item.user && item.user.value){
				users.push(item.user.value)
			} else {
				if(item.role && item.role.value){
					users.push(item.role.value)
				} else {
					users.push('')
				}		
			}			
		})
		let tempValue = []
		if(v && v.length>0){
			v.map((item)=>{
				if(users.includes(item.refpk)){
					if(users[index] === item.refpk) {
						tempValue.push(item)
					}
				} else {
					tempValue.push(item)
				}
			})
		}
		// 多选自动扩展行
		if(tempValue.length>0){
			let tempArr = []
			tempValue.map((item,newIndex)=>{
				let obj = {
					key: tableData.length,
					iscorppublic: 'N',
					pk_transsort: '',
					user: {
						display: '',
						value: '',
					},
					role: {
						display: '',
						value: '',
					},
					code: '',
					pk_transperm: '',
					pk_busiunit: ''
				};
				if(item.values.nodeType.value === '1'){
					obj.user.display = item.refname
					obj.user.value = item.refpk
					obj.user.refcode = item.refcode;
					obj.user.nodeType = item.values.nodeType;
					obj.code = item.refcode
					obj.role = {
						display: '',
						value: '',
					}
				}else{
					obj.role.display = item.refname
					obj.role.value = item.refpk
					obj.role.refcode = item.refcode;
					obj.role.nodeType = item.values.nodeType;
					obj.code = item.refcode
					obj.user = {
						display: '',
						value: '',
					}
				}
				tempArr.push(obj);
				if(newIndex === 0){
					tableData.splice(index+newIndex, 1, obj);
				}else{
					tableData.splice(index+newIndex-1, 0, obj);
				}
			})
		} else {
			tableData[index].user = null
			tableData[index].role = null 
			tableData[index].code = null
		}
		
		this.setState({
			tableData: this.state.tableData
		});
	}

	/**
	 * 将查询出来的权限转换为table数据
	 * @param {*} perms 
	 */
	transPermsToTableData(perms){
		let tableData = [];
		if(perms){
			perms.map((item) => {
				let data = {
					iscorppublic: 'N',
					pk_transsort: item.pk_transsort,
					code:item.code
				};
				if(item.user && item.user.value){
					let {display, value} = item.user;
					data.user = {
						display, value, 
						nodeType: {display: this.state.json['20020SDTCD-000012'], scale: "-1", value: "1"},/* 国际化处理： 用户*/
						refcode: item.code
					}
				}
				if(item.role && item.role.value){
					let {display, value} = item.role;
					data.role = {
						display, value,
						nodeType: {display: this.state.json['20020SDTCD-000013'], value:"2"},/* 国际化处理： 角色*/
						refcode: item.code
					}
				}
	
				tableData.push(data);
			});
		}
		return tableData;
	}

	render() {
		const { syncTree, DragWidthCom } = this.props;
		let { createSyncTree } = syncTree;
		let {
			mainData,
			accountingbook,
			selectedItem,
			user,
			pk_accountingbook,
			tabState,
			tableData,
			uprefStatus,
			addStatus,
			transpermsStatus
		} = this.state;
		let accbookRefCode = 'uapbd/refer/org/AccountBookTreeRef/index.js';
		return (
			<div className="nc-bill-tree-table">
				{/* 自定义结转分类定义-{tabState == 'org' ? '组织' : '集团'} */}
				<HeaderArea 
					title = {this.props.getSearchParam('n')}
					searchContent = {
						<div className={tabState == 'org' ? 'main-org' : 'main-org display-none'}>
						<ReferLoader
							tag="transferGroup"
							fieldid='accountingbook'
							refcode={accbookRefCode}
							isMultiSelectedEnabled={false}
							showGroup={false}
							showInCludeChildren={true}
							disabledDataShow = {true}
							value={{
								refname: this.state.accountingbook.refname,
								refpk: this.state.accountingbook.refpk
							}}
							queryCondition={{
								appcode: this.props.getSearchParam('c'),
								TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
							}}
							onChange={(v) => {
								this.getInitData(v.refpk);
								this.setState({
									accountingbook: v
								});
							}}
						/>
					</div>
					}
					btnContent = {this.props.button.createButtonApp({
						area: 'page_header',
						onButtonClick: onButtonClick.bind(this),
						// popContainer: document.querySelector('.header-button-area')
					})}
				/>
				<div className="tree-form-table-area">
					<DragWidthCom
						defLeftWid={'280px'}
						leftDom={createSyncTree({
							treeId: this.treeId,
							showLine: true,
							showModal: false, //是否使用弹出式编辑
							needSearch: false, //是否需要查询框，默认为true,显示。false: 不显示
							selectedForInit: true, //默认选中第一个子节点
							clickEditIconEve: this.clickEditIconEve.bind(this),
							onSelectEve: this.onSelectEve.bind(this), //选择节点回调方法
							clickAddIconEve: this.clickAddIconEve.bind(this), //增加节点
							clickDelIconEve: this.clickDelIconEve.bind(this)
						})}
						rightDom={[
							<NCDiv fieldid='transferGroup' areaCode={NCDiv.config.FORM} className="form-area">
								<div className="form-item" fieldid='transcode'>
									<label className='nc-theme-common-font-c'><i className='require-true'>*</i>
									{this.state.json['20020SDTCD-000016']}：</label>{/* 国际化处理： 分类编码*/}
									<InputItem
										type="customer"
										name="scale"
										fieldid='transcode'
										isViewMode={selectedItem.isEdit}
										defaultValue={selectedItem.transcode}
										onChange={(v) => {
											selectedItem.transcode = v;
											if (v.length > 20) {
												selectedItem.transcode = '';
											}
											this.setState({
												selectedItem
											});
											// this.props.getSaveDataItem(saveData)
										}}
									/>
								</div>
								<div className="form-item" fieldid='transname'>
									<label className='nc-theme-common-font-c'><i className='require-true'>*</i>{this.state.json['20020SDTCD-000017']}：</label>{/* 国际化处理： 分类名称*/}
									<InputItem
										type="customer"
										name="scale"
										fieldid='transname'
										isViewMode={selectedItem.isEdit}
										defaultValue={selectedItem.transname}
										onChange={(v) => {
											selectedItem.transname = v;
											if (v.length > 40) {
												selectedItem.transname = '';
											}
											this.setState({
												selectedItem
											});
											// this.props.getSaveDataItem(saveData)
										}}
									/>
								</div>
								<div className="form-item" fieldid='upper'>
									<label className='nc-theme-common-font-c'>{this.state.json['20020SDTCD-000018']}：</label>{/* 国际化处理： 分类上级*/}
									{this.state.uprefStatus ? (<span className='edit-txt-show nc-theme-title-font-c'>{selectedItem.upper?selectedItem.upper.refname:this.state.json['20020SDTCD-000002']}</span>):(/* 国际化处理： 结转分类*/
										TransSortTreeRef(
											({} = this.createCfg('TransSortTreeRef', {
												// isMultiSelectedEnabled: true,
												// disabled: this.state.uprefStatus,
												fieldid: 'upper',
												value: selectedItem.upper,
												queryCondition:{
													pk_accountingbook : '', //TODO 传递核算账簿主键
													"isDataPowerEnable": 'Y',
													"DataPowerOperationCode" : 'fi'
												},
								
												onChange: (v) => {
													selectedItem.upper = v;
													// selectedItem.pk_upertrans = v.refpk
	
													this.setState({
														selectedItem
													});
													// this.props.getSaveDataItem(saveData)
												}
											}))
										)
									)}
								</div>
								<div className="form-item" fieldid='transperms'>
									<label className='nc-theme-common-font-c'>{this.state.json['20020SDTCD-000019']}：</label>{/* 国际化处理： 应用范围*/}
									{addStatus ? (<span className='edit-txt-show nc-theme-title-font-c'>
										{selectedItem.transperms.length>0 && selectedItem.transperms[0].iscorppublic ==='Y' ? this.state.json['20020SDTCD-000014'] : this.state.json['20020SDTCD-000015']}{/* 国际化处理： 公有,私有*/}
									</span>): (
										<NCRadio.NCRadioGroup
										selectedValue={transpermsStatus}
										onChange={(v) => {
											let businessInfo = getBusinessInfo();
											let { userId, userName, userCode } = businessInfo;
											// selectedItem.transperms[0].iscorppublic = v;
											selectedItem.transperms[0] = {
												iscorppublic: v,
												pk_user: ''
											};
											transpermsStatus = v
											let pk_transperm = selectedItem.transperms
												? selectedItem.transperms.map((item) => item.pk_transperm)
												: [];
											if (v == 'N') {
												tableData[0] = {
													iscorppublic: 'N',
													pk_transsort: selectedItem.pk_transsort,
													user: {
														display: userName,
														value: userId,
														nodeType: {display: this.state.json['20020SDTCD-000012'], scale: "-1", value: "1"},/* 国际化处理： 用户*/
														refcode:userCode
													},
													role: {
														display: '',
														value: '',
														nodeType: ''
													},
													code: userCode,
													pk_transperm: pk_transperm[0],
													pk_busiunit: 'N/A'
												};
											} else {
												tableData = []
											}
											this.setState({
												selectedItem,
												tableData,
												transpermsStatus
											});
										}}
									>
										<NCRadio value="Y" 
											// disabled={addStatus}
										>
											{this.state.json['20020SDTCD-000014']}{/* 国际化处理： 公有*/}
										</NCRadio>
										<NCRadio
											value="N"
											// disabled={addStatus}
											className={tabState != 'org' ? 'display-none' : ''}
										>
											{this.state.json['20020SDTCD-000015']}{/* 国际化处理： 私有*/}
										</NCRadio>
									</NCRadio.NCRadioGroup>
									)
									}
									
								</div>
							</NCDiv>,
							<div className={
									selectedItem.transperms.length>0 && selectedItem.transperms[0].iscorppublic === 'Y' ? 'display-none' : 'table-area'
								}
							>
								<SelfDocTable
									mainData={tableData}
									isEdit={selectedItem.isEdit}
									addStatus={addStatus}
									accountingbook = {accountingbook.refpk}
									// sendMainData={this.sendMainData.bind(this)}
									// getSelectArr={this.getSelectArr.bind(this)}
									SelfDocTableRef={(init) => {
										this.SelfDocTable = init;
									}}
									handleTableDataChange = {this.handleTableDataChange}
								/>
							</div>
						]}
					/>
				</div>
			</div>
		);
	}
}

let SyncTreeDom = createPage({
//	initTemplate: initTemplate
})(SyncTree);

ReactDOM.render(<SyncTreeDom />, document.querySelector('#app'));
