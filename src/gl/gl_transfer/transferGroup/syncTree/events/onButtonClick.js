import { ajax, base, toast,cacheTools,print,withNav } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
let tableid = 'gl_brsetting';
// @withNav
export default function onButtonClick( props, id) {
    switch (id) {
        case 'save'://保存
            this.handleSave()
            break;
        case 'cancel'://取消
            this.handleCancel()
            break;
        case 'refresh'://刷新
            this.reFresh('refresh', () => {
                    toast({ title: this.state.json['20020SDTCD-000026'], color: 'success' });/* 国际化处理： 刷新成功*/
                })
            break;  
        case 'add_line'://增行
            this.handleAdd()
            break; 
        case 'del_line'://删行
            this.handleDel()
            break; 
        default:
        break;

    }
}
