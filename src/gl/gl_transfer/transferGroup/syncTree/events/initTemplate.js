import {ajax} from 'nc-lightapp-front';
export default function (props) {
    let page = this
    let meta = {};
    let pageCode =props.getSearchParam('p');
    let appcode =props.getSearchParam('c');
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do', 
		data: {
			pagecode: pageCode,
			appcode: appcode//小应用id
		},
		async:false,
		success: function(res) {
			if (res.data) {
				let button = res.data;
                props.button.setButtons(button);
			}
		}
	});
    meta = {
        // 树弹出框内容模板
        tree1: {
            moduletype: 'form',
            status:'edit',
            items: [
                {
                    visible:true,
                    label: page.state.json['20020SDTCD-000000'],/* 国际化处理： 节点名称*/
                    attrcode: 'name',
                    itemtype: 'input',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
                {
                    visible:true,
                    label: page.state.json['20020SDTCD-000001'],/* 国际化处理： 编码*/
                    attrcode: 'code',
                    itemtype: 'input',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
            ]
        },
    };

    props.meta.setMeta(meta)

}
