import { ajax, toast ,base,cardCache,getMultiLang} from 'nc-lightapp-front';

export const CREATESTATUS = {
    UNENABLE : "1",//未启用
    ENABLE : "2",//已启用
}

export const moduleId = '2002';
export const searchId = '20022018_query';
export const tableId = '20022018_list';