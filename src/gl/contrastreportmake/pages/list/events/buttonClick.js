import { base, ajax, toast,cacheTools } from 'nc-lightapp-front';
import clickSearchBtn from './searchBtnClick';
export default function buttonClick(props, id) {
    switch (id) {
        case 'refresh':
            let searchVal = cacheTools.get(this.searchId);
            let self = this;
            clickSearchButton(props,searchVal,'refresh',self);
        break;
    }
}

function clickSearchButton(props,searchVal,flag,self) {
    if(searchVal){
        let pageId='20022018_web_list'
        let searchId = '20022018_query';
        let tableId = '20022018_list';
        let data={
            querycondition : searchVal,
            pagecode : pageId,
            querytype:'tree',
        };
        //this.state.querydata=data;
        cacheTools.set(searchId, searchVal);
        ajax({
            url: '/nccloud/gl/contrast/contrastreportmakequery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        if(flag=='refresh')
                        {
                            toast({ color: 'success', title:self.state.json['20022018-000007']});/* 国际化处理：刷新成功！*/
                        }
                        props.table.setAllTableData(tableId, data[tableId]);
                    }else{
                        if(flag=='refresh')
                        {
                            toast({ color: 'success', title: self.state.json['20022018-000007']});/* 国际化处理：刷新成功！*/
                        }
                        props.table.setAllTableData(tableId, {rows:[]});           
                    }
                    
                }
            }
        });
    }
    
};
