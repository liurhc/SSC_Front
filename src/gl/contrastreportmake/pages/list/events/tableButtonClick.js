import { ajax, toast, base, cardCache, getMultiLang, cacheTools } from 'nc-lightapp-front';
let { NCMessage } = base;
let { setDefData, getDefData } = cardCache;
import { searchId, tableId } from '../config.js';
import clickSearchBtn from './searchBtnClick';
const tableButtonClick = (props, key, text, record, index, tableId, page) => {
    let data;
    let searchVal = cacheTools.get(searchId);
    switch (key) {
        // 表格操作按钮
        case 'create':
            if (record) {
                ajax({
                    url: '/nccloud/gl/contrast/contrastreportmakecreate.do',
                    data: { pk_contrastreportcreate: record.pk_contrastreportcreate.value },
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            if (data == true) {
                                toast({ color: 'success', content: page.state.json['20022018-000001'] });/* 国际化处理： 生成报告成功！*/
                                clickSearchBtn(props, searchVal, 'update');
                            }
                            else {
                                toast({ color: 'warning', content: page.state.json['20022018-000002'] });/* 国际化处理： 生成报告失败！*/
                                clickSearchBtn(props, searchVal, 'update');
                            }
                        }
                    }
                });
            }
            break;
        case 'discreate':
            if (record) {
                ajax({
                    url: '/nccloud/gl/contrast/contrastreportmakediscreate.do',
                    data: { pk_contrastreportcreate: record.pk_contrastreportcreate.value },
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            if (data == true) {
                                toast({ color: 'success', content: page.state.json['20022018-000003'] });/* 国际化处理： 取消生成成功！*/
                                clickSearchBtn(props, searchVal, 'update');
                            }
                            else {
                                toast({ color: 'warning', content: page.state.json['20022018-000004'] });/* 国际化处理： 取消生成失败！*/
                                clickSearchBtn(props, searchVal, 'update');
                            }
                        }
                    }
                });
            }
            break;
        default:
            break;
    }
};
export default tableButtonClick;
