import { createPage, ajax, base, toast,cacheTools } from 'nc-lightapp-front';
import clickSearchBtn from './searchBtnClick';
import {CREATESTATUS} from '../config';
import tableButtonClick from './tableButtonClick';
let { NCPopconfirm, NCIcon } = base;
let pageId = '20022018_web_list';
let searchId = '20022018_query';
let tableId = '20022018_list';

export default function (props) {
	let page=this;
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	cacheTools.set('reportmakeappcode',appcode); 
	props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta,page)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if(props.getUrlParam('jumpflag'))
				{
					let searchVal = cacheTools.get(searchId);
					clickSearchBtn(props,searchVal,'update');
				}
			}
		}
	)
}
function getListButtons (text, record, index, page) {
	let buttonArray = [];
	if (record.createstatus.value === CREATESTATUS.UNENABLE) {
		buttonArray = ['create'];
	} else {
		buttonArray = ['discreate'];
	}
	return buttonArray;
}
function modifierMeta(props, meta,page) {
	//查询区
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		if (item.attrcode == 'pk_contrastrule') 
		{
			item.queryCondition = ()=> 
			{
				return {'GridRefActionExt':'nccloud.web.gl.ref.ContrastRuleRefSqlBuilderForContarstReport'}
			}
		}
		if (item.attrcode == 'year') 
		{
			if(cacheTools.get('reportmakepk_accperiodscheme'))
			{
				item.queryCondition = ()=> 
				{
					return {"isadj":"N"}
				}
			}
			else
			{
				item.queryCondition = ()=> {
					return {
						"pk_accperiodscheme":cacheTools.get('reportmakepk_accperiodscheme'),
						"isadj":"N"
					}
				}
			}
		}
		else if (item.attrcode == 'accountingbook') 
		{
			item.isMultiSelectedEnabled = false;
			item.showGroup = false;
			item.showInCludeChildren = false;
			item.queryCondition = ()=> {
				return {
                    appcode: cacheTools.get('reportmakeappcode'),
                    pk_contrastrule: cacheTools.get('reportmakepk'),
                    isSelf:'Y',
					'TreeRefActionExt':'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstReport'
				}
			}
		}
		else if (item.attrcode == 'otheraccountingbook') 
		{
			item.isMultiSelectedEnabled = true;
			item.showGroup = true;
			item.showInCludeChildren = true;
			item.queryCondition = ()=> {
				return {
                    appcode: cacheTools.get('reportmakeappcode'),
                    pk_contrastrule: cacheTools.get('reportmakepk'),
                    isSelf:'N',
					'TreeRefActionExt':'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstReport'
				}
			}
		}
		else if (item.attrcode == 'confirmedstatus') 
		{
			item.initialvalue = {display:page.state.json['20022018-000005'],value:'4'};/* 国际化处理： 全部*/
		}
		else if (item.attrcode == 'createstatus') 
		{
			item.initialvalue = {display:page.state.json['20022018-000005'],value:'4'};/* 国际化处理： 全部*/
		}
		return item;
	})

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		 if (item.attrcode == 'month') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{ cursor: 'pointer' }}
						onClick={() => {
							props.linkTo('/gl/contrastreportmake/pages/card/index.html', {
								pagecode:'20022018_web_card',
								status: 'browse',
								id: record.pk_contrastreportcreate.value
							});
						}}
					>
						{record.month && record.month.value}
					</a>
				);
			};
		}
		return item;
	});
	meta[tableId].items.push({
		attrcode: 'opr',
		label: page.state.json['20022018-000009'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		className: 'table-opr',
		width:'160px',
		render: (text, record, index) => {
            return props.button.createOprationButton(getListButtons(text, record, index, page), {
                area: "contrastreportmake_col",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index,tableId,page)
            });
        }
	});
	let multiLang = props.MutiInit.getIntl('2052');
	return meta;
}
