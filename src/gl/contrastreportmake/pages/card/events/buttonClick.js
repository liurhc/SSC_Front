import { ajax, base, toast,cacheTools } from 'nc-lightapp-front';
export default function(props, id) {
  switch (id) {
    case 'back':
      props.linkTo('/gl/contrastreportmake/pages/list/index.html',
      {
				pagecode:'20022018_web_list',
				jumpflag:true
      })
      break
    case 'refresh':
      let data = { pk_contrastreportcreate: this.props.getUrlParam('id'), pagecode: this.pageid };
      ajax({
        url: '/nccloud/gl/contrast/contrastreportmakecard.do',
        data: data,
        success: (res) => {
          if (res.data) {
						toast({ color: 'success', title:this.state.json['20022018-000007']});/* 国际化处理：刷新成功！*/
            if (res.data.head) {
              this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
            }
            if (res.data.body) {
              this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
              setQueryContent(this.props,cacheTools.get('reportmakecontent'));
            }
          } else {
						toast({ color: 'success', title:this.state.json['20022018-000007']});/* 国际化处理：刷新成功！*/
            this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
            this.props.cardTable.setTableData(this.tableId, { rows: [] });
          }
        }
      });
      break
    default:
      break
  }
}
//设置隐藏列
function setQueryContent(props,content){
	let tableid = 'table';
	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_quantity','selfcontrastedquantity','selfsumquantity','opp_un_quantity','oppcontrastedquantity','opp_sum_quantity','sumbalance_quantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_quantity','selfcontrastedquantity','selfsumquantity','opp_un_quantity','oppcontrastedquantity','opp_sum_quantity','sumbalance_quantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_cur','selfcontrastedcur','selfsumcur','opp_un_cur','oppcontrastedcur','opp_sum_cur','sumbalance_cur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_cur','selfcontrastedcur','selfsumcur','opp_un_cur','oppcontrastedcur','opp_sum_cur','sumbalance_cur']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_orgcur','selfcontrastedorgcur','selfsumorgcur','opp_un_orgcur','oppcontrastedorgcur','opp_sum_orgcur','sumbalance_orgcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_orgcur','selfcontrastedorgcur','selfsumorgcur','opp_un_orgcur','oppcontrastedorgcur','opp_sum_orgcur','sumbalance_orgcur']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_groupcur','self_contrastedgroupcur','selfsumgroupcur','opp_un_groupcur','oppcontrastedgroupcur','opp_sum_groupcur','sumbalance_groupcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_groupcur','self_contrastedgroupcur','selfsumgroupcur','opp_un_groupcur','oppcontrastedgroupcur','opp_sum_groupcur','sumbalance_groupcur']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_globalcur','selfcontrastedglobalcur','selfsumglobalcur','opp_un_globalcur','oppcontrastedglobalcur','opp_sum_globalcur','sumbalance_globalcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_globalcur','selfcontrastedglobalcur','selfsumglobalcur','opp_un_globalcur','oppcontrastedglobalcur','opp_sum_globalcur','sumbalance_globalcur']);
	}
}
