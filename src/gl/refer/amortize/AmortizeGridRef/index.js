import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang:{
			domainName:'gl',
			currentLocale: 'simpchn', 
			moduleId: 'glrefer'
		},
		refType: 'grid',
		refName: 'glrefer-000000',/* 国际化处理： 周期凭证定义*/
	    placeholder: 'glrefer-000000',/* 国际化处理： 周期凭证定义*/
		refCode: 'gl.refer.amortize.AmortizeGridRef',
		queryGridUrl: '/nccloud/gl/ref/amortizeGridRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [{
            name: [ 'glrefer-000001', 'glrefer-000002' ],/* 国际化处理： 编号,名称*/
            code: [ 'refcode', 'refname' ]
        }]
	};

	return <Refer {...Object.assign(conf, props)} />
}
