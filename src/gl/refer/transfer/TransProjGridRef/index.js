import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang:{
			domainName:'gl',
			currentLocale: 'simpchn', 
			moduleId: 'glrefer'
		},
		refType: 'grid',
		refName: 'glrefer-000003',/* 国际化处理： 自定义结转方案*/
	    placeholder: 'glrefer-000003',/* 国际化处理： 自定义结转方案*/
		refCode: 'gl.refer.transfer.TransProjGridRef',
		queryGridUrl: '/nccloud/gl/ref/transProjGridRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [{
            name: [ 'glrefer-000004', 'glrefer-000005' ],/* 国际化处理： 规则编码,规则名称*/
            code: [ 'refcode', 'refname' ]
        }]
	};

	return <Refer {...Object.assign(conf, props)} />
}
