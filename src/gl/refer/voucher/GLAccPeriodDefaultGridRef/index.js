import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang:{
			domainName:'gl',
			currentLocale: 'simpchn', 
			moduleId: 'glrefer'
		},
		refType: 'grid',
		refName: 'glrefer-000021',/* 国际化处理： 会计期间方案*/
		placeholder: 'glrefer-000021',/* 国际化处理： 会计期间方案*/
		refCode: 'gl.refer.GLAccPeriodDefaultGridRef',
		queryGridUrl: '/nccloud/gl/ref/glAccPeriodDefaultGridRef.do',
		columnConfig: [
			{
				name: [ 'glrefer-000011', 'glrefer-000002'] ,/* 国际化处理： 编码,名称*/
				code: [ 'code', 'name' ]
			}
		],
		isMultiSelectedEnabled: false,
	};

	return <Refer {...Object.assign(conf, props)} />
}
