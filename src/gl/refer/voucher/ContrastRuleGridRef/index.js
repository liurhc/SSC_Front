import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
    var conf = {
        multiLang:{
            domainName:'gl',
            currentLocale: 'simpchn', 
            moduleId: 'glrefer'
        },
        refType: 'grid',
        refName: 'glrefer-000017',/* 国际化处理： 内部交易对账规则*/
        placeholder: 'glrefer-000017',/* 国际化处理： 内部交易对账规则*/
        columnConfig :[{name:['glrefer-000011','glrefer-000002'],code:["refcode","refname"]}],/* 国际化处理： 编码,名称*/
//         queryCondition : {pk_accountingbook:'1001A3100000000000PE'},
        refCode: 'gl.contrast.contrastruleref',
        queryGridUrl: '/nccloud/gl/contrast/contrastruleref.do'
    };

    return <Refer {...Object.assign(conf, props)} />
}
