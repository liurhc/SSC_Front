import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang:{
			domainName:'gl',
			currentLocale: 'simpchn', 
			moduleId: 'glrefer'
		},
		refType: 'grid',
		refName: 'glrefer-000016',/* 国际化处理： 对账规则*/
		refCode: 'gl.refer.voucher.BusiRuleGridRef',
		queryGridUrl: '/nccloud/gl/ref/busiRuleRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [{
            name: [ 'glrefer-000011', 'glrefer-000002' ],/* 国际化处理： 编码,名称*/
            code: [ 'refcode', 'refname' ]
        }]
	};

	return <Refer {...Object.assign(conf, props)} />
}
