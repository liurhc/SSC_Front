import { high } from 'nc-lightapp-front';

//快速分摊-分摊规则参照
//参照依赖：
//pk_accountingbook 财务核算账簿
//pk_accasoa 会计科目
//prepareddate 制单日期
//pk_unit 财务组织
const { Refer } = high;
export default function (props = {}) {
    var conf = {
        multiLang:{
			domainName:'gl',
			currentLocale: 'simpchn', 
			moduleId: 'glrefer'
		},
        refType: 'grid',
        refName: 'glrefer-000010',/* 国际化处理： 分摊规则*/
        refCode: 'gl.refer.voucher.ApportionRuleRef',
        queryGridUrl: '/nccloud/gl/ref/ApportionRuleRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [
            {
                name: ['glrefer-000011', 'glrefer-000002'],/* 国际化处理： 编码,名称*/
                code: ['refcode', 'refname']
            }
        ]
    };

    return <Refer {...Object.assign(conf, props) } />
}
