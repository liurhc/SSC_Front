import React from "react";
import {high} from "nc-lightapp-front";
const {Refer} = high;
/**
 * 元数据实体
 */

export default function(props = {}) {
    var conf = {
            multiLang:{
              domainName:'gl',
              currentLocale: 'simpchn', 
              moduleId: 'glrefer'
            },
        //{
            "placeholder": 'glrefer-000012',/* 国际化处理： 应用编码*/
            "refName": 'glrefer-000013',/* 国际化处理： 关联应用编码*/
            "refCode": "appcodeRef",
            "refType": "tree",
            "isTreelazyLoad": false,
            "onlyLeafCanSelect": true,
            "queryTreeUrl": "/nccloud/platform/appregister/appregref.do",
            "onChange": val => {
              // this.setFieldsValue({ cont: val });
            },
            // "disabled":
            //   this.state.fields.menuitemcode &&
            //   this.state.fields.menuitemcode.value.length < 8 &&
            //   isedit,
            "columnConfig": [
              {
                "name": ['glrefer-000011', 'glrefer-000002'],/* 国际化处理： 编码,名称*/
                "code": ["refcode", "refname"]
              }
            ],
            "isMultiSelectedEnabled": false
          //}
    };

    return <Refer {...Object.assign(conf, props)} />;
}
