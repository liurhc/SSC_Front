import React, { Component } from 'react';
import { base, high,ajax ,toast, getMultiLang} from 'nc-lightapp-front';
const { Refer } = high;
const { PopRefer } = high.Refer, // 引入PopRefer类
      { NCRadio: Radio ,NCTree, NCMenu, NCDropdown,NCButton, NCFormControl,NCCheckbox} = base,
	  { NCRadioGroup: RadioGroup } = Radio;
import AssidModal from '../../../public/components/voucherAssidModal'
const TreeNode = NCTree.NCTreeNode;
import './index.less';
import { debug } from 'util';
let searchId = '200210VCR';
window.localCheckedArray = [];
window.localAssData = [];
window.localPk_accountingbook = '';
class Ref extends PopRefer { // 继承PopRefer类
    constructor(props) {
		super(props);
		this.interval = 0;
        this.state = {
            ...this.state, // 继承state
			pretentAssData: {
				pk_accasoa: "",
				prepareddate: "",
				assData: [],
				checkedArray: []
			},
			isQueryShow: false,
			json:[]
		};
	}
	componentWillMount() {
		let callback=(json)=>{
			this.setState({ json })
		}
		getMultiLang({moduleId:'glrefer',domainName:'gl',callback});
	}
	handleClose(){
        this.setState({
            isQueryShow: false
        });
	}
	getParam = () => {
		var { queryCondition } = this.props,
		queryCondition = queryCondition ? typeof queryCondition === 'function' ? queryCondition(): typeof queryCondition === 'object' ? queryCondition : {}: {};
		return queryCondition;
    };
	
	    /**
     * 辅助核算确定事件
     */
    doConfirm(displayname, data){
		let arr = [], str = '', strArr = '';
		data.data.forEach((item, index) => {
			let obj = {};
			obj.pk_Checktype = item.pk_Checktype;
			obj.pk_Checkvalue = item.pk_Checkvalue;
			arr.push(obj);
			let strCheckvalue = '';
			if(item.pk_Checkvalue && item.pk_Checkvalue != undefined){
				for(var i=0; i<item.pk_Checkvalue.length; i++){
					if (i == item.pk_Checkvalue.length - 1) {
						strCheckvalue += item.pk_Checkvalue[i] + "";
					} else {
						strCheckvalue += item.pk_Checkvalue[i] + ",";
					}
				}
			} else {
				strCheckvalue = '~';
			}
			if (index == data.data.length - 1) {
				str += '{"pk_Checktype":"' + item.pk_Checktype+'","pk_Checkvalue":"'+ strCheckvalue +'"}';
			} else {
				str += '{"pk_Checktype":"' + item.pk_Checktype+'","pk_Checkvalue":"'+ strCheckvalue +'"},';
			}
		});
		strArr = "["+ str +"]";
		this.props.onChange({display:displayname,value: strArr},{})
		if (data.assData.length > 0) {
			localCheckedArray = data.checkedArray;
			localAssData = data.assData;
			localPk_accountingbook = data.assData[0].pk_accountingbook;
		}
		this.handleClose();
		
    }
	handleQueryClick(data){
		let displayname = data.assname;
		this.doConfirm(displayname,data);		  
    }
	iconClick = ()=> {
		let queryCondition = this.getParam();
		if (queryCondition.pk_accountingbook == null) {
			toast({color:"danger",content:this.state.json["glrefer-000035"]});
			return
		}
		if (queryCondition.checkboxShow == null) {
            queryCondition.checkboxShow=true;
		}
		if (queryCondition.assid == null) {
			queryCondition.assid = this.props.foolValue.value;
		}
		let checkedArray = [], assData = [];
		if (queryCondition.pk_accountingbook == localPk_accountingbook && localCheckedArray.length > 0) {
			checkedArray = localCheckedArray;
			assData = localAssData;
		} else {
			checkedArray = [];
			assData = [];
			localCheckedArray = [];
			localAssData = [];
		}
		queryCondition.checkedArray = checkedArray;
		queryCondition.assData = assData;
		this.setState({
			isQueryShow: true,
			pretentAssData: queryCondition
		})
        
	}
	
	render() {
		return (
			<div className='u-form-control-wrapper assid-container'>
				{
					this.props.foolValue.value == '' ? 
					<div type="text"  class="assid-hidden-input nc-theme-area-split-bc"/>
					: 
					<input readonly="readonly" className='nc-input u-form-control md ' value={this.props.foolValue.display}/>
				}
				<span className='icon-refer' onClick={this.iconClick.bind(this)}></span>
				<AssidModal
					className="account-ref"
					pretentAssData={this.state.pretentAssData}
					showOrHide = {this.state.isQueryShow}
					onConfirm = {this.handleQueryClick.bind(this)} 
					handleClose={this.handleClose.bind(this)}
				/>
			</div>
			
		)
	}
}


export default function (props) {
    var conf = {
        refCode: 'gl.refer.voucher.AccountRef',
		isMultiSelectedEnabled:false,
		refType:'grid',
    };
    return <Ref {...Object.assign(conf, props)} />
}
