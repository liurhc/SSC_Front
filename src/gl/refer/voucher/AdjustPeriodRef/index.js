import { high } from 'nc-lightapp-front';

//快速分摊-分摊规则参照
//参照依赖：
//pk_accountingbook 财务核算账簿 （必传）
//year 年
//beginDate 'yy-mm-dd'
//endDate 'yy-mm-dd'
const { Refer } = high;
export default function (props = {}) {
    var conf = {
        multiLang:{
			domainName:'gl',
			currentLocale: 'simpchn', 
			moduleId: 'glrefer'
		},
        refType: 'grid',
        refName: 'glrefer-000006',/* 国际化处理： 会计期间调整期*/
        refCode: 'gl.refer.voucher.AdjustPeriodRef',
        queryGridUrl: '/nccloud/gl/ref/AdjustPeriodRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [
            {
                name: [ 'glrefer-000007','glrefer-000008','glrefer-000009'],/* 国际化处理： 调整期,开始月份,结束月份*/
                code: [ "refcode", "beginmonth", "endmonth"]
            }
        ]
    };

    return <Refer {...Object.assign(conf, props) } />
}
