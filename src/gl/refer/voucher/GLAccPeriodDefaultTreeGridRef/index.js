import { high } from 'nc-lightapp-front';
import {conf as unitConf} from '../GLAccPeriodDefaultGridRef/index';
const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang:{
			domainName:'gl',
			currentLocale: 'simpchn', 
			moduleId: 'glrefer'
		},
		refType: 'gridTree',
		refName: 'glrefer-000022',/* 国际化处理： 会计期间*/
		refCode: 'gl.refer.GLAccperiodDefaultTreeGridRef',
		placeholder: 'glrefer-000023',/* 国际化处理： 会计期间档案*/
		queryTreeUrl: '/nccloud/gl/ref/glAccPeriodDefaultTreeRef.do',
		rootNode: { refname: 'glrefer-000024', refpk: 'root' },/* 国际化处理： 会计年度*/
		queryGridUrl: '/nccloud/gl/ref/glAccPeriodDefaultGridRef.do',
		columnConfig: [
			{
				name: [ 'glrefer-000025', 'glrefer-000026','glrefer-000027'] ,/* 国际化处理： 月份,开始期间,结束期间*/
				code: [ 'yearmth', 'begindate','enddate' ]
			}
		],
		isMultiSelectedEnabled: false,
		unitProps:unitConf,
        isShowUnit:false
	};

	return <Refer {...Object.assign(conf, props)} />
}
