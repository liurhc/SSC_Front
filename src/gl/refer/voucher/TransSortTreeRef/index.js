import {high} from 'nc-lightapp-front'
import { conf as unitProps } from '../../../../uapbd/refer/org/GroupDefaultTreeRef/index';
const { Refer } = high;

export var conf = {
	multiLang:{
		domainName:'gl',
		currentLocale: 'simpchn', 
		moduleId: 'glrefer'
	},
	refType: 'tree',
	refName: 'glrefer-000034',/* 国际化处理： 结转分类档案*/
	placeholder: 'glrefer-000034',/* 国际化处理： 结转分类档案*/
	rootNode: { refname: 'glrefer-000034', refpk: 'root' },/* 国际化处理： 结转分类档案*/
	treeConfig: { name: ['glrefer-000011', 'glrefer-000002'], code: ['refcode', 'refname'] },/* 国际化处理： 编码,名称*/
	refCode: 'gl.refer.voucher.TransSortTreeRef',
	queryTreeUrl: '/nccloud/gl/ref/transSortTreeRef.do',
	unitProps: unitProps,
};

export default function (props = {}) {
	return <Refer {...Object.assign(conf, props)} />
}
