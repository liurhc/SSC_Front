import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang:{
			domainName:'gl',
			currentLocale: 'simpchn', 
			moduleId: 'glrefer'
		},
		refType: 'grid',
		refName: 'glrefer-000020',/* 国际化处理： 辅助分析*/
		refCode: 'gl.refer.voucher.ExtendReportGridRef',
		queryGridUrl: '/nccloud/gl/ref/extendreportRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [{
            name: [ 'glrefer-000011', 'glrefer-000002'] ,/* 国际化处理： 编码,名称*/
            code: [ 'refcode', 'refname' ]
        }]
	};

	return <Refer {...Object.assign(conf, props)} />
}
