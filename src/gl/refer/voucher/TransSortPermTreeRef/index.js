import { high } from 'nc-lightapp-front';

import { conf as unitProps } from '../../../../uapbd/refer/org/GroupDefaultTreeRef/index';

const { Refer } = high;

export var conf = {
	multiLang:{
		domainName:'gl',
		currentLocale: 'simpchn', 
		moduleId: 'glrefer'
	},
	refType: 'tree',
	refName: 'glrefer-000033',/* 国际化处理： 角色和操作员*/
	placeholder: 'glrefer-000033',/* 国际化处理： 角色和操作员*/
	rootNode: { refname: 'glrefer-000033', refpk: 'root' },/* 国际化处理： 角色和操作员*/
	treeConfig: { name: ['glrefer-000011', 'glrefer-000002'], code: ['refcode', 'refname'] },/* 国际化处理： 编码,名称*/
	refCode: 'gl.refer.org.TransSortPermTreeRef',
	queryTreeUrl: '/nccloud/gl/voucher/transSortPermTreeRef.do',
	isShowDisabledData: true,
	unitProps: unitProps,
	isCacheable: true
};

export default function (props = {}) {
	return <Refer {...Object.assign(conf, props)} />
}
