import {high} from 'nc-lightapp-front'

const {Refer} = high;

/**
 * queryCondition 
 * {
 *      pk_src_crop : '',
 *      pk_des_crop : ''
 * }
 * @param {*} props 
 */
export default function (props = {}){
    var conf = {
        multiLang:{
			domainName:'gl',
			currentLocale: 'simpchn', 
			moduleId: 'glrefer'
		},
        refType: 'grid',
        refName: 'glrefer-000019',/* 国际化处理： 基础档案对照*/
        refCode: 'fipub.ref.pub.DocmapGridRef',
        queryGridUrl: '/nccloud/fipub/ref/docmapGridRef.do',
        isMultiSelectedEnabled: false,
        columnConfig: [{
            name: [ 'glrefer-000011', 'glrefer-000002'] ,/* 国际化处理： 编码,名称*/
            code: [ 'refcode', 'refname' ]
        }]
    };
    return <Refer {...Object.assign(conf, props)}/>
}
