import {ajax } from 'nc-lightapp-front';
let requestApiOverwrite = {
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/gl/notrecaccrelationset/listquery.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 删除
    delete: (opt) => {
        ajax({
            url: '/nccloud/gl/notrecaccrelationset/del.do',
            data: opt.data,
            success: opt.success
        });
    }
}

export default  requestApiOverwrite;
