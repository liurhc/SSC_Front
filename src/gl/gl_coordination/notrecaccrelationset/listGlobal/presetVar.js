/**页面全局变量 */
let currentVar = {
    pageId: 'yxysdy',
    list: 'notrecaccrelationset_query',
    searchArea: 'searchArea',
    headButtonArea: 'list_head',
    listButtonArea: 'list_inner',
    // cardUrl: '/gl/gl_coordination/notrecaccrelationset/cardGlobal/index.html',
    cardUrl: '/cardGlobal',
    cardPagecode: '20022311_NOTRCARD',
    dataSource: 'gl.gl_coordination.notrecaccrelationset',
    pkname:'pk_notrecaccrel'
}
window.presetVar = {
    ...currentVar
};
export default currentVar
