import {base, promptBox,toast} from 'nc-lightapp-front';
const {NCMessage} = base;
import simpleTablePageController from '../../../../public/common/simpleTablePageController'
import pubUtils from '../../../../public/common/pubUtil'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
import pubUtil from '../../../../public/common/pubUtil';
class simpleTablePageControllerChild extends simpleTablePageController{
    constructor(main, props, meta) {
        let newMeta = {
            ...meta, 
            dataSource: (meta || {}).dataSource || presetVar.dataSource,
            listPkField: (meta || {}).listPkField || 'pk_notrecaccrel',
            listId: (meta || {}).listId || presetVar.list,
            listButtonArea: (meta || {}).listButtonAreas || presetVar.listButtonArea,
            cardUrl: (meta || {}).cardUrl || presetVar.cardUrl,
            cardPageCode: (meta || {}).cardPageCode || presetVar.cardPagecode,
        };
        super(main, props, newMeta);
        if(this.cacheData.headRef){
            this.main.state.headRef=this.cacheData.headRef;
        }
       
    }
    /**
     * 页面初始化后处理事件
     */
    callBackInitTemplate(){
        if(this.cacheData && this.cacheData.headRef){
            this.main.setState({headRef : this.cacheData.headRef});
        }
    }
    initMeta(meta){
        meta[presetVar.list].items = meta[presetVar.list].items.map((item, key) => {
    
            //单据号添加下划线超链接
            if (item.attrcode == 'srcacccode') {
                item.render = (text, record, index) => {
                    return (
                        <span
                            style={{  cursor: 'pointer',color: 'red'}}
                            onClick={() => {

                                this.setAllPks();
                                let newCacheData = this.getCacheData();
                                if(newCacheData){
                                    this.setCacheData(newCacheData);
                                }
                                this.setDataSource(record, index);
                                pubUtil.pushTo(this.props, this.cardUrl,{
                                    appcode: this.props.getSearchParam('c'),
                                    pagecode: this.cardPageCode,
                                    status: 'browse',
                                    open_status: 'browse',
                                    id: record[this.listPkField].value,
                                    ...this.AddListRowDoubleClickParam(record, index)
                                }); 
                                // this.props.pushTo(presetVar.cardUrl, {
                                //     status: 'browse',
                                //     id: record.pk_notrecaccrel.value,
                                //     appcode: this.props.getSearchParam('c'),
                                //     pagecode: this.props.getSearchParam('p'),
                                //     open_status: 'browse'
                                // });
                            }}
                        >
                            {record.srcacccode && record.srcacccode.value}
                        </span>
                    );
                };
            }
            return item;
        });
        return meta;
    }
    queryData(key){
        requestApi.query({
            data: pubUtils.getSysCode(this.props),
            success: (data) => {
                this.setTableData(data);
            }
        })
    }
    getCacheData(){
        return {headRef: this.main.state.headRef};
    }
    doDelLine(data, index){
        let _this = this;
        requestApi.delete({
            data: {pk_notrecaccrel: data['pk_notrecaccrel'].value, ...pubUtils.getSysCode(_this.props)},
            success: (data) => {
                _this.props.table.deleteTableRowsByIndex(presetVar.list, index);
                toast({color:"success",content:this.main.state.json['200017-000001']})
            }
        })
        // promptBox({
        //     color:"warning",
        //     content:this.main.state.json['2002-0004'],
        //     beSureBtnClick: function () {
                
        //     }
        // })
    }
    Add(){
        super.Add();
    }
    // 自定义功能区---------------------------------------------------------------------------------------
    cfHeadRefChange(data){
        this.main.setState({headRef : data});
    }
}
export default simpleTablePageControllerChild;
