/**页面全局变量 */
let currentVar = {
    pageId: 'yxysdy',
    list: 'inflfactorlist',
    formId: 'notrecaccrelationset',
    head: 'notrecaccrelationset',
    body: 'inflfactorlist',
    searchArea: 'searchArea',
	headButtonArea: 'card_head',
    listButtonArea: 'card_body_inner',
    listShoulderButtonArea: 'card_body',
    // listURL: '/gl/gl_coordination/notrecaccrelationset/listGlobal/index.html',
    listURL: '/listGlobal',
    listPageCode: '20022311_NOTRLIST',

    /**
     * 表单字段
     */
    formField:{
        /**
         * 来源科目主键
         */
        srcaccpk: 'srcaccpk',
        /**
         * 来源科目编码
         */
        srcacccode:'srcacccode',
        /**
         * 来源科目名称
         */
        srcaccname: 'srcaccname',
        /**
         * 目的科目主键
         */
        desaccpk: 'desaccpk',
        /**
         * 目的科目编码
         */
        desacccode:'desacccode',
        /**
         * 目的科目名称
         */
        desaccname: 'desaccname',
        /**
         * 影响因素
         */
        inflfactor: 'inflfactor',
    },
    /**
     * 列表字段
     */
    listField:{
        /**
         * 目的科目主键
         */
        desaccpksp:'desaccpksp',
        /**
         * 目的科目编码
         */
        desacccode:'desacccodesp',
        /**
         * 目的科目名称
         */
        desaccname: 'desaccnamesp',
    }
}
window.presetVar = {
    ...currentVar
};
export default currentVar
