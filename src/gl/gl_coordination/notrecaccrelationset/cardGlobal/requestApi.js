import {ajax } from 'nc-lightapp-front';
let requestApiOverwrite = {
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/gl/notrecaccrelationset/cardquery.do',
            data: opt.data,
            success: opt.success
        });
    },
    /**
     * 查模板接口
     */
    queryTemplate: (opt) => {
        ajax({
            url: '/nccloud/gl/notrecaccrelationset/templatequery.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 保存
    save: (opt) => {
        ajax({
            url: '/nccloud/gl/notrecaccrelationset/save.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 删除
    delete: (opt) => {
        ajax({
            url: '/nccloud/gl/notrecaccrelationset/del.do',
            data: opt.data,
            success: opt.success
        });
    }
}

export default  requestApiOverwrite;
