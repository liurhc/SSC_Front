import changeListTemplate from '../../../../public/common/changeListTemplate'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
class fromChangeAfter{
    constructor(main, props) {
        this.main = main;
        this.props = props;
    }
    /**
     * 表头变更事件
     * @param {*} moduleId 
     * @param {*} key 
     * @param {*} value 
     */
    changeAfter(moduleId, key, value, valueA){
        let code=valueA.refcode;
        if(!valueA.hasOwnProperty('refcode')){
            code=valueA.code;
        }
        if(!this.main.cardPageControllerChild.refpk){
            switch(key){
                // 影响因素
                case presetVar.formField.inflfactor:
                        this.queryTemplate(value);
                        if(value.value){
                            this.props.button.setDisabled({
                                AddLine: false,
                                DelLine: true,
                                Copy: true
                            });
                        }else{
                            this.props.button.setDisabled({
                                AddLine: true,
                                DelLine: true,
                                Copy: true
                            });
                        }
                break;
            }
        }else{
            // 表头编辑后事件
            switch(key){
                // 来源科目编码
                case presetVar.formField.srcacccode:
                    // 带出来源科目编码
                    this.props.form.setFormItemsValue(presetVar.formId, {[presetVar.formField.srcacccode]: {
                        value:code,
                        display:code
                    }});
                    // 带出来源科目名称
                    this.props.form.setFormItemsValue(presetVar.formId, {[presetVar.formField.srcaccname]: {
                        value:valueA.refname,
                        display:valueA.refname
                    }});
                    // 带出来源科目主键
                    this.props.form.setFormItemsValue(presetVar.formId, {[presetVar.formField.srcaccpk]: {
                        value:valueA.refpk,
                        display:valueA.refpk
                    }});
                break;
                // 目的科目编码
                case presetVar.formField.desacccode:
                    // 带出目的科目编码
                    this.props.form.setFormItemsValue(presetVar.formId, {[presetVar.formField.desacccode]: {
                        value:code,
                        display:code
                    }});
                    // 带出目的科目名称
                    this.props.form.setFormItemsValue(presetVar.formId, {[presetVar.formField.desaccname]: {
                        value:valueA.refname,
                        display:valueA.refname
                    }});
                    // 带出目的科目主键
                    this.props.form.setFormItemsValue(presetVar.formId, {[presetVar.formField.desaccpk]: {
                        value:valueA.refpk,
                        display:valueA.refpk
                    }});
                break;
                // 影响因素
                case presetVar.formField.inflfactor:
                        this.queryTemplate(value);
                break;
            }
            if(value.value){
                this.props.button.setDisabled({
                    AddLine: false,
                    DelLine: true,
                    Copy: true
                });
            }else{
                this.props.button.setDisabled({
                    AddLine: true,
                    DelLine: true,
                    Copy: true
                });
            }
        }
        
    }
    /**
     * 查询模板
     * @param {*} value 
     */
    queryTemplate(value){
        if((value || {}).value){
            this.doQueryTemplate(value.value.split(','),value);
        }else{
            this.cfChangeListTemplate.call(this, {item:[]});
            let allRows = this.props.editTable.getAllRows(presetVar.list, true);
            if(allRows!=null){
                allRows.map((one)=>{
                    // TODO: 平台问题，同步情况下无法连续删除行
                    window.setTimeout(() => {
                        this.props.editTable.deleteTableRowsByRowId(presetVar.list, one.rowid);        
                    },10);
                })
            }
        }
    }
    /**
     * 查询模板
     * @param {*} key 
     */
    doQueryTemplate(key,value){
        requestApi.queryTemplate({
            data: {inflfactor: key},
            success: (data) => {
                this.callBackDoQueryTemplate(data,value);
            }
        })
    }
    /**
     * 查询模板回掉
     * @param {*} data 
     */
    callBackDoQueryTemplate(data,value){
        this.cfChangeListTemplate.call(this, data.data[presetVar.list],value);
        let allRows = this.props.editTable.getAllRows(presetVar.list, true);
        if(allRows!=null){
            allRows.map((one)=>{
                // TODO: 平台问题，同步情况下无法连续删除行
                window.setTimeout(() => {
                    this.props.editTable.deleteTableRowsByRowId(presetVar.list, one.rowid);        
                },10);
            })
        }
    }
    /**
     * 更新表体模板
     * @param {*} data 
     */
    cfChangeListTemplate(data,value){
        let meta=changeListTemplate(this.props, presetVar.list, data, ['numberindex'], ['pk_notrecaccrelsp','desaccpksp','desacccodesp','desaccnamesp','ts','opr']);
        
        meta[presetVar.list].items=meta[presetVar.list].items.map((one)=>{
            if(one.itemtype=="refer"){
                if(one.attrcode.indexOf('fac')!=-1){
                    one.fieldDisplayed='refname';
                    let pk_defdoclist='';
                    if(value){
                        let values=value.value.split(',');
                        for(let i=0;i<values.length;i++){
                            if(value[''+i].values){
                                if(value[''+i].refname==one.label){
                                    if(value[''+i].values.entityid.value){
                                        pk_defdoclist=value[''+i].values.entityid.value;
                                    }
                                }
                            }
                        }
                        one.queryCondition = () => {
                            return {
                                "pk_org":'GLOBLE00000000000000',
                                "isDataPowerEnable": 'Y',
                                "DataPowerOperationCode" : 'fi',
                                "isShowUnit":true,
                                "pk_defdoclist":pk_defdoclist,
                                "classid":pk_defdoclist,
                                busifuncode:'all'
                            };
                        }
                    }
                }
            }
            return one;
        })
        // let items = meta[presetVar.list].items;
        // items.map((one) => {
        //     one.itemtype = 'input';
        //     one.refcode=null;
        // });
        // meta[presetVar.list].items=items;
        this.props.meta.setMeta(meta);
        this.props.button.setDisabled({
            AddLine: false,
            DelLine: true,
            Copy: true
        });
    }
}export default fromChangeAfter;
