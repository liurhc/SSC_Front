import {base,cardCache,promptBox,cacheTools,deepClone,getBusinessInfo,toast} from 'nc-lightapp-front';
const {NCMessage} = base;
import cardPageController from '../../../../public/common/cardPageController'
import changeListTemplate from '../../../../public/common/changeListTemplate'
import pubUtil from '../../../../public/common/pubUtil'
import presetVar from '../presetVar'
import listPresetVar from '../../listGlobal/presetVar'
import requestApi from '../requestApi'
let { getCacheById, updateCache,addCache,getCurrentLastId,getNextId,deleteCacheById} = cardCache;
class cardPageControllerChild extends cardPageController{
    constructor(main, props, meta) {
        let newMeta = {
            ...meta, 
            dataSource: (meta || {}).dataSource || listPresetVar.dataSource,
            headId: (meta || {}).headId || presetVar.head,
            bodyId: (meta || {}).bodyId || presetVar.body,
            formId: (meta || {}).formId || presetVar.formId,
            listIds: (meta || {}).listIds || [presetVar.list],
            listButtonAreas: (meta || {}).listButtonAreas || {[presetVar.list]:presetVar.listButtonArea},
            listUrl: (meta || {}).listUrl || presetVar.listURL,
            listPageCode: (meta || {}).listPageCode || presetVar.listPageCode,
            pkField: 'pk_notrecaccrel'

        };
        super(main, props, newMeta);
        this.refpk=(this.cacheData.headRef || {}).refpk;
    }
    /**
     * 初始化模板
     * @param {*} meta 
     */
    initMeta(meta){
        // 影像因素多选
        meta[presetVar.formId].items.map((one)=>{
            if(one.attrcode=='inflfactor'){
                one.isMultiSelectedEnabled = true;
            }
        })
        // 列表页面没有选择账簿类型时本页面科目编码不可编辑
        if(!this.refpk){
            meta[presetVar.formId].items.map((one)=>{
                // if(one.attrcode == 'srcacccode' || one.attrcode == 'desacccode'){
                //     one.disabled=true;
                // } else 
                if(one.attrcode=='inflfactor'){
                    one.isMultiSelectedEnabled = true;
                }
            })
            // meta[presetVar.list].items.map((one)=>{
            //     if(one.attrcode == 'desacccodesp'){
            //         one.disabled=true;
            //     }
            // })
        }
    }
    /**
     * 初始化参照过滤条件
     * @param {*} meta 
     */
    initRefParam(meta){
        let businessInfo = getBusinessInfo();
        let groupId=businessInfo.groupId;
        let c=this.props.getSearchParam("c");
        if(c=='20022331'){
            groupId='GLOBLE00000000000000';
        }
        meta[presetVar.formId].items.map((one)=>{
            if(one.attrcode == 'srcacccode'){
                if(!this.refpk){
                    one.itemtype="input";
                }else{
                    one.isMultiSelectedEnabled=false;
                    one.isDataPowerEnable='Y',
                    // one.DataPowerOperationCode='fi',
                    one.isAccountRefer = false;
                    one.isShowDisabledData=false;
                    one.queryCondition = () => {
                        return {
                            "pk_setofbook": this.cacheData.headRef.refpk,
                            "versiondate": pubUtil.getVersionDateStr(),
                            dateStr: pubUtil.getSysDateStr(),
                            pk_org:groupId,
                            "isDataPowerEnable": 'Y',
                            // "DataPowerOperationCode" : 'fi'
                        };
                    } 
                }
            }
            if(one.attrcode == 'desacccode'){
                if(!this.refpk){
                    one.itemtype="input";
                }else{
                    one.isMultiSelectedEnabled=false;
                    one.isDataPowerEnable='Y',
                    // one.DataPowerOperationCode='fi',
                    one.onlyLeafCanSelect = true;
                    one.isAccountRefer = false;
                    one.isShowDisabledData=false;
                    one.queryCondition = () => {
                        return {
                            "pk_setofbook": this.cacheData.headRef.refpk,
                            "versiondate": pubUtil.getVersionDateStr(),
                            dateStr: pubUtil.getSysDateStr(),
                            pk_org:groupId,
                            "isDataPowerEnable": 'Y',
                            // "DataPowerOperationCode" : 'fi'
                        };
                    } 
                }
            }

        })
        meta[presetVar.list].items.map((one)=>{
            if(one.attrcode == 'desacccodesp'){
                if(!this.refpk){
                    one.itemtype="input";
                    one.refcode=null;
                }else{
                    one.isMultiSelectedEnabled=false;
                    one.isDataPowerEnable='Y',
                    // one.DataPowerOperationCode='fi',
                    one.onlyLeafCanSelect = true;
                    one.isAccountRefer = false;
                    one.isShowDisabledData=false;
                    one.queryCondition = () => {
                        return {
                            "pk_setofbook": this.cacheData.headRef.refpk,
                            "versiondate": pubUtil.getVersionDateStr(),
                            dateStr: pubUtil.getSysDateStr(),
                            pk_org:groupId,
                            "isDataPowerEnable": 'Y',
                            // "DataPowerOperationCode" : 'fi'
                        };
                    }
                }
            }
        })
        this.props.button.setDisabled({
            AddLine: true,
            DelLine: true,
            Copy: true
        });
    }

    Copy(AreaId, data, index){
        this.props.editTable.pasteRow(AreaId, data,index);
    }
    queryData(id){
        requestApi.query({
            data: {pk_notrecaccrel: id, ...pubUtil.getSysCode(this.props)},
            success: (data) => {
                this.cfCallCakckQueryData(data);
            }
        })
    }
    /**
     * 保存动作
     * @param {*data} 页面数据
     */
    doSave(data){
        let inflfactor= this.props.form.getFormItemsValue(presetVar.formId,'inflfactor');
        if(inflfactor){
            if(inflfactor.value){
                let datas = this.props.editTable.getAllRows(presetVar.list);
            let checkData=[];
            if(datas){
                if(datas.length>0){
                    for(let j=0;j<datas.length;j++){
                        checkData.push(datas[''+j]);
                    }
                    let check=this.props.editTable.checkRequired(presetVar.list,checkData);
                    if(check){
                        requestApi.save({
                            props: this.props,
                            data: {...data, ['userjson']: JSON.stringify(pubUtil.getSysCode(this.props))},
                            success: (data) => {
                                this.callBackSave(data);
                            }
                        })
                    }
                }else{
                    requestApi.save({
                        props: this.props,
                        data: {...data, ['userjson']: JSON.stringify(pubUtil.getSysCode(this.props))},
                        success: (data) => {
                            this.callBackSave(data);
                        }
                    })    
                }
            }
            }else{
                requestApi.save({
                    props: this.props,
                    data: {...data, ['userjson']: JSON.stringify(pubUtil.getSysCode(this.props))},
                    success: (data) => {
                        this.callBackSave(data);
                    }
                })
            }
        }else{
            requestApi.save({
                props: this.props,
                data: {...data, ['userjson']: JSON.stringify(pubUtil.getSysCode(this.props))},
                success: (data) => {
                    this.callBackSave(data);
                }
            }) 
        }
    }
    /**
     * 影像因素不选择不可以增行
     */
    AddLine(){
        if(this.props.form.getFormItemsValue(presetVar.formId, 'inflfactor').value){
            let meta=this.props.meta.getMeta();
            this.props.editTable.addRow(presetVar.list);
        }else{
            let multiLang = this.props.MutiInit.getIntl(2002); 
            // "2002DFKMDZGXSZ-0003": "请先选择影响因素",
            toast({color:"warning",content:this.main.state.json['20022311-000005']});
        }
    }

    // 自定义函数--------------------------------------------------------------
    /**
     * 更新表体模板
     * @param {*} data 
     */
    cfChangeListTemplate(data){
        changeListTemplate(this.props, presetVar.list, data, ['numberindex'], ['pk_notrecaccrelsp','desaccpksp','desacccodesp','desaccnamesp','ts','opr']);
    }
    cfCallCakckQueryData(data){
        if(data.data.template != null){
            this.listIds.map((one)=>{
                if(data.data.template.areas[one] != null){
                    this.cfChangeListTemplate(data.data.template.areas[one]);
                }
            })
        }
        this.setPageData(data);
        if(data.data){
            let value=data.data[presetVar.formId][presetVar.formId].rows[0].values;
            if(value.inflfactor.value){
                this.props.button.setDisabled({
                    AddLine: false
                });
            }else{
                this.props.button.setDisabled({
                    AddLine: true
                });
            }
        }
    }

    Delete(){
        let pk_notrecaccrel=this.props.form.getFormItemsValue(presetVar.formId, 'pk_notrecaccrel').value;
        promptBox({
            color:"warning",
            title: this.main.state.json['20022311-000000'],/* 国际化处理： 删除*/
                content: this.main.state.json['20022311-000001'],/* 国际化处理： 确认要删除吗？*/
            beSureBtnClick: this.doDelete.bind(this)
        })
        
        
    }
    doDelete(){
        let pk_notrecaccrel=this.props.form.getFormItemsValue(presetVar.formId, 'pk_notrecaccrel').value;
        requestApi.delete({
            data: {pk_notrecaccrel: pk_notrecaccrel, ...pubUtil.getSysCode(this.props)},
            success: (data) => {
                let nextId = getNextId(pk_notrecaccrel, this.dataSource);
                if(nextId){
                    requestApi.query({
                        data: {pk_notrecaccrel: nextId, ...pubUtil.getSysCode(this.props)},
                        success: (data) => {
                            this.cfCallCakckQueryData(data);
                        }
                    })
                }else{
                    this.setPageData(null);
                }
                
            }
        })
    }
/**
     * 页面状态变更处理
     * @param {*} state 
     */
    setPageState(state){
        super.setPageState(state);
        switch(state){
            case 'browse':
            this.setButtonStatusVisible(["DelLine","CopyToEnd","CancelTable"],false);
            break;
            case 'add':
            if(this.copyFlag==true){

            }else{

            }
            this.setButtonStatusVisible(["DelLine"],true);
            this.setButtonStatusVisible(["CancelTable","CopyToEnd"],false);
            break;
            case 'edit':
            this.setButtonStatusVisible(["DelLine"],true);
            this.setButtonStatusVisible(["CancelTable","CopyToEnd"],false);
            break;
        }
    }

     /**
     * 取得当前条显示按钮
     * @param {*} listAreaId 
     * @param {*} text 
     * @param {*} record 
     * @param {*} index 
     */
    getListButtons(listAreaId, text, record, index){
        if(this.main.state.copyFlag==true){
            return ['DelLine','Copy','insertLine'];
        }else{
            return ['CopyToThis'];
        }
    }

    /**
     * 行复制事件
     * @param {*} AreaId 
     * @param {*} data 
     * @param {*} index 
     */
    Copy(AreaId, data, index){
        this.props.editTable.pasteRow(AreaId, data,index);
        this.props.editTable.setValByKeyAndIndex(AreaId, index+1, "pk_notrecaccrelsp", { value:'', display:''})

    }

    CopyToThis(AreaId, data, index){
        let  cachedata = cacheTools.get('copyData');
        let clone = deepClone(cachedata);
        // let number=this.props.editTable.getNumberOfRows(AreaId);
        let datas=[];
        if(clone!=null){
            for(let i=0;i<clone.length;i++){
                clone[i].data.values.pk_appdetail={display:null,value:null};
                datas.push({values:clone[i].data.values});
            }
        }
        this.props.editTable.insertRowsAfterIndex(AreaId, datas,index);
    }

    /**
     *复制按钮事件
     */
    CopyAll(){
        let selectRows=this.props.editTable.getCheckedRows(presetVar.list);
        if(selectRows.length>0){
            cacheTools.set('copyData', selectRows);
            this.main.state.copyFlag=false;
            this.setButtonStatusVisible(["CopyToEnd","CancelTable"],true);
            this.setButtonStatusVisible(["AddLine","DelLine","Copy"],false);
            this.props.meta.setMeta(this.props.meta.getMeta());
        }
    }

    CancelTable(){
        this.main.state.copyFlag=true;
        this.setButtonStatusVisible(["AddLine","DelLine","Copy"],true);
        this.setButtonStatusVisible(["CopyToEnd","CancelTable"],false);
        this.props.meta.setMeta(this.props.meta.getMeta());
    }

    selectedChange(){
        let selectRows=this.props.editTable.getCheckedRows(presetVar.list);
        if(selectRows && selectRows.length>0){
            this.setButtonStatus(['DelLine',"Copy"],false);
        }else{
            this.setButtonStatus(['DelLine',"Copy"],true);
        }     
    }

    Cancel(){
        let status=this.props.getUrlParam('status');
        if(status=='edit'){
            let currId=this.props.getUrlParam('id');
            let cardData = getCacheById(currId, listPresetVar.dataSource);
            if(currId){
                this.props.setUrlParam({id:currId});
                if(cardData){
                    if(cardData != null && cardData.data != null){
                        this.props.form.setAllFormValue({[presetVar.formId]: cardData.data.head[presetVar.formId] || {rows:[]}});
                        if(nextCache.data.body != null){
                            this.props.editTable.setTableData(presetVar.list, cardData.data.body[presetVar.list] || {rows:[]});
                        }else{
                            this.props.editTable.setTableData(presetVar.list, {rows:[]});
                        }
                    }else{
                        this.props.form.EmptyAllFormValue(presetVar.formId); 
                    }
                }else{
                    this.queryData(currId);
                }
            }else{
                this.props.form.EmptyAllFormValue(presetVar.formId); 
                this.props.editTable.setTableData(presetVar.list, {rows:[]});
            }
        }else{
            let id = getCurrentLastId(listPresetVar.dataSource);
            let cardData = getCacheById(id, listPresetVar.dataSource);
            if(id){
                this.props.setUrlParam({id:id});
                if(cardData){
                    if(cardData != null && cardData.data != null){
                        this.props.form.setAllFormValue({[presetVar.formId]: cardData.data.head[presetVar.formId] || {rows:[]}});
                        if(nextCache.data.body != null){
                            this.props.editTable.setTableData(presetVar.list, cardData.data.body[presetVar.list] || {rows:[]});
                        }else{
                            this.props.editTable.setTableData(presetVar.list, {rows:[]});
                        }
                    }else{
                        this.props.form.EmptyAllFormValue(presetVar.formId); 
                    }
                }else{
                    this.queryData(id);
                }
            }else{
                this.props.form.EmptyAllFormValue(presetVar.formId); 
                this.props.editTable.setTableData(presetVar.list, {rows:[]});
            }
        }
        this.setPageState("browse");
        this.props.setUrlParam({status:'browse'})
    }
}export default cardPageControllerChild;
