import changeListTemplate from '../../../../public/common/changeListTemplate'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
class listChangeAfter{
    constructor(main, props) {
        this.main = main;
        this.props = props;
    }
    /**
     * 表头变更事件
     * @param {*} moduleId 
     * @param {*} key 
     * @param {*} value 
     */
    changeAfter(moduleId, key, value, changedrows, index, record){
        let code=value.refcode;
        if(!value.hasOwnProperty('refcode')){
            code=value.code;
        }
        if(this.main.cardPageControllerChild.refpk){
            // 列表编辑后事件
            switch(key){
                // 目的科目编码
                case presetVar.listField.desacccode:
                    // window.setTimeout(()=>{
                        // 带出目的科目主键
                        this.props.editTable.setValByKeyAndIndex(presetVar.list, index, presetVar.listField.desaccpksp, {
                            value:value.refpk,
                            display:value.refpk
                        });
                        // 带出目的科目编码
                        this.props.editTable.setValByKeyAndIndex(presetVar.list, index, presetVar.listField.desacccode, {
                            value:code,
                            display:code
                        });
                        // 带出目的科目名称
                        this.props.editTable.setValByKeyAndIndex(presetVar.list, index, presetVar.listField.desaccname, {
                            value:value.refname,
                            display:value.refname
                        });
                    // },10)
                break;
            }
        }
    }
}export default listChangeAfter;
