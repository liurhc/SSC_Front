import cardPageControllerGlobal from '../../cardGlobal/events/cardPageControllerChild'
class cardPageControllerChild extends cardPageControllerGlobal{
    constructor(main, props, meta) {
        super(main, props, {
            // listUrl: '/gl/gl_coordination/notrecaccrelationset/listGroup/index.html',
            listUrl: 'listGroup',
            listPageCode: '20022312_NOTRLIST'
        });
    }
    /**
     * 页面状态变更处理
     * @param {*} state 
     */
    setPageState(state){
        super.setPageState(state);
        if(this.props.getUrlParam('from_globle') == 'Y'){
            this.props.button.setButtonVisible(['Edit'], false);
        }
        switch(state){
            case 'browse':
            this.setButtonStatusVisible(["DelLine","CopyToEnd","CancelTable"],false);
            break;
            case 'add':
            this.setButtonStatusVisible(["DelLine"],true);
            this.setButtonStatusVisible(["CancelTable","CopyToEnd"],false);
            break;
            case 'edit':
            this.setButtonStatusVisible(["DelLine"],true);
            this.setButtonStatusVisible(["CancelTable","CopyToEnd"],false);
            break;
        }
    }
    
}export default cardPageControllerChild;    
