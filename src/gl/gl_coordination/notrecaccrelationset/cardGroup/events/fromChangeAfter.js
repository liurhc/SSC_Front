import fromChangeAfterGolbal from '../../cardGlobal/events/fromChangeAfter'
import changeListTemplate from '../../../../public/common/changeListTemplate'
import {getBusinessInfo } from 'nc-lightapp-front';
import presetVar from '../../cardGlobal/presetVar'
class fromChangeAfter extends fromChangeAfterGolbal{
    constructor(main, props){
        super(main, props);
    }

    /**
     * 更新表体模板
     * @param {*} data 
     */
    cfChangeListTemplate(data,value){
        let meta=changeListTemplate(this.props, presetVar.list, data, ['numberindex'], ['pk_notrecaccrelsp','desaccpksp','desacccodesp','desaccnamesp','ts','opr']);
        meta[presetVar.list].items.map((one)=>{
            if(one.attrcode.indexOf('fac')!=-1){
                one.fieldDisplayed='refname';
                if(one.itemtype=="refer"){
                    let pk_defdoclist='';
                    if(value){
                        let values=value.value.split(',');
                        for(let i=0;i<values.length;i++){
                            if(value[''+i].values){
                                if(value[''+i].refname==one.label){
                                    if(value[''+i].values.entityid.value){
                                        pk_defdoclist=value[''+i].values.entityid.value;
                                    }
                                }
                            }
                        }
                        let businessInfo = getBusinessInfo();
                        one.queryCondition = () => {
                            return {
                                "pk_org":businessInfo.groupId,
                                "isDataPowerEnable": 'Y',
                                "DataPowerOperationCode" : 'fi',
                                "isShowUnit":true,
                                "pk_defdoclist":pk_defdoclist,
                                "classid":pk_defdoclist,
                                busifuncode:'all'
                            };
                        }
                    }
                }
            }
        })
        this.props.meta.setMeta(meta);
        this.props.button.setDisabled({
            AddLine: false,
            DelLine: true,
            Copy: true
        });
    }
}export default fromChangeAfter;
