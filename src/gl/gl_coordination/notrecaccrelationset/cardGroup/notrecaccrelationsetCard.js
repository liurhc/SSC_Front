import {Component} from 'react';
import {createPage, base,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const {NCBackBtn} = base;
import cardPageControllerChild from './events/cardPageControllerChild'
import fromChangeAfter from './events/fromChangeAfter'
import listChangeAfter from './events/listChangeAfter'
import presetVar from '../cardGlobal/presetVar'
import listPresetVar from '../listGlobal/presetVar'
import './index.less';
import {CardLayout, Form, Table, ModalArea,TopArea} from '../../../../fipub/public/components/layout/CardLayout';
import {Header, HeaderButtonArea} from '../../../../fipub/public/components/layout/Header';


class NotrecaccrelationsetCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paginationShowClassName: '',
            copyFlag:true,
            json:{}
        };
        // 页面初始化
        this.cardPageControllerChild = new cardPageControllerChild(this, props);
        this.fromChangeAfter = new fromChangeAfter(this, props);
        this.listChangeAfter = new listChangeAfter(this, props);
    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
           window.onbeforeunload = () => {
            let status = this.props.getUrlParam("status");
            if (status == 'edit' || status == 'add') {
                return this.state.json['20022311-000002']/* 国际化处理： 确定要离开吗？*/
            }
        }
    }
    render() {
        const {editTable, search, button, cardPagination, modal, form} = this.props;
        const {createModal} = modal;
        const {createForm} = form;
        
        const { createCardPagination } = cardPagination;
        const {createEditTable} = editTable;
        const {createButtonApp} = button;
        let status=this.props.getUrlParam('status');
        let getCreateCardPagination=()=>{
            // if(this.props.getUrlParam('open_status') == 'browse' || this.props.getUrlParam('open_status') == 'edit'){
                if(this.props.getUrlParam('status') == 'browse'){
                return(
                    <div className={'header-cardPagination-area ' + this.state.paginationShowClassName} style={{float:'right'}}>
                    {createCardPagination({
                        dataSource: listPresetVar.dataSource,
                        handlePageInfoChange:(props,data)=>{this.cardPageControllerChild.pageInfoClick(data)}
                    })}
                </div>
                )
            }
        }
        return (
            <CardLayout>
                <TopArea>
                <Header 
                    showGoback={status=='browse'}
                    onBackClick={()=>{this.cardPageControllerChild.backButtonClick()}}
                >
                    <HeaderButtonArea>
                        {createButtonApp({
                            area: presetVar.headButtonArea, 
                            buttonLimit: 3,
                            onButtonClick: (props, actionId)=>{this.cardPageControllerChild.headButtonClick(actionId)}
                        })}
                    </HeaderButtonArea>
                    {getCreateCardPagination()}
                </Header>
                <Form>
                    {createForm(presetVar.formId, {
                        onAfterEvent: (props, moduleId, key, value, a, valueA)=>{this.fromChangeAfter.changeAfter(moduleId, key, value, valueA)}
                    })}
                </Form>
                </TopArea>
                <Table>
                <div className='header table-header'>
                    <div className='btn-group table-button-type'>
                            {createButtonApp({
                                area: presetVar.listShoulderButtonArea, 
                                buttonLimit: 3,
                                onButtonClick: (props, actionId)=>{this.cardPageControllerChild.headButtonClick(actionId)}
                            })}
                        </div>
                    </div>
                    {createEditTable(presetVar.list, {
                        dataSource: presetVar.dataSource,
                        onAfterEvent: (props, moduleId, key, value, changedrows, index, record)=>{this.listChangeAfter.changeAfter(moduleId, key, value, changedrows, index, record)},
                        showCheck: true,
                        selectedChange:(data)=>{this.cardPageControllerChild.selectedChange(data)},
                        showIndex: true
                    })}
                </Table>
            </CardLayout>
        )
    }
}
NotrecaccrelationsetCard = createPage({})(NotrecaccrelationsetCard);
export default NotrecaccrelationsetCard;
