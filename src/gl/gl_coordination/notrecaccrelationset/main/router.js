import { asyncComponent } from 'nc-lightapp-front';

const NotrecaccrelationsetListGlobal = asyncComponent(() => import(/* webpackChunkName: "gl/gl_coordination/notrecaccrelationset/listGlobal/listGlobal" */ /* webpackMode: "eager" */ '../listGlobal/notrecaccrelationsetList'));
const NotrecaccrelationsetCardGlobal = asyncComponent(() => import(/* webpackChunkName: "gl/gl_coordination/notrecaccrelationset/cardGlobal/cardGlobal" */ /* webpackMode: "eager" */ '../cardGlobal/notrecaccrelationsetCard'));
const NotrecaccrelationsetListGroup = asyncComponent(() => import(/* webpackChunkName: "gl/gl_coordination/notrecaccrelationset/listGroup/listGroup" */ /* webpackMode: "eager" */ '../listGroup/notrecaccrelationsetList'));
const NotrecaccrelationsetCardGroup = asyncComponent(() => import(/* webpackChunkName: "gl/gl_coordination/notrecaccrelationset/cardGroup/cardGroup" */ /* webpackMode: "eager" */ '../cardGroup/notrecaccrelationsetCard'));


const routes = [
	{
		path: '/listGlobal',
		component: NotrecaccrelationsetListGlobal
	},
	{
		path: '/cardGlobal',
		component: NotrecaccrelationsetCardGlobal
	},
	{
		path: '/listGroup',
		component: NotrecaccrelationsetListGroup
	},
	{
		path: '/cardGroup',
		component: NotrecaccrelationsetCardGroup
	}

];

export default routes;
