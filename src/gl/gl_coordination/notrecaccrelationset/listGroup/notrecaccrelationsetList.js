import {Component} from 'react';
import {createPage,getMultiLang,createPageIcon} from 'nc-lightapp-front';
import SetOfBookGridRef from '../../../../uapbd/refer/org/SetOfBookGridRef';
import simpleTablePageControllerChild from './events/simpleTablePageControllerChild';
import presetVar from '../listGlobal/presetVar'
import {ListLayout, Table} from '../../../../fipub/public/components/layout/ListLayout';
import {TopArea} from '../../../../fipub/public/components/layout/CardLayout';
import {Header, HeaderButtonArea, HeaderSearchArea} from '../../../../fipub/public/components/layout/Header'
import './index.less';

class NotrecaccrelationsetList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            headRef: {},
            json:{}
        };
        // 页面初始化
        this.simpleTablePageControllerChild = new simpleTablePageControllerChild(this, props);
    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
    }
    render() {
        const {table, search, button} = this.props;
        const {createSimpleTable} = table;
        const {createButtonApp} = button;

        return (
            <ListLayout>
                <TopArea>
                <Header>
                    <HeaderSearchArea>
                        <SetOfBookGridRef
                            fieldid='sefofbook_ref'
                            value={this.state.headRef}
                            onChange={(data)=>{this.simpleTablePageControllerChild.cfHeadRefChange(data)}}
                        />
                    </HeaderSearchArea>
                    <HeaderButtonArea>
                        {createButtonApp({
                            area: presetVar.headButtonArea, 
                            buttonLimit: 3,
                            onButtonClick: (props, actionId)=>{this.simpleTablePageControllerChild.headButtonClick(actionId)}
                        })}
                    </HeaderButtonArea>
                </Header>
                </TopArea>
                <Table>
                    {createSimpleTable(presetVar.list, {
                        dataSource: presetVar.dataSource,
                        onRowDoubleClick: (record, index)=>{this.simpleTablePageControllerChild.listRowDoubleClick(record, index)},
                        showCheck: false,
                        showIndex: true
                    })}
                </Table>
            </ListLayout>
        )
    }
}
NotrecaccrelationsetList = createPage({})(NotrecaccrelationsetList);
export default NotrecaccrelationsetList;
