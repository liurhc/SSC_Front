import simpleTablePageControllerClobal from '../../listGlobal/events/simpleTablePageControllerChild'
class simpleTablePageControllerChild extends simpleTablePageControllerClobal{
    constructor(main, props) {
        super(main, props, {
            // cardUrl: '/gl/gl_coordination/notrecaccrelationset/cardGroup/index.html',
            cardUrl: '/cardGroup',
            cardPageCode: '20022312_NOTRCARD'
        });
        if(this.cacheData.headRef){
            this.main.state.headRef=this.cacheData.headRef;
        }
    }
    /**
     * 取得表格行按钮
     * @param {*} text 
     * @param {*} record 
     * @param {*} index 
     */
    getListButtons(text, record, index){
        if((record.pk_org || {}).value == 'GLOBLE00000000000000'){
            return [];
        }else{
            return ['Edit', 'DelLine'];
        }
    }
    /**
     * 双击表体行页面跳转增加参数
     */
    AddListRowDoubleClickParam(record, index){
        if((record.pk_org || {}).value == 'GLOBLE00000000000000'){
            return {from_globle: 'Y'};
        }else{
            return {};
        }
    }
}
export default simpleTablePageControllerChild;
