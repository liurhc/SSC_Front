/**页面全局变量 */
let currentVar = {
    list: 'influecefactorlist',
    searchArea: 'search_Area',
    headButtonArea: 'list_head',
    listButtonArea: 'list_inner',
}
window.presetVar = {
    ...currentVar
};
export default currentVar
