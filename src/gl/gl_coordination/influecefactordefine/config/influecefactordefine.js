import {Component} from 'react';
import {createPage,getMultiLang,createPageIcon} from 'nc-lightapp-front';
import editTablePageControllerChild from './events/editTablePageControllerChild'
import presetVar from './presetVar'
import {Header, HeaderButtonArea} from '../../../../fipub/public/components/layout/Header'
import './index.less';

class Influecefactordefine extends Component {
    constructor(props) {
        super(props);
        this.state = {
            /**
             * 查询区显示控制
             */
            searchArea_disabled: '',
            json:{},
            inlt:null
        };
        this.editTablePageControllerChild = new editTablePageControllerChild(this, props);
    }
    selectedChange=(props,newVal,oldVal)=>{//选中行发生变化
        if(oldVal != 0){
            props.button.setButtonDisabled(['Delete'], false)
        }else{
            props.button.setButtonDisabled(['Delete'], true)
        }
    }
    componentWillMount() {
        let callback= (json,status,inlt) =>{
			this.setState({json:json,inlt},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
           window.onbeforeunload = () => {
               let status=this.props.editTable.getStatus(presetVar.list);
            if (status == 'edit' || status == 'add') {
                return this.state.json['20022310-000002']/* 国际化处理： 确定要离开吗？*/
            }
        }
    }
    render() {
        const {editTable, search, button} = this.props;
        const {createEditTable} = editTable;
        const {createButtonApp} = button;
        const {NCCreateSearch} = search;

        let getSearchAreaDispalyClassName = () => {
            if(this.state.searchArea_disabled){
                return 'hide';
            }else{
                return 'show';
            }
        }
        return (
            <div id="yxysdy" className="nc-bill-list nc-single-table">
                <Header>
                    <HeaderButtonArea>
                        {createButtonApp({
                            area: presetVar.headButtonArea, 
                            buttonLimit: 3,
                            onButtonClick: (props, actionId)=>{this.editTablePageControllerChild.headButtonClick(actionId)}
                        })}                        
                    </HeaderButtonArea>
                </Header>
                <div className={'nc-bill-search-area nc-singleTable-search-area ' + getSearchAreaDispalyClassName()}>
                    {NCCreateSearch(
                        presetVar.searchArea,//模块id
                        {
                            clickSearchBtn: (props, data, type, queryInfo)=>{this.editTablePageControllerChild.queryCondition(data)},//   点击查询按钮事件
                            defaultConditionsNum:4, //默认显示几个查询条件
                            showAdvBtn: true
                        }
                    )}
                </div>
                <div className='nc-bill-table-area nc-singleTable-table-area'>
                    {createEditTable(presetVar.list, {
                        adaptionHeight:true,
                        onAfterEvent:this.editTablePageControllerChild.afterEditEvent.bind(this),
                        showIndex:true,
                        showCheck:true,
                        selectedChange:this.selectedChange.bind(this)
                    })}
                </div>
            </div>
        )
    }
}
Influecefactordefine = createPage({})(Influecefactordefine);
export default Influecefactordefine;
