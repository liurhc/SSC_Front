import pubUtils from '../../../public/common/pubUtil'
import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    // 查询接口
    query: (opt) => {
        let paramData = {queryconditions:opt.data, ...pubUtils.getSysCode(opt.props)};
        ajax({
            url: '/nccloud/gl/influecefactor/influecefactordataquery.do',
            data: paramData,
            success: opt.success
        });
    },
    // 保存
    save: (opt) => {
        ajax({
            url: '/nccloud/gl/influecefactor/save.do',
            data: {...opt.data, ...{['userjson']: JSON.stringify(pubUtils.getSysCode(opt.props))}},
            success: opt.success
        });
    },
    // 删除
    delete: (opt) => {
        ajax({
            url: '/nccloud/gl/nbjyxt/yxysdy/DeleteAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 批量删除
    deleteAll: (opt) => {
        ajax({
            url: '/nccloud/gl/influecefactor/save.do',
            data: {...opt.data, ...{['userjson']: JSON.stringify(pubUtils.getSysCode(opt.props))}},
            success: opt.success
        });
    }
}

export default  requestApiOverwrite;
