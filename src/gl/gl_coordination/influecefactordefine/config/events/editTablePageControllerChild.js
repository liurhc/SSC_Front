import editTablePageController from '../../../../public/common/editTablePageController'
import pubUtils from '../../../../public/common/pubUtil'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
import {toast} from 'nc-lightapp-front';
class editTablePageControllerChild extends editTablePageController{
    constructor(main, props, meta) {
        let newMeta = {
            ...meta,
            headRefDisabled: (meta || {}).headRefDisabled || 'searchArea_disabled',
            listIds: (meta || {}).listIds || [presetVar.list],
            listButtonAreas: (meta || {}).listButtonAreas || {[presetVar.list]:presetVar.listButtonArea}
        };
        super(main, props, newMeta);
    }
	
	/**
     * 设置参照过滤
     * @param {*} meta 
     */
    initRefParam(meta){
        meta[presetVar.list].items.map((one)=>{
            one.width=150;
            if(one.attrcode == "detailpath"){
                one.itemtype = 'refer';
				one.refcode = 'uap/refer/riart/mdTreeByIdRef/index.js';
                one.queryCondition = {"beanId":"d0a97642-327d-43f3-8f90-bca3e91edbd3","isDataPowerEnable": 'Y',
                "DataPowerOperationCode" : 'fi'};
                one.isTreelazyLoad = true ;
            }
            if(one.attrcode == "entityid"){
                one.isMultiSelectedEnabled = false;
            }
            if(one.attrcode == "jointype"){
                one.initialvalue={display:this.main.state.json['20022310-000000'],value:"1"};/* 国际化处理： 等于*/
            }
        })
        meta[presetVar.searchArea].items.map((data)=>{
            if(data.itemtype == "refer"){
                if(data.fieldDisplayed == "code"){
                    data.fieldDisplayed = "refcode";
                }else if(data.fieldDisplayed == "name"){
                    data.fieldDisplayed = "refname";
                }
            }
            if(data.attrcode=='entityid' || data.attrcode=='creator'){
                data.isMultiSelectedEnabled=true;  
            }
        })
    }


    getListButtons(text, record, index){
        let buttonAry =["DelLine"];
        let status=null;
        this.listIds.map((one)=>{
            status=this.props.editTable.getStatus(one);
        })
        if(record.values.factorType){
            let factorType=record.values.factorType.value;
            if(factorType=="false"){
                buttonAry=[""];
            }
        }
        return buttonAry;
    }


    /**
     * 查询数据
     */
    queryData(queryKey){
        queryKey={
            conditions:[],
            logic:"and"
        }
        requestApi.query({
            props: this.props,
            data: queryKey,
            success: (data) => {
                this.setTableData(data);
            }
        })
    }
    /**
     * 查询数据
     */
    queryCondition(queryKey){
        requestApi.query({
            props: this.props,
            data: queryKey,
            success: (data) => {
                this.setTableData(data);
                if(data.data){
                    // toast({color:"success",content:"查询成功，共"+data[presetVar.list].rows.length+"条"});
                    toast({color:"success",content:this.main.state.inlt && this.main.state.inlt.get('20022310-000004',{count : data.data[presetVar.list].rows.length})});
                    
                }else{
                    toast({color:"warning",content:this.main.state.json['20022310-000005']/* 国际化处理： 未查询出符合条件的数据*/})
                }
            }
        })
    }
    /**
     * 保存处理
     * @param {*} data 
     */
    doSave(data){
        let allRows=this.props.editTable.getAllRows(presetVar.list);
        let allRow=[];
        if(null != allRows && allRows.length>0){
            for(let i=0;i<allRows.length;i++){
                allRow.push(allRows[''+i]);
            }
        }
        let flag=this.props.editTable.checkRequired(presetVar.list, allRow);
        if(flag){
            requestApi.save({
                props: this.props,
                data: data,
                success: (data) => {
                    this.callBackSave(data);
                }
            })
        }
    }
    /**
     * 删行处理
     * @param {*} data 
     */
    doDelLine(AreaId, data, index){
        requestApi.save({
            props: this.props,
            data: data,
            success: (data) => {
                this.callBAckDelLine(AreaId, data, index);
            }
        })
    }
    /**
     * 批量删行处理
     * @param {*} data 
     */
    doDelete(AreaId, data, index){

        requestApi.save({
            props: this.props,
            data: data,
            success: (data) => {
                this.callBAckDelete(AreaId, data, index);
            }
        })
    }
    setUnAbleData(){
        let globalTable=null;
        let index=[];
        this.listIds.map((one)=>{
            globalTable=this.props.editTable.getFilterRows(one, 'factorType', 'false');
        })
        let row=globalTable.rows;
        if(row.length>0){
            for(let i=0;i<row.length;i++){
                index.push(row[i].flterIndex);
            }
        }
        this.listIds.map((one)=>{
            this.props.editTable.setEditableRowByIndex(one,index, false);
        })
    }

    afterEditEvent(props, moduleId, key, value, changedrows, index, record) {
        switch(key){
            case "detailpath":
                props.editTable.setValByKeyAndIndex(presetVar.list, index, "detailpath", {
                    display:value.refname
                });
            break;
            case "entityid":
                props.editTable.setValByKeyAndIndex(presetVar.list, index, "entityid", {
                    display:value.refname
                });
            break;
        }
    }

    /**
     * 修改状态
     * @param {*} state 
     */
    setPageState(state){
        switch(state){
            case 'browse':
                this.props.button.setButtonVisible(this.editButtons, false);
                this.props.button.setButtonVisible(this.browseButtons, true);
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'browse');
                })
                this.props.button.setPopContent('DelLine',this.main.state.json['20022310-000001']);/* 国际化处理： 确定要删除吗？*/
                if(this.treeId){
                    this.props.syncTree.setNodeDisable(this.treeId, false);
                }
                if(this.headRefDisabled){
                    this.main.setState({[this.headRefDisabled]:false});
                }
                this.props.button.setButtonDisabled(['Delete'],true);
                this.props.button.setMainButton('Save',false);
                this.props.button.setMainButton('Add',true);
            break;
            case 'add':
                this.props.button.setButtonVisible(this.browseButtons, false);
                this.props.button.setButtonVisible(this.editButtons, true);
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'add');
                })
                this.props.button.setPopContent('DelLine');
                if(this.treeId){
                    this.props.syncTree.setNodeDisable(this.treeId, true);
                }
                if(this.headRefDisabled){
                    this.main.setState({[this.headRefDisabled]:true});
                }
                this.props.button.setButtonDisabled(['Delete'],true);
                this.setUnAbleData();
                this.props.button.setMainButton('Save',true);
                this.props.button.setMainButton('Add',false);
            break;
            case 'edit':
                this.props.button.setButtonVisible(this.browseButtons, false);
                this.props.button.setButtonVisible(this.editButtons, true);
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'edit');
                })
                this.props.button.setPopContent('DelLine');
                if(this.treeId){
                    this.props.syncTree.setNodeDisable(this.treeId, true);
                }
                if(this.headRefDisabled){
                    this.main.setState({[this.headRefDisabled]:true});
                }
                //设置不修改数据   
                this.props.button.setButtonDisabled(['Delete'],true);
                this.setUnAbleData();     
                this.props.button.setMainButton('Save',true);
                this.props.button.setMainButton('Add',false);
            break;
        }
    }
}
export default editTablePageControllerChild;
