import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ajax, base, high, createPage, cardCache, getMultiLang,toast } from 'nc-lightapp-front';
let { setDefData, getDefData } = cardCache;
const { Refer } = high;
import createScript from '../../../public/components/uapRefer.js';
import VoucherTypeDefaultGridRef from '../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef';
const { NCButton, NCButtonGroup, NCMenu: Menu, NCDropdown: Dropdown, NCRow, NCCol, NCForm,
    NCFormControl, NCCheckbox, NCDatePicker, NCIcon,NCDiv } = base;
const NCFormItem = NCForm.NCFormItem;
const { NCRangePicker } = NCDatePicker;
import './index.less';
class SearchUnConfirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            MainDataUnConfirmSe:[],
            pk_org: '',//账簿对应的组织
            pk_vouchertype: { display: '', value: '' },//账簿 对应的凭证类别
            queryCondition: [],//未确认的查询条件
            showMore: false,
            tabState: '1',
            searchData: {
                // begindate:'',
                // enddate:'',
                isselfquery: 'N',
                state: '1',
                isselfchecked: 'N',//已审核
                isselftallyed: 'N',//已记账
                pk_orgbook_self: {
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
                pk_orgbook_other: [],
                selfpreparedatestart: '',//本方制单日期开始
                selfpreparedateend: '',//本方制单日期结束
                reconciledatestart: '', //协同开始日期
                reconciledateend: '',//协同结束日期
                pk_othervouchertype: {
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
                othercodestart: '',//
                othercodeend: '',
                otherpreparer: '',
                otherpreparedatestart: '',//对方制单日期开始
                otherpreparedateend: '',//对方制单日期结束
                pk_selfvouchertype: {//凭证类别
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
                selfcodestart: '',//开始凭证号
                selfcodeend: '',//结束凭证号
                selfpreparer: {//制单人
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
                reconcilemaker: {//协同人
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
            }
        }
    }
    componentWillMount() {
        let callback = (json) => {
            this.setState({ json: json }, () => {  })
        }
        getMultiLang({ moduleId: '2002130210', domainName: 'gl', currentLocale: 'simpchn', callback });
    }
    componentDidMount() {
        let searchData = getDefData(11111, 'gl_obervision.discount.stateUNConfirm');
        if (searchData) {
            this.state = searchData;
            this.setState({
                searchData: searchData.searchData,
                MainDataUnConfirmSe: searchData.MainDataUnConfirmSe
            })
            this.handleSearch(searchData.MainDataUnConfirmSe);
            return;
        }
        let self = this;
        if (self.props.getUrlParam('detail')) {
            let { queryCondition } = this.state;
            let url = "/nccloud/gl/reconcile/reconciledataquery.do";
            let data = {
                detail: self.props.getUrlParam('detail'),
                state: '1'
            }
            //setDefData(1234, 'gl_coord.searchData.unConfirm', data);
            ajax({
                url,
                data,
                success: function (response) {
                    const { data, error, success } = response;
                    if (success && data) {
                        let searchData = self.state.searchData;
                        self.props.getMainDataUnConfirm(data, queryCondition)
                        searchData.pk_orgbook_self.refname = data[0].opp_bookname;
                        searchData.pk_orgbook_self.refpk = data[0].opp_accountingbook;
                        self.setState({
                            searchData
                        })
                    } else {
                        self.props.getMainDataUnConfirm([], queryCondition)
                    }
                }
            });
        }else{
            //获取默认账簿
            let url = '/nccloud/platform/appregister/queryappcontext.do';
            let data = {
                appcode: this.props.getSearchParam('c')
            }
            ajax({
                url,
                data,
                success: function (response) {
                    const { data, error, success } = response;
                    if (success && data) {
                        let searchData = self.state.searchData;
                        searchData.pk_orgbook_self.refname = data.defaultAccbookName;
                        searchData.pk_orgbook_self.refpk = data.defaultAccbookPk;
                        // searchData.pk_orgbook_other.refname=data.defaultAccbookName;
                        // searchData.pk_orgbook_other.refpk=data.defaultAccbookPk;
                        self.getCombineInfo(searchData.pk_orgbook_self)
                        self.setState({
                            searchData
                        })
                    } else {

                    }
                }
            });
        }
    }
    componentWillReceiveProps(nextProps) {
        let tabState = nextProps.tabState;
        let MainDataUnConfirmse = nextProps.MainDataUnConfirmse;
        if (MainDataUnConfirmse&&MainDataUnConfirmse.length) {
            this.setState({
                MainDataUnConfirmse:MainDataUnConfirmse
            })
            this.state.MainDataUnConfirmSe = MainDataUnConfirmse;
            setDefData(11111, 'gl_obervision.discount.stateUNConfirm', this.state);
        }
        let reHandleSearchUnconfirm = nextProps.reHandleSearchUnconfirm;
        if (reHandleSearchUnconfirm) {
            this.handleSearch([])
        }
        this.setState({
            tabState
        })
    }
    onStartChange(d) {
        // this.setState({
        // 	begindate:d
        // })
    }
    handleSearch = (mainSe) => {
        let searchData = JSON.parse(JSON.stringify(this.state.searchData));
        if (mainSe && !mainSe.length) {
            if (!searchData.pk_orgbook_self.refpk) {
                toast({ content: this.state.json['2002130210-000049'], color: 'warning' });/* 国际化处理： 请选择核算账簿*/
                return;
            }
        }
        setDefData(11111, 'gl_obervision.discount.stateUNConfirm', this.state);
        searchData.pk_orgbook_self = [searchData.pk_orgbook_self.refpk];
        searchData.pk_selfvouchertype = searchData.pk_selfvouchertype.refpk;
        searchData.selfpreparer = searchData.selfpreparer.refpk;
        let pk_orgbook_other = searchData.pk_orgbook_other;
        let localArr = [];
        for (let i = 0, len = pk_orgbook_other.length; i < len; i++) {
            localArr.push(pk_orgbook_other[i].refpk)
        }
        searchData.pk_orgbook_other = localArr;
        searchData.pk_othervouchertype = searchData.pk_othervouchertype.refpk;
        searchData.reconcilemaker = searchData.reconcilemaker.refpk;
        searchData.state = this.state.tabState;
        let data = {};
        for (let item in searchData) {
            if (searchData[item]) {
                data[item] = searchData[item]
            }
        }
        let self = this;
        let url = "/nccloud/gl/reconcile/reconciledataquery.do";
        setDefData(1234, 'gl_coord.searchData.unConfirm', data);
        ajax({
            url,
            data,
            success: function (response) {
                const { data, error, success } = response;
                if (success && data) {
                    if (mainSe) {
                        for (let i = 0, len = mainSe.length; i < len; i++) {
                            data[mainSe[i]].isSelected = true;
                        }
                    }
                    self.props.getMainDataUnConfirm(data)
                } else {
                    self.props.getMainDataUnConfirm([])
                }
            }
        });
    }
    handleClear = () => {
        this.setState({
            searchData: {
                // begindate:'',
                // enddate:'',
                isselfquery: 'Y',
                state: '2',
                isselfchecked: 'N',//已审核
                isselftallyed: 'N',//已记账
                pk_orgbook_self: {
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
                pk_orgbook_other: [],
                selfpreparedatestart: '',//本方制单日期开始
                selfpreparedateend: '',//本方制单日期结束
                reconciledatestart: '', //协同开始日期
                reconciledateend: '',//协同结束日期
                pk_othervouchertype: {
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
                othercodestart: '',//
                othercodeend: '',
                otherpreparer: {//制单人
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
                otherpreparedatestart: '',//对方制单日期开始
                otherpreparedateend: '',//对方制单日期结束
                pk_selfvouchertype: {//凭证类别
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
                selfcodestart: '',//开始凭证号
                selfcodeend: '',//结束凭证号
                selfpreparer: {//制单人
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
                reconcilemaker: {//协同人
                    refcode: '',
                    refname: '',
                    refpk: ''
                },
            }
        })
    }
    //获取日期等
    getCombineInfo = (v) => {
        let self = this;
        ajax({
            url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
            data: { "pk_accountingbook": v.refpk, 'needaccount': false },
            success: function (response) {
                const { data, error, success } = response;
                let { searchData, period_pk, bizDate, pk_org, pk_vouchertype } = self.state;
                if (success && data) {
                    searchData.selfpreparedatestart = data.begindate;
                    searchData.selfpreparedateend = data.bizDate;
                    searchData.otherpreparedatestart = data.begindate;
                    searchData.otherpreparedateend = data.bizDate;
                    searchData.reconciledatestart = data.begindate;
                    searchData.reconciledateend = data.bizDate;
                    pk_vouchertype = data.pk_vouchertype;
                    pk_org = data.unit.value;
                    self.setState({
                        searchData, pk_org, pk_vouchertype
                    })
                } else {

                }
            }
        });
    }
    render() {
        let { searchData, tabState } = this.state;
        let mybook, userMaker, userCoor, mybook1, oppUserMake;
        // let self = this;
        // let referUrl= item.refcode+'/index.js';
        let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
        let userUrl = 'uap/refer/riart/userRefer' + '/index.js';
        if (!this.state['myattrcode']) {
            { createScript.call(this, referUrl, 'myattrcode') }
        } else {
            mybook = (
                <NCRow>
                    <NCCol xs={12} md={12}>
                        {this.state['myattrcode'] ? (this.state['myattrcode'])(
                            {
                                fieldid:'pk_orgbook_self',
                                value: { refname: searchData.pk_orgbook_self.refname, refpk: searchData.pk_orgbook_self.refpk, refcode: searchData.pk_orgbook_self.refcode },
                                disabledDataShow: true,
                                isMultiSelectedEnabled: false,
                                queryCondition: {
                                    "TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    "appcode": this.props.getSearchParam('c')
                                },
                                onChange: (v) => {
                                    searchData.pk_orgbook_self.refcode = v.refcode;
                                    searchData.pk_orgbook_self.refname = v.refname;
                                    searchData.pk_orgbook_self.refpk = v.refpk;
                                    this.getCombineInfo(searchData.pk_orgbook_self)
                                    this.setState({
                                        searchData
                                    })
                                }
                            }
                        ) : <div />}
                    </NCCol>
                </NCRow>
            );
            mybook1 = (
                <NCRow>
                    <NCCol xs={12} md={12}>
                        {this.state['myattrcode'] ? (this.state['myattrcode'])(
                            {
                                fieldid:'pk_orgbook_other',
                                value: searchData.pk_orgbook_other,
                                disabledDataShow: true,
                                queryCondition: {
                                    // "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    "appcode": this.props.getSearchParam('c'),
                                    flag: true,
                                    multiGroup: 'Y'
                                },
                                isMultiSelectedEnabled: true,
                                showGroup: true,
                                flag: true,
                                onChange: (v) => {
                                    searchData.pk_orgbook_other = v;
                                    this.setState({
                                        searchData
                                    })
                                }
                            }
                        ) : <div />}
                    </NCCol>
                </NCRow>
            )
        }
        if (!this.state['userCode']) {
            { createScript.call(this, userUrl, 'userCode') }
        } else {
            userMaker = (
                <NCRow>
                    <NCCol xs={12} md={12}>
                        {this.state['userCode'] ? (this.state['userCode'])(
                            {
                                fieldid:'selfpreparer',
                                isMultiSelectedEnabled: false,
                                value: {
                                    refcode: searchData.selfpreparer.refcode, refname: searchData.selfpreparer.refname,
                                    refpk: searchData.selfpreparer.refpk
                                },
                                queryCondition: {
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi',
                                },
                                placeholder: this.state.json['2002130210-000050'],/* 国际化处理： 制单人*/
                                onChange: (v) => {
                                    searchData.selfpreparer = v;
                                    this.setState({
                                        searchData
                                    })
                                }
                            }
                        ) : <div />}
                    </NCCol>
                </NCRow>
            )
            userCoor = (
                <NCRow>
                    <NCCol xs={12} md={12}>
                        {this.state['userCode'] ? (this.state['userCode'])(
                            {
                                fieldid:'reconcilemaker',
                                isMultiSelectedEnabled: false,
                                placeholder: this.state.json['2002130210-000051'],/* 国际化处理： 协同人*/
                                value: {
                                    refcode: searchData.reconcilemaker.refcode, refname: searchData.reconcilemaker.refname,
                                    refpk: searchData.reconcilemaker.refpk
                                },
                                queryCondition: {
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi',
                                },
                                onChange: (v) => {
                                    searchData.reconcilemaker.refcode = v.refcode;
                                    searchData.reconcilemaker.refname = v.refname;
                                    searchData.reconcilemaker.refpk = v.refpk;
                                    this.setState({
                                        searchData
                                    })
                                }
                            }
                        ) : <div />}
                    </NCCol>
                </NCRow>
            )
            oppUserMake = (
                <NCRow>
                    <NCCol xs={12} md={12}>
                        {this.state['userCode'] ? (this.state['userCode'])(
                            {
                                fieldid:'otherpreparer',
                                isMultiSelectedEnabled: false,
                                value: {
                                    refcode: searchData.otherpreparer.refcode, refname: searchData.otherpreparer.refname,
                                    refpk: searchData.otherpreparer.refpk
                                },
                                queryCondition: {
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi',
                                },
                                placeholder: this.state.json['2002130210-000050'],/* 国际化处理： 制单人*/
                                onChange: (v) => {
                                    searchData.otherpreparer.refcode = v.refcode;
                                    searchData.otherpreparer.refname = v.refname;
                                    searchData.otherpreparer.refpk = v.refpk;
                                    this.setState({
                                        searchData
                                    })
                                }
                            }
                        ) : <div />}
                    </NCCol>
                </NCRow>
            )
        }
        return (
            <div className='search-coordinate' style={{ 'padding': '5px', 'padding-top': '10px' }}>
            <NCDiv areaCode={NCDiv.config.SEARCH}>
                <div>
                    <div className='tittle1'>
                        <h2>{this.state.json['2002130210-000017']}</h2>
                        <i class="uf uf-triangle-right"></i>
                    </div>
                    <div className='sertch-content sertch-content1'>
                        <NCCol md={2} xs={2} sm={2}>
                            <span style={{ 'color': '#E14C46', 'position': 'absolute', 'left': '4px', 'top': '9px', 'z-index': '2' }}>*</span>
                            {mybook}
                        </NCCol>
                        
                        <NCCol md={2} xs={2} sm={2} style={{ 'width': '150px' }}>
                            <NCIcon type={this.state.showMore ? "uf-triangle-up" : 'uf-triangle-down'} onClick={(e) => {
                                let showMore = this.state.showMore;
                                showMore = !showMore;
                                this.props.setUnConShow(showMore);
                                this.setState({
                                    showMore: showMore
                                })
                            }}></NCIcon>
                            <NCButton fieldid="handleSearch" colors="primary" type="primary" onClick={this.handleSearch.bind(this)}>{this.state.json['2002130210-000061']}</NCButton>
                            <NCButton fieldid="handleClear" onClick={this.handleClear}>{this.state.json['2002130210-000062']}</NCButton>
                        </NCCol>
                    </div>
                </div>
                <div style={{ 'margin-top': '10px' }} className={this.state.showMore ? '' : 'display-none'}>
                    <div className='tittle1'>
                        <h2>{this.state.json['2002130210-000063']}</h2>
                        <i class="uf uf-triangle-right"></i>
                    </div>
                    <div className='sertch-content sertch-content1'>
                        <NCCol md={2} xs={2} sm={2}>
                            <NCRangePicker
                                fieldid="reconciledate"
                                placeholder={this.state.json['2002130210-000056']}/* 国际化处理： 协同日期*/
                                dateInputPlaceholder={[this.state.json['2002130210-000052'], this.state.json['2002130210-000053']]}/* 国际化处理： 开始,结束*/
                                showClear={true}
                                onChange={(v) => {
                                    searchData.reconciledatestart = v[0];
                                    searchData.reconciledateend = v[1];
                                    searchData.reconciledate = v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                value={searchData.reconciledatestart && searchData.reconciledateend ? [searchData.reconciledatestart, searchData.reconciledateend] : []}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} >
                            <NCFormControl name="name" placeholder={this.state.json['2002130210-000057']} /* 国际化处理： 协同号开始*/
                                fieldid="reconcilecodestart"
                                value={searchData.reconcilecodestart}
                                onChange={(v) => {
                                    searchData.reconcilecodestart = v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} >
                            <NCFormControl name="name" placeholder={this.state.json['2002130210-000058']} /* 国际化处理： 协同号结束*/
                                fieldid="reconcilecodeend"
                                value={searchData.reconcilecodeend}
                                onChange={(v) => {
                                    searchData.reconcilecodeend = v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} >
                            {userCoor}
                        </NCCol>
                    </div>
                </div>
                <div style={{ 'margin-top': '10px' }} className={this.state.showMore ? '' : 'display-none'}>
                    <div className='tittle1'>
                        <h2>{this.state.json['2002130210-000018']}</h2>
                        <i class="uf uf-triangle-right"></i>
                    </div>
                    <div className='sertch-content sertch-content1'>
                        <NCCol md={2} xs={2} sm={2} style={{ 'margin-top': '-2px' }}>
                            {mybook1}
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} style={{ 'margin-top': '-2px' }}>
                            <NCRangePicker
                                fieldid="otherpreparedate"
                                placeholder={this.state.json['2002130210-000030']}/* 国际化处理： 制单日期*/
                                dateInputPlaceholder={[this.state.json['2002130210-000052'], this.state.json['2002130210-000053']]}/* 国际化处理： 开始,结束*/
                                showClear={true}
                                onChange={(v) => {
                                    searchData.otherpreparedatestart = v[0];
                                    searchData.otherpreparedateend = v[1];
                                    searchData.otherpreparedate = v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                value={searchData.otherpreparedatestart && searchData.otherpreparedateend ? [searchData.otherpreparedatestart, searchData.otherpreparedateend] : []}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} style={{ 'margin-top': '-2px' }} className={searchData.pk_orgbook_other.length == 1 ? '' : 'display-none'}>
                            <VoucherTypeDefaultGridRef
                                fieldid="pk_othervouchertype"
                                value={{
                                    refname: searchData.pk_othervouchertype.refname,
                                    refpk: searchData.pk_othervouchertype.refpk
                                }}
                                placeholder={this.state.json['2002130210-000031']}/* 国际化处理： 凭证类别*/
                                queryCondition={{
                                    GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
                                    pk_org: searchData.pk_orgbook_other[0] ? searchData.pk_orgbook_other[0].refpk : '',
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi',
                                }}
                                onChange={(v) => {
                                    searchData.pk_othervouchertype.refname = v.refname;
                                    searchData.pk_othervouchertype.refpk = v.refpk;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} className={searchData.pk_orgbook_other.length == 1 ? '' : 'display-none'}>
                            <NCFormControl name='explanation' className='explanation' type='text' value={searchData.othercodestart}
                                fieldid="othercodestart"
                                autocomplete="off"
                                placeholder={this.state.json['2002130210-000054']}/* 国际化处理： 开始凭证号*/
                                onChange={(value) => {
                                    let lastItem = value.slice(value.length - 1)
                                    let testNum = /[0-9]/
                                    if (!testNum.test(lastItem) && lastItem !== '') {
                                        searchData.othercodestart = searchData.othercodestart;
                                    } else {
                                        searchData.othercodestart = value;
                                    }
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} className={searchData.pk_orgbook_other.length == 1 ? '' : 'display-none'}>
                            <NCFormControl name='explanation' className='explanation' type='text' value={searchData.othercodeend}
                                fieldid="othercodeend"
                                autocomplete="off"
                                placeholder={this.state.json['2002130210-000055']}/* 国际化处理： 结束凭证号*/
                                onChange={(value) => {
                                    let lastItem = value.slice(value.length - 1)
                                    let testNum = /[0-9]/
                                    if (!testNum.test(lastItem) && lastItem !== '') {
                                        searchData.othercodeend = searchData.othercodeend;
                                    } else {
                                        searchData.othercodeend = value;
                                    }
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} style={{ 'margin-top': '10px', 'margin-bottom': '10px' }} className={searchData.pk_orgbook_other.length == 1 ? '' : 'display-none'}>
                            {oppUserMake}
                        </NCCol>
                    </div>
                </div>
                </NCDiv>
            </div>
        )
    }
}
SearchUnConfirm = createPage({})(SearchUnConfirm);
export default SearchUnConfirm;
