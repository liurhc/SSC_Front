import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { base,cardCache,getMultiLang,createPage} from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
import ButtonsConfirm from '../buttons/confirm';
import ButtonsUnConfirm from '../buttons/unConfirm';
import CoorTableConfirm from '../table/confirm';
import CoorTableUnConfirm from '../table/unConfirm';
import SearchConfirm from '../search/confirm';
import SearchUnConfirm from '../search/unConfirm';
import './index.less';
const { NCTabs,NCDiv } = base;
const NCTabPane = NCTabs.NCTabPane;
class CoordinationCenter extends Component {
    constructor(props){
        super(props);
        this.state={
            json:{},
            tabState:'1',
            selectArr:[],
            selectArr1:[],
            MainDataConfirm:[],
            MainDataConfirmse:[],
            MainDataUnConfirm:[],
            MainDataUnConfirmse:[],
            queryCondition:{},//未确认的查询条件
            conShow:false,
            unconShow:false
        }
    }
    componentWillMount(){
        let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
        getMultiLang({moduleId:['20021005card','2002130210','20021005list'],domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentDidMount(){
        let tabState = getDefData('tabState', 'gl_obervision.discount.tabState');
        if(tabState){
            this.setState({
                tabState
            })
        }
    }
    getMainDataConfirm=(data)=>{
        this.setState({
            MainDataConfirm:data
        })
    }
    getMainDataUnConfirm=(data,condition)=>{
        this.setState({
            selectArr:[],
            MainDataUnConfirm:data,
            queryCondition:condition
        })
    }
    getSelectArr1=(data,maindata)=>{
        let MainDataConfirmse = [];
        if(maindata&&maindata.length>0){
            for(let i=0,len=maindata.length;i<len;i++){
                if(maindata[i].isSelected){
                    MainDataConfirmse.push(i)
                }
            }
        }
        this.setState({
            selectArr1:data,
            MainDataConfirmse
        })
    }
    getSelectArr=(data,maindata)=>{
        let MainDataUnConfirmse = [];
        if(maindata&&maindata.length>0){
            for(let i=0,len=maindata.length;i<len;i++){
                if(maindata[i].isSelected){
                    MainDataUnConfirmse.push(i)
                }
            }
        }
        this.setState({
            selectArr:data,
            MainDataUnConfirmse
        })
    }
    setConShow=(data)=>{//设置已确认表格长度
        this.setState({
            conShow:data
        })
    }
    setUnConShow=(data)=>{//设置未确认表格长度
        this.setState({
            unconShow:data
        })
    }
    render() {
        let {tabState,selectArr,selectArr1,MainDataConfirm,MainDataUnConfirm,queryCondition,conShow,
            unconShow,MainDataConfirmse,MainDataUnConfirmse
        }=this.state;
        return (
            <div className='coord-main'>
                <NCDiv areaCode={NCDiv.config.HEADER}>
                    <NCDiv fieldid={this.props.getSearchParam('n')} areaCode={NCDiv.config.Title}></NCDiv>
                </NCDiv>
                <NCTabs navtype="turn" contenttype="moveleft" activeKey={tabState} onChange={(v)=>{
                    this.setState({
                        tabState:v
                    })
                    setDefData('tabState', 'gl_obervision.discount.tabState',v);
                }}>
                    <NCTabPane tab={this.state.json['2002130210-000047']} key="1">
                        <div className="nc-bill-list">
                            <div className="nc-bill-search-area">
                                <SearchUnConfirm
                                    getMainDataUnConfirm={this.getMainDataUnConfirm.bind(this)}
                                    tabState={tabState}
                                    MainDataUnConfirmse={MainDataUnConfirmse}
                                    setUnConShow={this.setUnConShow.bind(this)}
                                />
                            </div>
                        </div>
                        <NCDiv fieldid="search" areaCode={NCDiv.config.TABLE}>
                        <ButtonsUnConfirm tabState={tabState} 
                            getMainDataUnConfirm={this.getMainDataUnConfirm.bind(this)}
                            selectArr={selectArr}
                            queryCondition={queryCondition}
                            className={MainDataUnConfirm.length>0?'':'display-none-coord'}
                            maindata={MainDataUnConfirm}
                        />
                        <CoorTableUnConfirm maindata={MainDataUnConfirm}
                            unconShow={unconShow}
                            queryCondition={queryCondition}
                            getSelectArr={this.getSelectArr.bind(this)}
                            className={MainDataUnConfirm.length>0?'':'display-none-coord'}
                        />
                        </NCDiv>
                    </NCTabPane>
                    <NCTabPane tab={this.state.json['2002130210-000048']} key="2">
                        <div className="nc-bill-list">
                            <div className="nc-bill-search-area">
                                <SearchConfirm 
                                    getMainDataConfirm={this.getMainDataConfirm.bind(this)}
                                    tabState={tabState}
                                    mainDataConfirmse={MainDataConfirmse}
                                    setConShow={this.setConShow.bind(this)}
                                />
                            </div>
                        </div>
                        <NCDiv fieldid="search" areaCode={NCDiv.config.TABLE}>
                        <ButtonsConfirm tabState={tabState} 
                            selectArr1={selectArr1}
                            maindata={MainDataConfirm}
                            getMainDataConfirm={this.getMainDataConfirm.bind(this)}
                            className={MainDataConfirm.length>0?'':'display-none-coord'}
                        />
                        <CoorTableConfirm maindata={MainDataConfirm}
                            getSelectArr1={this.getSelectArr1.bind(this)}
                            className={MainDataConfirm.length>0?'':'display-none-coord'}
                            conShow={conShow}
                        />
                        </NCDiv>
                    </NCTabPane>
                </NCTabs>
            </div>
        )
    }
}
CoordinationCenter = createPage({

})(CoordinationCenter);
export default CoordinationCenter;

// ReactDOM.render(<CoordinationCenter />, document.querySelector('#app'));
