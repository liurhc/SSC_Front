import React, { Component } from "react";
import {ajax,base,getMultiLang} from 'nc-lightapp-front';
const { NCButton, NCButtonGroup,NCMenu:Menu,NCTable,NCButton:Button,NCCheckbox:Checkbox } = base;
import Welcome from '../../../gl_voucher/container/Welcome';
export default class ConfirmTable extends Component {
  constructor(props){
    super(props);
    this.state = {
      json:{},
      mainData: [],
      checkedAll:false,
      selectData:[],
      selIds:[],
      checkedArray: [
        false,
        false,
        false,
      ],
      columns:[]
    };
  }  
  // componentWillReceiveProps(nextProps){//不执行
  //   let mainData = nextProps.tableData;
  //   // mainData.map((item,index)=>{
  //   //   item.key=index
  //   // })
    
  //   this.setState({
  //     mainData:mainData
  //   })
  // }
  componentWillMount(){
    let {tableData}=this.props;
    this.setState({
      mainData:tableData
    })
    let callback= (json) =>{
         this.setState({json:json,
        columns:[
          {
            title: json['2002130210-000036'],/* 国际化处理： 序号*/
            dataIndex: "num",
            key: "num",
            render: (text, record, index) => (
              <span>
                {index}
              </span>
            )
          },
          {
            title: json['2002130210-000037'],/* 国际化处理： 摘要*/
            dataIndex: "explanation",
            key: "explanation",
            render: (text, record, index) => (
              <span>
              {this.state.mainData[index].explanation}</span>
            )
          },
          {
            title: json['2002130210-000038'],/* 国际化处理： 科目*/
            dataIndex: "reconciler",
            key: "reconciler",
            render: (text, record, index) => (
              <span>
              {/* {this.state.mainData[index].reconciler} */}
              </span>
            )
          },
          {
            title: json['2002130210-000039'],/* 国际化处理： 辅助核算*/
            dataIndex: "reconciler",
            key: "reconciler",
            render: (text, record, index) => (
              <span>
              {/* {this.state.mainData[index].reconciler} */}
              </span>
            )
          },
          {
            title: json['2002130210-000040'],/* 国际化处理： 币种*/
            dataIndex: "reconciler",
            key: "reconciler",
            render: (text, record, index) => (
              <span>
              {/* {this.state.mainData[index].reconciler} */}
              </span>
            )
          },
          {
            title: json['2002130210-000041'],/* 国际化处理： 原币*/
            dataIndex: "reconciler",
            key: "reconciler",
            render: (text, record, index) => (
              <span>
              {/* {this.state.mainData[index].reconciler} */}
              </span>
            )
          },
          {
            title: json['2002130210-000042'],/* 国际化处理： 借方*/
            dataIndex: "totaldebit",
            key: "totaldebit",
            render: (text, record, index) => (
              <span>
              {this.state.mainData[index].totaldebit}</span>
            )
          },
          {
            title: json['2002130210-000043'],/* 国际化处理： 贷方*/
            dataIndex: "totalcredit",
            key: "totalcredit",
            render: (text, record, index) => (
              <span>
              {this.state.mainData[index].totalcredit}</span>
            )
          },
          {
            title: json['2002130210-000044'],/* 国际化处理： 数量*/
            dataIndex: "num",
            key: "num",
            render: (text, record, index) => (
              <span>
              {this.state.mainData[index].num}</span>
            )
          },
          {
            title: json['2002130210-000045'],/* 国际化处理： 集团本币*/
            dataIndex: "reconciler",
            key: "reconciler",
            render: (text, record, index) => (
              <span>
              {/* {this.state.mainData[index].reconciler} */}
              </span>
            )
          },
          {
            title: json['2002130210-000046'],/* 国际化处理： 全局本币*/
            dataIndex: "reconciler",
            key: "reconciler",
            render: (text, record, index) => (
              <span>
              {/* {this.state.mainData[index].reconciler} */}
              </span>
            )
          },
        ]  
      },()=>{ })
          }
    getMultiLang({moduleId:'2002130210',domainName:'gl',currentLocale:'simpchn',callback}); 
  }
  //处理多选
  onAllCheckChange = () => {
    let self = this;
    let checkedArray = [];
    let listData = self.state.mainData.concat();
    let selIds = [];
    // let id = self.props.multiSelect.param;
    for (var i = 0; i < self.state.checkedArray.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
    }
    self.setState({
      checkedAll: !self.state.checkedAll,
      checkedArray: checkedArray,
      // selIds: selIds
    });
    // self.props.getSelectArr(selIds);
  };
  onCheckboxChange = (text, record, index) => {
    let self = this;
    let allFlag = false;
    let selIds = self.state.selIds;
    // let id = self.props.postId;
    let checkedArray = self.state.checkedArray.concat();
    if (self.state.checkedArray[index]) {
      selIds.remove(record.key);
    } else {
      selIds.push(record.key);
    }
    checkedArray[index] = !self.state.checkedArray[index];
    for (let i = 0; i < self.state.checkedArray.length; i++) {
      if (!checkedArray[i]) {
        allFlag = false;
        break;
      } else {
        allFlag = true;
      }
    }
    self.setState({
      checkedAll: allFlag,
      checkedArray: checkedArray,
      selIds: selIds
    });
    let mainData = this.state.mainData;
    let selectArr = [];
    for (let i = 0; i < selIds.length; i++) {
      selectArr.push(mainData[i])
    }
    self.setState({
      selectData:selectArr
    });
  };

  renderColumnsMultiSelect=(columns)=>{
    const { data,checkedArray } = this.state;
    const { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // let indeterminate_bool1 = true;
    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArray.length;
      while(i--){
          if(checkedArray[i]){
            indeterminate_bool = true;
            break;
          }
      }
      let defaultColumns = [
        {
          title: (
            <Checkbox
              className="table-checkbox"
              checked={this.state.checkedAll}
              indeterminate={indeterminate_bool&&!this.state.checkedAll}
              onChange={this.onAllCheckChange}
            />
          ),
          key: "checkbox",
          dataIndex: "checkbox",
          width: "5%",
          render: (text, record, index) => {
            return (
              <Checkbox
                className="table-checkbox"
                checked={this.state.checkedArray[index]}
                onChange={this.onCheckboxChange.bind(this, text, record, index)}
              />
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
  render() {
    // let columns = this.columns; 
    let {mainData}=this.state;
  
    let columns = this.renderColumnsMultiSelect(this.state.columns);

    return (
      <Welcome  datavoucher={this.props.tableData}  />
        // <div>
        //     <span style={{'padding':'1px'}}></span>
        //     <NCTable
        //         columns={columns}
        //         data={mainData}
        //         bordered
        //         // scroll={{ x: "130%", y: 240 }}
        //     />
        // </div>
    );
  }
}
