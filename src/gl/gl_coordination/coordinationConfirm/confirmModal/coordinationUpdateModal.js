import React, { Component } from 'react';
import { ajax,base,getMultiLang }  from 'nc-lightapp-front';
const { NCRadio, NCForm, NCFormControl,NCCheckbox,NCTabs,
    NCModal:Modal,NCRow,NCCol,NCButton:Button,NCDatePicker,NCButtonGroup } = base;
const NCFormItem = NCForm.NCFormItem;
import Welcome from '../../../gl_voucher/container/ExportWelcome';
const NCTabPane = NCTabs.NCTabPane;
export default class CoordinationUpdateModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            CashFlowModalShow:false,//现金流量模态框
            saveData:[],//存储数据
            selectArr:[{
                self_bookname:''
            }],
            voucherUpdateData:[],
            currentRow:{
                currentBatchNo:'',//当前选中行的batchNo
                currentRowId:'',
                rowCount:'',//总行数
            } 
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
		let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
        getMultiLang({moduleId:'2002130210',domainName:'gl',currentLocale:'simpchn',callback}); 
	}
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.confirmModalShow;
        //let selectArr = nextProps.selectArr;
        let {voucherUpdateData,currentRow}=this.state;
        let tableData = nextProps.tableData;
        currentRow = nextProps.currentRow;
        
        if(tableData&&tableData!=this.state.voucherUpdateData){
            voucherUpdateData=tableData;
            this.setState({
                showModal,
                //selectArr,
                currentRow,
                voucherUpdateData
            })
        }else{
            this.setState({
                showModal
            })
        }
        
    }
    componentDidMount(){}
    onRef = (ref) => {
        this.child = ref
    }
    //增加
    handleAddClick=()=>{
        this.child.handleAddLast();
        //this.refs.getSwordButton.print();
    }
    //删除
    handleDeleteClick=()=>{
        this.child.handleCancel()
    }
    
    //插入
    handleInsertClick=()=>{
        this.child.handleAdd();
    }
    //得到选中行数据
    handleGetRowData=()=>{
        let row=this.child.handleGetRow();
        return row;
    }
    //确定
    confirmClick=()=>{
        let self=this;
        let {voucherUpdateData}=self.state;
        let rows=self.child.handleGetAllRows();
        self.props.coordinationEnsure(rows);
    }
    //取消
    cancleClick=()=>{
        this.props.coordinationClose(false);
    }
    //关闭
    close() {
        this.props.coordinationClose(false);
    }
    
    render () {
        let {voucherUpdateData,CashFlowModalShow,saveData,currentRow} = this.state;
        return (
        <div>
            <Modal
            fieldid="coordination"
            show = { this.state.showModal }
            onHide = { this.close }
            className="analyseModal"
            size={'lg'}
            draggable={false}
            id="coordinationWelcome"
            >
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['2002130210-000016']}</Modal.Title>
                </Modal.Header>
                <Modal.Body >
                    <Welcome 
                        onRef={this.onRef} 
                        ref="voucherWelcome" 
                        datavoucher={voucherUpdateData} 
                        currentRow={currentRow}
                        voucherView={false} 
                    />
                    <NCButtonGroup style={{'margin-left':'59px','padding':'10px'}}>
                        {/* <Button colors="primary" onClick={this.handleCashFlowClick.bind(this)}>现金流量分析</Button> */}
                        <Button fieldid="assist" >{this.state.json['2002130210-000034']}</Button>
                        {/* <Button colors="primary" onClick={this.coordinationConfirm.bind(this)}>协同确认</Button> */}
                        <Button fieldid="confirmClick" colors="primary" onClick={this.confirmClick.bind(this)}>{this.state.json['2002130210-000035']}</Button>
                        <Button fieldid="cancleClick" colors="primary" onClick={this.cancleClick.bind(this)}>{this.state.json['2002130210-000004']}</Button>
                        <Button fieldid="handleAddClick" colors="primary" onClick={this.handleAddClick.bind(this)}>{this.state.json['2002130210-000021']}</Button>
                        <Button fieldid="handleDeleteClick"colors="primary" onClick={this.handleDeleteClick.bind(this)}>{this.state.json['2002130210-000022']}</Button>
                        <Button fieldid="handleInsertClick" colors="primary" onClick={this.handleInsertClick.bind(this)}>{this.state.json['2002130210-000023']}</Button>
                        {/* <Button colors="primary" onClick={this.coordinationUpdate.bind(this)}>协同修改</Button> */}
                    </NCButtonGroup> 
                </Modal.Body>

                <Modal.Footer fieldid="bottom_area">
                    <Button fieldid="close" onClick={ this.close } shape="border" style={{marginRight: 50}}>{this.state.json['2002130210-000004']}</Button>
                    {/* <Button onClick={ this.handleAnalyse } colors="primary">分析</Button> */}
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
