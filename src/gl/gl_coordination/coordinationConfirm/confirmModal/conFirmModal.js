import React, { Component } from 'react';
import { ajax, createPage,getMultiLang,toast } from 'nc-lightapp-front';
import { base } from 'nc-lightapp-front';
const {
	NCRadio,
	NCForm,
	NCFormControl,
	NCCheckbox,
	NCTabs,
	NCModal: Modal,
	NCRow,
	NCCol,
	NCButton: Button,
	NCTZDatePickClientTime,
	NCButtonGroup,
	NCNumber
} = base;
const NCFormItem = NCForm.NCFormItem;
import VoucherTypeDefaultGridRef from '../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef';
import Welcome from '../../../gl_voucher/container/ExportWelcome/index.js';
import Welcomecopy from '../../../gl_voucher/container/ExportWelcome/copyIndex.js';
import CashFlow from '../../../gl_voucher/container/Cashflow';
import CoordinationUpdateModal from './CoordinationUpdateModal';
const NCTabPane = NCTabs.NCTabPane;
import './conFirmModal.less';
class ConfirmModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json:{},
			currentRowIndex:-1,//当前选中本方凭证行
			queryCondition: {}, //查询条件
			coordinationStatus: 'uncoordination', //协同修改状态coordination,非协同状态uncoordination
			showModal: false,
			CashFlowModalShow: false, //现金流量模态框
			CoordinationUpdateShow: false, //协同修改按钮
			saveData: [], //存储数据
			selectArr: [
				{
					self_bookname: ''
				}
			],
			searchData: {
				preparedatestart: { value: '' },
				pk_vouchertype: {
					//凭证类别 默认值
					//refcode:'01',
					display: '',
					value: ''
				},
				formNum: ''
			},
			UpdateUncoordinationDatas: {
				//对方凭证的数据
				voucher: {},
				paraInfo: {}
			},
			voucherData: {}, //凭证的数据
			selectData: [], //选中行数据做协同修改
			voucherUpdateData: [], //协同修改数据
			currentRow: {
				currentBatchNo: '0000', //当前选中行的batchNo
				currentRowId: '',
				rowCount: '' //总行数
			},
			activeTabKey: 0
		};
		this.close = this.close.bind(this);
	}
	componentWillMount(){
		let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
        getMultiLang({moduleId:['20021005card','2002130210','20021005list'],domainName:'gl',currentLocale:'simpchn',callback}); 
	}
	componentWillReceiveProps(nextProps) {
		let showModal = nextProps.confirmModalShow;
		let selectArr = nextProps.selectArr;
		let tableData = nextProps.tableData;
		let { voucherData, searchData, queryCondition } = this.state;
		queryCondition = nextProps.queryCondition;
		if (tableData&&tableData.voucher && tableData != this.state.voucherData) {
			voucherData = tableData;
			if (voucherData.voucher) {
                if(!voucherData.voucher.pk_vouchertype&&queryCondition.pk_vouchertype){
                    voucherData.voucher.pk_vouchertype = queryCondition.pk_vouchertype;
                }
				
				voucherData.voucher.details.map((item, index) => {
					if(item.userdata&&item.userdata.value){
						item.deleteStatus = '0'; //0 表示是协同分录不可删除，其余行可以删除
					}
				});
			}

			if (tableData.voucher) {
				searchData.preparedatestart = tableData.voucher.prepareddate;
				searchData.pk_vouchertype = tableData.paraInfo.pk_vouchertype;
			}
			this.setState({
				showModal,
				selectArr,
				voucherData,
				searchData,
				queryCondition
			});
		} else {
			this.setState({
				showModal,
				selectArr,
				queryCondition
			});
		}
	}
	componentDidMount() { }
	onRef = (ref) => {
		this.child = ref;
	};
	copyonRef = (ref) => {
		this.childcopy = ref;
	};
	//增加
	handleAddClick = () => {
		let { currentRow } = this.state;
		currentRow.currentBatchNo = '0000';
		this.setState({ currentRow });
		this.child.handleAddLast();
	};
	//删除
	handleDeleteClick = () => {
		let rowData = this.handleGetRowData();
		if (rowData) {
			this.child.handleCancel();
		}else {
			toast({ content: this.state.json['2002130210-000068'], color: 'warning' });/* 国际化处理： 请选中一行数据再进行删除操作*/
			return false;
		}
	};

	//插入
	handleInsertClick = () => {
		this.child.handleAdd();
	};
	//现金流量分析
	handleCashFlowClick = () => {
		let self = this;

		let { saveData, CashFlowModalShow, voucherData } = self.state;
		let rows = self.child.handleGetAllRows();
		//更新 组织本币借贷方金额
		rows.map((item, index) => {
			if (item.direction.value == 'C') {
				item.creditamount.value = item.amount.value;
			} else if (item.direction.value == 'D') {
				item.debitamount.value = item.amount.value;
			}
		});
		voucherData.voucher.details = rows;
		voucherData.voucher.paraInfo=voucherData.paraInfo;
		saveData = voucherData;
		CashFlowModalShow = true;
		self.setState(
			{
				saveData,
				CashFlowModalShow
			},
			() => {
				self.props.modal.show('CashFlowModal');
			}
		);
	};
	//协同确认
	coordinationConfirm = () => {
		let self = this;
		let { voucherData, searchData } = self.state;
		let rows = self.child.handleGetAllRows();
		//更新 组织本币借贷方金额
		rows.map((item, index) => {
			if (item.direction.value == 'C') {
				item.creditamount.value = item.amount.value;
			} else if (item.direction.value == 'D') {
				item.debitamount.value = item.amount.value;
			}
		});
		voucherData.voucher.details = rows;
		let param = voucherData.voucher;
		param.prepareddate = searchData.preparedatestart;
		param.pk_vouchertype = searchData.pk_vouchertype;
		let url = '/nccloud/gl/reconcile/reconcileconfirm.do';
		ajax({
			url: url,
			data: param,
			success: function(response) {
				const { data, error, success } = response;
				if (success) {
					self.props.refreshData();
					self.props.getConfirmModalShow(false);
				} else {
					toast({ content: self.state.json['2002130210-000008'], color: 'warning' });/* 国际化处理： 协同确认失败*/
					return false;
				}
			}
		});
	};
	//协同拆分 进入协同修改状态coordination,非协同状态uncoordination
	coordinationUpdate = () => {
		let self = this;
		let rowData = self.handleGetRowData();
		// if (rowData.pk_accasoa) {
		if (rowData) {
			//如果选中了一行，点击协同拆分时复制当前行并且在当前行下新增一行
			if(rowData.userdata&&rowData.userdata.value){//控制只有协同分录行才可以做协同拆分
				this.child.coordinationResolute();
			}else{
				toast({ content: this.state.json['2002130210-000067'], color: 'warning' });/* 国际化处理： 只有协同分录行才能进行协同拆分操作*/
				return false;
			}
			
		} else {
			toast({ content: this.state.json['2002130210-000009'], color: 'warning' });/* 国际化处理： 请选中一行数据再进行协同拆分操作*/
			return false;
		}
	};
	//取消协同修改状态
	uncoordinationUpdate = () => {
		let { coordinationStatus } = this.state;
		coordinationStatus = 'coordination';
		this.setState({
			coordinationStatus
		},()=>{
			this.props.getConfirmModalShow(false);
		});
	};
	//协同修改框确定  跟新凭证行数据voucherData
	coordinationEnsure = (data) => {
		let self = this;
		let { voucherData, currentRow } = self.state;
		data.map((list, index) => {
			if (index == 0) {
				voucherData.voucher.details[currentRow.currentRowId - 1] = data[index];
			} else {
				voucherData.voucher.details.push(list);
			}
		});
		voucherData.voucher.details.map((item, index) => {
			item.key = ++index;
		});
		self.setState({
			//voucherData:rightNowData,
			currentRowIndex:-1,
			voucherData,
			CoordinationUpdateShow: false
		});
	};
	//协同修改框关闭
	coordinationClose = () => {
		this.setState({
			CoordinationUpdateShow: false
		});
	};
	//得到选中行数据
	handleGetRowData = () => {
		let row = this.child.handleGetRow();
		return row;
	};
	//关闭
	close() {
		this.setState({
			activeTabKey: 0,
			currentRowIndex:-1
		},()=>{
			this.props.getConfirmModalShow(false);
		})
	}
	//更新对方凭证渲染的数据，和状态status=browse
	UpdateUncoordination = (key) => {
		let self = this;
		let { UpdateUncoordinationDatas, currentRowIndex,voucherData } = self.state;
		let rowData = self.handleGetRowData();
		if (rowData) {
			currentRowIndex = rowData.key - 1;
			if(!voucherData.voucher.details[currentRowIndex].oppVoucher){
				toast({ content: self.state.json['2002130210-000010'], color: 'warning' });/* 国际化处理： 非协同分录不能联查对方凭证*/
				return false;
			}
			self.setState({
				currentRowIndex,
				activeTabKey: key

			});
		} else {
			toast({ content: self.state.json['2002130210-000011'], color: 'warning' });/* 国际化处理： 请先选中一行数据再联查对方凭证*/
			return false;
		}
		
	};
	//首页
	handleFirstPage = () => {
		let self = this;
		let { currentRowIndex } = self.state;
		let allRowsData = self.child.handleGetAllRows();
		let allRowsDataLength=allRowsData.length;
		currentRowIndex = 0;
		for(let i=currentRowIndex;i<allRowsDataLength;i++){
			if(!allRowsData[currentRowIndex].oppVoucher){
				currentRowIndex = currentRowIndex - 0 + 1;
				continue;
			}else{
				self.setState({
					currentRowIndex
				});
				break;
			}
		}
	};
	//上一页
	handlePrevPage = () => {
		let self = this;
		let { currentRowIndex } = self.state;
		let allRowsData = self.child.handleGetAllRows();
		let allRowsDataLength=allRowsData.length;
		for(let i=currentRowIndex;i>=0;i--){
			if (currentRowIndex == 0) {
				toast({ content: self.state.json['2002130210-000012'], color: 'warning' });/* 国际化处理： 当前页已经是第一页了*/
				return false;
			}else{
				currentRowIndex = currentRowIndex -1;
				if(!allRowsData[currentRowIndex].oppVoucher){
					continue;
				}else{
					self.setState({
						currentRowIndex
					});
					break;
				}
			}
		}
	};
	//下一页
	handleNextPage = () => {
		let self = this;
		let { currentRowIndex,voucherData } = self.state;
		let allRowsData = self.child.handleGetAllRows();
		let allRowsDataLength=allRowsData.length;
		for(let i=currentRowIndex;i<allRowsDataLength;i++){
			if (currentRowIndex == allRowsData.length - 1) {
				toast({ content: self.state.json['2002130210-000013'], color: 'warning' });/* 国际化处理： 当前页已经是最后一页了*/
				return false;
			}else{
				currentRowIndex = currentRowIndex - 0 + 1;
				if(!allRowsData[currentRowIndex].oppVoucher){
					continue;
				}else{
					self.setState({
						currentRowIndex
					});
					break;
				}
			}
		}
	};
	//末页
	handleLastPage = () => {
		let self = this;
		let { currentRowIndex } = self.state;
		let allRowsData = self.child.handleGetAllRows();
		currentRowIndex = allRowsData.length - 1;
		for(let i=currentRowIndex;i>=0;i--){
			if(!allRowsData[currentRowIndex].oppVoucher){
				currentRowIndex = currentRowIndex -1;
				continue;
			}else{
				self.setState({
					currentRowIndex
				});
				break;
			}
		}
	};

	onTabClick = (key) => {
		if (key === 1) {
			this.UpdateUncoordination(key);
		} else if (key === 0) {
			this.childcopy.resetVoucherData();
			this.setState({
				activeTabKey: key
			});
		}
		
	};
	render() {
		let {
			selectData,
			selectArr,
			searchData,
			voucherData,
			CashFlowModalShow,
			saveData,
			CoordinationUpdateShow,
			voucherUpdateData,
			currentRow,
			coordinationStatus,
			UpdateUncoordinationDatas,
			currentRowIndex,
			queryCondition
		} = this.state;
		let { modal } = this.props;
		let { createModal } = modal;
		let rows = [];
		let defaultPrepareddate, defaultPk_vouchertype, defaultAttachment,self_attachment=0;
		if (saveData.voucher) {
			rows = saveData.voucher.details;
		} else {
			rows = [];
		}
		if (voucherData.voucher) {
			defaultPrepareddate = voucherData.voucher&&voucherData.voucher.prepareddate?voucherData.voucher.prepareddate.value:'';
			defaultPk_vouchertype = {
				refpk: voucherData.paraInfo&&voucherData.paraInfo.pk_vouchertype?voucherData.paraInfo.pk_vouchertype.value:'',//voucherData.voucher.pk_vouchertype.value,
				refname: voucherData.paraInfo&&voucherData.paraInfo.pk_vouchertype?voucherData.paraInfo.pk_vouchertype.display:''//voucherData.voucher.pk_vouchertype.display
			};
			defaultAttachment = voucherData.voucher.attachment.value;
			if(currentRowIndex!=-1&&voucherData.voucher.details[currentRowIndex]&&voucherData.voucher.details[currentRowIndex].oppVoucher){
				self_attachment=voucherData.voucher.details[currentRowIndex].oppVoucher.attachment.value;
			}
		} else {
			defaultPrepareddate = '';
			defaultPk_vouchertype = { refname: '', refpk: '' };
			defaultAttachment = '';
		}
		
		let { showModal, activeTabKey } = this.state;
		return (
			<div>
				<Modal 
					fieldid="confirm"
					show={showModal} 
					onHide={this.close} 
					className="analyseModal welcomeModal" 
					size={'xlg'}
				>
					<Modal.Header closeButton fieldid="header-area">
						<Modal.Title>{this.state.json['2002130210-000016']}</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="tabs-header">
							<div className="tabs">
								<div
									className={activeTabKey === 0 ? 'active-tab tab' : 'tab'}
									onClick={this.onTabClick.bind(this, 0)}
								>
									{this.state.json['2002130210-000017']}
								</div>
								<div
									className={activeTabKey === 1 ? 'active-tab tab' : 'tab'}
									onClick={this.onTabClick.bind(this, 1)}
								>
									{this.state.json['2002130210-000018']}
								</div>
							</div>
							<div className="btns">
								{activeTabKey === 0 ? (
									<NCButtonGroup>
										<Button fieldid="handleCashFlowClick" onClick={this.handleCashFlowClick.bind(this)}>{this.state.json['2002130210-000019']}</Button>
										<Button fieldid="coordinationConfirm" onClick={this.coordinationConfirm.bind(this)}>{this.state.json['2002130210-000020']}</Button>
										<Button fieldid="uncoordinationUpdate" onClick={this.uncoordinationUpdate.bind(this)}>{this.state.json['2002130210-000004']}</Button>
										<Button fieldid="handleAddClick" onClick={this.handleAddClick.bind(this)}>{this.state.json['2002130210-000021']}</Button>
										<Button fieldid="handleDeleteClick" onClick={this.handleDeleteClick.bind(this)}>{this.state.json['2002130210-000022']}</Button>
										<Button fieldid="handleInsertClick" onClick={this.handleInsertClick.bind(this)}>{this.state.json['2002130210-000023']}</Button>
										<Button fieldid="coordinationUpdate" onClick={this.coordinationUpdate.bind(this)}>{this.state.json['2002130210-000024']}</Button>
									</NCButtonGroup>
								) : (
									<NCButtonGroup>
										<Button fieldid="confirm" onClick={this.close.bind(this)}>{this.state.json['2002130210-000003']}</Button>
										<Button fieldid="close" onClick={this.close.bind(this)}>{this.state.json['2002130210-000004']}</Button>
										<Button fieldid="handleFirstPage" onClick={this.handleFirstPage.bind(this)}>{this.state.json['2002130210-000025']}</Button>
										<Button fieldid="handlePrevPage" onClick={this.handlePrevPage.bind(this)}>{this.state.json['2002130210-000026']}</Button>
										<Button fieldid="handleNextPage" onClick={this.handleNextPage.bind(this)}>{this.state.json['2002130210-000027']}</Button>
										<Button fieldid="handleLastPage" onClick={this.handleLastPage.bind(this)}>{this.state.json['2002130210-000028']}</Button>
									</NCButtonGroup>
								)}
							</div>
						</div>
						{activeTabKey === 0 && [
							<div className="search-line nc-theme-form-label-c">
								<div className="search-item">
									<label>{this.state.json['2002130210-000029']}:</label>
									<label className="accounting-book-name">
										{selectArr[0] ? selectArr[0].opp_bookname : ''}
									</label>
								</div>
								<div className="search-item">
									<label>{this.state.json['2002130210-000030']}</label>
									<div className="search-content">
										<NCTZDatePickClientTime
											fieldid="preparedatestart"
											format={'YYYY-MM-DD'}
											onChange={(v) => {
												let { voucher } = voucherData;
												voucher.prepareddate.value = v;
												searchData.preparedatestart.value = v;
												this.setState({
													searchData,
													voucherData
												});
											}}
											showToday={true}
											value={defaultPrepareddate}
											autofocus={false}
										/>
									</div>
								</div>

								<div className="search-item">
									<label>{this.state.json['2002130210-000031']}</label>
									<div className="search-content">
										<VoucherTypeDefaultGridRef
											fieldid="pk_vouchertype"
											value={defaultPk_vouchertype}
											queryCondition={{
												GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
												pk_org: selectArr[0] ? selectArr[0].opp_accountingbook : '',
												isDataPowerEnable: 'Y',
                                    			DataPowerOperationCode: 'fi',
											}}
											onChange={(v) => {
                                                let { voucher,paraInfo } = voucherData;
                                                let self=this;
                                                if(voucher.pk_vouchertype){
                                                    voucher.pk_vouchertype={display:v.refname,value:v.refpk};
                                                }else{
                                                    voucher.pk_vouchertype={}
                                                    voucher.pk_vouchertype={display:v.refname,value:v.refpk};  
                                                }
												//根据凭证列表请求对应的币种跟汇率
												if(v.refpk){
													let url = '/nccloud/gl/voucher/voucherValueChange.do';
													let data = {
														key: 'pk_vouchertype',
														value: v.refpk,
														pk_accountingbook: voucherData.voucher.pk_accountingbook.value,
														prepareddate: voucherData.voucher.prepareddate.value
													};
													ajax({
														url,
														data,
														success: function(response) {
															if (response.data) {
																voucherData.paraInfo.vouchertypeParaInfo={}
																voucherData.paraInfo.vouchertypeParaInfo={
																	pk_currtype : response.data.currinfo,
																	excrate2 : response.data.excrate2,
																	excrate3 : response.data.excrate3,
																	excrate4 : response.data.excrate4,
																	excrate2scale:response.data.excrate2scale,
																	excrate3scale:response.data.excrate3scale,
																	excrate4scale:response.data.excrate4scale,
																	pricescale:response.data.pricescale?response.data.pricescale:'0',
																	scale : response.data.scale//原币币种精度
																}
																self.setState({
																	voucherData
																});
															}
														}
													});
												}
                                                paraInfo.pk_vouchertype={display:v.refname,value:v.refpk};
                                                
												searchData.pk_vouchertype={display:v.refname,value:v.refpk};
												self.setState({
													searchData,
													voucherData
												});
											}}
										/>
									</div>
								</div>
								<div className="search-item">
									<label>{this.state.json['2002130210-000032']}</label>
									<div className="search-content">
										<NCNumber
											fieldid="formNum"
											scale={0}
											value={defaultAttachment}
											onChange={(v) => {
												let { voucher } = voucherData;
												voucher.attachment.value = v;
												searchData.formNum = v;
												this.setState({
													searchData,
													voucherData
												});
											}}
										/>
									</div>
								</div>
							</div>,
							<Welcome
								onRef={this.onRef}
								coordinationStatus={coordinationStatus}
								ref="voucherWelcome"
								datavoucher={voucherData}
                                voucherView={false}
                                json={this.state.json}
								currentRow={currentRow}
							/>
						]}
						{activeTabKey === 1 && [
							<div className="search-line nc-theme-form-label-c">
								<div className="search-item">
									<label>{this.state.json['2002130210-000029']}：</label>
									<label>{voucherData.voucher.details[currentRowIndex].oppVoucher&&voucherData.voucher.details[currentRowIndex].oppVoucher.pk_accountingbook.display}</label>
								</div>
								<div className="search-item">
									<label>{this.state.json['2002130210-000030']}</label>
									<label>{voucherData.voucher.details[currentRowIndex].oppVoucher&&voucherData.voucher.details[currentRowIndex].oppVoucher.prepareddate.value}</label>
								</div>

								<div className="search-item">
									<label>{this.state.json['2002130210-000031']}</label>
									<label>{voucherData.voucher.details[currentRowIndex].oppVoucher&&voucherData.voucher.details[currentRowIndex].oppVoucher.pk_vouchertype.display}</label>
									{/* {selectArr[0] ? selectArr[0].self_vouchertype:''} */}
								</div>

								<div className="search-item">
								<label>{voucherData.voucher.details[currentRowIndex].oppVoucher&&voucherData.voucher.details[currentRowIndex].oppVoucher.num.value}</label>
								</div>

								<div className="search-item">
									<label>{this.state.json['2002130210-000032']}</label>
									<label>{self_attachment}</label>
								</div>
							</div>,

							<Welcomecopy
								copyonRef={this.copyonRef}
								coordinationStatus={coordinationStatus}
								ref="copyVoucherWelcome"
								resourceVoucherData={voucherData}
                                currentRow={currentRowIndex}
                                json={this.state.json}
								voucherView={true}
							/>
						]}
					</Modal.Body>

					<Modal.Footer fieldid="bottom_area">
						<Button fieldid="close" onClick={this.close}>{this.state.json['2002130210-000033']}</Button>
						{/* <Button onClick={ this.handleAnalyse } >分析</Button> */}
					</Modal.Footer>
				</Modal>
				{createModal('CashFlowModal', {
					title: this.state.json['2002130210-000015'],/* 国际化处理： 现金流量*/
					content: (
						<CashFlow
							onRef={(ref)=>{this.cashFlow=ref}}
							cashFlow={rows}
							sendData={saveData.voucher}
							cashflowType={false}
							getCashFlow={(e) => {
								this.props.modal.close('CashFlowModal');
							}}
						/>
					),
					size: 'xlg',
					beSureBtnClick: ()=>{this.cashFlow&&this.cashFlow.onConfirm()}
					// noFooter: true
				})}
				<CoordinationUpdateModal
					confirmModalShow={CoordinationUpdateShow}
					coordinationClose={this.coordinationClose.bind(this)}
					coordinationEnsure={this.coordinationEnsure.bind(this)}
					tableData={voucherUpdateData}
					currentRow={currentRow}
				/>
			</div>
		);
	}
}
ConfirmModal = createPage({})(ConfirmModal);
export default ConfirmModal;
