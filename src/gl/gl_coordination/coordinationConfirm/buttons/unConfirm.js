import React, { Component } from 'react';
import { ajax, base, cardCache, createPage, getMultiLang } from 'nc-lightapp-front';
import axios from 'axios';
const { NCButton, NCButtonGroup, NCMenu: Menu, NCDropdown: Dropdown } = base;
import ConfirmModal from '../confirmModal/conFirmModal';
import './index.less';
let { setDefData, getDefData } = cardCache;
import { pushToLinkVoucher } from '../../../public/common/voucherUtils';
class ButtonsUnConfirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabState: '3',
            json: {},
            selectArr: [],
            confirmModalShow: false,
            tableData: [],
            queryCondition: {}//未确认的查询条件
        }
    }
    componentWillMount() {
        let callback = (json) => {
            this.setState({ json: json }, () => {  })
        }
        getMultiLang({ moduleId: '2002130210', domainName: 'gl', currentLocale: 'simpchn', callback });
        let pagecode = this.props.getSearchParam('p')
        let appcode = this.props.getSearchParam('c')
        let buttonUN = getDefData('buttonUN', 'coord.search.un');
        if (buttonUN) {
            this.props.button.setButtons(buttonUN);
            this.setState({
                buttonsData: buttonUN
            })
            return;
        }
        let self = this;
        ajax({
            url: '/nccloud/platform/appregister/queryallbtns.do',
            data: {
                pagecode: pagecode,
                appcode: appcode
            },
            success: (res) => {
                if (!res.data) {
                    return
                }
                /*  拿到按钮数据，调用setButtons方法将数据设置到页面上，然后执行后续的按钮逻辑 */
                self.props.button.setButtons([]);
                self.props.button.setButtons(res.data);
                this.setState({
                    buttonsData: res.data
                })
                setDefData('buttonUN', 'coord.search.un', res.data);
            }
        });
        let { selectArr } = this.state;
        if (selectArr.length) {
            this.props.button.setButtonDisabled({ linkself: false, linkopp: false, selfedit: false });
        } else {
            this.props.button.setButtonDisabled({ linkself: true, linkopp: true, selfedit: true });
        }
    }
    componentWillReceiveProps(nextProps) {
        let { queryCondition, tabState, selectArr } = this.state;
        tabState = nextProps.tabState;
        selectArr = nextProps.selectArr;
        queryCondition = nextProps.queryCondition;
        if(selectArr.length==0){
            let maindata = nextProps.maindata;
            maindata.map((item,key)=>{
                if(item.isSelected){
                    selectArr.push(item)
                }
            })
        }
        this.setState({
            tabState,
            selectArr,
            queryCondition
        })
        if (selectArr.length) {
            this.props.button.setButtonDisabled({ linkself: false, linkopp: false, selfedit: false });
        } else {
            this.props.button.setButtonDisabled({ linkself: true, linkopp: true, selfedit: true });
        }
    }

    handleRepare = () => {
        // this.setState({
        //     confirmModalShow:true
        // })
        let selectArr = this.state.selectArr;
        let data = Object.assign([], selectArr);
        let parm = [];
        data.map((item, index) => {
            parm.push(item.pk_recothertmp);
        })
        // data.isselfquery = 'N';
        let self = this;
        let url = "/nccloud/gl/reconcile/reconcileselfedit.do";
        if (selectArr.length > 0) {
            ajax({
                url: url,
                data: parm,
                success: function (response) {
                    const { data, error, success } = response;
                    if (success) {
                        //   self.props.getMainData(data)
                        self.setState({
                            tableData: data,
                            confirmModalShow: true
                        })
                    } else {

                    }
                }
            });
            // axios
            // 	.post(url, data)
            // 	.then(function (response) {

            // 	})
            // 	.catch(function (error) {

            // 	});
        }
    }
    getConfirmModalShow = (data) => {
        this.setState({
            confirmModalShow: false
        })
        this.handleRE();
    }
    handleTovoucher = (type) => {
        let url;
        if (type == 2) {
            url = '/nccloud/gl/reconcile/reconcilelinkopp.do'
        } else if (type == 1) {
            url = '/nccloud/gl/reconcile/reconcilelinkself.do'
        }
        let localData = this.state.selectArr[0];
        localData.isselfquery = 'N';
        let self = this;
        ajax({
            url,
            data: localData,
            success: function (response) {
                const { data, error, success } = response;
                if (success && data) {
                    let param = {
                        voucher: data,
                        titlename: self.state.json['2002130210-000000'],/* 国际化处理： 制单*/
                        backUrl: '/confirm',
                        backAppCode: self.props.getUrlParam('backAppCode') || self.props.getSearchParam('c'),
                        backPageCode: self.props.getUrlParam('backPageCode') || self.props.getSearchParam('p'),
                    }
                    pushToLinkVoucher(self,param);
                    // setDefData('voucher_coorder', dataSourceCoord, data);
                    // let voucherapp=voucherRelatedApp(voucher_gen);
                    // self.props.pushTo(
                    //     '/Welcome',
                    //     {
                    //         appcode: voucherapp.appcode,//'20020PREPA',
                    //         ifshowQuery: true,
                    //         n: self.state.json['2002130210-000000'],/* 国际化处理： 制单*/
                    //         status: 'browse',
                    //         backUrl: '/confirm',
                    //         pagekey: 'link',
                    //         backAppCode: self.props.getUrlParam('backAppCode') || self.props.getSearchParam('c'),
                    //         backPageCode: self.props.getUrlParam('backPageCode') || self.props.getSearchParam('p'),
                    //     }
                    // );
                } else {

                }
            }
        });
        // this.props.pushTo(
        //     '/Welcome',
        //      {appcode:'20020PREPA', ifshowQuery:true,n:'制单' }
        //     // state: {
        //     // 	id: '111'
        //     // }}
        // );
    }
    refreshData = () => {
        let self = this;
        let { queryCondition, selectArr, tableData } = self.state;
        let url = "/nccloud/gl/reconcile/reconciledataquery.do";
        selectArr = [];
        tableData = [];
        ajax({
            url: url,
            data: queryCondition,
            success: function (response) {
                const { data, error, success } = response;
                if (success && data) {
                    self.props.getMainDataUnConfirm(data, queryCondition);
                } else {
                    self.props.getMainDataUnConfirm([], queryCondition)
                }
            }
        });
        self.setState({
            selectArr, tableData
        })
    }
    handleRE = () => {
        let self = this;
        let url = "/nccloud/gl/reconcile/reconciledataquery.do";
        let data = getDefData(1234, 'gl_coord.searchData.unConfirm');
        let { queryCondition } = this.state
        ajax({
            url,
            data,
            success: function (response) {
                const { data, error, success } = response;
                if (success && data) {
                    self.props.getMainDataUnConfirm(data, queryCondition)
                } else {
                    self.props.getMainDataUnConfirm([], queryCondition)
                }
            }
        });
    }
    onButtonClick = (props, id) => {
        switch (id) {
            case 'selfedit':
                this.handleRepare()
                break;
            case 'linkself':
                this.handleTovoucher(1);
                break;
            case 'linkopp':
                this.handleTovoucher(2)
                break;
            default:
                break;
        }
    }
    render() {
        let { tabState, selectArr, confirmModalShow, tableData, queryCondition } = this.state;
        let { createButtonApp } = this.props.button;
        return (
            <div>
                <div className='header table-header coord-buttons'>
                    <div className='btn-group table-button-type'>
                        {createButtonApp({
                            area: 'unconfirm',
                            onButtonClick: this.onButtonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })}
                    </div>
                </div>
                <ConfirmModal
                    confirmModalShow={confirmModalShow}
                    queryCondition={queryCondition}
                    getConfirmModalShow={this.getConfirmModalShow.bind(this)}
                    refreshData={this.refreshData.bind(this)}//协同确认后的刷新操作
                    selectArr={selectArr}
                    tableData={tableData}
                />
            </div>
        )
    }
}
ButtonsUnConfirm = createPage({
    // initTemplate: initTemplate
})(ButtonsUnConfirm);
export default ButtonsUnConfirm;
