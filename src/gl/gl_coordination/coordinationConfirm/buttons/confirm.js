import React,{Component} from 'react';
import {ajax,base,toast,cardCache,createPage,promptBox,getMultiLang} from 'nc-lightapp-front';
const { NCButton, NCButtonGroup,NCMenu:Menu,NCDropdown:Dropdown } = base;
import './index.less';
let {setDefData, getDefData } = cardCache;
import { pushToLinkVoucher } from '../../../public/common/voucherUtils';
class ButtonsConfirm extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            tabState:'3',
            selectArr:[]
        }
    }
    componentWillMount() {
        let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
        getMultiLang({moduleId:'2002130210',domainName:'gl',currentLocale:'simpchn',callback}); 
        let pagecode = this.props.getSearchParam('p')
        let appcode  = this.props.getSearchParam('c')
        let buttonCon = getDefData('buttonCon', 'coord.search.confirm');
        if(buttonCon){
            this.props.button.setButtons(buttonCon);
            this.setState({
                buttonsData:buttonCon
            })
            return;
        }
        let self = this;
        ajax({
            url: '/nccloud/platform/appregister/queryallbtns.do',
            data: {
                pagecode: pagecode,
                appcode: appcode
            },
            success: (res) => {
                if(!res.data){
                    return
                }
                /*  拿到按钮数据，调用setButtons方法将数据设置到页面上，然后执行后续的按钮逻辑 */
                self.props.button.setButtons(res.data);
                this.setState({
                    buttonsData:res.data
                })
                setDefData('buttonCon', 'coord.search.confirm', res.data);  
            }
        }); 
        let {selectArr}=this.state;
        if(selectArr.length){
            this.props.button.setButtonDisabled({linkself:false,linkopp:false,cancelconfirm:false});
        }else{
            this.props.button.setButtonDisabled({linkself:true,linkopp:true,cancelconfirm:true});
        }
    }
    componentWillReceiveProps(nextProps){
        let tabState = nextProps.tabState;
        let selectArr = nextProps.selectArr1;
        if(selectArr.length==0){
            let maindata = nextProps.maindata;
            maindata.map((item,key)=>{
                if(item.isSelected){
                    selectArr.push(item)
                }
            })
        }
        this.setState({
            tabState,
            selectArr
        })
        if(selectArr.length){
            this.props.button.setButtonDisabled({linkself:false,linkopp:false,cancelconfirm:false});
        }else{
            this.props.button.setButtonDisabled({linkself:true,linkopp:true,cancelconfirm:true});
        }
    }
    handleProduce=()=>{
        let selectArr = this.state.selectArr;
        let data=selectArr;
        let self = this;
        let url = "/nccloud/gl/reconcile/reconcilegenerate.do";
        ajax({
            url,
            data,
            success: function(response) { 
                const { data, error, success } = response;
                if (success) {
                //   self.props.getMainData(data)
                    window.location.reload()
                } else {
                    
                }    
            }
        });
    }
    handleCancel=()=>{
        let selectArr = this.state.selectArr;
        let data=selectArr;
        let self = this;
        let url = "/nccloud/gl/reconcile/cancelconfirm.do";
        ajax({
            url,
            data,
            success: function(response) { 
                const { data, error, success } = response;
                if (success) {
                //   self.props.getMainData(data)
                    // window.location.reload()
                    self.handleRE()
                } 
                else {
                    toast({ content: response.message, color: 'warning' });
                }  
            },
            error: function (response) {
				toast({ content: response.message, color: 'warning' });
			}
        });
    }
    handleTovoucher=(type)=>{
        let url;
        if(type==2){
            url = '/nccloud/gl/reconcile/reconcilelinkopp.do'
        }else if(type==1){
            url = '/nccloud/gl/reconcile/reconcilelinkself.do'
        }
        let localData = this.state.selectArr[0];
        localData.isselfquery='N'
        let self = this;
        ajax({
            url,
            data:localData,
            success: function(response) { 
                const { data, error, success } = response;
                if (success&&data) {
                    let param = {
                        voucher: data,
                        titlename: self.state.json['2002130210-000000'],/* 国际化处理： 制单*/
                        backUrl: '/confirm',
                        backAppCode: self.props.getUrlParam('backAppCode') || self.props.getSearchParam('c'),
                        backPageCode: self.props.getUrlParam('backPageCode') || self.props.getSearchParam('p'),
                    }
                    pushToLinkVoucher(self,param);
        //             setDefData('voucher_coorder', dataSourceCoord, data);
        //             let voucherapp=voucherRelatedApp(voucher_gen);
        //             self.props.pushTo(
        //                 '/Welcome',
        //                 {
        //                     appcode:voucherapp.appcode,//'20020PREPA', 
        //                     ifshowQuery:true,
        //                     n:self.state.json['2002130210-000000'],/* 国际化处理： 制单*/
        //                     status:'browse',
        //                     backUrl: '/confirm',
        //                     pagekey:'link',
        //                     backAppCode:self.props.getUrlParam('backAppCode') || self.props.getSearchParam('c'),
        //                     backPageCode:self.props.getUrlParam('backPageCode') || self.props.getSearchParam('p'),   
        //                 }
        //             );
                } else {
                    
                }    
            }
        });
        // this.props.pushTo(
        //     '/Welcome',
        //      {appcode:'20020PREPA', ifshowQuery:true,n:'制单' }
        //     // state: {
        //     // 	id: '111'
        //     // }}
        // );
    }
    handleConfirm=()=>{
        promptBox({
            color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
            title: this.state.json['2002130210-000001'],                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 请注意*/
            content: this.state.json['2002130210-000002'],            // 提示内容,非必输/* 国际化处理： 取消确认将会删除已生成凭证，是否取消确认*/
            noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
            beSureBtnName: this.state.json['2002130210-000003'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
            cancelBtnName: this.state.json['2002130210-000004'],           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
            beSureBtnClick: this.handleCancel,   // 确定按钮点击调用函数,非必输
        })
        
    }
    handleRE=()=>{
        let self = this;
        let url = "/nccloud/gl/reconcile/reconciledataquery.do";
        let data = getDefData(12345, 'gl_coord.searchData.confirm');
        ajax({
            url,
            data,
            success: function(response) { 
                const { data, error, success } = response;
                if (success&&data) {
                  self.props.getMainDataConfirm(data)
                } else {
                    self.props.getMainDataConfirm([])
                }    
            }
        });
    }
    onButtonClick=(props, id)=>{
        switch (id) {
            case 'cancelconfirm':
                this.handleConfirm()
                break;
            case 'linkself':
                this.handleTovoucher(1);
                break;
            case 'linkopp':
                this.handleTovoucher(2)
                break;
            default:
                break;
        }
    }
    render (){
        let {tabState,selectArr}=this.state;
        let { createButtonApp } = this.props.button;
        return(
            <div className='header table-header coord-buttons'>
                <div className='btn-group table-button-type'>
                        {createButtonApp({
                            area: 'confirm',
                            onButtonClick: this.onButtonClick.bind(this), 
                            popContainer: document.querySelector('.header-button-area')
                        })}
                </div>
            </div>
        )
    }
}
ButtonsConfirm = createPage({
	// initTemplate: initTemplate
})(ButtonsConfirm);
export default ButtonsConfirm;
