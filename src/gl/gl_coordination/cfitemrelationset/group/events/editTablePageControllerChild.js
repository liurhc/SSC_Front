import editTablePageControllerGlobal from '../../global/events/editTablePageControllerChild'
import pubUtils from '../../../../public/common/pubUtil'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
class editTablePageControllerChild extends editTablePageControllerGlobal{
    constructor(main, props, meta) {
        let newMeta = {
            ...meta,
            listIds: (meta || {}).listIds || [presetVar.list],
        };
        super(main, props, newMeta);
    }
    /**
     * 设置参照过滤
     * @param {*} meta 
     */
    initRefParam(meta){
        meta[presetVar.list].items.map((one)=>{
            if(one.attrcode == presetVar.listField.srccfitemcode 
                || one.attrcode == presetVar.listField.descfitemcode){
                one.queryCondition = () => {
                    return {
                        TreeRefActionExt:'nccloud.web.gl.ref.CashflowTreeRef4CFIRel',
                        'type': 'group',"isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi'};
                }
            }
            if(one.attrcode == presetVar.listField.descfitemcode){
                one.onlyLeafCanSelect = true;
            }
        })
    }
    /**
     * 查询数据
     * @param {*} key 
     */
    queryData(key){
        requestApi.query({
            data: {['userjson']: JSON.stringify({type:'group', ...pubUtils.getSysCode(this.props)})},
            success: (data) => {
                this.setTableData(data);
            }
        })
    }
    /**
     * 设置行操作上的按钮
     * @param {*} key 
     */
    getListButtons(text, record, index){
        let buttonAry =["DelLine"];
        if(record.values.relationType){
            let relationType=record.values.relationType.value;
            if(relationType=="false"){
                buttonAry=[""];
            }
        }
        return buttonAry;
    }
    /**
     * 保存处理
     * @param {*} data 
     */
    doSave(data){
        requestApi.save({
            props: this.props,
            data: data,
            success: (data) => {
                this.callBackSave(data);
            }
        })
    }
    /**
     * 删行处理
     * @param {*} data 
     */
    doDelLine(AreaId, data, index){
        requestApi.save({
            props: this.props,
            data: data,
            success: (data) => {
                this.callBAckDelLine(AreaId, data, index);
            }
        })
    }
    /**
     * 修改状态
     * @param {*} state 
     */
    setPageState(state){
        switch(state){
            case 'browse':
                this.props.button.setButtonVisible(this.editButtons, false);
                this.props.button.setButtonVisible(this.browseButtons, true);
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'browse');
                })
                this.props.button.setPopContent('DelLine',this.main.state.json['2002130105-000004']);
                if(this.treeId){
                    this.props.syncTree.setNodeDisable(this.treeId, false);
                }
                if(this.headRefDisabled){
                    this.main.setState({[this.headRefDisabled]:false});
                }
            break;
            case 'add':
                this.props.button.setButtonVisible(this.browseButtons, false);
                this.props.button.setButtonVisible(this.editButtons, true);
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'add');
                })
                this.props.button.setPopContent('DelLine');
                if(this.treeId){
                    this.props.syncTree.setNodeDisable(this.treeId, true);
                }
                if(this.headRefDisabled){
                    this.main.setState({[this.headRefDisabled]:true});
                }
                this.setUnAbleData();
            break;
            case 'edit':
                this.props.button.setButtonVisible(this.browseButtons, false);
                this.props.button.setButtonVisible(this.editButtons, true);
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'edit');
                })
                this.props.button.setPopContent('DelLine');
                if(this.treeId){
                    this.props.syncTree.setNodeDisable(this.treeId, true);
                }
                if(this.headRefDisabled){
                    this.main.setState({[this.headRefDisabled]:true});
                }
                //设置不修改数据   
                this.setUnAbleData();     
            break;
        }
    }

    setUnAbleData(){
        let globalTable=null;
        let index=[];
        this.listIds.map((one)=>{
            globalTable=this.props.editTable.getFilterRows(one, 'relationType', 'false');
        })
        let row=globalTable.rows;
        if(row.length>0){
            for(let i=0;i<row.length;i++){
                index.push(row[i].flterIndex);
            }
        }
        this.listIds.map((one)=>{
            this.props.editTable.setEditableRowByIndex(one,index, false);
        })
    }
}
export default editTablePageControllerChild;