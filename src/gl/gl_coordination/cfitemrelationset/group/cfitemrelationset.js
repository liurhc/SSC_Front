import {Component} from 'react';
import {createPage, base,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const {NCSelect, NCIcon} = base;

import editTablePageControllerChild from './events/editTablePageControllerChild'
import presetVar from './presetVar'
import requestApi from './requestApi'
import {ListLayout, Table} from '../../../../fipub/public/components/layout/ListLayout'
import {Header, HeaderButtonArea} from '../../../../fipub/public/components/layout/Header'

import './index.less';



class Cfitemrelationset extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{}
        };
        this.editTablePageControllerChild = new editTablePageControllerChild(this, props);
        // 页面初始化
        // initTemplate.call(this, props);
    }
    componentDidMount() {
        
    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
           window.onbeforeunload = () => {
           let status=this.props.editTable.getStatus(presetVar.list);
            if (status == 'edit' || status == 'add') {
               return this.state.json['2002130105-000003'];
            }
        }
    }
    render() {
        let multiLang = this.props.MutiInit.getIntl(2002); 
        const {editTable, button} = this.props;
        const {createEditTable} = editTable;
        const {createButtonApp} = button;

        return (
            <ListLayout>
                <Header>
                    <HeaderButtonArea>
                        {createButtonApp({
                            area: presetVar.headButtonArea, 
                            buttonLimit: 3,
                            onButtonClick: (props, actionId)=>{this.editTablePageControllerChild.headButtonClick(actionId)}
                        })}
                    </HeaderButtonArea>
                </Header>
                <Table>
                    {createEditTable(presetVar.list, {
                        adaptionHeight:true,
                        onAfterEvent:this.editTablePageControllerChild.afterEditEvent.bind(this),
                        showIndex:true
                    })}
                </Table>
            </ListLayout>
        )
    }
}
Cfitemrelationset = createPage({mutiLangCode: '2002'})(Cfitemrelationset);
export default Cfitemrelationset;