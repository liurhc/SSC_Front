import pubUtils from '../../../public/common/pubUtil'
import {ajax } from 'nc-lightapp-front';
import presetVar from './presetVar'
let requestApiOverwrite = {
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/gl/cfitemrelation/query.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 保存
    save: (opt) => {
        let paramData = {
            [presetVar.list]:opt.data[presetVar.list],
            ['userjson']: JSON.stringify({...{type:'group'}, ...pubUtils.getSysCode(opt.props)})
        };
        ajax({
            url: '/nccloud/gl/cfitemrelation/save.do',
            data: paramData,
            success: opt.success
        });
    }
}
export default  requestApiOverwrite;