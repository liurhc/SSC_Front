import editTablePageController from '../../../../public/common/editTablePageController'
import pubUtils from '../../../../public/common/pubUtil'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
class editTablePageControllerChild extends editTablePageController{
    constructor(main, props, meta) {
        let newMeta = {
            ...meta,
            listIds: (meta || {}).listIds || [presetVar.list],
            listButtonAreas: (meta || {}).listButtonAreas || {[presetVar.list]:presetVar.listButtonArea},
        };
        super(main, props, newMeta);
    }
    /**
     * 设置参照过滤
     * @param {*} meta 
     */
    initRefParam(meta){
        meta[presetVar.list].items.map((one)=>{
            if(one.attrcode == presetVar.listField.srccfitemcode 
                || one.attrcode == presetVar.listField.descfitemcode){
                one.queryCondition = () => {
                    return {
                        TreeRefActionExt:'nccloud.web.gl.ref.CashflowTreeRef4CFIRel',
                        'type': 'global',"isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi'};
                }
            }
            if(one.attrcode == presetVar.listField.descfitemcode){
                one.onlyLeafCanSelect = true;
            }
        })
    }
    /**
     * 查询数据
     * @param {*} key 
     */
    queryData(key){
        requestApi.query({
            data: {['userjson']: JSON.stringify({type:'global', ...pubUtils.getSysCode(this.props)})},
            success: (data) => {
                this.setTableData(data);
            }
        })
    }
    /**
     * 保存处理
     * @param {*} data 
     */
    doSave(data){
        if(data[presetVar.list]){
            let rows=[];
            if(data[presetVar.list].rows.length>0){
                    let check=this.props.editTable.checkRequired(presetVar.list,data[presetVar.list].rows);
                    if(check){
                        requestApi.save({
                            props: this.props,
                            data: data,
                            success: (data) => {
                                this.callBackSave(data);
                            }
                        })
                    }
            }
        }
        
    }
    /**
     * 删行处理
     * @param {*} data 
     */
    doDelLine(AreaId, data, index){
        requestApi.save({
            props: this.props,
            data: data,
            success: (data) => {
                this.callBAckDelLine(AreaId, data, index);
            }
        })
    }

    /**
     * 表单编辑后事件
     * @param {*} props 
     * @param {string moduleId}  区域ID
     * @param {string key} 编辑字段
     * @param {*} value 
     * @param {*} changedrows 
     * @param {*} index 
     * @param {*} record 
     */
    afterEditEvent(props, moduleId, key, value, changedrows, index, record) {
        switch(key){
            case presetVar.listField.srccfitemcode:
                props.editTable.setValByKeyAndIndex(presetVar.list, index, presetVar.listField.srccfitempk, {
                    value:value.refpk
                });
                props.editTable.setValByKeyAndIndex(presetVar.list, index, presetVar.listField.srccfitemcode, {
                    value:value.refcode,
                    display:value.refcode
                });
                props.editTable.setValByKeyAndIndex(presetVar.list, index, presetVar.listField.srccfitemname, {
                    value:value.refname,
                    display:value.refname
                });
            break;
            case presetVar.listField.descfitemcode:
                props.editTable.setValByKeyAndIndex(presetVar.list, index, presetVar.listField.descfitempk, {
                    value:value.refpk
                });
                props.editTable.setValByKeyAndIndex(presetVar.list, index, presetVar.listField.descfitemcode, {
                    value:value.refcode,
                    display:value.refcode
                });
                props.editTable.setValByKeyAndIndex(presetVar.list, index, presetVar.listField.descfitemname, {
                    value:value.refname,
                    display:value.refname
                });
            break;
        }
    }
}
export default editTablePageControllerChild;