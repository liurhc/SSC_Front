/**页面全局变量 */
let currentVar = {
    pageId: 'xjllxmdz',
    list: 'cfitemrelation',
    headButtonArea: 'list_head',
    listButtonArea: 'list_inner',
    /**
     * 列表字段
     */
    listField:{
        /**
         * 来源现金流量项目PK
         */
        srccfitempk:'srccfitempk',
        /**
         * 来源现金流量项目编码
         */
        srccfitemcode:'srccfitemcode',
        /**
         * 来源现金流量项目名称
         */
        srccfitemname:'srccfitemname',
        /**
         * 目的现金流量项目PK
         */
        descfitempk:'descfitempk',
        /**
         * 目的现金流量项目编码
         */
        descfitemcode:'descfitemcode',
        /**
         * 目的现金流量项目名称
         */
        descfitemname:'descfitemname',
    }
}
export default currentVar

