import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { base,cardCache,getMultiLang,createPage } from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
import SearchComponent from '../search';
import ButtonsComponent from '../buttons';
import ButtonsConfirm from '../buttons/confirm';
import ButtonsUnConfirm from '../buttons/unConfirm';
import CoorTable from '../table';
import CoorTableConfirm from '../table/confirm';
import CoorTableUnConfirm from '../table/unConfirm';
import SearchConfirm from '../search/confirm';
import SearchUnConfirm from '../search/unConfirm';
import './index.less';
const { NCTabs,NCDiv } = base;
const NCTabPane = NCTabs.NCTabPane;
class CoordinationCenter extends Component {
    constructor(props){
        super(props);
        this.state={
            json:{},
            maindata:[],
            mainSe:[],
            tabState:'3',
            selectArr:[],
            selectArr1:[],
            selectArr2:[],
            MainDataConfirm:[],
            MainDataConfirmSe:[],
            MainDataUnConfirm:[],
            MainDataUnConfirmSe:[],
            reHandleSearch:false,
            reHandleSearchUnconfirm:false,
            conShow:false,
            unconShow:false
        }
    }
    componentWillMount(){
        let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
        getMultiLang({moduleId:'2002130205',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentDidMount(){
        let tabState = getDefData(111, 'gl_obervision.discount.tabState');
        if(tabState){
            this.setState({
                tabState
            })
        }
    }
    checkForm = (flag,obj) => {
    }
    getMainData=(data)=>{//获取查询数据
        this.setState({
            maindata:data,
            reHandleSearch:false,
            selectArr:[]
        })
    }
    getMainDataConfirm=(data)=>{
        this.setState({
            MainDataConfirm:data,
            selectArr1:[]
        })
    }
    getMainDataUnConfirm=(data)=>{
        this.setState({
            MainDataUnConfirm:data,
            reHandleSearchUnconfirm:false,
            selectArr2:[]
        })
    }
    getSelectArr=(data,maindata)=>{
        let mainSe = [];
        if(maindata&&maindata.length>0){
            for(let i=0,len=maindata.length;i<len;i++){
                if(maindata[i].isSelected){
                    mainSe.push(i)
                }
            }
        }
        this.setState({
            selectArr:data,
            mainSe:mainSe
        })
    }
    getSelectArr1=(data,maindata)=>{
        let MainDataUnConfirmSe = [];
        if(maindata&&maindata.length>0){
            for(let i=0,len=maindata.length;i<len;i++){
                if(maindata[i].isSelected){
                    MainDataUnConfirmSe.push(i)
                }
            }
        }
        this.setState({
            selectArr1:data,
            MainDataUnConfirmSe
        })
    }
    clearSelectArr1=()=>{
        this.setState({MainDataUnConfirmSe:[]})
    }
    getSelectArr2=(data,maindata)=>{
        let MainDataConfirmSe = [];
        if(maindata&&maindata.length>0){
            for(let i=0,len=maindata.length;i<len;i++){
                if(maindata[i].isSelected){
                    MainDataConfirmSe.push(i)
                }
            }
        }
        this.setState({
            selectArr2:data,
            MainDataConfirmSe
        })
    }
    reHandleSearch=(data)=>{
        this.setState({
            reHandleSearch:data
        })
    }
    reHandleSearchUnconfirm=(data)=>{
        this.setState({
            reHandleSearchUnconfirm:data,
            MainDataUnConfirmSe:[]
        })
    }
    setConShow=(data)=>{//设置已确认表格长度
        this.setState({
            conShow:data
        })
    }
    setUnConShow=(data)=>{//设置未确认表格长度
        this.setState({
            unconShow:data
        })
    }
    render() {
        let {maindata,tabState,selectArr,selectArr1,selectArr2,MainDataConfirm,MainDataUnConfirm,
            reHandleSearch,reHandleSearchUnconfirm,mainSe,MainDataConfirmSe,MainDataUnConfirmSe,conShow,unconShow}=this.state;
        return (
            <div className='coord-main'>
                <NCDiv areaCode={NCDiv.config.HEADER}>
                    <NCDiv fieldid={this.props.getSearchParam('n')} areaCode={NCDiv.config.Title}></NCDiv>
                </NCDiv>
                <NCTabs navtype="turn" contenttype="moveleft" activeKey={tabState} onChange={(v)=>{
                    this.setState({
                        tabState:v
                    },()=>{
                        setDefData(111, 'gl_obervision.discount.tabState',v);
                    })
                    
                }}>
                    <NCTabPane tab={this.state.json['2002130205-000001']} key="3">
                        <div className="nc-bill-list">
                            <div className="nc-bill-search-area">
                                <SearchComponent 
                                    getMainData={this.getMainData.bind(this)}
                                    tabState={tabState}
                                    mainSe={mainSe}
                                    reHandleSearch={reHandleSearch}
                                />
                            </div>
                        </div>
                        <NCDiv fieldid="center" areaCode={NCDiv.config.TABLE}>
                        <ButtonsComponent tabState={tabState} 
                            selectArr={selectArr}
                            getMainData={this.getMainData.bind(this)}
                            reHandleSearch={this.reHandleSearch.bind(this)}
                            maindata={maindata}
                            className={maindata.length>0?'':'display-none-coord'}
                        />
                        <CoorTable maindata={maindata} 
                            className={maindata.length>0?'':'display-none-coord'}
                            getSelectArr={this.getSelectArr.bind(this)}
                        />
                        </NCDiv>
                    </NCTabPane>
                    <NCTabPane tab={this.state.json['2002130205-000002']} key="1">
                        <div className="nc-bill-list">
                            <div className="nc-bill-search-area">
                                <SearchUnConfirm
                                    getMainDataUnConfirm={this.getMainDataUnConfirm.bind(this)}
                                    tabState={tabState}
                                    MainDataUnConfirmSe={MainDataUnConfirmSe}
                                    setUnConShow={this.setUnConShow.bind(this)}
                                    reHandleSearchUnconfirm={reHandleSearchUnconfirm}
                                />
                            </div>
                        </div>
                        <NCDiv fieldid="center" areaCode={NCDiv.config.TABLE}>
                        <ButtonsUnConfirm tabState={tabState} 
                            selectArr1={selectArr1}
                            MainDataUnConfirm={MainDataUnConfirm}
                            clearSelectArr1={this.clearSelectArr1.bind(this)}
                            getMainDataUnConfirm={this.getMainDataUnConfirm.bind(this)}
                            reHandleSearchUnconfirm={this.reHandleSearchUnconfirm.bind(this)}
                            className={MainDataUnConfirm.length>0?'':'display-none-coord'}
                        />
                        <CoorTableUnConfirm maindata={MainDataUnConfirm}
                            className={MainDataUnConfirm.length>0?'':'display-none-coord'}
                            getSelectArr1={this.getSelectArr1.bind(this)}
                            unconShow={unconShow}
                        />
                        </NCDiv>
                    </NCTabPane>
                    <NCTabPane tab={this.state.json['2002130205-000003']} key="2">
                        <div className="nc-bill-list">
                            <div className="nc-bill-search-area">
                                <SearchConfirm 
                                    getMainDataConfirm={this.getMainDataConfirm.bind(this)}
                                    MainDataConfirmSe={MainDataConfirmSe}
                                    tabState={tabState}
                                    setConShow={this.setConShow.bind(this)}
                                />
                            </div>
                        </div>
                        <NCDiv fieldid="center" areaCode={NCDiv.config.TABLE}>
                        <ButtonsConfirm tabState={tabState} 
                            selectArr2={selectArr2}
                            getMainDataConfirm={this.getMainDataConfirm.bind(this)}
                            MainDataConfirm={MainDataConfirm}
                            className={MainDataConfirm.length>0?'':'display-none-coord'}
                        />
                        <CoorTableConfirm maindata={MainDataConfirm}
                            getSelectArr2={this.getSelectArr2.bind(this)}
                            className={MainDataConfirm.length>0?'':'display-none-coord'}
                            conShow={conShow}
                        />
                        </NCDiv>
                    </NCTabPane>
                </NCTabs>
            </div>
        )
    }
}
CoordinationCenter = createPage({

})(CoordinationCenter);
export default CoordinationCenter;
