import React,{Component} from 'react';
import {ajax,base,createPage,cardCache,getMultiLang} from 'nc-lightapp-front';
import {dataSourceCoord} from '../../../public/components/constJSON';
const { NCButton, NCButtonGroup,NCMenu:Menu,NCDropdown:Dropdown } = base;
let {setDefData, getDefData } = cardCache;
import { voucher_gen } from '../../../public/components/constJSON';
// import { voucherRelatedApp } from '../../../public/components/oftenApi';
import { pushToLinkVoucher } from '../../../public/common/voucherUtils';
import './index.less';
class ButtonsConfirm extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            tabState:'3',
            selectArr:[]
        }
    }
    componentWillReceiveProps(nextProps){
        let tabState = nextProps.tabState;
        let selectArr = nextProps.selectArr2;
        this.setState({
            tabState,
            selectArr
        })
        if(selectArr.length==0){
            let maindata = nextProps.MainDataConfirm;
            maindata.map((item,key)=>{
                if(item.isSelected){
                    selectArr.push(item)
                }
            })
        }
        if(selectArr.length){
            this.props.button.setButtonDisabled({linkself:false,linkopp:false});
        }else{
            this.props.button.setButtonDisabled({linkself:true,linkopp:true});
        }
    }
    componentWillMount() {
        let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
        getMultiLang({moduleId:'2002130205',domainName:'gl',currentLocale:'simpchn',callback}); 
        let pagecode = this.props.getSearchParam('p')
        let appcode  = this.props.getSearchParam('c')
        let buttonCon = getDefData('buttonCon', 'coord.search.confirm');
        if(buttonCon){
            this.props.button.setButtons(buttonCon);
            this.setState({
                buttonsData:buttonCon
            })
            return;
        }
        let self = this;
        ajax({
            url: '/nccloud/platform/appregister/queryallbtns.do',
            data: {
                pagecode: pagecode,
                appcode: appcode
            },
            success: (res) => {
                if(!res.data){
                    return
                }
                /*  拿到按钮数据，调用setButtons方法将数据设置到页面上，然后执行后续的按钮逻辑 */
                self.props.button.setButtons(res.data);
                this.setState({
                    buttonsData:res.data
                })
                setDefData('buttonCon', 'coord.search.confirm', res.data); 
            }
        }); 
        let {selectArr}=this.state;
        if(selectArr.length){
            this.props.button.setButtonDisabled({linkself:false,linkopp:false});
        }else{
            this.props.button.setButtonDisabled({linkself:true,linkopp:true});
        }
    }
    handleProduce=()=>{
        let selectArr = this.state.selectArr;
        let data=selectArr;
        let self = this;
        let url = "/nccloud/gl/reconcile/reconcilegenerate.do";
        ajax({
            url,
            data,
            success: function(response) { 
                const { data, error, success } = response;
                if (success) {
                //   self.props.getMainData(data)
                self.handleRE()
                } else {
                    
                }    
            }
        });
    }
    handleCancel=()=>{
        let selectArr = this.state.selectArr;
        let data=selectArr;
        let self = this;
        let url = "/nccloud/gl/reconcile/cancelreconcile.do";
        ajax({
            url,
            data,
            success: function(response) { 
                const { data, error, success } = response;
                if (success) {
                //   self.props.getMainData(data)
                self.handleRE()
                } else {
                    
                }    
            }
        });
    }
    handleTovoucher=(type)=>{
        let url;
        if(type==2){
            url = '/nccloud/gl/reconcile/reconcilelinkopp.do'
        }else if(type==1){
            url = '/nccloud/gl/reconcile/reconcilelinkself.do'
        }
        let localData = this.state.selectArr[0];
        localData.isselfquery='Y'
        let self = this;
        ajax({
            url,
            data:localData,
            success: function(response) { 
                const { data, error, success } = response;
                if (success&&data) {
                    let param = {
                        voucher: data,
                        titlename: self.state.json['2002130205-000000'],/* 国际化处理： 制单*/
                        backUrl: '/center',
                        backAppCode: self.props.getUrlParam('backAppCode') || self.props.getSearchParam('c'),
                        backPageCode: self.props.getUrlParam('backPageCode') || self.props.getSearchParam('p'),
                    }
                    pushToLinkVoucher(self,param);
        //             setDefData('voucher_coorder', dataSourceCoord, data);
        //             let voucherapp=voucherRelatedApp(voucher_gen);
        //             self.props.pushTo(
        //                 '/Welcome',
        //                 {
        //                     appcode:voucherapp.appcode,//'20020PREPA', 
        //                     ifshowQuery:true,
        //                     n:self.state.json['2002130205-000000'],/* 国际化处理： 制单*/
        //                     status:'browse',
        //                     backUrl: '/center',
        //                     pagekey:'link',
        //                     backAppCode:self.props.getUrlParam('backAppCode') || self.props.getSearchParam('c'),
        //                     backPageCode:self.props.getUrlParam('backPageCode') || self.props.getSearchParam('p'),
        //                 }
        //                 // state: {
        //                 // 	id: '111'
        //                 // }}
        //             );
                } else {
                    
                }    
            }
        });
    }
    handleRE=()=>{//刷新
        let self = this;
        let url = "/nccloud/gl/reconcile/reconciledataquery.do";
        let data = getDefData(123, 'gl_coord.searchData.confirm');
        ajax({
            url,
            data,
            success: function(response) { 
                const { data, error, success } = response;
                if (success&&data) {
                  self.props.getMainDataConfirm(data)
                } else {
                    self.props.getMainDataConfirm([])
                }    
            }
        });
    }
    onButtonClick=(props, id)=>{
        switch (id) {
            case 'linkself':
                this.handleTovoucher(1);
                break;
            case 'linkopp':
                this.handleTovoucher(2)
                break;
            default:
                break;
        }
    }
    render (){
        let {tabState,selectArr}=this.state;
        let { createButtonApp } = this.props.button;
        return(
            <div className='header table-header coord-buttons'>
                <div className='btn-group table-button-type'>
                        {createButtonApp({
                            area: 'confirm',
                            onButtonClick: this.onButtonClick.bind(this), 
                            popContainer: document.querySelector('.header-button-area')
                        })}
                </div>
            </div>
        )
    }
}
ButtonsConfirm = createPage({
	// initTemplate: initTemplate
})(ButtonsConfirm);
export default ButtonsConfirm;
