import React, { Component } from 'react';
import { ajax, base, cardCache, createPage, getMultiLang } from 'nc-lightapp-front';
const { NCButton, NCButtonGroup, NCMenu: Menu, NCDropdown: Dropdown } = base;
import './index.less';
import { toast } from '../../../public/components/utils';
import { dataSourceCoord } from '../../../public/components/constJSON';
let { setDefData, getDefData } = cardCache;
// import { voucher_gen } from '../../../public/components/constJSON';
// import { voucherRelatedApp } from '../../../public/components/oftenApi';
import { pushToLinkVoucher } from '../../../public/common/voucherUtils';
class ButtonsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            tabState: '3',
            selectArr: []
        }
    }
    componentWillMount() {
        let callback = (json) => {
            this.setState({ json: json }, () => {  })
        }
        getMultiLang({ moduleId: '2002130205', domainName: 'gl', currentLocale: 'simpchn', callback });
        let pagecode = this.props.getSearchParam('p')
        let appcode = this.props.getSearchParam('c')
        let buttonIndex = getDefData('buttonIndex', 'coord.search.index');
        if (buttonIndex) {
            this.props.button.setButtons(buttonIndex);
            this.setState({
                buttonsData: buttonIndex
            })
            return;
        }
        let self = this;
        ajax({
            url: '/nccloud/platform/appregister/queryallbtns.do',
            data: {
                pagecode: pagecode,
                appcode: appcode
            },
            success: (res) => {
                if (!res.data) {
                    return
                }
                /*  拿到按钮数据，调用setButtons方法将数据设置到页面上，然后执行后续的按钮逻辑 */
                self.props.button.setButtons(res.data);
                this.setState({
                    buttonsData: res.data
                })
                setDefData('buttonIndex', 'coord.search.index', res.data);
            }
        });
        let { selectArr } = this.state;
        if (selectArr.length) {
            this.props.button.setButtonDisabled({ linkself: false, linkopp: false, reconcile: false });
        } else {
            this.props.button.setButtonDisabled({ linkself: true, linkopp: true, reconcile: true });
        }
    }
    componentWillReceiveProps(nextProps) {
        let tabState = nextProps.tabState;
        let selectArr = nextProps.selectArr;
        this.setState({
            tabState,
            selectArr
        })
        if (selectArr.length == 0) {
            let maindata = nextProps.maindata;
            maindata.map((item, key) => {
                if (item.isSelected) {
                    selectArr.push(item)
                }
            })
        }
        if (selectArr.length) {
            this.props.button.setButtonDisabled({ linkself: false, linkopp: false, reconcile: false });
        } else {
            this.props.button.setButtonDisabled({ linkself: true, linkopp: true, reconcile: true });
        }
    }
    handleProduce = () => {//协同生成
        let selectArr = this.state.selectArr;
        let data = selectArr;
        let self = this;
        let url = "/nccloud/gl/reconcile/reconcilegenerate.do";
        ajax({
            url,
            data,
            success: function (response) {
                const { data, error, success } = response;
                if (success) {
                    if (data) {
                        toast({ content: data, color: 'warning' });
                    }
                    self.props.reHandleSearch(true)
                    // window.location.reload()
                } else {

                }
            }
        });
    }
    handleCancel = () => {
        let selectArr = this.state.selectArr;
        let data = selectArr;
        let self = this;
        let url = "/nccloud/gl/reconcile/cancelreconcile.do";
        ajax({
            url,
            data,
            success: function (response) {
                const { data, error, success } = response;
                if (success) {
                    //   self.props.getMainData(data)
                    self.handleRE()
                } else {

                }
            }
        });
    }
    handleTovoucher = (type) => {
        let url;
        if (type == 2) {
            url = '/nccloud/gl/reconcile/reconcilelinkopp.do'
        } else if (type == 1) {
            url = '/nccloud/gl/reconcile/reconcilelinkself.do'
        }
        let localData = this.state.selectArr[0];
        localData.isselfquery = 'Y'
        let self = this;
        ajax({
            url,
            data: localData,
            success: function (response) {
                const { data, error, success } = response;
                if (success && data) {
                    let param = {
                        voucher: data,
                        titlename: self.state.json['2002130205-000000'],/* 国际化处理： 制单*/
                        backUrl: '/center',
                        backAppCode: self.props.getUrlParam('backAppCode') || self.props.getSearchParam('c'),
                        backPageCode: self.props.getUrlParam('backPageCode') || self.props.getSearchParam('p'),
                    }
                    pushToLinkVoucher(self,param);
                    // setDefData('voucher_coorder', dataSourceCoord, data);
                    // let voucherapp=voucherRelatedApp(voucher_gen);
                    // self.props.pushTo(
                    //     '/Welcome',
                    //     {
                    //         appcode:voucherapp.appcode,// '20020PREPA',
                    //         ifshowQuery: true,
                    //         n: self.state.json['2002130205-000000'],/* 国际化处理： 制单*/
                    //         status: 'browse',
                    //         backUrl: '/center',
                    //         pagekey: 'link',
                    //         backAppCode: self.props.getUrlParam('backAppCode') || self.props.getSearchParam('c'),
                    //         backPageCode: self.props.getUrlParam('backPageCode') || self.props.getSearchParam('p'),
                    //     }
                    //     // state: {
                    //     // 	id: '111'
                    //     // }}
                    // );
                } else {

                }
            }
        });
        // this.props.pushTo(
        //     '/Welcome',
        //      {appcode:'20020PREPA', ifshowQuery:true,n:'制单' }
        //     // state: {
        //     // 	id: '111'
        //     // }}
        // );
    }
    handleRE = () => {//刷新
        let self = this;
        let url = "/nccloud/gl/reconcile/reconciledataquery.do";
        let data = getDefData(1234, 'gl_coord.searchData.indexData');
        ajax({
            url,
            data,
            success: function (response) {
                const { data, error, success } = response;
                if (success && data) {
                    self.props.getMainData(data)
                } else {
                    self.props.getMainData([])
                }
            }
        });
    }
    onButtonClick = (props, id) => {
        switch (id) {
            case 'reconcile':
                this.handleProduce()
                break;
            case 'linkself':
                this.handleTovoucher(1);
                break;
            default:
                break;
        }
    }
    render() {
        let { tabState, selectArr } = this.state;
        let { createButtonApp } = this.props.button;
        return (
            <div className='header table-header coord-buttons'>
                <div className='btn-group table-button-type'>
                    {createButtonApp({
                        area: 'unreconcile',
                        onButtonClick: this.onButtonClick.bind(this),
                        popContainer: document.querySelector('.header-button-area')
                    })}
                </div>
            </div>
        )
    }
}
ButtonsComponent = createPage({
    // initTemplate: initTemplate
})(ButtonsComponent);
export default ButtonsComponent;
