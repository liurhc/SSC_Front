import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { high, ajax, base, createPage, getMultiLang, toast } from 'nc-lightapp-front';
import { gzip } from 'nc-lightapp-front';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
	NCRow: Row, NCCol: Col, NCTree: Tree, NCMessage: Message, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
	NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCButtonGroup: ButtonGroup, NCModal: Modal
} = base;
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';

import { json } from 'graphlib';
class FuncPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json:{},
			showModal: false,
			mainData: [],
			selectArr: [],
			selFunction: '',
			checkedAll: false,//是否全选
			selectData: [],//选中数据
			selIds: [],//选中行号
			checkedArray: [//各行选中判断

			],
			localenv: ''
		};
	}
	componentWillMount() {
		let self = this;
		let url = "/nccloud/gl/accountrep/expandfun.do";

		let gziptools = new gzip();
		let getParm = this.props.getUrlParam('param');
		let paramData = gziptools.unzip(getParm)

		let localenv = '', localFunc = '';
		localFunc = paramData.function;
		localenv = paramData.env;
		this.setState({
			selFunction: localFunc,
			localenv: localenv
		})
		
		let callback= (json) =>{
			this.setState({
			  json:json,
			},()=>{
				let data = { "function": localFunc };
				if (!data.function) {
					data = { "function": json['2002130205-000036'] };//GLCLOSEBAL(1001,2019,01,,借,本币,Y,001,组织本币,0001)
				}
				ajax({
					url,
					data,
					success: function (response) {
						const { data, error, success } = response;
						if (success && data) {
							let mainData = []
							let dataArr = [];
							for (let i = 0, len = data.length; i < len; i++) {
								dataArr.push(JSON.parse(JSON.stringify({
									value: data[i],
									key: i + 1
								})))
							}
							self.setState({
								mainData: dataArr
							})
							self.onAllCheckChange();
						} else {
							self.setState({
								mainData: []
							})
						}
					}
				});
				// initTemplate.call(this, this.props);
			})
		  }
		  getMultiLang({moduleId:'2002130205',domainName:'gl',currentLocale:'simpchn',callback});
	}
	componentWillReceiveProps(nextProps) {
		//    this.setState({
		//         showModal:nextProps.showMore
		//    })
	}
	linkAll = (type) => {//联查汇总数
		let self = this;
		let url = "/nccloud/gl/accountrep/ufolink.do";
		let getParm = this.props.getUrlParam;
		// let data = {"function":getParm('function')};
		if (self.state.selectData.length == 0) {
			toast({ content: this.state.json['2002130205-000037'], color: 'warning' });/* 国际化处理： 未选择公式，请选择*/
			return
		}
		let localArr = [];
		for (let i = 0, len = self.state.selectData.length; i < len; i++) {
			localArr.push(self.state.selectData[i].value)
		}
		let data = {
			"env": JSON.parse(self.state.localenv)
			, "isBal": type == 2 ? "N" : "Y", "functions": localArr
		};
		ajax({
			url,
			data,
			success: function (response) {
				const { data, error, success } = response;
				if (success && data) {
					data.env = self.state.localenv;// 环境变量
					let gziptools = new gzip();
					self.props.openTo(data.url,
						{ status: gziptools.zip(JSON.stringify(data)), appcode: data.appcode }
					)
				} else {
				}
			}
		});
	}
	//处理多选
	onAllCheckChange = () => {//全选
		let self = this;
		let checkedArray = [];
		let mainData = this.state.mainData;
		let listData = self.state.mainData.concat();
		let selIds = [];
		// let id = self.props.multiSelect.param;
		for (var i = 0; i < mainData.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
			selIds.push(i);
		}
		if (self.state.checkedAll) {
			selIds = []
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			selIds: selIds,
			checkedArray
		});
		// self.props.setCheckedArray(checkedArray)

		let selectArr = [];
		for (let i = 0; i < selIds.length; i++) {
			selectArr.push(JSON.parse(JSON.stringify(mainData[selIds[i]])));
		}
		this.setState({
			selectData: selectArr
		})
	};
	onCheckboxChange = (text, record, index) => {//单选
		let self = this;
		let allFlag = false;
		let selIds = self.state.selIds;
		// let id = self.props.postId;
		let checkedArray = self.state.checkedArray.concat();
		if (self.state.checkedArray[index]) {
			selIds.splice(record.key, 1);
			for (let i = 0, len = selIds.length; i < len; i++) {
				if (record.key == selIds[i]) {
					selIds.splice(i, 1)
					break;
				}
			}
		} else {
			selIds.push(record.key);
		}
		checkedArray[index] = !self.state.checkedArray[index];
		for (let i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		self.setState({
			checkedAll: allFlag,
			selIds: selIds,
			checkedArray
		});
		// self.props.setCheckedArray(checkedArray)
		let mainData = this.state.mainData;
		let selectArr = [];
		selIds = this.sortarr(selIds)
		for (let i = 0; i < selIds.length; i++) {
			for (let j = 0; j < mainData.length; j++) {
				if (selIds[i] == mainData[j].key) {
					selectArr.push(JSON.parse(JSON.stringify(mainData[j])));
				}
			}
		}
		this.setState({
			selectData: selectArr
		})
	};
	renderColumnsMultiSelect(columns) {
		const { data, checkedArray, mainData } = this.state;
		let { multiSelect } = this.props;
		let select_column = {};
		let indeterminate_bool = false;
		// multiSelect= "checkbox";
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === "checkbox") {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
						<Checkbox
							className="table-checkbox"
							width="60px"
							checked={this.state.checkedAll}
							indeterminate={indeterminate_bool && !this.state.checkedAll}
							onChange={this.onAllCheckChange}
						/>
					),
					key: "checkbox",
					dataIndex: "checkbox",
					width: "60px",
					render: (text, record, index) => {
						return (
							<Checkbox
								className="table-checkbox"
								checked={this.state.checkedArray[index]}
								onChange={this.onCheckboxChange.bind(this, text, record, index)}
							/>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}
	sortarr = (arr) => {//排序
		for (let i = 0; i < arr.length - 1; i++) {
			for (let j = 0; j < arr.length - 1 - i; j++) {
				if (arr[j] > arr[j + 1]) {
					var temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}
		return arr;
	}
	render() {
		let { pk_accountingbook, mainData, selFunction } = this.state;
		let columns = this.renderColumnsMultiSelect(
			[
				{
					title: this.state.json['2002130205-000038'],/* 国际化处理： 公式*/
					dataIndex: "reconcileno",
					key: "reconcileno",
					width: '400px',
					render: (text, record, index) => (
						<span>
							{this.state.mainData[index].value}
						</span>
					)
				}
			]
		);
		return (
			<div className='funcpage nc-bill-list'>
				<HeaderArea
					title={this.state.json['2002130205-000032']}//{this.state.json['20020RECON-000018']}/* 国际化处理： 总账函数列表*/
					btnContent={
						<div>
							<Button fieldid="linkAll" onClick={this.linkAll.bind(this)}>{this.state.json['2002130205-000033']}</Button>
							<Button fieldid="linkDetails" colors="primary" onClick={this.linkAll.bind(this, 2)}>{this.state.json['2002130205-000034']}</Button>
						</div>
					}
				/>
				{/* <div className='tittle'>
              <h1>总账函数列表</h1>
          </div> */}
				<div className='content'>
					<div className="nc-bill-search-area">
						<span className="title">{selFunction}</span>
						{/* <input name='explanation' className="explanation" type='text' value={selFunction}
							autocomplete="off"
							placeholder={'函数'}
						/> */}
					</div>
					<Table
						columns={columns}
						data={mainData}
						bordered
						className='glCoorditonTable'
					// scroll={{ x:true,y:}}
					></Table>

				</div>
			</div>
		)
	}
}
FuncPage = createPage({})(FuncPage);
FuncPage.defaultProps = {
	prefixCls: "bee-table",
	multiSelect: {
		type: "checkbox",
		param: "key"
	}
};
// export default FuncPage;
ReactDOM.render(<FuncPage />, document.querySelector('#app'));
