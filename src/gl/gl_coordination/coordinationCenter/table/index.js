import React, { Component } from "react";
import {ajax,base,getMultiLang} from 'nc-lightapp-front';
const { NCButton, NCButtonGroup,NCMenu:Menu,NCTable,NCButton:Button,NCCheckbox:Checkbox,NCTooltip } = base;
import {getTableHeight } from '../../../public/common/method.js';
import './index.less';
export default class CoorTable extends Component {
  constructor(props){
    super(props);
    this.state = {
      json:{},
      mainData: [],
      checkedAll:false,
      selectData:[],
      selIds:[],
      indeterminate_bool:false,
      checkedArray: []
    };
    this.columns=[];
  }  
  componentWillMount(){
    let callback= (json) =>{
      this.columns=[
        {
          title: (<div fieldid="reconcileno" className="mergecells">{json['2002130205-000024']}</div>),/* 国际化处理： 协同号*/
          dataIndex: "reconcileno",
          key: "reconcileno",
          width:'120px',
          render: (text, record, index) => (
            <div>
              {this.state.mainData[index].reconcileno?this.state.mainData[index].reconcileno:<span>&nbsp;</span>}
            </div>
          )
        },
        {
          title: (<div fieldid="reconciledate" className="mergecells">{json['2002130205-000014']}</div>),/* 国际化处理： 协同日期*/
          dataIndex: "reconciledate",
          key: "reconciledate",
          width:'120px',
          render: (text, record, index) => (
            <div>
            {this.state.mainData[index].reconciledate?this.state.mainData[index].reconciledate:<span>&nbsp;</span>}</div>
          )
        },
        {
          title: (<div fieldid="reconciler" className="mergecells">{json['2002130205-000007']}</div>),/* 国际化处理： 协同人*/
          dataIndex: "reconciler",
          key: "reconciler",
          width:'120px',
          render: (text, record, index) => (
            <div>
            {this.state.mainData[index].reconciler?this.state.mainData[index].reconciler:<span>&nbsp;</span>}</div>
          )
        },
        {
          title: json['2002130205-000023'],/* 国际化处理： 对方凭证*/
          children: [
            {
              title: (<div fieldid="opp_bookname" className="mergecells">{json['2002130205-000025']}</div>),/* 国际化处理： 对方核算账簿*/
              dataIndex: "opp_bookname",
              key: "opp_bookname",
              width:'150px',
              render: (text, record, index) => (
                <div >
                {this.state.mainData[index].opp_bookname?this.state.mainData[index].opp_bookname:<span>&nbsp;</span>}</div>
              )
            },
            {
              title: (<div fieldid="opp_explanation" className="mergecells">{json['2002130205-000026']}</div>),/* 国际化处理： 摘要*/
              dataIndex: "opp_explanation",
              key: "opp_explanation",
              width:'120px',
              render: (text, record, index) => (
                <NCTooltip trigger="hover" placement="top" inverse={false} overlay={this.state.mainData[index].opp_explanation}>
                <div className={this.state.mainData[index].opp_explanation?'nowRap':'nowRap display-none'}>
                {this.state.mainData[index].opp_explanation?this.state.mainData[index].opp_explanation:<span>&nbsp;</span>}</div>
                </NCTooltip>
              )
            },
            {
              title: (<div fieldid="opp_voucherno" className="mergecells">{json['2002130205-000027']}</div>),/* 国际化处理： 凭证号*/
              dataIndex: "opp_voucherno",
              key: "opp_voucherno",
              width:'120px',
              render: (text, record, index) => (
                <div>
                  {this.state.mainData[index].opp_voucherno?this.state.mainData[index].opp_voucherno:<span>&nbsp;</span>}
                </div>
              )
            },
            {
              title: (<div fieldid="opp_preparer" className="mergecells">{json['2002130205-000006']}</div>),/* 国际化处理： 制单人*/
              dataIndex: "opp_preparer",
              key: "opp_preparer",
              width:'120px',
              render: (text, record, index) => (
                <div >
                {this.state.mainData[index].opp_preparer?this.state.mainData[index].opp_preparer:<span>&nbsp;</span>}</div>
              )
            },
            {
              title: (<div fieldid="opp_preparedate" className="mergecells">{json['2002130205-000008']}</div>),/* 国际化处理： 制单日期*/
              dataIndex: "opp_preparedate",
              key: "opp_preparedate",
              width:'120px',
              render: (text, record, index) => (
                <div  className={this.state.mainData[index].opp_preparedate?'':'display-none'}>
                {this.state.mainData[index].opp_preparedate?this.state.mainData[index].opp_preparedate:<span>&nbsp;</span>}</div>
              )
            },
            {
              title: (<div fieldid="opp_debit" className="mergecells">{json['2002130205-000028']}</div>),/* 国际化处理： 借方*/
              dataIndex: "opp_debit",
              key: "opp_debit",
              width:'120px',
              className:'t-a-r',
              render: (text, record, index) => (
                <div>
                {this.state.mainData[index].opp_debit?this.state.mainData[index].opp_debit:<span>&nbsp;</span>}</div>
              )
            },
            {
              title: (<div fieldid="opp_credit" className="mergecells">{json['2002130205-000029']}</div>),/* 国际化处理： 贷方*/
              dataIndex: "opp_credit",
              key: "opp_credit",
              width:'120px',
              className:'t-a-r',
              render: (text, record, index) => (
                <div >
                {this.state.mainData[index].opp_credit?this.state.mainData[index].opp_credit:<span>&nbsp;</span>}</div>
              )
            },
          ]
        },
        {
          title: json['2002130205-000017'],/* 国际化处理： 本方凭证*/
          children: [
            {
              title: (<div fieldid="self_bookname" className="mergecells">{json['2002130205-000030']}</div>),/* 国际化处理： 本方核算账簿*/
              width:'150px',
              dataIndex: "self_bookname",
              key: "self_bookname",
              render: (text, record, index) => (
                <div >
                {this.state.mainData[index].self_bookname?this.state.mainData[index].self_bookname:<span>&nbsp;</span>}</div>
              )
            },
            {
              title: (<div fieldid="self_explanation" className="mergecells">{json['2002130205-000026']}</div>),/* 国际化处理： 摘要*/
              dataIndex: "self_explanation",
              key: "self_explanation",
              width:'120px',
              render: (text, record, index) => (
                <NCTooltip trigger="hover" placement="top" inverse={false} overlay={this.state.mainData[index].self_explanation}>
                <div className="nowRap">
                {this.state.mainData[index].self_explanation?this.state.mainData[index].self_explanation:<span>&nbsp;</span>}</div>
                </NCTooltip>
              )
            },
            {
              title: (<div fieldid="self_voucherno" className="mergecells">{json['2002130205-000027']}</div>),/* 国际化处理： 凭证号*/
              dataIndex: "self_voucherno",
              key: "self_voucherno",
              width:'120px',
              render: (text, record, index) => (
                <div>
                  {this.state.mainData[index].self_voucherno?this.state.mainData[index].self_voucherno:<span>&nbsp;</span>}
                </div>
              )
            },
            {
              title: (<div fieldid="self_preparer" className="mergecells">{json['2002130205-000006']}</div>),/* 国际化处理： 制单人*/
              dataIndex: "self_preparer",
              key: "self_preparer",
              width:'120px',
              render: (text, record, index) => (
                <div>
                {this.state.mainData[index].self_preparer?this.state.mainData[index].self_preparer:<span>&nbsp;</span>}</div>
              )
            },
            {
              title: (<div fieldid="self_preparedate" className="mergecells">{json['2002130205-000008']}</div>),/* 国际化处理： 制单日期*/
              dataIndex: "self_preparedate",
              key: "self_preparedate",
              width:'120px',
              render: (text, record, index) => (
                <div>
                {this.state.mainData[index].self_preparedate?this.state.mainData[index].self_preparedate:<span>&nbsp;</span>}</div>
              )
            },
            {
              title: (<div fieldid="self_debit" className="mergecells">{json['2002130205-000028']}</div>),/* 国际化处理： 借方*/
              dataIndex: "self_debit",
              key: "self_debit",
              width:'120px',
              className:'t-a-r',
              render: (text, record, index) => (
                <div>
                {this.state.mainData[index].self_debit?this.state.mainData[index].self_debit:<span>&nbsp;</span>}</div>
              )
            },
            {
              title: (<div fieldid="self_credit" className="mergecells">{json['2002130205-000029']}</div>),/* 国际化处理： 贷方*/
              dataIndex: "self_credit",
              key: "self_credit",
              className:'t-a-r',
              width:'120px',
              render: (text, record, index) => (
                <div>
                {this.state.mainData[index].self_credit?this.state.mainData[index].self_credit:<span>&nbsp;</span>}</div>
              )
            },
          ]
        }
      ]
           this.setState({json:json},()=>{ })
            }
    getMultiLang({moduleId:'2002130205',domainName:'gl',currentLocale:'simpchn',callback}); 
  }
  componentWillReceiveProps(nextProps){
    let mainData = nextProps.maindata;
    let seLen = 0;
    let selectArr=[];
    let {indeterminate_bool,checkedAll}=this.state;
    mainData.map((item,index)=>{
      item.key=index
      if(item.isSelected){
        seLen++
        selectArr.push(item)
      }
    })
    if(seLen==mainData.length){
      checkedAll=true;
      indeterminate_bool=false;
    }else if(seLen<mainData.length&&seLen){
      checkedAll=false;
      indeterminate_bool=true;
    }else if(!seLen){
      checkedAll=false;
      indeterminate_bool=false;
    }
    this.setState({
      mainData:mainData,
      indeterminate_bool,
      checkedAll
    })
  }
    //处理多选
    onAllCheckChange = () => {//全选
      let {mainData,indeterminate_bool,checkedAll}=this.state;
      let selectArr=[];
      for (var i = 0; i < mainData.length; i++) {
        mainData[i].isSelected = !checkedAll;
      }
      checkedAll = !checkedAll;
      this.setState({
        checkedAll,mainData
      });
      if(checkedAll){
        this.props.getSelectArr(mainData,mainData);
      }else{
        this.props.getSelectArr([]);
      }
    };
    onCheckboxChange = (text,index,record) => {
      let {mainData,indeterminate_bool,checkedAll}=this.state;
      mainData[index].isSelected=!mainData[index].isSelected
      let seLen = 0;
      let selectArr=[];
      for(let i=0,len=mainData.length;i<len;i++){
        if(mainData[i].isSelected){
          seLen++
          selectArr.push(mainData[i])
        }
      }
      if(seLen==mainData.length){
        checkedAll=true;
        indeterminate_bool=false;
      }else if(seLen<mainData.length&&seLen){
        checkedAll=false;
        indeterminate_bool=true;
      }else if(!seLen){
        checkedAll=false;
        indeterminate_bool=false;
      }
      this.setState({
          mainData,
          indeterminate_bool,
          checkedAll
      })
      this.props.getSelectArr(selectArr,mainData);
    };
  
    renderColumnsMultiSelect=(columns)=>{
      const { multiSelect } = this.props;
      let indeterminate_bool = this.state.indeterminate_bool;
      if (multiSelect && multiSelect.type === "checkbox") {
        let defaultColumns = [
          {
            title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
              <Checkbox
                className="table-checkbox"
                checked={this.state.checkedAll}
                indeterminate={indeterminate_bool&&!this.state.checkedAll}
                onChange={this.onAllCheckChange}
              />
            }</div>),
            key: "checkbox",
            dataIndex: "checkbox",
            width: "50px",
            render: (text, record, index) => {
              return (
                <div fieldid="firstcol">
                <Checkbox
                  className="table-checkbox"
                  checked={this.state.mainData[index].isSelected}
                  onChange={this.onCheckboxChange.bind( text, record, index)}
                />
                </div>
              );
            }
          }
        ];
        columns = defaultColumns.concat(columns);
      }
      return columns;
    }
  render() {
    // let columns = this.columns; 
    let {mainData}=this.state;
    let columns = this.columns?this.renderColumnsMultiSelect(this.columns):[];
    return (
        <div >
            <NCTable
                columns={columns}
                data={mainData}
                className='glCoorditonTable'
                bordered
                // scroll={{ x:true, y: 280 }}
                bodyStyle={{height:getTableHeight(260)}}
                scroll={{ x: true, y: getTableHeight(260) }}
            />
        </div>
    );
  }
}
CoorTable.defaultProps = {
  prefixCls: "bee-table",
  multiSelect: {
    type: "checkbox",
    param: "key"
  }
};
