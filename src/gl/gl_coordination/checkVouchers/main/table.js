import React, { Component } from "react";
import {ajax,base} from 'nc-lightapp-front';
const { NCButton, NCButtonGroup,NCMenu:Menu,NCTable,NCButton:Button,NCCheckbox:Checkbox } = base;

export default class ConfirmTable extends Component {
  constructor(props){
    super(props);
    this.state = {
      mainData: [],
      checkedAll:false,
      selectData:[],
      selIds:[],
      checkedArray: [
        false,
        false,
        false,
      ]
    };
    this.columns=[
      {
        title: "序号",
        dataIndex: "num",
        key: "num",
        render: (text, record, index) => (
          <span>
            {index}
          </span>
        )
      },
      {
        title: "摘要",
        dataIndex: "explanation",
        key: "explanation",
        render: (text, record, index) => (
          <span>
          {this.state.mainData[index].explanation}</span>
        )
      },
      {
        title: "科目",
        dataIndex: "reconciler",
        key: "reconciler",
        render: (text, record, index) => (
          <span>
          {/* {this.state.mainData[index].reconciler} */}
          </span>
        )
      },
      {
        title: "辅助核算",
        dataIndex: "reconciler",
        key: "reconciler",
        render: (text, record, index) => (
          <span>
          {/* {this.state.mainData[index].reconciler} */}
          </span>
        )
      },
      {
        title: "币种",
        dataIndex: "reconciler",
        key: "reconciler",
        render: (text, record, index) => (
          <span>
          {/* {this.state.mainData[index].reconciler} */}
          </span>
        )
      },
      {
        title: "原币",
        dataIndex: "reconciler",
        key: "reconciler",
        render: (text, record, index) => (
          <span>
          {/* {this.state.mainData[index].reconciler} */}
          </span>
        )
      },
      {
        title: "借方",
        dataIndex: "totaldebit",
        key: "totaldebit",
        render: (text, record, index) => (
          <span>
          {this.state.mainData[index].totaldebit}</span>
        )
      },
      {
        title: "贷方",
        dataIndex: "totalcredit",
        key: "totalcredit",
        render: (text, record, index) => (
          <span>
          {this.state.mainData[index].totalcredit}</span>
        )
      },
      {
        title: "数量",
        dataIndex: "num",
        key: "num",
        render: (text, record, index) => (
          <span>
          {this.state.mainData[index].num}</span>
        )
      },
      {
        title: "集团本币",
        dataIndex: "reconciler",
        key: "reconciler",
        render: (text, record, index) => (
          <span>
          {/* {this.state.mainData[index].reconciler} */}
          </span>
        )
      },
      {
        title: "全局本币",
        dataIndex: "reconciler",
        key: "reconciler",
        render: (text, record, index) => (
          <span>
          {/* {this.state.mainData[index].reconciler} */}
          </span>
        )
      },
    ];
  }  
  componentWillReceiveProps(nextProps){
    let mainData = nextProps.tableData;
    // mainData.map((item,index)=>{
    //   item.key=index
    // })
    
    this.setState({
      mainData:[mainData]
    })
  }
  //处理多选
  onAllCheckChange = () => {
    let self = this;
    let checkedArray = [];
    let listData = self.state.mainData.concat();
    let selIds = [];
    // let id = self.props.multiSelect.param;
    for (var i = 0; i < self.state.checkedArray.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
    }
    self.setState({
      checkedAll: !self.state.checkedAll,
      checkedArray: checkedArray,
      // selIds: selIds
    });
    // self.props.getSelectArr(selIds);
  };
  onCheckboxChange = (text, record, index) => {
    let self = this;
    let allFlag = false;
    let selIds = self.state.selIds;
    // let id = self.props.postId;
    let checkedArray = self.state.checkedArray.concat();
    if (self.state.checkedArray[index]) {
      selIds.remove(record.key);
    } else {
      selIds.push(record.key);
    }
    checkedArray[index] = !self.state.checkedArray[index];
    for (let i = 0; i < self.state.checkedArray.length; i++) {
      if (!checkedArray[i]) {
        allFlag = false;
        break;
      } else {
        allFlag = true;
      }
    }
    self.setState({
      checkedAll: allFlag,
      checkedArray: checkedArray,
      selIds: selIds
    });
    let mainData = this.state.mainData;
    let selectArr = [];
    for (let i = 0; i < selIds.length; i++) {
      selectArr.push(mainData[i])
    }
    self.setState({
      selectData:selectArr
    });
  };

  renderColumnsMultiSelect=(columns)=>{
    const { data,checkedArray } = this.state;
    const { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // let indeterminate_bool1 = true;
    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArray.length;
      while(i--){
          if(checkedArray[i]){
            indeterminate_bool = true;
            break;
          }
      }
      let defaultColumns = [
        {
          title: (
            <Checkbox
              className="table-checkbox"
              checked={this.state.checkedAll}
              indeterminate={indeterminate_bool&&!this.state.checkedAll}
              onChange={this.onAllCheckChange}
            />
          ),
          key: "checkbox",
          dataIndex: "checkbox",
          width: "5%",
          render: (text, record, index) => {
            return (
              <Checkbox
                className="table-checkbox"
                checked={this.state.checkedArray[index]}
                onChange={this.onCheckboxChange.bind(this, text, record, index)}
              />
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
  render() {
    // let columns = this.columns; 
    let {mainData}=this.state;
    let columns = this.renderColumnsMultiSelect(this.columns);

    return (
        <div>
            <span style={{'padding':'1px'}}></span>
            <NCTable
                columns={columns}
                data={mainData}
                bordered
                // scroll={{ x: "130%", y: 240 }}
            />
        </div>
    );
  }
}