import React, { Component } from 'react';
import {ajax } from 'nc-lightapp-front';
import { base }  from 'nc-lightapp-front';
const { NCRadio, NCForm, NCFormControl,NCCheckbox,NCTabs,
    NCModal:Modal,NCRow,NCCol,NCButton:Button,NCDatePicker,NCButtonGroup,NCPanel } = base;
const NCFormItem = NCForm.NCFormItem;
import VoucherTypeDefaultGridRef from '../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef';
import ConfirmTable from './table'
const NCTabPane = NCTabs.NCTabPane;
class ConfirmModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            selectArr:[{
                self_bookname:''
            }],
            searchData:{
                preparedatestart:'',
                pk_vouchertype:{//凭证类别
                    refcode:'',
                    refname:'',
                    refpk:''
                },
                formNum:''
            },
            tableData:[],
            open:false
        };
        this.close = this.close.bind(this);
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.confirmModalShow;
        let selectArr = nextProps.selectArr;
        let tableData = nextProps.tableData;
        this.setState({
            showModal,
            selectArr,
            tableData
        })
    }
    close() {
        this.props.getConfirmModalShow(false);
    }
    
    render () {
        let {selectArr,searchData,tableData} = this.state;
        return (
        <div>
            <NCRow useRow={true} showSubmit={false} >
                <NCCol md={6} xs={6} sm={6}>
                    核算账簿：{selectArr[0].self_bookname}
                </NCCol>
                <NCCol md={6} xs={6} sm={6} style={{'text-align':'right'}}>
                    <NCCheckbox colors="info" name="agree"
                        value={searchData.isselftallyed=='Y'?true:false}
                        disabled
                    >
                        数量调整
                    </NCCheckbox>
                    <NCCheckbox colors="info" name="agree"
                        value={searchData.isselftallyed=='Y'?true:false}
                        disabled
                    >
                        差异凭证
                    </NCCheckbox>
                </NCCol>
            </NCRow>
            <NCRow useRow={true} showSubmit={false} >
                <NCCol md={1} xs={1} sm={1}>
                    制单日期
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    凭证类别
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    会计区间
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    附单据数
                </NCCol>
            </NCRow>
            <ConfirmTable
                tableData={tableData}
            />
            <NCRow useRow={true} showSubmit={false} >
                <NCCol md={1} xs={1} sm={1}>
                    合计差额
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    组织借方合计
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    组织贷方合计
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    大写合计
                </NCCol>
            </NCRow>          
            <NCRow useRow={true} showSubmit={false} >
                <NCCol md={1} xs={1} sm={1}>
                    原币
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    组织本币汇率
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    组织本币
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    数量
                </NCCol>
            </NCRow>    
            <NCRow useRow={true} showSubmit={false} >
                <NCCol md={1} xs={1} sm={1}>
                    结算方式
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    票据号
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    票据日期
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    单价
                </NCCol>
            </NCRow>    
            <NCRow useRow={true} showSubmit={false} >
                <NCCol md={1} xs={1} sm={1}>
                    辅助核算
                </NCCol>
            </NCRow>
            <NCRow useRow={true} showSubmit={false} >
                <NCCol md={6} xs={6} sm={6}>
                    现金流量
                </NCCol>
                <NCCol md={6} xs={6} sm={6}>
                    来源系统
                </NCCol>
            </NCRow>
            <NCRow >
                <NCCol md={1} xs={1} sm={1}>
                    记账人
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    审核人
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    签字人
                </NCCol>
                <NCCol md={1} xs={1} sm={1}>
                    制单人
                </NCCol>
            </NCRow>
            <div>
                <Button colors="primary" onClick={() => this.setState({open: !this.state.open})}>
                    审计信息
                </Button>
                <NCPanel collapsible expanded={this.state.open}>
                <NCRow >
                    <NCCol md={1} xs={1} sm={1}>
                        创建人
                    </NCCol>
                    <NCCol md={1} xs={1} sm={1}>
                        创建时间
                    </NCCol>
                    <NCCol md={1} xs={1} sm={1}>
                        修改人
                    </NCCol>
                    <NCCol md={1} xs={1} sm={1}>
                        修改时间
                    </NCCol>
                </NCRow>
                </NCPanel>
            </div>
        </div>
        )
    }
}
ReactDOM.render(<ConfirmModal />,document.querySelector('#app'));