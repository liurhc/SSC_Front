import { asyncComponent } from 'nc-lightapp-front';
import CoordinationCenter from '../../coordinationCenter/main';
import confirm from '../../coordinationConfirm/main';
import Voucher from 'gl/voucher_card';

const search = asyncComponent(() => import(/* webpackChunkName: "gl/gl_coordination/coordinationSearch/main" */ '../../coordinationSearch/main'));
// const confirm = asyncComponent(() => import(/* webpackChunkName: "gl/gl_coordination/coordinationConfirm/main" */  '../../coordinationConfirm/main'));
// const Voucher = asyncComponent(() => import(/* webpackChunkName: "gl/gl_coordination/container/Welcome" */  '../../../gl_voucher/container/Welcome'));
const routes = [
	{
		path: '/',
		component: CoordinationCenter,
		exact: true
	},
	{
		path: '/center',
		component: CoordinationCenter
	},
	{
		path: '/search',
		component: search
	},
	{
		path: '/confirm',
		component: confirm
	},
    {
      path: '/Welcome',
      component: Voucher,
    },
];

export default routes;
