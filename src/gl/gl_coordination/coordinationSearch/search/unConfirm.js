import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {ajax,base,high,createPage,cardCache,getMultiLang,toast} from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
const { Refer } = high;
import createScript from '../../../public/components/uapRefer.js';
import VoucherTypeDefaultGridRef from '../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef';
const { NCButton, NCButtonGroup,NCMenu:Menu,NCDropdown:Dropdown,NCRow,NCCol,NCForm, 
    NCFormControl,NCCheckbox,NCDatePicker,NCIcon,NCDiv } = base;
const NCFormItem = NCForm.NCFormItem;
const {NCRangePicker} = NCDatePicker;
import './index.less';
class SearchUnConfirm extends Component {
    constructor(props){
        super(props);
        this.state={
            json:{},
            MainDataUnConfirmSe: [],
            showMore:false,
            tabState:'1',
            searchData:{
                // begindate:'',
                // enddate:'',
                isselfquery:'Y',
                state:'1',
                isselfchecked:'N',//已审核
                isselftallyed:'N',//已记账
                pk_orgbook_self:{
                    refcode:'',
                    refname:'',
                    refpk:''
                },
                pk_orgbook_other:[],
                selfpreparedatestart:'',//本方制单日期开始
                selfpreparedateend:'',//本方制单日期结束
                reconciledatestart:'', //协同开始日期
                reconciledateend:'',//协同结束日期
                pk_othervouchertype:{
                    refcode:'',
                    refname:'',
                    refpk:''
                },
                othercodestart:'',//
                othercodeend:'',
                otherpreparer:'',
                otherpreparedatestart:'',//对方制单日期开始
                otherpreparedateend:'',//对方制单日期结束
                pk_selfvouchertype:{//凭证类别
                    refcode:'',
                    refname:'',
                    refpk:''
                },
                selfcodestart:'',//开始凭证号
                selfcodeend:'',//结束凭证号
                selfpreparer:{//制单人
                    refcode:'',
                    refname:'',
                    refpk:''
                },
                reconcilemaker:{//协同人
                    refcode:'',
                    refname:'',
                    refpk:''
                },
            }
        }
    }
    componentWillMount(){
        let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
        getMultiLang({moduleId:'2002130215',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentDidMount(){
        let searchData = getDefData(11111, 'gl_obervision.discount.stateUNConfirm');
        if (searchData) {
            this.state = searchData;
            this.setState({
                searchData: searchData.searchData,
                MainDataUnConfirmSe: searchData.MainDataUnConfirmSe
            })
            this.handleSearch(searchData.MainDataUnConfirmSe);
            return;
        }
        //获取默认账簿
        let url = '/nccloud/platform/appregister/queryappcontext.do';
        let data={
            appcode: this.props.getSearchParam('c')
        }
        let self=this;
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                let searchData = self.state.searchData;
                searchData.pk_orgbook_self.refname=data.defaultAccbookName;
                searchData.pk_orgbook_self.refpk=data.defaultAccbookPk;
                searchData.pk_orgbook_other.refname=data.defaultAccbookName;
                searchData.pk_orgbook_other.refpk=data.defaultAccbookPk;
                self.getCombineInfo(searchData.pk_orgbook_self)
                self.setState({
                    searchData
                })
                } else {
                }
            }
        });
    }
    componentWillReceiveProps(nextProps){
        let MainDataUnConfirmSe = nextProps.MainDataUnConfirmSe;
        if (MainDataUnConfirmSe&&MainDataUnConfirmSe.length) {
            this.state.MainDataUnConfirmSe = MainDataUnConfirmSe;
            setDefData(11111, 'gl_obervision.discount.stateUNConfirm', this.state);
            this.setState({
                MainDataUnConfirmSe: MainDataUnConfirmSe
            })
        }
        let tabState = nextProps.tabState;
        this.setState({
            tabState
        })
    }
    onStartChange(d) {
    }
    handleSearch=(mainSe)=>{//查询
        let searchData = JSON.parse(JSON.stringify(this.state.searchData));
        if(!searchData.pk_orgbook_self.refpk){
            toast({ content: this.state.json['2002130215-000004'], color: 'warning' });/* 国际化处理： 请选择核算账簿*/
            return;
        }
        setDefData(11111, 'gl_obervision.discount.stateUNConfirm',this.state);
        searchData.pk_orgbook_self = [searchData.pk_orgbook_self.refpk];
        searchData.pk_selfvouchertype = searchData.pk_selfvouchertype.refpk;
        searchData.selfpreparer = searchData.selfpreparer.refpk;
        let pk_orgbook_other=searchData.pk_orgbook_other;
        let localArr = [];
        for(let i=0,len=pk_orgbook_other.length;i<len;i++){
            localArr.push(pk_orgbook_other[i].refpk)
        }
        searchData.pk_orgbook_other=localArr;
        searchData.pk_othervouchertype=searchData.pk_othervouchertype.refpk;
        searchData.reconcilemaker=searchData.reconcilemaker.refpk;
        searchData.state=this.state.tabState;
        let data = {};
        for(let item in searchData){
            if(searchData[item]){
                data[item]=searchData[item]
            }
        }
        let self = this;
        data.needGroup='Y';
        let url = "/nccloud/gl/reconcile/reconciledataquery.do";
        ajax({
            url,
            data,
            success: function(response) { 
                const { data, error, success } = response;
                if (success&&data) {
                    if (mainSe) {
                        for (let i = 0, len = mainSe.length; i < len; i++) {
                            data[mainSe[i]].isSelected = true;
                        }
                    }
                    self.props.getMainDataUnConfirm(data)
                } else {
                    self.props.getMainDataUnConfirm([])
                }    
            }
        });
    }
    handleClear=()=>{
        this.setState({
            searchData:{
                // begindate:'',
                // enddate:'',
                isselfquery:'Y',
                state:'2',
                isselfchecked:'N',//已审核
                isselftallyed:'N',//已记账
                pk_orgbook_self:{
                    refcode:'',
                    refname:'',
                    refpk:''
                },
                pk_orgbook_other:[],
                selfpreparedatestart:'',//本方制单日期开始
                selfpreparedateend:'',//本方制单日期结束
                reconciledatestart:'', //协同开始日期
                reconciledateend:'',//协同结束日期
                pk_othervouchertype:{
                    refcode:'',
                    refname:'',
                    refpk:''
                },
                othercodestart:'',//
                othercodeend:'',
                otherpreparer:{//制单人
                    refcode:'',
                    refname:'',
                    refpk:''
                },
                otherpreparedatestart:'',//对方制单日期开始
                otherpreparedateend:'',//对方制单日期结束
                pk_selfvouchertype:{//凭证类别
                    refcode:'',
                    refname:'',
                    refpk:''
                },
                selfcodestart:'',//开始凭证号
                selfcodeend:'',//结束凭证号
                selfpreparer:{//制单人
                    refcode:'',
                    refname:'',
                    refpk:''
                },
                reconcilemaker:{//协同人
                    refcode:'',
                    refname:'',
                    refpk:''
                },
            }
        })
    }
     //获取日期等
     getCombineInfo=(v)=>{
        let self=this;
        ajax({
            url:'/nccloud/gl/voucher/queryBookCombineInfo.do',
            data:{"pk_accountingbook":v.refpk,'needaccount':false},
            success: function(response) { 
                const { data, error, success } = response;
                let {searchData,period_pk,bizDate}=self.state;
                if (success&&data) {
                    searchData.selfpreparedatestart=data.begindate
                    searchData.selfpreparedateend=data.bizDate
                    searchData.otherpreparedatestart=data.begindate
                    searchData.otherpreparedateend=data.bizDate
                    searchData.reconciledatestart=data.begindate
                    searchData.reconciledateend=data.bizDate
                    self.setState({
                        searchData
                    })
                } else {
                    
                }    
            }
        });
    }
    render() {
        let { searchData,tabState } = this.state;
        let mybook,userMaker,userCoor,mybook1;
		// let self = this;
		// let referUrl= item.refcode+'/index.js';
        let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
        let userUrl= 'uap/refer/riart/userRefer' + '/index.js';
		if(!this.state['myattrcode']){
			{createScript.call(this,referUrl,'myattrcode')}
		}else{
			mybook =  (
				<NCRow>
					<NCCol xs={12} md={12}>
					{this.state['myattrcode']?(this.state['myattrcode'])(
						{
                            fieldid:'pk_orgbook_self',
							value:{refname:searchData.pk_orgbook_self.refname,refpk:searchData.pk_orgbook_self.refpk,refcode:searchData.pk_orgbook_self.refcode},
                            disabledDataShow:true,
                            isMultiSelectedEnabled:false,
                            queryCondition:{
                                "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                "appcode": this.props.getSearchParam('c')
                            },
                            onChange: (v)=>{
								searchData.pk_orgbook_self.refcode=v.refcode;
                                searchData.pk_orgbook_self.refname=v.refname;
                                searchData.pk_orgbook_self.refpk=v.refpk;
                                this.getCombineInfo(searchData.pk_orgbook_self)
                                this.setState({
                                    searchData
                                })
							  }
						}
					):<div/>}
					</NCCol>
				</NCRow>
            );	
            mybook1=(
                <NCRow>
					<NCCol xs={12} md={12}>
					{this.state['myattrcode']?(this.state['myattrcode'])(
						{
                            fieldid:'pk_orgbook_other',
                            value:searchData.pk_orgbook_other,
                            disabledDataShow:true,
                            queryCondition:{
                                // "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                "appcode": this.props.getSearchParam('c'),
                                flag: true,
                                multiGroup: 'Y'
                            },
                            isMultiSelectedEnabled: true,
                            showGroup: true,
                            flag: true,
							onChange: (v)=>{
								searchData.pk_orgbook_other=v;
                                this.setState({
                                    searchData
                                })
							  }
						}
					):<div/>}
					</NCCol>
				</NCRow>
            )
        }
        if(!this.state['userCode']){    
            {createScript.call(this,userUrl,'userCode')}
		}else{
            userMaker=(
                <NCRow>
					<NCCol xs={12} md={12}>
					{this.state['userCode']?(this.state['userCode'])(
						{
                            fieldid:'selfpreparer',
                            isMultiSelectedEnabled:false, 
                            placeholder:this.state.json['2002130215-000005'],/* 国际化处理： 制单人*/
                            value:{refcode:searchData.selfpreparer.refcode,refname:searchData.selfpreparer.refname,
                                refpk:searchData.selfpreparer.refpk},
                            queryCondition:{isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',},
							onChange: (v)=>{
								searchData.selfpreparer=v;
                                this.setState({
                                    searchData
                                })
							  }
						}
					):<div/>}
					</NCCol>
				</NCRow>
            )
            userCoor=(
                <NCRow>
					<NCCol xs={12} md={12}>
					{this.state['userCode']?(this.state['userCode'])(
						{
                            fieldid:'reconcilemaker',
                            isMultiSelectedEnabled:false,
                            placeholder:this.state.json['2002130215-000006'],/* 国际化处理： 协同人*/
                            value:{refcode:searchData.reconcilemaker.refcode,refname:searchData.reconcilemaker.refname,
                                refpk:searchData.reconcilemaker.refpk},
                            queryCondition:{isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',},
							onChange: (v)=>{
								searchData.reconcilemaker.refcode=v.refcode;
                                searchData.reconcilemaker.refname=v.refname;
                                searchData.reconcilemaker.refpk=v.refpk;
                                this.setState({
                                    searchData
                                })
							  }
						}
					):<div/>}
					</NCCol>
				</NCRow>
            )
		}
        return(
            <div className='search-coordinate' style={{'padding':'5px','padding-top':'10px'}}>
            <NCDiv areaCode={NCDiv.config.SEARCH}>
                <div>
                    <div className='tittle'>
                        <h2>{this.state.json['2002130215-000016']}</h2>
                        <i class="uf uf-triangle-right"></i>
                    </div>
                    <div className='sertch-content'>
                        <NCCol md={2} xs={2} sm={2}>
                            <span style={{'color':'#E14C46','position': 'absolute','left': '4px','top': '9px','z-index': '2'}}>*</span>
                            {mybook}
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} className='form-input nc-theme-area-split-bc'>
                            <NCCheckbox colors="info" name="agree"
                                fieldid='isselfchecked'
                                checked={searchData.isselfchecked=='Y'?true:false}
                                onChange={(v)=>{
                                    if(v){
                                        searchData.isselfchecked='Y';
                                    }else{
                                        searchData.isselfchecked='N';
                                    }
                                    this.setState({
                                        searchData
                                    })
                                }}
                            >
                                {this.state.json['2002130215-000017']}
                            </NCCheckbox>
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} className='form-input nc-theme-area-split-bc'>
                            <NCCheckbox colors="info" name="agree"
                                fieldid='isselftallyed'
                                checked={searchData.isselftallyed=='Y'?true:false}
                                onChange={(v)=>{
                                    if(v){
                                        searchData.isselftallyed='Y';
                                    }else{
                                        searchData.isselftallyed='N';
                                    }
                                    this.setState({
                                        searchData
                                    })
                                }}
                            >
                                {this.state.json['2002130215-000018']}
                            </NCCheckbox>
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} className='search-item'>
                            <NCRangePicker
                                fieldid='preparedate'
                                placeholder={this.state.json['2002130215-000007']}/* 国际化处理： 制单日期*/
                                dateInputPlaceholder={[this.state.json['2002130215-000008'], this.state.json['2002130215-000009']]}/* 国际化处理： 开始,结束*/
                                value={searchData.selfpreparedatestart&&searchData.selfpreparedateend?[searchData.selfpreparedatestart, searchData.selfpreparedateend]:[]}
                                showClear={true}
                                onChange={(v)=>{
                                    searchData.selfpreparedatestart=v[0];
                                    searchData.selfpreparedateend = v[1];
                                    searchData.preparedate=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                // style={{'display':'inline','width':'10%'}}
                                // showToday={true}
                                // value={searchData.preparedate}
                                // autofocus={false}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} className='search-item'>
                            <VoucherTypeDefaultGridRef 
                                fieldid='pk_selfvouchertype'
                                value={{refname:searchData.pk_selfvouchertype.refname,
                                refpk:searchData.pk_selfvouchertype.refpk}}
                                placeholder={this.state.json['2002130215-000010']}/* 国际化处理： 凭证类别*/
                                queryCondition={{
                                    GridRefActionExt:'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
                                    pk_org:searchData.pk_orgbook_self.refpk,
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi',
                                }}
                                onChange={(v)=>{
                                    searchData.pk_selfvouchertype.refname=v.refname;
                                    searchData.pk_selfvouchertype.refpk=v.refpk;
                                    this.setState({
                                        searchData
                                    })
                                }}    
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} style={{'width':'150px'}}>
                            <NCIcon type={this.state.showMore?"uf-triangle-up":'uf-triangle-down'} onClick={(e) => {
                                let showMore = this.state.showMore;
                                showMore=!showMore;
                                this.props.setUnConShow(showMore);
                                this.setState({
                                    showMore:showMore
                                })
                            }}></NCIcon>
                            <NCButton fieldid="handleSearch" colors="primary" type="primary" onClick={this.handleSearch.bind(this)}>{this.state.json['2002130215-000019']}</NCButton>
                            <NCButton fieldid="handleClear" onClick={this.handleClear}>{this.state.json['2002130215-000020']}</NCButton>
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} style={{'margin-bottom':'0'}}>
                            <div >
                                <NCFormControl name='explanation' className="explanation" type='text' value={searchData.selfcodestart}
                                    fieldid='selfcodestart'
                                    autocomplete="off"
                                    placeholder={this.state.json['2002130215-000011']}/* 国际化处理： 开始凭证号*/
                                    onChange={(value)=>{
                                        let lastItem = value.slice(value.length-1)
                                        let testNum = /[0-9]/
                                        if(!testNum.test(lastItem)&&lastItem!==''){
                                            searchData.selfcodestart=searchData.selfcodestart;
                                        }else{
                                            searchData.selfcodestart=value;
                                        }
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </div>
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} style={{'margin-bottom':'0'}}>
                            <div >
                                <NCFormControl name='explanation' className="explanation" type='text' value={searchData.selfcodeend}
                                    fieldid='selfcodeend'
                                    autocomplete="off"
                                    placeholder={this.state.json['2002130215-000012']}/* 国际化处理： 结束凭证号*/
                                    onChange={(value)=>{
                                        let lastItem = value.slice(value.length-1)
                                        let testNum = /[0-9]/
                                        if(!testNum.test(lastItem)&&lastItem!==''){
                                            searchData.selfcodeend=searchData.selfcodeend;
                                        }else{
                                            searchData.selfcodeend=value;
                                        }
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </div>
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} style={{'margin-bottom':'0','margin-top':'-2px'}}>
                            {userMaker}
                        </NCCol>
                    </div>
                </div>
                <div style={{'margin-top':'10px'}} className={this.state.showMore?'':'display-none'}>
                    <div className='tittle1'>
                        <h2>{this.state.json['2002130215-000021']}</h2>
                        <i class="uf uf-triangle-right"></i>
                    </div>
                    <div className='sertch-content sertch-content1'>
                        <NCCol md={2} xs={2} sm={2}>
                            <NCRangePicker
                                fieldid='reconciledate'
                                placeholder={this.state.json['2002130215-000013']}/* 国际化处理： 协同日期*/
                                dateInputPlaceholder={[this.state.json['2002130215-000008'], this.state.json['2002130215-000009']]}/* 国际化处理： 开始,结束*/
                                showClear={true}
                                onChange={(v)=>{
                                    searchData.reconciledatestart=v[0];
                                    searchData.reconciledateend = v[1];
                                    searchData.reconciledate=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                // style={{'display':'inline','width':'10%'}}
                                // showToday={true}
                                value={searchData.reconciledatestart&&searchData.reconciledateend?[searchData.reconciledatestart, searchData.reconciledateend]:[]}
                                // autofocus={false}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} >
                            <NCFormControl  name="name" placeholder={this.state.json['2002130215-000014']} /* 国际化处理： 协同号开始*/
                                fieldid='reconcilecodestart'
                                value={searchData.reconcilecodestart}
                                onChange={(v)=>{
                                    searchData.reconcilecodestart=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} >
                            <NCFormControl  name="name" placeholder={this.state.json['2002130215-000015']} /* 国际化处理： 协同号结束*/
                                fieldid='reconcilecodeend'
                                value={searchData.reconcilecodeend}
                                onChange={(v)=>{
                                    searchData.reconcilecodeend=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                        <NCCol md={2} xs={2} sm={2} >
                            {userCoor}
                        </NCCol>
                    </div>
                </div>
                <div style={{'margin-top':'10px'}} className={this.state.showMore?'':'display-none'}>
                    <div className='tittle1'>
                        <h2>{this.state.json['2002130215-000022']}</h2>
                        <i class="uf uf-triangle-right"></i>
                    </div>
                    <div className='sertch-content sertch-content1'>
                        <NCCol md={2} xs={2} sm={2}>
                            {mybook1}
                        </NCCol>
                    </div>
                </div>
                </NCDiv>
            </div>
        )
    }
}
SearchUnConfirm = createPage({})(SearchUnConfirm);
export default SearchUnConfirm;
