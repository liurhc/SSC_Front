const dataSource = 'fip.gl.init.';
const context = 'context';

function setData(ViewModel, key, data){
    ViewModel.setData(dataSource + key, data);
}

function getData(ViewModel, key){
    return ViewModel.getData(dataSource + key);
}

function setContext(ViewModel, data){
    setData(ViewModel, context, data);
}

function getContext(ViewModel){
    return getData(ViewModel, context);
}

const CacheUtil = {
    setContext : setContext,
    getContext : getContext
}

export default CacheUtil;
