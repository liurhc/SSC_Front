/**
 * 将请求回来的试算数据整理成组件需要的格式
 * @param {*} dataList 
 */
function organizeCalculationResult(dataList){
    dataList.forEach(function(item, index){
		item.key = item.pk_unit;
		if(item.fi){
			item.fi.leftVOs.forEach(function (item) {
				item.key = item.pk_account;
			})
			item.fi.rightVOs.forEach(function (item) {
				item.key = item.pk_account;
			})
		}
		if(item.pri){
			item.pri.leftVOs.forEach(function (item) {
				item.key = item.pk_account;
			})
			item.pri.rightVOs.forEach(function (item) {
				item.key = item.pk_account;
			})
		}
	});
}

export {organizeCalculationResult}