import { ajax, toast, getMultiLangByID } from 'nc-lightapp-front';
import cacheUtils from './cacheUtils'

// var lockMap = new Map()
// 并发
class lockAPI {
    // 加锁
    addAccountLock = (sendData) => {
        let page = this
        let url = '/nccloud/gl/init/addAccountLock.do'
        let lockMap = cacheUtils.getLockMap()
        let {pk_accasoa} = sendData
        let lockFlag = null
        ajax({
            url,
            async: false,
            data: sendData,
            success: function (res) {
                const { data, message, success } = res;
                if(success){
                    if(data){
                        lockMap.set(pk_accasoa, data)                      	
                        cacheUtils.setLockMap(lockMap)
                    }else{
                        toast({ content: getMultiLangByID('20020401-000108'), color: 'warning' });/* 国际化处理： 有其他操作员正在编辑该科目的期初余额，您只能浏览，不能编辑！*/
                        // if(callback){
                        //     callback()
                        // }	
                    }
                    lockFlag = data                           
                }else {
                    toast({ content: message.message, color: 'warning' });
                }
            },
            error: function (res) {
                toast({ content: res.message, color: 'danger' });
                
            }
        });
        return lockFlag
    }
    
    /**
     * 释放所有锁
     * 默认异步
     */
    freeAllInitLock = (async=true, callback) => {
		let url = '/nccloud/gl/init/freeAllInitLock.do'
		ajax({
            url,
            async: async,
			success: function (res) {
				const { data, message, success } = res;
				if(success){
                    if(callback){
                        callback()
                    }
				}else {
					toast({ content: message.message, color: 'warning' });
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });
				
			}
		});
	}
}

export default new lockAPI()
