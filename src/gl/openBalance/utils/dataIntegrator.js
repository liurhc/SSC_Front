// 辅助核算页数据列取和
function consChacheData(assInitVOs){
    
    let sumData = {
        creditamount: 0,
        creditglobalamount: 0,
        creditgroupamount: 0,
        localcreditamount: 0,
        creditquantity: 0,

        debitamount: 0,
        debitglobalamount: 0,
        debitgroupamount: 0,
        localdebitamount: 0,
        debitquantity: 0,

        endamount: 0,
        endglobalamount: 0,
        endgroupamount: 0,
        endlocalamount: 0,
        endquantity: 0,

        yearamount: 0,
        yearglobalamount: 0,
        yeargroupamount: 0,
        yearlocalamount: 0,
        yearquantity: 0,
    }

    if(assInitVOs){
        assInitVOs.map((item)=>{
            
            // 贷方累计 
            sumData.creditamount += Number(item.creditamount.display)
            sumData.creditglobalamount += Number(item.creditglobalamount.display)
            sumData.creditgroupamount += Number(item.creditgroupamount.display)
            sumData.localcreditamount += Number(item.localcreditamount.display)
            sumData.creditquantity += Number(item.creditquantity.display) //数量
            // 借方累计
            sumData.debitamount += Number(item.debitamount.display)
            sumData.debitglobalamount += Number(item.debitglobalamount.display)
            sumData.debitgroupamount += Number(item.debitgroupamount.display)
            sumData.localdebitamount += Number(item.localdebitamount.display)				
            sumData.debitquantity += Number(item.debitquantity.display)
            // 期初余额
            sumData.endamount += Number(item.endamount.display) //原币
            sumData.endglobalamount += Number(item.endglobalamount.display) //全局本币
            sumData.endgroupamount += Number(item.endgroupamount.display) //集团本币
            sumData.endlocalamount += Number(item.endlocalamount.display)  // 组织本币
            sumData.endquantity += Number(item.endquantity.display)  //数量
            // 年初余额
            sumData.yearamount += Number(item.yearamount.display) //原币
            sumData.yearglobalamount += Number(item.yearglobalamount.display) //全局本币
            sumData.yeargroupamount += Number(item.yeargroupamount.display) //集团本币
            sumData.yearlocalamount += Number(item.yearlocalamount.display) // 组织本币
            sumData.yearquantity += Number(item.yearquantity.display) //数量
        })
    }
    return sumData
}

// 精度处理
function distShow (num,scale,type=''){		
    let tempNum = ''		
    let finalStr = ''
    if (isNaN(Number(num)) || !num) {
        return null;
    }
    let _num = num.toString()
    if (scale > 0) {
        let start = _num.split('.')[0];
        let end = _num.split('.')[1];
        if (!end) {
            end = '';
        }
        let len = end.length;
        if (len < scale) {
            end = end.padEnd(scale, '0');
        }
        tempNum = start + '.' + end;
    } else {
        tempNum =  _num;
    }
    if(type === 'display') {
        if(Number(tempNum)===0){
            finalStr = ''
        }else {
            finalStr = tempNum
        }
    } else { 			
        finalStr = tempNum
    }
    return finalStr
}

// 辅助核算返回期初余额前，处理缓存数据
function consInfo(assInitVOs, newInitVOs){
    let sumData = this.consChacheData(assInitVOs)
    if(sumData){
        // 贷方累计 
        newInitVOs.creditamount.display = this.distShow(sumData.creditamount, newInitVOs.creditamount.scale, 'display') 
        newInitVOs.creditamount.value = this.distShow(sumData.creditamount, newInitVOs.creditamount.scale) 

        newInitVOs.creditglobalamount.display = this.distShow(sumData.creditglobalamount, newInitVOs.creditglobalamount.scale, 'display')
        newInitVOs.creditglobalamount.value = this.distShow(sumData.creditglobalamount, newInitVOs.creditglobalamount.scale)

        newInitVOs.creditgroupamount.display = this.distShow(sumData.creditgroupamount, newInitVOs.creditgroupamount.scale, 'display') 
        newInitVOs.creditgroupamount.value = this.distShow(sumData.creditgroupamount, newInitVOs.creditgroupamount.scale) 

        newInitVOs.localcreditamount.display = this.distShow(sumData.localcreditamount, newInitVOs.localcreditamount.scale, 'display')
        newInitVOs.localcreditamount.value = this.distShow(sumData.localcreditamount, newInitVOs.localcreditamount.scale)

        newInitVOs.creditquantity.display = this.distShow(sumData.creditquantity, newInitVOs.creditquantity.scale, 'display')  //数量
        newInitVOs.creditquantity.value = this.distShow(sumData.creditquantity, newInitVOs.creditquantity.scale)  //数量
        // 借方累计
        newInitVOs.debitamount.display = this.distShow(sumData.debitamount, newInitVOs.debitamount.scale, 'display') 
        newInitVOs.debitamount.value = this.distShow(sumData.debitamount, newInitVOs.debitamount.scale) 

        newInitVOs.debitglobalamount.display = this.distShow(sumData.debitglobalamount, newInitVOs.debitglobalamount.scale, 'display') 
        newInitVOs.debitglobalamount.value = this.distShow(sumData.debitglobalamount, newInitVOs.debitglobalamount.scale) 

        newInitVOs.debitgroupamount.display = this.distShow(sumData.debitgroupamount, newInitVOs.debitgroupamount.scale, 'display') 
        newInitVOs.debitgroupamount.value = this.distShow(sumData.debitgroupamount, newInitVOs.debitgroupamount.scale) 

        newInitVOs.localdebitamount.display = this.distShow(sumData.localdebitamount, newInitVOs.localdebitamount.scale, 'display') 				
        newInitVOs.localdebitamount.value = this.distShow(sumData.localdebitamount, newInitVOs.localdebitamount.scale) 

        newInitVOs.debitquantity.display = this.distShow(sumData.debitquantity, newInitVOs.debitquantity.scale, 'display') 
        newInitVOs.debitquantity.value = this.distShow(sumData.debitquantity, newInitVOs.debitquantity.scale) 
        // 期初余额
        newInitVOs.endamount.display = this.distShow(sumData.endamount, newInitVOs.endamount.scale, 'display')  //原币
        newInitVOs.endamount.value = this.distShow(sumData.endamount, newInitVOs.endamount.scale)  //原币

        newInitVOs.endglobalamount.display = this.distShow(sumData.endglobalamount, newInitVOs.endglobalamount.scale, 'display') //全局本币
        newInitVOs.endglobalamount.value = this.distShow(sumData.endglobalamount, newInitVOs.endglobalamount.scale) //全局本币

        newInitVOs.endgroupamount.display = this.distShow(sumData.endgroupamount, newInitVOs.endgroupamount.scale, 'display') //集团本币
        newInitVOs.endgroupamount.value = this.distShow(sumData.endgroupamount, newInitVOs.endgroupamount.scale) //集团本币

        newInitVOs.endlocalamount.display = this.distShow(sumData.endlocalamount, newInitVOs.endlocalamount.scale, 'display')  // 组织本币
        newInitVOs.endlocalamount.value = this.distShow(sumData.endlocalamount, newInitVOs.endlocalamount.scale)  // 组织本币

        newInitVOs.endquantity.display = this.distShow(sumData.endquantity, newInitVOs.endquantity.scale, 'display')  //数量
        newInitVOs.endquantity.value = this.distShow(sumData.endquantity, newInitVOs.endquantity.scale)  //数量
        // 年初余额
        newInitVOs.yearamount.display = this.distShow(sumData.yearamount, newInitVOs.yearamount.scale, 'display') //原币
        newInitVOs.yearamount.value = this.distShow(sumData.yearamount, newInitVOs.yearamount.scale) //原币

        newInitVOs.yearglobalamount.display = this.distShow(sumData.yearglobalamount, newInitVOs.yearglobalamount.scale, 'display')  //全局本币
        newInitVOs.yearglobalamount.value = this.distShow(sumData.yearglobalamount, newInitVOs.yearglobalamount.scale)  //全局本币

        newInitVOs.yeargroupamount.display = this.distShow(sumData.yeargroupamount, newInitVOs.yeargroupamount.scale, 'display') //集团本币
        newInitVOs.yeargroupamount.value = this.distShow(sumData.yeargroupamount, newInitVOs.yeargroupamount.scale) //集团本币

        newInitVOs.yearlocalamount.display = this.distShow(sumData.yearlocalamount, newInitVOs.yearlocalamount.scale, 'display')  // 组织本币
        newInitVOs.yearlocalamount.value = this.distShow(sumData.yearlocalamount, newInitVOs.yearlocalamount.scale)  // 组织本币
        
        newInitVOs.yearquantity.display = this.distShow(sumData.yearquantity, newInitVOs.yearquantity.scale)  //数量
        newInitVOs.yearquantity.value = this.distShow(sumData.yearquantity, newInitVOs.yearquantity.scale, 'display')  //数量
    }
    return newInitVOs
}

export{consChacheData, distShow, consInfo }
