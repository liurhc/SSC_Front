import { cardCache } from 'nc-lightapp-front';

const   dataSource = 'gl.openBalance.accountAssistant.table';


const openBalanceKey = 'openBalance'  // 主页，期初余额key

const accountAssistantKey = 'accountAssistant'  //辅助核算 key

const trialBalanceKey = 'trialBalance'  // 试算平衡 key

const firstBalanceKey = 'firstBalance'  //期初建账 步骤一 key

const secondBalanceKey = 'secondBalance'  // 期初建账 步骤二 key

const initAllDataKey = 'initAllData'

const balanceData_org = 'balanceData_org';
const balanceData_group = 'balanceData_group';
const balanceData_global = 'balanceData_global';

const lockKey = 'lock';

var setDefData = cardCache.setDefData
var getDefData = cardCache.getDefData

class cacheUtils {
    setOpenData(data) {
        setDefData(openBalanceKey, dataSource, data);
    }
    getOpenData() {
        return getDefData(openBalanceKey, dataSource);
    }

    setAssisData(data) {
        setDefData(accountAssistantKey, dataSource, data);
    }
    getAssisData() {
        return getDefData(accountAssistantKey, dataSource);
    }

    setTrialData(data) {
        setDefData(trialBalanceKey, dataSource, data);
    }
    getTrialData() {
        return getDefData(trialBalanceKey, dataSource);
    }

    setFirstData(data) {
        setDefData(firstBalanceKey, dataSource, data);
    }
    getFirstData() {
        return getDefData(firstBalanceKey, dataSource);
    }

    setSecondData(data) {
        setDefData(secondBalanceKey, dataSource, data);
    }
    getSecondData() {
        return getDefData(secondBalanceKey, dataSource);
    }

    setData(key, data){
        setDefData(key, dataSource, data);
    }
    getData(key){
        return getDefData(key, dataSource);
    }

    setinitAllData(data) {
        setDefData(initAllDataKey, dataSource, data);
    }
    getInitAllData() {
        return getDefData(initAllDataKey, dataSource);
    }

    setLockMap(data){
        setDefData(lockKey, dataSource, data);
    }
    getLockMap(){
        let lockMap = getDefData(lockKey, dataSource)
        if(lockMap){
            return lockMap
        }else {
            lockMap = new Map();
            this.setLockMap(lockMap);
            return this.getLockMap();
        }
        // return getDefData(lockKey, dataSource);
        // var lockMap = new Map()
    }
}

export default new cacheUtils()
