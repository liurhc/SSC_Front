const balanceOrient = {
    equal : '3',
    credit : '1', //贷
    debit : '0' //借
}

/**
 * 集团本币借贷平衡控制
 */
const GL116 = {
    UNCTRL : '1',
    INFO : '2',
    CTRL : '3'
}

/**
 * 全局本币借贷平衡控制
 */
const GL118 = {
    UNCTRL : '1',
    INFO : '2',
    CTRL : '3'
}

const actions = {
    clearInitData:"/nccloud/gl/init/clearInitData.do"
}

export {balanceOrient, GL116, GL118, actions};
