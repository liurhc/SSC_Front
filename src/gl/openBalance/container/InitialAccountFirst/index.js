import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';

import {high,base,ajax,deepClone,createPage,promptBox, toast, getMultiLang, initMultiLangByModule, getMultiLangByID} from 'nc-lightapp-front';
import './index.less';
import {balanceOrient, GL116, GL118} from '../../consts';
const { NCStep,NCTable:Table, NCRow, NCCol, NCTooltip: Tooltip, NCDiv} = base;
import { onButtonClick, initTemplate} from '../OpenBalance/events';
import cacheUtils from '../../utils/cacheUtils'
import TrialResultTable from '../../../public/components/TrialResultTable';
import {organizeCalculationResult} from '../../utils/utils'
import {FISelect} from '../../../public/components/base'
const Select = FISelect;
const NCSteps = NCStep.NCSteps;
const Option = Select.FIOption;

class InitialAccountFirst extends Component {
	constructor(props) {
		super(props);
//每定义一个state 都要添加备注
		this.state = {
			current: 0,
			resultData: {},
			dataList: [],
			currtypeList: [],
			currtype: getMultiLangByID('20020401-000000'),      //选择的币种/* 国际化处理： 组织本币*/
			book: '',
			yearPeriod: '', //会计期间
			versionDate: '',
			year: '',
			month: '',
			isGlobal: false,
			isGroup: false,
			pk_accountingbook: '',
			isBUBalanceCheck: true,
			isStartBUSecond: false, //是否启用二级核算单元
			isZuzhiCan: true, //组织本币是否可建账
			groupAmountCtrl: '1',
			globalAmountCtrl: '1',
			canBuild: false, //期初数据是否通过检查
			selectedRowIndex: 0,  //左table 选中行
			json: {}
		}
		this.steps = []
	}

	updateBtnStatus = (visible) => {
		// 首步 上一步 下一步 建账  
		this.props.button.setButtonVisible(['first_step'], false);
		this.props.button.setButtonVisible(['pre_step', 'final_build'], visible);
		this.props.button.setButtonVisible(['next_step'], !visible);
	}
	// 更新按钮状态

	//上一步
	goPreStep() {
		this.steps = [{
			title: getMultiLangByID('20020401-000032'),/* 国际化处理： 试算平衡*/
			description: getMultiLangByID('20020401-000040')/* 国际化处理： 已完成*/
		}, {
			title: getMultiLangByID('20020401-000034'),/* 国际化处理： 建账*/
			description: getMultiLangByID('20020401-000033')/* 国际化处理： 进行中*/
		}];
		this.setState({
			current: 0
		})
		this.updateBtnStatus(false)
	}

	//下一步
	goNextStep = () => {
		let canBuild = this.getIsCanBuild()
		this.steps = [{
			title: getMultiLangByID('20020401-000032'),/* 国际化处理： 试算平衡*/
			description: getMultiLangByID('20020401-000040')/* 国际化处理： 已完成*/
		}, {
			title: getMultiLangByID('20020401-000034'),/* 国际化处理： 建账*/
			description: getMultiLangByID('20020401-000033')/* 国际化处理： 进行中*/
		}];
		this.setState({
			current: 1
		})
		this.updateBtnStatus(true)
		if(!canBuild){
			this.props.button.setButtonDisabled(['final_build'], true);
		}
	}
	// 判断是否可以建账
	getIsCanBuild = () => {
		let {currtypeList, groupAmountCtrl, globalAmountCtrl} = this.state;
		let canBuild = currtypeList.every((item) => {
			item = item.pk_currtype;
			let pass = this.checkCurrBalance(cacheUtils.getData(item));
			if(!pass){
				if(item === getMultiLangByID('20020401-000001')/* 国际化处理： 集团本币*/){
					if(groupAmountCtrl !== GL116.CTRL){
						pass = true;
					}
				}else if(item === getMultiLangByID('20020401-000002')/* 国际化处理： 全局本币*/){
					if(globalAmountCtrl !== GL118.CTRL){
						pass = true;
					}
				}
			}
			return pass;
		});
		return canBuild
	}
	// 建账
	finalBuild = () => {
		let canBuild = this.getIsCanBuild()
		if (!canBuild) {
			toast({ content: getMultiLangByID('20020401-000038'), color: 'warning' });/* 国际化处理： 期初数据未检查通过，不可以建账*/
			return;
		}
		let self = this;
		let url = '/nccloud/gl/voucher/initBuild.do';
		let data = {
			pk_accountingbook: self.state.pk_accountingbook,
			year: self.state.year,
		}
		ajax({
			loading: true,
			url,
			data,
			success: function (res) {
				const { data, error, success } = res;
				if(success){
					let allData = cacheUtils.getOpenData()
					allData.flag.isInitBuild = true
					cacheUtils.setOpenData(allData)
					toast({ content: getMultiLangByID('20020401-000039'), color: 'success' });/* 国际化处理： 建账成功*/
					self.props.updateButtonStatus(true)
					self.props.updateBuildStatus(true)
					self.props.afterBuildCloseModal()
					// self.props.button.setButtonDisabled(['final_build'], true);
					// self.props.pushTo('/', {})
					
				}else {
					toast({ content: error.message, color: 'warning' });
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'warning' });
				
			}
		});
	}

	getInitData = () => {
		let  data = cacheUtils.getFirstData()

		let year = data.info.year;
		let month = data.info.period;
		let currtypeList = [{
			pk_currtype: getMultiLangByID('20020401-000000'),/* 国际化处理： 组织本币*/
			name: getMultiLangByID('20020401-000000'),/* 国际化处理： 组织本币*/
		}];
		let trialData = {
			pk_accountingbook: data.info.pk_accountbook,
			pk_currtype: data.info.pk_currtype,
			year,
			month,
			versionDate: data.names.versiondate,
		};
		if (data.flag.isGroup) {
			currtypeList.push({
				pk_currtype: getMultiLangByID('20020401-000001'),/* 国际化处理： 集团本币*/
				name: getMultiLangByID('20020401-000001'),/* 国际化处理： 集团本币*/
			});
		}

		if (data.flag.isGlobal) {
			currtypeList.push({
				pk_currtype: getMultiLangByID('20020401-000002'),/* 国际化处理： 全局本币*/
				name: getMultiLangByID('20020401-000002'),/* 国际化处理： 全局本币*/
			});
		}

		this.setState({
			pk_accountingbook: data.info.pk_accountbook,
			// currtype: data.info.pk_currtype,
			book: data.names.book,
			yearPeriod: data.names.yearPeriod,
			versionDate: data.names.versiondate,
			isBUBalanceCheck: data.flag.isBUBalanceCheck,
			isStartBUSecond: data.flag.isStartBUSecond,
			isGlobal: data.flag.isGlobal,
			isGroup: data.flag.isGroup,
			groupAmountCtrl: data.flag.groupAmountCtrl,
			globalAmountCtrl: data.flag.globalAmountCtrl,
			currtypeList,
			year,
			month,
		}, () => {
			this.getTrial(trialData);		
		})
	}
	componentWillMount() {
		
	}

	componentDidMount() {
		initTemplate.call(this, this.props);
		this.getInitData()
		this.steps = [{
			title: getMultiLangByID('20020401-000032'),/* 国际化处理： 试算平衡*/
			description: getMultiLangByID('20020401-000033')/* 国际化处理： 进行中*/
		}, {
			title: getMultiLangByID('20020401-000034'),/* 国际化处理： 建账*/
			description: ''
		}];
		this.updateBtnStatus(false)
	}

	getTrial = (trialData) => {
		let self = this;
		let url = '/nccloud/gl/voucher/calculation.do';
		let {currtypeList} = self.state;
		currtypeList.map((item) => {
			let data = deepClone(trialData);
			data.pk_currtype = item.pk_currtype;
			self.getBalanceData(data, (requestData, data) => {self.processBalanceData(requestData, data)})
		});
	}

	/**
	 * 查询试算结果
	 */
	getBalanceData = (requestData, callback) =>{
		let url = '/nccloud/gl/voucher/calculation.do';
		ajax({
		    loading: true,
		    url,
		    data : requestData,
		    success: function (res) {
		        const { data, error, success } = res;
				if(success){
					if(callback){
						callback(requestData, data);
					}
				}
			},
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
			}
		});
	}

	/**
	 * 处理查询回来的试算数据
	 */
	processBalanceData = (requestData, data) => {
		let page = this;
		let dataList = data;
		organizeCalculationResult(dataList);
		let isZuzhiCan = true;
		if (requestData.pk_currtype === getMultiLangByID('20020401-000000')) {/* 国际化处理： 组织本币*/

			page.setState({
				dataList: dataList,
				isZuzhiCan,
			})
		}else if(requestData.pk_currtype === getMultiLangByID('20020401-000001')/* 国际化处理： 集团本币*/){
			let pass = page.checkCurrBalance(dataList);
			if(!pass && page.state.groupAmountCtrl === GL116.INFO){
				promptBox({
					color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
					title: getMultiLangByID('20020401-000029'),                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 询问*/
					content: getMultiLangByID('20020401-000030'),             // 提示内容,非必输/* 国际化处理： 集团本位币金额试算结果不平衡,是否允许建账?*/
					noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
					noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
					beSureBtnName: getMultiLangByID('20020401-000008'),          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 是*/
					cancelBtnName: getMultiLangByID('20020401-000009'),           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 否*/
					beSureBtnClick: () => {
						page.updateCtrlState2({groupAmountCtrl:GL116.UNCTRL})
					},   // 确定按钮点击调用函数,非必输
					cancelBtnClick: () => {
						page.updateCtrlState2({groupAmountCtrl:GL116.CTRL})
					}  // 取消按钮点击调用函数,非必输
				})
	
			}
		}else if(requestData.pk_currtype === getMultiLangByID('20020401-000002')/* 国际化处理： 全局本币*/){
			let pass = page.checkCurrBalance(dataList);
			if(!pass && page.state.globalAmountCtrl === GL118.INFO){
				promptBox({
					color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
					title: getMultiLangByID('20020401-000029'),                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 询问*/
					content: getMultiLangByID('20020401-000031'),             // 提示内容,非必输/* 国际化处理： 全局本位币金额试算结果不平衡,是否允许建账?*/
					noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
					noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
					beSureBtnName: getMultiLangByID('20020401-000008'),          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 是*/
					cancelBtnName: getMultiLangByID('20020401-000009'),           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 否*/
					beSureBtnClick: () => {
						page.updateCtrlState({globalAmountCtrl:GL118.UNCTRL});
					},   // 确定按钮点击调用函数,非必输
					cancelBtnClick: () => {
						page.updateCtrlState({globalAmountCtrl:GL118.CTRL});
					}  // 取消按钮点击调用函数,非必输
				})
	
			}
		}
		cacheUtils.setData(requestData.pk_currtype, dataList);
	}



	updateCtrlState = (data) => {
		let page = this;
		page.setState(data);
	}

	updateCtrlState2 = (data) => {
		let page = this;
		page.setState(data);
	}
	/**
	 * 校验每一币种合计是否平衡
	 */
	checkCurrBalance = (data) =>{
		let page = this;
		if(page.state.isBUBalanceCheck == true){
			return data.every((item) => {
				return item.balanResult.value === balanceOrient.equal;
			});
		}
		return data.every((item) => {
			if(item.pk_unit){
				return true;
			}else{ //合计
				return item.balanResult.value === balanceOrient.equal;
			}
		});
	}

	refresh = () => {

	}
	classItemClick(item, index){
		this.setState({
			resultData: item,
			selectedRowIndex: index
		});
	}
	handleCurrtypeChange(value) {
		let data = cacheUtils.getData(value);
		this.setState({
			currtype: value,
			dataList: data
		}, );
	}

	// 渲染第一步内容	
	renderFirstContent = () => {
		let { DragWidthCom } = this.props
		let { dataList, resultData, selectedRowIndex, isStartBUSecond } = this.state
		let defLeftWid = isStartBUSecond ? '230px' : '0'
		let isResultData = JSON.stringify(resultData) == "{}"
		let defaultValue = isResultData ? dataList[0] : resultData 
		let leftDom = 
                <div className={`nc-theme-area-split-bc ${isStartBUSecond ? 'table-left' : 'none'}`} fieldid='special_tree-area'>
				
					<div className="left-header nc-theme-title-font-c" fieldid='unit-result'>
						{getMultiLangByID('20020401-000037')}{/* 国际化处理： 业务单元结果*/}
					</div>
					<NCDiv fieldid="nav" areaCode={NCDiv.config.Area}>
						<div className="left-body nc-theme-area-split-bc">
							{dataList.length != 0 &&
								dataList.map((item, index) => {
									let tip = (<div>{item.name}</div>)
									return (
										<div
											key={item.name}
											fieldid={`${item.name}_item`}
											class={
												selectedRowIndex === index ? (
													'class-item selected-item nc-theme-tree-item-active-bgc'
												) : (
													'class-item'
												)
											}
											onClick={this.classItemClick.bind(this, item, index)}
										>	
											<Tooltip trigger="hover" placement="top" inverse={true} overlay={tip}>
												<div className="class-name nc-theme-tree-item-active-c">{item.name}</div>
											</Tooltip>
											<div className={(item.balanResult.display!==3) ? 'light_red result_txt' : 'result_txt'} fieldid='result'>{item.balanResult.display}</div>{/* 国际化处理： 平*/}
											
										</div>
									);
								})}
						</div>
					</NCDiv>
				</div>
        let rightDom = 
            <div className="table-area">
                <div className=''>
					<NCDiv fieldid="up" areaCode={NCDiv.config.Area}>
						{/* 财务科目试算结果 */}
						{defaultValue&&defaultValue.fi?(
							<TrialResultTable 
								typeName = {getMultiLangByID('20020401-000114')/*财务科目*/} 
								data={defaultValue.fi}
								showUnits = {this.state.isStartBUSecond}
							/>
						):''}
					</NCDiv>
					<NCDiv fieldid="down" areaCode={NCDiv.config.Area}>
						{/* 预算科目试算结果 */}
						{defaultValue&&defaultValue.pri?(
							<TrialResultTable
								typeName = {getMultiLangByID('20020401-000115')/*预算科目*/} 
								data={defaultValue.pri}
								showUnits = {this.state.isStartBUSecond}
							/>
						):''}
					</NCDiv>
				</div>
            </div>
		return(
			<div className="tree-table table_all nc-theme-area-split-bc">
				<DragWidthCom
					leftDom={leftDom}     //左侧区域dom
					rightDom={rightDom}     //右侧区域dom
					defLeftWid={defLeftWid}      // 默认左侧区域宽度，px/百分百
				/>
			</div>
		)
	}

	// 渲染第二部内容
	renderSecondContent = () => {
		let canBuild = this.getIsCanBuild()
		let resultTxtP,resultTxtSpan, iconCls, checkResult;
		
		if(canBuild){
			resultTxtP = `${this.state.year}${getMultiLangByID('20020401-000043')}`/* 国际化处理： 年度期初数据检查通过*/
			resultTxtSpan = getMultiLangByID('20020401-000041')/* 国际化处理： 可以期初建账*/
			iconCls = 'iconfont icon-wancheng'
			checkResult = 'pass'
		}else{
			resultTxtP = `${this.state.year}${getMultiLangByID('20020401-000044')}`/* 国际化处理： 年度期初数据检查未通过*/
			resultTxtSpan = getMultiLangByID('20020401-000042')/* 国际化处理： 不可以期初建账*/
			iconCls = 'iconfont icon-shibai'
			checkResult = 'failed'
		}
		return(
			<div className='check_container'>
				<NCRow>
					<div className="check_result">
						<div className='check_result_icon'>
							<i className={iconCls}/>	
						</div>
						<div className='check_result_des' fieldid={checkResult}>
							<p className='nc-theme-common-font-c'>{resultTxtP}</p>
							<span className='nc-theme-title-font-c'>{resultTxtSpan}</span>
						</div>
					</div>
				</NCRow>
			</div>
		)
	}

	render() {		
		let { current, book, yearPeriod, currtype, currtypeList } = this.state
		return (
			<div className="initialAccountFirst">
				{/* 国际化处理： 期初建账*/}
				{/* <div className="nc-bill-header-area" >
					<div className="header-title-search-area">
                        <h2 className="title-search-detail">{getMultiLangByID('20020401-000036')}</h2>
                    </div>
					<div className="header-button-area">									
						{this.props.button.createButtonApp({
							area: 'build_header',
							onButtonClick: onButtonClick.bind(this), 
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div> */}
				<div className='step'>
					<NCSteps current={current}>
						{this.steps.map(item => <NCStep key={item.title} title={item.title} 
						description={item.description}/>)}
					</NCSteps>
				</div>
				
				<NCDiv areaCode={NCDiv.config.SEARCH}>
					<div className="search_params nc-theme-area-split-bc">
						<div className="search_params_des nc-theme-common-font-c">
							{getMultiLangByID('20020401-000023')}：{/* 国际化处理： 财务核算账簿*/}
							<span fieldid='account' className='nc-theme-title-font-c'>{book}</span>
						</div>
						<div className="search_params_des nc-theme-common-font-c">
							{getMultiLangByID('20020401-000025')}：{/* 国际化处理： 会计期间*/}
							<span fieldid='yearPeriod' className='nc-theme-title-font-c'>{yearPeriod}</span>
						</div>
						{ current==0 ? <div className="search_params_des nc-theme-common-font-c">
							<p>{getMultiLangByID('20020401-000028')}：</p>{/* 国际化处理： 币种*/}
							<div>
								<Select
									// size="lg"
									fieldid='currtype'
									style={{ width: 130, height: 30 }}
									value={currtype}										
									onChange={this.handleCurrtypeChange.bind(this)}					   	
								>
									{currtypeList.map((item, index) => {
										return <Option value={item.pk_currtype} key={item.pk_currtype}>{item.name}</Option>
									} )}												
								</Select>
							</div>						
						</div> : ''}
					</div>
				</NCDiv>

				{ current==0 ? this.renderFirstContent() : this.renderSecondContent() }
				<NCDiv areaCode={NCDiv.config.BOTTOM}>
					<div className='footBtn nc-theme-area-split-bc'>
						<div className="header-button-area">									
							{this.props.button.createButtonApp({
								area: 'build_header',
								onButtonClick: onButtonClick.bind(this), 
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCDiv>			
			</div>
		)
									
	}
}

InitialAccountFirst = createPage({
	// initTemplate: initTemplate,
})(InitialAccountFirst)
export default InitialAccountFirst
