import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
import {high,base,ajax,deepClone, createPage,toast, getMultiLang, initMultiLangByModule, getMultiLangByID } from 'nc-lightapp-front';
import './index.less';
import {organizeCalculationResult} from '../../utils/utils'
import {accAdd} from '../../../public/common/method'
const { Refer } = high;
const { NCButton: Button,NCBackBtn,NCTable:Table, NCTooltip: Tooltip, NCDiv} = base;
import cacheUtils from '../../utils/cacheUtils'
import { onButtonClick, initTemplate} from '../OpenBalance/events';
import PrintModal from '../../../public/components/printModal'
import { printRequire, mouldOutput } from '../../../public/components/printModal/events'
import TrialResultTable from '../../../public/components/TrialResultTable'
import {FISelect} from '../../../public/components/base'
const Select = FISelect;
const Option = Select.FIOption;

//react-demo 13.html
class TrialBalance extends Component {
	constructor(props) {
		super(props);

		//每定义一个state 都要添加备注
		this.state = this.getInitState();
	}

	getInitState(){
		let  data = cacheUtils.getTrialData()
		let currtype = {
			key: data.info.pk_currtype,
			label: data.names.currtypeName
		}
		let year = data.info.year;
		let month = data.info.period;
		let pk_units = data.info.pk_units
		let pk_unit = data.info.pk_unit
	
		let state = {
			pk_accountingbook: data.info.pk_accountbook,
			currtype: currtype,
			book: data.names.book,
			yearPeriod: data.names.yearPeriod,
			versionDate: data.names.versiondate,
			isStartBUSecond: data.flag.isStartBUSecond,
			isGlobal: data.flag.isGlobal,
			isGroup: data.flag.isGroup,
			dataList: [],
			currtypeList: [],
			resultData: {},
			year: year,
			month: month,
			showPrintModal: false,
			pk_units: [] && pk_units,
			pk_unit: pk_unit,
			selectedRowIndex: 0,
			json: {}
		}
		return state;
	}
	

	goOpenBalance() {
		// let tableAll = cacheUtils.getOpenData()
		// cacheUtils.setOpenData(tableAll)
		this.props.pushTo('/', {})
	}

	componentDidMount() {
		this.requestCalculateResult();
		this.getCurrtypeList();
	}

	requestCalculateResult = ()=>{
		let page = this;		
		let {pk_accountingbook, currtype, year, month, versionDate} = this.state;
		let trialData = {
			pk_accountingbook: pk_accountingbook,
			pk_currtype: currtype.key,
			year,
			month,
			versionDate
		};

		let callback = (data)=>{
			let dataList = deepClone(data);
			organizeCalculationResult(dataList);
			page.setState({
				dataList: dataList,
				resultData: {},
				selectedRowIndex: dataList && (dataList.length - 1) || 0
			});
		}
		this.getTrial(trialData, callback);
	}

	getCurrtypeList = () => {
		
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		let data = {
			showGlobalCurr: (this.state.isGlobal ? '1' : '0'),
			showGroupCurr: (this.state.isGroup ? '1' : '0'),
		};
		

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
		        	self.setState({
		        		currtypeList: data,
		        	})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
	}

	getTrial = (trialData, callback) => {
		let self = this;
		let url = '/nccloud/gl/voucher/calculation.do';
		let data = trialData;

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					if(callback){
						callback(data);
					}
		            
		        }else {
		        	toast({ content: message.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
	}
	// 更新数据回调
	updateTrialData = (type, value='') => {
		let page = this;
		let {pk_accountingbook, currtype, year, month, versionDate} = page.state;
		let trialData = {
			pk_accountingbook: pk_accountingbook,
			pk_currtype: value ?  value.key : currtype.key,
			year,
			month,
			versionDate
		};

		let callback = (data)=>{
			if(type==='change'){
				toast({ title:  getMultiLangByID('20020401-000110'), color: 'success' });
				/* 国际化处理： 切换成功*/
			} else {
				toast({ title:  getMultiLangByID('20020401-000109'), color: 'success' });
				/* 国际化处理： 刷新成功*/
			}
			// toast({color:"success"})
			
			let dataList = deepClone(data);
			organizeCalculationResult(dataList);
			page.setState({
				dataList: dataList,
				resultData: {},
				selectedRowIndex: 0
			});
		}
		page.getTrial(trialData, callback);
		if(value){
			page.setState({
				currtype: value,
			});
		}
	}
	//刷新
	reFresh = () => {
		this.updateTrialData('refresh')
	}
	// 切换币种
	handleCurrtypeChange(value) {
		this.updateTrialData('change', value)
	}
	//打印
	showPrintModal(){
		this.setState({
			showPrintModal: true
		})
	}
	
	// 获取查询条件
	getPrintParams(){
		let {dataList,resultData,currtype,pk_accountingbook,yearPeriod,versionDate,pk_units, pk_unit, year, month} = this.state;
		let printParams = {}
		
		let isResultData = JSON.stringify(resultData) == "{}"
		let tempArr = isResultData ? dataList[0] : resultData 		
		if(tempArr){	
			let resultVal = tempArr.balanResult.value
			let resultTxt = (resultVal==='3') ? getMultiLangByID('20020401-000106') : getMultiLangByID('20020401-000107')/* 国际化处理： 试算结果平衡,试算结果不平衡*/
			printParams.hasTally = 'Y'  //是否包含未记账
			printParams.pk_accountingbook = pk_accountingbook //核算账簿
			printParams.year = year   //会计年
			printParams.period = month  //会计期间
			printParams.month = month  //会计期间
			printParams.versionDate = versionDate //版本日期
			printParams.pk_currtype = currtype.key //币种	
			// printParams.pk_units = pk_units // 二级账簿
			printParams.pk_unit = pk_unit // 二级账簿
			printParams.occurmargin = this.getDiffStr(tempArr.fi.incurDiff,tempArr.pri&&tempArr.pri.incurDiff);   //发生额差额
			printParams.balancemargin = this.getDiffStr(tempArr.fi.balanDiff, tempArr.pri&&tempArr.pri.balanDiff);  //余额差额
			printParams.result = resultTxt // 试算结果
			printParams.currname = currtype.label  //币种		
		}
		return printParams	
	}

	/**
	 * 生成差额字符串
	 */
	getDiffStr(fiDiff, priDiff){
		if((!fiDiff || fiDiff == 0.0) && (!priDiff || priDiff == 0.0))
			return '';
		let fiLable = getMultiLangByID('20020401-000114'); /* 财务科目 */
		let priLable = getMultiLangByID('20020401-000115'); /* 预算科目 */
		let str = '';
		if(fiDiff && fiDiff != 0.0){
			str = `${fiLable}：${fiDiff}`;
		}
		if(priDiff && priDiff != 0.0){
			str = `${str}    ${priLable}：${priDiff}`;
		}
		return str;
	}

	//打印框 确认
	handlePrint(data) {
		let printUrl = '/nccloud/gl/voucher/checkbalanceprint.do'
		let printParams = this.getPrintParams()
		let appcode = '20020TRYBL'
		let { ctemplate, nodekey } = data
		printParams.queryvo = data
		printRequire(printUrl, appcode, nodekey, ctemplate, printParams)
		this.setState({
			showPrintModal: false
		});
	}
	//模板输出
	showOutputModal(){
		let outputUrl = '/nccloud/gl/voucher/checkbalanceoutput.do'
		let printParams = this.getPrintParams()
		let appcode = '20020TRYBL'
        mouldOutput(outputUrl, appcode, 'nodekey', 'ctemplate', printParams)
	}
	
	classItemClick(item, index){
		this.setState({
			resultData: item,
			selectedRowIndex: index,
			pk_unit: item.pk_unit
		});
	}
	componentWillMount() {
		// let callback= (json) =>{
		// 	this.setState({
		// 		json:json,
		// 	},()=>{		  
		// 		initTemplate.call(this, this.props);
		// 	})
		// }
		// getMultiLang({moduleId:'20020401',domainName:'gl',currentLocale:'simpchn',callback});
		initTemplate.call(this, this.props);
		this.props.button.setButtonVisible(['save','cancel','calculate','init_build_drop','year_start','batch','import','export','add_line','del_line','clearData'], false);
	}
	renderContainer = () => {
		let { DragWidthCom } = this.props
		let data = this.state.dataList;
		let { resultData,selectedRowIndex, isStartBUSecond } = this.state
		let isResultData = JSON.stringify(resultData) == "{}"
		let defaultValue = isResultData ? data[selectedRowIndex] : resultData 
		let defLeftWid = isStartBUSecond ? '230px' : '0'
		let leftDom = 
                <div className={`{nc-theme-area-split-bc ${isStartBUSecond ? 'table-left' : 'none'}`}>
					
					<div className="left-header nc-theme-title-font-c">
						{getMultiLangByID('20020401-000037')}{/* 国际化处理： 业务单元结果*/}
					</div>
					<NCDiv fieldid="nav" areaCode={NCDiv.config.Area}>
						<div className="left-body nc-theme-area-split-bc">
							{data.length != 0 &&
								data.map((item, index) => {
									let tip = (<div>{item.name}</div>)
									return (
										<div
											fieldid={`${item.name}_item`}
											key={item.name}
											class={
												selectedRowIndex === index ? (
													'class-item selected-item nc-theme-tree-item-active-bgc'
												) : (
													'class-item'
												)
											}
											onClick={this.classItemClick.bind(this, item, index)}
										>
											<Tooltip trigger="hover" placement="top" inverse={true} overlay={tip}>
												<div className="class-name nc-theme-tree-item-active-c">{item.name}</div>
											</Tooltip>
											<div className={(item.balanResult.value!==3) ? 'light_red result_txt' : 'result_txt'}>
												{item.balanResult.display}
											</div>{/* 国际化处理： 平*/}
											
										</div>
									);
								})}
						</div>
					</NCDiv>
				</div>
        let rightDom = 
            <div className="table-area">
                <div className=''>
					<NCDiv fieldid="up" areaCode={NCDiv.config.Area}>
						{defaultValue&&defaultValue.fi?(
							<TrialResultTable
								typeName = {getMultiLangByID('20020401-000114')/*财务科目*/} 
								data={defaultValue.fi}
								showUnits = {this.state.isStartBUSecond}
							/>
						):''}
					</NCDiv>
					<div style={{height:'50px'}}></div>
					<NCDiv fieldid="down" areaCode={NCDiv.config.Area}>
						{defaultValue&&defaultValue.pri?(
							<TrialResultTable
								typeName = {getMultiLangByID('20020401-000115')/*预算科目*/} 
								data={defaultValue.pri}
								showUnits = {this.state.isStartBUSecond}
							/>
						):''}
					</NCDiv>
				</div>
            </div>
		return(
			<div className="tree-table table_all nc-theme-area-split-bc">
				<DragWidthCom
					leftDom={leftDom}     //左侧区域dom
					rightDom={rightDom}     //右侧区域dom
					defLeftWid={defLeftWid}      // 默认左侧区域宽度，px/百分百
				/>
			</div>
		)

	}
	render() {
		// let data = this.state.dataList;
		let { resultData,selectedRowIndex, book, yearPeriod, currtypeList, currtype } = this.state
		// let isResultData = JSON.stringify(resultData) == "{}"
		// let defaultValue = isResultData ? data[selectedRowIndex] : resultData 
		return (

			<div className="trialBalance">
				
				<NCDiv areaCode={NCDiv.config.SEARCH}>
					<div className="search_params">
						<div className="search_params_des nc-theme-common-font-c">
							{getMultiLangByID('20020401-000023')}：{/* 国际化处理： 财务核算账簿*/}
							<span className='nc-theme-title-font-c' fieldid='account'>{book}</span>
						</div>
						<div className="search_params_des nc-theme-common-font-c">
							{getMultiLangByID('20020401-000025')}：{/* 国际化处理： 会计期间*/}
							<span className='nc-theme-title-font-c' fieldid='yearPeriod'>{yearPeriod}</span>
						</div>
						<div className="search_params_des nc-theme-common-font-c">
							<p>{getMultiLangByID('20020401-000028')}：</p>{/* 国际化处理： 币种*/}
							<div>
								<Select
									// size="lg"
									fieldid='currtype'
									showClear={false}
									labelInValue={true}
									style={{ width: 130, height: 30}}	
									value={currtype}	
									onChange={this.handleCurrtypeChange.bind(this)}				   	
								>
									<Option value='' key='' disabled={true}>{getMultiLangByID('20020401-000028')}</Option>{/* 国际化处理： 币种*/}
									{currtypeList.map((item, index) => {
										return <Option value={item.pk_currtype} key={item.pk_currtype} >{item.name}</Option>
								} )}												
								</Select>		
							</div>						
						</div>
						<div className="header-button-area">								
							{this.props.button.createButtonApp({
								area: 'page_header',
								onButtonClick: onButtonClick.bind(this), 
								popContainer: document.querySelector('.header-button-area'),
								modalRelation:'trialBalanceModal'
							})}
						</div>
					</div>
				</NCDiv>

				{this.renderContainer()}

				<PrintModal
					noRadio={true}
					noCheckBox={true}
					appcode='20020TRYBL'
					visible={this.state.showPrintModal}
					handlePrint={this.handlePrint.bind(this)}
					handleCancel={() => {
						this.setState({
							showPrintModal: false
						})
					}}
				/>
			</div>

			)									
	}
}

TrialBalance = createPage({
	// initTemplate: initTemplate,
	// mutiLangCode: '2002'
})(TrialBalance)
export default TrialBalance
