import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';

import {base,deepClone, getMultiLangByID } from 'nc-lightapp-front';
const { NCButton: Button,NCTable:Table,NCModal: Modal, NCDiv} = base;
import './index.less';

/**
 * 集中重算结果报告
 */
export default class ReCountResultModal extends Component {

	constructor(props) {
		super(props);
		this.state={
			checkFormNow:false,//控制表单form回调
			results: [],
		}
	}

	static defaultProps = {
		show: false
	};


	handleGTypeChange =() =>{
		
	};


	getResults = (datas) => {
		this.setState({
			results: datas,
		})
	}
	handleCancel(){
        let { handleCancel } = this.props
        if (handleCancel) {
			let {results} = this.state;
            handleCancel(results);
        }
    }
	getTableColumns = () => {
		let columns = [
            {
				title: (<span fieldid='accountingbookName'>{getMultiLangByID('20020401-000096')}</span>),/* 国际化处理： 核算账簿名称*/
                dataIndex: "accountingbookName",
				key: "accountingbookName",
				render: (text, record, index) => (<span fieldid='accountingbookName'>{text}</span>)
			},
			{
                title: (<span fieldid='accountingbookCode'>{getMultiLangByID('20020401-000097')}</span>),/* 国际化处理： 核算账簿编码*/
                dataIndex: "accountingbookCode",
				key: "accountingbookCode",
				render: (text, record, index) => (<span fieldid='accountingbookCode'>{text}</span>)
			},
			{
                title: (<span fieldid='resultStr'>{getMultiLangByID('20020401-000098')}</span>),/* 国际化处理： 重算结果*/
                dataIndex: "resultStr",
				key: "resultStr",
				render: (text, record, index) => (<span fieldid='resultStr'>{text}</span>)		  
			},
			{
                title: (<span fieldid='reasonStr'>{getMultiLangByID('20020401-000101')}</span>),/* 国际化处理： 原因*/
                dataIndex: "reasonStr",
				key: "reasonStr",
				render: (text, record, index) => (<span fieldid='reasonStr'>{text}</span>)
			}
		]
		return columns
	}
	render() {
		let { show }= this.props;
		let mydata = deepClone(this.state.results);	
		let columns = this.getTableColumns()
		const emptyFunc = () => <span>{getMultiLangByID('20020401-000102')}！</span>/* 国际化处理： 这里没有数据*/	
		return(
			<Modal
				style={{width:'680px',height: '500px'}}
				fieldid='reCountResultModal'
				show={ show } 
				backdrop='static'
				onHide={this.handleCancel.bind(this)}
				>
				<Modal.Header closeButton>
					<Modal.Title>{getMultiLangByID('20020401-000103')}</Modal.Title>{/* 国际化处理： 集中重算结果报告*/}
				</Modal.Header >
				<Modal.Body>
					<NCDiv fieldid="result" areaCode={NCDiv.config.TableCom}>
						<Table
							columns={columns}
							data={mydata ? mydata : []}
							emptyText={emptyFunc}
						/>
					</NCDiv>
				</Modal.Body>
				<Modal.Footer>
					<Button fieldid='confirm' onClick={this.handleCancel.bind(this)}>{getMultiLangByID('20020401-000046')}</Button>{/* 国际化处理： 确定*/}
				</Modal.Footer>
			</Modal>
		)
	}
}
