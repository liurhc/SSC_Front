import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
import {base,ajax,deepClone, createPage, promptBox,toast,getMultiLang, initMultiLangByModule, getMultiLangByID, createPageIcon } from 'nc-lightapp-front';
const { NCDiv } = base;
import TrialBalance from '../TrialBalance';
import InitialAccountFirst from '../InitialAccountFirst';
import OpenTable from './OpenTable';
import MainSelect from './MainSelect';
import BeginYearCalculateModal from './BeginYearCalculateModal';
import BeginYearAdjustModal from './BeginYearAdjustModal';
import CentralReCountModal from './CentralReCountModal';
import CentralConstructorModal from './CentralConstructorModal';
import ResultModal from './ResultModal';  //集中建账结果报告
import ReCountResultModal from './ReCountResultModal';  //集中重算结果报告
import PrintModal from '../../../public/components/printModal'
import { printRequire, mouldOutput } from '../../../public/components/printModal/events'
import ImportFile from '../../../public/components/ImportFile'
// import ExportFile from '../../../public/components/ExportFile'
import { onButtonClick, initTemplate} from './events';
import cacheUtils from '../../utils/cacheUtils'
import lockAPI from '../../utils/lockAPI'
import ClickCounter from '../../../public/common/ClickCounter';
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';

class OpenBalance extends Component {
	constructor(props) {
		super(props);
		this.state = {
			appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
			ctemplate: '', //模板输出-模板
			nodekey: '',
			CalculateModalShow: false,  //显示年初重算模态框
			CentralReCountShow: false,  //显示集中重算模态框
			CentralConstructorShow: false, //显示集中建账模态框
			tableAll: null,              //表格全体数据
			ResultModalShow: false,     //集中建账结果
			ReCountResultModalShow: false, //集中重算结果
			initTableAll: null,  //初始查询出的表格全体数据
			showPrintModal: false,
			showImportModal: false,		// 导入导出框 显示
			tmpletType: '',  // 导入 or 导出
			json: {}
		}

		/* 处理连续点击事件 */
		this.counter = new ClickCounter(500);
		this.handlePrint = this.handlePrint.bind(this)
	}
	
	beforChangeSearchParams = (callback) => {
		let changesList = this.OpenTable.getChangeList()
		if (changesList.length > 0) {
			promptBox({
				color:'warning',
				content: getMultiLangByID('20020401-000077'),/* 国际化处理： 是否保存所做修改？*/
				beSureBtnClick: () => {
					// this.refreshSave();   
					this.saveTable(null,()=>{
						callback()
					})  
				},
				cancelBtnClick: () => {
					// this.refresh();
					this.freeLock()
					// let {initTableAll} = this.state
					let initTableAll = this.getOldTableAll()
					this.OpenTable.resetChangeData()
					this.OpenTable.resetChangeList()
					cacheUtils.setOpenData(initTableAll)
					callback()
				},
			});
		} else {
			callback()
		}
	}
	//集中重算确定
	handleCentralReModal(CentralReCountShow, datas) {
		let self = this;
		let {tableAll} = self.state
		setTimeout(() => self.refresh(), 1000 );
		this.setState({ CentralReCountShow: false });
		let url = '/nccloud/gl/voucher/batchReCalculate.do';
		let data;
		data = {
			"pk_accountbooks": datas.pk_accountbooks,	//核算账簿
			"year": tableAll.info.year,					//会计年
			"period": tableAll.info.period,					//会计期间
			"yearMonDay": tableAll.names.versiondate,			//启用期间  制单日期
			"assignAccountCode": datas.assignAccountCode,			//指定科目代码   科目代码的code
			"assVOs": datas.assVOs,	//指定科目辅助核算信息    暂时先传有type字符的三个，把参照中的相应三个值传入
			"handleInOrDecreType":datas.handleInOrDecreType, //损益科目处理方式 1：结转到指定科目， 2：不结转，3：按所选科目结转
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){					
					self.showReCountResultModal(data);		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
		this.setState({
			CalculateModalShow: false,
		})
	}

	//集中建账确定按钮
	handleCentralConstructorModal(CentralConstructorShow, datas) {
		this.setState({ CentralConstructorShow: false });
		let self = this;
		let {tableAll} = self.state
		let pk_accountbook = tableAll.info.pk_accountbook
		let url = '/nccloud/gl/voucher/batchInitBuild.do';
		let data;
		data = {
			"pk_accountbooks": datas,
			"year":tableAll.info.year,
			"period":tableAll.info.period,
			"versionDate":tableAll.names.versiondate,
			"pk_accountingbook": pk_accountbook,
		}

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
				const { data, error, success } = res;
		        if(success){
					self.showResultModal(data);		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

		this.setState({
			CalculateModalShow: false,
		})
	}

	// 集中建账  点击确定后 弹框
	showResultModal(data) {
		this.refs.ResultModal.getResults(data);
		this.setState({
			ResultModalShow: true,
		})
	}
	// 集中重算  点击确定后 弹框
	showReCountResultModal(data) {
		this.refs.ReCountResultModal.getResults(data);
		this.setState({
			ReCountResultModalShow: true,
		})
	}
	// 集中建账结果报告 关闭
	handleResultModalClose = (data) => {
		// let { tableAll } = this.state
		let { accountingbookCode } = this.state.tableAll.info
		// let resultCodes = data.map(item => item.accountingbookCode);
		if(data.length > 0){
			data.map((item)=>{
				if(item.accountingbookCode === accountingbookCode && item.resultCode ==='1'){
					let allData = cacheUtils.getOpenData()
					allData.flag.isInitBuild = true
					// this.setState({
					// 	tableAll
					// })
					cacheUtils.setOpenData(allData)
					this.updateButtonStatus(true)
					this.updateBuildStatus(true)
				}
			})
		}
		this.setState({ 
			ResultModalShow: false
		});	
	}
	// 集中重算结果报告 关闭
	handleRecountResultModalClose = (data) => {
		let {accountingbookCode, pk_accountbook} = this.state.tableAll.info;
		if(data && data.length>0){
			let length = data.length;
			for(let i = 0;  i < length; i++){
				let result = data[i];
				if(result.accountingbookCode === accountingbookCode){
					if(result.resultCode == "1"){
						this.refresh();
						this.OpenTable.resetChangeList();
					}
					break;
				}
			}
		}

		this.setState({ 
			ReCountResultModalShow: false
		});
	}

	//年初重算框确定按钮
	handleBeginYearCalculateModal = (CalculateModalShow, datas) => {
		let self = this;
		let {tableAll} = self.state
		let url = '/nccloud/gl/voucher/reCalculateInit.do';
		let data = {
			"pk_accountingbook": tableAll.info.pk_accountbook,
			"year": tableAll.info.year,
		}

		data = {
			"pk_accountbook": tableAll.info.pk_accountbook,	//核算账簿
			"year": tableAll.info.year,					//会计年
			"period": tableAll.info.period,					//会计期间
			"yearMonDay": tableAll.names.versiondate,			//启用期间  制单日期
			"accSubCodes": datas.accSubCodes,
			"assignAccountCode": datas.assignAccountCode,			//指定科目代码   科目代码的code
			"assVOs": datas.assVOs,	//指定科目辅助核算信息    暂时先传有type字符的三个，把参照中的相应三个值传入
			"handleInOrDecreType":datas.handleInOrDecreType, //损益科目处理方式 1：结转到指定科目， 2：不结转，3：按所选科目结转
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					toast({ content: getMultiLangByID('20020401-000074'), color: 'success' });/* 国际化处理： 年初重算成功*/
					setTimeout(() => self.refresh(), 1000 );

		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });

		    }
		});


		this.setState({
			CalculateModalShow: false,
		})

	}

	//年初调整框确定按钮
	handleBeginYearAdjustModal = (AdjustModalShow, datas) => {
		let self = this;
		let {tableAll} = self.state
		let url = '/nccloud/gl/voucher/adjustInitAfterBuild.do';
		let data = {
			"pk_accountingbook": tableAll.info.pk_accountbook,
			"year": tableAll.info.year,
		}

		data = {
			"pk_accountbook": tableAll.info.pk_accountbook,	//核算账簿
			"year": tableAll.info.year,					//会计年
			"period": tableAll.info.period,					//会计期间
			"yearMonDay": tableAll.names.versiondate,			//启用期间  制单日期
			"accSubCodes": datas.accSubCodes,
			"assignAccountCode": datas.assignAccountCode,			//指定科目代码   科目代码的code
			"assVOs": datas.assVOs,	//指定科目辅助核算信息    暂时先传有type字符的三个，把参照中的相应三个值传入
			"handleInOrDecreType":datas.handleInOrDecreType, //损益科目处理方式 1：结转到指定科目， 2：不结转，3：按所选科目结转
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					toast({ content: getMultiLangByID('20020401-000075'), color: 'success' });/* 国际化处理： 年初调整成功*/
					setTimeout(() => self.refresh(), 1000 );
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});

		this.setState({
			AdjustModalShow: false,
		})

	}
	//按钮保存
	saveTable = (index,callback) => {
		this.OpenTable.saveTable(index, callback);
	}
	getOldTableAll = () => {
		let oldTableData = this.OpenTable.getBeforeChangesData()
		let { tableAll } = this.state
		tableAll.data = oldTableData
		return tableAll
	}

	resetOldData = (oldTableAll) => {		
		let tableAll =  oldTableAll ? oldTableAll : this.getOldTableAll()
		this.setState({
			tableAll
		})
		this.OpenTable.resetChangeData()
	}

	// 按钮取消
	cancelBtn(){
		promptBox({
			color:'warning',
			title: getMultiLangByID('20020401-000047'),/* 国际化处理： 取消*/
			content: getMultiLangByID('20020401-000076'),/* 国际化处理： 确定要取消吗？*/
			beSureBtnClick: () => {
				this.resetOldData()

				this.freeLock()   
				this.OpenTable.resetChangeList()     
			},
			cancelBtnClick: () => {}
		});
	}
	// 按钮刷新
	reFresh(){
		let changeList = this.OpenTable.getChangeList()
		if(changeList.length>0){
			promptBox({
				color:'warning',
				content: getMultiLangByID('20020401-000077'),/* 国际化处理： 是否保存所做修改？*/
				beSureBtnClick: () => {
					this.refreshSave();            
				},
				cancelBtnClick: () => {
					this.refresh('',()=>{
						toast({ title: getMultiLangByID('20020401-000109'), color: 'success' });/* 国际化处理： 刷新成功*/
					});
					this.OpenTable.resetChangeList()
				}
			});
		} else{
			this.refresh('',()=>{
				toast({ title: getMultiLangByID('20020401-000109'), color: 'success' });/* 国际化处理： 刷新成功*/
			});
		}
		
	}

	refresh = (num='', callback) => {
		this.MainSelect.getTables(num, callback);
	}
	// 刷新前先保存
	refreshSave() {
		// 3表示刷新前保存
		this.saveTable(3);

	}
	// 跳转到辅助核算页
	goAccountAssistant = (index) => {
		let tableAll = this.state.tableAll;
		let changesList = this.OpenTable.getChangeList()
		if (changesList.length > 0) {
			promptBox({
				color:'warning',
				content: getMultiLangByID('20020401-000077'),/* 国际化处理： 是否保存所做修改？*/
				beSureBtnClick: () => {
					// this.refreshSave();   
					this.saveTable(null,()=>{
						this.OpenTable.beforGoAccount(index, tableAll)  
					})  
					// this.beforGoOtherPage(tableAll)       
				},
				cancelBtnClick: () => {
					this.freeLock()
					let initTableAll = this.getOldTableAll()
					// this.OpenTable.resetChangeData()
					this.resetOldData(initTableAll)
					this.OpenTable.resetChangeList()
					this.OpenTable.beforGoAccount(index, initTableAll)  
				},
			});
		} else {
			this.OpenTable.beforGoAccount(index, tableAll)
		}
	}
	// 试算平衡缓存
	beforGoOtherPage(tableAll){
		let info = {
			names: tableAll.names,
			flag: tableAll.flag,
			info: tableAll.info,
		}
		cacheUtils.setTrialData(info)
		cacheUtils.setOpenData(tableAll)
		// this.props.pushTo('/trialBalance', {})
		this.props.modal.show('trialBalanceModal');
	}
	// 试算平衡前是否保存
	goOtherPage = (tableAll) => {
		let changesList = this.OpenTable.getChangeList()
		if (changesList.length > 0) {
			promptBox({
				color:'warning',
				content: getMultiLangByID('20020401-000077'),/* 国际化处理： 是否保存所做修改？*/
				hasCloseBtn:true, //控制“X”按钮，显示true，不显示false，默认不显示
				beSureBtnClick: () => {
					this.saveTable(null,()=>{
						this.beforGoOtherPage(tableAll)   
					})  
				},
				cancelBtnClick: () => {
					this.freeLock()
					let initTableAll = this.getOldTableAll()
					// this.OpenTable.resetChangeData()
					this.resetOldData(initTableAll)
					this.OpenTable.resetChangeList()
					this.beforGoOtherPage(initTableAll)
				},
				// 点击“X”按钮，继续停留在期初余额页面，可继续编辑数据
				closeBtnClick: () => {}
			});
		} else {
			this.beforGoOtherPage(tableAll)
		}
	}
	// 试算平衡
	goTrialBalance() {
		let tableAll = this.state.tableAll;
		if (!tableAll) {
			toast({ content: getMultiLangByID('20020401-000078'), color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
			return
		}
		this.goOtherPage(tableAll)
	}

	beforGoInitBuild(tableAll){
		let info = {
			names: tableAll.names,
			flag: tableAll.flag,
			info: tableAll.info,
		}
		cacheUtils.setFirstData(info)
		cacheUtils.setOpenData(tableAll)
		this.props.modal.show('InitialAccountFirstModal');
	}
	// 跳转期初建账
	initBuild() {	
		let tableAll = this.state.tableAll;
		if (!tableAll) {
			toast({ content: getMultiLangByID('20020401-000078'), color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
			return
		}
		if (this.state.tableAll.flag.isInitBuild) {
			toast({ content: getMultiLangByID('20020401-000079'), color: 'warning' });/* 国际化处理： 本年度已经期初建账，不能进行重复建账！*/
			return
		}
		let changeList = this.OpenTable.getChangeList()
		if(changeList.length>0){
			promptBox({
				color:'warning',
				hasCloseBtn:true, //控制“X”按钮，显示true，不显示false，默认不显示
				content: getMultiLangByID('20020401-000077'),/* 国际化处理： 是否保存所做修改？*/
				beSureBtnClick: () => {
					this.saveTable(null, ()=>{
						this.beforGoInitBuild(tableAll) 
					})      
				},
				cancelBtnClick: () => {
					this.freeLock()
					let initTableAll = this.getOldTableAll()
					// this.OpenTable.resetChangeData()
					this.resetOldData(initTableAll)
					this.OpenTable.resetChangeList()
					this.beforGoInitBuild(initTableAll)
				},
				// 点击“X”按钮，继续停留在期初余额页面，可继续编辑数据
				closeBtnClick: () => {}
			});
		} else{
			this.beforGoInitBuild(tableAll)
		}
	}
	// 取消建账
	cancelInitBuild() {	
		let self = this;
		let {tableAll} = self.state	
		if (!tableAll) {
			toast({ content: getMultiLangByID('20020401-000078'), color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
			return
		}
		if (!tableAll.flag.isInitBuild) {
			toast({ content: getMultiLangByID('20020401-000080'), color: 'warning' });/* 国际化处理： 还未期初建账，不需要取消建账！*/
			return
		}
		let url = '/nccloud/gl/voucher/cancelInitBuild.do';
		let data = {
			"pk_accountingbook": tableAll.info.pk_accountbook,
			"year": tableAll.info.year,
		}

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){

					toast({ content: getMultiLangByID('20020401-000081'), color: 'success' });/* 国际化处理： 取消建账成功*/
					// 2表示取消建账
					// self.MainSelect.getRates(2)
					let {tableAll} = self.state;
					tableAll.flag.isInitBuild = false;
					self.setState({tableAll});
					self.MainSelect.updateState({isInitBuild:tableAll.flag.isInitBuild});
					self.updateButtonStatus(false);
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
	}

	// 年初重算
	ReCalculate(){
		if (!this.state.tableAll) {
			toast({ content: getMultiLangByID('20020401-000078'), color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
			return
		}
		if (this.state.tableAll.flag.isInitBuild) {
			toast({ content: getMultiLangByID('20020401-000082'), color: 'warning' });/* 国际化处理： 本年度已经期初建账，不能进行年初重算！*/
			return
		}

		let data = {
			versionDate: this.state.tableAll.names.versiondate,
			pk_accountingbook: this.state.tableAll.info.pk_accountbook,
			buorg: this.state.tableAll.info.pk_BUorg,
		};
		this.refs.BeginYearCalculateModal.getEndAccount(data);


		this.setState({
			CalculateModalShow: true,
		})
	}

	// 年初调整
	adJust(){
		if (!this.state.tableAll) {
			toast({ content: getMultiLangByID('20020401-000078'), color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
			return
		}
		if (!this.state.tableAll.flag.isInitBuild) {
			toast({ content: getMultiLangByID('20020401-000083'), color: 'warning' });/* 国际化处理： 本年度还未期初建账，不需进行年初调整！*/
			return
		}
		let data = {
			versionDate: this.state.tableAll.names.versiondate,
			pk_accountingbook: this.state.tableAll.info.pk_accountbook,
		};
		this.refs.BeginYearAdjustModal.getEndAccount(data);
		
		this.setState({
			AdjustModalShow: true,
		})
	}
	
	// 集中重算
	batchReCalculate(){
		if (!this.state.tableAll) {
			toast({ content: getMultiLangByID('20020401-000078'), color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
			return
		}
		let data = {
			versionDate: this.state.tableAll.names.versiondate,
			pk_accountingbook: this.state.tableAll.info.pk_accountbook,
			buorg: this.state.tableAll.info.pk_BUorg,
		};
		this.refs.CentralReCountModal.setInfo(data);
		this.setState({
			CentralReCountShow: true,
		})
	}
	// 集中建账
	batchBuild(){
		this.setState({
			CentralConstructorShow: true,
		})
	}
	// 导入
	handleImport(){
		this.setState({
			showImportModal: true,
			tmpletType: 'import'
		})
	}
	//导出
	handleExport(){
		this.setState({
			showImportModal: true,
			tmpletType: 'export'
		})
	}
	// 导入导出 数据整合
	getImportData(){
		let { tableAll, tmpletType } = this.state
		let data = {}
		if(tableAll){	
			data.year = tableAll.info.year  //会计年
			data.period = tableAll.info.period //会计期间
			data.versionDate = tableAll.names.versiondate // 版本日期
			data.pk_accountingbook = tableAll.info.pk_accountbook  //refpk
			data.pk_currtype = tableAll.info.pk_currtype  //币种
			if(tmpletType==='import'){
				data.action = '/nccloud/gl/voucher/initImport.do'  //导入接口地址，必传
				data.pk_unit = tableAll.info.pk_unit // 二级单位 refpk
			} else {
				data.action = '/nccloud/gl/voucher/initExport.do'  //导出接口地址，必传
				data.pk_units = tableAll.info.pk_units // 二级单位组 refpk
			}
		}	
		return data
	}
	
	//打印
	showPrintModal() {
		this.setState({
			showPrintModal: true
		})
	}
	// 获取查询条件
	getPrintParams(){
		let { tableAll } = this.state
		let printParams = {}
		if(tableAll){	
			printParams.pk_accountingbook = tableAll.info.pk_accountbook  //refpk
			printParams.year = tableAll.info.year  //会计年
			printParams.period = tableAll.info.period //会计期间
			printParams.versionDate = tableAll.names.versiondate // 版本日期
			printParams.pk_currtype = tableAll.info.pk_currtype  //币种
			printParams.pk_units = tableAll.info.pk_units // 二级单位组 refpk		
		}
		return printParams	
	}
	//打印框 确认
	handlePrint(data) {
		let printUrl = '/nccloud/gl/voucher/initbalanceprint.do'
		let printParams = this.getPrintParams()
		let {  appcode } = this.state
		let { ctemplate, nodekey } = data
		printParams.queryvo = data
		this.setState({
			ctemplate: ctemplate,
			nodekey: nodekey
        })
		printRequire(printUrl, appcode, nodekey, ctemplate, printParams)
		this.setState({
			showPrintModal: false
		});
	}

	//模板输出
	showOutputModal() {
		let outputUrl = '/nccloud/gl/voucher/initbalanceoutput.do'
		let printParams = this.getPrintParams()
		let { appcode, nodekey, ctemplate } = this.state
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)
    }
	freeLock = () =>{
		lockAPI.freeAllInitLock(true, ()=>{	
			let lockMap = cacheUtils.getLockMap()
			lockMap.clear()	
		})
	}
	componentWillMount() {
		// let callback= (json) =>{
		// 	this.setState({
		// 		json:json,
		// 	},()=>{		  
		// 		initTemplate.call(this, this.props);
		// 	})
		// }
		// getMultiLang({moduleId:'20020401',domainName:'gl',currentLocale:'simpchn',callback});
		initTemplate.call(this, this.props);

		this.props.button.setButtonDisabled(['excel_opt','import','export','print','out_template'], true);
		this.props.button.setButtonVisible(['save','cancel','add_line','del_line'], false);
		this.initButtonStatus(true)
		this.hideProBtn();
	}

	shouldComponentUpdate(){
		return true
	}
	componentWillUpdate(nextProps, nextState) {
		if(nextState.tableAll && nextState.tableAll.info.pk_accountbook){
			this.initButtonStatus(false)
			let { flag, info } = nextState.tableAll
			if(!flag.isInitBuild && (flag.currtype !== getMultiLangByID('20020401-000000'))) {				
				let { isStartBUSecond } = flag
				let { buSecond } = info
				let isEdit = isStartBUSecond ? (isStartBUSecond &&  buSecond.length === 1 ? true : false) : true
				this.updataImportBtnStatus(!isEdit)
				
			} else {
				this.updataImportBtnStatus(true) //view				
			}
		}
		if(!nextState.tableAll || !nextState.tableAll.info.pk_accountbook){
			this.initButtonStatus(true)  //view
		}
    }
	
	componentDidMount(){
		let data = cacheUtils.getOpenData()
		this.setState({
			tableAll: data,
		})
		// 添加 离开页面的监听事件
		// window.addEventListener("beforeunload", this.freeLock)
		//点浏览器‘x’,弹提示框
        window.onbeforeunload = () => {
			let changesList = this.OpenTable.getChangeList()
			if (changesList.length > 0) {
	    		return '';
			}
		}
		//关闭浏览器,释放锁
	    window.onunload = () => {
			// 同步ajax触发
			lockAPI.freeAllInitLock(false, ()=>{	
				let lockMap = cacheUtils.getLockMap()
				lockMap.clear()	
			})
		}
	}
	componentWillUnmount(){
		// 移除 离开页面的监听事件
		// window.removeEventListener("beforeunload", this.freeLock)
	}
	updateButtonStatus = (disable) => {
		// 已建账 ，不可以导入
		// 未建账 ， 1.未启用二级账簿可以编辑(导入)；2.启用二级账簿且只选一个二级账簿时，可以编辑（导入）
		this.props.button.setButtonDisabled(['print','out_template','export'], false);
		this.props.button.setButtonDisabled(['import','build','re_calculate'], disable);
		this.props.button.setButtonDisabled(['cancel_build','adjust'], !disable);
	}
	// 更新导入按钮状态
	updataImportBtnStatus = (disable) => {
		// 1.未启用二级账簿可以编辑(导入)；2.启用二级账簿且只选一个二级账簿时，可以编辑（导入）
		this.props.button.setButtonDisabled(['import'], disable);
		this.props.button.setButtonVisible(['save','cancel'], !disable);
	}
	initButtonStatus = (disable) => {
		let self = this
		self.props.button.setButtonDisabled(['refresh','calculate','init_build_drop','year_start','more'], disable);
		self.props.button.setButtonVisible(['save','cancel'], !disable);
	}
	// 期初建账、集中建账，更新建账状态
	updateBuildStatus = (isInitBuild) => {
		this.MainSelect.updateBuildStatus(isInitBuild)
	}
	// 更新Table数据
	updateTableData = (data) => {
		this.setState({
			tableAll: data,
		})
	}
	
	/**
	 * 显示清空期初余额按钮
	 */
	onTitleClick = (event) =>{
		let page = this;
		page.counter.count();
		let clickTimes = page.counter.getCount();

		if(clickTimes >= 6){
			this.showProBtn();
		}
	}

	showProBtn = () =>{
		let page = this;
		page.props.button.setButtonVisible(["clearData"], true);
	}

	hideProBtn = () => {
		let page = this;
		page.props.button.setButtonVisible(["clearData"], false);
	}
	// 页面弹框
	modalContent = (type) => {
		let content 
		switch (type) {
			case 'trialBalance': content= <TrialBalance />
				break;
			case 'InitialAccountFirst': content= <InitialAccountFirst 
			updateButtonStatus = {this.updateButtonStatus}
			updateBuildStatus = {this.updateBuildStatus}
			afterBuildCloseModal = {this.afterBuildCloseModal}/> 
				break;
		}
		return (
            <div className="modalContent">
               {content}
            </div>
        )
	}
	// 建账后关闭弹框
	afterBuildCloseModal = () => {		
		this.props.modal.close('InitialAccountFirstModal');
	}
	//弹框确定按钮事件
	beSureBtnClick= () => {

	}

	closeModalEve = () => {

	}
	render() {	
		let { tableAll,json } = this.state
		let pk_accountingbook,pk_org,isInitBuild
		if(tableAll && tableAll.info){
			pk_accountingbook = tableAll.info.pk_accountbook,
			pk_org = tableAll.info.pk_org
			isInitBuild = tableAll.flag.isInitBuild
		}
		const {modal } = this.props;
    	let { createModal } = modal;
		return (
			<div className="nc-bill-list openBalance">
				<HeaderArea 
					title = {this.props.getSearchParam('n')} /* 总账期初余额 */
					titleClick = {this.onTitleClick}
					searchContent = {tableAll? isInitBuild ? (
						<span className='buildblock build_y'>{getMultiLangByID('20020401-000084')}</span>/* 国际化处理： 已建账*/
					) : (
						<span className='buildblock build_n'>{getMultiLangByID('20020401-000085')}</span>	/* 国际化处理： 未建账*/
					) : ''}
					btnContent = {this.props.button.createButtonApp({
						area: 'page_header',
						onButtonClick: onButtonClick.bind(this), 
						withAutoFocus: false
						// popContainer: document.querySelector('.header-button-area')
					})}
				/>
				<NCDiv areaCode={NCDiv.config.SEARCH}>
					<div className="nc-bill-search-area">
						<MainSelect
							// json={json}
							appcode={this.state.appcode}
							onSendData={ (tableAll) => {
								this.setState({
									tableAll,
								})

							}}
							beforChangeSearchParams = {this.beforChangeSearchParams}
							updateButtonStatus = {this.updateButtonStatus}
							MainSelectRef={(init) => { this.MainSelect = init }}
						/>
					</div>
				</NCDiv> 

				<div className="nc-bill-table-area">
					<OpenTable
						tableAll={this.state.tableAll}
						goAccountAssistant={this.goAccountAssistant}
						updateTableData={this.updateTableData}
						OpenTableRef={(init) => { this.OpenTable = init }}
						onRefresh={() => {						
							this.refresh();
						}}
					/>
				</div>
				
				<BeginYearCalculateModal
					show={this.state.CalculateModalShow}
					ref='BeginYearCalculateModal'
					pk_accountingbook = {pk_accountingbook}
					buorg = {pk_org}
					appcode={this.state.appcode}
					onConfirm={(data) => {
						this.handleBeginYearCalculateModal(this.state.CalculateModalShow, data);
					}}
					handleCancel={() => {
						this.setState({ 
							CalculateModalShow: false
						});
					}}
				/>

				<BeginYearAdjustModal
					show={this.state.AdjustModalShow}
					ref='BeginYearAdjustModal'
					pk_accountingbook = {pk_accountingbook}
					buorg = {pk_org}
					appcode={this.state.appcode}
					onConfirm={(data) => {
						this.handleBeginYearAdjustModal(this.state.AdjustModalShow, data);
					}}
					handleCancel={() => {
						this.setState({ 
							AdjustModalShow: false
						});
					}}
				/>
				
				<CentralReCountModal
					show={this.state.CentralReCountShow}
					appcode={this.state.appcode}
					ref='CentralReCountModal'
					buorg={pk_org}
					onConfirm={(data) => {
						this.handleCentralReModal(this.state.CentralReCountShow, data);
					}}
					handleCancel={() => {
						this.setState({ 
							CentralReCountShow: false 
						});
					}}
				/>
				{/* 集中建账框 */}
				<CentralConstructorModal
					// json={json}
					show={this.state.CentralConstructorShow}
					appcode={this.state.appcode}
					ref='CentralConstructorModal'
					onConfirm={(data) => {
						this.handleCentralConstructorModal(this.state.CentralConstructorShow, data);
					}}
					onCancel={() => {
						let CentralConstructorShow = false;
						this.setState({ CentralConstructorShow });
					}}
				/>
				
				{/* 集中建账结果报告 */}
				<ResultModal
					// json={json}
					show={this.state.ResultModalShow}
					ref='ResultModal'
					onConfirm={(assDatas) => {}}
								
					handleCancel={this.handleResultModalClose}
				/>

				<ReCountResultModal
					// json={json}
					show={this.state.ReCountResultModalShow}
					ref='ReCountResultModal'
					onConfirm={() => {}}
					handleCancel={this.handleRecountResultModalClose}
				/>
				<ImportFile
					tmpletType={this.state.tmpletType}
					impData = {this.getImportData()}
					visible={this.state.showImportModal}
					handleRefresh = {this.refresh.bind(this)}
					handleCancel={() => {
						this.setState({
							showImportModal: false
						})
					}}
				/>
				
				<PrintModal
					noRadio={true}
					noCheckBox={true}
					appcode={this.state.appcode}
					visible={this.state.showPrintModal}
					handlePrint={this.handlePrint}
					handleCancel={() => {
						this.setState({
							showPrintModal: false
						})
					}}
				/>
				<div>
					{createModal('InitialAccountFirstModal', {
						title: getMultiLangByID('20020401-000036'), /* 国际化处理： 期初建账*/
						content: this.modalContent('InitialAccountFirst'), //弹框内容，可以是字符串或dom
						closeModalEve: this.closeModalEve, //关闭按钮事件回调
						// userControl:true,  // 点 确定/取消 按钮后，是否自动关闭弹框.true:手动关。false:自动关,默认false
						size:'xlg', //  模态框大小 sm/lg/xlg
						bodyHeight: '540px',
						noFooter : true, //是否需要底部按钮,默认有footer,有false,没有true
						closeByClickBackDrop:false,//点击遮罩关闭提示框，true是点击关闭，false是阻止关闭,默认是false
						hasBackDrop:true,//显示遮罩层，显示是true，不显示是false，默认是true
						zIndex:200//遮罩的层级为zIndex，弹框默认为zIndex+1。默认为200，非比传项
					})}
        		</div>
				<div>
					{createModal('trialBalanceModal', {
						title: getMultiLangByID('20020401-000032'), /* 国际化处理： 试算平衡*/
						content: this.modalContent('trialBalance'), //弹框内容，可以是字符串或dom
						closeModalEve: this.closeModalEve, //关闭按钮事件回调
						size:'xlg', //  模态框大小 sm/lg/xlg
						bodyHeight: '540px',
						noFooter : true, //是否需要底部按钮,默认有footer,有false,没有true
					closeByClickBackDrop:false,//点击遮罩关闭提示框，true是点击关闭，false是阻止关闭,默认是false
					hasBackDrop:true,//显示遮罩层，显示是true，不显示是false，默认是true
					zIndex:200//遮罩的层级为zIndex，弹框默认为zIndex+1。默认为200，非比传项
					})}
        		</div>

			</div>
		)		
	}
}


OpenBalance = createPage({})(OpenBalance)
export default OpenBalance
