import React, { Component } from 'react';

import {base, getMultiLangByID } from 'nc-lightapp-front';
const { NCButton: Button,NCRow:Row,NCCol:Col,NCLoading:Loading,NCModal:Modal} = base;
import './index.less';
import ReferLoader from '../../../../public/ReferLoader/index.js'


export default class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
			accountingbook:[],
			// accountingbook: {
			// 	refname: '',
			// 	refpk: '',
			// },  //核算账簿
			checkedAll:false,
			checkedArray: [
			  false,
			  false,
			  false,
			],
		}
	
	}

	static defaultProps = {		
		show: false
	};


	handleGTypeChange =() =>{
		
	};

	onConfirm(){
		let {accountingbook} = this.state
		let pk_accountingbooks = accountingbook.map(item => item.refpk);
		this.props.onConfirm(pk_accountingbooks)
	}

	//财务核算账簿refer
	renderReferLoader() {
		let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
		let defaultValue = {refname: this.state.accountingbook.refname, refpk: this.state.accountingbook.refpk}
		return(
			<Row>
				<Col xs={12} md={12}>
					<ReferLoader
						tag = 'test'
						fieldid='account'
						refcode = {referUrl}
						value = {this.state.accountingbook}
						// value = {defaultValue}
						isMultiSelectedEnabled = {true}
						queryCondition = {() => {
							return {
								"TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
								"appcode": this.props.appcode
							}
						}}
						onChange = {(v)=>{
							this.setState({
								accountingbook: v,
							})
						  }}
					/>
				</Col>
			</Row>
		)
	}
	render() {
		let { show }= this.props;
		
		return <Modal
			style={{width:'410px',height: '210px'}}
			fieldid='centralConstructor'
			show={ show } 
			backdrop='static'
			onHide={() => {this.props.onCancel(false)}}
		    >
			<Modal.Header closeButton>
				<Modal.Title>
					{getMultiLangByID('20020401-000023')}{/* 国际化处理： 财务核算账簿*/}
				</Modal.Title>
			</Modal.Header >
			<Modal.Body >
				{this.renderReferLoader()}			
			</Modal.Body>
			<Modal.Footer>
				<Button colors='primary' fieldid='confirm' onClick={this.onConfirm.bind(this)}>{getMultiLangByID('20020401-000046')}</Button>{/* 国际化处理： 确定*/}
				<Button fieldid='cancel' onClick={() => {this.props.onCancel(false)}}>{getMultiLangByID('20020401-000047')}</Button>{/* 国际化处理： 取消*/}
			</Modal.Footer>

			<Loading fullScreen showBackDrop={true} show={this.state.isLoading} />

		</Modal>;
	}
}
