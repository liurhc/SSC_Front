import React, { Component } from 'react';

import {base,ajax,deepClone,high, getBusinessInfo, toast,getMultiLang,getMultiLangByID } from 'nc-lightapp-front';
const { NCButton: Button,NCRadio:Radio,NCIcon:Icon,NCLoading:Loading,NCModal:Modal} = base;
const { Transfer } = high
import ReferWraper from '../../../../public/components/ReferWraper'
import ReferLoader from '../../../../public/ReferLoader/index.js';
import {accountRefCode} from '../../../../public/ReferLoader/constants.js';
import AssidModal from '../../../../public/components/assidModal'
import './index.less';

export default class BeginYearAdjustModal extends Component {

	constructor(props) {
		super(props);
		this.state={
			showAssidModal: false,
			assidCondition: '',
			selectedValue: '1',
			isLoading: false,
			versionDate: '',
			pk_accountingbook: '',
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buorg: '',
			accasoaList: [],
			targetKeys: [],
			mockData: [],
			selectedKeys: [],
			assDisplay: '',
			jsonData: [],
			assid:'',
			assValues:[],
			json: {}
		}
	
	}
	
	componentWillMount() {
		// let callback= (json) =>{
		// 	this.setState({
		// 		json:json,
		// 		assDisplay: json['20020401-000045'],/* 国际化处理： 请选择辅助核算*/
		// 	})
		// }
		// getMultiLang({moduleId:'20020401',domainName:'gl',currentLocale:'simpchn',callback});
	}
	componentDidMount() {

	}

	getEndAccount = (enddata) => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryEndAccount.do';
		let data = {
			pk_accountingbook: enddata.pk_accountingbook,
			versionDate: enddata.versionDate,
		}

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
		        	let accasoaList = data;
		        	accasoaList.forEach(function (item, index) {
		        		item.key = item.accountCode;
		        		item.title = item.displayName;
		        	})
					self.setState({
						accasoaList,
						pk_accountingbook: enddata.pk_accountingbook,
						versionDate: enddata.versionDate,
						buorg: enddata.buorg,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {

		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleRadioChange(value) {
		this.setState({selectedValue: value});
	}


	handleGTypeChange =() =>{
		
	};
	onConfirm() {
		let {selectedValue,targetKeys,accasoa,assValues} = this.state
		if(selectedValue === '1' && !accasoa.refpk) {
			toast({ content: getMultiLangByID('20020401-000048'), color: 'warning' });/* 国际化处理： 请选择核算的会计科目！*/
			return;
		}
		let assignAccountCode = selectedValue === '1' ? accasoa.nodeData.refcode : ''
		let data = {
			accSubCodes: targetKeys,
			assignAccountCode: assignAccountCode,
			assVOs: assValues,
			handleInOrDecreType: selectedValue,
		};
		let newdata = deepClone(data);
		if (newdata.handleInOrDecreType != '1') {
			newdata.assignAccountCode = '';
			newdata.assVOs = [];

		}
		this.props.onConfirm(newdata)
		this.setState({
			assDisplay: '',
			assValues: [],
			targetKeys: [],
			accasoa: {           //科目
				refname:'',
				refpk: '',
			}
		})
	}
	handleCancel(){
		let {handleCancel} = this.props
		if(handleCancel){
			handleCancel()
		}
		this.setState({
			assDisplay: '',
			assValues: [],
			targetKeys: [],
			accasoa: {           //科目
				refname:'',
				refpk: '',
			}
		})
	}
	// 显示辅助核算框
	showAssidModal() {
		let businessInfo = getBusinessInfo()
        let businessDate = businessInfo.businessDate
		let { accasoa } = this.state
		let { pk_accountingbook, buorg } = this.props;
		let assidCondition = {
			pk_accountingbook: pk_accountingbook,
			pk_accasoa: accasoa.refpk,
			prepareddate: businessDate.split(' ')[0], //业务日期
			pk_org: buorg, //财务组织
			assData: [],
			assid: '',
			checkboxShow: true
		};
		this.setState({
			showAssidModal: true,
			assidCondition: assidCondition,
		})
	}
	handleAssidOK(modalData) {
		let assValues = [];
		if(modalData && modalData.data && modalData.data.length > 0){
			modalData.data.map((item) => {
				let data = {
					pk_checktype:{value:item.pk_Checktype},
					checkvaluecode:{value:item.checkvaluecode},
					checkvaluename:{value:item.checkvaluename},
					pk_checkvalue:{value:item.pk_Checkvalue}
				}
				assValues.push(data);
			});
		}
		this.setState({
			showAssidModal: false,
			assDisplay: modalData.assname,
			assValues: assValues,
			assid: modalData.assid
		});
	}

	onTargetKeysChange = (targetKeys) => {
        this.setState({
            targetKeys:targetKeys
        })
	}
	
	render() {
		let { show, appcode }= this.props;

		let accasoaIf;

		this.state.selectedValue == '1' ?  (accasoaIf = {}) : (accasoaIf = {display: 'none'});


		//控制选择按钮的状态
		let { accasoa,accasoaList, targetKeys } = this.state
		let dis = accasoa.refpk ? false:true
		const transferProps = {
            dataSource: accasoaList,
			targetKeys: targetKeys,
			showSearch: true,
			searchPlaceholder: getMultiLangByID('20020401-000111'), /* 国际化处理： 搜索会计科目*/
            onTargetKeysChange: this.onTargetKeysChange,
			lazy: {container:"modal"},
            listRender: ({ key, title }) => title
            // listRender: ({ key, title }) => key + ' ' + title
		};
		
		return <Modal style={{width:'680px',height: '510px'}}
			fieldid='beginYearAdjust'
			show={ show } 
			backdrop='static'
			onHide={this.handleCancel.bind(this)}
		    >
			<Modal.Header closeButton>
				<Modal.Title>
					{getMultiLangByID('20020401-000049')}{/* 国际化处理： 选择需要年初调整的末级科目*/}
				</Modal.Title>
			</Modal.Header >
			<Modal.Body>
				<div className="beginYearAdjustModal">
					<div className="mark nc-theme-area-bgc nc-theme-common-font-c nc-theme-area-split-bc">
						<div className="mark_inco">
							<Icon type='uf-i-c-2' className="mark_inco_i nc-theme-area-bgc"/>
						</div>
						{getMultiLangByID('20020401-000050')}：{getMultiLangByID('20020401-000051')}，{getMultiLangByID('20020401-000052')}，{getMultiLangByID('20020401-000054')}？{/* 国际化处理： 提示,一定不要选择手工修改过期初余额的科目,否则将造成修改过的期初余额被冲掉,继续吗*/}
					</div>
					<div className='transfer'>
						<Transfer  {...transferProps} />
					</div>
					

					<div className="cnetral_style" fieldid='carryover'>
						<div className='lable_name nc-theme-common-font-c'>{getMultiLangByID('20020401-000055')}：</div>{/* 国际化处理： 结转方式*/}
						<div className='con_radio'>
							<Radio.NCRadioGroup
								name="fruit"
								selectedValue={this.state.selectedValue}							
								onChange={this.handleRadioChange.bind(this)}
							>						
								<Radio value="1" >	
									{getMultiLangByID('20020401-000056')}	{/* 国际化处理： 结转到指定科目*/}
								</Radio>
								<Radio value="2" >{getMultiLangByID('20020401-000057')}</Radio>{/* 国际化处理： 不结转*/}
								<Radio value="3" >{getMultiLangByID('20020401-000058')}</Radio>{/* 国际化处理： 按所选科目结转*/}

							</Radio.NCRadioGroup>
						</div>

				    	<div className="con_ref" style={accasoaIf}>
							<div className="account_ref_w200">
								<ReferLoader
									tag='account'
									fieldid='account'
								   	showStar={true}
								   	refcode={accountRefCode}
									value={this.state.accasoa}
									isAccountRefer={true}   
									onlyLeafCanSelect={true}  //是否只有叶子节点可选
						   			queryCondition = {() => {
		 								let businessInfo = getBusinessInfo()
         								let businessDate = businessInfo.businessDate
						   				return {
						   					"dateStr": businessDate.split(' ')[0],
											"pk_accountingbook": this.state.pk_accountingbook,
											"isDataPowerEnable": 'Y',
											"DataPowerOperationCode" : 'fi',
											'TreeRefActionExt': "nccloud.web.gl.ref.FilterInOrDecreaseAccSqlBuilder",
											appcode: appcode
						   				}
						   			}}
						   			onChange={(v)=>{
										this.setState({
											accasoa: v,
											assDisplay: '',
										})

						   			  }
						   			}
						   		/>
						   			
							</div>

							{/* 辅助核算 */}	
							<ReferWraper
								outStyle={{width: '200px', marginLeft: '10px'}}
								placeholder = {getMultiLangByID('20020401-000045')} /* 国际化处理： 请选择辅助核算*/
								display={this.state.assDisplay}
								disabled={dis}
								onClick={this.showAssidModal.bind(this,this.state.assid)}
							/>

				    	</div>
					</div>
					
				</div>


			</Modal.Body>
			<Modal.Footer>
				<Button colors='primary' fieldid='confirm' onClick={this.onConfirm.bind(this)}>{getMultiLangByID('20020401-000046')}</Button>{/* 国际化处理： 确定*/}
				<Button fieldid='cancel' onClick={this.handleCancel.bind(this)}>{getMultiLangByID('20020401-000047')}</Button>{/* 国际化处理： 取消*/}
			</Modal.Footer> 

			<Loading fullScreen showBackDrop={true} show={this.state.isLoading} />
			
			<AssidModal
				pretentAssData={this.state.assidCondition}
				showOrHide = {this.state.showAssidModal}
				onConfirm={this.handleAssidOK.bind(this)} 
				showDisableData = {false}
				handleClose={() => {
					this.setState({ 
						showAssidModal: false
					 });
				}}
			/>

		</Modal>;
	}
}
