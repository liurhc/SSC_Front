import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
import { base,ajax,deepClone,getBusinessInfo, createPage,toast, promptBox, getMultiLang,initMultiLangByModule, getMultiLangByID } from 'nc-lightapp-front';
import {FISelect} from '../../../../public/components/base'
const { NCRow:Row,NCCol:Col,NCLoading:Loading} = base;
const Select=FISelect;
const Option = Select.FIOption;
import './index.less';
import BusinessUnitVersionDefaultAllTreeRef from '../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';
import ReferLoader from '../../../../public/ReferLoader/index.js';
import { onButtonClick, initTemplate} from '../events';
import getDefaultAccountBook from '../../../../public/components/getDefaultAccountBook'
import cacheUtils from '../../../utils/cacheUtils'
import lockAPI from '../../../utils/lockAPI'

class MainSelect extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false, //遮罩
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			isInitBuild: false,  //是否已建账 2
			isStartBUSecond: false, //是否启用二级核算 2
			isGlobal: false,   //是否启用全局本币 2
			isGroup: false,   //是否启用集团本币 2
			isYearStart: true,     //是否年初 2
			globalCurrType: null, //全局本币 2			
			groupCurrType: null,  //集团本币 2
			localCurrType: null, //组织本币 2
			groupAmountCtrl: '1',
			globalAmountCtrl: '1',
			NC001: 'raw_convert', 
			NC002: 'raw_convert', 
			buSecond: [],      //选择的二级单位
			accasoa: {           //定位科目
				refname:'',
				refpk: '',
			},
			versiondate: '',     //制单日期 ？？科目版本 2
			yearList: [],       //会计年下拉列表
			selectedYear: '',    //选中的会计年 2
			currPeriod:'',
			yearPeriod: '',      //会计期间   给后端的year与返回的year一致，则是后端返回的year和period拼接；否则用selectedYear + '01'  2
			currtype: '',      //选择的币种 2
			currtypeName: '',  //选择的币种名称 2
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			// tableDatas: null,
			// rates: {}, //汇率
			pk_org: '',
			json: {}
		};
		this.tableAll = null
		// this.getInfoByBook.bind(this);
		// this.getYears.bind(this);

	}
	componentWillMount() {
		initTemplate.call(this, this.props);
		this.getInfoByBook.bind(this);
		this.getYears.bind(this);
		this.getCurrtypeList();
		this.botQueryAccbookInfo.bind(this);
	}
	componentDidMount() {
		let  allData = cacheUtils.getOpenData()
		if(allData){
			let {data,flag,info,names} = allData
			this.setState({
				isInitBuild: flag.isInitBuild, //是否已建账
				isStartBUSecond: flag.isStartBUSecond,
				isYearStart: flag.isYearStart, //启用期间是否年初
				isGlobal: flag.isGlobal,   //是否启用全局本币
				isGroup: flag.isGroup,   //是否启用集团本币

				currtype: flag.currtype,     //币种，组织本币特殊
				globalCurrType: flag.globalCurrType, //全局本币 //下同，用来判断列是否可编辑用的	
				groupCurrType: flag.groupCurrType,  //集团本币
				localCurrType: flag.localCurrType, //组织本币

				isBUBalanceCheck: flag.isBUBalanceCheck, //是否二级按业务单元进行平衡检查 true 校验所有业务单元试算平衡结果，false 只校验合计结果是否平衡
				groupAmountCtrl: flag.groupAmountCtrl,
				globalAmountCtrl: flag.globalAmountCtrl,

				accountingbook: {
					refpk: info.pk_accountbook,
					refname: names.book,
				}, 

				NC001: info.NC001,
				NC002: info.NC002,
				selectedYear: info.year,
				currPeriod: info.currPeriod,
				yearList: info.yearList,
				currtypeList: info.currtypeList,
				buSecond: info.buSecond,
				pk_org: info.pk_org,
				currtypeName: names.currtypeName,
				yearPeriod: names.yearPeriod,
				versiondate: names.versiondate,
			},()=>{
				let flagbtn = this.state.isInitBuild
				if(flagbtn){
					this.props.updateButtonStatus(true);
				}else {
					this.props.updateButtonStatus(false);
				}
			})
		}else{
			let appcode=this.props.getSearchParam("c");
			let param = this.props.getSearchParam('param');// 小友参数
			// 如果是小友打开的节点，将参数传到后台取账簿；否则取个性化中心设置的默认账簿。
			if (param) {
				this.botQueryAccbookInfo();
			} else {
				getDefaultAccountBook(appcode).then((defaultAccouontBook) => {
					if (defaultAccouontBook.value) {
						let accountingbook = {
							refname: defaultAccouontBook.display,
							refpk: defaultAccouontBook.value
						}
						this.setState({
							accountingbook
						}, () => {
							this.getInfoByBook()
						});
					}
				})
			}
		}
		
		this.props.MainSelectRef(this)
	}

	/**
	 * 小友：查询账簿信息
	 */
	botQueryAccbookInfo = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/botQueryAccbookInfo.do';
		let param = this.props.getSearchParam('param');
		ajax({
			loading: true,
			url,
			data: param,
			success: function (res) {
				let { data, success } = res;
				if (success) {
					let accountingbook = {
						refname: data.refname,
						refpk: data.refpk
					}
					self.setState({
						accountingbook
					}, () => {
						self.getInfoByBook()
					});
				}
			}
		});
	}

	updateState(source){
		let state = this.state;
		if(source){
			Object.assign(state, source);
			this.setState(state);
		}
	}
	beforChangeSearchParams = (callback) => {
		this.props.beforChangeSearchParams(callback)
	}
	//更新建账状态
	updateBuildStatus = (isInitBuild) => {
		this.setState({
			isInitBuild: isInitBuild,
			// tableDatas: tableDatas,
		})
	}
	//获取表格数据
	getTables(num,callback) {
		let self = this;
		self.setState({
			isLoading: true
		});
		let url = '/nccloud/gl/voucher/getInit.do';

		let buSecond = this.state.buSecond;
		let pk_units = []
		let unit=''
		if (this.state.isStartBUSecond) {			
			if (buSecond){
				buSecond.map((item) => {
					pk_units.push(item.refpk);
				})
			}
			if(buSecond.length===1){
				unit = buSecond[0].refname
			}
		}
		// let pk_units = v.map((item) => item.refpk);

		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,
			year: this.state.selectedYear,
			period: this.state.currPeriod,
			versionDate: this.state.versiondate,
			pk_currtype: this.state.currtype,
			pk_units,  //二级核算在获取表格中需要

		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, message, success } = res;

		        if(success){
					// 释放锁
					lockAPI.freeAllInitLock(true, ()=>{	
						let lockMap = cacheUtils.getLockMap()
						lockMap.clear()	
					})

		        	let flag = {
		        		isInitBuild: self.state.isInitBuild, //是否已建账
		        		isYearStart: self.state.isYearStart, //启用期间是否年初
		        		isGlobal: self.state.isGlobal,   //是否启用全局本币
		        		isGroup: self.state.isGroup,   //是否启用集团本币
		        		currtype: self.state.currtype,     //币种，组织本币特殊
		        		globalCurrType: self.state.globalCurrType, //全局本币 //下同，用来判断列是否可编辑用的	
		        		groupCurrType: self.state.groupCurrType,  //集团本币
		        		localCurrType: self.state.localCurrType, //组织本币
		        		isStartBUSecond: self.state.isStartBUSecond,
		        		isBUBalanceCheck: self.state.isBUBalanceCheck, //是否二级按业务单元进行平衡检查 true 校验所有业务单元试算平衡结果，false 只校验合计结果是否平衡
		        		groupAmountCtrl: self.state.groupAmountCtrl,
		        		globalAmountCtrl: self.state.globalAmountCtrl,
		        	};
		        	if(num == 2) {
		        		flag.isInitBuild = false;
		        	}
		        	let info = {
						pk_accountbook: self.state.accountingbook.refpk,
						accountingbookCode:self.state.accountingbook.refcode,
		        		pk_unit: (pk_units.length == 0 ? '' : pk_units[0]),
						year: self.state.selectedYear,
						period: self.state.currPeriod,
		        		pk_currtype: self.state.currtype,
		        		// pk_BUorg: self.state.buSecond.refpk || '',
		        		NC001: self.state.NC001,
		        		NC002: self.state.NC002,
						pk_units: pk_units,
						pk_org: self.state.pk_org,
						yearList: self.state.yearList,
						currtypeList: self.state.currtypeList,
						buSecond: self.state.buSecond
		        	};
		        	let names = {
		        		book: self.state.accountingbook.refname,
						unit: unit,
		        		yearPeriod: self.state.yearPeriod,
		        		currtypeName: self.state.currtypeName,
		        		versiondate: self.state.versiondate,
		        	}
		        	let isInitBuild = self.state.isInitBuild;
		        	if(num == 2) {
		        		isInitBuild = false;
		        	}
					// let rates = self.state.rates;
					self.tableAll = {data, flag, info, names}
		        	self.setState({
		        		isLoading: false,
		        		isInitBuild,
		        	})
					cacheUtils.setOpenData(self.tableAll)						
					self.props.onSendData(self.tableAll);
					if(callback){
						callback()
					}
		        }else {
		            toast({ content: message.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'danger' });
		        
		    }
		});

	}
	

	getCurrtypeList = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';


		let isGlobal = this.state.isGlobal;
		let isGroup = this.state.isGroup;


		let data = {
			showGlobalCurr: '0',
			showGroupCurr: '0',
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
		        	let currtypeList = data;
		        	let currtypeName;
		        	currtypeList.forEach(function (item, index) {
		        		if (item.pk_currtype == self.state.currtype ) {
		        			currtypeName = item.name;
		        		}
		        	})

					self.setState({
						currtypeList,
						currtypeName,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	getInfoByBook = (type,changeYear) => {
		if (!this.state.accountingbook || !this.state.accountingbook.refpk) {
			this.tableAll = null
			this.setState({
				// tableDatas: null,
				buSecond: [],
				yearList: [],       
				selectedYear: '',    
				currPeriod:'',
				yearPeriod: '',
				currtype: '',      //选择的币种 2
				currtypeName: '',  //选择的币种名称 2
				// currtypeList: [],  
			},()=>{
				cacheUtils.setOpenData(this.tableAll)
						
				this.props.onSendData(this.tableAll);
			})
			return;
		}
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		
		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,	
			// year: this.state.selectedYear,		
		};
		// 选用会计年时，传year， 其他情况不传
		if(type===2){
			data.year = this.state.selectedYear
		}

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;

		        if(success){
					if(data){
						let flagbtn = data.isInitBuild
						if(flagbtn){
							self.props.updateButtonStatus(true);
						}else {
							self.props.updateButtonStatus(false);
						}
					} 
					let yearPeriod = ''
		        	if (type == 2) {
						if(changeYear===data.year){
							yearPeriod = '' + data.year + getMultiLangByID('20020401-000086') + data.period + getMultiLangByID('20020401-000087')					/* 国际化处理： 年,月*/
						}else{
							yearPeriod = '' + changeYear + getMultiLangByID('20020401-000086') + getMultiLangByID('20020401-000088')/* 国际化处理： 年,01月*/
						}
		        	} else {
		        		yearPeriod = '' + data.year + getMultiLangByID('20020401-000086') + data.period + getMultiLangByID('20020401-000087')/* 国际化处理： 年,月*/
		        		self.setState({
							selectedYear: data.year,
							
		        		})
		        		self.getYears();
					}
					let currPeriod;
					/* isYearStart 该年份的期间是否为01 */
					if(!data.isYearStart){
						currPeriod = data.period;
					}else{
						currPeriod = '01';
					}
					self.setState({
						currPeriod,
						yearPeriod: yearPeriod,
					})

		        	let isGlobal = false;   //是否启用全局本币
		        	let isGroup = false;    //是否启用集团本币
		        	if (data.globalCurrType && data.globalCurrType.pk_currtype) {
		        		isGlobal = true;
		        	}

		        	if (data.groupCurrType && data.groupCurrType.pk_currtype) {
		        		isGroup = true;
		        	}
		        	let currtypeName;
		        	let currtypeList = self.state.currtypeList;
		        	currtypeList.forEach(function (item, index) {
		        		if (item.pk_currtype == data.defaultCurrtype.pk_currtype ) {
		        			currtypeName = item.name;
		        		}
		        	})
		        	let buSecond = [];
		        	if (data.defaultBUOrg) {
		        		buSecond = [{
		        			refname: data.defaultBUOrg.orgName,
		        			refpk: data.defaultBUOrg.pk_org,
		        		}];
		        	}

					let {accountingbook} = self.state;
					accountingbook.refcode = data.code;
		        	self.setState({
						accountingbook:accountingbook,
		        		isInitBuild: data.isInitBuild,
		        		isStartBUSecond: data.isStartBUSecond,
		        		isBUBalanceCheck: data.isBUBalanceCheck,
		        		currtype: data.defaultCurrtype.pk_currtype,
		        		versiondate: data.versiondate,
						isYearStart: data.isYearStart,
						currtypeName,
		        		isGlobal,
		        		isGroup,
		        		buSecond,
		        		NC001: data.NC001,
		        		NC002: data.NC002,
		        		groupAmountCtrl: data.groupAmountCtrl,
		        		globalAmountCtrl: data.globalAmountCtrl,
		        		localCurrType: data.localCurrType.pk_currtype,
						globalCurrType: (data.globalCurrType ?  data.globalCurrType.pk_currtype : null),
						pk_org: data.pk_org,			
		        		groupCurrType: (data.groupCurrType ? data.groupCurrType.pk_currtype : null),
		        	// }, self.getRates)
		        	}, () => {
						// self.getRates();
						self.getTables();
		        		// self.getCurrtypeList();
		        	} )
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	getYears() {
		let self = this;
		let url = '/nccloud/gl/voucher/yearcombo.do';
		let data = {
			pk_accountingbook: self.state.accountingbook.refpk,			
		};


		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){

		        	self.setState({
		        		yearList: data,
		        	})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		            self.setState({
		            	isLoading: false
		            });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
	}

	handleYearChange(value) {
		let callback = () => {
			this.setState({
				selectedYear: value,
			}, () => {
				this.getInfoByBook(2,value);
			})
		}
		this.beforChangeSearchParams(callback)		
		//参数2表示是切换会计年列表进行请求的，此情况不可以用返回的新启用年把selectedYear给替换掉
		
	}

	handleCurrtypeChange = (value) => {
		let self = this;
		let callback = () =>{
			if (!self.state.accountingbook || !self.state.accountingbook.refpk) {
				toast({ content: getMultiLangByID('20020401-000078'), color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
				return;
			}
			let currtypeList = self.state.currtypeList;
			currtypeList.forEach(function (item, index) {
				if (item.pk_currtype == value ) {
					self.setState({
						currtypeName: item.name,
					})
				}
			})
	
			self.setState({
				currtype: value,
			}, self.getTables);
		}
		
		self.beforChangeSearchParams(callback)
	}

	//财务核算账簿refer
	renderReferLoader() {
		let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
		let defaultValue = {refname: this.state.accountingbook.refname, refpk: this.state.accountingbook.refpk}
		return(
			<Row>
				<Col xs={12} md={12}>
					<ReferLoader
						tag = 'test'
						fieldid = 'accountingbook'
						refcode = {referUrl}
						value = {defaultValue}
						isMultiSelectedEnabled = {false}
						showGroup = {false}
						showInCludeChildren = {false}
						queryCondition = {() => {
							return {
								"TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
								"appcode": this.props.appcode
							}
						}}
						onChange = {(v)=>{
							this.beforChangeSearchParams(()=>{
								this.setState({
									accountingbook: v,
								},this.getInfoByBook)
							})
						  }}
					/>
				</Col>
			</Row>
		)
	}
	render() {
		let allButtons = this.props.button.getButtons();
		let pk_accountingbook = this.state.accountingbook.refpk;
		let buziInfo = getBusinessInfo();
		return (
			<div className="main-select">
				{/* 核算账簿 */}
				<div className="width_210 margin_r20">
					{this.renderReferLoader()}
				</div>
				{/* 二级账簿 */}
				<div className={this.state.isStartBUSecond ? "margin_r20" : "none"}>
					<div className="width_210">
				   		<BusinessUnitVersionDefaultAllTreeRef
						   	fieldid='buSecond'
							value={this.state.buSecond}
			    			isMultiSelectedEnabled={true}
							queryCondition = {() =>{
								return {
									"TreeRefActionExt": 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder',
									"isDataPowerEnable": 'Y',
									"DataPowerOperationCode" : 'fi',
									"pk_accountingbook":pk_accountingbook,
									"VersionStartDate":buziInfo.businessDate.split(' ')[0]
								};
							}}
   				   			onChange={(v)=>{
								this.beforChangeSearchParams(()=>{
									this.setState({
										buSecond: v,
									}, () => {
									 
										if(this.state.accountingbook && this.state.accountingbook.refpk) {
											this.getTables();
										}
									})
								}									)
				   			  }
				   			} 
				   		/>
					
					</div>

				</div>
				{/* 会计年 */}
				<div className=" margin_r20">
			        <Select
						// size="lg"
						fieldid='accountYear'
						showClear={false}
						placeholder={getMultiLangByID('20020401-000089')}/* 国际化处理： 请选择会计年*/
						style={{ width: 100, height: 30 }}
			           	value={this.state.selectedYear}
			           	onChange={this.handleYearChange.bind(this)}
			        >	
						<Option value='' key='' disabled={true}>{getMultiLangByID('20020401-000091')}</Option>{/* 国际化处理： 会计年*/}
			         	{this.state.yearList.map((item, index) => {
			         		return <Option value={item} key={item} >{item}</Option>
			         	} )}
	
			        </Select>
				</div>
				
				{/* 币种 */}
				<div className="margin_r20">
					<Select
						// size="lg"
						fieldid='currtype'
						showClear={false}
						placeholder={getMultiLangByID('20020401-000090')}/* 国际化处理： 请选择币种*/
						style={{ width: 100, height: 30 }}
					   	value={this.state.currtype}
					   	onChange={this.handleCurrtypeChange.bind(this)}
					>
						<Option value='' key='' disabled={true}>{getMultiLangByID('20020401-000028')}</Option>{/* 国际化处理： 币种*/}
					 	{this.state.currtypeList.map((item, index) => {
					 		return <Option value={item.pk_currtype} key={item.pk_currtype} >{item.name}</Option>
					 	} )}
					
					</Select>
	
				</div>

				<div className='yearperiod nc-theme-common-font-c' fieldid='yearperiod'>
					{getMultiLangByID('20020401-000025')}：{/* 国际化处理： 会计期间*/}
					<span className='nc-theme-title-font-c'>
						{this.state.yearPeriod}
					</span>
				</div>
	
				<Loading fullScreen showBackDrop={true} show={this.state.isLoading} />


			</div>
			)

			
				
		
	}
}

MainSelect = createPage({
	// initTemplate: initTemplate,
	// mutiLangCode: '2002'
})(MainSelect)
export default MainSelect
