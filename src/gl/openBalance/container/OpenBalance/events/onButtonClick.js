import { ajax, base, toast,cacheTools,print,withNav, promptBox, getMultiLangByID } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
import {requestApi} from '../../../requestApi';
let tableid = 'gl_brsetting';
import { autoBlur } from '../../../../public/common/findNextFocusItemInTable'
// @withNav
export default function onButtonClick(props, id) {
    autoBlur()
    switch (id) {
        case 'excel_opt'://  导入导出         
            break;
        case 'import'://导入
            this.handleImport()
            break;
        case 'export'://导出
            this.handleExport()
            break;
        case 'save'://保存
            this.saveTable() //保存后不刷新
            // this.saveTable(null,()=>{
            //     this.refresh()
            // }) //保存后刷新
            break;
        case 'cancel'://取消
            this.cancelBtn()
            break;
        case 'refresh'://刷新
            this.reFresh()
            break; 
        case 'calculate'://试算平衡
            this.goTrialBalance()
            break; 
        case 'init_build_drop'://
            break;
        case 'build'://期初建账
            this.initBuild()
            break;
        case 'cancel_build'://取消建账
            this.cancelInitBuild()
            break; 
        case 'year_start'://年初处理
            break;   
        case 're_calculate'://年初重算
            this.ReCalculate()
            break; 
        case 'adjust':// 年初调整
            this.adJust()
            break;
        case 'batch'://集中处理
            break;
        case 'batch_re_calculate'://集中重算
            this.batchReCalculate()
            break; 
        case 'batch_build'://集中建账
            this.batchBuild()
            break;  
        case 'print_group'://打印按钮组
            break;
        case 'print'://打印
            this.showPrintModal()
            break; 
        case 'out_template'://模板输出
            this.showOutputModal()
            break;  
        case 'add_line'://增行
            this.addTable()
            break; 
        case 'del_line'://删行
            this.delTable()
            break; case 'out_template'://模板输出
            this.showOutputModal()
            break;  
        case 'add_line'://增行
            this.addTable()
            break; 
        case 'del_line'://删行
            this.delTable()
            break; 
            case 'del_line'://删行
            this.delTable()
        case 'first_step'://首页
            this.goFistStep()
            break; 
        case 'pre_step'://上一步
            // this.goOpenBalance()
            this.goPreStep()
            break;  
        case 'next_step'://下一步
            this.goNextStep()
            break; 
       
        case 'final_build'://建账
            this.finalBuild()
            break;
        case 'clearData'://清空期初余额
            clearInitData(this);
            break;
        default:
        break;
    }
}

/**
 * 清除期初余额数据
 * @param {*} page 
 */
function clearInitData(page){
    let {info, flag} = page.state.tableAll;
    let data = {};
    if(flag.isInitBuild){
        toast({content:getMultiLangByID('20020401-000070'), color:'warning'});/* 国际化处理： 已经期初建账，不允许删除期初数据*/
        return;
    }
    if(flag.isStartBUSecond){
        if(!info.buSecond || info.buSecond.length > 1){
            toast({content:getMultiLangByID('20020401-000071'), color:'warning'});/* 国际化处理： 操作不支持业务单元多选！*/
            return;
        }else{
            data.pk_unit = info.buSecond[0].refpk;
        }
    }

    data.pk_accountingbook = info.pk_accountbook;
    data.year = info.year;
    promptBox({
        color:'warning',
        title:getMultiLangByID('20020401-000050'),/* 国际化处理： 提示*/
        content:getMultiLangByID('20020401-000072'),/* 国际化处理： 确实要删除本公司本业务单元本年度的所有期初数据吗？*/
        beSureBtnName: getMultiLangByID('20020401-000046'),/* 国际化处理： 确定*/
        cancelBtnName: getMultiLangByID('20020401-000047'),/* 国际化处理： 取消*/
        beSureBtnClick:()=>{
            requestApi.clearInitData(data, (res) => {
                if(res.success){
                    page.refresh();
                    toast({content:getMultiLangByID('20020401-000073')})/* 国际化处理： 清除成功*/
                }else{
                    toast({content:res.msg, color:'warning'});
                }
            });
        }
    });
}
