import React, { Component } from 'react';
import {base,deepClone,toast, getMultiLang, getMultiLangByID } from 'nc-lightapp-front';
const { NCButton: Button,NCRadio:Radio,NCIcon:Icon,NCFormControl: FormControl,NCLoading:Loading,NCModal:Modal} = base;
import './index.less';
import ReferWraper from '../../../../public/components/ReferWraper'
import AssidModal from '../../../../public/components/assidModal'

import ReferLoader from '../../../../public/ReferLoader/index.js';

export default class CentralReCountModal extends Component {

	constructor(props) {
		super(props);
		this.state={
			selectedValue: '1',
			isLoading: false,
			versionDate: '',
			buorg: '', //所属财务组织
			pk_accountingbook: '',
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			assDisplay: '',
			accountingbook: {
				refname: '',
				refpk: '',
			},
			accountingbooks: [],
			assValues:[],
			pk_accountingbooks: [],
			showAssidModal: false,
			assidCondition:'',
			json: {}
		}
	
	}

	static defaultProps = {
		show: false,
	};
	componentWillMount() {
	}
	componentDidMount() {

	}

	componentWillReceiveProps(nextProps) {
	}


	setInfo = (enddata) => {
		this.setState({
			pk_accountingbook: enddata.pk_accountingbook,
			versionDate: enddata.versionDate,
			buorg: enddata.enddata,
		})
	}

	handleRadioChange(value) {
		this.setState({selectedValue: value});
	}


	handleGTypeChange =() =>{
		
	};
	showAssidModal(){
		let {pk_accountingbook,pk_accountingbooks,accasoa,versionDate} =this.state
		let {buorg} = this.props;
		if(!accasoa.refpk){
			return 
		}
		let assidCondition = {
			pk_accountingbook: pk_accountingbook,
			pk_accasoa: accasoa.refpk,
			prepareddate: versionDate, //业务日期
			pk_org: buorg, //财务组织
			assData: [],
			assid: '',
			checkboxShow: true,
		};
		this.setState({
			showAssidModal: true,
			assidCondition: assidCondition,
			// assid: record.value
		})
	}
	//辅助核算框 点击确定
	handleAssidOK(modalData) {
		let assValues = [];
		if(modalData && modalData.data && modalData.data.length > 0){
			modalData.data.map((item) => {
				let data = {
					pk_checktype:{value:item.pk_Checktype},
					checkvaluecode:{value:item.checkvaluecode},
					checkvaluename:{value:item.checkvaluename},
					pk_checkvalue:{value:item.pk_Checkvalue}
				}
				assValues.push(data);
			});
		}

		this.setState({
			showAssidModal: false,
			assDisplay: modalData.assname,
			assValues: assValues
		});	
	}

	handleChange = (nextTargetKeys, direction, moveKeys) => {
	  this.setState({ targetKeys: nextTargetKeys });
	}

	handleScroll = (direction, e) => {

	}

	onConfirm = () => {								
		let {pk_accountingbooks,assValues,selectedValue,accasoa} = this.state
		// let pk_accountingbooks = accountingbooks.map(item => item.refpk);
		if(selectedValue === '1' && !accasoa.refpk) {
			toast({ content: getMultiLangByID('20020401-000048'), color: 'warning' });/* 国际化处理： 请选择核算的会计科目！*/
		}
		let assignAccountCode = selectedValue === '1' ? accasoa.nodeData.refcode : ''
		let data = {
			assignAccountCode: assignAccountCode,
			assVOs: assValues,
			handleInOrDecreType: selectedValue,
			pk_accountbooks: pk_accountingbooks,
		};
		let newdata = deepClone(data);
		if (newdata.handleInOrDecreType != '1') {
			newdata.assignAccountCode = '';
			newdata.assVOs = [];
		}
		this.props.onConfirm(newdata)
		this.setState({
			assDisplay: '',
			accountingbooks: [],
			assValues: [],
			accasoa: {           //科目
				refname:'',
				refpk: '',
			}
		})
		
	}
	handleCancel(){
		let {handleCancel} =this.props
		if(handleCancel){
			handleCancel()
		}
		this.setState({
			assDisplay: '',
			accountingbooks: [],
			assValues: [],
			accasoa: {           //科目
				refname:'',
				refpk: '',
			}
		})
	}
	renderReferLoader() {
		let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
		return(
			<ReferLoader
				tag = 'account'
				fieldid='account'
				refcode = {referUrl}
				value={this.state.accountingbooks}
				// value = {defaultValue}
				showInCludeChildren={true}
				isMultiSelectedEnabled = {true}
				showGroup = {false}
				queryCondition = {() => {
					return {
						"TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
						"appcode": this.props.appcode
					}
				}}
				onChange = {(v)=>{
					let pk_accountingbooks = v.map(item => item.refpk);
					this.setState({
						accountingbooks: v,
						pk_accountingbooks: pk_accountingbooks,
					})
				}}
			/>
		)
	}
	render() {
		let { show, appcode }= this.props;

		let accasoaIf;

		this.state.selectedValue == '1' ?  (accasoaIf = {}) : (accasoaIf = {display: 'none'});
		let accasoaUrl = 'uapbd/refer/fiacc/AccountDefaultGridTreeRef' + '/index.js';
		//控制选择按钮的状态
		let {accasoa} = this.state
		let dis = accasoa.refpk ? false : true

		return <Modal style={{width:'680px',height: '300px'}}
			fieldid='beginYearCount'
			show={ show } 
			backdrop='static'
			onHide= {this.handleCancel.bind(this)}
		    >
			<Modal.Header closeButton >
				<Modal.Title>{getMultiLangByID('20020401-000068')}</Modal.Title>{/* 国际化处理： 损益类科目处理方式*/}
			</Modal.Header >
			<Modal.Body>
				<div className='beginYearCount'>
					<div className="mark nc-theme-area-bgc nc-theme-common-font-c nc-theme-area-split-bc">
						<div className="mark_inco">
							<Icon type='uf-i-c-2' className="mark_inco_i nc-theme-area-bgc"/>
						</div>
						
						{getMultiLangByID('20020401-000050')}：{getMultiLangByID('20020401-000060')}，{getMultiLangByID('20020401-000061')}，{getMultiLangByID('20020401-000054')}？
						{/* 国际化处理： 提示,年初重算将采用上年余额重新生成本年期初余额,所有本年已录入期初将被删除,继续吗*/}
					</div>
					<div className='cnetral_style' fieldid='carryover'>
						<div className='lable_name nc-theme-common-font-c'>{getMultiLangByID('20020401-000055')}：</div>{/* 国际化处理： 结转方式*/}
						<div className='con_radio'>
							<Radio.NCRadioGroup
								name="fruit"
								selectedValue={this.state.selectedValue}							
								onChange={this.handleRadioChange.bind(this)}
							>						
								<Radio value="1" >	
									{getMultiLangByID('20020401-000056')}	{/* 国际化处理： 结转到指定科目*/}
								</Radio>
								<Radio value="2" >{getMultiLangByID('20020401-000057')}</Radio>{/* 国际化处理： 不结转*/}
								<Radio value="3" >{getMultiLangByID('20020401-000069')}</Radio>{/* 国际化处理： 对应结转*/}

							</Radio.NCRadioGroup>
						</div>
						<div className='con_ref' style={accasoaIf}>
							<div className='account_ref_w200'>
								<ReferLoader
									tag = 'accasoa'
									fieldid='accasoa'
									refcode = {accasoaUrl}
									showStar={true}
									value={this.state.accasoa}
									isMultiSelectedEnabled = {false}
									showGroup = {false}
									onlyLeafCanSelect={true}  //是否只有叶子节点可选
									showInCludeChildren = {false}
									queryCondition = {() => {
										return {
											"versiondate": this.state.versionDate,
											"pk_accountingbook": this.state.pk_accountingbook,
											"isDataPowerEnable": 'Y',
											"DataPowerOperationCode" : 'fi',
											"TreeRefActionExt":"nccloud.web.gl.ref.FilterInOrDecreaseAccSqlBuilder",
											appcode:appcode
										}
									}}
									onChange = {(v)=>{
										this.setState({
											accasoa: v,
											assDisplay: '',
										})

									}}
								/>
							</div>
			
							<ReferWraper
								outStyle={{width: '200px', marginLeft: '10px'}}
								placeholder = {getMultiLangByID('20020401-000045')} /* 国际化处理： 请选择辅助核算*/
								display={this.state.assDisplay}
								disabled={dis}
								onClick={this.showAssidModal.bind(this)}
							/>

						</div>
					</div>
					<div className='account'>
						<div className='lable_name nc-theme-common-font-c'>{getMultiLangByID('20020401-000023')}：</div>{/* 国际化处理： 财务核算账簿*/}
						<div className='account_ref_w210'>
							{this.renderReferLoader()}
						</div>
					</div>				
				</div>

			</Modal.Body>
			<Modal.Footer>
				<Button colors='primary' fieldid='confirm' onClick={this.onConfirm.bind(this)}>{getMultiLangByID('20020401-000046')}</Button>{/* 国际化处理： 确定*/}
        <Button fieldid='cancel' onClick={this.handleCancel.bind(this)}>{getMultiLangByID('20020401-000047')}</Button>{/* 国际化处理： 取消*/}
			</Modal.Footer>		
			<Loading fullScreen showBackDrop={true} show={this.state.isLoading} />
			<AssidModal
				pretentAssData={this.state.assidCondition}
				showOrHide = {this.state.showAssidModal}
				onConfirm={this.handleAssidOK.bind(this)} 
				handleClose={() => {
					this.setState({ 
						showAssidModal: false
					 });
				}}
			/>

		</Modal>;
	}
}
