
import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
import { base, ajax, deepClone, createPage, promptBox, toast, getMultiLang, initMultiLangByModule, getMultiLangByID } from 'nc-lightapp-front';
const { NCButton: Button, NCNumber, NCTable: Table, NCTooltip: Tooltip, NCDiv } = base;
import './index.less';

import { accAdd, Subtr } from '../../../../verify/verify/vrify/events/method.js';
import { setParent, getDiffValue, valueChanged, commafy, removeThousands, initNumber } from './updateParentUtil.js'
import cacheUtils from '../../../utils/cacheUtils'
import lockAPI from '../../../utils/lockAPI'
import { onEnter, onArrowUpDown } from '../../../../public/common/findNextFocusItemInTable'
import ReferWraper from '../../../../public/components/ReferWraper'
import AccountAssistant from '../../AccountAssistant';
import { getTableHeight } from '../../../../public/common/method.js';
const emptyCell = <span>&nbsp;</span>
class OpenTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			flag: {},
			mydata: [],
			info: null,
			// olddata: [], //每次刚查询出的原始数据
			names: null,
			changesList: [], //只要触发了onInputChange就将index放入该数组，不考虑是否真正变动
			// rates: {},   //汇率接口数据 
			endamount: '', //期初余额 原币
			endlocalamount: '', // 组织本币
			endgroupamount: '', // 集团本币
			endglobalamount: '', // 全局本币
			// 借方累计
			debitamount: '', // 借方累计 原币
			localdebitamount: '', // 组织本币
			debitgroupamount: '', // 集团本币
			debitglobalamount: '', // 全局本币
			// 贷方累计
			creditamount: '', // 贷方累计 原币
			localcreditamount: '', // 组织本币
			creditgroupamount: '', // 集团本币
			creditglobalamount: '', // 全局本币

			yearamount: '', // 年初余额 原币
			yearlocalamount: '', // 组织本币
			yeargroupamount: '', // 集团本币
			yearglobalamount: '', // 全局本币
			json: {},
			beforeChangesData: []
			// assisData: {}
		}
		this.ViewModel = this.props.ViewModel;
		this.tableId = 'openBalance'
	}
	componentWillMount() {
		// let callback= (json) =>{
		// 	this.setState({
		// 		json:json,
		// 	})
		// }
		// getMultiLang({moduleId:'20020401',domainName:'gl',currentLocale:'simpchn',callback});
	}
	getChangeList() {
		let { changesList } = this.state
		return changesList
	}
	resetChangeList() {
		this.setState({
			changesList: []
		})
	}
	getOldData = () => {
		return this.state.beforeChangesData
	}
	//取消修改，恢复原始数据
	getBeforeChangesData = () => {
		let { beforeChangesData, mydata } = this.state
		if (beforeChangesData.length > 0) {
			beforeChangesData.map((item) => {
				let _index = item.xuhao - 1
				let tempRowData = mydata[_index]
				let diff = getDiffValue(item, tempRowData);
				valueChanged(tempRowData, diff);
			})
		}
		// this.resetChangeData()
		return mydata
	}
	resetChangeData = () => {
		this.setState({
			beforeChangesData: []
		})
	}

	saveTable = (index, callback) => {
		let self = this;
		if (self.state.flag.isInitBuild) {
			toast({ content: getMultiLangByID('20020401-000003'), color: 'warning' });/* 国际化处理： 已建账，无须保存*/
		}
		let mydata = self.state.mydata;

		let info = self.state.info;

		let url = '/nccloud/gl/voucher/saveInit.do';
		let newInitVOs = [];

		let { changesList } = self.state
		let list = [...new Set(changesList)]
		newInitVOs = mydata.filter(function (item, index) {
			return list.indexOf(index) > -1;
		})
		if (newInitVOs.length == 0) {
			toast({ content: getMultiLangByID('20020401-000005'), color: 'warning' });/* 国际化处理： 未做修改，无需保存*/
			return
		}


		let data = {
			pk_accountbook: info.pk_accountbook,
			pk_unit: info.pk_unit,
			year: info.year,
			pk_currtype: info.pk_currtype,
			newInitVOs,

		};
		ajax({
			loading: true,
			url,
			data,
			success: function (res) {
				const { data, message, success } = res;
				if (success) {

					if (index == 3) {
						self.props.onRefresh();
					} else {
						toast({ content: getMultiLangByID('20020401-000006'), color: 'success' });/* 国际化处理： 保存成功*/
						// self.props.onRefresh();
						self.setState({
							changesList: [],
							beforeChangesData: []
						})
						// 保存成功后清除缓存锁
						let lockMap = cacheUtils.getLockMap()
						lockMap.clear()
						self.updateTableData(mydata) // 更新table数据
						if (callback) {
							callback()
						}
					}

				} else {
					toast({ content: message.message, color: 'warning' });
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });

			}
		});
	}

	// 期初余额
	// 原币 endamount
	// 组织本币 endlocalamount
	// 集团本币 endgroupamount
	// 全局本币 endglobalamount

	// 年初余额
	// 原币 yearamount
	// 组织本币 yearlocalamount
	// 集团本币 yeargroupamount
	// 全局本币 yearglobalamount

	// 借方累计
	// 原币  debitamount
	// 组织本币  localdebitamount
	// 集团本币 debitgroupamount
	// 全局本币 debitglobalamount

	// 贷方累计
	// 原币  creditamount
	// 组织本币  localcreditamount
	// 集团本币 creditgroupamount
	// 全局本币 creditglobalamount

	//direction:方向,0:借;debit:借方; credit:贷方; year:年初; cycle:期初
	calcYearBalances(direction, cycle, debit, credit, scale) {
		let _scale = scale ? Number(scale) : 2
		let add, sub, year
		// let _cycle = Number(cycle)
		// let _debit = Number(debit)
		// let _credit = Number(credit)
		if (direction === '0') {
			// 借：年初余额 = 期初余额 - 借方累计 + 贷方累计
			add = accAdd(cycle, credit);
			sub = Subtr(add, debit);
			year = sub
		} else {
			// 贷：年初余额 = 期初余额 + 借方累计 - 贷方累计		
			add = accAdd(cycle, debit);
			sub = Subtr(add, credit);
			year = sub
		}
		let _year = initNumber(year, _scale)
		//  // 如果数据有小数点尝试四舍五入
		//  if (String(year).includes(".")) {
		// 	_year = ncRounding(year, _scale);
		//   } else {
		// 	// 如果没有小数点就尝试进行补零
		// 	_year = addZero(year, _scale);
		//   }
		return _year
	}
	updateTableData = (mydata) => {
		let tableAll = cacheUtils.getOpenData()
		tableAll.data = mydata
		cacheUtils.setOpenData(tableAll)
		this.props.updateTableData(tableAll) // 更新table数据
	}

	//数据联动
	linkData = (index, key, value, data, oldRow, rowData, info, changesList, beforeChangesData) => {
		let { flag } = this.state
		// 方向: balanorient
		let direction = data[index].balanorient.value
		// let rowData = data[index]
		// 原币
		let endamount = rowData.endamount.value
		let debitamount = rowData.debitamount.value
		let creditamount = rowData.creditamount.value
		let amountScale = rowData.endamount.scale
		let initAmountVal = initNumber(value, amountScale)
		// 组织本币
		let endlocalamount = rowData.endlocalamount.value
		let localdebitamount = rowData.localdebitamount.value
		let localcreditamount = rowData.localcreditamount.value
		let localamountScale = rowData.endlocalamount.scale
		let initLocalamountVal = initNumber(value, localamountScale)
		// 集团本币
		let endgroupamount = rowData.endgroupamount.value
		let debitgroupamount = rowData.debitgroupamount.value
		let creditgroupamount = rowData.creditgroupamount.value
		let groupamountScale = rowData.endgroupamount.scale
		let initGroupamountVal = initNumber(value, groupamountScale)
		// 全局本币
		let endglobalamount = rowData.endglobalamount.value
		let debitglobalamount = rowData.debitglobalamount.value
		let creditglobalamount = rowData.creditglobalamount.value
		let globalamountScale = rowData.endglobalamount.scale
		let initGlobalamountVal = initNumber(value, globalamountScale)
		// 数量
		let endquantity = rowData.endquantity.value
		let debitquantity = rowData.debitquantity.value
		let creditquantity = rowData.creditquantity.value
		// let quantityScale = rowData.endquantity.scale
		let tempVal
		let scale = data[index].yearamount.scale
		let localScale = data[index].yearlocalamount.scale
		let groupScale = data[index].yeargroupamount.scale
		let globalScale = data[index].yearglobalamount.scale
		let qtScale = data[index].yearquantity.scale

		// let currentScale = data[index][key].scale
		// let initCurrentVal = initNumber(value, currentScale)
		data[index][key].display = value;
		data[index][key].value = value;
		//期初余额原币变
		if (key == "endamount") {
			tempVal = this.calcYearBalances(direction, value, debitamount, creditamount, scale)
			// 原币
			data[index].yearamount.display = tempVal;
			data[index].yearamount.value = tempVal;

			if (flag.currtype == flag.localCurrType) {
				// 组织本币
				data[index].endlocalamount.display = initLocalamountVal;
				data[index].endlocalamount.value = value;
				data[index].yearlocalamount.display = tempVal;
				data[index].yearlocalamount.value = tempVal;
			}

			if (flag.currtype == flag.groupCurrType && info.NC001 == 'raw_convert') {
				// 集团本币
				data[index].endgroupamount.display = initGroupamountVal;
				data[index].endgroupamount.value = value;
				data[index].yeargroupamount.display = tempVal;
				data[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && info.NC002 == 'raw_convert') {
				// 全局本币
				data[index].endglobalamount.display = initGlobalamountVal;
				data[index].endglobalamount.value = value;
				data[index].yearglobalamount.display = tempVal;
				data[index].yearglobalamount.value = tempVal;
			}

			if (flag.currtype == flag.groupCurrType && flag.currtype == flag.localCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data[index].endgroupamount.display = initGroupamountVal;
				data[index].endgroupamount.value = value;
				data[index].yeargroupamount.display = tempVal;
				data[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && flag.currtype == flag.localCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data[index].endglobalamount.display = initGlobalamountVal;
				data[index].endglobalamount.value = value;
				data[index].yearglobalamount.display = tempVal;
				data[index].yearglobalamount.value = tempVal;
			}

		}

		//借方原币变
		if (key == 'debitamount') {
			tempVal = this.calcYearBalances(direction, endamount, value, creditamount, scale)
			// 原币
			data[index].yearamount.display = tempVal;
			data[index].yearamount.value = tempVal;

			if (flag.currtype == flag.localCurrType) {
				// 组织本币
				data[index].localdebitamount.display = initLocalamountVal;
				data[index].localdebitamount.value = value;
				data[index].yearlocalamount.display = tempVal;
				data[index].yearlocalamount.value = tempVal;
			}

			if (flag.currtype == flag.groupCurrType && info.NC001 == 'raw_convert') {
				// 集团本币
				data[index].debitgroupamount.display = initGroupamountVal;
				data[index].debitgroupamount.value = value;
				data[index].yeargroupamount.display = tempVal;
				data[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && info.NC002 == 'raw_convert') {
				// 全局本币
				data[index].debitglobalamount.display = initGlobalamountVal;
				data[index].debitglobalamount.value = value;
				data[index].yearglobalamount.display = tempVal;
				data[index].yearglobalamount.value = tempVal;
			}

			if (flag.currtype == flag.groupCurrType && flag.currtype == flag.localCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data[index].debitgroupamount.display = initGroupamountVal;
				data[index].debitgroupamount.value = value;
				data[index].yeargroupamount.display = tempVal;
				data[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && flag.currtype == flag.localCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data[index].debitglobalamount.display = initGlobalamountVal;
				data[index].debitglobalamount.value = value;
				data[index].yearglobalamount.display = tempVal;
				data[index].yearglobalamount.value = tempVal;
			}

		}

		//贷方原币变
		if (key == 'creditamount') {
			tempVal = this.calcYearBalances(direction, endamount, debitamount, value, scale)
			// 原币
			data[index].yearamount.display = tempVal;
			data[index].yearamount.value = tempVal;

			if (flag.currtype == flag.localCurrType) {
				// 组织本币
				data[index].localcreditamount.display = initLocalamountVal;
				data[index].localcreditamount.value = value;
				data[index].yearlocalamount.display = tempVal;
				data[index].yearlocalamount.value = tempVal;
			}

			if (flag.currtype == flag.groupCurrType && info.NC001 == 'raw_convert') {
				// 集团本币
				data[index].creditgroupamount.display = initGroupamountVal;
				data[index].creditgroupamount.value = value;
				data[index].yeargroupamount.display = tempVal;
				data[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && info.NC002 == 'raw_convert') {
				// 全局本币
				data[index].creditglobalamount.display = initGlobalamountVal;
				data[index].creditglobalamount.value = value;
				data[index].yearglobalamount.display = tempVal;
				data[index].yearglobalamount.value = tempVal;
			}


			if (flag.currtype == flag.groupCurrType && flag.currtype == flag.localCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data[index].creditgroupamount.display = initGroupamountVal;
				data[index].creditgroupamount.value = value;
				data[index].yeargroupamount.display = tempVal;
				data[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && flag.currtype == flag.localCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data[index].creditglobalamount.display = initGlobalamountVal;
				data[index].creditglobalamount.value = value;
				data[index].yearglobalamount.display = tempVal;
				data[index].yearglobalamount.value = tempVal;
			}

		}


		// 期初组织本币变
		if (key == "endlocalamount") {
			tempVal = this.calcYearBalances(direction, value, localdebitamount, localcreditamount, localScale)

			// // 组织本币
			data[index].yearlocalamount.display = tempVal;
			data[index].yearlocalamount.value = tempVal;

			if (flag.localCurrType == flag.groupCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data[index].endgroupamount.display = initGroupamountVal;
				data[index].endgroupamount.value = value;
				data[index].yeargroupamount.display = tempVal;
				data[index].yeargroupamount.value = tempVal;
			}

			if (flag.localCurrType == flag.globalCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data[index].endglobalamount.display = initGlobalamountVal;
				data[index].endglobalamount.value = value;
				data[index].yearglobalamount.display = tempVal;
				data[index].yearglobalamount.value = tempVal;
			}

		}


		//借方方组织本币变
		if (key == 'localdebitamount') {
			tempVal = this.calcYearBalances(direction, endlocalamount, value, localcreditamount, localScale)
			// // 组织本币
			data[index].yearlocalamount.display = tempVal;
			data[index].yearlocalamount.value = tempVal;

			if (flag.localCurrType == flag.groupCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data[index].debitgroupamount.display = initGroupamountVal;
				data[index].debitgroupamount.value = value;
				data[index].yeargroupamount.display = tempVal;
				data[index].yeargroupamount.value = tempVal;
			}

			if (flag.localCurrType == flag.globalCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data[index].debitglobalamount.display = initGlobalamountVal;
				data[index].debitglobalamount.value = value;
				data[index].yearglobalamount.display = tempVal;
				data[index].yearglobalamount.value = tempVal;
			}

		}

		//贷方组织本币变
		if (key == 'localcreditamount') {
			tempVal = this.calcYearBalances(direction, endlocalamount, localdebitamount, value, localScale)
			// // 组织本币
			data[index].yearlocalamount.display = tempVal;
			data[index].yearlocalamount.value = tempVal;

			if (flag.localCurrType == flag.groupCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data[index].creditgroupamount.display = initGroupamountVal;
				data[index].creditgroupamount.value = value;
				data[index].yeargroupamount.display = tempVal;
				data[index].yeargroupamount.value = tempVal;
			}

			if (flag.localCurrType == flag.globalCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data[index].creditglobalamount.display = initGlobalamountVal;
				data[index].creditglobalamount.value = value;
				data[index].yearglobalamount.display = tempVal;
				data[index].yearglobalamount.value = tempVal;
			}
		}

		// 期初集团本币
		if (key == 'endgroupamount') {
			tempVal = this.calcYearBalances(direction, value, debitgroupamount, creditgroupamount, groupScale)
			data[index].yeargroupamount.display = tempVal;
			data[index].yeargroupamount.value = tempVal;
		}
		// 借方集团本币
		if (key == 'debitgroupamount') {
			tempVal = this.calcYearBalances(direction, endgroupamount, value, creditgroupamount, groupScale)
			data[index].yeargroupamount.display = tempVal;
			data[index].yeargroupamount.value = tempVal;
		}
		// 贷方集团本币
		if (key == 'creditgroupamount') {
			tempVal = this.calcYearBalances(direction, endgroupamount, debitgroupamount, value, groupScale)
			data[index].yeargroupamount.display = tempVal;
			data[index].yeargroupamount.value = tempVal;
		}

		// 期初全局本币
		if (key == 'endglobalamount') {
			tempVal = this.calcYearBalances(direction, value, debitglobalamount, creditglobalamount, globalScale)
			data[index].yearglobalamount.display = tempVal;
			data[index].yearglobalamount.value = tempVal;
		}
		// 借方全局本币
		if (key == 'debitglobalamount') {
			tempVal = this.calcYearBalances(direction, endgroupamount, value, creditgroupamount, globalScale)
			data[index].yearglobalamount.display = tempVal;
			data[index].yearglobalamount.value = tempVal;
		}
		// 贷方全局本币
		if (key == 'creditglobalamount') {
			tempVal = this.calcYearBalances(direction, endgroupamount, debitgroupamount, value, globalScale)
			data[index].yearglobalamount.display = tempVal;
			data[index].yearglobalamount.value = tempVal;
		}

		// 期初数量
		if (key == 'endquantity') {
			tempVal = this.calcYearBalances(direction, value, debitquantity, creditquantity, qtScale)
			data[index].yearquantity.display = tempVal;
			data[index].yearquantity.value = tempVal;
		}
		// 借方数量
		if (key == 'debitquantity') {
			tempVal = this.calcYearBalances(direction, endquantity, value, creditquantity, qtScale)
			data[index].yearquantity.display = tempVal;
			data[index].yearquantity.value = tempVal;
		}
		// 贷方数量
		if (key == 'creditquantity') {
			tempVal = this.calcYearBalances(direction, endquantity, debitquantity, value, qtScale)
			data[index].yearquantity.display = tempVal;
			data[index].yearquantity.value = tempVal;
		}

		/**
		 * 若当前行有上级则更新其上级数据
		 */
		if (data[index].parent) {
			let diff = getDiffValue(data[index], oldRow);
			valueChanged(data[index].parent, diff);
		}
		this.setState({ mydata: data, changesList, beforeChangesData })
	}
	onBlur = (index, key) => {
		return value => {
			//只要触发了onBlur就将index放入数组
			let { info, changesList, beforeChangesData } = this.state
			changesList.push(index);
			let data = this.state.mydata;
			let rowData = data[index]
			let oldRow = JSON.parse(JSON.stringify(rowData));
			let { json } = this.state
			let pk_accasoa = data[index].pk_accasoa.value

			let lock = cacheUtils.getLockMap().get(pk_accasoa)
			let sendData = {
				"pk_accountingbook": info.pk_accountbook,
				"pk_unit": info.pk_unit ? info.pk_unit : info.pk_org,
				"year": info.year,
				"pk_currtype": info.pk_currtype,
				"pk_accasoa": pk_accasoa
			}

			if (!lock) {
				beforeChangesData.push(oldRow)
				if (!lockAPI.addAccountLock(sendData)) {
					data[index] = oldRow
					this.setState({
						mydata: data,
					})
				} else {
					this.linkData(index, key, value, data, oldRow, rowData, info, changesList, beforeChangesData)
				}
			} else {
				this.linkData(index, key, value, data, oldRow, rowData, info, changesList, beforeChangesData)
			}
		};
	};

	beforGoAccount(index, allData) {
		// let allData = cacheUtils.getOpenData()
		let info = this.setInfo(index);
		cacheUtils.setOpenData(allData)
		cacheUtils.setAssisData(info)

		// this.props.pushTo('/accountAssistant', {})
		this.props.modal.show('AccountAssistantModal');
	}
	goAccountAssistant = (index) => {
		this.props.goAccountAssistant(index)
	}

	setInfo = (index) => {
		let book = this.state.info.pk_accountbook;
		let unit = this.state.info.pk_unit;
		let year = this.state.info.year;
		let currtype = this.state.info.pk_currtype;
		let buorg = this.state.info.pk_org;
		let pk_org = this.state.info.pk_org;//财务组织pk
		let account = this.state.mydata[index].accountName.value;  //科目主键
		// let rates = this.state.rates;
		let NC001 = this.state.info.NC001;
		let NC002 = this.state.info.NC002;
		let kemucode = this.state.mydata[index].accountCode.display;  //会计科目编码
		let kemuname = this.state.mydata[index].accountName.display;    //会计科目名称
		let fangxiang = this.state.mydata[index].balanorient.display;  //方向
		let accAssItems = this.state.mydata[index].accAssItems;   //辅助信息

		let newInitVOs = this.state.mydata[index];  //整行信息
		let buSecond = this.state.info.buSecond

		let namesAll = {
			book: this.state.names.book,    //核算账簿名称
			unit: this.state.names.unit,    //业务单元名称
			yearPeriod: this.state.names.yearPeriod,    //会计期间名称
			currtypeName: this.state.names.currtypeName,  //币种名称
			versiondate: this.state.names.versiondate, //版本日期
			kemucode,
			kemuname,
			fangxiang
		};
		//本地存储给辅助核算页使用


		let info = {
			key: {
				book,
				unit,
				year,
				currtype,
				account,
				buorg,
				NC001,
				NC002,
				pk_org,
				buSecond
			},
			namesAll,
			flag: this.state.flag,
			accAssItems,
			newInitVOs,
			// lineNum: index
			// rates,

		}
		return info;
	}
	componentWillReceiveProps(nextProps) {
		// let tableAll = cacheUtils.getOpenData()
		let tableAll = nextProps.tableAll || {
			flag: {
				isInitBuild: false, //是否已建账
				isYearStart: true, //启用期间是否年初
				isGlobal: false,   //是否启用全局本币
				isGroup: false,   //是否启用集团本币
				currtype: '',     //币种，组织本币特殊
				globalCurrType: null, //全局本币		//下同，用来判断列是否可编辑用的	
				groupCurrType: null,  //集团本币
				localCurrType: null, //组织本币
			},
			data: [],
			info: null,
			names: null,
			// rates: {},
		}

		let flag = tableAll.flag;
		let mydata = tableAll.data ? tableAll.data : [];
		if (mydata.length > 0) {
			mydata.forEach(function (item, index) {
				item.key = item.accountCode.value;
				item.xuhao = index + 1;
			})
		}
		if (nextProps.tableAll && nextProps.tableAll.info !== this.state.info) {
			setParent(mydata); /* 设置父子级关联关系 */
		}


		this.setState({
			flag: flag,
			mydata: mydata,
			info: tableAll.info,
			names: tableAll.names,
			// olddata: mydata,
			// rates: rates,
		})
	}
	componentDidMount() {
		let allData = cacheUtils.getOpenData()
		let { mydata } = this.state
		if (allData) {
			mydata = allData.data
			this.setState({
				mydata
			})
		}

		this.props.OpenTableRef(this)
	}
	// 辅助核算页面弹框
	modalContent = () => {
		return (
			<div className="modalContent">
				<AccountAssistant
					handleCloseModal={this.handleCloseModal}
					AccountAssistantRef={(init) => { this.AccountAssistant = init }}
				/>
			</div>
		)
	}
	//弹框'确定'按钮事件
	beSureBtnClick = () => {
		this.AccountAssistant.goBack()
	}
	//弹框'取消'按钮事件
	cancelBtnClick = () => {
		this.AccountAssistant.goBack()
	}
	//辅助核算 弹框'x'事件
	closeModalEve = () => {
		this.AccountAssistant.goBack()
	}
	handleCloseModal = () => {
		this.props.modal.close('AccountAssistantModal');
	}
	renderNCNumber(text, attrcode, index, placeholder, isEdit = false, ) {
		let txt = `${getMultiLangByID('20020401-000022')}${placeholder}`/* 国际化处理： 请输入*/
		if (isEdit) {
			return (
				<div fieldid={attrcode}
					id={`hot-key-${this.tableId}-${attrcode}-${index}`}
				>
					<NCNumber
						// renderInTable={true} //解决在table中悬浮到其他行自动失去焦点的问题
						fieldid={attrcode}
						scale={Number(text.scale)}
						placeholder={txt}
						value={text.display || ''}
						onBlur={this.onBlur(index, attrcode)}
						onKeyDown={(e)=>{
							if(e.keyCode=='38'|| e.key === "ArrowUp"||e.keyCode=='40'|| e.key === "ArrowDown"){// 40 ArrowDown 38 ArrowUp
								onArrowUpDown(e, `hot-key-${this.tableId}-${attrcode}-${index}`, this.tableId)
							}
						}}
						onEnter = {(e)=> {
							onEnter(e.e.currentTarget, this.ViewModel, this.tableId)
						}}
						tabindex="0" 
					/>
				</div>
			)
		} else {
			return (
				<div className='number-text' fieldid={attrcode}>
					{text.display ? commafy(removeThousands(text.display)) : emptyCell}
				</div>
				// <NCNumber
				//  disabled = {true}
				//  scale={Number(text.scale)}
				//  value={text.display  || ''}
				// />
			)
		}
	}

	render() {
		let { flag, info } = this.state
		let isStartBUSecond = flag
		// let { buSecond } = info
		let buSecond = info && info.buSecond ? info.buSecond : []
		// 1.未启用二级账簿可以编辑；2.启用二级账簿且只选一个二级账簿时，可以编辑
		let isEditNum = isStartBUSecond ? (isStartBUSecond && buSecond.length === 1 ? true : false) : true
		const { modal } = this.props;
		let { createModal } = modal;
		let modalHeight = getTableHeight(140)
		let columns1 = [
			// { title: "color", dataIndex: "endflag", key: "endflag", width: '10px',  },
			{
				title: (<div fieldid='xuhao' className='mergecells'>{getMultiLangByID('20020401-000013')}</div>),
				dataIndex: "xuhao", key: "xuhao", width: '80px', fixed: "left", /* 国际化处理： 序号*/
				render: (text, record, index) => (
					<div className='text_c' fieldid='xuhao'>{text ? text : emptyCell}</div>
				)
			},

			{
				title: (<div fieldid='accountCode' className='mergecells'>{getMultiLangByID('20020401-000093')}</div>),
				dataIndex: "accountCode", key: "accountCode", width: '100px', fixed: "left",/* 国际化处理： 科目编码*/
				render: (text, record, index) => (
					<div fieldid='accountCode'>{text.display ? text.display : emptyCell}</div>
				)
			},

			{
				title: (<div fieldid='accountName' className='mergecells'>{getMultiLangByID('20020401-000094')}</div>),
				dataIndex: "accountName", key: "accountName", width: '160px', fixed: "left",/* 国际化处理： 科目名称*/
				render: (text, record, index) => {
					let tip = (<div>{text.display}</div>)
					return (
						<Tooltip trigger="hover" placement="top" inverse={true} overlay={tip}>
							<div className='assid-btn' fieldid='accountName'>{text.display ? text.display : emptyCell}</div>
						</Tooltip>
					)
				}
			},
			{
				title: (<div fieldid='balanorient' className='mergecells'>{getMultiLangByID('20020401-000027')}</div>),
				dataIndex: "balanorient", key: "balanorient", width: '80px', fixed: "left",/* 国际化处理： 方向*/
				render: (text, record, index) => (
					<div fieldid='balanorient'>{text.display ? text.display : emptyCell}</div>
				)
			},
			{
				title: (<div fieldid='accAssItems' className='mergecells'>{getMultiLangByID('20020401-000014')}</div>),
				dataIndex: "accAssItems", key: "accAssItems", width: '160px', fixed: "left",/* 国际化处理： 辅助核算*/
				render: (text, record, index) => {
					if (text && text.length > 0) {
						let names = '';
						for (var i = 0; i < text.length; i++) {
							names += text[i].name + ' ';
						}
						let tip = (<div>{names}</div>)
						if (record.endflag && record.endflag.value == 'Y') {
							return (		
								<Tooltip trigger="hover" placement="top" inverse={true} overlay={tip}>
									<div fieldid='accAssItems'>
										<a
											className='assid-btn'
											// fieldid='accAssItems'
											// disabled={!(record.endflag && record.endflag.value == 'Y')} 
											onClick={this.goAccountAssistant.bind(this, index)}
										// shape="border" colors="info"
										>
											{names}
										</a>
									</div>
								</Tooltip>
							)
						} else {
							return (
								<Tooltip trigger="hover" placement="top" inverse={true} overlay={tip}>
									<div className='assid-btn' fieldid='accAssItems'>
										{names}
									</div>
								</Tooltip>
							)
						}


					} else {
						return (
							<div fieldid='accAssItems'>{emptyCell}</div>
						)
					}
				}

			},
			{
				title: (<div fieldid='unit' className='mergecells'>{getMultiLangByID('20020401-000095')}</div>),
				dataIndex: "unit", key: "unit", width: '100px', /* 国际化处理： 计量单位*/
				render: (text, record, index) => (

					<div fieldid='accAssItems'>{text.display ? text.display : emptyCell}</div>
				)
			},

		];
		let columns2;



		if (flag.isYearStart) {
			//1.全局、集团、组织本币
			//2.全局、集团、非组织本币
			//3.全局、非集团、组织本币
			//4.全局、非集团、非组织本币
			//5.非全局、集团、组织本币
			//6.非全局、集团、非组织本币
			//7.非全局、非集团、组织本币
			//8.非全局、非集团、非组织本币

			// 数量对象
			let shuliangObj = {
				title: (<div fieldid='endquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "endquantity",
				key: "endquantity",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endquantity")
				)

			};

			//数量//数量对象可编辑
			let shuliangObjEdit = {
				title: (<div fieldid='endquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "endquantity",
				key: "endquantity",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						if (record.unit && record.unit.value) {

							return (
								this.renderNCNumber(text, "endquantity", index, getMultiLangByID('20020401-000016'), true)/* 国际化处理： 数量*/

							)
						} else {
							return (
								this.renderNCNumber(text, "endquantity")
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "endquantity")
						)
					}
				}
			}

			// 原币对象
			let yuanbiObj = {
				title: (<div fieldid='endamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "endamount",
				key: "endamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endamount")

				)
			};

			// 原币对象可编辑
			let yuanbiObjEdit = {
				title: (<div fieldid='endamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "endamount",
				key: "endamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					//带辅助核算的到辅助核算页录入
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						return (
							this.renderNCNumber(text, "endamount", index, getMultiLangByID('20020401-000017'), true)/* 国际化处理： 原币*/

						)
					} else {
						return (
							this.renderNCNumber(text, "endamount")
						)

					}

				}
			};

			// 组织本币对象
			let zuzhiObj = {
				title: (<div fieldid='endlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "endlocalamount",
				key: "endlocalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endlocalamount")
				)
			};

			// 组织本币对象可编辑
			let zuzhiObjEdit = {
				title: (<div fieldid='endlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "endlocalamount",
				key: "endlocalamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						// 原币与组织本币币种相同，则不可编辑
						if (flag.currtype == flag.localCurrType) {
							return (
								this.renderNCNumber(text, "endlocalamount")
							)
						} else {

							return (
								this.renderNCNumber(text, "endlocalamount", index, getMultiLangByID('20020401-000000'), true)/* 国际化处理： 组织本币*/
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "endlocalamount")

						)
					}

				}
			};

			//全局本币对象
			let quanjuObj = {
				title: (<div fieldid='endglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "endglobalamount",
				key: "endglobalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endglobalamount")
				)
			}

			//判断全局本币是否可编辑
			//全局本币对象可编辑
			let quanjuObjEdit = {
				title: (<div fieldid='endglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "endglobalamount",
				key: "endglobalamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						//原逻辑：全局本币与原币或组织本币其中一个相同，则不可编辑，与哪个相同，就在录入哪个字段时自动带出。现更改。
						// if (flag.currtype == flag.globalCurrType ||  flag.localCurrType == flag.globalCurrType) {
						// 	return <span>{text.display  || ''}</span>
						// } else {

						// 	return <NCNumber
						// 		scale={Number(text.scale)}
						// 		placeholder="请输入全局本币"
						// 		value={text.display  || ''}
						// 		onBlur={this.onBlur(index, "endglobalamount")}
						// 	/>
						// }


						if (info.NC002 == 'raw_convert') {
							if (flag.currtype == flag.globalCurrType) {
								return (
									this.renderNCNumber(text, "endglobalamount")

								)
							} else {
								return (
									this.renderNCNumber(text, "endglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/

								)
							}
						}

						if (info.NC002 == 'local_convert') {
							if (flag.localCurrType == flag.globalCurrType) {
								return (
									this.renderNCNumber(text, "endglobalamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "endglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
								)
							}
						}





					} else {
						return (
							this.renderNCNumber(text, "endglobalamount")
						)
					}

				}
			}

			//集团本币对象
			let jituanObj = {
				title: (<div fieldid='endgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "endgroupamount",
				key: "endgroupamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endgroupamount")
				)
			}

			//集团本币对象可编辑
			let jituanObjEdit = {
				title: (<div fieldid='endgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "endgroupamount",
				key: "endgroupamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						// // 集团本币与原币或组织本币其中一个相同，则不可编辑，与哪个相同，就在录入哪个字段时自动带出
						// if (flag.currtype == flag.groupCurrType || flag.localCurrType == flag.groupCurrType) {
						// 	return <div>{text.display  || ''}</div>
						// } else {
						// 	return <NCNumber
						// 		scale={Number(text.scale)}
						// 		placeholder="请输入集团本币"
						// 		value={text.display  || ''}
						// 		onBlur={this.onBlur(index, "endgroupamount")}
						// 	/>
						// }

						if (info.NC001 == 'raw_convert') {
							if (flag.currtype == flag.groupCurrType) {
								return (
									this.renderNCNumber(text, "endgroupamount")

								)
							} else {
								return (
									this.renderNCNumber(text, "endgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
								)
							}
						}

						if (info.NC001 == 'local_convert') {
							if (flag.localCurrType == flag.groupCurrType) {
								return (
									this.renderNCNumber(text, "endgroupamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "endgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
								)
							}
						}



					} else {
						return (
							this.renderNCNumber(text, "endgroupamount")
						)
					}

				}
			}


			//1.全局、集团、组织本币
			if (flag.isGlobal && flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				//大条件已经是组织本币了，所以都不可编辑

				columns2 = [
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [shuliangObj, zuzhiObj, jituanObj, quanjuObj],
					},
				];
			}

			//2.全局、集团、非组织本币
			if (flag.isGlobal && flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditNum && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObjEdit, yuanbiObjEdit, zuzhiObjEdit, jituanObjEdit, quanjuObjEdit],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObj, yuanbiObj, zuzhiObj, jituanObj, quanjuObj],
						},
					];
				}

			}

			//3.全局、非集团、组织本币
			if (flag.isGlobal && !flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [shuliangObj, zuzhiObj, quanjuObj],
					},
				];

			}

			//4.全局、非集团、非组织本币

			if (flag.isGlobal && !flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditNum && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObjEdit, yuanbiObjEdit, zuzhiObjEdit, quanjuObjEdit],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObj, yuanbiObj, zuzhiObj, quanjuObj],
						},
					];
				}

			}

			//5.非全局、集团、组织本币
			if (!flag.isGlobal && flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [shuliangObj, zuzhiObj, jituanObj,],
					},
				];

			}

			//6.非全局、集团、非组织本币
			if (!flag.isGlobal && flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditNum && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObjEdit, yuanbiObjEdit, zuzhiObjEdit, jituanObjEdit,],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObj, yuanbiObj, zuzhiObj, jituanObj,],
						},
					];
				}

			}

			//7.非全局、非集团、组织本币
			if (!flag.isGlobal && !flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [shuliangObj, zuzhiObj,],
					},
				];
			}


			//8.非全局、非集团、非组织本币
			if (!flag.isGlobal && !flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditNum && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObjEdit, yuanbiObjEdit, zuzhiObjEdit,],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObj, yuanbiObj, zuzhiObj,],
						},
					];
				}

			}


		} else {
			//1.全局、集团、组织本币
			//2.全局、集团、非组织本币
			//3.全局、非集团、组织本币
			//4.全局、非集团、非组织本币
			//5.非全局、集团、组织本币
			//6.非全局、集团、非组织本币
			//7.非全局、非集团、组织本币
			//8.非全局、非集团、非组织本币

			// 年初数量对象

			// 2018.5.24 年初都不可编辑？先做成年初都不可编辑吧
			let nianchushuliangObj = {
				title: (<div fieldid='yearquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "yearquantity",
				key: "yearquantity",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "yearquantity")
				)
			}

			// 年初数量对象可编辑
			let nianchushuliangObjEdit = {
				title: (<div fieldid='yearquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "yearquantity",
				key: "yearquantity",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						if (record.unit && record.unit.value) {

							return (
								this.renderNCNumber(text, "yearquantity", index, getMultiLangByID('20020401-000016'), true)/* 国际化处理： 数量*/
							)

						} else {
							return (
								this.renderNCNumber(text, "yearquantity")
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "yearquantity")
						)
					}
				}

			}

			// 年初原币对象
			let nianchuyuanbiObj = {
				title: (<div fieldid='yearamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "yearamount",
				key: "yearamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "yearamount")
				)
			}


			// 年初原币对象可编辑
			let nianchuyuanbiObjEdit = {
				title: (<div fieldid='yearamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "yearamount",
				key: "yearamount",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {


						return (
							this.renderNCNumber(text, "yearamount", index, getMultiLangByID('20020401-000017'), true)/* 国际化处理： 原币*/
						)

					} else {
						return (
							this.renderNCNumber(text, "yearamount")
						)
					}

				}
			}



			// 年初组织本币对象
			let nianchuzuzhiObj = {
				title: (<div fieldid='yearlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "yearlocalamount",
				key: "yearlocalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "yearlocalamount")
				)
			};

			// 年初组织本币对象可编辑
			let nianchuzuzhiObjEdit = {
				title: (<div fieldid='yearlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "yearlocalamount",
				key: "yearlocalamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						// 所选币种与辅助核算带的组织本币币种相同，则不可编辑
						if (flag.currtype == flag.localCurrType) {
							return (
								this.renderNCNumber(text, "yearlocalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "yearlocalamount", index, getMultiLangByID('20020401-000000'), true)/* 国际化处理： 组织本币*/
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "yearlocalamount")
						)
					}

				}

			};

			// 年初全局本币对象 disabled
			let nianchuquanjuObj = {
				title: (<div fieldid='yearglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "yearglobalamount",
				key: "yearglobalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "yearglobalamount")
				)
			}

			// 年初全局本币对象可编辑
			let nianchuquanjuObjEdit = {
				title: (<div fieldid='yearglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "yearglobalamount",
				key: "yearglobalamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						if (flag.currtype == flag.globalCurrType || flag.localCurrType == flag.globalCurrType) {
							return (
								this.renderNCNumber(text, "yearglobalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "yearglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "yearglobalamount")
						)
					}

				}


			}

			// 年初集团本币对象
			let nianchujituanObj = {
				title: (<div fieldid='yeargroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "yeargroupamount",
				key: "yeargroupamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "yeargroupamount")
				)
			}

			// 年初集团本币对象可编辑
			let nianchujituanObjEdit = {
				title: (<div fieldid='yeargroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "yeargroupamount",
				key: "yeargroupamount",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						if (flag.currtype == flag.groupCurrType || flag.localCurrType == flag.groupCurrType) {
							return (
								this.renderNCNumber(text, "yeargroupamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "yeargroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "yeargroupamount")
						)
					}


				}
			}

			// 期初数量对象
			let qichushuliangObj = {
				title: (<div fieldid='endquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "endquantity",
				key: "endquantity",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endquantity")
				)

			};

			// 期初数量对象可编辑
			let qichushuliangObjEdit = {
				title: (<div fieldid='endquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "endquantity",
				key: "endquantity",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						if (record.unit && record.unit.value) {
							return (
								this.renderNCNumber(text, "endquantity", index, getMultiLangByID('20020401-000016'), true)/* 国际化处理： 数量*/
							)

						} else {
							return (
								this.renderNCNumber(text, "endquantity")
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "endquantity")
						)
					}


				}

			};

			// 期初原币对象
			let qichuyuanbiObj = {
				title: (<div fieldid='endamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "endamount",
				key: "endamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endamount")
				)
			};

			// 期初原币对象可编辑
			let qichuyuanbiObjEdit = {
				title: (<div fieldid='endamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "endamount",
				key: "endamount",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						return (
							this.renderNCNumber(text, "endamount", index, getMultiLangByID('20020401-000017'), true)/* 国际化处理： 原币*/
						)



					} else {
						return (
							this.renderNCNumber(text, "endamount")
						)
					}

				}
			};

			// 期初组织本币对象
			let qichuzuzhiObj = {
				title: (<div fieldid='endlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "endlocalamount",
				key: "endlocalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endlocalamount")
				)
			};

			// 期初组织本币对象可编辑
			let qichuzuzhiObjEdit = {
				title: (<div fieldid='endlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "endlocalamount",
				key: "endlocalamount",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						if (flag.currtype == flag.localCurrType) {
							return (
								this.renderNCNumber(text, "endlocalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "endlocalamount", index, getMultiLangByID('20020401-000000'), true)/* 国际化处理： 组织本币*/
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "endlocalamount")
						)
					}

				}

			};

			// 期初全局本币对象
			let qichuquanjuObj = {
				title: (<div fieldid='endglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "endglobalamount",
				key: "endglobalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endglobalamount")
				)
			}

			// 期初全局本币对象可编辑
			let qichuquanjuObjEdit = {
				title: (<div fieldid='endglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "endglobalamount",
				key: "endglobalamount",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						// if (flag.currtype == flag.globalCurrType  || flag.localCurrType == flag.globalCurrType) {
						//   return <div>{text.display  || ''}</div>
						// } else {

						//   return <NCNumber
						//   	scale={Number(text.scale)}
						//   	placeholder="请输入全局本币"
						//   	value={text.display  || ''}
						//   	onBlur={this.onBlur(index, "endglobalamount")}
						//   />
						// }


						if (info.NC002 == 'raw_convert') {
							if (flag.currtype == flag.globalCurrType) {
								return (
									this.renderNCNumber(text, "endglobalamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "endglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
								)
							}
						}

						if (info.NC002 == 'local_convert') {
							if (flag.localCurrType == flag.globalCurrType) {
								return (
									this.renderNCNumber(text, "endglobalamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "endglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
								)
							}
						}




					} else {
						return (
							this.renderNCNumber(text, "endglobalamount")
						)
					}

				}


			}

			// 期初集团本币对象
			let qichujituanObj = {
				title: (<div fieldid='endgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "endgroupamount",
				key: "endgroupamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endgroupamount")
				)
			}

			// 期初集团本币对象可编辑
			let qichujituanObjEdit = {
				title: (<div fieldid='endgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "endgroupamount",
				key: "endgroupamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						// if (flag.currtype == flag.groupCurrType || flag.localCurrType == flag.groupCurrType) {
						//   return <span>{text.display  || ''}</span>
						// } else {

						//   return <NCNumber
						//   	scale={Number(text.scale)}
						//   	placeholder="请输入集团本币"
						//   	value={text.display  || ''}
						//   	onBlur={this.onBlur(index, "endgroupamount")}
						//   />
						// }


						if (info.NC001 == 'raw_convert') {
							if (flag.currtype == flag.groupCurrType) {
								return (
									this.renderNCNumber(text, "endgroupamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "endgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
								)
							}
						}

						if (info.NC001 == 'local_convert') {
							if (flag.localCurrType == flag.groupCurrType) {
								return (
									this.renderNCNumber(text, "endgroupamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "endgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
								)
							}
						}

					} else {
						return (
							this.renderNCNumber(text, "endgroupamount")
						)
					}
				}

			}


			// 借方数量对象
			let jiefangshuliangObj = {
				title: (<div fieldid='debitquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "debitquantity",
				key: "debitquantity",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "debitquantity")
				)
			}

			// 借方数量对象可编辑
			let jiefangshuliangObjEdit = {
				title: (<div fieldid='debitquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "debitquantity",
				key: "debitquantity",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						if (record.unit && record.unit.value) {

							return (
								this.renderNCNumber(text, "debitquantity", index, getMultiLangByID('20020401-000016'), true)/* 国际化处理： 数量*/
							)

						} else {
							return (
								this.renderNCNumber(text, "debitquantity")
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "debitquantity")
						)
					}

				}

			}

			// 借方原币对象
			let jiefangyuanbiObj = {
				title: (<div fieldid='debitamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "debitamount",
				key: "debitamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "debitamount")
				)
			}

			// 借方原币对象可编辑
			let jiefangyuanbiObjEdit = {
				title: (<div fieldid='debitamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "debitamount",
				key: "debitamount",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						return (
							this.renderNCNumber(text, "debitamount", index, getMultiLangByID('20020401-000017'), true)/* 国际化处理： 原币*/
						)
					} else {
						return (
							this.renderNCNumber(text, "debitamount")
						)
					}


				}

			}

			// 借方组织本币对象
			let jiefangzuzhiObj = {
				title: (<div fieldid='localdebitamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "localdebitamount",
				key: "localdebitamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "localdebitamount")
				)
			};

			// 借方组织本币对象可编辑
			let jiefangzuzhiObjEdit = {
				title: (<div fieldid='localdebitamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "localdebitamount",
				key: "localdebitamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						// 所选币种与辅助核算带的组织本币币种相同，则不可编辑
						if (flag.currtype == flag.localCurrType) {
							return (
								this.renderNCNumber(text, "localdebitamount")
							)
						} else {

							return (
								this.renderNCNumber(text, "localdebitamount", index, getMultiLangByID('20020401-000000'), true)/* 国际化处理： 组织本币*/
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "localdebitamount")
						)
					}

				}

			};

			// 借方全局本币对象
			let jiefangquanjuObj = {
				title: (<div fieldid='debitglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "debitglobalamount",
				key: "debitglobalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "debitglobalamount")
				)
			};

			// 借方全局本币对象可编辑
			let jiefangquanjuObjEdit = {
				title: (<div fieldid='debitglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "debitglobalamount",
				key: "debitglobalamount",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						if (info.NC002 == 'raw_convert') {
							if (flag.currtype == flag.globalCurrType) {
								return (
									this.renderNCNumber(text, "debitglobalamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "debitglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
								)
							}
						}

						if (info.NC002 == 'local_convert') {
							if (flag.localCurrType == flag.globalCurrType) {
								return (
									this.renderNCNumber(text, "debitglobalamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "debitglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
								)
							}
						}


					} else {
						return (
							this.renderNCNumber(text, "debitglobalamount")
						)
					}
				}

			};

			// 借方集团本币对象
			let jiefangjituanObj = {
				title: (<div fieldid='debitgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "debitgroupamount",
				key: "debitgroupamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "debitgroupamount")
				)
			};

			// 借方集团本币对象可编辑
			let jiefangjituanObjEdit = {
				title: (<div fieldid='debitgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "debitgroupamount",
				key: "debitgroupamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						// if (flag.currtype == flag.groupCurrType || flag.localCurrType == flag.groupCurrType) {
						//   return <span>{text.display  || ''}</span>
						// } else {

						//   return <NCNumber
						//   	scale={Number(text.scale)}
						//   	placeholder="请输入集团本币"
						//   	value={text.display  || ''}
						//   	onBlur={this.onBlur(index, "debitgroupamount")}
						//   />
						// }

						if (info.NC001 == 'raw_convert') {
							if (flag.currtype == flag.groupCurrType) {
								return (
									this.renderNCNumber(text, "debitgroupamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "debitgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
								)
							}
						}

						if (info.NC001 == 'local_convert') {
							if (flag.localCurrType == flag.groupCurrType) {
								return (
									this.renderNCNumber(text, "debitgroupamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "debitgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
								)
							}
						}

					} else {
						return (
							this.renderNCNumber(text, "debitgroupamount")
						)
					}

				}

			};


			// 贷方数量对象
			let daifangshuliangObj = {
				title: (<div fieldid='creditquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "creditquantity",
				key: "creditquantity",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "creditquantity")
				)

			};

			// 贷方数量对象可编辑
			let daifangshuliangObjEdit = {
				title: (<div fieldid='creditquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "creditquantity",
				key: "creditquantity",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						if (record.unit && record.unit.value) {

							return (
								this.renderNCNumber(text, "creditquantity", index, getMultiLangByID('20020401-000016'), true)/* 国际化处理： 数量*/
							)

						} else {
							return (
								this.renderNCNumber(text, "creditquantity")
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "creditquantity")
						)
					}
				}

			};

			// 贷方原币对象
			let daifangyuanbiObj = {
				title: (<div fieldid='creditamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "creditamount",
				key: "creditamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "creditamount")
				)
			};

			// 贷方原币对象可编辑
			let daifangyuanbiObjEdit = {
				title: (<div fieldid='creditamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "creditamount",
				key: "creditamount",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {

						return (
							this.renderNCNumber(text, "creditamount", index, getMultiLangByID('20020401-000017'), true)/* 国际化处理： 原币*/
						)

					} else {
						return (
							this.renderNCNumber(text, "creditamount")
						)
					}
				}
			};

			// 贷方组织本币对象
			let daifangzuzhiObj = {
				title: (<div fieldid='localcreditamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "localcreditamount",
				key: "localcreditamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "localcreditamount")
				)
			};

			// 贷方组织本币对象可编辑
			let daifangzuzhiObjEdit = {
				title: (<div fieldid='localcreditamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "localcreditamount",
				key: "localcreditamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						// 所选币种与辅助核算带的组织本币币种相同，则不可编辑
						if (flag.currtype == flag.localCurrType) {
							return (
								this.renderNCNumber(text, "localcreditamount")
							)
						} else {

							return (
								this.renderNCNumber(text, "localcreditamount", index, getMultiLangByID('20020401-000000'), true)/* 国际化处理： 组织本币*/
							)
						}

					} else {
						return (
							this.renderNCNumber(text, "localcreditamount")
						)
					}
				}
			};

			// 贷方全局本币对象
			let daifangquanjuObj = {
				title: (<div fieldid='creditglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "creditglobalamount",
				key: "creditglobalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "creditglobalamount")
				)
			}

			// 贷方全局本币对象可编辑
			let daifangquanjuObjEdit = {
				title: (<div fieldid='creditglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "creditglobalamount",
				key: "creditglobalamount",
				width: '160px',
				render: (text, record, index) => {

					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						// 所选币种与辅助核算带的组织本币币种相同，则不可编辑

						if (info.NC002 == 'raw_convert') {
							if (flag.currtype == flag.globalCurrType) {
								return (
									this.renderNCNumber(text, "creditglobalamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "creditglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
								)
							}
						}

						if (info.NC002 == 'local_convert') {
							if (flag.localCurrType == flag.globalCurrType) {
								return (
									this.renderNCNumber(text, "creditglobalamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "creditglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
								)
							}
						}

					} else {
						return (
							this.renderNCNumber(text, "creditglobalamount")
						)
					}
				}
			}

			// 贷方集团本币对象
			let daifangjituanObj = {
				title: (<div fieldid='creditgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "creditgroupamount",
				key: "creditgroupamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "creditgroupamount")
				)
			}

			// 贷方集团本币对象可编辑
			let daifangjituanObjEdit = {
				title: (<div fieldid='creditgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "creditgroupamount",
				key: "creditgroupamount",
				width: '160px',
				render: (text, record, index) => {
					//该行是末级科目，则该行原则上大多都可编辑
					if (record.endflag && record.endflag.value == 'Y' && (!record.accAssItems)) {
						// 所选币种与辅助核算带的组织本币币种相同，则不可编辑

						if (info.NC001 == 'raw_convert') {
							if (flag.currtype == flag.groupCurrType) {
								return (
									this.renderNCNumber(text, "creditgroupamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "creditgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
								)
							}
						}

						if (info.NC001 == 'local_convert') {
							if (flag.localCurrType == flag.groupCurrType) {
								return (
									this.renderNCNumber(text, "creditgroupamount")
								)
							} else {
								return (
									this.renderNCNumber(text, "creditgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
								)
							}
						}

					} else {
						return (
							this.renderNCNumber(text, "creditgroupamount")
						)
					}

				}

			}

			//1.全局、集团、组织本币
			if (flag.isGlobal && flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/

				columns2 = [
					{
						title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
						children: [jiefangshuliangObj, jiefangzuzhiObj, jiefangjituanObj, jiefangquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
						children: [daifangshuliangObj, daifangzuzhiObj, daifangjituanObj, daifangquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [qichushuliangObj, qichuzuzhiObj, qichujituanObj, qichuquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
						children: [nianchushuliangObj, nianchuzuzhiObj, nianchujituanObj, nianchuquanjuObj],
					},
				];
			}

			//2.全局、集团、非组织本币
			if (flag.isGlobal && flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/

				if (isEditNum && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObjEdit, jiefangyuanbiObjEdit, jiefangzuzhiObjEdit, jiefangjituanObjEdit, jiefangquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObjEdit, daifangyuanbiObjEdit, daifangzuzhiObjEdit, daifangjituanObjEdit, daifangquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObjEdit, qichuyuanbiObjEdit, qichuzuzhiObjEdit, qichujituanObjEdit, qichuquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchujituanObj, nianchuquanjuObj],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObj, jiefangyuanbiObj, jiefangzuzhiObj, jiefangjituanObj, jiefangquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObj, daifangyuanbiObj, daifangzuzhiObj, daifangjituanObj, daifangquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObj, qichuyuanbiObj, qichuzuzhiObj, qichujituanObj, qichuquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchujituanObj, nianchuquanjuObj],
						},
					];
				}


			}

			//3.全局、非集团、组织本币
			if (flag.isGlobal && !flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
						children: [jiefangshuliangObj, jiefangzuzhiObj, jiefangquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
						children: [daifangshuliangObj, daifangzuzhiObj, daifangquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [qichushuliangObj, qichuzuzhiObj, qichuquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
						children: [nianchushuliangObj, nianchuzuzhiObj, nianchuquanjuObj],
					},
				];

			}

			//4.全局、非集团、非组织本币

			if (flag.isGlobal && !flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/

				if (isEditNum && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObjEdit, jiefangyuanbiObjEdit, jiefangzuzhiObjEdit, jiefangquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObjEdit, daifangyuanbiObjEdit, daifangzuzhiObjEdit, daifangquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObjEdit, qichuyuanbiObjEdit, qichuzuzhiObjEdit, qichuquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchuquanjuObj],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObj, jiefangyuanbiObj, jiefangzuzhiObj, jiefangquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObj, daifangyuanbiObj, daifangzuzhiObj, daifangquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObj, qichuyuanbiObj, qichuzuzhiObj, qichuquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchuquanjuObj],
						},
					];
				}

			}

			//5.非全局、集团、组织本币
			if (!flag.isGlobal && flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
						children: [jiefangshuliangObj, jiefangzuzhiObj, jiefangjituanObj,],
					},
					{
						title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
						children: [daifangshuliangObj, daifangzuzhiObj, daifangjituanObj,],
					},
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [qichushuliangObj, qichuzuzhiObj, qichujituanObj,],
					},
					{
						title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
						children: [nianchushuliangObj, nianchuzuzhiObj, nianchujituanObj,],
					},

				];

			}

			//6.非全局、集团、非组织本币
			if (!flag.isGlobal && flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditNum && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObjEdit, jiefangyuanbiObjEdit, jiefangzuzhiObjEdit, jiefangjituanObjEdit,],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObjEdit, daifangyuanbiObjEdit, daifangzuzhiObjEdit, daifangjituanObjEdit,],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObjEdit, qichuyuanbiObjEdit, qichuzuzhiObjEdit, qichujituanObjEdit,],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchujituanObj,],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObj, jiefangyuanbiObj, jiefangzuzhiObj, jiefangjituanObj,],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObj, daifangyuanbiObj, daifangzuzhiObj, daifangjituanObj,],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObj, qichuyuanbiObj, qichuzuzhiObj, qichujituanObj,],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchujituanObj,],
						},
					];
				}
			}



			//7.非全局、非集团、组织本币
			if (!flag.isGlobal && !flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
						children: [jiefangshuliangObj, jiefangzuzhiObj,],
					},
					{
						title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
						children: [daifangshuliangObj, daifangzuzhiObj,],
					},
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [qichushuliangObj, qichuzuzhiObj,],
					},
					{
						title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
						children: [nianchushuliangObj, nianchuzuzhiObj,],
					},
				];
			}


			// 8.非全局、非集团、非组织本币
			if (!flag.isGlobal && !flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditNum && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObjEdit, jiefangyuanbiObjEdit, jiefangzuzhiObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObjEdit, daifangyuanbiObjEdit, daifangzuzhiObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObjEdit, qichuyuanbiObjEdit, qichuzuzhiObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObj, jiefangyuanbiObj, jiefangzuzhiObj],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObj, daifangyuanbiObj, daifangzuzhiObj],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObj, qichuyuanbiObj, qichuzuzhiObj],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj],
						},
					];
				}

			}


		}

		let columns = columns1.concat(columns2);
		let theme = window.top.nccColor  //获取当前主题'black'
		// 添加空白列
		columns.push({ title: '', key: '', width: 1 })

		// let data = deepClone(this.state.mydata);
		let data = this.state.mydata;
		let iWidth = window.innerWidth
		let iHeight = window.innerHeight
		return (
			<div className='opentable'>
				<NCDiv fieldid={this.tableId} areaCode={NCDiv.config.TableCom}>
					<Table
						// ref='table'
						bordered  //出现一个横向不可滚动的滚动条时会出现错行，bbq建议先去掉
						columns={columns}
						// bordered={true}
						// height={30} //固定行目前有错行问题
						headerHeight={26}
						heightConsistent={true} //修改折行错行问题
						adaptionHeight={true}
						data={data}
						// scroll={{ x: iWidth - 60, y: iHeight - 180 }}
						rowClassName={(record) => {
							if (record.endflag.value === 'N') {
								return theme==='black' ? 'blue_b' : 'blue'  //黑色主题透明度50%
							}
						}}
					/>
            	</NCDiv>
				<div>
					{createModal('AccountAssistantModal', {
						title: getMultiLangByID('20020401-000014'), /* 国际化处理： 辅助核算*/
						content: this.modalContent('AccountAssistant'), //弹框内容，可以是字符串或dom
						closeModalEve: this.closeModalEve, //关闭按钮事件回调
						size: 'xxlg', //  模态框大小 sm/lg/xlg
						bodyHeight: modalHeight,
						draggable: false,
						resizable: false,
						validateCloseBtn: true,  //点击关闭按钮的校验,默认是false不校验，校验是true
						noFooter: true, //是否需要底部按钮,默认有footer,有false,没有true
						closeByClickBackDrop: false,//点击遮罩关闭提示框，true是点击关闭，false是阻止关闭,默认是false
						hasBackDrop: true,//显示遮罩层，显示是true，不显示是false，默认是true
						zIndex: 200//遮罩的层级为zIndex，弹框默认为zIndex+1。默认为200，非比传项
					})}
				</div>
			</div>
		)
	}
}

OpenTable = createPage({})(OpenTable);
export default OpenTable
