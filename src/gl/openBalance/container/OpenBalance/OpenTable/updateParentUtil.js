import {accAdd, Subtr} from '../../../../public/common/method.js'
/**
 * // 期初余额
 * // 数量  endquantity
	// 原币 endamount
	// 组织本币 endlocalamount
	// 集团本币 endgroupamount
	// 全局本币 endglobalamount

    // 年初余额
    // 数量  yearquantity
	// 原币 yearamount
	// 组织本币 yearlocalamount
	// 集团本币 yeargroupamount
	// 全局本币 yearglobalamount

    // 借方累计
    // 数量  debitquantity
	// 原币  debitamount
	// 组织本币  localdebitamount
	// 集团本币 debitgroupamount
	// 全局本币 debitglobalamount

    // 贷方累计
    // 数量  creditquantity
	// 原币  creditamount
	// 组织本币  localcreditamount
	// 集团本币 creditgroupamount
	// 全局本币 creditglobalamount
 */
/**
 * 遍历数组将父级关联到子级数据上
 * @param {*} data 
 */
function setParent(data){
    for(let i = data.length - 1; i >= 0; i--){
        let row = data[i];
        // accountLevel: 级别
        if(row.accountLevel.value > 1){
            for(let j = i - 1; j >= 0; j--){
                let rowUp = data[j];
                // accountCode: 科目编码
                if(row.accountCode.display.startsWith(rowUp.accountCode.display)){
                    row.parent = rowUp;
                    break;
                }
            }
        }
    }
}

/**
 * 获取末级差值
 * @param {*} newRow 
 * @param {*} oldRow 
 */
function getDiffValue(newRow, oldRow){
    const diff = [];
    let j = 0;
    diff[j++] = Subtr(newRow.debitquantity.value, oldRow.debitquantity.value);
    diff[j++] = Subtr(newRow.debitamount.value, oldRow.debitamount.value);
    diff[j++] = Subtr(newRow.localdebitamount.value, oldRow.localdebitamount.value);
    diff[j++] = Subtr(newRow.debitgroupamount.value, oldRow.debitgroupamount.value);
    diff[j++] = Subtr(newRow.debitglobalamount.value, oldRow.debitglobalamount.value);

    diff[j++] = Subtr(newRow.creditquantity.value, oldRow.creditquantity.value);
    diff[j++] = Subtr(newRow.creditamount.value, oldRow.creditamount.value);
    diff[j++] = Subtr(newRow.localcreditamount.value, oldRow.localcreditamount.value);
    diff[j++] = Subtr(newRow.creditgroupamount.value, oldRow.creditgroupamount.value);
    diff[j++] = Subtr(newRow.creditglobalamount.value, oldRow.creditglobalamount.value);

    diff[j++] = Subtr(newRow.endquantity.value, oldRow.endquantity.value);
    diff[j++] = Subtr(newRow.endamount.value, oldRow.endamount.value);
    diff[j++] = Subtr(newRow.endlocalamount.value, oldRow.endlocalamount.value);
    diff[j++] = Subtr(newRow.endgroupamount.value, oldRow.endgroupamount.value);
    diff[j++] = Subtr(newRow.endglobalamount.value, oldRow.endglobalamount.value);

    diff[j++] = Subtr(newRow.yearquantity.value, oldRow.yearquantity.value);
    diff[j++] = Subtr(newRow.yearamount.value, oldRow.yearamount.value);
    diff[j++] = Subtr(newRow.yearlocalamount.value, oldRow.yearlocalamount.value);
    diff[j++] = Subtr(newRow.yeargroupamount.value, oldRow.yeargroupamount.value);
    diff[j++] = Subtr(newRow.yearglobalamount.value, oldRow.yearglobalamount.value);
    return diff;
}

/**
 * 数字转换成千分位 格式
 * @param {*} num 
 */
function commafy(num) {
    let pointIndex, intPart, pointPart;
    if (num === "-") {
      return "-";
    }
    if (isNaN(num)) {
      return "";
    }

    num = num + "";
    if (/^.*\..*$/.test(num)) {
      pointIndex = num.lastIndexOf(".");
      intPart = num.substring(0, pointIndex);
      pointPart = num.substring(pointIndex + 1, num.length);
      intPart = intPart + "";
      let re = /(-?\d+)(\d{3})/;
      while (re.test(intPart)) {
        intPart = intPart.replace(re, "$1,$2");
      }
      num = intPart + "." + pointPart;
    } else {
      num = num + "";
      let re = /(-?\d+)(\d{3})/;
      while (re.test(num)) {
        num = num.replace(re, "$1,$2");
      }
    }
    return num;
}

/**
 * 精度处理
 * @param {*} value 
 * @param {*} scale 
 */
function formatDot(value, scale = 6) {
    let formatVal, dotSplit, val;
    val = (value || 0).toString();
    dotSplit = val.split(".");
    if (dotSplit.length > 2 || !value) {
      return value;
    }
    if (val.indexOf(".") > -1) {
      if (scale == 0) {
        formatVal = dotSplit[0];
      } else {
        // 向下取整模式
        formatVal = val.substring(0, val.indexOf(".") + scale + 1);
      }
    } else {
      formatVal = val;
    }

    return formatVal;
}

/**
 * 四舍五入
 * @param {*} value 
 * @param {*} scale 
 */
function ncRounding(value, scale) {
    // 如果没有精度，不需要处理四舍五入
    if (!scale) return value;
  
    let [_value, _scale] = [value, scale];
  
    if (
      !Object.prototype.toString.call(scale) !== "[object Number]" &&
      !isNaN(Number(scale))
    )
      _scale = Number(scale);
  
    // 校验参数
    if (Object.prototype.toString.call(value) !== "[object String]")
      _value = String(value);
  
    //移除千分位
    // _value = _value.replace(/\,/gi, "");
  
    const re = /^(\-|\+)?(\d+)?\.?(\d+)?$/;
  
    if (!re.test(_value)) {
      // console.warn("处理参数异常");
      return value;
    }
  
    // 分割value
    let [beforePoint, afterPoint] = _value.split(".");
  
    // 有小数位数
    if (afterPoint && afterPoint !== "") {
      // 判断小数位数与精度的关系
      if (afterPoint.length > _scale) {
        _value = Number(_value);
        // 进行四舍五入操作
        _value = Number(_value.toFixed(_scale + 1));
  
        _value = _value * Math.pow(10, _scale);
  
        _value = Math.round(_value);
  
        _value = _value / Math.pow(10, _scale);
      } else {
        _value = addZero(value, scale)
      }
    }
  
    return _value;
  }

/**
 * 移除千分位
 * @param {*} val 
 */
function removeThousands(val) {
    return val ? String(val).replace(/\,/gi, "") : "";
}

/**
 * 补零
 * @param {strinng/Number} num 
 * @param {精度} scale 
 */
function addZero(num, scale) {
    if (isNaN(Number(num)) || !num) {
        return null;
    }
    let _num = num.toString()
    if (scale > 0) {
        let start = _num.split('.')[0];
        let end = _num.split('.')[1];
        if (!end) {
            end = '';
        }
        let len = end.length;
        if (len < scale) {
            end = end.padEnd(scale, '0');
        }
        return start + '.' + end;
    } else {
        return _num;
    }
}

/**
 * 初始化整数补零或四舍五入
 * @param {*} value 
 * @param {*} scale 
 */
function initNumber(value, scale) {
      // 初始化整数补零
      let endVal 
      if (value && scale) {
        // 如果数据有小数点尝试四舍五入
        if (String(value).includes(".")) {
            endVal = ncRounding(value, scale);
        } else {
          // 如果没有小数点就尝试进行补零
           endVal = addZero(value, scale);
        }
      }
      return endVal
}

/**
 * num为零时，区分display,value
 * @param {string/Number} num 
 * @param {精度} scale 
 * @param {string} type 
 */
function distShow (num,scale,type=''){		
    let finalStr = ''
    let tempNum = initNumber(num, scale)
    if(type === 'display') {
        if(Number(tempNum)===0){
            finalStr = ''
        }else {
            finalStr = tempNum
        }
    } else { 			
        finalStr = tempNum
    }
    return finalStr
}

function valueChanged(row, diff){
    let j = 0;
    // 借方累计
    let debitquantityValue = accAdd(row.debitquantity.value, diff[j++]); //数量
    row.debitquantity.display = distShow(debitquantityValue, row.debitquantity.scale, 'display')
    row.debitquantity.value = distShow(debitquantityValue, row.debitquantity.scale)

    let debitamountValue = accAdd(row.debitamount.value, diff[j++]); //原币
    row.debitamount.display = distShow(debitamountValue, row.debitamount.scale, 'display')
    row.debitamount.value = distShow(debitamountValue, row.debitamount.scale)

    let localdebitamountValue = accAdd(row.localdebitamount.value, diff[j++]); // 组织本币
    row.localdebitamount.display = distShow(localdebitamountValue, row.localdebitamount.scale, 'display')
    row.localdebitamount.value = distShow(localdebitamountValue, row.localdebitamount.scale)

    let debitgroupamountValue = accAdd(row.debitgroupamount.value, diff[j++]); //集团本币
    row.debitgroupamount.display = distShow(debitgroupamountValue, row.debitgroupamount.scale, 'display')
    row.debitgroupamount.value = distShow(debitgroupamountValue, row.debitgroupamount.scale)

    let debitglobalamountValue = accAdd(row.debitglobalamount.value, diff[j++]); //全局本币
    row.debitglobalamount.display = distShow(debitglobalamountValue, row.debitglobalamount.scale, 'display')
    row.debitglobalamount.value = distShow(debitglobalamountValue, row.debitglobalamount.scale)

    // 贷方累计 
    let creditquantityValue = accAdd(row.creditquantity.value, diff[j++]);
    row.creditquantity.display = distShow(creditquantityValue, row.creditquantity.scale, 'display')
    row.creditquantity.value = distShow(creditquantityValue, row.creditquantity.scale)

    let creditamountValue = accAdd(row.creditamount.value, diff[j++]);
    row.creditamount.display = distShow(creditamountValue, row.creditamount.scale, 'display')
    row.creditamount.value = distShow(creditamountValue, row.creditamount.scale)

    let localcreditamountValue = accAdd(row.localcreditamount.value, diff[j++]);
    row.localcreditamount.display = distShow(localcreditamountValue, row.localcreditamount.scale, 'display')
    row.localcreditamount.value = distShow(localcreditamountValue, row.localcreditamount.scale)

    let creditgroupamountValue = accAdd(row.creditgroupamount.value, diff[j++]);
    row.creditgroupamount.display = distShow(creditgroupamountValue, row.creditgroupamount.scale, 'display')
    row.creditgroupamount.value = distShow(creditgroupamountValue, row.creditgroupamount.scale)

    let creditglobalamountValue = accAdd(row.creditglobalamount.value, diff[j++]);
    row.creditglobalamount.display = distShow(creditglobalamountValue, row.creditglobalamount.scale, 'display')
    row.creditglobalamount.value = distShow(creditglobalamountValue, row.creditglobalamount.scale)

    // 期初余额
    let endquantityValue = accAdd(row.endquantity.value, diff[j++]);
    row.endquantity.display = distShow(endquantityValue, row.endquantity.scale, 'display')
    row.endquantity.value = distShow(endquantityValue, row.endquantity.scale)

    let endamountValue = accAdd(row.endamount.value, diff[j++]);
    row.endamount.display = distShow(endamountValue, row.endamount.scale, 'display')
    row.endamount.value = distShow(endamountValue, row.endamount.scale)

    let endlocalamountValue = accAdd(row.endlocalamount.value, diff[j++]);
    row.endlocalamount.display = distShow(endlocalamountValue, row.endlocalamount.scale, 'display')
    row.endlocalamount.value = distShow(endlocalamountValue, row.endlocalamount.scale)

    let endgroupamountValue = accAdd(row.endgroupamount.value, diff[j++]);
    row.endgroupamount.display = distShow(endgroupamountValue, row.endgroupamount.scale, 'display')
    row.endgroupamount.value = distShow(endgroupamountValue, row.endgroupamount.scale)

    let endglobalamountValue = accAdd(row.endglobalamount.value, diff[j++]);
    row.endglobalamount.display = distShow(endglobalamountValue, row.endglobalamount.scale, 'display')
    row.endglobalamount.value = distShow(endglobalamountValue, row.endglobalamount.scale)

    // 年初余额
    let yearquantityValue = accAdd(row.yearquantity.value, diff[j++]);
    row.yearquantity.display = distShow(yearquantityValue, row.yearquantity.scale, 'display')
    row.yearquantity.value = distShow(yearquantityValue, row.yearquantity.scale)

    let yearamountValue = accAdd(row.yearamount.value, diff[j++]);
    row.yearamount.display = distShow(yearamountValue, row.yearamount.scale, 'display')
    row.yearamount.value = distShow(yearamountValue, row.yearamount.scale)

    let yearlocalamountValue = accAdd(row.yearlocalamount.value, diff[j++]);
    row.yearlocalamount.display = distShow(yearlocalamountValue, row.yearlocalamount.scale, 'display')
    row.yearlocalamount.value = distShow(yearlocalamountValue, row.yearlocalamount.scale)

    let yeargroupamountValue = accAdd(row.yeargroupamount.value, diff[j++]);
    row.yeargroupamount.display = distShow(yeargroupamountValue, row.yeargroupamount.scale, 'display')
    row.yeargroupamount.value = distShow(yeargroupamountValue, row.yeargroupamount.scale)

    let yearglobalamountValue = accAdd(row.yearglobalamount.value, diff[j++]);
    row.yearglobalamount.display = distShow(yearglobalamountValue, row.yearglobalamount.scale, 'display')
    row.yearglobalamount.value = distShow(yearglobalamountValue, row.yearglobalamount.scale)

    if(row.parent){
        valueChanged(row.parent, diff);
    }
}

export {setParent, getDiffValue, valueChanged, commafy, formatDot, ncRounding, removeThousands, addZero, initNumber, distShow};
