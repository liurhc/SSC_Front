import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
import { base, ajax, deepClone, getBusinessInfo, promptBox, createPage, toast, getMultiLang, initMultiLangByModule, getMultiLangByID } from 'nc-lightapp-front';
const { NCButton: Button, NCBackBtn, NCTable: Table, NCCheckbox: Checkbox, NCNumber, NCTooltip: Tooltip, NCPagination, NCDiv, NCTooltip } = base;
import { accAdd, Subtr } from '../../../verify/verify/vrify/events/method.js';
import AssidModal from '../../../public/components/assidModal'
import './index.less';
import { commonApi } from '../../../public/common/actions.js'
import { onButtonClick, initTemplate } from '../OpenBalance/events';
import ImportFile from '../../../public/components/ImportFile'
import cacheUtils from '../../utils/cacheUtils'
import lockAPI from '../../utils/lockAPI'
import { onEnter, onArrowUpDown } from '../../../public/common/findNextFocusItemInTable'
import { getTableHeight } from '../../../public/common/method.js';
import { setParent, getDiffValue, valueChanged, commafy, removeThousands, initNumber, distShow } from '../OpenBalance/OpenTable/updateParentUtil.js'

let multiSelect = {
	type: "checkbox",
	param: "key"
};

const emptyCell = <span>&nbsp;</span>
//react-demo 13.html
class AccountAssistant extends Component {
	constructor(props) {
		super(props);
		//每定义一个state 都要添加备注
		this.state = {
			changesList: [],
			tmpletType: '',
			showImportModal: false,		// 导入导出框 显示
			assid: '',
			assidCondition: '',
			mydata: {},
			initVOslength: null,
			isQueryShow: false,
			showAssidModal: false,
			pretentAssData: {},
			// AssistModalShow: false,
			selected: [],
			checkedAll: false,
			checkedArray: [
			],
			// flag: [],
			info: {
				key: {
					book: '',
					unit: '',
					year: '',
					currtype: '',
					account: '',
					buorg: '',
					NC001: '',
					NC002: '',
					buSecond: []
				},
				namesAll: {
					book: '',
					unit: '',
					yearPeriod: '',
					currtypeName: '',
					kemucode: '',
					kemuname: '',
					fangxiang: '',
					versiondate: '',
				},
				// rates: {
				// 	NC001: '',
				// 	NC002: '',
				// 	excrate2: '',
				// 	excrate3: '',
				// 	excrate4: '',
				// },
				flag: {
					isInitBuild: null, //是否已建账
					isYearStart: false, //启用期间是否年初
					isGlobal: false,   //是否启用全局本币
					isGroup: false,   //是否启用集团本币
					currtype: '',     //币种，组织本币特殊
					globalCurrType: null, //全局本币		//下同，用来判断列是否可编辑用的	
					groupCurrType: null,  //集团本币
					localCurrType: null, //组织本币
				},
				accAssItems: [],
				newInitVOs: { unit: null },
			},
			json: {},
		}
		this.context1 = {};
		let buziInfo = getBusinessInfo();
		this.context1.buziInfo = buziInfo;
		this.versionDate = buziInfo.businessDate.split(' ')[0];
		this.tableId = 'assistant'
		this.ViewModel = this.props.ViewModel;
	}
	setDelBtnStatus = (checkedArray) => {
		if (checkedArray.includes(true)) {
			this.props.button.setButtonDisabled(['del_line'], false)
		} else {
			this.props.button.setButtonDisabled(['del_line'], true)
		}
	}
	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		let listData = self.state.mydata.assInitVOs.concat();
		let selIds = [];

		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}

		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray,

		}, self.setDelBtnStatus(checkedArray));

	}

	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		self.setState({
			checkedAll: allFlag,
			checkedArray: checkedArray,

		}, self.setDelBtnStatus(checkedArray));

	}

	renderColumnsMultiSelect(columns) {
		const { checkedArray } = this.state;
		let select_column = {};
		let indeterminate_bool = false;
		if (multiSelect && multiSelect.type === "checkbox") {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
						<div fieldid="firstcol" className='checkbox-mergecells'>
							<Checkbox	
								className="table-checkbox"
								checked={this.state.checkedAll}
								indeterminate={indeterminate_bool && !this.state.checkedAll}
								onChange={this.onAllCheckChange}
							/>
						</div>
					),
					key: "checkbox",
					attrcode: 'checkbox',
					dataIndex: 'checkbox',
					// width: "5%",
					width: "60px",
					fixed: "left",
					render: (text, record, index) => {
						return (
							<div fieldid="firstcol">
								<Checkbox
									className="table-checkbox"
									checked={this.state.checkedArray[index]}
									onChange={this.onCheckboxChange.bind(this, text, record, index)}
								/>
							</div>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}

	checkChange = () => {
		let { mydata, changesList } = this.state

		// let mydata = this.state.mydata;
		// let changesList = deepClone(this.state.changesList);

		let list = [...new Set(changesList)]
		// let array = deepClone(mydata.assInitVOs);
		let array = mydata.assInitVOs
		let assInit = array.filter(function (item, index) {
			return list.indexOf(index) > -1;
		})
		return assInit
	}

	// 保存数据只留value
	filterValues = (item) => {
		let obj = {}
		Object.keys(item).map((key) => {
			if (item[key]) {
				if (item[key] instanceof Array) {
					let tempArry = []
					for (let i = 0; i < item[key].length; i++) {
						// let tempObj = this.filterValues(item[key][i]);
						let tempObj = {
							'pk_checktype': item[key][i].pk_checktype,
							'pk_checkvalue': item[key][i].pk_checkvalue
						}
						tempArry.push(tempObj);
					}
					obj[key] = tempArry
				} else {
					obj[key] = { 'value': item[key].value }
				}
			}
		})
		delete obj.xuhao
		return obj
	}
	saveAss = (callback) => {
		if (this.state.info.flag.isInitBuild) {
			toast({ content: getMultiLangByID('20020401-000003'), color: 'warning' });/* 国际化处理： 已建账，无须保存*/
		}
		let { initVOslength, mydata } = this.state

		if (initVOslength === 0 && mydata.assInitVOs.length === 0) {
			toast({ content: getMultiLangByID('20020401-000004'), color: 'warning' });/* 国际化处理： 没有需要保存的数据*/
		}

		let isChangeArr = this.checkChange()
		if (initVOslength === mydata.assInitVOs.length && isChangeArr.length == 0) {
			toast({ content: getMultiLangByID('20020401-000005'), color: 'warning' });/* 国际化处理： 未做修改，无需保存*/
			return
		}

		// let self = this;
		let info = this.state.info;
		let url = '/nccloud/gl/voucher/saveInit.do';

		let data = {
			pk_accountbook: info.key.book,
			pk_unit: info.key.unit,
			year: info.key.year,
			pk_currtype: info.key.currtype,
			newInitVOs: [this.state.info.newInitVOs],


		};

		let tempArr = mydata.assInitVOs.map((item) => {
			return this.filterValues(item)
		})
		data.newInitVOs[0].assInitWebVOs = tempArr

		ajax({
			loading: true,
			url,
			data,
			success: (res) => {
				const { data, message, success } = res;
				if (success) {
					let consInfo
					if (mydata.assInitVOs.length > 0) {
						consInfo = this.consInfo()
					} else {
						consInfo = info
					}

					// 保存成功后删除缓存锁
					let pk_accasoa = mydata.pk_accasoa
					let lockMap = cacheUtils.getLockMap()
					lockMap.delete(pk_accasoa)
					this.setState({
						changesList: [],
						initVOslength: mydata.assInitVOs.length,
					})
					if (callback) {
						callback()
					}
				} else {
					let pk_accasoa = mydata.pk_accasoa
					let lockMap = cacheUtils.getLockMap()
					lockMap.delete(pk_accasoa)
					toast({ content: message.message, color: 'warning' });
				}
			}
		});
	}
	saveTable = () => {
		let self = this;
		self.saveAss(() => {
			toast({ content: getMultiLangByID('20020401-000006'), color: 'success' });/* 国际化处理： 保存成功*/
			self.cacheData()
		})
	}
	cacheData = () => {
		let allData = cacheUtils.getOpenData()
		let assisData = cacheUtils.getAssisData();
		let index = assisData.newInitVOs.xuhao ? assisData.newInitVOs.xuhao - 1 : null;
		let newInitValue = this.getFinalInitVO();
		let diff = getDiffValue(newInitValue, allData.data[index]);
		valueChanged(allData.data[index], diff);
		cacheUtils.setOpenData(allData)
	}
	// 返回前缓存数据
	setBackData() {
		this.cacheData()
		this.props.handleCloseModal()
	}
	// 返回
	goBack() {
		let isChangeArr = this.checkChange()
		let { initVOslength, mydata } = this.state

		if (initVOslength !== mydata.assInitVOs.length || isChangeArr.length > 0) {
			promptBox({
				color: 'warning',
				content: getMultiLangByID('20020401-000007'),/* 国际化处理： 是保存已修改数据*/
				beSureBtnName: getMultiLangByID('20020401-000008'),          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 是*/
				cancelBtnName: getMultiLangByID('20020401-000009'),  /* 国际化处理： 否*/
				beSureBtnClick: () => {
					this.saveAss(() => {
						toast({ content: getMultiLangByID('20020401-000006'), color: 'success' });/* 国际化处理： 保存成功*/
						this.setBackData()
					})
				},
				cancelBtnClick: () => {
					this.props.handleCloseModal()
				}
			});
		} else {
			this.setBackData()
		}

	}

	//direction:方向,0:借;debit:借方; credit:贷方; year:年初; cycle:期初	
	calcYearBalances(direction, cycle, debit, credit, scale) {
		let _scale = scale ? Number(scale) : 2
		let add, sub, year
		let directionVal = direction === getMultiLangByID('20020401-000010') ? '0' : '1'/* 国际化处理： 借*/
		if (directionVal === '0') {
			// 借：年初余额 = 期初余额 - 借方累计 + 贷方累计
			add = accAdd(cycle, credit);
			sub = Subtr(add, debit);
			year = sub
		} else {
			// 贷：年初余额 = 期初余额 + 借方累计 - 贷方累计	
			add = accAdd(cycle, debit);
			sub = Subtr(add, credit);
			year = sub
		}
		let _year = initNumber(year, _scale)
		// let _year = addZero(year, _scale)
		return _year
	}
	consLockData() {
		let { info, mydata } = this.state
		let pk_accasoa = mydata.pk_accasoa
		let sendData = {
			"pk_accountingbook": info.key.book,
			"pk_unit": info.key.unit ? info.key.unit : info.key.buorg,
			"year": info.key.year,
			"pk_currtype": info.key.currtype,
			"pk_accasoa": pk_accasoa
		}
		return sendData
	}
	//数据联动
	linkData = (index, key, value, data, rowData, flag, info, changesList) => {
		data.assInitVOs[index][key].display = value;
		data.assInitVOs[index][key].value = value;

		// 方向: balanorient
		let direction = this.state.info.namesAll.fangxiang
		// 原币
		let endamount = rowData.endamount.value
		let debitamount = rowData.debitamount.value
		let creditamount = rowData.creditamount.value
		let amountScale = rowData.endamount.scale
		let initAmountVal = initNumber(value, amountScale)
		// 组织本币
		let endlocalamount = rowData.endlocalamount.value
		let localdebitamount = rowData.localdebitamount.value
		let localcreditamount = rowData.localcreditamount.value
		let localamountScale = rowData.endlocalamount.scale
		let initLocalamountVal = initNumber(value, localamountScale)
		// 集团本币
		let endgroupamount = rowData.endgroupamount.value
		let debitgroupamount = rowData.debitgroupamount.value
		let creditgroupamount = rowData.creditgroupamount.value
		let groupamountScale = rowData.endgroupamount.scale
		let initGroupamountVal = initNumber(value, groupamountScale)
		// 全局本币
		let endglobalamount = rowData.endglobalamount.value
		let debitglobalamount = rowData.debitglobalamount.value
		let creditglobalamount = rowData.creditglobalamount.value
		let globalamountScale = rowData.endglobalamount.scale
		let initGlobalamountVal = initNumber(value, globalamountScale)
		// yearquantity 年初数量
		// endquantity  期初数量
		// debitquantity 借方数量
		// creditquantity  贷方数量
		let endquantity = rowData.endquantity.value
		let debitquantity = rowData.debitquantity.value
		let creditquantity = rowData.creditquantity.value
		let tempVal
		let scale = data.assInitVOs[index].yearamount.scale

		let localScale = data.assInitVOs[index].yearlocalamount.scale
		let groupScale = data.assInitVOs[index].yeargroupamount.scale
		let globalScale = data.assInitVOs[index].yearglobalamount.scale
		let qtScale = data.assInitVOs[index].yearquantity.scale
		//期初余额原币变
		if (key == "endamount") {

			// if (flag.currtype == flag.localCurrType)
			tempVal = this.calcYearBalances(direction, value, debitamount, creditamount, scale)
			// 原币
			data.assInitVOs[index].yearamount.display = tempVal;
			data.assInitVOs[index].yearamount.value = tempVal;

			if (flag.currtype == flag.localCurrType) {
				// 组织本币
				data.assInitVOs[index].endlocalamount.display = initLocalamountVal;
				data.assInitVOs[index].endlocalamount.value = value;
				data.assInitVOs[index].yearlocalamount.display = tempVal;
				data.assInitVOs[index].yearlocalamount.value = tempVal;
			}

			if (flag.currtype == flag.groupCurrType && info.NC001 == 'raw_convert') {
				// 集团本币
				data.assInitVOs[index].endgroupamount.display = initGroupamountVal;
				data.assInitVOs[index].endgroupamount.value = value;
				data.assInitVOs[index].yeargroupamount.display = tempVal;
				data.assInitVOs[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && info.NC002 == 'raw_convert') {
				// 全局本币
				data.assInitVOs[index].endglobalamount.display = initGlobalamountVal;
				data.assInitVOs[index].endglobalamount.value = value;
				data.assInitVOs[index].yearglobalamount.display = tempVal;
				data.assInitVOs[index].yearglobalamount.value = tempVal;
			}

			if (flag.currtype == flag.groupCurrType && flag.currtype == flag.localCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data.assInitVOs[index].endgroupamount.display = initGroupamountVal;
				data.assInitVOs[index].endgroupamount.value = value;
				data.assInitVOs[index].yeargroupamount.display = tempVal;
				data.assInitVOs[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && flag.currtype == flag.localCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data.assInitVOs[index].endglobalamount.display = initGlobalamountVal;
				data.assInitVOs[index].endglobalamount.value = value;
				data.assInitVOs[index].yearglobalamount.display = tempVal;
				data.assInitVOs[index].yearglobalamount.value = tempVal;
			}

		}

		//借方原币变
		if (key == 'debitamount') {
			tempVal = this.calcYearBalances(direction, endamount, value, creditamount, scale)
			// 原币
			data.assInitVOs[index].yearamount.display = tempVal;
			data.assInitVOs[index].yearamount.value = tempVal;
			if (flag.currtype == flag.localCurrType) {
				// 组织本币
				data.assInitVOs[index].localdebitamount.display = initLocalamountVal;
				data.assInitVOs[index].localdebitamount.value = value;
				data.assInitVOs[index].yearlocalamount.display = tempVal;
				data.assInitVOs[index].yearlocalamount.value = tempVal;
			}

			if (flag.currtype == flag.groupCurrType && info.NC001 == 'raw_convert') {
				// 集团本币
				data.assInitVOs[index].debitgroupamount.display = initGroupamountVal;
				data.assInitVOs[index].debitgroupamount.value = value;
				data.assInitVOs[index].yeargroupamount.display = tempVal;
				data.assInitVOs[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && info.NC002 == 'raw_convert') {
				// 全局本币
				data.assInitVOs[index].debitglobalamount.display = initGlobalamountVal;
				data.assInitVOs[index].debitglobalamount.value = value;
				data.assInitVOs[index].yearglobalamount.display = tempVal;
				data.assInitVOs[index].yearglobalamount.value = tempVal;
			}

			if (flag.currtype == flag.groupCurrType && flag.currtype == flag.localCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data.assInitVOs[index].debitgroupamount.display = initGroupamountVal;
				data.assInitVOs[index].debitgroupamount.value = value;
				data.assInitVOs[index].yeargroupamount.display = tempVal;
				data.assInitVOs[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && flag.currtype == flag.localCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data.assInitVOs[index].debitglobalamount.display = initGlobalamountVal;
				data.assInitVOs[index].debitglobalamount.value = value;
				data.assInitVOs[index].yearglobalamount.display = tempVal;
				data.assInitVOs[index].yearglobalamount.value = tempVal;
			}

		}

		//贷方原币变
		if (key == 'creditamount') {
			tempVal = this.calcYearBalances(direction, endamount, debitamount, value, scale)
			// 原币
			data.assInitVOs[index].yearamount.display = tempVal;
			data.assInitVOs[index].yearamount.value = tempVal;

			if (flag.currtype == flag.localCurrType) {
				// 组织本币
				data.assInitVOs[index].localcreditamount.display = initLocalamountVal;
				data.assInitVOs[index].localcreditamount.value = value;
				data.assInitVOs[index].yearlocalamount.display = tempVal;
				data.assInitVOs[index].yearlocalamount.value = tempVal;
			}

			if (flag.currtype == flag.groupCurrType && info.NC001 == 'raw_convert') {
				// 集团本币
				data.assInitVOs[index].creditgroupamount.display = initGroupamountVal;
				data.assInitVOs[index].creditgroupamount.value = value;
				data.assInitVOs[index].yeargroupamount.display = tempVal;
				data.assInitVOs[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && info.NC002 == 'raw_convert') {
				// 全局本币
				data.assInitVOs[index].creditglobalamount.display = initGlobalamountVal;
				data.assInitVOs[index].creditglobalamount.value = value;
				data.assInitVOs[index].yearglobalamount.display = tempVal;
				data.assInitVOs[index].yearglobalamount.value = tempVal;
			}


			if (flag.currtype == flag.groupCurrType && flag.currtype == flag.localCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data.assInitVOs[index].creditgroupamount.display = initGroupamountVal;
				data.assInitVOs[index].creditgroupamount.value = value;
				data.assInitVOs[index].yeargroupamount.display = tempVal;
				data.assInitVOs[index].yeargroupamount.value = tempVal;
			}

			if (flag.currtype == flag.globalCurrType && flag.currtype == flag.localCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data.assInitVOs[index].creditglobalamount.display = initGlobalamountVal;
				data.assInitVOs[index].creditglobalamount.value = value;
				data.assInitVOs[index].yearglobalamount.display = tempVal;
				data.assInitVOs[index].yearglobalamount.value = tempVal;
			}

		}


		// 期初组织本币变
		if (key == "endlocalamount") {

			tempVal = this.calcYearBalances(direction, value, localdebitamount, localcreditamount, localScale)

			// // 组织本币
			data.assInitVOs[index].yearlocalamount.display = tempVal;
			data.assInitVOs[index].yearlocalamount.value = tempVal;

			if (flag.localCurrType == flag.groupCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data.assInitVOs[index].endgroupamount.display = initGroupamountVal;
				data.assInitVOs[index].endgroupamount.value = value;
				data.assInitVOs[index].yeargroupamount.display = tempVal;
				data.assInitVOs[index].yeargroupamount.value = tempVal;
			}

			if (flag.localCurrType == flag.globalCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data.assInitVOs[index].endglobalamount.display = initGlobalamountVal;
				data.assInitVOs[index].endglobalamount.value = value;
				data.assInitVOs[index].yearglobalamount.display = tempVal;
				data.assInitVOs[index].yearglobalamount.value = tempVal;
			}

		}


		//借方方组织本币变
		if (key == 'localdebitamount') {

			tempVal = this.calcYearBalances(direction, endlocalamount, value, localcreditamount, localScale)
			// // 组织本币
			data.assInitVOs[index].yearlocalamount.display = tempVal;
			data.assInitVOs[index].yearlocalamount.value = tempVal;

			if (flag.localCurrType == flag.groupCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data.assInitVOs[index].debitgroupamount.display = initGroupamountVal;
				data.assInitVOs[index].debitgroupamount.value = value;
				data.assInitVOs[index].yeargroupamount.display = tempVal;
				data.assInitVOs[index].yeargroupamount.value = tempVal;
			}

			if (flag.localCurrType == flag.globalCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data.assInitVOs[index].debitglobalamount.display = initGlobalamountVal;
				data.assInitVOs[index].debitglobalamount.value = value;
				data.assInitVOs[index].yearglobalamount.display = tempVal;
				data.assInitVOs[index].yearglobalamount.value = tempVal;
			}

		}

		//贷方组织本币变
		if (key == 'localcreditamount') {

			tempVal = this.calcYearBalances(direction, endlocalamount, localdebitamount, value, localScale)
			// // 组织本币
			data.assInitVOs[index].yearlocalamount.display = tempVal;
			data.assInitVOs[index].yearlocalamount.value = tempVal;

			if (flag.localCurrType == flag.groupCurrType && info.NC001 == 'local_convert') {
				// 集团本币
				data.assInitVOs[index].creditgroupamount.display = initGroupamountVal;
				data.assInitVOs[index].creditgroupamount.value = value;
				data.assInitVOs[index].yeargroupamount.display = tempVal;
				data.assInitVOs[index].yeargroupamount.value = tempVal;
			}

			if (flag.localCurrType == flag.globalCurrType && info.NC002 == 'local_convert') {
				// 全局本币
				data.assInitVOs[index].creditglobalamount.display = initGlobalamountVal;
				data.assInitVOs[index].creditglobalamount.value = value;
				data.assInitVOs[index].yearglobalamount.display = tempVal;
				data.assInitVOs[index].yearglobalamount.value = tempVal;
			}
		}

		// 期初集团本币
		if (key == 'endgroupamount') {
			tempVal = this.calcYearBalances(direction, value, debitgroupamount, creditgroupamount, groupScale)
			data.assInitVOs[index].yeargroupamount.display = tempVal;
			data.assInitVOs[index].yeargroupamount.value = tempVal;
		}
		// 借方集团本币
		if (key == 'debitgroupamount') {
			tempVal = this.calcYearBalances(direction, endgroupamount, value, creditgroupamount, groupScale)
			data.assInitVOs[index].yeargroupamount.display = tempVal;
			data.assInitVOs[index].yeargroupamount.value = tempVal;
		}
		// 贷方集团本币
		if (key == 'creditgroupamount') {
			tempVal = this.calcYearBalances(direction, endgroupamount, debitgroupamount, value, groupScale)
			data.assInitVOs[index].yeargroupamount.display = tempVal;
			data.assInitVOs[index].yeargroupamount.value = tempVal;
		}

		// 期初全局本币
		if (key == 'endglobalamount') {
			tempVal = this.calcYearBalances(direction, value, debitglobalamount, creditglobalamount, globalScale)
			data.assInitVOs[index].yearglobalamount.display = tempVal;
			data.assInitVOs[index].yearglobalamount.value = tempVal;
		}
		// 借方全局本币
		if (key == 'debitglobalamount') {
			tempVal = this.calcYearBalances(direction, endgroupamount, value, creditgroupamount, globalScale)
			data.assInitVOs[index].yearglobalamount.display = tempVal;
			data.assInitVOs[index].yearglobalamount.value = tempVal;
		}
		// 贷方全局本币
		if (key == 'creditglobalamount') {
			tempVal = this.calcYearBalances(direction, endgroupamount, debitgroupamount, value, globalScale)
			data.assInitVOs[index].yearglobalamount.display = tempVal;
			data.assInitVOs[index].yearglobalamount.value = tempVal;
		}

		// yearquantity 年初数量
		// endquantity  期初数量
		// debitquantity 借方数量
		// creditquantity  贷方数量

		if (key == 'endquantity') {
			tempVal = this.calcYearBalances(direction, value, debitquantity, creditquantity, qtScale)
			// 期初数量
			data.assInitVOs[index].yearquantity.display = tempVal;
			data.assInitVOs[index].yearquantity.value = tempVal;
		}
		if (key == 'debitquantity') {
			tempVal = this.calcYearBalances(direction, endquantity, value, creditquantity, qtScale)
			// 借方数量
			data.assInitVOs[index].yearquantity.display = tempVal;
			data.assInitVOs[index].yearquantity.value = tempVal;
		}
		if (key == 'creditquantity') {
			tempVal = this.calcYearBalances(direction, endquantity, debitquantity, value, qtScale)
			// 贷方数量
			data.assInitVOs[index].yearquantity.display = tempVal;
			data.assInitVOs[index].yearquantity.value = tempVal;
		}

		this.setState({ mydata: data, changesList });
	}
	// 编辑输入框
	onBlur = (index, key) => {
		return value => {
			//只要触发了onBlur就将index放入数组
			let changesList = this.state.changesList;
			changesList.push(index);

			let data = this.state.mydata
			// let orgData = deepClone(data)
			let rowData = data.assInitVOs[index]
			let oldRow = JSON.parse(JSON.stringify(rowData));
			let flag = this.state.info.flag;
			let info = {
				NC001: this.state.info.key.NC001,
				NC002: this.state.info.key.NC002,
			}
			let pk_accasoa = data.pk_accasoa
			let lock = cacheUtils.getLockMap().get(pk_accasoa)
			let sendData = this.consLockData()
			if (!lock) {
				if (!lockAPI.addAccountLock(sendData)) {
					data.assInitVOs[index] = oldRow
					this.setState({
						mydata: data
						// mydata: orgData
					})
				} else {
					this.linkData(index, key, value, data, rowData, flag, info, changesList)
				}
			} else {
				this.linkData(index, key, value, data, rowData, flag, info, changesList)
			}


		};
	};

	addTable() {
		// let mydata = deepClone(this.state.mydata);
		let { mydata, checkedArray } = this.state
		let pk_accasoa = mydata.pk_accasoa
		let lock = cacheUtils.getLockMap().get(pk_accasoa)
		let sendData = this.consLockData()
		if (!lock) {
			lockAPI.addAccountLock(sendData)
		}
		//新增行的默认数据，部分携带值未加
		let preinfo = this.state.info.newInitVOs
		let newLine = {
			accAssItems: null,
			accountCode: {
				display: '',
				scale: null,
				value: '',
			},
			accountLevel: {
				display: '',
				scale: null,
				value: '',
			},
			accountName: {
				display: '',
				scale: null,
				value: '',
			},
			assDefines: null,
			assInitWebVOs: null,
			assValues: [],
			assid: {
				display: '',
				scale: null,
				value: '',
			},
			balanorient: {
				display: '',
				scale: null,
				value: '',
			},
			creditamount: {
				display: '',
				scale: preinfo.creditamount.scale,
				value: '0',
			},
			creditglobalamount: {
				display: '',
				scale: preinfo.creditglobalamount.scale,
				value: '0',
			},
			creditgroupamount: {
				display: '',
				scale: preinfo.creditgroupamount.scale,
				value: '0',
			},
			creditquantity: {
				display: '',
				scale: preinfo.creditquantity.scale,
				value: '0',
			},
			debitamount: {
				display: '',
				scale: preinfo.debitamount.scale,
				value: '0',
			},
			debitglobalamount: {
				display: '',
				scale: preinfo.debitglobalamount.scale,
				value: '0',
			},
			debitgroupamount: {
				display: '',
				scale: preinfo.debitgroupamount.scale,
				value: '0',
			},
			debitquantity: {
				display: '',
				scale: preinfo.debitquantity.scale,
				value: '0',
			},
			endamount: {
				display: '',
				scale: preinfo.endamount.scale,
				value: '0',
			},
			endflag: {
				display: '',
				scale: null,
				value: '0',
			},
			endglobalamount: {
				display: '',
				scale: preinfo.endglobalamount.scale,
				value: '0',
			},
			endgroupamount: {
				display: '',
				scale: preinfo.endgroupamount.scale,
				value: '0',
			},
			endlocalamount: {
				display: '',
				scale: preinfo.endlocalamount.scale,
				value: '0',
			},
			endquantity: {
				display: '',
				scale: preinfo.endquantity.scale,
				value: '0',
			},
			key: `row${mydata.assInitVOs.length + 1}`,
			localcreditamount: {
				display: '',
				scale: preinfo.localcreditamount.scale,
				value: '0',
			},
			localdebitamount: {
				display: '',
				scale: preinfo.localdebitamount.scale,
				value: '0',
			},
			pk_accasoa: {
				display: '',
				scale: null,
				value: '',
			},
			unit: null,
			yearamount: {
				display: '',
				scale: preinfo.yearamount.scale,
				value: '0',
			},
			yearglobalamount: {
				display: '',
				scale: preinfo.yearglobalamount.scale,
				value: '0',
			},
			yeargroupamount: {
				display: '',
				scale: preinfo.yeargroupamount.scale,
				value: '0',
			},
			yearlocalamount: {
				display: '',
				scale: preinfo.yearlocalamount.scale,
				value: '0',
			},
			yearquantity: {
				display: '',
				scale: preinfo.yearquantity.scale,
				value: '0',
			},
			xuhao: mydata.assInitVOs.length + 1,
		}
		// let checkedArray = deepClone(this.state.checkedArray);
		checkedArray.push(false)

		// let mydata = deepClone(this.state.mydata);
		mydata.assInitVOs.push(newLine)
		this.setState({
			mydata,
			checkedArray,
		})
	}

	delTable() {
		let { mydata, checkedArray } = this.state
		let pk_accasoa = mydata.pk_accasoa
		let lock = cacheUtils.getLockMap().get(pk_accasoa)
		let sendData = this.consLockData()
		if (!lock) {
			lockAPI.addAccountLock(sendData)
		}

		let { assInitVOs } = mydata
		let newArr = [];
		let newCheckArr = [];
		checkedArray.forEach(function (item, index) {
			if (item == false) {
				newArr.push(assInitVOs[index]);
				newCheckArr.push(false)
			}
		})

		newArr.forEach(function (item, index) {
			item.xuhao = index + 1;
		})
		mydata.assInitVOs = newArr;
		this.setState({
			mydata,
			checkedArray: newCheckArr,
			selected: [],
		}, this.setDelBtnStatus(newCheckArr))
	}


	getQueryStringArgs() {
		var search = (location.search.length > 0 ? location.search.slice(1) : "");
		var obj = {};
		var reg = /([^?&=]+)=([^?&=]*)/g;
		search.replace(reg, function (match, $1, $2) {
			var name = decodeURIComponent($1);
			var val = decodeURIComponent($2);
			obj[name] = val;
			return match;
		});
		return obj;
	}
	// 辅助核算页数据列取和
	consChacheData() {
		let { mydata, info } = this.state
		let assInitVOs = mydata.assInitVOs
		let sumData = {
			creditamount: 0,
			creditglobalamount: 0,
			creditgroupamount: 0,
			localcreditamount: 0,
			creditquantity: 0,

			debitamount: 0,
			debitglobalamount: 0,
			debitgroupamount: 0,
			localdebitamount: 0,
			debitquantity: 0,

			endamount: 0,
			endglobalamount: 0,
			endgroupamount: 0,
			endlocalamount: 0,
			endquantity: 0,

			yearamount: 0,
			yearglobalamount: 0,
			yeargroupamount: 0,
			yearlocalamount: 0,
			yearquantity: 0,
		}

		if (assInitVOs) {
			assInitVOs.map((item) => {

				// 贷方累计 
				sumData.creditamount += Number(item.creditamount.value)
				sumData.creditglobalamount += Number(item.creditglobalamount.value)
				sumData.creditgroupamount += Number(item.creditgroupamount.value)
				sumData.localcreditamount += Number(item.localcreditamount.value)
				sumData.creditquantity += Number(item.creditquantity.value) //数量
				// 借方累计
				sumData.debitamount += Number(item.debitamount.value)
				sumData.debitglobalamount += Number(item.debitglobalamount.value)
				sumData.debitgroupamount += Number(item.debitgroupamount.value)
				sumData.localdebitamount += Number(item.localdebitamount.value)
				sumData.debitquantity += Number(item.debitquantity.value)
				// 期初余额
				sumData.endamount += Number(item.endamount.value) //原币
				sumData.endglobalamount += Number(item.endglobalamount.value) //全局本币
				sumData.endgroupamount += Number(item.endgroupamount.value) //集团本币
				sumData.endlocalamount += Number(item.endlocalamount.value)  // 组织本币
				sumData.endquantity += Number(item.endquantity.value)  //数量
				// 年初余额
				sumData.yearamount += Number(item.yearamount.value) //原币
				sumData.yearglobalamount += Number(item.yearglobalamount.value) //全局本币
				sumData.yeargroupamount += Number(item.yeargroupamount.value) //集团本币
				sumData.yearlocalamount += Number(item.yearlocalamount.value) // 组织本币
				sumData.yearquantity += Number(item.yearquantity.value) //数量
			})
		}
		return sumData
	}
	distShow(num, scale, type = '') {
		let finalStr = ''
		let tempNum = initNumber(num, scale)
		if (type === 'display') {
			if (Number(tempNum) === 0) {
				finalStr = ''
			} else {
				finalStr = tempNum
			}
		} else {
			finalStr = tempNum
		}
		return finalStr
	}

	// 辅助核算返回期初余额前，处理缓存数据
	consInfo() {
		let { info } = this.state
		let sumData = this.consChacheData()
		if (sumData) {
			// 贷方累计 
			info.newInitVOs.creditamount.display = this.distShow(sumData.creditamount, info.newInitVOs.creditamount.scale, 'display')
			info.newInitVOs.creditamount.value = this.distShow(sumData.creditamount, info.newInitVOs.creditamount.scale)

			info.newInitVOs.creditglobalamount.display = this.distShow(sumData.creditglobalamount, info.newInitVOs.creditglobalamount.scale, 'display')
			info.newInitVOs.creditglobalamount.value = this.distShow(sumData.creditglobalamount, info.newInitVOs.creditglobalamount.scale)

			info.newInitVOs.creditgroupamount.display = this.distShow(sumData.creditgroupamount, info.newInitVOs.creditgroupamount.scale, 'display')
			info.newInitVOs.creditgroupamount.value = this.distShow(sumData.creditgroupamount, info.newInitVOs.creditgroupamount.scale)

			info.newInitVOs.localcreditamount.display = this.distShow(sumData.localcreditamount, info.newInitVOs.localcreditamount.scale, 'display')
			info.newInitVOs.localcreditamount.value = this.distShow(sumData.localcreditamount, info.newInitVOs.localcreditamount.scale)

			info.newInitVOs.creditquantity.display = this.distShow(sumData.creditquantity, info.newInitVOs.creditquantity.scale, 'display')  //数量
			info.newInitVOs.creditquantity.value = this.distShow(sumData.creditquantity, info.newInitVOs.creditquantity.scale)  //数量
			// 借方累计
			info.newInitVOs.debitamount.display = this.distShow(sumData.debitamount, info.newInitVOs.debitamount.scale, 'display')
			info.newInitVOs.debitamount.value = this.distShow(sumData.debitamount, info.newInitVOs.debitamount.scale)

			info.newInitVOs.debitglobalamount.display = this.distShow(sumData.debitglobalamount, info.newInitVOs.debitglobalamount.scale, 'display')
			info.newInitVOs.debitglobalamount.value = this.distShow(sumData.debitglobalamount, info.newInitVOs.debitglobalamount.scale)

			info.newInitVOs.debitgroupamount.display = this.distShow(sumData.debitgroupamount, info.newInitVOs.debitgroupamount.scale, 'display')
			info.newInitVOs.debitgroupamount.value = this.distShow(sumData.debitgroupamount, info.newInitVOs.debitgroupamount.scale)

			info.newInitVOs.localdebitamount.display = this.distShow(sumData.localdebitamount, info.newInitVOs.localdebitamount.scale, 'display')
			info.newInitVOs.localdebitamount.value = this.distShow(sumData.localdebitamount, info.newInitVOs.localdebitamount.scale)

			info.newInitVOs.debitquantity.display = this.distShow(sumData.debitquantity, info.newInitVOs.debitquantity.scale, 'display')
			info.newInitVOs.debitquantity.value = this.distShow(sumData.debitquantity, info.newInitVOs.debitquantity.scale)
			// 期初余额
			info.newInitVOs.endamount.display = this.distShow(sumData.endamount, info.newInitVOs.endamount.scale, 'display')  //原币
			info.newInitVOs.endamount.value = this.distShow(sumData.endamount, info.newInitVOs.endamount.scale)  //原币

			info.newInitVOs.endglobalamount.display = this.distShow(sumData.endglobalamount, info.newInitVOs.endglobalamount.scale, 'display') //全局本币
			info.newInitVOs.endglobalamount.value = this.distShow(sumData.endglobalamount, info.newInitVOs.endglobalamount.scale) //全局本币

			info.newInitVOs.endgroupamount.display = this.distShow(sumData.endgroupamount, info.newInitVOs.endgroupamount.scale, 'display') //集团本币
			info.newInitVOs.endgroupamount.value = this.distShow(sumData.endgroupamount, info.newInitVOs.endgroupamount.scale) //集团本币

			info.newInitVOs.endlocalamount.display = this.distShow(sumData.endlocalamount, info.newInitVOs.endlocalamount.scale, 'display')  // 组织本币
			info.newInitVOs.endlocalamount.value = this.distShow(sumData.endlocalamount, info.newInitVOs.endlocalamount.scale)  // 组织本币

			info.newInitVOs.endquantity.display = this.distShow(sumData.endquantity, info.newInitVOs.endquantity.scale, 'display')  //数量
			info.newInitVOs.endquantity.value = this.distShow(sumData.endquantity, info.newInitVOs.endquantity.scale)  //数量
			// 年初余额
			info.newInitVOs.yearamount.display = this.distShow(sumData.yearamount, info.newInitVOs.yearamount.scale, 'display') //原币
			info.newInitVOs.yearamount.value = this.distShow(sumData.yearamount, info.newInitVOs.yearamount.scale) //原币

			info.newInitVOs.yearglobalamount.display = this.distShow(sumData.yearglobalamount, info.newInitVOs.yearglobalamount.scale, 'display')  //全局本币
			info.newInitVOs.yearglobalamount.value = this.distShow(sumData.yearglobalamount, info.newInitVOs.yearglobalamount.scale)  //全局本币

			info.newInitVOs.yeargroupamount.display = this.distShow(sumData.yeargroupamount, info.newInitVOs.yeargroupamount.scale, 'display') //集团本币
			info.newInitVOs.yeargroupamount.value = this.distShow(sumData.yeargroupamount, info.newInitVOs.yeargroupamount.scale) //集团本币

			info.newInitVOs.yearlocalamount.display = this.distShow(sumData.yearlocalamount, info.newInitVOs.yearlocalamount.scale, 'display')  // 组织本币
			info.newInitVOs.yearlocalamount.value = this.distShow(sumData.yearlocalamount, info.newInitVOs.yearlocalamount.scale)  // 组织本币

			info.newInitVOs.yearquantity.display = this.distShow(sumData.yearquantity, info.newInitVOs.yearquantity.scale)  //数量
			info.newInitVOs.yearquantity.value = this.distShow(sumData.yearquantity, info.newInitVOs.yearquantity.scale, 'display')  //数量
		}
		return info
	}
	queryAss(searchObj) {

		let self = this;
		let url = '/nccloud/gl/voucher/getAssInit.do';
		/*		let data = {
					pk_accountingbook:"'160px'1A3'160px'000000000PQ",  //账簿主键
					pk_account:"0001Z0'160px'000000001A8",  //科目主键
					pk_currtype:"'160px'2Z0'160px'000000001K1",  //币种主键
					pk_units:["0001A3'160px'00000000NNH"],  //业务单元主键
					year:"2017"			//会计年			
				};*/

		let data = {
			pk_accountingbook: searchObj.pk_accountingbook,
			pk_account: searchObj.pk_account,
			pk_currtype: searchObj.pk_currtype,
			pk_units: searchObj.pk_units ? searchObj.pk_units : [],
			year: searchObj.year,
		};

		ajax({
			loading: true,
			url,
			data,
			delayTime: 500,
			success: function (res) {
				let { data, message, success } = res;
				let checkedArray = [];
				if (!data) {
					// data = deepClone([]);
					data = []
				}
				// let mydata = deepClone(data);
				let mydata = data
				if (!mydata.assInitVOs) {
					mydata.assInitVOs = []
				} else {
					mydata.assInitVOs.forEach(function (item, index) {
						// item.key = item.assid.value;
						item.xuhao = index + 1;
						checkedArray.push(false)
					})
				}

				if (success) {
					// cacheUtils.setAssisData(mydata)
					self.setState({
						initVOslength: mydata.assInitVOs.length,
						mydata: mydata,
						checkedArray,
						// data,
					})

				} else {
					toast({ content: message.message, color: 'warning' });
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });

			}
		});

	}

	/**
	 * 获取合计值VO
	 * 此方法需要consChacheData中的key值与VO保持一致
	 */
	getFinalInitVO = () => {
		let values = this.consChacheData();
		let vo = {};
		Object.keys(values).map((key) => {

			vo[key] = this.generateValueObject(key, values[key]);
		})
		return vo;
	}

	generateValueObject(key, value, display, scale) {
		let obj = { value, display, scale };
		return obj;
	}

	// 显示AssidModal框
	showAssidModal = (index, text, record) => {
		let assDatas = this.getAssidCondition(index);
		let { versionDate } = this;
		let changesList = this.state.changesList;
		changesList.push(index);
		let assisData = cacheUtils.getAssisData();
		let { buSecond, pk_org } = assisData.key
		if (buSecond.length === 1) {

		}
		let assid_org = buSecond.length === 1 ? (buSecond[0].values ? buSecond[0].values.pk_org.value : buSecond[0].refpk) : pk_org
		let assidCondition = {
			pk_accountingbook: this.state.info.key.book,
			pk_accasoa: this.state.info.newInitVOs.pk_accasoa.value,
			prepareddate: versionDate, //业务日期
			pk_org: assid_org, //财务组织
			assData: assDatas,
			assid: this.state.assid,
			checkboxShow: false,
			linenum: index,
		};
		this.setState({
			showAssidModal: true,
			assidCondition: assidCondition,
			assid: record.assid.value
		})
	}

	// 辅助核算AssidModal框 接收数据处理
	getAssidCondition(index) {
		let assInitVOs = this.state.mydata.assInitVOs
		let assArr = this.state.info.accAssItems
		let assData = []
		let refCode, refnodename
		if (assArr) {
			assArr.map((item, index) => {
				let data = {}
				refCode = item.refCode
				data.key = index;
				data.checktypecode = item.code
				data.checktypename = item.name
				data.pk_Checktype = item.pk_accassitem
				data.refnodename = item.refCode
				data.refCode = item.refCode
				data.m_classid = item.classid
				data.pk_accassitem = item.classid
				data.classid = item.classid;
				data.pk_defdoclist = item.classid;
				data.inputlength = item.inputlength;
				assData.push(data);
			})
		}
		if (assInitVOs && assInitVOs.length > index) {
			let initVO = assInitVOs[index];
			if (initVO.assValues && initVO.assValues.length > 0) {
				let { assValues } = initVO;
				assData.map((item) => {
					assValues.map((value) => {
						if (value.pk_checktype.value === item.pk_Checktype) {
							item.pk_Checkvalue = value.pk_checkvalue.value;
							item.checkvaluecode = value.checkvaluecode.value;
							item.checkvaluename = value.checkvaluename.value;
							return;
						}
					});
					return item;
				})
			}
		}
		return assData;
	}

	//辅助核算框 点击确定
	handleAssidOK(modalData) {
		this.setState({
			showAssidModal: false,
		});
		let { assidCondition, mydata } = this.state
		let assData = assidCondition.assData
		let linenum = assidCondition.linenum

		if (assData.length == 0) {
			this.setState({
				showAssidModal: false,
			});
			return false;
		}
		// let assInitVOs = deepClone(this.state.mydata.assInitVOs);
		let { assInitVOs } = mydata
		assInitVOs[linenum].assid.value = modalData.assid;
		assInitVOs[linenum].assid.display = modalData.assname;
		let assValues = [];
		if (modalData && modalData.data && modalData.data.length > 0) {
			modalData.data.map((item) => {
				let data = {
					pk_checktype: { value: item.pk_Checktype },
					checkvaluecode: { value: item.checkvaluecode },
					checkvaluename: { value: item.checkvaluename },
					pk_checkvalue: { value: item.pk_Checkvalue }
				}
				assValues.push(data);
			});
		}
		assInitVOs[linenum].assValues = assValues;
		// let mydata = deepClone(this.state.mydata)
		mydata.assInitVOs = assInitVOs;

		this.setState({
			showAssidModal: false,
			mydata: mydata
		});
	}

	getInit() {
		let info = cacheUtils.getAssisData()
		info = deepClone(info)
		this.setState({
			info,
		}, () => {
			let { currtype, isInitBuild, isStartBUSecond } = this.state.info.flag
			let { buSecond } = this.state.info.key
			// 1.未启用二级账簿可以编辑；2.启用二级账簿且只选一个二级账簿时，可以编辑
			let isEditAssid = isStartBUSecond ? (isStartBUSecond && buSecond.length === 1 ? true : false) : true
			if (currtype == getMultiLangByID('20020401-000000')) {/* 国际化处理： 组织本币*/
				// 导出按钮可用
				this.props.button.setButtonDisabled(['save', 'excel_opt', 'import', 'add_line', 'del_line'], true);
			} else {
				if (isInitBuild) {
					// 导出按钮可用
					this.props.button.setButtonDisabled(['save', 'import', 'add_line', 'del_line'], true);
				}
				if (!isEditAssid) {
					this.props.button.setButtonDisabled(['add_line', 'save', 'import'], true);
				}
			}
		});

		let pk_units;
		let { flag, key } = info;
		if (flag.isStartBUSecond) {
			pk_units = [];
			if (key.buSecond) {
				key.buSecond.map((item) => pk_units.push(item.refpk));
			}
		}

		let searchObj = {
			pk_accountingbook: info.key.book,
			pk_account: info.key.account,
			pk_currtype: info.key.currtype,
			// pk_units: info.key.unit,
			pk_units,
			year: info.key.year,
		};
		this.queryAss(searchObj);
	}

	showPromptBox(tmpletType) {
		let isChangeArr = this.checkChange()
		if (isChangeArr.length > 0) {
			promptBox({
				color: 'warning',
				content: getMultiLangByID('20020401-000011'),/* 国际化处理： 请保存已修改数据*/
				beSureBtnClick: () => {
					this.saveAss(() => {
						this.setState({
							showImportModal: true,
							tmpletType: tmpletType
						})
					})

				},
				cancelBtnClick: () => {
					// this.refresh();
				}
			});
		} else {
			this.setState({
				showImportModal: true,
				tmpletType: tmpletType
			})
		}
	}
	// 导入
	handleImport() {
		this.showPromptBox('import')
	}
	//导出
	handleExport() {
		let { initVOslength, mydata } = this.state

		if (initVOslength === 0 && mydata.assInitVOs.length === 0) {
			toast({ content: getMultiLangByID('20020401-000012'), color: 'warning' });/* 国际化处理： 没有需要导出的数据*/
		} else {
			this.showPromptBox('export')
		}
	}
	// 导入导出 数据整合
	getImportData() {
		let { mydata, info, tmpletType } = this.state
		let data = {}
		if (mydata) {
			data.year = mydata.year  //会计年
			data.period = mydata.period //会计期间
			data.versionDate = info.namesAll.versionDate // 版本日期,核算账簿返回的versionDate
			data.pk_accountingbook = mydata.accountintBook ? mydata.accountintBook.value : ''  //refpk
			data.pk_currtype = mydata.currType ? mydata.currType.value : ''  //币种
			data.pk_accasoa = mydata.pk_accasoa   //
			if (tmpletType === 'import') {
				data.action = '/nccloud/gl/voucher/initAssImport.do'  //导入接口地址，必传
				data.pk_unit = info.unit // 二级单位 refpk
			} else {
				data.action = '/nccloud/gl/voucher/initAssExport.do'  //导出接口地址，必传
				data.pk_units = [] // 二级单位组 refpk
				if (info.flag.isStartBUSecond) {
					if (info.key.buSecond) {
						info.key.buSecond.map((item) => data.pk_units.push(item.refpk));
					}
				}
			}
		}
		return data
	}
	refresh = () => {
		this.getInit()
		// this.MainSelect.getTables();
	}
	componentWillMount() {
		initTemplate.call(this, this.props);
		this.props.button.setButtonVisible(['cancel', 'refresh', 'calculate', 'init_build_drop', 'year_start', 'batch', 'print', 'out_template', 'clearData'], false);
		this.props.button.setButtonDisabled(['del_line'], true)
	}
	componentDidMount() {
		this.getInit()
		this.props.AccountAssistantRef(this)
	}
	renderNCNumber(text, attrcode, index, placeholder, isEdit = false) {
		let txt = `${getMultiLangByID('20020401-000022')}${placeholder}`/* 国际化处理： 请输入*/
		if (isEdit) {
			return (
				<div fieldid={attrcode}
					id={`hot-key-${this.tableId}-${attrcode}-${index}`}
				>
					<NCNumber
						// renderInTable={true} //解决在table中悬浮到其他行自动失去焦点的问题
						fieldid={attrcode}
						scale={Number(text.scale)}
						placeholder={txt}
						value={text.display || ''}
						onBlur={this.onBlur(index, attrcode)}
						onKeyDown={(e)=>{
							if(e.keyCode=='38'|| e.key === "ArrowUp"||e.keyCode=='40'|| e.key === "ArrowDown"){// 40 ArrowDown 38 ArrowUp
								onArrowUpDown(e, `hot-key-${this.tableId}-${attrcode}-${index}`)
							}
						}}
						onEnter = {(e)=> {
							onEnter(e.e.currentTarget, this.ViewModel)
						}}
						tabindex="0" 
					/>
				</div>
			)
		} else {
			return (
				<div className='number-text' fieldid={attrcode}>
					{text.display ? commafy(removeThousands(text.display)): emptyCell}
				</div>
			)

		}
	}
	render() {
		let assisData = cacheUtils.getAssisData();
		let { buSecond } = assisData.key
		let { isStartBUSecond, isInitBuild, currtype } = assisData.flag
		// 1.未启用二级账簿可以编辑；2.启用二级账簿且只选一个二级账簿时，辅助核算可以编辑
		let isEditAssid = isStartBUSecond ? (isStartBUSecond && buSecond.length === 1 ? true : false) : true
		let columns1 = [
			{
				title: (<div fieldid='xuhao' className='mergecells'>{getMultiLangByID('20020401-000013')}</div>),
				dataIndex: "xuhao", key: "xuhao", width: '80px', fixed: "left", /* 国际化处理： 序号*/
				render: (text, record, index) => (
					<div className='text_c' fieldid='xuhao'>{text ? text : emptyCell}</div>
				)
			},
			{
				title: (<div fieldid='assid' className='mergecells'>{getMultiLangByID('20020401-000014')}</div>),
				dataIndex: "assid", key: "assid", width: '390px', fixed: "left",/* 国际化处理： 辅助核算*/
				render: (text, record, index) => {
					let name = text && text.display ? text.display : getMultiLangByID('20020401-000015')/* 国际化处理： 待选择*/
					let tip = (<div>{name}</div>)

					if (isEditAssid && !isInitBuild && currtype !== getMultiLangByID('20020401-000000')) {
						return (
							<Tooltip trigger="hover" placement="top" inverse={true} overlay={tip}>
								<div fieldid='assid'>
									<a
										className='assid-btn'
										// disabled = {!(isEditAssid && !isInitBuild)}
										onClick={this.showAssidModal.bind(this, index, text, record)}
										shape="border" colors="info"
									>
										{name}
									</a>
								</div>
							</Tooltip>
						)
					} else {
						return (
							<div className='' fieldid='assid'> {name ? name : emptyCell} </div>
						)
					}
				}
			},

		];

		let columns2;
		let flag = this.state.info.flag;
		let recordUnit = this.state.info.newInitVOs.unit;
		let info = {
			NC001: this.state.info.key.NC001,
			NC002: this.state.info.key.NC002,
		}
		let { namesAll } = this.state.info

		if (flag.isYearStart) {
			//1.全局、集团、组织本币
			//2.全局、集团、非组织本币
			//3.全局、非集团、组织本币
			//4.全局、非集团、非组织本币
			//5.非全局、集团、组织本币
			//6.非全局、集团、非组织本币
			//7.非全局、非集团、组织本币
			//8.非全局、非集团、非组织本币

			// 数量对象
			let shuliangObj = {
				title: (<div fieldid='endquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "endquantity",
				key: "endquantity",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endquantity")
				)

			};

			//数量对象可编辑
			let shuliangObjEdit = {
				title: (<div fieldid='endquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "endquantity",
				key: "endquantity",
				width: '160px',
				render: (text, record, index) => {

					if (recordUnit && recordUnit.value) {


						return (
							this.renderNCNumber(text, "endquantity", index, getMultiLangByID('20020401-000016'), true)/* 国际化处理： 数量*/
						)

					} else {
						return (
							this.renderNCNumber(text, "endquantity")
						)
					}


				}
			}

			// 原币对象
			let yuanbiObj = {
				title: (<div fieldid='endamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "endamount",
				key: "endamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endamount")

				)
			};

			// 原币对象可编辑
			let yuanbiObjEdit = {
				title: (<div fieldid='endamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "endamount",
				key: "endamount",
				width: '160px',
				render: (text, record, index) => {

					return (
						this.renderNCNumber(text, "endamount", index, getMultiLangByID('20020401-000017'), true)/* 国际化处理： 原币*/
					)
				}
			};

			// 组织本币对象
			let zuzhiObj = {
				title: (<div fieldid='endlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "endlocalamount",
				key: "endlocalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endlocalamount")
				)
			};

			// 组织本币对象可编辑
			let zuzhiObjEdit = {
				title: (<div fieldid='endlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "endlocalamount",
				key: "endlocalamount",
				width: '160px',
				render: (text, record, index) => {



					// 所选币种与辅助核算带的组织本币币种相同，则不可编辑
					if (flag.currtype == flag.localCurrType) {
						return (
							this.renderNCNumber(text, "endlocalamount")
						)
					} else {

						return (
							this.renderNCNumber(text, "endlocalamount", index, getMultiLangByID('20020401-000000'), true)/* 国际化处理： 组织本币*/
						)
					}

				}
			};

			//全局本币对象
			let quanjuObj = {
				title: (<div fieldid='endglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "endglobalamount",
				key: "endglobalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endglobalamount")
				)
			}


			//全局本币对象可编辑
			let quanjuObjEdit = {
				title: (<div fieldid='endglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "endglobalamount",
				key: "endglobalamount",
				width: '160px',
				render: (text, record, index) => {

					if (info.NC002 == 'raw_convert') {
						if (flag.currtype == flag.globalCurrType) {
							return (
								this.renderNCNumber(text, "endglobalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "endglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/

							)
						}
					}

					if (info.NC002 == 'local_convert') {
						if (flag.localCurrType == flag.globalCurrType) {
							return (
								this.renderNCNumber(text, "endglobalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "endglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/

							)
						}
					}
				}
			}

			//集团本币对象
			let jituanObj = {
				title: (<div fieldid='endgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "endgroupamount",
				key: "endgroupamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endgroupamount")
				)
			}

			//集团本币对象可编辑
			let jituanObjEdit = {
				title: (<div fieldid='endgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "endgroupamount",
				key: "endgroupamount",
				width: '160px',
				render: (text, record, index) => {

					if (info.NC001 == 'raw_convert') {
						if (flag.currtype == flag.groupCurrType) {
							return (
								this.renderNCNumber(text, "endgroupamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "endgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
							)
						}
					}

					if (info.NC001 == 'local_convert') {
						if (flag.localCurrType == flag.groupCurrType) {
							return (
								this.renderNCNumber(text, "endgroupamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "endgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
							)
						}
					}


				}
			}


			//1.全局、集团、组织本币 
			if (flag.isGlobal && flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				//大条件已经是组织本币了，所以都不可编辑

				columns2 = [
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [shuliangObj, zuzhiObj, jituanObj, quanjuObj],
					},
				];
			}

			//2.全局、集团、非组织本币
			if (flag.isGlobal && flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditAssid && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObjEdit, yuanbiObjEdit, zuzhiObjEdit, jituanObjEdit, quanjuObjEdit],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObj, yuanbiObj, zuzhiObj, jituanObj, quanjuObj],
						},
					];
				}
			}

			//3.全局、非集团、组织本币
			if (flag.isGlobal && !flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [shuliangObj, zuzhiObj, quanjuObj],
					},
				];

			}

			//4.全局、非集团、非组织本币

			if (flag.isGlobal && !flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditAssid && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObjEdit, yuanbiObjEdit, zuzhiObjEdit, quanjuObjEdit],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObj, yuanbiObj, zuzhiObj, quanjuObj],
						},
					];
				}
			}

			//5.非全局、集团、组织本币
			if (!flag.isGlobal && flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [shuliangObj, zuzhiObj, jituanObj,],
					},
				];

			}

			//6.非全局、集团、非组织本币
			if (!flag.isGlobal && flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditAssid && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObjEdit, yuanbiObjEdit, zuzhiObjEdit, jituanObjEdit,],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObj, yuanbiObj, zuzhiObj, jituanObj,],
						},
					];
				}

			}

			//7.非全局、非集团、组织本币
			if (!flag.isGlobal && !flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [shuliangObj, zuzhiObj,],
					},
				];
			}


			//8.非全局、非集团、非组织本币
			if (!flag.isGlobal && !flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditAssid && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObjEdit, yuanbiObjEdit, zuzhiObjEdit,],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [shuliangObj, yuanbiObj, zuzhiObj,],
						},
					];
				}
			}


		} else {
			//1.全局、集团、组织本币
			//2.全局、集团、非组织本币
			//3.全局、非集团、组织本币
			//4.全局、非集团、非组织本币
			//5.非全局、集团、组织本币
			//6.非全局、集团、非组织本币
			//7.非全局、非集团、组织本币
			//8.非全局、非集团、非组织本币

			// 年初数量对象
			let nianchushuliangObj = {
				title: (<div fieldid='yearquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "yearquantity",
				key: "yearquantity",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "yearquantity")
				)
			}

			// 年初数量对象可编辑
			let nianchushuliangObjEdit = {
				title: (<div fieldid='yearquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "yearquantity",
				key: "yearquantity",
				width: '160px',
				render: (text, record, index) => {

					if (recordUnit && recordUnit.value) {

						return (
							this.renderNCNumber(text, "yearquantity", index, getMultiLangByID('20020401-000016'), true)/* 国际化处理： 数量*/
						)

					} else {
						return (
							this.renderNCNumber(text, "yearquantity")
						)
					}

				}

			}

			// 年初原币对象
			let nianchuyuanbiObj = {
				title: (<div fieldid='yearamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "yearamount",
				key: "yearamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "yearamount")
				)
			}


			// 年初原币对象可编辑
			let nianchuyuanbiObjEdit = {
				title: (<div fieldid='yearamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "yearamount",
				key: "yearamount",
				width: '160px',
				render: (text, record, index) => {

					return (
						this.renderNCNumber(text, "yearamount", index, getMultiLangByID('20020401-000017'), true)/* 国际化处理： 原币*/
					)
				}
			}

			// 年初组织本币对象
			let nianchuzuzhiObj = {
				title: (<div fieldid='yearlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "yearlocalamount",
				key: "yearlocalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "yearlocalamount")
				)
			};

			// 年初组织本币对象可编辑
			let nianchuzuzhiObjEdit = {
				title: (<div fieldid='yearlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "yearlocalamount",
				key: "yearlocalamount",
				width: '160px',
				render: (text, record, index) => {


					// 所选币种与辅助核算带的组织本币币种相同，则不可编辑
					if (flag.currtype == flag.localCurrType) {
						return (
							this.renderNCNumber(text, "yearlocalamount")
						)
					} else {
						return (
							this.renderNCNumber(text, "yearlocalamount", index, getMultiLangByID('20020401-000000'), true)/* 国际化处理： 组织本币*/
						)
					}

				}

			};

			// 年初全局本币对象
			let nianchuquanjuObj = {
				title: (<div fieldid='yearglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "yearglobalamount",
				key: "yearglobalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "yearglobalamount")
				)
			}

			// 年初全局本币对象可编辑
			let nianchuquanjuObjEdit = {
				title: (<div fieldid='yearglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "yearglobalamount",
				key: "yearglobalamount",
				width: '160px',
				render: (text, record, index) => {
					// 所选币种与辅助核算带的组织本币币种相同，则不可编辑
					if (flag.currtype == flag.globalCurrType) {
						return (
							this.renderNCNumber(text, "yearglobalamount")
						)
					} else {
						return (
							this.renderNCNumber(text, "yearglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
						)
					}
				}

			}

			// 年初集团本币对象
			let nianchujituanObj = {
				title: (<div fieldid='yeargroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "yeargroupamount",
				key: "yeargroupamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "yeargroupamount")
				)
			}

			// 年初集团本币对象可编辑
			let nianchujituanObjEdit = {
				title: (<div fieldid='yeargroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "yeargroupamount",
				key: "yeargroupamount",
				width: '160px',
				render: (text, record, index) => {

					// 所选币种与辅助核算带的组织本币币种相同，则不可编辑
					if (flag.currtype == flag.groupCurrType) {
						return (
							this.renderNCNumber(text, "yeargroupamount")
						)
					} else {
						return (
							this.renderNCNumber(text, "yeargroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
						)
					}

				}
			}

			// 期初数量对象
			let qichushuliangObj = {
				title: (<div fieldid='endquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "endquantity",
				key: "endquantity",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endquantity")
				)

			};

			// 期初数量对象可编辑
			let qichushuliangObjEdit = {
				title: (<div fieldid='endquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "endquantity",
				key: "endquantity",
				width: '160px',
				render: (text, record, index) => {

					if (recordUnit && recordUnit.value) {

						return (
							this.renderNCNumber(text, "endquantity", index, getMultiLangByID('20020401-000016'), true)/* 国际化处理： 数量*/
						)

					} else {
						return (
							this.renderNCNumber(text, "endquantity")
						)
					}

				}

			};

			// 期初原币对象
			let qichuyuanbiObj = {
				title: (<div fieldid='endamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "endamount",
				key: "endamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endamount")
				)
			};

			// 期初原币对象可编辑
			let qichuyuanbiObjEdit = {
				title: (<div fieldid='endamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "endamount",
				key: "endamount",
				width: '160px',
				render: (text, record, index) => {

					return (
						this.renderNCNumber(text, "endamount", index, getMultiLangByID('20020401-000017'), true)/* 国际化处理： 原币*/
					)
				}
			};

			// 期初组织本币对象
			let qichuzuzhiObj = {
				title: (<div fieldid='endlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "endlocalamount",
				key: "endlocalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endlocalamount")
				)
			};

			// 期初组织本币对象可编辑
			let qichuzuzhiObjEdit = {
				title: (<div fieldid='endlocalamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "endlocalamount",
				key: "endlocalamount",
				width: '160px',
				render: (text, record, index) => {

					// 所选币种与辅助核算带的组织本币币种相同，则不可编辑
					if (flag.currtype == flag.localCurrType) {
						return (
							this.renderNCNumber(text, "endlocalamount")
						)
					} else {
						return (
							this.renderNCNumber(text, "endlocalamount", index, getMultiLangByID('20020401-000000'), true)/* 国际化处理： 组织本币*/
						)
					}

				}

			};

			// 期初全局本币对象
			let qichuquanjuObj = {
				title: (<div fieldid='endglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "endglobalamount",
				key: "endglobalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endglobalamount")
				)
			}

			// 期初全局本币对象可编辑
			let qichuquanjuObjEdit = {
				title: (<div fieldid='endglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "endglobalamount",
				key: "endglobalamount",
				width: '160px',
				render: (text, record, index) => {

					if (info.NC002 == 'raw_convert') {
						if (flag.currtype == flag.globalCurrType) {
							return (
								this.renderNCNumber(text, "endglobalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "endglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
							)
						}
					}

					if (info.NC002 == 'local_convert') {
						if (flag.localCurrType == flag.globalCurrType) {
							return (
								this.renderNCNumber(text, "endglobalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "endglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
							)
						}
					}

				}
			}

			// 期初集团本币对象
			let qichujituanObj = {
				title: (<div fieldid='endgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "endgroupamount",
				key: "endgroupamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "endgroupamount")
				)
			}

			// 期初集团本币对象可编辑
			let qichujituanObjEdit = {
				title: (<div fieldid='endgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "endgroupamount",
				key: "endgroupamount",
				width: '160px',
				render: (text, record, index) => {

					if (info.NC001 == 'raw_convert') {
						if (flag.currtype == flag.groupCurrType) {
							return (
								this.renderNCNumber(text, "endgroupamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "endgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
							)
						}
					}

					if (info.NC001 == 'local_convert') {
						if (flag.localCurrType == flag.groupCurrType) {
							return (
								this.renderNCNumber(text, "endgroupamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "endgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
							)
						}
					}

				}

			}


			// 借方数量对象
			let jiefangshuliangObj = {
				title: (<div fieldid='debitquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "debitquantity",
				key: "debitquantity",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "debitquantity")
				)
			}

			// 借方数量对象可编辑
			let jiefangshuliangObjEdit = {
				title: (<div fieldid='debitquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "debitquantity",
				key: "debitquantity",
				width: '160px',
				render: (text, record, index) => {

					if (recordUnit && recordUnit.value) {

						return (
							this.renderNCNumber(text, "debitquantity", index, getMultiLangByID('20020401-000016'), true)/* 国际化处理： 数量*/
						)

					} else {
						return (
							this.renderNCNumber(text, "debitquantity")
						)
					}

				}

			}

			// 借方原币对象
			let jiefangyuanbiObj = {
				title: (<div fieldid='debitamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "debitamount",
				key: "debitamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "debitamount")
				)
			}

			// 借方原币对象可编辑
			let jiefangyuanbiObjEdit = {
				title: (<div fieldid='debitamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "debitamount",
				key: "debitamount",
				width: '160px',
				render: (text, record, index) => {
					return (
						this.renderNCNumber(text, "debitamount", index, getMultiLangByID('20020401-000017'), true)/* 国际化处理： 原币*/
					)
				}

			}

			// 借方组织本币对象
			let jiefangzuzhiObj = {
				title: (<div fieldid='localdebitamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "localdebitamount",
				key: "localdebitamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "localdebitamount")
				)
			};

			// 借方组织本币对象可编辑
			let jiefangzuzhiObjEdit = {
				title: (<div fieldid='localdebitamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "localdebitamount",
				key: "localdebitamount",
				width: '160px',
				render: (text, record, index) => {

					// 所选币种与辅助核算带的组织本币币种相同，则不可编辑
					if (flag.currtype == flag.localCurrType) {
						return (
							this.renderNCNumber(text, "localdebitamount")
						)
					} else {

						return (
							this.renderNCNumber(text, "localdebitamount", index, getMultiLangByID('20020401-000000'), true)/* 国际化处理： 组织本币*/
						)
					}

				}

			};

			// 借方全局本币对象
			let jiefangquanjuObj = {
				title: (<div fieldid='debitglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "debitglobalamount",
				key: "debitglobalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "debitglobalamount")
				)
			};

			// 借方全局本币对象可编辑
			let jiefangquanjuObjEdit = {
				title: (<div fieldid='debitglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "debitglobalamount",
				key: "debitglobalamount",
				width: '160px',
				render: (text, record, index) => {

					if (info.NC002 == 'raw_convert') {
						if (flag.currtype == flag.globalCurrType) {
							return (
								this.renderNCNumber(text, "debitglobalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "debitglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
							)
						}
					}

					if (info.NC002 == 'local_convert') {
						if (flag.localCurrType == flag.globalCurrType) {
							return (
								this.renderNCNumber(text, "debitglobalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "debitglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
							)
						}
					}
				}
			};

			// 借方集团本币对象
			let jiefangjituanObj = {
				title: (<div fieldid='debitgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "debitgroupamount",
				key: "debitgroupamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "debitgroupamount")
				)
			};

			// 借方集团本币对象可编辑
			let jiefangjituanObjEdit = {
				title: (<div fieldid='debitgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "debitgroupamount",
				key: "debitgroupamount",
				width: '160px',
				render: (text, record, index) => {

					if (info.NC001 == 'raw_convert') {
						if (flag.currtype == flag.groupCurrType) {
							return (
								this.renderNCNumber(text, "debitgroupamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "debitgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
							)
						}
					}

					if (info.NC001 == 'local_convert') {
						if (flag.localCurrType == flag.groupCurrType) {
							return (
								this.renderNCNumber(text, "debitgroupamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "debitgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
							)
						}
					}
				}
			};


			// 贷方数量对象
			let daifangshuliangObj = {
				title: (<div fieldid='creditquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "creditquantity",
				key: "creditquantity",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "creditquantity")
				)

			};

			// 贷方数量对象可编辑
			let daifangshuliangObjEdit = {
				title: (<div fieldid='creditquantity'>{getMultiLangByID('20020401-000016')}</div>),/* 国际化处理： 数量*/
				dataIndex: "creditquantity",
				key: "creditquantity",
				width: '160px',
				render: (text, record, index) => {
					if (recordUnit && recordUnit.value) {
						return (
							this.renderNCNumber(text, "creditquantity", index, getMultiLangByID('20020401-000016'), true)/* 国际化处理： 数量*/
						)

					} else {
						return (
							this.renderNCNumber(text, "creditquantity")
						)
					}
				}

			};

			// 贷方原币对象
			let daifangyuanbiObj = {
				title: (<div fieldid='creditamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "creditamount",
				key: "creditamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "creditamount")
				)
			};

			// 贷方原币对象可编辑
			let daifangyuanbiObjEdit = {
				title: (<div fieldid='creditamount'>{getMultiLangByID('20020401-000017')}</div>),/* 国际化处理： 原币*/
				dataIndex: "creditamount",
				key: "creditamount",
				width: '160px',
				render: (text, record, index) => {

					return (
						this.renderNCNumber(text, "creditamount", index, getMultiLangByID('20020401-000017'), true)/* 国际化处理： 原币*/
					)
				}
			};

			// 贷方组织本币对象
			let daifangzuzhiObj = {
				title: (<div fieldid='localcreditamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "localcreditamount",
				key: "localcreditamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "localcreditamount")
				)
			};

			// 贷方组织本币对象可编辑
			let daifangzuzhiObjEdit = {
				title: (<div fieldid='localcreditamount'>{getMultiLangByID('20020401-000000')}</div>),/* 国际化处理： 组织本币*/
				dataIndex: "localcreditamount",
				key: "localcreditamount",
				width: '160px',
				render: (text, record, index) => {

					// 所选币种与辅助核算带的组织本币币种相同，则不可编辑
					if (flag.currtype == flag.localCurrType) {
						return (
							this.renderNCNumber(text, "localcreditamount")
						)
					} else {

						return (
							this.renderNCNumber(text, "localcreditamount", index, getMultiLangByID('20020401-000000'), true)/* 国际化处理： 组织本币*/
						)
					}

				}
			};

			// 贷方全局本币对象
			let daifangquanjuObj = {
				title: (<div fieldid='creditglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "creditglobalamount",
				key: "creditglobalamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "creditglobalamount")
				)
			}

			// 贷方全局本币对象可编辑
			let daifangquanjuObjEdit = {
				title: (<div fieldid='creditglobalamount'>{getMultiLangByID('20020401-000002')}</div>),/* 国际化处理： 全局本币*/
				dataIndex: "creditglobalamount",
				key: "creditglobalamount",
				width: '160px',
				render: (text, record, index) => {

					if (info.NC002 == 'raw_convert') {
						if (flag.currtype == flag.globalCurrType) {
							return (
								this.renderNCNumber(text, "creditglobalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "creditglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
							)
						}
					}

					if (info.NC002 == 'local_convert') {
						if (flag.localCurrType == flag.globalCurrType) {
							return (
								this.renderNCNumber(text, "creditglobalamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "creditglobalamount", index, getMultiLangByID('20020401-000002'), true)/* 国际化处理： 全局本币*/
							)
						}
					}

				}
			}

			// 贷方集团本币对象
			let daifangjituanObj = {
				title: (<div fieldid='creditgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "creditgroupamount",
				key: "creditgroupamount",
				width: '160px',
				render: (text, record, index) => (
					this.renderNCNumber(text, "creditgroupamount")
				)
			}

			// 贷方集团本币对象可编辑
			let daifangjituanObjEdit = {
				title: (<div fieldid='creditgroupamount'>{getMultiLangByID('20020401-000001')}</div>),/* 国际化处理： 集团本币*/
				dataIndex: "creditgroupamount",
				key: "creditgroupamount",
				width: '160px',
				render: (text, record, index) => {
					if (info.NC001 == 'raw_convert') {
						if (flag.currtype == flag.groupCurrType) {
							return (
								this.renderNCNumber(text, "creditgroupamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "creditgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
							)
						}
					}

					if (info.NC001 == 'local_convert') {
						if (flag.localCurrType == flag.groupCurrType) {
							return (
								this.renderNCNumber(text, "creditgroupamount")
							)
						} else {
							return (
								this.renderNCNumber(text, "creditgroupamount", index, getMultiLangByID('20020401-000001'), true)/* 国际化处理： 集团本币*/
							)
						}
					}

				}

			}

			//1.全局、集团、组织本币
			if (flag.isGlobal && flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/

				columns2 = [
					{
						title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
						children: [jiefangshuliangObj, jiefangzuzhiObj, jiefangjituanObj, jiefangquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
						children: [daifangshuliangObj, daifangzuzhiObj, daifangjituanObj, daifangquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [qichushuliangObj, qichuzuzhiObj, qichujituanObj, qichuquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
						children: [nianchushuliangObj, nianchuzuzhiObj, nianchujituanObj, nianchuquanjuObj],
					},
				];
			}

			//2.全局、集团、非组织本币
			if (flag.isGlobal && flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditAssid && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObjEdit, jiefangyuanbiObjEdit, jiefangzuzhiObjEdit, jiefangjituanObjEdit, jiefangquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObjEdit, daifangyuanbiObjEdit, daifangzuzhiObjEdit, daifangjituanObjEdit, daifangquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObjEdit, qichuyuanbiObjEdit, qichuzuzhiObjEdit, qichujituanObjEdit, qichuquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchujituanObj, nianchuquanjuObj],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObj, jiefangyuanbiObj, jiefangzuzhiObj, jiefangjituanObj, jiefangquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObj, daifangyuanbiObj, daifangzuzhiObj, daifangjituanObj, daifangquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObj, qichuyuanbiObj, qichuzuzhiObj, qichujituanObj, qichuquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchujituanObj, nianchuquanjuObj],
						},
					];
				}
			}

			//3.全局、非集团、组织本币
			if (flag.isGlobal && !flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
						children: [jiefangshuliangObj, jiefangzuzhiObj, jiefangquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
						children: [daifangshuliangObj, daifangzuzhiObj, daifangquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [qichushuliangObj, qichuzuzhiObj, qichuquanjuObj],
					},
					{
						title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
						children: [nianchushuliangObj, nianchuzuzhiObj, nianchuquanjuObj],
					},
				];

			}

			//4.全局、非集团、非组织本币

			if (flag.isGlobal && !flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditAssid && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObjEdit, jiefangyuanbiObjEdit, jiefangzuzhiObjEdit, jiefangquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObjEdit, daifangyuanbiObjEdit, daifangzuzhiObjEdit, daifangquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObjEdit, qichuyuanbiObjEdit, qichuzuzhiObjEdit, qichuquanjuObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchuquanjuObj],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObj, jiefangyuanbiObj, jiefangzuzhiObj, jiefangquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObj, daifangyuanbiObj, daifangzuzhiObj, daifangquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObj, qichuyuanbiObj, qichuzuzhiObj, qichuquanjuObj],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchuquanjuObj],
						},
					];
				}
			}

			//5.非全局、集团、组织本币
			if (!flag.isGlobal && flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
						children: [jiefangshuliangObj, jiefangzuzhiObj, jiefangjituanObj,],
					},
					{
						title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
						children: [daifangshuliangObj, daifangzuzhiObj, daifangjituanObj,],
					},
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [qichushuliangObj, qichuzuzhiObj, qichujituanObj,],
					},
					{
						title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
						children: [nianchushuliangObj, nianchuzuzhiObj, nianchujituanObj,],
					},

				];

			}

			//6.非全局、集团、非组织本币
			if (!flag.isGlobal && flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditAssid && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObjEdit, jiefangyuanbiObjEdit, jiefangzuzhiObjEdit, jiefangjituanObjEdit,],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObjEdit, daifangyuanbiObjEdit, daifangzuzhiObjEdit, daifangjituanObjEdit,],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObjEdit, qichuyuanbiObjEdit, qichuzuzhiObjEdit, qichujituanObjEdit,],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchujituanObj,],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObj, jiefangyuanbiObj, jiefangzuzhiObj, jiefangjituanObj,],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObj, daifangyuanbiObj, daifangzuzhiObj, daifangjituanObj,],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObj, qichuyuanbiObj, qichuzuzhiObj, qichujituanObj,],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj, nianchujituanObj,],
						},
					];
				}
			}


			//7.非全局、非集团、组织本币
			if (!flag.isGlobal && !flag.isGroup && (flag.currtype == getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				columns2 = [
					{
						title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
						children: [jiefangshuliangObj, jiefangzuzhiObj,],
					},
					{
						title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
						children: [daifangshuliangObj, daifangzuzhiObj,],
					},
					{
						title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
						children: [qichushuliangObj, qichuzuzhiObj,],
					},
					{
						title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
						children: [nianchushuliangObj, nianchuzuzhiObj,],
					},
				];
			}


			// 8.非全局、非集团、非组织本币
			if (!flag.isGlobal && !flag.isGroup && (flag.currtype != getMultiLangByID('20020401-000000'))) {/* 国际化处理： 组织本币*/
				if (isEditAssid && !flag.isInitBuild) {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObjEdit, jiefangyuanbiObjEdit, jiefangzuzhiObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObjEdit, daifangyuanbiObjEdit, daifangzuzhiObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObjEdit, qichuyuanbiObjEdit, qichuzuzhiObjEdit],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj],
						},
					];
				} else {
					columns2 = [
						{
							title: getMultiLangByID('20020401-000019'), /* 国际化处理： 借方累计*/
							children: [jiefangshuliangObj, jiefangyuanbiObj, jiefangzuzhiObj],
						},
						{
							title: getMultiLangByID('20020401-000020'), /* 国际化处理： 贷方累计*/
							children: [daifangshuliangObj, daifangyuanbiObj, daifangzuzhiObj],
						},
						{
							title: getMultiLangByID('20020401-000018'), /* 国际化处理： 期初余额*/
							children: [qichushuliangObj, qichuyuanbiObj, qichuzuzhiObj],
						},
						{
							title: getMultiLangByID('20020401-000021'), /* 国际化处理： 年初余额*/
							children: [nianchushuliangObj, nianchuyuanbiObj, nianchuzuzhiObj],
						},
					];
				}
			}

		}




		let columns3 = columns1.concat(columns2);


		let data3 = this.state.mydata.assInitVOs || [];
		// data3 = deepClone(data3);
		let columns4 = this.renderColumnsMultiSelect(columns3);

		// 添加空白列
		columns4.push({ title: '', key: '', width: 1 })

		let Width = window.innerWidth - 152
		// let Height = window.innerHeight - 334 //有footbtn
		let Height = window.innerHeight //无footbtn
		let minHeight = Height - 202
		let tableHeight = getTableHeight(280)
		return (
			<div className="nc-bill-list accountAssistant">
				<div className='btn-area'>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'page_header',
							onButtonClick: onButtonClick.bind(this),
							popContainer: document.querySelector('.header-button-area'),
							modalRelation: 'AccountAssistantModal',
							withAutoFocus: false
						})}
					</div>
				</div>


				<NCDiv areaCode={NCDiv.config.SEARCH}>
					<div className="search_params">
						<div className="search_params_des" fieldid='account'>
							<div className='search_params_des_lable'>
								{getMultiLangByID('20020401-000023')}
							</div>：{/* 国际化处理： 财务核算账簿*/}
							<NCTooltip trigger="hover" placement="top" inverse={true} overlay={<div>{namesAll.book || ''}</div>}>
								<span className='nc-theme-title-font-c'>{namesAll.book || ''}</span>
							</NCTooltip>
						</div>

						{isStartBUSecond ? <div className="search_params_des">
							<div className='search_params_des_lable' fieldid='unit'>
								{getMultiLangByID('20020401-000024')}
							</div>：{/* 国际化处理： 业务单元*/}
							<NCTooltip trigger="hover" placement="top" inverse={true} overlay={<div>{namesAll.unit || ''}</div>}>
								<span className='nc-theme-title-font-c'>{namesAll.unit || ''}</span>
							</NCTooltip>
						</div> : <div />}

						<div className="search_params_des" fieldid='yearPeriod'>
							{getMultiLangByID('20020401-000025')}：{/* 国际化处理： 会计期间*/}
							<NCTooltip trigger="hover" placement="top" inverse={true} overlay={<div>{namesAll.yearPeriod}</div>}>
								<span className='nc-theme-title-font-c'>{namesAll.yearPeriod}</span>
							</NCTooltip>
						</div>


						<div className="search_params_des" fieldid='kemu'>
							{getMultiLangByID('20020401-000026')}：						{/* 国际化处理： 会计科目*/}
							<NCTooltip trigger="hover" placement="top" inverse={true} overlay={<div>{`${namesAll.kemucode}-${namesAll.kemuname}`}</div>}>
								<span className='nc-theme-title-font-c'>{`${namesAll.kemucode}-${namesAll.kemuname}`}</span>
							</NCTooltip>
						</div>

						<div className="search_params_des" fieldid='direction'>
							{getMultiLangByID('20020401-000027')}：{/* 国际化处理： 方向*/}
							<NCTooltip trigger="hover" placement="top" inverse={true} overlay={<div>{namesAll.fangxiang}</div>}>
								<span className='nc-theme-title-font-c'>{namesAll.fangxiang}</span>
							</NCTooltip>
						</div>

						<div className="search_params_des" fieldid='currtype'>
							{getMultiLangByID('20020401-000028')}： {/* 国际化处理： 币种*/}
							<NCTooltip trigger="hover" placement="top" inverse={true} overlay={<div>{namesAll.currtypeName}</div>}>
								<span className='nc-theme-title-font-c'>{namesAll.currtypeName}</span>
							</NCTooltip>
						</div>
					</div>
				</NCDiv>
				<NCDiv fieldid={this.tableId} areaCode={NCDiv.config.TableCom}>
					<Table
						// height={40} //固定行目前有错行问题
						bordered  //出现一个横向不可滚动的滚动条时会出现错行，bbq建议先去掉
						headerHeight={26}
						heightConsistent={true} //修改折行错行问题
						columns={columns4}
						data={data3}
						rowKey={(record, index) => record.assid && record.assid.value || record.key}
						// className="main-table"
						bodyStyle={{height: tableHeight}}
						scroll={{ x: Width, y: tableHeight }}
						// adaptionHeight={true}
					/>
				</NCDiv>

				<AssidModal
					hotKeyboard = {true} //快捷键
					ViewModel={this.ViewModel}
					pretentAssData={this.state.assidCondition}
					showOrHide={this.state.showAssidModal}
					onConfirm={this.handleAssidOK.bind(this)}
					showDisableData={false}
					handleClose={() => {
						this.setState({
							showAssidModal: false
						});
					}}
				/>
				<ImportFile
					tmpletType={this.state.tmpletType}
					impData={this.getImportData()}
					visible={this.state.showImportModal}
					assExport={true}
					handleRefresh={this.refresh.bind(this)}
					handleCancel={() => {
						this.setState({
							showImportModal: false
						})
					}}
				/>
			</div>
		)
	}
}

AccountAssistant = createPage({})(AccountAssistant)
export default AccountAssistant
