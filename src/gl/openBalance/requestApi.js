import {ajax} from 'nc-lightapp-front'
import {actions} from './consts'

const requestApi = {
    /**
     * 清空期初余额数据
     * data：{pk_accountingbook, pk_unit, year}
     * 没有开启二级核算的账簿可不传pk_unit
     */
    clearInitData:(data, callback) =>{
        let url = actions.clearInitData;
        ajax({
            url, data,
            success:(res) => {
                if(callback){ callback(res); }
            }
        });
    }
}

export {requestApi}
