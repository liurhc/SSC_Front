import {asyncComponent} from 'nc-lightapp-front';
import OpenBalance from '../openBalance';

//辅助核算
const AccountAssistant = asyncComponent(() => import(/* webpackChunkName: "gl/openBalance/pages/accountAssistant" */ /* webpackMode: "eager" */ '../accountAssistant'));

//试算平衡
const TrialBalance = asyncComponent(() => import(/* webpackChunkName: "gl/openBalance/pages/trialBalance" */ /* webpackMode: "eager" */ '../trialBalance'));

//期初建账步骤一
const InitialAccountFirst = asyncComponent(() => import(/* webpackChunkName: "gl/openBalance/pages/initialAccountFirst" */ /* webpackMode: "eager" */ '../initialAccountFirst'));


const routes = [
    {
      path: '/',
      component: OpenBalance,
      exact: true
    },
    {
      path: '/accountAssistant',
      component: AccountAssistant
    },
    {
      path: '/trialBalance',
      component: TrialBalance
    },
    {
      path: '/initialAccountFirst',
      component: InitialAccountFirst
    }
  ];
  
  export default routes;
