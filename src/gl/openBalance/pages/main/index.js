import {RenderRouter, initMultiLangByModule} from 'nc-lightapp-front';
import routes from './router';

// (function main(routers,htmlTagid){
//   RenderRouter(routers,htmlTagid);
// })(routes,"app");

(function main(routers, htmlTagid) {
	let moduleIds = { gl: [ '20020401', 'publiccommon' ] };
	initMultiLangByModule(moduleIds, () => {
		RenderRouter(routers, htmlTagid);
	});
})(routes, 'app');