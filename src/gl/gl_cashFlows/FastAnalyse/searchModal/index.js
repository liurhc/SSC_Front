import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,createPage,getBusinessInfo,getMultiLang,toast} from 'nc-lightapp-front';
import createScript from '../../../public/components/uapRefer.js';
const { NCModal,NCButton,NCRadio,NCRow:Row,NCCol:Col,NCDatePicker,NCCheckbox,NCTable:Table,NCNumber,NCSelect,NCDiv} = base;
import AccperiodMonthTreeGridRef from '../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import VoucherTypeDefaultGridRef from '../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef';
import AccountBookTreeRef from '../../pk_book/refer_pk_book';
import './index.less';
import {InputItem,} from '../../../public/components/FormItems';
import ReferLoader from '../../../public/ReferLoader/index.js' 
import OperatorDefaultRef from '../../../refer/voucher/OperatorDefaultRef/index';
import BusinessUnitWithGlobleAndCurrGroupTreeRef from '../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef'
const NCOption = NCSelect.NCOption;
class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            pk_org:'',//财务组织，参照用
            tableData:[],
            checkedArray:[],//选中数组
            showModal: false,
            configs: {},//人员参照
            selectArr:[],//辅助核算选中项
            selIds:[],//多选需要
            pk_unitState:false,
            pk_unit:[],
            pk_accountingbook:{
                refcode:"",
                refname:"",
                refpk:""
            },
            period_pk:{
                refname:"",
                refpk:"",
                pk_accperiodscheme:''
            },
            pk_vouchertype:{
                refcode:"",
                refname:"",
                refpk:""
            },
            pk_prepared:{
                refcode:"",
                refname:"",
                refpk:""
            },
            accountcode:[],
            oppaccountcode:[],
            bizDate:'',//业务日期
            pk_currtype:{
                refcode:"",
                refname:"",
                refpk:""
            },
            pk_currtypes:[],//币种数据
            searchData:{
                pk_accountingbook:'',  //核算账簿
                pk_unit:[], // 业务单元  参照多选 数组
                querybyperiod:'Y', //是否按期间查询  Y/N
                year:'',
                period:'',
                begindate:'',
                enddate:'',
                analyze:'2',  // 0未指定  1已指定 2 全部
                includeuntally:'Y', //包含未记账  Y/N
                includeuncash:'Y', //包含非现金凭证  Y/N
                pk_vouchertype:'',//凭证类别
                startvoucherno:'', //开始凭证号
                endvoucherno:'',  //结束凭证号
                pk_prepared:'',//制单人
                accountcode:'',// 科目编码   注意是编码不是主键  数组
                oppaccountcode:'', // 对方科目编码  注意是编码不是主键  数组
                pk_unitState:false,//启用业务单元
                pk_unitSearch:false,//启用业务单元
                NC001:true,//启用全局本币(false时)
                NC002:true,//启用全局子表
                // assvo :[{pk_checktype,pk_checkvalue},{pk_checktype,pk_checkvalue}],  //辅助核算
                //下面条件是快速分析多的2个参数
                returncurr:'1', //返回币种  1 组织本币，2集团本币， 3全局本币
                pk_currtype:'', //币种主键
            },
        };
        this.businessInfo='';
        this.close = this.close.bind(this);
        this.columns = [];
    }
    componentWillMount(){
        let url = '/nccloud/platform/appregister/queryappcontext.do';
        let data={
            appcode: this.props.getSearchParam('c')
        }
        let self=this;
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                   let pk_accountingbook={
                        refname:data.defaultAccbookName,
                        refpk:data.defaultAccbookPk
                    }
                   self.setState({
                    pk_accountingbook
                   })
                   self.bookChange(pk_accountingbook);
                } else {
                }
            }
        });
        let callback= (json) =>{
            let searchData = this.state.searchData;
            searchData.pk_currtype=json['20021CFQANY-000026']
            this.setState({json:json,searchData:searchData},()=>{
//                 initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'20021CFQANY',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentDidMount(){

    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.SearchModalShow;
        this.setState({
            showModal
        })
    }
    close() {
        this.props.closeSearchModal(false);
    }
    componentDidMount(){
        if(getBusinessInfo()){
            if(getBusinessInfo().businessDate){
                this.businessInfo=getBusinessInfo().businessDate.split(' ')[0]
            }
        }
        this.getVoucherTypes()
    }
    getVoucherTypes(){
        let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
        let self = this;
        ajax({
            url,
            data:{"localType":"1","showAllCurr":"1"},
            success: function(response) { 
                const { data, error, success } = response;
                if (success&&data) {
                   self.setState({
                    pk_currtypes:data
                   })
                }  
            }
        });
    }
    handleConfirm=()=>{
        let {searchData,tableData,selectArr}=this.state;
        if(!searchData.pk_accountingbook){
            toast({ content: this.state.json['20021CFQANY-000027'], color: 'warning' });
            return
        }
        if(!searchData.begindate){
            toast({ content: this.state.json['20021CFQANY-000066'], color: 'warning' });/* 国际化处理： 日期判断*/
            return
        }
        let obj={};
        let localArr=[];
        for(let i=0,len=tableData.length;i<len;i++){
            for(let j=0,lenj=selectArr.length;j<lenj;j++){
                if(tableData[i].pk_checktype==selectArr[j].pk_checktype){
                    obj={
                        pk_checktype:tableData[i].pk_checktype,
                        pk_checkvalue:tableData[i].value.refpk
                    }
                    localArr.push(Object.assign({},obj));
                }   
            }
        }
        for(let i=0,len=localArr.length;i<len;i++){
            for(let j=0,lenj=selectArr.length;j<lenj;j++){
                if(localArr[i].pk_checktype!=selectArr[j].pk_checktype){
                    localArr.splice(i,1);
                    len--;
                }   
            }
            if(selectArr.length==0){
                localArr = []
            }
        }
        searchData.assvo=localArr;
        if(!searchData.pk_currtype){
            searchData.pk_currtype=this.state.json['20021CFQANY-000026']
            // searchData.pk_currtype=this.state.json['20021CFQANY-000065']
        }
        this.props.closeSearchModal(false);
        this.props.getMainData(searchData)
    }
    createCfg(id, param) {//
        var obj = {
            value: this.state.configs[id] ? this.state.configs[id].value : [],
            onChange: function (val) {
                var temp = Object.assign(this.state.configs[id], { value: val });
                this.setState(Object.assign(this.state.configs, temp));
            }.bind(this)
        }
        this.state.configs[id] = obj;
        var result_param = Object.assign(obj, param)
        return result_param;
    }
    //处理多选
  onAllCheckChange = () => {//全选
    let self = this;
    let checkedArray = [];
    let listData = self.state.tableData.concat();
    let selIds = [];
    // let id = self.props.multiSelect.param;
    for (var i = 0; i < self.state.checkedArray.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
      selIds.push(i);
    }
    if(self.state.checkedAll){
      selIds=[]
    }
    self.setState({
      checkedAll: !self.state.checkedAll,
      checkedArray: checkedArray,
      selIds: selIds
    });
    let tableData = this.state.tableData;
    let selectArr = [];
    
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(tableData[selIds[i]])));
    }
    this.setState({
        selectArr
    })
  };
  onCheckboxChange = (text, record, index) => {//单选
    let self = this;
    let allFlag = false;
    let selIds = self.state.selIds;
    // let id = self.props.postId;
    let checkedArray = self.state.checkedArray.concat();
    if (self.state.checkedArray[index]) {
      selIds.splice(record.key,1);
      for(let i=0,len=selIds.length;i<len;i++){
        if(record.key==selIds[i]){
          selIds.splice(i,1)
          break;
        }
      }
    } else {
      selIds.push(record.key);
    }
    checkedArray[index] = !self.state.checkedArray[index];
    for (let i = 0; i < self.state.checkedArray.length; i++) {
      if (!checkedArray[i]) {
        allFlag = false;
        break;
      } else {
        allFlag = true;
      }
    }
    self.setState({
      checkedAll: allFlag,
      checkedArray: checkedArray,
      selIds: selIds
    });
    
    let tableData = this.state.tableData;
    let selectArr = [];
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(tableData[selIds[i]])));
    }
    this.setState({
        selectArr
    })
  };
  renderColumnsMultiSelect(columns) {
    const { data,checkedArray,tableData } = this.state;
    let { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // multiSelect= "checkbox";
    // let indeterminate_bool1 = true;
    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArray.length;
      while(i--){
          if(checkedArray[i]){
            indeterminate_bool = true;
            break;
          }
      }
      let defaultColumns = [
        {
          title: (
            <NCCheckbox
              className="table-checkbox"
              checked={this.state.checkedAll}
              indeterminate={indeterminate_bool&&!this.state.checkedAll}
              onChange={this.onAllCheckChange}
            />
          ),
          key: "checkbox",
          attrcode: "checkbox",
          dataIndex: "checkbox",
          width: "57px",
          render: (text, record, index) => {
            return (
              <NCCheckbox
                className="table-checkbox"
                checked={this.state.checkedArray[index]}
                onChange={this.onCheckboxChange.bind(this, text, record, index)}
              />
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
  dealTableData=(data)=>{
    let checkedArray=[];
    for(let i=0,len=data.length;i<len;i++){
        checkedArray.push(false);
        data[i].key=i;
        data[i].value={
            refpk:'',
            refname:''
        }
    }
    this.columns=[
        {
        title: (<span fieldid="name">{this.state.json['20021CFQANY-000028']}</span>),
            dataIndex: "name",
            key: "name",
            width:'180px',
            render: (text, record, index) => (
              <span fieldid="name">
                {this.state.tableData[index].name}
              </span>
            )
          },
          {
            title: (<span fieldid="namep">{this.state.json['20021CFQANY-000029']}</span>),
            dataIndex: "name",
            key: "name",
            width:'180px',
            render: (text, record, index) => (
              this.getContent(this.state.tableData[index], index)
            )
          }
    ]
    this.setState({
        tableData:data,
        checkedArray
    })
  }
  getParm=(parm)=>{
    let appUrl = decodeURIComponent(window.location.href).split('ml&');//分割查询
    if (appUrl && appUrl[1]){
        let appPrams = appUrl[1].split('&');
        if(appPrams && appPrams instanceof Array){
            let parmObj={};
            appPrams.forEach(item=>{
                let key = item.split('=')[0];
                let value = item.split('=')[1];
                parmObj[key] = value;
            })
            return parmObj[parm];
        }
    }
}
  bookChange=(v)=>{
    let {pk_accountingbook,searchData}=this.state;
    pk_accountingbook.refcode=v.refcode;
    pk_accountingbook.refname=v.refname;
    pk_accountingbook.refpk=v.refpk;
    searchData.pk_accountingbook = v.refpk;
    this.setState({
        pk_accountingbook,
        searchData
    })
    let url='/nccloud/gl/accountrep/assbalancequeryobject.do';
    let self = this;
    ajax({
        url,
        data:{"pk_accountingbook":v.refpk,'needaccount':false},
        success: function(response) { 
            const { data, error, success } = response;
            if (success&&data) {
                self.dealTableData(data);
            } else {
                
            }    
        }
    });
    let url1 = "/nccloud/gl/cashflow/isbustartflag.do"
    ajax({
        url:url1,
        data:{"pk_accountingbook":v.refpk},
        success: function(response) { 
            const { data, error, success } = response;
            if (success&&data) {
                if(data=='Y'){
                    searchData.pk_unitState=true;
                    self.setState({
                        pk_unitState:true,
                        searchData
                    });
                }else{
                    searchData.pk_unitState=false;
                    self.setState({
                        pk_unitState:false,
                        searchData
                    });
                }
            } else {
                
            }    
        }
    });
    ajax({
        url:'/nccloud/gl/voucher/queryBookCombineInfo.do',
        data:{"pk_accountingbook":v.refpk,'needaccount':false},
        success: function(response) { 
            const { data, error, success } = response;
            let {searchData,period_pk,bizDate}=self.state;
            if (success&&data) {
                searchData.year=data.bizPeriod.split('-')[0];
                searchData.period=data.bizPeriod.split('-')[1];
                searchData.begindate=data.begindate;
                searchData.enddate=data.enddate;
                searchData.NC001=data.NC001;
                searchData.NC002=data.NC002;
                searchData.pk_unitSearch = data.unit.value;
                period_pk.pk_accperiodscheme = data.pk_accperiodscheme;
                period_pk.refname=data.bizPeriod;
                period_pk.refpk=data.bizPeriod;
                bizDate=data.bizDate;
                self.setState({
                    pk_org:data.unit.value,
                    period_pk,
                    searchData,
                    bizDate
                })
            } else {
                
            }    
        }
    });
  }
  getContent=(data, index)=>{
      let flag = Number(data.datatype);
      let tableData = this.state.tableData;
      if(flag){
        if(flag==1||flag==4){
            return (
                <InputItem
                    type="customer"
                    name="scale"
                    defaultValue={data.value.refpk}
                    onChange={(v) => {
                         tableData[index].value.refpk=v
                         this.setState({
                            tableData
                         })
                    }}
                />
            )
          }else if(flag==31){
            return (
                <NCNumber
                    scale={Number(data.digit)}
                    value={data.value.refpk}
                    onChange={(v) => {
                         tableData[index].value.refpk=v
                         this.setState({
                            tableData
                         })
                    }}
                />
            )
          }
          else if(flag==32){
            return (
                <NCSelect
                    value={data.value.refpk}
                    className='search-boolean'
                    onChange={(v)=>{
                        tableData[index].value.refpk=v
                        this.setState({
                            tableData
                        })
                    }}
                >
                    <NCOption value="Y">{this.state.json['20021CFQANY-000036']}</NCOption>
                    <NCOption value="N">{this.state.json['20021CFQANY-000037']}</NCOption>
                    <NCOption value="">{this.state.json['20021CFQANY-000038']}</NCOption>
                </NCSelect>
            )
          }
          else if(flag==33||flag==34){
            return (
                <NCDatePicker
                    value={tableData[index].value.refpk}
                    showTime={true}
                    format='YYYY-MM-DD HH:mm:ss'
                    onChange={(v)=>{
                        tableData[index].value.refpk=v;
                        this.setState({
                            tableData
                         })
                    }}
                />
            )
          }
      }else {
        if(data.refpath){
            let referUrl= data.refpath+ '.js';
            if(!this.state[data.pk_checktype]){
                {createScript.call(this,referUrl,data.pk_checktype)}
            }
            else{
                if(data.classid.length==20){
                    return (
                        <div>
                            <div  style={{height:40}} class='u-col-md-12 u-col-sm-12 u-col-xs-12'>
                                {this.state[data.pk_checktype] ? (this.state[data.pk_checktype])(
                                    {    
                                        // isMultiSelectedEnabled: true,
                                        value:tableData[index].value,
                                        queryCondition:{
                                            pk_org:this.state.searchData.pk_unit.length!=0?this.state.searchData.pk_unit[0]:this.state.pk_org,
                                            isDataPowerEnable: 'Y',
                                            DataPowerOperationCode: 'fi'
                                        },
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi',
                                        isShowDisabledData:true,
                                        "pk_defdoclist":data.classid,
                                        onChange: (v) => {
                                            tableData[index].value.refpk=v.refpk;
                                            tableData[index].value.refname=v.refname;
                                            this.setState({
                                                tableData
                                            })
                                        },
                                    }
                                ) : <div />}
                            </div>
                        </div>
                    );
                }else if(data.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||data.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'){
                    return (
                        <div>
                            <div  style={{height:40}} class='u-col-md-12 u-col-sm-12 u-col-xs-12'>
                                {this.state[data.pk_checktype] ? (this.state[data.pk_checktype])(
                                    {    
                                        value:tableData[index].value,
                                        unitProps:{
                                            refType: 'tree',
                                            refName: this.state.json['20021CFQANY-000030'],
                                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                            rootNode:{refname:this.state.json['20021CFQANY-000030'],refpk:'root'},
                                            placeholder:this.state.json['20021CFQANY-000030'],
                                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                            treeConfig:{name:[this.state.json['20021CFQANY-000031'], this.state.json['20021CFQANY-000032']],code: ['refcode', 'refname']},
                                            isMultiSelectedEnabled: false,
                                            //unitProps:unitConf,
                                        },
                                        unitCondition:{
                                            pk_financeorg:this.state.searchData.pk_unit.length!=0?this.state.searchData.pk_unit[0]:this.state.pk_org,
                                            TreeRefActionExt:'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder',
                                            isDataPowerEnable: 'Y',
                                            DataPowerOperationCode: 'fi'
                                        },
                                        isShowUnit:true,
                                        // isMultiSelectedEnabled:true,
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi',
                                        isShowDisabledData:true,
                                        "unitValueIsNeeded":false,
                                        "isShowDimission":true,
                                        queryCondition:{
                                            isDataPowerEnable: 'Y',
                                            DataPowerOperationCode: 'fi',
                                            "busifuncode":"all",
                                            pk_org:this.state.searchData.pk_unit.length!=0?this.state.searchData.pk_unit[0]:this.state.pk_org,
                                            isShowDimission:true
                                        },
                                        onChange: (v) => {
                                            tableData[index].value.refpk=v.refpk;
                                            tableData[index].value.refname=v.refname;
                                            this.setState({
                                                tableData
                                            })
                                        },
                                    }
                                ) : <div />}
                            </div>
                        </div>
                    );
                }else if(data.classid=='8c6510dd-3b8a-4cfc-a5c5-323d53c6006f'){
                    return (
                        <div>
                            <div  class='u-col-md-12 u-col-sm-12 u-col-xs-12'>
                                {this.state[data.pk_checktype] ? (this.state[data.pk_checktype])(
                                    {    
                                        // isMultiSelectedEnabled: data.isMultiSelectedEnabled,
                                        value:tableData[index].value,
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi',
                                        isShowDisabledData:true,
                                        queryCondition:{
                                            pk_org:this.state.searchData.pk_unit.length!=0?this.state.searchData.pk_unit[0]:this.state.pk_org,
                                            "DataPowerOperationCode" : 'fi',
                                        },
                                        onChange: (v) => {
                                            tableData[index].value.refpk=v.refpk;
                                            tableData[index].value.refname=v.refname;
                                            this.setState({
                                                tableData
                                            })
                                        },
                                    }
                                ) : <div />}
                            </div>
                        </div>
                    );
                }
                else{
                    return (
                        <div>
                            <div  style={{height:40}} class='u-col-md-12 u-col-sm-12 u-col-xs-12'>
                                {this.state[data.pk_checktype] ? (this.state[data.pk_checktype])(
                                    {    
                                        // isMultiSelectedEnabled: data.isMultiSelectedEnabled,
                                        value:tableData[index].value,
                                        queryCondition:{
                                            pk_org:this.state.searchData.pk_unit.length!=0?this.state.searchData.pk_unit[0]:this.state.pk_org,
                                            isDataPowerEnable: 'Y',
                                        DataPowerOperationCode: 'fi'
                                    },
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi',
                                        isShowDisabledData:true,
                                        onChange: (v) => {
                                            tableData[index].value.refpk=v.refpk;
                                            tableData[index].value.refname=v.refname;
                                            this.setState({
                                                tableData
                                            })
                                        },
                                    }
                                ) : <div />}
                            </div>
                        </div>
                    );
                }
            }
        }else{
            return <span></span>
        }
        
      }
  }
    render () {
        let {pk_accountingbook,period_pk,searchData,pk_vouchertype,bizDate,pk_unit,
            pk_prepared,accountcode,oppaccountcode,tableData,pk_unitState,pk_org,pk_currtype,
            pk_currtypes}=this.state;
        let columns = this.renderColumnsMultiSelect(this.columns);
        let AccountDefaultGrid;
		let referUrl = 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js';
		if(!this.state['myattrcode']){
			{createScript.call(this,referUrl,'myattrcode')}
		}else{	
            AccountDefaultGrid =  (
				<Row>
					<Col xs={12} md={12}>
					{this.state['myattrcode']?(this.state['myattrcode'])(
						{
                            fieldid:"oppaccountcode",
							placeholder:this.state.json['20021CFQANY-000035'],
                            value:oppaccountcode,
                            isMultiSelectedEnabled:true,
                            queryCondition:{
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',
                                "pk_accountingbook":pk_accountingbook.refpk,"pk_org":pk_org,
                                'dateStr':this.businessInfo
                            },
                            onChange:(v)=>{
                                // oppaccountcode=v;
                                let obj = {},localArr=[];
                                for(let i=0,len=v.length;i<len;i++){
                                    obj = {
                                        refname:v[i].refcode,
                                        refpk:v[i].refpk,
                                    }
                                    oppaccountcode.push(obj)
                                    localArr.push(v[i].refcode);
                                }
                                searchData.oppaccountcode=localArr.join(',')
                                this.setState({
                                    oppaccountcode,
                                    searchData
                                })
                            }
						}
					):<div/>}
					</Col>
				</Row>
			);	
		}
        return (
            <div>
              <NCModal 
              fieldid="query"
              show = {this.state.showModal}
              className='dnd-cancel senior searchModalCash'
            //   size={'lg'}
              onHide={ this.close }
            //   animation={true}
              >
                <NCModal.Header closeButton className='clearfix' fieldid="header-area">
                  <NCModal.Title fieldid={`${this.state.json['20021CFQANY-000039']}_title`}>{this.state.json['20021CFQANY-000039']}</NCModal.Title>
                </NCModal.Header>

                <NCModal.Body>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                    <Row>
                        <Col lg={2} sm={2} xs={2}>
                        <span style={{'color':'#E14C46'}}>*</span>{this.state.json['20021CFQANY-000040']}:
                        </Col>
                        <Col lg={4} sm={4} xs={4}>
                            <AccountBookTreeRef
                                fieldid="pk_accountingbook"
                                value={{refcode:pk_accountingbook.refcode,refname:pk_accountingbook.refname,
                                    refpk:pk_accountingbook.refpk}}
                                isMultiSelectedEnabled={false}
                                disabledDataShow={true}
                                queryCondition={{
                                    "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    "appcode": this.props.getSearchParam('c')
                                }}
                                onChange={(v)=>{
                                    this.bookChange(v)    
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={2} sm={2} xs={2} className={pk_unitState?'':'display-none'}>
                            {this.state.json['20021CFQANY-000030']}
                        </Col>
                        <Col lg={4} sm={4} xs={4} className={pk_unitState?'':'display-none'}>
                            <BusinessUnitWithGlobleAndCurrGroupTreeRef
                                fieldid="pk_unit"
                                isMultiSelectedEnabled={true}
                                value={pk_unit}
                                queryCondition={{
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi',
                                    "pk_accountingbook":pk_accountingbook.refpk,
                                    VersionStartDate:bizDate,
                                    "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder',
                                }} 
                                onChange={(v)=>{
                                    pk_unit=v;
                                    searchData.pk_unit=[]
                                    let obj = {},localArr=[];
                                    for(let i=0,len=v.length;i<len;i++){
                                        searchData.pk_unit.push(v[i].refpk)
                                    }
                                    this.bookChange(pk_accountingbook) 
                                    this.setState({
                                        pk_unit,
                                        searchData
                                    })
                                }}
                            />
                        </Col>
                    </Row>
                    <div className='dateContent'>
                        <div className='preriod'>
                            <AccperiodMonthTreeGridRef
                                fieldid="period_pk"
                                value = {{refpk:period_pk.refpk,refname:period_pk.refname}}
                                disabled={searchData.querybyperiod!='Y'}
                                queryCondition = {() => {
                                    return { 
                                        GridRefActionExt:'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder',
                                        pk_accperiodscheme:period_pk.pk_accperiodscheme
                                    }}
                                }
                                onChange={(v)=>{
                                    if(v.refname){
                                        searchData.year=v.values.yearmth.value.split('-')[0];
                                        searchData.period=v.values.accperiodmth.value;
                                        searchData.begindate=v.values.begindate.value;
                                        searchData.enddate=v.values.enddate.value;
                                        period_pk.refname=v.refname;
                                        period_pk.refpk=v.refpk;
                                    }else{
                                        searchData.year='';
                                        searchData.period='';
                                        searchData.begindate='';
                                        searchData.enddate='';
                                        period_pk.refname='';
                                        period_pk.refpk='';
                                    }
                                    
                                    this.setState({
                                        period_pk,
                                        searchData
                                    })
                                }}
                            />
                        </div>
                        <NCRadio.NCRadioGroup
                            fieldid="querybyperiod"
                            name="fruit"
                            selectedValue={searchData.querybyperiod}
                            onChange={(v)=>{
                                searchData.querybyperiod=v;
                                this.setState({
                                    searchData
                                })
                            }}>

                            <NCRadio value="Y" >
                                <Row>
                                    <Col lg={2} sm={2} xs={2}>
                                    {this.state.json['20021CFQANY-000041']}
                                    </Col>
                                    <Col lg={4} sm={4} xs={4}>
                                        
                                    </Col>
                                </Row>
                            </NCRadio>
                            <NCRadio value="N" className='displayB'>
                            {this.state.json['20021CFQANY-000042']}
                            </NCRadio>
                        </NCRadio.NCRadioGroup>
                        <Row className='search-data-ob'>
                            <Col lg={4} sm={4} xs={4}>
                                <NCDatePicker
                                    fieldid="begindate"
                                    value={searchData.begindate}
                                    disabled={searchData.querybyperiod=='Y'}
                                    onChange={(v)=>{
                                        searchData.begindate=v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </Col>
                            {/* <Col lg={1} sm={1} xs={1}>
                                    ---
                            </Col> */}
                            <Col lg={4} sm={4} xs={4}>
                                <NCDatePicker
                                    fieldid="enddate"
                                    value={searchData.enddate}
                                    disabled={searchData.querybyperiod=='Y'}
                                    onChange={(v)=>{
                                        searchData.enddate=v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </Col>
                        </Row>
                    </div>
                    
                    <Row>
                        <Col lg={2} sm={2} xs={2}>
                        {this.state.json['20021CFQANY-000033']}
                        </Col>
                        <Col lg={4} sm={4} xs={4}>
                            <VoucherTypeDefaultGridRef 
                                fieldid="pk_vouchertype"
                                placeholder={this.state.json['20021CFQANY-000033']}
                                value={{refcode:pk_vouchertype.refcode,refname:pk_vouchertype.refname,
                                    refpk:pk_vouchertype.refpk}}
                                queryCondition={{
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi',
                                    GridRefActionExt:'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
                                    pk_org:pk_accountingbook.refpk
                                }} 
                                onChange= {(v)=>{
                                    pk_vouchertype.refcode=v.refcode;
                                    pk_vouchertype.refname=v.refname;
                                    pk_vouchertype.refpk=v.refpk;
                                    searchData.pk_vouchertype = v.refpk;
                                    this.setState({
                                        pk_vouchertype,
                                        searchData
                                    })
                                  }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={2} sm={2} xs={2}>
                        {this.state.json['20021CFQANY-000043']}
                        </Col>
                        <Col lg={3} sm={3} xs={3}>
                            <div className="explanation">
                                <NCNumber 
                                    fieldid="startvoucherno"
                                    name='explanation'
                                    type='text' value={searchData.startvoucherno}
                                    autocomplete="off"
                                    onChange={(value)=>{
                                        searchData.startvoucherno=value;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </div>
                        </Col>
                        <Col lg={1} sm={1} xs={1} style={{'text-align':'center','width':'17px'}}>
                                <sapn>~</sapn>
                        </Col>
                        <Col lg={3} sm={3} xs={3}>
                            <div className="explanation">
                                <NCNumber 
                                    fieldid="endvoucherno" 
                                    name='explanation'
                                    type='text'
                                    value={searchData.endvoucherno}
                                    autocomplete="off"
                                    onChange={(value)=>{
                                        searchData.endvoucherno=value;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <NCRadio.NCRadioGroup
                            fieldid="analyze" 
                            name="12"
                            selectedValue={searchData.analyze}
                            style={{'padding-left':'14px'}}
                            onChange={(v)=>{
                                searchData.analyze=v;
                                this.setState({
                                    searchData
                                })
                            }}>
                            <NCRadio  value="0">{this.state.json['20021CFQANY-000044']}</NCRadio>
                            <NCRadio  value="1">{this.state.json['20021CFQANY-000045']}</NCRadio>
                            <NCRadio  value="2">{this.state.json['20021CFQANY-000046']}</NCRadio>
                        </NCRadio.NCRadioGroup>
                    </Row>
                    <Row className='select-more'>
                        <Col lg={5} sm={5} xs={5}>
                            <NCCheckbox colors="info" name="agree"
                                checked={searchData.includeuntally=='Y'?true:false}
                                onChange={(v)=>{
                                    if(v){
                                        searchData.includeuntally='Y';
                                    }else{
                                        searchData.includeuntally='N';
                                    }
                                    this.setState({
                                        searchData
                                    })
                                }}
                                >
                                    {this.state.json['20021CFQANY-000047']}
                            </NCCheckbox>
                            </Col>
                            <Col lg={5} sm={5} xs={5}>
                                <NCCheckbox colors="info" name="agree"
                                    checked={searchData.includeuncash=='Y'?true:false}
                                    onChange={(v)=>{
                                        if(v){
                                            searchData.includeuncash='Y';
                                        }else{
                                            searchData.includeuncash='N';
                                        }
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                    >
                                        {this.state.json['20021CFQANY-000048']}
                                </NCCheckbox>
                            </Col>
                    </Row>
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                        {this.state.json['20021CFQANY-000049']}：
                        </Col>
                        <Col md={4} xs={4} sm={4}>
                            {OperatorDefaultRef({} = this.createCfg("OperatorDefaultRef", {
                                fieldid:"pk_prepared" ,
                                // isMultiSelectedEnabled: true,
                                queryCondition: function () {
                                    let condition = {
                                        isDataPowerEnable: 'Y',
                                        DataPowerOperationCode: 'fi',
                                        pk_accountingbook : pk_accountingbook.refpk,
                                        user_Type : '0',
                                        pk_org:pk_org,
                                        GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder'
                                    }
                                    return condition;
                                },
                                value:{refcode:pk_prepared.refcode,refname:pk_prepared.refname,
                                    refpk:pk_prepared.refpk},
                                onChange: (v)=>{
                                    pk_prepared.refcode=v.refcode;
                                    pk_prepared.refname=v.refname;
                                    pk_prepared.refpk=v.refpk;
                                    searchData.pk_prepared = v.refpk;
                                    this.setState({
                                        pk_prepared,
                                        searchData
                                    })
                                    }
                            }))}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                        {this.state.json['20021CFQANY-000034']}：
                        </Col>
                        <Col md={4} xs={4} sm={4}>
                            {/* <AccountDefaultModelTreeRef  */}
                            <ReferLoader
                                fieldid="accountcode" 
                                tag='AccountDefaultGridTreeRefCode'
                                refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
                                placeholder={this.state.json['20021CFQANY-000034']} 
                                value={accountcode}
                                isMultiSelectedEnabled={true}
                                queryCondition={{"pk_accountingbook":pk_accountingbook.refpk,"pk_org":pk_org,
                                    'dateStr':this.businessInfo,isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi'
                                }} 
                                onChange= {(v)=>{
                                    accountcode=[];
                                    let obj = {},localArr=[];
                                    for(let i=0,len=v.length;i<len;i++){
                                        obj = {
                                            refname:v[i].refcode,
                                            refpk:v[i].refpk,
                                        }
                                        accountcode.push(Object.assign({},obj))
                                        localArr.push(v[i].refcode);
                                    }
                                    searchData.accountcode=localArr.join(',')
                                    this.setState({
                                        accountcode,
                                        searchData
                                    })
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                        {this.state.json['20021CFQANY-000035']}：
                        </Col>
                        <Col md={4} xs={4} sm={4}>
                            {AccountDefaultGrid}
                        </Col>
                    </Row>
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                        {this.state.json['20021CFQANY-000050']}：
                        </Col>
                        <Col md={4} xs={4} sm={4}>
                            <NCSelect
                                fieldid="pk_currtype" 
                                defaultValue={searchData.pk_currtype}
                                style={{ width: '100%', marginRight: 2 }}
                                onChange= {(v)=>{
                                    searchData.pk_currtype = v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                            >
                                {pk_currtypes.map((e)=>{
                                    return(<NCOption value={e.pk_currtype}>{e.name}</NCOption>)
                                })}
                            </NCSelect>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={12} sm={12} xs={12}>
                        <div fieldid="search_table">
                            <Table
                                columns={columns}
                                data={tableData}
                                onRowClick={this.clickRow}
                                scroll={{ x:false, y: 210 }}
                                className='searchTableCash'
                                // bordered
                            />  
                            </div>     
                        </Col>
                    </Row>
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                        {this.state.json['20021CFQANY-000051']}
                        </Col>
                        <Col md={8} xs={8} sm={8}>
                            <NCRadio.NCRadioGroup
                                fieldid="returncurr"
                                name="12"
                                selectedValue={searchData.returncurr}
                                onChange={(v)=>{
                                    searchData.returncurr=v;
                                    this.setState({
                                        searchData
                                    })
                                }}>
                                <NCRadio  value="1">{this.state.json['20021CFQANY-000052']}</NCRadio>
                                <NCRadio  value="2" disabled={!searchData.NC001}>{this.state.json['20021CFQANY-000053']}</NCRadio>
                                <NCRadio  value="3" disabled={!searchData.NC002}>{this.state.json['20021CFQANY-000054']}</NCRadio>
                            </NCRadio.NCRadioGroup>
                        </Col>
                    </Row>
                    </NCDiv>
                </NCModal.Body>

                <NCModal.Footer fieldid="bottom_area">
                    <NCButton onClick={this.handleConfirm} colors="primary" fieldid="confirm">{this.state.json['20021CFQANY-000005']}</NCButton>
                    <NCButton onClick={this.close} fieldid="close">{this.state.json['20021CFQANY-000006']}</NCButton>
                </NCModal.Footer>

              </NCModal>
            </div>
        )
    }
}
SearchModal = createPage({})(SearchModal);
export default SearchModal;
SearchModal.defaultProps = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
};
