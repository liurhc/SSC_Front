import React, { Component } from 'react';
import { high, base, ajax, createPage, cardCache, getMultiLang, promptBox,toast } from 'nc-lightapp-front';
const { NCTable: Table, NCCheckbox: Checkbox, NCRadio,NCDiv } = base;
let { setDefData, getDefData } = cardCache;
import {getTableHeight } from '../../../public/common/method.js';
class Demo12 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			searchData: {},
			mainData: [],
			json: {},
			buttonStatus: {
				subject: false,
				assit: {
					statu: false, data: {
						pk_accasoa: '',
						prepareddate: '',
						pk_accountingbook: '',
					}
				},
				cashflow: false,
				index: '',
				backState: false,
				rowData: [],
				selectedRowIndex: 0
			},
			columns: []
		};
		// columns=[];
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({
				json: json,
				columns: [
					//  { title: "序号", dataIndex: "prepareddate", key: "prepareddate", 
					//     width:'3%',
					//     render: (text, record, index) => (
					//       <span>
					//         {index}
					//       </span>
					//   )
					// },
					{
						title: (<div fieldid="subjcode" className="mergecells">{json['20021CFQANY-000034']}</div>),
						dataIndex: "subjcode",
						key: "subjcode",
						width: '120px',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="subjcode">
								{this.state.mainData[index].subjcode.display?this.state.mainData[index].subjcode.display:<span>&nbsp;</span>}
							</div>
						)
					},
					{
						title: (<div fieldid="subjname" className="mergecells">{json['20021CFQANY-000055']}</div>),
						dataIndex: "subjname",
						key: "subjname",
						width: '120px',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="subjname">
								{this.state.mainData[index].subjname.display?this.state.mainData[index].subjname.display:<span>&nbsp;</span>}</div>
						)
					},
					{
						title: (<div fieldid="pk_currtype" className="mergecells">{json['20021CFQANY-000050']}</div>),
						dataIndex: "pk_currtype",
						key: "pk_currtype",
						width: '120px',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="pk_currtype">
								{this.state.mainData[index].pk_currtype.display?this.state.mainData[index].pk_currtype.display:<span>&nbsp;</span>}</div>
						)
					},
					{
						title: (<div fieldid="endflag" className="mergecells">{json['20021CFQANY-000056']}</div>),
						dataIndex: "endflag",
						key: "endflag",
						width: '120px',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="endflag">
								{(this.state.mainData[index].endflag == 0) ? <span>&nbsp;</span> : (this.state.mainData[index].endflag == 1 ? this.state.json['20021CFQANY-000036'] : this.state.json['20021CFQANY-000037'])}</div>
						)
					},
					{
						title: (<div fieldid="pk_cashflow" className="mergecells">{json['20021CFQANY-000057']}</div>),
						dataIndex: "pk_cashflow",
						key: "pk_cashflow",
						width: '120px',
						className: 't-a-r',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="pk_cashflow">
								{this.state.mainData[index].mainmoney.display?this.state.mainData[index].mainmoney.display:<span>&nbsp;</span>}</div>
						)
					},
					{
						title: (<div fieldid="subjcode1" className="mergecells">{json['20021CFQANY-000034']}</div>),
						dataIndex: "subjcode1",
						key: "subjcode1",
						width: '120px',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="subjcode1">
								{this.state.mainData[index].subjcode.display?this.state.mainData[index].subjcode.display:<span>&nbsp;</span>}</div>
						)
					},
					{
						title: (<div fieldid="subjname1" className="mergecells">{json['20021CFQANY-000055']}</div>),
						dataIndex: "subjname1",
						key: "subjname1",
						width: '120px',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="subjname1">
								{this.state.mainData[index].subjname.display?this.state.mainData[index].subjname.display:<span>&nbsp;</span>}</div>
						)
					},
					{
						title: (<div fieldid="pk_currtype1" className="mergecells">{json['20021CFQANY-000050']}</div>),
						dataIndex: "pk_currtype1",
						key: "pk_currtype1",
						width: '120px',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="pk_currtype1">
								{this.state.mainData[index].pk_currtype.display?this.state.mainData[index].pk_currtype.display:<span>&nbsp;</span>}</div>
						)
					},
					{
						title: (<div fieldid="endflag1" className="mergecells">{json['20021CFQANY-000056']}</div>),
						dataIndex: "endflag1",
						key: "endflag1",
						width: '120px',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="endflag1">
								{(this.state.mainData[index].endflag == 0) ? <span>&nbsp;</span> : (this.state.mainData[index].endflag == 1 ? this.state.json['20021CFQANY-000036'] : this.state.json['20021CFQANY-000037'])}</div>
						)
					},
					{
						title: (<div fieldid="pk_cashflow1" className="mergecells">{json['20021CFQANY-000058']}</div>),
						dataIndex: "pk_cashflow1",
						key: "pk_cashflow1",
						width: '120px',
						className: 't-a-r',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="pk_cashflow1">
								{this.state.mainData[index].mainmoney.display?this.state.mainData[index].mainmoney.display:<span>&nbsp;</span>}</div>
						)
					},
				]
			}, () => {

			})
		}
		getMultiLang({ moduleId: '20021CFQANY', domainName: 'gl', currentLocale: 'simpchn', callback });
	}
	componentWillReceiveProps(nextProps) {
		let columns = this.state.columns.length ? this.state.columns : [
			{
				title: (<div fieldid="subjcode" className="mergecells">{this.state.json['20021CFQANY-000034']}</div>),
				dataIndex: "subjcode",
				key: "subjcode",
				width: '120px',
				render: (text, record, index) => (
					<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="subjcode">
						{this.state.mainData[index].subjcode.display?this.state.mainData[index].subjcode.display:<span>&nbsp;</span>}
					</div>
				)
			},
			{
				title: (<div fieldid="subjname" className="mergecells">{this.state.json['20021CFQANY-000055']}</div>),
				dataIndex: "subjname",
				key: "subjname",
				width: '120px',
				render: (text, record, index) => (
					<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="subjname">
						{this.state.mainData[index].subjname.display?this.state.mainData[index].subjname.display:<span>&nbsp;</span>}</div>
				)
			},
			{
				title: (<div fieldid="pk_currtype" className="mergecells">{this.state.json['20021CFQANY-000050']}</div>),
				dataIndex: "pk_currtype",
				key: "pk_currtype",
				width: '120px',
				render: (text, record, index) => (
					<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="pk_currtype">
						{this.state.mainData[index].pk_currtype.display?this.state.mainData[index].pk_currtype.display:<span>&nbsp;</span>}</div>
				)
			},
			{
				title: (<div fieldid="endflag" className="mergecells">{this.state.json['20021CFQANY-000056']}</div>),
				dataIndex: "endflag",
				key: "endflag",
				width: '120px',
				render: (text, record, index) => (
					<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="endflag">
						{(this.state.mainData[index].endflag == 0) ? <span>&nbsp;</span> : (this.state.mainData[index].endflag == 1 ? this.state.json['20021CFQANY-000036'] : this.state.json['20021CFQANY-000037'])}</div>
				)
			},
			{
				title: (<div fieldid="pk_cashflow" className="mergecells">{this.state.json['20021CFQANY-000057']}</div>),
				dataIndex: "pk_cashflow",
				key: "pk_cashflow",
				width: '120px',
				className: 't-a-r',
				render: (text, record, index) => (
					<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="pk_cashflow">
						{this.state.mainData[index].mainmoney.display?this.state.mainData[index].mainmoney.display:<span>&nbsp;</span>}</div>
				)
			},
			{
				title: (<div fieldid="subjcode1" className="mergecells">{this.state.json['20021CFQANY-000034']}</div>),
				dataIndex: "subjcode1",
				key: "subjcode1",
				width: '120px',
				render: (text, record, index) => (
					<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="subjcode1">
						{this.state.mainData[index].subjcode.display?this.state.mainData[index].subjcode.display:<span>&nbsp;</span>}</div>
				)
			},
			{
				title: (<div fieldid="subjname1" className="mergecells">{this.state.json['20021CFQANY-000055']}</div>),
				dataIndex: "subjname1",
				key: "subjname1",
				width: '120px',
				render: (text, record, index) => (
					<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="subjname1">
						{this.state.mainData[index].subjname.display?this.state.mainData[index].subjname.display:<span>&nbsp;</span>}</div>
				)
			},
			{
				title: (<div fieldid="pk_currtype1" className="mergecells">{this.state.json['20021CFQANY-000050']}</div>),
				dataIndex: "pk_currtype1",
				key: "pk_currtype1",
				width: '120px',
				render: (text, record, index) => (
					<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="pk_currtype1">
						{this.state.mainData[index].pk_currtype.display?this.state.mainData[index].pk_currtype.display:<span>&nbsp;</span>}</div>
				)
			},
			{
				title: (<div fieldid="endflag1" className="mergecells">{this.state.json['20021CFQANY-000056']}</div>),
				dataIndex: "endflag1",
				key: "endflag1",
				width: '120px',
				render: (text, record, index) => (
					<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="endflag1">
						{(this.state.mainData[index].endflag == 0) ?<span>&nbsp;</span>: (this.state.mainData[index].endflag == 1 ? this.state.json['20021CFQANY-000036'] : this.state.json['20021CFQANY-000037'])}</div>
				)
			},
			{
				title: (<div fieldid="pk_cashflow1" className="mergecells">{this.state.json['20021CFQANY-000058']}</div>),
				dataIndex: "pk_cashflow1",
				key: "pk_cashflow1",
				width: '120px',
				className: 't-a-r',
				render: (text, record, index) => (
					<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="pk_cashflow1">
						{this.state.mainData[index].mainmoney.display?this.state.mainData[index].mainmoney.display:<span>&nbsp;</span>}</div>
				)
			},
		]

		if (nextProps.searchData) {
			if (nextProps.searchData.pk_unitState != this.state.searchData.pk_unitState ||
				nextProps.searchData.pk_currtype != this.state.searchData.pk_currtype) {
				if (nextProps.searchData.pk_unitState && columns[0].dataIndex != 'pk_unit') {//业务单元
					columns.splice(0, 0, {
						title: (<div fieldid="pk_unit" className="mergecells">{this.state.json['20021CFQANY-000030']}</div>),
						dataIndex: "pk_unit",
						key: "pk_unit",
						width: '120px',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="pk_unit">
								{this.state.mainData[index].pk_unit.display?this.state.mainData[index].pk_unit.display:<span>&nbsp;</span>}
							</div>
						)
					});
					if (nextProps.searchData.pk_currtype != this.state.json['20021CFQANY-000026']) {
						columns.splice(6, 0, {
							title: (<div fieldid="pk_unit1" className="mergecells">{this.state.json['20021CFQANY-000030']}</div>),
							dataIndex: "pk_unit1",
							key: "pk_unit1",
							width: '120px',
							render: (text, record, index) => (
								<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="pk_unit1">
									{this.state.mainData[index].pk_unit.display?this.state.mainData[index].pk_unit.display:<span>&nbsp;</span>}
								</div>
							)
						});
					} else if (nextProps.searchData.pk_currtype == this.state.json['20021CFQANY-000026']) {
						columns.splice(7, 0, {
							title: (<div fieldid="pk_unit1" className="mergecells">{this.state.json['20021CFQANY-000030']}</div>),
							dataIndex: "pk_unit1",
							key: "pk_unit1",
							width: '120px',
							render: (text, record, index) => (
								<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="pk_unit1">
									{this.state.mainData[index].pk_unit.display?this.state.mainData[index].pk_unit.display:<span>&nbsp;</span>}
								</div>
							)
						});
					}
				} else if (!nextProps.searchData.pk_unitState) {
					for (let i = 0, len = columns.length; i < len; i++) {
						if (columns[i].dataIndex == 'pk_unit') {
							columns.splice(i, 1);
							len--
						}
						if (columns[i].dataIndex == 'pk_unit1') {
							columns.splice(i, 1);
							len--
						}
					}
				}

				if (nextProps.searchData.pk_currtype != this.state.json['20021CFQANY-000026'] && nextProps.searchData.pk_unitState && columns[5].dataIndex != 'money') {//原币
					columns.splice(5, 0, {
						title: (<div fieldid="money" className="mergecells">{this.state.json['20021CFQANY-000059']}</div>),
						dataIndex: "money",
						key: "money",
						width: '120px',
						className: 't-a-r',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="money">
								{this.state.mainData[index].money.display?this.state.mainData[index].money.display:<span>&nbsp;</span>}
							</div>
						)
					});
					columns.splice(12, 0, {
						title: (<div fieldid="money1" className="mergecells">{this.state.json['20021CFQANY-000060']}</div>),
						dataIndex: "money1",
						key: "money1",
						width: '120px',
						className: 't-a-r',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="money1">
								{this.state.mainData[index].money.display?this.state.mainData[index].money.display:<span>&nbsp;</span>}
							</div>
						)
					});
				} else if (nextProps.searchData.pk_currtype != this.state.json['20021CFQANY-000026'] && !nextProps.searchData.pk_unitState && columns[4].dataIndex != 'money') {//原币
					columns.splice(4, 0, {
						title: (<div fieldid="money" className="mergecells">{this.state.json['20021CFQANY-000059']}</div>),
						dataIndex: "money",
						key: "money",
						width: '120px',
						className: 't-a-r',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 1 ? '' : 'display-none'} fieldid="money">
								{this.state.mainData[index].money.display?this.state.mainData[index].money.display:<span>&nbsp;</span>}
							</div>
						)
					});
					columns.splice(10, 0, {
						title: (<div fieldid="money1" className="mergecells">{this.state.json['20021CFQANY-000060']}</div>),
						dataIndex: "money1",
						key: "money1",
						width: '120px',
						className: 't-a-r',
						render: (text, record, index) => (
							<div className={this.state.mainData[index].balient&&this.state.mainData[index].balient.value == 2 ? '' : 'display-none'} fieldid="money1">
								{this.state.mainData[index].money.display?this.state.mainData[index].money.display:<span>&nbsp;</span>}
							</div>
						)
					});
				}
				else if (nextProps.searchData.pk_currtype == this.state.json['20021CFQANY-000026']) {
					for (let i = 0, len = columns.length; i < len; i++) {
						if (columns[i].dataIndex == 'money') {
							columns.splice(i, 1);
							len--
						}
					}
				}
			}
		}
		let mainData = nextProps.mainData;
		// if(mainData.length){
		//   mainData = nextProps.mainData.map((item,index)=>{
		//     item.key=index;
		//     return (item);
		//   })
		// }
		for (let i = 0, len = mainData.length; i < len; i++) {
			mainData[i].key = i + 1
		}
		this.setState({
			searchData: Object.assign({}, nextProps.searchData),
			mainData,
			columns
		})
	}
	expandRow = (record, index) => {//展开
		let buttonStatus = this.state.buttonStatus;
		let expandLen = 0;
		if (record.status == 5 || record.status == 6) {
			return
		}
		if (record.status == 99) {
			toast({ content: this.state.json['20021CFQANY-000061'], color: 'warning' });
		} else {
			if (record.didExpand) {//已展开双击取消
				this.delExpandRow(record, index);
				// this.delExpandRowAll(record, index);
				buttonStatus.backState = false;
				this.setState({
					buttonStatus
				})
				return;
			} else {
				// expandLen = this.delExpandRowAll(record,index);
				buttonStatus.backState = true;
				this.setState({
					buttonStatus
				})
				let data = {
					params: this.props.searchData,
					rowdata: record
				};
				let self = this;
				let url = "";
				if (record.endflag == 2) {
					url = '/nccloud/gl/cashflow/cfQuickAnaShowSubAcc.do'
				} else if (record.endflag == 1 || record.endflag == 0) {
					if (record.cashflag == 1 || record.cashflag == 0) {
						toast({ content: this.state.json['20021CFQANY-000063'], color: 'warning' });
						return;
					} else if (record.cashflag == 2) {
						url = "/nccloud/gl/cashflow/cfQuickAnaShowCf.do";
					}
				}
				ajax({
					url,
					data,
					success: function (response) {
						const { data, error, success } = response;
						if (success && data) {
							let mainData = self.state.mainData;
							for (let i = 0, len = data.length; i < len; i++) {
								data[i].isExpand = index + 1;
								if (mainData[index].localLevel == 0) {
									data[i].localLevel = 1;
								} else if (!mainData[index].localLevel) {
									data[i].localLevel = 1;
								} else {
									data[i].localLevel = mainData[index].localLevel + 1;
									data[i].isExpand2 = mainData[index].isExpand
								}
							}
							mainData[index].didExpand = index + 1;
							mainData.splice(index + 1, 0, ...data);
							self.props.setMainData(mainData);
						} else {
							// self.props.setMainData([]);
							toast({ content: self.state.json['20021CFQANY-000063'], color: 'warning' });
						}
					}
				});
			}
			buttonStatus.assit.data = {
				pk_accasoa: record.subjcode.value,
				prepareddate: '2018-04-01',
				pk_accountingbook: record.pk_accountingbook.value,
			}
			buttonStatus.rowData = this.state.mainData[index];
			buttonStatus.index = index;
			this.props.getButtonStatus(buttonStatus)
		}
	}
	delExpandRow = (record, index) => {//收回
		let mainData = this.state.mainData;
		let j = 0;
		for (let i = 0, len = mainData.length; i < len; i++) {
			if (mainData[i]) {
				if (mainData[i].isExpand) {
					if (record.key < mainData[i].key) {//收回下边的展开
						if (mainData[i].isExpand == record.didExpand) {
							if (mainData[i].didExpand) {
								for (let h = i + 1, lenh = mainData.length; h < lenh; h++) {
									if (mainData[h]) {
										if (mainData[h].localLevel > 1) {
											mainData.splice(h, 1)
											j++;
											h--;
										}
									}
								}
							}
							mainData.splice(i, 1)
							j++;
							i--;
						}
					}
				}
			}
		}
		delete mainData[index].didExpand;
		this.props.setMainData(mainData);
		return j;
	}
	delExpandRowAll = (record, index) => {//收回其他
		let mainData = this.state.mainData;
		let j = 0;
		for (let i = 0, len = mainData.length; i < len; i++) {
			if (mainData[i]) {
				if (mainData[i].isExpand) {
					if (mainData[i].isExpand) {
						mainData.splice(i, 1)
						j++;
						i--;
					}
				}
			}
		}
		delete mainData[index].didExpand;
		this.props.setMainData(mainData);
		return j;
	}
	changeButtonState = (record, index) => {//修改按钮状态
		let { mainData, buttonStatus } = this.state;
		buttonStatus.assit.data = {
			pk_accasoa: record.subjcode.value,
			prepareddate: '2018-04-01',
			pk_accountingbook: record.pk_accountingbook.value,
		}
		buttonStatus.rowData = record;
		buttonStatus.index = record.key-1;//index;
		if (record.endflag == 2&&!record.didExpand) {//1末级无展开
			buttonStatus.subject = true
		} else {
			buttonStatus.subject = false
		}
		if (record.cashflag == 2) {//1现金类不显示
			buttonStatus.cashflow = true
		} else {
			buttonStatus.cashflow = false
		}
		if (record.isHasAss == false) {//true存在辅助核算
			buttonStatus.assit.statu = true
		} else {
			buttonStatus.assit.statu = false
		}
		if (record.status == 99) {
			buttonStatus.assit.statu = false
			buttonStatus.cashflow = false
			buttonStatus.subject = false
		}
		if (mainData[index].didExpand) {
			buttonStatus.backState = true;
		} else {
			buttonStatus.backState = false;
		}
		this.props.getButtonStatus(buttonStatus)
		this.setState({
			selectedRowIndex: record.key
		})
	}
	renderColumnsMultiSelect(columns) {
		let defaultColumns = [];
		columns = defaultColumns.concat(columns);
		return columns;
	}
	render() {
		let { mainData, columns } = this.state;
		// let columns = this.state.columns;
		columns = this.renderColumnsMultiSelect(columns);
		let theme = window.top.nccColor  //获取当前主题'black'
		return (
			<NCDiv fieldid="fastanalyse" areaCode={NCDiv.config.TableCom}>
				<Table
					columns={columns}
					data={mainData}
					bordered
					rowClassName={(record, index, indent) => {
						if (this.state.selectedRowIndex == record.key) {
							return 'nctable-selected-row';//'selectedColor';
						} else if (record.endflag == 2) {
							return theme==='black' ? 'endflagColor_b' : 'endflagColor' //黑色主题透明度50%
						} else if (record.isHasAss == true) {
							return theme==='black' ? 'isHasAssColor_b' : 'isHasAssColor'
						}
					}}

					onRowDoubleClick={(record, index, event) => {
						this.expandRow(record, index)
					}}
					onRowClick={(record, index, event) => {
						this.changeButtonState(record, index)
					}}
					bodyStyle={{height:getTableHeight(80)}}
					scroll={{ x: true, y: getTableHeight(80) }}
				/>
			</NCDiv>
		)

	}
}
Demo12 = createPage({
	// initTemplate: initTemplate
})(Demo12);
export default Demo12;