import React, {
    Component
} from 'react';
import ReactDOM from 'react-dom';
import {
    ajax,gzip,
    base,
    createPage,
    cardCache, promptBox,
    cacheTools, getMultiLang,
    createPageIcon,toast
} from 'nc-lightapp-front';
import SearchModal from '../searchModal';
import AppointModal from '../handAppointModal';
import HeaderArea from '../../../public/components/HeaderArea';
import { openToVoucher } from '../../../public/common/voucherUtils';
const {
    NCButton,
    NCButtonGroup,
    NCMenu: Menu,
    NCDropdown: Dropdown,
    NCIcon
} = base;
const {
    Item
} = Menu;
const gziptools = new gzip();
class Buttons extends Component {
    constructor(props) {
        super(props),
            this.state = {
                json: {},
                SearchModalShow: false,
                AppointModalShow: false,
                buttonStatus: {
                    subject: true,
                    assit: {
                        statu: true,
                        data: {
                            pk_accasoa: '',
                            prepareddate: '',
                            pk_accountingbook: '',
                        }
                    },
                    cashflow: true,
                    index: '',
                    backState: false,
                    rowData: {
                        status: ''
                    }
                },
                assitArr: [],
                pk_checktype: '',
                buttonsData: [],
                searchData: null
            }
    }
    componentWillMount() {
        let pagecode = this.props.getSearchParam('p')
        let appcode = this.props.getSearchParam('c')
        let self = this;
        ajax({
            url: '/nccloud/platform/appregister/queryallbtns.do',
            data: {
                pagecode: pagecode,
                appcode: appcode
            },
            success: (res) => {
                /*  拿到按钮数据，调用setButtons方法将数据设置到页面上，然后执行后续的按钮逻辑 */
                if (!res.data) {
                    return
                }
                self.props.button.setButtons(res.data);
                this.setState({
                    buttonsData: res.data
                })
                self.props.button.setButtonDisabled({
                    'shouacc': true,
                    'showass': true,
                    'showcf': true,
                    'unexpand': true,
                    'setcf': true,
                    'cancelcf': true,
                    'linkvou': true,
                })
                // self.props.button.setButtonDisabled({'save':true,'cancel':true,'add':false,'edit':false}) 
            }
        });
        let callback = (json) => {
            this.setState({ json: json }, () => {
                //                 initTemplate.call(this, this.props);
            })
        }
        getMultiLang({ moduleId: '20021CFQANY', domainName: 'gl', currentLocale: 'simpchn', callback });
    }
    componentWillReceiveProps(nextProps) {
        let buttonStatus = nextProps.buttonStatus;
        this.setButtonDis(buttonStatus);
        this.setState({
            buttonStatus: Object.assign({}, nextProps.buttonStatus)
        })
        if (nextProps.buttonStatus.index == this.state.buttonStatus.index) {
            return
        }
        let self = this;
        let url = "/nccloud/gl/voucher/queryAssItem.do";
        let data = buttonStatus.assit.data;
        data.includeSub = 'Y';
        let buttonsData = self.state.buttonsData;
        ajax({
            url,
            data,
            loading: false,
            success: function (response) {
                const {
                    data,
                    error,
                    success
                } = response;
                if (success && data) {
                    let assitArr = data.map((item, index) => {
                        let localItem = {
                            "id": item.pk_accassitem,
                            "type": "button_secondary",
                            "key": item.pk_accassitem,
                            "title": item.name,
                            "area": "head",
                            "iskeyfunc": false,
                            "isenable": true,
                            "order": "5",
                            "parentCode": "assgroup",
                            "children": [

                            ]
                        }
                        return localItem
                    })
                    buttonsData[1].children[1].children[0].children = assitArr;
                    self.setState({
                        buttonsData
                    })
                    self.props.button.setButtons(buttonsData)
                    self.props.button.setButtonDisabled({
                        'shouacc': true,
                        'showass': true,
                        'showcf': true,
                        'unexpand': true,
                        'setcf': true,
                        'cancelcf': true,
                        'linkvou': true,
                    })
                    self.setButtonDis(buttonStatus);
                } else {
                    buttonsData[1].children[1].children[0].children = [
                        {
                            "id": '',
                            "type": "button_secondary",
                            "key": '',
                            "title": self.state.json['20021CFQANY-000000'],//无辅助核算项
                            "area": "head",
                            "iskeyfunc": false,
                            "isenable": true,
                            "order": "5",
                            "parentCode": "assgroup",
                            "children": [

                            ]
                        }
                    ]
                    self.setState({
                        buttonsData
                    })
                    self.props.button.setButtons(buttonsData)
                    self.props.button.setButtonDisabled({
                        'shouacc': true,
                        'showass': true,
                        'showcf': true,
                        'unexpand': true,
                        'setcf': true,
                        'cancelcf': true,
                        'linkvou': true,
                    })
                    self.setButtonDis(buttonStatus);
                }
            }
        });
        self.setState({
            buttonStatus: Object.assign({}, nextProps.buttonStatus)
        })
    }
    
    setButtonDis = (buttonStatus) => {
        if (!this.state.buttonsData.length) {
            return
        }
        if (buttonStatus.rowData.endflag == 2&&buttonStatus.subject) {
            this.props.button.setButtonDisabled({
                'shouacc': false,
            })
        }else{
            this.props.button.setButtonDisabled({
                'shouacc': true,
            })
        }
        // if(this.state.buttonsData[1].children[1].children[0].children[0].title != this.state.json['20021CFQANY-000000']){
        //     this.props.button.setButtonDisabled({
        //         'showass':false,
        //     }) 
        // }
        if (buttonStatus.rowData.isHasAss) {
            this.props.button.setButtonDisabled({
                'showass': false,
            })
        }
        if (buttonStatus.rowData.status == 2 || buttonStatus.rowData.status == 4 || buttonStatus.rowData.status == 3) {
            this.props.button.setButtonDisabled({
                'showcf': false,
            })
        }
        if (buttonStatus.backState) {
            this.props.button.setButtonDisabled({
                'unexpand': false,
            })
        } else {
            this.props.button.setButtonDisabled({
                'unexpand': true,
            })
        }
        if (buttonStatus.rowData.status == 2 || buttonStatus.rowData.status == 3 || buttonStatus.rowData.status == 6 || buttonStatus.rowData.status == 4) {
            this.props.button.setButtonDisabled({
                'setcf': false,
            })
        }
        if (buttonStatus.rowData.status == 2 || buttonStatus.rowData.status == 3 || buttonStatus.rowData.status == 5 || buttonStatus.rowData.status == 4 || buttonStatus.rowData.status == 9) {
            this.props.button.setButtonDisabled({
                'cancelcf': false,
            })
        }
        if (buttonStatus.rowData.status != 99 || !buttonStatus.rowData.isSub) {
            this.props.button.setButtonDisabled({
                'linkvou': false,
            })
        }
        if (buttonStatus.rowData.isSub) {
            this.props.button.setButtonDisabled({
                'linkvou': true,
            })
        } else {
            this.props.button.setButtonDisabled({
                'linkvou': false,
            })
        }
    }
    onButtonClick = (props, id) => {
        let buttonStatus = this.state.buttonStatus;
        if (!buttonStatus.rowData.status && id != 'query' && id != 'refresh') {
            toast({
                content: this.state.json['20021CFQANY-000001'],//无选中数据
                color: 'warning'
            });
            return
        }
        switch (id) {
            case 'query':
                this.handleSearch();
                break;
            case 'shouacc':
                this.handleExpandDetails(1)
                break;
            case 'showcf':
                this.handleExpandDetails(3);
                break;
            case 'unexpand':
                this.handleDelExpandAll();
                break;
            case 'cancelcf':
                this.cancelCashflow();
                break;
            case 'linkvou':
                this.handleTovoucher();
                break;
            case 'refresh':
                if (this.state.searchData) {
                    this.props.getSearchData(this.state.searchData);
                }
                break;
            case 'autoset':
                this.handleDisAppoint(0);
                break;
            case 'handset':
                this.handleDisAppoint(1);
                break;
            case 'analysiscf':
                this.handleCashflow(3);
                break;
            default:
                this.handleExpandDetails(2, id);
                break;
        }
    }
    handleExpandDetails = (type, pk_checktype) => { //展开
        let self = this;
        self.handleDelExpand();
        let url;
        if (type == 1) {
            url = '/nccloud/gl/cashflow/cfQuickAnaShowSubAcc.do';
        } else if (type == 2) {
            url = '/nccloud/gl/cashflow/cfQuickAnaShowAss.do';
        } else if (type == 3) {
            url = '/nccloud/gl/cashflow/cfQuickAnaShowCf.do';
        }
        let {
            buttonStatus,
            searchData
        } = self.state;
        buttonStatus.backState = true;
        buttonStatus.subject=false;
        let mainData = self.props.mainData;
        // let index = buttonStatus.index;
        let data = {
            params: searchData,
            rowdata: buttonStatus.rowData
        };
        if (type == 2) {
            data.pk_checktype = pk_checktype
        }
        self.expandDetailsAjax(mainData,buttonStatus,data,url,type);
        // ajax({
        //     url,
        //     data,
        //     success: function (response) {
        //         const {
        //             data,
        //             error,
        //             success
        //         } = response;
        //         if (success && data) {
        //             for (let i = 0, len = data.length; i < len; i++) {
        //                 data[i].isExpand = index + 1;
        //                 data[i].key = mainData[index].key + 1+i;
        //                 data[i].parentTimeStamp=mainData[index].timeStamp;
        //                 data[i].timeStamp = (data[i].subjcode?data[i].subjcode.value:'')+(new Date()).valueOf()+i;//每行数据加上唯一标识 编码+时间戳
        //                 if (mainData[index].localLevel == 0) {
        //                     data[i].localLevel = 1;
        //                 }
        //                 if (mainData[index].localLevel == 1) {
        //                     data[i].localLevel = 2;
        //                     data[i].isExpand2 = mainData[index].isExpand
        //                 }
        //                 if (type == 1) {
        //                     data[i].isSub = false;
        //                 } else {
        //                     data[i].isSub = true;
        //                 }
        //             }

        //             mainData[index].didExpand = index + 1;
        //             buttonStatus.rowData.didExpand= index + 1;
        //             mainData.splice(index + 1, 0, ...data);
        //             self.props.getMainData(mainData, buttonStatus)
        //         } else {

        //         }
        //     }
        // });
    }
    //展开
    expandDetailsAjax=(mainData,buttonStatus,ajaxData,url,type)=>{
        let self = this;
        let index = buttonStatus.index;
        ajax({
            url,
            data:ajaxData,
            success: function (response) {
                const {
                    data,
                    error,
                    success
                } = response;
                if (success && data) {
                    for (let i = 0, len = data.length; i < len; i++) {
                        data[i].isExpand = index + 1;
                        data[i].key = mainData[index].key + 1+i;
                        data[i].parentTimeStamp=mainData[index].timeStamp;
                        data[i].timeStamp = (data[i].subjcode?data[i].subjcode.value:'')+(new Date()).valueOf()+i;//每行数据加上唯一标识 编码+时间戳
                        if (mainData[index].localLevel == 0) {
                            data[i].localLevel = 1;
                        }
                        if (mainData[index].localLevel == 1) {
                            data[i].localLevel = 2;
                            data[i].isExpand2 = mainData[index].isExpand
                        }
                        if (type == 1) {
                            data[i].isSub = false;
                        } else {
                            data[i].isSub = true;
                        }
                    }

                    mainData[index].didExpand = index + 1;
                    buttonStatus.rowData.didExpand= index + 1;
                    mainData.splice(index + 1, 0, ...data);
                    self.props.getMainData(mainData, buttonStatus)
                } else {

                }
            }
        });
    }
    handleDelExpandCash = (type) => {//现金收回
        let {
            buttonStatus
        } = this.state;
        let mainData = this.props.mainData;
        let record = buttonStatus.rowData;
        let index = buttonStatus.index;
        // if(record.endflag==1){
        //     return
        // }
        for (let i = 0, len = mainData.length; i < len; i++) {
            if (mainData[i]) {
                // if (mainData[i].isExpand == record.isExpand) {
                    if(record.parentTimeStamp==mainData[i].parentTimeStamp&&mainData[i].timeStamp!=mainData[i].parentTimeStamp){
                        mainData.splice(i, 1);
                        i--;
                        index--;
                    }
                // }
            }
        }
        buttonStatus.backState = false;
        // if (mainData[index].didExpand) {
        //     delete mainData[index].didExpand;
        // }
        this.setState({
            buttonStatus
        });
        this.props.getMainData(mainData, buttonStatus);
        // return index;
        return buttonStatus.rowData.isExpand;
    }
    handleDelExpand = (type) => {//行上收回
        let {
            buttonStatus
        } = this.state;
        let mainData = this.props.mainData;
        let record = buttonStatus.rowData;
        let index = buttonStatus.index;
        // if(record.endflag==1){
        //     return
        // }
        if (type == 2) {
            for (let i = 0, len = mainData.length; i < len; i++) {
                if (mainData[i]) {
                    if (mainData[i].isExpand && record.didExpand) {
                        // ||mainData[i].isExpand==record.isExpand
                        if (mainData[i].isExpand == record.didExpand || mainData[i].isExpand2 == record.didExpand) {
                            if(record.timeStamp==mainData[i].parentTimeStamp&&mainData[i].timeStamp!=mainData[i].parentTimeStamp){
                                mainData.splice(i, 1);
                                i--;
                                if (index > i) {
                                    index--;
                                }
                            }
                            
                        }
                    }
                }
            }
        } else {
            for (let i = 0; i < mainData.length; i++) {
                if (mainData[i]) {
                    if (mainData[i].isExpand) {
                        // ||mainData[i].isExpand==record.isExpand
                        if (mainData[i].isExpand == record.didExpand) {
                            // if (record.key < mainData[i].key) {
                            if(record.timeStamp==mainData[i].parentTimeStamp&&mainData[i].timeStamp!=mainData[i].parentTimeStamp){
                                mainData.splice(i, 1);
                                i--;
                                if (index > i) {
                                    index--;
                                }
                            }
                        }
                    }
                }
            }
        }
        buttonStatus.backState = false;
        if (mainData[index]&&mainData[index].didExpand) {
            delete mainData[index].didExpand;
        }
        this.setState({
            buttonStatus
        });
        this.props.getMainData(mainData, buttonStatus);
        // return record.isExpand;
        return buttonStatus.rowData.key;//buttonStatus.rowData.isExpand;
    }
    handleDelExpandAll = () => {//收回
        let {
            buttonStatus
        } = this.state;
        let mainData = this.props.mainData;
        let record = buttonStatus.rowData;
        let index = buttonStatus.index;
        for (let i = 0, len = mainData.length; i < len; i++) {
            if (mainData[i]) {
                if (mainData[i].isExpand) {
                    mainData.splice(i, 1)
                    i--;
                }
            }
        }
        buttonStatus.backState = false;
        buttonStatus.subject=true;
        if (mainData[index]&&mainData[index].didExpand) {
            delete mainData[index].didExpand;
        }
        this.setState({
            buttonStatus
        });
        this.props.getMainData(mainData, buttonStatus);
    }
    handleSearch = () => { //查询框
        this.setState({
            SearchModalShow: true
        })
    }

    closeSearchModal = (data) => {
        this.setState({
            SearchModalShow: data
        })
    }
    getMainData = (data) => {//查询整体数据
        this.props.getSearchData(data)
        this.setState({
            searchData: data
        })
    }
    handleDisAppoint = (index) => {//现金流量提示
        promptBox({
            color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
            title: this.state.json['20021CFQANY-000003'],                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
            content: this.state.json['20021CFQANY-000004'],             // 提示内容,非必输
            noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
            beSureBtnName: this.state.json['20021CFQANY-000005'],          // 确定按钮名称, 默认为"确定",非必输
            cancelBtnName: this.state.json['20021CFQANY-000006'],         // 取消按钮名称, 默认为"取消",非必输
            beSureBtnClick: () => {
                this.handleAppoint(index)
            }, // 确定按钮点击调用函数,非必输
            closeByClickBackDrop: false,    //点击遮罩关闭提示框，默认是false点击不关闭,点击关闭是true
            zIndex: 200                     //遮罩的层级为zIndex，弹框默认为zIndex+1。默认为200，非比传项
        })
    }
    handleAppoint = (type) => {//指定现金流量分析
        let {
            buttonStatus,
            searchData
        } = this.state;
        let index = buttonStatus.index;
        let record=buttonStatus.rowData;
        let mainData = this.props.mainData;
        if (buttonStatus.rowData.status == 5 || buttonStatus.rowData.status == 6 || buttonStatus.rowData.status == 9) {
            index = this.handleDelExpandCash();
            mainData.map((item,k)=>{
                if(item.timeStamp==record.timeStamp||item.timeStamp==record.parentTimeStamp){
                    index=k;
                }
            })
        } else {
            if (buttonStatus.rowData.localLevel == 0) {
                this.handleDelExpand(2);
            } else if (buttonStatus.rowData.localLevel > 0) {
                this.handleDelExpand();
            }
            mainData.map((item,k)=>{
                if(item.timeStamp==record.timeStamp){
                    index=k;
                }
            })
        }
        buttonStatus.backState = true;
        // let mainData = this.props.mainData;
        let data = {
            params: searchData,
            rowdata: buttonStatus.rowData
        };
        let self = this;
        let url;
        if (type == 0) {
            url = '/nccloud/gl/cashflow/cfQuickAnaAutoAssign.do';
        } else if (type == 1) {
            this.setState({
                AppointModalShow: true
            })
            return;
        } else if (type == 2) {
            url = '/nccloud/gl/cashflow/cfQuickAnaShowCf.do';
        }
        ajax({
            url,
            data,
            success: function (response) {
                const {
                    data,
                    error,
                    success
                } = response;
                if (success && data) {
                    let cfdata = data.cfdata;
                    for (let i = 0, len = cfdata.length; i < len; i++) {
                        cfdata[i].isExpand = index + 1;
                        cfdata[i].parentTimeStamp=mainData[index].timeStamp;
                        cfdata[i].timeStamp = (cfdata[i].subjcode?cfdata[i].subjcode.value:'')+(new Date()).valueOf()+i;//每行数据加上唯一标识 编码+时间戳
                        if (mainData[index].localLevel == 0) {
                            cfdata[i].localLevel = 1;
                        }
                        if (mainData[index].localLevel == 1) {
                            cfdata[i].localLevel = 2;
                            cfdata[i].isExpand2 = mainData[index].isExpand
                        }
                    }
                    mainData[index].didExpand = index + 1;
                    mainData.splice(index + 1, 0, ...cfdata)
                    self.props.getMainData(mainData, buttonStatus)
                    toast({
                        content: data.message,
                        color: 'success'
                    });
                } else {

                }
            }
        });
    }
    closeAppointModal = (data) => {
        this.setState({
            AppointModalShow: data
        })
    }
    getAppointData = (value) => {
        this.handleDelExpand();
        let {
            buttonStatus,
            searchData
        } = this.state;
        buttonStatus.backState = true;
        let mainData = this.props.mainData;
        let index = buttonStatus.index;
        let record=buttonStatus.rowData;
        if (buttonStatus.rowData.status == 5 || buttonStatus.rowData.status == 6 || buttonStatus.rowData.status == 9) {
            mainData.map((item,k)=>{
                if(item.timeStamp==record.timeStamp||item.timeStamp==record.parentTimeStamp){
                    index=k;
                }
            })
        } else {
            mainData.map((item,k)=>{
                if(item.timeStamp==record.timeStamp){
                    index=k;
                }
            })
        }
        let data = {
            params: searchData,
            rowdata: buttonStatus.rowData,
            cfpks: [value.v1.refpk, value.v2.refpk]
        };
        let self = this;
        let url = '/nccloud/gl/cashflow/cfQuickAnaAssign.do';
        ajax({
            url,
            data,
            success: function (response) {
                const {
                    data,
                    error,
                    success
                } = response;
                if (success && data) {
                    let cfdata = data.cfdata;
                    for (let i = 0, len = cfdata.length; i < len; i++) {
                        cfdata[i].isExpand = index + 1;
                        cfdata[i].parentTimeStamp=mainData[index].timeStamp;
                        cfdata[i].timeStamp = (cfdata[i].subjcode?cfdata[i].subjcode.value:'')+(new Date()).valueOf()+i;//每行数据加上唯一标识 编码+时间戳
                        if (mainData[index].localLevel == 0) {
                            cfdata[i].localLevel = 1;
                        }
                        if (mainData[index].localLevel == 1) {
                            cfdata[i].localLevel = 2;
                            cfdata[i].isExpand2 = mainData[index].isExpand
                        }
                    }
                    mainData[index].didExpand = index + 1;
                    mainData.map((item,k)=>{
                        if (buttonStatus.rowData.status == 5 || buttonStatus.rowData.status == 6 || buttonStatus.rowData.status == 9) {
                            if(record.timeStamp==item.timeStamp||record.parentTimeStamp==item.timeStamp){
                                mainData.splice(k + 1, 0, ...cfdata)
                            }
                        }else{
                            if(record.timeStamp==item.timeStamp){
                                mainData.splice(k + 1, 0, ...cfdata)
                            }
                            
                        }
                        
                    })
                    // mainData.splice(index + 1, 0, ...cfdata)
                    self.props.getMainData(mainData, buttonStatus)
                    toast({
                        content: data.message,
                        color: 'success'
                    });
                } else {

                }
            }
        });
    }
    handleCashflow = () => {
        let {
            buttonStatus,
            searchData
        } = this.state;
        let data = {
            params: searchData,
            rowdata: buttonStatus.rowData,
        };
        this.props.openTo('/gl/gl_cashFlows/CashFlowAnalyse/main/index.html', {
            searchDataCash: gziptools.zip(JSON.stringify(data)),
            appcode: '20020CFANY',
            c: '20020CFANY',
            // p:'20020CFANYPAGE',
            // ar:'0001Z31000000002I8HQ',
            // n:'现金流量分析'
        })
    }
    cancelCashflow = () => {//取消现金流量
        // this.handleDelExpand(2);
        let {
            buttonStatus,
            searchData
        } = this.state;
        let data = {
            params: searchData,
            rowdata: buttonStatus.rowData
        };
        let index = buttonStatus.index;
        let record=buttonStatus.rowData;
        let mainData = this.props.mainData;
        let parentRowData=[];
        if (buttonStatus.rowData.status == 5 || buttonStatus.rowData.status == 6 || buttonStatus.rowData.status == 9) {
            let localIndex = this.handleDelExpandCash()
            // index = localIndex ? localIndex : index;
            mainData.map((item,k)=>{
                if(item.timeStamp==record.timeStamp||item.timeStamp==record.parentTimeStamp){
                    index=k;
                    parentRowData=item;
                }
            })
        } else {
            let localIndex = this.handleDelExpand(2)
            // index = localIndex ? localIndex : index;
            mainData.map((item,k)=>{
                if(item.timeStamp==record.timeStamp){
                    index=k;
                    parentRowData=item;
                }
            })
        }
        
        let self = this;
        ajax({
            url: '/nccloud/gl/cashflow/cfQuickAnaCancel.do',
            data,
            success: function (response) {
                const {
                    data,
                    error,
                    success
                } = response;
                if (success && data) {
                    let cfdata = data.cfdata;
                    if(record.isExpand){//选中展开行取消现金流量的话需要根据父级去展开现金流量
                        toast({
                            content: data.message,
                            color: 'warning'
                        });
                        let url = '/nccloud/gl/cashflow/cfQuickAnaShowCf.do';
                        buttonStatus.index=index;
                        buttonStatus.rowData=parentRowData;
                        let mainData = self.props.mainData;
                        let ajaxData = {
                            params: searchData,
                            rowdata: buttonStatus.rowData
                        };
                        self.expandDetailsAjax(mainData,buttonStatus,ajaxData,url);
                    }else{
                        for (let i = 0, len = cfdata.length; i < len; i++) {
                            cfdata[i].isExpand = index + 1;
                            cfdata[i].parentTimeStamp=mainData[index].timeStamp;
                            cfdata[i].timeStamp = (cfdata[i].subjcode?cfdata[i].subjcode.value:'')+(new Date()).valueOf()+i;//每行数据加上唯一标识 编码+时间戳
                            cfdata[i].key = mainData[index].key + 1+i;
                            if (mainData[index].localLevel == 0) {
                                cfdata[i].localLevel = 1;
                            }
                            if (mainData[index].localLevel > 0) {
                                cfdata[i].localLevel = mainData[index].localLevel + 1;
                                cfdata[i].isExpand2 = mainData[index].isExpand
                            }
                        }
                        mainData[index].didExpand = index + 1;
                        buttonStatus.rowData.didExpand= index + 1;
                        mainData.map((item,k)=>{
                            // if(item.key==index){
                            if (buttonStatus.rowData.status == 5 || buttonStatus.rowData.status == 6 || buttonStatus.rowData.status == 9) {
                                if(record.timeStamp==item.timeStamp||record.parentTimeStamp==item.timeStamp){
                                    mainData.splice(k + 1, 0, ...cfdata)
                                }
                            }else{
                                if(record.timeStamp==item.timeStamp){
                                    mainData.splice(k + 1, 0, ...cfdata)
                                }
                                
                            }
                            
                        })
                        self.props.getMainData(mainData, buttonStatus)
                        toast({
                            content: data.message,
                            color: 'warning'
                        });
                    }
                }
            }
        });
    }
    handleTovoucher = () => {//联查
        let key = 'checkedData'
        let self = this;
        ajax({//获取联查数据及联查
            url: '/nccloud/gl/cashflow/cfQuickAnaQueryVouPk.do',
            data: {
                params: this.state.searchData,
                rowdata: this.props.buttonStatus.rowData
            },
            success: function (response) {
                const { data, error, success } = response;
                if (success && data) {
                    let param = {
                        pk_voucher: data,
                        titlename: self.state.json['20021CFQANY-000015']/* 国际化处理： 凭证联查*/
                    }
                    openToVoucher(self,param)
                    
                } else {
                    return
                }
            }
        });
    }

    render() {
        let {
            SearchModalShow,
            buttonStatus,
            assitArr,
            pk_checktype,
            AppointModalShow
        } = this.state;
        let { createButtonApp } = this.props.button;
        return (
            <div>
                <HeaderArea 
                    title ={this.state.json['20021CFQANY-000008']}
                    btnContent = {
                        createButtonApp({
                            area: 'head',
                            onButtonClick: this.onButtonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })
                        
                    }
                />
                {/* <div className="header-title-search-area">
                    {createPageIcon()}
                    <h2 className='title-search-detail' fieldid={`${this.state.json['20021CFQANY-000008']}_title`}>{this.state.json['20021CFQANY-000008']}</h2>
                </div> */}
                <div className="header-button-area">
                    {/* <div className="header-button-area">
                        {createButtonApp({
                            area: 'head',
                            onButtonClick: this.onButtonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })}
                    </div> */}
                    <SearchModal SearchModalShow={SearchModalShow}
                        closeSearchModal={this.closeSearchModal.bind(this)}
                        getMainData={this.getMainData.bind(this)}
                    ></SearchModal>
                    <AppointModal AppointModalShow={AppointModalShow}
                        getAppointData={this.getAppointData.bind(this)}
                        closeAppointModal={this.closeAppointModal.bind(this)}
                        pk_unit={this.props.pk_unit}
                    />
                </div>
            </div>
        )
    }
}
Buttons = createPage({})(Buttons);
export default Buttons;