import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,getMultiLang } from 'nc-lightapp-front';
import Buttons from './buttons';
import FastAnalyseTable from './table';
import './index.less';
class CashFlows extends Component{
    constructor(props){
        super(props),
        this.state={
            json:{},
            mainData:[],
            searchData:{},
            buttonStatus:{
                subject:true,
                assit:{statu:true,data:[]},
                cashflow:true,
                index:'',
                backState:false,
                rowData:{}
            }
        }
    }
    componentWillMount(){
        let callback= (json) =>{
            this.setState({json:json},()=>{
//                 initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'20021CFQANY',domainName:'gl',currentLocale:'simpchn',callback});
    }
    handleGet=(value)=>{
    }
    getSearchData=(data)=>{
        let self = this;
        let url = "/nccloud/gl/cashflow/cfQuickAnaQuery.do";
        this.setState({
            searchData:data
        })
        let initButtonStatus={
            subject: true,
            assit: {
                statu: true,
                data: {
                    pk_accasoa: '',
                    prepareddate: '',
                    pk_accountingbook: '',
                }
            },
            cashflow: true,
            index: '',
            backState: false,
            rowData: {
                status: ''
            }
        };
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                    data.map((item,index)=>{
                        let timeStamp=(item.subjcode?item.subjcode.value:'')+(new Date()).valueOf()+index;//每行数据加上唯一标识 编码+时间戳
                        item.parentTimeStamp=timeStamp;
                        item.timeStamp = timeStamp;
                    })
                    let buttonStatus = self.state.buttonStatus;
                    buttonStatus.rowData=data[0]
                    self.setState({
                        mainData:data,
                        buttonStatus:initButtonStatus,
                        // searchData:data
                    })
                } else {
                    self.setState({
                        mainData:[],
                        buttonStatus:initButtonStatus,
                        // searchData:data
                    })
                }
            }
        });
    }
    getMainData=(data,buttonStatus)=>{
        data = data.map((item,index)=>{
            item.key=index;
            if(!item.localLevel){
                item.localLevel = 0;
            }
            return (item);
        })
        this.setState({
            mainData:data,
            buttonStatus:buttonStatus
        })
    }
    getButtonStatus=(data)=>{
        this.setState({
            buttonStatus:data
        })
    }
    setMainData=(mainData)=>{
        // mainData = mainData.map((item,index)=>{
        //     item.key=index;
        //     return (item);
        // })
        this.setState({
            mainData:mainData
        })
    };
    render(){
        let {mainData,searchData,buttonStatus} = this.state
        return (
            <div className='cashflow-fast-main nc-bill-list'>
                <Buttons getSearchData={this.getSearchData.bind(this)}
                    buttonStatus={buttonStatus} mainData={mainData}
                    getMainData={this.getMainData.bind(this)}
                    pk_unit={searchData.pk_unit&&(searchData.pk_unit.length!=0?this.state.searchData.pk_unit[0]:searchData.pk_unitSearch)}
                    searchData={searchData}
                ></Buttons>
                <FastAnalyseTable mainData={mainData} searchData={searchData}
                    getButtonStatus={this.getButtonStatus.bind(this)}
                    setMainData={this.setMainData.bind(this)}
                ></FastAnalyseTable>
            </div>
        )
    }
}
export default CashFlows;