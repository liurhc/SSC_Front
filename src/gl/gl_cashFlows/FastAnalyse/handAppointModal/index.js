import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,getMultiLang } from 'nc-lightapp-front';
import createScript from '../../../public/components/uapRefer.js';
const { NCModal,NCButton,NCRadio,NCDiv } = base;
const { Refer } = high;
import CashflowTreeRef from '../../../../uapbd/refer/fiacc/CashflowTreeRef';
export default class AppointModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            selectedValue:{
                v1:{refpk:'',refname:''},
                v2:{refpk:'',refname:''}
            }
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
            this.setState({json:json},()=>{
//                 initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'20021CFQANY',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.AppointModalShow;
        this.setState({
            showModal
        })
    }
    close() {
        this.props.closeAppointModal(false);
    }
    handleConfirm=()=>{
        let selectedValue = this.state.selectedValue;
        this.props.getAppointData(selectedValue);
        this.props.closeAppointModal(false);
        selectedValue={
            v1:{refpk:'',refname:''},
            v2:{refpk:'',refname:''}
        }
        this.setState({
            selectedValue
        })
    }
    render () {
        let {pk_accountingbook,period_pk,searchData,selectedValue}=this.state;
        return (
            <div>
              
              <NCModal 
              fieldid="query"
              show = {this.state.showModal}
              onHide={ this.close }
              animation={true}
              >
                <NCModal.Header closeButton fieldid="header-area">
                  <NCModal.Title fieldid={`${this.state.json['20021CFQANY-000023']}_title`}>{this.state.json['20021CFQANY-000023']}</NCModal.Title>
                </NCModal.Header>

                <NCModal.Body >
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}>
                    <div className="handAppoint">
                        <CashflowTreeRef 
                            fieldid="selectedValue"
                            placeholder={this.state.json['20021CFQANY-000021']}
                            value={selectedValue.v1}
                            queryCondition = {() => {
                                return { 
                                    ismain:'Y',
                                    TreeRefActionExt:'nccloud.web.gl.ref.CashFlowTypeRefSqlBuilder',
                                    pk_org:this.props.pk_unit,
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi'
                                }}
                            }
                            onlyLeafCanSelect={true}
                            onChange = {(v)=>{
                                selectedValue.v1.refpk = v.refpk;
                                selectedValue.v1.refname = v.refname;
                                this.setState({
                                    selectedValue
                                })
                            }}
                        />
                        <CashflowTreeRef 
                            fieldid="selectedValue_two"
                            value={selectedValue.v2}
                            placeholder={this.state.json['20021CFQANY-000022']}
                            onlyLeafCanSelect={true}
                            queryCondition = {() => {
                                return { 
                                    ismain:'N',
                                    TreeRefActionExt:'nccloud.web.gl.ref.CashFlowTypeRefSqlBuilder',
                                    pk_org:this.props.pk_unit,
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi'
                                }}
                            }
                            onChange = {(v)=>{
                                selectedValue.v2.refpk = v.refpk;
                                selectedValue.v2.refname = v.refname;
                                this.setState({
                                    selectedValue
                                })
                            }}
                        />
                    </div>
                  </NCDiv>  
                </NCModal.Body>

                <NCModal.Footer fieldid="bottom_area">
                  <NCButton colors='primary' onClick={this.handleConfirm} style={{'margin-right':'6px'}} fieldid="confirm">{this.state.json['20021CFQANY-000024']}</NCButton>          
                  <NCButton onClick={this.close} fieldid="close">{this.state.json['20021CFQANY-000025']}</NCButton>
                </NCModal.Footer>

              </NCModal>
            </div>
        )
    }
}

