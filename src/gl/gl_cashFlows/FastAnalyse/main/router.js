import { asyncComponent } from 'nc-lightapp-front';
import CashFlows from '../index';

// const list = asyncComponent(() => import(/* webpackChunkName: "gl/gl_cashFlows/FastAnalyse/main" *//* webpackMode: "eager" */ '../../../gl_voucher/container/Welcome'));
const routes = [
	{
		path: '/',
		component: CashFlows,
		exact: true
	},
	{
		path: '/cashFlows',
		component: CashFlows
	}
];

export default routes;
