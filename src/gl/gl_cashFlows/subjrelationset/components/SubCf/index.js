import React, { Component } from 'react'
import { createPage, base, ajax, high, deepClone,getMultiLang} from 'nc-lightapp-front'
const { NCModal, NCButton, NCMessage } = base
const { Transfer } = high
import pubUtil from '../../../../public/common/pubUtil'
import './index.less'

class SubCf extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedTreeNode: null, // 左侧树选中行
            rows: [],
            selectedRow: null,
            pk_accountingbook: '',
            selectedTreeNodes: [],
            selectedTreeNodesAll: [],
            selectedTreeNodesCancel: [],
            pks: '',
            targetKeys:[],
            json:{}
        }
        this.status="";
    }

    componentDidMount() {
        //  this.qryData()
    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
    }
    componentWillReceiveProps(nextProps) {
            
        if (nextProps.pk_accountingbook && (JSON.stringify(nextProps.pk_accountingbook)) != "{}") {
            if (nextProps.pk_accountingbook != this.state.pk_accountingbook) {
                this.setState({ pk_accountingbook: nextProps.pk_accountingbook,targetKeys:nextProps.pks }, () => {
                    this.qryData();
                });
            }
        }
        if(nextProps.pks && (JSON.stringify(nextProps.pks)) != "{}"){
            if (nextProps.pks != this.state.targetKeys) {
                this.setState({targetKeys:nextProps.pks });
            }
        }
    }


    // 查询数据
    qryData = () => {
        const { pk_accountingbook } = this.props

        // 查左树
        ajax({
            url: '/nccloud/uapbd/ref/AccountModelRefer.do',
            data: {
                pageInfo: {
                    pageSize: 99999,
                    pageIndex: -1
                },
                isAccountRefer: true,
                queryCondition: {
                    pk_accountingbook: pk_accountingbook,
                    versiondate: pubUtil.getVersionDateStr(),
                    dateStr: pubUtil.getSysDateStr(),
                    isShowUnit: false,
                    "isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi'
                }
            },


            success: (data) => {
                let queryData = ((data.data || {}).rows || {}).treeData || [];
                function add(value) {
                    if (value.children.length > 0) {
                        for (let j = 0; j < value.children.length; j++) {
                            add(value.children[j]);
                        }
                    }
                }
                if (queryData.length > 0) {
                    // queryData.map((one) => {
                    //     one.value = one.refpk;
                    //     if (one.children.length > 0) {
                    //         for (let i = 0; i < one.children.length; i++) {
                    //             add(one.children[i]);
                    //         }
                    //     }
                    // })
                    let deleteDataChildrenProp = function (node) {
                        if (!node.children || node.children.length == 0) {
                            delete node.children;
                        }
                        else {
                            node.children.forEach((e) => {
                                deleteDataChildrenProp(e);
                            });
                        }
                    };
                    queryData.forEach((e) => {
                        deleteDataChildrenProp(e);
                    });
                    // queryData.push({ value: "root", refpk: "root", refcode: "", refname: "会计科目" });
                    // this.props.syncTree.setSyncTreeData('SubTransferTree', queryData);
                    this.setState({ selectedTreeNodes: queryData });
                } else {
                    this.props.syncTree.setSyncTreeData('SubTransferTree', []);
                }
            }
        })
        // 查右表
        ajax({
            url: '/nccloud/gl/cashflow/subCfToSubjQueryAction.do',
            data: {
                pk_accountingbook: pk_accountingbook,
            },
            success: (data) => {
            }
        })
    }
    // convertToTreeData(typeDatas,datas,rule){
    // var treeData = []; //树数据
    // /***
    //  * 获得科目分类的主键数组
    //  * @param array 结果集
    //  * @param typeDatas 2002
    //  */
    // const getTypeDataPks = (typeDatas)=>{
    //     let array = new Array();
    //     typeDatas.map((typeData)=>{
    //         array.push(typeData['pk_acctype']);
    //     })
    //     return array;
    // }
    // 保存
    save = (value) => {
        const {pk_accountingbook} = this.props
        let targetKeys=this.state.targetKeys;
        ajax({
            url: '/nccloud/gl/cashflow/subCfToSubjSaveAction.do',
            data: {
                pk_accountingbook:this.state.pk_accountingbook,
                accountPks:this.state.targetKeys
            },
            success: (data) => {
            }
        })

        this.cancelFormButtonClick.call(this);
    }

    // 记录选中节点
    onSelectEve = (refpk) => {
        let selectedTreeNode = this.props.syncTree.getSyncTreeValue('SubTransferTree', refpk)
        this.setState({
            selectedTreeNode
        })
    }

    selectOne = () => {
        let { selectedTreeNodesAll } = this.state;
        if (selectedTreeNodesAll.length <= 0) {
            //NCMessage.create({ content: this.state.json['20020CFSBG-000011'], color: 'warning', position: 'top' })/* 国际化处理： 还没有选中的节点*/
            toast({color:"warning",content:this.state.json['20020CFSBG-000011']});/* 国际化处理： 还没有选中的节点*/
        } else {
            this.props.syncTree.setSyncTreeData('SubTransferTreeCancel', selectedTreeNodesAll);
        }
        this.state.selectedRow("");
        this.setState({ selectedTreeNodes: selectedTreeNodesAll });
    }
    contain(value) {
        if (pks.indexOf(value)) {
            return true;
        } else {
            return false;
        }
    }

    onCheckEveCancel = (data, refpk) => {
        let { selectedTreeNodesAll, selectedTreeNodes } = this.state;
        let clone = deepClone(selectedTreeNodes);
        let selectedTreeNodesCancel = [];
        let pks = this.state.pks;
        if (refpk.length > 0) {
            for (let i = 0; i < refpk.length; i++) {
                let selectedTreeNode = this.props.syncTree.getSyncTreeValue('SubTransferTree', refpk[i]);
                selectedTreeNodesCancel.push(selectedTreeNode);
                for (let j = 0; j < clone.length; j++) {
                    if (clone[j].refpk == refpk[i]) {
                        clone.splice(j, 1);
                        pks.replace(refpk[i] + ",", "");
                    }
                }
            }
        }
        this.setState({
            selectedTreeNodes: clone, pks: pks, selectedTreeNodesAll: selectedTreeNodesAll,
            selectedTreeNodesCancel: selectedTreeNodesCancel
        });
    }
    onCheckEve = (data, refpk) => {
        let selectedTreeNodes = [];
        let pks = "";
        function add(value) {
            if (value.children) {
                if (value.children.length > 0) {
                    for (let j = 0; j < value.children.length; j++) {
                        add(value.children[j]);
                    }
                } else {
                    value.pid = "";
                    if (pks.indexOf(value.refpk) < 0) {
                        selectedTreeNodes.push(value);
                        pks += value.refpk + ",";
                    }
                }
            } else {
                value.pid = "";
                if (pks.indexOf(value.refpk) < 0) {
                    selectedTreeNodes.push(value);
                    pks += value.refpk + ",";
                }
            }
        }
        if (refpk.length > 0) {
            for (let i = 0; i < refpk.length; i++) {
                let selectedTreeNode = this.props.syncTree.getSyncTreeValue('SubTransferTree', refpk[i]);
                if (selectedTreeNode.children) {
                    if (selectedTreeNode.children.length > 0) {
                        for (let x = 0; x < selectedTreeNode.children.length; x++) {
                            add(selectedTreeNode.children[x]);
                        }
                    } else {
                        selectedTreeNode.pid = "";
                        if (!pks.indexOf(selectedTreeNode.refpk < 0)) {
                            selectedTreeNodes.push(selectedTreeNode);
                            pks += selectedTreeNode.refpk + ",";
                        }
                    }
                } else {
                    selectedTreeNode.pid = "";
                    if (pks.indexOf(selectedTreeNode.refpk) < 0) {
                        selectedTreeNodes.push(selectedTreeNode);
                        pks += selectedTreeNode.refpk + ",";
                    }
                }
            }
        }
        this.setState({
            selectedTreeNodes: selectedTreeNodes,
            selectedTreeNodesAll: selectedTreeNodes,
            pks: this.state.pks + "," + pks
        })
    }
    selectAll = () => {
        let selectedTreeNode = this.props.syncTree.getSyncTreeValue('SubTransferTree');
        let selectedTreeNodes = [];
        let pks = "";
        function add(value) {
            if (value.children) {
                if (value.children.length > 0) {
                    for (let j = 0; j < value.children.length; j++) {
                        add(value.children[j]);
                    }
                } else {
                    value.pid = "";
                    if (pks.indexOf(value.refpk) < 0) {
                        selectedTreeNodes.push(value);
                        pks += value.refpk + ",";
                    }
                }
            } else {
                value.pid = "";
                if (pks.indexOf(value.refpk) < 0) {
                    selectedTreeNodes.push(value);
                    pks += value.refpk + ",";
                }
            }
        }

        for (let i = 0; i < selectedTreeNode.length; i++) {
            if (selectedTreeNode[i].children) {
                if (selectedTreeNode[i].children.length > 0) {
                    for (let x = 0; x < selectedTreeNode[i].children.length; x++) {
                        add(selectedTreeNode[i].children[x]);
                    }
                } else {
                    selectedTreeNode[i].pid = "";
                    if (!pks.indexOf(selectedTreeNode[i].refpk < 0)) {
                        selectedTreeNodes.push(selectedTreeNode[i]);
                        pks += selectedTreeNode[i].refpk + ",";
                    }
                }
            } else {
                selectedTreeNode[i].pid = "";
                if (pks.indexOf(selectedTreeNode[i].refpk) < 0) {
                    selectedTreeNodes.push(selectedTreeNode[i]);
                    pks += selectedTreeNode[i].refpk + ",";
                }
            }
        }
        this.setState({
            selectedTreeNodes: selectedTreeNodes,
            pks: this.state.pks + "," + pks
        })
        this.props.syncTree.setSyncTreeData('SubTransferTreeCancel', selectedTreeNodes);
    }

    deleteOne = () => {
        const { selectedTreeNodes } = this.state
        if (selectedTreeNodes.length <= 0) {
            this.props.syncTree.setSyncTreeData('SubTransferTreeCancel', []);
        } else {
            this.props.syncTree.setSyncTreeData('SubTransferTreeCancel', selectedTreeNodes);
        }
    }

    deleteAll = () => {
        this.setState({ selectedTreeNodes: [] })
        let newTree = this.props.syncTree.createTreeData([]);
        this.props.syncTree.setSyncTreeData('SubTransferTreeCancel', []);

    }
    /**
     * 取消按钮点击
     */
    cancelFormButtonClick() {
        this.props.parent.setState({ [this.props.showFormModalState]: false });
        this.setState({ showFormModal: false, HszbRef: {}, NbjydzgzRef: {}, KmRef: {}, NdValue: '' });
    };

    onTargetKeysChange = (targetKeys) => {
        let flag=true;
        if(targetKeys.length>0){
            if(targetKeys[0].key){
                flag=false;
            }
        }
        if(flag==false){
            targetKeys=[];
        }
        this.setState({
            targetKeys:targetKeys
        });
    };

    render() {

        let { selectedTreeNodesAll, selectedTreeNodes } = this.state;

        const {
            showFormModal,
            // pk_accountingbook, 
            cancel,
            syncTree: { createSyncTree }
        } = this.props

        const {
            onSelectEve,
            selectOne,
            selectAll,
            deleteOne,
            deleteAll,
            onCheckEve
        } = this

        let getShowFlag = () => {
            if (showFormModal) {
                // this.qryData();
                return true;
            } else {
                return false;
            }
        }

        const transferProps = {
            dataSource: this.state.selectedTreeNodes,
            targetKeys: this.state.targetKeys,
            rowKey: 'refpk', // 和默认值相同时可以不指定
            rowTitle: 'refname',
            rowChildren: 'children',
            treeType: true,
            onTargetKeysChange: this.onTargetKeysChange,
            checkable: true,
            className: 'my-transfer-demo',
            showMoveBtn: false,
            listRender: ({ key, title }) => key + ' ' + title
        };


        return (
            <NCModal 
                fieldid="query" 
                show={getShowFlag()}
                id='SubTransfer'
                className='SubTransfer-table'>

            <NCModal.Header fieldid="header-area">
                    <NCModal.Title fieldid={`${this.state.json['20020CFSBG-000018']}_title`}>{this.state.json['20020CFSBG-000018']}</NCModal.Title>{/* 国际化处理： 附表分析科目设置*/}
                 </NCModal.Header>
                 <NCModal.Body>
                    <Transfer  {...transferProps} />
                 </NCModal.Body>
                 <NCModal.Footer fieldid="bottom_area">
                     <NCButton fieldid="save" colors="primary" onClick={this.save.bind(this)} >{this.state.json['20020CFSBG-000019']}</NCButton>{/* 国际化处理： 确定*/}
                     <NCButton fieldid="cancel" onClick={this.cancelFormButtonClick.bind(this)}>{this.state.json['20020CFSBG-000020']}</NCButton>{/* 国际化处理： 取消*/}
                 </NCModal.Footer>
            </NCModal>
        )
    }
}
SubCf = createPage({})(SubCf);
export default SubCf;
