import React, {Component} from 'react';
import {createPage, base, ajax, toast,getBusinessInfo,getMultiLang} from 'nc-lightapp-front';
const { NCModal, NCButton,NCDiv} = base;
import AssVoModalRedner from '../../../../public/components/AssVoModalRedner'
import pubUtil from '../../../../public/common/pubUtil'
class BatchIn extends Component {
    constructor(props) {
        super();
        this.state = {
            clean:false,
            json:{}
        };
        this.clean=false;
        this.formId = 'subjrelation';
        this.status='';
        props.createUIDom(
            {},
            (data) => {
                let meta = data.template;
                meta[this.formId].moduletype = 'form';
                meta[this.formId].items.map((one, index)=>{
                    
                    if(one.attrcode == 'pk_debitsubject' || one.attrcode == 'pk_creditsubject'){
                        one = {...one, ...this.getAccountingbook(props)}
                        meta[this.formId].items[index] = one;
                    }else if(one.attrcode == 'm_dfreevalueid' || one.attrcode == 'm_cfreevalueid'){
                        one = {
                            attrcode: one.attrcode,
                            label: one.label,
                            itemtype: 'refer',
                            visible: one.visible,
                        }
                        meta[this.formId].items[index] = one;
                    }else if(one.attrcode == 'm_isdd' || one.attrcode == 'm_iscd'){
                        one.visible=false;
                    }else if(one.disabled){
                        meta[this.formId].items.splice(index,1);
                    }
                    one.ititemtype="input"
                })
                 props.meta.setMeta(meta); 
                props.renderItem(
                    'form',
                    this.formId,
                    'm_dfreevalueid',
                    <AssVoModalRedner
                        //position="form"
                        quotePosition="form"
                        onRef={this.onRef}
                        getPretentAssData={()=>{return this.getPretentAssData(props, 'm_dfreevalueid')}}
                        doConfirm={(data, display)=>{
                            this.doConfirm(data,"m_dfreevalueid",props,display);
                        }}
                    />
                );
                props.renderItem(
                    'form',
                    this.formId,
                    'm_cfreevalueid',
                    <AssVoModalRedner
                        //position="form"
                        quotePosition="form"
                        onRef={this.onRef}
                        getPretentAssData={()=>{return this.getPretentAssData(props, 'm_cfreevalueid')}}
                        doConfirm={(data, display)=>{
                            this.doConfirm(data,"m_cfreevalueid",props,display);
                        }}
                    />
                );
            }
        )
    }
    onRef=(ref)=>{
        this.child=ref;
    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
    }
    doConfirm(data,attcode,props,display){
        if(display.length > 10){
            display=display.substring(0, 10) + '...';
        }else{
            display= display;
        }
        props.form.setFormItemsValue([this.formId], {[attcode]:{value:data, display: display}});
    }
    getAccountingbook(props){
        if(props.type == 'group'){
            let businessInfo = getBusinessInfo();
            let groupId=businessInfo.groupId;
            return {
                isAccountRefer: false,
                onlyLeafCanSelect:true,
                queryCondition: () => {
                    return {
                        "pk_setofbook": this.props.pk_accountingbook,
                        "versiondate": pubUtil.getVersionDateStr(),
                        dateStr: pubUtil.getSysDateStr(),
                        pk_org:groupId,
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi'
                    }
                }
            }
        }else{
            return {
                isAccountRefer: true,
                onlyLeafCanSelect:true,
                queryCondition: () => {
                    return {
                        "pk_accountingbook": this.props.pk_accountingbook,
                        "versiondate": pubUtil.getVersionDateStr(),
                        dateStr: pubUtil.getSysDateStr(),
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi'
                    }
                }
            }
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.status && (JSON.stringify(nextProps.status)) != "{}") {
            if(nextProps.status!=this.status){
                this.props.form.setFormStatus(this.formId, nextProps.status);
            }
        }
    }
    getPretentAssData(props, key){
        let ref = {};
        if(this.props.parent.editTablePageControllerChild.pk_org){
            ref.pk_org = this.props.parent.editTablePageControllerChild.pk_org;
        }
        ref.pk_accountingbook=props.pk_accountingbook;
        if(key == 'm_dfreevalueid'){
            ref.pk_accasoa= props.form.getFormItemsValue([this.formId], 'pk_debitsubject').value;
        }else if(key == 'm_cfreevalueid'){
            ref.pk_accasoa= props.form.getFormItemsValue([this.formId], 'pk_creditsubject').value;
        }
        ref.checkboxShow=true;
        return ref;
    }
    /**
     * 保存按钮点击
     */
    saveFormButtonClick() { 
        if(this.props.form.isCheckNow(this.formId)){
            let data={};
            let formData = this.props.form.getAllFormValue(this.formId);
            if(formData.rows.length>1){
                toast({color:"warning",content:this.state.json['20020CFSBG-000000']});/* 国际化处理： 辅助核算只能选择一种辅助核算类型*/
                return;
            }
            if(formData.rows.length==1){
                let m_cfreevalueid=formData.rows[0].values.m_cfreevalueid;
                let m_dfreevalueid=formData.rows[0].values.m_dfreevalueid;
                if(m_cfreevalueid.value && m_dfreevalueid.value){
                    toast({color:"warning",content:this.state.json['20020CFSBG-000000']});/* 国际化处理： 辅助核算只能选择一种辅助核算类型*/
                    return;
                }
                if(m_cfreevalueid.value){
                    let cfreevalue=[];
                    if(m_cfreevalueid.value.length>1){
                        for(let x=0;x<m_cfreevalueid.value.length;x++){
                            if(m_cfreevalueid.value){
                                if(m_cfreevalueid.value[x]){
                                    if(m_cfreevalueid.value[x].pk_Checkvalue){
                                        cfreevalue.push(m_cfreevalueid.value[x].pk_Checkvalue);
                                    }
                                }
                            }
                        }
                        if(cfreevalue.length>1){
                            toast({color:"warning",content:this.state.json['20020CFSBG-000001']});/* 国际化处理： 辅助核算项目只能选择一种*/
                            return;
                        }
                        
                    }
                }
                if(m_dfreevalueid.value){
                    let dfreevalue=[];
                    if(m_dfreevalueid.value.length>1){
                        for(let y=0;y<m_dfreevalueid.value.length;y++){
                            if(m_cfreevalueid.value){
                                if(m_cfreevalueid.value[y]){
                                    if(m_dfreevalueid.value[y].pk_Checkvalue){
                                        cfreevalue.push(m_dfreevalueid.value[y].pk_Checkvalue);
                                    }
                                }
                            }
                        }
                        if(dfreevalue.length>1){
                            toast({color:"warning",content:this.state.json['20020CFSBG-000001']});/* 国际化处理： 辅助核算项目只能选择一种*/
                            return;
                        }
                    }
                }
            }
            let dataValue=formData.rows[0].values;
            if(!dataValue.pk_creditsubject.value && !dataValue.pk_debitsubject.value){
                toast({color:"warning",content:this.state.json['20020CFSBG-000002']});/* 国际化处理： 请选择会计科目*/
                return;
            }else{
                if(dataValue.pk_creditsubject.value){
                    if(dataValue.pk_debitsubject.value){
                        toast({color:"warning",content:this.state.json['20020CFSBG-000003']});/* 国际化处理： 批量引入只能选择借方科目或者贷方科目其中一种！*/
                        return;
                    }
                    if(!dataValue.m_cfreevalueid.value){
                        toast({color:"warning",content:this.state.json['20020CFSBG-000004']});/* 国际化处理： 请选择贷方辅助核算*/
                        return;
                    }else{
                        if(dataValue.m_cfreevalueid.value.length>0){
                            if(!dataValue.m_cfreevalueid.value[0].pk_Checkvalue || dataValue.m_cfreevalueid.value[0].pk_Checkvalue.length==0){
                                toast({color:"warning",content:this.state.json['20020CFSBG-000004']});/* 国际化处理： 请选择贷方辅助核算*/
                                return;
                            }
                        }else{
                            toast({color:"warning",content:this.state.json['20020CFSBG-000004']});/* 国际化处理： 请选择贷方辅助核算*/
                            return;
                        }
                    }
                }
                if(dataValue.pk_debitsubject.value){
                    if(dataValue.pk_creditsubject.value){
                        toast({color:"warning",content:this.state.json['20020CFSBG-000003']});/* 国际化处理： 批量引入只能选择借方科目或者贷方科目其中一种！*/
                        return;
                    }
                    if(!dataValue.m_dfreevalueid.value){
                        toast({color:"warning",content:this.state.json['20020CFSBG-000005']});/* 国际化处理： 请选择借方辅助核算*/
                        return;
                    }else{
                        if(dataValue.m_dfreevalueid.value.length>0){
                            if(!dataValue.m_dfreevalueid.value[0].pk_Checkvalue || dataValue.m_dfreevalueid.value[0].pk_Checkvalue.length==0){
                                toast({color:"warning",content:this.state.json['20020CFSBG-000005']});/* 国际化处理： 请选择借方辅助核算*/
                                return;
                            }
                        }else{
                            toast({color:"warning",content:this.state.json['20020CFSBG-000005']});/* 国际化处理： 请选择借方辅助核算*/
                            return;
                        }
                    }
                }
            }
            formData.areaType = 'table';
            formData.rows[0].status = '2';
            let userjson = {

            };
            if(this.props.type == 'group'){
                userjson = {
                    pk_cashflow: this.props.parent.selectTreeId,
                    pk_setofbook: this.props.pk_accountingbook,
                    type:'group'
                };
            }else{
                userjson = {
                    pk_cashflow: this.props.parent.selectTreeId,
                    pk_glorgbook: this.props.pk_accountingbook,
                    type:'org'
                };
            }
            data.userjson = JSON.stringify(userjson);
            data.subjrelation = formData;
            ajax({
                url: '/nccloud/gl/cashflow/subRelationBatchInportAction.do',
                data: data,
                success: (data)=>{
                    toast({color:"success"});
                    this.cancelFormButtonClick();
                }
            });
        }
    }
    /**
     * ȡ����ť���
     */
    cancelFormButtonClick() {
        this.props.parent.editTablePageControllerChild.cfSelectTree(this.props.parent.selectTreeId,this.props.parent.selectTreeId);
        this.props.parent.setState({[this.props.showFormModalState]: false});
        this.setState({showFormModal: false, HszbRef : {}, NbjydzgzRef : {}, KmRef : {}, NdValue: ''});
        this.status='';
    };
    onAfterEvent(props, moduleId, key, value, changedrows, i, s){
        if(key=="pk_creditsubject"){
            let meta=props.meta.getMeta();
            props.form.setFormItemsValue(this.formId, {m_cfreevalueid:{display:null,value:null}});
            // this.child.clear();
        }
        if(key=="pk_debitsubject"){
            // if(!value.value){
                props.form.setFormItemsValue(this.formId, {m_dfreevalueid:{display:null,value:null}});
                // this.child.clear();
            // }
        }
    }
    render(){
        const {form} = this.props;
        const {createForm} = form;
        let doOpen = () => {
            if(this.props.showFormModal){
                return true;
            }else{
                return false;
            }
        }
        return(
            <div id="jsqcye">
                <NCModal 
                    fieldid="query"
                    show={doOpen()} 
                    size="lg" 
                    className='jsqcye-modal' id="jsqcyeya">
                    <NCModal.Header fieldid="header-area">
                        {/* "2002DFKMDZGXSZ-0006": "��������" */}
                        <NCModal.Title fieldid={`${this.state.json['20020CFSBG-000030']}_title`}>{this.state.json['20020CFSBG-000030']}</NCModal.Title>
                    </NCModal.Header>
                    <NCModal.Body>
                        <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}>
                            {createForm(this.formId, {
                                onAfterEvent:this.onAfterEvent.bind(this)
                            })}
                        </NCDiv>
                    </NCModal.Body>
                    <NCModal.Footer fieldid="bottom_area">
                        <NCButton
                            fieldid="save"
                            colors="primary"
                            onClick={this.saveFormButtonClick.bind(this)}
                        >
                            {/* "2002-0010": "ȷ��" */}
                            {this.state.json['2002-0010']}
                        </NCButton>
                        <NCButton onClick={this.cancelFormButtonClick.bind(this)} fieldid="cancel">
                            {/* "2002-0011": "ȡ��" */}
                            {this.state.json['2002-0011']}
                        </NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}
BatchIn = createPage({})(BatchIn);
export default BatchIn;
