import React, {Component} from 'react';
import {createPage, base, ajax,getBusinessInfo,getMultiLang} from 'nc-lightapp-front';
const { NCModal, NCButton,NCDiv} = base;
import AssidModalRedner from '../../../../public/components/assidModalRedner'
import pubUtil from '../../../../public/common/pubUtil'
class QueryCf extends Component {
    constructor(props) {
        super();
        this.state = {
            json:{}
        };
        this.status='';
       
    }
    getAccountingbook(props){
        if(props.type == 'group'){
            let businessInfo = getBusinessInfo();
            let groupId=businessInfo.groupId;
            return {
                isAccountRefer: false,
                onlyLeafCanSelect:true,
                queryCondition: () => {
                    return {
                        "pk_setofbook": this.props.pk_accountingbook,
                        "versiondate": pubUtil.getVersionDateStr(),
                        dateStr: pubUtil.getSysDateStr(),
                        pk_org:groupId,
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi'
                    }
                }
            }
        }else{
            return {
                isAccountRefer: true,
                onlyLeafCanSelect:true,
                queryCondition: () => {
                    return {
                        "pk_accountingbook": this.props.pk_accountingbook,
                        "versiondate": pubUtil.getVersionDateStr(),
                        dateStr: pubUtil.getSysDateStr(),
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi'
                    }
                }
            }
        }

    }
    getPretentAssData(that, props, key){
        let ref = {};
        if(this.props.parent.editTablePageControllerChild.pk_org){
            ref.pk_org = this.props.parent.editTablePageControllerChild.pk_org;
        }
        ref.pk_accountingbook=that.props.pk_accountingbook;
        if(key == 'dAss'){
            ref.assid= props.form.getFormItemsValue('form', 'dAss').value;
            ref.pk_accasoa= props.form.getFormItemsValue('form', 'pk_debitsubject').value;
        }else if(key == 'cAss'){
            ref.assid= props.form.getFormItemsValue('form', 'cAss').value;
            ref.pk_accasoa= props.form.getFormItemsValue('form', 'pk_creditsubject').value;
        }
        return ref;
    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
                this.setMeta(this);
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
    }
    setMeta(_this) {
        _this.props.meta.setMeta({
            form:{
                code:'form',
                moduletype: 'form',
                items: [
                    {
                        label:  _this.state.json['20020CFSBG-000007'],/* 国际化处理： 借方会计科目*/
                        attrcode: 'pk_debitsubject',
                        itemtype: 'refer',
                        refcode: 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js',
                        visible: true,
                        ..._this.getAccountingbook(_this.props)
                    },
                    {
                        label: _this.state.json['20020CFSBG-000008'],/* 国际化处理： 借方辅助核算*/
                        attrcode: 'dAss',
                        visible: true
                    },
                    {
                        label: _this.state.json['20020CFSBG-000009'],/* 国际化处理： 贷方会计科目*/
                        attrcode: 'pk_creditsubject',
                        itemtype: 'refer',
                        refcode: 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js',
                        visible: true,
                        ..._this.getAccountingbook(_this.props)
                    },
                    {
                        label: _this.state.json['20020CFSBG-000010'],/* 国际化处理： 贷方辅助核算*/
                        attrcode: 'cAss',
                        visible: true
                    }
                ]
            }
        });
        window.setTimeout(()=>{
            _this.props.renderItem(
                    'form',
                    'form',
                    'dAss',
                    <AssidModalRedner
                        subjrelationset='subjrelationset'
                        onRef={(ref)=>{ this.child=ref}}
                        position="form"
                        getPretentAssData={()=>{return _this.getPretentAssData(_this, _this.props, 'dAss')}}
                        doConfirm={(data)=>{
                            _this.props.form.setFormItemsValue('form', {dAss:{value:data}});
                        }}
                    />
                );
                _this.props.renderItem(
                'form',
                'form',
                'cAss',
                <AssidModalRedner
                    subjrelationset='subjrelationset'
                    onRef={(ref)=>{ this.child=ref}}
                    position="form"
                    getPretentAssData={()=>{return _this.getPretentAssData(_this, _this.props, 'cAss')}}
                    doConfirm={(data)=>{
                        _this.props.form.setFormItemsValue('form', {cAss:{value:data}});
                    }}
                />
            );
            _this.props.form.setFormStatus('form', 'edit');
        },10)
    }
    
    /**
     * 保存按钮点击
     */
    saveFormButtonClick() { 
        
        if(this.props.form.isCheckNow(this.formId)){
            let data={};
            if(this.props.type == 'group'){
                data.type = 'group';
            }else{
                data.type = 'org';
            }
            // 核算账簿
            data.bookpk = this.props.pk_accountingbook;
            // 贷方
            data.pk_creditsubject = this.props.form.getFormItemsValue('form', 'pk_creditsubject').value;
            data.cfreevalueid = this.props.form.getFormItemsValue('form', 'cAss').value;
            // 借方
            data.pk_debitsubject = this.props.form.getFormItemsValue('form', 'pk_debitsubject').value;
            data.dfreevalueid = this.props.form.getFormItemsValue('form', 'dAss').value;

            ajax({
                url: '/nccloud/gl/cashflow/subRelationQueryCfItemAction.do',
                data: data,
                success: (data)=>{
                    this.cancelFormButtonClick();
                    this.props.saveFormButtonClick(data);
                }
            });


        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.status && (JSON.stringify(nextProps.status)) != "{}") {
            if(nextProps.status!=this.status){
                this.props.form.setFormStatus('form', nextProps.status);
            }
        }
    }
    /**
     * 取消按钮点击
     */
    cancelFormButtonClick() {
        this.props.parent.setState({[this.props.showFormModalState]: false});
        this.setState({showFormModal: false, HszbRef : {}, NbjydzgzRef : {}, KmRef : {}, NdValue: ''});
        this.status='';
    };
    render(){
        const {form} = this.props;
        const {createForm} = form;
        let doOpen = () => {
            if(this.props.showFormModal){
                return true;
            }else{
                return false;
            }
        }
        return(
            <div id="jsqcye">
                <NCModal 
                    fieldid="query"
                    show={doOpen()} 
                    size="lg" 
                    className='jsqcye-modal'
                    id="cashreali">
                    <NCModal.Header fieldid="header-area">
                        {/* "2002DFKMDZGXSZ-0005": "现金流量表项查询" */}
                        <NCModal.Title fieldid={`${this.state.json['20020CFSBG-000029']}_title`}>{this.state.json['20020CFSBG-000029']}</NCModal.Title>
                    </NCModal.Header>
                    <NCModal.Body>
                        <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}  className="nc-theme-form-label-c">
                            {createForm('form', {})}
                        </NCDiv>
                    </NCModal.Body>
                    <NCModal.Footer fieldid="bottom_area">
                        <NCButton
                            fieldid="save"
                            colors="primary"
                            onClick={this.saveFormButtonClick.bind(this)}
                        >
                            {/* "2002-0010": "确定" */}
                            {this.state.json['2002-0010']}
                        </NCButton>
                        <NCButton onClick={this.cancelFormButtonClick.bind(this)} fieldid="cancel">
                            {/* "2002-0011": "取消" */}
                            {this.state.json['2002-0011']}
                        </NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}
QueryCf = createPage({})(QueryCf);
export default QueryCf;
