import React, {Component} from 'react';
import {ajax, base,toast,promptBox,getMultiLang} from 'nc-lightapp-front';
const { NCModal, NCButton, NCMessage,NCDiv} = base;
import ReferLoader from '../../../../public/ReferLoader/index.js';

class CopyTo extends Component {
    constructor(props) {
        super();
        this.state = {
            headRef: [],
            json:{}
        }
    }
    /**
     * 参照变更
     * @param {*} data 
     */
    headRefChange(data){
        this.setState({headRef : data});
    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
    }
    /**
     * 保存按钮点击
     */
    saveFormButtonClick() { 
        let pk_glorgbooks = [];
        let glorgbooks_names = [];
        this.state.headRef.map((one)=>{
            pk_glorgbooks.push(one.refpk);
            glorgbooks_names.push(one.refname);
        })
        ajax({
            url: '/nccloud/gl/cashflow/subRelationCopyToOrgAction.do',
            data: {
                pk_accountingbook: this.props.pk_accountingbook,
                pk_glorgbooks: pk_glorgbooks,
                glorgbooks_names: glorgbooks_names
            },
            success: (data)=>{
                this.props.parent.props.saveFormButtonClick  && this.props.saveFormButtonClick(data);
                let message = data.data;
                let content=this.contents(message);
                this.props.parent.content=content;
                this.props.parent.props.modal.show('copy');
                this.cancelFormButtonClick();
                
            }
        });
    }
    contents(data){
        return  (<div className="nc-theme-form-label-c">{data.map((one)=>{
                   return   <div>
                                {one.bookName}<br/>
                                {one.cfitemname}<br/>
                                {one.subRelDescription}<br/>
                                {one.ret}<br/>{one.reason}<br/>
                            <br/></div>;
                })}</div> ); 
    }
    /**
     * 取消按钮点击
     */
    cancelFormButtonClick() {
        //取消清空参照map
        this.props.parent.setState({[this.props.showFormModalState]: false});
        this.setState({headRef : {}});
    };
    render(){
        return(
            <div>
                <NCModal 
                    className='copy-to-org' 
                    show={this.props.showFormModal} 
                    size="lg" id="copyya" 
                    fieldid="query">
                    <NCModal.Header fieldid="header-area">
                        {/* "2002DFKMDZGXSZ-0004": "复制到" */}
                        <NCModal.Title fieldid={`${this.state.json['20020CFSBG-000028']}_title`}><span>{this.state.json['20020CFSBG-000028']}</span></NCModal.Title>
                    </NCModal.Header>
                    <NCModal.Body>
                    <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                        <span style={{fontSize:'13px',marginRight:'10px'}}>{this.state.json['20020CFSBG-000006']}</span>{/* 国际化处理： 财务核算账簿*/}
                        <ReferLoader
                            fieldid="AccountBookTreeRef"
                            style={{    display: 'inline-block',verticalAlign: 'middle'}}
                            tag={'test'}
                            refcode={'uapbd/refer/org/AccountBookTreeRef/index.js'}
                            disabledDataShow={true}
                            queryCondition={()=>{
                                return {
                                    "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    "appcode": '20020CFSBS',
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
                            value={this.state.headRef}
                            onChange={this.headRefChange.bind(this)}
                            isMultiSelectedEnabled={true}
                        />
                        </NCDiv>
                    </NCModal.Body>
                    <NCModal.Footer fieldid="bottom_area">
                        <NCButton
                            colors="primary"
                            onClick={this.saveFormButtonClick.bind(this)}
                            fieldid="saveForm"
                        >
                            {/* "2002-0010": "确定" */}
                            {this.state.json['2002-0010']}
                        </NCButton>
                        <NCButton onClick={this.cancelFormButtonClick.bind(this)} fieldid="cancelForm">
                            {/* "2002-0011": "取消" */}
                            {this.state.json['2002-0011']}
                        </NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}
export default CopyTo;
