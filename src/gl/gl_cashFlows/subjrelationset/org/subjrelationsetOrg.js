import { Component } from 'react';
import { createPage, base, getBusinessInfo,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const { NCSelect, NCIcon } = base;

import ReferLoader from '../../../public/ReferLoader/index.js';
import editTablePageControllerChild from './events/editTablePageControllerChild';
import QueryCf from '../components/QueryCf';
import CopyToOrg from '../components/CopyToOrg';
import SubCf from '../components/SubCf';
import BatchIn from '../components/BatchIn';

import presetVar from './presetVar';
import requestApi from './requestApi';
import BusinessUnitVersionDefaultAllTreeRef from '../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';
import {Header, HeaderSearchArea, HeaderButtonArea} from '../../../../fipub/public/components/layout/Header'

import './index.less';



class SubjrelationsetOrg extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchAreaDispalyClassName: '',
            headRef: {},
            headRef_disabled: false,
            showCopyToOrgForm: false,
            QueryCfShow: false,
            SubCfShow: false,
            BatchInShow: false,
            pk_accountingbook: '',
            headOrgRef: {},
            isShowUnit: false,
            pks:[],
            json:{}
        };
        this.status='';
        this.selectTreeId = '';
        this.editTablePageControllerChild = new editTablePageControllerChild(this, props);
        this.content='';
        // 页面初始化
        // initTemplate.call(this, props);
    }
    componentDidMount() {

    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
           window.onbeforeunload = () => {
            let status=this.props.editTable.getStatus(presetVar.list);
            if (status == 'edit' || status == 'add') {
                return this.state.json['20020CFSBG-000024']/* 国际化处理： 确定要离开吗？*/
            }
        }
    }
getTableHeight = () => {
    let tableHeight=document.getElementById('app').offsetHeight-100;
    return tableHeight;
}
    render() {
        let multiLang = this.props.MutiInit.getIntl(2002);
        const { editTable, button, DragWidthCom, syncTree,modal } = this.props;
        let { createSyncTree } = syncTree;
        const { createEditTable } = editTable;
        const { createButtonApp } = button;
        let { createModal, show } = modal;
        {/* 左树区域 tree-area*/ }
        let leftDom = () => {
            return (
                <div className="tree-area nc-theme-area-split-bc">
                    {createSyncTree({
                        defaultExpandAll: true,
                        treeId: presetVar.treeId,
                        needEdit: false,
                        needSearch: false,
                        onSelectEve: (data) => { this.editTablePageControllerChild.cfSelectTree(data) }
                    })}
                </div>
            )
        }
        {/* 右区域*/ }
        let rightDom = () => {
            return (
                <div>
                    <div className="table-area">
                        {createEditTable(presetVar.list, {
                            showIndex: false,
                            height:this.getTableHeight(),
                            onAfterEvent: (props, moduleId, key, value, changedrows, record, index) =>
                                this.editTablePageControllerChild.onAfterEvent(props, moduleId, key, value, changedrows, record, index),
                        })}
                    </div>
                </div>
            )
        }
        return (
            <div id="yxysdy" className="nc-single-table">
                <Header>
                    <HeaderSearchArea>
                        <ReferLoader
                            fieldid="AccountBookTreeRef"
                            tag={'test'}
                            refcode={'uapbd/refer/org/AccountBookTreeRef/index.js'}
                            disabledDataShow={true}
                            queryCondition={() => {
                                return {
                                    "TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    "appcode": this.props.getSearchParam('c'),
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
                            disabled={this.state.headRef_disabled}
                            value={this.state.headRef}
                            onChange={(data) => { this.editTablePageControllerChild.cfHeadRefChange(data) }}
                            isMultiSelectedEnabled={false}
                        />
                        {this.state.isShowUnit == true ?
                            <BusinessUnitVersionDefaultAllTreeRef
                                fieldid="headOrgRef"
                                queryCondition={() => {
                                    return {
                                        pk_accountingbook: this.state.pk_accountingbook,
                                        TreeRefActionExt: 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder',
                                        VersionStartDate: getBusinessInfo().businessDate.split(' ')[0],
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi'
                                    }
                                }}
                                disabled={this.state.headRef_disabled}
                                value={this.state.headOrgRef}
                                isMultiSelectedEnabled={false}
                                onChange={(data) => {
                                    this.editTablePageControllerChild.cfHeadOrgRefChange(data);
                                    this.setState({ headOrgRef: data })
                                }}
                            />
                        : ''}
                    </HeaderSearchArea>
                    <HeaderButtonArea>
                        {createButtonApp({
                            area: presetVar.headButtonArea,
                            buttonLimit: 3,
                            onButtonClick: (props, actionId) => { this.editTablePageControllerChild.headButtonClick(actionId) }
                        })}
                    </HeaderButtonArea>
                </Header>
                {/* 树表区域 tree-table*/}
                <div className="tree-table">
                    <DragWidthCom
                        leftDom={leftDom()}     //左侧区域dom
                        rightDom={rightDom()}     //右侧区域dom
                        defLeftWid='280px'      // 默认左侧区域宽度，px/百分百
                    />
                </div>
                <CopyToOrg
                    showFormModal={this.state.showCopyToOrgForm}
                    showFormModalState='showCopyToOrgForm'
                    parent={this}
                    pk_accountingbook={this.state.headRef.refpk}
                />
                <QueryCf
                    type='org'
                    showFormModal={this.state.QueryCfShow}
                    showFormModalState='QueryCfShow'
                    parent={this}
                    status={this.status}
                    pk_accountingbook={this.state.headRef.refpk}
                    saveFormButtonClick={(data) => { this.editTablePageControllerChild.cfDoQueryCf(data) }}
                />
                <SubCf
                    showFormModal={this.state.SubCfShow}
                    showFormModalState='SubCfShow'
                    parent={this}
                    pk_accountingbook={this.state.headRef.refpk}
                    pks={this.state.pks}
                />
                <BatchIn
                    type='org'
                    showFormModal={this.state.BatchInShow}
                    showFormModalState='BatchInShow'
                    parent={this}
                    status={this.status}
                    pk_accountingbook={this.state.headRef.refpk}
                    saveFormButtonClick={(data) => { this.editTablePageControllerChild.cfDoBatchIn(data) }}
                />

                {createModal('copy', {
                    color:'info',
                    title:this.state.json['20020CFSBG-000025'],/* 国际化处理： 复制科目关系报告*/
                    content:this.content,
                    hasCloseBtn:true,//控制“X”按钮，显示true，不显示false，默认不显示
                    size:'xlg',
                    noFooter : true,
                    className:'combine',
                    hasBackdrop:false
                 })}

            </div>
        )
    }
}
SubjrelationsetOrg = createPage({ mutiLangCode: '2002' })(SubjrelationsetOrg);
export default SubjrelationsetOrg;
