import pubUtils from '../../../public/common/pubUtil'
import {ajax } from 'nc-lightapp-front';
import presetVar from './presetVar'
let requestDomain =  '';

let requestApiOverwrite = {
    // 查询接口
    queryTree: (opt) => {
        ajax({
            url: '/nccloud/gl/cashflow/subRelationTreeQuery.do',
            data: {
                type: 'org',
                pk_org:opt.data.queryKey,
                flag:opt.data.flag,
                ...pubUtils.getSysCode(opt.props)
            },
            success: opt.success
        });
    },
    // 查询接口
    queryAccount: (opt) => {
        ajax({
            url: '/nccloud/gl/cashflow/subCfToSubjQueryAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查询接口
    queryData: (opt) => {
        ajax({
            url: '/nccloud/gl/cashflow/subRelationDataQuery.do',
            data: {
                type: 'org',
                pk_cashflow: opt.data.pk_cashflow,
                pk_glorgbook: opt.data.headRefPk,
                pk_org:opt.data.headOrgRef,
                ...pubUtils.getSysCode(opt.props)
            },
            success: opt.success
        });
    },
    // 查询末级科目
    subRelationDataQuery: (opt) => {
        ajax({
            url: '/nccloud/gl/cashflow/SubCfQueryAccasoaAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 保存
    save: (opt) => {
        let paramData = {
            [presetVar.list]:opt.data[presetVar.list],
            ['userjson']: JSON.stringify({
                type:'org', 
                pk_cashflow: opt.data['userjson'].pk_cashflow, 
                pk_glorgbook: opt.data['userjson'].headRefPk,
                pk_org:opt.data['userjson'].headOrgRefPk,
                ...pubUtils.getSysCode(opt.props)
            })
        };
        ajax({
            url: '/nccloud/gl/cashflow/subRelationDataSave.do',
            data: paramData,
            success: opt.success
        });
    },
    // 查询接口
    queryUnit: (opt) => {
        ajax({
            url: '/nccloud/gl/cashflow/isbustartflag.do',
            data:{"pk_accountingbook":opt.data},
            success: opt.success
        });
    },
    // 查询
    queryAccountingBook: (opt) => {
        ajax({
            url: '/nccloud/gl/cashflow/subrelationqueryaccountingbookaction.do',
            data:{"pk_accountingbook":opt.data},
            success: opt.success
        });
    },
}

export default  requestApiOverwrite;
