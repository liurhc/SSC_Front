import {base,deepClone,toast} from 'nc-lightapp-front';
const {NCMessage} = base;
import editTablePageControllerGroup from '../../group/events/editTablePageControllerChild'
import pubUtil from '../../../../public/common/pubUtil'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
import {commonApi} from '../../../../public/common/actions';
class editTablePageControllerChild extends editTablePageControllerGroup{
    constructor(main, props, meta) {
        let newMeta = {
            browseButtons: ['QueryCf', 'SubCf', 'CopyToOrg','BatchIn'],
            editButtons:[]
        };
        super(main, props, newMeta);
    }

    initTemplate(params){
        this.props.createUIDom(
            params,
            (data) => {
                let meta = data.template;
                this.initMeta(meta);
                this.initRefParam(meta);
                window.setTimeout(() => {
                    this.listIds.map((one)=>{
                        if(this.listButtonAreas[one] != null){
                            this.addListOpr(meta, one, this.listButtonAreas[one]);
                        }
                    })
                    this.props.meta.setMeta(meta);
                    this.props.button.setButtons(data.button);
                    this.queryData(data);
                    this.setPageState('browse');
                }, 1);
            }
        )
    }
    /**
     * 设置参照过滤
     * @param {*} meta 
     */
    initRefParam(meta){
        meta[presetVar.list].items.map((one)=>{
            if(one.attrcode == 'pk_debitsubject'||one.attrcode == 'pk_creditsubject'){
                one.isAccountRefer = true;
                one.isShowDisabledData=true;
                one.queryCondition = () => {
                    return {
                        "pk_accountingbook": this.main.state.headRef.refpk,
                        "versiondate": pubUtil.getVersionDateStr(),
                        "dateStr": pubUtil.getSysDateStr(),
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi'
                    };
                }
            }
        })
    }

    queryData(data){
        let queryKey={
            refpk:data.context.defaultAccbookPk,
            refname:data.context.defaultAccbookName

        };
        this.cfHeadRefChange(queryKey);
    }
    /**
     * 取得表格行按钮
     * @param {*} text 
     * @param {*} record 
     * @param {*} index 
     */
    getListButtons(text, record, index){
        if((record.values.canDel || {}).value == false){
            return [];
        }else{
            return ['DelLine'];
        }
        
    }
    /**
     * 保存处理
     * @param {*} data 
     */
    doSave(data){
        let headRefPk= this.main.state.headRef.refpk;
        data['userjson'] = {pk_cashflow: this.main.selectTreeId, headRefPk: headRefPk,headOrgRefPk: this.main.state.headOrgRef.refpk}
        requestApi.save({
            props: this.props,
            data: data,
            success: (data) => {
                this.callBackSave(data);
            }
        })
    }
    /**
     * 删行处理
     * @param {*} data 
     */
    doDelLine(AreaId, data, index){
        let headRefPk= this.main.state.headRef.refpk;
        let headOrgRefPk="";
        if(this.main.state.isShowUnit==true){
            // headRefPk=this.main.state.headOrgRef.refpk;
            headOrgRefPk=this.main.state.headOrgRef.refpk;
        }
        data['userjson'] = {pk_cashflow: this.main.selectTreeId, headRefPk: headRefPk,headOrgRefPk:headOrgRefPk}
        requestApi.save({
            props: this.props,
            data: data,
            success: (data) => {
                this.callBAckDelLine(AreaId, data, index);
            }
        })
    }
    /**
     * 复制到核算账簿
     */
    CopyToOrg(){
        if(!(this.main.state.headRef || {}).refpk){
            let multiLang = this.props.MutiInit.getIntl(2002); 
            // 2002-0005: 请先选择
            toast({color:"warning",content:this.state.json['2002-0005']});
        }else{
            this.main.setState({showCopyToOrgForm: true});
        }
    }
    /**
     * 附表分析科目设置
     */
    SubCf(){
        let pks=[]
        let data={
            pk_accountingbook:this.main.state.headRef.refpk
        }
        requestApi.queryAccount({
            data: data,
            success: (res) => {
                if(res.data){
                    if(res.data.length>0){
                        for(let i =0;i<res.data.length;i++){
                            pks.push(res.data[i].pk_accasoa);
                        }
                    }
                }
                if(!(this.main.state.headRef || {}).refpk){
                    let multiLang = this.props.MutiInit.getIntl(2002); 
                    // 2002-0005: 请先选择
                    toast({color:"warning",content:this.state.json['2002-0005']});
                }else{
                    this.main.setState({SubCfShow: true,pks:pks});
                }
            }
        })
        
    }
    // 自定义功能--------------------------------------------------------------------------
    /**
     * 查询树
     * @param {*} queryKey 
     */
    cfQueryTree(queryKey,flag){
        if(!flag){
            flag="group"
        }
        let queryValue={
            queryKey:queryKey,
            flag:flag
        }
        requestApi.queryTree({
            props: this.props,
            data: queryValue,
            success: (data) => {
                this.cfcallBackQueryTree(data);
            }
        })
        //切换账簿后，清空列表数据
        let emptyData = {
            rows:[]
        };
        this.props.button.setDisabled({
            Add: true,
            Edit:true,
            BatchIn: true
        });
        this.props.editTable.setTableData(presetVar.list, emptyData);
    }
    /**
     * 查询表格数据
     * @param {*} data 
     */
    cfQueryData(data, selectedPk){
        requestApi.queryData({
            props: this.props,
            data: data,
            success: (data) => {
                this.setTableData(data, selectedPk);
            }
        })
    }
    setPageState(state){
        super.setPageState(state);
        switch(state){
            case 'browse':
            break;
            case 'add':
                let allData=this.props.editTable.getAllRows(presetVar.list);
                let values=[];
                if(allData && allData.length>0){
                    for(let i=0;i<allData.length;i++){
                        if((allData[i].values.canDel || {}).value == false){
                            values.push(allData[i].rowid);
                        }
                    }
                    this.props.editTable.setEditableRowByRowId(presetVar.list, values, false);
                }
            break;
            case 'edit': 
                let allData1=this.props.editTable.getAllRows(presetVar.list);
                let values1=[];
                if(allData1 && allData1.length>0){
                    for(let i=0;i<allData1.length;i++){
                        if((allData1[i].values.canDel || {}).value == false){
                            values1.push(allData1[i].rowid);
                        }
                    }
                    this.props.editTable.setEditableRowByRowId(presetVar.list, values1, false);
                }  
            break;
        }
    }
    /**
     * 批量引入
     */
    BatchIn(){
        if(this.main.selectTreeId == ''){
            let multiLang = this.props.MutiInit.getIntl(2002); 
            // 2002-0005: 请先选择
            toast({color:"warning",content:this.state.json['2002-0005']});
        }else{
            this.main.status='edit';
            this.main.setState({BatchInShow:true});
        }
    }

    /**
     * 显示业务单元
     * @param {*} queryKey 
     */
    // setOrg(queryKey){
    //     let key=false;
    //     this.main.setState({pk_accountingbook:queryKey.refpk});
    //     commonApi.queryBookCombineInfo({data:queryKey.refpk, success:(res) => {
    //         res.data.isShowUnit=true;
    //         if(res.data.isShowUnit==true){
    //             this.main.setState({isShowUnit:res.data.isShowUnit});
    //             let refData={
    //                 refpk:res.data.unit.value,
    //                 refname:res.data.unit.display
    //             }
    //             this.main.setState({headOrgRef:refData});
    //             key=true;
    //         }
    //     }})
    //     return key;
    // }

    /**
     * 表头参照变更
     * @param {*} queryKey 
     */
    cfHeadRefChange(queryKey){
        this.main.setState({pk_accountingbook:queryKey.refpk});
        this.main.setState({headRef : queryKey});
        this.main.setState({headOrgRef:{}});
        if(JSON.stringify(queryKey) !='{}'){
            requestApi.queryUnit({
                props: this.props,
                data: queryKey.refpk,
                success: (data) => {
                    if(data.data){
                        if(data.data=="Y"){
                            commonApi.queryBookCombineInfo({data:queryKey.refpk, success:(res) => {
                                // TOP 辅助核算需要传业务单元参数(NCCLOUD-42101) ADD
                                if((res.data || {}).unit){
                                    this.pk_org = res.data.unit.value;
                                }
                                // BTM 辅助核算需要传业务单元参数(NCCLOUD-42101)
                                if(res.data){
                                    this.main.setState({isShowUnit:res.data.isShowUnit});
                                    let refData={
                                        refpk:res.data.unit.value,
                                        refname:res.data.unit.display
                                    }
                                    this.main.setState({headOrgRef:refData});
                                    this.cfHeadOrgRefChange(refData);
                                }else{
                                    if(queryKey != null && queryKey.refpk != null){
                                        this.cfQueryTree(queryKey.refpk);
                                        this.props.button.setDisabled({
                                            QueryCf: false,
                                            SubCf:false,
                                            CopyToOrg:false
                                        });
                                    }else{
                                        this.props.syncTree.setSyncTreeData(presetVar.treeId, []);
                                        this.props.button.setDisabled({
                                            Add: true,
                                            Edit:true,
                                            QueryCf: true,
                                            BatchIn: true,
                                            SubCf:true,
                                            CopyToOrg:true
                                        });
                                    }
                                }
                            }})
                        }else{
                            requestApi.queryAccountingBook({
                                props: this.props,
                                data: queryKey.refpk,
                                success: (res) => {
                                    if(res.data){
                                        this.pk_org=res.data;
                                        this.main.setState({isShowUnit:false});
                                        if(queryKey != null && queryKey.refpk != null){
                                            this.cfQueryTree(queryKey.refpk);
                                            this.props.button.setDisabled({
                                                QueryCf: false,
                                                SubCf:false,
                                                CopyToOrg:false
                                            });
                                        }else{
                                            this.props.syncTree.setSyncTreeData(presetVar.treeId, []);
                                            this.props.button.setDisabled({
                                                Add: true,
                                                Edit:true,
                                                QueryCf: true,
                                                BatchIn: true,
                                                SubCf:true,
                                                CopyToOrg:true
                                            });
                                        } 
                                    }
                                }
                            })
                            
                        }
                    }
                    
                }
            })
        }else{
            this.props.syncTree.setSyncTreeData(presetVar.treeId, []);
            this.props.button.setDisabled({
                Add: true,
                Edit:true,
                QueryCf: true,
                BatchIn: true,
                SubCf:true,
                CopyToOrg:true
            });
        }
        
        
    }
    /**
     * 表头业务单元参照变更
     * @param {*} queryKey 
     */
    cfHeadOrgRefChange(queryKey){
            this.main.setState({headOrgRef : queryKey});
            if(queryKey != null && queryKey.refpk != null){
                // TOP 辅助核算需要传业务单元参数(NCCLOUD-42101) ADD
                this.pk_org = queryKey.refpk;
                // BTM 辅助核算需要传业务单元参数(NCCLOUD-42101)
                this.cfQueryTree(this.main.state.headRef.refpk);
                this.props.button.setDisabled({
                    QueryCf: false,
                    SubCf:false,
                    CopyToOrg:false
                });
            }else{
                this.props.syncTree.setSyncTreeData(presetVar.treeId, []);
                this.props.button.setDisabled({
                    Add: true,
                    Edit:true,
                    QueryCf: true,
                    BatchIn: true,
                    SubCf:true,
                    CopyToOrg:true
                });
            }
    }

    /**
     * 查询数据
     * @param {*} queryKey 
     */
    cfSelectTree(data, selectedPk){
        if(data && data != 'root'){
            this.main.selectTreeId = data;
            let refpk=this.main.state.headRef.refpk;
            if(this.main.state.headOrgRef){

            }
            let paramData = {
                pk_cashflow: data,
                headRefPk: refpk,
                headOrgRef:this.main.state.headOrgRef.refpk
            }
            this.cfQueryData(paramData, selectedPk);
            this.props.button.setDisabled({
                Add: false,
                Edit:false,
                BatchIn: false
            });
        }else{
            this.main.selectTreeId = '';
            this.setTableData({});
            this.props.button.setDisabled({
                Add: true,
                Edit:true,
                BatchIn: true
            });
        }
    }

    onAfterEvent(props, moduleId, key, value, changedrows,index, record){
        if(key=='pk_creditsubject'){
            if(value && JSON.stringify(value)!="{}"){
                let refpk=value.refpk;
                let code=value.code;
                if(value.refcode){
                    code=value.refcode;
                }
                let pk_accountingbook=this.main.state.headRef.refpk;
                let data={
                    "pk_accasoa":code,//科目编码
                    "pk_accountingbook":pk_accountingbook,//核算账簿
                    "flag":"org"
                }
                requestApi.subRelationDataQuery({
                    props: this.props,
                    data: data,
                    success: (res) => {
                        if(res.data){
                            // if(res.data.length>0){
                            //         let datas=[];
                            //         for(let i =0;i<res.data.length;i++){
                            //             let accasoaVo=res.data[i];
                            //             let dataValue=record.values;
                            //             let clone = deepClone(record.values);
                            //             clone.pk_creditsubject={value:accasoaVo.pk_accasoa,display:accasoaVo.code};
                            //             datas.push({values:clone});
                            //         }
                            //         props.editTable.deleteTableRowsByIndex(moduleId, index, false);
                            //         props.editTable.insertRowsAfterIndex(moduleId,datas,index);
                            // }
                            if(res.data.length>0){
                                let datas=[];
                                for(let i =0;i<res.data.length;i++){
                                    let accasoaVo=res.data[i];
                                    let dataValue=record.values;
                                    let clone = deepClone(record.values);
                                    clone.pk_creditsubject={value:accasoaVo.pk_accasoa,display:accasoaVo.code};
                                    if(i==0){
                                        props.editTable.setValByKeyAndIndex(moduleId,index,'pk_creditsubject',clone.pk_creditsubject);
                                        props.editTable.setValByKeyAndIndex(moduleId,index,'m_cfreevalueid',{value:null,display:null});
                                        
                                    }else{     
                                        datas.push({values:clone});
                                    }
                                }
                                // props.editTable.deleteTableRowsByIndex(moduleId, index, false);
                                props.editTable.insertRowsAfterIndex(moduleId,datas,index);
                        }
                        }
                    }
                })
            }
            
        }

        if(key=='pk_debitsubject'){
            if(value && JSON.stringify(value)!="{}"){
                let code=value.code;
                if(value.refcode){
                    code=value.refcode;
                }
                let pk_accountingbook=this.main.state.headRef.refpk;
                let data={
                    "pk_accasoa":code,//科目编码
                    "pk_accountingbook":pk_accountingbook,//核算账簿
                    "flag":"org"
                }
                requestApi.subRelationDataQuery({
                    props: this.props,
                    data: data,
                    success: (res) => {
                        if(res.data){
                            if(res.data.length>0){
                                // if(res.data.length!=1){
                                    let datas=[];
                                    for(let i =0;i<res.data.length;i++){
                                        let accasoaVo=res.data[i];
                                        let dataValue=record.values;
                                        let clone = deepClone(record.values);
                                        clone.pk_debitsubject={value:accasoaVo.pk_accasoa,display:accasoaVo.code};
                                        if(i==0){
                                            props.editTable.setValByKeyAndIndex(moduleId,index,'pk_debitsubject',clone.pk_debitsubject);
                                            props.editTable.setValByKeyAndIndex(moduleId,index,'m_dfreevalueid',{value:null,display:null});
                                        }else{     
                                            datas.push({values:clone});
                                        }
                                        // datas.push({values:clone});
                                    }
                                    // props.editTable.deleteTableRowsByIndex(moduleId, index, false);
                                    props.editTable.insertRowsAfterIndex(moduleId,datas,index);
                                // }
                            }
                        }
                    }
                })
            }
            
        }
    }
}
export default editTablePageControllerChild;
