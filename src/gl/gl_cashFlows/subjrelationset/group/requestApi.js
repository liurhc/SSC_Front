import pubUtils from '../../../public/common/pubUtil'
import {ajax } from 'nc-lightapp-front';
import presetVar from './presetVar'
let requestDomain =  '';

let requestApiOverwrite = {
    // 查询接口
    queryTree: (opt) => {
        ajax({
            url: '/nccloud/gl/cashflow/subRelationTreeQuery.do',
            data: {
                type: 'group',
                pk_glorgbook:opt.data,
                ...pubUtils.getSysCode(opt.props)
            },
            success: opt.success
        });
    },
    // 查询接口
    queryData: (opt) => {
        ajax({
            url: '/nccloud/gl/cashflow/subRelationDataQuery.do',
            data: {
                type: 'group',
                pk_cashflow: opt.data.pk_cashflow,
                pk_setofbook: opt.data.headRefPk,
                ...pubUtils.getSysCode(opt.props)
            },
            success: opt.success
        });
    },
    // 保存
    save: (opt) => {
        let paramData = {
            [presetVar.list]:opt.data[presetVar.list],
            ['userjson']: JSON.stringify({
                type:'group', 
                pk_cashflow: opt.data['userjson'].pk_cashflow, 
                pk_setofbook: opt.data['userjson'].headRefPk,
                ...pubUtils.getSysCode(opt.props)
            })
        };
        ajax({
            url: '/nccloud/gl/cashflow/subRelationDataSave.do',
            data: paramData,
            success: opt.success
        });
    },

     // 查询末级科目
     subRelationDataQuery: (opt) => {
        ajax({
            url: '/nccloud/gl/cashflow/SubCfQueryAccasoaAction.do',
            data: opt.data,
            success: opt.success
        });
    },
}

export default  requestApiOverwrite;
