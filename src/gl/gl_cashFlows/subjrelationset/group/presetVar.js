/**页面全局变量 */
let currentVar = {
    pageId: 'yxysdy',
    list: 'subjrelation',
    treeId: 'tree',
    headButtonArea: 'list_head',
    listButtonArea: 'list_inner',
    listShoulderButtonArea: 'listShoulderButtonArea'
}
window.presetVar = {
    ...currentVar
};
export default currentVar
