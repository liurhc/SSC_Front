import {Component} from 'react';
import {createPage, base,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const {NCSelect, NCIcon} = base;

import SetOfBookGridRef from '../../../../uapbd/refer/org/SetOfBookGridRef';
import editTablePageControllerChild from './events/editTablePageControllerChild'
import QueryCf from '../components/QueryCf'
import BatchIn from '../components/BatchIn'
import presetVar from './presetVar'
import requestApi from './requestApi'
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
import {Header, HeaderSearchArea, HeaderButtonArea} from '../../../../fipub/public/components/layout/Header'



class SubjrelationsetGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchAreaDispalyClassName: '',
            headRef: {},
            headRef_disabled: false,
            QueryCfShow: false,
            BatchInShow: false,
            json:{}
        };
        this.selectTreeId = '';
        this.status='';
        this.editTablePageControllerChild = new editTablePageControllerChild(this, props);
    }
    componentDidMount() {
        
    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
           window.onbeforeunload = () => {
            let status=this.props.editTable.getStatus(presetVar.list);
            if (status == 'edit' || status == 'add') {
                return this.state.json['20020CFSBG-000024']/* 国际化处理： 确定要离开吗？*/
            }
        }
    }
    getTableHeight = () => {
        let tableHeight=document.getElementById('app').offsetHeight-100;
        return tableHeight;
    }
    render() {
        const {editTable, button, DragWidthCom, syncTree} = this.props;
        let {createSyncTree} = syncTree;
        const {createEditTable} = editTable;
        const {createButtonApp} = button;

        {/* 左树区域 tree-area*/}
        let leftDom = () => {
            return(
                <div className="tree-area subj-left-tree">
                    {createSyncTree({
                        defaultExpandAll: true,
                        treeId: presetVar.treeId,
                        needEdit: false,
                        needSearch: false,
                        onSelectEve: (data)=>{this.editTablePageControllerChild.cfSelectTree(data)}
                    })}
                </div>
            )
        }
        {/* 右区域*/}
        let rightDom = () => {
            return (
                <div>
                    <div className="table-area">
                        {createEditTable(presetVar.list, {
                            onAfterEvent: (props, moduleId, key, value, changedrows, index, record)=>{this.editTablePageControllerChild.changeAfter(props,moduleId, key, value, changedrows, index, record)},
                            height:this.getTableHeight(),
                            showIndex:false
                        })}
                    </div>
                </div>
            )
        }
        return (
            <div id="yxysdy" className="nc-single-table">
                <Header>
                    <HeaderSearchArea>
                        <SetOfBookGridRef
                            fieldid="headRef"
                            disabled={this.state.headRef_disabled}
                            value={this.state.headRef}
                            onChange={(data)=>{this.editTablePageControllerChild.cfHeadRefChange(data)}}
                        />
                    </HeaderSearchArea>
                    <HeaderButtonArea>
                        {createButtonApp({
                            area: presetVar.headButtonArea, 
                            buttonLimit: 3,
                            onButtonClick: (props, actionId)=>{this.editTablePageControllerChild.headButtonClick(actionId)}
                        })}
                    </HeaderButtonArea>
                </Header>
                {/* 树表区域 tree-table*/}
                <div className="tree-table">
                    <DragWidthCom
                        leftDom={leftDom()}     //左侧区域dom
                        rightDom={rightDom()}     //右侧区域dom
                        defLeftWid='280px'      // 默认左侧区域宽度，px/百分百
                    />
                </div>
                <QueryCf 
                    type='group'
                    showFormModal={this.state.QueryCfShow}
                    showFormModalState='QueryCfShow'
                    parent={this}
                    pk_accountingbook={this.state.headRef.refpk}
                    status={this.status}
                    saveFormButtonClick={(data)=>{this.editTablePageControllerChild.cfDoQueryCf(data)}}
                />
                <BatchIn 
                    type='group'
                    showFormModal={this.state.BatchInShow}
                    showFormModalState='BatchInShow'
                    parent={this}
                    status={this.status}
                    pk_accountingbook={this.state.headRef.refpk}
                    saveFormButtonClick={(data)=>{this.editTablePageControllerChild.cfDoBatchIn(data)}}
                />
            </div>
        )
    }
}
SubjrelationsetGroup = createPage({})(SubjrelationsetGroup);
export default SubjrelationsetGroup;
