import {base,getBusinessInfo,deepClone,toast} from 'nc-lightapp-front';
const {NCMessage} = base;
import editTablePageController from '../../../../public/common/editTablePageController'
import AssidModalRedner from '../../../../public/components/assidModalRedner'
import pubUtil from '../../../../public/common/pubUtil'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
import {commonApi} from '../../../../public/common/actions';
class editTablePageControllerChild extends editTablePageController{
    constructor(main, props, meta) {
        let newMeta = {
            ...meta,
            headRefDisabled: (meta || {}).headRefDisabled || 'headRef_disabled',
            treeId: (meta || {}).treeId || [presetVar.treeId],
            listIds: (meta || {}).listIds || [presetVar.list],
            listButtonAreas: (meta || {}).listButtonAreas || {[presetVar.list]:presetVar.listButtonArea},
            browseButtons: (meta || {}).browseButtons || ['QueryCf','BatchIn'],
            editButtons: (meta || {}).editButtons || []
        };
        super(main, props, newMeta);
        // TOP 辅助核算需要传业务单元参数(NCCLOUD-42101) ADD
        this.pk_org = null;
        this.flag=false;
        // BTM 辅助核算需要传业务单元参数(NCCLOUD-42101)
    }

    queryData(data){

    }
    /**
     * 初始化模板
     * @param {*} meta 
     */
    initMeta(meta){
        meta[presetVar.list].items.map((one, index)=>{
            // 借方辅助核算
            if(one.attrcode == 'm_dfreevalueid' || one.attrcode == 'm_cfreevalueid'){
                one = {
                     label: one.label,
                     itemtype: 'refer',
                     attrcode: one.attrcode,
                     visible: one.visible || false,
                     width: one.width,
                     render: (text, record, index) =>{
                         return (
                             <AssidModalRedner
                                subjrelationset='subjrelationset'
                                onRef={(ref)=>{ this.child=ref}}
                                getPretentAssData={()=>{ return this.getPretentAssData(one.attrcode, record, index)}}
                                doConfirm={(assid, data, display)=>{return this.cfAssidModalRednerDoConfirm(assid, data, one.attrcode, index, display)}}
                                flag={this.flag}
                             />
                         );
                     }
                 }
                 meta[presetVar.list].items[index] = one;
            }
            if(one.attrcode == 'm_isdd' || one.attrcode == 'm_iscd'){
                meta[presetVar.list].items[index] = one;
            }
        })
        this.props.button.setDisabled({
            Add: true,
            Edit:true,
            QueryCf: true,
            BatchIn: true,
            SubCf:true,
            CopyToOrg:true
        });
    }
    /**
     * 取得辅助核算弹窗数据
     * @param {*} key 
     * @param {*} record 
     * @param {*} index 
     */
    getPretentAssData(key, record, index){
        let ref = {};
        // TOP 辅助核算需要传业务单元参数(NCCLOUD-42101) ADD
        if(this.pk_org){
            ref.pk_org = this.pk_org;
        }
        // ref.pk_org ='0001A210000000003E4U';
        // BTM 辅助核算需要传业务单元参数(NCCLOUD-42101)
        ref.pk_accountingbook=this.main.state.headRef.refpk;
        // ref.pk_accountingbook='';
        if(key == 'm_dfreevalueid'){
            ref.assid = (record.values.m_dfreevalueid||{}).value;
            ref.pk_accasoa= (record.values.pk_debitsubject||{}).value;
        }else if(key == 'm_cfreevalueid'){
            ref.assid = (record.values.m_cfreevalueid||{}).value;
            ref.pk_accasoa= (record.values.pk_creditsubject||{}).value;
        }
        ref.checkboxShow=true;
        return ref;
    }
    /**
     * 设置参照过滤
     * @param {*} meta 
     */
    initRefParam(meta){
        meta[presetVar.list].items.map((one)=>{
            if(one.attrcode == 'pk_debitsubject'||one.attrcode == 'pk_creditsubject'){
                let businessInfo = getBusinessInfo();
                let groupId=businessInfo.groupId;
                one.isAccountRefer = false;
                one.queryCondition = () => {
                    return {
                        "pk_setofbook": this.main.state.headRef.refpk,
                        "versiondate": pubUtil.getVersionDateStr(),
                        dateStr: pubUtil.getSysDateStr(),
                        pk_accountingbook:'',
                        pk_org:groupId,
                        "isDataPowerEnable": 'Y',
                        // "DataPowerOperationCode" : 'fi'
                    };
                }
            }
        })
    }
    /**
     * 重写新增方法，增加树选择校验
     */
    Add(){
        if(this.main.selectTreeId == ''){
            let multiLang = this.props.MutiInit.getIntl(2002); 
            toast({color:"warning",content:this.state.json['2002-0005']});
        }else{
            super.Add();
        }
    }
    /**
     * 重写修改方法，增加树选择校验
     */
    Edit(){
        if(this.main.selectTreeId == ''){
            let multiLang = this.props.MutiInit.getIntl(2002); 
            toast({color:"warning",content:this.state.json['2002-0005']});
        }else{
            super.Edit();
        }
    }    

    /**
     * 保存处理
     * @param {*} data 
     */
    doSave(data){
        data['userjson'] = {pk_cashflow: this.main.selectTreeId, headRefPk: this.main.state.headRef.refpk}
        requestApi.save({
            props: this.props,
            data: data,
            success: (data) => {
                this.callBackSave(data);
            }
        })
    }
    /**
     * 删行处理
     * @param {*} data 
     */
    doDelLine(AreaId, data, index){
        data['userjson'] = {pk_cashflow: this.main.selectTreeId, headRefPk: this.main.state.headRef.refpk}
        requestApi.save({
            props: this.props,
            data: data,
            success: (data) => {
                this.callBAckDelLine(AreaId, data, index);
            }
        })
    }
    /**
     * 现金流量表项查询
     */
    QueryCf(){
        if(!(this.main.state.headRef || {}).refpk){
            let multiLang = this.props.MutiInit.getIntl(2002); 
            // 2002-0005: 请先选择
            toast({color:"warning",content:this.state.json['2002-0005']});
        }else{
            this.main.setState({QueryCfShow:true});
            this.main.status='edit';
        }
    }
    /**
     * 批量引入
     */
    BatchIn(){
        if(this.main.selectTreeId == ''){
            let multiLang = this.props.MutiInit.getIntl(2002); 
            // 2002-0005: 请先选择
            toast({color:"warning",content:this.state.json['2002-0005']});
        }else{
            this.main.status='edit';
            this.main.setState({BatchInShow:true});
            
        }
    }
    /**
     * 重写设置数据增加默认选择项
     * @param {*} data 
     * @param {*} selectedPk 
     */
    setTableData(data, selectedPk){
        if((data.data || {})[presetVar.list] != null){
            data.data[presetVar.list].rows.map((one)=>{
                one.values.cAss.value = JSON.stringify(one.values.cAss.value);
                one.values.dAss.value = JSON.stringify(one.values.dAss.value);
            })
        }
        super.setTableData(data);
        if(selectedPk){
            window.setTimeout(()=>{
                let tableDatas = this.props.editTable.getAllRows(presetVar.list, true);
                if(tableDatas){
                    tableDatas.map((one, index)=>{
                        if(one.values.pk_subrelation.value == selectedPk){
                            this.props.editTable.focusRowByIndex(presetVar.list, index);
                        }
                    })
                }
            },10);
        }
    }
    // 自定义功能--------------------------------------------------------------------------

    cfSaveFormButtonClick(data){
        
    }

    /**
     * 辅助核算确定事件
     */
    cfAssidModalRednerDoConfirm(assid, data, attrcode, index, display){
        let ass = [];
        let assId=[];
        let displayname="";
        let flag=false;
        data.data.map((one)=>{
            if(one && flag==false){
                if(one.pk_Checkvalue){
                    displayname += '【'+ one.checktypename+':'+(one.checkvaluename?one.checkvaluename:'~')+'】';
                    ass.push({
                        m_checktypecode: one.checktypecode,
                        m_checktypename: one.checktypename,
                        m_checkvaluecode: one.checkvaluecode,
                        m_checkvaluename: one.checkvaluename,
                        m_pk_checktype: one.pk_Checktype,
                        m_pk_checkvalue: one.pk_Checkvalue,
                        m_classid: one.m_classid
                    });
                    // continue;
                }else{
                    toast({color:"danger",content:this.main.state.json['20020CFSBG-000021']+one.checktypename+this.main.state.json['20020CFSBG-000022']});/* 国际化处理：  辅助核算,不允许为空！*/
                    flag=true;
                }
                
            }
        })
        
        if(flag==false){
            this.props.editTable.setValByKeyAndIndex(presetVar.list, index, attrcode, {value: assid, display: display, isEdit: true});
            if(attrcode == 'm_cfreevalueid'){
                this.props.editTable.setValByKeyAndIndex(presetVar.list, index, 'cAss', {value: JSON.stringify(ass), isEdit: true});
            }else{
                this.props.editTable.setValByKeyAndIndex(presetVar.list, index, 'dAss', {value: JSON.stringify(ass), isEdit: true});
            }
            if(this.props.editTable.getRowStatus(presetVar.list, index) == '0'){
                this.props.editTable.setRowStatus(presetVar.list, index, '1');
            }
        }
        // window.setTimeout(()=>{
           
        // },10);
        return flag;
    }

    /**
     * 现金流量表项查询
     * @param {*} data 
     */
    cfDoQueryCf(data){
        if((((data.data || {}).subjrelation || {}).rows || []).length>0){
            let pk_cashflow = null;
            let pk_subrelation = null;
            if((((data.data || {}).subjrelation || {}).rows || []).length==1){
                pk_cashflow = data.data.subjrelation.rows[0].values.pk_cashflow.value;
                pk_subrelation = data.data.subjrelation.rows[0].values.pk_subrelation.value;
            }else{
                for(let i=0;i<(((data.data || {}).subjrelation || {}).rows || []).length;i++){
                    let pk_accountingbook = data.data.subjrelation.rows[i].values.pk_glorgbook;
                    if(pk_accountingbook.value){
                        pk_cashflow = data.data.subjrelation.rows[i].values.pk_cashflow.value;
                        pk_subrelation = data.data.subjrelation.rows[i].values.pk_subrelation.value;
                    }
                }
                
            }
            this.props.syncTree.setNodeSelected(presetVar.treeId, pk_cashflow);
            this.cfSelectTree(pk_cashflow, pk_subrelation);
            this.props.editTable.selectTableRows(presetVar.list, [0], true);
        }else{
            toast({color:"danger",content:this.main.state.json['20020CFSBG-000023']})/* 国际化处理： 没有找到对应的科目关系设置项！*/
        }
    }
    /**
     * 表头参照变更
     * @param {*} queryKey 
     */
    cfHeadRefChange(queryKey){
        this.main.setState({headRef : queryKey},()=>{
            if(queryKey != null && queryKey.refpk != null){
                this.cfQueryTree(queryKey.refpk);
                this.props.button.setDisabled({
                    QueryCf: false,
                    SubCf:false,
                    CopyToOrg:false
                });
            }else{
                this.props.syncTree.setSyncTreeData(presetVar.treeId, []);
                this.props.button.setDisabled({
                    Add: true,
                    Edit:true,
                    BatchIn: true
                });
            }
            this.setOrg(queryKey);

        });
        // this.initRefParam(this.props.meta.getMeta());
    }
    /**
     * 显示业务单元
     * @param {*} queryKey 
     */
    setOrg(queryKey){
        
    }
    /**
     * 查询树
     * @param {*} queryKey 
     */
    cfQueryTree(queryKey){
        requestApi.queryTree({
            props: this.props,
            data: queryKey,
            success: (data) => {
                this.cfcallBackQueryTree(data);
            }
        })
        //切换账簿后，清空列表数据
        let emptyData = {
            rows:[]
        };
        this.props.button.setDisabled({
            Add: true,
            Edit:true,
            BatchIn: true,
        });
        this.props.editTable.setTableData(presetVar.list, emptyData);
    }
    /**
     * 查询树回掉
     * @param {*} data 
     */
    cfcallBackQueryTree(data){
        let newTree = this.props.syncTree.createTreeData(data.data.nodes); 
        this.props.syncTree.setSyncTreeData(presetVar.treeId, newTree);
    }
    /**
     * 查询数据
     * @param {*} queryKey 
     */
    cfSelectTree(data, selectedPk){
        if(data && data != 'root'){
            this.main.selectTreeId = data;
            let paramData = {
                pk_cashflow: data,
                headRefPk: this.main.state.headRef.refpk
            }
            this.cfQueryData(paramData, selectedPk);
            this.props.button.setDisabled({
                Add: false,
                Edit:false,
                QueryCf: false,
                BatchIn: false,
                SubCf:false,
                CopyToOrg:false
            });
        }else{
            this.main.selectTreeId = '';
            this.setTableData({});
            this.props.button.setDisabled({
                Add: true,
                Edit:true,
                BatchIn: true,
            });
        }
    }
    /**
     * 查询表格数据
     * @param {*} data 
     */
    cfQueryData(data, selectedPk){
        requestApi.queryData({
            props: this.props,
            data: data,
            success: (data) => {
                this.setTableData(data, selectedPk);
            }
        })
    }

    changeAfter(props, moduleId, key, value, changedrows,index, record){
        if(key=='pk_creditsubject'){
            if(value && JSON.stringify(value)!="{}"){
                let pk_setofbook=this.main.state.headRef.refpk;
                
                let queryCondition = {
                    pk_accountingbook:'', // 核算账簿（组织级必传）
                    pk_setofbook:pk_setofbook,    // 账簿类型（政策性科目参照必传）
                    pk_org:'',         //政策性科目集团级必传，全局传空
                    dateStr:'' ,
                    flag:"group",
                    pk_defdoclist:null,
                    DataPowerOperationCode:null,
                    isDataPowerEnable:true,
                    isShowUnit:false,
                    "isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi'
                }
                let businessInfo = getBusinessInfo();
                let buziDate = businessInfo.businessDate;
                let groupId=businessInfo.groupId;
                if(buziDate){
                    queryCondition.dateStr = buziDate.split(' ')[0];
                }
                queryCondition.pk_org=groupId;
                let code=value.code;
                if(value.refcode){
                    code=value.refcode;
                }
                let data={
                    pid:"",
                    keyword:"",
                    queryCondition:queryCondition,
                    pageInfo:{"pageSize":10,"pageIndex":-1},
                    isQuickSearch:false,
                    isAccountRefer:false,
                    flag:"group",
                    pk_accasoa:code
                }
                requestApi.subRelationDataQuery({
                    props: this.props,
                    data: data,
                    success: (res) => {
                        if(res.data){
                            if(res.data.length>0){
                                // if(res.data.length!=1){
                                    let datas=[];
                                    for(let i =0;i<res.data.length;i++){
                                        let accasoaVo=res.data[i];
                                        let dataValue=record.values;
                                        let clone = deepClone(record.values);
                                        clone.pk_creditsubject={value:accasoaVo.pk_accasoa,display:accasoaVo.code};
                                        if(i==0){
                                            props.editTable.setValByKeyAndIndex(moduleId,index,'pk_creditsubject',clone.pk_creditsubject);
                                            props.editTable.setValByKeyAndIndex(moduleId,index,'m_cfreevalueid',{value:null,display:null});
                                            
                                        }else{     
                                            datas.push({values:clone});
                                        }
                                        // datas.push({values:clone});
                                    }
                                    // props.editTable.deleteTableRowsByIndex(moduleId, index, false);
                                    props.editTable.insertRowsAfterIndex(moduleId,datas,index);
                                // }
                            }
                        }
                    }
                })
            }
            
        }

        if(key=='pk_debitsubject'){
            if(value && JSON.stringify(value)!="{}"){
                let pk_setofbook=this.main.state.headRef.refpk;
                let queryCondition = {
                    pk_accountingbook:'', // 核算账簿（组织级必传）
                    pk_setofbook:pk_setofbook,    // 账簿类型（政策性科目参照必传）
                    pk_org:'',         //政策性科目集团级必传，全局传空
                    dateStr:'' ,
                    flag:"group",
                    pk_defdoclist:null,
                    DataPowerOperationCode:null,
                    isDataPowerEnable:true,
                    isShowUnit:false,
                    "isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi'
                }
                let businessInfo = getBusinessInfo();
                let buziDate = businessInfo.businessDate;
                let groupId=businessInfo.groupId;
                if(buziDate){
                    queryCondition.dateStr = buziDate.split(' ')[0];
                }
                let code=value.code;
                if(value.refcode){
                    code=value.refcode;
                }
                queryCondition.pk_org=groupId;
                let data={
                    pid:"",
                    keyword:"",
                    queryCondition:queryCondition,
                    pageInfo:{"pageSize":10,"pageIndex":-1},
                    isQuickSearch:false,
                    isAccountRefer:false,
                    flag:"group",
                    pk_accasoa:code
                }
                requestApi.subRelationDataQuery({
                    props: this.props,
                    data: data,
                    success: (res) => {
                        if(res.data){
                            if(res.data.length>0){
                                // if(res.data.length!=1){
                                    let datas=[];
                                    for(let i =0;i<res.data.length;i++){
                                        let accasoaVo=res.data[i];
                                        let dataValue=record.values;
                                        let clone = deepClone(record.values);
                                        clone.pk_debitsubject={value:accasoaVo.pk_accasoa,display:accasoaVo.code};
                                        if(i==0){
                                            props.editTable.setValByKeyAndIndex(moduleId,index,'pk_debitsubject',clone.pk_debitsubject);
                                            props.editTable.setValByKeyAndIndex(moduleId,index,'m_dfreevalueid',{value:null,display:null});
                                        }else{     
                                            datas.push({values:clone});
                                        }
                                        // datas.push({values:clone});
                                    }
                                    // props.editTable.deleteTableRowsByIndex(moduleId, index, false);
                                    props.editTable.insertRowsAfterIndex(moduleId,datas,index);
                                // }
                            }
                        }
                    }
                })
            }
            
        }
    }
}
export default editTablePageControllerChild;
