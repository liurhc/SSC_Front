import React, { Component } from "react";
import {high,base,ajax,getMultiLang,promptBox,toast } from 'nc-lightapp-front';
const { 
    NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,
    NCRadio,NCBreadcrumb:Breadcrumb,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCDiv
    } = base;
import {InputItem,} from '../../../public/components/FormItems';
const NCOption = NCSelect.NCOption;

export default class cashAnalyseTable extends Component {
  constructor(props){
    super(props)
    this.state={
      json:{},
      tableData:[],//表格数据
      checkedAll:false,//是否全选
      selectData:[],//选中数据
      selIds:[],//选中行号
      checkedArray: [//各行选中判断
        
      ],
      selectedRowIndex:0,
      saveEdit:false,
      searchData:{},
      columns:[]
    }
  }
  componentWillMount(){
    let callback= (json) =>{
         this.setState({json:json,
        columns:[
          {
            title: (<div fieldid="prepareddate" className="mergecells">{json['20020CFANY-000021']}</div>),
            dataIndex: "prepareddate",
            key: "prepareddate",
            width:'120px',
            render: (text, record, index) => (
              <div fieldid="prepareddate">
                {this.state.tableData[index].prepareddate.display?this.state.tableData[index].prepareddate.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid="vouchno" className="mergecells">{json['20020CFANY-000022']}</div>),
            dataIndex: "vouchno",
            key: "vouchno",
            width:'120px',
            render: (text, record, index) => (
              <div fieldid="vouchno">
                {this.state.tableData[index].vouchno.display?this.state.tableData[index].vouchno.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid="explanation" className="mergecells">{json['20020CFANY-000023']}</div>),
            dataIndex: "explanation",
            key: "explanation",
            width:'120px',
            render: (text, record, index) => (
              <div fieldid="explanation">
                {this.state.tableData[index].explanation.display?this.state.tableData[index].explanation.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid="pk_accasoa" className="mergecells">{json['20020CFANY-000024']}</div>),
            dataIndex: "pk_accasoa",
            key: "pk_accasoa",
            width:'200px',
            render: (text, record, index) => (
              <div fieldid="pk_accasoa">
                {this.state.tableData[index].pk_accasoa.display?this.state.tableData[index].pk_accasoa.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid="assid" className="mergecells">{json['20020CFANY-000025']}</div>),
            dataIndex: "assid",
            key: "assid",
            width:'150px',
            render: (text, record, index) => (
              <div fieldid="assid">
                {this.state.tableData[index].assid.display?this.state.tableData[index].assid.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid="localdebitamount" className="mergecells">{json['20020CFANY-000026']}</div>),
            dataIndex: "localdebitamount",
            key: "localdebitamount",
            width:'120px',
            className:'t-a-r',
            render: (text, record, index) => (
              <div fieldid="localdebitamount">
                {this.state.tableData[index].localdebitamount.display?this.state.tableData[index].localdebitamount.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid="localcreditamount" className="mergecells">{json['20020CFANY-000027']}</div>),
            dataIndex: "localcreditamount",
            key: "localcreditamount",
            width:'120px',
            className:'t-a-r',
            render: (text, record, index) => (
              <div fieldid="localcreditamount">
                {this.state.tableData[index].localcreditamount.display?this.state.tableData[index].localcreditamount.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid="debitamount" className="mergecells">{json['20020CFANY-000028']}</div>),
            dataIndex: "debitamount ",
            key: "debitamount ",
            className:'t-a-r',
            width:'120px',
            render: (text, record, index) => (
              <div fieldid="debitamount">
                {this.state.tableData[index].debitamount.display?this.state.tableData[index].debitamount.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid="creditamount" className="mergecells">{json['20020CFANY-000029']}</div>),
            dataIndex: "creditamount",
            key: "creditamount",
            className:'t-a-r',
            width:'120px',
            render: (text, record, index) => (
              <div fieldid="creditamount">
                {this.state.tableData[index].creditamount.display?this.state.tableData[index].creditamount.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid="analyzeType" className="mergecells">{json['20020CFANY-000030']}</div>),
            dataIndex: "analyzeType",
            key: "analyzeType",
            width:'120px',
            render: (text, record, index) => (
              <NCSelect
                fieldid="analyzeType"
                value={this.state.tableData[index].analyzetype.value}
                onKeyDown={(e)=>{
                  e.stopPropagation();
                  e.preventDefault();
                }}
                onChange={(v)=>{
                  let pk_voucher = this.state.tableData[index].pk_voucher.value;
                  this.props.getMainData(pk_voucher,v);
                }}
              >
                <NCOption value={json['20020CFANY-000031']}>{json['20020CFANY-000031']}</NCOption>
                <NCOption value={json['20020CFANY-000032']}>{json['20020CFANY-000032']}</NCOption>
                <NCOption value={json['20020CFANY-000033']}>{json['20020CFANY-000033']}</NCOption>
                <NCOption value={json['20020CFANY-000034']}>{json['20020CFANY-000034']}</NCOption>
                <NCOption value="">{json['20020CFANY-000040']}</NCOption>
            </NCSelect>
            )
          },
          {
            title: (<div fieldid="pk_currtype" className="mergecells">{json['20020CFANY-000002']}</div>),
            dataIndex: "pk_currtype",
            key: "pk_currtype",
            width:'120px',
            render: (text, record, index) => (
              <div fieldid="pk_currtype">
                {this.state.tableData[index].pk_currtype.display?this.state.tableData[index].pk_currtype.display:<span>&nbsp;</span>}
              </div>
            )
          }
        ]
      },()=>{ })
          }
      getMultiLang({moduleId:'20020CFANY',domainName:'gl',currentLocale:'simpchn',callback}); 
  }
  componentWillReceiveProps(nextProps){
    let tableData = nextProps.mainData;
    let saveEdit = nextProps.saveEdit;
    let checkedArray = nextProps.checkedArray;
    let selIds = nextProps.selIds;
    this.setState({
      checkedArray,selIds,saveEdit
    })
    if(!checkedArray[0]&&checkedArray.length){
      this.setState({
        checkedAll:false
      })
    }
    let columns = this.state.columns.length?this.state.columns:
    [
      {
        title: (<div fieldid="prepareddate" className="mergecells">{this.state.json['20020CFANY-000021']}</div>),
        dataIndex: "prepareddate",
        key: "prepareddate",
        width:'120px',
        render: (text, record, index) => (
          <div fieldid="prepareddate">
            {this.state.tableData[index].prepareddate.display?this.state.tableData[index].prepareddate.display:<span>&nbsp;</span>}
          </div>
        )
      },
      {
        title: (<div fieldid="vouchno" className="mergecells">{this.state.json['20020CFANY-000022']}</div>),
        dataIndex: "vouchno",
        key: "vouchno",
        width:'120px',
        render: (text, record, index) => (
          <div fieldid="vouchno">
            {this.state.tableData[index].vouchno.display?this.state.tableData[index].vouchno.display:<span>&nbsp;</span>}
          </div>
        )
      },
      {
        title: (<div fieldid="explanation" className="mergecells">{this.state.json['20020CFANY-000023']}</div>),
        dataIndex: "explanation",
        key: "explanation",
        width:'120px',
        render: (text, record, index) => (
          <div fieldid="explanation">
            {this.state.tableData[index].explanation.display?this.state.tableData[index].explanation.display:<span>&nbsp;</span>}
          </div>
        )
      },
      {
        title: (<div fieldid="pk_accasoa" className="mergecells">{this.state.json['20020CFANY-000024']}</div>),
        dataIndex: "pk_accasoa",
        key: "pk_accasoa",
        width:'200px',
        render: (text, record, index) => (
          <div fieldid="pk_accasoa">
            {this.state.tableData[index].pk_accasoa.display?this.state.tableData[index].pk_accasoa.display:<span>&nbsp;</span>}
          </div>
        )
      },
      {
        title: (<div fieldid="assid" className="mergecells">{this.state.json['20020CFANY-000025']}</div>),
        dataIndex: "assid",
        key: "assid",
        width:'150px',
        render: (text, record, index) => (
          <div fieldid="assid">
            {this.state.tableData[index].assid.display?this.state.tableData[index].assid.display:<span>&nbsp;</span>}
          </div>
        )
      },
      {
        title: (<div fieldid="localdebitamount" className="mergecells">{this.state.json['20020CFANY-000026']}</div>),
        dataIndex: "localdebitamount",
        key: "localdebitamount",
        width:'120px',
        className:'t-a-r',
        render: (text, record, index) => (
          <div fieldid="localdebitamount">
            {this.state.tableData[index].localdebitamount.display?this.state.tableData[index].localdebitamount.display:<span>&nbsp;</span>}
          </div>
        )
      },
      {
        title: (<div fieldid="localcreditamount" className="mergecells">{this.state.json['20020CFANY-000027']}</div>),
        dataIndex: "localcreditamount",
        key: "localcreditamount",
        width:'120px',
        className:'t-a-r',
        render: (text, record, index) => (
          <div fieldid="localcreditamount">
            {this.state.tableData[index].localcreditamount.display?this.state.tableData[index].localcreditamount.display:<span>&nbsp;</span>}
          </div>
        )
      },
      {
        title: (<div fieldid="debitamount" className="mergecells">{this.state.json['20020CFANY-000028']}</div>),
        dataIndex: "debitamount ",
        key: "debitamount ",
        className:'t-a-r',
        width:'120px',
        render: (text, record, index) => (
          <div fieldid="debitamount">
            {this.state.tableData[index].debitamount.display?this.state.tableData[index].debitamount.display:<span>&nbsp;</span>}
          </div>
        )
      },
      {
        title: (<div fieldid="creditamount" className="mergecells">{this.state.json['20020CFANY-000029']}</div>),
        dataIndex: "creditamount",
        key: "creditamount",
        className:'t-a-r',
        width:'120px',
        render: (text, record, index) => (
          <div fieldid="creditamount">
            {this.state.tableData[index].creditamount.display?this.state.tableData[index].creditamount.display:<span>&nbsp;</span>}
          </div>
        )
      },
      {
        title: (<div fieldid="analyzeType" className="mergecells">{this.state.json['20020CFANY-000030']}</div>),
        dataIndex: "analyzeType",
        key: "analyzeType",
        width:'120px',
        render: (text, record, index) => (
          <NCSelect
            fieldid="analyzeType"
            value={this.state.tableData[index].analyzetype.value}
            onChange={(v)=>{
              let pk_voucher = this.state.tableData[index].pk_voucher.value;
              this.props.getMainData(pk_voucher,v);
            }}
          >
            <NCOption value={this.state.json['20020CFANY-000031']}>{this.state.json['20020CFANY-000031']}</NCOption>
            <NCOption value={this.state.json['20020CFANY-000032']}>{this.state.json['20020CFANY-000032']}</NCOption>
            <NCOption value={this.state.json['20020CFANY-000033']}>{this.state.json['20020CFANY-000033']}</NCOption>
            <NCOption value={this.state.json['20020CFANY-000034']}>{this.state.json['20020CFANY-000034']}</NCOption>
            <NCOption value="">{this.state.json['20020CFANY-000040']}</NCOption>
        </NCSelect>
        )
      },
      {
        title: (<div fieldid="pk_currtype" className="mergecells">{this.state.json['20020CFANY-000002']}</div>),
        dataIndex: "pk_currtype",
        key: "pk_currtype",
        width:'120px',
        render: (text, record, index) => (
          <div fieldid="pk_currtype">
            {this.state.tableData[index].pk_currtype.display?this.state.tableData[index].pk_currtype.display:<span>&nbsp;</span>}
          </div>
        )
      }
    ]
    ;
    for(let i=0,len=tableData.length;i<len;i++){
      tableData[i].key=i;
    }
    this.setState({
      tableData,
      columns
    })
    //添加判断是否第一次加载，是否存在业务单元，是否增加了业务单元
    if(nextProps.searchData){
      if(nextProps.searchData.pk_unitState!=this.state.searchData.pk_unitState||
        nextProps.searchData.NC001!=this.state.searchData.NC001||
        nextProps.searchData.NC002!=this.state.searchData.NC002
      ){
        if(nextProps.searchData.pk_unitState&&columns[3].dataIndex!='pk_unit'){//业务单元
          columns.splice(3, 0, {
            title: (<div fieldid="pk_unit" className="mergecells">{this.state.json['20020CFANY-000006']}</div>),
            dataIndex: "pk_unit",
            key: "pk_unit",
            width:'120px',
            render: (text, record, index) => (
              <div fieldid="pk_unit">
                  {this.state.tableData[index].pk_unit.display?this.state.tableData[index].pk_unit.display:<span>&nbsp;</span>}
              </div>
            )
          });
        }else{
          for(let i=0,len=columns.length;i<len;i++){
            if(columns[i].dataIndex=='pk_unit'){
              columns.splice(i, 1);
              return
            }
          }
        }
        if(columns[3].dataIndex!='pk_unit'){//集团本币是否显示
          if(nextProps.searchData.NC001&&columns[7].dataIndex!='groupdebitamount'){
            columns.splice(7, 0, {
              title: (<div fieldid="groupdebitamount" className="mergecells">{this.state.json['20020CFANY-000035']}</div>),
              dataIndex: "groupdebitamount",
              key: "groupdebitamount",
              width:'120px',
              className:'t-a-r',
              render: (text, record, index) => (
                <div fieldid="groupdebitamount">
                  {this.state.tableData[index].groupdebitamount.display?this.state.tableData[index].groupdebitamount.display:<span>&nbsp;</span>}
                </div>
              )
            },
            {
              title: (<div fieldid="groupcreditamount" className="mergecells">{this.state.json['20020CFANY-000036']}</div>),
              dataIndex: "groupcreditamount",
              key: "groupcreditamount",
              width:'120px',
              className:'t-a-r',
              render: (text, record, index) => (
                <div fieldid="groupcreditamount">
                  {this.state.tableData[index].groupcreditamount.display?this.state.tableData[index].groupcreditamount.display:<span>&nbsp;</span>}
                </div>
              )
            });
          }else if(!nextProps.searchData.NC001){
            for(let i=0,len=columns.length;i<len;i++){
              if(columns[i].dataIndex=='groupdebitamount'){
                columns.splice(i, 1);
                len--
              }
              if(columns[i].dataIndex=='groupcreditamount'){
                columns.splice(i, 1);
                len--
              }
            }
          }
        }else if(columns[3].dataIndex=='pk_unit'){
          if(nextProps.searchData.NC001&&columns[8].dataIndex!='groupdebitamount'){
            columns.splice(8, 0, {
              title: (<div fieldid="groupdebitamount" className="mergecells">{this.state.json['20020CFANY-000035']}</div>),
              dataIndex: "groupdebitamount",
              key: "groupdebitamount",
              className:'t-a-r',
              width:'120px',
              render: (text, record, index) => (
                <div fieldid="groupdebitamount">
                  {this.state.tableData[index].groupdebitamount.display?this.state.tableData[index].groupdebitamount.display:<span>&nbsp;</span>}
                </div>
              )
            },
            {
              title: (<div fieldid="groupcreditamount" className="mergecells">{this.state.json['20020CFANY-000036']}</div>),
              dataIndex: "groupcreditamount",
              key: "groupcreditamount",
              className:'t-a-r',
              width:'120px',
              render: (text, record, index) => (
                <div fieldid="groupcreditamount">
                  {this.state.tableData[index].groupcreditamount.display?this.state.tableData[index].groupcreditamount.display:<span>&nbsp;</span>}
                </div>
              )
            });
          }else if(!nextProps.searchData.NC001){
            for(let i=0,len=columns.length;i<len;i++){
              if(columns[i].dataIndex=='groupdebitamount'){
                columns.splice(i, 1);
                len--
              }
              if(columns[i].dataIndex=='groupcreditamount'){
                columns.splice(i, 1);
                len--
              }
            }
          }
        }
        let moneyglobalNum ;
        if(columns[3].dataIndex!='pk_unit'){//集团本币是否显示
          if(columns[7].dataIndex!='groupdebitamount'){
            moneyglobalNum=7
          }else if(columns[7].dataIndex=='groupdebitamount'){
            moneyglobalNum=9
          }
        }else if(columns[3].dataIndex=='pk_unit'){
          if(columns[8].dataIndex!='groupdebitamount'){
            moneyglobalNum=8
          }else if(columns[8].dataIndex=='groupdebitamount'){
            moneyglobalNum=10
          }
        }
        if(nextProps.searchData.NC002&&columns[moneyglobalNum].dataIndex!='globaldebitamount'){
          columns.splice(moneyglobalNum, 0, {
            title: (<div fieldid="globaldebitamount" className="mergecells">{this.state.json['20020CFANY-000037']}</div>),
            dataIndex: "globaldebitamount",
            key: "globaldebitamount",
            className:'t-a-r',
            width:'120px',
            render: (text, record, index) => (
              <div fieldid="globaldebitamount">
                {this.state.tableData[index].globaldebitamount.display?this.state.tableData[index].globaldebitamount.display:<span>&nbsp;</span>}
              </div>
            )
          },
          {
            title: (<div fieldid="globalcreditamount" className="mergecells">{this.state.json['20020CFANY-000038']}</div>),
            dataIndex: "globalcreditamount",
            key: "globalcreditamount",
            className:'t-a-r',
            width:'120px',
            render: (text, record, index) => (
              <div fieldid="globalcreditamount">
                {this.state.tableData[index].globalcreditamount.display?this.state.tableData[index].globalcreditamount.display:<span>&nbsp;</span>}
              </div>
            )
          });
        }else if(!nextProps.searchData.NC002){
          for(let i=0,len=columns.length;i<len;i++){
            if(columns[i].dataIndex=='globalcreditamount'){
              columns.splice(i, 1);
              len--
            }
            if(columns[i].dataIndex=='globaldebitamount'){
              columns.splice(i, 1);
              len--
            }
          }
        }
      }
      
    }
    this.setState({
      searchData:Object.assign({},nextProps.searchData),
      checkedArray,
      columns
    })
  }
  // shouldComponentUpdate(nextProps,nextState){
  //   if(nextProps.mainData.length==this.state.tableData){
  //     return false
  //   }else{
  //     return true
  //   }
  // }
  clickRow=(data,index)=>{
    let saveEdit = this.state.saveEdit;
    if(!saveEdit){
      this.props.getTableDetail(data);      
    }else{  
      // toast({ content: this.state.json['20020CFANY-000039'], color: 'warning' });
      promptBox({
        color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
        title: this.state.json['20020CFANY-000014'],                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
        content: this.state.json['20020CFANY-000039'],             // 提示内容,非必输
        beSureBtnName: this.state.json['20020CFANY-000016'],          // 确定按钮名称, 默认为"确定",非必输
        cancelBtnName: this.state.json['20020CFANY-000017'],         // 取消按钮名称, 默认为"取消",非必输
        hasCloseBtn:false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
        beSureBtnClick: ()=>{
          this.props.getTableDetail(data,'1');
        },   // 确定按钮点击调用函数,非必输
        cancelBtnClick: ()=>{
          this.props.getTableDetail(data,'2');
        },   // 确定按钮点击调用函数,非必输
      })
      // this.props.getSaveEdit(false)
      // return;
    }
    this.setState({
      selectedRowIndex: data.key
    })
  }
  handleChangeType=(v)=>{
  }
  getSelectedDataFunc=(data)=>{
    this.props.getSelectArr(data)
  }

  //处理多选
  onAllCheckChange = () => {//全选
    let self = this;
    let checkedArray = [];
    let listData = self.state.tableData.concat();
    let selIds = [];
    // let id = self.props.multiSelect.param;
    for (var i = 0; i < self.state.checkedArray.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
      selIds.push(i);
    }
    if(self.state.checkedAll){
      selIds=[]
    }
    self.setState({
      checkedAll: !self.state.checkedAll
    });
    self.props.setCheckedArray(checkedArray);
    self.props.setCheckedselIds(selIds);
    let tableData = this.state.tableData;
    let selectArr = [];
    
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(tableData[selIds[i]])));
    }
    this.props.getSelectArr(selectArr);
    // self.props.getSelectData(selIds);
  };
  onCheckboxChange = (text, record, index) => {//单选
    let self = this;
    let allFlag = false;
    let selIds = self.state.selIds;
    // let id = self.props.postId;
    let checkedArray = self.state.checkedArray.concat();
    if (self.state.checkedArray[index]) {
      selIds.splice(record.key,1);
      for(let i=0,len=selIds.length;i<len;i++){
        if(record.key==selIds[i]){
          selIds.splice(i,1)
          break;
        }
      }
    } else {
      selIds.push(record.key);
    }
    checkedArray[index] = !self.state.checkedArray[index];
    for (let i = 0; i < self.state.checkedArray.length; i++) {
      if (!checkedArray[i]) {
        allFlag = false;
        break;
      } else {
        allFlag = true;
      }
    }
    self.setState({
      checkedAll: allFlag
    });
    self.props.setCheckedArray(checkedArray);
    self.props.setCheckedselIds(selIds);
    let tableData = this.state.tableData;
    let selectArr = [];
    selIds = this.sortarr(selIds);
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(tableData[selIds[i]])));
    }
    this.props.getSelectArr(selectArr);
  };
  renderColumnsMultiSelect(columns) {
    const { data,checkedArray,tableData } = this.state;
    let { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // multiSelect= "checkbox";
    // let indeterminate_bool1 = true;
    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArray.length;
      while(i--){
          if(checkedArray[i]){
            indeterminate_bool = true;
            break;
          }
      }
      let defaultColumns = [
        {
          title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
            <Checkbox
              className="table-checkbox"
              checked={this.state.checkedAll}
              indeterminate={indeterminate_bool&&!this.state.checkedAll}
              onChange={this.onAllCheckChange}
            />
          }</div>),
          key: "checkbox",
          attrcode: "checkbox",
          dataIndex: "checkbox",
          width: "50px",
          render: (text, record, index) => {
            return (
              <div fieldid="firstcol">
              <Checkbox
                className="table-checkbox"
                checked={this.state.checkedArray[index]}
                onChange={this.onCheckboxChange.bind(this, text, record, index)}
              />
              </div>
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
  sortarr=(arr)=>{//排序
    for(let i=0;i<arr.length-1;i++){
        for(let j=0;j<arr.length-1-i;j++){
            if(arr[j]>arr[j+1]){
                var temp=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
    return arr;
}

  render() {
    let {busrelation,tableData}=this.state;
    let columns = this.renderColumnsMultiSelect(this.state.columns);
    return (
        <NCDiv fieldid="cashAnalyse" areaCode={NCDiv.config.TableCom}>
            <Table
                columns={columns}
                data={tableData}
                onRowClick={this.clickRow}
                scroll={{ x:true, y:240 }}
                rowClassName={(record,index,indent)=>{
                  if (this.state.selectedRowIndex == index) {
                    return 'nctable-selected-row';
                  }else{
                    return '';
                  }
                }}
            />
        </NCDiv>
      
    );
  }
}
cashAnalyseTable.defaultProps = {
  prefixCls: "bee-table",
  multiSelect: {
    type: "checkbox",
    param: "key"
  }
};
