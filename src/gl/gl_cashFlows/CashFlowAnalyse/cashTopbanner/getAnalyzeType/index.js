import React, { Component } from 'react';
import {high,base,ajax,createPage,cardCache,getMultiLang } from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
const { 
    NCFormControl: FormControl,NCButton: Button,
    NCRadio,NCRow:Row,NCCol:Col,NCTree:Tree,
    NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,
	NCCheckbox:Checkbox,NCModal:Modal,NCDatePicker,NCDiv
    } = base;
const { Refer } = high;
const NCOption = NCSelect.NCOption;
import AccountBookTreeRef from '../../../../gl_cashFlows/pk_book/refer_pk_book';
class AnalyzeTypeModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            selectedType:''
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json,selectedType:this.state.json['20020CFANY-000031']/* 国际化处理： 不分析*/},()=>{ })
            }
        getMultiLang({moduleId:'20020CFANY',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.analyzeTypeShow
        this.setState({
            showModal
        })
    }
    close=()=>{
        this.props.setAnalyzeTypeShow(false);
    }
    handleConfirm=()=>{
        let selectedType = this.state.selectedType;
        this.props.setAnalyzeTypeShow(false);
        this.props.setAnalyzeTypeFa(selectedType);
    }
    render () {
        let {showModal,selectedType} = this.state;
        return (
        <div className='log-search-modal'>
            {/* <Button onClick={()=>{
                this.setState({
                    showModal:true
                })
            }} colors="primary">this.state.json['20020CFANY-000065']</Button> */}
            <Modal
            fieldid="query"
            show = { showModal }
            // size = 'lg'
            className='log-search-con cashflow-small-modal-a'
            onHide = { this.close } >
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20020CFANY-000065']}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                        <Col lg={3} sm={3} xs={3} style={{'line-height':'30px'}}>
                            {this.state.json['20020CFANY-000030']}
                        </Col>
                        <Col lg={9} sm={9} xs={9}>
                            <NCSelect
                                fieldid="selectedType"
                                value={selectedType}
                                onChange={(v)=>{
                                    selectedType = v;
                                    this.setState({
                                        selectedType
                                    })
                                }}
                            >
                                <NCOption value={this.state.json['20020CFANY-000031']}>{this.state.json['20020CFANY-000031']}</NCOption>
                                <NCOption value={this.state.json['20020CFANY-000032']}>{this.state.json['20020CFANY-000032']}</NCOption>
                                <NCOption value={this.state.json['20020CFANY-000033']}>{this.state.json['20020CFANY-000033']}</NCOption>
                                <NCOption value={this.state.json['20020CFANY-000034']}>{this.state.json['20020CFANY-000034']}</NCOption>
                                <NCOption value="">{this.state.json['20020CFANY-000040']}</NCOption>
                            </NCSelect>
                        </Col>
                    </NCDiv>
                </Modal.Body>

                <Modal.Footer fieldid="bottom_area">
                    <Button fieldid="confirm" onClick={ this.handleConfirm } style={{'min-width':'60px','font-size':'13px'}} style={{marginRight: 6}} colors="primary">{this.state.json['20020CFANY-000016']}</Button>
                    <Button firldid="close" onClick={ this.close } style={{'min-width':'60px','font-size':'13px'}} >{this.state.json['20020CFANY-000017']}</Button>
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
AnalyzeTypeModal = createPage({
	// initTemplate: initTemplate
})(AnalyzeTypeModal);
export default AnalyzeTypeModal;
