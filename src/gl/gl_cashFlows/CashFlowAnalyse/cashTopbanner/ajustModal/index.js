import React, { Component } from 'react';
import {high,ajax,base,createPage,getMultiLang } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCButtonGroup:ButtonGroup,NCModal:Modal,NCDiv
    } = base;
const { Refer } = high;
import { toast } from '../../../../public/components/utils.js';
import createScript from '../../../../public/components/uapRefer.js';
import CashflowTreeRef from '../../../../../uapbd/refer/fiacc/CashflowTreeRef';
import AccountBookTreeRef from '../../../pk_book/refer_pk_book';
class AjustModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            pk_accountingbook:{
                display:'',
                value:''
            },
            saveData:[{
                key:1,
                srcpk:{display:'',value:''},
                despk:{display:'',value:''}
            }],
            selectItem:{},
            pk_org:'',
        };
        this.columns =[];
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ 
            this.columns= [
                { title: (<div fieldid="key" className="mergecells">{this.state.json['20020CFANY-000041']}</div>),
                 dataIndex: "key",
                  key: "key",
                   width: 100,
                   render: (text, record, index) => {
                       return <div fieldid="key">{text}</div>
                   } 
                },
                { title: (<div fieldid="srcpk" className="mergecells">{this.state.json['20020CFANY-000042']}</div>),
                 dataIndex: "srcpk",
                  key: "srcpk",
                   width:150,
                render: (text, record, index) => (
                    <CashflowTreeRef
                        fieldid="srcpk"
                        value =  {{
                            refname: this.state.saveData[index].srcpk.display, 
                            refpk: this.state.saveData[index].srcpk.value
                        }}
                        queryCondition={{
                            "pk_accountingbook": this.state.pk_accountingbook.value,
                            "pk_org": this.state.pk_org,
                            isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi'                 
                        }}
                        onChange={(v)=>{
                            let {saveData} = this.state;
                            saveData[index].srcpk.display = v.refname;	
                            saveData[index].srcpk.value = v.refpk;	
                            this.setState({
                                saveData
                            })                       
                        }} 
                    />
                  )
                },
                { title: (<div fieldid="despk" className="mergecells">{this.state.json['20020CFANY-000043']}</div>),
                 dataIndex: "despk",
                  key: "despk",
                   width:150,
                render: (text, record, index) => (
                    <CashflowTreeRef
                        fieldid="despk"
                        value =  {{
                            refname: this.state.saveData[index].despk.display, 
                            refpk: this.state.saveData[index].despk.value
                        }}
                        onlyLeafCanSelect={true}
                        queryCondition={{
                            "pk_accountingbook": this.state.pk_accountingbook.value,
                            "pk_org": this.state.pk_org,
                            isDataPowerEnable: 'Y',
                            DataPowerOperationCode: 'fi',
                            pk_cashflow:this.state.saveData[index].srcpk.value,
                            'TreeRefActionExt':'nccloud.web.gl.cashflow.action.CashFLowSonRefSqlBuilder',
                            
                        }}
                        onChange={(v)=>{
                            let {saveData} = this.state;
                            saveData[index].despk.display = v.refname;	
                            saveData[index].despk.value = v.refpk;	
                            this.setState({
                                saveData
                            })                       
                        }} 
                    />
                  )
                },
                { title: (<div fieldid="descz" className="mergecells">{this.state.json['20020CFANY-000005']}</div>),
                 dataIndex: "descz",
                  key: "descz",
                   width:100,
                render: (text, record, index) => (
                    <a shape='border' fieldid="descz"
                        onClick={this.handleDel.bind(this,record, index)}
                        style={{'margin':'0'}}
                    >{this.state.json['20020CFANY-000009']}</a>
                  )
                }
              ]
        })
            }
        getMultiLang({moduleId:'20020CFANY',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.ajustModalShow;
        this.setState({
            showModal
        })
    }
    close() {
        this.props.getAjustShow(false)
    }
    componentDidMount(){
        let url = '/nccloud/platform/appregister/queryappcontext.do';
        let data={
            appcode: this.props.getSearchParam('c')
        }
        let self=this;
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                   let pk_accountingbook={
                        display:data.defaultAccbookName,
                        value:data.defaultAccbookPk
                    }
                   self.setState({
                    pk_accountingbook
                   })
                    ajax({
                        url:'/nccloud/gl/glpub/queryFinanceOrg.do',
                        data:{"pk_accountingbook":pk_accountingbook.value,'needaccount':false},
                        success: function(response) { 
                            const { data, error, success } = response;
                            if (success&&data) {
                                self.setState({
                                    pk_org:data.pk_org
                                })
                            } else {
                                
                            }    
                        }
                    });
                } else {
                }
            }
        });
    }
    handleAdd=()=>{
        let {saveData}=this.state;
        let obj = {
            key:saveData.length+1,
            srcpk:{display:'',value:''},
            despk:{display:'',value:''}
        }
        
        saveData = [...saveData,JSON.parse(JSON.stringify(obj))];
        this.setState({
            saveData
        })
    }
    handleDel=(record,index)=>{
        let {saveData}=this.state;
        saveData.map((item,key)=>{
            if(item.key==record.key){
                saveData.splice(key,1)
            } 
        })
        this.setState({
            saveData
        })
    }
    handleConfirm=()=>{
        let {pk_accountingbook,saveData}=this.state;
        let localArr = [];
        let obj={}
        saveData.map((item,key)=>{
            obj={srcpk:item.srcpk.value,despk:item.despk.value}
            localArr.push(JSON.parse(JSON.stringify(obj)))
        })
        if(!pk_accountingbook.value){
            toast({ content: this.state.json['20020CFANY-000096'], color: 'warning' });
            return;
        }
        if(localArr.length==0){
            toast({ content: this.state.json['20020CFANY-000097'], color: 'warning' });
            return;
        }
        let data={
            pk_accountingbook:pk_accountingbook.value,
            adjust:localArr
        }
        let self = this;
        let url = "/nccloud/gl/cashflow/adjustcf.do";
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                self.props.getAjustShow(false)
                if (success) {
                    toast({ content: self.state.json['20020CFANY-000044'], color: 'success' });
                } else {
                    // self.setState({
                    //     detailTableData:[]
                    // })
                    // toast({ content: self.state.json['20020CFANY-000045'], color: 'warning' });
                }
                // self.props.getBack(self.state.detailTableData);
            }
        });
    }
    clickRow=(data)=>{
        this.setState({
            selectItem:data
        })
    }
    
    render () {
        let {pk_accountingbook,saveData}=this.state;
        let columns=this.columns;
        return (
        <div>
            <Modal
            fieldid="query"
            show = { this.state.showModal }
            className='cashflow-small-modal-a cashflow-small-modal-c'
            onHide = { this.close } >
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20020CFANY-000046']}</Modal.Title>
                </Modal.Header>
                <Modal.Body style={{'max-height':'325px'}}>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}  className="nc-theme-form-label-c">
                    <Row style={{'margin-top':'-10px','padding-bottom':'5px'}}> 
                        {/* <Col lg={2} sm={2} xs={2}>财务核算账簿：</Col> */}
                        <Col lg={10} sm={10} xs={10} style={{'padding-left':'20px'}} >
                            <span style={{color:'red',position: 'absolute',left: '22px',top: '0px','z-index': '2'}}>*</span>
                            <AccountBookTreeRef
                                fieldid="pk_accountingbook"
                                value={{'refname': pk_accountingbook.display, refpk: pk_accountingbook.value}}
                                disabledDataShow={true}
                                autoFocus={true}
                                queryCondition={{
                                    "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    "appcode": this.props.getSearchParam('c'),
                                }}
                                onChange={(v)=>{
                                    pk_accountingbook.value=v.refpk
                                    pk_accountingbook.display=v.refname
                                    this.setState({
                                        pk_accountingbook:pk_accountingbook
                                    })
                                    let self =this;
                                    ajax({
                                        url:'/nccloud/gl/glpub/queryFinanceOrg.do',
                                        data:{"pk_accountingbook":v.refpk,'needaccount':false},
                                        success: function(response) { 
                                            const { data, error, success } = response;
                                            if (success&&data) {
                                                self.setState({
                                                    pk_org:data.pk_org
                                                })
                                            } else {
                                                
                                            }    
                                        }
                                    });
                                }}
                            />
                        </Col>
                        <Col style={{'line-height':"25px",'float':'right','padding-right':'20px'}}>
                            <Button fieldid="add" onClick={this.handleAdd} style={{'min-width':'60px','font-size':'13px'}}>{this.state.json['20020CFANY-000099']}</Button>
                        </Col>
                    </Row>
                    <NCDiv fieldid="ajust" areaCode={NCDiv.config.TableCom}>
                        <Table
                            columns={columns}
                            data={saveData}
                            bordered
                            onRowClick={this.clickRow}
                        />
                    </NCDiv>
                    </NCDiv>
                </Modal.Body>

                <Modal.Footer fieldid="bottom_area">
                    <Button fieldid="confirm" onClick={ this.handleConfirm } style={{marginRight: 6,'min-width':'60px','font-size':'13px'}} colors="primary">{this.state.json['20020CFANY-000016']}</Button>
                    <Button fieldid="close" onClick={ this.close } style={{'min-width':'60px','font-size':'13px'}}>{this.state.json['20020CFANY-000017']}</Button>
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
AjustModal = createPage({})(AjustModal);
export default AjustModal;
