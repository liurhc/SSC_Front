import React, { Component } from 'react';
import { ajax,base,getMultiLang,promptBox,toast }  from 'nc-lightapp-front';
const { NCRadio, NCForm, NCFormControl,NCCheckbox,NCModal:Modal,NCRow:Row,NCCol:Col,NCButton:Button,NCDiv } = base;
const NCFormItem = NCForm.NCFormItem;
export default class AnalyseModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            selectedData:{
                analsyistype:1,
                analsyisscope:2,
                amountmatch:'N'
            },
            selectArr:[],
            pk_details:[]
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
        getMultiLang({moduleId:'20020CFANY',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.analyseModalShow;
        let mainData=nextProps.mainData;
        let selectArr = nextProps.selectArr;
        let arrLocal=[],localArr=[],details=[],detailObj={};
        for(let i=0,len=mainData.length;i<len;i++){
            let obj = {
                pk_voucher:mainData[i].pk_voucher.value,
                analyzetype:mainData[i].analyzetype.value
            }
            arrLocal.push(JSON.parse(JSON.stringify(obj)));
        }
        for(let i=0,len=selectArr.length;i<len;i++){
            let obj = {
                pk_voucher:selectArr[i].pk_voucher.value,
                analyzetype:selectArr[i].analyzetype.value
            }
            localArr.push(JSON.parse(JSON.stringify(obj)));
            detailObj={
                pk_detail:selectArr[i].pk_detail.value,
                pk_voucher:selectArr[i].pk_voucher.value
            }
            details.push(JSON.parse(JSON.stringify(detailObj)))
        }
        this.setState({
            mainData:arrLocal,
            selectArr:localArr,
            showModal,
            pk_details:details
        })
    }
    close() {
        this.props.getAnalyseShow(false);
    }
    handleAnalyse=()=>{
        let {mainData,selectedData,selectArr,pk_details} = this.state;
        let anaObj={},anaArr=[];
        if(selectArr.length>0){
            for(let i=0,len=selectArr.length;i<len;i++){
                if(!anaObj[selectArr[i].pk_voucher]){
                    anaObj[selectArr[i].pk_voucher]=i+1;
                    anaArr.push(selectArr[i]);
                }
            }
        }else{
            for(let i=0,len=mainData.length;i<len;i++){
                if(!anaObj[mainData[i].pk_voucher]){
                    anaObj[mainData[i].pk_voucher]=i+1;
                    anaArr.push(mainData[i]);
                }
            }
        }
        // if(anaArr.length>1){
        //     anaArr.splice(1,1)
        // }
        let data={
            voucher:anaArr,
            analsyistype:selectedData.analsyistype,
            analsyisscope :selectedData.analsyisscope,
            amountmatch:selectedData.amountmatch,
            pk_detail:pk_details
        }
        let self = this;
        if(data.analsyisscope==1){
            promptBox({
                color: 'success',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                title: "请注意",                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                content: '可能会改变已进行了现金流量分析的数据，是否继续？',             // 提示内容,非必输
                beSureBtnName: "确定",          // 确定按钮名称, 默认为"确定",非必输
                cancelBtnName: "取消",         // 取消按钮名称, 默认为"取消",非必输
                hasCloseBtn:false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
                beSureBtnClick: ()=>{
                    let url = "/nccloud/gl/cashflow/autoanacf.do";
                    ajax({
                        url,
                        data,
                        success: function(response) {
                            const { data, error, success } = response;
                            self.setState({
                                showModal:false
                            })
                            self.props.getAnalyseShow(false);
                            if (success&&data) {
                                toast({ content: self.state.json['20020CFANY-000048'], color: 'success' });/* 国际化处理： 自动分析成功*/
                            } else {
                                toast({ content: self.state.json['20020CFANY-000048'], color: 'success' });
                            }
                        }
                    });
                },   // 确定按钮点击调用函数,非必输
            })
        }else{
            let url = "/nccloud/gl/cashflow/autoanacf.do";
            ajax({
                url,
                data,
                success: function(response) {
                    const { data, error, success } = response;
                    self.setState({
                        showModal:false
                    })
                    self.props.getAnalyseShow(false);
                    if (success&&data) {
                        toast({ content: self.state.json['20020CFANY-000048'], color: 'success' });/* 国际化处理： 自动分析成功*/
                    } else {
                        toast({ content: self.state.json['20020CFANY-000048'], color: 'success' });
                    }
                }
            });
        }
    }
    render () {
        return (
        <div>
            <Modal
            fieldid="query"
            show = { this.state.showModal }
            onHide = { this.close }
            className="analyseModal cashflow-small-modal-a"
            >
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20020CFANY-000049']}</Modal.Title>
                </Modal.Header>

                <Modal.Body >
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                        {this.state.json['20020CFANY-000050']}
                        </Col>
                        <Col md={10} xs={10} sm={10}>
                            <NCRadio.NCRadioGroup
                                name="fruit"
                                selectedValue={this.state.selectedData.analsyistype}
                                onChange={(v)=>{
                                    let selectedData=this.state.selectedData;
                                    selectedData.analsyistype=v
                                    this.setState({
                                        selectedData
                                    })
                                }}>
                                <NCRadio value={1} >{this.state.json['20020CFANY-000051']}</NCRadio>
                                <NCRadio value={2} >{this.state.json['20020CFANY-000052']}</NCRadio>
                            </NCRadio.NCRadioGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                            {this.state.json['20020CFANY-000053']}
                        </Col>
                        <Col md={10} xs={10} sm={10}>
                            <NCRadio.NCRadioGroup
                                name="fruit"
                                selectedValue={this.state.selectedData.analsyisscope}
                                onChange={(v)=>{
                                    let selectedData=this.state.selectedData;
                                    selectedData.analsyisscope=v
                                    this.setState({
                                        selectedData
                                    })
                                }}>
                                <NCRadio value={1} >{this.state.json['20020CFANY-000054']}</NCRadio>
                                <NCRadio value={2} >{this.state.json['20020CFANY-000055']}</NCRadio>
                            </NCRadio.NCRadioGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                           { this.state.json['20020CFANY-000030']}
                        </Col>
                        <Col md={10} xs={10} sm={10}>
                            <NCCheckbox
                            onChange={(v)=>{
                                let selectedData=this.state.selectedData;
                                if(v){
                                    selectedData.amountmatch='Y'
                                }else{
                                    selectedData.amountmatch='N'
                                }
                                this.setState({
                                    selectedData
                                })    
                            }}
                            checked={this.state.selectedData.amountmatch=='Y'?true:false}
                            >{this.state.json['20020CFANY-000056']}
                            </NCCheckbox>
                        </Col>
                    </Row>
                    </NCDiv>
                </Modal.Body>

                <Modal.Footer fieldid="bottom_area">
                    <Button  fieldid="confirm" onClick={ this.handleAnalyse } style={{'min-width':'60px','font-size':'13px'}} style={{marginRight: 6}} colors="primary">{this.state.json['20020CFANY-000057']}</Button>
                    <Button  fieldid="close" onClick={ this.close } style={{'min-width':'60px','font-size':'13px'}} >{this.state.json['20020CFANY-000017']}</Button>
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
