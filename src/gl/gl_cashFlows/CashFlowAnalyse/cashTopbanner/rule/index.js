import React, { Component } from 'react';
import {InputItem,} from '../../../../public/components/FormItems';
import {high,base,ajax,toast,getMultiLang } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCButtonGroup:ButtonGroup,NCModal:Modal,NCDiv
    } = base;
export default class RuleModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            busrulename:''
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CFANY',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let {busrulename}=this.state;
        let showModal = nextProps.ruleShow;
        if(showModal){
            busrulename='';
        }
        this.setState({
            showModal,busrulename
        })
    }
    close() {
        this.props.getRuleShow(false);
    }
    confirm=()=>{
        let {busrulename}=this.state;
        if(!busrulename){
            toast({ content: this.state.json['20020CFANY-000074'], color: 'warning' });/* 国际化处理： 请输入业务规则*/
            return
        }
        let data={
            busrulename,
            pk_voucher:this.props.selectDetail.pk_voucher.value
        }
        let self = this;
        let url = "/nccloud/gl/cashflow/busruleset.do";
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                toast({ content: self.state.json['20020CFANY-000101'], color: 'success' });/* 国际化处理： 请输入业务规则*/
                self.props.getRuleShow(false);
                if (success&&data) {
                } else {
                    // self.setState({
                    //     detailTableData:[]
                    // })
                    // toast({ content: '无请求数据', color: 'warning' });
                }
                // self.props.getBack(self.state.detailTableData);
                self.setState({
                    busrulename:''
                })
            }
        });
    }
    render () {
        return (
        <div>
            <Modal
            fieldid="query"
            show = { this.state.showModal }
            className='cashflow-small-modal-a'
            onHide = { this.close } >
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20020CFANY-000072']}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                    <Row>
                        <Col lg={2} sm={2} xs={2} >
                            <span style={{'color':'#E14C46'}}>*</span>{this.state.json['20020CFANY-000075']}
                        </Col>
                        <Col lg={10} sm={10} xs={10}>
                            <InputItem
                                fieldid="busrulename"
                                type="customer"
                                name="scale"
                                defaultValue={this.state.busrulename}
                                maxlength={40}
                                onChange={(v) => {
                                    let {busrulename} = this.state;
                                    busrulename= v;	
                                    this.setState({
                                        busrulename
                                    })
                                    // this.props.getSaveDataItem(saveData)
                                }}
                            />
                        </Col>
                    </Row>
                    </NCDiv>
                </Modal.Body>

                <Modal.Footer fieldid="bottom_area">
                <Button fieldid="confirm" onClick={ this.confirm } style={{'min-width':'60px','font-size':'13px','margin-right':'6px'}} colors="primary">{this.state.json['20020CFANY-000016']}</Button>
                    <Button fieldid="close" onClick={ this.close } style={{'min-width':'60px','font-size':'13px'}} >{this.state.json['20020CFANY-000017']}</Button>
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
