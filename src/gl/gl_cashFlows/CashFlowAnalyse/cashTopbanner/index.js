import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { high, base, ajax, createPage, cacheTools, getMultiLang, createPageIcon } from 'nc-lightapp-front';
import { toast } from '../../../public/components/utils.js';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCMessage: Message, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCButtonGroup: ButtonGroup, NCModal: Modal
} = base;
import HeaderArea from '../../../public/components/HeaderArea';
import AnalyseModal from './analyseModal';
import GatherModal from './gatherModal';
import RuleModal from './rule';
import AjustModal from './ajustModal';
import SearchModal from './searchModal';
import './index.less';
import AnalyzeTypeModal from './getAnalyzeType';
import { openToVoucher } from '../../../public/common/voucherUtils';

class CashTopBanner extends Component {
    constructor(props) {
        super(props),
            this.state = {
                json: {},
                mainData: [],
                analyseModalShow: false,
                gatherModalShow: false,
                ruleShow: false,
                ajustModalShow: false,
                searchModalShow: false,
                analyzeTypeShow: false,
                selectArr: []
            }
    }
    componentWillMount() {
        // document.cookie="JSESSIONID=0E873800A252E16E39D08FD587915EAB"
        let pagecode = this.props.getSearchParam('p')
        let appcode = this.props.getSearchParam('c')
        let self = this;
        ajax({
            url: '/nccloud/platform/appregister/queryallbtns.do',
            data: {
                pagecode: pagecode,
                appcode: appcode
            },
            success: (res) => {
                if (!res.data) {
                    return
                }
                /*  拿到按钮数据，调用setButtons方法将数据设置到页面上，然后执行后续的按钮逻辑 */
                self.props.button.setButtons(res.data);
                // self.props.button.setButtonDisabled({'save':true,'cancel':true,'add':false,'edit':false}) 
                if (!self.state.selectArr.length) {
                    self.props.button.setButtonDisabled({ 'linkvou': true, 'settype': true, 'setrule': true })
                }
            }
        });
        let callback = (json) => {
            this.setState({ json: json }, () => {  })
        }
        getMultiLang({ moduleId: '20020CFANY', domainName: 'gl', currentLocale: 'simpchn', callback });
    }
    componentWillReceiveProps(nextProps) {
        let selectArr = nextProps.selectArr
        let mainData = nextProps.mainData;
        this.setState({
            mainData, selectArr
        })
        if (selectArr.length == 0) {
            this.props.button.setButtonDisabled({ 'linkvou': true, 'settype': true, 'setrule': true, 'gather': true })
            if(mainData.length){
                if(!mainData.every((item)=>{return item.editable.display==='Y'})){
                    this.props.button.setButtonDisabled({ 'autoanalysis': true })
                }else{
                    this.props.button.setButtonDisabled({ 'autoanalysis': false })
                }
            }
        } else {
            this.props.button.setButtonDisabled({ 'linkvou': false, 'settype': false, 'setrule': false, 'gather': false })
            if(!selectArr.every((item)=>{return item.editable.display==='Y'})){
                this.props.button.setButtonDisabled({ 'autoanalysis': true })
            }else{
                this.props.button.setButtonDisabled({ 'autoanalysis': false })
            }
        }
        this.setButtonDis(selectArr)
    }
    setButtonDis = (data) => {
        let flag = false;
        data.map((item) => {
            if (item.editable.display == 'N') {
                this.props.button.setButtonDisabled({ 'autoanalysis': true, 'gather': true })
                flag = true;
            }
            if (!flag) {
                this.props.button.setButtonDisabled({ 'autoanalysis': false, 'gather': false })
            }
        })
    }
    //自动分析
    handleAutoAnalyse = () => {
        let analyseModalShow = true;
        this.setState({
            analyseModalShow
        })
    }
    //业务规则
    setRule = () => {
        this.setState({
            ruleShow: true
        })
    }
    checkVoucher = () => {
        let { selectArr } = this.state;
        let idData = [];
        selectArr.map((item) => {
            if(!idData.includes(item.pk_voucher.value)){
               idData.push(item.pk_voucher.value) ;
            }
            
        })
        let param={
            pk_voucher: idData,
            titlename:this.state.json['20020CFANY-000066'],/* 国际化处理： 凭证联查*/
        }
        openToVoucher(this,param)
        // let appCodeNumber=voucherRelatedApp(voucher_gen);
        // if (idData.length == 1) {
        //     this.props.openTo('/gl/gl_voucher/pages/main/index.html#/Welcome',
        //         {
        //             appcode: appCodeNumber.appcode,
        //             c: appCodeNumber.appcode,
        //             id: idData[0],
        //             status: 'browse',
        //             pagekey: 'link',
        //             ifshowQuery: true
        //         })
        // } else if (idData.length > 1) {
        //     //数据关键信息存入缓存中
        //     cacheTools.set('checkedData', { id: idData });
        //     this.props.openTo('/gl/gl_voucher/pages/main/index.html#/', {
        //         status: 'browse',
        //         appcode: appCodeNumber.appcode,
        //         c: appCodeNumber.appcode,
        //         pagekey: 'link',
        //         n: this.state.json['20020CFANY-000066'],/* 国际化处理： 凭证联查*/
        //         ifshowQuery: true
        //     });
        // }
    }
    //修改模态框状态
    getAnalyseShow = (data) => {
        this.setState({
            analyseModalShow: data
        })
    }
    //归集
    gatherAnalyse = () => {
        let cashFlag = this.props.cashFlag;
        if (cashFlag) {
            toast({ content: this.state.json['20020CFANY-000067'], color: 'warning' });/* 国际化处理： 所选分录包含现金类科目，不能进行归集操作!*/
        } else {
            this.setState({
                gatherModalShow: true
            })
        }
    }
    //取消rule MODAL
    getRuleShow = (data) => {
        this.setState({
            ruleShow: data
        })
    }
    //归集成功刷新子表数据
    getChildTableData = (data) => {
        this.props.getChildTableData(data)
    }
    //归集取消弹框
    getGatherShow = (data) => {
        this.setState({
            gatherModalShow: data
        })
    }
    //调整
    handleAjust = () => {
        let ajustModalShow = true;
        this.setState({
            ajustModalShow
        })
    }
    //取消调整模态框
    getAjustShow = (data) => {
        this.setState({
            ajustModalShow: data
        })
    }
    //批量设置业务方式
    setAnalyzeTypeFa = (data, flag) => {
        this.props.setAnalyzeType(data)
    }
    getsearchModalShow = (data) => {
        this.setState({
            searchModalShow: data
        })
    }
    getSearchData = (data) => {
        this.props.getSearchData(data)
    }
    handleShowSearch = () => {
        let searchModalShow = true
        this.setState({
            searchModalShow
        })
    }
    setAnalyzeType = () => {
        let analyzeTypeShow = true
        this.setState({
            analyzeTypeShow
        })
    }
    setAnalyzeTypeShow = (data) => {
        this.setState({
            analyzeTypeShow: data
        })
    }
    setCurrinfo = (data) => {
        this.props.setCurrinfoFa(data)
    }
    onButtonClick = (props, id) => {
        switch (id) {
            case 'query':
                this.handleShowSearch();
                break;
            case 'autoanalysis':
                this.handleAutoAnalyse();
                break;
            case 'gather':
                this.gatherAnalyse();
                break;
            case 'linkvou':
                this.checkVoucher();
                break;
            case 'setrule':
                this.setRule();
                break;
            case 'settype':
                this.setAnalyzeType();
                break;
            case 'adjust':
                this.handleAjust();
                break;
            default:
                break;
        }
    }
    render() {
        let { analyseModalShow, mainData, gatherModalShow, ruleShow, ajustModalShow, searchModalShow, analyzeTypeShow } = this.state
        let { createButtonApp } = this.props.button;
        return (
            <div>
                <HeaderArea 
                    title = {this.state.json['20020CFANY-000068']}
                    
                    btnContent = {
                        createButtonApp({
                            area: 'head',
                            onButtonClick: this.onButtonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })
                        
                    }
                />
                {/* <div className='header-title-search-area'>
                    {createPageIcon()}
                    <h2 className='title-search-detail' fieldid={`${this.state.json['20020CFANY-000068']}_title`}>{this.state.json['20020CFANY-000068']}</h2>
                </div>
                <div className="header-button-area">
                    <div className="header-button-area">
                        {createButtonApp({
                            area: 'head',
                            onButtonClick: this.onButtonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })}
                    </div>
                </div> */}
                <div className=''>
                    <AnalyzeTypeModal
                        analyzeTypeShow={analyzeTypeShow}
                        setAnalyzeTypeShow={this.setAnalyzeTypeShow.bind(this)}
                        setAnalyzeTypeFa={this.setAnalyzeTypeFa.bind(this)}
                    />
                </div>

                <SearchModal
                    searchModalShow={searchModalShow}
                    getsearchModalShow={this.getsearchModalShow.bind(this)}
                    getSearchData={this.getSearchData.bind(this)}
                    setCurrinfo={this.setCurrinfo.bind(this)}
                />
                <AnalyseModal analyseModalShow={analyseModalShow}
                    mainData={mainData}
                    selectArr={this.props.selectArr}
                    getAnalyseShow={this.getAnalyseShow.bind(this)} />

                <GatherModal gatherModalShow={gatherModalShow}
                    mainData={mainData}
                    selectArr={this.props.selectArr}
                    getGatherShow={this.getGatherShow.bind(this)}
                    getChildTableData={this.getChildTableData.bind(this)}
                    currinfo={this.props.currinfo}
                />

                <RuleModal ruleShow={ruleShow} selectDetail={this.props.selectDetail}
                    getRuleShow={this.getRuleShow.bind(this)}
                />

                <AjustModal ajustModalShow={ajustModalShow} getAjustShow={this.getAjustShow.bind(this)} />

            </div>
        )
    }
}
CashTopBanner = createPage({})(CashTopBanner);
export default CashTopBanner;
