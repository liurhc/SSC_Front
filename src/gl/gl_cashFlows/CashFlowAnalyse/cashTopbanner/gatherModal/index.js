import React, { Component } from 'react';
import {InputItem,} from '../../../../public/components/FormItems';
import {high,base,ajax,getMultiLang } from 'nc-lightapp-front';
// import multiSelect from "tinper-bee/lib/multiSelect.js";
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCButtonGroup:ButtonGroup,NCModal:Modal,NCDiv
    } = base;
const { Refer } = high;
import CashflowTreeRef from '../../../../../uapbd/refer/fiacc/CashflowTreeRef';
import CurrtypeGridRef from '../../../../../uapbd/refer/pubinfo/CurrtypeGridRef';
import { toast } from '../../../../public/components/utils.js';
import FinanceOrgTreeRef from '../../../../../uapbd/refer/org/FinanceOrgAllGroupAllDataTreeRef/index'; 
import './index.less';
export default class GatherModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal: false,
            selectedData:{
                pk_cashflow:{
                    value:'',
                    display:''
                },
                scale:{
                    value:'100',
                    display:'100'
                },
                pk_innercorp:{
                    value:'',
                    display:''
                },
                pk_currtype:{
                    value:'1002Z0100000000001K1',
                    display:''/* 国际化处理： 人民币*/
                },
            },
            selectArr:[]
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
        let callback= (json) =>{
           this.setState({json:json,
            pk_currtype:{
                value:'1002Z0100000000001K1',
                display:this.state.json['20020CFANY-000058']/* 国际化处理： 人民币*/
            }  
        },()=>{ })
            }
        getMultiLang({moduleId:'20020CFANY',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let showModal = nextProps.gatherModalShow;
        let selectArr = nextProps.selectArr;
        let selectedData = this.state.selectedData;
        selectedData.pk_currtype=nextProps.currinfo;
        this.setState({
            selectArr,
            showModal,
            selectedData
        })
    }
    close() {
        this.props.getGatherShow(false);
    }
    handleGather=()=>{
        let {selectArr,selectedData} = this.state;
        let anaObj={};
        let anaArr=[];
        for(let i=0,len=selectArr.length;i<len;i++){
            anaArr.push(selectArr[i].pk_detail.value);
        }
        if(selectedData.pk_cashflow.value==''){
            toast({ content: this.state.json['20020CFANY-000011'], color: 'danger' });/* 国际化处理： 未选择现金流量！*/
            return;
            }
        let data={
            pk_details:anaArr,
            pk_cashflow:selectedData.pk_cashflow.value,
            pk_innercorp :selectedData.pk_innercorp.value,
            scale:Number(selectedData.scale.value),
            pk_currtype:selectedData.pk_currtype.value
        }
        let self = this;
        let url = "/nccloud/gl/cashflow/gathercf.do";
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                // self.setState({
                //     showModal:false
                // })
                self.props.getGatherShow(false);
                if (success&&data) {
                   self.props.getChildTableData(data);
                   toast({ content: self.state.json['20020CFANY-000059'], color: 'success' });/* 国际化处理： 归集成功*/
                } else {
                    toast({ content: self.state.json['20020CFANY-000060'], color: 'warning' });/* 国际化处理： 自动分析失败*/
                }
                // self.props.getBack(self.state.detailTableData);
            }
        });
    }
    //获取当前日期
    getNowFormatDate=()=> {
        let date = new Date();
        let seperator1 = "-";
        let seperator2 = ":";
        let month = date.getMonth() + 1;
        let strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
            
        return currentdate;
    }
    render () {
        return (
        <div>
            <Modal
            fieldid="query"
            show = { this.state.showModal }
            onHide = { this.close }
            className="analyseModal cashflow-small-modal-a"
            >
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20020CFANY-000062']}</Modal.Title>
                </Modal.Header>

                <Modal.Body >
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}  className="nc-theme-form-label-c">
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                            <span style={{'color':'#E14C46'}}>*</span>{this.state.json['20020CFANY-000063']}
                        </Col>
                        <Col md={10} xs={10} sm={10}>
                            <CashflowTreeRef
                                fieldid="pk_cashflow"
                                value =  {{
                                    refname: this.state.selectedData.pk_cashflow.display, 
                                    refpk: this.state.selectedData.pk_cashflow.value
                                }}
                                onlyLeafCanSelect = {true}
                                queryCondition={{
                                    "pk_accountingbook": this.state.selectArr[0]?this.state.selectArr[0].pk_accountingbook.value:'',
                                    "pk_org": this.state.selectArr[0]?this.state.selectArr[0].pk_unit.value:'',
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi'
                                }}
                                onChange={(v)=>{
                                    let {selectedData} = this.state;
                                    selectedData.pk_cashflow.display = v.refname;	
                                    selectedData.pk_cashflow.value = v.refpk;	
                                    this.setState({
                                        selectedData
                                    })                       
                                }} 
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                            {this.state.json['20020CFANY-000001']}
                        </Col>
                        <Col md={10} xs={10} sm={10}>
                            <FinanceOrgTreeRef
                                fieldid="pk_innercorp"
                                value =  {{
                                    refname: this.state.selectedData.pk_innercorp.display, 
                                    refpk: this.state.selectedData.pk_innercorp.value
                                }}
                                queryCondition={{
                                    isDataPowerEnable: 'Y',
                                    DataPowerOperationCode: 'fi'
                                }}
                                onChange={(v)=>{
                                    let {selectedData} = this.state;
                                    selectedData.pk_innercorp.display = v.refname;	
                                    selectedData.pk_innercorp.value = v.refpk;	
                                    this.setState({
                                        selectedData
                                    })                       
                                }} 
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                            {this.state.json['20020CFANY-000064']}
                        </Col>
                        <Col md={10} xs={10} sm={10}>
                        <InputItem
                            fieldid="scale"
                            type="customer"
                            name="scale"
                            defaultValue={this.state.selectedData.scale.display}
                            onChange={(v) => {
                                let {selectedData} = this.state;
                                if(v<-100||v>100){
                                    selectedData.scale.display = '';	
                                    selectedData.scale.value = '';	
                                }else{
                                    selectedData.scale.display = v;	
                                    selectedData.scale.value = v;	
                                }
                                this.setState({
                                    selectedData
                                })
                                // this.props.getSaveDataItem(saveData)
                            }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={2} xs={2} sm={2}>
                            {this.state.json['20020CFANY-000002']}
                        </Col>
                        <Col md={10} xs={10} sm={10}>
                        <CurrtypeGridRef
                            fieldid="pk_currtype"
                            value =  {{
                                refname: this.state.selectedData.pk_currtype.display, 
                                refpk: this.state.selectedData.pk_currtype.value
                            }}
                            // defaultValue={{refname:this.props.display,refpk:this.props.value}}
                            queryCondition={
                                {'versiondate': this.getNowFormatDate(),
                                "pk_accountingbook": '1001A3100000000000PE',
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi'
                                }
                            }
                            onChange={(v)=>{
                                let {selectedData} = this.state;
                                selectedData.pk_currtype.display = v.refname;	
                                selectedData.pk_currtype.value = v.refpk;	
                                this.setState({
                                    selectedData
                                })                       
                            }} 
                        />
                        </Col>
                    </Row>
                    </NCDiv>
                </Modal.Body>

                <Modal.Footer fieldid="bottom_area">
                    <Button  fieldid="confirm" onClick={ this.handleGather } style={{'min-width':'60px','font-size':'13px'}} style={{marginRight: 6}} colors="primary">{this.state.json['20020CFANY-000057']}</Button>
                    <Button fieldid="close" onClick={ this.close } style={{'min-width':'60px','font-size':'13px'}} >{this.state.json['20020CFANY-000017']}</Button>
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
