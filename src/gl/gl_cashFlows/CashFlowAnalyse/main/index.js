import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,createPage,getMultiLang,gzip,viewModel} from 'nc-lightapp-front';
let { setGlobalStorage} = viewModel;
import { toast } from '../../../public/components/utils.js';
import CashTopBanner from '../cashTopbanner/index.js';
import CashAnalyseTable from '../cashAnalyseTable/index.js'; 
import CashAnalyseItem from '../cashAnalyseItem/index.js';
// import CashAnalyseSearch from './cashAnalyseSearch';
const { 
    NCLoading:Loading,
    } = base;
import '../index.less';
const gziptools = new gzip();
class CashFlowAnalyse extends Component{
    constructor(props){
        super(props),
        this.state={
            json:{},
            isLoading:false,
            mainData:[],//整体数据
            detailTableData:[],//子表数据
            selectDetail:{},//当前选中表格
            selectArr:[],//选中项
            cashFlag:0,
            pk_accountingbook:'',
            direct:'in',//借方
            saveEdit:false,//判断子表编辑状态
            saveEditChild:false,//下表编辑状态
            searchData:{
                nc001:null,
                nc002:null,
                pk_unitState:null

            },//查询数据
            checkedArray:[],//判断选中
            selIds:[],
            saveNum:'',//判断下表保存
            currinfo:{value:'',display:''}//默认账簿币种
        }
    }
    componentWillMount(){
        let self = this;
        let {searchData}=this.state;
        let cashData =gziptools.unzip(this.props.getUrlParam('searchDataCash'));
        if(cashData){
            if(cashData.params){//快速分析联查
                this.setState({
                    searchData:cashData.params
                })
            }else if(!cashData.params){//外部联查，请求一些数据
                ajax({
                    url:'/nccloud/gl/voucher/queryBookCombineInfo.do',
                    data:{"pk_accountingbook":cashData.param.pk_accountingbook[0],'needaccount':false},
                    success: (response) =>  {
                        const { data, error, success } = response;
                        if (success&&data) {
                            searchData.NC001=data.NC001;
                            searchData.NC002=data.NC002;
                            let currinfo = data.currinfo;
                            self.setState({
                                searchData,currinfo,
                                pk_accountingbook:cashData.param.pk_accountingbook[0]
                            })
                        } else {
                            
                        }    
                    }
                });
                let url1 = "/nccloud/gl/cashflow/isbustartflag.do"
                ajax({
                    url:url1,
                    data:{"pk_accountingbook":cashData.param.pk_accountingbook[0]},
                    success: function(response) { 
                        const { data, error, success } = response;
                        if (success&&data) {
                            if(data=='Y'){
                                searchData.pk_unitState=true;
                                self.setState({
                                    searchData
                                });
                            }else{
                                searchData.pk_unitState=false;
                                self.setState({
                                    searchData
                                });
                            }
                        } else {
                            
                        }    
                    }
                });
            }
           
            let self = this;
            let data = cashData;
            let url = "/nccloud/gl/cashflow/cfQuickAnaLinkDetail.do";
            ajax({
                url,
                data,
                success: function(response) {
                    self.setState({
                        isLoading: false
                    });
                    const { data, error, success } = response;
                    if (success&&data) {
                        let checkedArray = [];
                        for(let i=0,len=data.length;i<len;i++){
                            checkedArray.push(false)
                        } 
                        self.setState({
                            mainData:data,
                            checkedArray,
                            selIds:[]
                        })
                        self.getTableDetail(data[0]);
                        setGlobalStorage('localStorage','searchDataCash','');
                    } else {
                        self.setState({
                            mainData:[],
                            checkedArray:[],
                            selIds:[]
                        })
                        // toast({ content: '无请求数据', color: 'warning' });
                    }
                }
            });
        }
        let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
        getMultiLang({moduleId:'20020CFANY',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    getSearchData=(v)=>{
        let self = this;
        let data = v;
        this.setState({
            pk_accountingbook:v.pk_accountingbook,
            searchData:v
        })
        let url = "/nccloud/gl/cashflow/querycfdetail.do";
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                    let checkedArray = [];
                    for(let i=0,len=data.length;i<len;i++){
                        checkedArray.push(false)
                    }
                    toast({ content: `共查询${data.length}条`, color: 'success' });/* 国际化处理： 日期判断*/
                    self.setState({
                        mainData:data,
                        checkedArray, 
                        selIds:[],
                        selectArr: []
                    })
                    self.getTableDetail(data[0])
                } else {
                    toast({ content: `查询内容为空`, color: 'warning' });/* 国际化处理： 日期判断*/
                    self.setState({
                        mainData:[],
                        detailTableData:[],
                        checkedArray:[],
                        selIds:[],
                        selectArr: []
                    })
                }
            }
        });
    }
    setCheckedArray=(data)=>{//获取选中
        this.setState({
            checkedArray:data
        })
	}
	setCheckedselIds=(data)=>{
		this.setState({
			selIds:data
		})
	}
    //获取子表数据
    getTableDetail=(v,type)=>{
        let self = this;
        let data = {
            pk_detail: v.pk_detail.value
        };
        let direct;
        if(Number(v.groupdebitamount.value)&&Number(v.localdebitamount.value)&&Number(v.globaldebitamount.value)){
            direct='in'
        }else{
            direct='out'
        }
        this.setState({
            selectDetail:v,
            // cashFlag:v.cashflag.value,
            direct,
            saveEditChild:type,saveEdit:false,saveNum:''
        })
        let url = "/nccloud/gl/cashflow/querycfcase.do";
        ajax({
            url,
            data,
            loading:false,
            success: function(response) {
                let { data, error, success } = response;
                if (success&&data) {
                    data=data.map((item)=>{
                        let moneyrate = item.money.display&&item.money.display.split('.')[1].length;
                        let moneymainrate = item.moneymain.display&&item.moneymain.display.split('.')[1].length;
                        let moneygrouprate = item.moneygroup.display&&item.moneygroup.display.split('.')[1].length;
                        let moneyglobalrate = item.moneyglobal.display&&item.moneyglobal.display.split('.')[1].length;
                        item.rateData={
                            "scale":moneyrate,
                            "groupscale":moneygrouprate,
                            "excrate3":'',
                            "NC002":false,
                            "excrate2":'',
                            "NC001":"",
                            "orgscale":moneymainrate,
                            "globalmode":true,
                            "orgmode":false,
                            "excrate4":0,
                            "groupmode":false,
                            globalscale:moneyglobalrate
                        }
                        return item;
                    })
                    self.setState({
                        detailTableData:data
                    })

                } else {
                    self.setState({
                        detailTableData:[]
                    })
                    // toast({ content: '无请求数据', color: 'warning' });
                }
            }
        });
    }
    //修改关联分析
    getMainData=(pk_voucher,v)=>{
        let {mainData,selectArr} = this.state;
        for(let i=0,len=mainData.length;i<len;i++){
            if(mainData[i].pk_voucher.value==pk_voucher){
                mainData[i].analyzetype.value=v
            }
        }   
        if(selectArr.length){
            for(let i=0,len=selectArr.length;i<len;i++){
                if(selectArr[i].pk_voucher.value==pk_voucher){
                    selectArr[i].analyzetype.value=v
                }
            } 
        } 
        this.setState({
            mainData,
            selectArr
        })
    }
    setCurrinfoFa=(data)=>{//获取默认币种
        this.setState({
            currinfo:data
        })
    }
    //选中表格数据
    getSelectArr=(data)=>{
        let dataLen=0
        let cashFlag = this.state.cashFlag;
        for(let i=0,len=data.length;i<len;i++){
            if(data[i].cashflag.value>0){
                cashFlag=true
            }else{
                dataLen++
            }
        }
        if(dataLen==data.length){
            cashFlag=false
        }else{
            cashFlag=true
        }
        this.setState({
            selectArr:data,
            cashFlag
        })
    }
    //获取归集子表数据
    getChildTableData=(data)=>{
        this.setState({
            detailTableData:data
        })
    }
    //获取编辑状态
    getSaveEdit=(data,num)=>{
        this.setState({
            saveEdit:data,
            saveNum:num,
            saveEditChild:false
        })
    }
    setAnalyzeType=(data)=>{//批量设置分配方式
       let {mainData,selectArr}=this.state;
        for(let i=0,len=mainData.length;i<len;i++){
            for(let j=0,lenj=selectArr.length;j<lenj;j++){
                if(mainData[i].pk_voucher.value==selectArr[j].pk_voucher.value){
                    mainData[i].analyzetype.value=data
                }
                selectArr[j].analyzetype.value=data
            }  
        }  
        toast({ content: this.state.json['20020CFANY-000095'], color: 'success' });/* 国际化处理： 分配成功*/
        this.setState({
            mainData,selectArr
        }) 
    }
    render(){
        let { mainData,detailTableData,selectDetail,selectArr,cashFlag,pk_accountingbook,searchData,
            direct,saveEdit,checkedArray,selIds,currinfo,saveEditChild,saveNum } = this.state;
        return (
            <div className="section-cash-analyze nc-bill-list">
                <CashTopBanner mainData={mainData} selectArr={selectArr} 
                    selectDetail={selectDetail}
                    getChildTableData={this.getChildTableData.bind(this)}
                    cashFlag={cashFlag}
                    getSearchData={this.getSearchData.bind(this)}
                    setAnalyzeType={this.setAnalyzeType.bind(this)}
                    currinfo={currinfo}
                    setCurrinfoFa={this.setCurrinfoFa.bind(this)}
                />
                {/* <CashAnalyseSearch /> */}
                <div className='cashflow-tables nc-theme-area-bgc'>
                    <CashAnalyseTable mainData={mainData} 
                        getTableDetail={this.getTableDetail.bind(this)}
                        getMainData={this.getMainData.bind(this)}
                        getSelectArr={this.getSelectArr.bind(this)}
                        saveEdit={saveEdit}
                        getSaveEdit={this.getSaveEdit.bind(this)}
                        searchData={searchData}
                        checkedArray={checkedArray}
                        selIds={selIds}
                        setCheckedArray={this.setCheckedArray.bind(this)}
                        setCheckedselIds={this.setCheckedselIds.bind(this)}
                    />
                    <CashAnalyseItem detailTableData={detailTableData} 
                        selectDetail={selectDetail}
                        cashFlag={cashFlag}
                        pk_accountingbook={pk_accountingbook}
                        saveEditChild={saveEditChild}
                        direct={direct}
                        saveNum={saveNum}
                        getChildTableData={this.getChildTableData.bind(this)}
                        getSaveEdit={this.getSaveEdit.bind(this)}
                        searchData={searchData}
                    />
                </div>
                
                <Loading fullScreen showBackDrop={true} show={this.state.isLoading} />
            </div>
        )
    }
}
CashFlowAnalyse = createPage({})(CashFlowAnalyse);
// export default CashFlowAnalyse;
ReactDOM.render(
    <CashFlowAnalyse/>,
    document.querySelector("#app")
);
