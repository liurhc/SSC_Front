import React, { Component } from "react";
import {InputItem,} from '../../../public/components/FormItems';
import {high,ajax,base,getMultiLang } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
  NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
  NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCButtonGroup:ButtonGroup,NCPopconfirm,NCDiv
  } = base;
import TableButtons from './tableButtons';
import CashflowTreeRef from '../../../../uapbd/refer/fiacc/CashflowTreeRef';
import FinanceOrgTreeRef from '../../../../uapbd/refer/org/FinanceOrgAllGroupAllDataTreeRef/index';
import CurrtypeGridRef from '../../../../uapbd/refer/pubinfo/CurrtypeGridRef';
import BusinessUnitWithGlobleAndCurrGroupTreeRef from '../../../../uapbd/refer/org/BusinessUnitWithGlobleAndCurrGroupTreeRef/index'
import {getTableHeight } from '../../../public/common/method.js';
export default class cashAnalyseItem extends Component {
  constructor(props){
    super(props)
    this.state={
      json:{},
      detailTableData:[],//子表数据
      selectDetail:{},//选中父表项
      selectItem:{},//选中子表项
      cashFlag:'',//现金判断
      pk_accountingbook:"",
      pk_org:"",
      direct:'',//借贷
      itemtype:'',//流入流出
      isPlus:'',//正数
      searchData:'',//查询数据
      deleteStr:'',//删除子表的父pk
      rateData:{
        "scale":"",
        "groupscale":"",
        "excrate3":'',
        "NC002":false,
        "excrate2":'',
        "NC001":"",
        "orgscale":"",
        "globalmode":true,
        "orgmode":false,
        "excrate4":0,
        "groupmode":false
    },//税率相关数据
    columns:[]
    }
  }
  componentWillMount(){
    this.setJsonData();
    let self=this;
    if(self.props.pk_accountingbook){
      let url = "/nccloud/gl/glpub/queryFinanceOrg.do";
      let param = {};
      param.pk_accountingbook = pk_accountingbook;
      ajax({
          url: url,
          data: param,
          success: function(response) {
              let { data, success } = response;
              if (success) {
                  self.setState({
                      pk_org: data.pk_org
                  });
              }
          }
      });
   }
  }
  componentWillReceiveProps(nextProps){
      let direct = nextProps.direct;
      let detailTableData=[];
      let selectDetail={};
      let cashFlag='';
      let pk_accountingbook='';
      selectDetail = nextProps.selectDetail;
      detailTableData = nextProps.detailTableData;
      cashFlag = nextProps.cashFlag;
      pk_accountingbook= nextProps.pk_accountingbook;
      let self = this;
    //   if(pk_accountingbook){
    //     let url = "/nccloud/gl/glpub/queryFinanceOrg.do";
    //     let param = {};
    //     param.pk_accountingbook = pk_accountingbook;
    //     ajax({
    //         url: url,
    //         data: param,
    //         success: function(response) {
    //             let { data, success } = response;
    //             if (success) {
    //                 self.setState({
    //                     pk_org: data.pk_org
    //                 });
    //             }
    //         }
    //     });
    //  }

      if(detailTableData){
        for(let i=0,len=detailTableData.length;i<len;i++){
          if(!detailTableData[i].key){
            detailTableData[i].key=i+1;
            this.getRateData(detailTableData[i].pk_currtype.value,i,false,detailTableData)
          }
        }
      }
      if(JSON.stringify(this.state.json)=='{}'){
        this.setJsonData();
      }
      let columns=this.state.columns.length>0?this.state.columns:
      [
        {
          title: (<div fieldid="pk_cashflow" className="mergecells">{this.state.json['20020CFANY-000000']}</div>),
          dataIndex: "pk_cashflow",
          key: "pk_cashflow",
          width:'300px',
          render: (text, record, index) => (
            !record.isEdit?
            <div fieldid="pk_cashflow">{this.state.detailTableData[index].pk_cashflow.display?this.state.detailTableData[index].pk_cashflow.display:<span>&nbsp;</span>}</div>:
            <div fieldid="pk_cashflow">
            <CashflowTreeRef
                fieldid="pk_cashflow"
                value =  {{
                  refname: this.state.detailTableData[index].pk_cashflow.display, 
                  refpk: this.state.detailTableData[index].pk_cashflow.value
                }}
                disabled={!this.state.detailTableData[index].isEdit}
                queryCondition={{
                  "pk_accountingbook": this.state.pk_accountingbook,
                  "pk_org": this.state.pk_org,
                  isDataPowerEnable:'Y',
                  DataPowerOperationCode:'fi'
                }}
                onlyLeafCanSelect={true}
                isDataPowerEnable='Y'
                DataPowerOperationCode='fi'
                onChange={(v)=>{
                  let {detailTableData} = this.state;
                  detailTableData[index].pk_cashflow.display = v.refname;	
                  detailTableData[index].pk_cashflow.value = v.refpk;	
                  this.props.getChildTableData(detailTableData);
                  let data = { pk_cashflow_main:v.refpk,pk_cashflow_ass:''};
                  let self=this;
                  let url = '/nccloud/gl/glpub/cfitemquery.do';
                  ajax({
                    url,
                    data,
                    success: function(response) {
                        const { data, error, success } = response;
                        if (success&&data) {
                            //let direct = self.state.direct;
                            let direct = self.state.selectDetail.localcreditamount.value == 0?'in':'out';
                            if(data.ismain){
                              detailTableData[index].mainflag='Y'
                            }else{
                              detailTableData[index].mainflag='N'
                            }
                            if(data.ismain){ //主表项才需要计算
                              //if(data.itemtype){//判断正负
                              let nativeAmount;
                              if (self.state.selectDetail.localcreditamount.value == 0) {
                                nativeAmount = self.state.selectDetail.localdebitamount.value;
                              } else {
                                nativeAmount = self.state.selectDetail.localcreditamount.value;
                              }
                                if((direct=='in' && data.itemtype==1)
                                    ||(direct=='out' && data.itemtype==2)){
                                  // if(data.itemtype==1){
                                  //   detailTableData[index].isPlus='1'
                                  // }else{
                                  //   detailTableData[index].isPlus='0'
                                  // }
                                  if ((nativeAmount > 0 && detailTableData[index].moneymain.value > 0) 
                                        || (nativeAmount < 0 && detailTableData[index].moneymain.value < 0)) {
                                      detailTableData[index].money.value = -(detailTableData[index].money.value)
                                      detailTableData[index].money.display = -(detailTableData[index].money.display)
                                      detailTableData[index].moneymain.value = -(detailTableData[index].moneymain.value)
                                      detailTableData[index].moneymain.display = -(detailTableData[index].moneymain.display)
                                      detailTableData[index].moneygroup.value = -(detailTableData[index].moneygroup.value)
                                      detailTableData[index].moneygroup.display = -(detailTableData[index].moneygroup.display)
                                      detailTableData[index].moneyglobal.value = -(detailTableData[index].moneyglobal.value)
                                      detailTableData[index].moneyglobal.display = -(detailTableData[index].moneyglobal.display)
                                    }
                                } 
                                if((direct=='in' && data.itemtype==2)
                                          ||(direct=='out' && data.itemtype==1)){
                                        if ((nativeAmount > 0 && detailTableData[index].moneymain.value < 0) 
                                          || (nativeAmount < 0 && detailTableData[index].moneymain.value > 0)) {
                                          detailTableData[index].money.value = -(detailTableData[index].money.value)
                                          detailTableData[index].money.display = -(detailTableData[index].money.display)
                                          detailTableData[index].moneymain.value = -(detailTableData[index].moneymain.value)
                                          detailTableData[index].moneymain.display = -(detailTableData[index].moneymain.display)
                                          detailTableData[index].moneygroup.value = -(detailTableData[index].moneygroup.value)
                                          detailTableData[index].moneygroup.display = -(detailTableData[index].moneygroup.display)
                                          detailTableData[index].moneyglobal.value = -(detailTableData[index].moneyglobal.value)
                                          detailTableData[index].moneyglobal.display = -(detailTableData[index].moneyglobal.display)
                                        }
                                }
                                if(self.state.selectDetail.cashflag.value>0){
                                  detailTableData[index].money.value = -(detailTableData[index].money.value)
                                  detailTableData[index].money.display = -(detailTableData[index].money.display)
                                  detailTableData[index].moneymain.value = -(detailTableData[index].moneymain.value)
                                  detailTableData[index].moneymain.display = -(detailTableData[index].moneymain.display)
                                  detailTableData[index].moneygroup.value = -(detailTableData[index].moneygroup.value)
                                  detailTableData[index].moneygroup.display = -(detailTableData[index].moneygroup.display)
                                  detailTableData[index].moneyglobal.value = -(detailTableData[index].moneyglobal.value)
                                  detailTableData[index].moneyglobal.display = -(detailTableData[index].moneyglobal.display)
                                }
                              //}
                              // if(detailTableData[index].isPlus=='1'){
                              //     detailTableData[index].money.value = (detailTableData[index].money.value)
                              //     detailTableData[index].money.display = (detailTableData[index].money.display)
                              //     detailTableData[index].moneymain.value = (detailTableData[index].moneymain.value)
                              //     detailTableData[index].moneymain.display = (detailTableData[index].moneymain.display)
                              //     detailTableData[index].moneygroup.value = (detailTableData[index].moneygroup.value)
                              //     detailTableData[index].moneygroup.display = (detailTableData[index].moneygroup.display)
                              //     detailTableData[index].moneyglobal.value = (detailTableData[index].moneyglobal.value)
                              //     detailTableData[index].moneyglobal.display = (detailTableData[index].moneyglobal.display)
                              // }
                              // else if(detailTableData[index].isPlus=='0'){
                              //   detailTableData[index].money.value = -(detailTableData[index].money.value)
                              //     detailTableData[index].money.display = -(detailTableData[index].money.display)
                              //     detailTableData[index].moneymain.value = -(detailTableData[index].moneymain.value)
                              //     detailTableData[index].moneymain.display = -(detailTableData[index].moneymain.display)
                              //     detailTableData[index].moneygroup.value = -(detailTableData[index].moneygroup.value)
                              //     detailTableData[index].moneygroup.display = -(detailTableData[index].moneygroup.display)
                              //     detailTableData[index].moneyglobal.value = -(detailTableData[index].moneyglobal.value)
                              //     detailTableData[index].moneyglobal.display = -(detailTableData[index].moneyglobal.display)
                              // }
                              detailTableData.forEach((item,index,input)=>{  
                                item.money.value=item.money.display=self.addZero(item.money.value,item.rateData.scale); 
                                item.moneymain.value=item.moneymain.display=self.addZero(item.moneymain.value,item.rateData.orgscale); 
                                item.moneygroup.value=item.moneygroup.display=self.addZero(item.moneygroup.value,item.rateData.groupscale); 
                                item.moneyglobal.value=item.moneyglobal.display=self.addZero(item.moneyglobal.value,item.rateData.globalscale); 
                              })  
                            }
                            self.setState({
                              itemtype:data.itemtype,
                              detailTableData
                            })
                          } else {
                            // toast({ content: '无请求数据', color: 'warning' });
                        }
                    }
                  });                      
                }}
              />
            </div>
          )
        },
        {
          title: (<div fieldid="pk_innercorp" className="mergecells">{this.state.json['20020CFANY-000001']}</div>),
          dataIndex: "pk_innercorp",
          key: "pk_innercorp",
          width:'300px',
          render: (text, record, index) => (
            !record.isEdit?
            <div fieldid="pk_innercorp">{this.state.detailTableData[index].pk_innercorp.display?this.state.detailTableData[index].pk_innercorp.display:<span>&nbsp;</span>}</div>:
            <div fieldid="pk_innercorp">
            <FinanceOrgTreeRef
              fieldid="pk_innercorp"
              value =  {{
                refname: this.state.detailTableData[index].pk_innercorp.display, 
                refpk: this.state.detailTableData[index].pk_innercorp.value,
              }}
              queryCondition = {() => {
                return {
                    "isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi'
                }
              }}
              disabled={!this.state.detailTableData[index].isEdit}
              onChange={(v)=>{
                let {detailTableData} = this.state;
                detailTableData[index].pk_innercorp.display = v.refname;	
                detailTableData[index].pk_innercorp.value = v.refpk;	
                this.props.getChildTableData(detailTableData);                     
              }}
            />
            </div>
          )
        },
        {
          title: (<div fieldid="pk_detail_opp" className="mergecells">{this.state.json['20020CFANY-000002']}</div>),
          dataIndex: "pk_detail_opp",
          key: "pk_detail_opp",
          width:'150px',
          render: (text, record, index) => (
            !record.isEdit?
            <div fieldid="pk_detail_opp">{this.state.detailTableData[index].pk_currtype.display?this.state.detailTableData[index].pk_currtype.display:<span>&nbsp;</span>}</div>:
            <div fieldid="pk_detail_opp">
            <CurrtypeGridRef
              fieldid="pk_detail_opp"
              value =  {{
                refname: this.state.detailTableData[index].pk_currtype.display, 
                refpk: this.state.detailTableData[index].pk_currtype.value
              }}
              disabled={!this.state.detailTableData[index].isEdit}
              onChange={(v)=>{
                let {detailTableData} = this.state;
                detailTableData[index].pk_currtype.display = v.refname;	
                detailTableData[index].pk_currtype.value = v.refpk;	
                this.props.getChildTableData(detailTableData);       
                this.getRateData(v.refpk,index,true);          
              }}
            />
            </div>
          )
        },
        {
          title: (<div fieldid="money" className="mergecells">{this.state.json['20020CFANY-000003']}</div>),
          dataIndex: "money",
          key: "money",
          width:'150px',
          className:'t-a-r',
          render: (text, record, index) => (
            !record.isEdit?
            <div fieldid="money">{this.state.detailTableData[index].money.display?this.state.detailTableData[index].money.display:<span>&nbsp;</span>}</div>:
            <div fieldid="money">
            <NCNumber
                fieldid="money"
                scale={this.state.detailTableData[index].rateData&&Number(this.state.detailTableData[index].rateData.scale)}
                disabled={!this.state.detailTableData[index].isEdit}
                value={
                  this.state.detailTableData[index].money.display
                }
                onBlur={(v) => {
                  let {detailTableData} = this.state;
                  // v = this.addZero(v,Number(this.state.detailTableData[index].rateData.scale))
                  if(detailTableData[index].money.display*100!=v*100){
                    detailTableData[index].money.display = v;	
                    detailTableData[index].money.value = v;
                    this.onRateChange( '',2,index,detailTableData);
                  }
                }}
            />
            </div>
          )
        },
        {
          title: (<div fieldid="moneymain" className="mergecells">{this.state.json['20020CFANY-000004']}</div>),
          dataIndex: "moneymain",
          key: "moneymain",
          width:'150px',
          className:'t-a-r',
          render: (text, record, index) => (
            !record.isEdit?
            <div fieldid="moneymain">{this.state.detailTableData[index].moneymain.display?this.state.detailTableData[index].moneymain.display:<span>&nbsp;</span>}</div>:
            <div fieldid="moneymain">
            <NCNumber
              fieldid="moneymain"
                scale={this.state.detailTableData[index].rateData&&Number(this.state.detailTableData[index].rateData.orgscale)}
                disabled={!this.state.detailTableData[index].isEdit}
                value={
                  this.state.detailTableData[index].moneymain.display
                }
                onBlur={(v) => {
                  let {detailTableData} = this.state;
                  // v = this.addZero(v,Number(this.state.detailTableData[index].rateData.scale))
                  if(detailTableData[index].moneymain.display!=v){
                    detailTableData[index].moneymain.display = v;
                    detailTableData[index].moneymain.value = v;
                    this.onRateChange( '',3,index,detailTableData);
                  }
                }}
            />
            </div>
          )
        },
        {
          title: (<div fieldid="cjijjz" className="mergecells">{this.state.json['20020CFANY-000005']}</div>),
          dataIndex: "cjijjz",
          key: "cjijjz",
          width:'80px',
          fixed:'right',
          render: (text, record, index) => (
            <a size='sm' colors="warning" shape='border' fieldid="cjijjz"
            className=''
            style={{'display':(this.state.detailTableData[index].isEdit?'':'none')}}
            onClick={
              ()=>{
                let {detailTableData,deleteStr} = this.state;
                deleteStr=detailTableData[index].pk_detail.value;
                detailTableData.splice(index,1);
                this.setState({
                  detailTableData,
                  deleteStr
                })
              }
            }>{this.state.json['20020CFANY-000009']}</a>
          )
        },
      ]
      ;
      
      this.setState({
        detailTableData,
        selectDetail,
        cashFlag,
        pk_accountingbook,
        direct,
        columns,
        searchData:Object.assign({},nextProps.searchData)
      })
      //添加判断是否第一次加载，是否存在业务单元，是否增加了业务单元
      if(nextProps.searchData){
        if(nextProps.searchData.pk_unitState!=this.state.searchData.pk_unitState||
          nextProps.searchData.NC001!=this.state.searchData.NC001||
          nextProps.searchData.NC002!=this.state.searchData.NC002
        ){
          if(nextProps.searchData.pk_unitState&&columns[0].dataIndex!='pk_unit'){//业务单元
            columns.splice(0, 0, {
              title: (<div fieldid="pk_unit" className="mergecells">{this.state.json['20020CFANY-000006']}</div>),
              dataIndex: "pk_unit",
              key: "pk_unit",
              width:'150px',
              render: (text, record, index) => (
                !record.isEdit?
                <div fieldid="pk_unit">{this.state.detailTableData[index].pk_unit.display?this.state.detailTableData[index].pk_unit.display:<span>&nbsp;</span>}</div>:
                <div fieldid="pk_unit">
                <BusinessUnitWithGlobleAndCurrGroupTreeRef
                  fieldid="pk_unit"
                  value =  {{
                    refname: this.state.detailTableData[index].pk_unit.display, 
                    refpk: this.state.detailTableData[index].pk_unit.value
                  }}
                  queryCondition={{
                    isDataPowerEnable: 'Y',
                    DataPowerOperationCode: 'fi'
                  }}
                  disabled={true}
                  onChange={(v)=>{
                    let {detailTableData} = this.state;
                    detailTableData[index].pk_unit.display = v.refname;	
                    detailTableData[index].pk_unit.value = v.refpk;	
                    this.props.getChildTableData(detailTableData);                     
                  }}
                />
                </div>
              )
            });
          }else{
            for(let i=0,len=columns.length;i<len;i++){
              if(columns[i].dataIndex=='pk_unit'){
                columns.splice(i, 1);
                return
              }
            }
          }
          if(columns[0].dataIndex!='pk_unit'){//集团本币是否显示
            if(nextProps.searchData.NC001&&columns[5].dataIndex!='moneygroup'){
              columns.splice(5, 0, {
                title: (<div fieldid="moneygroup" className="mergecells">{this.state.json['20020CFANY-000007']}</div>),
                dataIndex: "moneygroup",
                key: "moneygroup",
                width:'150px',
                className:'t-a-r',
                render: (text, record, index) => (
                  !record.isEdit?
                  <div fieldid="moneygroup">{this.state.detailTableData[index].moneygroup.display?this.state.detailTableData[index].moneygroup.display:<span>&nbsp;</span>}</div>:
                  <div fieldid="moneygroup">
                  <NCNumber
                      fieldid="moneygroup"
                      scale={this.state.detailTableData[index].rateData&&Number(this.state.detailTableData[index].rateData.groupscale)}
                      disabled={!this.state.detailTableData[index].isEdit}
                      value={
                        this.state.detailTableData[index].moneygroup.display
                      }
                      onBlur={(v) => {
                        let {detailTableData} = this.state;
                        v = this.addZero(v,Number(this.state.detailTableData[index].rateData.scale))
                        detailTableData[index].moneygroup.display = v;
                        detailTableData[index].moneygroup.value = v;	
                        this.setState({
                          detailTableData
                        })
                      }}
                  />
                  </div>
                )
              });
            }else if(!nextProps.searchData.NC001){
              for(let i=0,len=columns.length;i<len;i++){
                if(columns[i].dataIndex=='moneygroup'){
                  columns.splice(i, 1);
                  return
                }
              }
            }
          }else if(columns[0].dataIndex=='pk_unit'){
            if(nextProps.searchData.NC001&&columns[6].dataIndex!='moneygroup'){
              columns.splice(6, 0, {
                title: (<div fieldid="moneygroup" className="mergecells">{this.state.json['20020CFANY-000007']}</div>),
                dataIndex: "moneygroup",
                key: "moneygroup",
                width:'150px',
                className:'t-a-r',
                render: (text, record, index) => (
                  !record.isEdit?
                  <div fieldid="moneygroup">{this.state.detailTableData[index].moneygroup.display?this.state.detailTableData[index].moneygroup.display:<span>&nbsp;</span>}</div>:
                  <div fieldid="moneygroup">
                  <NCNumber
                      fieldid="moneygroup"
                      scale={this.state.detailTableData[index].rateData&&Number(this.state.detailTableData[index].rateData.groupscale)}
                      disabled={!this.state.detailTableData[index].isEdit}
                      value={
                        this.state.detailTableData[index].moneygroup.display
                      }
                      onBlur={(v) => {
                        let {detailTableData} = this.state;
                        v = this.addZero(v,Number(this.state.detailTableData[index].rateData.scale))
                        detailTableData[index].moneygroup.display = v;
                        detailTableData[index].moneygroup.value = v;
                        this.setState({
                          detailTableData
                        })
                      }}
                  />
                  </div>
                )
              });
            }else if(!nextProps.searchData.NC001){
              for(let i=0,len=columns.length;i<len;i++){
                if(columns[i].dataIndex=='moneygroup'){
                  columns.splice(i, 1);
                  return
                }
              }
            }
          }
          let moneyglobalNum ;
          if(columns[0].dataIndex!='pk_unit'){//集团本币是否显示
            if(columns[5].dataIndex!='moneygroup'){
              moneyglobalNum=5
            }else if(columns[5].dataIndex=='moneygroup'){
              moneyglobalNum=6
            }
          }else if(columns[0].dataIndex=='pk_unit'){
            if(columns[6].dataIndex!='moneygroup'){
              moneyglobalNum=6
            }else if(columns[6].dataIndex=='moneygroup'){
              moneyglobalNum=7
            }
          }
          if(nextProps.searchData.NC002&&columns[moneyglobalNum].dataIndex!='moneyglobal'){
            columns.splice(moneyglobalNum, 0, {
                title: (<div fieldid="moneyglobal" className="mergecells">{this.state.json['20020CFANY-000008']}</div>),
                dataIndex: "moneyglobal",
                key: "moneyglobal",
                width:'150px',
                className:'t-a-r',
                render: (text, record, index) => (
                  !record.isEdit?
                  <div fieldid="moneyglobal">{this.state.detailTableData[index].moneyglobal.display?this.state.detailTableData[index].moneyglobal.display:<span>&nbsp;</span>}</div>:
                  <div fieldid="moneyglobal">
                  <NCNumber
                    fieldid="moneyglobal"
                    scale={this.state.detailTableData[index].rateData&&Number(this.state.detailTableData[index].rateData.globalscale)}
                    disabled={!this.state.detailTableData[index].isEdit}
                    value={
                      this.state.detailTableData[index].moneyglobal.display
                    }
                    onBlur={(v) => {
                      let {detailTableData} = this.state;
                      v = this.addZero(v,Number(this.state.detailTableData[index].rateData.scale))
                      detailTableData[index].moneyglobal.display = v;	
                      detailTableData[index].moneyglobal.value = v;
                      this.setState({
                        detailTableData
                      })
                    }}
                />
                </div>
                )
              });
          }else if(!nextProps.searchData.NC002){
            for(let i=0,len=columns.length;i<len;i++){
              if(columns[i].dataIndex=='moneyglobal'){
                columns.splice(i, 1);
                return
              }
            }
          }
        }
        this.setState({
          columns
        })
    }
      
  }
  setJsonData=()=>{
    let callback= (json) =>{
         this.setState({json:json,
        columns:[
          {
            title: (<div fieldid="pk_cashflow" className="mergecells">{json['20020CFANY-000000']}</div>),
            dataIndex: "pk_cashflow",
            key: "pk_cashflow",
            width:'300px',
            render: (text, record, index) => (
              !record.isEdit?
              <div fieldid="pk_cashflow">{this.state.detailTableData[index].pk_cashflow.display?this.state.detailTableData[index].pk_cashflow.display:<span>&nbsp;</span>}</div>:
              <div fieldid="pk_cashflow">
              <CashflowTreeRef
                fieldid="pk_cashflow"
                value =  {{
                  refname: this.state.detailTableData[index].pk_cashflow.display, 
                  refpk: this.state.detailTableData[index].pk_cashflow.value
                }}
                disabled={!this.state.detailTableData[index].isEdit}
                queryCondition={{
                  "pk_accountingbook": this.state.pk_accountingbook,
                  "pk_org": this.state.pk_org,
                  isDataPowerEnable:'Y',
                  DataPowerOperationCode:'fi'
                }}
                onlyLeafCanSelect={true}
                isDataPowerEnable='Y'
                DataPowerOperationCode='fi'
                onChange={(v)=>{
                  let {detailTableData} = this.state;
                  detailTableData[index].pk_cashflow.display = v.refname;	
                  detailTableData[index].pk_cashflow.value = v.refpk;	
                  this.props.getChildTableData(detailTableData);
                  let data = { pk_cashflow_main:v.refpk,pk_cashflow_ass:''};
                  let self=this;
                  let url = '/nccloud/gl/glpub/cfitemquery.do';
                  ajax({
                    url,
                    data,
                    success: function(response) {
                        const { data, error, success } = response;
                        if (success&&data) {
                            //let direct = self.state.direct;
                            let direct = self.state.selectDetail.localcreditamount.value == 0?'in':'out';
                            if(data.ismain){
                              detailTableData[index].mainflag='Y'
                            }else{
                              detailTableData[index].mainflag='N'
                            }
                            if(data.ismain){ //主表项才需要计算
                              //if(data.itemtype){//判断正负
                              let nativeAmount;
                              if (self.state.selectDetail.localcreditamount.value == 0) {
                                nativeAmount = self.state.selectDetail.localdebitamount.value;
                              } else {
                                nativeAmount = self.state.selectDetail.localcreditamount.value;
                              }
                                if((direct=='in' && data.itemtype==1)
                                    ||(direct=='out' && data.itemtype==2)){
                                  // if(data.itemtype==1){
                                  //   detailTableData[index].isPlus='1'
                                  // }else{
                                  //   detailTableData[index].isPlus='0'
                                  // }
                                  if ((nativeAmount > 0 && detailTableData[index].moneymain.value > 0) 
                                        || (nativeAmount < 0 && detailTableData[index].moneymain.value < 0)) {
                                      detailTableData[index].money.value = -(detailTableData[index].money.value)
                                      detailTableData[index].money.display = -(detailTableData[index].money.display)
                                      detailTableData[index].moneymain.value = -(detailTableData[index].moneymain.value)
                                      detailTableData[index].moneymain.display = -(detailTableData[index].moneymain.display)
                                      detailTableData[index].moneygroup.value = -(detailTableData[index].moneygroup.value)
                                      detailTableData[index].moneygroup.display = -(detailTableData[index].moneygroup.display)
                                      detailTableData[index].moneyglobal.value = -(detailTableData[index].moneyglobal.value)
                                      detailTableData[index].moneyglobal.display = -(detailTableData[index].moneyglobal.display)
                                    }
                                } 
                                if((direct=='in' && data.itemtype==2)
                                          ||(direct=='out' && data.itemtype==1)){
                                        if ((nativeAmount > 0 && detailTableData[index].moneymain.value < 0) 
                                          || (nativeAmount < 0 && detailTableData[index].moneymain.value > 0)) {
                                          detailTableData[index].money.value = -(detailTableData[index].money.value)
                                          detailTableData[index].money.display = -(detailTableData[index].money.display)
                                          detailTableData[index].moneymain.value = -(detailTableData[index].moneymain.value)
                                          detailTableData[index].moneymain.display = -(detailTableData[index].moneymain.display)
                                          detailTableData[index].moneygroup.value = -(detailTableData[index].moneygroup.value)
                                          detailTableData[index].moneygroup.display = -(detailTableData[index].moneygroup.display)
                                          detailTableData[index].moneyglobal.value = -(detailTableData[index].moneyglobal.value)
                                          detailTableData[index].moneyglobal.display = -(detailTableData[index].moneyglobal.display)
                                        }
                                }
                                if(self.state.selectDetail.cashflag.value>0){
                                  detailTableData[index].money.value = -(detailTableData[index].money.value)
                                  detailTableData[index].money.display = -(detailTableData[index].money.display)
                                  detailTableData[index].moneymain.value = -(detailTableData[index].moneymain.value)
                                  detailTableData[index].moneymain.display = -(detailTableData[index].moneymain.display)
                                  detailTableData[index].moneygroup.value = -(detailTableData[index].moneygroup.value)
                                  detailTableData[index].moneygroup.display = -(detailTableData[index].moneygroup.display)
                                  detailTableData[index].moneyglobal.value = -(detailTableData[index].moneyglobal.value)
                                  detailTableData[index].moneyglobal.display = -(detailTableData[index].moneyglobal.display)
                                }
                              //}
                              // if(detailTableData[index].isPlus=='1'){
                              //     detailTableData[index].money.value = (detailTableData[index].money.value)
                              //     detailTableData[index].money.display = (detailTableData[index].money.display)
                              //     detailTableData[index].moneymain.value = (detailTableData[index].moneymain.value)
                              //     detailTableData[index].moneymain.display = (detailTableData[index].moneymain.display)
                              //     detailTableData[index].moneygroup.value = (detailTableData[index].moneygroup.value)
                              //     detailTableData[index].moneygroup.display = (detailTableData[index].moneygroup.display)
                              //     detailTableData[index].moneyglobal.value = (detailTableData[index].moneyglobal.value)
                              //     detailTableData[index].moneyglobal.display = (detailTableData[index].moneyglobal.display)
                              // }
                              // else if(detailTableData[index].isPlus=='0'){
                              //   detailTableData[index].money.value = -(detailTableData[index].money.value)
                              //     detailTableData[index].money.display = -(detailTableData[index].money.display)
                              //     detailTableData[index].moneymain.value = -(detailTableData[index].moneymain.value)
                              //     detailTableData[index].moneymain.display = -(detailTableData[index].moneymain.display)
                              //     detailTableData[index].moneygroup.value = -(detailTableData[index].moneygroup.value)
                              //     detailTableData[index].moneygroup.display = -(detailTableData[index].moneygroup.display)
                              //     detailTableData[index].moneyglobal.value = -(detailTableData[index].moneyglobal.value)
                              //     detailTableData[index].moneyglobal.display = -(detailTableData[index].moneyglobal.display)
                              // }
                              detailTableData.forEach((item,index,input)=>{  
                                item.money.value=item.money.display=self.addZero(item.money.value,item.rateData.scale); 
                                item.moneymain.value=item.moneymain.display=self.addZero(item.moneymain.value,item.rateData.orgscale); 
                                item.moneygroup.value=item.moneygroup.display=self.addZero(item.moneygroup.value,item.rateData.groupscale); 
                                item.moneyglobal.value=item.moneyglobal.display=self.addZero(item.moneyglobal.value,item.rateData.globalscale); 
                              })  
                            }
                            self.setState({
                              itemtype:data.itemtype,
                              detailTableData
                            })
                          } else {
                            // toast({ content: '无请求数据', color: 'warning' });
                        }
                    }
                  });                      
                }}
              />
              </div>
            )
          },
          {
            title: (<div fieldid="pk_innercorp" className="mergecells">{json['20020CFANY-000001']}</div>),
            dataIndex: "pk_innercorp",
            key: "pk_innercorp",
            width:'300px',
            render: (text, record, index) => (
              !record.isEdit?
              <div fieldid="pk_innercorp">{this.state.detailTableData[index].pk_innercorp.display?this.state.detailTableData[index].pk_innercorp.display:<span>&nbsp;</span>}</div>:
              <div fieldid="pk_innercorp">
              <FinanceOrgTreeRef
                fieldid="pk_innercorp"
                value =  {{
                  refname: this.state.detailTableData[index].pk_innercorp.display, 
                  refpk: this.state.detailTableData[index].pk_innercorp.value,
    
                }}
                disabled={!this.state.detailTableData[index].isEdit}
                onChange={(v)=>{
                  let {detailTableData} = this.state;
                  detailTableData[index].pk_innercorp.display = v.refname;	
                  detailTableData[index].pk_innercorp.value = v.refpk;	
                  this.props.getChildTableData(detailTableData);                     
                }}
              />
              </div>
            )
          },
          {
            title: (<div fieldid="pk_detail_opp" className="mergecells">{json['20020CFANY-000002']}</div>),
            dataIndex: "pk_detail_opp",
            key: "pk_detail_opp",
            width:'150px',
            render: (text, record, index) => (
              !record.isEdit?
              <div fieldid="pk_detail_opp">{this.state.detailTableData[index].pk_currtype.display?this.state.detailTableData[index].pk_currtype.display:<span>&nbsp;</span>}</div>:
              <div fieldid="pk_detail_opp">
              <CurrtypeGridRef
                fieldid="pk_detail_opp"
                value =  {{
                  refname: this.state.detailTableData[index].pk_currtype.display, 
                  refpk: this.state.detailTableData[index].pk_currtype.value
                }}
                disabled={!this.state.detailTableData[index].isEdit}
                onChange={(v)=>{
                  let {detailTableData} = this.state;
                  detailTableData[index].pk_currtype.display = v.refname;	
                  detailTableData[index].pk_currtype.value = v.refpk;	
                  this.props.getChildTableData(detailTableData);       
                  this.getRateData(v.refpk,index,true);          
                }}
              />
              </div>
            )
          },
          {
            title: (<div fieldid="money" className="mergecells">{json['20020CFANY-000003']}</div>),
            dataIndex: "money",
            key: "money",
            width:'150px',
            className:'t-a-r',
            render: (text, record, index) => (
              !record.isEdit?
              <div fieldid="money">{this.state.detailTableData[index].money.display?this.state.detailTableData[index].money.display:<span>&nbsp;</span>}</div>:
              <div fieldid="money">
              <NCNumber
                    fieldid="money"
                    scale={this.state.detailTableData[index].rateData&&Number(this.state.detailTableData[index].rateData.scale)}
                    disabled={!this.state.detailTableData[index].isEdit}
                    value={
                      this.state.detailTableData[index].money.display
                    }
                    onBlur={(v) => {
                      let {detailTableData} = this.state;
                      // v = this.addZero(v,Number(this.state.detailTableData[index].rateData.scale))
                      if(detailTableData[index].money.display*100!=v*100){
                        detailTableData[index].money.display = v;	
                        detailTableData[index].money.value = v;
                        this.onRateChange( '',2,index,detailTableData);
                      }
                    }}
                />
              </div>
            )
          },
          {
            title: (<div fieldid="moneymain" className="mergecells">{json['20020CFANY-000004']}</div>),
            dataIndex: "moneymain",
            key: "moneymain",
            width:'150px',
            className:'t-a-r',
            render: (text, record, index) => (
              !record.isEdit?
              <div fieldid="moneymain">{this.state.detailTableData[index].moneymain.display?this.state.detailTableData[index].moneymain.display:<span>&nbsp;</span>}</div>:
              <div fieldid="moneymain">
              <NCNumber
                  fieldid="moneymain"
                  scale={this.state.detailTableData[index].rateData&&Number(this.state.detailTableData[index].rateData.orgscale)}
                  disabled={!this.state.detailTableData[index].isEdit}
                  value={
                    this.state.detailTableData[index].moneymain.display
                  }
                  onBlur={(v) => {
                    let {detailTableData} = this.state;
                    // v = this.addZero(v,Number(this.state.detailTableData[index].rateData.scale))
                    if(detailTableData[index].moneymain.display!=v){
                      detailTableData[index].moneymain.display = v;
                      detailTableData[index].moneymain.value = v;
                      this.onRateChange( '',3,index,detailTableData);
                    }
                  }}
              />
              </div>
            )
          },
          {
            title: (<div fieldid="cjijjz" className="mergecells">{json['20020CFANY-000005']}</div>),
            dataIndex: "cjijjz",
            key: "cjijjz",
            width:'80px',
            fixed:'right',
            render: (text, record, index) => (
              !record.isEdit?
              <div fieldid="cjijjz"></div>
              :<a size='sm' colors="warning" shape='border' fieldid="cjijjz"
              className=''
              // style={{'display':(this.state.detailTableData[index].isEdit?'':'none')}}
              onClick={
                (e)=>{
                  e.stopPropagation();
                  e.preventDefault();
                  let {detailTableData,deleteStr} = this.state;
                  deleteStr=detailTableData[index].pk_detail.value;
                  detailTableData.splice(index,1);
                  this.setState({
                    detailTableData,
                    deleteStr
                  })
                }
              }>{json['20020CFANY-000009']}</a>
            )
          },
        ]
      },()=>{ })
          }
      getMultiLang({moduleId:'20020CFANY',domainName:'gl',currentLocale:'simpchn',callback}); 
  }
  clickRow=(record, index)=>{
    this.setState({
      selectItem:record
    })
    //查询该项项税率信息
    let detailTableData = this.state.detailTableData;
    if(!detailTableData[index].rateData.excrate2){
      this.getRateData(detailTableData[index].pk_currtype.value,index)
    }
  }
  getBack=(data)=>{
    let flag=true;
    for(let i=0,len=data.length;i<len;i++){
      data[i].key=i+1;
      if(data[i].isEdit){
        flag=false
      }
    }
    if(flag){
      this.props.getSaveEdit(false)
    }else{
      this.props.getSaveEdit(true)
    }
    this.props.getChildTableData(data);
  }
  //获取当前日期
  getNowFormatDate=()=> {
    let date = new Date();
    let seperator1 = "-";
    let seperator2 = ":";
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
          
    return currentdate;
  }
  //获取编辑状态
  getSaveEdit=(data,num)=>{
    this.props.getSaveEdit(data,num);
  }
  //获取汇率
  getRateData=(pk_currtype,index,type,detailTableData)=>{
    let {pk_accountingbook} = this.state;
    let date = this.state.selectDetail.prepareddate.value;//日期
    if(!detailTableData){
      detailTableData=this.state.detailTableData;
    }
    let data = {pk_accountingbook:pk_accountingbook,pk_currtype:pk_currtype,date:date};
    let self=this;
    let url = '/nccloud/gl/glpub/ratequery.do';
    ajax({
      url,
      data,
      success: function(response) {
          const { data, error, success } = response;
          if (success&&data) {
            detailTableData[index].rateData=data
            self.setState({
              detailTableData
            })
            if(type){
              self.onRateChange(data,1,index)
            }
          }
        }
      });
  }
  //税率换算
  onRateChange=(sendRateData,type,index)=>{
    setTimeout(
      this.onRateChangeHold(sendRateData,type,index),
      500)
  }
  onRateChangeHold=(sendRateData,type,index)=>{
    let {detailTableData,pk_accountingbook} = this.state;
    let rateData = sendRateData.excrate2?sendRateData:detailTableData[index].rateData;
    let rate = rateData.excrate2;
    let rate3 = rateData.excrate3;
    let rate4 = rateData.excrate4;
    if(type==1){//修改币种时
      if(rateData.orgmode){//判断组织本币
        detailTableData[index].moneymain.display = Number(detailTableData[index].money.display)/rate;
        detailTableData[index].moneymain.value = Number(detailTableData[index].money.value)/rate;	
      }else if(!rateData.orgmode){
        detailTableData[index].moneymain.display = Number(detailTableData[index].money.display)*rate;
        detailTableData[index].moneymain.value = Number(detailTableData[index].money.value)*rate;	
      }
      if(rateData.NC001=='local_convert'){//判断集团本币
        if(rateData.groupmode){
          detailTableData[index].moneygroup.display = Number(detailTableData[index].moneymain.display)/rate3;
          detailTableData[index].moneygroup.value = Number(detailTableData[index].moneymain.value)/rate3;	
        }else if(!rateData.groupmode){
          detailTableData[index].moneygroup.display = Number(detailTableData[index].moneymain.display)*rate3;
          detailTableData[index].moneygroup.value = Number(detailTableData[index].moneymain.value)*rate3;	
        }
      }else if(rateData.NC001=='raw_convert'){
        if(rateData.groupmode){
          detailTableData[index].moneygroup.display = Number(detailTableData[index].money.display)/rate3;
          detailTableData[index].moneygroup.value = Number(detailTableData[index].money.value)/rate3;	
        }else if(!rateData.groupmode){
          detailTableData[index].moneygroup.display = Number(detailTableData[index].money.display)*rate3;
          detailTableData[index].moneygroup.value = Number(detailTableData[index].money.value)*rate3;	
        }
      }
      if(rateData.NC002=='local_convert'){//判断全局本币
        if(rateData.globalmode){
          detailTableData[index].moneyglobal.display = Number(detailTableData[index].moneymain.display)/rate4;
          detailTableData[index].moneyglobal.value = Number(detailTableData[index].moneymain.value)/rate4;	
        }else if(!rateData.globalmode){
          detailTableData[index].moneyglobal.display = Number(detailTableData[index].moneymain.display)*rate4;
          detailTableData[index].moneyglobal.value = Number(detailTableData[index].moneymain.value)*rate4;	
        }
      }else if(rateData.NC002=='raw_convert'){
        if(rateData.globalmode){
          detailTableData[index].moneyglobal.display = Number(detailTableData[index].money.display)/rate4;
          detailTableData[index].moneyglobal.value = Number(detailTableData[index].money.value)/rate4;	
        }else if(!rateData.globalmode){
          detailTableData[index].moneyglobal.display = Number(detailTableData[index].money.display)*rate4;
          detailTableData[index].moneyglobal.value = Number(detailTableData[index].money.value)*rate4;	
        }
      }
    }
  else if(type==2){//修改本币时
    if(rateData.orgmode){//判断组织本币
      detailTableData[index].moneymain.display = Number(detailTableData[index].money.display)/rate;
      detailTableData[index].moneymain.value = Number(detailTableData[index].money.value)/rate;	
    }else if(!rateData.orgmode){
      detailTableData[index].moneymain.display = Number(detailTableData[index].money.display)*rate;
      detailTableData[index].moneymain.value = Number(detailTableData[index].money.value)*rate;	
    }
    if(rateData.NC001=='local_convert'){//判断集团本币
      if(rateData.groupmode){
        detailTableData[index].moneygroup.display = Number(detailTableData[index].moneymain.display)/rate3;
        detailTableData[index].moneygroup.value = Number(detailTableData[index].moneymain.value)/rate3;	
      }else if(!rateData.groupmode){
        detailTableData[index].moneygroup.display = Number(detailTableData[index].moneymain.display)*rate3;
        detailTableData[index].moneygroup.value = Number(detailTableData[index].moneymain.value)*rate3;	
      }
    }else if(rateData.NC001=='raw_convert'){
      if(rateData.groupmode){
        detailTableData[index].moneygroup.display = Number(detailTableData[index].money.display)/rate3;
        detailTableData[index].moneygroup.value = Number(detailTableData[index].money.value)/rate3;	
      }else if(!rateData.groupmode){
        detailTableData[index].moneygroup.display = Number(detailTableData[index].money.display)*rate3;
        detailTableData[index].moneygroup.value = Number(detailTableData[index].money.value)*rate3;	
      }
    }
    if(rateData.NC002=='local_convert'){//判断全局本币
      if(rateData.globalmode){
        detailTableData[index].moneyglobal.display = Number(detailTableData[index].moneymain.display)/rate4;
        detailTableData[index].moneyglobal.value = Number(detailTableData[index].moneymain.value)/rate4;	
      }else if(!rateData.globalmode){
        detailTableData[index].moneyglobal.display = Number(detailTableData[index].moneymain.display)*rate4;
        detailTableData[index].moneyglobal.value = Number(detailTableData[index].moneymain.value)*rate4;	
      }
    }else if(rateData.NC002=='raw_convert'){
      if(rateData.globalmode){
        detailTableData[index].moneyglobal.display = Number(detailTableData[index].money.display)/rate4;
        detailTableData[index].moneyglobal.value = Number(detailTableData[index].money.value)/rate4;	
      }else if(!rateData.globalmode){
        detailTableData[index].moneyglobal.display = Number(detailTableData[index].money.display)*rate4;
        detailTableData[index].moneyglobal.value = Number(detailTableData[index].money.value)*rate4;	
      }
    }
  }else if(type==3){//修改组织本币
    if(rateData.NC001=='local_convert'){//判断集团本币
      if(rateData.groupmode){
        detailTableData[index].moneygroup.display = Number(detailTableData[index].moneymain.display)/rate3;
        detailTableData[index].moneygroup.value = Number(detailTableData[index].moneymain.value)/rate3;	
      }else if(!rateData.groupmode){
        detailTableData[index].moneygroup.display = Number(detailTableData[index].moneymain.display)*rate3;
        detailTableData[index].moneygroup.value = Number(detailTableData[index].moneymain.value)*rate3;	
      }
    }else if(rateData.NC001=='raw_convert'){
      if(rateData.groupmode){
        detailTableData[index].moneygroup.display = Number(detailTableData[index].money.display)/rate3;
        detailTableData[index].moneygroup.value = Number(detailTableData[index].money.value)/rate3;	
      }else if(!rateData.groupmode){
        detailTableData[index].moneygroup.display = Number(detailTableData[index].money.display)*rate3;
        detailTableData[index].moneygroup.value = Number(detailTableData[index].money.value)*rate3;	
      }
    }
    if(rateData.NC002=='local_convert'){//判断全局本币
      if(rateData.globalmode){
        detailTableData[index].moneyglobal.display = Number(detailTableData[index].moneymain.display)/rate4;
        detailTableData[index].moneyglobal.value = Number(detailTableData[index].moneymain.value)/rate4;	
      }else if(!rateData.globalmode){
        detailTableData[index].moneyglobal.display = Number(detailTableData[index].moneymain.display)*rate4;
        detailTableData[index].moneyglobal.value = Number(detailTableData[index].moneymain.value)*rate4;	
      }
    }else if(rateData.NC002=='raw_convert'){
      if(rateData.globalmode){
        detailTableData[index].moneyglobal.display = Number(detailTableData[index].money.display)/rate4;
        detailTableData[index].moneyglobal.value = Number(detailTableData[index].money.value)/rate4;	
      }else if(!rateData.globalmode){
        detailTableData[index].moneyglobal.display = Number(detailTableData[index].money.display)*rate4;
        detailTableData[index].moneyglobal.value = Number(detailTableData[index].money.value)*rate4;	
      }
    }	
  }else if(type==4){//修改集团本币
      // if(rate==1){
      //   detailTableData[index].money.display = Number(detailTableData[index].moneygroup.display)/rate3;	
      //   detailTableData[index].money.value = Number(detailTableData[index].moneygroup.value)/rate3;
      // }
      // detailTableData[index].moneymain.display = Number(detailTableData[index].moneygroup.display)/rate3;
      // detailTableData[index].moneymain.value = Number(detailTableData[index].moneygroup.value)/rate3;	
  
    }
    detailTableData.forEach((item,index,input)=>{  
      item.money.value=item.money.display=this.addZero(item.money.value,item.rateData.scale); 
      item.moneymain.value=item.moneymain.display=this.addZero(item.moneymain.value,item.rateData.orgscale); 
      item.moneygroup.value=item.moneygroup.display=this.addZero(item.moneygroup.value,item.rateData.groupscale); 
      item.moneyglobal.value=item.moneyglobal.display=this.addZero(item.moneyglobal.value,item.rateData.globalscale); 
    })  
    this.setState({
      detailTableData:detailTableData,
      rateData
    })                               
  }
  addZero=(num, scale)=>{
    if (isNaN(Number(num)) || !num) {
			return null;
    }
    num =String(num);
    scale = Number(scale);
		if (scale > 0) {
			let start = num.split('.')[0];
			let end = num.split('.')[1];
			if (!end) {
				end = '';
			}
			let len = end.length;
			if (len < scale) {
				end = end.padEnd(scale, '0');
			}
			return start + '.' + end;
		} else {
			return num;
		}
  }
  renderColumnsMultiSelect(columns) {
    const { data,checkedArray,tableData } = this.state;
    let { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // multiSelect= "checkbox";
    // let indeterminate_bool1 = true;
    let defaultColumns = [
      {
        title: (
          ''
        ),
        key: "checkbox",
        attrcode: "checkbox",
        dataIndex: "checkbox",
        width: "0px",
        render: (text, record, index) => {
          return (
            ''
          );
        }
      }
    ];
    // columns = defaultColumns.concat(columns);
    return columns;
  }
  render() {
    let {detailTableData,selectDetail,selectItem,cashFlag,deleteStr}=this.state;
    let columns = this.renderColumnsMultiSelect(this.state.columns);
    return (
        <div className="CashFlowItemTable">
           <TableButtons detailTableData={detailTableData} selectItem={selectItem} 
            getBack={this.getBack.bind(this)} selectDetail={selectDetail}
            // handleCancel={this.handleCancel.bind(this)}
            deleteStr={deleteStr}
            cashFlag={cashFlag}
            searchData={this.props.searchData}
            saveEditChild={this.props.saveEditChild}
            getSaveEdit={this.getSaveEdit.bind(this)}
            saveNum={this.props.saveNum}
            ></TableButtons>
          <div>
            <div style={{'position':'relative'}}>
              <span  className={'cashflowxx'+' '+( (this.state.detailTableData.length?this.state.detailTableData[0].isEdit:'')?'':'disN')}>*</span>
            </div>
            <NCDiv fieldid="cashanalyseitem" areaCode={NCDiv.config.TableCom}>
              <Table
                  columns={columns}
                  data={detailTableData}
                  onRowClick={this.clickRow.bind(this)}
                  bodyStyle={{height:getTableHeight(300)}}
                scroll={{ x: true, y: getTableHeight(300) }}
              />
            </NCDiv>
          </div>
        </div>
    );
  }
}