import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import { toast } from '../../../../public/components/utils.js';
import {high,base,ajax,promptBox,getMultiLang,createPage } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCButtonGroup:ButtonGroup
    } = base;
import './index.less';
class TableButtons extends Component{
    constructor(props){
        super(props);
        this.state = {
            json:{},
            detailTableData:[],//子表数据
            selectDetail:{},//选中父表值
            isLoading:false,
            deleteStr:'',//删除项，给后台用
            cashFlag:'',//现金判断
            isEdit:false,//编辑状态
            rateData:'',//判断有没有税率用
            saveEditChild:'',
            saveNum:'',
            sended:false
        }
    }
    componentWillMount(){
        // let callback= (json) =>{
        //        this.setState({json:json},()=>{ })
        //         }
        // getMultiLang({moduleId:'20020CFANY',domainName:'gl',currentLocale:'simpchn',callback}); 
        let pagecode = this.props.getSearchParam('p')
        let appcode  = this.props.getSearchParam('c')
        let self = this;
        ajax({
            url: '/nccloud/platform/appregister/queryallbtns.do',
            data: {
                pagecode: pagecode,
                appcode: appcode
            },
            success: (res) => {
                if(!res.data){
                    return
                }
                /*  拿到按钮数据，调用setButtons方法将数据设置到页面上，然后执行后续的按钮逻辑 */
                self.props.button.setButtons(res.data);
                // self.props.button.setButtonDisabled({'save':true,'cancel':true,'add':true,'edit':true})
                self.props.button.setButtonVisible({'save':false,'cancel':false,'add':false,'edit':false})
            }
        }); 
        let callback= (json) =>{
           this.setState({json:json},()=>{ })
            }
        getMultiLang({moduleId:'20020CFANY',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    
    // shouldComponentUpdate(nextProps){
    //     return this.state.checked === nextState.checked;
    // }
    componentWillReceiveProps(nextProps){
        let selectDetail = nextProps.selectDetail;
        let cashFlag = nextProps.cashFlag;
        this.props.button.setMainButton({'save':false,'add':true})
        // this.props.button.setButtonDisabled({'add':false,'edit':false}) 
        this.props.button.setButtonVisible({'add':true,'edit':true}) 
        if(nextProps.detailTableData&&nextProps.detailTableData.length>0){
            this.props.button.setButtonVisible({'edit':true})
            if(nextProps.detailTableData[0].isEdit){
                this.props.button.setButtonVisible({'edit':false}) 
                this.props.button.setMainButton({'save':true,'add':false})
            }
        }else{
            this.props.button.setButtonVisible({'edit':false}) 
        }
        // this.state.saveEditChild!=nextProps.saveEditChild;
        
        if(nextProps.saveEditChild==1&&!nextProps.saveNum&&!this.state.sended){
            this.handleSave();
            this.setState({
                saveNum:nextProps.saveNum,
                sended:true
            })
        }else if(nextProps.saveEditChild==2){
            // this.props.button.setButtonDisabled({'save':true,'cancel':true,'add':false})
            this.props.button.setButtonVisible({'save':false,'cancel':false,'add':true})
            this.props.button.setButtonVisible({'edit':false}) 
        }
        if(JSON.stringify(selectDetail)!='{}'){
            if(selectDetail.editable.value=='N'){
                // this.props.button.setButtonDisabled({'add':true}) 
                this.props.button.setButtonVisible({'add':false}) 
                this.props.button.setButtonVisible({'edit':false}) 
            }else{
                // this.props.button.setButtonDisabled({'add':false}) 
                this.props.button.setButtonVisible({'add':true}) 
            }
        }
        this.setState({
                detailTableData:nextProps.detailTableData,
                selectDetail,
                cashFlag
        })
    }
    //增加
    handleAdd=()=>{
        let {detailTableData,selectDetail,rateData} = this.state;
        if(selectDetail.key<0){
            toast({ content: this.state.json['20020CFANY-000010'], color: 'warning' });
            return;
        }
        let date = selectDetail.prepareddate.value;//日期
        let data = {pk_accountingbook:selectDetail.pk_accountingbook.value,pk_currtype:selectDetail.pk_currtype.value,date:date};
        let self=this;
        let url = '/nccloud/gl/glpub/ratequery.do';
        let obj={
            isEdit:true,
            key:detailTableData.length+1,
            "pk_cashflowcase":{"display":"","value":"","scale":0},
            //贷方或借方取有值的
            "money":{"display":selectDetail.debitamount.value!=0?selectDetail.debitamount.value:selectDetail.creditamount.value,
                "value":selectDetail.debitamount.value!=0?selectDetail.debitamount.value:selectDetail.creditamount.value,
                "scale":2},
            "moneymain":{"display":selectDetail.localdebitamount.value!=0?selectDetail.localdebitamount.value:selectDetail.localcreditamount.value,
                "value":selectDetail.localdebitamount.value!=0?selectDetail.localdebitamount.value:selectDetail.localcreditamount.value,
                "scale":2},
            "moneygroup":{"display":selectDetail.groupdebitamount.value!=0?selectDetail.groupdebitamount.value:selectDetail.groupcreditamount.value,
                "value":selectDetail.groupdebitamount.value!=0?selectDetail.groupdebitamount.value:selectDetail.groupcreditamount.value,
                "scale":2},
            "moneyglobal":{"display":selectDetail.globaldebitamount.value!=0?selectDetail.globaldebitamount.value:selectDetail.globalcreditamount.value,
                "value":selectDetail.globaldebitamount.value!=0?selectDetail.globaldebitamount.value:selectDetail.globalcreditamount.value,
                "scale":2},
            "pk_cashflow":{"display":"","value":"","scale":0},
            "pk_detail":{"display":selectDetail.pk_detail.display,"value":selectDetail.pk_detail.value,"scale":0},
            "pk_detail_opp":selectDetail.pk_currtype.value,
            "pk_accountingbook":{"display":selectDetail.pk_accountingbook.display,"value":selectDetail.pk_accountingbook.value,"scale":0},
            "pk_currtype":{"display":selectDetail.pk_currtype.display,"value":selectDetail.pk_currtype.value,"scale":0},
            "pk_unit":{"display":selectDetail.pk_unit.display,"value":selectDetail.pk_unit.value,"scale":0},
            "pk_innercorp":{display:selectDetail.innercorp?selectDetail.innercorp.display:'', value: selectDetail.innercorp?selectDetail.innercorp.value:'', scale: "0"},
            "quickflag":{"display":"","value":"","scale":0}
        }
        //isMAIN 时减去
        let money = Number(obj.money.value);
        let moneymain=Number(obj.moneymain.value);
        let moneygroup=Number(obj.moneygroup.value);
        let moneyglobal=Number(obj.moneyglobal.value);
        
        for(let i=0,len=detailTableData.length;i<len;i++){
            if(detailTableData[i].mainflag=='Y'){
                money=money-Number(detailTableData[i].money.value);
                moneymain=moneymain-Number(detailTableData[i].moneymain.value);
                moneygroup=moneygroup-Number(detailTableData[i].moneygroup.value);
                moneyglobal=moneyglobal-Number(detailTableData[i].moneyglobal.value);
            }
        }
        obj.money.display=money;
        obj.moneymain.display=moneymain;
        obj.moneygroup.display=moneygroup;
        obj.moneyglobal.display=moneyglobal;
        obj.money.value=money;
        obj.moneymain.value=moneymain;
        obj.moneygroup.value=moneygroup;
        obj.moneyglobal.value=moneyglobal;
        if(!rateData||selectDetail.key!=rateData.key){//获取税率
            ajax({
                url,
                data,
                success: function(response) {
                    const { data, error, success } = response;
                    if (success&&data) {
                        data.key = selectDetail.key
                        self.setState({
                            rateData:data
                        })
                        obj.rateData=data
                        obj.money.value=obj.money.display=self.addZero(obj.money.value,obj.rateData.scale); 
                        obj.moneymain.value=obj.moneymain.display=self.addZero(obj.moneymain.value,obj.rateData.orgscale); 
                        obj.moneygroup.value=obj.moneygroup.display=self.addZero(obj.moneygroup.value,obj.rateData.groupscale); 
                        obj.moneyglobal.value=obj.moneyglobal.display=self.addZero(obj.moneyglobal.value,obj.rateData.globalscale); 
                        detailTableData=[...detailTableData,obj]
                        self.props.getBack(detailTableData);
                        self.setState({
                            isEdit:true
                        })
                    }
                    }
                });
        }else{
            detailTableData=[...detailTableData,obj]
            obj.rateData=rateData
            detailTableData.forEach((item,index,input)=>{  
                item.money.value=item.money.display=self.addZero(item.money.value,item.rateData.scale); 
                item.moneymain.value=item.moneymain.display=self.addZero(item.moneymain.value,item.rateData.orgscale); 
                item.moneygroup.value=item.moneygroup.display=self.addZero(item.moneygroup.value,item.rateData.groupscale); 
                item.moneyglobal.value=item.moneyglobal.display=self.addZero(item.moneyglobal.value,item.rateData.globalscale); 
            })  
            this.props.getBack(detailTableData);
            this.setState({
                isEdit:true
            })
        }
        this.setButtonDis(true);
    }
    addZero=(num, scale)=>{
        if (isNaN(Number(num)) || !num) {
                return null;
        }
        num =String(num);
        scale = Number(scale);
        if (scale > 0) {
            let start = num.split('.')[0];
            let end = num.split('.')[1];
            if (!end) {
                end = '';
            }
            let len = end.length;
            if (len < scale) {
                end = end.padEnd(scale, '0');
            }
            return start + '.' + end;
        } else {
            return num;
        }
    }
    //保存
    handleSave=()=>{
        let self = this;
        let {detailTableData,selectDetail} = this.state;
        for(let i=0,len=detailTableData.length;i<len;i++){
            if(!detailTableData[i].pk_cashflow.display){
                toast({ content: this.state.json['20020CFANY-000011'], color: 'warning' });
                return;
            }
        }
        let localDetailTableData = Object.assign({},detailTableData);
        let data = {
            insert:detailTableData,
            delete:this.props.deleteStr,
            detail:selectDetail
        }
        if(!this.props.searchData.NC002){
            data.moneyglobal={
                "display":null,
                "value":null,
                "scale":2
            }
        }
        // this.setState({
        //     isLoading:true
        // })
        let url = "/nccloud/gl/cashflow/savecf.do";
        ajax({
            url,
            data,
            success: function(response) {
                // self.setState({
                //     isLoading: false
                // });
                const { data, error, success } = response;
                if (success&&data) {
                    let num = Math.random();
                    self.props.getSaveEdit(false,num);
                    for(let i=0,len=data.length;i<len;i++){
                        data[i].rateData=localDetailTableData[i].rateData;
                        data[i].isEdit=false
                    }
                    self.setState({
                        detailTableData:data,
                        isEdit:false
                    })
                    self.props.getBack(self.state.detailTableData);
                    toast({ content: self.state.json['20020CFANY-000012'], color: 'success' });
                    self.setButtonDis(false);
                } else {
                    self.setState({
                        detailTableData:[],
                        isEdit:false
                    })
                    let num = Math.random();
                    self.props.getSaveEdit(false,num);
                    self.setButtonDis(false);
                }
                // if(JSON.stringify(self.state.detailTableData)!=JSON.stringify(self.props.detailTableData)){
                //     self.props.getBack(self.state.detailTableData);
                // }
            }
        });
    }
    //取消
    handleCancel=()=>{
        let self = this;
        let {selectDetail,rateData} = this.state;
        // let detailTableData=this.state.detailTableData;
        // if(detailTableData.length==0){
        //     toast({ content: this.state.json['20020CFANY-000013'], color: 'warning' });
        //     return;
        // }
        let data = {
            pk_detail: selectDetail.pk_detail.value//detailTableData[0].pk_detail.value
        };
        let url = "/nccloud/gl/cashflow/querycfcase.do";
        ajax({
            url,
            data,
            success: function(response) {
                const { data, error, success } = response;
                if (success&&data) {
                    for(let i=0,len=data.length;i<len;i++){
                        data[i].rateData=rateData
                        data[i].isEdit=false
                    }
                    self.setState({
                        detailTableData:data,
                        isEdit:false
                    })
                } else {
                    self.setState({
                        detailTableData:[],
                        isEdit:false
                    })
                    // toast({ content: '无请求数据', color: 'warning' });
                }
                self.props.getBack(self.state.detailTableData);
            }
        });
        this.setButtonDis(false);
      }
    //编辑
    handlerRevise=()=>{
        let {detailTableData,isEdit} = this.state;
        if(detailTableData.length==0){
            toast({ content: this.state.json['20020CFANY-000013'], color: 'warning' });
            return;
        }
        for(let i=0,len=detailTableData.length;i<len;i++){//添加编辑状态
            detailTableData[i].isEdit=true;
            this.props.getBack(detailTableData);
        }   
        isEdit=true;
        this.setState({isEdit})
        this.setButtonDis(true);
    }
    onButtonClick=(props, id)=>{
        switch (id) {
            case 'add':
                this.handleAdd();
                break;
            case 'edit':
                this.handlerRevise();
                break;
            case 'save':
                this.handleSave();
                break;
            case 'cancel':
                promptBox({
                    color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                    title: this.state.json['20020CFANY-000014'],                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                    content: this.state.json['20020CFANY-000015'],             // 提示内容,非必输
                    beSureBtnName: this.state.json['20020CFANY-000016'],          // 确定按钮名称, 默认为"确定",非必输
                    cancelBtnName: this.state.json['20020CFANY-000017'],         // 取消按钮名称, 默认为"取消",非必输
                    hasCloseBtn:false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
                    beSureBtnClick: ()=>{
                        this.handleCancel()
                    },   // 确定按钮点击调用函数,非必输
                })
                break;
            default:
                break;
        }
    }
    setButtonDis=(isEdit)=>{
        if(!isEdit){
            // this.props.button.setButtonDisabled({'save':true,'cancel':true}) 
            this.props.button.setButtonVisible({'save':false,'cancel':false}) 
            this.props.button.setButtonVisible({'edit':true}) 
            this.props.button.setMainButton({'save':false,'add':true})
        }else{
            this.props.button.setButtonVisible({'edit':false}) 
            // this.props.button.setButtonDisabled({'save':false,'cancel':false})
            this.props.button.setButtonVisible({'save':true,'cancel':true})
            this.props.button.setMainButton({'save':true,'add':false})
        }
    }
    render(){
        let { cashFlag,selectDetail,isEdit}=this.state;
        let { createButtonApp } = this.props.button;
        return (
            <div className="verification-buttons clearfix positon-t0 nc-theme-area-bgc nc-theme-form-label-c nc-theme-area-split-bc">
                <div className='fl'  fieldid="header_area">
                    <h3  fieldid={`${this.state.json['20020CFANY-000000']}_title`}>{this.state.json['20020CFANY-000000']}</h3>
                </div>
                <div className="fr" >
                    <div className="header-button-area" style={{'float':'left'}}>
                        {createButtonApp({
                            area: 'body1',
                            onButtonClick: this.onButtonClick.bind(this), 
                            popContainer: document.querySelector('.header-button-area')
                        })}
                    </div>
                    <div className="header-button-area" style={{'float':'left'}}>
                        {createButtonApp({
                            area: 'body2',
                            onButtonClick: this.onButtonClick.bind(this), 
                            popContainer: document.querySelector('.header-button-area')
                        })}
                    </div>
                </div>
            </div>
        )
    }
}
TableButtons = createPage({})(TableButtons);
export default TableButtons;
