import React, {
  Component
} from "react";
import {
  high,
  base
} from 'nc-lightapp-front';
const {Refer} = high;
const {NCRow,NCCol}=base;
import createScript from '../../public/components/uapRefer';
export default class AccountBookTreeRef extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentWillReceiveProps(nextProps) {

  }
  render() {
    let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
    let mybook;
		if(!this.state[this.props.attrcode]){
            {createScript.call(this,referUrl,this.props.attrcode)}
		}else{
			mybook = (
				<NCRow>
					<NCCol xs={12} md={12}>
					{this.state[this.props.attrcode]?(this.state[this.props.attrcode])(
            {
              fieldid:this.props.fieldid?this.props.fieldid:'AccountBookTreeRef',
              value: this.props.value,
              disabledDataShow: true,
              queryCondition: this.props.queryCondition,
              onChange: this.props.onChange,
              disabled:this.props.disabled
						}
					):<div/>}
					</NCCol>
				</NCRow>
      );
		}
    return (
      <div>
          {mybook}
      </div>

    );
  }
}
AccountBookTreeRef.defaultProps={
  attrcode:'defaultAttrcode'
};