import React, {
    Component
} from 'react';
import ReactDOM from 'react-dom';
import {
    high,
    base,
    createPage,
    ajax,
    getMultiLang
} from 'nc-lightapp-front';
const {
    NCFormControl: FormControl,
    NCDatePicker: DatePicker,
    NCButton: Button,
    NCRadio: Radio,
    NCBreadcrumb: Breadcrumb,
    NCRow: Row,
    NCCol: Col,
    NCTree: Tree,
    NCMessage: Message,
    NCIcon: Icon,
    NCLoading: Loading,
    NCTable: Table,
    NCSelect: Select,
    NCCheckbox: Checkbox,
    NCNumber,
    AutoComplete,
    NCDropdown: Dropdown,
    NCButtonGroup: ButtonGroup,
    NCModal: Modal,
    NCPopconfirm: Popconfirm,NCDiv
} = base;
import {
    toast
} from '../../../public/components/utils.js';
const {
    Refer
} = high;
import createScript from '../../../public/components/uapRefer.js';
import AccountBookTreeRef from '../../pk_book/refer_pk_book';
import BusinessUnitWithGlobleAndCurrGroupTreeRef from '../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef'
class CashFlowSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            pk_accountingbook: {
                value: '',
                display: ''
            },
            pk_unitState:'',
            pk_unit:{},
            DataSource: [{
                key: '',
                value: ""
            }],
            defaultValueS: '',
            addState: '',
            selectValue: '',
            isEdit: false,
            bizDate:''
        };
    }
    componentWillMount() {
        let url = '/nccloud/platform/appregister/queryappcontext.do';
        let data = {
            appcode: this.props.getSearchParam('c')
        }
        let self = this;
        ajax({
            url,
            data,
            success: function(response) {
                const {
                    data,
                    error,
                    success
                } = response;
                if (success && data) {
                    let pk_accountingbook = {
                        display: data.defaultAccbookName,
                        value: data.defaultAccbookPk
                    }
                    self.setState({
                        pk_accountingbook
                    })
                    let url1 = "/nccloud/gl/cashflow/isbustartflag.do";
                    ajax({
                        url:'/nccloud/gl/voucher/queryBookCombineInfo.do',
                        data:{"pk_accountingbook":pk_accountingbook.value,'needaccount':false},
                        success: function(response) { 
                            const { data, error, success } = response;
                            let {bizDate}=self.state;
                            if (success&&data) {
                                bizDate=data.bizDate;
                                let pk_unit={refpk:data.unit.value,refname:data.unit.display};
                                self.setState({
                                    bizDate,
                                    pk_unit
                                })
                                ajax({
                                    url:url1,
                                    data:{"pk_accountingbook":pk_accountingbook.value},
                                    success: function(response) { 
                                        const { data, error, success } = response;
                                        let pk_unitState=null;
                                        if (success&&data) {
                                            if(data=='Y'){
                                                pk_unitState=true;
                                                self.setState({
                                                    pk_unitState:true
                                                });
                                                self.props.getPk(pk_accountingbook,pk_unit,pk_unitState);
                                            }else{
                                                pk_unitState=false;
                                                self.setState({
                                                    pk_unitState:false
                                                });
                                                self.props.getPk(pk_accountingbook,{refpk:'',refname:''});
                                            }
                                        } else {
                                            
                                        }    
                                    }
                                });
                            } else {
                                
                            }    
                        }
                    })
                } else {}
            }
        });
        let callback= (json) =>{
            this.setState({json:json,
            DataSource:[{
                key:json['20020BURLS-000024'],
                value: ""
            }],
            defaultValueS:json['20020BURLS-000024']
        },()=>{
//                 initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'20020BURLS',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps) {
        let DataSource = nextProps.dataSource;
        let defaultValueS = nextProps.defaultName;
        let selectValue = nextProps.selectValue;
        let isEdit = nextProps.isEditState;
        if(DataSource){
            if(DataSource.length==0){
                DataSource=[
                    {
                        key:this.state.json['20020BURLS-000024'],
                        value: ""
                    }
                ]
            }
        }
        this.setState({
            DataSource,
            defaultValueS,
            addState: nextProps.addState,
            isEdit,
            ruleNamedis:defaultValueS,
            selectValue
        })
    }
    changeState = () => {
        let stateType = this.state.isEdit;
        if (stateType) {
            toast({
                content: this.state.json['20020BURLS-000025'],
                color: 'warning'
            });
        }
    }
    getParm = (parm) => {
        let appUrl = decodeURIComponent(window.location.href).split('?'); //分割查询
        if (appUrl && appUrl[1]) {
            let appPrams = appUrl[1].split('&');
            if (appPrams && appPrams instanceof Array) {
                let parmObj = {};
                appPrams.forEach(item => {
                    let key = item.split('=')[0];
                    let value = item.split('=')[1];
                    parmObj[key] = value;
                })
                return parmObj[parm];
            }
        }
    }
    render() {
        let {
            DataSource,
            pk_accountingbook,
            defaultValueS,
            addState,
            isEdit,
            ruleNamedis,
            pk_unitState,
            pk_unit,
            bizDate
        } = this.state;
        return (
            <div className="searchButton">
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}>
                <Row>
                    <Col lg={3} sm={3} xs={3}  onClick={this.changeState.bind(this)}>
                        <AccountBookTreeRef
                            fieldid="pk_accountingbook"
                            value={ {'refname': pk_accountingbook.display, refpk: pk_accountingbook.value} }
                            placeholder={this.state.json['20020BURLS-000026']}
                            disabledDataShow={true}
                            isMultiSelectedEnabled={false}
                            queryCondition={{
                                "pk_accountingbook":pk_accountingbook.value,
                                "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                "appcode": this.props.getSearchParam('c')
                            }}  
                            disabled={this.state.addState=="revise"}
                            onChange={(v)=>{
                                // 根据选定的pk 实现过滤
                                // if(this.state.addState=="revise"){
                                //     toast({ content: '有未保存内容，请先保存或取消', color: 'warning' });
                                //     return;
                                // }
                                pk_accountingbook.value=v.refpk
                                pk_accountingbook.display=v.refname
                                this.props.getPk(pk_accountingbook);
                                this.setState({
                                    pk_accountingbook:pk_accountingbook
                                })
                                let self=this;
                                ajax({
                                    url:'/nccloud/gl/glpub/queryFinanceOrg.do',
                                    data:{"pk_accountingbook":v.refpk,'needaccount':false},
                                    success: function(response) { 
                                        const { data, error, success } = response;
                                        if (success&&data) {
                                            self.props.getPk_org(data.pk_org)
                                        } else {
                                            
                                        }    
                                    }
                                });
                                let url1 = "/nccloud/gl/cashflow/isbustartflag.do";
                                ajax({
                                    url:'/nccloud/gl/voucher/queryBookCombineInfo.do',
                                    data:{"pk_accountingbook":pk_accountingbook.value,'needaccount':false},
                                    success: function(response) { 
                                        const { data, error, success } = response;
                                        let {bizDate}=self.state;
                                        if (success&&data) {
                                            bizDate=data.bizDate;
                                            let pk_unit={
                                                refpk:data.unit.value,
                                                refname:data.unit.display
                                            };
                                            self.setState({
                                                bizDate,
                                                pk_unit
                                            })
                                            ajax({
                                                url:url1,
                                                data:{"pk_accountingbook":pk_accountingbook.value},
                                                success: function(response) { 
                                                    const { data, error, success } = response;
                                                    let pk_unitState=null;
                                                    if (success&&data) {
                                                        if(data=='Y'){
                                                            pk_unitState=true;
                                                            self.setState({
                                                                pk_unitState:true
                                                            });
                                                            self.props.getPk(pk_accountingbook,pk_unit,pk_unitState);
                                                        }else{
                                                            pk_unitState=false;
                                                            self.setState({
                                                                pk_unitState:false
                                                            });
                                                            self.props.getPk(pk_accountingbook,'',pk_unitState);
                                                        }
                                                    } else {
                                                        
                                                    }    
                                                }
                                            });
                                        } else {
                                            
                                        }    
                                    }
                                })
                            }}                          
                        /> 
                    </Col>
                    <Col lg={3} sm={3} xs={3} className={!pk_unitState?"display-none":""} style={{marginLeft:'12px'}}>
                        <BusinessUnitWithGlobleAndCurrGroupTreeRef
                            fieldid="pk_unit"
                            value={pk_unit}
                            // {value:pk_unit.refpk,display:pk_unit.display}
                            disabled={this.state.addState=="revise"}
                            queryCondition={{
                                isDataPowerEnable: 'Y',
                                DataPowerOperationCode: 'fi',
                                "pk_accountingbook":pk_accountingbook.value,
                                VersionStartDate:bizDate,
                                pk_org:this.props.pk_org,
                                "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder',
                            }} 
                            onChange={(v)=>{
                                pk_unit=v;
                                this.props.getPk(pk_accountingbook,v,true,true);
                                this.setState({
                                    pk_unit
                                })
                            }}
                        />
                    </Col>
                    <Col lg={3} sm={3} xs={3} onClick={this.changeState.bind(this)} className={this.state.addState=="revise"?"display-none":""} style={{marginLeft:'12px'}}>
                        
                        <Select
                            fieldid="selectValue"
                            showClear={false}
                            disabled={this.state.addState=="revise"||!pk_accountingbook.value}
                            value={this.state.selectValue?this.state.selectValue:defaultValueS}
                            // defaultValue={defaultValueS}
                            placeholder={this.state.json['20020BURLS-000025']}
                            onChange={(v)=>{
                                this.setState({
                                    selectValue:v
                                })
                                this.props.getBusrulename(v);
                            }}
                            
                            data={DataSource}
                        >
                        </Select>
                    </Col>
                    <Col lg={3} sm={3} xs={3} className={this.state.addState=="revise"?"":"display-none"} style={{marginLeft:'12px'}}>
                        <span  className='cashflowxx'>*</span>
                        <FormControl
                        fieldid="ruleNamedis"
                        type="text" 
                        className="u-form-control" // nc-theme-from-input-bgc nc-theme-form-input-c
                        value={ruleNamedis}
                        placeholder={this.state.json['20020BURLS-000024']}
                        onBlur={(value)=>{
                            this.props.getNewName(value)
                        }}
                        onChange={(value)=>{
                            this.setState({
                                ruleNamedis:value
                            })
                        }}
                        />
                        {/* <input 
                            fieldid="ruleNamedis"
                            type="text" 
                            className="u-form-control nc-theme-from-input-bgc nc-theme-form-input-c" 
                            value={ruleNamedis}
                            placeholder={this.state.json['20020BURLS-000024']}
                            onBlur={(event)=>{
                                this.props.getNewName(event.target.value)
                            }}
                            onChange={(event)=>{
                                this.setState({
                                    ruleNamedis:event.target.value
                                })
                            }}
                        /> */}
                    </Col>
                </Row>
                </NCDiv>
            </div>
        )
    }
}
CashFlowSearch = createPage({})(CashFlowSearch);
export default CashFlowSearch;