import React, {
	Component
} from "react";
import {
	InputItem,
} from '../../../public/components/FormItems';
import AddDelate from './AddDelate';
import {
	high,
	base, getBusinessInfo, getMultiLang
} from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	AutoComplete,
	NCDropdown: Dropdown,
	NCButtonGroup: ButtonGroup,
	NCModal: Modal,
	NCPopconfirm: Popconfirm, NCDiv
} = base;
import {
	toast
} from '../../../public/components/utils.js';
const {
	Refer
} = high;
import ReferLoader from '../../../public/ReferLoader/index.js'
import ReferLoader2 from './refer.js';
export default class CashFlowSubject extends Component {
	constructor(props) {
		super(props)
		this.state = {
			json: {},
			checkedAll: false,//是否全选
			selectData: [],//选中数据
			selIds: [],//选中行号
			checkedArray: [//各行选中判断

			],
			selectedRowIndex: '',
			busdetail: [],
			saveData: {},
			columns: []
		}
		this.businessInfo = '';
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({
				json: json,
				columns: [{
					title: (<div fieldid="pk_debitaccasoa" className="mergecells">{json['20020BURLS-000027']}</div>),
					dataIndex: "pk_debitaccasoa",
					key: "pk_debitaccasoa",
					width: '600px',
					render: (text, record, index) => (
						// <AccountDefaultModelTreeRef
						this.props.addState == 'check' ?
							<div>{this.state.busdetail[index].pk_debitaccasoa.display?this.state.busdetail[index].pk_debitaccasoa.display:<span>&nbsp;</span>}</div> 
							:
							<div fieldid="pk_debitaccasoa">
								<ReferLoader
									fieldid="pk_debitaccasoa"
									tag={'AccountDefaultGridTreeRef' + index}
									refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
									value={{
										refname: this.state.busdetail[index].pk_debitaccasoa.display,
										refpk: this.state.busdetail[index].pk_debitaccasoa.value
									}}
									// disabled={!record.isEdit}
									queryCondition={{
										"pk_accountingbook": this.state.saveData.pk_accountingbook.value,
										'dateStr': this.businessInfo, isDataPowerEnable: 'Y', DataPowerOperationCode: 'fi'
									}}
									onlyLeafCanSelect={true}
									onChange={(v) => {
										let { busdetail, saveData } = this.state;
										busdetail[index].pk_debitaccasoa.display = v.dispname;
										busdetail[index].pk_debitaccasoa.value = v.refpk;
										if (!(v.cashtype)||v.cashtype > 0) {
											this.props.getIsCashFlow(true)
										} else {
											this.props.getIsCashFlow(false)
										}
										busdetail[index].cashtype = v.cashtype;
										// if(!v.refname&&!busdetail[index].pk_creditaccasoa.value){
										//   toast({ content: '科目不能为空，请选择科目或删除', color: 'warning' });
										// }
										if (v.refname && busdetail[index].pk_creditaccasoa.value) {
											busdetail[index].pk_creditaccasoa.value = '';
											busdetail[index].pk_creditaccasoa.display = '';
										}
										this.props.getSaveDataSub(busdetail)
										this.setState({
											busdetail,
											saveData
										})
									}}
								/>
							</div>
					)
				}, {
					title: (<div fieldid="pk_creditaccasoa" className="mergecells">{json['20020BURLS-000028']}</div>),
					dataIndex: "pk_creditaccasoa",
					key: "pk_creditaccasoa",
					width: '600px',
					render: (text, record, index) => (
						this.props.addState == 'check' ?
							<div>{this.state.busdetail[index].pk_creditaccasoa.display?this.state.busdetail[index].pk_creditaccasoa.display:<span>&nbsp;</span>}</div> 
							:
							<div fieldid="pk_creditaccasoa">
								<ReferLoader2
									fieldid="pk_creditaccasoa"
									tag={'AccountDefaultGridTreeRefCredit' + index}
									isMultiSelectedEnabled={false}
									refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
									value={{
										refname: this.state.busdetail[index].pk_creditaccasoa.display,
										refpk: this.state.busdetail[index].pk_creditaccasoa.value,
									}}
									// disabled={!record.isEdit}
									onlyLeafCanSelect={true}
									queryCondition={{
										"pk_accountingbook": this.state.saveData.pk_accountingbook.value,
										isDataPowerEnable: 'Y',
										DataPowerOperationCode: 'fi',
										'dateStr': this.businessInfo
									}}
									onChange={(v) => {
										let { busdetail, saveData } = this.state;
										busdetail[index].pk_creditaccasoa.display = v.dispname;
										busdetail[index].pk_creditaccasoa.value = v.refpk;
										if (!(v.cashtype)||v.cashtype > 0) {
											this.props.getIsCashFlow(true)
										} else {
											this.props.getIsCashFlow(false)
										}
										busdetail[index].cashtype = v.cashtype;
										// if(!v.refname&&!busdetail[index].pk_debitaccasoa.value){
										//   toast({ content: '科目不能为空，请选择科目或删除', color: 'warning' });
										// }
										if (v.refname && busdetail[index].pk_creditaccasoa.value) {
											busdetail[index].pk_debitaccasoa.value = '';
											busdetail[index].pk_debitaccasoa.display = '';
										}
										this.props.getSaveDataSub(busdetail)
										this.setState({
											busdetail,
											saveData
										})
									}}
								/>
							</div>
					)
				}
				]
			}, () => { })
		}
		getMultiLang({ moduleId: '20020BURLS', domainName: 'gl', currentLocale: 'simpchn', callback });
	}
	componentDidMount() {
		if (getBusinessInfo()) {
			if (getBusinessInfo().businessDate) {
				this.businessInfo = getBusinessInfo().businessDate.split(' ')[0]
			}
		}
		this.setState({
			selectData: [],//选中数据
			selIds: [],//选中行号
			checkedArray: [//各行选中判断

			]
		})
	}
	componentWillReceiveProps(nextProps) {
		let busdetail = null;
		busdetail = nextProps.localSubject.busdetail;
		let saveData = nextProps.localSubject;
		if (busdetail) {
			for (let i = 0, len = busdetail.length; i < len; i++) {
				busdetail[i].key = i + 1;
			}
		}
		this.setState({
			busdetail: busdetail,
			saveData: saveData
		})
	}
	clickRow = (data, rowClassName) => {
		let {busdetail,saveData} = this.state;
		for (let i = 0, len = busdetail.length; i < len; i++) {
			busdetail[i].isSelected = false;
		}
		busdetail[rowClassName].isSelected = true;
		this.setState({
			selectData: data,
			busdetail
		})
		if (!(data.cashtype)||data.cashtype > 0) {
			this.props.getIsCashFlow(true)
		} else {
			this.props.getIsCashFlow(false)
		}
		this.props.getBusrelation(data);
		this.setState({
			selectedRowIndex: rowClassName
		})
	}
	getBack = (v, isDel) => {
		this.setState({
			busdetail: v
		})
		let saveData = this.state.saveData;
		saveData.busdetail = v;
		this.props.getSaveDataSub(v);
		if (isDel) {
			this.setState({
				checkedArray: [],
				selectData: [],
				checkedAll: false,
				selIds: []
			})
		}
	}
	//获取当前日期
	getNowFormatDate = () => {
		let date = new Date();
		let seperator1 = "-";
		let seperator2 = ":";
		let month = date.getMonth() + 1;
		let strDate = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		return currentdate;
	}
	//处理多选
	onAllCheckChange = () => {//全选
		let self = this;
		let checkedArray = [];
		let busdetail = this.state.busdetail;
		let listData = self.state.busdetail.concat();
		let selIds = [];
		// let id = self.props.multiSelect.param;
		for (var i = 0; i < busdetail.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
			selIds.push(i);
		}
		if (self.state.checkedAll) {
			selIds = []
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			selIds: selIds,
			checkedArray
		});
		// self.props.setCheckedArray(checkedArray)

		let selectArr = [];
		for (let i = 0; i < selIds.length; i++) {
			selectArr.push(JSON.parse(JSON.stringify(busdetail[selIds[i]])));
		}
		this.setState({
			selectData: selectArr
		})
	};
	onCheckboxChange = (text, record, index) => {//单选
		let self = this;
		let allFlag = false;
		let selIds = self.state.selIds;
		// let id = self.props.postId;
		let checkedArray = self.state.checkedArray.concat();
		if (self.state.checkedArray[index]) {
			selIds.splice(record.key, 1);
			for (let i = 0, len = selIds.length; i < len; i++) {
				if (record.key == selIds[i]) {
					selIds.splice(i, 1)
					break;
				}
			}
		} else {
			selIds.push(record.key);
		}
		checkedArray[index] = !self.state.checkedArray[index];
		for (let i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		self.setState({
			checkedAll: allFlag,
			selIds: selIds,
			checkedArray
		});
		// self.props.setCheckedArray(checkedArray)
		let busdetail = this.state.busdetail;
		let selectArr = [];
		selIds = this.sortarr(selIds)
		for (let i = 0; i < selIds.length; i++) {
			for (let j = 0; j < busdetail.length; j++) {
				if (selIds[i] == busdetail[j].key) {
					selectArr.push(JSON.parse(JSON.stringify(busdetail[j])));
				}
			}
		}
		this.setState({
			selectData: selectArr
		})
	};
	renderColumnsMultiSelect(columns) {
		const { data, checkedArray, busdetail } = this.state;
		let { multiSelect } = this.props;
		let select_column = {};
		let indeterminate_bool = false;
		// multiSelect= "checkbox";
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === "checkbox") {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
						<Checkbox
							className="table-checkbox"
							width="60px"
							checked={this.state.checkedAll}
							indeterminate={indeterminate_bool && !this.state.checkedAll}
							onChange={this.onAllCheckChange}
						/>
					}</div>),
					key: "checkbox",
					attrcode: "checkbox",
					dataIndex: "checkbox",
					width: "60px",
					render: (text, record, index) => {
						return (
							<div fieldid="firstcol">
								<Checkbox
									className="table-checkbox"
									checked={this.state.checkedArray[index]}
									onChange={this.onCheckboxChange.bind(this, text, record, index)}
								/>
							</div>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}
	sortarr = (arr) => {//排序
		for (let i = 0; i < arr.length - 1; i++) {
			for (let j = 0; j < arr.length - 1 - i; j++) {
				if (arr[j] > arr[j + 1]) {
					var temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}
		return arr;
	}
	render() {
		let {
			busdetail,
			selectData
		} = this.state;
		let columns = this.renderColumnsMultiSelect(this.state.columns);
		return (
			<div className="CashFlowSubjectTable">
				<AddDelate
					subjectData={busdetail}
					getBack={this.getBack.bind(this)}
					selectDataSub={selectData}
					addState={this.props.addState}
				/>
				<ReferLoader
					tag={'AccountDefaultGridTreeRef'}
					style={{ 'display': 'none' }}
					refcode='uapbd/refer/fiacc/AccountDefaultGridTreeRef/index.js'
				/>
				<NCDiv fieldid="cashFlowsubject" areaCode={NCDiv.config.TableCom}>
					<Table
						columns={columns}
						data={busdetail}
						onRowClick={(record, index, event) => {
							// this.changeButtonState()
							this.clickRow(record, index)
						}}
						rowClassName={(record, index, indent) => {
							if (this.state.selectedRowIndex == index) {
								return 'nctable-selected-row';
							} else {
								return '';
							}
						}}
						bodyStyle={{height:'230px'}}
						scroll={{ x: false, y: 230 }}
						// bordered
					/>
				</NCDiv>
			</div>

		);
	}
}
CashFlowSubject.defaultProps = {
	prefixCls: "bee-table",
	multiSelect: {
		type: "checkbox",
		param: "key"
	}
};