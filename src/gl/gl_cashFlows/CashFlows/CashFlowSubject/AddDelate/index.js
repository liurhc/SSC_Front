import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,getMultiLang } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCButtonGroup:ButtonGroup,NCModal:Modal,NCPopconfirm:Popconfirm
    } = base;
import { toast } from '../../../../public/components/utils.js';
import './index.less';
export default class AddDelate extends Component{
    constructor(props){
        super(props);
        this.state = {
            json:{},
            subjectData:[],
            selectData:[]
        }
    }
    componentWillMount(){
        let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
            getMultiLang({moduleId:'20020BURLS',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let selectData = nextProps.selectDataSub;
        this.setState({
                subjectData:nextProps.subjectData,
                selectData
        })
    }
    handleAdd=()=>{
        let subjectData = this.state.subjectData;
        let subjectDataArr = [];
        if(subjectData.length==0){//子表数据为空时
            let obj={
                pk_busdetail:JSON.stringify(subjectData.length+1),
                pk_creditaccasoa:{display: "", value: "", scale: 0},
                // pk_busrule:JSON.stringify(subjectData.length),
                pk_debitaccasoa:{display: null, value: null, scale: null},
                busrelation:[],
                isEdit:true,
                key:1
            } 
            subjectData.push(JSON.parse(JSON.stringify(obj)));
        }else{
            let obj=JSON.parse(JSON.stringify(subjectData[0]));
            obj.isEdit = true;
            obj.key = JSON.stringify(subjectData.length+1);
            obj.pk_busdetail = JSON.stringify(subjectData.length+1);
            obj.pk_creditaccasoa={display: "", value: "", scale: 0};
            // obj.pk_busrule=JSON.stringify(subjectData.length);
            obj.pk_debitaccasoa={display: null, value: null, scale: null};
            obj.busrelation=[];
            subjectData.push(JSON.parse(JSON.stringify(obj)));
        }
        this.props.getBack(subjectData);
    }
    handleDelate=()=>{
        let subjectData = this.state.subjectData;
        if(typeof(subjectData)=='string'){//孙表数据为空时
            toast({ content: this.state.json['20020BURLS-000016'], color: 'warning' });
        }else{
            let selectData = this.state.selectData;
            for(let i=0,len=subjectData.length;i<len;i++){
                if(selectData.length){
                    for(let j=0,lenj=selectData.length;j<lenj;j++){
                        if(subjectData[i].key==selectData[j].key){
                            subjectData.splice(i,1);
                            len--;
                        }
                    }
                }else{
                    toast({ content: this.state.json['20020BURLS-000017'], color: 'warning' });
                }
            }
            this.props.getBack(subjectData,true);
        }
    }
    render(){
        return (
            <div className="positon-t0 nc-theme-area-bgc nc-theme-area-split-bc" >
                    <div className="tittle"  fieldid="header_area">
                        <h3 className="nc-theme-common-font-c" fieldid={`${this.state.json['20020BURLS-000019']}_title`}>{this.state.json['20020BURLS-000019']}</h3>
                    </div>
                    <div className="fr" style={{'display':(this.props.addState=='revise'?'block':'none')}}>
                        <ButtonGroup style={{'margin':'0'}}>
                            <Button onClick={this.handleAdd} style={{'margin-right':'2px','min-width':'60px','font-size':'13px'}} fieldid="add">{this.state.json['20020BURLS-000020']}</Button>
                            <Button onClick={this.handleDelate} style={{'min-width':'60px','font-size':'13px'}} fieldid="delete"
                                disabled={this.state.selectData.length==0}
                            >{this.state.json['20020BURLS-000021']}</Button>
                        </ButtonGroup>
                    </div>
            </div>
        )
    }
}