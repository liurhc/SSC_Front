import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ajax, base, promptBox, getMultiLang, createPageIcon,toast,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage} = viewModel;
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCMessage: Message, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCButtonGroup: ButtonGroup, NCModal: Modal, NCPopconfirm: Popconfirm
} = base;
import CashFlowSearch from '../CashFlowSearch';
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
export default class CashFlowButtons extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            stateType: 'check',
            isLoading: ''
        }

    }
    componentWillMount() {
        let callback = (json) => {
                        this.setState({ json: json }, () => {
                //                 initTemplate.call(this, this.props);
                        })
                }
                getMultiLang({ moduleId: '20020BURLS', domainName: 'gl', currentLocale: 'simpchn', callback });
    }
    componentDidMount() {
        let pagecode = this.props.seProps.getSearchParam('p')
        let appcode = this.props.seProps.getSearchParam('c')
        let self = this;
        ajax({
            url: '/nccloud/platform/appregister/queryallbtns.do',
            data: {
                pagecode: pagecode,
                appcode: appcode
            },
            success: (res) => {
                /*  拿到按钮数据，调用setButtons方法将数据设置到页面上，然后执行后续的按钮逻辑 */
                self.props.seProps.button.setButtons(res.data);
                self.props.seProps.button.setButtonVisible({ 'save': false, 'cancel': false })
                if (!this.props.pk_accountingbook.value) {
                    this.props.seProps.button.setButtonDisabled({ 'add': true, 'edit': true, 'delete': true })
                }
            }
        });
    }
    componentWillReceiveProps(nextProps) {
        // 'add':true,'edit':true,'delete':true
        this.props.seProps.button.setButtonVisible({ 'save': false, 'cancel': false })
        this.props.seProps.button.setButtonDisabled({ 'add': false, 'edit': false, 'delete': false })
        if (!nextProps.defaultName) {
            this.props.seProps.button.setButtonDisabled({ 'edit': true, 'delete': true })
        }
        if (!nextProps.pk_accountingbook.value) {
            // this.props.seProps.button.setButtonVisible({'edit':false,'delete':false})
            this.props.seProps.button.setButtonDisabled({ 'add': true, 'edit': true, 'delete': true })
        }
        if (nextProps.addState == 'check') {
            this.props.seProps.button.setButtonVisible({ 'add': true, 'edit': true, 'delete': true, 'save': false, 'cancel': false })
        }
        if (nextProps.addState == 'revise') {
            this.props.seProps.button.setButtonVisible({ 'delete': false, 'save': true, 'cancel': true, 'add': false, 'edit': false })
        }
    }
    //保存
    handleSave = () => {
        let self = this;
        let localData = JSON.parse(JSON.stringify(self.props.saveData));
        let defaultName = this.props.defaultName;
        // return;
        let sendData;
        for (let i = 0, len = localData.length; i < len; i++) {
            if (localData[i].busdetail == 0 || !localData[i].busdetail) {
                toast({ content: self.state.json['20020BURLS-000000'], color: 'warning' });
                return;
            }
            if (localData[i].busrulename.value == defaultName) {
                sendData = localData[i];
                localData[i].pk_unit = { value: this.props.pk_unit.refpk, display: this.props.pk_unit.refname };
                let itemChild = localData[i].busdetail;
                for (let j = 0, lenj = itemChild.length; j < lenj; j++) {
                    if (!itemChild[j].pk_creditaccasoa.value && !itemChild[j].pk_debitaccasoa.value) {
                        toast({ content: self.state.json['20020BURLS-000001'], color: 'warning' });
                        return;
                    }
                }
            } else if (!defaultName) {
                sendData = localData[0];
                localData[0].pk_unit = { value: this.props.pk_unit.refpk, display: this.props.pk_unit.refname };
                let itemChild = localData[0].busdetail;
                for (let j = 0, lenj = itemChild.length; j < lenj; j++) {
                    if (!itemChild[j].pk_creditaccasoa.value && !itemChild[j].pk_debitaccasoa.value) {
                        toast({ content: self.state.json['20020BURLS-000001'], color: 'warning' });
                        return;
                    }
                }
                break;
            }
        }

        sendData.pk_accountingbook = this.props.pk_accountingbook;
        let url = "/nccloud/gl/cashflow/savebusrule.do";
        ajax({
            url,
            data: sendData,
            success: function (response) {
                const { data, error, success } = response;
                if (success) {
                    let pk_busrule = data.pk_busrule.value;
                    // toast({ content: '保存成功', color: 'success' });
                    for (let i = 0, len = localData.length; i < len; i++) {
                        if (localData[i].busrulename.value == defaultName) {
                            let itemChild = localData[i].busdetail;
                            localData[i].pk_busrule = { display: pk_busrule, value: pk_busrule, scale: 0 }
                            for (let j = 0, lenj = itemChild.length; j < lenj; j++) {
                                itemChild[j].isEdit = false;
                                if (itemChild[j].busrelation) {
                                    let itemGrand = itemChild[j].busrelation;
                                    if (itemGrand.length > 0) {
                                        for (let x = 0, lenx = itemGrand.length; x < lenx; x++) {
                                            itemGrand[x].isEdit = false;
                                        }
                                    }
                                }
                            }
                            toast({ content: self.state.json['20020BURLS-000002'], color: 'success' });
                            self.props.changeState('check');
                            self.props.handleSave(localData);
                        }
                        // return;
                    }
                    // self.props.seProps.button.setButtonVisible({'save':true,'cancel':true,'add':false,'edit':false}) 
                } else {
                    toast({ content: error, color: 'warning' });
                }
            }
        });
        this.props.isEditState(false);
    }
    //增加
    handleAdd = () => {
        this.props.changeState('revise');
        this.props.clearContent(true);
        this.props.newPageData(true)
        this.setState({
            stateType: 'revise'
        })
        this.props.seProps.button.setButtonVisible({ 'save': true, 'cancel': true, 'add': false, 'edit': false })
    }
    //修改
    handleRevise = () => {
        let self = this;
        let localData = JSON.parse(JSON.stringify(self.props.saveData));
        setGlobalStorage('localStorage',"localReData", JSON.stringify(self.props.saveData));
        this.props.isEditState("true");
        let defaultName = self.props.defaultName;
        for (let i = 0, len = localData.length; i < len; i++) {
            if (localData[i].busrulename.value == defaultName) {
                let itemChild = localData[i].busdetail;
                for (let j = 0, lenj = itemChild.length; j < lenj; j++) {
                    itemChild[j].isEdit = true;
                    if (itemChild[j].busrelation) {
                        let itemGrand = itemChild[j].busrelation;
                        if (itemGrand.length > 0) {
                            for (let x = 0, lenx = itemGrand.length; x < lenx; x++) {
                                itemGrand[x].isEdit = true;
                            }
                        }
                    }
                }
                self.props.handleRevise(localData);
                self.props.changeState('revise');
            }
            // return;
        }
    }
    //删除
    handleDelate = () => {
        let self = this;
        let localData = JSON.parse(JSON.stringify(self.props.saveData));
        let defaultName = self.props.defaultName;
        let data = { pk_busrule: '' };
        for (let i = 0, len = localData.length; i < len; i++) {
            if (localData[i].busrulename.value == defaultName) {
                data.pk_busrule = localData[i].pk_busrule.value;
            }
        }
        let url = "/nccloud/gl/cashflow/delbusrule.do";
        ajax({
            url,
            data,
            success: function (response) {
                const { data, error, success } = response;
                if (success) {
                    for (let i = 0, len = localData.length; i < len; i++) {
                        if (localData[i].busrulename.value == defaultName) {
                            localData.splice(i, 1);
                            self.props.getDidDelData(localData);
                            toast({ content: self.state.json['20020BURLS-000003'], color: 'success' });
                            return;
                        }
                    }

                } else {
                    toast({ content: error, color: 'warning' });
                }
            }
        });
    }
    //取消
    handleCancel = () => {
        this.props.handleCancel()
        this.props.isEditState(false);
        this.props.changeState('check');
        this.setState({
            stateType: 'check'
        })
        this.props.seProps.button.setButtonVisible({ 'save': false, 'cancel': false, 'add': true, 'edit': true })
    }
    onButtonClick = (props, id) => {
        switch (id) {
            case 'add':
                this.handleAdd();
                break;
            case 'save':
                this.handleSave();
                break;
            case 'edit':
                this.handleRevise();
                break;
            case 'cancel':
                promptBox({
                    color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                    title: this.state.json['20020BURLS-000004'],                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                    content: this.state.json['20020BURLS-000005'],             // 提示内容,非必输
                    beSureBtnName: this.state.json['20020BURLS-000006'],          // 确定按钮名称, 默认为"确定",非必输
                    cancelBtnName: this.state.json['20020BURLS-000007'],         // 取消按钮名称, 默认为"取消",非必输
                    hasCloseBtn: false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
                    beSureBtnClick: this.handleCancel,   // 确定按钮点击调用函数,非必输
                    closeByClickBackDrop: false,//点击遮罩关闭提示框，默认是false点击不关闭,点击关闭是true
                })
                break;
            case 'delete':
                promptBox({
                    color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                    title: this.state.json['20020BURLS-000004'],                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                    content: this.state.json['20020BURLS-000008'],             // 提示内容,非必输
                    beSureBtnName: this.state.json['20020BURLS-000006'],          // 确定按钮名称, 默认为"确定",非必输
                    cancelBtnName: this.state.json['20020BURLS-000007'],         // 取消按钮名称, 默认为"取消",非必输
                    hasCloseBtn: false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
                    beSureBtnClick: this.handleDelate,   // 确定按钮点击调用函数,非必输
                    closeByClickBackDrop: false,//点击遮罩关闭提示框，默认是false点击不关闭,点击关闭是true
                })
                break;
            default:
                break;
        }
    }
    getNewName = (data) => {
        this.props.getNewName(data);
    }
    render() {
        let { createButtonApp } = this.props.seProps.button;
        return (
            <div>
                <HeaderArea 
                    title = {this.state.json['20020BURLS-000011']}
                    searchContent = {
                        <div className='search-mid'>
                            <CashFlowSearch
                                getPk={this.props.getPk}
                                dataSource={this.props.dataSource}
                                getBusrulename={this.props.getBusrulename}
                                addState={this.props.addState}
                                getNewName={this.props.getNewName.bind(this)}
                                defaultName={this.props.defaultName}
                                isEditState={this.props.isEditStateSearch}
                                getPk_org={this.props.getPk_org}
                                pk_org={this.props.pk_org}
                                selectValue={this.props.selectValue}
                            />
                        </div>
                    }
                    btnContent = {
                        createButtonApp({
                            area: 'head',
                            onButtonClick: this.onButtonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })
                        
                    }
                />
                <Loading fullScreen showBackDrop={true} show={this.state.isLoading} />
            </div>
        )
    }
}