import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import CashFlowSubject from '../CashFlowSubject';
import CashFlowItem from '../CashFlowItem';
import CashFlowButtons from '../CashFlowButtons';
import CashFlowSearch from '../CashFlowSearch';
import {high,base,ajax,createPage,getMultiLang,toast } from 'nc-lightapp-front';
const { NCLoading:Loading,
    } = base;
import '../index.less';

class CashFlows extends Component{
    constructor(props){
        super(props),
        this.state={
            json:{},
            busrulenames:[],//业务规则名称
            saveData:[],//整体数据
            localSubject:{},//子表数据
            localItem:[],//孙表数据
            addState:'check',//增加状态
            defaultName:'',//展示业务规则名称
            isEditState:false,//判断编辑状态
            pk_accountingbook:'',//财务账簿pk
            isLoading:'',
            isCashflow:false,//控制子表编辑
            pk_org:'',//组织，参照过滤用
            pk_unit:{value:'',display:''},//业务单元
            pk_unitState:'',//业务单元显示
            selectValue:''//选中业务规则
        }
    }
    componentWillMount(){
        let callback= (json) =>{
               this.setState({json:json},()=>{ })
                }
        getMultiLang({moduleId:'20020BURLS',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentDidMount(){
        window.isCloseHint = true;
        //初始化关闭
        let self = this;
        // window.addEventListener("click", function(e) {
        // })
        window.addEventListener("beforeunload", function(e) {
            if (self.state.addState=="revise") {
                var confirmationMessage = self.state.json['20020BURLS-000029'];
                (e || window.event).returnValue = confirmationMessage; // 兼容 Gecko + IE
                return confirmationMessage; // 兼容 Gecko + Webkit, Safari, Chrome
            }
        });  
    }
    //获取选中pk，请求整体数据
    getPk=(v,pk_unit,pk_unitState,UnitIsChange)=>{
        if(this.state.addState!='revise'&&this.state.isEditState!="true"){
            this.clearContent();
        }
            let self = this;
            let data;
            this.setState({
                localSubject:[],
                localItem:[],
                pk_accountingbook:v,
                pk_unitState:pk_unitState
            })
            if(pk_unit&&pk_unitState){
                data = {pk_accountingbook: v.value,pk_unit:pk_unit.refpk};
                this.setState({
                    pk_unit:pk_unit
                })
            }else if(!pk_unitState){
                data = {pk_accountingbook: v.value}; 
            }else if(pk_unitState&&!pk_unit){
                return
            }
        
            let url = "/nccloud/gl/cashflow/querybusrule.do";
            ajax({
                url,
                data,
                success: function(response) {
                    const { data, error, success } = response;
                    if (success&&data) {
                        if(data.length){
                            let obj = {};
                            self.state.busrulenames = [];
                            let arr = [];
                            for(let i=0,len=data.length;i<len;i++){
                                obj.key = data[i].busrulename.display;
                                obj.value = data[i].busrulename.value;
                                arr.push(JSON.parse(JSON.stringify(obj)));
                            }
                            let defaultName = self.state.defaultName;
                            if(!defaultName){
                                let localSubject = data[0];
                                let localItem =  localSubject.busdetail[0];
                                let defaultName = data[0].busrulename.value;
                                let isCashflow=false;
                                if(localItem.cashtype>0){
                                    isCashflow=true
                                }
                                self.setState({
                                    saveData:data,
                                    busrulenames:arr,
                                    localSubject:localSubject,
                                    localItem:localItem,
                                    defaultName,
                                    isCashflow
                                });
                            }else{
                                for(let i=0,len=data.length;i<len;i++){
                                    let isCashflow;
                                    if(data[i].busrulename.value==defaultName){
                                        let localSubject = data[i];
                                        let localItem =  localSubject.busdetail[i];
                                        let defaultName = data[i].busrulename.value;                                  
                                        if(localItem.cashtype>0){
                                            isCashflow=true
                                        }
                                        self.setState({
                                            saveData:data,
                                            localSubject,
                                            localItem,
                                            defaultName,
                                            busrulenames:arr,
                                            isCashflow
                                        })
                                    }
                                }
                            }
                        }else{
                            self.setState({
                                saveData:[],
                                localSubject:{},
                                localItem:{},
                                defaultName:'',
                                busrulenames:[],
                                isCashflow:false
                            });
                        }
                        
                    } else {
                        // toast({ content: '无请求数据', color: 'warning' });
                        self.setState({
                            saveData:[],
                            localSubject:{},
                            localItem:{},
                            defaultName:'',
                            busrulenames:[],
                            isCashflow:false
                        });
                    }
                }
            });
        // }
    }
    //获取是否编辑子表状态
    getIsCashFlow=(data)=>{
        this.setState({
            isCashflow:data
        })
    }
    //获取当前busrulename，切换子表
    getBusrulename = (v) => {
        let data = this.state.saveData;
        let defaultName = v;
        for(let i=0,len=data.length;i<len;i++){
            if(v==data[i].busrulename.value){
                let localSubject = data[i]
                let localItem =  localSubject.busdetail[0];
                this.setState({
                    selectValue:v,
                    localSubject,
                    localItem,
                    defaultName
                })
            } 
        }
    }
    //获取孙表数据
    getBusrelation=(data)=>{
        this.setState({
            localItem:data
        })
    }
    //获取保存数据
    getSaveDataItem=(data)=>{
        let saveData = this.state.saveData;
        let defaultName = this.state.defaultName;
        let dataArr=[];
        if(data.data){
            let datalen=data.data;
            for(let k=0;k<datalen.length;k++){
                if(datalen[k].pk_cashflow&&datalen[k].pk_cashflow.value){
                    dataArr.push(datalen[k]);
                }
            }
        }
        let busrelationData=[];
        if(data.busrelation){
            let busrelationlen=data.busrelation;
            for(let k=0;k<busrelationlen.length;k++){
                if(busrelationlen[k].pk_cashflow&&busrelationlen[k].pk_cashflow.value){
                    busrelationData.push(busrelationlen[k]);
                }
            }
        }
        for(let i=0,len=saveData.length;i<len;i++){
            if(defaultName==saveData[i].busrulename.value){
                let item = saveData[i].busdetail;
                if(item){
                    for(let j=0,lenj=item.length;j<lenj;j++){
                        if(item[j].pk_busdetail==data.pk_busdetail){
                            if(data.data){
                                item[j].busrelation=dataArr ;
                            }else if(data.busrelation){
                               item[j].busrelation=busrelationData ;
                            }else{
                                item[j]=data ;
                            }
                            
                        }
                    }
                }
            }
        }
        
        this.state.saveData = saveData;
    }
    //修改子表后保存
    getSaveDataSub=(data)=>{
        let saveData = this.state.saveData;
        let defaultName = this.state.defaultName;
        for(let i=0,len=saveData.length;i<len;i++){
            if(defaultName == saveData[i].busrulename.value){
                saveData[i].busdetail=JSON.parse(JSON.stringify(data))
            }
        }
        if(!defaultName){
            saveData[0].busdetail=JSON.parse(JSON.stringify(data))
        }
        // this.state.saveData = saveData;
        this.setState({
            saveData
        })
    }
    //切换新增状态
    changeState=(data)=>{
        this.setState({
            addState:data,localItem:[]
        })
    }
    //新增下拉项
    getNewName=(data)=>{
        let {saveData,pk_accountingbook,busrulenames,defaultName} = this.state;
        let obj;
        if(saveData[0]&&!saveData[0].busrulename.value){
            saveData[0].busrulename={"display":data,"value":data,"scale":0}
            let nameObj={
                key : data,
                value : data
            };
            // busrulenames.unshift(JSON.parse(JSON.stringify(nameObj)));
            busrulenames[0]=nameObj;
            defaultName = data;
        }else if(saveData.length){
            let len = saveData.length;
            for(let i=0;i<len;i++){
                if(defaultName===saveData[i].busrulename.display){
                    saveData[i].busrulename={"display":data,"value":data,"scale":0}
                    let nameObj={
                        key : data,
                        value : data
                    };
                    busrulenames[i]=nameObj;
                }
            }
            defaultName = data;
        }
        this.setState({
            busrulenames,
            saveData,
            defaultName
        })
    }
    //删除后重新渲染数据
    getDidDelData=(data)=>{
        let self = this;
        let saveData = data;
        let obj = {};
        self.state.busrulenames = [];
        let arr = [];
        for(let i=0,len=data.length;i<len;i++){
            obj.key = data[i].busrulename.display;
            obj.value = data[i].busrulename.value;
            arr.push(JSON.parse(JSON.stringify(obj)));
        }
        let localSubject = data[0];
        let localItem;
        let defaultName; 
        if(localSubject){
            localItem =  localSubject.busdetail[0];
            defaultName = data[0].busrulename.value; 
        }else{
            localSubject=[]
            localItem = [];
            defaultName = '';
        }
        self.setState({
            selectValue:'',
            saveData,
            busrulenames:arr,
            localSubject:localSubject,
            localItem:localItem,
            defaultName
        });         
    }
    //修改
    handleRevise=(data)=>{
        let defaultName = this.state.defaultName;
        for(let i=0,len=data.length;i<len;i++){
            if(defaultName==data[i].busrulename.value){
                let localSubject = data[i]
                let localItem =  localSubject.busdetail[0];
                this.setState({
                    localSubject,
                    localItem
                })
                
            } 
        }
    }
    //取消
    handleCancel=()=>{
        this.getPk(this.state.pk_accountingbook)
    }
    //清空
    clearContent=(data)=>{
        this.setState({
            localSubject:[],
            localItem:[],
            selectValue:'',
            defaultName:''
        })
    }
    newPageData=()=>{//新增页面数据
        let {saveData,pk_accountingbook} = this.state;
        let obj;
        obj={
            "busdetail":[],
            "busrulename":{"display":'',"value":'',"scale":0},
            "pk_unit":{"display":"","value":"","scale":0},
            "pk_accountingbook":{"display":pk_accountingbook.display,"value":pk_accountingbook.value,"scale":0},
            // "pk_busrule":{"display":data,"value":data,"scale":0}
        }
        // saveData.push(obj);
        saveData.unshift(obj);
        let busrulenames = [];
        let nameObj={};
        // let defaultName = data;
        for(let i=0,len=saveData.length;i<len;i++){
            nameObj.key = saveData[i].busrulename.display;
            nameObj.value = saveData[i].busrulename.value;
            busrulenames.push(JSON.parse(JSON.stringify(nameObj)));
        }
        let localSubject = saveData[0]
        let localItem =  localSubject.busdetail[0];
        // if(!localItem){
        //     localItem={
        //         cashtype: "",
        //         key: '',
        //         pk_busdetail: "",
        //         pk_busrule: "",
        //         pk_creditaccasoa: {display: "", value: "", scale: "0"},
        //         pk_debitaccasoa: {display: null, value: null, scale: null}
        //     }
        // }
        this.setState({
            busrulenames,
            localSubject,
            localItem,
            saveData,
            // defaultName
        })
    }
    //修改时禁止切换
    isEditState=(data)=>{
        this.setState({
            isEditState:data
        })
    }
    //保存时取消编辑
    handleSave=(data)=>{
        let defaultName = this.state.defaultName;
        let addState = this.state.addState;
        if(addState=="revise"){
            addState = 'check';
            this.setState({
                addState
            })
        }
        let {localSubject}=this.state;
        data.map((item,index)=>{
            if(item.busrulename.value==defaultName){
                localSubject=item;
            }
        })
        this.setState({
            saveData:data,
            localSubject
        })
    }
    getPk_org=(data)=>{
        this.setState({
            pk_org:data
        })
    }

    render(){
        let { isEditState,defaultName,addState,busrulenames,saveData,localSubject,
            localItem,isCashflow,pk_org,pk_accountingbook,pk_unit,selectValue } = this.state;
        return (
            <div className='cashflows-content-m nc-bill-list'>
                <CashFlowButtons 
                    saveData={saveData}
                    changeState={this.changeState.bind(this)}
                    defaultName={defaultName}
                    pk_accountingbook={pk_accountingbook}
                    pk_unit={pk_unit}
                    pk_org={pk_org}
                    getDidDelData={this.getDidDelData.bind(this)}
                    handleCancel={this.handleCancel.bind(this)}
                    handleRevise={this.handleRevise.bind(this)}
                    isEditState={this.isEditState.bind(this)}
                    handleSave={this.handleSave.bind(this)}
                    clearContent={this.clearContent.bind(this)}
                    seProps={this.props}
                    newPageData={this.newPageData.bind(this)}
                    selectValue={selectValue}
                    //传递查询
                            getPk={this.getPk.bind(this)} 
                            dataSource={busrulenames} 
                            getBusrulename={this.getBusrulename.bind(this)}
                            addState={addState}
                            getNewName = {this.getNewName.bind(this)}
                            // defaultName={defaultName}
                            isEditStateSearch={isEditState}
                            getPk_org={this.getPk_org.bind(this)}
                    ></CashFlowButtons>
                <div className='cashflow-tables'>
                    <CashFlowSubject 
                        localSubject={localSubject} 
                        getIsCashFlow={this.getIsCashFlow.bind(this)}
                        getBusrelation={this.getBusrelation.bind(this)}
                        getSaveDataSub={this.getSaveDataSub.bind(this)}
                        saveData={saveData}
                        addState={addState}
                    ></CashFlowSubject>
                    <CashFlowItem 
                        localItem={localItem} 
                        getSaveDataItem={this.getSaveDataItem.bind(this)}
                        isCashflow={isCashflow}
                        addState={addState}
                        pk_org={pk_org}
                        pk_accountingbook={pk_accountingbook.value}
                    ></CashFlowItem>
                </div>
                <Loading fullScreen showBackDrop={true} show={this.state.isLoading} />
            </div>
        )
    }
}
CashFlows = createPage({
	// initTemplate: initTemplate
})(CashFlows);

// export default CashFlows
ReactDOM.render(
    <CashFlows/>,
    document.querySelector("#app")
);