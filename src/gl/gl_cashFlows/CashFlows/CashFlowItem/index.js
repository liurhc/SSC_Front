import React, {Component} from "react";
import {InputItem} from '../../../public/components/FormItems';
import AddDelate from './AddDelate';
import {
  high,toast,
  base,getMultiLang
} from 'nc-lightapp-front';
const {
  NCTable: Table,
  NCCheckbox: Checkbox,
  NCNumber,NCDiv
} = base;
const {
  Refer
} = high;
import CashflowTreeRef from '../../../../uapbd/refer/fiacc/CashflowTreeRef';
import {getTableHeight } from '../../../public/common/method.js';
import './index.less';
export default class CashFlowItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      json:{},
      checkedAll:false,//是否全选
      selectData:[],//选中数据
      selIds:[],//选中行号
      checkedArray: [//各行选中判断
        
      ],
      busrelation: [],
      saveData: {},
      isAllow: true,
      isCashflow: false,
      columns:[]
    }
  }
  componentWillMount(){
    let callback= (json) =>{
            this.setState({json:json,
            columns:[{
            title: (<div fieldid="pk_cashflow" className="mergecells">{json['20020BURLS-000019']}</div>),
              dataIndex: "pk_cashflow",
              key: "pk_cashflow",
              width: '600px',
              render: (text, record, index) => (
                this.props.addState=='check'?
                <div>{this.state.busrelation[index].pk_cashflow.display?this.state.busrelation[index].pk_cashflow.display:<span>&nbsp;</span>}</div>
                :
                <div fieldid="pk_cashflow">
                  <CashflowTreeRef
                    fieldid="pk_cashflow"
                    value =  {{
                      refname: this.state.busrelation[index].pk_cashflow.display, 
                      refpk: this.state.busrelation[index].pk_cashflow.value
                    }}
                    disabled={!record.isEdit}
                    onlyLeafCanSelect={true}
                    queryCondition={{
                      "pk_accountingbook": this.props.pk_accountingbook,
                      pk_org:this.props.pk_org,isDataPowerEnable: 'Y',
                      DataPowerOperationCode: 'fi'
                    }}
                    onChange={(v)=>{
                      let {busrelation} = this.state;
                      busrelation[index].pk_cashflow.display = v.refname;	
                      busrelation[index].pk_cashflow.value = v.refpk;	
                      let saveData = this.state.saveData;
                      saveData.busrelation = busrelation;
                      this.props.getSaveDataItem(saveData);
                      this.setState({
                        busrelation:busrelation
                      })                       
                    }}   
                  />
                  </div>
              )
            }, {
              title: (<div fieldid="scale" className="mergecells">{json['20020BURLS-000022']}</div>),
              dataIndex: "scale",
              key: "scale",
              width: '600px',
              render: (text, record, index) => (
                this.props.addState=='check'?
                <div>{this.state.busrelation[index]?this.state.busrelation[index].scale.display:<span>&nbsp;</span>}</div>
                :
                <div fieldid="scale">
                  <NCNumber
                    fieldid="scale"
                    scale={4}
                    isViewMode={!this.state.busrelation[index].isEdit}
                    value={this.state.busrelation[index]?this.state.busrelation[index].scale.display:''}
                    disabled={!record.isEdit}
                    onChange={(v) => {
                      let {busrelation} = this.state;
                      if(v<-100||v>100){
                        busrelation[index].scale.display = '';	
                        busrelation[index].scale.value = '';	
                      }else{
                        busrelation[index].scale.display = v;	
                        busrelation[index].scale.value = v;	
                        let scaleAdd = 0;
                        for(let i=0,len=busrelation.length;i<len;i++){
                          scaleAdd += Number(busrelation[i].scale.value);
                          if(scaleAdd>100){
                            toast({ content: json['20020BURLS-000023'], color: 'warning' });
                            busrelation[index].scale.display = '';	
                            busrelation[index].scale.value = '';	
                            this.setState({
                              busrelation:busrelation
                            })
                            return;
                          }
                        }
                      }
                      let saveData = this.state.saveData;
                      saveData.busrelation = busrelation;
                      
                      this.setState({
                        busrelation:busrelation
                      })
                      this.props.getSaveDataItem(saveData)
                    }}
                  />
                </div>
              )
            }]
        })
        }
        getMultiLang({moduleId:'20020BURLS',domainName:'gl',currentLocale:'simpchn',callback}); 
  }
  
  componentWillReceiveProps(nextProps) {
    let data = nextProps.localItem;
    let isCashflow = nextProps.isCashflow;
    let busrelation = [];
    if (data) { //新增业务规则时为空
      if (data.pk_creditaccasoa) {
        if (!data.pk_creditaccasoa.value && !data.pk_debitaccasoa.value) {
          this.setState({
            isAllow: false
          })
        } else {
          this.setState({
            isAllow: true
          })
        }
      }
      if (data.busrelation) {
        busrelation = data.busrelation;
      } else {
        busrelation = []
      }
      if (busrelation.length > 0) {
        for (let i = 0, len = busrelation.length; i < len; i++) {
          busrelation[i].key = i + 1;
        }
      }
    } else {
      busrelation = []
      this.setState({
        isAllow: false
      })
    }
    this.setState({
      saveData: nextProps.localItem,
      busrelation: busrelation,
      isCashflow
    })
  }
  getBack = (v,isDel) => {
    
    this.props.getSaveDataItem(v);
    if(isDel){
      this.setState({
        checkedArray:[],
        selectData:[],
        checkedAll:false,
        selIds:[],
        busrelation: v.data
      })
    }else{
      this.setState({
        busrelation: v.data
      })
    }
  }
  // clickRow = (data) => {
  //   let selectData = data;
  //   this.setState({
  //     selectData
  //   })
  // }
  //处理多选
  onAllCheckChange = () => {//全选
    let self = this;
    let checkedArray = [];
    let listData = self.state.busrelation.concat();
    let selIds = [];
    let busrelation = this.state.busrelation;
    for (var i = 0; i < busrelation.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
      selIds.push(i);
    }
    if(self.state.checkedAll){
      selIds=[]
    }
    self.setState({
      checkedAll: !self.state.checkedAll,
      selIds: selIds,
      checkedArray
    });
    // self.props.setCheckedArray(checkedArray)
    
    let selectArr = [];
    for (let i=0;i<selIds.length;i++) {
      selectArr.push(JSON.parse(JSON.stringify(busrelation[selIds[i]])));
    }
    this.setState({
      selectData:selectArr
    })
  };
  onCheckboxChange = (text, record, index) => {//单选
    let self = this;
    let allFlag = false;
    let selIds = self.state.selIds;
    // let id = self.props.postId;
    let checkedArray = self.state.checkedArray.concat();
    if (self.state.checkedArray[index]) {
      selIds.splice(record.key,1);
    } else {
      selIds.push(record.key);
    }
    checkedArray[index] = !self.state.checkedArray[index];
    for (let i = 0; i < self.state.checkedArray.length; i++) {
      if (!checkedArray[i]) {
        allFlag = false;
        break;
      } else {
        allFlag = true;
      }
    }
    self.setState({
      checkedAll: allFlag,
      selIds: selIds,
      checkedArray
    });
    // self.props.setCheckedArray(checkedArray)
    let busrelation = this.state.busrelation;
    let selectArr = [];
    selIds = this.sortarr(selIds)
    for (let i=0;i<selIds.length;i++) {
      for(let j=0;j<busrelation.length;j++){
        if(busrelation[j].key==selIds[i]){
          selectArr.push(JSON.parse(JSON.stringify(busrelation[j])));
        }
      }
    }
    this.setState({
      selectData:selectArr
    })
  };
  renderColumnsMultiSelect(columns) {
    const { data,checkedArray,busrelation } = this.state;
    let { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;
    // multiSelect= "checkbox";
    // let indeterminate_bool1 = true;
    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArray.length;
      while(i--){
          if(checkedArray[i]){
            indeterminate_bool = true;
            break;
          }
      }
      let defaultColumns = [
        {
          title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
            <Checkbox
              className="table-checkbox"
              checked={this.state.checkedAll}
              indeterminate={indeterminate_bool&&!this.state.checkedAll}
              onChange={this.onAllCheckChange}
            />
          }</div>),
          key: "checkbox",
          attrcode: "checkbox",
          dataIndex: "checkbox",
          width: "50px",
          render: (text, record, index) => {
            return (
              <div fieldid="firstcol">
                <Checkbox
                  className="table-checkbox"
                  checked={this.state.checkedArray[index]}
                  onChange={this.onCheckboxChange.bind(this, text, record, index)}
                />
              </div>
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
  sortarr=(arr)=>{//排序
    for(let i=0;i<arr.length-1;i++){
        for(let j=0;j<arr.length-1-i;j++){
            if(arr[j]>arr[j+1]){
                var temp=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
    return arr;
  }
  render() {
    let {
      busrelation,
      isCashflow
    } = this.state;
    let columns = this.renderColumnsMultiSelect(this.state.columns);
    return (
      <div className="CashFlowItemTable">
            <AddDelate 
              itemData={busrelation.length>0?busrelation:
                [{
                  pk_busrelation:{display: this.props.localItem?this.props.localItem.key:'', value: this.props.localItem?this.props.localItem.key:'', scale: 0}
                }]
                } 
              getBack={this.getBack.bind(this)}
              selectData={this.state.selectData}
              isAllow = {this.state.isAllow}
              isCashflow={isCashflow}
              localItem={this.props.localItem?this.props.localItem:{pk_busdetail:''}}
              addState={this.props.addState}
              busrelation={this.props.busrelation}
            />
            <div>
            </div>
            <NCDiv fieldid="cashflowitem" areaCode={NCDiv.config.TableCom}>
            <Table
                columns={columns}
                data={busrelation}
                bodyStyle={{height:getTableHeight(350)}}
                scroll={{ x: true, y: getTableHeight(350) }}
                // bordered
            />
            </NCDiv>
        </div>

    );
  }
}
CashFlowItem.defaultProps = {
  prefixCls: "bee-table",
  multiSelect: {
    type: "checkbox",
    param: "key"
  }
};