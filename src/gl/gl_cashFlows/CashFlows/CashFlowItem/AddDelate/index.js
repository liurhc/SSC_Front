import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import {ajax,base,getMultiLang} from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCButtonGroup:ButtonGroup,NCModal:Modal,NCPopconfirm:Popconfirm
    } = base;
import { toast } from '../../../../public/components/utils.js';
import './index.less';
export default class AddDelate extends Component{
    constructor(props){
        super(props);
        this.state = {
            json:{},
            itemData:[],
            selectData:[],
        }
    }
    componentWillMount(){
        let callback= (json) =>{
            this.setState({json:json},()=>{
//                 initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'20020BURLS',domainName:'gl',currentLocale:'simpchn',callback}); 
    }
    componentWillReceiveProps(nextProps){
        let selectData = nextProps.selectData;
        this.setState({
                itemData:nextProps.itemData,
                selectData
        })
    }
    handleAdd=()=>{
        let itemData = this.state.itemData;
        if(!itemData[0].pk_cashflow){//孙表数据为空时
            let obj=itemData[0];
            obj.isEdit = true;
            obj.pk_cashflow={display: "", value: "", scale: 0};
            obj.scale={display: "", value: "", scale: 2};
            obj.key = 1;
            // itemData.push(JSON.parse(JSON.stringify(obj)));
            this.props.getBack({'pk_busdetail':'','data':itemData});
        }else{
            let obj=JSON.parse(JSON.stringify(itemData[0]));
            obj.isEdit = true;
            obj.pk_cashflow={display: "", value: "", scale: 0};
            obj.scale={display: "", value: "", scale: 2};
            // itemData.push(JSON.parse(JSON.stringify(obj)));
            obj.key = itemData.length+1;
            itemData= [...itemData, obj];
            let pk_busdetail=itemData[0].pk_busdetail&&itemData[0].pk_busdetail.value?itemData[0].pk_busdetail.value:'';
            this.props.getBack({'pk_busdetail':pk_busdetail,'data':itemData});
        }
    }
    handleDelate=()=>{
        let itemData = this.state.itemData;
        if(typeof(itemData)=='string'){//孙表数据为空时
            toast({ content: this.state.json['20020BURLS-000016'], color: 'warning' });
        }else{
            let selectData = this.state.selectData;
            for(let i=0,len=itemData.length;i<len;i++){
                if(selectData.length){
                    for(let j=0,lenj=selectData.length;j<lenj;j++){
                        if(itemData[i].key==selectData[j].key){
                            itemData.splice(i,1);
                            len--;
                        }
                    }
                }else{
                    toast({ content: this.state.json['20020BURLS-000017'], color: 'warning' });
                }
            }
            let pk_busdetail=selectData[0].pk_busdetail&&selectData[0].pk_busdetail.value?selectData[0].pk_busdetail.value:'';
            this.props.getBack({'pk_busdetail':pk_busdetail,'data':itemData},true);
        }
    }
    handleIsAllow=()=>{
        if(!this.props.isAllow){
            toast({ content: this.state.json['20020BURLS-000018'], color: 'warning' });
        }
    }
    render(){
        let {itemData,selectData}=this.state;
        return (
            <div className="positon-t1 nc-theme-area-bgc nc-theme-area-split-bc" 
            onClick={this.handleIsAllow.bind(this)}>
                <div className="tittle" fieldid="header_area">
                    <h3 className="nc-theme-common-font-c" fieldid={`${this.state.json['20020BURLS-000019']}_title`}>{this.state.json['20020BURLS-000019']}</h3>
                </div>
                <div className="fr" style={{'display':((this.props.addState=='revise'&&this.props.localItem.pk_busdetail)?'block':'none')}}>
                    <ButtonGroup style={{'margin':'0'}}>
                        <Button onClick={this.handleAdd} fieldid="add"
                            disabled={!this.props.isAllow||this.props.isCashflow} 
                            style={{'margin-right':'2px','min-width':'60px','font-size':'13px'}}
                        >{this.state.json['20020BURLS-000020']}</Button>
                        <Button style={{'min-width':'60px','font-size':'13px'}} disabled={this.props.isCashflow||!selectData.length} onClick={this.handleDelate} fieldid="delete">
                            {this.state.json['20020BURLS-000021']}
                        </Button>
                    </ButtonGroup>
                    {/* {this.props.isAllow} */}
                </div>
            </div>
        )
    }
}