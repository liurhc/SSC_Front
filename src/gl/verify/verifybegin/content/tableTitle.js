/**
 *   Created by Liqiankun on 2018/7/18
 */

export const checkColumns = [
    {
        title: this.state.json['20020NAIPT-000026'],/* 国际化处理： 科目名称*/
        dataIndex: "accountname",
        key: "accountname",
        width: 200
    },
    {
        title: this.state.json['20020NAIPT-000052'],/* 国际化处理： 辅助核算*/
        dataIndex: "assvo",
        key: "assvo",
        width: 200
    },
    {
        title: this.state.json['20020NAIPT-000021'],/* 国际化处理： 币种*/
        dataIndex: "currtypename",
        key: "currtypename",
        width: 150
    },
    {
        title: this.state.json['20020NAIPT-000086'],/* 国际化处理： 未核销余额*/
        children: [
            {
                title: this.state.json['20020NAIPT-000032'],/* 国际化处理： 原币*/
                dataIndex: "verifyamount",
                key: "verifyamount",
                width: 200
            },
            {
                title: this.state.json['20020NAIPT-000055'],/* 国际化处理： 组织本币*/
                dataIndex: "localverifyamount",
                key: "localverifyamount",
                width: 200
            },
        ]
    },
    {
        title: this.state.json['20020NAIPT-000087'],/* 国际化处理： 总账余额*/
        children: [
            {
                title: this.state.json['20020NAIPT-000032'],/* 国际化处理： 原币*/
                dataIndex: "generalledgeramount",
                key: "generalledgeramount",
                width: 200
            },
            {
                title: this.state.json['20020NAIPT-000055'],/* 国际化处理： 组织本币*/
                dataIndex: "localgeneralledgeramount",
                key: "localgeneralledgeramount",
                width: 200
            },
        ]
    },
    {
        title: this.state.json['20020NAIPT-000088'],/* 国际化处理： 是否平衡*/
        dataIndex: "ifbalance",
        key: "ifbalance",
        width: 120
    },
];
