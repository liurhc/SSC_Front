import React, { Component } from "react";
import { high, base, ajax, deepClone, createPage, promptBox,toast,getMultiLang,createPageIcon } from "nc-lightapp-front";
import Immutable from 'immutable';
import { buttonReg, initTemplate} from './events';
const {
    NCFormControl: FormControl,
    NCDatePicker: DatePicker,
    NCButton: Button,
    NCTable: Table,
    NCSelect: Select,
    NCCheckbox: Checkbox,
    NCNumber,
    NCForm,
    NCModal: Modal,
    NCTooltip:Tooltip,NCAffix,
    NCPagination,NCDiv
} = base;
const { NCFormItem: FormItem } = NCForm;
import {SelectItem,InputItem,} from "../../../public/components/FormItems";
import QueryModal from "./queryModal/querymodal.js";
import createScript from "../../../public/components/uapRefer";
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import paginationUtils from '../../../public/common/paginationUtils.js'
import { getNowFormatDate } from "../../../manageReport/common/modules/modules";
import { getVerifyInitHeadTitle } from "../../../public/components/getVerifyInitHeadTitle.js";
import PrintModal from '../../../public/components/printModal';
const { PrintOutput } = high;
import { printRequire, mouldOutput } from '../../../public/components/printModal/events';
import ImportModal from "../../../public/components/file/importModal";
import ExportModal from '../../../public/components/file/exportModal';
import AssidModal from '../../../public/components/assidModal/index.js';
import {generatePretentAssData, generateAssValues} from '../../../public/components/assidModal/utils';
import { accAdd, Subtr, accMul, accDiv,getTableHeight } from '../../../public/common/method.js';
import globalStore from '../../../public/components/PageCache/GlobalStore';
import HeaderArea from '../../../public/components/HeaderArea';
import getHeadData from './getHeadData.js';
import getTableData from './getTableData.js';
import "./index.less"
import "./search.less"
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};
class VerifyBalances extends Component {
    constructor(props) {
        super(props);
        this.allChecked = false;
        this.valueArrnew=[];
        this.addDataFlag = false;//表示是否点击了新增按钮
        this.chageFlag = false;//是否修改了内容
        this.state = {
            resourceQueryData:{},//原始查询条件
            resourceQueryReultParam:{},
            deletearr:[],
            useflag:'',
            renderDataSource:[],//当前渲染的数据
            checkSubject:{},
            json:{},
            amountSum:'',//原币合计
            localamountSum:'',//本币合计
            querypk_unit:[],//查询条件所选业务单元的value数组值
            CurrinfoScale:0,//默认精度0
            isShowUnit:true,//是否启用业务单元
            localamountDisabled:false,//组织本币是否可编辑
            currencyDisabled:false,//分路航币种是否可编辑
            querypk_currtype:{display:'',value:''},//存储查询条件的币种
            querybalanorient:'',//科目方向
            querycurrinfo:{},//账簿对应的组织本币
            startDateBefore:'',//开始日期前一天
            assidCondition:{},//辅助核算弹框需要的数据
            appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            showPrintModal: false,
            outputData: {},
            showLeadIn: false, //控制是否显示导入页面
            addNewData: {}, //存放新增的一行数据，用于报错时恢复显示
            AssistCheckShow: false,
            queryData: {}, //查询条件
            flag: true,
            getPkOrgValue: "",
            allChecked: false, //总复选框
            allCheckedclone:false,//复制总复选框
            leadInAllChecked: false, //导入页面的总复选框
            checkedOne: false, //但行复选框
            addRefer: [], //新增数据中的参照数据，从选择"核销科目"时获取
            checkedList: [], //存储所有数据复选框的值
     
            leadCheckList: [], //存储导入页面中所有数据复选框的值
            tableAssistCheck: [], //表格中的每行"辅助核算"弹框中的内容
            tableTableReferCells: [], //存储表格中
            amountSum:"",
            amountSumNew:"",
            // localamountSum:"",
            localamountNew:"",
            totalamount:"",
            totallocalamount:"",
            showAddBtn: true, //新增按钮是否可用 true:不可用； false：可用
            delBtn:true,//删除按钮
            modifyBtn:true,//修改按钮
            searchBtn:false,//查询按钮
            saveBtn:true,//保存按钮是否可用 true:不可用； false：可用  之前是false
            outputBtn:true,//导入导出  之前是true
            checkBtn: true, //检查按钮是否可用 true:不可用； false：可用
            showPrintBtn: true, //打印按钮是否可用 true:不可用； false：可用
            modelBtn:true,//模板输出按钮是否可用 true:不可用； false：可用
            cancelBtn:true,//取消
            pk_currtype: {}, //币种
            modifyData:[],//保存修改之前的数据状态
            modalDefaultValue:{
                pk_accountingbook:{display:'',value:''},
                appcode:this.props.getSearchParam("c")
            },
            status:"normal",
            columns:[],
            dataSource: [], //table的数据
            count: 0,
            leadInDatasource: [], //导入数据表格的数据
            inlt:null,
            selectedRecord: "0", //选中分录行的key
            getrowkey: "", //获取指定行的Key
            listItem: {
                // accountingbookValue:{display:'',value:''},
                verObj: { display: "", value: "" },
                asscontent: { display: "", value: "" },
                begindate: { display: "", value: "" }
            },
            isQueryShow: false, //查询条件对话框
            columnsVerify: [],
            verifyBalancesData: [],
            queryCondition: [], //查询条件
            selectedData: {}, //选中行的数据
            changeStyle: "", //新增，修改，删除；insert update delete
            enableEdit: true, //是否可编辑 true: 不可编辑； false：可编辑
            importItemData:{},
            exportModalConfig:{},
            importModalConfig:{},
            debite:'0',//组织本币借方总额
            credit:'0',//组织本币贷方总额
            amdebite:'0',//原本币借方总额
            amcredit:'0',//原本币方总额
            verifynoValue:[],//存储核销号
            activePage: 1,
            pageData: [],//当前页面数据
            checkBoxArr:[],
            oldActivePage:null,
            assidModalState:{defaultvalue:{assData:[]}, show:false}
        };
        this.max_row = 10
        this.paginationUtils = new paginationUtils()
        this.handleChange = this.handleChange.bind(this);
        this.renderPkCurrency = this.renderPkCurrency.bind(this); //渲染币种参照
        this.VoucherTypeChange = this.VoucherTypeChange.bind(this); //凭证类别参照修改方法
        this.changeCheckValue = this.changeCheckValue.bind(this); //点击复选框事件
        this.allCheckFn = this.allCheckFn.bind(this); //点击总复选框事件
        this.renderCheckRow = this.renderCheckRow.bind(this); //用于渲染选择框
        this.queryDate = this.queryDate.bind(this);
       this.onDateChange = this.onDateChange.bind(this);
       this.ajaxData = this.ajaxData.bind(this);
        this.leadInFn = this.leadInFn.bind(this);
        this.getLeadInData = this.getLeadInData.bind(this); //getLeadInData(){//获取导入的数据
        this.leadInBackFn = this.leadInBackFn.bind(this); //leadInBackFn(){//点击导入页面的"导入"按钮
    }

	// 翻页
	handleSelect = (eventKey) => {
        // this.props.goPage(eventKey)
        let{renderDataSource,checkedList}=this.state
		let pageData = this.paginationUtils.getPageData(eventKey)
        this.setState({
		  activePage: eventKey,
          pageData,
          checkedList
        });
	}




    getNewRefer(param) {
        //选择"核销科目"时获取对应参照数据
        if(param){
            this.setState({
                addRefer: [...param]
            });
        }else{
            this.setState({
                addRefer: []
            });
        }
    }
    renderAssistCheck(text, record, index) {
        //渲染辅助核算
        let {queryCondition}=this.state;
        let showArr = [];
        if (text && text.length > 0) {
            text.map(item => {
               // ${item.checkvaluecode.display}
                item.pk_org=record.pk_unit&&record.pk_unit.value?record.pk_unit.value:record.pk_org?record.pk_org:queryCondition.pk_org;
               // let itemCell = `【${item.checktypename.display}:/${item.checkvaluename.display}】`;
               let itemCell
                if(item.checkvaluecode && item.checkvaluecode.value){
                    itemCell = `【${item.checktypename.display}:${item.checkvaluecode.display}/${item.checkvaluename.display}】`;
                }else{
                    itemCell = `【${item.checktypename.display}:${item.checkvaluename.display}】`;
                }
                showArr.push(itemCell);
            });
        }
        return (
            <div
                className={!this.state.enableEdit ? "canEdit" : "NoEdit"}
                fieldid="assvo"
                onClick={
                    //点击时把此行的"辅助和算"通过'tableAssistCheck'传给弹框
                    !this.state.enableEdit
                        ? () => {
                            this.showAssidModal(index, record);
                        }
                        : ""
                }
            >
            {record['assid']&&record['assid'].display?record['assid'].display:showArr}
                {/* {showArr} */}
            </div>
        );
    }
    renderCheckRow(json) {
        let {isShowUnit}=this.state;
        const dateInputPlaceholder = json['20020NAIPT-000037'];/* 国际化处理： 选择日期*/
        let columns= [ 
            {
                title: (<div fieldid="key"  className="mergecells">{json['20020NAIPT-000089']}</div>),   /* 国际化处理： 行号*/
                key: 'key',
                attrcode: 'key',
                dataIndex: 'key',
                width: '70px',
                // fixed: "left",
                render: (text, record, index) => <div class='indexCode' fieldid="key">{index + 1}</div>
            },
            {
                title: (<div fieldid="prepareddate" className="mergecells">{json['20020NAIPT-000027']}</div>),/* 国际化处理： 凭证日期*/
                dataIndex: "prepareddate",
                key: "prepareddate",
                width: 120,
                className:"data_busin",
                className:"updowncenter",
                render: (text, record, index) => {
                    return (
                        // this.state.status=="normal"&&!record.rowEdited?
                        (this.state.status=="normal"||!record.rowEdited)?
                            <div fieldid="prepareddate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                        :
                        <div fieldid="prepareddate">
                            <DatePicker
                                fieldid="prepareddate"
                                disabled={this.state.enableEdit}
                                value={text.display}
                                isclickTrigger={true}
                                onChange={value =>
                                    this.onDateChange(value, index, "prepareddate")
                                }
                                placeholder={dateInputPlaceholder}
                            /> 
                        </div>
                    );
                }
            },
            {
                title: (<div fieldid="businessdate" className="mergecells">{json['20020NAIPT-000028']}</div>),/* 国际化处理： 业务日期*/
                dataIndex: "businessdate",
                key: "businessdate",
                width: 120,
                className:"data_busin",
                className:"updowncenter",
                render: (text, record, index) => {
                    return (
                        (this.state.status=="normal"||!record.rowEdited)?
                            <div fieldid="businessdate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                        :
                        <div fieldid="businessdate">
                            <DatePicker
                                fieldid="businessdate"
                                disabled={this.state.enableEdit}
                                value={text.display}
                                isclickTrigger={true}
                                onChange={value =>
                                    this.onDateChange(value, index, "businessdate")
                                }
                                placeholder={dateInputPlaceholder}
                            />
                        </div>
                    );
                }
            },
            {
                title: (<div fieldid="pk_vouchertype" className="mergecells">{json['20020NAIPT-000051']}</div>),/* 国际化处理： 凭证类别*/
                dataIndex: "pk_vouchertype",
                key: "pk_vouchertype",
                width: 120,
                className:"updowncenter uppadding",
                render: (text, record, index) => {
                    return (this.state.status=="normal"||!record.rowEdited)?
                    <Tooltip trigger="hover" placement="top" inverse overlay={text.display}>
                            <div fieldid="pk_vouchertype">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                        </Tooltip>
                    :this.rednerVoucherType(
                        record,
                        text,
                        index,
                        "pk_vouchertype",
                        this.VoucherTypeChange
                    )
                }
            },
            {
                title: (<div fieldid="voucherno" className="mergecells">{json['20020NAIPT-000029']}</div>),/* 国际化处理： 凭证号*/
                dataIndex: "voucherno",
                key: "voucherno",
                width: 100,
                className:"voucherno",
                render: (text, record, index) => {
                    return (
                        (this.state.status=="normal"||!record.rowEdited)?
                        <Tooltip trigger="hover" placement="top" inverse overlay={text.display}>
                            <div fieldid="voucherno">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                        </Tooltip>
                        :
                        <div fieldid="voucherno">
                            <NCNumber
                                fieldid="voucherno"
                                scale={0}
                                disabled={this.state.enableEdit}
                                value={text.value}
                                onChange={
                                    
                                    value => this.onDateChange(value, index, "voucherno")
                                }
                            />
                        </div>
                    );
                }
            },
            {
                title: (<div fieldid="pk_unit" className="mergecells">{json['20020NAIPT-000019']}</div>),/* 国际化处理： 业务单元*/
                dataIndex: "pk_unit",
                key: "pk_unit",
                width: 200,
                className:"updowncenter uppadding",
                render: (text, record, index) => {
                    return (this.state.status=="normal"||!record.rowEdited)?
                    <Tooltip trigger="hover" placement="top" inverse overlay={text.display}>
                        <div fieldid="pk_unit">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                    </Tooltip>
                    :this.rednerpk_unit(
                        record,
                        text,
                        index,
                        "pk_unit"
                    ); 
                }
            },
            {
                title: (<div fieldid="assvo" className="mergecells">{json['20020NAIPT-000052']}</div>),/* 国际化处理： 辅助核算*/
                dataIndex: "assvo",
                key: "assvo",
                width: 200,
                className:"updowncenter assvo",
                render: (text, record, index) => {
                    let showArr = [];
                    if (text && text.length > 0) {
                        text.map(item => {
                            let itemCell = `【${item.checkvaluename.display}】`;
                            showArr.push(itemCell);
                        });
                    }
                    return (this.state.status=="normal"||!record.rowEdited)?
                    <Tooltip trigger="hover" placement="top" inverse overlay={record['assid']&&record['assid'].display?record['assid'].display:showArr}>
                        <div id="vouchertypeaaa">
                            {record['assid']&&record['assid'].display?record['assid'].display:showArr}
                            {/* this.renderAssistCheck(text, record, index) */}
                        </div>
                    </Tooltip>
                    :
                    <div className="assvo_next">{
                        this.renderAssistCheck(text, record, index)
                    }</div>
                }
            },
            {
                title: (<div fieldid="explanation" className="mergecells">{json['20020NAIPT-000030']}</div>),/* 国际化处理： 摘要*/
                dataIndex: "explanation",
                key: "explanation",
                width : 100,
                className:"explanation",
                render: (text, record, index) => {
                    return (
                        (this.state.status=="normal"||!record.rowEdited)?
                        <Tooltip trigger="hover" placement="top" inverse overlay={text.value}>
                            <div fieldid="explanation">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                        </Tooltip>
                        :
                        <div>
                            {this.renderExplanation(
                                record,
                                text,
                                index,
                                "explanation",
                                this.VoucherTypeChange
                            )}
                        </div> 
                    );
                }
            },
            {
                title: (<div fieldid="verifyno" className="mergecells">{json['20020NAIPT-000031']}</div>),/* 国际化处理： 核销号*/
                dataIndex: "verifyno",
                key: "verifyno",
                width: 100,
                className :"verifyno",
                render: (text, record, index) => {
                    return (
                        (this.state.status=="normal"||!record.rowEdited)?
                        <Tooltip trigger="hover" placement="top" inverse overlay={text.display}>
                            <div fieldid="verifyno">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                        </Tooltip>
                        :
                        <FormControl
                            fieldid="verifyno"
                            disabled={this.state.enableEdit}
                            value={text.value}
                            onChange={(value)=>{
                                this.onDateChange(value, index, "verifyno");
                            }}
                            onBlur={(value)=>{
                                if(value.length>40){
                                    toast({content:json['20020NAIPT-000053'],color:'danger'})/* 国际化处理： 请输入长度应小于40个字符的核销号*/
                                    return false;
                                }
                            }}
                        />  
                    );
                }
            },
            {
                title: (<div fieldid="pk_currtype" className="mergecells">{json['20020NAIPT-000021']}</div>),/* 国际化处理： 币种*/
                dataIndex: "pk_currtype",
                key: "pk_currtype",
                width: 100,
                className:"data_busin updowncenter uppadding",
                render: (text, record, index) => {
                    return  (this.state.status=="normal"||!record.rowEdited)?
                        <div fieldid="pk_currtype">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                    :
                    this.renderPkCurrency(text, record, index);
                    // this.renderPkCurrency(text, record, index);
                }
            },
            {
                title: (<div fieldid="orientation" className="mergecells">{json['20020NAIPT-000054']}</div>),/* 国际化处理： 方向*/
                dataIndex: "orientation",
                key: "orientation",
                width:70,
                render: (text, record, index) => {
                    return (
                        (this.state.status=="normal"||!record.rowEdited)?
                            <div fieldid="orientation">{(text.value=='1')?json['20020NAIPT-000038']:json['20020NAIPT-000063']}</div>
                        :
                        <div fieldid="orientation">
                            <Select
                                showClear={false}
                                fieldid="orientation"
                                disabled={this.state.enableEdit}
                                value={text.value}
                                onChange={value =>
                                    this.onDateChange(value, index, "orientation")
                                }
                            >
                                <Option value="1">{json['20020NAIPT-000038']}</Option>{/* 国际化处理： 借*/}
                                <Option value="-1">{json['20020NAIPT-000063']}</Option>{/* 国际化处理： 贷*/}
                            </Select>  
                        </div>
                    );
                }
            },
            {
                title: (<div fieldid="amount" className="mergecells">{json['20020NAIPT-000032']}</div>),/* 国际化处理： 原币*/
                dataIndex: "amount",
                key: "amount",
                width: 100,
                className:"amount",
                render: (text, record, index) => {
                    let {querycurrinfo}=this.state;
                    return (              
                        (this.state.status=="normal"||!record.rowEdited)?
                        <Tooltip trigger="hover" placement="top" inverse overlay={text.display}>
                            <div fieldid="amount">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                        </Tooltip>
                        :
                        <div fieldid="amount">
                            <NCNumber
                                fieldid="amount"
                                scale={Number(text.scale)}
                                disabled={this.state.enableEdit}
                                value={text.value}
                                onChange={
                                    (value) =>{
                                        this.onDateChange(value, index, "amount")
                                    }
                                }
                                onBlur={(value)=>{
                                    this.countAmountLocalamountSum();
                                }}
                            />
                        </div>
                    );
                }
            },
            {
                title: (<div fieldid="localamount" className="mergecells">{json['20020NAIPT-000055']}</div>),/* 国际化处理： 组织本币*/
                dataIndex: "localamount",
                key: "localamount",
                width:100,
                className:"amount",
                render: (text, record, index) => {
                    return (
                        (this.state.status=="normal"||!record.rowEdited)?
                        <Tooltip trigger="hover" placement="top" inverse overlay={text.display}>
                            <div fieldid="localamount">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                        </Tooltip>
                        :
                        <div fieldid="localamount">
                            <NCNumber
                                fieldid="localamount"
                                scale={Number(this.state.CurrinfoScale)}
                                disabled={this.state.enableEdit||record.localamountDisabled}
                                value={text.value}
                                onChange={value => {
                                    this.onDateChange(value, index, "localamount");
                                }}
                                onBlur={(value)=>{
                                    this.countAmountLocalamountSum();
                                }}
                            />
                        </div>
                    );
                }
            }
        ];
        //渲染查询框中的复选按钮;
        let checkRow = [
            {
                title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
                    <Checkbox
                        checked={
                            !this.state.showLeadIn
                                ? this.state.allChecked
                                : this.state.leadInAllChecked
                        }
                        onChange={value => {
                            this.allCheckFn();
                        }}
                    />
                    }</div>),
                key: "checkbox",
                attrcode: "checkbox",
                dataIndex: "checkbox",
                width: "30px",
                fixed: "left",
                render: (text, record, index) => {
                    return (
                        <div fieldid="firstcol">
                            <Checkbox
                                className="table-checkbox"
                                checked={
                                    !this.state.showLeadIn
                                        ? this.state.checkedList[index]
                                        : this.state.leadCheckList[index]
                                }
                                onChange={value => {
                                    this.changeCheckValue(value,record, index);
                                }}
                            />
                        </div>
                    );
                }
            }
        ];
        // let cerrentColumns=deepClone(columns);
        let $cerrentColumns=Immutable.fromJS(columns);
        let cerrentColumns=$cerrentColumns.toJS();
        if(!isShowUnit){//不显示业务单元
            cerrentColumns.map((list,index)=>{
                if(list.key=='pk_unit'){
                    cerrentColumns.splice(index,1);
                }
            })
        }
        let newColunms = checkRow.concat(cerrentColumns);
        return newColunms;
    }



    renderCheckRowtwo() {
        let {isShowUnit,json}=this.state;
        let checkColumns = [
            {
            title: (<div fieldid="unitname" className="mergecells">{json['20020NAIPT-000019']}</div>),/* 国际化处理： 业务单元*/
                dataIndex: "unitname",
                key: "unitname",
                width: 150,
                render: (text, record, index) => {
                    return <div fieldid="unitname">{!text?<span>&nbsp;</span>:text}</div>
                }
            },
            {
                title: (<div fieldid="accountname" className="mergecells">{json['20020NAIPT-000026']}</div>),/* 国际化处理： 科目名称*/
                dataIndex: "accountname",
                key: "accountname",
                width: 150,
                render: (text, record, index) => {
                    return <div fieldid="accountname">{!text?<span>&nbsp;</span>:text}</div>
                }
            },
            {
                title: (<div fieldid="assvo" className="mergecells">{json['20020NAIPT-000052']}</div>),/* 国际化处理： 辅助核算*/
                dataIndex: "assvo",
                key: "assvo",
                width: 200,
                render: (text, record, index) => {
                    return <div fieldid="assvo">{!text?<span>&nbsp;</span>:text}</div>
                }
            },
            {
                title: (<div fieldid="currtypename" className="mergecells">{json['20020NAIPT-000021']}</div>),/* 国际化处理： 币种*/
                dataIndex: "currtypename",
                key: "currtypename",
                width: 100,
                render: (text, record, index) => {
                    return <div fieldid="currtypename">{!text?<span>&nbsp;</span>:text}</div>
                }
            },
            {
                title: json['20020NAIPT-000086'],/* 国际化处理： 未核销余额*/
                children: [
                    {
                        title: (<div fieldid="verifyamount" className="mergecells">{json['20020NAIPT-000032']}</div>),/* 国际化处理： 原币*/
                        dataIndex: "verifyamount",
                        key: "verifyamount",
                        className :"generalledgeramount",
                        width: 150,
                        render: (text, record, index) => {
                            return <div fieldid="verifyamount">{!text?<span>&nbsp;</span>:text}</div>
                        }
                    },
                    {
                        title: (<div fieldid="localverifyamount" className="mergecells">{json['20020NAIPT-000055']}</div>),/* 国际化处理： 组织本币*/
                        dataIndex: "localverifyamount",
                        key: "localverifyamount",
                        className :"generalledgeramount",
                        width: 150,
                        render: (text, record, index) => {
                            return <div fieldid="localverifyamount">{!text?<span>&nbsp;</span>:text}</div>
                        }
                    },
                ]
            },
            {
                title: json['20020NAIPT-000087'],/* 国际化处理： 总账余额*/
                children: [
                    {
                        title: (<div fieldid="generalledgeramount" className="mergecells">{json['20020NAIPT-000032']}</div>),/* 国际化处理： 原币*/
                        dataIndex: "generalledgeramount",
                        key: "generalledgeramount",
                        className :"generalledgeramount",
                        width: 150,
                        render: (text, record, index) => {
                            return <div fieldid="generalledgeramount">{!text?<span>&nbsp;</span>:text}</div>
                        }
                    },
                    {
                        title: (<div fieldid="localgeneralledgeramount" className="mergecells">{json['20020NAIPT-000055']}</div>),/* 国际化处理： 组织本币*/
                        dataIndex: "localgeneralledgeramount",
                        key: "localgeneralledgeramount",
                        width: 150,
                        className :"generalledgeramount",
                        render: (text, record, index) => {
                            return <div fieldid="localgeneralledgeramount">{!text?<span>&nbsp;</span>:text}</div>
                        }
                    },
                ]
            },
            {
                title: (<div fieldid="ifbalance" className="mergecells">{json['20020NAIPT-000088']}</div>),/* 国际化处理： 是否平衡*/
                dataIndex: "ifbalance",
                key: "ifbalance",
                width: 120,
                render: (text, record, index) => {
                    return <div fieldid="ifbalance">{!text?<span>&nbsp;</span>:text}</div>
                }
            },
        ];
        if(!isShowUnit){//不显示业务单元
            checkColumns.map((list,index)=>{
                if(list.key=='unitname'){
                    checkColumns.splice(index,1);
                }
            })
        }
        return checkColumns;
    }

    //全选 点击总复选框事件
    allCheckFn(value, index) {
        let { allChecked,checkedList, leadCheckList,leadInAllChecked,useflag,dataSource} = this.state;

        if (!this.state.showLeadIn) {
            if(!this.state.allChecked){
                checkedList.map((item,index)=>{
                    checkedList[index]=true;
                })
                this.setState({
                    checkedList,
                    allChecked: !this.state.allChecked
                });
                if(useflag==false){
                    let buttonFalse=['delBtn']
                    this.props.button.setButtonDisabled(buttonFalse, false);
                }

            }else{
                checkedList.map((item,index)=>{
                    checkedList[index]=false;
                })
                this.setState({
                    checkedList,
                    allChecked: !this.state.allChecked
                });
                let buttonFalse=['delBtn']
                this.props.button.setButtonDisabled(buttonFalse, true);
            }
        }else{
            if (!this.state.leadInAllChecked) {//选中的时候
                leadCheckList.map((item,index)=>{
                    leadCheckList[index]=true;
                })
                this.setState({
                    leadCheckList,
                    leadInAllChecked: !this.state.leadInAllChecked
                });
            } else {//取消选中的时候
                leadCheckList.map((item,index)=>{
                    leadCheckList[index]=false;
                })
                this.setState({
                    leadCheckList,
                    leadInAllChecked: !this.state.leadInAllChecked
                });
            }
        }
    }
    //单选 点击复选框事件
    changeCheckValue(value, record,index) {        
        let { allChecked,checkedList, leadCheckList,leadInAllChecked, useflag,checkBoxArr} = this.state;
        checkBoxArr.push(index)
        if (!this.state.showLeadIn) {
            checkedList[index] = value;
            if(value){
                // let clonecheckedList=deepClone(checkedList);
                let $clonecheckedList=Immutable.fromJS(checkedList);
                let clonecheckedList=$clonecheckedList.toJS();
                clonecheckedList.map((item,index)=>{
                    clonecheckedList[index]=item.toString();
                })
                if(clonecheckedList.indexOf('false')==-1){
                    allChecked=true;
                }
                if(clonecheckedList.indexOf('true')!=-1){
                    if(useflag==false){
                        let buttonFalse=['delBtn']
                        this.props.button.setButtonDisabled(buttonFalse, false);
                    }
                }else{
                    let buttonFalse=['delBtn']
                    this.props.button.setButtonDisabled(buttonFalse, true);
                }
            }else{
                // let clonecheckedList2=deepClone(checkedList);
                let $clonecheckedList2=Immutable.fromJS(checkedList);
                let clonecheckedList2=$clonecheckedList2.toJS();
                clonecheckedList2.map((item,index)=>{
                    clonecheckedList2[index]=item.toString();
                })
                if(clonecheckedList2.indexOf('true')==-1){
                    allChecked=false;
                }
                if(clonecheckedList2.indexOf('true')!=-1){
                    if(useflag==false){
                        let buttonFalse=['delBtn']
                        this.props.button.setButtonDisabled(buttonFalse, false);
                    }
                }else{
                    let buttonFalse=['delBtn']
                    this.props.button.setButtonDisabled(buttonFalse, true);
                }
            }
            this.setState({
                checkedList,allChecked
            });
        } else {
            leadCheckList[index] = value;
            if(value){
                // let cloneleadCheckList=deepClone(leadCheckList);
                let $cloneleadCheckList=Immutable.fromJS(leadCheckList);
                let cloneleadCheckList=$cloneleadCheckList.toJS();
                cloneleadCheckList.map((item,index)=>{
                    cloneleadCheckList[index]=item.toString();
                })
                if(cloneleadCheckList.indexOf('false')==-1){
                    leadInAllChecked=true;
                }
            }else{
                // let cloneleadCheckList2=deepClone(leadCheckList);
                let $cloneleadCheckList2=Immutable.fromJS(leadCheckList);
                let cloneleadCheckList2=$cloneleadCheckList2.toJS();
                cloneleadCheckList2.map((item,index)=>{
                    cloneleadCheckList2[index]=item.toString();
                })
                if(cloneleadCheckList2.indexOf('true')==-1){
                    leadInAllChecked=false;
                }
            }
            this.setState({
                leadCheckList,leadInAllChecked
            });
        }
    }
    //切换币种请求汇率
    changeCurrtypeGetrate=(pk_accountingbook,pk_currtype,newDataSource,index)=>{
        let self=this;
        let businessdate=newDataSource[index]['businessdate'].value;
        let rateUrl="/nccloud/gl/glpub/ratequery.do";
        let param={
            'pk_accountingbook':pk_accountingbook,
            'pk_currtype':pk_currtype,
            'date':businessdate
        }
        ajax({
            url:rateUrl,
            data:param,
            async:false,
            success:function(res){
                let {success,data}=res;
                if(success){
                    let amountScale=data.scale;
                    newDataSource[index]['amount'].scale=amountScale;
                    self.setState({newDataSource});
                }
            },
            error:function(error){
                toast({content:error.message,color:'warning'});
            }
        })
    }
    componentWillMount() {
        let callback= (json,status, inlt) =>{
            this.columns=this.renderCheckRow(json)
            const importItemData = [
                { itemName: json['20020NAIPT-000039'], itemType: 'radio', itemKey: 'usecode', value: 'Y', itemChild: [{ value: 'Y', label: json['20020NAIPT-000040'] }, { value: 'N', label: json['20020NAIPT-000041'] }] },/* 国际化处理： 导入方式:,按编码,按名称*/
                { itemName: json['20020NAIPT-000042'], itemType: 'radio', itemKey: 'isroundup', value: 'Y', itemChild: [{ value: 'Y', label: json['20020NAIPT-000043'] }, { value: 'N', label:json['20020NAIPT-000044'] }] }/* 国际化处理： 精度处理方式:,四舍五入,舍位*/
            ]
            const exportItemData = [
                { itemName: json['20020NAIPT-000045'], itemType: 'radio', itemKey: 'exportdata', value:'Y', itemChild: [{ value: 'Y', label: json['20020NAIPT-000046'] }, { value: 'N', label: json['20020NAIPT-000047'] }] },/* 国际化处理： 导出内容:,样式+数据,样式*/
                { itemName: json['20020NAIPT-000048'], itemType: 'radio', itemKey: 'exporttype', value:'Y', itemChild: [ { value: 'Y', label: json['20020NAIPT-000040'] }, { value: 'N', label: json['20020NAIPT-000041'] } ] }/* 国际化处理： 导出方式:,按编码,按名称*/
            ]
            this.cellColumns=[
                {
                    title: (<div fieldid="checktypename"  className="mergecells">{json['20020NAIPT-000049']}</div>),/* 国际化处理： 核算类型名称*/
                    dataIndex: "checktypename",
                    key: "checktypename",
                    render: (text, record, index) => {
                        return <div fieldid="checktypename">{text==null||(text&&!text.value)?<span>&nbsp;</span>:text.value}</div>;
                    }
                },
                {
                    title: (<div fieldid="refpath"  className="mergecells">{json['20020NAIPT-000050']}</div>),/* 国际化处理： 核算内容*/
                    dataIndex: "refpath",
                    key: "refpath",
                    width: "200px",
                    render: (text, record, index) => {
                        return (
                        <div fieldid="refpath">{this.renderTableTableRefer(text, record)}</div>
                        )

                    }
                }
            ]
            this.loadQuery = [
                {
                    itemName: json['20020NAIPT-000018'],/* 国际化处理： 核算账簿*/
                    itemType: "refer",
                    itemKey: "pk_accountingbook",isMustItem:true,
                    config: { refCode: "uapbd/refer/org/AccountBookTreeRef" }
                },
                {
                    itemName: json['20020NAIPT-000019'],/* 国际化处理： 业务单元*/
                    itemType: "refer",
                    itemKey: "pk_units",isMustItem:false,
                    config: { refCode: "uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef" }
                },
                {
                    itemName: json['20020NAIPT-000020'],/* 国际化处理： 核销科目*/
                    itemType: "refer",
                    itemKey: "pk_accasoa",isMustItem:true,
                    config: { refCode: "uapbd/refer/fiacc/AccountDefaultGridTreeRef" }
                },
                {
                    itemName: json['20020NAIPT-000021'],/* 国际化处理： 币种*/
                    itemType: "refer",
                    itemKey: "pk_currency",isMustItem:false,
                    config: { refCode: "gl/refer/voucher/TransCurrGridRef" }
                }
            ];
            this.Defaultcolumns = [
                //借方
                {
                    title: (<div fieldid="m_sSubjCode" className="mergecells">{json['20020NAIPT-000025']}</div>),/* 国际化处理： 科目编码*/
                    dataIndex: "m_sSubjCode",
                    key: "m_sSubjCode",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_sSubjCode">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_sSubjName" className="mergecells">{json['20020NAIPT-000026']}</div>),/* 国际化处理： 科目名称*/
                    dataIndex: "m_sSubjName",
                    key: "m_sSubjName",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_sSubjName">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_prepareddate" className="mergecells">{json['20020NAIPT-000027']}</div>),/* 国际化处理： 凭证日期*/
                    dataIndex: "m_prepareddate",
                    key: "m_prepareddate",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_prepareddate">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_Businessdate" className="mergecells">{json['20020NAIPT-000028']}</div>),/* 国际化处理： 业务日期*/
                    dataIndex: "m_Businessdate",
                    key: "m_Businessdate",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_Businessdate">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_VoucherNo" className="mergecells">{json['20020NAIPT-000029']}</div>),/* 国际化处理： 凭证号*/
                    dataIndex: "m_VoucherNo",
                    key: "m_VoucherNo",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_VoucherNo">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_explanation" className="mergecells">{json['20020NAIPT-000030']}</div>),/* 国际化处理： 摘要*/
                    dataIndex: "m_explanation",
                    key: "m_explanation",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_explanation">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_VerifyNo" className="mergecells">{json['20020NAIPT-000031']}</div>),/* 国际化处理： 核销号*/
                    dataIndex: "m_VerifyNo",
                    key: "m_VerifyNo",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_VerifyNo">{text?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_debitamount" className="mergecells">{json['20020NAIPT-000032']}</div>),/* 国际化处理： 原币*/
                    dataIndex: "m_debitamount",
                    key: "m_debitamount",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_debitamount">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_localdebitamount" className="mergecells">{json['20020NAIPT-000033']}</div>),/* 国际化处理： 本币*/
                    dataIndex: "m_localdebitamount",
                    key: "m_localdebitamount",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_localdebitamount">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_Balancedebitamount" className="mergecells">{json['20020NAIPT-000034']}</div>),/* 国际化处理： 原币余额*/
                    dataIndex: "m_Balancedebitamount",
                    key: "m_Balancedebitamount",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_Balancedebitamount">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_Balancelocaldebitamount" className="mergecells">{json['20020NAIPT-000035']}</div>),/* 国际化处理： 本币余额*/
                    dataIndex: "m_Balancelocaldebitamount",
                    key: "m_Balancelocaldebitamount",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_Balancelocaldebitamount">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="m_Accountage" className="mergecells">{json['20020NAIPT-000036']}</div>),/* 国际化处理： 账龄*/
                    dataIndex: "m_Accountage",
                    key: "m_Accountage",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="m_Accountage">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                }
            ];
            this.checkColumns = [
                {
                    title: (<div fieldid="unitname" className="mergecells">{json['20020NAIPT-000019']}</div>),/* 国际化处理： 业务单元*/
                    dataIndex: "unitname",
                    key: "unitname",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="unitname">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="accountname" className="mergecells">{json['20020NAIPT-000026']}</div>),/* 国际化处理： 科目名称*/
                    dataIndex: "accountname",
                    key: "accountname",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="accountname">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="assvo" className="mergecells">{json['20020NAIPT-000052']}</div>),/* 国际化处理： 辅助核算*/
                    dataIndex: "assvo",
                    key: "assvo",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="assvo">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="currtypename" className="mergecells">{json['20020NAIPT-000021']}</div>),/* 国际化处理： 币种*/
                    dataIndex: "currtypename",
                    key: "currtypename",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="currtypename">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: json['20020NAIPT-000086'],/* 国际化处理： 未核销余额*/
                    children: [
                        {
                            title: (<div fieldid="verifyamount" className="mergecells">{json['20020NAIPT-000032']}</div>),/* 国际化处理： 原币*/
                            dataIndex: "verifyamount",
                            key: "verifyamount",
                            className :"generalledgeramount",
                            width: 100,
                            render: (text, record, index) => {
                                return <div fieldid="verifyamount">{text==null?<span>&nbsp;</span>:text}</div>
                            }
                        },
                        {
                            title: (<div fieldid="localverifyamount" className="mergecells">{json['20020NAIPT-000055']}</div>),/* 国际化处理： 组织本币*/
                            dataIndex: "localverifyamount",
                            key: "localverifyamount",
                            className :"generalledgeramount",
                            width: 100,
                            render: (text, record, index) => {
                                return <div fieldid="localverifyamount">{text==null?<span>&nbsp;</span>:text}</div>
                            }
                        },
                    ]
                },
                {
                    title: json['20020NAIPT-000087'],/* 国际化处理： 总账余额*/
                    children: [
                        {
                            title: (<div fieldid="generalledgeramount" className="mergecells">{json['20020NAIPT-000032']}</div>),/* 国际化处理： 原币*/
                            dataIndex: "generalledgeramount",
                            key: "generalledgeramount",
                            className :"generalledgeramount",
                            width: 100,
                            render: (text, record, index) => {
                                return <div fieldid="generalledgeramount">{text==null?<span>&nbsp;</span>:text}</div>
                            }
                        },
                        {
                            title: (<div fieldid="localgeneralledgeramount" className="mergecells">{json['20020NAIPT-000055']}</div>),/* 国际化处理： 组织本币*/
                            dataIndex: "localgeneralledgeramount",
                            key: "localgeneralledgeramount",
                            width: 100,
                            className :"generalledgeramount",
                            render: (text, record, index) => {
                                return <div fieldid="localgeneralledgeramount">{text==null?<span>&nbsp;</span>:text}</div>
                            }
                        },
                    ]
                },
                {
                    title: (<div fieldid="ifbalance" className="mergecells">{json['20020NAIPT-000088']}</div>),/* 国际化处理： 是否平衡*/
                    dataIndex: "ifbalance",
                    key: "ifbalance",
                    width: 120,
                    render: (text, record, index) => {
                        return <div fieldid="ifbalance">{text==null?<span>&nbsp;</span>:text}</div>
                    }
                },
            ];
            this.headData = [
                // {itemName:'核算账簿:',itemType:'textInput',itemKey:'accountingbookValue'},
                { itemName: this.state.json['20020NAIPT-000022'], itemType: "textInput", itemKey: "verObj" },/* 国际化处理： 核销对象科目:*/
                { itemName: this.state.json['20020NAIPT-000023'], itemType: "textInput", itemKey: "asscontent" },/* 国际化处理： 辅助核算:*/
                { itemName: this.state.json['20020NAIPT-000024'], itemType: "textInput", itemKey: "begindate" }/* 国际化处理： 启用日期:*/
            ];
            const dateInputPlaceholder = json['20020NAIPT-000037'];/* 国际化处理： 选择日期*/
            this.setState({
                json:json,
                inlt,
                importModalConfig:{show:false, itemsData:importItemData, data:{}},
                exportModalConfig:{show:false, itemsData:exportItemData, data:{}},
                
            },()=>{
                initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'20020NAIPT',domainName:'gl',currentLocale:'simpchn',callback});
        window.onbeforeunload = () => {            
            let {status}=this.state
            if (status == 'edit' || status == 'add') {
                return ''
            }
        }
    }
    /**
     * value:修改后的日期
     * index:修改的字段所在的行数
     * keyParam：当前修改字段的key值
     *
     * 凭证日期 /	业务日期 / 凭证号 /核销号 / 方向/ 原币
     * */
    onDateChange(value, index, keyParam) {
        let self=this;
        if(keyParam=='verifyno'){
            if(value.length>40){
                toast({content:self.state.json['20020NAIPT-000053'],color:'danger'});/* 国际化处理： 请输入长度应小于40个字符的核销号*/
                return false;
            }
        }
        self.chageFlag = true;
        let { dataSource, querycurrinfo,localamountDisabled,querypk_currtype} = self.state;
        let newDataSource = [...dataSource];
        if(keyParam=='pk_currtype'){
            newDataSource[index][keyParam].display = value.refname;
            newDataSource[index][keyParam].value = value.refpk;
            newDataSource[index].change = true;
            if(value.refpk==querycurrinfo.value){//录得币种跟所选本币相同
                newDataSource[index].localamountDisabled=true;
            }else{
                newDataSource[index].localamountDisabled=false;
            }
            //切换币种请求汇率
            let businessdate=newDataSource[index]['businessdate'].value;
            self.changeCurrtypeGetrate(self.state.queryData.pk_accountingbook,value.refpk,newDataSource,index);
        }else{
            newDataSource[index][keyParam].display = value;
            newDataSource[index][keyParam].value = value;
            newDataSource[index].change = true;
        }
        if(keyParam=='amount'){
            if(newDataSource[index].localamountDisabled){//录得币种跟组织本币相同
                newDataSource[index]['localamount'].display = value;
                newDataSource[index]['localamount'].value = value;
                newDataSource[index].change = true;
            }
        }
        self.setState({
            dataSource: newDataSource,localamountDisabled
        },()=>{
            // if(keyParam=='amount'||keyParam=='localamountSum'||keyParam=='localamount'){
            //     self.countAmountLocalamountSum();
            // }
        });
    }
    //渲染币种参照
    renderPkCurrency(text, record, index) {
        let {querypk_currtype,currencyDisabled,dataSource}=this.state;
        //渲染币种参照
        let mybook;
        let referUrl = "uapbd/refer/pubinfo/CurrtypeGridRef/index.js";
        let stateKey = "pk_current";
        let defaultValue = {};

        defaultValue.refname = typeof text.display === 'string' ? text.display: text.display;
        defaultValue.refpk = typeof text.value === 'string' ? text.value : text.display;
        if (!this.state[stateKey]) {
            {
                createScript.call(this, referUrl, stateKey);
            }
        } else {
            mybook = this.state[stateKey] ? (
                this.state[stateKey]({
                    fieldid:"pk_currtype",
                    value: Object.keys(this.state.pk_currtype).length> 0? this.state.pk_currtype : defaultValue,
                    isMultiSelectedEnabled:false,
                    disabled:this.state.enableEdit||record.currencyDisabled,
                    queryCondition: () => {
                        return Object.assign({
                        },config)
                    },
                    onChange: value => {
                        this.onDateChange(value, index, "pk_currtype");
                        //切换币种清空原币
                        record.amount.display=''
                        record.amount.value=''
                        record.localamount.display=''
                        record.localamount.value=''
                    }
                })
            ) : (
                <div />
            );
        }
        return mybook;
    }
    VoucherTypeChange(value, index, keyParam) {
        //凭证类别参照修改方法 摘要参照修改方法
        let { dataSource } = this.state;
        let newDataSource = [...dataSource];
        if(keyParam=='explanation'){//摘要传文本
            newDataSource[index][keyParam].display = value.refname;
            newDataSource[index][keyParam].value = value.refname;
        }else{
            newDataSource[index][keyParam].display = value.refname;
            newDataSource[index][keyParam].value = value.refpk;
        }
        
        newDataSource[index].change = true;
        this.setState({
            dataSource: newDataSource
        });
    }
    //表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.selfIndex == key) {
				rt = v;
			}
		});
		return rt;
	}
    /**
     * index: 修改字段所在的行数
     * keyParam：当前修改字段的key值
     * changFn：参照修改后的回调方法
     * */
    rednerVoucherType(record,text, index, keyParam, changFn) {
        //凭证类别参照
        let VoucherType;
        let referUrl = "uapbd/refer/fiacc/VoucherTypeDefaultGridRef/index.js";
        let stateKey = "pk_voucher_type";
        let defaultValue = {};
        defaultValue.refname = text.display;
        defaultValue.refpk = text.value;
        if (!this.state[stateKey]) {
            {
                createScript.call(this, referUrl, stateKey);
            }
        } else {
            VoucherType = this.state[stateKey] ? (
                this.state[stateKey]({
                    fieldid:keyParam,
                    value: defaultValue,
                    isMultiSelectedEnabled:false,
                    queryCondition: () => {
                        return Object.assign({
                            "pk_org": this.state.queryData.pk_accountingbook
                        },config)
                    },
                    disabled: this.state.enableEdit,
                    onChange: v => {
                        changFn(v, index, keyParam);
                    }
                })
            ) : (
                <div />
            );
        }
        return VoucherType;
    }
    //渲染业务单元
    rednerpk_unit(record,text, index, keyParam) {
        let {queryCondition,dataSource,querypk_unit}=this.state;
        let newDataSource = [...dataSource];
        let VoucherType;
        let referUrl = "uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef/index.js";
        let stateKey = "pk_unit";
        let defaultValue = {};
        defaultValue.refname = text.display;
        defaultValue.refpk = text.value;
        if (!this.state[stateKey]) {
            {
                createScript.call(this, referUrl, stateKey);
            }
        } else {
            VoucherType = this.state[stateKey] ? (
                this.state[stateKey]({
                    fieldid:keyParam,
                    value: defaultValue,
                    isMultiSelectedEnabled:false,
                    queryCondition: () => {
                        return Object.assign({
                            "pk_units":querypk_unit,
                            "pk_accountingbook": this.state.queryData.pk_accountingbook,
                            "TreeRefActionExt": 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder',
                            "isDataPowerEnable": 'Y',
                            "DataPowerOperationCode" : 'fi',
                        },config)
                    },
                    disabled: this.state.enableEdit,

                    onChange: value => {              
                            newDataSource[index].pk_org=value.refpk?value.refpk:'';// liuhuit 这个有问题，不应该更新查询条件里的，应该更新当前行的pk_org
                            newDataSource[index][keyParam]={display:value.refname?value.refname:'',value:value.refpk?value.refpk:''};
                            newDataSource[index].change = true;
                            //渲染业务单元
                            text.display=value.refname?value.refname:''
                            text.value=value.refpk?value.refpk:''
                            //清空辅助核算
                            newDataSource[index]['assid']={display:'',value:'',scale:null};
                            if(newDataSource[index]['assvo']){
                                newDataSource[index]['assvo'].map((list,_index)=>{
                                    list.checkvaluename={display:'',value:'',scale:null};
                                    list.pk_checkvalue={display:'',value:'',scale:null};
                                    list.checkvaluecode={display:'',value:'',scale:null};
    
                                })
                            }
                            this.setState({
                                dataSource: newDataSource
                            });
                    }
                })
            ) : (
                <div />
            );
        }
        return VoucherType;
    }

    renderExplanation(record,text, index, keyParam, changFn) {
        //摘要参照
        let Explanation;
        let referUrl = "fipub/ref/pub/SummaryRef/index.js";
        let stateKey = "explanation_refer";
        let defaultValue = {};
        defaultValue.refname = text.display;
        defaultValue.refpk = text.value;
        if (!this.state[stateKey]) {
            //undefined
            {
                createScript.call(this, referUrl, stateKey);
            }
        } else {
            Explanation = this.state[stateKey] ? (
                this.state[stateKey]({
                    fieldid:keyParam,
                    value: defaultValue,
                    //isMultiSelectedEnabled:true,
                    disabled: this.state.enableEdit,
                    queryCondition: () => {
                        return Object.assign({
                            // pk_org: this.state.queryCondition.pk_org
                            pk_org: record.pk_org?record.pk_org:this.state.queryCondition.pk_org
                        },config)
                    },
                    onChange: v => {
                        if(v.refpk){
                            if(v.refpk.length>200){
                                let cerrentValue=v.refpk;
                                v.refname=cerrentValue.substring(0,200);
                                v.refpk=cerrentValue.substring(0,200);
                                toast({content:this.state.json['20020NAIPT-000061'],color:'danger'})/* 国际化处理： 请输入长度应小于200个字符的摘要*/
                            }   
                        }
                        changFn(v, index, keyParam);
                    }
                })
            ) : (
                <div />
            );
        }
        return Explanation;
    }
    getPkOrg(value) {
        //在选择'核算账簿'后请求'gl.glpub.queryFinanceOrg'获得账簿所属于的组织，得到pk_Org
        let self = this;
        let url = "/nccloud/gl/glpub/queryFinanceOrg.do";
        let param = {};
        param.pk_accountingbook = value;
        ajax({
            url: url,
            data: param,
            success: function(response) {
                let { data, success } = response;
                if (success) {
                    self.setState({
                        getPkOrgValue: data.pk_org
                    });
                }
            },
            error: function(error) {
                toast({ content: error, color: "warning" });
            }
        });
    }

    //查询查询
    handleQuery = () => {
        this.setState({
            isQueryShow: !this.state.isQueryShow
        });

    };
    handleChange = (keyParam, valueParam) => {//没必要这样去包装
        this.setState({
            [keyParam]: valueParam
        });   
    };

    handleAdd = () => {
        //添加数据
        this.addDataFlag = true;
        const { count,  queryData,startDateBefore,querypk_currtype} = this.state;
        let {modifyData,currencyDisabled,querycurrinfo,querybalanorient,localamountDisabled,dataSource
            ,checkedList,allChecked,activePage,oldActivePage
        }=this.state;
        if(oldActivePage==null){
            oldActivePage=activePage
        }else{
            oldActivePage=oldActivePage
        }
        //this.getColumnTotal(this.state.dataSource)
   
        this.handleChange("changeStyle", "update");
    
        this.handleChange("enableEdit", false); //false: 可以编辑
        const newSetData = {
            change: true,
            prepareddate: {
                //凭证日期
                display: getNowFormatDate(false), // moment().format('YYYY-MM-DD'),
                value: getNowFormatDate(true), // moment().format('YYYY-MM-DD HH:mm:ss'),
                scale: null
            },
            businessdate: {
                //业务日期
                display: getNowFormatDate(false), //moment().format('YYYY-MM-DD'),
                value: getNowFormatDate(true), //moment().format('YYYY-MM-DD HH:mm:ss'),
                scale: null
            },
            pk_vouchertype: {
                //凭证类别
                display: "",
                value: "",
                scale: null
            },
            voucherno: {
                //凭证号
                display: "",
                value: "",
                scale: null
            },
            pk_unit:{
                display:'',
                value:'',
                scale:null
            },
            assid: {
                display: "",
                value: "",
                scale: null
            },
            explanation: {
                display: "",
                value: "",
                scale: null
            }, //摘要
            verifyno: {
                display: "",
                value: "",
                scale: null
            }, //核销号
            pk_currtype: {
                display: "",
                value: "",
                scale: null
            }, //币种
            orientation: {
                display: this.state.json['20020NAIPT-000038'],/* 国际化处理： 借*/
                value: "1",
                scale: null
            }, //方向
            amount: {
                display: "",
                value: "",
                scale: this.state.CurrinfoScale
            }, //原币
            localamount: {
                //组织本币
                display: "",
                value: "",
                scale: this.state.CurrinfoScale
            },
            //key: _index + 1,
        };
        // let newData = deepClone(newSetData);
        let $newData=Immutable.fromJS(newSetData);
        let newData =$newData.toJS();
        newData.prepareddate.display=startDateBefore;
        newData.prepareddate.value=startDateBefore;
        newData.businessdate.display=startDateBefore;
        newData.businessdate.value=startDateBefore;
        if(querypk_currtype.value&&querypk_currtype.value!=this.state.json['20020NAIPT-000062']){//所选币种不是所有币种的话，分录行的币种为所选币种且不可编辑机/* 国际化处理： 所有币种*/
            newData.pk_currtype.display=querypk_currtype.display;
            newData.pk_currtype.value=querypk_currtype.value;
            currencyDisabled=true;

        }else{
            currencyDisabled=false;

            newData.pk_currtype.display=querycurrinfo.display;
            newData.pk_currtype.value=querycurrinfo.value;
        }     
        if(newData.pk_currtype.value==querycurrinfo.value){//所选币种为查询条件的币种
            newData.localamountDisabled=true;
        }else{
            newData.localamountDisabled=false;
        }
        if(newData.pk_currtype.value==querypk_currtype.value){//所选币种为查询条件的币种
            newData.currencyDisabled=true;
        }else{
            newData.currencyDisabled=false;
        }
        if(querybalanorient){//科目方向
            newData.orientation.display=querybalanorient=='0'?this.state.json['20020NAIPT-000038']:this.state.json['20020NAIPT-000063'];/* 国际化处理： 借,贷*/
            newData.orientation.value=querybalanorient=='0'?'1':'-1';
        }
        newData.pk_accountingbook = queryData.pk_accountingbook; //确定新增的数据属于哪个账簿的
        newData.pk_accasoa = queryData.checkSubject.refpk;
        newData.assvo = []; //新增数据的辅助核算
        if(this.state.addRefer.length!=0){
            this.state.addRefer.map((item, index) => {
                let assvoObj = {};
                assvoObj.inputlength=item.inputlength?item.inputlength:'',
                assvoObj.digits=item.digits?item.digits:'0'
                assvoObj.parentIndex = this.state.dataSource.length;
                assvoObj.selfIndex = index;
                assvoObj.classid = { display: "", value: item.classid, scale: null };
                assvoObj.checkvaluename = { display: "", value: "", scale: null };
                assvoObj.checktypecode = {display:item.code , value:item.code};
                assvoObj.checktypename = {
                    display: item.name,
                    value: item.name,
                    scale: null
                };
                assvoObj.refpath = item.refnodename;
                assvoObj.pk_checktype = {
                    display: "",
                    value: item.pk_accassitem,
                    scale: null
                };
                newData.assvo.push(assvoObj);
            });
        }
        newData.index = this.state.dataSource.length;
        newData.rowEdited=true;
        let $modifyData=Immutable.fromJS(modifyData);
        let $$modifyData =$modifyData.toJS();
        let $dataSource=Immutable.fromJS(dataSource);
        let $$dataSource =$dataSource.toJS();
        this.paginationUtils.setData([...dataSource, newData])
        this.paginationUtils.setMaxRow(this.max_row)
        let totalPages = this.paginationUtils.getTotalPage()
        let defaultPageDate = this.paginationUtils.getPageData(totalPages)
        checkedList.push(false)
        this.setState({
            modifyData: modifyData?$$modifyData:$$dataSource,
            currencyDisabled,localamountDisabled,
            dataSource: [...dataSource, newData],
            count: count + 1,
            addNewData: { ...newData },
            status:"add",
            pageData: defaultPageDate,
            activePage:totalPages,
            oldActivePage,
            checkedList
        });





        let buttonTrue=['searchBtn','outputBtn','showPrintBtn','modelBtn'];
        let buttonFalse=['showAddBtn','modifyBtn','saveBtn','checkBtn','cancelBtn']
        this.props.button.setButtonDisabled(buttonTrue, true);
        this.props.button.setButtonDisabled(buttonFalse, false);
        this.props.button.setButtonVisible(['searchBtn','showAddBtn','modifyBtn','outputBtn','showPrintBtn','checkBtn'],false);
        this.props.button.setButtonVisible(['showAddBtn','delBtn','saveBtn','cancelBtn'],true);
    };
//取消
    canAddOrMod = () =>{
        let {dataSource,modifyData,checkedList,activePage,checkBoxArr,oldActivePage} = this.state;
        if(this.addDataFlag){//点击了新增按钮
            
            dataSource.forEach((value)=>{
                for(let i=0;i<dataSource.length;i++){
                    if(dataSource[i].change && !dataSource[i].pk_verifydetail){
                        dataSource.splice(i, 1)
                    }
                }
            })
        }
        checkedList=[]
        for(let i=0;i<modifyData.length;i++){
            checkedList.push(false)

        }

        this.paginationUtils.setData(modifyData)
        this.paginationUtils.setMaxRow(this.max_row)
        let defaultPageDate = this.paginationUtils.getPageData(oldActivePage)


        this.setState({
            selectedRecord:0,//取消操作之后选中行归0
            deletearr:[],
            allChecked:this.state.allCheckedclone,
            status:"normal",
            enableEdit:true,
            dataSource:this.state.modifyData,
            checkedList,
            verifynoValue:[],
            pageData:defaultPageDate,
            checkBoxArr:[],
            activePage:oldActivePage,
            oldActivePage:null
        })
        this.addDataFlag = false;
        this.chageFlag = false;
        let buttonTrue=['saveBtn','cancelBtn','delBtn'];
        let buttonFalse=['showAddBtn','modifyBtn','searchBtn','outputBtn','checkBtn','showPrintBtn','modelBtn','insertBtn','ineMonBtn','ExInsBtn','ExOutBtn']
        this.props.button.setButtonDisabled(buttonTrue, true);
        this.props.button.setButtonDisabled(buttonFalse, false);
        this.props.button.setButtonVisible(['secInsertBtn','secReturnBtn','saveBtn','cancelBtn'],false);
        this.props.button.setButtonVisible(['searchBtn','showAddBtn','modifyBtn','delBtn','outputBtn','showPrintBtn','checkBtn'],true);
    }
    //删除
    handleDele = value => {        
        let{checkedList,status,dataSource,deletearr}=this.state;
        if(checkedList.join().split()[0].indexOf('true') == -1){
            toast({content:this.state.json['20020NAIPT-000064'],color:'warning'});/* 国际化处理： 请先选中数据再做删除操作*/
            return false;
        }
        
        if(status=="normal"){
            promptBox({
                color:'warning',
                title:this.state.json['20020NAIPT-000008'],/* 国际化处理： 删除*/
                content:this.state.json['20020NAIPT-000065'],/* 国际化处理： 您确定要删除所选数据吗？*/
                noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                beSureBtnName:this.state.json['20020NAIPT-000003'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
                cancelBtnName:this.state.json['20020NAIPT-000004'],           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
                beSureBtnClick: this.dealOperate.bind(this),   // 确定按钮点击调用函数,非必输
                cancelBtnClick: this.cancelBtnClick.bind(this)  // 取消按钮点击调用函数,非必输
            })
            this.handleChange("enableEdit", true); //false: 可编辑
        }
        else{         
            for(let i=0;i<checkedList.length;i++){
                if(checkedList[i]==true && dataSource[i].change == true){
                    dataSource.splice(i,1)
                    checkedList.splice(i,1)                   
                    i--;
                }else if(checkedList[i]==true && dataSource[i].change != true){
                    this.handleChange("changeStyle", "delete");
                    deletearr.push(dataSource[i])
                    dataSource.splice(i,1) 
                    checkedList.splice(i,1)
                   i--;
                }
            }
            this.setState({
                dataSource,
                checkedList,
                deletearr,
                selectedRecord:'0'
            })
        }
    };

    //删除确认框的 确认，取消按钮
    dealOperate=()=>{
        let {queryData,checkedList,dataSource,activePage}=this.state;
        this.handleChange("changeStyle", "delete");
        setTimeout(() => this.ajaxData(), 0);
        if(!checkedList.every((item,index,arr)=>{//checkedList值默认false，删完行禁用删除按钮
            return item==false
        })){
            let buttonFalse=['delBtn']
            this.props.button.setButtonDisabled(buttonFalse, true);
        }


        this.paginationUtils.setData(dataSource)
        this.paginationUtils.setMaxRow(this.max_row)
        let defaultPageDate = this.paginationUtils.getPageData(activePage)
        this.setState({
            pageData: defaultPageDate,
            selectedRecord:"0"
        })
    }
    cancelBtnClick=()=>{
        return false;
    }
    handleModified = value => {
        //修改
        let {dataSource,modifyData,checkedList,allCheckedclone,allChecked,oldActivePage,activePage }=this.state
        // this.handleChange("changeStyle", "update");
        // this.handleChange("enableEdit", false); //false: 可编辑
        if(oldActivePage==null){
            oldActivePage=activePage
        }else{
            oldActivePage=oldActivePage
        }
        let $modifyData=Immutable.fromJS(modifyData);
        let $$modifyData =$modifyData.toJS();
        let $dataSource=Immutable.fromJS(dataSource);
        let $$dataSource =$dataSource.toJS();
        this.setState({
            oldActivePage,
            changeStyle:'update',
            enableEdit:false,
            modifyData: modifyData?$$modifyData:$$dataSource,//存储保存之前的修改状态
            // modifyData: deepClone(this.state.dataSource),
            status:"edit"
        })
        let buttonTrue=['searchBtn','outputBtn','showPrintBtn','modelBtn'];
        let buttonFalse=['showAddBtn','modifyBtn','saveBtn','checkBtn','cancelBtn']
        this.props.button.setButtonDisabled(buttonTrue, true);
        this.props.button.setButtonDisabled(buttonFalse, false);
        this.props.button.setButtonVisible(['searchBtn','showAddBtn','modifyBtn','outputBtn','showPrintBtn','checkBtn'],false);
        this.props.button.setButtonVisible(['showAddBtn','delBtn','saveBtn','cancelBtn'],true);
    };
    //保存
    handleSave = value => { 
        let{verifynoValue,dataSource,oldActivePage}=this.state
        verifynoValue=[]
            dataSource.map((k,j)=>{
                if(dataSource[j].verifyno.value!=''){
                    verifynoValue.push(dataSource[j].verifyno.value)
                }
                
            })
        let newverifynoValue=Array.from(new Set(verifynoValue))
        if(verifynoValue.length==newverifynoValue.length){
            this.handleSavea()
        }else{
            promptBox({
                color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输              // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                content: this.state.json['20020NAIPT-000091'],           // 本页面中存在重复的核销号！请问是否仍然保存?
                beSureBtnName: this.state.json['20020NAIPT-000003'],          // 确定按钮名称, 默认为"确定",非必输
                cancelBtnName: this.state.json['20020NAIPT-000004'],         // 取消按钮名称, 默认为"取消",非必输
                beSureBtnClick: this.handleSavea,   // 确定按钮点击调用函数,非必输
            })
        }
    };
    handleSavea=()=>{
        if(this.ajaxData()){
            this.setState({
                status:"normal",
                ['enableEdit']: true,
                verifynoValue:[],
                selectedRecord:"0"
            })
            // this.handleChange("enableEdit", true); //true不可编辑
            toast({
                duration: 3,
                color: 'success',
                title: this.state.json['20020NAIPT-000066']/* 国际化处理： 保存成功*/
            })

            let buttonTrue=['cancelBtn','saveBtn','delBtn'];
            let buttonFalse=['searchBtn','showAddBtn','modifyBtn','outputBtn','checkBtn','showPrintBtn','modelBtn']
            this.props.button.setButtonDisabled(buttonTrue, true);
            this.props.button.setButtonDisabled(buttonFalse, false);
            this.props.button.setButtonVisible(['secInsertBtn','secReturnBtn','saveBtn','cancelBtn'],false);
            this.props.button.setButtonVisible(['searchBtn','showAddBtn','modifyBtn','delBtn','outputBtn','showPrintBtn','checkBtn'],true);
        }
    }
    ajaxData = param => {
        //根据传入的类型，提交数据 updtate:更新保存；insert：新增数据；delete：删除数据
        let {
            dataSource,
            changeStyle,
            showLeadIn,
            allChecked,
            checkedList,
            leadInDatasource,
            leadCheckList,
            deletearr,
            status,
            resourceQueryReultParam,
            resourceQueryData
        } = this.state;
        let url = "/nccloud/gl/verify/verifyinitsave.do";
        let style = changeStyle;
        let resultData = [];
        let insertData = [];
        if(status=="normal"){//浏览态操作数据
            if (!showLeadIn) {
                if (style === "update" || style === "insert") {
                    dataSource.map((itemObj, index) => {
                        if (itemObj.change && itemObj.pk_verifydetail) {
                            //修改的
                            itemObj.rowno = String(index + 1);
                            resultData.push(itemObj);
                        } else if (itemObj.change && !itemObj.pk_verifydetail) {
                            //新增的
                            itemObj.rowno = String(index + 1);
                            insertData.push(itemObj);
                        }
                    });
                } else if (style === "delete") {
                    if (!checkedList.every((item, index, array)=>{
                        return item==true
                    })) {
                    checkedList.map((item, index) => {
                        if (item) {
                            dataSource[index].rowno = String(index + 1);
                            resultData.push(dataSource[index]);
                        }
                    });
                } else {
                    //全删除
                        dataSource.map((item, index) => {
                            item.rowno = String(index + 1);
                            resultData.push(item);
                        });
                    }
                }
            } else {
                leadInDatasource.map(function(cellItem, ind) {
                    if (leadCheckList[ind]) {
                        resultData.push(cellItem);
                    }
                });
            }

            let dataParam = {};
            if (!showLeadIn) {
                if (style === "update") {
                    if (resultData.length > 0) {
                        dataParam[style] = resultData;
                    }
                } else if (style === "delete") {
                    dataParam[style] = resultData;
                }
                if (insertData.length > 0) {
                    dataParam["insert"] = resultData;
                }
            } else {
                dataParam["insert"] = resultData;
            }
            let resultDataLen=resultData.length;
            let self = this;
            let successFlag=false;
            ajax({
                url,
                data: dataParam,
                async:false,
                success: function(response) {
                    successFlag=true;
                    if (self.state.showLeadIn) {
                        self.handleChange("showLeadIn", param);
                        if(self.state.showLeadIn){
                            let len = resultDataLen;
                            toast({
                                duration: 3,
                                color: 'success',
                                title: self.state.json['20020NAIPT-000067'],/* 国际化处理： 导入成功*/
                                content: `${self.state.inlt&&self.state.inlt.get('20020NAIPT-000075',{count:len})}`/* 国际化处理： 共导入,条数据*/
                            })
                        }
                        // self.leadInFn(false,true);
                        self.handleQueryClick(resourceQueryData, resourceQueryReultParam)

                    } else {
                        self.leadInFn(false);
                        self.queryDate(self.state.queryData, "sureBtn");
                    }
                    let buttonTrue=['cancelBtn','saveBtn'];
                    let buttonFalse=['searchBtn','showAddBtn','modifyBtn','outputBtn','checkBtn','showPrintBtn','modelBtn']
                    self.props.button.setButtonDisabled(buttonTrue, true);
                    self.props.button.setButtonDisabled(buttonFalse, false);
                    self.props.button.setButtonVisible(['secInsertBtn','secReturnBtn','saveBtn','cancelBtn'],false);
                    self.props.button.setButtonVisible(['searchBtn','showAddBtn','modifyBtn','delBtn','outputBtn','showPrintBtn','checkBtn'],true);
                },
                error: function(error) {
                    successFlag=false;
                    toast({ content: error.message, color: "warning" });
                }
            });

            return  successFlag;
        }else{//编辑态操作数据
        dataSource.map((itemObj, index) => {
            if (itemObj.change && itemObj.pk_verifydetail) {
                //修改的
                itemObj.rowno = String(index + 1);
                resultData.push(itemObj);
            } else if (itemObj.change && !itemObj.pk_verifydetail) {
                //新增的
                itemObj.rowno = String(index + 1);
                insertData.push(itemObj);
            }
        });
        let dataParam = {};
            if(insertData){
                dataParam["insert"] = insertData
            }
            if(resultData){
                dataParam["update"] = resultData
            }
            if(deletearr){
                dataParam["delete"] = deletearr
            }
        let resultDataLen=resultData.length;
        let self = this;
        let successFlag=false;
        ajax({
            url,
            data: dataParam,
            async:false,
            success: function(response) {
                successFlag=true;
                if (self.state.showLeadIn) {
                    self.handleChange("showLeadIn", param);
                    if(self.state.showLeadIn){
                        let len = resultDataLen;
                        toast({
                            duration: 3,
                            color: 'success',
                            title: self.state.json['20020NAIPT-000067'],/* 国际化处理： 导入成功*/
                            content: `${self.state.inlt&&self.state.inlt.get('20020NAIPT-000075',{count:len})}`/* 国际化处理： 共导入,条数据*/
                        })
                    }                  
                    self.leadInFn(false);
                   
                } else {              
                   // self.leadInFn(false);
                    self.queryDate(self.state.queryData, "sureBtn");
                }
                let buttonTrue=['cancelBtn','saveBtn'];
                let buttonFalse=['searchBtn','showAddBtn','modifyBtn','outputBtn','checkBtn','showPrintBtn','modelBtn']
                self.props.button.setButtonDisabled(buttonTrue, true);
                self.props.button.setButtonDisabled(buttonFalse, false);
                self.props.button.setButtonVisible(['secInsertBtn','secReturnBtn','saveBtn','cancelBtn'],false);
                self.props.button.setButtonVisible(['searchBtn','showAddBtn','modifyBtn','delBtn','outputBtn','showPrintBtn','checkBtn'],true);
            },
            error: function(error) {
                successFlag=false;
                toast({ content: error.message, color: "warning" });
            }
        });

        return  successFlag;
        }
    };

    handleCheck = value => {
        //检查函数
        this.props.modal.show("enableModal");
        this.checkBusColumn();
        let flag=false
        this.getVerifyCheckData(this.checkBusColumn(),flag);
    };
    //打印
    showPrintModal() {
        this.setState({
            showPrintModal: true
        })
    }
    handlePrint(data) {
        let printUrl = '/nccloud/gl/verify/verifybeginprint.do'
        let { queryCondition, appcode, queryData,debite,credit} = this.state
        let { ctemplate, nodekey } = data
        queryCondition.queryvo = data
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey,
            showPrintModal: false,
        })
        let listItem = {
            pk_accountingName: queryData.pk_accountingName ? queryData.pk_accountingName : "",
            verObj: queryData.checkSubject ? queryData.checkSubject.name : "",
            asscontent: queryData.asscontent ? queryData.asscontent : "",
            begindate: queryData.begindate ? queryData.begindate : ""
        };
        let verifysubj=listItem.verObj
        let asscontent=listItem.asscontent
        let begindate=listItem.begindate
        queryCondition.totalamount=this.state.amountSum
        queryCondition.totallocalamount=this.state.localamountSum
        queryCondition.verifysubj=listItem.verObj
        queryCondition.asscontent=listItem.asscontent
        queryCondition.begindate=listItem.begindate
        printRequire(printUrl, appcode, nodekey, ctemplate, queryCondition)
    }
    showOutputModal() {
        let outputUrl = '/nccloud/gl/verify/verifybeginoutput.do'
        let { appcode, nodekey, ctemplate, queryCondition,queryData } = this.state
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
        let listItem = {
            pk_accountingName: queryData.pk_accountingName ? queryData.pk_accountingName : "",
            verObj: queryData.checkSubject ? queryData.checkSubject.name : "",
            asscontent: queryData.asscontent ? queryData.asscontent : "",
            begindate: queryData.begindate ? queryData.begindate : ""
        };
        queryCondition.totalamount=this.state.amountSum
        queryCondition.totallocalamount=this.state.localamountSum
        queryCondition.verifysubj=listItem.verObj
        queryCondition.asscontent=listItem.asscontent
        queryCondition.begindate=listItem.begindate    
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, queryCondition)
    }
    handleOutput() {
    }
    cancelBtnClick=()=>{
      return false;
    }
    innerAjax = () => {
        let {startDateBefore,queryData,useflag}=this.state;
        let { pk_accountingbook, pk_accasoa } = queryData;
        let urlNew = "/nccloud/gl/verify/verifyobjinfo.do";
        let dataNew = {};
        dataNew.pk_accountingbook = pk_accountingbook;
        dataNew.pk_accasoa = pk_accasoa;
        ajax({
            url:urlNew,
            data:dataNew,
            success: response => {
                useflag=response.data.verifyobj.header.m_userflag;//核销科目是否启用
                startDateBefore=response.data.startDateBefore;
                if(useflag===true){
                    this.setState({
                        startDateBefore,
                        useflag
                    },()=>{
                        let buttonTrue=['showAddBtn','modifyBtn','saveBtn','outputBtn','cancelBtn','insertBtn','ineMonBtn','ExInsBtn','ExOutBtn'];
                        let buttonFalse=['searchBtn','checkBtn','showPrintBtn','modelBtn']
                        this.props.button.setButtonDisabled(buttonTrue, true);
                        this.props.button.setButtonDisabled(buttonFalse, false);
                    });
                }else if(useflag===false){
                    this.setState({
                        startDateBefore,
                        useflag
                    },()=>{
                        let buttonTrue=['saveBtn','cancelBtn'];
                        let buttonFalse=['showAddBtn','modifyBtn','searchBtn','outputBtn','checkBtn','showPrintBtn','modelBtn',		'insertBtn',
                        'ineMonBtn',
                        'ExInsBtn',
                        'ExOutBtn']
                        this.props.button.setButtonDisabled(buttonTrue, true);
                        this.props.button.setButtonDisabled(buttonFalse, false);
                    });

                }
            },
            error: error => {
                toast({ content: error.content, color: "warning" });
            }
        });

    }
    //查询确定按钮
    handleQueryClick = (data, reultParam) => {
        let {querypk_currtype,querybalanorient,querycurrinfo,isShowUnit,querypk_unit,CurrinfoScale,resourceQueryData,resourceQueryReultParam}=this.state;
        if(!reultParam || data.pk_accountingbook.display=="" || data.pk_currency.display==""){
            toast({content:this.state.json['20020NAIPT-000068'],color:"warning"})/* 国际化处理： *为必选项*/
        }else{
            let $data=Immutable.fromJS(data);
            let $$data =$data.toJS();
            let $reultParam=Immutable.fromJS(reultParam);
            let $$reultParam =$reultParam.toJS();
            resourceQueryReultParam=$$reultParam; 
            resourceQueryData=$$data;
            let childData = $$data;
            let assArr = [];
            if (childData.ass.length > 0) {
                childData.ass.map(item => {
                    let assObj = {};
                    assObj.pk_checktype = item.pk_Checktype;
                    assObj.pk_checkvalue = item.pk_Checkvalue;
                    assArr.push(assObj);
                });
            }
            let pkUnit = [];
            childData.pk_units.map(item => {
                pkUnit.push(item.value);
            });
            let resArr =
                pkUnit.length > 1 || (pkUnit.length === 1 && pkUnit[0] !== "")
                    ? pkUnit
                    : null;
            let param = {};
            //存储查询条件的币种信息
            querypk_currtype=childData.pk_currency;//查询条件币种
            querybalanorient=childData.balanorient;//查询条件科目方向
            querycurrinfo=childData.currinfo;//查询条件的组织本币
            querypk_unit=pkUnit.join();//查询条件里所选的业务单元 value值数组
            isShowUnit=childData.isShowUnit;
            CurrinfoScale=childData.CurrinfoScale;
            param.pk_org=childData.pk_org;
            param.pk_accountingName = childData.pk_accountingbook.display;
            param.pk_accountingbook = childData.pk_accountingbook.value;
            param.pk_unit = resArr;
            param.pk_accasoa = reultParam.refpk;
            param.pk_currtype = childData.pk_currency.value;
            param.asscontent=childData.asscontent;
            param.assvo = assArr;
            let checkSubject={name:reultParam.name,refpk:reultParam.refpk};
            // 拿取启动日期
            let {begindate} = getVerifyInitHeadTitle(param);
            this.setState(
                {
                    resourceQueryData,
                    resourceQueryReultParam,
                    queryData: { begindate, ...param,checkSubject },
                    // checkSubject: { ...reultParam },
                    querypk_currtype,querybalanorient,querycurrinfo,isShowUnit,querypk_unit,CurrinfoScale
                },
                () => {
                    this.queryDate(param, "sureBtn",childData);
                }
            );
            
        }
    };

    /**
     * param: 查询条件,
     * sourceBtn: 点击确定按钮查询获取数据
     * */
    queryDate(param, sourceBtn,childData) {
        //查询数据
        let self = this;
        let {
            listItem,
            columnsVerify,
            verifyBalancesData,dataSource,
            queryCondition,querycurrinfo,querypk_currtype,
            isQueryShow,modifyData,localamountSum,amountSum
        } = this.state;
        queryCondition=param;
        dataSource=[];
        let url = "/nccloud/gl/verify/verifyinitquery.do";
        ajax({
            url: url,
            data: param,
            success: function(response) {
                
                let { data, success } = response;
                if (success) {
                    let { checkedList } = self.state;
                    localamountSum=data.localamount;
                    amountSum=data.amount;
                    let resData=data.data;
                    checkedList=[];
                    resData.map((cellObj, index) => {
                        cellObj.key=index;
                        //设置每行的编辑性
                        cellObj.rowEdited = false;
                        //判断每一行币种与所选本币是否相同，相同的话所选本币不可编辑跟着原币联动
                        if(cellObj.pk_currtype.value==querypk_currtype.value){
                            cellObj.currencyDisabled=true;
                        }else{
                            cellObj.currencyDisabled=false;
                        }
                        if(cellObj.pk_currtype.value==querycurrinfo.value){
                            cellObj.localamountDisabled=true;
                        }else{
                            cellObj.localamountDisabled=false;
                        }
                        cellObj.change = false;
                        cellObj.index = index;
                        cellObj.rowno = index + "";
                        cellObj.assvo &&
                        cellObj.assvo.map((item, ind) => {
                            item.parentIndex = index;
                            item.selfIndex = ind;
                        });
                        checkedList.push(false);
                    });
                    if (sourceBtn === "sureBtn") {
                        isQueryShow=false;
                        dataSource=[...resData];
                        modifyData=resData;
                    } else {
                            dataSource=[...resData, this.state.addNewData];
                    }

                    self.paginationUtils.setData(dataSource)
					self.paginationUtils.setMaxRow(self.max_row)
                    let defaultPageDate = self.paginationUtils.getPageData(1)               

                    self.setState({
                        pageData: defaultPageDate,
                        isQueryShow,
                        dataSource,
                        modifyData,
                        checkedList,
                        allChecked:false,
                        showAddBtn: false, //false："新增"按钮可用
                        listItem,
                        queryCondition,
                        columnsVerify,
                        verifyBalancesData,
                        localamountSum,
                        amountSum,
                        activePage:1

                    },()=>{
                       // self.countAmountLocalamountSum();//计算合计
                        self.innerAjax();//
                    })
                }
            },
            error: function(error) {
                toast({ content: error.message, color: "warning" });
                self.setState({
                    isQueryShow: false,
                    dataSource
                });
            }
        });
    }

    //处理width
    changeWidth(arr) {
        arr.map((item, index) => {
            if (item.children) {
                this.changeWidth(item.children);
            } else {
                item["width"] = 100;
            }
        });
        return arr;
    }
    // 获取表格搜索项目listItem
    getHeadData = () => {
        let { queryData } = this.state;
        let listItem = {
           // verObj: queryData.pk_accountingName ? queryData.pk_accountingName : "",
            verObj: queryData.checkSubject ? queryData.checkSubject.name : "",
            asscontent:queryData.asscontent ? queryData.asscontent : "",
            begindate: queryData.begindate ? queryData.begindate : ""
        };
        let verifysubj=listItem.verObj;
        let asscontent=listItem.asscontent;
        let begindate=listItem.begindate;
        const headData = [
            // {itemName:'核算账簿:',itemType:'textInput',itemKey:'accountingbookValue'},
            { itemName: this.state.json['20020NAIPT-000022'], itemType: "textInput", itemKey: "verObj" },/* 国际化处理： 核销对象科目:*/
            { itemName: this.state.json['20020NAIPT-000023'], itemType: "textInput", itemKey: "asscontent" },/* 国际化处理： 辅助核算:*/
            { itemName: this.state.json['20020NAIPT-000024'], itemType: "textInput", itemKey: "begindate" }/* 国际化处理： 启动日期:*/
        ];
        return headData.map((item, i) => {
            let defValue = listItem[item.itemKey];
            switch (item.itemType) {
                case "textInput":
                    return (
                        <FormItem
                            inline={true}
                            //showMast={true}
                            labelXs={1}
                            labelSm={1}
                            labelMd={1}
                            xs={2}
                            md={2}
                            sm={2}
                            labelName={item.itemName}
                            isRequire={true}
                            method="change"
                        >
                        <Tooltip trigger="hover" placement="top" inverse overlay={defValue}>
                            <span>
                                <InputItem
                                    fieldid={item.itemKey}
                                    //isViewMode
                                    disabled={true}
                                    readOnly
                                    name={item.itemKey}
                                    type="customer"
                                    defaultValue={defValue}
                                />
                            </span>
                        </Tooltip>
                        </FormItem>
                    );
                default:
                    break;
            }
        });
    };
    //选中某一行
    getRow = (expanded, record) => {
        let that=this;
        let { getrowkey, selectedRecord, selectedData,dataSource,status } = that.state;
        if(status=="normal"){
            return false;
        }
        if((selectedRecord!=''&&dataSource[selectedRecord]!=undefined) || selectedRecord==0){
            dataSource[selectedRecord].rowEdited=false;
        }
        dataSource[record].rowEdited=true;
        that.setState({
            selectedRecord: record,
            dataSource
        });
    };
    tableAssistCheckconfirm() {
        //表中的辅助核算"确认"按钮
        this.getClassId();
        this.handleChange("AssistCheckShow", false);
    }
    /**
     * param:控制显示导入页面；还是查询页面；false:查询页面
     * */
    leadInBackFn = param => {
        //点击导入页面的"导入"按钮
        this.ajaxData(param);
    };
    leadInBalanceFn = param => {
        //点击导入页面的"导入期初余额"按钮
        let { queryData } = this.state;
        let { pk_accountingbook, pk_unit, pk_accasoa } = this.state.queryData;
        let url = "/nccloud/gl/verify/verifyinitimportqc.do";
        let data = {};
        data.pk_accountingbook = pk_accountingbook;
        data.pk_unit = pk_unit;
        data.pk_accasoa = pk_accasoa;
        ajax({
            url,
            data,
            success: response => {
                this.queryDate(queryData, "sureBtn");
                toast({
                    duration: 3,          // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
                    color: 'success',     // 提示类别，默认是 "success",非必输
                    title: this.state.json['20020NAIPT-000069'],      // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 已成功*/
                    content: this.state.json['20020NAIPT-000070'],   // 提示内容，批量操作要输入,非必输/* 国际化处理： 期初余额数据已导入成功！请勿重复导入*/
                })
            },
            error: error => {
                toast({ content: error.content, color: "warning" });
            }
        });
    };
    leadInFn=(param,flag)=>{//导入按钮组里的导入，导入页面得返回
        //点击下拉按钮中'导入'按钮触发事件；
        let {dataSource,leadInDatasource,leadCheckList}=this.state;
        this.handleChange("showLeadIn", param);
        if(!flag){
            this.getLeadInData();
        }else{
            leadCheckList.map((item,index)=>{
                if(item){
                    dataSource.push(leadInDatasource[index])
                }
            })
            this.setState({
                dataSource,
                leadInDatasource: [],
                leadCheckList: [],
                leadInAllChecked:false
            });
        }
        if(param){
            this.props.button.setButtonVisible(["showAddBtn","delBtn","searchBtn","modifyBtn","saveBtn","outputBtn","checkBtn","showPrintBtn","modelBtn","cancelBtn"], false);
            this.props.button.setButtonVisible(['secInsertBtn','secReturnBtn'],true);
        }else{
            this.props.button.setButtonVisible(["showAddBtn","delBtn","searchBtn","modifyBtn","outputBtn","checkBtn","showPrintBtn","modelBtn"], true);
            this.props.button.setButtonVisible(['secInsertBtn','secReturnBtn'],false);
        }
    }
    getLeadInData=()=> {
        //获取导入的数据
        let self = this;
        let url = "/nccloud/gl/verify/verifyinitfromvoucher.do";
        let data = { ...self.state.queryData };
        ajax({
            url,
            data,
            success: function(response) {
                let { data, success } = response;
                let { leadCheckList,leadInAllChecked } = self.state;

                data.map((item, index) => {
                    item.key=index*10;
                    item.rowno = String(index + 1);
                    leadCheckList[index] = true;
                });
                leadInAllChecked=true;
                let $data=Immutable.fromJS(data);
                let $$data =$data.toJS();
                let responseData = $$data;
                if (success) {
                    self.setState({
                        leadInDatasource: responseData,
                        leadCheckList: [...leadCheckList],
                        leadInAllChecked
                    });
                }
            },
            error: function(error) {
                toast({ content: error.content, color: "error" });
            }
        });
    }
    //启用检查columns
    createCheckColumns = columns => {
        let { showBusColumn } = this.state;
        let newColumns = JSON.parse(JSON.stringify(columns))
        newColumns.map((list,index)=>{
            if(showBusColumn==undefined && list.key == 'unitname'){
                newColumns.splice(index,1)
            }
        })
        return newColumns
    };
    formatData = data => {
        let result = [];
        data &&
        data.map((item, index) => {
            item.rowKey = index;
            result.push(item);
        });
        return result;
    };
    //启用检查ajax
    checkBusColumn = () => {
        //检查是否显示业务单元
        ajax({
            url: "/nccloud/gl/verify/verifyobjcheckbu.do",
            data: { pk_accountingbook: this.state.queryData.pk_accountingbook },
            success: res => {
                let { success, data } = res;
                if (success) {
                    if (data === "Y") {
                        this.setState({
                            showBusColumn: true
                        });
                    }else{
                        this.setState({
                            showBusColumn: undefined
                        });
                    }
                }
            },
            error: function(res) {
                toast({ content: res.message, color: "danger" });
            }
        });
    };
    getVerifyCheckData = (callback,flag) => {
        let param = {
            pk_accountingbook: this.state.queryData.pk_accountingbook,
            pk_accasoa: this.state.queryData.pk_accasoa,
            init: 'Y'
        };

        ajax({
            url: "/nccloud/gl/verify/verifyobjcheck.do",
            data: param,
            success: res => {
                let { success, data } = res;
                let { checkTableData } = this.state;
                if (success) {
                    if (data.checkvos) {
                        checkTableData = data.checkvos;
                    } else {
                        checkTableData = [];
                    }
                    this.setState(
                        {
                            checkTableData,
                            canenable: data.canenable
                        },
                        () => {
                            callback && callback(data);
                        }
                    );
                }
                if(flag==undefined){
                    toast({ content: this.state.json['20020NAIPT-000071'], color: "success" });/* 国际化处理： 刷新成功*/
                }
               
                
            },
            error: function(res) {
                toast({ content: this.state.json['20020NAIPT-000072'], color: "warning" });/* 国际化处理： 刷新失败*/
            }
        });
    };
    /**
     * 导入
     */
    openImportModal = () =>{
        let page = this;
        let {importModalConfig} = page.state;
        importModalConfig.show = true;
        page.setState({importModalConfig});
    }

    generateImportData = () => {
        let page = this;
        let {queryData} = page.state;
        let data = {
            pk_accountingbook:queryData.pk_accountingbook,
            pk_accasoa:queryData.pk_accasoa,
            pk_unit:queryData.pk_unit?queryData.pk_unit:[]
        };
        return data;
    }

    importCallBack = () => {
        let page = this;
        let {importModalConfig} = page.state;
        importModalConfig.show = false;
        page.setState({importModalConfig});
        page.queryDate(page.state.queryData, "sureBtn");
    }

    /**
     * 导出
     */
    openExportModal = () => {
        let page = this;
        let {exportModalConfig} = page.state;
        exportModalConfig.show = true;
        page.setState({exportModalConfig});
    }

    generateExportData = () => {
        let page = this;
        let {queryData, dataSource} = page.state;
        let data = {
            pk_accountingbook:queryData.pk_accountingbook,
            pk_accasoa: queryData.pk_accasoa,
            assvo: queryData.assvo,
            pk_unit: queryData.pk_unit ? queryData.pk_unit : [],
            data: dataSource
        }
        return data;
    }

    exportCallBack = () => {
        let page = this;
        let {exportModalConfig} = page.state;
        exportModalConfig.show = false;
        page.setState({exportModalConfig});
    }
 //获取默认会计期间 会计期间方案
 getDefaultYearmouth=(pk_accountingbook,modalDefaultValue)=>{
    let self=this;
    let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
    let pk_accpont = {"pk_accountingbook":pk_accountingbook}
    ajax({
        url:url,
        data:pk_accpont,
        async:false,
        success: function(response){
            const { success } = response;
            //渲染已有账表数据遮罩
            if (success) {
                if(response.data){
                    modalDefaultValue.pk_org = response.data.unit.value;
                    modalDefaultValue.isShowUnit=response.data.isShowUnit;
                    modalDefaultValue.currinfo=response.data.currinfo;//组织本币
                    modalDefaultValue.CurrinfoScale=response.data.scale;//精度
                }
                self.setState({
                    modalDefaultValue
                })
            }   
        }
    });
  }
    componentDidMount(){
        let {modalDefaultValue,}=this.state;
        let appcode=this.props.getSearchParam("c");
        getDefaultAccountBook(appcode).then((defaultAccouontBook)=>{
            if(defaultAccouontBook.value){
                modalDefaultValue.pk_accountingbook=defaultAccouontBook;
                this.getDefaultYearmouth(defaultAccouontBook.value,modalDefaultValue);
            }
        })
    }
    shouldComponentUpdate(nextProps,nextState){
        return true;
    }
    //计算合计
    countAmountLocalamountSum=()=>{
        let{dataSource,debite,credit,amdebite,amcredit,querypk_currtype,queryCondition}=this.state;
            debite=0;
            credit=0;
            for(let i=0;i<dataSource.length;i++){
                if(dataSource[i].orientation.value=="1"){                  
                    debite = accAdd(debite,dataSource[i].localamount.value)                               
                }else if(dataSource[i].orientation.value=="-1"){                     
                    credit=accAdd(credit,dataSource[i].localamount.value)                                                                     
                }
            }
            amdebite=0;
            amcredit=0;
            if(querypk_currtype.display!="所有币种"){
                for(let i=0;i<dataSource.length;i++){
                    if(dataSource[i].orientation.value=="1"){                  
                       amdebite = accAdd(amdebite,dataSource[i].amount.value)          
                    }else if(dataSource[i].orientation.value=="-1"){                  
                        amcredit = accAdd(amcredit,dataSource[i].amount.value)                                                           
                    }
                }
            }
            if(debite-credit==0){//平
                queryCondition.totalorient="0"
            }else if(debite-credit>0){//借
                queryCondition.totalorient="1"
            }else if(debite-credit<0){//贷
                queryCondition.totalorient="2"
            }        
        this.setState({
            localamountSum:Math.abs(debite-credit),
            amountSum:Math.abs(amdebite-amcredit)
        })
    }
   

    /**
     * 组织辅助核算数据，设置其为显示状态
     */
    showAssidModal = (index, record) =>{
        let page = this;
        let {assidModalState, queryCondition, startDateBefore,dataSource} = page.state;
        assidModalState.show = true;
        let pretentAssData = {};
        pretentAssData.pk_accountingbook = queryCondition.pk_accountingbook;
        let pk_org = record && record.pk_unit && record.pk_unit.value;
        pretentAssData.pk_org = pk_org || queryCondition.pk_org;
        pretentAssData.pk_accasoa = queryCondition.pk_accasoa;
        pretentAssData.prepareddate = startDateBefore;
        pretentAssData.linenum = index;
        pretentAssData.checkboxShow = false;

        pretentAssData.assData = page.generateAssDatas(record);

        assidModalState.defaultvalue = generatePretentAssData(pretentAssData);
        assidModalState.callback = (data) =>{
            //TODO 更新行数据
            record.assid.value = data.assid;
            record.assid.display = data.assname;
            this.updateAssValue(record, data);
            assidModalState.show = false;
            page.setState({assidModalState});
        }
        page.setState({assidModalState,dataSource});
    }

    generateAssDatas(record){
        let assDatas = [];
        record.assvo.map((item, index) => {
            let data = {};
			data.key = index;
            data.checktypename = item.checktypename&&item.checktypename.value;
            data.checktypecode = item.checktypecode&&item.checktypecode.value;
			data.pk_Checktype = item.pk_checktype.value;
			data.refnodename = item.refpath;
			data.refCode = item.refpath;
			data.m_classid = item.classid.value;
			data.classid = item.classid.value;
			data.pk_accassitem = item.pk_checktype.value;
            data.pk_defdoclist = item.classid.value;
            data.pk_Checkvalue = item.pk_checkvalue&&item.pk_checkvalue.value;
            data.checkvaluecode = item.checkvaluecode&&item.checkvaluecode.value;
            data.checkvaluename = item.checkvaluename&&item.checkvaluename.value;
            data.inputlength = item.inputlength;
            data.digits = item.digits;
			assDatas.push(data);
        })
        return assDatas;
    }

    updateAssValue(record, data){
        if(record.assvo && data.data){
            record.assvo.map((vo) => {
                data.data.map((item) => {
                    if(vo.pk_checktype.value == item.pk_Checktype){
                        vo.checkvaluecode = {display:item.checkvaluecode, value:item.checkvaluecode};
                        vo.checkvaluename = {display:item.checkvaluename, value:item.checkvaluename};
                        vo.pk_checkvalue = {value:item.pk_Checkvalue};
                    }
                });
            });
        }
    }

    render() {
        let totalPages = this.paginationUtils.getTotalPage()
        let { modal } = this.props;
        let { createModal } = modal;
        let { columnsVerify, listItem, importModalConfig, exportModalConfig, modalDefaultValue,
            amountSum,
            localamountSum,showLeadIn,queryData,json,checkedList,selectedRecord,allChecked, assidModalState,isShowUnit} = this.state;
        // let columns = this.renderCheckRow(this.state,json);
        if(!isShowUnit){//不显示业务单元
            this.columns.map((list,index)=>{
                if(list.key=='pk_unit'){
                    this.columns.splice(index,1);
                }
            })
        }
        let newColumns = this.renderCheckRowtwo();
        let renderDataSource = !this.state.showLeadIn
            ? this.state.dataSource
            : this.state.leadInDatasource; //是渲染查询的数据还是导入的数据

        if (columnsVerify.length == 0) {
            columnsVerify = this.Defaultcolumns;
        }
        const headData = [
            // {itemName:'核算账簿:',itemType:'textInput',itemKey:'accountingbookValue'},
            { itemName: this.state.json['20020NAIPT-000022'], itemType: "textInput", itemKey: "verObj" },/* 国际化处理： 核销对象科目:*/
            { itemName: this.state.json['20020NAIPT-000023'], itemType: "textInput", itemKey: "asscontent" },/* 国际化处理： 辅助核算:*/
            { itemName: this.state.json['20020NAIPT-000024'], itemType: "textInput", itemKey: "begindate" }/* 国际化处理： 启动日期:*/
        ];
        let config={
            pageData:renderDataSource,
            selectedRecord:selectedRecord,
            allChecked:allChecked,
            checkedList:checkedList,
            columns:this.columns?this.columns:[],
            getRow:this.getRow,
            renderDataSource:renderDataSource,
            getTableHeight:getTableHeight(170),
            // saveRenderDataSource:this.saveRenderDataSource
        }
        let tableWidth=this.refs.tableArea ? document.getElementById('tableArea').offsetWidth:'98%';
        return (
            <div className="verifyBalances nc-bill-list">
                <HeaderArea
                    title={this.state.json['20020NAIPT-000077']}/* 国际化处理： 总账往来期初*/

                    btnContent={
                        this.props.button.createButtonApp({
                            area: 'header_buttons',
                            onButtonClick: buttonReg.bind(this),
                        })
                    }
                />        
                {this.state.json['20020NAIPT-000013']&&createModal("enableModal", {
                    className:"checkboxzaa",
                    size:'lg',
                    title: this.state.json['20020NAIPT-000013'], // 弹框表头信息/* 国际化处理： 检查*/
                    content: (
                        <div fieldid="check_table">
                        <Table
                            id="checkboxzv"
                            columns={newColumns}
                            data={this.formatData(this.state.checkTableData)}
                            rowKey="rowKey"
                            scroll={{
                                x:true,
                                y:400
                              }}
                        />
                        </div>
                    ), //弹框内容，可以是字符串或dom
                    userControl: true, // 点确定按钮后，不自动关闭模态框
                    leftBtnName: this.state.json['20020NAIPT-000073'], //左侧按钮名称， 默认确认/* 国际化处理： 刷新*/
                    rightBtnName: this.state.json['20020NAIPT-000074'], //右侧按钮名称,默认关闭/* 国际化处理： 关闭*/
                    beSureBtnClick: this.getVerifyCheckData, //点击确定按钮事件
                    closeModalEve: () => {
                        if (this.state.canenable === "Y") this.verifyEnable();
                    }, //关闭按钮事件回调
                    cancelBtnClick: () => {
                        this.props.modal.close("enableModal");
                        if (this.state.canenable === "Y") this.verifyEnable();
                    }
                })}
                
                <QueryModal
                    modalDefaultValue={this.state.modalDefaultValue}
                    loadData={this.loadQuery}
                    showOrHide={this.state.isQueryShow}
                    onConfirm={this.handleQueryClick.bind(this)}
                    handleClose={this.handleQuery.bind(this)}
                    // getPkOrg={this.getPkOrg}
                    getNewRefer={this.getNewRefer.bind(this)}
                />
                {/* //表头 */}
                {!this.state.showLeadIn ? (
                    <div className="nc-bill-search-area nc-theme-form-label-c nc-theme-area-bgc">
                    <div fieldid="verifybegin_form-area">
                        <NCForm
                            useRow={true}
                            submitAreaClassName="classArea"
                            showSubmit={false}
                        >
                            {this.getHeadData(listItem)}
                        </NCForm>
                    </div>
                    </div>
                ) : (
                    <div />
                )}

                {/* //表体 */}
                <div ref="tableArea" id="tableArea">
                    {getTableData(config)}
                </div>
                <AssidModal
					pretentAssData={assidModalState.defaultvalue}
					showOrHide={assidModalState.show}
					onConfirm={assidModalState.callback}
					showDisableData={'no'}
					handleClose={() => {
						assidModalState.show = false;
						this.setState({
							assidModalState
						});
					}}
				/>

                {/* {this.state.showPrintModal? */}
                    <div>
                        <PrintModal
                            noRadio={true}
                            noCheckBox={true}
                            appcode={this.state.appcode}
                            visible={this.state.showPrintModal}
                            handlePrint={this.handlePrint.bind(this)}
                            handleCancel={() => {
                                this.setState({
                                    showPrintModal: false
                                })
                            }}
                        />
                        <PrintOutput
                            ref='printOutput'
                            url='/nccloud/gl/verify/verbalanceoutput.do'
                            data={this.state.outputData}
                            callback={this.handleOutput.bind(this)}
                        />
                    </div>
                {/* :<div/>} */}
                {/* {exportModalConfig.show? */}
                    <div>
                        <ImportModal
                            className = "importModalConfig"
                            itemsData = {importModalConfig.itemsData}
                            url = '/nccloud/gl/verify/verifyinitimportexcel.do'
                            data = {this.generateImportData()}
                            visible={importModalConfig.show}
                            handleDone={this.importCallBack}
                            handleCancel={() => {
                                importModalConfig.show = false;
                                this.setState({
                                    importModalConfig
                                })
                            }}
                        />
                        <ExportModal
                            itemsData = {exportModalConfig.itemsData}
                            url = '/nccloud/gl/verify/verifyinitexportexcel.do'
                            data = {this.generateExportData()}
                            visible={exportModalConfig.show}
                            handleDone={this.exportCallBack}
                            handleCancel={() => {
                                exportModalConfig.show = false;
                                this.setState({
                                    exportModalConfig
                                })
                            }}
                        />
                    </div>
                {/* :<div/>} */}
                {!showLeadIn ?
                    <NCDiv areaCode={NCDiv.config.TotalArea}>
                        <div className="sunBottom nc-bill-search-area nc-theme-area-bgc" fieldid="bottom-area" style={{width:tableWidth}}>
                            <div className="flLeft" >
                                <span className="suntitle">{this.state.json['20020NAIPT-000078']}：</span>{/* 国际化处理： 原币合计*/}
                                <span className="amountSum" >
                                    <NCNumber
                                        fieldid="amountSum"
                                        scale={Number(this.state.CurrinfoScale)}
                                        disabled={true}
                                        value={amountSum}
                                    />
                                </span>
                            </div>
                            <div className="flright" >
                                <span className="suntitle">{this.state.json['20020NAIPT-000079']}：</span>{/* 国际化处理： 本币合计*/}
                                <span className="localamountSum" >
                                    <NCNumber
                                        fieldid="localamountSum"
                                        scale={Number(this.state.CurrinfoScale)}
                                        disabled={true}
                                        value={localamountSum}
                                    />
                                </span>
                            </div>
                        </div>
                    </NCDiv>
                        :<div />}
            </div>
        );
    }
}
VerifyBalances = createPage({})(VerifyBalances);
ReactDOM.render(<VerifyBalances />, document.querySelector("#app"));
