import { base, ajax, toast } from 'nc-lightapp-front';
export default function buttonClick(props, id, _this) {
    let { tableData, selectedRow, currentRow } = _this.state;
    let param = _this.getSubmitFormData();
    if(id === 'add'){
        if(!_this.state.ref1.refpk){
            toast({ content: _this.state.json['20020NAIPT-000000'], color: 'warning' });/* 国际化处理： 请选择核算账簿*/
            return false;
        }
    } else if (id !== 'save' && id !== 'cancel') {
        if (!currentRow.pk_verifyobj){
            toast({ content: _this.state.json['20020NAIPT-000001'], color: 'warning' });/* 国际化处理： 没有选择记录！*/
            return false;
        }
    }
    switch (id) {
        //新增
        case 'add':
            toggleEditStatus('add');
            let addLine = JSON.parse(JSON.stringify(_this.addLine));
            addLine.editable = true;//编辑态标识，保存时不传给后台
            selectedRow = selectedRow.map(item => item = null);
            selectedRow.push(true);//高亮新增行
            tableData = [...tableData,addLine];
            _this.setState({
                addLine,
                editRow: addLine,
                editIndex: selectedRow.length - 1,
                selectedRow: selectedRow,
                tableData: tableData
            });
            break;
        //保存
        case 'save':
            let parms = _this.getSubmitFormData('edit');
            ajax({
                url: '/nccloud/gl/verify/verifyobjsave.do',
                data: parms,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        toggleEditStatus('browse');
                        _this.getData();
                    }
                },
                error: function (res) {
                    toast({ content: res.message, color: 'danger' });
                }
            });
            break;
        //取消
        case 'cancel':
            toggleEditStatus('browse');
            _this.getData();
            break;
         
    }

    function toggleEditStatus(type) {
        // let editable = type !== 'browse';
        let status = type === 'browse';
        //先设置隐藏
        props.button.setButtonsVisible({
            add: status,
            save: !status,
            cancel: !status
        });

    }
}

