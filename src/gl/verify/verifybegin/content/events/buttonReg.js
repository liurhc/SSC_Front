import { ajax, base, toast,cacheTools,print,withNav,promptBox } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
let tableid = 'gl_brsetting';

// @withNav
export default function buttonClick(props, id) {
    let {querycondition}=this.state;
    let rowDatas,pk_brsetting,pk_unit,pk_currtype,link;
    switch (id) {
        case 'showAddBtn':
        //新增
        this.handleAdd();
        break;
        // 删除
        case 'delBtn':
            this.handleDele();
            break;

        // 修改
        case 'modifyBtn':
            this.handleModified();
            break;

        // 查询
        case 'searchBtn':
            this.handleQuery();
            break;

        // 保存
        case 'saveBtn':
            this.handleSave();
            break;

        // 导入导出
        case 'outputBtn':
           // this.onSelect();
            break;





        // 导入
        case 'insertBtn':
            this.leadInFn(true);
            break;

        // 导入期初余额
        case 'ineMonBtn':
            promptBox({
                color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                title: "",                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                content: this.state.json['20020NAIPT-000002'],             // 提示内容,非必输/* 国际化处理： 你是否要导入该科目期初未达数据？（如果已经导过了，请勿重复导入）*/
                noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                beSureBtnName: this.state.json['20020NAIPT-000003'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
                cancelBtnName: this.state.json['20020NAIPT-000004'],           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
                beSureBtnClick: this.leadInBalanceFn,  // 确定按钮点击调用函数,非必输
                cancelBtnClick: this.cancelBtnClick.bind(this)  // 取消按钮点击调用函数,非必输
            }) 
            break;

        // excel导入
        case 'ExInsBtn':
            this.openImportModal();
            break;

        // excel导出
        case 'ExOutBtn':
            this.openExportModal();
            break;
        // 检查
        case 'checkBtn':
            this.handleCheck();
            break;

        // 打印
        case 'showPrintBtn':
            this.showPrintModal();
            break;

        // 模板输出
        case 'modelBtn':
            this.showOutputModal();
            break;

        // 取消
        case 'cancelBtn':
        promptBox({
            color:"warning",
            title: this.state.json['20020NAIPT-000004'],/* 国际化处理： 取消*/
            content: this.state.json['20020NAIPT-000005'],/* 国际化处理： 确认要取消吗?*/
            beSureBtnName: this.state.json['20020NAIPT-000006'],/* 国际化处理： 确认*/
            cancelBtnName: this.state.json['20020NAIPT-000004'] ,/* 国际化处理： 取消*/
            beSureBtnClick: this.canAddOrMod
            });
            
            break;
        // 导入
        case 'secInsertBtn':
        this.leadInBackFn();
        break;

        // 返回
        case 'secReturnBtn':
        this.leadInFn(false);
        break;
        default:
        break;

    }
}
export function cancelBtnClick(){
    return false;
}
