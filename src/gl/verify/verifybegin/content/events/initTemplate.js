import {
	createPage,
	ajax,
	base,
	toast,
	cacheTools,
	print
  } from "nc-lightapp-front";
  let { NCPopconfirm, NCIcon } = base;
  
  let searchId = "busirecon_query";
	const pageCode = "20020NAIPTPAGE";
	// 设置进入页面时的不可用按钮
	const disabledButtonsArr = [
		"showAddBtn",
		"delBtn",
		"modifyBtn",
		"saveBtn",
		"outputBtn",
		"checkBtn",
		"showPrintBtn",
		"modelBtn",
		"cancelBtn",
		'insertBtn',
		'ineMonBtn',
		'ExInsBtn',
		'ExOutBtn'
				];
  export default function(props) {
	let appcode = props.getSearchParam("c");
	ajax({
	  url: "/nccloud/platform/appregister/queryallbtns.do",
	  data: {
		pagecode: pageCode,
		appcode: appcode //小应用id
	  },
	  async: false,
	  success: function(res) {
		if (res.data) {
				  let button = res.data;
			props.button.setButtons(button)
			props.button.setButtonDisabled(disabledButtonsArr, true);
			props.button.setButtonVisible(['secInsertBtn','secReturnBtn','saveBtn','cancelBtn'],false);
			props.button.setButtonVisible(['searchBtn','showAddBtn','modifyBtn','delBtn','outputBtn','showPrintBtn','checkBtn'],true);
		}
	  }
	});
  }
  
