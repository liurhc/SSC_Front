import React, { Component } from 'react';
import { high, base, ajax, deepClone, createPage, promptBox, toast, getMultiLang } from "nc-lightapp-front";
const {
    NCForm, NCTable,
    NCInput
} = base;
const { NCFormItem: FormItem } = NCForm;
// import pure from 'recompose/pure';
class GetTableData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            checkedList: [],

        }
        this.renderDataSource = [];
    }
    componentWillMount() {
    }
    componentWillReceiveProps(nextProps) {
    }
    shouldComponentUpdate(nextProps, nextState) {//要考虑复选框勾选的情况，要不然勾选不上
        if (nextProps.renderDataSource.length > 0 && (nextProps.renderDataSource == this.props.renderDataSource) &&
            nextProps.selectedRecord > 0 && nextProps.selectedRecord == this.props.selectedRecord &&!nextProps.renderDataSource[nextProps.selectedRecord].rowEdited&&
            !nextProps.allChecked&&nextProps.allChecked == this.props.allChecked&&nextProps.checkedList == this.props.checkedList) {
            return false;
        }
        return true;
    }
    render() {
        return (
            <div fieldid="verifybegin_table">
                <NCTable
                    isDrag={true}
                    rowKey={(record) => {
                        return record.pk_verifydetail ? record.pk_verifydetail : record.key
                    }}
                    columns={this.props.columns}
                    onRowClick={this.props.getRow}
                    data={this.props.renderDataSource}
                    bodyStyle={{height:this.props.getTableHeight}}
                    scroll={{
                        x: true,
                        y: this.props.getTableHeight
                        //    y:"calc(100vh - 250px)"
                    }}
                />
            </div>
        )
    }
}
export default function (props = {}) {
    var conf = {
    };

    return <GetTableData {...Object.assign(conf, props)} />
}