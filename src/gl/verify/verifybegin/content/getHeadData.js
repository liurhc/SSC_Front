import React,{Component} from 'react';
import { high, base, ajax, deepClone, createPage, promptBox,toast,getMultiLang } from "nc-lightapp-front";
const {
    NCForm,
    NCInput
} = base;
const { NCFormItem: FormItem } = NCForm;
// import pure from 'recompose/pure';
class GetHeadFormData extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
        }
    }
    componentWillMount() {
        let callback= (json) =>{
            this.headData = [
                // {itemName:'核算账簿:',itemType:'textInput',itemKey:'accountingbookValue'},
                { itemName: this.state.json['20020NAIPT-000022'], itemType: "textInput", itemKey: "verObj" },/* 国际化处理： 核销对象科目:*/
                { itemName: this.state.json['20020NAIPT-000023'], itemType: "textInput", itemKey: "asscontent" },/* 国际化处理： 辅助核算:*/
                { itemName: this.state.json['20020NAIPT-000024'], itemType: "textInput", itemKey: "begindate" }/* 国际化处理： 启动日期:*/
            ];
          this.setState({
            json:json,
            
            })
        }
        getMultiLang({moduleId:'20020NAIPT',domainName:'gl',currentLocale:'simpchn',callback});
    }
    shouldComponentUpdate(nextProps,nextState){
        // if(nextProps.queryData&&(nextProps.queryData==this.props.queryData)){
        //     return false;
        // }
        return true;
    }
    getHeadData = (queryData,headData) => {
        // let { queryData } = this.state;
        let listItem = {
           // verObj: queryData.pk_accountingName ? queryData.pk_accountingName : "",
            verObj: queryData&&queryData.checkSubject ? queryData.checkSubject.name : "",
            asscontent:queryData&&queryData.asscontent  ? queryData.asscontent : "",
            begindate: queryData&&queryData.begindate ? queryData.begindate : ""
        };
        let verifysubj=listItem.verObj;
        let asscontent=listItem.asscontent;
        let begindate=listItem.begindate;
        // const headData = [
        //     // {itemName:'核算账簿:',itemType:'textInput',itemKey:'accountingbookValue'},
        //     { itemName: state.json['20020NAIPT-000022'], itemType: "textInput", itemKey: "verObj" },/* 国际化处理： 核销对象科目:*/
        //     { itemName: state.json['20020NAIPT-000023'], itemType: "textInput", itemKey: "asscontent" },/* 国际化处理： 辅助核算:*/
        //     { itemName: state.json['20020NAIPT-000024'], itemType: "textInput", itemKey: "begindate" }/* 国际化处理： 启动日期:*/
        // ];
        // if(headData){
            return headData.map((item, i) => {
                let defValue = listItem[item.itemKey];
                switch (item.itemType) {
                    case "textInput":
                        return (
                            <FormItem
                                inline={true}
                                //showMast={true}
                                labelXs={1}
                                labelSm={1}
                                labelMd={1}
                                xs={2}
                                md={2}
                                sm={2}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                {/* <FormControl
                                    value={defValue}
                                /> */}
                                <NCInput
                                    //isViewMode
                                    // disabled={true}
                                    readOnly
                                    name={item.itemKey}
                                    type="customer"
                                    defaultValue={defValue}
                                />
                            </FormItem>
                        );
                    default:
                        break;
                }
            });
        // }
    };
    render(){
        return (
            <NCForm
                useRow={true}
                submitAreaClassName="classArea"
                showSubmit={false}
            >
                {this.getHeadData(this.props.queryData,this.headData)}
            </NCForm>
        )
    }
}
export default function (props = {}) {
    var conf = {
    };

    return <GetHeadFormData {...Object.assign(conf, props)} />
}