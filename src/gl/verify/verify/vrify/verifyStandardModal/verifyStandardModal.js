import React, { Component } from 'react';
import {high,base,ajax,deepClone,getMultiLang} from 'nc-lightapp-front';
// import moment from 'moment';
// import zhCN from 'rc-calendar/lib/locale/zh_CN';
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../../public/components/FormItems';
const { Refer} = high;
import { toast } from '../../../../public/components/utils.js';
import createScript from '../../../../public/components/uapRefer.js';
// const format = 'YYYY-MM-DD';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm,NCDiv
} = base;
import '../index.less'
//import GetAssidData from '../events/GetAssidData';
import getAssDatas from "../../../../public/components/getAssDatas/index.js";
const {  NCFormItem:FormItem } = NCForm;

// const dateInputPlaceholder = this.state.json['20020VRIFYPAGE-000083'];/* 国际化处理： 选择日期*/


  const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
  };


export default class VerifyStandardModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal:false,
            isShowUnit:false,//是否显示业务单元
            assData: [//辅助核算信息
            ],
            condition:[],//存储父组件传过来的查询条件
            childAssData:[],
            loadData:[],//查询模板加载数据
            listItem:{},//模板数据对应值
            SelectedAssData:[],//选中的数据
            checkedAll:true,
            checkedArray: [
                // false,
                // false,
                // false,
            ],
            controlCondition:{//控制是否可编辑性的条件
                dateRangeDisabled:true,//默认不可编辑
                isCrossAccountVerify:false,//是否跨末级
                issPk_accsubjShow:false,//是否显示按末级科目
                sVerifyNoChecked:true,//默认勾选按核销号 不可编辑
                sPk_accsubjChecked:true,//默认勾按末级科目不可编辑
                sAssChecked:true,//默认勾选按辅助项不可编辑
                sPk_unitChecked:true//默认勾选业务单元可编辑
            },
            json:{}
           // columns10:this.columns10
        };
        this.close = this.close.bind(this);
    }
	componentWillMount() {

        let callback= (json) =>{
          this.setState({json:json},()=>{})
        }
        getMultiLang({moduleId:['20020VRIFYPAGE','publiccomponents'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentWillReceiveProps (nextProp) {
        let self=this;
        let {loadData,showOrHide,queryCondition}=nextProp;
        let { listItem,showModal,assData,checkedArray,checkedAll,condition,controlCondition,childAssData }=self.state;
        checkedArray=[];
        let currentLoadData=deepClone(loadData);
        let ass=queryCondition.resourceAss;
        condition=queryCondition;
        childAssData=queryCondition
        if(ass){
            ass.map((item,index)=>{
                item.key=index;
                if(queryCondition.isControlItems&&queryCondition.isControlItems.length>0&&queryCondition.isControlItems.indexOf(item.pk_accassitem)>-1){
                    checkedArray.push(true);
                }else{
                    checkedArray.push(false);
                }
            })
        }
        if(queryCondition.endflag){//是否末级 默认勾选不可编辑
            controlCondition.sPk_accsubjChecked=true;
        }else{//非末级
            controlCondition.sPk_accsubjChecked=false;
        }
        if(queryCondition.isControl){//严格控制为true时
            controlCondition.sAssChecked=true;
        }else{
            controlCondition.sAssChecked=false;
        }
        //控制辅助核算项严格控制的默认勾选且不可修改
        // for (var i = 0; i < checkedArray.length; i++) {
        //     checkedArray[i] = controlCondition.sAssChecked;
        //     }
        checkedAll=controlCondition.sAssChecked;
        // self.setState({
        //     checkedArray,checkedAll
        // })
        if (showOrHide) {//打开模态框 nextProp.loadData !== this.state.loadData&&queryCondition != self.state.condition
            // condition=queryCondition;
            currentLoadData.forEach((item,i)=>{
                let  key;
                if(item.itemType=='refer'){
                    key={
                        display:'',
                        value:''
                    }
                }else if(item.itemType=='checkbox'){
                    if(item.itemKey=='sVerifyNo'){
                        key={
                            value:'Y'
                        }
                    }else if(item.itemKey=='sAss'){
                        if(controlCondition.sAssChecked){
                            item.itemChild[0].checked=true;
                            key={
                                value:'Y'
                            }
                        }else{
                            item.itemChild[0].checked=false;
                            key={
                                value:'N'
                            }
                        }
                        
                    }else if(item.itemKey=='sPk_accsubj'){
                        if(queryCondition.endflag){
                            item.itemChild[0].checked=true;
                            key={
                                value:'Y'
                            }
                        }else{
                            item.itemChild[0].checked=false;
                            key={
                                value:'N'
                            }
                        }
                        
                    }else if(item.itemKey=='sPk_unit'){
                        if(condition.isShowUnit){
                            key={value:'Y'}
                        }else{
                            key={value:'N'}
                        }
                        
                    }else{
                        key={
                            value:''
                        }
                    }
                }else if(item.itemType=='textInput'){
                    key={value:0}
                }else{
                    key={
                        value:''
                    }
                    if(item.itemType=='date'||item.itemType=='Dbdate'){
                        key={
                            //value:moment().format("YYYY-MM-DD")
                            value:''
                        }
                    }
                }
                if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'){
                    item.itemKey.map((k,index)=>{
                        let name= k;
                        listItem[name]=key
                    });
                }else{
                    let name= item.itemKey;
                    listItem[name]=key
                }
            
            })
            
            self.setState({
                loadData:currentLoadData,
                showModal:showOrHide,
                assData:ass,controlCondition,
                listItem,checkedArray,checkedAll,condition,childAssData
            })
        }else{
            
            self.setState({
                loadData:currentLoadData,
                showModal:showOrHide,controlCondition,
                assData:ass,condition,childAssData,checkedArray,checkedAll
            })
        }
    }
    
    //表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

    close() {
        this.props.handleClose();
    }

    confirm=()=>{
        let { listItem,assData,SelectedAssData,checkedArray } =this.state;
        SelectedAssData=[];
        for(var i = 0; i < checkedArray.length; i++){
            if(checkedArray[i]==true){
                SelectedAssData.push(assData[i]);
            }
        }
        listItem.ass=SelectedAssData;//assData;
        this.props.onConfirm(deepClone(listItem));
    }

    queryList=(data)=>{
        let self=this;
        const dateInputPlaceholder = this.state.json['20020VRIFYPAGE-000083'];/* 国际化处理： 选择日期*/
        let { listItem,isShowUnit,assData,checkedArray,checkedAll,condition,controlCondition } =self.state;
			return data.length!=0?(
				data.map((item, i) => {                   
                   switch (item.itemType) {
                        case 'date':
                            return (
                                    <FormItem
                                        inline={true}
                                        showMast={true}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        method="blur"
                                        inputAlfer="%"
                                        errorMessage={this.state.json['20020VRIFYPAGE-000092']}/* 国际化处理： 输入格式错误*/
                                    >
                                        <DatePicker
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                            isRequire={true}
                                            //format={format}
                                            //disabled={isChange}
                                            // value={moment(listItem[item.itemKey].value)}
                                            // locale={zhCN}
                                            value={listItem[item.itemKey].value}
                                            onChange={(v) => {
                                                listItem[item.itemKey].value = v;
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                            placeholder={dateInputPlaceholder}
                                            />
                                        </FormItem>
                            );
                        case'Dbdate':
                            return(
                                <Row>
                                <Col xs={12} md={12} sm={12} className="dateMargin">
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={4} md={4} sm={4}
                                    labelName={item.itemName}
                                    method="blur"
                                    inputAlfer="%"
                                    errorMessage={this.state.json['20020VRIFYPAGE-000092']}/* 国际化处理： 输入格式错误*/
                                        inputAfter={
                                            <Col xs={12} md={12} sm={12}>
                                                <span className="online">&nbsp;--&nbsp;</span>
                                                <DatePicker
                                                    fieldid="end_date"
                                                    name={item.itemKey}
                                                    type="customer"
                                                    isRequire={true}
                                                    //format={format}
                                                    // value={moment(listItem.end_date.value)}
                                                    // locale={zhCN}
                                                    value={listItem.end_date.value}
                                                    onChange={(v) => {
                                                        listItem.end_date = { value: v }
                                                        this.setState({
                                                            listItem
                                                        })
                                                    }}
                                                    placeholder={dateInputPlaceholder}
                                                />
                                            </Col>
                                        }
                                    >
                                    <DatePicker
                                        fieldid="begin_date"
                                        name={item.itemKey}
                                        type="customer"
                                        isRequire={true}
                                        //format={format}
                                        //disabled={isChange}
                                        // value={moment(listItem.begin_date.value)}
                                        // locale={zhCN}
                                        value={listItem.begin_date.value}
                                        onChange={(v) => {
                                            listItem.begin_date={value: v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    />
                                </FormItem>
                                </Col>
                                </Row>
                            );
                        case 'textInput':
                            if(item.itemKey=='dateRange'){
                                return (
                                    <FormItem
                                        inline={true}
                                        showMast={true}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={6}  md={6} sm={6}
                                        labelName={item.itemName}
                                        // isRequire={true}
                                        method="change"
                                        inputAfter={<span>{this.state.json['20020VRIFYPAGE-000098']}</span>}/* 国际化处理： 天内*/
                                    >
                                    <NCNumber
                                        fieldid={item.itemKey}
                                        scale={0}
                                        disabled={controlCondition.dateRangeDisabled}
                                        value={listItem[item.itemKey].value}
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                    </FormItem>
                                );
                            }else{
                                return (
                                    <FormItem
                                        inline={true}
                                        showMast={true}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={6}  md={6} sm={6}
                                        labelName={item.itemName}
                                        isRequire={true}
                                        method="change"
                                        inputAfter={<span>{this.state.json['20020VRIFYPAGE-000098']}</span>}/* 国际化处理： 天内*/
                                    >
                                        <FormControl
                                            fieldid={item.itemKey}
                                            value={listItem[item.itemKey].value}
                                            onChange={(v) => {
                                                listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                        />
                                    </FormItem>
                                );
                            }
                        case 'DbtextInput':
                            return(
                            <Row>
                                <Col xs={12}  md={12} sm={12} className="dateMargin">
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2}  labelSm={2} labelMd={2}
                                    xs={4}  md={4} sm={4}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                    inputAfter={
                                    <Col xs={12} md={12} sm={12}>
                                        <span className="online">&nbsp;--&nbsp;</span>                                                                          
                                        <FormControl
                                            fieldid="mny_end"
                                            value={listItem.mny_end.value}
                                            onChange={(v) => {
                                                listItem.mny_end={value : v}
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                        />
                                    </Col>}
                                >
                                    <FormControl
                                        fieldid="mny_begin"
                                        value={listItem.mny_begin.value}
                                        onChange={(v) => {
                                            listItem.mny_begin={value : v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                                </Col>                                
                            </Row>);
                        case 'radio':
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                <RadioItem
                                    fieldid={item.itemKey}
                                    name={item.itemKey}
                                    type="customer"
                                //  defaultValue={this.state.assureInfo.contracttype.value}
                                    items={() => {
                                        return (item.itemChild) 
                                    }}
                                    onChange={(v)=>{
                                        listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                    }}
                                />
                            </FormItem>
                            )
                        case 'select':
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                <SelectItem 
                                    name={item.itemKey}
                                    fieldid={item.itemKey}
                                //  defaultValue={this.state[item.itemKey].value?this.state[item.itemKey].value:'all'} 
                                        items = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{
                                            
                                            listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                        }}
                            />
                            </FormItem>)
                        case 'checkbox':
                            if(item.itemKey=='sVerifyNo'){//按核销号
                                return(
                                    <FormItem
                                    inline={true}
                                    showMast={false}
                                    xs={4} md={4} sm={4}
                                    method="change"
                                    className='checkboxStyle'
                                    //inputAfter={item.itemName}
                                    >
                                        <CheckboxItem
                                            fieldid={item.itemKey} 
                                            name={item.itemKey} 
                                            disabled={controlCondition.sVerifyNoChecked}
                                            defaultValue={listItem[item.itemKey].value} 
                                            boxs = {
                                                () => {
                                                    return (item.itemChild) 
                                                }
                                            }
                                            onChange={(v)=>{
                                                
                                                listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                                this.setState({
                                                    listItem,controlCondition
                                                })
                                            }}
                                        />
                                    </FormItem>
                                )

                            }else if(item.itemKey=='sPk_accsubj'){//按末级科目
                                if(condition.isCrossAccountVerify){//末级 true 显示
                                    
                                    return(
                                        <FormItem
                                        inline={true}
                                        showMast={false}
                                        xs={4} md={4} sm={4}
                                        // labelName={item.itemName}
                                        // isRequire={true}
                                        method="change"
                                        className='checkboxStyle'
                                        //inputAfter={item.itemName}
                                        >
                                            <CheckboxItem 
                                                fieldid={item.itemKey}
                                                name={item.itemKey} 
                                                disabled={controlCondition.sPk_accsubjChecked}
                                                defaultValue={listItem[item.itemKey].value} 
                                                boxs = {
                                                    () => {
                                                        return (item.itemChild) 
                                                    }
                                                }
                                                onChange={(v)=>{
                                                
                                                    listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                                    this.setState({
                                                        listItem
                                                    })
                                                }}
                                            />
                                        </FormItem>
                                    )
                                }else{
                                    return(<div/>)
                                }

                            }else if(item.itemKey=='sPk_unit'){//按业务单元，启用二级核算单位的显示
                                if(condition.isShowUnit){//是否显示 true 显示
                                    return(
                                        <FormItem
                                        inline={true}
                                        showMast={false}
                                        xs={4} md={4} sm={4}
                                        // labelName={item.itemName}
                                        // isRequire={true}
                                        method="change"
                                        className='checkboxStyle'
                                        //inputAfter={item.itemName}
                                        >
                                            <CheckboxItem 
                                                fieldid={item.itemKey}
                                                name={item.itemKey} 
                                                disabled={false}
                                                defaultValue={listItem[item.itemKey].value} 
                                                boxs = {
                                                    () => {
                                                        return (item.itemChild) 
                                                    }
                                                }
                                                onChange={(v)=>{
                                                  
                                                    listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                                    this.setState({
                                                        listItem
                                                    })
                                                }}
                                            />
                                        </FormItem>
                                    )
                                }else{
                                    return(<div/>)
                                }

                            }else if(item.itemKey=='sAss'){//按辅助项
                                
                                return(
                                    <FormItem 
                                    inline={true}
                                    showMast={false}
                                    xs={12} md={12} sm={12}
                                    // labelName={item.itemName}
                                    // isRequire={true}
                                    method="change"
                                    className='checkboxStyle'
                                    //inputAfter={item.itemName}
                                    >
                                        <CheckboxItem 
                                            fieldid={item.itemKey}
                                            name={item.itemKey} 
                                            disabled={controlCondition.sAssChecked}
                                            defaultValue={listItem[item.itemKey].value} 
                                            boxs = {
                                                () => {
                                                    return (item.itemChild) 
                                                }
                                            }
                                            onChange={(v)=>{
                                               
                                                for (var i = 0; i <checkedArray.length; i++) {
                                                    checkedArray[i] = v[0].checked;
                                                }
                                                checkedAll=v[0].checked;
                                                listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                                this.setState({
                                                    listItem,checkedArray,checkedAll
                                                })
                                            }}
                                        />
                                    </FormItem>
                                )
                            }else if(item.itemKey=='sDateRange'){//按日期范围
                                return(
                                    <FormItem
                                    inline={true}
                                    showMast={false}
                                    xs={4} md={4} sm={4}
                                    method="change"
                                    className='checkboxStyle'
                                    >
                                        <CheckboxItem 
                                            fieldid={item.itemKey}
                                            name={item.itemKey} 
                                        //defaultValue={this.state.periodloan.value} 
                                            boxs = {
                                                () => {
                                                    return (item.itemChild) 
                                                }
                                            }
                                            onChange={(v)=>{
                                                controlCondition.dateRangeDisabled=!v[0].checked;
                                                listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                                this.setState({
                                                    listItem,controlCondition
                                                })
                                            }}
                                        />
                                    </FormItem>
                                )
                            }else{
                                return(
                                    <FormItem
                                    inline={true}
                                    showMast={false}
                                    xs={4} md={4} sm={4}
                                    method="change"
                                    className='checkboxStyle'
                                    >
                                        <CheckboxItem 
                                            fieldid={item.itemKey}
                                            name={item.itemKey} 
                                        //defaultValue={this.state.periodloan.value} 
                                            boxs = {
                                                () => {
                                                    return (item.itemChild) 
                                                }
                                            }
                                            onChange={(v)=>{
                                            
                                                listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                        />
                                    </FormItem>
                                )
                            }
                        default:
                        break;
                    }
                })
			):<div />;
           
    }

    //全选
    onAllCheckChange = () => {
        let self = this;
        let {condition,assData,checkedArray}=self.state;
        //let checkedArray = [];
        for (var i = 0; i <checkedArray.length; i++) {
            if(condition.isControlItems&&condition.isControlItems.length>0&&condition.isControlItems.indexOf(assData[i].pk_accassitem)>-1){//严格控制的辅助核算不让编辑
                checkedArray[i] = checkedArray[i];
            }else{
                checkedArray[i] = !self.state.checkedAll;
            }
          
        }
        self.setState({
          checkedAll: !self.state.checkedAll,
          checkedArray
        });
      };
      //单选
      onCheckboxChange = (text, record, index) => {
        let self = this;
        let {condition,assData}=self.state;
        let allFlag = false;
        let checkedArray = self.state.checkedArray.concat();
        checkedArray[index] = !self.state.checkedArray[index];
        if(condition.isControlItems&&condition.isControlItems.length>0&&condition.isControlItems.indexOf(assData[index].pk_accassitem)>-1){//严格控制的辅助核算不让编辑
            return false;
        }
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          if (!checkedArray[i]) {
            allFlag = false;
            break;
          } else {
            allFlag = true;
          }
        }
        self.setState({
          checkedAll: allFlag,
          checkedArray: checkedArray,
        });
      };
      
      renderColumnsMultiSelect(columns) {
        const {checkedArray } = this.state;
        const { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        // let indeterminate_bool1 = true;
        if (multiSelect && multiSelect.type === "checkbox") {
          let i = checkedArray.length;
          while(i--){
              if(checkedArray[i]){
                indeterminate_bool = true;
                break;
              }
          }
          let defaultColumns = [
            {
              title: <div fieldid="firstcol">{(
                <Checkbox
                  className="table-checkbox"
                  checked={this.state.checkedAll}
                  indeterminate={indeterminate_bool&&!this.state.checkedAll}
                  onChange={this.onAllCheckChange}
                />
            )}</div>,
              key: "checkbox",
              dataIndex: "checkbox",
              width: "50",
              render: (text, record, index) => {
                return (<div fieldid="firstcol">
                  <Checkbox
                    className="table-checkbox"
                    checked={this.state.checkedArray[index]}
                    onChange={this.onCheckboxChange.bind(this, text, record, index)}
                  />
                  </div>
                );
              }
            }
          ];
          columns = defaultColumns.concat(columns);
        }
        return columns;
      }

    render() {
        let{showOrHide,flag}=this.props;
        let { loadData,assData,checkedAll,checkedArray,childAssData} =this.state;
        let columns10 = [
            {
              title: this.state.json['20020VRIFYPAGE-000004'],/* 国际化处理： 核算类型*/
              dataIndex: "checktypename",
              key: "checktypename",
              width: "200",
              render: (text, record, index) => {
                return <span>{record.checktypename}</span>;
              }
            },
            {
              title: this.state.json['20020VRIFYPAGE-000005'],/* 国际化处理： 核算内容*/
              dataIndex: "checkvaluename",
              key: "checkvaluename",
              width: "300",
              render: (text, record, index) => {
                let { assData,condition } = this.state;
                let defaultValue = [];
                if (assData[index]["checkvaluename"]) {
                  assData[index]["checkvaluename"].split(",").map((item, _index) => {
                    defaultValue[_index] = { refname: item, refpk: "" };
                  });
                  assData[index]["pk_Checkvalue"].split(",").map((item, _index) => {
                    defaultValue[_index].refpk = item;
                  });
                } else {
                  defaultValue = [{ refname: "", refpk: "" }];
                }
                if (record.refnodename) {
                  let referUrl = record.refnodename + '.js';
                  if (!this.state[record.pk_accassitem]) {
                    { createScript.call(this, referUrl, record.pk_accassitem) }
                    return <div />
                  } else {
                    if (record.classid == 'b26fa3cb-4087-4027-a3b6-c83ab2a086a9' || record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                      return (
                        <FormItem
                          inline={true}
                          // showMast={true}
                          labelXs={2} labelSm={2} labelMd={2}
                          xs={10} md={10} sm={10}
                          //labelName={record.itemName}
                          isRequire={true}
                          method="change"
                        >
                          {this.state[record.pk_accassitem] ? (this.state[record.pk_accassitem])(
                            {
                              value: defaultValue,
                              isShowUnit: true,
                              unitProps: {
                                refType: 'tree',
                                refName: this.state.json['20020VRIFYPAGE-000017'],/* 国际化处理： 业务单元*/
                                refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                rootNode: { refname: this.state.json['20020VRIFYPAGE-000017'], refpk: 'root' },/* 国际化处理： 业务单元*/
                                placeholder: this.state.json['20020VRIFYPAGE-000017'],/* 国际化处理： 业务单元*/
                                queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                treeConfig: { name: [this.state.json['20020VRIFYPAGE-000084'], this.state.json['20020VRIFYPAGE-000085']], code: ['refcode', 'refname'] },/* 国际化处理： 编码,名称*/
                                isMultiSelectedEnabled: false,
                                //unitProps:unitConf,
                                isShowUnit: false
                              },
                              unitCondition:{
                                pk_financeorg:condition.pk_org,
                                'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                              },
                              isMultiSelectedEnabled: true,
                              "unitValueIsNeeded":false,
                              "isShowDimission":true,
                              queryCondition: () => {
                                if (record.classid && record.classid.length == 20) {//classid的长度大于20的话过滤条件再加一个pk_defdoclist
                                  return {
                                    "pk_org": condition.pk_org,
                                    "pk_defdoclist": record.pk_defdoclist
                                  }
                                } else {
                                  if (record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f') {//人员
                                    return {
                                      isShowDimission:true,
                                      "busifuncode": "all",
                                      "pk_org": condition.pk_org
                                    }
                                  } else {
                                    return {
                                      "pk_org": condition.pk_org
                                    }
                                  }
      
                                }
                              },
                              onChange: (v) => {
                                let { assData } = this.state;
                                let originData = this.findByKey(record.key, assData);
                                let refnameArr = [], refpkArr = [], refcodeArr = [];
                                if (originData) {
                                  v.map((arr, index) => {
                                    refnameArr.push(arr.refname);
                                    refpkArr.push(arr.refpk);
                                    refcodeArr.push(arr.refcode);
      
                                  })
                                  originData.checkvaluename = refnameArr.join();
                                  originData.pk_Checkvalue = refpkArr.join();
                                  originData.checkvaluecode = refcodeArr.join();
                                }
                                condition.assData = assData;
                                this.setState({
                                  assData, condition
                                })
                              }
                            }) : <div />}
                        </FormItem>);
                    } else {
                      return (
                        <FormItem
                          inline={true}
                          // showMast={true}
                          labelXs={2} labelSm={2} labelMd={2}
                          xs={10} md={10} sm={10}
                          //labelName={record.itemName}
                          isRequire={true}
                          method="change"
                        >
                          {this.state[record.pk_accassitem] ? (this.state[record.pk_accassitem])(
                            {
                              value: defaultValue,
                              isMultiSelectedEnabled: true,
                              queryCondition: () => {
                                if (record.classid && record.classid.length == 20) {//classid的长度等于20的话过滤条件再加一个pk_defdoclist
                                  return {
                                    "pk_org": condition.pk_org,
                                    "pk_defdoclist": record.pk_defdoclist
                                  }
                                } else {
                                  return {
                                    "pk_org": condition.pk_org
                                  }
                                }
                              },
                              onChange: (v) => {
                                let { assData } = this.state;
                                let originData = this.findByKey(record.key, assData);
                                let refnameArr = [], refpkArr = [], refcodeArr = [];
                                if (originData) {
                                  v.map((arr, index) => {
                                    refnameArr.push(arr.refname);
                                    refpkArr.push(arr.refpk);
                                    refcodeArr.push(arr.refcode);
      
                                  })
                                  originData.checkvaluename = refnameArr.join();
                                  originData.pk_Checkvalue = refpkArr.join();
                                  originData.checkvaluecode = refcodeArr.join();
                                }
                                condition.assData = assData;
                                this.setState({
                                  assData, condition
                                })
                              }
                            }) : <div />}
                        </FormItem>);
                    }
                  }
                } else {//不是参照的话要区分日期、字符、数值
                  if (record.classid == 'BS000010000100001033') {//日期
                    return (
                      <DatePicker
                        //name={item.itemKey}
                        type="customer"
                        isRequire={true}
                        placeholder={dateInputPlaceholder}
                        value={defaultValue[0].refname}
                        onChange={(v) => {
                          let { assData } = this.state;
                          let originData = this.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = assArr;
                            originData.pk_Checkvalue = assArr;
                            originData.checkvaluecode = assArr;
                          }
                          condition.assData = assData;
                          this.setState({
                            assData, condition
                          })
                        }}
      
                      />
                    )
                  } else if (record.classid == 'BS000010000100001031') {//数值
                    return (
                      <NCNumber
                        scale={2}
                        value={defaultValue[0].refname}
                        placeholder={this.state.json['20020VRIFYPAGE-000086']}/* 国际化处理： 请输入数字*/
                        onChange={(v) => {
                          let { assData } = this.state;
                          let originData = this.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = assArr;
                            originData.pk_Checkvalue = assArr;
                            originData.checkvaluecode = assArr;
                          }
                          condition.assData = assData;
                          this.setState({
                            assData, condition
                          })
                        }}
                      />
                    )
                  } else if (record.classid == 'BS000010000100001032') {//布尔
                    return (
                      <FormItem
                        inline={true}
                        // showMast={false}
                        labelXs={2}
                        labelSm={2}
                        labelMd={2}
                        xs={10}
                        md={10}
                        sm={10}
                        // labelName={item.itemName}
                        isRequire={true}
                        method="change"
                      >
                        <SelectItem name={record.checktypecode}
                          defaultValue={defaultValue[0].refname}
                          items={
                            () => {
                              return ([{
                                label: this.state.json['20020VRIFYPAGE-000087'],/* 国际化处理： 是*/
                                value: 'Y'
                              }, {
                                label: this.state.json['20020VRIFYPAGE-000088'],/* 国际化处理： 否*/
                                value: 'N'
                              }])
                            }
                          }
                          onChange={(v) => {
                            let { assData } = this.state;
                            let originData = this.findByKey(record.key, assData);
                            if (originData) {
                              let assArr = [];
                              assArr.push(v);
                              originData.checkvaluename = assArr;
                              originData.pk_Checkvalue = assArr;
                              originData.checkvaluecode = assArr;
                            }
                            condition.assData = assData;
                            this.setState({
                              assData, condition
                            })
                          }}
                        />
                      </FormItem>
                    )
                  } else {//字符
                    return (
                      <FormControl
                        value={defaultValue[0].refname}
                        onChange={(v) => {
                          let { assData } = this.state;
                          let originData = this.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = assArr;
                            originData.pk_Checkvalue = assArr;
                            originData.checkvaluecode = assArr;
                          }
                          condition.assData = assData;
                          this.setState({
                            assData, condition
                          })
                        }}
                      />
                    )
                  }
      
                }
              }
            }
          ];
        let columnsldad = this.renderColumnsMultiSelect(columns10);
        let pretentAssData={
            assData: assData||[],
            childAssData:childAssData||[],
            checkboxShow: true,//是否显示复选框
            showOrHide:true,
            checkedAll:checkedAll,//全选复选框的选中状态
            checkedArray:checkedArray||[],//复选框选中情况
            $_this:this
          };
        let title;
        if(flag=='1'){
            title=this.state.json['20020VRIFYPAGE-000096'];/* 国际化处理： 核销标准*/
        }else if(flag=='2'){
            title=this.state.json['20020VRIFYPAGE-000097'];/* 国际化处理： 红蓝对冲标准*/
        }
        const emptyFunc = () => <span>{this.state.json['20020VRIFYPAGE-000094']}！</span>/* 国际化处理： 这里没有数据*/
        return (
            <div className="fl">
                <Modal
                    fieldid="query"
                    className={'senior'}
                    show={showOrHide }
                    id="autoquery"
                    // backdrop={ this.state.modalDropup }
                    onHide={ this.close }
                    animation={true}
                    >
                    <Modal.Header closeButton fieldid="header-area">
                        <Modal.Title>{title}</Modal.Title>
                    </Modal.Header >
                    <Modal.Body >
                        {showOrHide?
                        <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}>
                        <NCForm useRow={true}
                            submitAreaClassName='classArea'
                            showSubmit={false}　>
                            {this.queryList(loadData)}  
                        </NCForm>
                        <div className="getAssDatas">
                        {/* <Table
                            columns={columnsldad}
                             data={assData}
                            emptyText={emptyFunc}
                            scroll={{x:true, y: 150 }}
                        /> */}
                        {getAssDatas({pretentAssData})}
                        </div>
                        </NCDiv>
                        :''}
                    </Modal.Body>
                    <Modal.Footer fieldid="bottom_area">
                        <Button colors="primary" onClick={ this.confirm } fieldid="confirm"> {this.state.json['20020VRIFYPAGE-000011']} </Button>{/* 国际化处理： 确定*/}
                        <Button onClick={ this.close } fieldid="close"> {this.state.json['20020VRIFYPAGE-000099']} </Button>{/* 国际化处理： 关闭*/}
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
VerifyStandardModal.defaultProps = defaultProps12;
