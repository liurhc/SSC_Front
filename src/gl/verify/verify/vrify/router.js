import {asyncComponent} from 'nc-lightapp-front';
//import Home from 'pages/so/home';
// import Verify from 'gl_front/verify/verify/vrify/exportVrify';
// import VrifyHistory from 'gl_front/verify/historyquery/list';
import Verify from './exportVrify.js';
//const Verify = asyncComponent(() => import(/* webpackChunkName: "gl_front/verify/verify/vrify/home" */'./exportVrify.js'));
const VrifyHistory = asyncComponent(() => import(/* webpackChunkName: "gl/verify/verify/vrify/apphome" */'../../historyquery/list/index.js'));

const routes = [
  {
    path: '/',
    component: Verify,
    exact: true,
  },
  {
    path: '/VrifyHistory',
    component: VrifyHistory,
  }
];

export default routes;
