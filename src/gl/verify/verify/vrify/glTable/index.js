import React, { Component } from "react";
import { base } from "nc-lightapp-front";
const {NCTable,NCCheckbox} = base;
// checkboxShow: true,//是否显示复选框
// checkedAll:checkedAll,//全选复选框的选中状态
// checkedArray:checkedArray||[],//复选框选中情况
// $_this:this
// columns={columnsDebit}//列
// className="nc-table" 自定义样式
// isDrag={true}
// isBordered 是否显示边框
// data={debitDataArr}
//bodyStyle={height:'100px'}表体高度
// scroll={{
//     x: true,
//     y: that.state.position ? that.getTableHeightPosi() : that.getTableHeight()
// }}

class GlTable extends Component{
    constructor(props){
        super(props);
        this.state={
            checkedAll:false, //全选
            checkedArray:[]//
        }
    }
    componentWillMount(){

    }
    componentDidMount(){

    }
    componentWillReceiveProps(nextProps){
        if(nextProps.checkedArray!=this.state.checkedArray){
            this.setState({
                checkedAll:nextProps.checkedAll, //全选
                checkedArray:nextProps.checkedArray||[]//
            })
        }
    }
    //处理column
    dealColumns=(columns)=>{
        console.log('columns',columns);
        columns.map((item,index)=>{
            if(item.title&&item.title.props&&item.title.props.fieldid){
                item.title=item.title;
            }else{
                let title=<div fieldid={item.dataIndex||item.attrcode} className="mergecells">{item.title}</div>
                item.title=title;
            }
        })
        return columns
    }
    //单选
    onCheckboxChange=()=>{
        let { checkedAll, checkedArray } = this.state;
        // let { checkedAll, checkedArray } =$_this.state;
        let allFlag = false;
        checkedArray[index] = !checkedArray[index];
        for (var i = 0; i < checkedArray.length; i++) {
            if (!checkedArray[i]) {
                allFlag = false;
                break;
            } else {
                allFlag = true;
            }
        }
        checkedAll = allFlag;
        this.setState({
            checkedAll, checkedArray
        })
    }
    //多选
    onAllCheckChange=()=>{
        let { checkedAll, checkedArray } = this.state;
        // let { checkedAll, checkedArray } = $_this.state;
        for (var i = 0; i < checkedArray.length; i++) {
            checkedArray[i] = !checkedAll;
        }
        checkedAll = !checkedAll;
        this.setState({
            checkedAll, checkedArray
        })
    }
    //添加复选框
    addCheckbox=(columns)=>{
        let { checkedAll, checkedArray } = this.state;
        // let { checkedAll, checkedArray } = $_this.state;
        const { multiSelect } = this.props;
        let indeterminate_bool = false;
        if (multiSelect && multiSelect.type === "checkbox") {
            let i = checkedArray.length;
            while (i--) {
                if (checkedArray[i]) {
                    indeterminate_bool = true;
                    break;
                }
            }
            let defaultColumns = [
                {
                    title: <div fieldid="firstcol">{(
                        <NCCheckbox
                            className="table-checkbox"
                            checked={checkedAll}
                            indeterminate={indeterminate_bool && !checkedAll}
                            onChange={()=>{this.onAllCheckChange}}
                        />
                    )}</div>,
                    key: "checkbox",
                    attrcode: 'checkbox',
                    dataIndex: "checkbox",
                    width: "60px",
                    render: (text, record, index) => {
                        return (<div fieldid="firstcol">
                            <NCCheckbox
                                className="table-checkbox"
                                checked={checkedArray[index]}
                                onChange={this.onCheckboxChange.bind(this, text, record, index)}
                            />
                            </div>
                        );
                    }
                }
            ];
            columns = defaultColumns.concat(columns);
        }
        return columns;
    }

    render(){
        let {columns}=this.props;
        let glColumns=[],lastColumns=[];
        if(columns&&columns.length>0){
            glColumns=this.dealColumns(columns);
            if (this.props.checkboxShow) {//复选框显示true
                lastColumns = this.addCheckbox(glColumns);
            } else {
                lastColumns = glColumns;
            }
        }
        console.log('this.props',this.props);
        console.log('lastColumns',lastColumns);
        return(
            <div className="gltable">
                <NCTable 
                    {...this.props}
                    columns={lastColumns}
                />
            </div>
        )
    }
}
GlTable.defaultProps= {
    prefixCls: "bee-table",
    multiSelect: {
        type: "checkbox",
        param: "key"
    }
};
export default GlTable;
// export default function (props = {}) {
//     var conf = {
//     };

//     return <GlTable {...Object.assign(conf, props)} />
// }
