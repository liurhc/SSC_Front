import handleQueryClick from './handleQueryClick';
import handleVerify from './handleVerify';
import handleHedging from './handleHedging';
import handleAutoVerify from './handleAutoVerify';
import handleAutoHedging from './handleAutoHedging';
import compareOtherData from './compareOtherData';
import handleUnMatch from './handleUnMatch';
import matchOtherData from './matchOtherData';
import verifyUnverifySum from './verifyUnverifySum';
import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import iseqAssinfo from './iseqAssinfo';
export {initTemplate,buttonClick };
export {handleQueryClick,handleVerify,handleHedging,handleAutoVerify,handleAutoHedging,
    compareOtherData,handleUnMatch,matchOtherData,verifyUnverifySum,iseqAssinfo}
