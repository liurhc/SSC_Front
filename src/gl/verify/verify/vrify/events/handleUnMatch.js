import {ajax,deepClone,toast} from 'nc-lightapp-front';
import handleAllChicked from './handleAllChicked';

export default function handleUnMatch(self){
    let { checkedArrayDebit,checkedArrayCredit,firstCheckedDatas} = self.state;
    for (let i = 0; i <checkedArrayDebit.length; i++) {
        checkedArrayDebit[i] = false;
    }
    for (let i = 0; i <checkedArrayCredit.length; i++) {
        checkedArrayCredit[i] = false;
    }
    firstCheckedDatas.type='';
    firstCheckedDatas.assinfo={};
    let allChickedData_debit = handleAllChicked(self,'debit');
    let allChickedData_credit = handleAllChicked(self,'credit');
    self.setState({
        firstCheckedDatas,
        allMatch:false,
        checkedAllDebit: false,
        checkedAllCredit: false,
        checkedArrayDebit,checkedArrayCredit,
        debitDataArr:allChickedData_debit,  
        creditDataArr:allChickedData_credit,
    },()=>{
        let buttonArr=['full_match','contrast','cancel_contrast'];
        self.props.button.setButtonDisabled(buttonArr, false);
    });
    handleAllChecked(self);
};
// 借方全选 控制 debitBoxClicked状态
function handleAllChecked(self) {
    let { checkedArrayDebit,debitBoxClicked,checkedArrayCredit,creditBoxClicked } = self.state;
    for(let i = 0; i < checkedArrayDebit.length; i++) {
        if(checkedArrayDebit[i] == true) {
            debitBoxClicked=true;
        }else{
            debitBoxClicked=false;
        }
    }
    for(let i = 0; i < checkedArrayCredit.length; i++) {
        if(checkedArrayCredit[i] == true) {
            creditBoxClicked=true;
        }else{
            creditBoxClicked=false;
        }
    }
    self.setState({
        debitBoxClicked,creditBoxClicked
    })
}
