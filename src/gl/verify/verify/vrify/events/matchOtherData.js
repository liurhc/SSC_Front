
import verifyUnverifySum from "./verifyUnverifySum";
import iseqAssinfo from "./iseqAssinfo";
export default function matchOtherData(self, type) {
  let {
    CompareStatus,
    creditSelectedData,
    debitSelectedData,
    resourceData_credit,
    resourceData_debit,
    checkedArrayDebit,
    checkedArrayCredit,
    debitDataArr,isControl,
    creditDataArr,firstCheckedDatas
  } = self.state;
  let currentData = []; //获取对照出来的数据
  
  if (type == "credit") {
    creditSelectedData = self.filterSelectedData("credit"); //获取借方选中的数据
    debitDataArr.map((item, index) => {
      // <- 添加了数组判定不为空
      if (creditSelectedData.length > 0) {
        if(isControl){
          if(iseqAssinfo(self,item.m_voAss,firstCheckedDatas)){//判断辅助核算项是否跟严格控制的辅助核算项相等，相等就勾选上
            checkedArrayDebit[index] = true;
          }
        }else{
          checkedArrayDebit[index] = true;
        }
      }
    });
    self.setState(
      {
        checkedArrayDebit
      },
      () => {
        verifyUnverifySum(self);
      }
    );
  } else if (type == "debit") {
    debitSelectedData = self.filterSelectedData("debit"); //获取借方选中的数据
    resourceData_credit.map((item, index) => {
      if(debitSelectedData.length>0){
        if (isControl){
          if (isControl&&iseqAssinfo(self,item.m_voAss,firstCheckedDatas)) {
            checkedArrayCredit[index] = true;
          }
        }else{
          checkedArrayCredit[index] = true;
        }
      }
    });
    self.setState(
      {
        checkedArrayCredit
      },
      () => {
        verifyUnverifySum(self);
      }
    );
  }
}
