import {ajax,deepClone,promptBox,toast} from 'nc-lightapp-front';
import verifyUnverifySum from './verifyUnverifySum';
export default function handleVerify(self){
    let url= '/nccloud/gl/verify/onverify.do';
    let {creditDataArr,creditSelectedData,debitSelectedData,queryCondition}=self.state;
    creditSelectedData=self.filterSelectedData('credit');//获取借方选中的数据
    debitSelectedData=self.filterSelectedData('debit');//获取借方选中的数据
    if(debitSelectedData.length<=0||creditSelectedData.length<=0){
        toast({ content: self.state.json['20020VRIFYPAGE-000014'], color: 'warning' });/* 国际化处理： 请选择借贷两方数据进行核销*/
        return false;
    }
    if(self.handleSumCY('debit')!=self.handleSumCY('credit')){
        promptBox({
            color:'info',
            title:self.state.json['20020VRIFYPAGE-000009'],/* 国际化处理： 提示*/
            content:self.state.json['20020VRIFYPAGE-000015'],/* 国际化处理： 参加核销的借贷方金额不相等，是否强制核销？*/
            noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
            noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
            beSureBtnName:self.state.json['20020VRIFYPAGE-000011'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
            cancelBtnName:self.state.json['20020VRIFYPAGE-000012'],           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
            beSureBtnClick: dealOperate.bind(self,url,creditSelectedData,debitSelectedData,queryCondition),   // 确定按钮点击调用函数,非必输
            cancelBtnClick: cancelBtnClick.bind(self)  // 取消按钮点击调用函数,非必输
        })
    }else{
        dealOperate.call(self,url,creditSelectedData,debitSelectedData,queryCondition);
    }
}
    export function dealOperate(url,creditSelectedData,debitSelectedData,queryCondition){
        let self=this;
        let {creditDataArr,checkedAllDebit,checkedAllCredit,firstCheckedDatas,resourceData_credit,resourceData_debit,
            checkedArrayCredit,checkedArrayDebit,creditBoxClicked,debitBoxClicked,creditOrDebitFlag,voucherVerifyflag}=self.state;
    
    if(voucherVerifyflag=='0'){//及时核销，不能把本方的选中状态去掉
        if(creditOrDebitFlag=='debit'){
            checkedAllCredit=false;
            checkedArrayCredit=[];
            queryCondition.end_date=debitSelectedData[0].m_prepareddate.display;
        }else if(creditOrDebitFlag=='credit'){
            checkedAllDebit=false;
            checkedArrayDebit=[];
            queryCondition.end_date=creditSelectedData[0].m_prepareddate.display;
        }
        
    }else{
        checkedAllDebit=false;
        checkedAllCredit=false;
        checkedArrayCredit=[];
        checkedArrayDebit=[];
    }
    let queryData={
        "credit":creditSelectedData,
        "debit":debitSelectedData,
        "condition":queryCondition
    }
    firstCheckedDatas.type='';//借方还是贷方
    firstCheckedDatas.assinfo={};//选中行的严格控制的辅助核算信息
    ajax({
        url:url,
        data:queryData,
        success: function(response){
            const { data, success } = response;
            if(success){
                let creditData = data.credit;
                let debitData = data.debit;
                creditBoxClicked=false;
                debitBoxClicked=false;
                if(creditData){
                    creditData.map((item,i)=>{
                        item.key=i;
                        //定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancecreditamount={
                            display:item.m_Balancecreditamount.value,
                            value:item.m_Balancecreditamount.value
                        }
                        item.empty_m_Balancelocalcreditamount={
                            display:item.m_Balancelocalcreditamount.value,
                            value :item.m_Balancelocalcreditamount.value
                        }
                        if(voucherVerifyflag=='0'&&creditOrDebitFlag=='credit'){//及时核销，不能把本方的选中状态去掉
                            item.m_dCredit_Money_Y=deepClone(item.m_Balancecreditamount);
                            item.m_dCredit_Money_B=deepClone(item.m_Balancelocalcreditamount);
                            item.m_Balancecreditamount.display=0;
                            item.m_Balancecreditamount.value=0;
                            item.m_Balancelocalcreditamount.display=0;
                            item.m_Balancelocalcreditamount.value=0;
                            creditBoxClicked=true;
                            if (!firstCheckedDatas.type) {//如果未选中一行数据，先选中第一行
                                creditData[0].m_voAss.map((item, index) => {
                                    if (queryCondition.isControlItems.indexOf(item.m_pk_checktype) != -1) {
                                        firstCheckedDatas.assinfo[item.m_pk_checktype] = item.m_pk_checkvalue;
                                    }
                                })
                                firstCheckedDatas.type = 'credit';
                            }
                        }else{
                            checkedArrayCredit.push(false);
                        }
                    })
                }else{
                    creditData=[];
                    checkedArrayCredit=[];
                }
                if(debitData){
                    debitData.map((item,i)=>{
                        item.key=i;
                        //定义两个临时变量存储未核销的原币和组织本币
                        //定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancedebitamount={
                            display:item.m_Balancedebitamount.value,
                            value:item.m_Balancedebitamount.value
                        }
                        item.empty_m_Balancelocaldebitamount={
                            display:item.m_Balancelocaldebitamount.value,
                            value:item.m_Balancelocaldebitamount.value
                        }
                        if(voucherVerifyflag=='0'&&creditOrDebitFlag=='debit'){//及时核销，不能把本方的选中状态去掉
                            item.m_dDebit_Money_Y=deepClone(item.m_Balancedebitamount);
                            item.m_dDebit_Money_B=deepClone(item.m_Balancelocaldebitamount);
                            item.m_Balancedebitamount.display=0;
                            item.m_Balancedebitamount.value=0;
                            item.m_Balancelocaldebitamount.display=0;
                            item.m_Balancelocaldebitamount.value=0;
                            debitBoxClicked=true;
                            if (!firstCheckedDatas.type) {//如果未选中一行数据，先选中第一行
                                debitData[0].m_voAss.map((item, index) => {
                                    if (queryCondition.isControlItems.indexOf(item.m_pk_checktype) != -1) {
                                        firstCheckedDatas.assinfo[item.m_pk_checktype] = item.m_pk_checkvalue;
                                    }
                                })
                                firstCheckedDatas.type = 'debit';
                            }
                        }else{
                            checkedArrayDebit.push(false);
                        }
                    })
                }else{
                    debitData=[];
                    checkedArrayDebit=[];
                }
                resourceData_credit=JSON.parse(JSON.stringify(creditData));
                resourceData_debit=JSON.parse(JSON.stringify(debitData));
                self.setState({
                    resourceData_credit,resourceData_debit,
                    creditDataArr: creditData,
                    debitDataArr: debitData,
                    creditBoxClicked,//贷方选中
			        debitBoxClicked,//借方选中
                    checkedArrayCredit,checkedArrayDebit,checkedAllDebit,checkedAllCredit,
                    firstCheckedDatas
                },()=>{
                    verifyUnverifySum(self);
                    if(voucherVerifyflag=='0'){
                        self.props.refreshVoucherData();//及时核销成功刷新凭证卡片数据
                    }
                })
            } else {
                toast({ content: self.state.json['20020VRIFYPAGE-000006'], color: 'warning' });/* 国际化处理： 没有获取数据*/
            }
        }
    });
}
export function cancelBtnClick(){
    return false;
}
