import {ajax,base} from "nc-lightapp-front";
const pageCode = "20020VRIFYPAGE";
export default function(props) {
  let appcode = props.getSearchParam("c");
  ajax({
    url: "/nccloud/platform/appregister/queryallbtns.do",
    data: {
      pagecode: pageCode,
      appcode: appcode //小应用id
    },
    async: false,
    success: function(res) {
      if (res.data) {
				let button = res.data;
				
        props.button.setButtons(button, () => {
          // 设置进入页面时的不可用按钮
          const disabledButtonsArr = [
            "verificationCancel",
            "RB_hedging",
            "auto_verificationCancel",
            "auto_RB_hedging",
            "full_match",
            "cancel_match",
            "contrast",
            "cancel_contrast",
            'link_voucher'
					];
          props.button.setButtonDisabled(disabledButtonsArr, true);
        });
      }
    }
  });
}
