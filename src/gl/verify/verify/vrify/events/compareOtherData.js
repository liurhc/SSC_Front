import {ajax} from 'nc-lightapp-front';
import verifyUnverifySum from './verifyUnverifySum';
export default function compareOtherData(self,type,status){
    let {CompareStatus,creditSelectedData,debitSelectedData,checkedArrayDebit,checkedArrayCredit,
        resourceData_credit,resourceData_debit,compareData_Credit,compareData_Debit,
        creditDataArr,debitDataArr}=self.state;
    let currentData=[];//多虑对照对来的数据
    
    if(type=='credit'){
        checkedArrayDebit=[];
        creditSelectedData=self.filterSelectedData('credit');//获取借方选中的数据
        if(creditSelectedData.length>0){
            compareData_Debit=true;
            let credit_assid=creditSelectedData[0].m_assid?creditSelectedData[0].m_assid.value:'';
                resourceData_debit.map((item,index)=>{//每次对照都取原始数据去对照
                if(item.m_pk_unit.value==creditSelectedData[0].m_pk_unit.value&&item.m_sSubjCode.value==creditSelectedData[0].m_sSubjCode.value
                    &&(item.m_assid?item.m_assid.value:'')==credit_assid&&item.m_debitamount.value==creditSelectedData[0].m_creditamount.value){
                    currentData.push(item);
                    checkedArrayDebit.push((item.m_bSelected=='Y')?true:false);
                }
            })
        }else{
            currentData=resourceData_debit;
            compareData_Debit=false;
            resourceData_debit.map((item,index)=>{
                checkedArrayDebit.push((item.m_bSelected=='Y')?true:false);
            })

        }
        self.setState({
            debitDataArr:currentData,
            checkedArrayDebit,
            compareData_Debit
        },()=>{
            verifyUnverifySum(self);
            // self.handleSumAmount('credit');
            self.handleSumAmount('debit'); 
        })
    }else if(type=='debit'){
        debitSelectedData=self.filterSelectedData('debit');//获取借方选中的数据
        checkedArrayCredit=[];
        if(debitSelectedData.length>0){
            compareData_Credit=true;
            let debit_assid=debitSelectedData[0].m_assid?debitSelectedData[0].m_assid.value:'';
            resourceData_credit.map((item,index)=>{//每次对照都取原始数据去对照
                if(item.m_pk_unit.value==debitSelectedData[0].m_pk_unit.value&&item.m_sSubjCode.value==debitSelectedData[0].m_sSubjCode.value
                    &&(item.m_assid?item.m_assid.value:'')==debit_assid&&item.m_creditamount.value==debitSelectedData[0].m_debitamount.value){
                    currentData.push(item);
                    checkedArrayCredit.push((item.m_bSelected=='Y')?true:false);
                }
            })
        }else{
            currentData=resourceData_credit;
            compareData_Credit=false;
            resourceData_credit.map((item,index)=>{
                checkedArrayCredit.push((item.m_bSelected=='Y')?true:false);
            })
        }
        self.setState({
            creditDataArr:currentData,
            checkedArrayCredit,
            compareData_Credit
        },()=>{
            verifyUnverifySum(self);
            self.handleSumAmount('credit');
            // self.handleSumAmount('debit'); 
        })
    }
}
