import {ajax} from 'nc-lightapp-front';
/*过滤选中数据 */
export default function filterSelectedData(state,type){
    let {checkedArrayDebit,checkedArrayCredit,debitSelectedData,debitDataArr,creditSelectedData,creditDataArr}=this.state;
    creditSelectedData=[];
    debitSelectedData=[];
    if(type=="debit"){//借方
        for(var i = 0; i < checkedArrayDebit.length; i++){
            if(checkedArrayDebit[i]==true){
                debitSelectedData.push(debitDataArr[i]);
            }
        }
        return debitSelectedData;
    }else{//
        for(var i = 0; i < checkedArrayCredit.length; i++){
            if(checkedArrayCredit[i]==true){
                creditSelectedData.push(creditDataArr[i]);
            }
        }
        return creditSelectedData;
    }
}
