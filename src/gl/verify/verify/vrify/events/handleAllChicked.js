// 点击全选按钮 获取修改后的数据
export default function handleAllChicked(self,type) {
    let {debitDataArr,checkedArrayDebit,creditDataArr,checkedArrayCredit} = self.state;
    let obj = {};
    let brr = [];
    let count = 0;
    if(type=='debit'){
        let len = checkedArrayDebit.length;
        for(let i = 0; i < debitDataArr.length; i++) {
            brr.push({
                m_dDebit_Money_Y: debitDataArr[i].m_Balancedebitamount.value,
                m_Balancedebitamount: 0
            })
        }
        
        for( let i = 0; i < len; i++) {
            if(checkedArrayDebit[i] == true) {
                count++;
            }
        }
        //m_dDebit_Money_Y 本次核销原币 m_dDebit_Money_B组织本币 未核销余额 原币m_Balancedebitamount 组织本币m_Balancelocaldebitamount
        checkedArrayDebit.map((k, j) => {
            if (k) {
                debitDataArr[j].m_dDebit_Money_Y.display = brr[j].m_dDebit_Money_Y;
                debitDataArr[j].m_dDebit_Money_B.display = debitDataArr[j].m_Balancelocaldebitamount.value;
                debitDataArr[j].m_Balancedebitamount.display = brr[j].m_Balancedebitamount;
                debitDataArr[j].m_Balancelocaldebitamount.display = 0;
                debitDataArr[j].m_dDebit_Money_Y.value = brr[j].m_dDebit_Money_Y;
                debitDataArr[j].m_dDebit_Money_B.value = debitDataArr[j].m_Balancelocaldebitamount.value;
                debitDataArr[j].m_Balancedebitamount.value = brr[j].m_Balancedebitamount;
                debitDataArr[j].m_Balancelocaldebitamount.value = 0;
                debitDataArr[j].m_dDebit_Money_Y.prevValue = brr[j].m_dDebit_Money_Y;
                debitDataArr[j].m_dDebit_Money_B.prevValue = debitDataArr[j].m_Balancelocaldebitamount.value;
            } else {
                debitDataArr[j].m_dDebit_Money_Y.display = 0// brr[j].m_dDebit_Money_Y;
                debitDataArr[j].m_dDebit_Money_B.display = 0;
                debitDataArr[j].m_Balancedebitamount.display = debitDataArr[j].empty_m_Balancedebitamount.value;
                debitDataArr[j].m_Balancelocaldebitamount.display = debitDataArr[j].empty_m_Balancelocaldebitamount.value;
                debitDataArr[j].m_dDebit_Money_Y.value = 0//brr[j].m_dDebit_Money_Y;
                debitDataArr[j].m_dDebit_Money_B.value = 0;
                debitDataArr[j].m_Balancedebitamount.value = debitDataArr[j].empty_m_Balancedebitamount.value;
                debitDataArr[j].m_Balancelocaldebitamount.value = debitDataArr[j].empty_m_Balancelocaldebitamount.value;
                debitDataArr[j].m_dDebit_Money_Y.prevValue = 0//brr[j].m_dDebit_Money_Y;
                debitDataArr[j].m_dDebit_Money_B.prevValue = 0;
            }
        })
        return debitDataArr;
        // }
    }else if(type=='credit'){
        let len = checkedArrayCredit.length;
        for(let i = 0; i < creditDataArr.length; i++) {
            brr.push({
                m_dCredit_Money_Y: creditDataArr[i].m_Balancecreditamount.value,
                m_Balancecreditamount: 0
            })
        }
        
        for( let i = 0; i < len; i++) {
            if(checkedArrayCredit[i] == true) {
                count++;
            }
        }
        checkedArrayCredit.map((k, n) => {
            if (k) {
                creditDataArr[n].m_dCredit_Money_Y.display = brr[n].m_dCredit_Money_Y;
                creditDataArr[n].m_dCredit_Money_B.display = creditDataArr[n].m_Balancelocalcreditamount.value;
                creditDataArr[n].m_Balancecreditamount.display = brr[n].m_Balancecreditamount;
                creditDataArr[n].m_Balancelocalcreditamount.display = 0;
                creditDataArr[n].m_dCredit_Money_Y.value = brr[n].m_dCredit_Money_Y;
                creditDataArr[n].m_dCredit_Money_B.value = creditDataArr[n].m_Balancelocalcreditamount.value;
                creditDataArr[n].m_Balancecreditamount.value = brr[n].m_Balancecreditamount;
                creditDataArr[n].m_Balancelocalcreditamount.value = 0;
                creditDataArr[n].m_dCredit_Money_Y.prevValue = brr[n].m_dCredit_Money_Y;
                creditDataArr[n].m_dCredit_Money_B.prevValue = creditDataArr[n].m_Balancelocalcreditamount.value;
            } else {
                creditDataArr[n].m_dCredit_Money_Y.display = 0;// brr[n].m_dCredit_Money_Y;
                creditDataArr[n].m_dCredit_Money_B.display = 0;
                creditDataArr[n].m_Balancecreditamount.display = creditDataArr[n].empty_m_Balancecreditamount.value;
                creditDataArr[n].m_Balancelocalcreditamount.display = creditDataArr[n].empty_m_Balancelocalcreditamount.value;
                creditDataArr[n].m_dCredit_Money_Y.value = 0;// brr[n].m_dCredit_Money_Y;
                creditDataArr[n].m_dCredit_Money_B.value = 0;
                creditDataArr[n].m_Balancecreditamount.value = creditDataArr[n].empty_m_Balancecreditamount.value;
                creditDataArr[n].m_Balancelocalcreditamount.value = creditDataArr[n].empty_m_Balancelocalcreditamount.value;
                creditDataArr[n].m_dCredit_Money_Y.prevValue = 0// brr[n].m_dCredit_Money_Y;
                creditDataArr[n].m_dCredit_Money_B.prevValue = 0;
            }
        })
        return creditDataArr;
        // }
    }
}
