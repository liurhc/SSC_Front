import {ajax,deepClone,toast} from 'nc-lightapp-front';
import verifyUnverifySum from './verifyUnverifySum';
export default function handleAutoHedging(state,data){        
	let self=this;
	let url= '/nccloud/gl/verify/onAutoRedBlue.do';
	let {creditDataArr,creditSelectedData,debitSelectedData,queryCondition,
		checkedAllDebit,checkedAllCredit,checkedArrayCredit,checkedArrayDebit,
		resourceData_credit,resourceData_debit,creditBoxClicked,debitBoxClicked,firstCheckedDatas}=state;
	checkedAllDebit=false;
	checkedAllCredit=false;
	checkedArrayCredit=[];
	checkedArrayDebit=[];
	let standardData = deepClone(data);
	for(let k in standardData){
		if(k=='ass'){
			
		}else if(k=='pk_accasoa'||k=='pk_units'){
			childData[k].map((item,index)=>{
				childData[k][index]=item.value;
			})
		}else{
			standardData[k]=standardData[k].value?standardData[k].value:'';
		}			
	}
	standardData.pk_accasoa=queryCondition.pk_accasoa;
	standardData.pk_accountingbook=queryCondition.pk_accountingbook;
	creditSelectedData=resourceData_credit;
	debitSelectedData=resourceData_debit;
	let autoVerifyData={
		"credit":creditSelectedData,
		"debit":debitSelectedData,
		"condition":queryCondition,
		"standard":standardData
	}
	firstCheckedDatas.type='';//借方还是贷方
    firstCheckedDatas.assinfo={};//选中行的严格控制的辅助核算信息
	ajax({
		url:url,
		data:autoVerifyData,
		success: function(response){
			const { data, success } = response;
			if(success){
				let creditData = data.credit;
				let debitData = data.debit;
				if(creditData){
					creditData.map((item,i)=>{
						item.key=i;
						//定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancecreditamount={
                            display:item.m_Balancecreditamount.value,
                            value:item.m_Balancecreditamount.value
                        }
                        item.empty_m_Balancelocalcreditamount={
                            display:item.m_Balancelocalcreditamount.value,
                            value :item.m_Balancelocalcreditamount.value
                        }
						checkedArrayCredit.push(false);
					})
				}else{
					creditData=[];
				}
				if(debitData){
					debitData.map((item,i)=>{
						item.key=i;
						//定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancedebitamount={
                            display:item.m_Balancedebitamount.value,
                            value:item.m_Balancedebitamount.value
                        }
                        item.empty_m_Balancelocaldebitamount={
                            display:item.m_Balancelocaldebitamount.value,
                            value:item.m_Balancelocaldebitamount.value
                        }
						checkedArrayDebit.push(false);
					})
				}else{
					debitData=[]
                }
                resourceData_credit=deepClone(creditData);
                resourceData_debit=deepClone(debitData);
				self.setState({
                    resourceData_credit,resourceData_debit,
					hedgingStandardshowModal:!self.state.hedgingStandardshowModal,
					creditDataArr: creditData,
					debitDataArr: debitData,
					creditBoxClicked:false,//贷方选中
			        debitBoxClicked:false,//借方选中
					checkedArrayCredit,checkedArrayDebit,checkedAllDebit,checkedAllCredit,
					firstCheckedDatas
				},()=>{
					verifyUnverifySum(self);
				})
			} else {
				toast({ content: self.state.json['20020VRIFYPAGE-000006'], color: 'warning' });/* 国际化处理： 没有获取数据*/
			}
		},
		error:function(){
			self.setState({
				hedgingStandardshowModal:!self.state.hedgingStandardshowModal,firstCheckedDatas
			})
		}
	});
}
