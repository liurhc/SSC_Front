import {ajax,deepClone,promptBox,toast} from 'nc-lightapp-front';
import verifyUnverifySum from './verifyUnverifySum';
export default function handleHedging(self){
    let credit_sum=0,debit_sum=0;
    let url= '/nccloud/gl/verify/onRedBlue.do';
    let {creditDataArr,creditSelectedData,debitSelectedData,queryCondition,
        checkedAllDebit,checkedAllCredit,checkedArrayCredit,checkedArrayDebit,
        creditBoxClicked,debitBoxClicked,resourceData_credit,resourceData_debit,}=self.state;
    checkedAllDebit=false;
    checkedAllCredit=false;
    checkedArrayCredit=[];
    checkedArrayDebit=[];
    creditSelectedData=self.filterSelectedData('credit');//获取借方选中的数据
    debitSelectedData=self.filterSelectedData('debit');//获取借方选中的数据
    if(debitSelectedData.length<=0&&creditSelectedData.length<=0){
        toast({ content: self.state.json['20020VRIFYPAGE-000007'], color: 'warning' });/* 国际化处理： 至少选择借方或者贷方其中一方数据进行红蓝对冲*/
        return false;
    }
    if(debitSelectedData.length>0&&creditSelectedData.length>0){
        toast({ content: self.state.json['20020VRIFYPAGE-000008'], color: 'warning' });/* 国际化处理： 只能选择借方或者贷方其中一方数据进行红蓝对冲*/
        return false;
    }
    if(creditSelectedData.length>0){
        creditSelectedData.map((item,index)=>{
            credit_sum+=item.m_dCredit_Money_Y.value-0;
        })
    }
    if(debitSelectedData.length>0){
        debitSelectedData.map((item,index)=>{
            debit_sum+=item.m_dDebit_Money_Y.value-0;
        })
    }
    if((creditSelectedData.length>0&&credit_sum!=0)||(debitSelectedData.length>0&&debit_sum!=0)){
        promptBox({
            color:'info',
            title:self.state.json['20020VRIFYPAGE-000009'],/* 国际化处理： 提示*/
            content:self.state.json['20020VRIFYPAGE-000010'],/* 国际化处理： 红蓝对冲金额不相等是否强制做红蓝对冲？*/
            noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
            noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
            beSureBtnName:self.state.json['20020VRIFYPAGE-000011'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
            cancelBtnName:self.state.json['20020VRIFYPAGE-000012'],           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
            beSureBtnClick: dealOperate.bind(self,url,creditSelectedData,debitSelectedData,queryCondition),   // 确定按钮点击调用函数,非必输
            cancelBtnClick: cancelBtnClick.bind(self)  // 取消按钮点击调用函数,非必输
        })
    }else{
        let queryData={
            "credit":creditSelectedData,
            "debit":debitSelectedData,
            "condition":queryCondition
        }
        ajax({
            url:url,
            data:queryData,
            success: function(response){
                const { data, success } = response;
                if(success){
                    let creditData = data.credit;
                    let debitData = data.debit;
                    if(creditData){
                        creditData.map((item,i)=>{
                            item.key=i;
                            //定义两个临时变量存储未核销的原币和组织本币
                            item.empty_m_Balancecreditamount={
                                display:item.m_Balancecreditamount.value,
                                value:item.m_Balancecreditamount.value
                            }
                            item.empty_m_Balancelocalcreditamount={
                                display:item.m_Balancelocalcreditamount.value,
                                value :item.m_Balancelocalcreditamount.value
                            }
                            checkedArrayCredit.push(false);
                        })
                    }else{
                        creditData=[];
                    }
                    if(debitData){
                        debitData.map((item,i)=>{
                            item.key=i;
                            //定义两个临时变量存储未核销的原币和组织本币
                            item.empty_m_Balancedebitamount={
                                display:item.m_Balancedebitamount.value,
                                value:item.m_Balancedebitamount.value
                            }
                            item.empty_m_Balancelocaldebitamount={
                                display:item.m_Balancelocaldebitamount.value,
                                value:item.m_Balancelocaldebitamount.value
                            }
                            checkedArrayDebit.push(false);
                        })
                    }else{
                        debitData=[]
                    }
                    resourceData_credit=deepClone(creditData);
                resourceData_debit=deepClone(debitData);
                    self.setState({
                        resourceData_credit,resourceData_debit,
                        creditDataArr: creditData,
                        debitDataArr: debitData,
                        creditBoxClicked:false,//贷方选中
                        debitBoxClicked:false,//借方选中
                        checkedArrayCredit,checkedArrayDebit,checkedAllDebit,checkedAllCredit
                    },()=>{
                        verifyUnverifySum(self);
                    })
                } else {
                    toast({ content: self.state.json['20020VRIFYPAGE-000006'], color: 'warning' });/* 国际化处理： 没有获取数据*/
                }
            }
        });
    }
    
}
export function dealOperate(url,creditSelectedData,debitSelectedData,queryCondition){
    let self=this;
    let {checkedAllDebit,checkedAllCredit,checkedArrayCredit,checkedArrayDebit,}=self.state;
    checkedAllDebit=false;
    checkedAllCredit=false;
    checkedArrayCredit=[];
    checkedArrayDebit=[];
    let queryData={
        "credit":creditSelectedData,
        "debit":debitSelectedData,
        "condition":queryCondition
    }
    ajax({
        url:url,
        data:queryData,
        success: function(response){
            const { data, success } = response;
            if(success){
                let creditData = data.credit;
                let debitData = data.debit;
                if(creditData){
                    creditData.map((item,i)=>{
                        item.key=i;
                        //定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancecreditamount=item.m_Balancecreditamount;
                        item.empty_m_Balancelocalcreditamount =item.m_Balancelocalcreditamount ;
                        checkedArrayCredit.push(false);
                    })
                }else{
                    creditData=[];
                }
                if(debitData){
                    debitData.map((item,i)=>{
                        item.key=i;
                        //定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancedebitamount=item.m_Balancedebitamount;
                        item.empty_m_Balancelocaldebitamount =item.m_Balancelocaldebitamount ;
                        checkedArrayDebit.push(false);
                    })
                }else{
                    debitData=[]
                }
                self.setState({
                    creditDataArr: creditData,
                    debitDataArr: debitData,
                    creditBoxClicked:false,//贷方选中
                    debitBoxClicked:false,//借方选中
                    checkedArrayCredit,checkedArrayDebit,checkedAllDebit,checkedAllCredit
                },()=>{
                    verifyUnverifySum(self);
                })
            } else {
                toast({ content: self.state.json['20020VRIFYPAGE-000006'], color: 'warning' });/* 国际化处理： 没有获取数据*/
            }
        }
    });
}
export function cancelBtnClick(){
    return false;
}
