import {ajax,toast} from 'nc-lightapp-front';
import verifyUnverifySum from './verifyUnverifySum';

export default function handleQueryClick(state,data,isrefreah){
    let self=this;
    let {defaultStatu,mockData,creditDataArr,debitDataArr,checkedAllDebit,checkedAllCredit,checkedArrayCredit,checkedArrayDebit,
        modalDefaultValue,isControl,queryCondition,pk_accasoa,pk_accountingbook,resourceData_credit,resourceData_debit,CompareStatus,
        resourveQuery,creditOrDebitFlag,voucherVerifyflag,creditScale_Y,creditScale_B,debitScale_Y,debitScale_B,
        creditBoxClicked,debitBoxClicked,firstCheckedDatas} = state; 
    resourveQuery=data;
    let childData = JSON.parse(JSON.stringify(data));//deepClone(data);
    if(voucherVerifyflag=='0'){//及时核销，不能把本方的选中状态去掉
        childData.isControl=queryCondition.isControl;
        childData.isControlItems=queryCondition.isControlItems;
        if(creditOrDebitFlag=='debit'){
            checkedAllCredit=false;
            checkedArrayCredit=[];
        }else if(creditOrDebitFlag=='credit'){
            checkedAllDebit=false;
            checkedArrayDebit=[];
        }
    }else{
        checkedAllDebit=false;
        checkedAllCredit=false;
        checkedArrayCredit=[];
        checkedArrayDebit=[];
    }
    resourceData_credit=[];
    resourceData_debit=[];
    creditDataArr=[];
    debitDataArr=[];
    defaultStatu=false; 
    let url = '/nccloud/gl/verify/query.do';
    pk_accasoa.display=childData.pk_accasoa.display;
    pk_accountingbook.display=childData.pk_accountingbook.display;
    for(let k in childData){
        if(k=='ass'||k=='resourceAss'||k=='account_currency'||k=='cashtype'||k=='pk_org'||k=='isShowUnit'
        ||k=='isControl'||k=='endflag'||k=='isCrossAccountVerify'||k=='isControlItems'){
        }else if(k=='pk_units'){
            if(childData[k].length>0){
                if(childData[k][0].value==""){
                    childData[k]=null;
                }else{
                    childData[k].map((item,index)=>{
                        childData[k][index]=item.value;
                    })
                }
            }else{
                childData[k]=null;
            }
        }else{
            childData[k]=childData[k].value?childData[k].value:null;
        }
    }
    if(voucherVerifyflag=='0'){
        childData.type='0';//type=0是即时核销，type=1 是普通核销 
        childData.pk_detail=queryCondition.pk_detail?queryCondition.pk_detail:null;
    }else{
        childData.type='1';//type=0是即时核销，type=1 是普通核销 
    }
    isControl=childData.isControl;
    if(childData.isShowUnit==undefined){
        childData.isShowUnit=modalDefaultValue.isShowUnit;
    }
    if(childData.isCrossAccountVerify==undefined){
        childData.isCrossAccountVerify=modalDefaultValue.isCrossAccountVerify;
    }
    firstCheckedDatas.type='';//借方还是贷方
    firstCheckedDatas.assinfo={};//选中行的严格控制的辅助核算信息
    let creditData=[],debitData=[];
    ajax({
        url:url,
        data:childData,
        success: function(response){
            let { data, success } = response;
            if(success){
                creditDataArr = data.credit;
                debitDataArr = data.debit;

                if((creditDataArr&&creditDataArr.length>0) || (debitDataArr&&debitDataArr.length>0)){
                    if(isrefreah){
                        toast({ title: self.state.json['20020VRIFYPAGE-000104'], color: "success" })/* 国际化处理： 刷新成功*/
    
                       }else{
                        
                        let creditDataLen = creditDataArr&&creditDataArr.length ? creditDataArr.length: 0;
                        let debitDataLen = debitDataArr&&debitDataArr.length ? debitDataArr.length :0;
                        toast({ content: self.state.inlt && self.state.inlt.get('20020VRIFYPAGE-000101',{credit : creditDataLen , debit : debitDataLen}), color: "success" })
                       } 
                }else{
                    if(isrefreah){
                        toast({ title: self.state.json['20020VRIFYPAGE-000104'], color: "success" })

                    }else{
                        toast({ content: self.state.json['20020VRIFYPAGE-000100'], color: "warning" })/* 国际化处理： 未查询出符合条件的数据*/
                    }
                }
                
                if(creditDataArr&&creditDataArr.length>0){//贷方
                    creditScale_Y=creditDataArr[0].m_creditamount.scale;
                    creditScale_B=creditDataArr[0].m_localcreditamount.scale;
                    creditDataArr.map((item,i)=>{
                        item.key=i;
                        //定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancecreditamount={
                            display:item.m_Balancecreditamount.display,
                            value:item.m_Balancecreditamount.value
                        }
                        item.empty_m_Balancelocalcreditamount={
                            display:item.m_Balancelocalcreditamount.display,
                            value :item.m_Balancelocalcreditamount.value
                        }
                        if(voucherVerifyflag=='0'&&creditOrDebitFlag=='credit'){//及时核销，不能把本方的选中状态去掉
                            item.m_dCredit_Money_Y=JSON.parse(JSON.stringify(item.m_Balancecreditamount));
                            item.m_dCredit_Money_B=JSON.parse(JSON.stringify(item.m_Balancelocalcreditamount));
                            item.m_Balancecreditamount.display=0;
                            item.m_Balancecreditamount.value=0;
                            item.m_Balancelocalcreditamount.display=0;
                            item.m_Balancelocalcreditamount.value=0;
                            creditBoxClicked=true;
                            if (!firstCheckedDatas.type&&creditDataArr[0].m_voAss) {//如果未选中一行数据，先选中第一行
                                creditDataArr[0].m_voAss.map((item, index) => {
                                    if (queryCondition.isControlItems.indexOf(item.m_pk_checktype) != -1) {
                                        firstCheckedDatas.assinfo[item.m_pk_checktype] = item.m_pk_checkvalue;
                                    }
                                })
                                firstCheckedDatas.type = 'credit';
                            }
                        }else{
                            checkedArrayCredit.push(false);
                        }
                        
                        defaultStatu=true;
                    })
                    
                }else{
                    creditDataArr=[];
                }
                if(debitDataArr&&debitDataArr.length>0){//借方
                    debitScale_Y=debitDataArr[0].m_debitamount.scale;
                    debitScale_B=debitDataArr[0].m_localdebitamount.scale;
                    debitDataArr.map((item,i)=>{
                        item.key=i;
                        //定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancedebitamount={
                            display:item.m_Balancedebitamount.display,
                            value:item.m_Balancedebitamount.value
                        }
                        item.empty_m_Balancelocaldebitamount={
                            display:item.m_Balancelocaldebitamount.display,
                            value:item.m_Balancelocaldebitamount.value
                        }
                        if(voucherVerifyflag=='0'&&creditOrDebitFlag=='debit'){//及时核销，不能把本方的选中状态去掉
                            item.m_dDebit_Money_Y=JSON.parse(JSON.stringify(item.m_Balancedebitamount));
                            item.m_dDebit_Money_B=JSON.parse(JSON.stringify(item.m_Balancelocaldebitamount));
                            item.m_Balancedebitamount.display=0;
                            item.m_Balancedebitamount.value=0;
                            item.m_Balancelocaldebitamount.display=0;
                            item.m_Balancelocaldebitamount.value=0;
                            debitBoxClicked=true;
                            if (!firstCheckedDatas.type&&debitDataArr[0].m_voAss) {//如果未选中一行数据，先选中第一行
                                debitDataArr[0].m_voAss.map((item, index) => {
                                    if (queryCondition.isControlItems.indexOf(item.m_pk_checktype) != -1) {
                                        firstCheckedDatas.assinfo[item.m_pk_checktype] = item.m_pk_checkvalue;
                                    }
                                })
                                firstCheckedDatas.type = 'debit';
                            }
                        }else{
                            checkedArrayDebit.push(false);
                        }
                        defaultStatu=true;
                    })     
                              
                }else{
                    debitDataArr=[]; 
                }
                resourceData_credit=JSON.parse(JSON.stringify(creditDataArr));//$$creditData;
                resourceData_debit=JSON.parse(JSON.stringify(debitDataArr));//$$debitData;
                self.setState({
                    resourveQuery,
                    isControl,
                    defaultStatu,
                    CompareStatus:'compare',
                    isQueryShow: false,
                    resourceData_credit,resourceData_debit,
                    queryCondition:childData,
                    creditDataArr,
                    debitDataArr,
                    pk_accasoa,pk_accountingbook,
                    checkedArrayCredit,checkedArrayDebit,checkedAllDebit,checkedAllCredit,
                    creditScale_Y,creditScale_B,debitScale_Y,debitScale_B,creditBoxClicked,debitBoxClicked,
                    firstCheckedDatas
                },()=>{
                    verifyUnverifySum(self);
                    self.handleSumAmount('credit');
                    self.handleSumAmount('debit'); 
                    const disabledButtonsArr = [
                        "verificationCancel",
                        "RB_hedging",
                        "auto_verificationCancel",
                        "auto_RB_hedging",
                        "full_match",
                        "cancel_match",
                        "contrast",
                        "cancel_contrast",
                        'link_voucher',
                        'history_operation'
                                ];
                      self.props.button.setButtonDisabled(disabledButtonsArr, false);
                })

            } else {
                toast({ content: self.state.json['20020VRIFYPAGE-000006'], color: 'warning' });/* 国际化处理： 没有获取数据*/
            }
        },
        error:function(result){
            self.setState({
                resourveQuery,
                isControl,
                defaultStatu,
                isQueryShow: false,
                CompareStatus:'compare',
                resourceData_credit,resourceData_debit,
                    queryCondition:childData,
                    creditDataArr,
                    debitDataArr,
                    pk_accasoa,pk_accountingbook,
                    checkedArrayCredit,checkedArrayDebit,checkedAllDebit,checkedAllCredit,
                    firstCheckedDatas
            },()=>{
                verifyUnverifySum(self);
            })
            toast({ content: result.message?result.message:self.state.json['20020VRIFYPAGE-000006'], color: 'warning' });/* 国际化处理： 没有获取数据*/
        }
    });
}
