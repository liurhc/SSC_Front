import {toast } from 'nc-lightapp-front';
import {handleVerify, handleHedging,handleUnMatch} from "./index.js";
// import {voucherRelatedApp} from "../../../../public/components/oftenApi.js";
// import {voucher_link,voucher_gen} from "../../../../public/components/constJSON";
import { openToVoucher } from '../../../../public/common/voucherUtils';
export default function buttonClick(props, id) {
    switch (id) {
        case 'refresh':
        //刷新
        this.handleRefresh();
        break;
        // 查询
        case 'inquire':
            this.handleClick();
            break;

        // 核销
        case 'verificationCancel':
            handleVerify(this);
            break;

        // 红蓝对冲
        case 'RB_hedging':
            handleHedging(this);
            break;

        // 自动核销
        case 'auto_verificationCancel':
            this.handleAutoVerifyClick();
            break;

        // 自动红蓝对冲
        case 'auto_RB_hedging':
            this.handleAutoHedgingClick();
            break;

        // 历史查询
        case 'history_operation':
            this.handleQueryHistory();
            break;

        // 全匹配
        case 'full_match':
            this.handleAllMatch();
            break;

        // 取消匹配
        case 'cancel_match':
            handleUnMatch(this);
            break;

        // 对照
        case 'contrast':
            this.handleCompare();
            break;

        // 取消对照
        case 'cancel_contrast':
            this.handleUnCompare();
            break;

        // 切换布局
        case 'switch_layout':
            this.handleChangeLayout();
            break;

        case 'link_voucher'://联查凭证
        let self=this;
            let pk_voucher;
            let creditSelectedData=this.filterSelectedData('credit');//获取借方选中的数据
            let debitSelectedData=this.filterSelectedData('debit');//获取借方选中的数据
            if(debitSelectedData.length<=0&&creditSelectedData.length<=0){
                toast({ content: self.state.json['20020VRIFYPAGE-000000'], color: 'warning' });/* 国际化处理： 请选择一行数据进行联查凭证*/
                return false;
            }
            if(debitSelectedData.length>1||creditSelectedData.length>1||(debitSelectedData.length==1&&creditSelectedData.length==1)){
                toast({ content: self.state.json['20020VRIFYPAGE-000001'], color: 'warning' });/* 国际化处理： 只支持单行数据进行联查凭证*/
                return false;
            }
            if(creditSelectedData.length==1){
                pk_voucher=creditSelectedData[0].m_Pk_voucher.value;
            }
            if(debitSelectedData.length==1){
                pk_voucher=debitSelectedData[0].m_Pk_voucher.value;
            }
            let param = {
                pk_voucher: pk_voucher,
                titlename: self.state.json['20020VRIFYPAGE-000002'],/* 国际化处理： 凭证联查*/
			}
			openToVoucher(self,param)
            // let voucherApp=voucherRelatedApp(voucher_link);
            // self.props.openTo('/gl/gl_voucher/pages/main/index.html#Welcome',
            //     {
            //         appcode:voucherApp.appcode,
            //         c:voucherApp.appcode,
            //         id:pk_voucher,
            //         pagekey:'link',
            //         n:self.state.json['20020VRIFYPAGE-000002'],/* 国际化处理： 凭证联查*/
            //         status:'browse'

            //     }
            // )
            break;    
        default:
        break;

    }
}
