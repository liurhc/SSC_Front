import React, { Component } from 'react';
import {high,base,ajax,getBusinessInfo,deepClone,getMultiLang, promptBox } from 'nc-lightapp-front';
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../../public/components/FormItems';
const { Refer} = high;
import { toast } from '../../../../public/components/utils.js';
import createScript from '../../../../public/components/uapRefer.js';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm,NCDiv
} = base;
import getAssDatas from "../../../../public/components/getAssDatas/index.js";
import checkMustItem from "../../../../public/common/checkMustItem.js";
const {  NCFormItem:FormItem } = NCForm;
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};

  const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
  };


export default class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            assidCondition :{
                pk_accountingbook:'',// '1001A3100000000000PE',
                pk_accasoa:'',// '0001Z0100000000001BD',
                prepareddate:'',// '2018-07-18',
                pk_org:'',// '0001A310000000000NN6',
                assData:[],// assData,
                assid: "",//'0001Z31000000006G5A9',
                checkboxShow: true
            },//辅助核算组件需要的参数
            json:{},
            showModal:false,
            isShowUnit:false,//是否显示业务单元
            mnyScale:'2',//核销范围精度
            scale:'2',//原币精度
            orgscale:'2',//组织本币精度
            groupscale:'2',//集团本币币种精度
            globalscale:'2',//全局本币币种精度
            NC001:false,//集团本币 false不显示
            NC002:false,//全局本币
            cashtype:'',//是否是银行类科目 2的时候是
            isCrossAccountVerify:false,//是否只有末级可选
            assData: [//辅助核算信息
            ],
            childAssData: {
                accountingbook_org:'',//保存账簿对应的组织
                pk_org: '',
                pk_accountingbook: ''
              },//接受父组件传过来的参数
            modalDefaultValue:{},//默认数据
            SelectedAssData:[],//选中的数据
            loadData:[],//查询模板加载数据
            listItem:{},//模板数据对应值
            checkedAll:true,
            checkedArray: [],
        };
        this.close = this.close.bind(this);
    }
    componentWillReceiveProps (nextProp) {
        let {loadData,showOrHide,modalDefaultValue}=nextProp;//this.props;
        let self=this;
        let { listItem,showModal,isShowUnit,childAssData,isCrossAccountVerify,assData,checkedArray,
            scale,orgscale,groupscale,globalscale,NC001,NC002 }=self.state;
        if (showOrHide&&nextProp.loadData !== this.state.loadData&&this.state.loadData.length==0 ) {//nextProp.loadData !== this.state.loadData
            childAssData.pk_org=modalDefaultValue.pk_org;
            childAssData.accountingbook_org=modalDefaultValue.pk_org;
            isShowUnit=modalDefaultValue.isShowUnit;
            isCrossAccountVerify=modalDefaultValue.isCrossAccountVerify;
            scale=modalDefaultValue.scale;
            orgscale=modalDefaultValue.orgscale;
            groupscale=modalDefaultValue.groupscale;
            globalscale=modalDefaultValue.globalscale;
            NC001=modalDefaultValue.NC001;
            NC002=modalDefaultValue.NC002;
            assData=modalDefaultValue.ass;
            assData.map((item,index)=>{
                item.key=index;
                checkedArray.push(true);
            })
            loadData.forEach((item,i)=>{
                let  key;
                if(item.itemType=='refer'){
                    if(item.itemKey=='pk_units'){
                        key=[{
                            display:'',
                            value:'',
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }]
                    }else if(item.itemKey=='pk_accountingbook'){
                        key={
                            display:modalDefaultValue.pk_accountingbook.display,
                            value:modalDefaultValue.pk_accountingbook.value,
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                    }else if(item.itemKey=='pk_currency'){
                        key={
                            display:modalDefaultValue.pk_currency.display,
                            value:modalDefaultValue.pk_currency.value,
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                    }else{
                        key={
                            display:'',
                            value:'',
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                    } 
                }else if(item.itemType=='select'||item.itemType=='Dbselect'||item.itemType=='radio'){//下拉框赋初始值
                    if(item.itemKey=='cmbDirect'){
                        key={
                            value:modalDefaultValue.cmbDirect
                        }
                    }else if(item.itemKey=='cmbMnyType'){
                        key={
                            value:modalDefaultValue.cmbMnyType ? modalDefaultValue.cmbMnyType : item.itemChild[0].value
                        }
                    }else{
                        key={
                            value:item.itemChild[0].value
                        }
                    }
                    
                }else if(item.itemType=='checkbox'){
                    item.itemChild[0].checked=modalDefaultValue.hasTally=='Y'?true:false;
                    key={
                        value:modalDefaultValue.hasTally
                    }
                }else{
                    key={
                        value:''
                    }
                    if(item.itemType=='date'||item.itemType=='Dbdate'){
                        if(item.itemKey=='begin_date'){
                            key = {
                              value:modalDefaultValue.begindate
                            };
                        }else if(item.itemKey=='end_date'){
                            key = {
                              value:modalDefaultValue.bizDate
                            };
                        }else{
                            key = {
                              value: ""
                            };
                        }
                    }
                }
                if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'){
                item.itemKey.map((k,index)=>{
                    let name= k;
                    listItem[name]=key
                });
                }else{
                    let name= item.itemKey;
                    listItem[name]=key
                }
            
            })
            if(this.props.voucherVerifyflag=='0'){//及时核销 设置科目并且不可编辑
                listItem['pk_accasoa']=modalDefaultValue.pk_accasoa;
            }
            if(this.props.voucherVerifyflag=='0'){//及时核销
                listItem['begin_date']={value:''};
                listItem['end_date']={value:modalDefaultValue.enddate};
            }else{
                listItem['begin_date']={value:modalDefaultValue.begindate};
                listItem['end_date']={value:modalDefaultValue.bizDate};
            }
            
            listItem.account_currency=modalDefaultValue.account_currency;
            listItem.pk_org = modalDefaultValue.pk_org;
            self.setState({
                modalDefaultValue:modalDefaultValue,
                loadData:loadData,
                showModal:showOrHide,assData,checkedArray,
                listItem,childAssData,isShowUnit,isCrossAccountVerify,
                scale,orgscale,groupscale,globalscale,NC001,NC002
            })
        }else{
            self.setState({
                showModal:showOrHide,childAssData,isShowUnit
            })
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (!nextProps.showOrHide && (nextProps.showOrHide == this.props.showOrHide) && nextProps.loadData == this.state.loadData) {
          return false;
        }
        return true;
      }
    //表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

    close() {
        this.props.handleClose();
    }

    confirm=()=>{
        let { listItem,assData,checkedArray,SelectedAssData,assidCondition } =this.state;
        let businessInfo = getBusinessInfo();
        let currrentDate = businessInfo.businessDate.split(' ')[0];
        SelectedAssData=[];
        let checkStatus=checkMustItem(listItem);//必输项校验
        if(!checkStatus.flag){
            toast({content:checkStatus.info+this.state.json['20020VRIFYPAGE-000103']+'!',color:'warning'});
            return false;
        }
        if(listItem['end_date'].value>currrentDate){
            toast({content:this.state.json['20020VRIFYPAGE-000089'],color:'warning'});/* 国际化处理： 核销终止日期不能晚于登录日期*/
            return false;
        }
        for(var i = 0; i < checkedArray.length; i++){
            if(checkedArray[i]==true){
                SelectedAssData.push(assData[i]);
            }
        }
        listItem.resourceAss=assData;
        listItem.ass=SelectedAssData;//assData;
        //TODO 校验辅助核算是否符合核销对象设置
        if(!this.isAssLegal(listItem, assData)){
            promptBox({
                color:'warning',
                content:this.state.json["20020VRIFYPAGE-000105"],/* 辅助核算不符合核销对象设置！ */
                noCancelBtn: true 
            });
            return;
        }

        this.setState({
            assidCondition
          },()=>{
            this.props.onConfirm(listItem);
          })
        // this.props.onConfirm(listItem);
    }
     //判断核销对象是否严格控制
     isControlFun=(pk_accountingbook,pk_accasoa)=>{
         let self=this;
         let {listItem}=self.state;
        let isControl;
        let url = '/nccloud/gl/verify/verifyobjinfo.do';
        let queryparam={
            'pk_accountingbook':pk_accountingbook,
            'pk_accasoa':pk_accasoa
        }
        ajax({
            url:url,
            data:queryparam,
            success:function(response){
                const { success } = response;
                if(success&&response.data){
                    isControl=response.data.verifyobj.header.m_bcontrol;//是否严格控制
                    listItem.isControlItems=[];
                    if(response.data.verifyobj.items){
                        response.data.verifyobj.items.map((item,index)=>{
                            listItem.isControlItems.push(item.m_pk_subjass);//哪个辅助核算项严格控制
                        })
                    }
                    listItem.isControl=isControl;
                    listItem.endflag=response.data.account.endflag;

                    self.setState({
                        listItem
                    })
                }
            },
            error:function(error){
            }
        })
    }

    isAssLegal = (listItem, assVOs) =>{
        if(!listItem.isControl){
            return true;
        }
        let controllPks = listItem.isControlItems;
        if(controllPks && controllPks.length > 0){
            if(!assVOs || assVOs.length <= 0){
                return false;
            }else{
                let assVONum = assVOs.length;
                for(var j = 0; j < controllPks.length; j++){
                    let pk = controllPks[j];
                    let i = 0;
                    for(i = 0; i<assVONum; i++){
                        let assVO = assVOs[i];
                        if(pk === assVO.pk_accassitem){
                            break;
                        }
                    }
                    if(i === assVONum){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    queryList=(data)=>{
        let self=this;
        function getNowFormatDate() {
            let date = new Date();
            let seperator1 = "-";
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            let currentdate = year + seperator1 + month + seperator1 + strDate;
            return currentdate;
        }
        // let currrentDate = getNowFormatDate();
        let businessInfo = getBusinessInfo();
        let currrentDate = businessInfo.businessDate.split(' ')[0];
        const dateInputPlaceholder = this.state.json['20020VRIFYPAGE-000083'];/* 国际化处理： 选择日期*/
        let { listItem,isShowUnit,mnyScale,scale,orgscale,groupscale,globalscale,NC001,NC002,assData,checkedArray,isCrossAccountVerify,childAssData,assidCondition,modalDefaultValue } =self.state;
			return data.length!=0?(
				data.map((item, i) => {                   
                   switch (item.itemType) {
                        case 'refer':
                            let referUrl= item.config.refCode+'/index.js';
                            let DBValue=[];
                            let defaultValue={}
                            if(listItem[item.itemKey].length){                           
                                listItem[item.itemKey].map((item,index)=>{
                                    DBValue[index]={ refname: item.display, refpk:item.value };
                                })
                            }else{
                                defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
                            }
                            //let defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
                            if(!self.state[item.itemKey]){
                                {createScript.call(self,referUrl,item.itemKey)}
                                return <div />
                            }else{
                                if(item.itemKey=='pk_accountingbook'){
                                    return(
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                           // isRequire={true}
                                            method="change"
                                        >
                                    {self.state[item.itemKey]?(self.state[item.itemKey])(
                                        {
                                            fieldid:item.itemKey,
                                            value:defaultValue,
                                            isMultiSelectedEnabled:false,
                                            showGroup:false,
                                            disabled:(self.props.voucherVerifyflag=='0')?true:false,
                                            disabledDataShow:true,
                                            queryCondition:() => {
                                                return Object.assign({
                                                    "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                                    "appcode":modalDefaultValue.appcode
                                                },config)
                                            },
                                            onChange:(v)=>{
                                                if(v.refpk){
                                                    //判断是否起用业务单元
                                                    let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
                                                    let pk_accpont = {"pk_accountingbook":v.refpk}
                                                    ajax({
                                                        url:url,
                                                        data:pk_accpont,
                                                        success: function(response){
                                                            const { success } = response;
                                                            //渲染已有账表数据遮罩
                                                            if (success) {
                                                                if(response.data){
                                                                    isShowUnit=response.data.isShowUnit;
                                                                    NC001=response.data.NC001;
                                                                    NC002=response.data.NC002;
                                                                    scale=response.data.scale;//原币精度
                                                                    orgscale=response.data.orgscale;//组织本币精度
                                                                    groupscale=response.data.groupscale;//集团本币币种精度
                                                                    globalscale=response.data.globalscale;//全局本币币种精度
                                                                    isCrossAccountVerify=response.data.isCrossAccountVerify;

                                                                    listItem.account_currency=response.data.currinfo.value;
                                                                    listItem['pk_currency'].value = response.data.currinfo.value;
                                                                    listItem['pk_currency'].display = response.data.currinfo.display;
                                                                    listItem['begin_date']={value:response.data.begindate};
                                                                    listItem['end_date']={value:response.data.bizDate};
                                                                    listItem.pk_org=response.data.unit.value;
                                                                    listItem.isCrossAccountVerify=isCrossAccountVerify;//是否跨末级
                                                                    listItem.isShowUnit=isShowUnit;
                                                                    childAssData.pk_org = response.data.unit.value;
                                                                    assidCondition.pk_org=response.data.unit.value;
                                                                    childAssData.accountingbook_org=response.data.unit.value;
                                                                }
                                                                self.setState({
                                                                    isShowUnit,scale,orgscale,groupscale,globalscale,NC001,NC002,isCrossAccountVerify,assidCondition
                                                                })
                                                            }   
                                                        }
                                                    });
                                                }
                                                assidCondition.pk_accountingbook=v.refpk;
                                                listItem[item.itemKey].value = v.refpk;
                                                listItem[item.itemKey].display = v.refname;
                                                //清空科目,业务单元，辅助核算项
                                                listItem['pk_accasoa'].display='';
                                                listItem['pk_accasoa'].value='';
                                                listItem['pk_units']=[{display:'',value:''}];
                                                assData=[];
                                                this.setState({
                                                    listItem,assidCondition,assData
                                                })
                                            }
                                        }
                                        ):<div/>}
                                    </FormItem>)
                                }else if(item.itemKey=='pk_accasoa'){
                                    return(
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                           // isRequire={true}
                                            method="change"
                                        >
                                    {self.state[item.itemKey] ? (
                                        self.state[item.itemKey])(
                                        {
                                            fieldid:item.itemKey,
                                            value:defaultValue,
                                            onlyLeafCanSelect:!isCrossAccountVerify,
                                            disabled:(self.props.voucherVerifyflag=='0')?true:false,
                                            queryCondition:() => {
                                                return Object.assign({
                                                    "refName":item.itemName,
                                                    "pk_accountingbook": listItem.pk_accountingbook.value? listItem.pk_accountingbook.value:'',
                                                    "dateStr":currrentDate,
                                                    "TreeRefActionExt":'nccloud.web.gl.verify.action.VerifyObjectRefSqlBuilder'
                                                    
                                                },config)    
                                            },
                                            onFocus:(v)=>{
                                                if(!listItem.pk_accountingbook.value){
                                                    toast({content:this.state.json['20020VRIFYPAGE-000090'],color:'warning'});/* 国际化处理： 请先选择核算账簿，核算账簿不能为空*/
                                                    return false;
                                                }
                                            },
                                            onChange:(v)=>{
                                                
                                                //根据选定的pk 实现过滤
                                                if(item.itemKey!='pk_accountingbook'&&!listItem.pk_accountingbook.value){
                                                    toast({ content: this.state.json['20020VRIFYPAGE-000091'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
                                                    return false
                                                }
                                                assData=[];
                                                checkedArray=[];
                                                if(v.refpk){
                                                    self.isControlFun(listItem.pk_accountingbook.value,v.refpk);
                                                    //请求辅助核算数据
                                                    let url = '/nccloud/gl/voucher/queryAssItem.do';
                                                    let queryData = {
                                                        pk_accasoa: v.refpk,
                                                        prepareddate: currrentDate,
                                                    };
                                                    ajax({
                                                        url:url,
                                                        data:queryData,
                                                        success: function(response){
                                                            const { success } = response;
                                                            //渲染已有账表数据遮罩
                                                            if (success) {
                                                                if(response.data){
                                                                    response.data.map((item,index)=>{
                                                                        checkedArray.push(true);
                                                                        let obj={
                                                                            key:index,
                                                                            "checktypecode":item.code,
                                                                            "checktypename" :item.name,
                                                                            "pk_Checktype": item.pk_accassitem,
                                                                            "refCode":item.refCode?item.refCode:item.code,
                                                                            "refnodename":item.refnodename,
                                                                            "pk_accassitem":item.pk_accassitem,
                                                                            "pk_accountingbook":listItem.pk_accountingbook.value,
                                                                            "classid": item.classid,
                                                                            "pk_defdoclist": item.classid,
                                                                        }
                                                                        assData.push(obj);
                                                                    })
                                                                }else{
                                                                    assData=[];
                                                                }
                                                                self.setState({
                                                                    assData,checkedArray
                                                                })
                                                            }   
                                                        }
                                                    });
                                                }
                                                listItem.cashtype=v.cashtype?v.cashtype:(v.nodeData&&v.nodeData.cashtype?v.nodeData.cashtype:'');
                                                listItem[item.itemKey].value = v.refpk;
                                                listItem[item.itemKey].display = v.refname;
                                                assidCondition.pk_accasoa=v.refpk;
                                                assidCondition.prepareddate=currrentDate;
                                                this.setState({
                                                    listItem,assidCondition,assData,checkedArray
                                                })
                                            }
                                            }
                                        ):<div/>}
                                    </FormItem>)
                                }else if(item.itemKey=='pk_units'){
                                    if(isShowUnit){
                                        return(
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                          //  isRequire={true}
                                            method="change"
                                        >
                                    {self.state[item.itemKey]?(self.state[item.itemKey])(
                                        {
                                            fieldid:item.itemKey,
                                            value:DBValue,
                                            isMultiSelectedEnabled:true,
                                            queryCondition:() => {
                                                    return Object.assign({
                                                        "pk_accountingbook": listItem.pk_accountingbook.value,
                                                        "TreeRefActionExt": 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                                    },config)
                                                },
                                            onChange:(v)=>{
                                                    //根据选定的pk 实现过滤
                                                    if(item.itemKey!='pk_accountingbook'&&!listItem.pk_accountingbook.value){
                                                        toast({ content: this.state.json['20020VRIFYPAGE-000091'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
                                                        return false
                                                    }
                                                    // if(v.length<=0){return false;}
                                                    listItem[item.itemKey]=[];
                                                    v.map((arr,index)=>{
                                                        let accasoa={
                                                            display:arr.refname,
                                                            value:arr.refpk
                                                        }
                                                        listItem[item.itemKey].push(accasoa);
                                                    })
                                                    if(v[0]){
                                                        listItem.pk_org=v[0].refpk;
                                                        childAssData.pk_org = v[0].refpk;
                                                        assidCondition.pk_org=v[0].refpk;
                                                    }else{
                                                        listItem.pk_org=childAssData.accountingbook_org;
                                                        childAssData.pk_org =childAssData.accountingbook_org;
                                                        assidCondition.pk_org=childAssData.accountingbook_org;
                                                    }
                                                    //辅助核算项的值
                                                    assData.map((item,index)=>{
                                                        item.checkvaluename = null;
                                                        item.pk_Checkvalue = null;
                                                        item.checkvaluecode = null;
                                                    })
                                                    // listItem[item.itemKey].value = v.refpk
                                                    // listItem[item.itemKey].display = v.refname
                                                    this.setState({
                                                        listItem,assidCondition,assData
                                                    })
                                            }
                                            }
                                        ):<div/>}
                                    </FormItem>)
                                    }else{
                                        return(<div/>)
                                    }
                                    
                                }else{
                                    return(
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                           // isRequire={true}
                                            method="change"
                                        >
                                    {self.state[item.itemKey]?(self.state[item.itemKey])(
                                        {
                                            fieldid:item.itemKey,
                                            value:defaultValue,
                                            disabled:(self.props.voucherVerifyflag=='0')?true:false,
                                            queryCondition:() => {
                                                    return Object.assign({
                                                        //"pk_accountingbook": self.state.pk_accountingbook.value
                                                    },config)
                                                },
                                            onChange:(v)=>{                                           
                                                    listItem[item.itemKey].value = v.refpk
                                                    listItem[item.itemKey].display = v.refname
                                                    this.setState({
                                                        listItem
                                                    })
                                            }
                                            }
                                        ):<div/>}
                                    </FormItem>)
                                }
                            }
                            break;
                        case 'date':
                            return (
                                    <FormItem
                                        inline={true}
                                        showMast={true}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        method="blur"
                                        inputAlfer="%"
                                        errorMessage={this.state.json['20020VRIFYPAGE-000092']}/* 国际化处理： 输入格式错误*/
                                    >
                                        <DatePicker
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                           // isRequire={true}
                                            //format={format}
                                            //disabled={isChange}
                                            //value={moment(listItem[item.itemKey].value)}
                                            //locale={zhCN}
                                            value={listItem[item.itemKey].value}
                                            onChange={(v) => {
                                                listItem[item.itemKey].value = v;
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                            placeholder={dateInputPlaceholder}
                                            />
                                        </FormItem>
                            );
                        case'Dbdate':
                            return(
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10} md={10} sm={10}
                                    labelName={item.itemName}
                                    method="blur"
                                    inputAlfer="%"
                                    errorMessage={this.state.json['20020VRIFYPAGE-000092']}/* 国际化处理： 输入格式错误*/
                                    inputAfter={
                                        <Col xs={12} md={12} sm={12} style={{paddingLeft:0}}>
                                            <span className="online">&nbsp;--&nbsp;</span>
                                            <div style={{display:'inline-block'}} >
                                                <DatePicker
                                                    fieldid="end_date"
                                                    name={item.itemKey}
                                                    type="customer"
                                                    value={listItem.end_date.value}
                                                    onChange={(v) => {
                                                        if(v>currrentDate){
                                                            toast({content:this.state.json['20020VRIFYPAGE-000089'],color:'warning'});/* 国际化处理： 核销终止日期不能晚于登录日期*/
                                                        }
                                                        listItem.end_date={value: v}
                                                        this.setState({
                                                            listItem
                                                        })
                                                    }}
                                                    placeholder={dateInputPlaceholder}
                                                />
                                            </div>
                                        </Col>
                                    }
                                    >
                                    <DatePicker
                                        fieldid="begin_date"
                                        name={item.itemKey}
                                        type="customer"
                                        value={listItem.begin_date.value}
                                        onChange={(v) => {
                                            listItem.begin_date={value: v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    />
                                </FormItem>
                            );
                        case 'textInput':
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                   // isRequire={true}
                                    method="change"
                                >
                                    <FormControl
                                        fieldid={item.itemKey}
                                        value={listItem[item.itemKey].value}
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            );
                        case 'DbtextInput':
                            return(
                                <Col xs={8}  md={8} sm={8} className="dateMargin labelMargin" style={{display:"none"}}>
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                   // labelXs={2}  labelSm={2} labelMd={2}
                                    xs={4}  md={4} sm={4}
                                    //labelName={item.itemName}
                                    // isRequire={true}
                                    method="change"
                                    inputAfter={
                                    <div style={{display:'inline-block'}} >
                                        <span className="online">&nbsp;--&nbsp;</span>   
                                        <div style={{display:'inline-block'}} >
                                            <NCNumber 
                                                fieldid="mny_end"
                                                scale={Number(mnyScale)}
                                                value={listItem.mny_end.value}
                                                onChange={(v) => {
                                                    listItem.mny_end={value : v}
                                                    this.setState({
                                                        listItem
                                                    })
                                                }}
                                            />
                                        </div>
                                    </div>
                                    }
                                >
                                    <NCNumber 
                                        fieldid="mny_begin"
                                        scale={Number(mnyScale)}
                                        value={listItem.mny_begin.value}
                                        onChange={(v) => {
                                            listItem.mny_begin={value : v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                                </Col>
                            );
                        case 'radio':
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                               // isRequire={true}
                                method="change"
                            >
                                <RadioItem
                                    // fieldid={item.itemKey}
                                    name={item.itemKey}
                                    type="customer"
                                    defaultValue={listItem[item.itemKey].value}
                                    items={() => {
                                        return (item.itemChild) 
                                    }}
                                    onChange={(v)=>{
                                        listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                    }}
                                />
                            </FormItem>
                            )
                        case 'select':
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                               // isRequire={true}
                                method="change"                               
                            >
                                <SelectItem 
                                    name={item.itemKey}
                                    fieldid={item.itemKey}
                                    defaultValue={listItem[item.itemKey].value} 
                                    disabled={(self.props.voucherVerifyflag=='0')?true:false}
                                    items = {
                                        () => {
                                                return item.itemChild;
                                            }
                                    }
                                    onChange={(v)=>{
                                        listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                    }}
                                />
                            </FormItem>)
                            case 'Dbselect':
                                let curreyArr=deepClone(item.itemChild);
                                if(item.itemKey=='cmbMnyType'){
                                    if(!NC001){
                                        curreyArr.map((list,index)=>{
                                            if(list.value=='2'){
                                                curreyArr.splice(index,1);
                                            }
                                        })
                                    }
                                    if(!NC002){
                                        curreyArr.map((list,index)=>{
                                            if(list.value=='3'){
                                                curreyArr.splice(index,1);
                                            }
                                        })
                                    }
                                }
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                               // isRequire={true}
                                method="change"
                                inputAfter={
                                    <Col xs={12}  md={12} sm={12} className="dateMargin labelMargin">
                                        <NCNumber 
                                            fieldid="mny_begin"
                                            scale={Number(mnyScale)}
                                            value={listItem.mny_begin.value}
                                            onChange={(v) => {
                                                listItem.mny_begin={value : v}
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                        />
                                        <span className="online">&nbsp;--&nbsp;</span>   
                                        <div style={{display:'inline-block'}} >
                                            <NCNumber 
                                                fieldid="mny_end"
                                                scale={Number(mnyScale)}
                                                value={listItem.mny_end.value}
                                                onChange={(v) => {
                                                    listItem.mny_end={value : v}
                                                    this.setState({
                                                        listItem
                                                    })
                                                }}
                                            />
                                        </div>
                                    </Col>
                                    }
                            >
                                <SelectItem name={item.itemKey}
                                    fieldid={item.itemKey}
                                    defaultValue={listItem[item.itemKey].value}
                                    items = {
                                        () => {
                                            if(item.itemKey=='cmbMnyType'){
                                                return (curreyArr);
                                            }else{
                                                return item.itemChild;
                                            }
                                        }
                                    }
                                    onChange={(v)=>{
                                        listItem[item.itemKey].value = v
                                        if(v&&v=='0'){//原币
                                            mnyScale=scale;
                                        }else if(v=='1'){//组织本币
                                            mnyScale=orgscale;
                                        }else if(v=='2'){//集团本币
                                            mnyScale=groupscale;
                                        }else if(v=='3'){//全局本币
                                            mnyScale=globalscale;
                                        }
                                        this.setState({
                                            listItem,mnyScale
                                        })
                                    }}
                                />
                            </FormItem>)
                        case 'checkbox':
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                xs={6}  md={6} sm={6}
                                // labelName={item.itemName}
                                // isRequire={true}
                                className='checkboxStyle'
                                method="change"
                            >
                                <CheckboxItem 
                                    name={item.itemKey} 
                                    fieldid={item.itemKey}
                                //defaultValue={this.state.periodloan.value} 
                                    boxs = {
                                        () => {
                                            return (item.itemChild) 
                                        }
                                    }
                                    onChange={(v)=>{
                                        listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                        this.setState({
                                            listItem
                                        })
                                    }}
                                />
                            </FormItem>
                            )
                        default:
                        break;
                   }
               })
			):<div/>;
           
    }


    onAllCheckChange = () => {
        let self = this;
        let checkedArray = [];
        let selIds = [];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          checkedArray[i] = !self.state.checkedAll;
        }
        self.setState({
          checkedAll: !self.state.checkedAll,
          checkedArray: checkedArray,
        });
      };
      onCheckboxChange = (text, record, index) => {
        let self = this;
        let allFlag = false;
        let checkedArray = self.state.checkedArray.concat();
        checkedArray[index] = !self.state.checkedArray[index];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          if (!checkedArray[i]) {
            allFlag = false;
            break;
          } else {
            allFlag = true;
          }
        }
        self.setState({
          checkedAll: allFlag,
          checkedArray: checkedArray,
        });
      };
      
      renderColumnsMultiSelect(columns) {
        const {checkedArray } = this.state;
        const { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        // let indeterminate_bool1 = true;
        if (multiSelect && multiSelect.type === "checkbox") {
          let i = checkedArray.length;
          while(i--){
              if(checkedArray[i]){
                indeterminate_bool = true;
                break;
              }
          }
          let defaultColumns = [
            {
              title: <div fieldid="firstcol">{(
                <Checkbox
                  className="table-checkbox"
                  checked={this.state.checkedAll}
                  indeterminate={indeterminate_bool&&!this.state.checkedAll}
                  onChange={this.onAllCheckChange}
                />
            )}</div>,
              key: "checkbox",
              dataIndex: "checkbox",
              width: 50,
              render: (text, record, index) => {
                return (<div fieldid="firstcol">
                  <Checkbox
                    className="table-checkbox"
                    checked={this.state.checkedArray[index]}
                    onChange={this.onCheckboxChange.bind(this, text, record, index)}
                  />
                  </div>
                );
              }
            }
          ];
          columns = defaultColumns.concat(columns);
        }
        return columns;
      }
      componentWillMount() {

        let callback= (json) =>{
          this.setState({json:json},()=>{})
        }
        getMultiLang({moduleId:['20020VRIFYPAGE','publiccommon','publiccomponents'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    render() {
        let{showOrHide}=this.props;
        let { loadData,assData,assidCondition,checkedAll,checkedArray,childAssData} =this.state;
        let columns10 = [
            {
              title: this.state.json['20020VRIFYPAGE-000004'],/* 国际化处理： 核算类型*/
              dataIndex: "checktypename",
              key: "checktypename",
              width: 140,
              render: (text, record, index) => {
                return <span>{record.checktypename}</span>;
              }
            },
            {
              title: this.state.json['20020VRIFYPAGE-000005'],/* 国际化处理： 核算内容*/
              dataIndex: "checkvaluename",
              key: "checkvaluename",
              width: 140,
              render: (text, record, index) => {
                let { assData,childAssData } = this.state;
                let defaultValue = [];
                if (assData[index]["checkvaluename"]) {
                  assData[index]["checkvaluename"].split(",").map((item, _index) => {
                    defaultValue[_index] = { refname: item, refpk: "" };
                  });
                  assData[index]["pk_Checkvalue"].split(",").map((item, _index) => {
                    defaultValue[_index].refpk = item;
                  });
                } else {
                  defaultValue = [{ refname: "", refpk: "" }];
                }
                if (record.refnodename) {
                  let referUrl = record.refnodename + '.js';
                  if (!this.state[record.refnodename]) {
                    { createScript.call(this, referUrl, record.refnodename) }
                    return <div />
                  } else {
                    if (record.classid == 'b26fa3cb-4087-4027-a3b6-c83ab2a086a9' || record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                      return (
                        <FormItem
                          inline={true}
                          // showMast={true}
                          labelXs={2} labelSm={2} labelMd={2}
                          xs={10} md={10} sm={10}
                          //labelName={record.itemName}
                          //isRequire={true}
                          method="change"
                        >
                          {this.state[record.refnodename] ? 
                          (this.state[record.refnodename])(
                            {
                              value: defaultValue,
                              isShowUnit: true,
                              unitProps: {
                                refType: 'tree',
                                refName: this.state.json['20020VRIFYPAGE-000017'],/* 国际化处理： 业务单元*/
                                refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                rootNode: { refname: this.state.json['20020VRIFYPAGE-000017'], refpk: 'root' },/* 国际化处理： 业务单元*/
                                placeholder: this.state.json['20020VRIFYPAGE-000017'],/* 国际化处理： 业务单元*/
                                queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                treeConfig: { name: [this.state.json['20020VRIFYPAGE-000084'], this.state.json['20020VRIFYPAGE-000085']], code: ['refcode', 'refname'] },/* 国际化处理： 编码,名称*/
                                isMultiSelectedEnabled: false,
                                //unitProps:unitConf,
                                isShowUnit: false
                              },
                              unitCondition:{
                                pk_financeorg:childAssData.pk_org,
                                'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                              },
                              isMultiSelectedEnabled: true,
                              "unitValueIsNeeded":false,
                              "isShowDimission":true,
                              queryCondition: () => {
                                  config.isShowDimission=true;
                                    return Object.assign({
                                      "busifuncode": "all",
                                      "pk_org": childAssData.pk_org
                                    },config)
                              },
                              onChange: (v) => {
                                let { assData } = this.state;
                                let originData = this.findByKey(record.key, assData);
                                let refnameArr = [], refpkArr = [], refcodeArr = [];
                                if (originData) {
                                  v.map((arr, index) => {
                                    refnameArr.push(arr.refname);
                                    refpkArr.push(arr.refpk);
                                    refcodeArr.push(arr.refcode);
      
                                  })
                                  originData.checkvaluename = (v.length>0)?refnameArr.join():null;
                                  originData.pk_Checkvalue = (v.length>0)?refpkArr.join():null;
                                  originData.checkvaluecode = (v.length>0)?refcodeArr.join():null;
                                }
                                childAssData.assData = assData;
                                this.setState({
                                  assData, childAssData
                                })
                              }
                            }) : <div />}
                        </FormItem>);
                    } else {
                      return (
                        <FormItem
                          inline={true}
                          // showMast={true}
                          labelXs={2} labelSm={2} labelMd={2}
                          xs={10} md={10} sm={10}
                          //labelName={record.itemName}
                         // isRequire={true}
                          method="change"
                        >
                          {this.state[record.refnodename] ? (this.state[record.refnodename])(
                            {
                              value: defaultValue,
                              isMultiSelectedEnabled: true,
                              queryCondition: () => {
                                if (record.classid && record.classid.length == 20) {//classid的长度等于20的话过滤条件再加一个pk_defdoclist
                                  return Object.assign({
                                    "pk_org": childAssData.pk_org,
                                    "pk_defdoclist": record.pk_defdoclist
                                  },config)
                                } else {
                                  return Object.assign({
                                    "pk_org": childAssData.pk_org
                                  },config)
                                }
                              },
                              onChange: (v) => {
                                let { assData } = this.state;
                                let originData = this.findByKey(record.key, assData);
                                let refnameArr = [], refpkArr = [], refcodeArr = [];
                                if (originData) {
                                  v.map((arr, index) => {
                                    refnameArr.push(arr.refname);
                                    refpkArr.push(arr.refpk);
                                    refcodeArr.push(arr.refcode);
      
                                  })
                                  originData.checkvaluename = (v.length>0)?refnameArr.join():null;
                                  originData.pk_Checkvalue = (v.length>0)?refpkArr.join():null;
                                  originData.checkvaluecode = (v.length>0)?refcodeArr.join():null;
                                }
                                childAssData.assData = assData;
                                this.setState({
                                  assData, childAssData
                                })
                              }
                            }) : <div />}
                        </FormItem>);
                    }
                  }
                } else {//不是参照的话要区分日期、字符、数值
                  if (record.classid == 'BS000010000100001033') {//日期
                    return (
                      <DatePicker
                        //name={item.itemKey}
                        type="customer"
                       // isRequire={true}
                        placeholder={dateInputPlaceholder}
                        value={defaultValue[0].refname}
                        onChange={(v) => {
                          let { assData } = this.state;
                          let originData = this.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = v?assArr.join():null;
                            originData.pk_Checkvalue = v?assArr.join():null;
                            originData.checkvaluecode = v?assArr.join():null;
                          }
                          childAssData.assData = assData;
                          this.setState({
                            assData, childAssData
                          })
                        }}
      
                      />
                    )
                  } else if (record.classid == 'BS000010000100001031') {//数值
                    return (
                      <NCNumber
                        scale={2}
                        value={defaultValue[0].refname}
                        placeholder={this.state.json['20020VRIFYPAGE-000086']}/* 国际化处理： 请输入数字*/
                        onChange={(v) => {
                          let { assData } = this.state;
                          let originData = this.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = v?assArr.join():null;
                            originData.pk_Checkvalue = v?assArr.join():null;
                            originData.checkvaluecode = v?assArr.join():null;
                          }
                          childAssData.assData = assData;
                          this.setState({
                            assData, childAssData
                          })
                        }}
                      />
                    )
                  } else if (record.classid == 'BS000010000100001032') {//布尔
                    return (
                      <FormItem
                        inline={true}
                        // showMast={false}
                        labelXs={2}
                        labelSm={2}
                        labelMd={2}
                        xs={10}
                        md={10}
                        sm={10}
                        // labelName={item.itemName}
                        //isRequire={true}
                        method="change"
                      >
                        <SelectItem name={record.checktypecode}
                          defaultValue={defaultValue[0].refname}
                          items={
                            () => {
                              return ([{
                                label: this.state.json['20020VRIFYPAGE-000087'],/* 国际化处理： 是*/
                                value: 'Y'
                              }, {
                                label: this.state.json['20020VRIFYPAGE-000088'],/* 国际化处理： 否*/
                                value: 'N'
                              }])
                            }
                          }
                          onChange={(v) => {
                            let { assData } = this.state;
                            let originData = this.findByKey(record.key, assData);
                            if (originData) {
                              let assArr = [];
                              assArr.push(v);
                              originData.checkvaluename = v?assArr.join():null;
                              originData.pk_Checkvalue = v?assArr.join():null;
                              originData.checkvaluecode = v?assArr.join():null;
                            }
                            childAssData.assData = assData;
                            this.setState({
                              assData, childAssData
                            })
                          }}
                        />
                      </FormItem>
                    )
                  } else {//字符
                    return (
                      <FormControl
                        value={defaultValue[0].refname}
                        onChange={(v) => {
                          let { assData } = this.state;
                          let originData = this.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = v?assArr.join():null;
                            originData.pk_Checkvalue = v?assArr.join():null;
                            originData.checkvaluecode = v?assArr.join():null;
                          }
                          childAssData.assData = assData;
                          this.setState({
                            assData, childAssData
                          })
                        }}
                      />
                    )
                  }
      
                }
              }
            }
          ];
        assidCondition.assData=assData;
        let columnsldad = this.renderColumnsMultiSelect(columns10);
        let pretentAssData={
            assData: assData||[],
            childAssData:childAssData||[],
            // accountingbook_org: '',//保存账簿对应的组织
            // pk_org:childAssData.pk_org,
            // pk_accountingbook: '',
            checkboxShow: true,//是否显示复选框
            showOrHide:true,
            checkedAll:checkedAll,//全选复选框的选中状态
            checkedArray:checkedArray||[],//复选框选中情况
            $_this:this
          };
        const emptyFunc = () => <span>{this.state.json['20020VRIFYPAGE-000094']}！</span>/* 国际化处理： 这里没有数据*/
        return (
            <div className="fl">
                <Modal
                    fieldid="query"
                    className={'combine'}
                    show={showOrHide }
                    id="queryone"
                    // backdrop={ this.state.modalDropup }
                    onHide={ this.close }
                    animation={true}
                    >
                    <Modal.Header closeButton fieldid="header-area">
                        <Modal.Title>{this.state.json['20020VRIFYPAGE-000095']}</Modal.Title>{/* 国际化处理： 核销查询*/}
                    </Modal.Header >
                    <Modal.Body >
                        <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                            <NCForm useRow={true}
                                submitAreaClassName='classArea'
                                showSubmit={false}　>
                                {loadData.length > 0 ? this.queryList(loadData) : ''}
                            </NCForm>
                        </NCDiv>
                        <div className="getAssDatas">
                        {getAssDatas({pretentAssData})}
                        </div>
                        
                    </Modal.Body>
                    <Modal.Footer fieldid="bottom_area">
                        <Button colors="primary" onClick={ this.confirm } fieldid="confirm"> {this.state.json['20020VRIFYPAGE-000061']} </Button>{/* 国际化处理： 查询*/}
                        <Button onClick={ this.close } fieldid="close"> {this.state.json['20020VRIFYPAGE-000012']} </Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
SearchModal.defaultProps = defaultProps12;
