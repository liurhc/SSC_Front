import React, { Component } from "react";
import { toast, base, ajax, deepClone, createPage, getMultiLang,createPageIcon } from "nc-lightapp-front";
const {
    NCButtonGroup: ButtonGroup,
    NCButton: Button,
    NCRow: Row,
    NCCol: Col,
    NCTable: Table,
    NCCheckbox: Checkbox,
    NCNumber,NCAffix,NCDiv
} = base;
import QueryModal from "./queryModal/querymodal.js";
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import VerifyStandardModal from "./verifyStandardModal/verifyStandardModal.js";
import HeaderArea from '../../../public/components/HeaderArea';
import { buttonClick, initTemplate, iseqAssinfo } from './events';
import "./index.less";
import "./search.less";
import "./searchpublic.less";
import { accAdd, Subtr, accMul, accDiv } from "../../../public/common/method.js";
import {
    handleQueryClick,
    handleVerify,
    handleAutoVerify,
    handleAutoHedging,
    compareOtherData,
    matchOtherData, verifyUnverifySum
} from "./events/index.js";
const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
        type: "checkbox",
        param: "key"
    }
};
class ExportVrify extends Component {
    constructor(props) {
        super(props);
        this.state = {
            creditScale_Y:'2',//贷方本次核销金额原组的精度
            creditScale_B:'2',//贷方本次核销金额原组的精度
            debitScale_Y:'2',//借方本次核销金额原组的精度
            debitScale_B:'2',//借方本次核销金额原组的精度
            json: {},
            inlt:null,
            paraInfo: '',//存储参照核销数据传过来的paraInfo
            mockData: '',
            verifyAllDatas: {},//及时核销传过来的数据
            creditOrDebitFlag: '00',//及时核销 选中数据是借方还是贷方
            voucherVerifyflag: '',//0及时核销，1参照核销
            layout: 12,
            pk_currency_query: "", //查询条件所选币种
            isControl: false, //true为严格控制，false为非严格控制
            firstCheckedDatas: {
                type: '',//借方还是贷方
                assinfo: {}//选中行的严格控制的辅助核算信息
            },//第一个勾选的数据
            CompareStatus: "compare", //compare对照状态，uncompare取消对照
            defaultStatu: false, //false代表页面没数据，true为有数据
            allMatch: false, //全匹配 true，取消匹配 false
            isQueryShow: false, //查询对话框
            showModal: false,
            verifyStandardshowModal: false, //自动核销核销标准对话框
            hedgingStandardshowModal: false, //自动红蓝对冲标准对话框
            queryStyle: false,
            creditQueryStyle: true,
            debitQueryStyle: false,
            checkedAllDebit: false, //借方全选
            checkedAllCredit: false, //贷方全选
            checkedArrayCredit: [], //贷方
            checkedArrayDebit: [],//借方
            modalDefaultValue: {
                NC001: false,//集团本币 false不显示
                NC002: false,//全局本币
                scale: '2',//原币精度
                orgscale: '2',//组织本币精度
                groupscale: '2',//集团本币币种精度
                globalscale: '2',//全局本币币种精度
                isCrossAccountVerify: false,//是否跨末级科目
                pk_org: '',//账簿对应的组织
                bizDate: '',//业务日期
                yearbegindate: '',//年初日期
                begindate: '',
                enddate: '',
                bizPeriod: '',//账簿对应的会计期间
                isShowUnit: false,//是否显示业务单元
                isOpenUpBusiUnit: '',//
                pk_accperiodscheme: '',//会计期间方案
                pk_accountingbook: { display: '', value: '' },
                pk_currency: { display: '', value: '' },//币种
                appcode: this.props.getSearchParam("c"),
                cmbDirect: '-1',//方向 默认双向
                ass: [],//辅助核算信息
                hasTally: false,
                // cmbMnyType:this.state.json['20020VRIFYPAGE-000024']/* 国际化处理： 原币*/

            },//模态框里默认数据
            pk_accountingbook: { display: "", value: "" }, //核算账簿
            pk_accasoa: { display: "", value: "" }, //科目
            creditDataArr: [], //贷方数据
            debitDataArr: [], //借方数据
            creditBoxClicked: false, //贷方选中
            debitBoxClicked: false, //借方选中
            cancellationY_num: "",
            cancellationZ_num: "",
            creditUY_num: "", //贷方的未核销余额（原）
            creditUZ_num: "", //贷方的未核销余额（组）
            debitUY_num: "", //借方的未核销余额（原）
            debitUZ_num: "", //借方的未核销余额（组）
            SumAmount:{
                creditY_num: "", //贷方的核销余额（原）
                creditZ_num: "", //贷方的核销余额（组）
                creditUY_num: "", //贷方的未核销余额（原）
                creditUZ_num: "", //贷方的未核销余额（组）
                debitY_num: "", //借方的核销余额（原）
                debitZ_num: "", //借方的核销余额（组）
                debitUY_num: "", //借方的未核销余额（原）
                debitUZ_num: "", //借方的未核销余额（组）
            },
            debitRate: "1", //汇率
            resourveQuery: {},//原始查询条件
            queryCondition: {},
            creditSelectedData: [], //贷方选中数据
            debitSelectedData: [], //借方选中数据
            compareData_Credit: false, //贷方数据是否对照出来的
            compareData_Debit: false, //借方数据是否为对照出来的
            resourceData_credit: [], //查询出来的历史数据贷方
            resourceData_debit: [], //查询出来的历史数据借方
            position: true// 是否是横铺布局

        };
    }
    componentWillMount() {
        let callback = (json,status,inlt) => {
            this.columns_credit=this.getCreditOrDebitCoumn('credit',json);
            this.columns_debit=this.getCreditOrDebitCoumn('debit',json);    
            this.loadQuery = [
                {
                    itemName: json['20020VRIFYPAGE-000016'],/* 国际化处理： 核算账簿*/
                    itemType: "refer",
                    itemKey: "pk_accountingbook", isMustItem: true,
                    config: { refCode: "uapbd/refer/org/AccountBookTreeRef" }
                },
                {
                    itemName: json['20020VRIFYPAGE-000017'],/* 国际化处理： 业务单元*/
                    itemType: "refer",
                    itemKey: "pk_units", isMustItem: false,
                    config: { refCode: "uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef" }
                },
                {
                    itemName: json['20020VRIFYPAGE-000018'],/* 国际化处理： 核销科目*/
                    itemType: "refer",
                    itemKey: "pk_accasoa", isMustItem: true,
                    config: { refCode: "uapbd/refer/fiacc/AccountDefaultGridTreeRef" }
                },
                {
                    itemName: json['20020VRIFYPAGE-000019'],/* 国际化处理： 日期范围*/
                    itemType: "Dbdate",
                    itemKey: ["begin_date", "end_date"]
                }, //begin_date,end_date（必传）
                {
                    itemName: json['20020VRIFYPAGE-000020'],/* 国际化处理： 分析日期*/
                    itemType: "radio",
                    itemKey: "dateType",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000021'],/* 国际化处理： 凭证日期*/
                            value: "prepareddate"
                        },
                        {
                            label: json['20020VRIFYPAGE-000022'],/* 国际化处理： 业务日期*/
                            value: "Businessdate"
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000023'],/* 国际化处理： 未核销范围*/
                    itemType: "Dbselect",
                    itemKey: 'cmbMnyType',//json['20020VRIFYPAGE-000024']/* 国际化处理： 原币*/,
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000024'],/* 国际化处理： 原币*/
                            value: "0"
                        },
                        {
                            label: json['20020VRIFYPAGE-000025'],/* 国际化处理： 组织本币*/
                            value: "1"
                        },
                        {
                            label: json['20020VRIFYPAGE-000026'],/* 国际化处理： 集团本币*/
                            value: "2"
                        },
                        {
                            label: json['20020VRIFYPAGE-000027'],/* 国际化处理： 全局本币*/
                            value: "3"
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000028'],/* 国际化处理： 金额范围*/
                    itemType: "DbtextInput",
                    itemKey: ["mny_begin", "mny_end"]
                }, //mny_begin,mny_end
                { itemName: json['20020VRIFYPAGE-000029'], itemType: "textInput", itemKey: "digest" },/* 国际化处理： 摘要*/
                {
                    itemName: json['20020VRIFYPAGE-000030'],/* 国际化处理： 方向*/
                    itemType: "select",
                    itemKey: "cmbDirect",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000031'],/* 国际化处理： 双向*/
                            value: "-1"
                        },
                        {
                            label: json['20020VRIFYPAGE-000032'],/* 国际化处理： 借方*/
                            value: "0"
                        },
                        {
                            label: json['20020VRIFYPAGE-000033'],/* 国际化处理： 贷方*/
                            value: "1"
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000034'],/* 国际化处理： 币种*/
                    itemType: "refer",
                    itemKey: "pk_currency", isMustItem: true,
                    config: { refCode: "uapbd/refer/pubinfo/CurrtypeGridRef" },
                    queryGridUrl: "/nccloud/gl/voucher/ref.do",
                    refType: "grid"
                },
                { itemName: json['20020VRIFYPAGE-000035'], itemType: "textInput", itemKey: "verifyno" },/* 国际化处理： 核销号*/
    
                {
                    itemName: json['20020VRIFYPAGE-000036'],/* 国际化处理： 包含未记账凭证*/
                    itemType: "checkbox",
                    itemKey: "hasTally",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000036'],/* 国际化处理： 包含未记账凭证*/
                            checked: false
                        }
                    ]
                }
            ];
            this.hedgingCondition = [
                {
                    itemName: json['20020VRIFYPAGE-000041'],/* 国际化处理： 按日期范围*/
                    itemType: "checkbox",
                    itemKey: "sDateRange",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000042'],/* 国际化处理： 按日期范围：*/
                            checked: false
                        }
                    ]
                },
                { itemName: json['20020VRIFYPAGE-000043'], itemType: "textInput", itemKey: "dateRange" },/* 国际化处理： 日期相差*/
                {
                    itemName: json['20020VRIFYPAGE-000045'],/* 国际化处理： 金额相等*/
                    itemType: "checkbox",
                    itemKey: "sMny",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000045'],/* 国际化处理： 金额相等*/
                            checked: false
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000074'],/* 国际化处理： 按末级科目*/
                    itemType: "checkbox",
                    itemKey: "sPk_accsubj",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000074'],/* 国际化处理： 按末级科目*/
                            checked: true
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000046'],/* 国际化处理： 按对账标识码*/
                    itemType: "checkbox",
                    itemKey: "sNetbankflag",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000046'],/* 国际化处理： 按对账标识码*/
                            checked: false
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000049'],/* 国际化处理： 按辅助项*/
                    itemType: "checkbox",
                    itemKey: "sAss",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000050'],/* 国际化处理： 按辅助项：*/
                            checked: false
                        }
                    ]
                }
            ];
            this.verifyStandard = [
                {
                    itemName: json['20020VRIFYPAGE-000041'],/* 国际化处理： 按日期范围*/
                    itemType: "checkbox",
                    itemKey: "sDateRange",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000042'],/* 国际化处理： 按日期范围：*/
                            checked: false
                        }
                    ]
                },
                { itemName: json['20020VRIFYPAGE-000043'], itemType: "textInput", itemKey: "dateRange" },/* 国际化处理： 日期相差*/
                {
                    itemName: json['20020VRIFYPAGE-000044'],/* 国际化处理： 按核销号*/
                    itemType: "checkbox",
                    itemKey: "sVerifyNo",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000044'],/* 国际化处理： 按核销号*/
                            checked: true
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000045'],/* 国际化处理： 金额相等*/
                    itemType: "checkbox",
                    itemKey: "sMny",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000045'],/* 国际化处理： 金额相等*/
                            checked: false
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000074'],/* 国际化处理： 按末级科目*/
                    itemType: "checkbox",
                    itemKey: "sPk_accsubj",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000074'],/* 国际化处理： 按末级科目*/
                            checked: true
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000046'],/* 国际化处理： 按对账标识码*/
                    itemType: "checkbox",
                    itemKey: "sNetbankflag",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000046'],/* 国际化处理： 按对账标识码*/
                            checked: false
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000047'],/* 国际化处理： 只核销已选记录*/
                    itemType: "checkbox",
                    itemKey: "sVerify",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000047'],/* 国际化处理： 只核销已选记录*/
                            checked: false
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000048'],/* 国际化处理： 按业务单元*/
                    itemType: "checkbox",
                    itemKey: "sPk_unit",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000048'],/* 国际化处理： 按业务单元*/
                            checked: true
                        }
                    ]
                },
                {
                    itemName: json['20020VRIFYPAGE-000049'],/* 国际化处理： 按辅助项*/
                    itemType: "checkbox",
                    itemKey: "sAss",
                    itemChild: [
                        {
                            label: json['20020VRIFYPAGE-000050'],/* 国际化处理： 按辅助项：*/
                            checked: false
                        }
                    ]
                }
            ];
            this.setState({ json: json,inlt }, () => {
                initTemplate.call(this, this.props);
            })
        }
        getMultiLang({ moduleId: ['20020VRIFYPAGE','publiccommon'], domainName: 'gl', currentLocale: 'simpchn', callback });
      }
    onInputChange = (index, key) => {
        return value => {
            const { debitDataArr } = this.state;
            const dataSource = [...debitDataArr];
            dataSource[index][key] = value;
            this.setState({ debitDataArr });
        };
    };

    //贷方数据组装
    renderColumnsMultiSelect(columns) {
        const { creditDataArr, checkedArrayCredit, creditQueryStyle } = this.state;
        let select_column = {};
        let indeterminate_bool = false;
        const { multiSelect } = this.props;
        if (multiSelect && multiSelect.type === "checkbox") {
            let i = checkedArrayCredit.length;
            while (i--) {
                if (checkedArrayCredit[i]) {
                    indeterminate_bool = true;
                    break;
                }
            }
            let defaultColumns = [
                {
                    title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
                        <Checkbox
                            className="table-checkbox" style={{ width: "50px" }}
                            checked={this.state.checkedAllCredit}
                            indeterminate={indeterminate_bool && !this.state.checkedAllCredit}
                            onChange={this.onAllCheckChangeDebit.bind(this, "credit", false)}
                        />
                    }</div>),
                    key: "checkbox",
                    dataIndex: "checkbox",
                    width: "50px",
                    fixed: "left",
                    render: (text, record, index) => {
                        return (
                            <div fieldid="firstcol">
                            <Checkbox
                                className="table-checkbox" style={{ width: "50px" }}
                                checked={this.state.checkedArrayCredit[index]}
                                onChange={this.onCheckboxChangeDebit.bind(
                                    this,
                                    "credit",
                                    text,
                                    record,
                                    index
                                )}
                            />
                            </div>
                        );
                    }
                }
            ];
            columns = defaultColumns.concat(columns);
        }
        return columns;
    }
    //借方数据组装
    renderColumnsMultiSelectDebit(columns) {
        const { debitDataArr, checkedArrayDebit } = this.state;
        const { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;

        if (multiSelect && multiSelect.type === "checkbox") {
            let i = checkedArrayDebit.length;
            while (i--) {
                if (checkedArrayDebit[i]) {
                    indeterminate_bool = true;
                    break;
                }
            }
            let defaultColumns = [
                {
                    title: <div fieldid="firstcol" className='checkbox-mergecells'>{(
                        <Checkbox
                            fieldid="firstcol"
                            className="table-checkbox"
                            checked={this.state.checkedAllDebit}
                            indeterminate={indeterminate_bool && !this.state.checkedAllDebit}
                            onChange={this.onAllCheckChangeDebit.bind(this, "debit", false)}
                        />
                    )}</div>,
                    key: "checkbox",
                    dataIndex: "checkbox",
                    width: 50,
                    fixed: "left",
                    render: (text, record, index) => {
                        return (
                            <div fieldid="firstcol">
                            <Checkbox
                                className="table-checkbox"
                                checked={this.state.checkedArrayDebit[index]}
                                onChange={this.onCheckboxChangeDebit.bind(
                                    this,
                                    "debit",
                                    text,
                                    record,
                                    index
                                )}
                            />
                            </div>
                        );
                    }
                }
            ];
            columns = defaultColumns.concat(columns);
        }
        return columns;
    }
    //贷方、借方全选功能
    onAllCheckChangeDebit = (type, matchClicked) => {
        let self = this;
        let listData = self.state.debitDataArr.concat();
        let selIds = [];
        let {
            checkedArrayDebit,
            checkedArrayCredit,
            checkedAllDebit,
            checkedAllCredit,
            debitDataArr,
            creditDataArr, allMatch,CompareStatus,
            debitSelectedData, isControl, firstCheckedDatas, queryCondition, creditOrDebitFlag
        } = this.state;
        let allChickedData_debit = [], allChickedData_credit = [];
        if(CompareStatus == "uncompare"){//对照状态下不让全选
            return false;
        }
        if (type == "debit") {
            if (this.props.voucherVerifyflag == '0' && creditOrDebitFlag == 'debit') {
                return false;
            }
            if (isControl &&debitDataArr[0]&& debitDataArr[0].m_voAss) {//如果是严格控制，且第一条数据有辅助核算
                if (!self.state.checkedAllDebit) {
                    if (!firstCheckedDatas.type) {//如果未选中一行数据，先选中第一行
                        debitDataArr[0].m_voAss.map((item, index) => {
                            if (queryCondition.isControlItems.indexOf(item.m_pk_checktype) != -1) {
                                firstCheckedDatas.assinfo[item.m_pk_checktype] = item.m_pk_checkvalue;
                            }
                        })
                        firstCheckedDatas.type = 'debit';//'credit';
                        checkedArrayDebit[0] = !self.state.checkedArrayDebit[0];
                        debitDataArr.map((item, index) => {
                            if (iseqAssinfo(self, item.m_voAss, firstCheckedDatas)) {//判断辅助核算项是否跟严格控制的辅助核算项相等，相等就勾选上
                                checkedArrayDebit[index] = true;
                            }
                        });
                        if (matchClicked) {//全匹配状态
                            creditDataArr.map((item, index) => {
                                if (iseqAssinfo(self, item.m_voAss, firstCheckedDatas)) {//判断辅助核算项是否跟严格控制的辅助核算项相等，相等就勾选上
                                    checkedArrayCredit[index] = true;
                                }
                            });
                            checkedAllCredit = true;
                        }

                    } else {//如果已选中一行，其他行根据选中行去匹配
                        debitDataArr.map((item, index) => {
                            if (iseqAssinfo(self, item.m_voAss, firstCheckedDatas)) {//判断辅助核算项是否跟严格控制的辅助核算项相等，相等就勾选上
                                checkedArrayDebit[index] = true;
                            }
                        });
                        if (matchClicked) {//全匹配状态
                            creditDataArr.map((item, index) => {
                                if (iseqAssinfo(self, item.m_voAss, firstCheckedDatas)) {//判断辅助核算项是否跟严格控制的辅助核算项相等，相等就勾选上
                                    checkedArrayCredit[index] = true;
                                }
                            });
                            checkedAllCredit = true;
                        }
                    }
                } else {//checkedAllDebit为false，如果对方也没有选中的则清空firstCheckedDatas
                    checkedAllCredit = self.state.checkedAllCredit;
                    for (let i = 0; i < checkedArrayDebit.length; i++) {
                        checkedArrayDebit[i] = false;
                    }
                    if (checkedArrayCredit.join().indexOf('true') == -1) {
                        firstCheckedDatas.type = '';
                        firstCheckedDatas.assinfo = {};
                    }
                }
            } else {//非严格控制，就直接全选中
                for (let i = 0; i < checkedArrayDebit.length; i++) {
                    checkedArrayDebit[i] = !self.state.checkedAllDebit;
                }
            }
            // let allChickedData = this.handleAllChicked("debit");// 点击全选按钮 获取修改后的数据
            self.setState({
                firstCheckedDatas,
                checkedAllDebit: !self.state.checkedAllDebit,
                checkedArrayCredit,
                checkedArrayDebit,
                checkedAllCredit
                // debitDataArr: allChickedData
            }, () => {

                if (!matchClicked) {//非全匹配状态
                    this.handleAllChecked("debit");// 借贷方全选 控制 debitBoxClicked状态
                    // this.handleAllChecked("credit");// 借贷方全选 控制 debitBoxClicked状态
                    allChickedData_debit = this.handleAllChicked("debit");// 点击全选按钮 获取修改后的数据
                    // let allChickedData_credit = this.handleAllChicked("credit");// 点击全选按钮 获取修改后的数据
                    self.setState({
                        debitDataArr: allChickedData_debit,
                        // creditDataArr: allChickedData_credit
                    },()=>{
                        self.handleSumAmount("debit");
                    })
                } else {//全匹配状态
                    if(!isControl){
                        this.handleAllChecked("debit");// 借贷方全选 控制 debitBoxClicked状态
                        // this.handleAllChecked("credit");// 借贷方全选 控制 debitBoxClicked状态
                        allChickedData_debit = this.handleAllChicked("debit");// 点击全选按钮 获取修改后的数据
                        // allChickedData_credit = this.handleAllChicked("credit");// 点击全选按钮 获取修改后的数据
                        self.setState({
                            debitDataArr: allChickedData_debit,
                            // creditDataArr: allChickedData_credit
                        },()=>{
                            self.handleSumAmount("debit");
                        })
                    }else{
                        this.handleAllChecked("debit");// 借贷方全选 控制 debitBoxClicked状态
                        this.handleAllChecked("credit");// 借贷方全选 控制 debitBoxClicked状态
                        allChickedData_debit = this.handleAllChicked("debit");// 点击全选按钮 获取修改后的数据
                        allChickedData_credit = this.handleAllChicked("credit");// 点击全选按钮 获取修改后的数据
                        self.setState({
                            debitDataArr: allChickedData_debit,
                            creditDataArr: allChickedData_credit
                        },()=>{
                            self.handleSumAmount("credit");
                            self.handleSumAmount("debit");
                        })
                    }
                }

            });

        } else if (type == "credit") {
            if (this.props.voucherVerifyflag == '0' && creditOrDebitFlag == 'credit') {
                return false;
            }
            if (isControl&& creditDataArr[0] && creditDataArr[0].m_voAss) {//如果是严格控制，且第一条数据有辅助核算
                if (!self.state.checkedAllCredit) {//checkedAllCredit为true
                    if (!firstCheckedDatas.type) {//如果未选中一行数据，先选中第一行
                        creditDataArr[0].m_voAss.map((item, index) => {
                            if (queryCondition.isControlItems.indexOf(item.m_pk_checktype) != -1) {
                                firstCheckedDatas.assinfo[item.m_pk_checktype] = item.m_pk_checkvalue;
                            }
                        })
                        firstCheckedDatas.type = 'credit';
                        checkedArrayCredit[0] = !self.state.checkedArrayCredit[0];
                        if (matchClicked) {//全匹配状态
                            debitDataArr.map((item, index) => {
                                if (iseqAssinfo(self, item.m_voAss, firstCheckedDatas)) {//判断辅助核算项是否跟严格控制的辅助核算项相等，相等就勾选上
                                    checkedArrayDebit[index] = true;
                                }
                            });
                            checkedAllDebit = true;
                        }
                        creditDataArr.map((item, index) => {
                            if (iseqAssinfo(self, item.m_voAss, firstCheckedDatas)) {//判断辅助核算项是否跟严格控制的辅助核算项相等，相等就勾选上
                                checkedArrayCredit[index] = true;
                            }
                        });

                    } else {//如果已选中一行，其他行根据选中行去匹配
                        if (matchClicked) {//全匹配状态
                            debitDataArr.map((item, index) => {
                                if (iseqAssinfo(self, item.m_voAss, firstCheckedDatas)) {//判断辅助核算项是否跟严格控制的辅助核算项相等，相等就勾选上
                                    checkedArrayDebit[index] = true;
                                }
                            });
                            checkedAllDebit = true;
                        }
                        creditDataArr.map((item, index) => {
                            if (iseqAssinfo(self, item.m_voAss, firstCheckedDatas)) {//判断辅助核算项是否跟严格控制的辅助核算项相等，相等就勾选上
                                checkedArrayCredit[index] = true;
                            }
                        });
                    }
                } else {//checkedAllCredit 为false，判断对方是否有选中的，没有的话就清空firstCheckedDatas
                    checkedAllDebit = self.state.checkedAllDebit;
                    for (let i = 0; i < checkedArrayCredit.length; i++) {
                        checkedArrayCredit[i] = false;
                    }
                    if (checkedArrayDebit.join().indexOf('true') == -1) {
                        firstCheckedDatas.type = '';
                        firstCheckedDatas.assinfo = {};
                    }
                }
            } else {//非严格控制，就直接全选中
                for (let i = 0; i < checkedArrayCredit.length; i++) {
                    checkedArrayCredit[i] = !self.state.checkedAllCredit;
                }
            }
            // let allChickedData = this.handleAllChicked("credit");// 点击全选按钮 获取修改后的数据
            self.setState({
                firstCheckedDatas,
                checkedAllCredit: !self.state.checkedAllCredit,
                checkedAllDebit,
                checkedArrayCredit,
                checkedArrayDebit
                //creditDataArr: allChickedData
            }, () => {
                if (!matchClicked) {//非全匹配状态
                    this.handleAllChecked("credit");// 借贷方全选 控制 debitBoxClicked状态
                    // this.handleAllChecked("debit");// 借贷方全选 控制 debitBoxClicked状态
                    allChickedData_credit = this.handleAllChicked("credit");// 点击全选按钮 获取修改后的数据
                    // let allChickedData_debit = this.handleAllChicked("debit");// 点击全选按钮 获取修改后的数据
                    self.setState({
                        creditDataArr: allChickedData_credit,
                        // debitDataArr: allChickedData_debit
                    },()=>{
                        self.handleSumAmount("credit");
                    })
                } else {//全匹配状态
                    if(!isControl){//非严格控制
                        this.handleAllChecked("credit");// 借贷方全选 控制 debitBoxClicked状态
                        allChickedData_credit = this.handleAllChicked("credit");// 点击全选按钮 获取修改后的数据
                        self.setState({
                            creditDataArr: allChickedData_credit
                        },()=>{
                            self.handleSumAmount("credit");
                        })
                    }else{//严格控制
                        this.handleAllChecked("credit");// 借贷方全选 控制 debitBoxClicked状态
                        this.handleAllChecked("debit");// 借贷方全选 控制 debitBoxClicked状态
                        allChickedData_credit = this.handleAllChicked("credit");// 点击全选按钮 获取修改后的数据
                        allChickedData_debit = this.handleAllChicked("debit");// 点击全选按钮 获取修改后的数据
                        self.setState({
                            creditDataArr: allChickedData_credit,
                            debitDataArr: allChickedData_debit
                        },()=>{
                            self.handleSumAmount("credit");
                            self.handleSumAmount("debit");
                        })
                    }
                }

            });

        }
    };

    // 贷方、借方单选功能
    onCheckboxChangeDebit = (type, text, record, index) => {
        let self = this;
        let { creditDataArr, debitDataArr, CompareStatus, allMatch, compareData_Credit, compareData_Debit, isControl,
            firstCheckedDatas, queryCondition, creditOrDebitFlag } = self.state;
        let allFlag = false;
        let checkedArrayDebit = self.state.checkedArrayDebit.concat();
        let checkedArrayCredit = self.state.checkedArrayCredit.concat();
        if (type == "debit") {
            if (self.props.voucherVerifyflag == '0' && creditOrDebitFlag == 'debit') {
                return false;
            }
            if (CompareStatus == "uncompare") {
                checkedArrayDebit.map((k, v) => {
                    if (v == index) {
                        checkedArrayDebit[v] = !self.state.checkedArrayDebit[index];

                    } else {
                        if (checkedArrayDebit[v]) {
                            const o = self.handleCancel("debit", v, !checkedArrayDebit[v]);
                            self.setState(
                                {
                                    debitDataArr: o,
                                    checkedAllDebit: false,
                                    checkedArrayDebit: checkedArrayDebit
                                },
                                () => {
                                    self.handleChecked("debit", v);
                                    self.handleSumAmount("debit");
                                    // self.handleSumCY("debit");
                                    // self.handleSumCZ("debit");
                                    // self.handleSumCUY("debit");
                                    // self.handleSumCUZ("debit");
                                })
                        }
                        checkedArrayDebit[v] = false;
                    }
                });
            } else {
                if (isControl && debitDataArr[index].m_voAss) {//如果是严格控制,取到控制的辅助核算项得value值
                    if (!firstCheckedDatas.type) {
                        debitDataArr[index].m_voAss.map((item, index) => {
                            if (queryCondition.isControlItems.indexOf(item.m_pk_checktype) != -1) {
                                firstCheckedDatas.assinfo[item.m_pk_checktype] = item.m_pk_checkvalue;
                            }
                        })
                        firstCheckedDatas.type = 'credit';
                        checkedArrayDebit[index] = !self.state.checkedArrayDebit[index];
                    } else {
                        if (self.state.checkedArrayDebit[index]) {
                            checkedArrayDebit[index] = false;
                            if (checkedArrayDebit.join().indexOf('true') == -1 && checkedArrayCredit.join().indexOf('true') == -1) {
                                firstCheckedDatas.type = '';
                                firstCheckedDatas.assinfo = {};
                            }
                        } else {
                            if (!debitDataArr[index].m_voAss || iseqAssinfo(self, debitDataArr[index].m_voAss, firstCheckedDatas)) {
                                checkedArrayDebit[index] = !self.state.checkedArrayDebit[index];
                            } else {
                                return false;
                            }
                        }
                    }
                } else {
                    checkedArrayDebit[index] = !self.state.checkedArrayDebit[index];
                }
            }

            for (let i = 0; i < self.state.checkedArrayDebit.length; i++) {
                if (!checkedArrayDebit[i]) {
                    allFlag = false;
                    break;
                } else {
                    allFlag = true;
                }
            }

            // 接收点击单选按钮传过来的数据，对debitDataArr进行修改
            const { creditDataArr } = self.state;
            const o = self.handleCancel("debit", index, checkedArrayDebit[index]);
            self.setState(
                {
                    debitDataArr: o, firstCheckedDatas,
                    checkedAllDebit: allFlag,
                    checkedArrayDebit: checkedArrayDebit
                },
                () => {
                    self.handleChecked("debit", index);
                    self.handleSumAmount("debit");
                    // self.handleSumCY("debit");
                    // self.handleSumCZ("debit");
                    // self.handleSumCUY("debit");
                    // self.handleSumCUZ("debit");
                    if (CompareStatus == "uncompare") {
                        //对照出另一方的数据
                        compareOtherData(self, "debit", self.state.compareData_Debit);
                    }
                    if (allMatch) {
                        //匹配出另一方的数据
                        matchOtherData(self, "debit");
                    }
                }
            );
        } else if (type == "credit") {//贷方
            if (self.props.voucherVerifyflag == '0' && creditOrDebitFlag == 'credit') {
                return false;
            }
            if (CompareStatus == "uncompare") {
                checkedArrayCredit.map((k, v) => {
                    if (v == index) {
                        checkedArrayCredit[v] = !self.state.checkedArrayCredit[index];

                    } else {
                        if (checkedArrayCredit[v]) {
                            const o = self.handleCancel("credit", v, !checkedArrayCredit[v]);
                            self.setState(
                                {
                                    creditDataArr: o,
                                    checkedAllCredit: false,
                                    checkedArrayCredit: checkedArrayCredit
                                },
                                () => {
                                    self.handleChecked("credit", v);
                                    self.handleSumAmount("credit");
                                    // self.handleSumCY("credit");
                                    // self.handleSumCZ("credit");
                                    // self.handleSumCUY("credit");
                                    // self.handleSumCUZ("credit");
                                })
                        }
                        checkedArrayCredit[v] = false;
                    }
                });
            } else {
                if (isControl && creditDataArr[index].m_voAss) {//如果是严格控制,取到控制的辅助核算项得value值
                    if (!firstCheckedDatas.type) {
                        creditDataArr[index].m_voAss.map((item, index) => {
                            if (queryCondition.isControlItems.indexOf(item.m_pk_checktype) != -1) {
                                firstCheckedDatas.assinfo[item.m_pk_checktype] = item.m_pk_checkvalue;
                            }
                        })
                        firstCheckedDatas.type = 'credit';
                        checkedArrayCredit[index] = !self.state.checkedArrayCredit[index];
                    } else {
                        if (self.state.checkedArrayCredit[index]) {
                            checkedArrayCredit[index] = false;
                            if (checkedArrayCredit.join().indexOf('true') == -1 && checkedArrayDebit.join().indexOf('true') == -1) {
                                firstCheckedDatas.type = '';
                                firstCheckedDatas.assinfo = {};
                            }
                        } else {
                            if (!creditDataArr[index].m_voAss || iseqAssinfo(self, creditDataArr[index].m_voAss, firstCheckedDatas)) {
                                checkedArrayCredit[index] = !self.state.checkedArrayCredit[index];
                            } else {
                                return false;
                            }
                        }
                    }
                } else {
                    checkedArrayCredit[index] = !self.state.checkedArrayCredit[index];
                }
            }
            for (let i = 0; i < self.state.checkedArrayCredit.length; i++) {
                if (!checkedArrayCredit[i]) {
                    allFlag = false;
                    break;
                } else {
                    allFlag = true;
                }
            }
            // 接收点击单选按钮传过来的数据，对debitDataArr进行修改

            const o = self.handleCancel("credit", index, checkedArrayCredit[index]);
            self.setState(
                {
                    creditDataArr: o, firstCheckedDatas,
                    checkedAllCredit: allFlag,
                    checkedArrayCredit: checkedArrayCredit
                },
                () => {
                    self.handleChecked("credit", index);
                    self.handleSumAmount("credit");
                    // self.handleSumCY("credit");
                    // self.handleSumCZ("credit");
                    // self.handleSumCUY("credit");
                    // self.handleSumCUZ("credit");
                    if (CompareStatus == "uncompare") {//对照状态
                        //对照出另一方的数据
                        compareOtherData(self, "credit", self.state.compareData_Credit);
                    }
                    // if (allMatch) {//匹配状态
                    //   //匹配出另一方的数据
                    //   matchOtherData(self, "credit");
                    // }
                }
            );
        }
    };

    //过滤选中的数据
    filterSelectedData(type) {
        let {
            checkedArrayDebit,
            debitSelectedData,
            debitDataArr,
            checkedArrayCredit,
            creditSelectedData,
            creditDataArr
        } = this.state;
        creditSelectedData = [];
        debitSelectedData = [];
        if (type == "debit") {
            //借方
            for (let i = 0; i < checkedArrayDebit.length; i++) {
                if (checkedArrayDebit[i] == true) {
                    debitSelectedData.push(debitDataArr[i]);
                }
            }
            return debitSelectedData;
        } else {
            //
            for (let i = 0; i < checkedArrayCredit.length; i++) {
                if (checkedArrayCredit[i] == true) {
                    creditSelectedData.push(creditDataArr[i]);
                }
            }
            return creditSelectedData;
        }
    }
    handleRefresh = () => {//刷新按钮
        let { resourveQuery, isQueryShow } = this.state;
        let isrefreah=true
        if (resourveQuery.pk_accountingbook) {
            this.setState({
                isQueryShow: false
            }, () => {
                handleQueryClick.call(this, this.state, resourveQuery,isrefreah);
            })

        }

    }
    //查询按钮
    handleClick() {
        let { layout } = this.state
        this.setState({
            isQueryShow: !this.state.isQueryShow,
            queryStyle: !this.state.queryStyle
        });
    }
    //自动核销按钮
    handleAutoVerifyClick = () => {
        this.setState({
            verifyStandardshowModal: !this.state.verifyStandardshowModal
        });
    };
    //自动红蓝对冲
    handleAutoHedgingClick = () => {
        this.setState({
            hedgingStandardshowModal: !this.state.hedgingStandardshowModal
        });
    };
    //查询历史
    handleQueryHistory = () => {
        let self = this;
        self.props.openTo(
            "/gl/verify/historyquery/list/index.html",
            {
                appcode: '20020VRIFY',
                pagecode: '20020VRIFYHISTORY'
            }
        );
    };
    //全匹配
    handleAllMatch() {
        let self = this;
        let { creditDataArr, debitDataArr, allMatch, isControl, firstCheckedDatas } = self.state;
        if (creditDataArr.length <= 0) {
            toast({ content: this.state.json['20020VRIFYPAGE-000077'], color: "warning" });/* 国际化处理： 贷方没数据不能全匹配,功能不可用*/
            return false;
        }
        allMatch = true;
        self.setState(
            {
                allMatch
            },
            () => {
                //触发全选操作
                if (!isControl) {
                    //非严格控制 界面数据全选全选
                    self.onAllCheckChangeDebit("credit", true);
                    self.onAllCheckChangeDebit("debit", true);
                } else {
                    // if(firstCheckedDatas.type){//严格控制 判断是否有勾选借方或贷方数据
                    //   matchOtherData(self, "credit");
                    //   matchOtherData(self, "debit");
                    // }else{
                    self.onAllCheckChangeDebit('credit', true);
                    // //严格控制 默认勾选贷方第一条数据
                    // self.onCheckboxChangeDebit("credit", "", "", 0);
                    // matchOtherData(self, "credit");
                    // }

                }
                let buttonArr = ['full_match', 'contrast', 'cancel_contrast'];
                self.props.button.setButtonDisabled(buttonArr, true);
            }
        );
    };
    //对照
    handleCompare = () => {
        let self = this;
        let {
            CompareStatus,
            creditSelectedData,
            debitSelectedData,
            resourceData_credit,
            resourceData_debit,
            checkedArrayDebit,
            checkedArrayCredit
        } = self.state;
        CompareStatus = "uncompare";
        let currentData = []; //多虑对照对来的数据
        self.setState({
            CompareStatus
        }, () => {
            let buttonArr = ['RB_hedging', 'auto_verificationCancel', 'auto_RB_hedging', 'history_operation', 'full_match', 'cancel_match',
                'contrast', 'link_voucher'];
            self.props.button.setButtonDisabled(buttonArr, true);
        });
    };
    //取消对照
    handleUnCompare = () => {
        let { CompareStatus, resourceData_credit, resourceData_debit, checkedArrayDebit, checkedArrayCredit, } = this.state;
        for (let i = 0; i < checkedArrayDebit.length; i++) {
            checkedArrayDebit[i] = false;
        }
        for (let i = 0; i < checkedArrayCredit.length; i++) {
            checkedArrayCredit[i] = false;
        }
        CompareStatus = "compare";
        this.setState({
            checkedAllDebit: false,
            checkedAllCredit: false,
            CompareStatus,
            creditDataArr: resourceData_credit,
            debitDataArr: resourceData_debit
        }, () => {
            this.handleSumAmount('credit');
            this.handleSumAmount('debit'); 
            let buttonArr = ['RB_hedging', 'auto_verificationCancel', 'auto_RB_hedging', 'history_operation', 'full_match', 'cancel_match',
                'contrast', 'link_voucher'];
            this.props.button.setButtonDisabled(buttonArr, false);
        });
    };
    /**
     * 切换借方和贷方表格的位置：左右 <- -> 上下
     */

    //获取默认会计期间 会计期间方案
    getDefaultYearmouth = (pk_accountingbook, modalDefaultValue) => {
        let self = this;
        let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
        let pk_accpont = { "pk_accountingbook": pk_accountingbook }
        ajax({
            url: url,
            data: pk_accpont,
            async: false,
            success: function (response) {
                const { success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if (response.data) {
                        modalDefaultValue.bizPeriod = response.data.bizPeriod;
                        modalDefaultValue.isShowUnit = response.data.isShowUnit;
                        modalDefaultValue.isOpenUpBusiUnit = 'Y';
                        modalDefaultValue.pk_accperiodscheme = response.data.pk_accperiodscheme;
                        if(!modalDefaultValue.begindate){
                            modalDefaultValue.begindate = response.data.begindate;
                        }
                        if(!modalDefaultValue.enddate){
                            modalDefaultValue.enddate = response.data.enddate;
                        }
                        
                        modalDefaultValue.bizDate = response.data.bizDate;
                        modalDefaultValue.yearbegindate = response.data.yearbegindate;
                        if(!modalDefaultValue.pk_currency.value){
                            modalDefaultValue.pk_currency.display = response.data.currinfo.display;
                            modalDefaultValue.pk_currency.value = response.data.currinfo.value;
                        }
                        
                        modalDefaultValue.pk_org = response.data.unit.value;
                        modalDefaultValue.account_currency = response.data.currinfo.value;
                        modalDefaultValue.isCrossAccountVerify = response.data.isCrossAccountVerify;
                        modalDefaultValue.NC001 = response.data.NC001;
                        modalDefaultValue.NC002 = response.data.NC002;
                        modalDefaultValue.scale = response.data.scale;//原币精度
                        modalDefaultValue.orgscale = response.data.orgscale;//组织本币精度
                        modalDefaultValue.groupscale = response.data.groupscale;//集团本币币种精度
                        modalDefaultValue.globalscale = response.data.globalscale;//全局本币币种精度
                    }
                    self.setState({
                        modalDefaultValue
                    })
                }
            }
        });
    }

    handleChangeLayout() {//切换布局
        this.setState({
            layout: this.state.layout === 12 ? 6 : 12,
            position: !this.state.position
        });
    }
    //及时核销请求数据
    queryData = (mockData,data) => {
        let self = this;
        let { checkedArrayCredit, checkedArrayDebit, checkedAllDebit, checkedAllCredit,defaultStatu,
            creditOrDebitFlag,debitBoxClicked, creditBoxClicked, queryCondition, pk_accasoa, pk_accountingbook, modalDefaultValue,
            creditScale_Y,creditScale_B,debitScale_Y,debitScale_B } = self.state;
        checkedArrayCredit = [];
        checkedArrayDebit = [];

        checkedAllDebit = false;
        checkedAllCredit = false;
        let url = '/nccloud/gl/verify/rtverify.do';
        ajax({
            url: url,
            data: mockData,
            success: function (response) {
                let { data, success } = response;
                if (success) {
                    self.updateVerifyData(mockData,data);
                } else {
                    toast({ content: this.state.json['20020VRIFYPAGE-000078'], color: 'warning' });/* 国际化处理： 没有获取到及时核销数据*/
                }
            },
            error: function (res) {
                toast({ content: res.message, color: 'warning' });
                self.props.closeModal(false);
            }
        });
    }
    //及时核销更新表体数据
    updateVerifyData=(mockData,data)=>{
        let self = this;
        let { checkedArrayCredit, checkedArrayDebit, checkedAllDebit, checkedAllCredit,defaultStatu,firstCheckedDatas,
            creditOrDebitFlag,debitBoxClicked, creditBoxClicked, queryCondition, pk_accasoa, pk_accountingbook, modalDefaultValue,
            creditScale_Y,creditScale_B,debitScale_Y,debitScale_B } = self.state;
        checkedArrayCredit = [];
        checkedArrayDebit = [];

        checkedAllDebit = false;
        checkedAllCredit = false;
        let creditData = data.credit;
        let debitData = data.debit;
        queryCondition = data.conVO;//及时核销后台返回的查询条件
        queryCondition.account_currency=mockData.paraInfo.orgCurrinfo.value;
        queryCondition.isControlItems = [];
        pk_accountingbook.display = data.accountingBook.name;
        pk_accountingbook.value = data.accountingBook.pk_accountingbook;
        pk_accasoa.display = data.verifyObj.header.m_sAccSubjName;
        pk_accasoa.value = data.verifyObj.header.m_Pk_accsubj;
        modalDefaultValue.pk_accountingbook = pk_accountingbook;
        modalDefaultValue.pk_accasoa = pk_accasoa;
        modalDefaultValue.pk_currency.display = data.conVO.currName;
        modalDefaultValue.pk_currency.value = data.conVO.pk_currency;
        modalDefaultValue.cmbMnyType = data.conVO.cmbMnyType;//未核销范围
        modalDefaultValue.cmbDirect = data.conVO.cmbDirect;//方向
        modalDefaultValue.hasTally = data.conVO.hasTally//包含未记账
        modalDefaultValue.begindate = data.conVO.begindate?data.conVO.begindate:'';//开始日期
        modalDefaultValue.enddate = data.conVO.end_date?data.conVO.end_date:'';//截止日期
        data.conVO.ass.map((item,index)=>{
            item.classid=item.m_classid;
        })
        modalDefaultValue.ass = data.conVO.ass;
        self.getDefaultYearmouth(pk_accountingbook.value, modalDefaultValue);
        if (creditData&&creditData.length>0) {
            creditScale_Y=creditData[0].m_creditamount.scale;
            creditScale_B=creditData[0].m_localcreditamount.scale;
            creditData.map((item, i) => {
                item.key = i;
                //定义两个临时变量存储未核销的原币和组织本币
                item.empty_m_Balancecreditamount = {
                    display: item.m_Balancecreditamount.value,
                    value: item.m_Balancecreditamount.value
                }
                item.empty_m_Balancelocalcreditamount = {
                    display: item.m_Balancelocalcreditamount.value,
                    value: item.m_Balancelocalcreditamount.value
                }
                if (item.m_pk_detail&&item.m_pk_detail.value == mockData.pk_detail) {
                    creditOrDebitFlag = 'credit';
                    checkedAllCredit = true;
                    checkedArrayCredit.push(true);
                    creditBoxClicked = true;
                    self.verifyIsControlFun(pk_accountingbook.value,pk_accasoa.value,queryCondition,creditData,'credit');
                    //把未核销金额赋给核销
                    item.m_dCredit_Money_Y = JSON.parse(JSON.stringify(item.m_Balancecreditamount));
                    item.m_dCredit_Money_B = JSON.parse(JSON.stringify(item.m_Balancelocalcreditamount));
                    item.m_Balancecreditamount.display = 0;
                    item.m_Balancecreditamount.value = 0;
                    item.m_Balancelocalcreditamount.display = 0;
                    item.m_Balancelocalcreditamount.value = 0;
                } else {
                    checkedArrayCredit.push(false);
                }

                defaultStatu = true;
            })
        } else {
            creditData = [];
        }
        if (debitData&&debitData.length>0) {
            debitScale_Y=debitData[0].m_debitamount.scale;
            debitScale_B=debitData[0].m_localdebitamount.scale;
            debitData.map((item, i) => {
                item.key = i;
                //定义两个临时变量存储未核销的原币和组织本币
                item.empty_m_Balancedebitamount = {
                    display: item.m_Balancedebitamount.value,
                    value: item.m_Balancedebitamount.value
                }
                item.empty_m_Balancelocaldebitamount = {
                    display: item.m_Balancelocaldebitamount.value,
                    value: item.m_Balancelocaldebitamount.value
                }
                if (item.m_pk_detail&&item.m_pk_detail.value == mockData.pk_detail) {
                    checkedArrayDebit.push(true);
                    checkedAllDebit = true;
                    creditOrDebitFlag = 'debit';
                    debitBoxClicked = true;
                    self.verifyIsControlFun(pk_accountingbook.value,pk_accasoa.value,queryCondition,debitData,'debit');
                    //把未核销金额赋给核销
                    item.m_dDebit_Money_Y = JSON.parse(JSON.stringify(item.m_Balancedebitamount));
                    item.m_dDebit_Money_B = JSON.parse(JSON.stringify(item.m_Balancelocaldebitamount));
                    item.m_Balancedebitamount.display = 0;
                    item.m_Balancedebitamount.value = 0;
                    item.m_Balancelocaldebitamount.display = 0;
                    item.m_Balancelocaldebitamount.value = 0;
                } else {
                    checkedArrayDebit.push(false);
                }
                defaultStatu = true;
            })
        } else {
            debitData = [];
        }

        self.setState({
            queryCondition,
            creditDataArr: creditData,
            debitDataArr: debitData,
            debitBoxClicked, creditBoxClicked,
            checkedArrayCredit, checkedArrayDebit,
            creditOrDebitFlag, firstCheckedDatas,
            pk_accasoa, pk_accountingbook,
            checkedArrayCredit, checkedArrayDebit, checkedAllDebit, checkedAllCredit,
            creditScale_Y,creditScale_B,debitScale_Y,debitScale_B,
            mockData: mockData,defaultStatu
        }, () => {
            // self.verifyIsControlFun(pk_accountingbook.value,pk_accasoa.value);
            verifyUnverifySum(self);
            self.handleSumAmount('credit');
            self.handleSumAmount('debit'); 
        })
    }
    //及时核销判断核销对象是否为严格控制
    verifyIsControlFun=(pk_accountingbook,pk_accasoa,queryCondition,debitCreditData,debitCreditType)=>{
        let self=this;
        let {isControl,firstCheckedDatas}=self.state;
       let url = '/nccloud/gl/verify/verifyobjinfo.do';
       let queryparam={
           'pk_accountingbook':pk_accountingbook,
           'pk_accasoa':pk_accasoa
       }
       ajax({
           url:url,
           data:queryparam,
           success:function(response){
               const { success } = response;
               if(success&&response.data){
                   isControl=response.data.verifyobj.header.m_bcontrol;//是否严格控制
                //    listItem.isControlItems=[];
                   if(response.data.verifyobj.items){
                       response.data.verifyobj.items.map((item,index)=>{
                        queryCondition.isControlItems.push(item.m_pk_subjass);//哪个辅助核算项严格控制
                       })
                   }
                   if (!firstCheckedDatas.type) {//如果未选中一行数据，先选中第一行
                        debitCreditData[0].m_voAss.map((item, index) => {
                            if (queryCondition.isControlItems.indexOf(item.m_pk_checktype) != -1) {
                                firstCheckedDatas.assinfo[item.m_pk_checktype] = item.m_pk_checkvalue;
                            }
                        })
                        firstCheckedDatas.type = debitCreditType;
                    }
                //    listItem.isControl=isControl;
                   queryCondition.isControl=isControl;
                //    listItem.endflag=response.data.account.endflag;

                   self.setState({
                       isControl,queryCondition,firstCheckedDatas
                   })
               }
           },
           error:function(error){
           }
       })
   }
    //全部
    handleQueryAll = () => {
        let { mockData } = this.state;
        this.queryData(mockData);
    }
    componentDidUpdate() {
        let { layout } = this.state


    }
    componentDidMount() {
        if (this.props.voucherVerifyflag && (this.props.voucherVerifyflag == '0' || this.props.voucherVerifyflag == '1')) {//0 表示及时核销 1表示参照核销
            this.props.onRef(this);
            this.setState({
                voucherVerifyflag: this.props.voucherVerifyflag
            })

            if (this.props.voucherVerifyflag && this.props.voucherVerifyflag == '0') {//及时核销
                let { data,rtverifyData } = this.props;
                // this.queryData(data);
                if(rtverifyData&&rtverifyData.conVO){
                    this.updateVerifyData(data,rtverifyData);
                }
            }
        } else {
            let { modalDefaultValue, voucherVerifyflag } = this.state;
            let appcode = this.props.getSearchParam("c");
            getDefaultAccountBook(appcode).then((defaultAccouontBook) => {
                if (defaultAccouontBook.value) {
                    modalDefaultValue.pk_accountingbook = defaultAccouontBook;
                    this.getDefaultYearmouth(defaultAccouontBook.value, modalDefaultValue);
                }
            })
            let upScroll = document.getElementsByClassName("u-table-body")[0];
            let downScroll = document.getElementsByClassName("u-table-body")[1];
            upScroll.addEventListener("scroll", function () {
                downScroll.scrollLeft = upScroll.scrollLeft
            });
            downScroll.addEventListener("scroll", function () {
                upScroll.scrollLeft = downScroll.scrollLeft
            })
        }
    }
    componentWillUpdate() { }
    componentWillReceiveProps(nextProps) {
        let self = this;
        let { mockData, creditDataArr, debitDataArr, checkedAllDebit, checkedAllCredit, checkedArrayCredit, checkedArrayDebit,
            queryCondition, pk_accasoa, pk_accountingbook, resourceData_credit, resourceData_debit, defaultStatu,
            creditScale_Y,creditScale_B,debitScale_Y,debitScale_B } = self.state;
        if (nextProps.data) {
            if ((nextProps.data.credit || nextProps.data.debit) && ((nextProps.data.credit && nextProps.data.credit != self.state.creditDataArr) || (nextProps.data.debit && nextProps.data.debit != self.state.debitDataArr))) {
                checkedAllDebit = false;
                checkedAllCredit = false;
                checkedArrayCredit = [];
                checkedArrayDebit = [];
                resourceData_credit = [];
                resourceData_debit = [];
                creditDataArr = [];
                debitDataArr = [];
                defaultStatu = false;
                let paraInfo = nextProps.data.paraInfo;
                queryCondition.account_currency=nextProps.data.paraInfo.orgCurrinfo.value;//组织本币取paraInfo里的
                let creditData = nextProps.data.credit;
                let debitData = nextProps.data.debit;
                pk_accasoa.display = nextProps.data.verifyObj.header.m_sAccSubjName;
                pk_accountingbook.display = nextProps.data.accountingBook.name;
                if (creditData&&creditData.length>0) {
                    creditScale_Y=creditData[0].m_creditamount.scale;
                    creditScale_B=creditData[0].m_localcreditamount.scale;
                    queryCondition.pk_currency=creditData[0].m_pk_currtype.value;
                    creditData.map((item, i) => {
                        item.key = i;

                        //定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancecreditamount = {
                            display: item.m_Balancecreditamount.value,
                            value: item.m_Balancecreditamount.value
                        }
                        item.empty_m_Balancelocalcreditamount = {
                            display: item.m_Balancelocalcreditamount.value,
                            value: item.m_Balancelocalcreditamount.value
                        }
                        checkedArrayCredit.push(false);
                        defaultStatu = true;
                    })
                } else {
                    creditData = [];
                }
                if (debitData&&debitData.length>0) {
                    debitScale_Y=debitData[0].m_debitamount.scale;
                    debitScale_B=debitData[0].m_localdebitamount.scale;
                    if(!queryCondition.pk_currency){
                        queryCondition.pk_currency=debitData[0].m_pk_currtype.value;
                    }
                    debitData.map((item, i) => {
                        item.key = i;

                        //定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancedebitamount = {
                            display: item.m_Balancedebitamount.value,
                            value: item.m_Balancedebitamount.value
                        }
                        item.empty_m_Balancelocaldebitamount = {
                            display: item.m_Balancelocaldebitamount.value,
                            value: item.m_Balancelocaldebitamount.value
                        }
                        checkedArrayDebit.push(false);
                        defaultStatu = true;
                    })
                } else {
                    debitData = [];
                }
                let $$creditData=JSON.parse(JSON.stringify(creditData));
                let $$debitData=JSON.parse(JSON.stringify(debitData));
                resourceData_credit = $$creditData;
                resourceData_debit = $$debitData;
                self.setState({
                    queryCondition,
                    paraInfo: paraInfo,
                    defaultStatu,
                    resourceData_credit, resourceData_debit,
                    creditDataArr: creditData,
                    creditDataArr: creditData,
                    debitDataArr: debitData,
                    pk_accasoa, pk_accountingbook,
                    checkedArrayCredit, checkedArrayDebit, checkedAllDebit, checkedAllCredit,
                    creditScale_Y,creditScale_B,debitScale_Y,debitScale_B
                }, () => {
                    verifyUnverifySum(self);
                    self.handleSumAmount('credit');
                    self.handleSumAmount('debit'); 
                })
            }
        }
    }
    //全部按钮调用此方法
    verifyAllData = (verifyData) => {
        let self = this;
        let { mockData, creditDataArr, debitDataArr, checkedAllDebit, checkedAllCredit, checkedArrayCredit, checkedArrayDebit,
            queryCondition, pk_accasoa, pk_accountingbook, resourceData_credit, resourceData_debit, defaultStatu } = self.state;
        checkedAllDebit = false;
        checkedAllCredit = false;
        checkedArrayCredit = [];
        checkedArrayDebit = [];
        resourceData_credit = [];
        resourceData_debit = [];
        creditDataArr = [];
        debitDataArr = [];
        defaultStatu = false;
        let creditData = verifyData.credit;
        let debitData = verifyData.debit;
        pk_accasoa.display = verifyData.verifyObj.header.m_sAccSubjName;
        pk_accountingbook.display = verifyData.accountingBook.name;
        if (creditData) {
            creditData.map((item, i) => {
                item.key = i;
                //定义两个临时变量存储未核销的原币和组织本币
                item.empty_m_Balancecreditamount = {
                    display: item.m_Balancecreditamount.value,
                    value: item.m_Balancecreditamount.value
                }
                item.empty_m_Balancelocalcreditamount = {
                    display: item.m_Balancelocalcreditamount.value,
                    value: item.m_Balancelocalcreditamount.value
                }
                checkedArrayCredit.push(false);
                defaultStatu = true;
                //把未核销金额赋给核销
                // item.m_dCredit_Money_Y=item.m_Balancecreditamount;
                // item.m_dCredit_Money_B=item.m_Balancelocalcreditamount;
                // item.m_Balancecreditamount=0;
                // item.m_Balancelocalcreditamount=0;
            })
        } else {
            creditData = [];
        }
        if (debitData) {
            debitData.map((item, i) => {
                item.key = i;
                //定义两个临时变量存储未核销的原币和组织本币
                item.empty_m_Balancedebitamount = {
                    display: item.m_Balancedebitamount.value,
                    value: item.m_Balancedebitamount.value
                }
                item.empty_m_Balancelocaldebitamount = {
                    display: item.m_Balancelocaldebitamount.value,
                    value: item.m_Balancelocaldebitamount.value
                }
                checkedArrayDebit.push(false);
                defaultStatu = true;
            })
        } else {
            debitData = [];
        }
        let $$creditData=JSON.parse(JSON.stringify(creditData));
        let $$debitData=JSON.parse(JSON.stringify(debitData));
        resourceData_credit = $$creditData;
        resourceData_debit = $$debitData;
        self.setState({
            defaultStatu,
            resourceData_credit, resourceData_debit,
            creditDataArr: creditData,
            creditDataArr: creditData,
            debitDataArr: debitData,
            pk_accasoa, pk_accountingbook,
            checkedArrayCredit, checkedArrayDebit, checkedAllDebit, checkedAllCredit
        }, () => {
            verifyUnverifySum(self);
        })
    }
    //获取选中行的原币金额和组织本币金额 返回给凭证
    getData_CDMoney = () => {
        let CDMoney_YB = {}
        CDMoney_YB.verifyFlag = true;
        let creditSelectData = this.filterSelectedData('credit');
        let debitSelectData = this.filterSelectedData('debit');
        let verifyFlag_credit = false;
        if (creditSelectData.length > 0 && debitSelectData.length > 0) {
            toast({ content: this.state.json['20020VRIFYPAGE-000059'], color: 'warning' });/* 国际化处理： 只能选择一方的数据*/
            CDMoney_YB.verifyFlag = false;
            return CDMoney_YB;
        }
        if (creditSelectData.length == 0 && debitSelectData.length == 0) {
            toast({ content: this.state.json['20020VRIFYPAGE-000060'], color: 'warning' });/* 国际化处理： 请选择一方的数据*/
            CDMoney_YB.verifyFlag = false;
            return CDMoney_YB;
        }
        if (creditSelectData.length > 0) {//判断核销号不同的分录应不能同时选择
            let creditfirstIndex = creditSelectData[0].m_VerifyNo?creditSelectData[0].m_VerifyNo.value:'';

            creditSelectData.map((item, index) => {
                if (index > 0 && ((item.m_VerifyNo?item.m_VerifyNo.value:'') != creditfirstIndex)) {
                    verifyFlag_credit = true;
                    CDMoney_YB.verifyFlag = false;
                }
            })
            if (verifyFlag_credit) {
                toast({ content: this.state.json['20020VRIFYPAGE-000080'], color: 'warning' });/* 国际化处理： 核销号不同的分录应不能同时选择*/
                CDMoney_YB.verifyFlag = false;
                return CDMoney_YB;
            }
        }
        if (debitSelectData.length > 0) {//判断核销号不同的分录应不能同时选择
            let debitfirstIndex = debitSelectData[0].m_VerifyNo?debitSelectData[0].m_VerifyNo.value:'';
            // let verifyFlag_debit=false;
            debitSelectData.map((item, index) => {
                if (index > 0 && ((item.m_VerifyNo?item.m_VerifyNo.value:'') != debitfirstIndex)) {
                    verifyFlag_credit = true;
                    CDMoney_YB.verifyFlag = false;
                }
            })
            if (verifyFlag_credit) {
                toast({ content: this.state.json['20020VRIFYPAGE-000080'], color: 'warning' });/* 国际化处理： 核销号不同的分录应不能同时选择*/
                CDMoney_YB.verifyFlag = false;
                return CDMoney_YB;
            }
        }
        if (creditSelectData.length > 0) {
            CDMoney_YB.money_Y = this.handleSumCY('credit');
            CDMoney_YB.money_B = this.handleSumCZ('credit');
            CDMoney_YB.direct = 'credit';
            CDMoney_YB.m_voCreditDetails = creditSelectData;
        } else if (debitSelectData.length > 0) {
            CDMoney_YB.money_Y = this.handleSumCY('debit');
            CDMoney_YB.money_B = this.handleSumCZ('debit');
            CDMoney_YB.direct = 'debit';
            CDMoney_YB.m_voDebitDetails = debitSelectData;
        }

        return CDMoney_YB;
    }
    //本次核销原币和
    handleSumCY(type) {
        let cY_num = 0;
        let {
            checkedArrayDebit,
            debitDataArr,
            checkedArrayCredit,
            creditDataArr,
            cancellationY_num,
            debitRate
        } = this.state;
        if (type == "debit") {
            for (let i = 0; i < checkedArrayDebit.length; i++) {
                if (checkedArrayDebit[i] == true) {
                    if (debitDataArr[i]&&debitDataArr[i].m_dDebit_Money_Y) {
                        cY_num = accAdd(
                            cY_num,
                            debitDataArr && debitDataArr[i].m_dDebit_Money_Y.value
                        );
                    } else {
                        cY_num += 0;
                    }
                }
            }
        } else if (type == "credit") {
            for (let ii = 0; ii < checkedArrayCredit.length; ii++) {
                if (checkedArrayCredit[ii] == true) {
                    if (creditDataArr[ii]&&creditDataArr[ii].m_dCredit_Money_Y) {
                        cY_num = accAdd(
                            cY_num,
                            creditDataArr && creditDataArr[ii].m_dCredit_Money_Y.value
                        );
                    } else {
                        cY_num += 0;
                    }
                }
            }
        }
        return cY_num;
    }

    //本次核销组织金币和
    handleSumCZ(type) {
        let cZ_num = 0;
        let {
            checkedArrayDebit,
            debitDataArr,
            checkedArrayCredit,
            creditDataArr,
            cancellationY_num,
            debitRate
        } = this.state;
        if (type == "debit") {
            for (let i = 0; i < checkedArrayDebit.length; i++) {
                if (checkedArrayDebit[i] == true) {
                    if (debitDataArr[i]&&debitDataArr[i].m_dDebit_Money_B) {
                        cZ_num = accAdd(
                            cZ_num,
                            debitDataArr && debitDataArr[i].m_dDebit_Money_B.value
                        );
                        //cZ_num +=0;
                    } else {
                        //cZ_num += debitDataArr&&debitDataArr[i].m_dDebit_Money_B*1;
                        cZ_num += 0;
                    }
                }
            }
        } else if (type == "credit") {
            for (let ii = 0; ii < checkedArrayCredit.length; ii++) {
                if (checkedArrayCredit[ii] == true) {
                    if (creditDataArr[ii]&&creditDataArr[ii].m_dCredit_Money_B) {
                        cZ_num = accAdd(
                            cZ_num,
                            creditDataArr && creditDataArr[ii].m_dCredit_Money_B.value
                        );
                        //cZ_num +=0;
                    } else {
                        //cZ_num += debitDataArr&&debitDataArr[i].m_dDebit_Money_B*1;
                        cZ_num += 0;
                    }
                }
            }
        }

        return cZ_num;
    }

    //本次未核销原币和
    handleSumCUY(type) {
        let cUY_num = 0;
        const {
            checkedArrayDebit,
            debitDataArr,
            checkedArrayCredit,
            creditDataArr,
            cancellationY_num,
            debitRate
        } = this.state;
        if (type == "debit") {
            for (let i = 0; i < checkedArrayDebit.length; i++) {
                //if(checkedArrayDebit[i] == false){
                if (debitDataArr[i]&&debitDataArr[i].m_Balancedebitamount) {
                    cUY_num = accAdd(
                        cUY_num,
                        debitDataArr && debitDataArr[i].m_Balancedebitamount.value
                    );
                } else {
                    cUY_num = 0;
                }
                //}
            }
        } else if (type == "credit") {
            for (let ii = 0; ii < checkedArrayCredit.length; ii++) {
                //if(checkedArrayCredit[i] == false){
                if (creditDataArr[ii]&&creditDataArr[ii].m_Balancecreditamount) {
                    cUY_num = accAdd(
                        cUY_num,
                        creditDataArr && creditDataArr[ii].m_Balancecreditamount.value
                    );
                } else {
                    cUY_num = 0;
                }
                //}
            }
        }
        return cUY_num;
    }
    //本次未核销组织金币和
    handleSumCUZ(type) {
        let cUZ_num = 0;
        const {
            checkedArrayDebit,
            debitDataArr,
            checkedArrayCredit,
            creditDataArr,
            cancellationY_num,
            debitRate
        } = this.state;
        if (type == "debit") {
            for (let i = 0; i < checkedArrayDebit.length; i++) {
                //if(checkedArrayDebit[i] == false){
                if (debitDataArr[i]&&debitDataArr[i].m_Balancelocaldebitamount) {
                    cUZ_num = accAdd(
                        cUZ_num,
                        debitDataArr &&
                        debitDataArr[i].m_Balancelocaldebitamount.value
                    );
                } else {
                    cUZ_num = 0;
                }
                //}
            }
        } else if (type == "credit") {
            for (let ii = 0; ii < checkedArrayCredit.length; ii++) {
                //if(checkedArrayCredit[i] == false){
                if (creditDataArr[ii] &&creditDataArr[ii].m_Balancelocalcreditamount) {
                    cUZ_num = accAdd(
                        cUZ_num,
                        creditDataArr &&
                        creditDataArr[ii].m_Balancelocalcreditamount.value
                    );
                } else {
                    cUZ_num = 0;
                }
                //}
            }
        }
        return cUZ_num;
    }
   
    // 单选 控制 debitBoxClicked状态
    handleChecked(type, index) {
        const {
            checkedArrayDebit,
            checkedArrayCredit
        } = this.state;
        let count = 0;

        if (type == "debit") {
            let len = checkedArrayDebit.length;
            for (let i = 0; i < len; i++) {
                if (checkedArrayDebit[i] != false) {
                } else {
                    count++;
                }
                if (count == len) {
                    this.setState({
                        debitBoxClicked: false
                    });
                } else {
                    this.setState({
                        debitBoxClicked: true
                    });
                }
            }
        } else if (type == "credit") {
            let len = checkedArrayCredit.length;
            for (let ii = 0; ii < len; ii++) {
                if (checkedArrayCredit[ii] != false) {
                } else {
                    count++;
                }
                if (count == len) {
                    this.setState({
                        creditBoxClicked: false
                    });
                } else {
                    this.setState({
                        creditBoxClicked: true
                    });
                }
            }
        }
    }

    // 借贷方全选 控制 debitBoxClicked状态
    handleAllChecked(type) {
        const {
            checkedArrayDebit,
            checkedArrayCredit
        } = this.state;
        if (type == "debit") {
            for (let i = 0; i < checkedArrayDebit.length; i++) {
                if (checkedArrayDebit[i] == true) {
                    this.setState({
                        debitBoxClicked: true
                    });
                }
            }
        } else if (type == "credit") {
            for (let ii = 0; ii < checkedArrayCredit.length; ii++) {
                if (checkedArrayCredit[ii] == true) {
                    this.setState({
                        creditBoxClicked: true
                    });
                }
            }
        }
    }

    // 原币 与组织本币之间的换算
    handleConversion(y) {
        let z = 1;
        const { debitRate } = this.state;
        if (y == null) {
            return null;
        } else {
            // z = y * debitRate;
            z = y
            return z;
        }
    }

    // 点击单个单选按钮 获取修改后的数据
    handleCancel(type, index, bool) {
        const { debitDataArr, creditDataArr } = this.state;
        let benY = 0,
            benZ = 0,
            weiY = 0,
            weiZ = 0,
            obj = {};
        if (type == "debit") {
            obj = {
                m_dDebit_Money_Y: debitDataArr[index].m_Balancedebitamount.value,
                m_Balancedebitamount: 0
            };
            if (bool == true) {
                //m_dDebit_Money_Y ,m_dDebit_Money_B,m_Balancedebitamount,m_Balancelocaldebitamount,m_Balancedebitamount
                debitDataArr[index].m_bSelected = 'Y';
                debitDataArr[index].m_bSelected = 'Y';
                debitDataArr[index].m_dDebit_Money_Y.display = obj.m_dDebit_Money_Y;
                debitDataArr[index].m_dDebit_Money_B.display = debitDataArr[index].m_Balancelocaldebitamount.display;
                debitDataArr[index].m_Balancedebitamount.display = obj.m_Balancedebitamount;
                debitDataArr[index].m_Balancelocaldebitamount.display = 0;
                debitDataArr[index].m_dDebit_Money_Y.value = obj.m_dDebit_Money_Y;
                debitDataArr[index].m_dDebit_Money_B.value = debitDataArr[index].m_Balancelocaldebitamount.value;
                debitDataArr[index].m_Balancedebitamount.value = obj.m_Balancedebitamount;
                debitDataArr[index].m_Balancelocaldebitamount.value = 0;
                debitDataArr[index].m_dDebit_Money_Y.prevValue = obj.m_dDebit_Money_Y;
                debitDataArr[index].m_dDebit_Money_B.prevValue = debitDataArr[index].m_Balancelocaldebitamount.value;
                return debitDataArr;
            } else {
                debitDataArr[index].m_bSelected = 'N';
                debitDataArr[index].m_bSelected = 'N';
                debitDataArr[index].m_dDebit_Money_Y.display = 0;
                debitDataArr[index].m_dDebit_Money_B.display = 0;
                debitDataArr[index].m_Balancedebitamount.display = debitDataArr[index].empty_m_Balancedebitamount.display;
                debitDataArr[index].m_Balancelocaldebitamount.display = debitDataArr[index].empty_m_Balancelocaldebitamount.display;
                debitDataArr[index].m_dDebit_Money_Y.value = 0;
                debitDataArr[index].m_dDebit_Money_B.value = 0;
                debitDataArr[index].m_Balancedebitamount.value = debitDataArr[index].empty_m_Balancedebitamount.value;
                debitDataArr[index].m_Balancelocaldebitamount.value = debitDataArr[index].empty_m_Balancelocaldebitamount.value;
                return debitDataArr;
            }
        } else if (type == "credit") {
            obj = {
                m_dCredit_Money_Y: creditDataArr[index].m_Balancecreditamount.value,
                m_Balancecreditamount: 0
            };
            if (bool == true) {
                //m_dDebit_Money_Y ,m_dDebit_Money_B,m_Balancedebitamount,m_Balancelocaldebitamount,m_Balancedebitamount
                creditDataArr[index].m_bSelected = 'Y';
                creditDataArr[index].m_bSelected = 'Y';
                creditDataArr[index].m_dCredit_Money_Y.display = obj.m_dCredit_Money_Y;
                creditDataArr[index].m_dCredit_Money_B.display = creditDataArr[index].m_Balancelocalcreditamount.display;
                creditDataArr[index].m_Balancecreditamount.display = obj.m_Balancecreditamount;
                creditDataArr[index].m_Balancelocalcreditamount.display = 0;
                creditDataArr[index].m_dCredit_Money_Y.value = obj.m_dCredit_Money_Y;
                creditDataArr[index].m_dCredit_Money_B.value = creditDataArr[index].m_Balancelocalcreditamount.value;
                creditDataArr[index].m_Balancecreditamount.value = obj.m_Balancecreditamount;
                creditDataArr[index].m_Balancelocalcreditamount.value = 0;
                creditDataArr[index].m_dCredit_Money_Y.prevValue = obj.m_dCredit_Money_Y;
                creditDataArr[index].m_dCredit_Money_B.prevValue = creditDataArr[index].m_Balancelocalcreditamount.value;
                return creditDataArr;
            } else {
                creditDataArr[index].m_bSelected = 'N';
                creditDataArr[index].m_bSelected = 'N';
                creditDataArr[index].m_dCredit_Money_Y.display = 0;
                creditDataArr[index].m_dCredit_Money_B.display = 0;
                creditDataArr[index].m_Balancecreditamount.display = creditDataArr[index].empty_m_Balancecreditamount.display;
                creditDataArr[index].m_Balancelocalcreditamount.display = creditDataArr[index].empty_m_Balancelocalcreditamount.display;
                creditDataArr[index].m_dCredit_Money_Y.value = 0;
                creditDataArr[index].m_dCredit_Money_B.value = 0;
                creditDataArr[index].m_Balancecreditamount.value = creditDataArr[index].empty_m_Balancecreditamount.value;
                creditDataArr[index].m_Balancelocalcreditamount.value = creditDataArr[index].empty_m_Balancelocalcreditamount.value;
                return creditDataArr;
            }
        }
    }

    // 点击全选按钮 获取修改后的数据
    handleAllChicked(type) {
        let {
            debitDataArr,
            checkedArrayDebit,
            creditDataArr,
            checkedArrayCredit
        } = this.state;
        let obj = {};
        let brr = [];
        let count = 0;
        if (type == "debit") {
            let len = checkedArrayDebit.length;
            for (let i = 0; i < debitDataArr.length; i++) {
                brr.push({
                    m_dDebit_Money_Y: debitDataArr[i].m_dDebit_Money_Y.value?accAdd(debitDataArr[i].m_dDebit_Money_Y.value,debitDataArr[i].m_Balancedebitamount.value):debitDataArr[i].m_Balancedebitamount.value,
                    m_Balancedebitamount: 0
                });
            }

            for (let ii = 0; ii < len; ii++) {
                if (checkedArrayDebit[ii] == true) {
                    count++;
                }
            }

            // if (len == count) {
            //m_dDebit_Money_Y 原币 ,m_dDebit_Money_B 组织本币,m_Balancedebitamount 未核销原币,m_Balancelocaldebitamount 未核销组织本币
            // for (let j = 0; j < debitDataArr.length; j++) {
            checkedArrayDebit.map((k, j) => {
                if (k) {
                    debitDataArr[j].m_dDebit_Money_Y.display = brr[j].m_dDebit_Money_Y;
                    debitDataArr[j].m_dDebit_Money_B.display = debitDataArr[j].m_dDebit_Money_B.display?accAdd(debitDataArr[j].m_dDebit_Money_B.display,debitDataArr[j].m_Balancelocaldebitamount.value):debitDataArr[j].m_Balancelocaldebitamount.value;
                    debitDataArr[j].m_dDebit_Money_Y.value = brr[j].m_dDebit_Money_Y;
                    debitDataArr[j].m_dDebit_Money_B.value = debitDataArr[j].m_dDebit_Money_B.value?accAdd(debitDataArr[j].m_dDebit_Money_B.value,debitDataArr[j].m_Balancelocaldebitamount.value):debitDataArr[j].m_Balancelocaldebitamount.value;
                    
                    debitDataArr[j].m_Balancedebitamount.display = brr[j].m_Balancedebitamount;//debitDataArr[j].m_dDebit_Money_Y.value&&debitDataArr[j].m_dDebit_Money_Y.prevValue?debitDataArr[j].m_Balancedebitamount.display:brr[j].m_Balancedebitamount;
                    debitDataArr[j].m_Balancelocaldebitamount.display =0;//debitDataArr[j].m_dDebit_Money_B.value&&debitDataArr[j].m_dDebit_Money_Y.prevValue?debitDataArr[j].m_Balancelocaldebitamount.display: 0;
                    
                    debitDataArr[j].m_Balancedebitamount.value = brr[j].m_Balancedebitamount;//debitDataArr[j].m_dDebit_Money_Y.value&&debitDataArr[j].m_dDebit_Money_Y.prevValue?debitDataArr[j].m_Balancedebitamount.value:brr[j].m_Balancedebitamount;
                    debitDataArr[j].m_Balancelocaldebitamount.value =0;// debitDataArr[j].m_dDebit_Money_B.value&&debitDataArr[j].m_dDebit_Money_Y.prevValue?debitDataArr[j].m_Balancelocaldebitamount.value:0;
                    debitDataArr[j].m_dDebit_Money_Y.prevValue = brr[j].m_dDebit_Money_Y;
                    debitDataArr[j].m_dDebit_Money_B.prevValue = debitDataArr[j].m_Balancelocaldebitamount.value;
                } else {
                    debitDataArr[j].m_dDebit_Money_Y.display = 0// brr[j].m_dDebit_Money_Y;
                    debitDataArr[j].m_dDebit_Money_B.display = 0;
                    debitDataArr[j].m_dDebit_Money_Y.value = 0//brr[j].m_dDebit_Money_Y;
                    debitDataArr[j].m_dDebit_Money_B.value = 0;
                    debitDataArr[j].m_Balancedebitamount.display = debitDataArr[j].empty_m_Balancedebitamount.value;
                    debitDataArr[j].m_Balancelocaldebitamount.display = debitDataArr[j].empty_m_Balancelocaldebitamount.value;
                    
                    debitDataArr[j].m_Balancedebitamount.value = debitDataArr[j].empty_m_Balancedebitamount.value;
                    debitDataArr[j].m_Balancelocaldebitamount.value = debitDataArr[j].empty_m_Balancelocaldebitamount.value;
                    debitDataArr[j].m_dDebit_Money_Y.prevValue = 0//brr[j].m_dDebit_Money_Y;
                    debitDataArr[j].m_dDebit_Money_B.prevValue = 0;
                }
            })
            // }
            return debitDataArr;
        } else if (type == "credit") {
            let len = checkedArrayCredit.length;
            for (let m = 0; m < creditDataArr.length; m++) {
                brr.push({
                    m_dCredit_Money_Y: creditDataArr[m].m_dCredit_Money_Y.value?accAdd(creditDataArr[m].m_dCredit_Money_Y.value,creditDataArr[m].m_Balancecreditamount.value):creditDataArr[m].m_Balancecreditamount.value,
                    m_Balancecreditamount: 0
                });
            }

            for (let mm = 0; mm < len; mm++) {
                if (checkedArrayCredit[mm] == true) {
                    count++;
                }
            }

            // if (len == count) {
            //m_dDebit_Money_Y 原币,m_dDebit_Money_B 组织本币,m_Balancedebitamount 未核销原币,m_Balancelocaldebitamount 未核销组织本币
            // for (let n = 0; n < creditDataArr.length; n++) {
            checkedArrayCredit.map((k, n) => {
                if (k) {
                    creditDataArr[n].m_dCredit_Money_Y.display = brr[n].m_dCredit_Money_Y;
                    creditDataArr[n].m_dCredit_Money_B.display = creditDataArr[n].m_dCredit_Money_B.display?accAdd(creditDataArr[n].m_dCredit_Money_B.display,creditDataArr[n].m_Balancelocalcreditamount.value):creditDataArr[n].m_Balancelocalcreditamount.value;
                    creditDataArr[n].m_dCredit_Money_Y.value = brr[n].m_dCredit_Money_Y;
                    creditDataArr[n].m_dCredit_Money_B.value = creditDataArr[n].m_dCredit_Money_B.value?accAdd(creditDataArr[n].m_dCredit_Money_B.value,creditDataArr[n].m_Balancelocalcreditamount.value):creditDataArr[n].m_Balancelocalcreditamount.value;
                    
                    creditDataArr[n].m_Balancecreditamount.display =brr[n].m_Balancecreditamount;// creditDataArr[n].m_dCredit_Money_Y.value&&creditDataArr[n].m_dCredit_Money_Y.prevValue?creditDataArr[n].m_Balancecreditamount.display:brr[n].m_Balancecreditamount;
                    creditDataArr[n].m_Balancelocalcreditamount.display =0;// creditDataArr[n].m_dCredit_Money_B.value&&creditDataArr[n].m_dCredit_Money_Y.prevValue?creditDataArr[n].m_Balancelocalcreditamount.display:0;
                    
                    creditDataArr[n].m_Balancecreditamount.value =brr[n].m_Balancecreditamount;// creditDataArr[n].m_dCredit_Money_Y.value&&creditDataArr[n].m_dCredit_Money_Y.prevValue?creditDataArr[n].m_Balancecreditamount.value:brr[n].m_Balancecreditamount;
                    creditDataArr[n].m_Balancelocalcreditamount.value =0;// creditDataArr[n].m_dCredit_Money_B.value&&creditDataArr[n].m_dCredit_Money_Y.prevValue?creditDataArr[n].m_Balancelocalcreditamount.value:0;
                    creditDataArr[n].m_dCredit_Money_Y.prevValue = brr[n].m_dCredit_Money_Y;
                    creditDataArr[n].m_dCredit_Money_B.prevValue = creditDataArr[n].m_Balancelocalcreditamount.value;
                } else {
                    creditDataArr[n].m_dCredit_Money_Y.display = 0;// brr[n].m_dCredit_Money_Y;
                    creditDataArr[n].m_dCredit_Money_B.display = 0;
                    creditDataArr[n].m_dCredit_Money_Y.value = 0;// brr[n].m_dCredit_Money_Y;
                    creditDataArr[n].m_dCredit_Money_B.value = 0;
                    creditDataArr[n].m_Balancecreditamount.display = creditDataArr[n].empty_m_Balancecreditamount.value;
                    creditDataArr[n].m_Balancelocalcreditamount.display = creditDataArr[n].empty_m_Balancelocalcreditamount.value;
                    
                    creditDataArr[n].m_Balancecreditamount.value = creditDataArr[n].empty_m_Balancecreditamount.value;
                    creditDataArr[n].m_Balancelocalcreditamount.value = creditDataArr[n].empty_m_Balancelocalcreditamount.value;
                    creditDataArr[n].m_dCredit_Money_Y.prevValue = 0// brr[n].m_dCredit_Money_Y;
                    creditDataArr[n].m_dCredit_Money_B.prevValue = 0;
                }
            })
            // }
            return creditDataArr;
        }
    }

    getTableHeightPosi = () => {
        let tableHeight=(document.getElementById('app').offsetHeight-292)/2;
        return tableHeight;
        }
    getTableHeight = () => {
        let accountContentHeight = this.refs.accountContent && getComputedStyle(this.refs.accountContent, null).height || "400px";
        let tableHeight = accountContentHeight.replace('px', '') - 200;
        return tableHeight;
    }
    getTableHeighttwo = () => {
        let h = (window.innerHeight - 265 + "px")
        return h;
    }
     //合并计算本次核销/未核销的原币组织本币的合计
     handleSumAmount=(type)=>{ 
        let {SumAmount,checkedArrayDebit,debitDataArr,checkedArrayCredit,creditDataArr,}=this.state;
        let cY_num = 0,cZ_num = 0,cUY_num = 0,cUZ_num = 0;
        if (type == "debit") {
            SumAmount.debitY_num=0; //借方的核销余额（原）
            SumAmount.debitZ_num=0; //借方的核销余额（组）
            SumAmount.debitUY_num=0; //借方的未核销余额（原）
            SumAmount.debitUZ_num=0; //借方的未核销余额（组）
            for (let i = 0; i < checkedArrayDebit.length; i++) {
                if (checkedArrayDebit[i] == true) {
                    if (debitDataArr[i]&&debitDataArr[i].m_dDebit_Money_Y) {
                        SumAmount.debitY_num = accAdd(
                            SumAmount.debitY_num,
                            debitDataArr && debitDataArr[i].m_dDebit_Money_Y.value
                        );
                    } else {
                        SumAmount.debitY_num += 0;
                    }
                    if (debitDataArr[i]&&debitDataArr[i].m_dDebit_Money_B) {
                        SumAmount.debitZ_num = accAdd(
                            SumAmount.debitZ_num,
                            debitDataArr && debitDataArr[i].m_dDebit_Money_B.value
                        );
                    } else {
                        SumAmount.debitZ_num += 0;
                    }
                }
                if (debitDataArr[i]&&debitDataArr[i].m_Balancedebitamount) {
                    SumAmount.debitUY_num = accAdd(
                        SumAmount.debitUY_num,
                        debitDataArr && debitDataArr[i].m_Balancedebitamount.value
                    );
                } else {
                    SumAmount.debitUY_num = 0;
                }
                if (debitDataArr[i]&&debitDataArr[i].m_Balancelocaldebitamount) {
                    SumAmount.debitUZ_num = accAdd(
                        SumAmount.debitUZ_num,
                        debitDataArr &&
                        debitDataArr[i].m_Balancelocaldebitamount.value
                    );
                } else {
                    SumAmount.debitUZ_num = 0;
                }
            }
        } else if (type == "credit") {
            SumAmount.creditY_num=0; //贷方的核销余额（原）
            SumAmount.creditZ_num=0; //贷方的核销余额（组）
            SumAmount.creditUY_num=0; //贷方的未核销余额（原）
            SumAmount.creditUZ_num=0; //贷方的未核销余额（组）
            for (let ii = 0; ii < checkedArrayCredit.length; ii++) {
                if (checkedArrayCredit[ii] == true) {
                    if (creditDataArr[ii]&&creditDataArr[ii].m_dCredit_Money_Y) {
                        SumAmount.creditY_num = accAdd(
                            SumAmount.creditY_num,
                            creditDataArr && creditDataArr[ii].m_dCredit_Money_Y.value
                        );
                    } else {
                        SumAmount.creditY_num += 0;
                    }
                    if (creditDataArr[ii]&&creditDataArr[ii].m_dCredit_Money_B) {
                        SumAmount.creditZ_num = accAdd(
                            SumAmount.creditZ_num,
                            creditDataArr && creditDataArr[ii].m_dCredit_Money_B.value
                        );
                    } else {
                        SumAmount.creditZ_num += 0;
                    }
                }
                if (creditDataArr[ii]&&creditDataArr[ii].m_Balancecreditamount) {
                    SumAmount.creditUY_num = accAdd(
                        SumAmount.creditUY_num,
                        creditDataArr && creditDataArr[ii].m_Balancecreditamount.value
                    );
                } else {
                    SumAmount.creditUY_num = 0;
                }
                if (creditDataArr[ii] &&creditDataArr[ii].m_Balancelocalcreditamount) {
                    SumAmount.creditUZ_num = accAdd(
                        SumAmount.creditUZ_num,
                        creditDataArr &&
                        creditDataArr[ii].m_Balancelocalcreditamount.value
                    );
                } else {
                    SumAmount.creditUZ_num = 0;
                }
            }
        }
        this.setState({SumAmount});
    }
    //导出原币组织本币合计值组件
    exportInput=(type)=>{
        let that=this;
        let {debitUY_num,debitUZ_num,creditUY_num,creditUZ_num,debitBoxClicked,creditBoxClicked,
            creditScale_Y,creditScale_B,debitScale_Y,debitScale_B,SumAmount}=that.state;
        if(type=='debit'){
            return(
                <NCDiv fieldid="total" areaCode={NCDiv.config.Area}>
        <div className=" nc-theme-form-label-c">
            <ul className="account-content1-credit-foot-first">
                <li >{that.state.json['20020VRIFYPAGE-000071']}</li>{/* 国际化处理： 本次核销金额*/}
                <li>
                    {that.state.json['20020VRIFYPAGE-000072']}：{/* 国际化处理： 原*/}
                    <NCNumber
                        fieldid="debitY_num"
                        scale={Number(debitScale_Y)}
                        disabled
                        value={SumAmount.debitY_num}
                        onChange={v => { }}
                    />
                </li>
                <li>
                    {that.state.json['20020VRIFYPAGE-000073']}：{/* 国际化处理： 组*/}
                    <NCNumber
                        fieldid="debitZ_num"
                        scale={Number(debitScale_B)}
                        disabled
                        value={SumAmount.debitZ_num}
                        onChange={v => { }}
                    />
                </li>
            </ul>
            <ul className="account-content1-credit-foot-secend">
                <li className="alignright">{that.state.json['20020VRIFYPAGE-000057']}</li>{/* 国际化处理： 未核销余额*/}
                <li>
                    {that.state.json['20020VRIFYPAGE-000072']}：{/* 国际化处理： 原*/}
                    <NCNumber
                        fieldid="debitUY_num"
                        scale={Number(debitScale_Y)}
                        disabled
                        value={SumAmount.debitUY_num}
                        onChange={v => { }}
                    />
                </li>
                <li>
                    {that.state.json['20020VRIFYPAGE-000073']}：{/* 国际化处理： 组*/}
                    <NCNumber
                        fieldid="debitUZ_num"
                        scale={Number(debitScale_B)}
                        disabled
                        value={SumAmount.debitUZ_num}
                        onChange={v => { }}
                    />
                </li>
            </ul>
        </div></NCDiv>)
        }else if(type=='credit'){
            return(
                <NCDiv fieldid="total" areaCode={NCDiv.config.Area}>
                <div className=" nc-theme-form-label-c">
            <ul className="account-content1-credit-foot-first">
                <li>{that.state.json['20020VRIFYPAGE-000071']}</li>{/* 国际化处理： 本次核销金额*/}
                <li>
                    {that.state.json['20020VRIFYPAGE-000072']}：{/* 国际化处理： 原*/}
                    <NCNumber
                        fieldid="creditY_num"
                        scale={Number(creditScale_Y)}
                        disabled
                        value={SumAmount.creditY_num}
                        onChange={v => { }}
                    />
                </li>
                <li>
                    {that.state.json['20020VRIFYPAGE-000073']}：{/* 国际化处理： 组*/}
                    <NCNumber
                        fieldid="creditZ_num"
                        scale={Number(creditScale_B)}
                        disabled
                        value={SumAmount.creditZ_num}
                        onChange={v => { }}
                    />
                </li>
            </ul>
            <ul className="account-content1-credit-foot-secend">
                <li className="alignright">{that.state.json['20020VRIFYPAGE-000057']}</li>{/* 国际化处理： 未核销余额*/}
                <li>
                    {that.state.json['20020VRIFYPAGE-000072']}：{/* 国际化处理： 原*/}
                    <NCNumber
                        fieldid="creditUY_num"
                        scale={Number(creditScale_Y)}
                        disabled
                        value={SumAmount.creditUY_num}
                        onChange={v => { }}
                    />
                </li>
                <li>
                    {that.state.json['20020VRIFYPAGE-000073']}：{/* 国际化处理： 组*/}
                    <NCNumber
                        fieldid="creditUZ_num"
                        scale={Number(creditScale_B)}
                        disabled
                        value={SumAmount.creditUZ_num}
                        onChange={v => { }}
                    />
                </li>
            </ul>
        </div></NCDiv>)
        }
    }
    //获取table列
    getCreditOrDebitCoumn=(type,json)=>{
        if(type=='credit'){
            return([
                // this.columns_credit = [
                    //贷方
                    {
                        title: (<div fieldid="m_pk_unit_v" className="mergecells">{json['20020VRIFYPAGE-000017']}</div>),/* 国际化处理： 业务单元*/
                        dataIndex: "m_pk_unit_v", //m_voAss
                        key: "m_pk_unit_v",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_pk_unit_v">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_sSubjCode" className="mergecells">{json['20020VRIFYPAGE-000051']}</div>),/* 国际化处理： 科目编码*/
                        dataIndex: "m_sSubjCode",
                        key: "m_sSubjCode",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_sSubjCode">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_sSubjName" className="mergecells">{json['20020VRIFYPAGE-000052']}</div>),/* 国际化处理： 科目*/
                        dataIndex: "m_sSubjName",
                        key: "m_sSubjName",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_sSubjName">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_prepareddate" className="mergecells">{json['20020VRIFYPAGE-000021']}</div>),/* 国际化处理： 凭证日期*/
                        dataIndex: "m_prepareddate",
                        key: "m_prepareddate",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_prepareddate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_Businessdate" className="mergecells">{json['20020VRIFYPAGE-000022']}</div>),/* 国际化处理： 业务日期*/
                        dataIndex: "m_Businessdate",
                        key: "m_Businessdate",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_Businessdate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_VerifyNo" className="mergecells">{json['20020VRIFYPAGE-000035']}</div>),/* 国际化处理： 核销号*/
                        dataIndex: "m_VerifyNo",
                        key: "m_VerifyNo",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_VerifyNo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_netbankflag" className="mergecells">{json['20020VRIFYPAGE-000075']}</div>),/* 国际化处理： 银行对账标识码*/
                        dataIndex: "m_netbankflag",
                        key: "m_netbankflag",
                        width: 150,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_netbankflag">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_VoucherNo" className="mergecells">{json['20020VRIFYPAGE-000053']}</div>),/* 国际化处理： 凭证号*/
                        dataIndex: "m_VoucherNo",
                        key: "m_VoucherNo",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_VoucherNo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_detailindex" className="mergecells">{json['20020VRIFYPAGE-000054']}</div>),/* 国际化处理： 分录号*/
                        dataIndex: "m_detailindex",
                        key: "m_detailindex",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_detailindex">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="assinfo" className="mergecells">{json['20020VRIFYPAGE-000003']}</div>),/* 国际化处理： 辅助核算*/
                        dataIndex: "assinfo",
                        key: "assinfo",
                        width: 200,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="assinfo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_explanation" className="mergecells">{json['20020VRIFYPAGE-000029']}</div>),/* 国际化处理： 摘要*/
                        dataIndex: "m_explanation",
                        key: "m_explanation",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_explanation">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: json['20020VRIFYPAGE-000055'],/* 国际化处理： 金额*/
                        children: [
                            {
                                title: (<div fieldid="m_creditamount" className="mergecells">{json['20020VRIFYPAGE-000024']}</div>),/* 国际化处理： 原币*/
                                className: "yeson",
                                dataIndex: "m_creditamount",
                                key: "m_creditamount",
                                width: 150,
                                render: (text, record, index) => {
                                    return (
                                        <div className="right" fieldid="m_creditamount">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                                    );
                                }
                            },
                            {
                                title: (<div fieldid="m_localcreditamount" className="mergecells">{json['20020VRIFYPAGE-000025']}</div>),/* 国际化处理： 组织本币*/
                                className: "yeson",
                                dataIndex: "m_localcreditamount",
                                key: "m_localcreditamount",
                                width: 150,
                                render: (text, record, index) => {
                                    return (
                                        <div className="right" fieldid="m_localcreditamount">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                                    );
                                }
                            }
                        ]
                    },
                    {
                        title: json['20020VRIFYPAGE-000056'],/* 国际化处理： 本次核销*/
                        children: [
                            {
                                title: (<div fieldid="m_dCredit_Money_Y" className="mergecells">{json['20020VRIFYPAGE-000024']}</div>),/* 国际化处理： 原币*/
                                dataIndex: "m_dCredit_Money_Y",
                                key: "m_dCredit_Money_Y",
                                width: 150,
                                render: (text, record, index) => {
                                    let {
                                        checkedArrayCredit,
                                        debitRate,
                                        queryCondition,
                                        CompareStatus, creditOrDebitFlag
                                    } = this.state;
                                    if (checkedArrayCredit[index] && creditOrDebitFlag != 'credit') {
                                        // 复选框被选中状态时return出可编辑的输入框
                                        return (
                                            <div fieldid="m_dCredit_Money_Y">
                                                <NCNumber
                                                    fieldid="m_dCredit_Money_Y"
                                                    value={text==null?null:text.display}
                                                    scale={Number(text.scale)}
                                                    title={json['20020VRIFYPAGE-000076']}/* 国际化处理： 输入的原币数值不可大于组织本币*/
                                                    onBlur={v => {
                                                        let { creditDataArr } = this.state;
                                                        if (creditDataArr[index].empty_m_Balancecreditamount.value - 0 >0) {
                                                            if (v - 0 < 0) {
                                                                creditDataArr[index].m_dCredit_Money_Y.display = creditDataArr[index].m_dCredit_Money_Y.prevValue;// creditDataArr[index].empty_m_Balancecreditamount.value;
                                                                creditDataArr[index].m_Balancecreditamount.display = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);//0;
                                                                creditDataArr[index].m_dCredit_Money_Y.value = creditDataArr[index].m_dCredit_Money_Y.prevValue;// creditDataArr[index].empty_m_Balancecreditamount.value;
                                                                creditDataArr[index].m_Balancecreditamount.value = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);//0;
                                                                // creditDataArr[index].m_dCredit_Money_Y.display = 0;
                                                                // creditDataArr[index].m_Balancecreditamount.display =creditDataArr[index].empty_m_Balancecreditamount.value;
                                                                // creditDataArr[index].m_dCredit_Money_Y.value = 0;
                                                                // creditDataArr[index].m_Balancecreditamount.value =creditDataArr[index].empty_m_Balancecreditamount.value;
            
                                                                if (queryCondition.pk_currency ==queryCondition.account_currency) {
                                                                    //币种相同组织本币不可以编辑的，跟着原币变化
                                                                    creditDataArr[index].m_dCredit_Money_B.display = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y.value);
                                                                    creditDataArr[index].m_Balancelocalcreditamount.display = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                    creditDataArr[index].m_dCredit_Money_B.value = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y.value);
                                                                    creditDataArr[index].m_Balancelocalcreditamount.value = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                }
                                                            } else {
                                                                if (Math.abs(v - 0) > Math.abs(creditDataArr[index].empty_m_Balancecreditamount.value - 0)) {
            
                                                                    creditDataArr[index].m_dCredit_Money_Y.display = creditDataArr[index].m_dCredit_Money_Y.prevValue;// creditDataArr[index].empty_m_Balancecreditamount.value;
                                                                    creditDataArr[index].m_dCredit_Money_Y.value = creditDataArr[index].m_dCredit_Money_Y.prevValue;// creditDataArr[index].empty_m_Balancecreditamount.value;
                                                                    creditDataArr[index].m_Balancecreditamount.value = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);//0;
                                                                    creditDataArr[index].m_Balancecreditamount.display = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);//0;
            
                                                                    if (queryCondition.pk_currency ==queryCondition.account_currency) {
                                                                        //币种相同组织本币不可以编辑的，跟着原币变化
                                                                        creditDataArr[index].m_dCredit_Money_B.display = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y.value);
                                                                        creditDataArr[index].m_Balancelocalcreditamount.display = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                        creditDataArr[index].m_dCredit_Money_B.value = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y.value);
                                                                        creditDataArr[index].m_Balancelocalcreditamount.value = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                    }
                                                                } else {
                                                                    creditDataArr[index].m_dCredit_Money_Y.display = v ? v : 0;
                                                                    creditDataArr[index].m_dCredit_Money_Y.value = v ? v : 0;
                                                                    creditDataArr[index].m_dCredit_Money_Y.prevValue = v ? v : 0;//prevValue存储当前输入的值
                                                                    creditDataArr[index].m_Balancecreditamount.display = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);
                                                                    creditDataArr[index].m_Balancecreditamount.value = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);
                                                                    if (queryCondition.pk_currency ==queryCondition.account_currency) {
                                                                        //币种相同组织本币不可以编辑的，跟着原币变化
                                                                        creditDataArr[index].m_dCredit_Money_B.display = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y.value);
                                                                        creditDataArr[index].m_Balancelocalcreditamount.display = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                        creditDataArr[index].m_dCredit_Money_B.value = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y.value);
                                                                        creditDataArr[index].m_Balancelocalcreditamount.value = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            if (v - 0 < 0) {
                                                                if (
                                                                    Math.abs(v - 0) > Math.abs(creditDataArr[index].empty_m_Balancecreditamount.value - 0)
                                                                ) {
                                                                    creditDataArr[index].m_dCredit_Money_Y.display = creditDataArr[index].m_dCredit_Money_Y.prevValue;// creditDataArr[index].empty_m_Balancecreditamount.value;
                                                                    creditDataArr[index].m_dCredit_Money_Y.value = creditDataArr[index].m_dCredit_Money_Y.prevValue;//creditDataArr[index].empty_m_Balancecreditamount.value;
                                                                    creditDataArr[index].m_Balancecreditamount.value = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);//0;
                                                                    creditDataArr[index].m_Balancecreditamount.display = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);//0;
            
                                                                    if (queryCondition.pk_currency ==queryCondition.account_currency) {
                                                                        //币种相同组织本币不可以编辑的，跟着原币变化
                                                                        creditDataArr[index].m_dCredit_Money_B.display = this.handleConversion([index].m_dCredit_Money_Y.value);
                                                                        creditDataArr[index].m_Balancelocalcreditamount.display = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                        creditDataArr[index].m_dCredit_Money_B.value = this.handleConversion([index].m_dCredit_Money_Y.value);
                                                                        creditDataArr[index].m_Balancelocalcreditamount.value = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                    }
                                                                } else {
                                                                    creditDataArr[index].m_dCredit_Money_Y.display = v ? v : 0;
                                                                    creditDataArr[index].m_dCredit_Money_Y.value = v ? v : 0;
                                                                    creditDataArr[index].m_dCredit_Money_Y.prevValue = v ? v : 0;
                                                                    creditDataArr[index].m_Balancecreditamount.display = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);
                                                                    creditDataArr[index].m_Balancecreditamount.value = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);
                                                                    if (queryCondition.pk_currency ==queryCondition.account_currency) {
                                                                        //币种相同组织本币不可以编辑的，跟着原币变化
                                                                        creditDataArr[index].m_dCredit_Money_B.display = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y.value);
                                                                        creditDataArr[index].m_Balancelocalcreditamount.display = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                        creditDataArr[index].m_dCredit_Money_B.value = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y.value);
                                                                        creditDataArr[index].m_Balancelocalcreditamount.value = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                    }
                                                                }
                                                            } else {
                                                                creditDataArr[index].m_dCredit_Money_Y.display = creditDataArr[index].m_dCredit_Money_Y.prevValue;//creditDataArr[index].empty_m_Balancecreditamount.value;
                                                                creditDataArr[index].m_dCredit_Money_Y.value = creditDataArr[index].m_dCredit_Money_Y.prevValue;// creditDataArr[index].empty_m_Balancecreditamount.value;
                                                                creditDataArr[index].m_Balancecreditamount.value = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);//0;
                                                                creditDataArr[index].m_Balancecreditamount.display = Subtr(creditDataArr[index].empty_m_Balancecreditamount.value, creditDataArr[index].m_dCredit_Money_Y.value);//0;
            
                                                                if (queryCondition.pk_currency ==queryCondition.account_currency) {
                                                                    //币种相同组织本币不可以编辑的，跟着原币变化
                                                                    creditDataArr[index].m_dCredit_Money_B.display = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y.value);
                                                                    creditDataArr[index].m_Balancelocalcreditamount.display = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                    creditDataArr[index].m_dCredit_Money_B.value = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y.value);
                                                                    creditDataArr[index].m_Balancelocalcreditamount.value = this.handleConversion(creditDataArr[index].m_Balancecreditamount.value);
                                                                }
                                                            }
                                                        }
                                                        this.setState(
                                                            {
                                                                creditDataArr
                                                            },
                                                            () => {
                                                                this.handleChecked("credit", index);
                                                                this.handleSumAmount("credit");
                                                                if (CompareStatus == "uncompare") {
                                                                    //对照出另一方的数据
                                                                    compareOtherData(this, "credit");
                                                                }
                                                            }
                                                        );
                                                        // if(v>creditDataArr[index].empty_m_Balancecreditamount){ return false;}
                                                        // creditDataArr[index].m_dCredit_Money_Y = v?v*debitRate:0;
                                                        // creditDataArr[index].m_dCredit_Money_B = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y);
                                                        // creditDataArr[index].m_Balancecreditamount = creditDataArr[index].empty_m_Balancecreditamount - creditDataArr[index].m_dCredit_Money_Y;
                                                        // creditDataArr[index].m_Balancelocalcreditamount = this.handleConversion(creditDataArr[index].m_Balancecreditamount);
                                                        // this.setState({
                                                        // 	creditDataArr
                                                        // })
                                                    }}
                                                />
                                            </div>
                                        );
                                    } else {
                                        // 复选框未被选中状态
                                        return (
                                            <div fieldid="m_dCredit_Money_Y">
                                            <NCNumber
                                                fieldid="m_dCredit_Money_Y"
                                                scale={Number(text.scale)}
                                                disabled
                                                value={text==null?null:text.display}
                                                onBlur={v => {
                                                    // let {creditDataArr,debitRate }=this.state;
                                                    // if(v>creditDataArr[index].empty_m_Balancecreditamount){ return false;}
                                                    // creditDataArr[index].m_dCredit_Money_Y = v?v*debitRate:0;
                                                    // creditDataArr[index].m_dCredit_Money_B = this.handleConversion(creditDataArr[index].m_dCredit_Money_Y);
                                                    // creditDataArr[index].m_Balancecreditamount = creditDataArr[index].empty_m_Balancecreditamount - creditDataArr[index].m_dCredit_Money_Y;
                                                    // creditDataArr[index].m_Balancelocalcreditamount = this.handleConversion(creditDataArr[index].m_Balancecreditamount);
                                                    // this.setState({
                                                    // 	creditDataArr
                                                    // })
                                                }}
                                            />
                                            </div>
                                        );
                                    }
                                }
                            },
                            {
                                title: (<div fieldid="m_dCredit_Money_B" className="mergecells">{json['20020VRIFYPAGE-000025']}</div>),/* 国际化处理： 组织本币*/
                                dataIndex: "m_dCredit_Money_B",
                                key: "m_dCredit_Money_B",
                                width: 150,
                                render: (text, record, index) => {
                                    let {
                                        checkedArrayCredit,
                                        debitRate,
                                        queryCondition,
                                        CompareStatus, creditOrDebitFlag, paraInfo
                                    } = this.state;
                                    if (checkedArrayCredit[index]) {
                                        if ((queryCondition.pk_currency && queryCondition.pk_currency == queryCondition.account_currency) ||
                                            (this.props.voucherVerifyflag == '0' && creditOrDebitFlag == 'credit') ||
                                            (this.props.voucherVerifyflag == '1' && record.m_pk_vouchertype.value == paraInfo.orgCurrinfo.value)) {
                                            //币种相同是不可以编辑的
                                            return (
                                                <div fieldid="m_dCredit_Money_B">
                                                <NCNumber
                                                    fieldid="m_dCredit_Money_B"
                                                    scale={Number(text.scale)}
                                                    disabled
                                                    value={text==null?null:text.display}
                                                    onChange={v => { }}
                                                />
                                                </div>
                                            );
                                        } else {
                                            return (
                                                <div fieldid="m_dCredit_Money_B">
                                                <NCNumber
                                                    fieldid="m_dCredit_Money_B"
                                                    value={text==null?null:text.display}
                                                    scale={Number(text.scale)}
                                                    onBlur={v => {
                                                        let { creditDataArr, CompareStatus } = this.state;
                                                        if (creditDataArr[index].empty_m_Balancelocalcreditamount.value -0 >0) {//未核销数是正数
                                                            if (v - 0 < 0) {//输入负数
                                                                //creditDataArr[index].m_dCredit_Money_Y=0;
                                                                creditDataArr[index].m_dCredit_Money_B.display = creditDataArr[index].m_dCredit_Money_B.prevValue;//0;
                                                                creditDataArr[index].m_Balancelocalcreditamount.display = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);
                                                                creditDataArr[index].m_dCredit_Money_B.value = creditDataArr[index].m_dCredit_Money_B.prevValue;//0;
                                                                creditDataArr[index].m_Balancelocalcreditamount.value = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);//creditDataArr[index].empty_m_Balancelocalcreditamount.value;
                                                            } else {
                                                                if (Math.abs(v - 0) >Math.abs(creditDataArr[index].empty_m_Balancelocalcreditamount.value - 0)) {
                                                                    creditDataArr[index].m_dCredit_Money_B.display = creditDataArr[index].m_dCredit_Money_B.prevValue;//0;
                                                                    creditDataArr[index].m_dCredit_Money_B.value = creditDataArr[index].m_dCredit_Money_B.prevValue;//0;
                                                                    creditDataArr[index].m_Balancelocalcreditamount.display = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);//creditDataArr[index].empty_m_Balancelocalcreditamount;
                                                                    creditDataArr[index].m_Balancelocalcreditamount.vlaue = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);//creditDataArr[index].empty_m_Balancelocalcreditamount;
                                                                } else {
                                                                    creditDataArr[index].m_dCredit_Money_B.display = v;
                                                                    creditDataArr[index].m_dCredit_Money_B.value = v;
                                                                    creditDataArr[index].m_dCredit_Money_B.prevValue = v;
                                                                    creditDataArr[index].m_Balancelocalcreditamount.display = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);
                                                                    creditDataArr[index].m_Balancelocalcreditamount.value = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);
                                                                }
                                                            }
                                                        } else {//未核销数为负数
                                                            if (v - 0 < 0) {
                                                                if (Math.abs(v - 0) > Math.abs(creditDataArr[index].empty_m_Balancelocalcreditamount.value - 0)) {
                                                                    creditDataArr[index].m_dCredit_Money_B.display = creditDataArr[index].m_dCredit_Money_B.prevValue;//0;
                                                                    creditDataArr[index].m_Balancelocalcreditamount.display = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);
                                                                    creditDataArr[index].m_dCredit_Money_B.value = creditDataArr[index].m_dCredit_Money_B.prevValue;//0;
                                                                    creditDataArr[index].m_Balancelocalcreditamount.value = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);
                                                                } else {
                                                                    creditDataArr[index].m_dCredit_Money_B.display = v;
                                                                    creditDataArr[index].m_dCredit_Money_B.value = v;
                                                                    creditDataArr[index].m_dCredit_Money_B.prevValue = v;
                                                                    creditDataArr[index].m_Balancelocalcreditamount.display = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);
                                                                    creditDataArr[index].m_Balancelocalcreditamount.value = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);
                                                                }
                                                            } else {
                                                                creditDataArr[index].m_dCredit_Money_B.display = creditDataArr[index].m_dCredit_Money_B.prevValue;//0;
                                                                creditDataArr[index].m_Balancelocalcreditamount.display = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);
                                                                creditDataArr[index].m_dCredit_Money_B.value = creditDataArr[index].m_dCredit_Money_B.prevValue;//0;
                                                                creditDataArr[index].m_Balancelocalcreditamount.value = Subtr(creditDataArr[index].empty_m_Balancelocalcreditamount.value,creditDataArr[index].m_dCredit_Money_B.value);
                                                            }
                                                        }
                                                        this.setState(
                                                            {
                                                                creditDataArr
                                                            },
                                                            () => {
                                                                this.handleChecked("credit", index);
                                                                this.handleSumAmount("credit");
                                                                if (CompareStatus == "uncompare") {
                                                                    //对照出另一方的数据
                                                                    compareOtherData(this, "credit");
                                                                }
                                                            }
                                                        );
                                                    }}
                                                />
                                                </div>
                                            );
                                        }
                                    } else {
                                        return (
                                            <div fieldid="m_dCredit_Money_B">
                                            <NCNumber
                                                fieldid="m_dCredit_Money_B"
                                                scale={Number(text.scale)}
                                                disabled
                                                value={text==null?null:text.display}
                                                onChange={v => { }}
                                            />
                                            </div>
                                        );
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: json['20020VRIFYPAGE-000057'],/* 国际化处理： 未核销余额*/
                        children: [
                            {
                                title: (<div fieldid="m_Balancecreditamount" className="mergecells">{json['20020VRIFYPAGE-000024']}</div>),/* 国际化处理： 原币*/
                                className: "yeson",
                                dataIndex: "m_Balancecreditamount",
                                key: "m_Balancecreditamount",
                                width: 150,
                                render: (text, record, index) => {
                                    return (
                                        <div fieldid="m_Balancecreditamount">
                                        <NCNumber
                                            fieldid="m_Balancecreditamount"
                                            scale={Number(text.scale)}
                                            disabled
                                            value={text==null?null:text.display}
                                            onChange={v => { }}
                                        />
                                        </div>
                                    );
                                }
                            },
                            {
                                title: (<div fieldid="m_Balancelocalcreditamount" className="mergecells">{json['20020VRIFYPAGE-000025']}</div>),/* 国际化处理： 组织本币*/
                                className: "yeson",
                                dataIndex: "m_Balancelocalcreditamount",
                                key: "m_Balancelocalcreditamount",
                                width: 150,
                                render: (text, record, index) => {
                                    return (
                                        <div ieldid="m_Balancelocalcreditamount">
                                        <NCNumber
                                            fieldid="m_Balancelocalcreditamount"
                                            scale={Number(text.scale)}
                                            disabled
                                            value={text==null?null:text.display}
                                            onChange={v => { }}
                                        />
                                        </div>
                                    );
                                }
                            }
                        ]
                    }
                ]);
            }else if(type=='debit'){
                // this.columns_debit = [
                return([
                    //借方
                    {
                        title: (<div fieldid="m_pk_unit_v" className="mergecells">{json['20020VRIFYPAGE-000017']}</div>),/* 国际化处理： 业务单元*/
                        dataIndex: "m_pk_unit_v", //m_voAss
                        key: "m_pk_unit_v",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_pk_unit_v">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_sSubjCode" className="mergecells">{json['20020VRIFYPAGE-000051']}</div>),/* 国际化处理： 科目编码*/
                        dataIndex: "m_sSubjCode",
                        key: "m_sSubjCode",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_sSubjCode">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_sSubjName" className="mergecells">{json['20020VRIFYPAGE-000052']}</div>),/* 国际化处理： 科目*/
                        dataIndex: "m_sSubjName",
                        key: "m_sSubjName",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_sSubjName">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_prepareddate" className="mergecells">{json['20020VRIFYPAGE-000021']}</div>),/* 国际化处理： 凭证日期*/
                        dataIndex: "m_prepareddate",
                        key: "m_prepareddate",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_prepareddate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_Businessdate" className="mergecells">{json['20020VRIFYPAGE-000022']}</div>),/* 国际化处理： 业务日期*/
                        dataIndex: "m_Businessdate",
                        key: "m_Businessdate",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_Businessdate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_VerifyNo" className="mergecells">{json['20020VRIFYPAGE-000035']}</div>),/* 国际化处理： 核销号*/
                        dataIndex: "m_VerifyNo",
                        key: "m_VerifyNo",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_VerifyNo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_netbankflag" className="mergecells">{json['20020VRIFYPAGE-000075']}</div>),/* 国际化处理： 银行对账标识码*/
                        dataIndex: "m_netbankflag",
                        key: "m_netbankflag",
                        width: 150,
                        className: "m_netbankflag",
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_netbankflag">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_VoucherNo" className="mergecells">{json['20020VRIFYPAGE-000053']}</div>),/* 国际化处理： 凭证号*/
                        dataIndex: "m_VoucherNo",
                        key: "m_VoucherNo",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_VoucherNo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_detailindex" className="mergecells">{json['20020VRIFYPAGE-000054']}</div>),/* 国际化处理： 分录号*/
                        dataIndex: "m_detailindex",
                        key: "m_detailindex",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_detailindex">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="assinfo" className="mergecells">{json['20020VRIFYPAGE-000003']}</div>),/* 国际化处理： 辅助核算*/
                        dataIndex: "assinfo", //m_voAss
                        key: "assinfo",
                        width: 200,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="assinfo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: (<div fieldid="m_explanation" className="mergecells">{json['20020VRIFYPAGE-000029']}</div>),/* 国际化处理： 摘要*/
                        dataIndex: "m_explanation",
                        key: "m_explanation",
                        width: 100,
                        render: (text, record, index) => {
                            return (
                                <div fieldid="m_explanation">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                            );
                        }
                    },
                    {
                        title: json['20020VRIFYPAGE-000055'],/* 国际化处理： 金额*/
                        children: [
                            {
                                title: (<div fieldid="m_debitamount" className="mergecells">{json['20020VRIFYPAGE-000024']}</div>),/* 国际化处理： 原币*/
                                className: "yeson",
                                dataIndex: "m_debitamount",
                                key: "m_debitamount",
                                width: 150,
                                render: (text, record, index) => {
                                    return (
                                        <div className="right" fieldid="m_debitamount">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                                    );
                                }
                            },
                            {
                                title: (<div fieldid="m_localdebitamount" className="mergecells">{json['20020VRIFYPAGE-000025']}</div>),/* 国际化处理： 组织本币*/
                                className: "yeson",
                                dataIndex: "m_localdebitamount",
                                key: "m_localdebitamount",
                                width: 150,
                                render: (text, record, index) => {
                                    return (
                                        <div className="right" fieldid="m_localdebitamount">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
                                    );
                                }
                            }
                        ]
                    },
                    {
                        title: json['20020VRIFYPAGE-000056'],/* 国际化处理： 本次核销*/
                        children: [
                            {
                                title: (<div fieldid="m_dDebit_Money_Y" className="mergecells">{json['20020VRIFYPAGE-000024']}</div>),/* 国际化处理： 原币*/
                                dataIndex: "m_dDebit_Money_Y",
                                key: "m_dDebit_Money_Y",
                                width: 150,
                                render: (text, record, index) => {
                                    let { checkedArrayDebit, debitRate, queryCondition, creditOrDebitFlag } = this.state;
                                    if (checkedArrayDebit[index] && creditOrDebitFlag != 'debit') {
                                        // 复选框被选中状态时return出可编辑的输入框
                                        return (
                                            <div fieldid="m_dDebit_Money_Y">
                                            <NCNumber
                                                fieldid="m_dDebit_Money_Y"
                                                value={text==null?null:text.display}
                                                scale={Number(text.scale)}
                                                title={json['20020VRIFYPAGE-000076']}/* 国际化处理： 输入的原币数值不可大于组织本币*/
                                                onBlur={v => {
                                                    // onBlur={(v) => {
                                                    let { debitDataArr, CompareStatus } = this.state;
                                                    if (debitDataArr[index].empty_m_Balancedebitamount.value - 0 > 0) {//未核销金额为正数
                                                        if (v - 0 < 0) {
                                                            debitDataArr[index].m_dDebit_Money_Y.display = debitDataArr[index].m_dDebit_Money_Y.prevValue;// debitDataArr[index].empty_m_Balancedebitamount.value;
                                                            debitDataArr[index].m_dDebit_Money_Y.value = debitDataArr[index].m_dDebit_Money_Y.prevValue;// debitDataArr[index].empty_m_Balancedebitamount.value;
                                                            debitDataArr[index].m_Balancedebitamount.display = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;// 0;
                                                            debitDataArr[index].m_Balancedebitamount.value = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;// 0;
                                                            // debitDataArr[index].m_dDebit_Money_Y.display = 0;
                                                            // debitDataArr[index].m_dDebit_Money_Y.value = 0;
                                                            // debitDataArr[index].m_Balancedebitamount.display = debitDataArr[index].empty_m_Balancedebitamount.value;
                                                            // debitDataArr[index].m_Balancedebitamount.value = debitDataArr[index].empty_m_Balancedebitamount.value;
                                                            if (queryCondition.pk_currency == queryCondition.account_currency) {
                                                                //币种相同组织本币不可以编辑的，跟着原币变化
                                                                debitDataArr[index].m_dDebit_Money_B.display = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                debitDataArr[index].m_dDebit_Money_B.value = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                debitDataArr[index].m_Balancelocaldebitamount.display = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                                debitDataArr[index].m_Balancelocaldebitamount.value = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                            }
                                                        } else {
                                                            if (Math.abs(v - 0) > Math.abs(debitDataArr[index].empty_m_Balancedebitamount.value - 0)) {
                                                                debitDataArr[index].m_dDebit_Money_Y.display = debitDataArr[index].m_dDebit_Money_Y.prevValue;// debitDataArr[index].empty_m_Balancedebitamount.value;
                                                                debitDataArr[index].m_dDebit_Money_Y.value = debitDataArr[index].m_dDebit_Money_Y.prevValue;// debitDataArr[index].empty_m_Balancedebitamount.value;
                                                                debitDataArr[index].m_Balancedebitamount.display = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;// 0;
                                                                debitDataArr[index].m_Balancedebitamount.value = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;// 0;
                                                                if (queryCondition.pk_currency == queryCondition.account_currency) {
                                                                    //币种相同组织本币不可以编辑的，跟着原币变化
                                                                    debitDataArr[index].m_dDebit_Money_B.display = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                    debitDataArr[index].m_dDebit_Money_B.value = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                    debitDataArr[index].m_Balancelocaldebitamount.display = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                                    debitDataArr[index].m_Balancelocaldebitamount.value = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                                }
                                                            } else {
                                                                debitDataArr[index].m_dDebit_Money_Y.display = (v ? v : 0);
                                                                debitDataArr[index].m_dDebit_Money_Y.value = (v ? v : 0);
                                                                debitDataArr[index].m_dDebit_Money_Y.prevValue = (v ? v : 0);//prevValue存储当前输入的值
                                                                debitDataArr[index].m_Balancedebitamount.display = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;
                                                                debitDataArr[index].m_Balancedebitamount.value = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;
                                                                if (queryCondition.pk_currency == queryCondition.account_currency) {
                                                                    //币种相同组织本币不可以编辑的，跟着原币变化
                                                                    debitDataArr[index].m_dDebit_Money_B.display = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                    debitDataArr[index].m_dDebit_Money_B.value = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                    debitDataArr[index].m_Balancelocaldebitamount.display = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                                    debitDataArr[index].m_Balancelocaldebitamount.value = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                                }
                                                            }
                                                        }
                                                    } else {//未核销金额为负数
                                                        if (v - 0 < 0) {//输入负数
                                                            if (Math.abs(v - 0) >Math.abs(debitDataArr[index].empty_m_Balancedebitamount.value - 0)) {
                                                                debitDataArr[index].m_dDebit_Money_Y.display = debitDataArr[index].m_dDebit_Money_Y.prevValue;//debitDataArr[index].empty_m_Balancedebitamount.value;
                                                                debitDataArr[index].m_dDebit_Money_Y.value = debitDataArr[index].m_dDebit_Money_Y.prevValue;//debitDataArr[index].empty_m_Balancedebitamount.value;
                                                                debitDataArr[index].m_Balancedebitamount.display = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;//0 ;
                                                                debitDataArr[index].m_Balancedebitamount.value = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;//0;
                                                                if (queryCondition.pk_currency == queryCondition.account_currency) {
                                                                    //币种相同组织本币不可以编辑的，跟着原币变化
                                                                    debitDataArr[index].m_dDebit_Money_B.display = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                    debitDataArr[index].m_Balancelocaldebitamount.display = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                                    debitDataArr[index].m_dDebit_Money_B.value = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                    debitDataArr[index].m_Balancelocaldebitamount.value = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                                }
                                                            } else {
                                                                debitDataArr[index].m_dDebit_Money_Y.display = (v ? v : 0);
                                                                debitDataArr[index].m_dDebit_Money_Y.value = (v ? v : 0);
                                                                debitDataArr[index].m_dDebit_Money_Y.prevValue = (v ? v : 0);
                                                                debitDataArr[index].m_Balancedebitamount.display = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;
                                                                debitDataArr[index].m_Balancedebitamount.value = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;
                                                                if (queryCondition.pk_currency == queryCondition.account_currency) {
                                                                    //币种相同组织本币不可以编辑的，跟着原币变化
                                                                    debitDataArr[index].m_dDebit_Money_B.display = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                    debitDataArr[index].m_Balancelocaldebitamount.display = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                                    debitDataArr[index].m_dDebit_Money_B.value = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                    debitDataArr[index].m_Balancelocaldebitamount.value = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                                }
                                                            }
                                                        } else {//输入正数
                                                            debitDataArr[index].m_dDebit_Money_Y.display = debitDataArr[index].m_dDebit_Money_Y.prevValue;
                                                            debitDataArr[index].m_Balancedebitamount.display = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;// 0;
                                                            debitDataArr[index].m_dDebit_Money_Y.value = debitDataArr[index].m_dDebit_Money_Y.prevValue;//debitDataArr[index].empty_m_Balancedebitamount.value;
                                                            debitDataArr[index].m_Balancedebitamount.value = debitDataArr[index].empty_m_Balancedebitamount.value - debitDataArr[index].m_dDebit_Money_Y.value;// 0;
                                                            if (queryCondition.pk_currency == queryCondition.account_currency) {
                                                                //币种相同组织本币不可以编辑的，跟着原币变化
                                                                debitDataArr[index].m_dDebit_Money_B.display = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                debitDataArr[index].m_Balancelocaldebitamount.display = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                                debitDataArr[index].m_dDebit_Money_B.value = this.handleConversion(debitDataArr[index].m_dDebit_Money_Y.value);
                                                                debitDataArr[index].m_Balancelocaldebitamount.value = this.handleConversion(debitDataArr[index].m_Balancedebitamount.value);
                                                            }
                                                        }
                                                    }
                                                    this.setState({ debitDataArr }, () => {
                                                        this.handleChecked("debit", index);
                                                        this.handleSumAmount("debit");
                                                        if (CompareStatus == "uncompare") {
                                                            //对照出另一方的数据
                                                            compareOtherData(this, "debit");
                                                        }
                                                    }
                                                    );
                                                }}
                                            />
                                            </div>
                                        );
                                    } else {
                                        // 复选框未被选中状态
                                        return (
                                            <div fieldid="m_dDebit_Money_Y">
                                                <NCNumber  fieldid="m_dDebit_Money_Y" scale={Number(text.scale)} disabled value={text==null?null:text.display} onBlur={v => { }} />
                                            </div>
                                        );
                                    }
                                }
                            },
                            {
                                title: (<div fieldid="m_dDebit_Money_B" className="mergecells">{json['20020VRIFYPAGE-000025']}</div>),/* 国际化处理： 组织本币*/
                                dataIndex: "m_dDebit_Money_B",
                                key: "m_dDebit_Money_B",
                                width: 150,
                                render: (text, record, index) => {
                                    let {
                                        checkedArrayDebit,
                                        debitRate,
                                        queryCondition,
                                        CompareStatus, creditOrDebitFlag, paraInfo
                                    } = this.state;
                                    if (checkedArrayDebit[index]) {
                                        if ((queryCondition.pk_currency && queryCondition.pk_currency == queryCondition.account_currency) ||
                                            (this.props.voucherVerifyflag == '0' && creditOrDebitFlag == 'debit') ||
                                            (this.props.voucherVerifyflag == '1' && record.m_pk_vouchertype.value == paraInfo.orgCurrinfo.value)) {
                                            //币种相同是不可以编辑的
                                            return (
                                                <div fieldid="m_dDebit_Money_B">
                                                <NCNumber
                                                    fieldid="m_dDebit_Money_B"
                                                    scale={Number(text.scale)}
                                                    disabled
                                                    value={text==null?null:text.display}
                                                    onChange={v => { }}
                                                />
                                                </div>
                                            );
                                        } else {
                                            return (
                                                <div fieldid="m_dDebit_Money_B">
                                                <NCNumber
                                                    fieldid="m_dDebit_Money_B"
                                                    value={text==null?null:text.display}
                                                    scale={Number(text.scale)}
                                                    title={json['20020VRIFYPAGE-000076']}/* 国际化处理： 输入的原币数值不可大于组织本币*/
                                                    onBlur={v => {
                                                        // onBlur={(v) => {
                                                        let { debitDataArr, CompareStatus } = this.state;
                                                        if (debitDataArr[index].empty_m_Balancelocaldebitamount.value - 0 > 0) {//未核销数正数
                                                            if (v - 0 < 0) {//输入负数
                                                                debitDataArr[index].m_dDebit_Money_B.display = debitDataArr[index].m_dDebit_Money_B.prevValue;// 0;
                                                                debitDataArr[index].m_Balancelocaldebitamount.display = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);//debitDataArr[index].empty_m_Balancelocaldebitamount.value;
                                                                debitDataArr[index].m_dDebit_Money_B.value = debitDataArr[index].m_dDebit_Money_B.prevValue;// 0;
                                                                debitDataArr[index].m_Balancelocaldebitamount.value = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);//debitDataArr[index].empty_m_Balancelocaldebitamount.value;
                                                            } else {
                                                                if (Math.abs(v - 0) > Math.abs(debitDataArr[index].empty_m_Balancelocaldebitamount.value - 0)) {
                                                                    debitDataArr[index].m_dDebit_Money_B.display = debitDataArr[index].m_dDebit_Money_B.prevValue;// 0;
                                                                    debitDataArr[index].m_Balancelocaldebitamount.display = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);// debitDataArr[index].empty_m_Balancelocaldebitamount.value;
                                                                    debitDataArr[index].m_dDebit_Money_B.value = debitDataArr[index].m_dDebit_Money_B.prevValue;//0;
                                                                    debitDataArr[index].m_Balancelocaldebitamount.value = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);// debitDataArr[index].empty_m_Balancelocaldebitamount.value;
                                                                } else {
                                                                    debitDataArr[index].m_dDebit_Money_B.display = v;
                                                                    debitDataArr[index].m_dDebit_Money_B.value = v;
                                                                    debitDataArr[index].m_dDebit_Money_B.prevValue = v;
                                                                    debitDataArr[index].m_Balancelocaldebitamount.display = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);
                                                                    debitDataArr[index].m_Balancelocaldebitamount.value = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);
                                                                }
                                                            }
                                                        } else {//未核销数为负数
                                                            if (v - 0 < 0) {
                                                                if (Math.abs(v - 0) > Math.abs(debitDataArr[index].empty_m_Balancelocaldebitamount.value - 0)) {
                                                                    debitDataArr[index].m_dDebit_Money_B.display = debitDataArr[index].m_dDebit_Money_B.prevValue;// 0; 
                                                                    debitDataArr[index].m_Balancelocaldebitamount.display = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);// debitDataArr[index].empty_m_Balancelocaldebitamount.value;
                                                                    debitDataArr[index].m_dDebit_Money_B.value =debitDataArr[index].m_dDebit_Money_B.prevValue;// 0; 
                                                                    debitDataArr[index].m_Balancelocaldebitamount.value = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);// debitDataArr[index].empty_m_Balancelocaldebitamount.value;
                                                                } else {
                                                                    debitDataArr[index].m_dDebit_Money_B.display = v;
                                                                    debitDataArr[index].m_dDebit_Money_B.value = v;
                                                                    debitDataArr[index].m_dDebit_Money_B.prevValue = v;
                                                                    debitDataArr[index].m_Balancelocaldebitamount.display = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);
                                                                    debitDataArr[index].m_Balancelocaldebitamount.value = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);
                                                                }
                                                            } else {
                                                                debitDataArr[index].m_dDebit_Money_B.display = debitDataArr[index].m_dDebit_Money_B.prevValue// 0;
                                                                debitDataArr[index].m_dDebit_Money_B.value = debitDataArr[index].m_dDebit_Money_B.prevValue// 0;
                                                                debitDataArr[index].m_Balancelocaldebitamount.display = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);// debitDataArr[index].empty_m_Balancelocaldebitamount.value;
                                                                debitDataArr[index].m_Balancelocaldebitamount.value = Subtr(debitDataArr[index].empty_m_Balancelocaldebitamount.value, debitDataArr[index].m_dDebit_Money_B.value);// debitDataArr[index].empty_m_Balancelocaldebitamount.value;
                                                            }
                                                        }
                                                        this.setState({ debitDataArr },
                                                            () => {
                                                                this.handleChecked("debit", index);
                                                                this.handleSumAmount("debit");
                                                                if (CompareStatus == "uncompare") {
                                                                    //对照出另一方的数据
                                                                    compareOtherData(this, "debit");
                                                                }
                                                            }
                                                        );
                                                    }}
                                                />
                                                </div>
                                            );
                                        }
                                    } else {
                                        return (
                                            <div fieldid="m_dDebit_Money_B">
                                            <NCNumber
                                                fieldid="m_dDebit_Money_B"
                                                scale={Number(text.scale)}
                                                disabled
                                                value={text==null?null:text.display}
                                                onChange={v => { }}
                                            />
                                            </div>
                                        );
                                    }
                                }
                            }
                        ]
                    },
                    {
                        title: json['20020VRIFYPAGE-000057'],/* 国际化处理： 未核销余额*/
                        children: [
                            {
                                title: (<div fieldid="m_Balancedebitamount" className="mergecells">{json['20020VRIFYPAGE-000024']}</div>),/* 国际化处理： 原币*/
                                className: "yeson",
                                dataIndex: "m_Balancedebitamount",
                                key: "m_Balancedebitamount",
                                width: 150,
                                render: (text, record, index) => {
                                    return (
                                        <div fieldid="m_Balancedebitamount">
                                        <NCNumber
                                            fieldid="m_Balancedebitamount"
                                            scale={Number(text.scale)}
                                            disabled
                                            value={text==null?null:text.display}
                                            onChange={v => { }}
                                        />
                                        </div>
                                    );
                                }
                            },
                            {
                                title: (<div fieldid="m_Balancelocaldebitamount" className="mergecells">{json['20020VRIFYPAGE-000025']}</div>),/* 国际化处理： 组织本币*/
                                className: "yeson",
                                dataIndex: "m_Balancelocaldebitamount",
                                key: "m_Balancelocaldebitamount",
                                width: 150,
                                render: (text, record, index) => {
                                    return (
                                        <div fieldid="m_Balancelocaldebitamount">
                                        <NCNumber
                                            fieldid="m_Balancelocaldebitamount"
                                            scale={Number(text.scale)}
                                            disabled
                                            value={text==null?null:text.display}
                                            onChange={v => { }}
                                        />
                                        </div>
                                    );
                                }
                            }
                        ]
                    }
                ]
            );
        }
    }
    render() {
        let that = this;
        let {
            layout,
            queryCondition,
            creditDataArr,
            debitDataArr,
            verifyStandardshowModal,
            hedgingStandardshowModal,
            pk_accasoa,
            pk_accountingbook,
            modalDefaultValue,json
        } = that.state;

        // let $$columns_credit=deepClone(that.columns_credit);
        // let $$columns_debit=deepClone(that.columns_debit);
        // let columns_credit=that.getCreditOrDebitCoumn('credit',json);
        // let columns_debit=that.getCreditOrDebitCoumn('debit',json);
        let columns_creditArr =deepClone(that.columns_credit);// $$columns_credit;
        let columns_debitArr = deepClone(that.columns_debit);//$$columns_debit;
        if (queryCondition.cashtype && queryCondition.cashtype != '2') {//银行类科目 核销的表头增加一列银行对账标志码
            columns_creditArr.map((list, index) => {
                if (list.key == 'm_netbankflag') {
                    columns_creditArr.splice(index, 1);
                }
            })
            columns_debitArr.map((list, index) => {
                if (list.key == 'm_netbankflag') {
                    columns_debitArr.splice(index, 1);
                }
            })
        }
        if (!queryCondition.isShowUnit) {//是否显示业务单元
            if(columns_creditArr){
                columns_creditArr.map((list, index) => {
                    if (list.key == 'm_pk_unit_v') {
                        columns_creditArr.splice(index, 1);
                    }
                })
            }
            if(columns_debitArr){
                columns_debitArr.map((list, index) => {
                    if (list.key == 'm_pk_unit_v') {
                        columns_debitArr.splice(index, 1);
                    }
                })
            }
        }
        let columnsCredit=[];
        if(columns_creditArr){
            columnsCredit = that.renderColumnsMultiSelect(columns_creditArr);
        }
        let columnsDebit=[];
        if(columns_debitArr){
            columnsDebit = that.renderColumnsMultiSelectDebit(columns_debitArr);
        }
        
        return (
            <div className={that.state.position ? "content" : "content vertical"} id="query_body">
                <HeaderArea 
                    verifyflag={that.props.voucherVerifyflag}
                    title = {(!that.props.voucherVerifyflag) ? that.state.json['20020VRIFYPAGE-000081']: ''}/* 国际化处理： 往来核销处理*/
                    searchContent = {
                        <ul className="account nc-theme-form-label-c">
                            <li className="account-content1-tit" fieldid="pk_accountingbook">
                                <lable>{that.state.json['20020VRIFYPAGE-000016']}：</lable>{/* 国际化处理： 核算账簿*/}
                                <span className="account_title" title={pk_accountingbook.display}><p className="account_book">{pk_accountingbook.display}</p></span>
                            </li>
                            <li className="account-content1-titsub" fieldid="pk_accasoa">
                                <lable className="account_obj">{that.state.json['20020VRIFYPAGE-000070']}：</lable>{/* 国际化处理： 核销对象*/}
                                <span className="account_subject" title={pk_accasoa.display}><p className="account_sub">{pk_accasoa.display}</p></span>
                            </li>
                        </ul>
                    }
                    btnContent = {
                        <div>
                            {
                                (!that.props.voucherVerifyflag) ?
                                    that.props.button.createButtonApp({
                                        area: 'header_buttons',
                                        onButtonClick: buttonClick.bind(that),
                                    })
                                    : (<div />)
                            }
                            {(that.props.voucherVerifyflag == '0' ?
                                <div className="buttons">
                                    <ButtonGroup>
                                        <Button fieldid="query" onClick={that.handleClick.bind(that)}>{that.state.json['20020VRIFYPAGE-000061']}</Button>{/* 国际化处理： 查询*/}
                                        <Button fieldid="queryall" onClick={that.handleQueryAll.bind(that)} className="bug">{that.state.json['20020VRIFYPAGE-000082']}</Button>{/* 国际化处理： 全部*/}
                                        <Button fieldid="verify" onClick={handleVerify.bind(that, that)} className="bug">{that.state.json['20020VRIFYPAGE-000039']}</Button>{/* 国际化处理： 核销*/}
                                        <Button fieldid="refresh" onClick={that.handleQueryAll.bind(that)} className="refeach"><i className="iconfont icon-shuaxin1"></i></Button>
                                        {/* <Button onClick={that.handleRefresh.bind(that)} className="refeach"><i className="iconfont icon-shuaxin1"></i></Button> */}
                                    </ButtonGroup>
                                </div>
                                : <div />)

                            }
                        </div>
                        
                    }
                />
                {/*  */}
                <QueryModal
                    modalDefaultValue={modalDefaultValue}
                    voucherVerifyflag={that.props.voucherVerifyflag}
                    loadData={that.loadQuery}
                    showOrHide={that.state.isQueryShow}
                    onConfirm={handleQueryClick.bind(that, that.state)}
                    handleClose={that.handleClick.bind(that)}
                />
                <VerifyStandardModal
                    loadData={that.verifyStandard}
                    flag={'1'}//1 表示自动核销弹框       
                    queryCondition={queryCondition}
                    showOrHide={verifyStandardshowModal}
                    onConfirm={handleAutoVerify.bind(that, that.state)}
                    handleClose={that.handleAutoVerifyClick.bind(that)}
                />
                <VerifyStandardModal
                    loadData={that.hedgingCondition}
                    flag={'2'} //2表示自动红蓝对冲
                    queryCondition={queryCondition}
                    showOrHide={hedgingStandardshowModal}
                    onConfirm={handleAutoHedging.bind(that, that.state)}
                    handleClose={that.handleAutoHedgingClick.bind(that)}
                />
                <div className="account-content1" ref="accountContent">
                    <Row>
                        <Col md={layout} sm={layout} xs={layout}>
                            <div className="account-content1-credit" id="credit" fieldid="debit_area">
                                <div className="account-content1-credit-tit nc-theme-area-bgc nc-theme-form-label-c">
                                    <p className="account-content1-credit-tit-title" id="credit_title">{that.state.json['20020VRIFYPAGE-000032']}</p>{/* 国际化处理： 借方*/}
                                    {that.state.position &&
                                        <div className="account-content1-credit-foot">
                                            {that.exportInput('debit')}
                                        </div>
                                    }
                                </div>
                                <NCDiv fieldid="debit" areaCode={NCDiv.config.TableCom}>
                                <Table
                                    columns={columnsDebit}
                                    className="nc-table"
                                    isDrag={true}
                                    bordered
                                    data={debitDataArr}
                                    scroll={{
                                        x: true,
                                        y: that.state.position ? that.getTableHeightPosi() : that.getTableHeight()
                                        // y :1000
                                    }}
                                />
                                </NCDiv>
                            </div>
                        </Col>
                        <Col md={layout} sm={layout} xs={layout}>
                            <div className="account-content1-credit" id="lender" fieldid="credit_area">
                                <div className="account-content1-credit-tit nc-theme-area-bgc nc-theme-form-label-c">
                                    <p className="account-content1-credit-tit-title" id="lender_title">{that.state.json['20020VRIFYPAGE-000033']}</p>{/* 国际化处理： 贷方*/}
                                    {that.state.position &&
                                        <div className="account-content1-credit-foot">
                                            {that.exportInput('credit')}
                                        </div>
                                    }
                                </div>
                                <NCDiv fieldid="credit" areaCode={NCDiv.config.TableCom}>
                                <Table
                                    columns={columnsCredit}
                                    isDrag={true}
                                    bordered
                                    data={creditDataArr}
                                    scroll={{
                                        x: true,
                                        y :that.state.position ? that.getTableHeightPosi() : that.getTableHeight()
                                        // y :1000
                                    }}
                                />
                                </NCDiv>
                            </div>
                        </Col>
                    </Row>
                    {
                        !that.state.position && <div className="bottom-foot nc-theme-gray-area-bgc" >
                            <div className="footerleft">{that.exportInput('debit')}</div>
                            <div className="footerright">{that.exportInput('credit')}</div>
                        </div>
                    }
                </div>
            </div>
        );
    }
}

ExportVrify.defaultProps = defaultProps12;
ExportVrify = createPage({

})(ExportVrify);

export default ExportVrify;
