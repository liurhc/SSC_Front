import { ajax, base, toast,cacheTools,print,withNav } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
let tableid = 'gl_brsetting';

// @withNav
export default function buttonClick(props, id) {
    let {querycondition}=this.state;
    let rowDatas,pk_brsetting,pk_unit,pk_currtype,link;
    switch (id) {
        case 'query':
        //查询
        this.handleQuery()
        break;
        // 刷新
        case 'refresh':
            this.handleRefresh()
            break;
        // 转换
        case 'switch':
        this.handleSwitchTableColumn()
            break;
        // 直接输出
        case 'print':
        this.print()
            break;
            //保存列宽
        case 'saveWidth':
        this.handleSaveColwidth()
            break;
       default:
        break;
    }
}
