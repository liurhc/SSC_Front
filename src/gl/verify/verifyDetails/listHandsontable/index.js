import React, { Component } from "react";
import { high, base, ajax, deepClone, toast, createPage, getMultiLang,gzip,createPageIcon } from "nc-lightapp-front";
import { buttonClick, initTemplate } from './events';
import Immutable from 'immutable';
const {NCForm,NCAffix,NCTooltip:Tooltip,NCRow,NCCol} = base;
const { NCFormItem: FormItem } = NCForm;
import {InputItem} from "../../../public/components/FormItems";
import QueryModal from "./queryModal/querymodal.js";
import HeaderArea from '../../../public/components/HeaderArea';
import { tableDefaultData } from "../../../manageReport/defaultTableData";
import { setData } from '../../../manageReport/common/simbleTableData.js';
import reportPrint from '../../../public/components/reportPrint.js';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import reportSaveWidths from '../../../public/common/reportSaveWidths.js';
import { gl_pkreport_verifyDetails } from '../../../public/common/reportPkConst.js';
import { SimpleTable } from "nc-report";
import './index.less'
class VerifyBalances extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json: {},
      accountType: 'columnInfo',
      flag: 0,//切换表头用的
      showTable: false,
      textAlginArr: [],//对其方式
      flow: false,
      resourceDatas: [], // 原始数据
      dataout: tableDefaultData,
      queryShow: true, //查询按钮默认显示
      listItem: {
        accountingbookValue: { display: "", value: "" },
        verObj: { display: "", value: "" },
        currency: { display: "", value: "" },
        endDate: { display: "", value: "" }
      },
      showModal: false, //查询条件对话框
      columnsVerify: [],
      queryCondition: [], //查询条件
      verifyBalancesData: [],
      conditionData: [],
      verBalanceVOData: {},
      modalDefaultValue: {
        pk_org: '',//账簿对应的组织
        bizDate: '',//业务日期
        yearbegindate: '',//年初日期
        begindate: '',
        enddate: '',
        bizPeriod: '',//账簿对应的会计期间
        isShowUnit: false,//是否显示业务单元
        isOpenUpBusiUnit: '',//
        pk_accperiodscheme: '',//会计期间方案
        pk_accountingbook: { display: '', value: '' },
        pk_currency: { display: '', value: '' },
        appcode: this.props.getSearchParam("c")
      },//模态框里默认数据
    };
  }
  //查询
  handleQuery = () => {
    this.setState({
      showModal: !this.state.showModal
    }, () => {
      let icon = document.getElementsByClassName("iconfont")
      icon[1].style.position = "absolute"
      icon[1].style.top = "-6px"
    });
  };
  //查询确定按钮
  handleQueryClick = (data) => {
    let self = this;
    let { listItem, columnsVerify, verifyBalancesData, queryCondition, resourceDatas, resourveQuery } = this.state;
    let url = "/nccloud/gl/verify/verDetailQuery.do";
    resourveQuery = data;
    let $data = Immutable.fromJS(data);
    let childData = $data.toJS();
    for (let k in childData) {
      if (k == "ass" || k == "acccode" || k == "accname") {
      } else if (k == "pk_currency") {
        childData.curName = childData[k].display ? childData[k].display : null;
        childData[k] = childData[k].value ? childData[k].value : null;
      } else if (k == "pk_accasoa" || k == "pk_units") {
        if (childData[k].length > 0) {
          if (childData[k][0].value == "") {
            childData[k] = null;
          } else {
            childData[k].map((item, index) => {
              //childData[k].push(item.value);
              childData[k][index] = item.value;
            });
          }
        } else {
          childData[k] = null;
        }
      } else {
        childData[k] = childData[k].value ? childData[k].value : null;
      }
    }
    childData.pagecode = gl_pkreport_verifyDetails;
    queryCondition = childData;
    let columnInfo = [], headtitle = [], balanceVO = [];
    ajax({
      url: url,
      data: childData,
      success: function (response) {
        let { data, success } = response;
        if (success) {
          listItem.accountingbookValue.value = data.accountingbookValue;
          listItem.verObj.value = data.verObj;
          listItem.currency.value = data.currency;
          listItem.endDate.value = data.endDate;
          if (data.columnInfo) {
            columnInfo = data.columnInfo
          }
          if (data.headtitle) {
            headtitle = data.headtitle;
          }
          if (data.detailVO) {
            balanceVO = data.detailVO;
          }
          resourceDatas = data;
          self.setState({
            resourveQuery, queryCondition, resourceDatas, listItem
          })
          let renderFirstData = {};
          renderFirstData.columnInfo = data.columnInfo;
          renderFirstData.data = data.detailVO ? data.detailVO : [];
          renderFirstData.columnInfo1 = data.columnInfo1;
          renderFirstData.columnInfo2 = data.columnInfo2;
          setData(self, renderFirstData);
          if (data.detailVO) {
            self.props.button.setButtonDisabled(['print', 'saveWidth', 'refresh', 'switch'], false);
          }
        }
      },
      error: function (error) {
        // toast({ content: error, color: 'warning' });
        self.setState({
          showModal: false
        });
      }
    });
  };
  //处理width
  changeWidth(arr) {
    arr.map((item, index) => {
      if (item.children) {
        this.changeWidth(item.children);
      } else {
        item["width"] = 100;
      }
    });
    return arr;
  }
  getHeadData = listItem => {
    const headData = [
      {
        itemName: this.state.json['20021VYQRY-000015'],/* 国际化处理： 核算账簿:*/
        itemType: "textInput",
        itemKey: "accountingbookValue"
      },
      { itemName: this.state.json['20021VYQRY-000016'], itemType: "textInput", itemKey: "verObj" },/* 国际化处理： 核销对象:*/
      { itemName: this.state.json['20021VYQRY-000017'], itemType: "textInput", itemKey: "currency" },/* 国际化处理： 币种:*/
      { itemName: this.state.json['20021VYQRY-000018'], itemType: "textInput", itemKey: "endDate" }/* 国际化处理： 截止日期:*/
    ];
    return (
      headData.map((item, i) => {
        let defValue = listItem[item.itemKey].value;
      // <NCRow>
      //   {headData.map((item, i) => {
      //     let defValue = listItem[item.itemKey].value;
      //     return(
      //       <NCCol labelXs={3} labelSm={3} labelMd={3}>
      //         <lable>{item.itemName}</lable>
      //         <span><Tooltip trigger="hover" placement="top" inverse overlay={defValue}>{defValue}</Tooltip></span>
      //       </NCCol>
      //     )
      //   })
      //   }
      //   </NCRow>
    // })
  // })
        switch (item.itemType) {
          case "textInput":
            return (
              <FormItem
                fieldid={item.itemKey}
                inline={true}
                //showMast={true}
                labelXs={2}
                labelSm={2}
                labelMd={2}
                xs={2}
                md={2}
                sm={2}
                labelName={item.itemName}
                className={item.itemKey === "accountingbookValue" ? "m-form-item" : ""}
                isRequire={true}
                method="change"
              >
              <Tooltip trigger="hover" placement="top" inverse overlay={defValue}>
                  <span>
                    <InputItem
                      //isViewMode
                      fieldid={item.itemKey}
                      disabled={true}
                      name={item.itemKey}
                      type="customer"
                      defaultValue={defValue}
                    />
                  </span>
                </Tooltip>
              </FormItem>
            );
          default:
            break;
        }
      })
    )
  };

  //获取默认会计期间 会计期间方案
  getDefaultYearmouth = (pk_accountingbook, modalDefaultValue) => {
    let self = this;
    // let {modalDefaultValue}=self.state;
    let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
    let pk_accpont = { "pk_accountingbook": pk_accountingbook }
    ajax({
      url: url,
      data: pk_accpont,
      async: false,
      success: function (response) {
        const { success } = response;
        //渲染已有账表数据遮罩
        if (success) {
          if (response.data) {
            modalDefaultValue.bizPeriod = response.data.bizPeriod;
            modalDefaultValue.isShowUnit = response.data.isShowUnit;
            modalDefaultValue.isOpenUpBusiUnit = 'Y';
            modalDefaultValue.pk_accperiodscheme = response.data.pk_accperiodscheme;
            modalDefaultValue.begindate = response.data.begindate;
            modalDefaultValue.enddate = response.data.enddate;
            modalDefaultValue.bizDate = response.data.bizDate;
            modalDefaultValue.yearbegindate = response.data.yearbegindate;
            modalDefaultValue.pk_currency.display = response.data.currinfo.display;
            modalDefaultValue.pk_currency.value = response.data.currinfo.value;
            modalDefaultValue.pk_org = response.data.unit.value;
          }
          self.setState({
            modalDefaultValue,
            dataout: self.state.dataout
          });
        }
      }
    });
  }
  componentDidMount() {
    let { modalDefaultValue, json } = this.state;
    let appcode = this.props.getSearchParam("c");  
    getDefaultAccountBook(appcode, json).then((defaultAccouontBook) => {
      if (defaultAccouontBook.value) {
        modalDefaultValue.pk_accountingbook = defaultAccouontBook;
        this.getDefaultYearmouth(defaultAccouontBook.value, modalDefaultValue);
      }
    })
  }
  //handlesonTable 单元格点击事件
  handlesonTableClick = (e, coords, td) => {
    // if(td.innerText!=""){
    //   this.props.button.setButtonDisabled(['detail','switch'], false)
    // }else{
    //   this.props.button.setButtonDisabled(['detail','switch'], true)

    // }
  }


  //刷新
  handleRefresh = () => {
    let { resourveQuery } = this.state;
    this.handleQueryClick(resourveQuery);
  };
  //转换
  handleSwitchTableColumn(showType) {
    let self = this;
    let { resourceDatas, flag, accountType } = self.state;
    let data = resourceDatas;
    let ChangecolumnInfo = data.columnInfo;
    let balanceVODatas = data.detailVO;
    if (flag == 0) {
      ChangecolumnInfo = data.columnInfo;
    } else if (flag == 1) {
      ChangecolumnInfo = data.columnInfo1;
    } else if (flag == 2) {
      ChangecolumnInfo = data.columnInfo2;
    }
    if (flag >= 2) {
      flag = 0;
    } else {
      flag++;
    }
    if (flag == 0) {
      accountType = 'columnInfo';
      // ChangecolumnInfo=data.columnInfo;
    } else if (flag == 1) {
      accountType = 'columnInfo1';
      // ChangecolumnInfo=data.columnInfo1;
    } else if (flag == 2) {
      accountType = 'columnInfo2';
      // ChangecolumnInfo=data.columnInfo2;
    }
    let renderFirstData = {};
    renderFirstData.columnInfo = data.columnInfo;
    renderFirstData.data = data.detailVO;
    renderFirstData.columnInfo1 = data.columnInfo1;
    renderFirstData.columnInfo2 = data.columnInfo2;
    self.setState({
      accountType, flag
    }, () => {
      setData(self, renderFirstData);
    })
  }
  //核销情况
  handleVerifyDetails = () => { };
  //直接输出
  print() {
    let { resourceDatas, dataout, textAlginArr } = this.state;
    let dataArr = [];
    let emptyArr = [];
    let mergeInfo = dataout.data.mergeInfo;
    dataout.data.cells.map((item, index) => {
      emptyArr = [];
      item.map((list, _index) => {
        //emptyArr=[];
        if (list) {
          emptyArr.push(list.title);
        }
      })
      dataArr.push(emptyArr);
    })
    reportPrint(mergeInfo, dataArr, textAlginArr);
  }
  //保存列宽
  handleSaveColwidth = () => {
    let info = this.refs.balanceTable.getReportInfo();
    let { json } = this.state
    let callBack = this.refs.balanceTable.resetWidths
    reportSaveWidths(gl_pkreport_verifyDetails, info.colWidths, json, callBack);
  }
  //返回
  handleBack = () => {
    window.history.go(-1);
  };
  GetRequest = urlstr => {
    var url = decodeURIComponent(urlstr); //(window.parent.location.href); //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
      var str = url.substr(url.indexOf("?") + 1);
      var strs = str.split("&");
      for (var i = 0; i < strs.length; i++) {
        theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
      }
    }
    return theRequest;
  };
  componentWillMount() {
    let self = this;
    let callback = (json) => {
    this.Defaultcolumns = [
      //借方
      {
        title: json['20021VYQRY-000019'],/* 国际化处理： 科目编码*/
        dataIndex: "m_sSubjCode",
        key: "m_sSubjCode",
        width: 100
      },
      {
        title: json['20021VYQRY-000020'],/* 国际化处理： 科目名称*/
        dataIndex: "m_sSubjName",
        key: "m_sSubjName",
        width: 100
      },
      {
        title: json['20021VYQRY-000005'],/* 国际化处理： 凭证日期*/
        dataIndex: "m_prepareddate",
        key: "m_prepareddate",
        width: 100
      },
      {
        title: json['20021VYQRY-000006'],/* 国际化处理： 业务日期*/
        dataIndex: "m_Businessdate",
        key: "m_Businessdate",
        width: 100
      },
      {
        title: json['20021VYQRY-000021'],/* 国际化处理： 凭证号*/
        dataIndex: "m_VoucherNo",
        key: "m_VoucherNo",
        width: 100
      },
      {
        title: json['20021VYQRY-000022'],/* 国际化处理： 摘要*/
        dataIndex: "m_explanation",
        key: "m_explanation",
        width: 100
      },
      {
        title: json['20021VYQRY-000011'],/* 国际化处理： 核销号*/
        dataIndex: "m_VerifyNo",
        key: "m_VerifyNo",
        width: 100
      },
      {
        title: json['20021VYQRY-000023'],/* 国际化处理： 原币*/
        dataIndex: "m_debitamount",
        key: "m_debitamount",
        width: 100
      },
      {
        title: json['20021VYQRY-000024'],/* 国际化处理： 本币*/
        dataIndex: "m_localdebitamount",
        key: "m_localdebitamount",
        width: 100
      },
      {
        title: json['20021VYQRY-000025'],/* 国际化处理： 原币余额*/
        dataIndex: "m_Balancedebitamount",
        key: "m_Balancedebitamount",
        width: 100
      },
      {
        title: json['20021VYQRY-000026'],/* 国际化处理： 本币余额*/
        dataIndex: "m_Balancelocaldebitamount",
        key: "m_Balancelocaldebitamount",
        width: 100
      },
      {
        title: json['20021VYQRY-000027'],/* 国际化处理： 账龄*/
        dataIndex: "m_Accountage",
        key: "m_Accountage",
        width: 100
      }
    ];
    this.loadQuery = [
      {
        itemName: json['20021VYQRY-000000'],/* 国际化处理： 核算账簿*/
        itemType: "refer",
        itemKey: "pk_accountingbook", isMustItem: true,
        config: { refCode: "uapbd/refer/org/AccountBookTreeRef" }
      },
      {
        itemName: json['20021VYQRY-000001'],/* 国际化处理： 业务单元*/
        itemType: "refer",
        itemKey: "pk_units", isMustItem: false,
        config: { refCode: "uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef" }
      },
      {
        itemName: json['20021VYQRY-000002'],/* 国际化处理： 核销科目*/
        itemType: "refer",
        itemKey: "pk_accasoa", isMustItem: true,
        config: { refCode: "uapbd/refer/fiacc/AccountDefaultGridTreeRef" }
      },
      {
        itemName: json['20021VYQRY-000003'],/* 国际化处理： 日期范围*/
        itemType: "Dbdate", isMustItem: true,
        itemKey: ["beginDate", "endDate"]
      },
      {
        itemName: json['20021VYQRY-000004'],/* 国际化处理： 分析日期*/
        itemType: "radio",
        itemKey: "dateType",
        itemChild: [
          {
            label: json['20021VYQRY-000005'],/* 国际化处理： 凭证日期*/
            value: "prepareddate"
          },
          {
            label: json['20021VYQRY-000006'],/* 国际化处理： 业务日期*/
            value: "Businessdate"
          }
        ]
      },
      {
        itemName: json['20021VYQRY-000007'],/* 国际化处理： 币种*/
        itemType: "refer",
        itemKey: "pk_currency", isMustItem: true,
        config: { refCode: "uapbd/refer/pubinfo/CurrtypeGridRef" },
        queryGridUrl: "/nccloud/gl/voucher/ref.do",
        refType: "grid"
      },
      {
        itemName: json['20021VYQRY-000008'],/* 国际化处理： 方向*/
        itemType: "radio",
        itemKey: "direct",
        itemChild: [
          {
            label: json['20021VYQRY-000009'],/* 国际化处理： 借*/
            value: "1"
          },
          {
            label: json['20021VYQRY-000010'],/* 国际化处理： 贷*/
            value: "-1"
          }
          // {
          // 	label: '双向',
          // 	value: '0',
          // }
        ]
      },
      //{itemName:'账龄',itemType:'DbtextInput',itemKey:['beginAge','endAge']},
      {
        itemName: json['20021VYQRY-000011'],/* 国际化处理： 核销号*/
        itemType: "DbtextInput",
        itemKey: ["beginVerifyNo", "endVerifyNo"]
      },
      {
        itemName: json['20021VYQRY-000012'],/* 国际化处理： 包含未记账凭证*/
        itemType: "checkbox",
        itemKey: "hasTally",
        itemChild: [
          {
            label: json['20021VYQRY-000012'],/* 国际化处理： 包含未记账凭证*/
            checked: false
          }
        ]
      },
      {
        itemName: json['20021VYQRY-000013'],/* 国际化处理： 包含已两清*/
        itemType: "checkbox",
        itemKey: "equal",
        itemChild: [
          {
            label: json['20021VYQRY-000013'],/* 国际化处理： 包含已两清*/
            checked: false
          }
        ]
      },
      {
        itemName: json['20021VYQRY-000014'],/* 国际化处理： 按核销号小计*/
        itemType: "checkbox",
        itemKey: "verifyNoSum",
        itemChild: [
          {
            label: json['20021VYQRY-000014'],/* 国际化处理： 按核销号小计*/
            checked: false
          }
        ]
      }
    ];
      self.setState({ json: json }, () => {
        initTemplate.call(self, self.props);
        // this.loadDept();
        //this.tableDefaultData=deepClone(tableDefaultData); 
      })
    }
    getMultiLang({ moduleId: ['20021VYQRY', 'publiccommon'], domainName: 'gl', currentLocale: 'simpchn', callback });
    let { listItem, queryCondition, resourceDatas, resourveQuery } = self.state;
    if (this.props.getUrlParam('link')) {
      let gziptools = new gzip();
      let verBalanceVOData = gziptools.unzip(this.props.getUrlParam('link'));
      let conditionData = gziptools.unzip(this.props.getUrlParam('condition'));
      let columnInfo = [], headtitle = [], balanceVO = [];
      if (verBalanceVOData) {

        let url = "/nccloud/gl/verify/verDetailLinkQuery.do";
        let param = { condition: conditionData, verBalanceVO: verBalanceVOData };
        ajax({
          url: url,
          data: param,
          success: function (response) {
            let { data, success } = response;
            if (success) {
              listItem.accountingbookValue.value = data.accountingbookValue;
              listItem.verObj.value = data.verObj;
              listItem.currency.value = data.currency;
              listItem.endDate.value = data.endDate;
              self.props.button.setButtonDisabled(['print', 'saveWidth', 'refresh', 'switch'], false);
              if (data.columnInfo) {
                columnInfo = data.columnInfo
              }
              if (data.headtitle) {
                headtitle = data.headtitle;
              }
              if (data.detailVO) {
                balanceVO = data.detailVO;
              }
              resourceDatas = data;
              self.setState({
                resourveQuery, queryCondition, resourceDatas, listItem
              })
              let renderFirstData = {};
              renderFirstData.columnInfo = data.columnInfo;
              renderFirstData.data = data.detailVO;
              renderFirstData.columnInfo1 = data.columnInfo1;
              renderFirstData.columnInfo2 = data.columnInfo2;
              setData(self, renderFirstData);
            }
          },
          error: function (error) {
            // toast({ content: error, color: 'warning' });
            // self.setState({
            //   queryShow:false
            // });
          }
        });
        self.setState({
          queryShow: false
        });
      }
    }
  }

  render() {

    let { columnsVerify, verifyBalancesData, listItem, queryShow, modalDefaultValue } = this.state;
    
    if (columnsVerify.length == 0) {
      columnsVerify = this.Defaultcolumns;
    }
    return (
      <div className="manageReportContainer verifyDetails nc-bill-list" id="m-verifyDetails">
			<HeaderArea
				title={this.state.json['20021VYQRY-000028']}/* 国际化处理： 往来核销情况查询*/

				btnContent={
					this.props.button.createButtonApp({
						area: 'button_area',
						onButtonClick: buttonClick.bind(this),
						// popContainer: document.querySelector('.header-button-area')
					})
				}
			/>
        <QueryModal
          modalDefaultValue={modalDefaultValue}
          loadData={this.loadQuery}
          showOrHide={this.state.showModal}
          onConfirm={this.handleQueryClick.bind(this)}
          handleClose={this.handleQuery.bind(this)}
        />
        {/* //表头 */}
        <div className="Mende-ncfrom-box-min nc-bill-search-area nc-theme-form-label-c nc-theme-area-bgc">
          <div  fieldid="verifyDetails_form-area">
            <NCForm
              useRow={true}
              submitAreaClassName="classArea "
              showSubmit={false}
            >
              {this.getHeadData(listItem)}
            </NCForm>
          </div>
        </div>
        <div fieldid="verifyDetails_report" className='report-table-area'>
        <SimpleTable
          ref="balanceTable"
          onCellMouseDown={this.handlesonTableClick.bind(this)}
          data={this.state.dataout}
        />
        </div>
      </div>
    );
  }
}
VerifyBalances = createPage({})(VerifyBalances);
ReactDOM.render(<VerifyBalances />, document.querySelector("#app"));

