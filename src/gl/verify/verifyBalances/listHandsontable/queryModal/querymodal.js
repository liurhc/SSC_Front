import React, { Component } from 'react';
import {high,base,ajax,getBusinessInfo,getMultiLang} from 'nc-lightapp-front';
import { CheckboxItem,RadioItem,SelectItem,} from '../../../../public/components/FormItems';
import { toast } from '../../../../public/components/utils.js';
import createScript from '../../../../public/components/uapRefer.js';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm,NCDiv
} = base;
import '../index.less'
import checkMustItem from "../../../../public/common/checkMustItem.js";
import getAssDatas from "../../../../public/components/getAssDatas/index.js";
const {  NCFormItem:FormItem } = NCForm;
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};

  const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
  };


export default class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal:false,
            isShowUnit:false,//是否显示业务单元
            assData: [//辅助核算信息
            ],
            modalDefaultValue:{},//默认数据
            loadData:[],//查询模板加载数据
            listItem:{},//模板数据对应值
            childAssData:{
                accountingbook_org:'',//保存账簿对应的组织
                pk_org:'',
                pk_accountingbook:''
            },//接受父组件传过来的参数
            SelectedAssData:[],//选中的数据
            checkedAll:true,
            checkedArray: [],
        };
        this.close = this.close.bind(this);
    }
    componentWillReceiveProps (nextProp) {
        let {loadData,showOrHide,modalDefaultValue}=nextProp;//this.props;
        let self=this;
        let { listItem,showModal,isShowUnit,childAssData }=self.state;
        
        if (showOrHide&&nextProp.loadData !== self.state.loadData&&self.state.loadData.length==0 ) {
            childAssData.pk_org=modalDefaultValue.pk_org;
            childAssData.accountingbook_org=modalDefaultValue.pk_org;
            isShowUnit=modalDefaultValue.isShowUnit;
            loadData.forEach((item,i)=>{
                let  key;
                if(item.itemType=='refer'){
                    if(item.itemKey=='pk_accasoa'||item.itemKey=='pk_units'){
                        key=[{
                            display:'',
                            value:'',
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }]
                    }else if(item.itemKey=='pk_accountingbook'){
                        key={
                            display:modalDefaultValue.pk_accountingbook.display,
                            value:modalDefaultValue.pk_accountingbook.value,
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                    }else if(item.itemKey=='pk_currency'){
                        key={
                            display:modalDefaultValue.pk_currency.display,
                            value:modalDefaultValue.pk_currency.value,
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                    }else{
                        key={
                            display:'',
                            value:'',
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                    }                   
                }else if(item.itemType=='select'||item.itemType=='Dbselect'||item.itemType=='radio'){//下拉框赋初始值
                    key={
                        value:item.itemChild[0].value
                    }
                }else{
                    key={
                        value:''
                    }
                    if(item.itemType=='date'||item.itemType=='Dbdate'){
                        if(item.itemKey[0]=='beginDate'){
                            key = {
                              value:modalDefaultValue.yearbegindate
                            };
                          }else if(item.itemKey[1]=='endDate'){
                            key = {
                              value:modalDefaultValue.bizDate
                            };
                          }else{
                            key = {
                              value: ""
                            };
                          }
                    }
                }
                if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'){
                    item.itemKey.map((k,index)=>{
                        let name= k;
                        listItem[name]=key
                    });
                }else{
                    let name= item.itemKey;
                    listItem[name]=key
                }
            
            })
            listItem.acccode=[];
            listItem.accname='';
            listItem['beginDate']={value:modalDefaultValue.yearbegindate};
            listItem['endDate']={value:modalDefaultValue.bizDate};
            self.setState({
                modalDefaultValue:modalDefaultValue,
                loadData:loadData,
                showModal:showOrHide,
                listItem,childAssData,isShowUnit
            })
        }
        // else{
        //     self.setState({
        //         showModal:showOrHide,childAssData,isShowUnit
        //     })
        // }
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (!nextProps.showOrHide && (nextProps.showOrHide == this.props.showOrHide) && nextProps.loadData == this.state.loadData) {
          return false;
        }
        return true;
      }
    //表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

    close() {
        this.props.handleClose();
    }

    confirm=()=>{
        let { listItem,assData,checkedArray,SelectedAssData } =this.state;
        SelectedAssData=[];
        let checkStatus=checkMustItem(listItem);//必输项校验
        if(!checkStatus.flag){
            toast({content:checkStatus.info+this.state.json['publiccommon-000001'],color:'warning'});
            return false;
        }
        for(var i = 0; i < checkedArray.length; i++){
            if(checkedArray[i]==true){
                SelectedAssData.push(assData[i]);
            }
        }
        listItem.ass=SelectedAssData;//assData
        this.props.onConfirm(listItem);
    }

    queryList=(data)=>{
        
        let self=this;
        const dateInputPlaceholder = self.state.json['20021VYBAL-000048'];/* 国际化处理： 选择日期*/
        function getNowFormatDate() {
            let date = new Date();
            let seperator1 = "-";
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            let currentdate = year + seperator1 + month + seperator1 + strDate;
            return currentdate;
        }
        let businessInfo = getBusinessInfo();
        let currrentDate = businessInfo.businessDate.split(' ')[0]; 
        let { listItem,isShowUnit,assData,checkedArray,childAssData,modalDefaultValue } =self.state;
			return data.length!=0?(
				data.map((item, i) => {                   
                    if(item.itemType=="refer") {
                       //case 'refer':
                       let referUrl= item.config.refCode+'/index.js';
                       let DBValue=[];
                       let defaultValue={}
                       if(listItem[item.itemKey].length){                           
                            listItem[item.itemKey].map((item,index)=>{
                                DBValue[index]={ refname: item.display, refpk:item.value };
                            })
                        }else{
                            defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
                        }
					   if(!self.state[item.itemKey]){
                           {createScript.call(self,referUrl,item.itemKey)}
                           return <div />
					    }else{
                            if(item.itemKey=='pk_accountingbook'){
                                return (
                                    <FormItem
                                         inline={true}
                                         showMast={item.isMustItem}
                                         labelXs={2} labelSm={2} labelMd={2}
                                         xs={10} md={10} sm={10}
                                         labelName={item.itemName}
                                         isRequire={true}
                                         method="change"
                                        >
                                        {self.state[item.itemKey]?(self.state[item.itemKey])(
                                            {
                                                fieldid:item.itemKey,
                                                value:defaultValue,
                                                isMultiSelectedEnabled:false,
                                                showGroup:false,
                                                disabledDataShow:true,
                                                queryCondition:() => {
                                                    return Object.assign({
                                                        "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                                            "appcode":modalDefaultValue.appcode
                                                    },config)
                                                 },
                                                onChange:(v)=>{
                                                    if(v.refpk){
                                                        //判断是否起用业务单元
                                                        let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
                                                        let pk_accpont = {"pk_accountingbook":v.refpk}
                                                        ajax({
                                                            url:url,
                                                            data:pk_accpont,
                                                            success: function(response){
                                                                const { success } = response;
                                                                //渲染已有账表数据遮罩
                                                                if (success) {
                                                                    if(response.data){
                                                                        isShowUnit=response.data.isShowUnit;
                                                                        childAssData.pk_org=response.data.unit.value;
                                                                        childAssData.accountingbook_org=response.data.unit.value;
                                                                        listItem['pk_currency'].value = response.data.currinfo.value;
                                                                        listItem['pk_currency'].display = response.data.currinfo.display;
                                                                        listItem['beginDate']={value:response.data.yearbegindate};
                                                                        listItem['endDate']={value:response.data.bizDate};
                                                                    }
                                                                    self.setState({
                                                                        isShowUnit,childAssData
                                                                    })
                                                                }   
                                                            }
                                                        });
                                                    }
                                                    listItem[item.itemKey].value = v.refpk
                                                    listItem[item.itemKey].display = v.refname
                                                    //清空科目,业务单元，辅助核算项
                                                    // listItem['pk_accasoa'][0].display='';
                                                    // listItem['pk_accasoa'][0].value='';
                                                    if(listItem['pk_accasoa'].length>0){
                                                        listItem['pk_accasoa'][0].display='';
                                                    }
                                                    if(listItem['pk_accasoa'].length>0){
                                                        listItem['pk_accasoa'][0].value='';
                                                    }
                                                    listItem['pk_units']=[{display:'',value:''}];
                                                    this.setState({
                                                        listItem
                                                    })
                                                }
                                            }
                                        ):<div />}
                                </FormItem>);
                            }else if(item.itemKey=='pk_accasoa'){
                                return (
                                    <FormItem
                                         inline={true}
                                         showMast={item.isMustItem}
                                         labelXs={2} labelSm={2} labelMd={2}
                                         xs={10} md={10} sm={10}
                                         labelName={item.itemName}
                                         isRequire={true}
                                         method="change"
                                     >
                                    {self.state[item.itemKey]?(self.state[item.itemKey])(
                                        {
                                            fieldid:item.itemKey,
                                            value:DBValue,
                                            isMultiSelectedEnabled:true,
                                            queryCondition:() => {
                                                return Object.assign({
                                                    "refName":item.itemName,
                                                    "pk_accountingbook": listItem.pk_accountingbook.value? listItem.pk_accountingbook.value:'',
                                                    "dateStr":currrentDate,
                                                    "TreeRefActionExt":'nccloud.web.gl.verify.action.VerifyObjectRefSqlBuilder'
                                                    
                                                },config)
                                            },
                                            onFocus:(v)=>{
                                                if(!listItem.pk_accountingbook.value){
                                                    toast({content:this.state.json['20021VYBAL-000056'],color:'warning'});/* 国际化处理： 请先选择核算账簿，核算账簿不能为空*/
                                                    return false;
                                                }
                                            },
                                            onChange:(v)=>{
                                               
                                                listItem[item.itemKey]=[];
                                                listItem.acccode=[];
                                                let paramAccasoa=[];
                                                v.map((arr,index)=>{
                                                    paramAccasoa.push(arr.refpk);
                                                    listItem.acccode.push(arr.refcode);
                                                    listItem.accname=v[0].refname;
                                                    let accasoa={
                                                        display:arr.refname,
                                                        value:arr.refpk
                                                    }
                                                    listItem[item.itemKey].push(accasoa);
                                                })
                                                //根据选定的pk 实现过滤
                                                if(item.itemKey!='pk_accountingbook'&&!listItem.pk_accountingbook.value){
                                                    toast({ content: this.state.json['20021VYBAL-000057'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
                                                    return false
                                                }
                                                //请求辅助核算数据
                                                let url = '/nccloud/gl/voucher/queryAssItem.do';
                                                let queryData = {
                                                    pk_accasoa: paramAccasoa,
                                                    prepareddate: currrentDate,
                                                };
                                                assData=[];
                                                checkedArray=[];//选中的数据
                                                if(v.length>0){
                                                    ajax({
                                                        url:url,
                                                        data:queryData,
                                                        success: function(response){
                                                            const { success } = response;
                                                            //渲染已有账表数据遮罩
                                                            if (success) {
                                                                if(response.data){
                                                                    if(response.data.length>0){
                                                                        response.data.map((item,index)=>{
                                                                            checkedArray.push(true);
                                                                            let obj={
                                                                                key:index,
                                                                                "checktypecode":item.code,
                                                                                "checktypename" :item.name,
                                                                                "pk_Checktype": item.pk_accassitem,
                                                                                "refCode":item.refCode?item.refCode:item.code,
                                                                                "refnodename":item.refnodename,
                                                                                "pk_accassitem":item.pk_accassitem,
                                                                                "pk_accountingbook":listItem.pk_accountingbook.value,
                                                                                "classid":item.classid,
                                                                                "pk_defdoclist":item.classid,
                                                                                "pk_accountingbook":childAssData.pk_accountingbook
                                                                                }
                                                                            assData.push(obj);
                                                                        })
                                                                    }
                                                                }
                                                                self.setState({
                                                                    assData,checkedArray
                                                                })
                                                            }   
                                                        }
                                                    });
                                                }
                                                // listItem[item.itemKey].value = v.refpk
                                                // listItem[item.itemKey].display = v.refname
                                                this.setState({
                                                    listItem,assData,checkedArray
                                                })
                                            }
                                        }
                                    ):<div />}
                                </FormItem>);
                            }else if(item.itemKey=='pk_units'){
                                if(isShowUnit){
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                        >
                                            {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                {
                                                    fieldid:item.itemKey,
                                                    value:DBValue,
                                                    isMultiSelectedEnabled:true,
                                                    queryCondition:() => {
                                                        return Object.assign({
                                                            "pk_accountingbook": listItem.pk_accountingbook.value? listItem.pk_accountingbook.value:'',
                                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                                        },config)
                                                    },
                                                    onChange:(v)=>{
                                                        //根据选定的pk 实现过滤
                                                        if(item.itemKey!='pk_accountingbook'&&!listItem.pk_accountingbook.value){
                                                            toast({ content: this.state.json['20021VYBAL-000057'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
                                                            return false
                                                        }
                                                        // if(v.length<=0){return false;}
                                                        listItem[item.itemKey]=[];
                                                        v.map((arr,index)=>{
                                                            let accasoa={
                                                                display:arr.refname,
                                                                value:arr.refpk
                                                            }
                                                            listItem[item.itemKey].push(accasoa);
                                                        })
                                                        if(v[0]){
                                                            childAssData.pk_org=v[0].refpk;
                                                        }else{
                                                            childAssData.pk_org=childAssData.accountingbook_org;//v[0];
                                                        }
                                                        //辅助核算项的值
                                                        assData.map((item,index)=>{
                                                            item.checkvaluename = null;
                                                            item.pk_Checkvalue = null;
                                                            item.checkvaluecode = null;
                                                        })
                                                        // listItem[item.itemKey].value = v.refpk
                                                        // listItem[item.itemKey].display = v.refname
                                                        this.setState({
                                                            listItem,childAssData,assData
                                                        })
                                                    }
                                                }
                                            ):<div />}
                                    </FormItem>
                                    );
                                }else{
                                    return(<div/>)
                                }
                            }else{
                                return (
                                    <FormItem
                                         inline={true}
                                         showMast={item.isMustItem}
                                         labelXs={2} labelSm={2} labelMd={2}
                                         xs={10} md={10} sm={10}
                                         labelName={item.itemName}
                                         isRequire={true}
                                         method="change"
                                        >
                                        {self.state[item.itemKey]?(self.state[item.itemKey])(
                                            {
                                                fieldid:item.itemKey,
                                                value:defaultValue,
                                                //isMultiSelectedEnabled:true,
                                                queryCondition:() => {
                                                    return Object.assign({
                                                        //"pk_accountingbook": self.state.pk_accountingbook.value
                                                    },config)
                                                },    
                                                onChange:(v)=>{
                                                    listItem[item.itemKey].value = v.refpk
                                                    listItem[item.itemKey].display = v.refname
                                                    this.setState({
                                                        listItem
                                                    })
                                                }
                                            }
                                        ):<div />}
                                </FormItem>);
                            } 
						   
					    }
                        //case 'date':
                    }else if(item.itemType=="date"){ 
                            return (
                                    <FormItem
                                        inline={true}
                                        showMast={true}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        method="blur"
                                        inputAlfer="%"
                                        errorMessage={this.state.json['20021VYBAL-000058']}/* 国际化处理： 输入格式错误*/
                                    >
                                        <DatePicker
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                            isRequire={true}
                                            //format={format}
                                            //disabled={isChange}
                                            //value={moment(listItem[item.itemKey].value)}
                                            //locale={zhCN}
                                            value={listItem[item.itemKey].value}
                                            onChange={(v) => {
                                                listItem[item.itemKey].value = v;
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                            placeholder={dateInputPlaceholder}
                                            />
                                        </FormItem>
                            );
                        //case'Dbdate':
                    }else if(item.itemType=="Dbdate"){     
                            return(
                                // <Row>
                                // <Col xs={12} md={12} sm={12} className="dateMargin">
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10} md={10} sm={10}
                                    labelName={item.itemName}
                                    method="blur"
                                    inputAlfer="%"
                                    errorMessage={this.state.json['20021VYBAL-000058']}/* 国际化处理： 输入格式错误*/
                                    inputAfter={
                                        <Col xs={12} md={12} sm={12}>
                                        <span className="online">{this.state.json['publiccommon-000005']/*至*/}</span>
                                        <div style={{display:'inline-block'}} > 
                                        <DatePicker
                                        fieldid='endDate'
                                        name={item.itemKey}
                                        type="customer"
                                        isRequire={true}
                                        value={listItem.endDate.value}
                                        onChange={(v) => {
                                            listItem.endDate={value: v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    />
                                    </div>
                                    </Col>}
                                    >
                                    <DatePicker
                                        fieldid='beginDate'
                                        name={item.itemKey}
                                        type="customer"
                                        isRequire={true}
                                        value={listItem.beginDate.value}
                                        onChange={(v) => {
                                            listItem.beginDate={value: v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    />
                                </FormItem>
                                // </Col>
                                // </Row>
                            );
                        //case 'textInput':
                    }else if(item.itemType=="textInput"){     
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                    <FormControl
                                        fieldid={item.itemKey}
                                        value={listItem[item.itemKey].value}
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            );
                        //case 'DbtextInput':
                    }else if(item.itemType=="DbtextInput"){    
                            return(
                            // <Row>
                            //     <Col xs={8}  md={8} sm={8} className="dateMargin labelMargin">
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                   labelXs={2}  labelSm={2} labelMd={2}
                                    xs={10}  md={10} sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                    inputAfter={
                                    <Col xs={12} md={12} sm={12}>
                                        <span className="online">{this.state.json['publiccommon-000005']/*至*/}</span>    
                                        <div style={{display:'inline-block'}} >                                                                    
                                        <FormControl
                                        fieldid={item.itemKey[1]}
                                            value={listItem[item.itemKey[1]].value}
                                            //disabled={true}
                                        // onFocus={this.focus}
                                            onChange={(v) => {
                                                let startkey=item.itemKey[0];
                                                let endkey=item.itemKey[1];
                                                // listItem[item.itemKey].value = v
                                                listItem[endkey]={value : v}
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                        />
                                        </div>  
                                    </Col>}
                                >
                                    <FormControl
                                    fieldid={item.itemKey[0]}
                                        value={listItem[item.itemKey[0]].value}
                                        //disabled={true}
                                    // onFocus={this.focus}
                                        className="DbtextInput"
                                        onChange={(v) => {
                                            listItem[item.itemKey[0]]={value : v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            //     </Col>                                
                            // </Row>
                            );
                        //case 'radio':
                    }else if(item.itemType=="radio"){    
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                                className={item.itemKey!='direct'?'radioDateType':''}
                            >
                                    <RadioItem
                                        fieldid={item.itemKey}
                                        name={item.itemKey}
                                        type="customer"
                                        defaultValue={listItem[item.itemKey].value}
                                        items={() => {
                                            return (item.itemChild) 
                                        }}
                                        onChange={(v)=>{
                                            listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                        }}
                                    />
                            </FormItem>
                            )
                        //case 'select':
                    }else if(item.itemType=="select"){    
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                <SelectItem 
                                    name={item.itemKey}
                                    fieldid={item.itemKey}
                                    defaultValue={listItem[item.itemKey].value} 
                                        items = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{

                                            listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                        }}
                            />
                            </FormItem>)
                        //case 'Dbselect':
                    }else if(item.itemType=="Dbselect"){    
                            return(
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={2}
                                    md={2}
                                    sm={2}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                    >
                                    <SelectItem 
                                        name={item.itemKey}
                                        fieldid={item.itemKey}
                                        defaultValue={listItem[item.itemKey].value} 
                                            items = {
                                                () => {
                                                    return (item.itemChild) 
                                                }
                                            }
                                            onChange={(v)=>{
                                                listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                    />
                                </FormItem>
                            )
                        //case 'checkbox':
                    }else if(item.itemType=="checkbox"){    
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                xs={12}  md={12} sm={12}
                                className='checkboxStyle'
                                method="change"
                            >
                                <CheckboxItem name={item.itemKey}
                                    boxs = {
                                        () => {
                                            return (item.itemChild) 
                                        }
                                    }
                                    onChange={(v)=>{
                            
                                        listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                        this.setState({
                                            listItem
                                        })
                                    }}
                                />
                            </FormItem>
                            )
                   }
               })
			):<div/>;
           
    }

    componentWillMount() {

        let callback= (json) =>{
          this.setState({json:json},()=>{

          })
        }
        getMultiLang({moduleId:['20021VYBAL','publiccommon','publiccomponents'],domainName:'gl',currentLocale:'simpchn',callback});
      }
    onAllCheckChange = () => {
        let self = this;
        let checkedArray = [];
        let selIds = [];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          checkedArray[i] = !self.state.checkedAll;
        }
        self.setState({
          checkedAll: !self.state.checkedAll,
          checkedArray: checkedArray,
        });
      };
      onCheckboxChange = (text, record, index) => {
        let self = this;
        let allFlag = false;
        let checkedArray = self.state.checkedArray.concat();
        checkedArray[index] = !self.state.checkedArray[index];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          if (!checkedArray[i]) {
            allFlag = false;
            break;
          } else {
            allFlag = true;
          }
        }
        self.setState({
          checkedAll: allFlag,
          checkedArray: checkedArray,
        });
      };
      
      renderColumnsMultiSelect(columns) {
        const {checkedArray } = this.state;
        const { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        // let indeterminate_bool1 = true;
        if (multiSelect && multiSelect.type === "checkbox") {
          let i = checkedArray.length;
          while(i--){
              if(checkedArray[i]){
                indeterminate_bool = true;
                break;
              }
          }
          let defaultColumns = [
            {
              title: (
                <Checkbox
                  className="table-checkbox"
                  checked={this.state.checkedAll}
                  indeterminate={indeterminate_bool&&!this.state.checkedAll}
                  onChange={this.onAllCheckChange}
                />
              ),
              key: "checkbox",
              dataIndex: "checkbox",
              width: 50,
              render: (text, record, index) => {
                return (
                  <Checkbox
                    className="table-checkbox"
                    checked={this.state.checkedArray[index]}
                    onChange={this.onCheckboxChange.bind(this, text, record, index)}
                  />
                );
              }
            }
          ];
          columns = defaultColumns.concat(columns);
        }
        return columns;
      }

    render() {
        let{showOrHide}=this.props;
        let { loadData,assData,checkedAll,checkedArray,childAssData} =this.state;
        let columns10=[
            {
              title: this.state.json['20021VYBAL-000049'],/* 国际化处理： 核算类型*/
              dataIndex: "checktypename",
              key: "checktypename",
              width: "30%",
              render: (text, record, index) => {
				return <span>{record.checktypename}</span>;
			    }
            },
            {
              title: this.state.json['20021VYBAL-000050'],/* 国际化处理： 核算内容*/
              dataIndex: "checkvaluename",
              key: "checkvaluename",
                render: (text, record, index) => {
                    let { assData,childAssData } =this.state;
                    let defaultValue=[];
                    if(assData[index]['checkvaluename']){                           
                        assData[index]['checkvaluename'].split(",").map((item,_index)=>{
                            defaultValue[_index]={ refname: item, refpk:'' };
                        })
                        assData[index]['pk_Checkvalue'].split(",").map((item,_index)=>{
                            defaultValue[_index].refpk=item;
                        })
                    }else{
                        defaultValue=[{refname:"",refpk:""}];
                    }
                    if(record.refnodename){
                        let referUrl= record.refnodename+'.js'; 
                        if(!this.state[record.pk_accassitem]){
                            {createScript.call(this,referUrl,record.pk_accassitem)}
                            return <div />
                        }else{
                            if(record.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'){//部门，人员
                                return (
                                    <FormItem
                                        inline={true}
                                    // showMast={true}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={10} md={10} sm={10}
                                        //labelName={record.itemName}
                                        isRequire={true}
                                        method="change"
                                    >
                                    {this.state[record.pk_accassitem]?(this.state[record.pk_accassitem])(
                                        {
                                            value:defaultValue,
                                            isShowUnit:true,
                                            unitProps:{
                                                refType: 'tree',
                                                refName: this.state.json['20021VYBAL-000001'],/* 国际化处理： 业务单元*/
                                                refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                                rootNode:{refname:this.state.json['20021VYBAL-000001'],refpk:'root'},/* 国际化处理： 业务单元*/
                                                placeholder:this.state.json['20021VYBAL-000001'],/* 国际化处理： 业务单元*/
                                                queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                                treeConfig:{name:[this.state.json['20021VYBAL-000051'], this.state.json['20021VYBAL-000052']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                                                isMultiSelectedEnabled: false,
                                                //unitProps:unitConf,
                                                isShowUnit:false
                                            },
                                            unitCondition:{
                                                pk_financeorg:childAssData.pk_org,
                                                'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                                            },
                                            isMultiSelectedEnabled:true,
                                            "unitValueIsNeeded":false,
                                            "isShowDimission":true,
                                            queryCondition:() => {
                                                config.isShowDimission=true;
                                                return Object.assign({
                                                    "busifuncode":"all",
                                                    "pk_org":childAssData.pk_org
                                                },config) 
                                            },
                                            onChange:(v)=>{
                                                let { assData } =this.state;
                                                let originData = this.findByKey(record.key, assData);
                                                let refnameArr=[],refpkArr=[],refcodeArr=[];
                                                if (originData) {
                                                    v.map((arr,index)=>{
                                                            refnameArr.push(arr.refname);
                                                            refpkArr.push(arr.refpk);
                                                            refcodeArr.push(arr.refcode);
                                                    
                                                    })    
                                                    originData.checkvaluename = (v.length>0)?refnameArr.join():null;
                                                    originData.pk_Checkvalue = (v.length>0)?refpkArr.join():null;
                                                    originData.checkvaluecode=(v.length>0)?refcodeArr.join():null;
                                                }
                                                childAssData.assData=assData; 
                                                this.setState({
                                                assData,childAssData
                                                })
                                            }
                                        }):<div />}
                                    </FormItem>);
                            }else{
                                return (
                                <FormItem
                                    inline={true}
                                // showMast={true}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10} md={10} sm={10}
                                    //labelName={record.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                {this.state[record.pk_accassitem]?(this.state[record.pk_accassitem])(
                                    {
                                        value:defaultValue,
                                        isMultiSelectedEnabled:true,
                                        queryCondition:() => {
                                            if(record.classid&&record.classid.length==20){//classid的长度等于20的话过滤条件再加一个pk_defdoclist
                                                return Object.assign({
                                                    "pk_org":childAssData.pk_org,
                                                    "pk_defdoclist":record.pk_defdoclist
                                                },config)
                                            }else{
                                            return Object.assign({
                                                    "pk_org":childAssData.pk_org
                                                },config) 
                                            }   
                                        },
                                        onChange:(v)=>{
                                            let { assData } =this.state;
                                            let originData = this.findByKey(record.key, assData);
                                            let refnameArr=[],refpkArr=[],refcodeArr=[];
                                            if (originData) {
                                                v.map((arr,index)=>{
                                                        refnameArr.push(arr.refname);
                                                        refpkArr.push(arr.refpk);
                                                        refcodeArr.push(arr.refcode);
                                                
                                                })    
                                                originData.checkvaluename = (v.length>0)?refnameArr.join():null;
                                                originData.pk_Checkvalue = (v.length>0)?refpkArr.join():null;
                                                originData.checkvaluecode=(v.length>0)?refcodeArr.join():null;
                                            }
                                            childAssData.assData=assData; 
                                            this.setState({
                                            assData,childAssData
                                            })
                                        }
                                    }):<div />}
                                </FormItem>);
                            }
                        }
                    }else{//不是参照的话要区分日期、字符、数值
                        if(record.classid=='BS000010000100001033'){//日期
                            return(
                                <DatePicker
                                    //name={item.itemKey}
                                    type="customer"
                                    isRequire={true}
                                    placeholder={dateInputPlaceholder}
                                    value={defaultValue[0].refname}
                                    onChange={(v) => {
                                        let { assData } =this.state;
                                        let originData = this.findByKey(record.key, assData);
                                        if (originData) {
                                            let assArr=[];
                                            assArr.push(v);
                                            originData.checkvaluename = v?assArr.join():null;
                                            originData.pk_Checkvalue =v?assArr.join():null;
                                            originData.checkvaluecode=v?assArr.join():null;
                                        }
                                        childAssData.assData=assData; 
                                        this.setState({
                                        assData,childAssData
                                        })
                                    }}
                                    
                                    />
                            )
                        }else if(record.classid=='BS000010000100001031'){//数值
                            return(
                                <NCNumber
                                    scale={2}
                                    value={defaultValue[0].refname}
                                    placeholder={this.state.json['20021VYBAL-000053']}/* 国际化处理： 请输入数字*/
                                    onChange={(v)=>{
                                        let { assData } =this.state;
                                        let originData = this.findByKey(record.key, assData);
                                        if (originData) {
                                            let assArr=[];
                                            assArr.push(v);
                                            originData.checkvaluename = v?assArr.join():null;
                                            originData.pk_Checkvalue =v?assArr.join():null;
                                            originData.checkvaluecode=v?assArr.join():null;
                                        }
                                        childAssData.assData=assData; 
                                        this.setState({
                                        assData,childAssData
                                        })
                                    }}
                                />
                            )
                        }else if(record.classid=='BS000010000100001032'){//布尔
                            return(
                                    <FormItem
                                    inline={true}
                                    // showMast={false}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    // labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                    <SelectItem name={record.checktypecode}
                                        defaultValue={defaultValue[0].refname} 
                                        items = {
                                            () => {
                                                return ([{
                                                    label: this.state.json['20021VYBAL-000054'],/* 国际化处理： 是*/
                                                    value: 'Y'
                                                }, {
                                                    label: this.state.json['20021VYBAL-000055'],/* 国际化处理： 否*/
                                                    value: 'N'
                                                }]) 
                                            }
                                        }
                                        onChange={(v)=>{
                                            let { assData } =this.state;
                                            let originData = this.findByKey(record.key, assData);
                                            if (originData) {
                                                let assArr=[];
                                                assArr.push(v);
                                                originData.checkvaluename = v?assArr.join():null;
                                                originData.pk_Checkvalue =v?assArr.join():null;
                                                originData.checkvaluecode=v?assArr.join():null;
                                            }
                                            childAssData.assData=assData; 
                                            this.setState({
                                            assData,childAssData
                                            })
                                        }}
                                    />
                                </FormItem>
                            )
                        }else{//字符
                            return(
                                <FormControl
                                    value={defaultValue[0].refname}
                                    onChange={(v)=>{
                                        let { assData } =this.state;
                                        let originData = this.findByKey(record.key, assData);
                                        if (originData) {
                                            let assArr=[];
                                            assArr.push(v);
                                            originData.checkvaluename = v?assArr.join():null;
                                            originData.pk_Checkvalue =v?assArr.join():null;
                                            originData.checkvaluecode=v?assArr.join():null;
                                        }
                                        childAssData.assData=assData; 
                                        this.setState({
                                        assData,childAssData
                                        })
                                    }}
                                />
                            )
                        }
                        
                    }
                }
            }
        ];
        let columnsldad = this.renderColumnsMultiSelect(columns10);
        let pretentAssData={
            assData: assData||[],
            childAssData:childAssData||[],
            checkboxShow: true,//是否显示复选框
            showOrHide:true,
            checkedAll:checkedAll,//全选复选框的选中状态
            checkedArray:checkedArray||[],//复选框选中情况
            $_this:this
          };
        const emptyFunc = () => <span>{this.state.json['20021VYBAL-000060']}！</span>/* 国际化处理： 这里没有数据*/
        return (
            <div className="fl">
                <Modal
                    fieldid="query"
                    className={'combine'}
                    show={showOrHide }
                    id="queryone"
                    // backdrop={ this.state.modalDropup }
                    onHide={ this.close }
                    animation={true}
                    >
                    <Modal.Header closeButton fieldid="header-area">
                        <Modal.Title>{this.state.json['20021VYBAL-000061']}</Modal.Title>{/* 国际化处理： 核销余额表查询*/}
                    </Modal.Header >
                    <Modal.Body >
                        <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                            <NCForm useRow={true}
                                submitAreaClassName='classArea'
                                showSubmit={false}　>
                                {loadData.length > 0 ? this.queryList(loadData) : ''}
                            </NCForm>
                        </NCDiv>
                        <div className="getAssDatas">
                        {/* <Table
                            columns={columnsldad} data={assData}
                            emptyText={emptyFunc}
                            scroll={{
                                x: true,
                               y:124
                            }}
                        /> */}
                        {getAssDatas({pretentAssData})}
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button colors="primary" onClick={ this.confirm } fieldid="confirm"> {this.state.json['20021VYBAL-000041']} </Button>{/* 国际化处理： 查询*/}
                        <Button onClick={ this.close } fieldid="close"> {this.state.json['20021VYBAL-000062']} </Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
SearchModal.defaultProps = defaultProps12;
