import { ajax, base, toast,cacheTools,print,withNav } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
import { setData } from '../../../../manageReport/common/simbleTableData.js';
let tableid = 'gl_brsetting';
// @withNav
export default function buttonClick(props, id) {
    let {querycondition}=this.state;
    let rowDatas,pk_brsetting,pk_unit,pk_currtype,link;
    switch (id) {
        case 'refresh'://刷新
            this.handleRefresh();
            break;
        case 'query'://查询
            this.setState({
                showModal:true
            })
            break;
        case 'switch'://切换
          /**
         * 切换显示方式
         * @param {Number} showType - 显示方式：1 - 名称和编码均显示；2 - 只显示名称；3 - 只显示编码；
         */
        
        let self=this;
        let {resourceDatas,flag,accountType}=self.state;

        let data=resourceDatas;
        let ChangecolumnInfo=data.columnInfo;
        if(flag>=2){
            flag=0;
        }else{
            flag++;
        }
        if(flag==0){
            accountType='columnInfo';
            // ChangecolumnInfo=data.columnInfo;
        }else if(flag==1){
            accountType='columnInfo1';
            // ChangecolumnInfo=data.columnInfo1;
        }else if(flag==2){
            accountType='columnInfo2';
            // ChangecolumnInfo=data.columnInfo2;
        }
        let renderFirstData = {};
          renderFirstData.columnInfo = data.columnInfo;
          renderFirstData.data = data.balanceVO;
          renderFirstData.columnInfo1= data.columnInfo1;
          renderFirstData.columnInfo2 =data.columnInfo2;
          self.setState({
            accountType,flag
          },()=>{
              setData(self,renderFirstData);
          })
          
        // self.changeToHandsonTableData(self,data,ChangecolumnInfo,data.headtitle,data.busirecon);
            break;
        case 'detail'://核销情况
        this.handleVerifyDetails();
        break;
        case 'print'://直接输出
            this.print();
            break;
        case 'modalout'://模板输出
            this.showOutputModal()
            break;
        case 'handprint'://打印
             this.showPrintModal()
            break;
        case 'savewidth'://保存列宽
            this.handleSaveColwidth()
            break;

        
        default:
        break;

    }
}
