import React, { Component } from "react";
import { high, base, ajax, deepClone, createPage, toast, getMultiLang, gzip, createPageIcon } from "nc-lightapp-front";
import { buttonClick, initTemplate } from './events';
import './index.less'
const { NCForm, NCAffix, NCTooltip: Tooltip } = base;
const { NCFormItem: FormItem } = NCForm;
import { InputItem } from "../../../public/components/FormItems";
import QueryModal from "./queryModal/querymodal.js";
import { SimpleTable } from "nc-report";
import HeaderArea from '../../../public/components/HeaderArea';
import { tableDefaultData } from "../../../manageReport/defaultTableData";
import reportPrint from '../../../public/components/reportPrint.js';
import PrintModal from '../../../public/components/printModal'
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
const { PrintOutput } = high;
import { printRequire, mouldOutput } from '../../../public/components/printModal/events';
import { setData } from '../../../manageReport/common/simbleTableData.js';
import { rowBackgroundColor } from "../../../manageReport/common/htRowBackground";
import reportSaveWidths from '../../../public/common/reportSaveWidths.js';
import { gl_pkreport_verifyBalances } from '../../../public/common/reportPkConst.js';
class VerifyBalances extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSources: [],//记录查询出来的数据
      dataLen: "",//记录数据条数
      json: {},
      accountType: 'columnInfo',
      flag: 0,//切换表头用的
      appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
      showPrintModal: false,
      outputData: {},
      ctemplate: '',
      nodekey: '',
      flag: 0,
      showTable: true, //控制是否显示表格
      flow: false, //控制表头是否分级
      parentMerge: [], //父级要合并的个数
      showType: 1,
      resourveQuery: {},//原始查询条件
      resourceDatas: [], // 原始数据
      dataout: tableDefaultData,
      selectedRecord: "", //复制的key
      getrowkey: "", //获取指定行的Key
      listItem: {
        accountingbookValue: { display: "", value: "" },
        verObj: { display: "", value: "" },
        currency: { display: "", value: "" },
        endDate: { display: "", value: "" }
      },
      showModal: false, //查询条件对话框
      columnsVerify: [],
      verifyBalancesData: [],
      querycondition: [], //查询条件
      selectedData: {},//选中行的数据
      textAlginArr: [],
      modalDefaultValue: {
        pk_org: '',//账簿对应的组织
        bizDate: '',//业务日期
        yearbegindate: '',//年初日期
        begindate: '',
        enddate: '',
        bizPeriod: '',//账簿对应的会计期间
        isShowUnit: false,//是否显示业务单元
        isOpenUpBusiUnit: '',//
        pk_accperiodscheme: '',//会计期间方案
        pk_accountingbook: { display: '', value: '' },
        pk_currency: { display: '', value: '' },
        appcode: this.props.getSearchParam("c")
      },//模态框里默认数据
    };
    this.childCount = this.childCount.bind(this);
    this.rowBackgroundColor = rowBackgroundColor.bind(this);
    this.handlesonTableClick = this.handlesonTableClick.bind(this)
  }
  //直接输出
  print() {
    let { resourceDatas, dataout, textAlginArr } = this.state;
    let dataArr = [];
    let emptyArr = [];
    let mergeInfo = dataout.data.mergeInfo;
    dataout.data.cells.map((item, index) => {
      emptyArr = [];
      item.map((list, _index) => {
        if (list) {
          emptyArr.push(list.title);
        }
      })
      dataArr.push(emptyArr);
    })

    reportPrint(mergeInfo, dataArr, textAlginArr);
  }
  //查询
  handleQuery = () => {

    this.setState({
      showModal: true
    });
  };
  //查询
  handleClose = () => {
    this.setState({
      showModal: false
    });
  };
  childCount(data) {
    //计算父级要合并的个数
    for (let i = 0; i < data.columnInfo.length; i++) {
      let value = data.columnInfo[i];
      //*********
      if (value.children) {
        //有子元素的
        let childLeng = value.children.length;


        let headCol = currentCol;
        for (let i = 0; i < value.children.length; i++) {

          let childlen = 0;
          if (value.children[i].children) {
            self.state.innerChild = value.children[i].children.length;
            currentCol = currentCol + self.state.innerChild;
          } else {
            //子元素里面没有子元素的
            mergeCells.push([
              columheadrow + 1,
              currentCol,
              columheadrow + 2,
              currentCol
            ]); //cell
            currentCol++;
          }
        }
      } else {
        //没有子元素对象
        this.state.parentMerge.push(0);
      }
    }
  }
  //查询确定按钮
  handleQueryClick = (data) => {
    let self = this;
    let {
      listItem,
      columnsVerify,
      verifyBalancesData,
      querycondition, resourceDatas, resourveQuery,
    } = this.state;
    let url = "/nccloud/gl/verify/verBalanceQuery.do";
    resourveQuery = deepClone(data);
    let childData = deepClone(data);
    for (let k in childData) {
      if (k == "ass" || k == "acccode" || k == "accname") {
      } else if (k == "pk_currency") {
        childData.curName = childData[k].display ? childData[k].display : null;
        childData[k] = childData[k].value ? childData[k].value : null;
      } else if (k == "pk_accasoa" || k == "pk_units") {
        if (childData[k].length > 0) {
          if (childData[k][0].value == "") {
            childData[k] = null;
          } else {
            childData[k].map((item, index) => {
              childData[k][index] = item.value;
            });
          }
        } else {
          childData[k] = null;
        }
      } else {
        childData[k] = childData[k].value ? childData[k].value : null;
      }
    }
    childData.pagecode = gl_pkreport_verifyBalances;
    querycondition = childData;
    let columnInfo = [], headtitle = [], balanceVO = [];
    ajax({
      url: url,
      data: childData,
      success: function (response) {
        let { data, success } = response;
        if (success) {
          listItem.accountingbookValue.value = data.accountingbookValue;
          listItem.verObj.value = data.verObj;
          listItem.currency.value = data.currency;
          listItem.endDate.value = data.endDate;
          let num = 1;
          let numArr = [];


          if (data.columnInfo) {
            columnInfo = data.columnInfo
          }
          if (data.headtitle) {
            headtitle = data.headtitle;
          }
          if (data.balanceVO) {
            balanceVO = data.balanceVO;
          }
          resourceDatas = data;
          self.setState({
            dataLen: balanceVO.length,
            resourveQuery, querycondition, resourceDatas, listItem,
            dataSources: balanceVO,
            selectRowIndex: 0

          })
          let renderFirstData = {};
          renderFirstData.columnInfo = data.columnInfo;
          renderFirstData.data = data.balanceVO ? data.balanceVO : [];
          renderFirstData.columnInfo1 = data.columnInfo1;
          renderFirstData.columnInfo2 = data.columnInfo2;
          setData(self, renderFirstData);
          if (data.balanceVO) {
            self.props.button.setButtonDisabled(['switch', 'handprint', 'modalout', 'print', 'savewidth', 'refresh',], false);
          }
        }
      },
      error: function (error) {
        //toast({ content: error, color: 'warning' });
        self.setState({
          showModal: false
        });
      }
    });
  };

  //处理width
  changeWidth(arr) {
    arr.map((item, index) => {
      if (item.children) {
        this.changeWidth(item.children);
      } else {
        item["width"] = 100;
      }
    });
    return arr;
  }

  getHeadData = listItem => {
    const headData = [
      {
        itemName: this.state.json['20021VYBAL-000020'],/* 国际化处理： 核算账簿:*/
        itemType: "textInput",
        itemKey: "accountingbookValue"
      },
      { itemName: this.state.json['20021VYBAL-000021'], itemType: "textInput", itemKey: "verObj" },/* 国际化处理： 核销对象:*/
      { itemName: this.state.json['20021VYBAL-000022'], itemType: "textInput", itemKey: "currency" },/* 国际化处理： 币种:*/
      { itemName: this.state.json['20021VYBAL-000023'], itemType: "textInput", itemKey: "endDate" }/* 国际化处理： 截止日期:*/
    ];
    return headData.map((item, i) => {
      let defValue = listItem[item.itemKey].value;
      switch (item.itemType) {
        case "textInput":
          return (
            <FormItem
              fieldid={item.itemKey}
              inline={true}
              //showMast={true}
              labelXs={2} labelSm={2} labelMd={2}
              xs={2} md={2} sm={2}
              labelName={item.itemName}
              className={item.itemKey === "accountingbookValue" ? "m-form-item" : ""}
              isRequire={true}
              method="change"
            >
              <Tooltip trigger="hover" placement="top" inverse overlay={defValue}>
                <span>
                  <InputItem
                  fieldid={item.itemKey}
                    //isViewMode
                    disabled={true}
                    name={item.itemKey}
                    type="customer"
                    defaultValue={defValue}
                  />
                </span>
              </Tooltip>
            </FormItem>
          );
        default:
          break;
      }
    });
  };
  componentWillMount() {

    let callback = (json) => {
      this.setState({ json: json }, () => {
        initTemplate.call(this, this.props);
      })
    }
    getMultiLang({ moduleId: ['20021VYBAL', 'publiccommon'], domainName: 'gl', currentLocale: 'simpchn', callback });
    }
  //刷新
  handleRefresh = () => {
    let { resourveQuery } = this.state;
    this.handleQueryClick(resourveQuery);

  };
  //获取默认会计期间 会计期间方案
  getDefaultYearmouth = (pk_accountingbook, modalDefaultValue) => {
    let self = this;
    let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
    let pk_accpont = { "pk_accountingbook": pk_accountingbook }
    ajax({
      url: url,
      data: pk_accpont,
      async: false,
      success: function (response) {
        const { success } = response;
        //渲染已有账表数据遮罩
        if (success) {
          if (response.data) {
            modalDefaultValue.bizPeriod = response.data.bizPeriod;
            modalDefaultValue.isShowUnit = response.data.isShowUnit;
            modalDefaultValue.isOpenUpBusiUnit = 'Y';
            modalDefaultValue.pk_accperiodscheme = response.data.pk_accperiodscheme;
            modalDefaultValue.begindate = response.data.begindate;
            modalDefaultValue.enddate = response.data.enddate;
            modalDefaultValue.bizDate = response.data.bizDate;
            modalDefaultValue.yearbegindate = response.data.yearbegindate;
            modalDefaultValue.pk_currency.display = response.data.currinfo.display;
            modalDefaultValue.pk_currency.value = response.data.currinfo.value;
            modalDefaultValue.pk_org = response.data.unit.value;
          }
          self.setState({
            modalDefaultValue,
            dataout: self.state.dataout
          })
        }
      }
    });
  }
  componentDidMount() {
    let { modalDefaultValue } = this.state;
    let appcode = this.props.getSearchParam("c");
    getDefaultAccountBook(appcode).then((defaultAccouontBook) => {
      if (defaultAccouontBook.value) {
        modalDefaultValue.pk_accountingbook = defaultAccouontBook;
        this.getDefaultYearmouth(defaultAccouontBook.value, modalDefaultValue);
      }
    })
  }


  //获取当前选中行数据
  getSelectRowData = () => {
    let selectRowData = this.refs.balanceTable.getRowRecord();
    return selectRowData;
  }
  //核销情况
  handleVerifyDetails = () => {
    let { querycondition } = this.state;
    let rowDatas = this.getSelectRowData();
    let gziptools = new gzip();
    if (!rowDatas || rowDatas[0] == null || rowDatas.length == 0) {
      toast({ content: this.state.json['20021VYBAL-000033'], color: 'warning' });/* 国际化处理： 请先选中一行数据再进行联查*/
      return false;
    }
    rowDatas.map((item, index) => {
      if (item) {
        if (item.key == "m_Explanation" && (item.title == this.state.json['20021VYBAL-000034'] || item.title == this.state.json['20021VYBAL-000035'] || item.title == this.state.json['20021VYBAL-000036'])) {//总计/* 国际化处理： 总计,业务单元小计,科目小计*/
          toast({ content: this.state.json['20021VYBAL-000037'], color: 'warning' });//选中行带有“总计”的数据不能做联查/* 国际化处理： 选中行带有“总计/业务单元小计/科目小计”的数据不能做联查*/
          return false;
        }
      }
    })
    let link = rowDatas[0].link;
    this.props.openTo('/gl/verify/verifyDetails/listHandsontable/index.html',
      {
        condition: gziptools.zip(JSON.stringify(querycondition)),
        link: gziptools.zip(JSON.stringify(link)),
        appcode: '20021VYQRY',
        pagecode: '20021VYQRY'

      }
    )
  };
  //保存列宽
  handleSaveColwidth = () => {
    let info = this.refs.balanceTable.getReportInfo();
    let { json } = this.state
    let callBack = this.refs.balanceTable.resetWidths
    reportSaveWidths(gl_pkreport_verifyBalances, info.colWidths, json, callBack);
  }
  //打印
  showPrintModal() {
    this.setState({
      showPrintModal: true
    })
  }
  handlePrint(data) {
    let printUrl = '/nccloud/gl/verify/verbalanceprint.do'
    let { querycondition, appcode } = this.state
    let { ctemplate, nodekey } = data
    querycondition.queryvo = data
    this.setState({
      ctemplate: ctemplate,
      nodekey: nodekey
    })
    printRequire(printUrl, appcode, nodekey, ctemplate, querycondition)
    this.setState({
      showPrintModal: false
    });
  }
  showOutputModal() {
    let outputUrl = '/nccloud/gl/verify/verbalanceoutput.do'
    let { appcode, nodekey, ctemplate, querycondition } = this.state
    mouldOutput(outputUrl, appcode, nodekey, ctemplate, querycondition)
  }
  handleOutput() {
  }
  //选中某一行

  getRow = (expanded, record) => {
    let copyData = deepClone(expanded);
    //表体数据增删获取key
    let { getrowkey, selectedRecord, selectedData } = this.state;
    selectedData = copyData;
    let getrow = expanded.key;
    this.setState({
      getrowkey: getrow,
      selectedRecord: record,
      selectedData
    });
  };
  //handlesonTable 单元格点击事件

  handlesonTableClick(e, coords, td, props) {
    let { dataLen, dataSources } = this.state
    let rowDatas = this.getSelectRowData();//获取当前行数据
    if (rowDatas && rowDatas[0]) {//数据无对账维度时，对账维度按钮不可用
      rowDatas.map((item, index) => {
        if (item) {
          if (item.key.indexOf("m_Explanation") != -1) {//摘要列
            if (item.title == this.state.json['20021VYBAL-000063'] || item.title == this.state.json['20021VYBAL-000064'] || item.title == this.state.json['20021VYBAL-000065'] || item.title == this.state.json['20021VYBAL-000066'] || item.title == this.state.json['20021VYBAL-000067']) {//总计
              this.props.button.setButtonDisabled(['detail'], true);
            } else {
              if (item.title) {
                this.props.button.setButtonDisabled(['detail', 'switch'], false);
              } else {
                this.props.button.setButtonDisabled(['detail'], true);
              }
            }
          }

        }
      })
    } else {
      this.props.button.setButtonDisabled(['detail'], true);
    }
    // if(dataSources[coords.row-1].m_Explanation==this.state.json['20021VYBAL-000063'] || dataSources[coords.row-1].m_Explanation==this.state.json['20021VYBAL-000064'] || coords.row==dataLen || dataSources[coords.row-1].m_Explanation==this.state.json['20021VYBAL-000065'] || dataSources[coords.row-1].m_Explanation==this.state.json['20021VYBAL-000066']){
    //   this.props.button.setButtonDisabled(['detail'], true)
    // }else{
    //   this.props.button.setButtonDisabled(['detail','switch'], false)
    // }
  }
  render() {
    let { columnsVerify, verifyBalancesData, listItem, showType, modalDefaultValue } = this.state;
    const loadQuery = [
      {
        itemName: this.state.json['20021VYBAL-000000'],/* 国际化处理： 核算账簿*/
        itemType: "refer",
        itemKey: "pk_accountingbook", isMustItem: true,
        config: { refCode: "uapbd/refer/org/AccountBookTreeRef" }
      },
      {
        itemName: this.state.json['20021VYBAL-000001'],/* 国际化处理： 业务单元*/
        itemType: "refer",
        itemKey: "pk_units", isMustItem: false,
        config: { refCode: "uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef" }
      },
      {
        itemName: this.state.json['20021VYBAL-000002'],/* 国际化处理： 核销科目*/
        itemType: "refer",
        itemKey: "pk_accasoa", isMustItem: true,
        config: { refCode: "uapbd/refer/fiacc/AccountDefaultGridTreeRef" }
      },
      {
        itemName: this.state.json['20021VYBAL-000003'],/* 国际化处理： 日期范围*/
        itemType: "Dbdate",
        itemKey: ["beginDate", "endDate"]
      }, //begin_date,end_date（必传）
      {
        itemName: this.state.json['20021VYBAL-000004'],/* 国际化处理： 核销范围*/
        itemType: "radio",
        itemKey: "rangeType",
        itemChild: [
          {
            label: this.state.json['20021VYBAL-000005'],/* 国际化处理： 最终核销日期*/
            value: "finaldate"
          },
          {
            label: this.state.json['20021VYBAL-000006'],/* 国际化处理： 截止日期*/
            value: "enddate"
          }
        ]
      },
      {
        itemName: this.state.json['20021VYBAL-000007'],/* 国际化处理： 分析日期*/
        itemType: "radio",
        itemKey: "dateType",
        itemChild: [
          {
            label: this.state.json['20021VYBAL-000008'],/* 国际化处理： 凭证日期*/
            value: "prepareddate"
          },
          {
            label: this.state.json['20021VYBAL-000009'],/* 国际化处理： 业务日期*/
            value: "Businessdate"
          }
        ]
      },
      {
        itemName: this.state.json['20021VYBAL-000010'],/* 国际化处理： 币种*/
        itemType: "refer",
        itemKey: "pk_currency", isMustItem: true,
        config: { refCode: "uapbd/refer/pubinfo/CurrtypeGridRef" }
      },
      {
        itemName: this.state.json['20021VYBAL-000011'],/* 国际化处理： 方向*/
        itemType: "radio",
        itemKey: "direct",
        itemChild: [
          {
            label: this.state.json['20021VYBAL-000012'],/* 国际化处理： 借*/
            value: "1"
          },
          {
            label: this.state.json['20021VYBAL-000013'],/* 国际化处理： 贷*/
            value: "-1"
          },
          {
            label: this.state.json['20021VYBAL-000014'],/* 国际化处理： 双向*/
            value: "0"
          }
        ]
      },
      {
        itemName: this.state.json['20021VYBAL-000015'],/* 国际化处理： 账龄*/
        itemType: "DbtextInput",
        itemKey: ["beginAge", "endAge"]
      },
      {
        itemName: this.state.json['20021VYBAL-000016'],/* 国际化处理： 核销号*/
        itemType: "DbtextInput",
        itemKey: ["beginVerifyNo", "endVerifyNo"]
      },
      {
        itemName: this.state.json['20021VYBAL-000017'],/* 国际化处理： 包含未记账凭证*/
        itemType: "checkbox",
        itemKey: "hasTally",
        itemChild: [
          {
            label: this.state.json['20021VYBAL-000017'],/* 国际化处理： 包含未记账凭证*/
            checked: false
          }
        ]
      },
      {
        itemName: this.state.json['20021VYBAL-000018'],/* 国际化处理： 包含已两清*/
        itemType: "checkbox",
        itemKey: "equal",
        itemChild: [
          {
            label: this.state.json['20021VYBAL-000018'],/* 国际化处理： 包含已两清*/
            checked: false
          }
        ]
      },
      {
        itemName: this.state.json['20021VYBAL-000019'],/* 国际化处理： 按核销号小计*/
        itemType: "checkbox",
        itemKey: "verifyNoSum",
        itemChild: [
          {
            label: this.state.json['20021VYBAL-000019'],/* 国际化处理： 按核销号小计*/
            checked: false
          }
        ]
      }
    ];
    const Defaultcolumns = [
      //借方
      {
        title: this.state.json['20021VYBAL-000024'],/* 国际化处理： 科目编码*/
        dataIndex: "m_sSubjCode",
        key: "m_sSubjCode",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000025'],/* 国际化处理： 科目名称*/
        dataIndex: "m_sSubjName",
        key: "m_sSubjName",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000008'],/* 国际化处理： 凭证日期*/
        dataIndex: "m_prepareddate",
        key: "m_prepareddate",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000009'],/* 国际化处理： 业务日期*/
        dataIndex: "m_Businessdate",
        key: "m_Businessdate",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000026'],/* 国际化处理： 凭证号*/
        dataIndex: "m_VoucherNo",
        key: "m_VoucherNo",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000027'],/* 国际化处理： 摘要*/
        dataIndex: "m_explanation",
        key: "m_explanation",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000016'],/* 国际化处理： 核销号*/
        dataIndex: "m_VerifyNo",
        key: "m_VerifyNo",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000028'],/* 国际化处理： 原币*/
        dataIndex: "m_debitamount",
        key: "m_debitamount",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000029'],/* 国际化处理： 本币*/
        dataIndex: "m_localdebitamount",
        key: "m_localdebitamount",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000030'],/* 国际化处理： 原币余额*/
        dataIndex: "m_Balancedebitamount",
        key: "m_Balancedebitamount",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000031'],/* 国际化处理： 本币余额*/
        dataIndex: "m_Balancelocaldebitamount",
        key: "m_Balancelocaldebitamount",
        width: 100
      },
      {
        title: this.state.json['20021VYBAL-000015'],/* 国际化处理： 账龄*/
        dataIndex: "m_Accountage",
        key: "m_Accountage",
        width: 100
      }
    ];
    if (columnsVerify.length == 0) {
      columnsVerify = Defaultcolumns;
    }
    return (
      <div className="manageReportContainer verifyBalances nc-bill-list" id="verifyBalances">
			<HeaderArea
				title={this.state.json['20021VYBAL-000040']}/* 国际化处理： 往来核销余额表*/

				btnContent={
					this.props.button.createButtonApp({
						area: 'button_area',
						onButtonClick: buttonClick.bind(this),
						// popContainer: document.querySelector('.header-button-area')
					})
				}
			/>
        <QueryModal
          loadData={loadQuery}
          modalDefaultValue={modalDefaultValue}
          showOrHide={this.state.showModal}
          onConfirm={this.handleQueryClick.bind(this)}
          handleClose={this.handleClose.bind(this)}
        />
        {/* //表头 */}
        <div className="Mende-ncfrom-box-min nc-bill-search-area nc-theme-form-label-c nc-theme-area-bgc">
			<div fieldid="verifyBalance_form-area">
				<NCForm
					useRow={true}
					submitAreaClassName="classArea"
					showSubmit={false}
				>
					{this.getHeadData(listItem)}
				</NCForm>
          	</div>
        </div>
        <div fieldid="verifyDetails_report" className='report-table-area'>
          <SimpleTable
            ref="balanceTable"
            onCellMouseDown={(e, coords, td) => {
              this.handlesonTableClick(e, coords, td);
              this.rowBackgroundColor(e, coords, td)
            }}
            data={this.state.dataout}
          />
        </div>
        <PrintModal
          noRadio={true}
          noCheckBox={true}
          appcode={this.state.appcode}
          visible={this.state.showPrintModal}
          handlePrint={this.handlePrint.bind(this)}
          handleCancel={() => {
            this.setState({
              showPrintModal: false
            })
          }}
        />
        <PrintOutput
          ref='printOutput'
          url='/nccloud/gl/verify/verbalanceoutput.do'
          data={this.state.outputData}
          callback={this.handleOutput.bind(this)}
        />
      </div>
    );
  }
}


VerifyBalances = createPage({})(VerifyBalances);
ReactDOM.render(<VerifyBalances />, document.querySelector("#app"));
