import asyncComponent from 'pubs/basecomponent/lazyload';
//import Home from 'pages/so/home';
// import Verify from 'gl_front/verify/verify/vrify/exportVrify';
// import VrifyHistory from 'gl_front/verify/historyquery/list';

const Verify = asyncComponent(() => import(/* webpackChunkName: "SoEdit" */'./verify/vrify/exportVrify.js'));
const VrifyHistory = asyncComponent(() => import(/* webpackChunkName: "AppHome" */'./historyquery/list/index.js'));

const routes = [
  {
    path: '/',
    component: Verify,
    exact: true,
  },
  {
    path: '/VrifyHistory',
    component: VrifyHistory,
  }
];

export default routes;
