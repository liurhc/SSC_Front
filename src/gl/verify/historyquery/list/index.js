import React, { Component } from "react";
import { hashHistory, Redirect, Link,toast } from "react-router";
import { high, base, ajax, withShell, deepClone, createPage,getMultiLang,createPageIcon  } from "nc-lightapp-front";
import filterSelectedData from './events/filterSelectedData';
const {
  NCFormControl: FormControl,
  NCDatePicker: DatePicker,
  NCButtonGroup: ButtonGroup,
  NCButton: Button,
  NCRadio: Radio,
  NCBreadcrumb: Breadcrumb,
  NCRow: Row,
  NCCol: Col,
  NCTree: Tree,
  NCMessage: Message,
  NCIcon: Icon,
  NCLoading: Loading,
  NCTable: Table,
  NCSelect: Select,
  NCCheckbox: Checkbox,
  NCNumber,
  AutoComplete,
  NCDropdown: Dropdown,
  NCPanel: Panel,NCAffix,NCDiv
} = base;
import {
  CheckboxItem,
  RadioItem,
  TextAreaItem,
  ReferItem,
  SelectItem,
  InputItem,
  DateTimePickerItem
} from "../../../public/components/FormItems";
const { ColumnGroup, Column } = Table;
import HeaderArea from '../../../public/components/HeaderArea';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import SearchModal from "./searchModal/searchModal";
import PrintModal from '../../../public/components/printModal';
const { PrintOutput } = high;
import { printRequire, mouldOutput } from '../../../public/components/printModal/events';
import {getTableHeight } from '../../../public/common/method.js';
import "./index.less";
import {
  handleQueryClick,
  handleUnverify,
  handleDetail,
  handleSum
} from "./events/index.js";
import { buttonClick, initTemplate} from './events';

const defaultProps12 = {
  prefixCls: "bee-table",
  multiSelect: {
    type: "checkbox",
    param: "key"
  }
};

class HistoryQuery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json:{},
      appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
      showPrintModal: false,
      outputData: {},
      isQueryShow: false,
      showModal: false,
      queryStyle: false,
      creditQueryStyle: true,
      debitQueryStyle: false,
      defaultStatu: true, //false代表页面没数据，true为有数据
      checkedAll: false, //全选
      checkedAllSum: false, //汇总全选
      checkedAllDetail: false, //明细全选
      checkedArrayDetail: [
        //明细
        //false,
      ],
      checkedArraySum: [
        //汇总
        //false,
      ],
      modalDefaultValue:{
        pk_org:'',//账簿对应的组织
        bizDate:'',//业务日期
        yearbegindate:'',//年初日期
        begindate:'',
        enddate:'',
        bizPeriod:'',//账簿对应的会计期间
        isShowUnit:false,//是否显示业务单元
        isOpenUpBusiUnit:'',//
        pk_accperiodscheme:'',//会计期间方案
        pk_accountingbook:{display:'',value:''},
        pk_currency:{display:'',value:''},
        appcode:this.props.getSearchParam("c")
      },//模态框里默认数据
      historyDataArr: [], //明细数据
      historySumDataArr: [], //汇总数据
      debitRate: "1", //汇率
      queryDataObj:{},
      detailSelectedData: [], //明细选中数据
      sumSelectedData: [], //汇总选中数据
      resourceData_sum: [], //查询出来的历史数据
      resourceData_details: [],
      sumOrDetail: "sum" //汇总详细标记:
    };
  }

  onInputChange = (index, key) => {
    return value => {
      const { historySumDataArr } = this.state;
      const dataSource = [...historySumDataArr];
      dataSource[index][key] = value;
      this.setState({ historySumDataArr });
    };
  };

  //明细数据组装
  renderColumnsMultiSelect(columns) {
    const {
      historyDataArr,
      checkedArrayDetail,
      checkedAllDetail,
      creditQueryStyle
    } = this.state;
    let select_column = {};
    let indeterminate_bool = false;

    let defaultColumns = [
      {
        title: <div fieldid="firstcol" className='checkbox-mergecells'>{(
          <Checkbox
            className="table-checkbox"
            //checked={this.state.creditQueryStyle}
            checked={this.state.checkedAllDetail}
            indeterminate={indeterminate_bool && !this.state.checkedAllDetail}
            onChange={this.onAllCheckChangeDebit.bind(this, "details")}
          />
          )}</div>,
        key: "checkbox",
        dataIndex: "checkbox",
        width: 73,
        render: (text, record, index) => {
          return (
            <div fieldid="firstcol">
            <Checkbox
              className="table-checkbox"
              // checked={this.state.creditQueryStyle}
              checked={this.state.checkedArrayDetail[index]}
              onChange={this.onCheckboxChangeDebit.bind(
                this,
                "details",
                text,
                record,
                index
              )}
            />
            </div>
          );
        }
      }
    ];
    columns = defaultColumns.concat(columns);
    return columns;
  }
  //汇总,明细全选功能
  onAllCheckChangeDebit = type => {
    let self = this;
    //let checkedArraySum = [];
    let listData = self.state.historySumDataArr.concat();
    let selIds = [];
    let {
      checkedArraySum,
      checkedArrayDetail,
      checkedAllSum,
      checkedAllDetail,
      historySumDataArr,
      historyDataArr,
      sumSelectedData
    } = this.state;
    // debitSelectedData=historySumDataArr;
    if (type == "sum") {
      for (var i = 0; i < checkedArraySum.length; i++) {
        checkedArraySum[i] = !self.state.checkedAllSum;
      }
      //let allChickedData = this.handleAllChicked();
      self.setState({
        checkedAllSum: !self.state.checkedAllSum,
        checkedArraySum
        //historySumDataArr:allChickedData,
        //   debitSelectedData
      });
      //this.handleAllChecked();
    } else if (type == "details") {
      for (var i = 0; i < checkedArrayDetail.length; i++) {
        checkedArrayDetail[i] = !self.state.checkedAllDetail;
      }
      //let allChickedData = this.handleAllChicked();
      self.setState({
        checkedAllDetail: !self.state.checkedAllDetail,
        checkedArrayDetail
        //	historyDataArr:allChickedData,
        //   debitSelectedData
      });
      //this.handleAllChecked();
    }
  };

  // 汇总/明细单选功能
  onCheckboxChangeDebit = (type, text, record, index) => {
    let self = this;
    let allFlag = false;
    if (type == "sum") {
      let checkedArraySum = self.state.checkedArraySum.concat();
      checkedArraySum[index] = !self.state.checkedArraySum[index];
      for (var i = 0; i < self.state.checkedArraySum.length; i++) {
        if (!checkedArraySum[i]) {
          allFlag = false;
          break;
        } else {
          allFlag = true;
        }
      }
      // 接收点击单选按钮传过来的数据，对historySumDataArr进行修改
      // const { historySumDataArr } = this.state;
      // const o = this.handleCancel(index,checkedArraySum[index]);
      // self.setState({
      // 	historySumDataArr:o
      // })
      self.setState({
        checkedAllSum: allFlag,
        checkedArraySum: checkedArraySum
      });
      //this.handleChecked(index);
    } else if (type == "details") {
      let checkedArrayDetail = self.state.checkedArrayDetail.concat();
      checkedArrayDetail[index] = !self.state.checkedArrayDetail[index];
      for (var i = 0; i < self.state.checkedArrayDetail.length; i++) {
        if (!checkedArrayDetail[i]) {
          allFlag = false;
          break;
        } else {
          allFlag = true;
        }
      }
      // 接收点击单选按钮传过来的数据，对historyDataArr进行修改
      //const { historyDataArr } = this.state;
      // const o = this.handleCancel(index,checkedArrayDetail[index]);
      // self.setState({
      // 	historyDataArr:o
      // })
      self.setState({
        checkedAllDetail: allFlag,
        checkedArrayDetail: checkedArrayDetail
      });
      //this.handleChecked(index);
    }
  };
  //汇总数据组装
  renderColumnsMultiSelectDebit(columns) {
    const { historySumDataArr, checkedArraySum } = this.state;
    const { multiSelect } = this.props;
    let select_column = {};
    let indeterminate_bool = false;

    if (multiSelect && multiSelect.type === "checkbox") {
      let i = checkedArraySum.length;
      while (i--) {
        if (checkedArraySum[i]) {
          indeterminate_bool = true;
          break;
        }
      }
      let defaultColumns = [
        {
          title: <div fieldid="firstcol" className='checkbox-mergecells'>{(
            <Checkbox
              className="table-checkbox"
              checked={this.state.checkedAllSum}
              indeterminate={indeterminate_bool && !this.state.checkedAllSum}
              onChange={this.onAllCheckChangeDebit.bind(this, "sum")}
            />
            )}</div>,
          key: "checkbox",
          dataIndex: "checkbox",
          width: 73,
          render: (text, record, index) => {
            return (
              <div fieldid="firstcol">
              <Checkbox
                className="table-checkbox"
                checked={this.state.checkedArraySum[index]}
                onChange={this.onCheckboxChangeDebit.bind(
                  this,
                  "sum",
                  text,
                  record,
                  index
                )}
              />
              </div>
            );
          }
        }
      ];
      columns = defaultColumns.concat(columns);
    }
    return columns;
  }
  componentWillMount() {

    let callback= (json) =>{
      this.setState({
        json:json,
        queryDataObj: {
          pk_accountingbook: "1001A3100000000008MT",
          pk_units: "",
          pk_accasoa: "1001A3100000000006Z2",
          begin_date: "2017-05-02",
          end_date: "2018-05-17",
          mny_begin: "",
          mny_end: "",
          pk_currency: "1002Z0100000000001K1",
          verifyno: "",
          digest: "",
          acccode: "1122",
          dateType: "opdate",
          redBlue: "N",
          ass: [
            {
              key: 0,
              checktypecode: "0004",
              checktypename: this.state.json['20020VRIFYHISTORY-000030'],/* 国际化处理： 客商*/
              pk_Checktype: "0001Z0100000000005CV",
              // "checkvaluecode": "",
              // "checkvaluename": "",
              // "pk_Checkvalue": "",
              refCode: "uap.custsupplier",
              pk_accountingbook: "1001A3100000000008MT"
            },
            {
              key: 1,
              checktypecode: "0001",
              checktypename: this.state.json['20020VRIFYHISTORY-000031'],/* 国际化处理： 部门*/
              pk_Checktype: "0001Z0100000000005CS",
              // "checkvaluecode": "",
              // "checkvaluename": "",
              // "pk_Checkvalue": "",
              refCode: "uap.dept",
              pk_accountingbook: "1001A3100000000008MT"
            }
          ]
        }
      },()=>{
        initTemplate.call(this, this.props);
                // this.loadDept();
                //this.tableDefaultData=deepClone(tableDefaultData); 
      })
    }
    getMultiLang({moduleId:'20020VRIFYHISTORY',domainName:'gl',currentLocale:'simpchn',callback});
  }
  //查询
  handleClick = () => {
    this.setState({
      isQueryShow: !this.state.isQueryShow,
      showModal: !this.state.showModal,
      queryStyle: !this.state.queryStyle
    });
  };
  //打印
  showPrintModal() {
    this.setState({
      showPrintModal: true
    })
  }
  handlePrint(data) {
    let printUrl = '/nccloud/gl/verify/verhistoryprint.do'
    let { queryDataObj, appcode,sumOrDetail} = this.state
    let { ctemplate, nodekey } = data
    queryDataObj.queryvo = data
    queryDataObj.selectedSumBatchid=[];
    queryDataObj.selectedDetailBatchid=[];
    if(sumOrDetail=='sum'){
      let m_Batchid=[];//批次
      let cerrentBatchidDSata=[];//过来当前批次选中的数据
      let sumSelectedData=filterSelectedData(this.state,'sum');//获取汇总选中的数据批次
      if(sumSelectedData.length>0){
        sumSelectedData.map((item,i)=>{
              if(m_Batchid.indexOf(item.m_Batchid)==-1){
                  m_Batchid.push(item.m_Batchid.value);
              }
          })
      }

      queryDataObj.selectedSumBatchid=m_Batchid;
      queryDataObj.issum='Y';
      this.setState({
          ctemplate: ctemplate,
          nodekey: nodekey
      })
      printRequire(printUrl, appcode, nodekey, ctemplate, queryDataObj)
     this.setState({
        showPrintModal: false
      }); 
         
     

    }else if(sumOrDetail=='details'){
      let m_Batchid=[];//批次
      let m_detaillogid=[];//批次
      let cerrentBatchidDSata=[];//过来当前批次选中的数据
      let sumSelectedData=filterSelectedData(this.state,'sum');//获取汇总选中的数据批次
      if(sumSelectedData.length>0){
        sumSelectedData.map((item,i)=>{
              if(m_Batchid.indexOf(item.m_Batchid)==-1){
                  m_Batchid.push(item.m_Batchid.value);
              }
          })
      }
      let detailSelectedData=filterSelectedData(this.state,'details');//获取明细选中的数据批次
      if(detailSelectedData.length>0){
          detailSelectedData.map((item,i)=>{
              if(m_detaillogid.indexOf(item.m_Logid.value)==-1){
                m_detaillogid.push(item.m_Logid.value);
              }
          })
      }
      queryDataObj.selectedSumBatchid=m_Batchid;
      queryDataObj.selectedDetailBatchid=m_detaillogid;     
      queryDataObj.issum='N';
      this.setState({
          ctemplate: ctemplate,
          nodekey: nodekey
      })
      printRequire(printUrl, appcode, nodekey, ctemplate, queryDataObj)
     this.setState({
        showPrintModal: false
      }); 
      
      
      
    }
    
   
  }
  showOutputModal() {
    // this.refs.printOutput.open()
     let outputUrl = '/nccloud/gl/verify/verhistoryoutput.do'
          // let { appcode, nodekey, ctemplate, queryDataObj } = this.state
          // mouldOutput(outputUrl, appcode, nodekey, ctemplate, queryDataObj)
    // let outputData = mouldOutput(appcode, nodekey, ctemplate, queryDataObj)
    // this.setState({
    //     outputData: outputData
    // })


    let { queryDataObj, appcode,sumOrDetail,nodekey,ctemplate} = this.state
   // let { ctemplate, nodekey } = data
    //queryDataObj.queryvo = data
    queryDataObj.selectedSumBatchid=[];
    queryDataObj.selectedDetailBatchid=[];
    if(sumOrDetail=='sum'){
      let m_Batchid=[];//批次
      let cerrentBatchidDSata=[];//过来当前批次选中的数据
      let sumSelectedData=filterSelectedData(this.state,'sum');//获取汇总选中的数据批次
      if(sumSelectedData.length>0){
        sumSelectedData.map((item,i)=>{
              if(m_Batchid.indexOf(item.m_Batchid)==-1){
                  m_Batchid.push(item.m_Batchid.value);
              }
          })
      }

      queryDataObj.selectedSumBatchid=m_Batchid;
      queryDataObj.issum='Y';
      this.setState({
          ctemplate: ctemplate,
          nodekey: nodekey
      })
      mouldOutput(outputUrl, appcode, nodekey, ctemplate, queryDataObj)
         
     

    }else if(sumOrDetail=='details'){
      let m_Batchid=[];//批次
      let m_detaillogid=[];//批次
      let cerrentBatchidDSata=[];//过来当前批次选中的数据
      let sumSelectedData=filterSelectedData(this.state,'sum');//获取汇总选中的数据批次
      if(sumSelectedData.length>0){
        sumSelectedData.map((item,i)=>{
              if(m_Batchid.indexOf(item.m_Batchid)==-1){
                  m_Batchid.push(item.m_Batchid.value);
              }
          })
      }
      let detailSelectedData=filterSelectedData(this.state,'details');//获取明细选中的数据批次
      if(detailSelectedData.length>0){
          detailSelectedData.map((item,i)=>{
              if(m_detaillogid.indexOf(item.m_Logid.value)==-1){
                m_detaillogid.push(item.m_Logid.value);
              }
          })
      }
      queryDataObj.selectedSumBatchid=m_Batchid;
      queryDataObj.selectedDetailBatchid=m_detaillogid;     
      queryDataObj.issum='N';
      this.setState({
          ctemplate: ctemplate,
          nodekey: nodekey
      })
      mouldOutput(outputUrl, appcode, nodekey, ctemplate, queryDataObj)
      
      
    }



  }
  handleOutput() {
     
  }
  //返回
  handleBack = () => {
    // this.props.history.go(-1);
    this.props.pushTo("/");
    // window.history.go(-1);
    //window.location.href='../../verify/vrify/index.html';
  };
  //获取默认会计期间 会计期间方案
  getDefaultYearmouth=(pk_accountingbook,modalDefaultValue)=>{
    let self=this;
    // let {modalDefaultValue}=self.state;
    let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
    let pk_accpont = {"pk_accountingbook":pk_accountingbook}
    ajax({
        url:url,
        data:pk_accpont,
        async:false,
        success: function(response){
            const { success } = response;
            //渲染已有账表数据遮罩
            if (success) {
                if(response.data){
                    modalDefaultValue.bizPeriod=response.data.bizPeriod;
                    modalDefaultValue.isShowUnit=response.data.isShowUnit;
                    modalDefaultValue.isOpenUpBusiUnit='Y';
                    modalDefaultValue.pk_accperiodscheme=response.data.pk_accperiodscheme;
                    modalDefaultValue.begindate=response.data.begindate;
                    modalDefaultValue.enddate=response.data.enddate;
                    modalDefaultValue.bizDate=response.data.bizDate;
                    modalDefaultValue.yearbegindate=response.data.yearbegindate;
                    modalDefaultValue.pk_currency.display= response.data.currinfo.display;
                    modalDefaultValue.pk_currency.value = response.data.currinfo.value;
                    modalDefaultValue.pk_org = response.data.unit.value;
                }
                self.setState({
                    modalDefaultValue,
                    dataout: self.state.dataout
                })
            }   
        }
    });
  }
  componentDidMount() {
    let {modalDefaultValue}=this.state;
    let appcode=this.props.getSearchParam("c");
        // let defaultAccouontBook=getDefaultAccountBook(appcode);
    getDefaultAccountBook(appcode).then((defaultAccouontBook)=>{        
      if(defaultAccouontBook.value){
          modalDefaultValue.pk_accountingbook=defaultAccouontBook;
          this.getDefaultYearmouth(defaultAccouontBook.value,modalDefaultValue);
      }       
      // this.setState({
      //   // modalDefaultValue,
      //   dataout: this.state.dataout
      // });
    })
  }

  componentWillUpdate() {
    let {sumOrDetail} = this.state

  }

  // 单选 控制 boxChicked状态
  handleChecked(index) {
    const { checkedArraySum, boxChicked } = this.state;
    var count = 0,
      len = checkedArraySum.length;
    for (var i = 0; i < len; i++) {
      if (checkedArraySum[i] != false) {
        // this.setState({
        // 	boxChicked: true
        // })
      } else {
        count++;
      }
      if (count == len) {
        this.setState({
          boxChicked: false
        });
      } else {
        this.setState({
          boxChicked: true
        });
      }
    }
  }

  // 汇总全选 控制 boxChicked状态
  handleAllChecked() {
    const { checkedArraySum, boxChicked } = this.state;
    for (var i = 0; i < checkedArraySum.length; i++) {
      if (checkedArraySum[i] == true) {
        this.setState({
          boxChicked: true
        });
      } else {
        this.setState({
          boxChicked: false
        });
      }
    }
  }

  // 原币 与组织本币之间的换算
  handleConversion(y) {
    var z = 1;
    const { debitRate } = this.state;
    if (y == null) {
      return null;
    } else {
      z = y * debitRate;
      return z;
    }
  }

  // 点击单个单选按钮 获取修改后的数据
  handleCancel(index, bool) {
    const { historySumDataArr } = this.state;
    let benY = 0,
      benZ = 0,
      weiY = 0,
      weiZ = 0,
      obj = {};
    obj = {
      m_dDebit_Money_Y: historySumDataArr[index].m_Balancedebitamount,
      m_Balancedebitamount: 0
    };
    if (bool == true) {
      //m_dDebit_Money_Y ,m_dDebit_Money_B,m_Balancedebitamount,m_Balancelocaldebitamount,m_Balancedebitamount
      historySumDataArr[index].m_dDebit_Money_Y = obj.m_dDebit_Money_Y;
      historySumDataArr[index].m_dDebit_Money_B = this.handleConversion(
        historySumDataArr[index].m_dDebit_Money_Y
      );
      historySumDataArr[index].m_Balancedebitamount = obj.m_Balancedebitamount;
      historySumDataArr[index].m_Balancelocaldebitamount =
        obj.m_Balancedebitamount;
      return historySumDataArr;
    } else {
      historySumDataArr[index].m_dDebit_Money_Y = obj.m_dDebit_Money_Y;
      historySumDataArr[index].m_dDebit_Money_B = this.handleConversion(
        historySumDataArr[index].m_dDebit_Money_Y
      );
      historySumDataArr[index].m_Balancedebitamount =
        historySumDataArr[index].empty_m_Balancedebitamount;
      historySumDataArr[index].m_Balancelocaldebitamount =
        historySumDataArr[index].empty_m_Balancedebitamount;
      return historySumDataArr;
    }
  }

  // 点击全选按钮 获取修改后的数据
  handleAllChicked() {
    const { historySumDataArr, checkedArraySum } = this.state;
    var obj = {},
      brr = [];
    for (var i = 0; i < historySumDataArr.length; i++) {
      brr.push({
        m_dDebit_Money_Y: historySumDataArr[i].m_Balancedebitamount,
        m_Balancedebitamount: 0
      });
    }

    var count = 0,
      len = checkedArraySum.length;
    for (var i = 0; i < len; i++) {
      if (checkedArraySum[i] == true) {
        count++;
      }
    }

    if (len == count) {
      //m_dDebit_Money_Y ,m_dDebit_Money_B,m_Balancedebitamount,m_Balancelocaldebitamount,m_Balancedebitamount
      for (var j = 0; j < historySumDataArr.length; j++) {
        historySumDataArr[j].m_dDebit_Money_Y = brr[j].m_dDebit_Money_Y;
        historySumDataArr[j].m_dDebit_Money_B = this.handleConversion(
          historySumDataArr[j].m_dDebit_Money_Y
        );
        historySumDataArr[j].m_Balancedebitamount = brr[j].m_Balancedebitamount;
        historySumDataArr[j].m_Balancelocaldebitamount =
          brr[j].m_Balancedebitamount;
      }
      return historySumDataArr;
    } else {
      for (var j = 0; j < historySumDataArr.length; j++) {
        historySumDataArr[j].m_dDebit_Money_Y = brr[j].m_dDebit_Money_Y;
        historySumDataArr[j].m_dDebit_Money_B = this.handleConversion(
          historySumDataArr[j].m_dDebit_Money_Y
        );
        historySumDataArr[j].m_Balancedebitamount =
          historySumDataArr[j].empty_m_Balancedebitamount;
        historySumDataArr[j].m_Balancelocaldebitamount =
          historySumDataArr[j].empty_m_Balancedebitamount;
      }
      return historySumDataArr;
    }
  }

  render() {
    let {
      isQueryShow,
      queryStyle,
      historyDataArr,
      historySumDataArr,
      checkedArraySum,
      sumOrDetail,
      showModal,
      defaultStatu,modalDefaultValue
    } = this.state;
    // let columnsSum = this.renderColumnsMultiSelect(columns_sum);
    const columns_sum = [
      //汇总
      {
      title: (<div fieldid="m_oprDate" className="mergecells">{this.state.json['20020VRIFYHISTORY-000018']}</div>),/* 国际化处理： 核销日期*/
        dataIndex: "m_oprDate",
        key: "m_oprDate",
        width: 200,
        render:(text, record, index)=>{
          return (
            <div fieldid="m_oprDate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
          );
        }
      },
      {
        title: (<div fieldid="m_Batchid" className="mergecells">{this.state.json['20020VRIFYHISTORY-000022']}</div>),/* 国际化处理： 核销批号*/
        dataIndex: "m_Batchid",
        key: "m_Batchid",
        width: 200,
        render:(text, record, index)=>{
          return (
            <div fieldid="m_Batchid">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
          );
        }
      },
      {
        title: this.state.json['20020VRIFYHISTORY-000023'],/* 国际化处理： 借方*/
        children: [
          {
            title: (<div fieldid="m_debit_y" className="mergecells">{this.state.json['20020VRIFYHISTORY-000024']}</div>),/* 国际化处理： 原币*/
            dataIndex: "m_debit_y",
            key: "m_debit_y",
            width: 200,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_debit_y">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_debit_b" className="mergecells">{this.state.json['20020VRIFYHISTORY-000025']}</div>),/* 国际化处理： 组织本币*/
            dataIndex: "m_debit_b",
            key: "m_debit_b",
            width: 200,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_debit_b">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          }
        ]
      },
      {
        title: this.state.json['20020VRIFYHISTORY-000026'],/* 国际化处理： 贷方*/
        children: [
          {
            title: (<div fieldid="m_credit_y" className="mergecells">{this.state.json['20020VRIFYHISTORY-000024']}</div>),/* 国际化处理： 原币*/
            dataIndex: "m_credit_y",
            key: "m_credit_y",
            width: 200,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_credit_y">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_credit_b" className="mergecells">{this.state.json['20020VRIFYHISTORY-000025']}</div>),/* 国际化处理： 组织本币*/
            dataIndex: "m_credit_b",
            key: "m_credit_b",
            width: 200,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_credit_b">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          }
        ]
      }
    ];
    const columns_details = [
      //详细
      {
        title: (<div fieldid="m_oprDate" className="mergecells">{this.state.json['20020VRIFYHISTORY-000018']}</div>),/* 国际化处理： 核销日期*/
        dataIndex: "m_oprDate",
        key: "m_oprDate",
        width: 100,
        render:(text, record, index)=>{
          return (
            <div fieldid="m_oprDate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
           
          );  
        }
      },
      {
        title: (<div fieldid="m_Batchid" className="mergecells">{this.state.json['20020VRIFYHISTORY-000022']}</div>),/* 国际化处理： 核销批号*/
        dataIndex: "m_Batchid",
        key: "m_Batchid",
        width: 100,
        render:(text, record, index)=>{
          return (
            <div fieldid="m_Batchid">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
          );
        }
      },
      {
        title: this.state.json['20020VRIFYHISTORY-000023'],/* 国际化处理： 借方*/
        children: [
          {
            title: (<div fieldid="m_sDebit_PkUnit" className="mergecells">{this.state.json['20020VRIFYHISTORY-000008']}</div>),/* 国际化处理： 业务单元*/
            dataIndex: "m_sDebit_PkUnit",
            key: "m_sDebit_PkUnit",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_sDebit_PkUnit">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_debit_voucherDate" className="mergecells">{this.state.json['20020VRIFYHISTORY-000016']}</div>),/* 国际化处理： 凭证日期*/
            dataIndex: "m_debit_voucherDate",
            key: "m_debit_voucherDate",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_debit_voucherDate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_debit_BusinessDate" className="mergecells">{this.state.json['20020VRIFYHISTORY-000017']}</div>),/* 国际化处理： 业务日期*/
            dataIndex: "m_debit_BusinessDate",
            key: "m_debit_BusinessDate",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_debit_BusinessDate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_Debit_VerifyNo" className="mergecells">{this.state.json['20020VRIFYHISTORY-000013']}</div>),/* 国际化处理： 核销号*/
            dataIndex: "m_Debit_VerifyNo",
            key: "m_Debit_VerifyNo",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_Debit_VerifyNo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_debit_vouchNo" className="mergecells">{this.state.json['20020VRIFYHISTORY-000027']}</div>),/* 国际化处理： 凭证号*/
            dataIndex: "m_debit_vouchNo",
            key: "m_debit_vouchNo",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_debit_vouchNo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_sDebit_DetailIndex" className="mergecells">{this.state.json['20020VRIFYHISTORY-000028']}</div>),/* 国际化处理： 分录号*/
            dataIndex: "m_sDebit_DetailIndex",
            key: "m_sDebit_DetailIndex",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_sDebit_DetailIndex">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="debit_assInfo" className="mergecells">{this.state.json['20020VRIFYHISTORY-000029']}</div>),/* 国际化处理： 辅助核算*/
            dataIndex: "debit_assInfo",
            key: "debit_assInfo",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="debit_assInfo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_Debit_Digest" className="mergecells">{this.state.json['20020VRIFYHISTORY-000014']}</div>),/* 国际化处理： 摘要*/
            dataIndex: "m_Debit_Digest",
            key: "m_Debit_Digest",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_Debit_Digest">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_debit_y" className="mergecells">{this.state.json['20020VRIFYHISTORY-000024']}</div>),/* 国际化处理： 原币*/
            dataIndex: "m_debit_y",
            key: "m_debit_y",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_debit_y">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_debit_b" className="mergecells">{this.state.json['20020VRIFYHISTORY-000025']}</div>),/* 国际化处理： 组织本币*/
            dataIndex: "m_debit_b",
            key: "m_debit_b",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_debit_b">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          }
        ]
      },
      {
        title: this.state.json['20020VRIFYHISTORY-000026'],/* 国际化处理： 贷方*/
        children: [
          {
            title: (<div fieldid="m_sCredit_PkUnit" className="mergecells">{this.state.json['20020VRIFYHISTORY-000008']}</div>),/* 国际化处理： 业务单元*/
            dataIndex: "m_sCredit_PkUnit",
            key: "m_sCredit_PkUnit",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_sCredit_PkUnit">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_credit_voucherDate" className="mergecells">{this.state.json['20020VRIFYHISTORY-000016']}</div>),/* 国际化处理： 凭证日期*/
            dataIndex: "m_credit_voucherDate",
            key: "m_credit_voucherDate",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_credit_voucherDate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_credit_BusinessDate" className="mergecells">{this.state.json['20020VRIFYHISTORY-000017']}</div>),/* 国际化处理： 业务日期*/
            dataIndex: "m_credit_BusinessDate",
            key: "m_credit_BusinessDate",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_credit_BusinessDate">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_Credit_verifyNo" className="mergecells">{this.state.json['20020VRIFYHISTORY-000013']}</div>),/* 国际化处理： 核销号*/
            dataIndex: "m_Credit_verifyNo",
            key: "m_Credit_verifyNo",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_Credit_verifyNo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_credit_vouchNo" className="mergecells">{this.state.json['20020VRIFYHISTORY-000027']}</div>),/* 国际化处理： 凭证号*/
            dataIndex: "m_credit_vouchNo",
            key: "m_credit_vouchNo",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_credit_vouchNo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_sCredit_DetailIndex" className="mergecells">{this.state.json['20020VRIFYHISTORY-000028']}</div>),/* 国际化处理： 分录号*/
            dataIndex: "m_sCredit_DetailIndex",
            key: "m_sCredit_DetailIndex",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_sCredit_DetailIndex">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="credit_assInfo" className="mergecells">{this.state.json['20020VRIFYHISTORY-000029']}</div>),/* 国际化处理： 辅助核算*/
            dataIndex: "credit_assInfo",
            key: "credit_assInfo",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="credit_assInfo">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_Credit_Digest" className="mergecells">{this.state.json['20020VRIFYHISTORY-000014']}</div>),/* 国际化处理： 摘要*/
            dataIndex: "m_Credit_Digest",
            key: "m_Credit_Digest",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_Credit_Digest">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_credit_y" className="mergecells">{this.state.json['20020VRIFYHISTORY-000024']}</div>),/* 国际化处理： 原币*/
            dataIndex: "m_credit_y",
            key: "m_credit_y",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_credit_y">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          },
          {
            title: (<div fieldid="m_credit_b" className="mergecells">{this.state.json['20020VRIFYHISTORY-000025']}</div>),/* 国际化处理： 组织本币*/
            dataIndex: "m_credit_b",
            key: "m_credit_b",
            width: 100,
            render:(text, record, index)=>{
              return (
                <div fieldid="m_credit_b">{text==null||(text&&!text.display)?<span>&nbsp;</span>:text.display}</div>
              );
            }
          }
        ]
      }
    ];
    let columnsSum = this.renderColumnsMultiSelectDebit(columns_sum);
    let columnsDetails = this.renderColumnsMultiSelect(columns_details);
    let credit = historyDataArr[0];
    const loadQuery = [
      {
        itemName: this.state.json['20020VRIFYHISTORY-000007'],/* 国际化处理： 核算账簿*/
        itemType: "refer",
        itemKey: "pk_accountingbook", isMustItem: true,
        config: { refCode: "uapbd/refer/org/AccountBookTreeRef" }
      },
      {
        itemName: this.state.json['20020VRIFYHISTORY-000008'],/* 国际化处理： 业务单元*/
        itemType: "refer",
        itemKey: "pk_units", isMustItem: false,
        config: { refCode: "uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef" }
      },
      {
        itemName: this.state.json['20020VRIFYHISTORY-000009'],/* 国际化处理： 核销科目*/
        itemType: "refer",
        itemKey: "pk_accasoa", isMustItem: true,
        config: { refCode: "uapbd/refer/fiacc/AccountDefaultGridTreeRef" }
      },
      {
        itemName: this.state.json['20020VRIFYHISTORY-000010'],/* 国际化处理： 日期范围*/
        itemType: "Dbdate",
        itemKey: ["begin_date", "end_date"]
      }, //begin_date,end_date（必传）
      {
        itemName: this.state.json['20020VRIFYHISTORY-000011'],/* 国际化处理： 金额范围*/
        itemType: "DbtextInput",
        itemKey: ["mny_begin", "mny_end"]
      }, //mny_begin,mny_end
      {
        itemName: this.state.json['20020VRIFYHISTORY-000012'],/* 国际化处理： 币种*/
        itemType: "refer",
        itemKey: "pk_currency", isMustItem: true,
        config: { refCode: "uapbd/refer/pubinfo/CurrtypeGridRef" }
      },
      { itemName: this.state.json['20020VRIFYHISTORY-000013'], itemType: "textInput", itemKey: "verifyno" },/* 国际化处理： 核销号*/
      { itemName: this.state.json['20020VRIFYHISTORY-000014'], itemType: "textInput", itemKey: "digest" },/* 国际化处理： 摘要*/
      {
        itemName: this.state.json['20020VRIFYHISTORY-000015'],/* 国际化处理： 日期类型*/
        itemType: "radio",
        itemKey: "dateType",
        itemChild: [
          {
            label: this.state.json['20020VRIFYHISTORY-000016'],/* 国际化处理： 凭证日期*/
            value: "voucherDate"
          },
          {
            label: this.state.json['20020VRIFYHISTORY-000017'],/* 国际化处理： 业务日期*/
            value: "Businessdate"
          },
          {
            label: this.state.json['20020VRIFYHISTORY-000018'],/* 国际化处理： 核销日期*/
            value: "opdate"
          }
        ]
      },
      {
        itemName: this.state.json['20020VRIFYHISTORY-000019'],/* 国际化处理： 处理方式*/
        itemType: "radio",
        itemKey: "redBlue",
        itemChild: [
          {
            label: this.state.json['20020VRIFYHISTORY-000020'],/* 国际化处理： 核销*/
            value: "N"
          },
          {
            label: this.state.json['20020VRIFYHISTORY-000021'],/* 国际化处理： 红蓝对冲*/
            value: "Y"
          }
        ]
      }
    ];
    return (
      <div className="history nc-bill-list" id="historyquery">
			<HeaderArea
				title={this.state.json['20021VYBAL-000040']}/* 国际化处理： 往来核销余额表*/

				btnContent={
					this.props.button.createButtonApp({
						area: 'header_buttons',
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-buttons-area')
					})
				}
			/> 
        <SearchModal
         modalDefaultValue={modalDefaultValue}
          loadData={loadQuery}
          showOrHide={showModal}
          onConfirm={handleQueryClick.bind(this, this.state)}
          handleClose={this.handleClick.bind(this)}
        />
        <div className="account-content1" fieldid="historysum_area">
          <div className="account-content1-credit">
            {sumOrDetail == "sum" ? (
              <NCDiv fieldid="historysum" areaCode={NCDiv.config.TableCom}>
              <Table
                columns={columnsSum}
                bordered
                // onRowClick={this.getRow}
                data={historySumDataArr}
                bodyStyle={{height:getTableHeight(120)}}
                scroll={{
                  x: true,
                  y: getTableHeight(120)
                }}
              />
              </NCDiv>
            ) : null}
          </div>
        </div>
        <div className="account-content1" fieldid="historydetails_area">
          <div className="account-content1-credit">
            {sumOrDetail == "details" ? (
              <NCDiv fieldid="historydetails" areaCode={NCDiv.config.TableCom}>
              <Table
                columns={columnsDetails}
                bordered
                data={historyDataArr}
                bodyStyle={{height:getTableHeight(120)}}
                scroll={{
                  x: true,
                  y: getTableHeight(120)
                }}
              />
              </NCDiv>
             ) : null}
          </div>
        </div>
        <PrintModal
            noRadio={true}
            noCheckBox={true}
            appcode={this.state.appcode}
            nodekey={this.state.sumOrDetail}
            visible={this.state.showPrintModal}
            handlePrint={this.handlePrint.bind(this)}
            handleCancel={() => {
              this.setState({
                showPrintModal: false
              })
            }}
        />
        <PrintOutput                    
            ref='printOutput'
            url='/nccloud/gl/verify/verbalanceoutput.do'
            data={this.state.outputData}
            callback={this.handleOutput.bind(this)}
        />
      </div>
    );
  }
}
// HistoryQuery = createPage({})(HistoryQuery);
HistoryQuery.defaultProps = defaultProps12;

HistoryQuery = createPage({
//	initTemplate: initTemplate,
	mutiLangCode: '2002' // <- 这个2002意义不明，以后再说
})(HistoryQuery);


ReactDOM.render(<HistoryQuery />, document.querySelector('#app'));
// export default HistoryQuery;
