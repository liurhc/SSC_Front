import {ajax} from 'nc-lightapp-front';
import { toast } from '../../../../public/components/utils.js';
/*过滤选中数据 */
export default function filterSelectedData(state,type){
    let {checkedArraySum,checkedArrayDetail,sumSelectedData,detailSelectedData,historySumDataArr,historyDataArr}=state;
    sumSelectedData=[];
    detailSelectedData=[];
    if(type=="sum"){//借方
        for(var i = 0; i < checkedArraySum.length; i++){
            if(checkedArraySum[i]==true){
                sumSelectedData.push(historySumDataArr[i]);
            }
        }
        return sumSelectedData;
    }else if(type=='details'){//
        for(var i = 0; i < checkedArrayDetail.length; i++){
            if(checkedArrayDetail[i]==true){
                detailSelectedData.push(historyDataArr[i]);
            }
        }
        return detailSelectedData;
    }
}
