import {ajax} from 'nc-lightapp-front';
import { toast } from '../../../../public/components/utils.js';
import filterSelectedData from './filterSelectedData';

export default function handleDetail(self){
    // let self=this;
    let {sumOrDetail,historyDataArr,checkedArraySum,checkedArrayDetail,resourceData_details,checkedAll}=self.state;
    let m_Batchid=[];//批次
    let cerrentBatchidData=[];//过来当前批次选中的数据
    checkedArrayDetail=[];
    let sumSelectedData=filterSelectedData(self.state,'sum');//获取汇总选中的数据的批次 m_Batchid
    if(sumSelectedData.length>0){
        sumSelectedData.map((item,i)=>{
            if(m_Batchid.indexOf(item.m_Batchid.value)==-1){
                m_Batchid.push(item.m_Batchid.value);
            }			
        })
        resourceData_details.map((list,k)=>{
            if(m_Batchid.indexOf(list.m_Batchid.value)!=-1){
                cerrentBatchidData.push(list);
                checkedArrayDetail.push(false);
            }
        })
        // checkedArraySum.map((v,k)=>{
        //     checkedArraySum[k]=false;
        // })
    }else{
        toast({ content: self.state.json['20020VRIFYHISTORY-000000'], color: "warning" });/* 国际化处理： 请先至少选择一行数据进行操作*/
        return false;
        // cerrentBatchidData=resourceData_details;
    }
    sumOrDetail='details';
    self.setState({
        sumOrDetail,checkedArraySum,
        historyDataArr:cerrentBatchidData,
        checkedArrayDetail
        // checkedAllSum: false
    },()=>{
        self.props.button.setButtonDisabled('detail', true);
        self.props.button.setButtonDisabled('verifySum', false);
    })
}
