import { ajax, base, toast,cacheTools,print,withNav } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
let tableid = 'gl_brsetting';

import {
    handleUnverify,
    handleDetail,
    handleSum
  } from "./index.js";

// @withNav
export default function buttonClick(props, id) {
    let {querycondition}=this.state;
    let rowDatas,pk_brsetting,pk_unit,pk_currtype,link;
    switch (id) {
        // 查询
        case 'inquiry':
            this.handleClick();
            break;

        // 反核销
        case 'reverse_verificationCancel':
            handleUnverify(this);
            break;

        // 详细
        case 'detail':
            handleDetail(this);
            break;
        case 'verifySum':
            handleSum(this);
            break;
        case 'print'://打印
            this.showPrintModal();
            break;
        case 'showOutputModal'://模板输出
            this.showOutputModal();
            break;
        default:
        break;

    }
}
