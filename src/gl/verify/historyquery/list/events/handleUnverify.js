import { ajax, promptBox } from "nc-lightapp-front";
import { toast } from "../../../../public/components/utils.js";
import filterSelectedData from "./filterSelectedData";
export default function handleUnverify(self) {
	// let self=this;
	let state = self.state;
	let url = "/nccloud/gl/verify/unverify.do";
	let {
		creditSelectedData,
		queryDataObj,
		checkedAllSum,
		checkedArrayDetail,
		checkedArraySum,
		historySumDataArr,
		historyDataArr,
		sumOrDetail, resourceData_details
	} = state;
	checkedAllSum = false;
	checkedArrayDetail = [];
	checkedArraySum = [];
	let detailSelectedData = []; //选中的明细数据
	// creditSelectedData=historyDataArr;//贷方数据
	let sumSelectedData = filterSelectedData(state, "sum"); //获取汇总选中的数据
	if (sumSelectedData.length > 0 && sumOrDetail == 'sum') {
		let m_BatchidArr = [];
		for (var i = 0; i < sumSelectedData.length; i++) {
			m_BatchidArr.push(sumSelectedData[i].m_Batchid.value);
		}
		for (var k = 0; k < resourceData_details.length; k++) {
			if (m_BatchidArr.toString().indexOf(resourceData_details[k].m_Batchid.value) != -1) {
				detailSelectedData.push(resourceData_details[k]);
			}
		}
	} else if (filterSelectedData(state, "details").length > 0 && sumOrDetail == 'details') {
		detailSelectedData = filterSelectedData(state, "details"); //获取明细选中的数据
	} else {
		toast({ content: self.state.json['20020VRIFYHISTORY-000002'], color: "warning" });/* 国际化处理： 请至少选择一行数据进行反核销*/
		return false;
	}
	promptBox({
		color: 'warning',
		title: self.state.json['20020VRIFYHISTORY-000003'],/* 国际化处理： 提示*/
		content: self.state.json['20020VRIFYHISTORY-000004'],/* 国际化处理： 是否反核销？*/
		noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
		noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
		beSureBtnName: self.state.json['20020VRIFYHISTORY-000005'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
		cancelBtnName: self.state.json['20020VRIFYHISTORY-000006'],           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
		beSureBtnClick: dealOperate.bind(self, url, detailSelectedData, queryDataObj),   // 确定按钮点击调用函数,非必输
		cancelBtnClick: cancelBtnClick.bind(self)  // 取消按钮点击调用函数,非必输
	})

}
export function dealOperate(url, detailSelectedData, queryDataObj) {
	let self = this;
	let {
		checkedAllSum,
		checkedArrayDetail,
		checkedArraySum, sumOrDetail
	} = self.state;
	checkedAllSum = false;
	checkedArrayDetail = [];
	checkedArraySum = [];
	let data = {
		displogVO: detailSelectedData,
		condition: queryDataObj
	};
	ajax({
		url: url,
		data: data,
		success: function (response) {
			const { data, success } = response;
			if (success) {
				let historyData = data.historyData;
				let historySumData = data.historySumData;
				let m_BatchidArr = [], cerrentBatchidData = [];
				if(sumOrDetail=='sum'){
					if (historyData) {
						let sumSelectedData = filterSelectedData(self.state, "sum"); //获取汇总选中的数据
						for (var i = 0; i < sumSelectedData.length; i++) {
							m_BatchidArr.push(sumSelectedData[i].m_Batchid.value);
						}
						historyData.map((list, k) => {
							if (m_BatchidArr.indexOf(list.m_Batchid.value) != -1) {
								cerrentBatchidData.push(list);
								checkedArrayDetail.push(false);
							}
						})
					} else {
						historyData = [];
					}
					if (historySumData) {
						historySumData.map((item, i) => {
							item.key = i;
							checkedArraySum.push(false);
						});
					} else {
						historySumData = [];
					}
				}else if(sumOrDetail=='details'){
					if(historyData){
						historyData.map((list, k) => {
							cerrentBatchidData.push(list);
							checkedArrayDetail.push(false);	
						})
					}else{
						historyData = [];
					}
					if (historySumData) {
						historySumData.map((item, i) => {
							item.key = i;
							checkedArraySum.push(false);
						});
					} else {
						historySumData = [];
					}
				}
				self.setState({
					historyDataArr: cerrentBatchidData,
					historySumDataArr: historySumData,
					resourceData_details: historyData,
					resourceData_sum: historySumData,
					checkedArrayDetail, checkedAllSum,
					checkedArraySum
				});
			} else {
				toast({ content: self.state.json['20020VRIFYHISTORY-000001'], color: "warning" });/* 国际化处理： 没有获取数据*/
			}
		}
	});
}
export function cancelBtnClick() {
	return false;
}
