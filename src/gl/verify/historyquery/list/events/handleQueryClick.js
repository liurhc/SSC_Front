import {ajax,deepClone} from 'nc-lightapp-front';
import { toast } from '../../../../public/components/utils.js';

export default function handleQueryClick(state,data){
    let self=this;
    let {queryDataObj,mockData,historyDataArr,historySumDataArr,checkedAllSum,checkedArrayDetail,checkedArraySum,
        resourceData_sum,resourceData_details,defaultStatu} = state; 
    let childData = deepClone(data);
    checkedAllSum=false;
    checkedArrayDetail=[];
    checkedArraySum=[];
    let url = '/nccloud/gl/verify/historyquery.do';
    for(let k in childData){
        if(k=='ass'){
            // e.ass.map((item,i)=>{
            // 	for(let k in item){
            // 		item[k]=item[k].value?item[k].value:'';
            // 	}
            // })
        }else if(k=='pk_units'){
            if(childData[k].length>0){
                if(childData[k][0].value==""){
                    childData[k]=null;
                }else{
                    childData[k].map((item,index)=>{
                        //childData[k].push(item.value);
                        childData[k][index]=item.value;
                    })
                }
            }else{
                childData[k]=null;
            }
        }else{
            childData[k]=childData[k].value?childData[k].value:null;
        }			
    }
//childData.pk_accasoa= "1001A3100000000006Z2";
    ajax({
        url:url,
        data:childData,
        success: function(response){//historyData是明细的数据 historySumData是汇总的数据
            let { data, success } = response;
            if(success){
                let historyData = data.historyData;
                let historySumData = data.historySumData;
                if(historyData){
                    defaultStatu=false;
                    historyData.map((item,i)=>{
                        item.key=i;
                        checkedArrayDetail.push(false);
                        //把未核销金额赋给核销
                        item.m_dCredit_Money_Y=item.m_Balancecreditamount;
                        item.m_dCredit_Money_B=item.m_Balancelocalcreditamount;
                        item.m_Balancecreditamount=0;
                        item.m_Balancelocalcreditamount=0;
                    })
                }else{
                    historyData=[];
                }
                if(historySumData){
                    defaultStatu=false;
                    historySumData.map((item,i)=>{
                        item.key=i;
                        //定义两个临时变量存储未核销的原币和组织本币
                        item.empty_m_Balancedebitamount=item.m_Balancedebitamount;
                        item.empty_m_Balancelocaldebitamount =item.m_Balancelocaldebitamount ;
                        checkedArraySum.push(false);
                    })
                }else{
                    historySumData=[];
                }
                self.setState({
                    sumOrDetail:'sum',
                    queryDataObj:childData,
                    historyDataArr: historyData,
                    historySumDataArr: historySumData,
                    resourceData_details:historyData,
                    resourceData_sum:historySumData,
                    checkedArrayDetail,checkedArraySum,
                    showModal: !self.state.showModal,
                    defaultStatu                   
                },()=>{
                    if(historySumData.length>0){
                        self.props.button.setButtonDisabled(['reverse_verificationCancel','detail','print','showOutputModal'], false);
                        self.props.button.setButtonDisabled('verifySum', true);
                    }else{
                        self.props.button.setButtonDisabled(['reverse_verificationCancel','detail','print','showOutputModal'], true);
                    }
                })
            } else {
                toast({ content: self.state.json['20020VRIFYHISTORY-000001'], color: 'warning' });/* 国际化处理： 没有获取数据*/
            }
        },
        error:function(){
            self.setState({
                showModal: !self.state.showModal,
            })
        }
    });
}
