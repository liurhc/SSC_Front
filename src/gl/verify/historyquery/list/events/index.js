import handleQueryClick from './handleQueryClick';
import handleUnverify from './handleUnverify';
import handleDetail from './handleDetail';
import handleSum from './handleSum';
import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
export{handleQueryClick,handleUnverify,handleDetail,handleSum, buttonClick, initTemplate}
