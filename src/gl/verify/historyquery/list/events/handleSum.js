import {ajax} from 'nc-lightapp-front';
import { toast } from '../../../../public/components/utils.js';
import filterSelectedData from './filterSelectedData';
export default function handleSum(self){
    // let self=this;
    let {sumOrDetail,historySumDataArr,checkedArraySum,checkedArrayDetail,resourceData_sum,checkedAll}=self.state;
    let m_Batchid=[];//批次
    let cerrentBatchidData=[];//过来当前批次选中的数据
    let detailSelectedData=filterSelectedData(self.state,'details');//获取汇总选中的数据批次
    // if(detailSelectedData.length>0){
    //     detailSelectedData.map((item,i)=>{
    //         if(m_Batchid.indexOf(item.m_Batchid.value)==-1){
    //             m_Batchid.push(item.m_Batchid.value);
    //         }
    //     })
    //     resourceData_sum.map((list,k)=>{
    //         if(m_Batchid.indexOf(list.m_Batchid.value)!=-1){
    //             cerrentBatchidData.push(list);
    //         }
    //     })
    //     checkedArrayDetail.map((v,k)=>{
    //         checkedArrayDetail[k]=false;
    //     }) 
    // }else{
        // toast({ content: "请先至少选择一行数据进行操作", color: "warning" });
        // return false;
        cerrentBatchidData=resourceData_sum;
    // }
    sumOrDetail='sum';
    self.setState({
        sumOrDetail,checkedArrayDetail,
        historySumDataArr:cerrentBatchidData,
        checkedAllDetail: false
    },()=>{
        self.props.button.setButtonDisabled('detail', false);
        self.props.button.setButtonDisabled('verifySum', true);
    })
}
