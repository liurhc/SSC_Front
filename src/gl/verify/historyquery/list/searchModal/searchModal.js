import React, { Component } from 'react';
import {high,base,ajax,getBusinessInfo,getMultiLang} from 'nc-lightapp-front';
// import moment from 'moment';
// import zhCN from 'rc-calendar/lib/locale/zh_CN';
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../../public/components/FormItems';
const { Refer} = high;
import { toast } from '../../../../public/components/utils.js';
import createScript from '../../../../public/components/uapRefer.js';
// const format = 'YYYY-MM-DD';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm
} = base;
import './index.less'
import getAssDatas from "../../../../public/components/getAssDatas/index.js";
import checkMustItem from "../../../../public/common/checkMustItem.js";
const {  NCFormItem:FormItem } = NCForm;

//const dateInputPlaceholder = this.state.json['20020VRIFYHISTORY-000038'];/* 国际化处理： 选择日期*/
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};

  const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
  };


export default class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            assidCondition :{
                pk_accountingbook:'',// '1001A3100000000000PE',
                pk_accasoa:'',// '0001Z0100000000001BD',
                prepareddate:'',// '2018-07-18',
                pk_org:'',// '0001A310000000000NN6',
                assData:[],// assData,
                assid: "",//'0001Z31000000006G5A9',
                checkboxShow: true
              },//辅助核算组件需要的参数
            showModal:false,
            isShowUnit:false,//是否显示业务单元
            assData: [//辅助核算信息
            ],
            childAssData: {
                accountingbook_org:'',//保存账簿对应的组织
                pk_org: '',
                pk_accountingbook: ''
              },//接受父组件传过来的参数
            modalDefaultValue:{},//默认数据
            loadData:[],//查询模板加载数据
            listItem:{},//模板数据对应值
            SelectedAssData:[],//选中的数据
            checkedAll:true,
            checkedArray: [],
        };
        this.close = this.close.bind(this);
    }
    componentWillMount() {

        let callback= (json) =>{
          this.setState({json:json},()=>{
          })
        }
        getMultiLang({moduleId:['20020VRIFYHISTORY','publiccommon','publiccomponents'],domainName:'gl',currentLocale:'simpchn',callback});
      }
    componentWillReceiveProps (nextProp) {

        let self=this;
        let {loadData,showOrHide,modalDefaultValue}=nextProp;
        let { listItem,showModal,isShowUnit,childAssData }=self.state;
        
        if (showOrHide&&nextProp.loadData != self.state.loadData&&self.state.loadData.length==0 ) {
            childAssData.pk_org=modalDefaultValue.pk_org;
            childAssData.accountingbook_org=modalDefaultValue.pk_org;
            isShowUnit=modalDefaultValue.isShowUnit;
            loadData.forEach((item,i)=>{
                let  key;
                if(item.itemType=='refer'){
                    if(item.itemKey=='pk_units'){
                        key=[{
                            display:'',
                            value:'',
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }]
                    }else if(item.itemKey=='pk_accountingbook'){
                        key={
                            display:modalDefaultValue.pk_accountingbook.display,
                            value:modalDefaultValue.pk_accountingbook.value,
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                    }else if(item.itemKey=='pk_currency'){
                        key={
                            display:modalDefaultValue.pk_currency.display,
                            value:modalDefaultValue.pk_currency.value,
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                      }else{
                        key={
                            display:'',
                            value:'',
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                    } 
                } else if (item.itemType == "radio") {
                    if(item.itemKey=='dateType'){
                        key = {
                            value: item.itemChild[2].value
                        };
                    }else{
                        key = {
                            value: item.itemChild[0].value
                        };
                    }
                    
                }else{
                    key={
                        value:''
                    }
                    if(item.itemType=='date'||item.itemType=='Dbdate'){
                        if(item.itemKey=='begin_date'){
                            key = {
                              value:modalDefaultValue.begindate
                            };
                          }else if(item.itemKey=='end_date'){
                            key = {
                              value:modalDefaultValue.bizDate
                            };
                          }else{
                            key = {
                              value: ""
                            };
                          }
                    }
                }
                if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'){
                item.itemKey.map((k,index)=>{
                    let name= k;
                    listItem[name]=key
                });
                }else{
                    let name= item.itemKey;
                    listItem[name]=key
                }
            
            })
            listItem['begin_date']={value:modalDefaultValue.begindate};
            listItem['end_date']={value:modalDefaultValue.bizDate};
            self.setState({
                modalDefaultValue:modalDefaultValue,
                loadData:loadData,
                showModal:showOrHide,
                listItem,childAssData,isShowUnit
            })
        }else{
            self.setState({
                showModal:showOrHide,childAssData,isShowUnit
            })
        }
    }

    //表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

    close() {
        this.props.handleClose();
    }

    confirm=()=>{
        let { listItem,assData,checkedArray,SelectedAssData,assidCondition } =this.state;
        SelectedAssData=[];
        let checkStatus=checkMustItem(listItem);//必输项校验
        if(!checkStatus.flag){
            toast({content:checkStatus.info+this.state.json['publiccommon-000001'],color:'warning'});
            return false;
        }
        for(var i = 0; i < checkedArray.length; i++){
            if(checkedArray[i]==true){
                SelectedAssData.push(assData[i]);
            }
        }
        listItem.ass=SelectedAssData;//assData
        // this.props.onConfirm(listItem);
        this.setState({
            assidCondition
          },()=>{
            this.props.onConfirm(listItem);
          })
    }

    queryList=(data)=>{
        let self=this;
        function getNowFormatDate() {
            let date = new Date();
            let seperator1 = "-";
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            let currentdate = year + seperator1 + month + seperator1 + strDate;
            return currentdate;
        }
        // let currrentDate = getNowFormatDate();
        let dateInputPlaceholder = this.state.json['20020VRIFYHISTORY-000038'];/* 国际化处理： 选择日期*/
        let businessInfo = getBusinessInfo();
        let currrentDate = businessInfo.businessDate.split(' ')[0]; 
        let { listItem,isShowUnit,assData,checkedArray,childAssData,assidCondition,modalDefaultValue } =self.state;
			return data.length!=0?(
				data.map((item, i) => {                   
                   switch (item.itemType) {
                       case 'refer':
                       let referUrl= item.config.refCode+'/index.js';
                       let DBValue=[];
                       let defaultValue={}
                       if(listItem[item.itemKey].length){                           
                            listItem[item.itemKey].map((item,index)=>{
                                DBValue[index]={ refname: item.display, refpk:item.value };
                            })
                        }else{
                            defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
                        }
                       //let defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
					   if(!self.state[item.itemKey]){
                           {createScript.call(self,referUrl,item.itemKey)}
                           return <div />
					    }else{
                            if(item.itemKey=='pk_accountingbook'){
                                return(
                                    <FormItem
                                        inline={true}
                                        showMast={item.isMustItem}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={10} md={10} sm={10}
                                        labelName={item.itemName}
                                        //isRequire={true}
                                        method="change"
                                    >
                                {self.state[item.itemKey]?(self.state[item.itemKey])(
                                    {
                                        fieldid:item.itemKey,
                                        value:defaultValue,
                                        isMultiSelectedEnabled:false,
                                        showGroup:false,
                                        disabledDataShow:true,
                                        queryCondition:() => {
                                            return Object.assign({
                                                "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                                "appcode":modalDefaultValue.appcode
                                            },config)
                                        },
                                        onChange:(v)=>{
                                            if(v.refpk){
                                                //判断是否起用业务单元
                                                let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
                                                let pk_accpont = {"pk_accountingbook":v.refpk}
                                                ajax({
                                                    url:url,
                                                    data:pk_accpont,
                                                    success: function(response){
                                                        const { success } = response;
                                                        //渲染已有账表数据遮罩
                                                        if (success) {
                                                            if(response.data){
                                                                isShowUnit=response.data.isShowUnit;
                                                                listItem['pk_currency'].value = response.data.currinfo.value;
                                                                listItem['pk_currency'].display = response.data.currinfo.display;
                                                                listItem['begin_date']={value:response.data.begindate};
                                                                listItem['end_date']={value:response.data.bizDate};
                                                                childAssData.pk_org = response.data.unit.value;
                                                                childAssData.accountingbook_org=response.data.unit.value;
                                                                assidCondition.pk_org=response.data.unit.value;
                                                            }
                                                            self.setState({
                                                                isShowUnit,childAssData,assidCondition
                                                            })
                                                        }   
                                                    }
                                                });
                                            }
                                            assidCondition.pk_accountingbook=v.refpk;
                                            listItem[item.itemKey].value = v.refpk
                                            listItem[item.itemKey].display = v.refname
                                            //清空科目,业务单元，辅助核算项
                                            listItem['pk_accasoa'].display='';
                                            listItem['pk_accasoa'].value='';
                                            listItem['pk_units']=[{display:'',value:''}];
                                            assData=[];
                                            this.setState({
                                                listItem,assidCondition,assData
                                            })
                                        }
                                    }
                                    ):<div/>}
                                </FormItem>)
                            }else if(item.itemKey=='pk_accasoa'){
                                return(
                                    <FormItem
                                        inline={true}
                                        showMast={item.isMustItem}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={10} md={10} sm={10}
                                        labelName={item.itemName}
                                        //isRequire={true}
                                        method="change"
                                    >
                                {self.state[item.itemKey]?(self.state[item.itemKey])(
                                    {
                                        fieldid:item.itemKey,
                                        value:defaultValue,
                                        isMultiSelectedEnabled:false,
                                        queryCondition:() => {
                                            return Object.assign({
                                                "refName":item.itemName,
                                                "pk_accountingbook": listItem.pk_accountingbook.value? listItem.pk_accountingbook.value:'',
                                                "dateStr":currrentDate,
                                                "TreeRefActionExt":'nccloud.web.gl.verify.action.VerifyObjectRefSqlBuilder'
                                                
                                            },config)    
                                        },
                                        onFocus:(v)=>{
                                            if(!listItem.pk_accountingbook.value){
                                                toast({content:this.state.json['20020VRIFYHISTORY-000046'],color:'warning'});/* 国际化处理： 请先选择核算账簿，核算账簿不能为空*/
                                                return false;
                                            }
                                        },
                                        onChange:(v)=>{
                                            //根据选定的pk 实现过滤
                                            if(item.itemKey!='pk_accountingbook'&&!listItem.pk_accountingbook.value){
                                                toast({ content: this.state.json['20020VRIFYHISTORY-000047'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
                                                return false
                                            }
                                            assData=[];
                                            checkedArray=[];//选中的数据
                                            if(v.refpk){
                                                //请求辅助核算数据
                                                let url = '/nccloud/gl/voucher/queryAssItem.do';
                                                let queryData = {
                                                    pk_accasoa: v.refpk,
                                                    prepareddate: currrentDate,
                                                };
                                                
                                                ajax({
                                                    url:url,
                                                    data:queryData,
                                                    success: function(response){
                                                        const { success } = response;
                                                        //渲染已有账表数据遮罩
                                                        if (success) {
                                                            if(response.data){
                                                                response.data.map((item,index)=>{
                                                                    checkedArray.push(true);
                                                                    let obj={
                                                                        key:index,
                                                                        "checktypecode":item.code,
                                                                        "checktypename" :item.name,
                                                                        "pk_Checktype": item.pk_accassitem,
                                                                        "refCode":item.refCode?item.refCode:item.code,
                                                                        "refnodename":item.refnodename,
                                                                        "pk_accassitem":item.pk_accassitem,
                                                                        "pk_accountingbook":listItem.pk_accountingbook.value,
                                                                        "classid": item.classid,
                                                                        "pk_defdoclist": item.classid,
                                                                        }
                                                                    assData.push(obj);
                                                                })
                                                            }else{
                                                                assData=[];
                                                            }
                                                            self.setState({
                                                                assData,checkedArray
                                                            })
                                                        }   
                                                    }
                                                });
                                            }
                                            listItem[item.itemKey].value = v.refpk;
                                            listItem[item.itemKey].display = v.refname;
                                            assidCondition.pk_accasoa=v.refpk;
                                            assidCondition.prepareddate=currrentDate;
                                            this.setState({
                                                listItem,assidCondition,assData,checkedArray
                                            })
                                        }
                                    }
                                    ):<div/>}
                                </FormItem>)
                            }else if(item.itemKey=='pk_units'){
                                if(isShowUnit){
                                    return(
                                    <FormItem
                                        inline={true}
                                        showMast={item.isMustItem}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={10} md={10} sm={10}
                                        labelName={item.itemName}
                                        //isRequire={true}
                                        method="change"
                                    >
                                {self.state[item.itemKey]?(self.state[item.itemKey])(
                                    {
                                        fieldid:item.itemKey,
                                        value:DBValue,
                                            isMultiSelectedEnabled:true,
                                        queryCondition:() => {
                                            return Object.assign({
                                                "pk_accountingbook": listItem.pk_accountingbook.value? listItem.pk_accountingbook.value:'',
                                                "TreeRefActionExt": 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                            },config)
                                        },
                                        onChange:(v)=>{
                                            //根据选定的pk 实现过滤
                                            if(item.itemKey!='pk_accountingbook'&&!listItem.pk_accountingbook.value){
                                                toast({ content: this.state.json['20020VRIFYHISTORY-000047'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
                                                return false
                                            }
                                            listItem[item.itemKey]=[];
                                            v.map((arr,index)=>{
                                                let accasoa={
                                                    display:arr.refname,
                                                    value:arr.refpk
                                                }
                                                listItem[item.itemKey].push(accasoa);
                                            })
                                            if(v[0]){
                                                childAssData.pk_org = v[0].refpk;
                                                assidCondition.pk_org=v[0].refpk;
                                            }else{
                                                childAssData.pk_org =childAssData.accountingbook_org;
                                                assidCondition.pk_org=childAssData.accountingbook_org;
                                            }
                                            
                                            //辅助核算项的值
                                            assData.map((item,index)=>{
                                                item.checkvaluename =null;
                                                item.pk_Checkvalue = null;
                                                item.checkvaluecode = null;
                                            })
                                            // listItem[item.itemKey].value = v.refpk
                                            // listItem[item.itemKey].display = v.refname
                                            this.setState({
                                                listItem,childAssData,assidCondition
                                            })
                                        }
                                    }
                                    ):<div/>}
                                </FormItem>)
                                }else{
                                    return(<div/>)
                                }
                                
                            }else{
                                return(
                                    <FormItem
                                        inline={true}
                                        showMast={item.isMustItem}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={10} md={10} sm={10}
                                        labelName={item.itemName}
                                        //isRequire={true}
                                        method="change"
                                    >
                                {self.state[item.itemKey]?(self.state[item.itemKey])(
                                    {
                                        fieldid:item.itemKey,
                                        value:defaultValue,
                                        queryCondition:() => {
                                                return Object.assign({
                                                    //"pk_accountingbook": self.state.pk_accountingbook.value
                                                },config)
                                            },
                                        onChange:(v)=>{                                           
                                                listItem[item.itemKey].value = v.refpk
                                                listItem[item.itemKey].display = v.refname
                                                this.setState({
                                                    listItem
                                                })
                                        }
                                        }
                                    ):<div/>}
                                </FormItem>)
                            }
                        }
                        break;
                        case 'date':
                            return (
                                    <FormItem
                                        inline={true}
                                        showMast={false}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        method="blur"
                                        inputAlfer="%"
                                        errorMessage={this.state.json['20020VRIFYHISTORY-000048']}/* 国际化处理： 输入格式错误*/
                                    >
                                        <DatePicker
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                            //isRequire={true}
                                            // format={format}
                                            //disabled={isChange}
                                            // value={moment(listItem[item.itemKey].value)}
                                            value={listItem[item.itemKey].value}
                                            // locale={zhCN}
                                            onChange={(v) => {
                                                listItem[item.itemKey].value = v;
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                            placeholder={dateInputPlaceholder}
                                            />
                                        </FormItem>
                            );
                        case'Dbdate':
                            return(
                                // <Row>
                                // <Col xs={12} md={12} sm={12} className="dateMargin">
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10} md={10} sm={10}
                                    labelName={item.itemName}
                                    method="blur"
                                    inputAlfer="%"
                                    errorMessage={this.state.json['20020VRIFYHISTORY-000048']}/* 国际化处理： 输入格式错误*/
                                    inputAfter={
                                        <Col xs={12} md={12} sm={12}>
											<span className="online">&nbsp;--&nbsp;</span>
											<div style={{display:'inline-block'}} >
												<DatePicker
													fieldid="end_date"
													name={item.itemKey}
													type="customer"
												// isRequire={true}
													// format={format}
													// value={moment(listItem.end_date.value)}
													value={listItem.end_date.value}
													// locale={zhCN}
													onChange={(v) => {
														listItem.end_date={value: v}
														this.setState({
															listItem
														})
													}}
													placeholder={dateInputPlaceholder}
												/>
											</div>
										</Col>
									}
                                    >
                                    <DatePicker
                                        fieldid="begin_date"
                                        name={item.itemKey}
                                        type="customer"
                                       // isRequire={true}
                                        // format={format}
                                        //disabled={isChange}
                                        // value={moment(listItem.begin_date.value)}
                                        value={listItem.begin_date.value}
                                        // locale={zhCN}
                                        onChange={(v) => {
                                            listItem.begin_date={value: v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    />
                                </FormItem>
                                // </Col>
                                // </Row>
                            );
                        case 'textInput':
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                   // isRequire={true}
                                    method="change"
                                >
                                    <FormControl
                                        fieldid={item.itemKey}
                                        value={listItem[item.itemKey].value}
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            );
                        case 'DbtextInput':
                            return(
                            // <Row>
                            //     <Col xs={12}  md={12} sm={12} className="dateMargin">
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    labelXs={2}  labelSm={2} labelMd={2}
                                    xs={10}  md={10} sm={10}
                                    labelName={item.itemName}
                                   // isRequire={true}
                                    method="change"
                                    inputAfter={
                                    <Col xs={12} md={12} sm={12}>
                                        <span className="online">&nbsp;--&nbsp;</span>     
										<div style={{display:'inline-block'}} >
                                        <NCNumber 
                                            fieldid="mny_end"
                                            scale={2}
                                            value={listItem.mny_end.value}
                                            onChange={(v) => {
                                                listItem.mny_end={value : v}
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                        />    
										</div> 
                                    </Col>}
                                >
                                    <NCNumber 
                                        fieldid="mny_begin"
                                        scale={2}
                                        value={listItem.mny_begin.value}
                                        onChange={(v) => {
                                            listItem.mny_begin={value : v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            //     </Col>                                
                            // </Row>
                        );
                        case 'radio':
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                               // isRequire={true}
                                method="change"
                            //  change={self.handleGTypeChange.bind(this, 'contracttype')}
                            >
                                    <RadioItem
                                        fieldid={item.itemKey}
                                        name={item.itemKey}
                                        type="customer"
                                        defaultValue={listItem[item.itemKey].value}
                                        items={() => {
                                            return (item.itemChild) 
                                        }}
                                        onChange={(v)=>{
                                            listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                        }}
                                    />
                            </FormItem>
                            )
                        case 'select':
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                                //isRequire={true}
                                method="change"
                            //  change={self.handleGTypeChange.bind(this, 'contracttype')}
                            >
                                <SelectItem name={item.itemKey}
                                    fieldid={item.itemKey}
                                //  defaultValue={this.state[item.itemKey].value?this.state[item.itemKey].value:'all'} 
                                        items = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{
                                           
                                            listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                        }}
                            />
                            </FormItem>)
                        case 'checkbox':
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                xs={6}
                                md={6}
                                sm={6}
                                // labelName={item.itemName}
                                // isRequire={true}
                                method="change"
                            //  change={self.handleGTypeChange.bind(this, 'contracttype')}
                            >
                                    <CheckboxItem name={item.itemKey} 
                                    //defaultValue={this.state.periodloan.value} 
                                        boxs = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{
                                        
                                        }}
                                />
                            </FormItem>
                            )
                        default:
                        break;
                   }
               })
			):<div/>;
           
    }


    onAllCheckChange = () => {
        let self = this;
        let checkedArray = [];
        let selIds = [];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          checkedArray[i] = !self.state.checkedAll;
        }
        self.setState({
          checkedAll: !self.state.checkedAll,
          checkedArray: checkedArray,
        });
      };
      onCheckboxChange = (text, record, index) => {
        let self = this;
        let allFlag = false;
        let checkedArray = self.state.checkedArray.concat();
        checkedArray[index] = !self.state.checkedArray[index];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          if (!checkedArray[i]) {
            allFlag = false;
            break;
          } else {
            allFlag = true;
          }
        }
        self.setState({
          checkedAll: allFlag,
          checkedArray: checkedArray,
        });
      };
      
      renderColumnsMultiSelect(columns) {
        const {checkedArray } = this.state;
        const { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        // let indeterminate_bool1 = true;
        if (multiSelect && multiSelect.type === "checkbox") {
          let i = checkedArray.length;
          while(i--){
              if(checkedArray[i]){
                indeterminate_bool = true;
                break;
              }
          }
          let defaultColumns = [
            {
              title: (
                <Checkbox
                  className="table-checkbox"
                  checked={this.state.checkedAll}
                  indeterminate={indeterminate_bool&&!this.state.checkedAll}
                  onChange={this.onAllCheckChange}
                />
              ),
              key: "checkbox",
              dataIndex: "checkbox",
              width: 50,
              render: (text, record, index) => {
                return (
                  <Checkbox
                    className="table-checkbox"
                    checked={this.state.checkedArray[index]}
                    onChange={this.onCheckboxChange.bind(this, text, record, index)}
                  />
                );
              }
            }
          ];
          columns = defaultColumns.concat(columns);
        }
        return columns;
      }

    render() {
        let{showOrHide}=this.props;
        let { loadData,assData,assidCondition,checkedAll,checkedArray,childAssData} =this.state;
        let dateInputPlaceholder = this.state.json['20020VRIFYHISTORY-000038'];/* 国际化处理： 选择日期*/
        let columns10 = [
            {
              title: this.state.json['20020VRIFYHISTORY-000039'],/* 国际化处理： 核算类型*/
              dataIndex: "checktypename",
              key: "checktypename",
              width: "30%",
              render: (text, record, index) => {
                return <span>{record.checktypename}</span>;
              }
            },
            {
              title: this.state.json['20020VRIFYHISTORY-000040'],/* 国际化处理： 核算内容*/
              dataIndex: "checkvaluename",
              key: "checkvaluename",
              render: (text, record, index) => {
                let { assData,childAssData } = this.state;
                let defaultValue = [];
                if (assData[index]["checkvaluename"]) {
                  assData[index]["checkvaluename"].split(",").map((item, _index) => {
                    defaultValue[_index] = { refname: item, refpk: "" };
                  });
                  assData[index]["pk_Checkvalue"].split(",").map((item, _index) => {
                    defaultValue[_index].refpk = item;
                  });
                } else {
                  defaultValue = [{ refname: "", refpk: "" }];
                }
                if (record.refnodename) {
                  let referUrl = record.refnodename + '.js';
                  if (!this.state[record.pk_accassitem]) {
                    { createScript.call(this, referUrl, record.pk_accassitem) }
                    return <div />
                  } else {
                    if (record.classid == 'b26fa3cb-4087-4027-a3b6-c83ab2a086a9' || record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                      return (
                        <FormItem
                          inline={true}
                          // showMast={true}
                          labelXs={2} labelSm={2} labelMd={2}
                          xs={10} md={10} sm={10}
                          //labelName={record.itemName}
                          //isRequire={true}
                          method="change"
                        >
                          {this.state[record.pk_accassitem] ? (this.state[record.pk_accassitem])(
                            {
                              value: defaultValue,
                              isShowUnit: true,
                              unitProps: {
                                refType: 'tree',
                                refName: this.state.json['20020VRIFYHISTORY-000008'],/* 国际化处理： 业务单元*/
                                refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                rootNode: { refname: this.state.json['20020VRIFYHISTORY-000008'], refpk: 'root' },/* 国际化处理： 业务单元*/
                                placeholder: this.state.json['20020VRIFYHISTORY-000008'],/* 国际化处理： 业务单元*/
                                queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                treeConfig: { name: [this.state.json['20020VRIFYHISTORY-000041'], this.state.json['20020VRIFYHISTORY-000042']], code: ['refcode', 'refname'] },/* 国际化处理： 编码,名称*/
                                isMultiSelectedEnabled: false,
                                //unitProps:unitConf,
                                isShowUnit: false
                              },
                              unitCondition:{
                                pk_financeorg:childAssData.pk_org,
                                'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                              },
                              isMultiSelectedEnabled: true,
                              "unitValueIsNeeded":false,
                              "isShowDimission":true,
                              queryCondition: () => {
                                // if (record.classid && record.classid.length == 20) {//classid的长度大于20的话过滤条件再加一个pk_defdoclist
                                //   return Object.assign({
                                //     "pk_org": childAssData.pk_org,
                                //     "pk_defdoclist": record.pk_defdoclist
                                //   },config)
                                // } else {
                                //   if (record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f') {//人员
                                config.isShowDimission=true;    
                                return Object.assign({
                                      "busifuncode": "all",
                                      "pk_org": childAssData.pk_org
                                    },config)
                                //   } else {
                                //     return Object.assign({
                                //       "pk_org": childAssData.pk_org
                                //     },config)
                                //   }
      
                                // }
                              },
                              onChange: (v) => {
                                let { assData } = this.state;
                                let originData = this.findByKey(record.key, assData);
                                let refnameArr = [], refpkArr = [], refcodeArr = [];
                                if (originData) {
                                  v.map((arr, index) => {
                                    refnameArr.push(arr.refname);
                                    refpkArr.push(arr.refpk);
                                    refcodeArr.push(arr.refcode);
      
                                  })
                                  originData.checkvaluename = refnameArr.join();
                                  originData.pk_Checkvalue = refpkArr.join();
                                  originData.checkvaluecode = refcodeArr.join();
                                }
                                childAssData.assData = assData;
                                this.setState({
                                  assData, childAssData
                                })
                              }
                            }) : <div />}
                        </FormItem>);
                    } else {
                      return (
                        <FormItem
                          inline={true}
                          // showMast={true}
                          labelXs={2} labelSm={2} labelMd={2}
                          xs={10} md={10} sm={10}
                          //labelName={record.itemName}
                         // isRequire={true}
                          method="change"
                        >
                          {this.state[record.pk_accassitem] ? (this.state[record.pk_accassitem])(
                            {
                              value: defaultValue,
                              isMultiSelectedEnabled: true,
                              queryCondition: () => {
                                if (record.classid && record.classid.length == 20) {//classid的长度等于20的话过滤条件再加一个pk_defdoclist
                                  return Object.assign({
                                    "pk_org": childAssData.pk_org,
                                    "pk_defdoclist": record.pk_defdoclist
                                  },config)
                                } else {
                                  return Object.assign({
                                    "pk_org": childAssData.pk_org
                                  },config)
                                }
                              },
                              onChange: (v) => {
                                let { assData } = this.state;
                                let originData = this.findByKey(record.key, assData);
                                let refnameArr = [], refpkArr = [], refcodeArr = [];
                                if (originData) {
                                  v.map((arr, index) => {
                                    refnameArr.push(arr.refname);
                                    refpkArr.push(arr.refpk);
                                    refcodeArr.push(arr.refcode);
      
                                  })
                                  originData.checkvaluename = refnameArr.join();
                                  originData.pk_Checkvalue = refpkArr.join();
                                  originData.checkvaluecode = refcodeArr.join();
                                }
                                childAssData.assData = assData;
                                this.setState({
                                  assData, childAssData
                                })
                              }
                            }) : <div />}
                        </FormItem>);
                    }
                  }
                } else {//不是参照的话要区分日期、字符、数值
                  if (record.classid == 'BS000010000100001033') {//日期
                    return (
                      <DatePicker
                        //name={item.itemKey}
                        type="customer"
                       // isRequire={true}
                        placeholder={dateInputPlaceholder}
                        value={defaultValue[0].refname}
                        onChange={(v) => {
                          let { assData } = this.state;
                          let originData = this.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = assArr;
                            originData.pk_Checkvalue = assArr;
                            originData.checkvaluecode = assArr;
                          }
                          childAssData.assData = assData;
                          this.setState({
                            assData, childAssData
                          })
                        }}
      
                      />
                    )
                  } else if (record.classid == 'BS000010000100001031') {//数值
                    return (
                      <NCNumber
                        scale={2}
                        value={defaultValue[0].refname}
                        placeholder={this.state.json['20020VRIFYHISTORY-000043']}/* 国际化处理： 请输入数字*/
                        onChange={(v) => {
                          let { assData } = this.state;
                          let originData = this.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = assArr;
                            originData.pk_Checkvalue = assArr;
                            originData.checkvaluecode = assArr;
                          }
                          childAssData.assData = assData;
                          this.setState({
                            assData, childAssData
                          })
                        }}
                      />
                    )
                  } else if (record.classid == 'BS000010000100001032') {//布尔
                    return (
                      <FormItem
                        inline={true}
                        // showMast={false}
                        labelXs={2}
                        labelSm={2}
                        labelMd={2}
                        xs={10}
                        md={10}
                        sm={10}
                        // labelName={item.itemName}
                        //isRequire={true}
                        method="change"
                      >
                        <SelectItem name={record.checktypecode}
                          defaultValue={defaultValue[0].refname}
                          items={
                            () => {
                              return ([{
                                label: this.state.json['20020VRIFYHISTORY-000044'],/* 国际化处理： 是*/
                                value: 'Y'
                              }, {
                                label: this.state.json['20020VRIFYHISTORY-000045'],/* 国际化处理： 否*/
                                value: 'N'
                              }])
                            }
                          }
                          onChange={(v) => {
                            let { assData } = this.state;
                            let originData = this.findByKey(record.key, assData);
                            if (originData) {
                              let assArr = [];
                              assArr.push(v);
                              originData.checkvaluename = assArr;
                              originData.pk_Checkvalue = assArr;
                              originData.checkvaluecode = assArr;
                            }
                            childAssData.assData = assData;
                            this.setState({
                              assData, childAssData
                            })
                          }}
                        />
                      </FormItem>
                      // <NCNumber
                      //     scale={2}
                      //     value={defaultValue[0].refname}
                      //     placeholder={'请输入数字'}
                      //     onChange={(v)=>{
                      // let { assData } =this.state;
                      // let originData = this.findByKey(record.key, assData);
                      // if (originData) {
                      //     let assArr=[];
                      //     assArr.push(v);
                      //     originData.checkvaluename = assArr;
                      //     originData.pk_Checkvalue =assArr;
                      //     originData.checkvaluecode=assArr;
                      // }
                      // childAssData.assData=assData; 
                      // this.setState({
                      // assData,childAssData
                      // })
                      //     }}
                      // />
                    )
                  } else {//字符
                    return (
                      <FormControl
                        value={defaultValue[0].refname}
                        onChange={(v) => {
                          let { assData } = this.state;
                          let originData = this.findByKey(record.key, assData);
                          if (originData) {
                            let assArr = [];
                            assArr.push(v);
                            originData.checkvaluename = assArr;
                            originData.pk_Checkvalue = assArr;
                            originData.checkvaluecode = assArr;
                          }
                          childAssData.assData = assData;
                          this.setState({
                            assData, childAssData
                          })
                        }}
                      />
                    )
                  }
      
                }
              }
            }
          ];
        let columnsldad = this.renderColumnsMultiSelect(columns10);
        let pretentAssData={
            assData: assData||[],
            childAssData:childAssData||[],
            checkboxShow: true,//是否显示复选框
            showOrHide:true,
            checkedAll:checkedAll,//全选复选框的选中状态
            checkedArray:checkedArray||[],//复选框选中情况
            $_this:this
          };
        assidCondition.assData=assData;
        const emptyFunc = () => <span>{this.state.json['20020VRIFYHISTORY-000050']}！</span>/* 国际化处理： 这里没有数据*/
        return (
            <div className="fl">
                <Modal
                    fieldid="query"
                    className={'combine'}
                    id="queryone"
                    show={showOrHide }
                    // backdrop={ this.state.modalDropup }
                    onHide={ this.close }
                    animation={true}
                    >
                    <Modal.Header closeButton fieldid="header-area">
                        <Modal.Title>{this.state.json['20020VRIFYHISTORY-000051']}</Modal.Title>{/* 国际化处理： 凭证查询*/}
                    </Modal.Header >
                    <Modal.Body >
                        <div fieldid="query_form-area">
                            <NCForm useRow={true}
                                submitAreaClassName='classArea'
                                showSubmit={false}　>
                                {loadData.length > 0 ? this.queryList(loadData) : ''}
                            </NCForm>
                        </div>
                        <div className="getAssDatas">
                        {/* <Table
                            columns={columnsldad} data={assData}
                            emptyText={emptyFunc}
                        /> */}
                        {getAssDatas({pretentAssData})}
                        </div>
                    </Modal.Body>
                    <Modal.Footer fieldid="bottom_area">
                        <Button colors="primary" onClick={ this.confirm } fieldid="confirm"> {this.state.json['20020VRIFYHISTORY-000032']} </Button>{/* 国际化处理： 查询*/}
                        <Button onClick={ this.close } fieldid="close"> {this.state.json['20020VRIFYHISTORY-000006'] }</Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
SearchModal.defaultProps = defaultProps12;
