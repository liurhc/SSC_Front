import React, { Component } from 'react';
import {high,base,ajax,getBusinessInfo,createPage,getMultiLang} from 'nc-lightapp-front';
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../../public/components/FormItems';
const { Refer} = high;
import { toast } from '../../../../public/components/utils.js';
import createScript from '../../../../public/components/uapRefer.js';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox:Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm,NCDiv
} = base;
import '../index.less'
import getAssDatas from "../../../../public/components/getAssDatas/index.js";
import RatioSettingModal from '../ratioSettingModal/ratioSettingModal.js';
import checkMustItem from "../../../../public/common/checkMustItem.js";
const {  NCFormItem:FormItem } = NCForm;
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};
  const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
  };
export default class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            verifyRangeDisabled:false,//核销范围是否可编辑
            accountAgeDisabled:true,//收款账龄分析的控制 默认不可用
            analysisTypeDisabled:false,//分析方式
            directDisabled:false,//分析方向
            virtualDisabled:false,//收款账龄分析是否可选
            showModal:false,
            isShowUnit:false,//是否显示业务单元
            // pk_org:{display:'',value:''},//根据账簿获取的unint的值
            assData: [//辅助核算信息
            ],
            ratioData:[],//比率数据
            modalDefaultValue:{},//默认数据
            childAssData: {
                accountingbook_org:'',//保存账簿对应的组织
                pk_org: '',
                pk_accountingbook: ''
            },//接受父组件传过来的参数
            SelectedAssData:[],//选中的数据
            loadData:[],//查询模板加载数据
            listItem:{},//模板数据对应值
            checkedAll:true,
            checkedArray: [],
            rateflag:false
        };
        this.close = this.close.bind(this);
    }
	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
		getMultiLang({moduleId:['20021DLANL','publiccommon','publiccomponents'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentWillReceiveProps (nextProp) {
        let {loadData,showOrHide,modalDefaultValue,resourveQuery}=nextProp;//this.props
        let self=this;
        let { listItem,showModal,isShowUnit,childAssData }=self.state;      
        if (showOrHide&&nextProp.loadData !== self.state.loadData&&self.state.loadData.length==0 ) {
            childAssData.pk_org=modalDefaultValue.pk_org;
            childAssData.accountingbook_org=modalDefaultValue.pk_org;
            isShowUnit=modalDefaultValue.isShowUnit;
            loadData.forEach((item,i)=>{
                let  key;
                if(item.itemType=='refer'){
                    if(item.itemKey=='pk_accasoa'||item.itemKey=='pk_units'){
                    
                        key=[{
                            display:'',
                            value:'',
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }]
                    }else if(item.itemKey=='pk_accountingbook'){
                        key=[{
                            display:modalDefaultValue.pk_accountingbook.display,
                            value:modalDefaultValue.pk_accountingbook.value,
                            isMustItem:item.isMustItem,itemName:item.itemName
                        }]
                    }else if(item.itemKey=='pk_currency'){
                        key=[{
                            display:modalDefaultValue.pk_currency.display,
                            value:modalDefaultValue.pk_currency.value,
                            isMustItem:item.isMustItem,itemName:item.itemName
                        }]
                    }else{
                        key={
                            display:'',
                            value:'',
                            isMustItem:item.isMustItem,itemName:item.itemName
                        }
                    } 
                }else if(item.itemType=='select'||item.itemType=='Dbselect'||item.itemType=='radio'){//下拉框赋初始值
                    if(item.itemKey=='verifyRange'){
                        key={
                            value:item.itemChild[1].value
                        }
                    }else{
                        key={
                            value:item.itemChild[0].value
                        }
                    }
                }else if(item.itemType=='checkbox'){
                    key={
                        value:'N'
                    }
                }else{
                    key={
                        value:''
                    }
                    if(item.itemType=='date'||item.itemType=='Dbdate'){
                        if(item.itemKey=='endDate'){
                            key = {
                              value:modalDefaultValue.bizDate
                            };
                          }else{
                            key = {
                              value: ""
                            };
                          }
                    }
                }
                if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'){
                    item.itemKey.map((k,index)=>{
                        let name= k;
                        listItem[name]=key
                    });
                }else{
                    let name= item.itemKey;
                    listItem[name]=key
                }
            
            })
            listItem.acccode=[];//科目编码
            listItem.accname=[];//科目名称
            listItem.curName=[];//币种名称
            // listItem['beginDate']={value:modalDefaultValue.yearbegindate};
            listItem['endDate']={value:modalDefaultValue.bizDate};
            listItem.pk_org=modalDefaultValue.pk_org;//组织
            self.setState({
                modalDefaultValue:modalDefaultValue,
                loadData:loadData,
                showModal:showOrHide,
                listItem,childAssData,isShowUnit
            })
        }else{
                self.setState({
                    showModal:showOrHide,childAssData,isShowUnit
                })
        }
    }

    //表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

    close() {
        this.props.handleClose();
    }

    confirm=()=>{
        let { listItem,assData,checkedArray,SelectedAssData } =this.state;
        SelectedAssData=[];
        let checkStatus=checkMustItem(listItem);//必输项校验
        if(!checkStatus.flag){
            toast({content:checkStatus.info+this.state.json['publiccommon-000001'],color:'warning'});
            return false;
        }
        if(listItem['accountAge'].value=='Y'&&!listItem.startDate.value){
            toast({content:this.state.json['20021DLANL-000062'],color:'warning'});/* 国际化处理： 勾选收款账龄分析选项时，启用日期不能为空*/
            return false;
        }
        for(var i = 0; i < checkedArray.length; i++){
            if(checkedArray[i]==true){
                SelectedAssData.push(assData[i]);
            }
        }
        listItem.ass=SelectedAssData;//assData;
        this.props.onConfirm(listItem);
    }
    
    dealRedio=(item) => {
        let{analysisTypeDisabled,directDisabled,verifyRangeDisabled}=this.state;
        if(item.itemKey=="analysisType"){//分析方式
            item.itemChild.map((k,index)=>{
                k.disabled=analysisTypeDisabled
            })
        }
        if(item.itemKey=="direct"){//分析方向
            item.itemChild.map((k,index)=>{
                k.disabled=directDisabled
            })
        }
        if(item.itemKey=='verifyRange'){//核销范围
            item.itemChild.map((k,index)=>{
                k.disabled=verifyRangeDisabled
            })
        }
        return (item.itemChild)
     }
     dealRedio_verifyRange=(item) => {
        let{analysisTypeDisabled,directDisabled,verifyRangeDisabled}=this.state;
        if(item.itemKey=='verifyRange'){//核销范围
            item.itemChild.map((k,index)=>{
                k.disabled=verifyRangeDisabled
            })
        }
        return (item.itemChild)
     }
    queryList=(data)=>{
        let self=this;
        const dateInputPlaceholder = self.state.json['20021DLANL-000040'];/* 国际化处理： 选择日期*/
        function getNowFormatDate() {
            let date = new Date();
            let seperator1 = "-";
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            let currentdate = year + seperator1 + month + seperator1 + strDate;
            return currentdate;
        }
        // let currrentDate = getNowFormatDate();
        let businessInfo = getBusinessInfo();
        let currrentDate = businessInfo.businessDate.split(' ')[0]; 
        let { listItem,isShowUnit,pk_org,assData,loadData,checkedArray,childAssData,modalDefaultValue,
            accountAgeDisabled,analysisTypeDisabled,directDisabled,verifyRangeDisabled,virtualDisabled,rateflag } =self.state;
			return data.length!=0?(
				data.map((item, i) => {                   
                   switch (item.itemType) {
                        case 'refer':
                            let referUrl= item.config.refCode+'/index.js';
                            let DBValue=[];
                            let defaultValue={}
                            if(listItem[item.itemKey].length){                           
                                listItem[item.itemKey].map((item,index)=>{
                                    DBValue[index]={ refname: item.display, refpk:item.value };
                                })
                            }else{
                                defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
                            }
                            //let defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
                            if(!self.state[item.itemKey]){
                                {createScript.call(self,referUrl,item.itemKey)}
                                return <div />
                            }else{
                                if(item.itemKey=='pk_accountingbook'){
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                            >
                                            {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                {
                                                    fieldid:item.itemKey,
                                                    value:DBValue,
                                                    isMultiSelectedEnabled:true,
                                                    showGroup:false,
                                                    disabledDataShow:true,
                                                    queryCondition:() => {
                                                        return Object.assign({
                                                            "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                                            "appcode":modalDefaultValue.appcode
                                                        },config)
                                                    },
                                                    onChange:(v)=>{
                                                       
                                                        if(v.length>0){
                                                            //判断是否起用业务单元
                                                            let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
                                                            let pk_accpont={};
                                                            if(v.length>0){
                                                                pk_accpont = {"pk_accountingbook":v[0].refpk}
                                                            }else{
                                                                toast({ content: this.state.json['20021DLANL-000043'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
                                                                return false
                                                            }
                                                         
                                                            ajax({
                                                                url:url,
                                                                data:pk_accpont,
                                                                async:false,
                                                                success: function(response){
                                                                    const { success } = response;
                                                                    //渲染已有账表数据遮罩
                                                                    if (success) {
                                                                        if(response.data){
                                                                            isShowUnit=response.data.isShowUnit;
                                                                            childAssData.pk_org = response.data.unit.value;
                                                                            childAssData.accountingbook_org=response.data.unit.value;
                                                                            listItem.pk_org= response.data.unit.value;
                                                                            // pk_org.display=response.data.unit.display;
                                                                            // pk_org.value=response.data.unit.value;
                                                                            listItem['pk_currency'][0].value = response.data.currinfo.value;
                                                                            listItem['pk_currency'][0].display = response.data.currinfo.display;
                                                                            // listItem['startDate']={value:response.data.yearbegindate};
                                                                            listItem['endDate']={value:response.data.bizDate};
                                                                        }
                                                                        self.setState({
                                                                            listItem,isShowUnit,childAssData
                                                                        })
                                                                    }   
                                                                }
                                                            });
                                                        }
                                                        listItem[item.itemKey]=[];
                                                        v.map((arr,index)=>{
                                                            let accountingbook={
                                                                display:arr.refname,
                                                                value:arr.refpk
                                                            }
                                                            listItem[item.itemKey].push(accountingbook);
                                                        })
                                                        if(v.length>1){//多选账簿不显示业务单元
                                                            isShowUnit=false;
                                                        }
                                                        //情空科目
                                                      
                                                        if(listItem['pk_accasoa'].length>0){
                                                            listItem['pk_accasoa'][0].display='';
                                                        }
                                                        if(listItem['pk_accasoa'].length>0){
                                                            listItem['pk_accasoa'][0].value='';
                                                        }
            
                                                        if(listItem['pk_units'].length>0){
                                                            listItem['pk_units'][0].value='';
                                                        }                                             
                                                        self.setState({
                                                            listItem,isShowUnit,pk_org,childAssData
                                                        })
                                                    }
                                                }
                                            ):<div />}
                                    </FormItem>);
                                }else if(item.itemKey=='pk_accasoa'){
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                        >
                                        {self.state[item.itemKey]?(self.state[item.itemKey])(
                                            {
                                                fieldid:item.itemKey,
                                                value:DBValue,
                                                isMultiSelectedEnabled:true,
                                                queryCondition:() => {
                                                    if(item.itemKey=='pk_accasoa'){
                                                        return Object.assign({
                                                            "refName":item.itemName,
                                                            "pk_accountingbook": listItem.pk_accountingbook[0].value? listItem.pk_accountingbook[0].value:'',
                                                            "dateStr":currrentDate,
                                                            "TreeRefActionExt":'nccloud.web.gl.verify.action.VerifyObjectRefSqlBuilder'
                                                            
                                                        },config)
                                                    }
                                                },
                                                onFocus:(v)=>{
                                                    if(listItem.pk_accountingbook.length==0){
                                                        toast({content:this.state.json['20021DLANL-000063'],color:'warning'});/* 国际化处理： 请先选择核算账簿，核算账簿不能为空*/
                                                        return false;
                                                    }else{
                                                        if(listItem.pk_accountingbook[0].value&&listItem.pk_accountingbook[0].refpk){
                                                            toast({content:this.state.json['20021DLANL-000063'],color:'warning'});/* 国际化处理： 请先选择核算账簿，核算账簿不能为空*/
                                                            return false;
                                                        }
                                                    }
                                                },
                                                onChange:(v)=>{
                                                    let balanorient
                                                    if(v.length>0){
                                                        balanorient=v[0].nodeData.balanorient;
                                                    }else{
                                                        balanorient='0';//默认借方
                                                    }
                                                    
                                                    if(balanorient=="0"){//0代表借方 1代表贷方
                                                        virtualDisabled=false;
                                                    }else if(balanorient=="1"){
                                                        listItem['accountAge'].value='N';
                                                        virtualDisabled=true;
                                                    }
                                                    listItem[item.itemKey]=[];
                                                    listItem.acccode=[];
                                                    listItem.accname=[];
                                                    let paramAccasoa=[];
                                                    //listItem[item.itemKey].shift();
                                                    v.map((arr,index)=>{
                                                        paramAccasoa.push(arr.refpk);
                                                        listItem.acccode.push(arr.refcode);
                                                        listItem.accname.push(arr.refname);
                                                        let accasoa={
                                                            display:arr.refname,
                                                            value:arr.refpk
                                                        }
                                                        listItem[item.itemKey].push(accasoa);
                                                    })
                                                    //根据选定的pk 实现过滤
                                                    if(item.itemKey!='pk_accountingbook'&&!listItem.pk_accountingbook[0].value){
                                                        toast({ content: this.state.json['20021DLANL-000043'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
                                                        return false
                                                    }
                                                    //请求辅助核算数据
                                                    let url = '/nccloud/gl/voucher/queryAssItem.do';
                                                    let queryData={};
                                                    if(v.length>0){
                                                        queryData = {
                                                            pk_accasoa:paramAccasoa,
                                                            prepareddate: currrentDate,
                                                        };
                                                    }else{
                                                        queryData = {
                                                            pk_accasoa: '',
                                                            prepareddate: currrentDate,
                                                        };
                                                    }
                                                    
                                                    assData=[];
                                                    checkedArray=[];//选中的数据
                                                    if(v.length>0){
                                                   
                                                        ajax({
                                                            url:url,
                                                            data:queryData,
                                                            success: function(response){
                                                                const { success } = response;
                                                                //渲染已有账表数据遮罩
                                                                if (success) {
                                                                    if(response.data){
                                                                        if(response.data.length>0){
                                                                            response.data.map((item,index)=>{
                                                                                checkedArray.push(true);
                                                                                let obj={
                                                                                    key:index,
                                                                                    "checktypecode":item.code,
                                                                                    "checktypename" :item.name,
                                                                                    "pk_Checktype": item.pk_accassitem,
                                                                                    "refCode":item.refCode?item.refCode:item.code,
                                                                                    "refnodename":item.refnodename,
                                                                                    "pk_accassitem":item.pk_accassitem,
                                                                                    "pk_accountingbook":listItem.pk_accountingbook[0].value,
                                                                                    "classid": item.classid,
                                                                                    "pk_defdoclist": item.classid,
                                                                                    "pk_accountingbook": childAssData.pk_accountingbook
                                                                                    }
                                                                                assData.push(obj);
                                                                            })
                                                                        }
                                                                    }
                                                                    self.setState({
                                                                        assData,checkedArray
                                                                    })
                                                                }   
                                                            }
                                                        });
                                                    }
                                                    // listItem[item.itemKey].value = v.refpk
                                                    // listItem[item.itemKey].display = v.refname
                                                    self.setState({
                                                        listItem,virtualDisabled,assData,checkedArray
                                                    })
                                                }
                                            }
                                        ):<div />}
                                    </FormItem>);
                                }else if(item.itemKey=='pk_units'){
                                    if(isShowUnit){
                                     
                                        return (
                                            <FormItem
                                                inline={true}
                                                showMast={item.isMustItem}
                                                labelXs={2} labelSm={2} labelMd={2}
                                                xs={4} md={4} sm={4}
                                                className='pk_unitspan'
                                                labelName={item.itemName}
                                                isRequire={true}
                                                method="change" 
                                            >
                                                {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                    {
                                                        fieldid:item.itemKey,
                                                        value:DBValue,
                                                        isMultiSelectedEnabled:true,
                                                        queryCondition:() => {
                                                        
                                                            return Object.assign({
                                                                
                                                                "pk_accountingbook": listItem.pk_accountingbook[0].value? listItem.pk_accountingbook[0].value:'',
                                                                "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                                            },config)
                                                        },
                                                        onChange:(v)=>{
                                                            //根据选定的pk 实现过滤
                                                        
                                                            if(item.itemKey!='pk_accountingbook'&&!listItem.pk_accountingbook[0].value){
                                                                toast({ content: this.state.json['20021DLANL-000043'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
                                                                return false
                                                            }
                                                            
                                                            listItem[item.itemKey]=[];
                                                            v.map((arr,index)=>{
                                                                let accasoa={
                                                                    display:arr.refname,
                                                                    value:arr.refpk
                                                                }
                                                                listItem[item.itemKey].push(accasoa);
                                                            })      
                                                            if(v[0]){
                                                                listItem.pk_org=v[0].refpk;
                                                                childAssData.pk_org = v[0].refpk;
                                                            }else{
                                                                listItem.pk_org=childAssData.accountingbook_orgv;
                                                                childAssData.pk_org =childAssData.accountingbook_org;
                                                            }
                                                            //辅助核算项的值
                                                            assData.map((item,index)=>{
                                                                item.checkvaluename = null;
                                                                item.pk_Checkvalue = null;
                                                                item.checkvaluecode = null;
                                                            })
                                                            this.setState({
                                                                listItem,childAssData,assData
                                                            })
                                                        }
                                                    }
                                                ):<div />}
                                        </FormItem>
                                        );
                                    }else{
                                        return (<div/>)
                                    }                               
                                }else if(item.itemKey=="pk_currency"){
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                            >
                                            {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                {
                                                    fieldid:item.itemKey,
                                                    value:DBValue,
                                                    isMultiSelectedEnabled:true,
                                                    queryCondition:() => {
                                                        return Object.assign({
                                                            //"pk_accountingbook": self.state.pk_accountingbook.value
                                                        },config)                                                     
                                                    },    
                                                    onChange:(v)=>{
                                                        listItem[item.itemKey]=[];
                                                        listItem.curName=[];
                                                        v.map((arr,index)=>{
                                                            listItem.curName.push(arr.refname);
                                                            let accasoa={
                                                                display:arr.refname,
                                                                value:arr.refpk
                                                            }
                                                            listItem[item.itemKey].push(accasoa);
                                                        })
                                                        this.setState({
                                                            listItem
                                                        })
                                                    }
                                                }
                                            ):<div />}
                                    </FormItem>);
                                }else{
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                            >
                                            {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                {
                                                    fieldid:item.itemKey,
                                                    value:defaultValue,
                                                    //isMultiSelectedEnabled:true,
                                                    queryCondition:() => {
                                                    
                                                        if(item.itemKey=='accoutAgePeriod'){//账龄
                                                            return Object.assign({
                                                                "pk_org": listItem.pk_org? listItem.pk_org:'',  
                                                            },config)
                                                        }else{
                                                            return Object.assign({
                                                                //"pk_accountingbook": self.state.pk_accountingbook.value
                                                            },config)
                                                        }                                                        
                                                    },    
                                                    onChange:(v)=>{
                                                     
                                                        if(item.itemKey=='accoutAgePeriod'){
                                                         
                                                            self.ratioSetting(v.refpk);
                                                        }
                                                        if(v.refpk&&listItem['accountAge'].value=='N'){//选了账龄之后坏账估算才可用
                                                            accountAgeDisabled=false;
                                                        }else{
                                                            accountAgeDisabled=true;
                                                        }
                                                        listItem[item.itemKey].value = v.refpk
                                                        listItem[item.itemKey].display = v.refname 
                                                        this.setState({
                                                            listItem,accountAgeDisabled
                                                        })
                                                    }
                                                }
                                            ):<div />}
                                    </FormItem>);
                                } 						   
                            }
                            break;
                        case 'date':
                            return (
                                    <FormItem
                                        inline={true}
                                        showMast={true}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        method="blur"
                                        inputAlfer="%"
                                        errorMessage={this.state.json['20021DLANL-000044']}/* 国际化处理： 输入格式错误*/
                                    >
                                        <DatePicker
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                            isRequire={true}
                                            value={listItem[item.itemKey].value}
                                            onChange={(v) => {
                                                listItem[item.itemKey].value = v;
                                                self.setState({
                                                    listItem
                                                })
                                            }}
                                            placeholder={dateInputPlaceholder}
                                            />
                                        </FormItem>
                            );
                        case'Dbdate':
                            return(
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10} md={10} sm={10}
                                    labelName={item.itemName}
                                    method="blur"
                                    inputAlfer="%"
                                    errorMessage={this.state.json['20021DLANL-000044']}/* 国际化处理： 输入格式错误*/
                                    inputAfter={
                                        <Col xs={12} md={12} sm={12}>
                                            <span className="online">{this.state.json['publiccommon-000005']/*至*/}</span>
                                            <div style={{display:'inline-block'}} >
                                                <DatePicker
                                                    fieldid='endDate'
                                                    name={item.itemKey}
                                                    type="customer"
                                                    isRequire={true}
                                                    value={listItem.endDate.value}
                                                    onChange={(v) => {
                                                        listItem.endDate={value: v}
                                                        self.setState({
                                                            listItem
                                                        })
                                                    }}
                                                    placeholder={dateInputPlaceholder}
                                                />
                                            </div>
                                        </Col>
                                    }
                                    >
                                    <DatePicker
                                        fieldid='startDate'
                                        name={item.itemKey}
                                        type="customer"
                                        isRequire={true}
                                        disabled={listItem['accountAge'].value=='N'?true:false}
                                        value={listItem.startDate.value}
                                        onChange={(v) => {
                                            listItem.startDate={value: v}
                                            self.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    />
                                </FormItem>
                            );
                        case 'textInput':
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                    <FormControl
                                        fieldid={item.itemKey}
                                        value={listItem[item.itemKey].value}
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            self.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            );
                        case 'DbtextInput':
                            return(
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                   labelXs={2}  labelSm={2} labelMd={2}
                                    xs={10}  md={10} sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                    inputAfter={
                                    <Col xs={4} md={4} sm={4}>
                                        <span className="online">{this.state.json['publiccommon-000005']/*至*/}</span>                                                                          
                                        <FormControl
                                            fieldid={item.itemKey[1]}
                                            value={listItem[item.itemKey[1]].value}
                                            onChange={(v) => {
                                                let startkey=item.itemKey[0];
                                                let endkey=item.itemKey[1];
                                                // listItem[item.itemKey].value = v
                                                listItem[endkey]={value : v}
                                                self.setState({
                                                    listItem
                                                })
                                            }}
                                        />
                                    </Col>}
                                >
                                    <FormControl
                                        fieldid={item.itemKey[0]}
                                        value={listItem[item.itemKey[0]].value}
                                        onChange={(v) => {
                                            listItem[item.itemKey[0]]={value : v}
                                            self.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            );
                        case 'radio':
                            if(directDisabled!=''&&item.itemKey=="direct"){//分析方向
                                return(
                                    <FormItem
                                        inline={true}
                                        showMast={false}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        isRequire={true}
                                        method="change"
                                    >
                                        <RadioItem
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                            defaultValue={listItem[item.itemKey].value}
                                            items={
                                                self.dealRedio.bind(self,item)
                                            }
                                            onChange={(v)=>{
                                                listItem[item.itemKey].value = v
                                                    self.setState({
                                                        listItem
                                                    })
                                            }}
                                        />
                                </FormItem>
                                )
                            }else if(item.itemKey=="verifyRange"){//核销范围
                                return(
                                    <FormItem
                                        inline={true}
                                        showMast={false}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        isRequire={true}
                                        method="change"
                                    >
                                        <RadioItem
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                            defaultValue={listItem[item.itemKey].value}
                                            items={
                                                self.dealRedio.bind(self,item)
                                            }
                                            onChange={(v)=>{
                                                listItem[item.itemKey].value = v
                                                    self.setState({
                                                        listItem
                                                    })
                                            }}
                                        />
                                </FormItem>
                                )
                            }else if(item.itemKey=="analysisType"){//分析方式
                                return(
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                    <RadioItem
                                        fieldid={item.itemKey}
                                        name={item.itemKey}
                                        type="customer"
                                        defaultValue={listItem[item.itemKey].value}
                                        items={
                                            self.dealRedio.bind(self,item)
                                        }
                                        onChange={(v)=>{
                                            if(v=='virtual'){//选中余额账龄分析, 分析方向为双向，收款账龄分析不可选
                                                listItem['direct'].value='';
                                                listItem['accountAge'].value='N';
                                                virtualDisabled=true;
                                                directDisabled=true;
                                            }else{
                                                listItem['direct'].value='';
                                                virtualDisabled=false;
                                                directDisabled=false;
                                            }
                                            listItem[item.itemKey].value = v
                                                self.setState({
                                                    listItem,virtualDisabled,directDisabled
                                                },()=>{
                                                    listItem['direct'].value='both';
                                                    self.setState({
                                                        listItem
                                                    })
                                                })
                                        }}
                                    />
                                </FormItem>
                                )
                            }else{
                                return(
                                    <FormItem
                                        inline={true}
                                        showMast={false}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        isRequire={true}
                                        method="change"
                                    >
                                        <RadioItem
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                            defaultValue={listItem[item.itemKey].value}
                                            items={
                                                self.dealRedio.bind(self,item)
                                            }
                                            onChange={(v)=>{
                                                listItem[item.itemKey].value = v
                                                    self.setState({
                                                        listItem
                                                    })
                                            }}
                                        />
                                </FormItem>
                                )
                            }
                        break;
                        case 'select':
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                <SelectItem 
                                    name={item.itemKey}
                                    fieldid={item.itemKey}
                                    defaultValue={listItem[item.itemKey].value} 
                                        items = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{
                                            listItem[item.itemKey].value = v
                                                self.setState({
                                                    listItem
                                                })
                                        }}
                            />
                            </FormItem>)
                            case 'Dbselect':
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={2}
                                md={2}
                                sm={2}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                <SelectItem 
                                    fieldid={item.itemKey}
                                    name={item.itemKey}
                                    defaultValue={item.itemChild[0].value?item.itemChild[0].value:''} 
                                        items = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{
                                            listItem[item.itemKey].value = v
                                            self.setState({
                                                listItem
                                            })
                                        }}
                            />
                            </FormItem>)
                        case 'checkbox':
                            if(item.itemKey=="badAccount"){//坏账估算
                                return(
                                    <FormItem
                                        inline={true}
                                       // showMast={false}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={10}  md={10} sm={10}
                                        labelName={item.itemName}
                                        // isRequire={true}
                                        className='checkboxStyleFirst'
                                        method="change"
                                        inputAfter={
                                        <Col xs={12}  md={12} sm={12}>
                                            <button fieldid="badAccount" colors="primary" disabled={listItem['badAccount'].value=='Y'?false:true} onClick={self.rateSetting.bind(self)}>{this.state.json['20021DLANL-000046']}</button>{/* 国际化处理： 比率设置*/}
                                        </Col>}
                                    >
                                    <CheckboxItem
                                        fieldid={item.itemKey}
                                        name={item.itemKey} 
                                        disabled={accountAgeDisabled}
                                        defaultValue={listItem[item.itemKey].value} 
                                        boxs = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{
                                            listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                            self.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                                )
                            }else if(item.itemKey=="multBusi"){//多业务单元合并
                                if(isShowUnit){
                                    return(<FormItem
                                        inline={true}
                                        // showMast={false}
                                        labelXs={1} labelSm={1} labelMd={1}
                                        xs={5}  md={5} sm={5}
                                        className='checkboxStyle one'
                                        method="change"
                                    >
                                        <CheckboxItem
                                            fieldid={item.itemKey}
                                            name={item.itemKey} 
                                            disabled={false}
                                            defaultValue={listItem[item.itemKey].value} 
                                            boxs = {
                                                () => {
                                                    return (item.itemChild) 
                                                }
                                            }
                                            onChange={(v)=>{
                                                listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                                self.setState({
                                                    listItem
                                                })
                                            }}
                                        />
                                    </FormItem>)
                                }else{
                                    return(<div/>)
                                }
                                
                            }else if(item.itemKey=='accountAge'){//收款账龄分析
                                return(<FormItem
                                    inline={true}
                                    // showMast={false}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10}  md={10} sm={10}
                                    className='checkboxStyle two'
                                    method="change"
                                >
                                    <CheckboxItem
                                        fieldid={item.itemKey}
                                        name={item.itemKey} 
                                        disabled={virtualDisabled}
                                        defaultValue={listItem[item.itemKey].value} 
                                        boxs = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{
                                            if(item.itemKey=='accountAge'){//勾选收款账龄分析之后分析方向和坏账估算不可用
                                                if(v[0].checked==true){
                                                    listItem['direct'].value='';
                                                    accountAgeDisabled=true;
                                                    directDisabled=true;
                                                    listItem['verifyRange'].value='';
                                                    verifyRangeDisabled=true;
                                                    listItem.startDate={value:modalDefaultValue.yearbegindate};
                                                    listItem['badAccount'].value='N';
                                                    loadData.map((_item,_index)=>{
                                                        if(_item.itemKey=='badAccount'){
                                                            _item.itemChild[0].checked=false;
                                                        }
                                                    })
                                                }else{
                                                    listItem['direct'].value='';
                                                    if(listItem['accoutAgePeriod'].value){
                                                        accountAgeDisabled=false;
                                                    }
                                                    directDisabled=false;
                                                    listItem['verifyRange'].value='';
                                                    verifyRangeDisabled=false;
                                                    listItem.startDate={value:''};
                                                }
                                            }else{

                                            }
                                            listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                            self.setState({
                                                listItem,accountAgeDisabled,analysisTypeDisabled,directDisabled,verifyRangeDisabled
                                            },()=>{
                                                if(v[0].checked==true){
                                                    listItem['direct'].value='debit';
                                                }else{
                                                    listItem['direct'].value='both';
                                                }
                                                listItem['verifyRange'].value='final';
                                                self.setState({
                                                    listItem,loadData
                                                })
                                            })
                                        }}
                                    />
                                </FormItem>
                                )
                            }else{
                                return(<FormItem
                                    inline={true}
                                    // showMast={false}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10}  md={10} sm={10}
                                    className='checkboxStyle three'
                                    method="change"
                                >
                                    <CheckboxItem
                                        fieldid={item.itemKey}
                                        name={item.itemKey} 
                                        disabled={false}
                                        defaultValue={listItem[item.itemKey].value} 
                                        boxs = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{
                                            if(item.itemKey=='accountAge'){//勾选收款账龄分析之后分析方向和坏账估算不可用
                                                if(v[0].checked==true){
                                                    accountAgeDisabled=true;
                                                    directDisabled=true;
                                                    listItem['badAccount'].value='N';
                                                }else{
                                                    accountAgeDisabled=false;
                                                    directDisabled=false;
                                                }
                                            }else{

                                            }
                                            listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                            self.setState({
                                                listItem,accountAgeDisabled,analysisTypeDisabled,directDisabled
                                            })
                                        }}
                                    />
                                </FormItem>
                                )
                            }
                            break;
                        default:
                        break;
                   }
               })
			):<div />;
           
    }


    onAllCheckChange = () => {
        let self = this;
        let checkedArray = [];
        let selIds = [];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          checkedArray[i] = !self.state.checkedAll;
        }
        self.setState({
          checkedAll: !self.state.checkedAll,
          checkedArray: checkedArray,
        });
      };
      onCheckboxChange = (text, record, index) => {
        let self = this;
        let allFlag = false;
        let checkedArray = self.state.checkedArray.concat();
        checkedArray[index] = !self.state.checkedArray[index];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          if (!checkedArray[i]) {
            allFlag = false;
            break;
          } else {
            allFlag = true;
          }
        }
        self.setState({
          checkedAll: allFlag,
          checkedArray: checkedArray,
        });
      };
      
      renderColumnsMultiSelect(columns) {
        const {checkedArray } = this.state;
        const { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        // let indeterminate_bool1 = true;
        if (multiSelect && multiSelect.type === "checkbox") {
          let i = checkedArray.length;
          while(i--){
              if(checkedArray[i]){
                indeterminate_bool = true;
                break;
              }
          }
          let defaultColumns = [
            {
              title: (
                <Checkbox
                  className="table-checkbox"
                  checked={this.state.checkedAll}
                  indeterminate={indeterminate_bool&&!this.state.checkedAll}
                  onChange={this.onAllCheckChange}
                />
              ),
              key: "checkbox",
              dataIndex: "checkbox",
              width: 50,
              render: (text, record, index) => {
                return (
                  <Checkbox
                    className="table-checkbox"
                    checked={this.state.checkedArray[index]}
                    onChange={this.onCheckboxChange.bind(this, text, record, index)}
                  />
                );
              }
            }
          ];
          columns = defaultColumns.concat(columns);
        }
        return columns;
      }
      //比率设置
    rateSetting=()=>{
        let {listItem}=this.state;
        // if(!listItem.accoutAgePeriod.value){
        //     toast({content:'请先选中账龄区间',color:'warning'});
        //     return false; 
        // }
        this.setState({rateflag:true},()=>{
            this.props.modal.show('ratioSetting');
        })
        this.props.modal.show('ratioSetting');
        // this.props.handleRatioSetting();
    }
    //传入账龄区间主键渲染表格
    ratioSetting=(pk_accountAgePeriod)=>{
        let self=this;
        let {listItem}=self.state;
        let ratioDataArr=[];
        if(pk_accountAgePeriod){
           
            let url = '/nccloud/gl/verify/badaccountrate.do';
            let param={"pk_accountAgePeriod":pk_accountAgePeriod};
                ajax({
                    url:url,
                    data:param,
                    success:function(res){
                        let ratioDatas=res.data;
                       
                        ratioDatas.map((item,index)=>{
                            ratioDataArr.push({'accoutAgePeriod':item,'badRatio':''})
                        })
                        self.setState({
                            ratioData:ratioDataArr
                        })
                    },
                    error: function (res) {
                        toast({ content: res.message, color: 'warning' });
                        
                    }
                })
            }
    }
    showTable=(ratioData)=>{
        let { loadData,assData} =this.state;
        let columnsRatio=[
            {
              title: this.state.json['20021DLANL-000025'],/* 国际化处理： 账龄区间*/
              dataIndex: "accoutAgePeriod",
              key: "accoutAgePeriod",
              width: "30%"
            },
            {
              title: this.state.json['20021DLANL-000051'],/* 国际化处理： 坏账比率(%)*/
              dataIndex: "badRatio",
              key: "badRatio",
              render: (text, record, index) => {
                  return (
                    <div className="textCenter">
                        <FormControl
                            onFocus={this.focus}
                            value={text[item.key]?text[item.key].value : ''}
                            onChange={(v) => {
                                let { rows } = this.state.evidenceData;
                                let originData = this.findByKey(record.key, rows);
                                if (originData) {
                                    originData[item.key] = {
                                        value: v
                                    };
                                }
                                this.setState({
                                    evidenceData: this.state.evidenceData
                                });
                            }}
                        />
                    </div>
                )}
            }
        ];
        let columnsldad = this.renderColumnsMultiSelect(columnsRatio);
        const emptyFuncshow = () => <span>{this.state.json['20021DLANL-000047']}！</span>/* 国际化处理： 这里没有数据*/
        return (
            <Table
                columns={columnsldad} data={ratioData}
                emptyText={emptyFuncshow}
            />
        )
    }
    ratioSettingComfirm=()=>{
        let { listItem} =this.state;
        let ratioData=this.refs.ratioComponent.confirm();
        let ratesArr=[];
        if(ratioData){
            ratioData.map((item,index)=>{
                ratesArr[index]=item.badRatio;
            })
        }
        listItem.rates=ratesArr;
        this.setState({
            listItem,
            rateflag:false
        })
    }
    ratioCancelBtnClick=()=>{
        this.setState({
            rateflag:false
        })
    }
    render() {
        let{showOrHide}=this.props;
        let {modal} = this.props;
        let { createModal } = modal;
        let { loadData,assData,isratioShow,ratioData,rateflag,checkedAll,checkedArray,childAssData} =this.state;
        //const dateInputPlaceholder = this.state.json['20021DLANL-000040'];/* 国际化处理： 选择日期*/
        let columns10=[
            {
              title: this.state.json['20021DLANL-000041'],/* 国际化处理： 核算类型*/
              dataIndex: "checktypename",
              key: "checktypename",
              width: "30%",
              render: (text, record, index) => {
				return <span>{record.checktypename}</span>;
			    }
            },
            {
              title: this.state.json['20021DLANL-000042'],/* 国际化处理： 核算内容*/
              dataIndex: "checkvaluename",
              key: "checkvaluename",
                render: (text, record, index) => {
                    let { assData ,childAssData} =this.state;
                    let defaultValue=[];
                    if(assData[index]['checkvaluename']){                           
                        assData[index]['checkvaluename'].split(",").map((item,_index)=>{
                            defaultValue[_index]={ refname: item, refpk:'' };
                        })
                        assData[index]['pk_Checkvalue'].split(",").map((item,_index)=>{
                            defaultValue[_index].refpk=item;
                        })
                    }else{
                        defaultValue=[{refname:"",refpk:""}];
                    }
                    if (record.refnodename) {
                        let referUrl = record.refnodename + '.js';
                        if (!this.state[record.pk_accassitem]) {
                          { createScript.call(this, referUrl, record.pk_accassitem) }
                          return <div />
                        } else {
                          if (record.classid == 'b26fa3cb-4087-4027-a3b6-c83ab2a086a9' || record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                            return (
                              <FormItem
                                inline={true}
                                // showMast={true}
                                labelXs={2} labelSm={2} labelMd={2}
                                xs={10} md={10} sm={10}
                                //labelName={record.itemName}
                                isRequire={true}
                                method="change"
                              >
                                {this.state[record.pk_accassitem] ? (this.state[record.pk_accassitem])(
                                  {
                                    value: defaultValue,
                                    isShowUnit: true,
                                    unitProps: {
                                      refType: 'tree',
                                      refName: this.state.json['20021DLANL-000008'],/* 国际化处理： 业务单元*/
                                      refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                      rootNode: { refname: this.state.json['20021DLANL-000008'], refpk: 'root' },/* 国际化处理： 业务单元*/
                                      placeholder: this.state.json['20021DLANL-000008'],/* 国际化处理： 业务单元*/
                                      queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                      treeConfig: { name: [this.state.json['20021DLANL-000057'], this.state.json['20021DLANL-000058']], code: ['refcode', 'refname'] },/* 国际化处理： 编码,名称*/
                                      isMultiSelectedEnabled: false,
                                      //unitProps:unitConf,
                                      isShowUnit: false
                                    },
                                    unitCondition:{
                                        pk_financeorg:childAssData.pk_org,
                                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                                    },
                                    isMultiSelectedEnabled: true,
                                    "unitValueIsNeeded":false,
                                    "isShowDimission":true,
                                    queryCondition: () => {
                                        config.isShowDimission=true;
                                          return Object.assign({
                                            "busifuncode": "all",
                                            "pk_org": childAssData.pk_org
                                          },config)
                                    },
                                    onChange: (v) => {
                                      let { assData } = this.state;
                                      let originData = this.findByKey(record.key, assData);
                                      let refnameArr = [], refpkArr = [], refcodeArr = [];
                                      if (originData) {
                                        v.map((arr, index) => {
                                          refnameArr.push(arr.refname);
                                          refpkArr.push(arr.refpk);
                                          refcodeArr.push(arr.refcode);
            
                                        })
                                        originData.checkvaluename = (v.length>0)?refnameArr.join():null;
                                        originData.pk_Checkvalue = (v.length>0)?refpkArr.join():null;
                                        originData.checkvaluecode = (v.length>0)?refcodeArr.join():null;
                                      }
                                      childAssData.assData = assData;
                                      this.setState({
                                        assData, childAssData
                                      })
                                    }
                                  }) : <div />}
                              </FormItem>);
                          } else {
                            return (
                              <FormItem
                                inline={true}
                                // showMast={true}
                                labelXs={2} labelSm={2} labelMd={2}
                                xs={10} md={10} sm={10}
                                //labelName={record.itemName}
                                isRequire={true}
                                method="change"
                              >
                                {this.state[record.pk_accassitem] ? (this.state[record.pk_accassitem])(
                                  {
                                    value: defaultValue,
                                    isMultiSelectedEnabled: true,
                                    queryCondition: () => {
                                      if (record.classid && record.classid.length == 20) {//classid的长度等于20的话过滤条件再加一个pk_defdoclist
                                        return Object.assign({
                                          "pk_org": childAssData.pk_org,
                                          "pk_defdoclist": record.pk_defdoclist
                                        },config)
                                      } else {
                                        return Object.assign({
                                          "pk_org": childAssData.pk_org
                                        },config)
                                      }
                                    },
                                    onChange: (v) => {
                                      let { assData } = this.state;
                                      let originData = this.findByKey(record.key, assData);
                                      let refnameArr = [], refpkArr = [], refcodeArr = [];
                                      if (originData) {
                                        v.map((arr, index) => {
                                          refnameArr.push(arr.refname);
                                          refpkArr.push(arr.refpk);
                                          refcodeArr.push(arr.refcode);
            
                                        })
                                        originData.checkvaluename = (v.length>0)?refnameArr.join():null;
                                        originData.pk_Checkvalue = (v.length>0)?refpkArr.join():null;
                                        originData.checkvaluecode = (v.length>0)?refcodeArr.join():null;
                                      }
                                      childAssData.assData = assData;
                                      this.setState({
                                        assData, childAssData
                                      })
                                    }
                                  }) : <div />}
                              </FormItem>);
                          }
                        }
                      } else {//不是参照的话要区分日期、字符、数值
                        if (record.classid == 'BS000010000100001033') {//日期
                          return (
                            <DatePicker
                              //name={item.itemKey}
                              type="customer"
                              isRequire={true}
                              placeholder={this.state.json['20021DLANL-000040']}
                              value={defaultValue[0].refname}
                              onChange={(v) => {
                                let { assData } = this.state;
                                let originData = this.findByKey(record.key, assData);
                                if (originData) {
                                  let assArr = [];
                                  assArr.push(v);
                                  originData.checkvaluename = v?assArr.join():null;
                                  originData.pk_Checkvalue = v?assArr.join():null;
                                  originData.checkvaluecode = v?assArr.join():null;
                                }
                                childAssData.assData = assData;
                                this.setState({
                                  assData, childAssData
                                })
                              }}
            
                            />
                          )
                        } else if (record.classid == 'BS000010000100001031') {//数值
                          return (
                            <NCNumber
                              scale={2}
                              value={defaultValue[0].refname}
                              placeholder={this.state.json['20021DLANL-000059']}/* 国际化处理： 请输入数字*/
                              onChange={(v) => {
                                let { assData } = this.state;
                                let originData = this.findByKey(record.key, assData);
                                if (originData) {
                                  let assArr = [];
                                  assArr.push(v);
                                  originData.checkvaluename = v?assArr.join():null;
                                  originData.pk_Checkvalue = v?assArr.join():null;
                                  originData.checkvaluecode = v?assArr.join():null;
                                }
                                childAssData.assData = assData;
                                this.setState({
                                  assData, childAssData
                                })
                              }}
                            />
                          )
                        } else if (record.classid == 'BS000010000100001032') {//布尔
                          return (
                            <FormItem
                              inline={true}
                              // showMast={false}
                              labelXs={2}
                              labelSm={2}
                              labelMd={2}
                              xs={10}
                              md={10}
                              sm={10}
                              // labelName={item.itemName}
                              isRequire={true}
                              method="change"
                            >
                              <SelectItem name={record.checktypecode}
                                defaultValue={defaultValue[0].refname}
                                items={
                                  () => {
                                    return ([{
                                      label: this.state.json['20021DLANL-000060'],/* 国际化处理： 是*/
                                      value: 'Y'
                                    }, {
                                      label: this.state.json['20021DLANL-000061'],/* 国际化处理： 否*/
                                      value: 'N'
                                    }])
                                  }
                                }
                                onChange={(v) => {
                                  let { assData } = this.state;
                                  let originData = this.findByKey(record.key, assData);
                                  if (originData) {
                                    let assArr = [];
                                    assArr.push(v);
                                    originData.checkvaluename = v?assArr.join():null;
                                    originData.pk_Checkvalue = v?assArr.join():null;
                                    originData.checkvaluecode = v?assArr.join():null;
                                  }
                                  childAssData.assData = assData;
                                  this.setState({
                                    assData, childAssData
                                  })
                                }}
                              />
                            </FormItem>
                          )
                        } else {//字符
                          return (
                            <FormControl
                              value={defaultValue[0].refname}
                              onChange={(v) => {
                                let { assData } = this.state;
                                let originData = this.findByKey(record.key, assData);
                                if (originData) {
                                  let assArr = [];
                                  assArr.push(v);
                                  originData.checkvaluename = v?assArr.join():null;
                                  originData.pk_Checkvalue = v?assArr.join():null;
                                  originData.checkvaluecode = v?assArr.join():null;
                                }
                                childAssData.assData = assData;
                                this.setState({
                                  assData, childAssData
                                })
                              }}
                            />
                          )
                        }
            
                      }
              }
            }
        ];
        let columnsldad = this.renderColumnsMultiSelect(columns10);
        let pretentAssData={
            assData: assData||[],
            childAssData:childAssData||[],
            checkboxShow: true,//是否显示复选框
            showOrHide:true,
            checkedAll:checkedAll,//全选复选框的选中状态
            checkedArray:checkedArray||[],//复选框选中情况
            $_this:this
          };
        const emptyFunc = () => <span>{this.state.json['20021DLANL-000047']}！</span>/* 国际化处理： 这里没有数据*/
        return (
            <div className="fl">
                <Modal
                    fieldid="query"
                    className={'combine'}
                    id="queryone"
                    show={showOrHide}
                    // backdrop={ this.state.modalDropup }
                    onHide={this.close}
                    animation={true}
                >
                    <Modal.Header closeButton fieldid="header-area">
                        <Modal.Title>{this.state.json['20021DLANL-000053']}</Modal.Title>{/* 国际化处理： 往来账龄分析*/}
                    </Modal.Header >
                    <Modal.Body >
                        {!rateflag ?
                            <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                                <NCForm useRow={true}
                                    submitAreaClassName='classArea'
                                    showSubmit={false}　>
                                    {loadData.length > 0 ? this.queryList(loadData) : ''}
                                </NCForm>
                                <div className="getAssDatas">
                                    {/* <Table
                                        columns={columnsldad}
                                        data={assData}
                                        emptyText={emptyFunc}
                                    /> */}
                                    {getAssDatas({pretentAssData})}
                                </div>
                            </NCDiv>
                        : ''}
                    </Modal.Body>
                    <Modal.Footer fieldid="bottom_area">
                        <Button colors="primary" onClick={this.confirm} fieldid="confirm"> {this.state.json['20021DLANL-000034']} </Button>{/* 国际化处理： 查询*/}
                        <Button onClick={this.close} fieldid="close"> {this.state.json['20021DLANL-000064']} </Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                    <div className="small">
                        {this.state.json['20021DLANL-000056'] && createModal('ratioSetting', {
                            className: "smallbox",
                            title: this.state.json['20021DLANL-000046'],// 弹框表头信息/* 国际化处理： 比率设置*/
                            content: <RatioSettingModal ref='ratioComponent' ratioData={ratioData} />,//this.ratioSetting(this), //弹框内容，可以是字符串或dom
                            beSureBtnClick: this.ratioSettingComfirm.bind(this), //点击确定按钮事件
                            cancelBtnClick: this.ratioCancelBtnClick.bind(this), //取消按钮事件回调
                            closeModalEve: this.ratioCancelBtnClick.bind(this), //关闭按钮事件回调
                            userControl: false,  // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
                            size: 'sm', //  模态框大小 sm/lg/xlg
                            rightBtnName: this.state.json['20021DLANL-000050'], //左侧按钮名称,默认关闭/* 国际化处理： 关闭*/
                            leftBtnName: this.state.json['20021DLANL-000056'], //右侧按钮名称， 默认确认/* 国际化处理： 确认*/
                        })}
                    </div>
                </Modal>

            </div>
        )
    }
}
SearchModal = createPage({})(SearchModal)
SearchModal.defaultProps = defaultProps12;
