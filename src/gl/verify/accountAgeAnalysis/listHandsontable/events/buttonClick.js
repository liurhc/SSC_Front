import {handleVerifyDetails,handleVerifySum} from './index.js';
export default function buttonClick(props, id) {
    switch (id) {
        case 'query':
        //查询
            this.handleQuery()
        break;
        // 刷新
        case 'refresh':
            this.handleRefresh()
            break;
        // 转换
        case 'switch':
            this.handleChange()
            break;
        // 明细
        case 'detailed':
            handleVerifyDetails(this,this.state,props)
            break;
            //汇总
        case 'total':
            handleVerifySum(this,this.state,props)
            let button=['total'];
            this.props.button.setButtonDisabled(button, true);
            let buttontwo=['detailed'];
            this.props.button.setButtonDisabled(buttontwo, false);
            break;
            //直接输出
        case 'print':
            this.handlePrint()
            break;
            //保存列宽
        case 'savewidth':
            this.handleSaveColwidth()
            break;
       default:
        break;
    }
}
