import {ajax,toast} from 'nc-lightapp-front';
import { setData } from '../../../../manageReport/common/simbleTableData.js';
export default function handleDetail(self,state){
    let {queryDataObj,selectedData,listItem,detailOrSum,resourceData_detail,resourceData_sum,verifyBalancesData}=state;
    let rowDatas=[];
    if(queryDataObj.direct=='both'){//方向选的是双向时需要选中一行数据
        rowDatas=self.getSelectRowData();
    }else{
        rowDatas=[{"link":null}];
    }
    

    if(queryDataObj.direct=='both'&&((!rowDatas||rowDatas[0]==null||rowDatas.length==0)||(rowDatas&&rowDatas.length>0&&rowDatas[0].style=='head'))){//选择双向时,没有选择有效数据行才提示
        toast({ content:self.state.json['20021DLANL-000052'], color: 'warning' });/* 国际化处理： 请先选中一行数据再进行明细操作*/
        return false;
    }
    let buttonTrue=['detailed'];
    self.props.button.setButtonDisabled(buttonTrue, true);
    let buttonthree=['total'];
    self.props.button.setButtonDisabled(buttonthree, false);
    let link=rowDatas[0].link;
    queryDataObj.accountAgeData=link;
    let columnInfo=[],headtitle=[],balanceVO=[];
    let url='/nccloud/gl/verify/accountAgeAnalysisDetail.do';
    ajax({
        url:url,
        data:queryDataObj,
        success:function(response){
            let { data, success } = response;
            if(success){
                listItem.verObj.value=data.verObj;
                listItem.currency.value=data.currency;
                listItem.endDate.value=data.endDate;
                detailOrSum='sum';
                resourceData_detail=data;
                if(data.columnInfo){
                    columnInfo=data.columnInfo;
                }
                if(data.headtitle){
                    headtitle=data.headtitle;
                }
                if(data.detailVO){
                    balanceVO=data.detailVO;
                }
                self.setState({
                  listItem,detailOrSum,resourceData_detail
                })
                let renderFirstData = {};
                    renderFirstData.columnInfo = data.columnInfo;
                    renderFirstData.data = data.detailVO;
                    renderFirstData.columnInfo1= data.columnInfo1;
                    renderFirstData.columnInfo2 =data.columnInfo2;
                    setData(self,renderFirstData);
            }
        },
        error:function(error){
            toast({ content:self.state.json['20021DLANL-000005'], color: 'warning' });/* 国际化处理： 没有请求到后台数据*/
        }
    })
}
