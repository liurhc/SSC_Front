import {ajax,toast} from 'nc-lightapp-front';
export default function handleRefresh(state){
    let self=this;
    let {refersh,detailOrSum,listItem,queryDataObj,resourceData_sum}=state;
    let url='/nccloud/gl/verify/accountAgeAnalysis.do';
    refersh=true;
    let childData=queryDataObj;
    let columnInfo=[],headtitle=[],balanceVO=[];
    ajax({
        url:url,
        data:childData,
        success:function(response){
            let { data, success } = response;
            if(success){
                listItem.accountingbookValue.value=data.accountingbookValue
                listItem.verObj.value=data.verObj
                listItem.currency.value=data.currency
                listItem.endDate.value=data.endDate
                if(data.columnInfo){
                    columnInfo=data.columnInfo2
                }
                if(data.headtitle){
                    headtitle=data.headtitle;
                }
                if(data.totalVO){
                    balanceVO=data.totalVO;
                }
                resourceData_sum=data;
                detailOrSum='detail';
                self.setState({
                  resourceData_sum,listItem,
                  detailOrSum
                })
                let renderFirstData = {};
                renderFirstData.columnInfo = data.columnInfo;
                renderFirstData.data = data.totalVO;
                renderFirstData.columnInfo1= data.columnInfo1;
                renderFirstData.columnInfo2 =data.columnInfo2;
                setData(self,renderFirstData);

            }
        },
        error:function(error){
            toast({ content: self.state.json['20021DLANL-000003'], color: 'warning' });/* 国际化处理： 未请求到数据*/
            self.setState({
                isQueryShow: false,
            })
        }
    })

}
