import {ajax} from 'nc-lightapp-front';
import { toast } from '../../../../public/components/utils.js';
import changeToHandsonTableData from '../../../../public/common/changeToHandsonTableData.js';
import { setData } from '../../../../manageReport/common/simbleTableData.js';
export default function handleSum(self,state){
   // let self=this;
    let {resourceData_sum,detailOrSum}=state;
    detailOrSum='detail';
    let columnInfo=[],headtitle=[],balanceVO=[];
    let data=resourceData_sum;
    if(data.columnInfo){
        columnInfo=data.columnInfo
    }
    if(data.headtitle){
        headtitle=data.headtitle;
    }
    if(data.totalVO){
        balanceVO=data.totalVO;
    }
    self.setState({
      detailOrSum
    })
    let renderFirstData = {};
    renderFirstData.columnInfo = data.columnInfo;
    renderFirstData.data = data.totalVO;
    renderFirstData.columnInfo1= data.columnInfo1;
    renderFirstData.columnInfo2 =data.columnInfo2;
    setData(self,renderFirstData);
    // self.changeToHandsonTableData(self, data, columnInfo, headtitle, balanceVO);

    // self.setState({
    //     columnsVerify:resourceColumns_sum,
    //     verifyBalancesData:resourceData_sum,   
    //     detailOrSum
    // })
}
