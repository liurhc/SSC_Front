import { ajax, deepClone } from 'nc-lightapp-front';
import { toast } from '../../../../public/components/utils.js';
import { setData } from '../../../../manageReport/common/simbleTableData.js';
import { gl_pkreport_accountAgeAnalysis } from '../../../../public/common/reportPkConst.js';
export default function handleQueryClick(state, data) {
    let self = this;
    let { refersh, detailOrSum, listItem, queryDataObj, resourceData_sum,resourveQuery, queryCondition } = state;
    let url = '/nccloud/gl/verify/accountAgeAnalysis.do';
    resourveQuery = deepClone(data);
    let childData = deepClone(data);
    for (let k in childData) {
        if (k == 'ass' || k == 'acccode' || k == 'accname' || k == 'curName' || k == 'rates') {
        } else if (k == 'pk_accountingbook' || k == 'pk_accasoa' || k == 'pk_units' || k == 'pk_currency') {
            if (childData[k].length > 0) {
                if (childData[k][0].value == "") {
                    childData[k] = null;
                } else {
                    childData[k].map((item, index) => {
                        childData[k][index] = item.value;
                    })
                }
            } else {
                childData[k] = null;
            }
        } else {
            childData[k] = childData[k].value ? childData[k].value : null;
        }
    }
    queryCondition = childData;
    let columnInfo = [], headtitle = [], balanceVO = [];
    if (refersh) {
        childData = queryDataObj
    } else {
        childData = childData;
    }
    childData.pagecode = gl_pkreport_accountAgeAnalysis;
    ajax({
        url: url,
        data: childData,
        success: function (response) {
            let { data, success } = response;
            if (success) {
                listItem.accountingbookValue.value = data.accountingbookValue
                listItem.verObj.value = data.verObj
                listItem.currency.value = data.currency
                listItem.endDate.value = data.endDate
                if (data.columnInfo) {
                    columnInfo = data.columnInfo2
                }
                if (data.headtitle) {
                    headtitle = data.headtitle;
                }
                if (data.totalVO) {
                    balanceVO = data.totalVO;
                }
                resourceData_sum = data;
                detailOrSum = 'detail';
                self.setState({
                    resourveQuery, queryCondition, resourceData_sum, listItem,
                    queryDataObj: childData, detailOrSum, showModal: false,
                })
                let renderFirstData = {};
                renderFirstData.columnInfo = data.columnInfo;
                renderFirstData.data = data.totalVO ? data.totalVO : [];
                renderFirstData.columnInfo1 = data.columnInfo1;
                renderFirstData.columnInfo2 = data.columnInfo2;
                setData(self, renderFirstData, true);
                if (data.totalVO.length > 0) {
                    self.props.button.setButtonDisabled(['detailed', 'print', 'savewidth', 'refresh', 'switch'], false);
                    self.props.button.setButtonDisabled(['total'], true);
                }else{
                    self.props.button.setButtonDisabled(['detailed', 'print', 'savewidth', 'refresh', 'switch','total'], true);
                }
            }
        },
        error: function (error) {
            toast({ content: self.state.json['20021DLANL-000003'], color: 'warning' });/* 国际化处理： 未请求到数据*/
            self.setState({
                showModal: false,
            })
        }
    })
}
