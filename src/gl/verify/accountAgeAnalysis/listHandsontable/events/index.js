import handleQueryClick from './handleQueryClick';
import handleVerifyDetails from './handleVerifyDetails';
import handleVerifySum from './handleVerifySum';
import handleRefresh from './handleRefresh';
import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
export {initTemplate, buttonClick}
export{handleQueryClick,handleVerifyDetails,handleVerifySum,handleRefresh}
