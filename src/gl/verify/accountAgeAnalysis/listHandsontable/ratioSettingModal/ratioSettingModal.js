import React, { Component } from 'react';
import {high,base,ajax,getMultiLang} from 'nc-lightapp-front';
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../../public/components/FormItems';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm
} = base;
import '../index.less'
const {  NCFormItem:FormItem } = NCForm;

//const dateInputPlaceholder = this.state.json['20021DLANL-000040'];/* 国际化处理： 选择日期*/


  const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
  };


export default class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal:false,
            isShowUnit:true,//是否显示业务单元
            assData: [//辅助核算信息
            ],
            loadData:[],//查询模板加载数据
            listItem:{},//模板数据对应值
            checkedAll:false,
            checkedArray: [
                false,
                false,
                false,
            ],
           // columns10:this.columns10,
            ratioData:this.props.ratioData,
            json:{}
        };
        this.close = this.close.bind(this);
    }
    componentWillMount() {
		let callback= (json) =>{
            this.columns10=[
                {
                title: (<span fieldid="accoutAgePeriod">{json['20021DLANL-000025']}</span>),/* 国际化处理： 账龄区间*/
                  dataIndex: "accoutAgePeriod",
                  key: "accoutAgePeriod",
                  width: "30%",
                  render:(text, record, index)=>{
                      return <span fieldid="accoutAgePeriod">{text}</span>
                  }
                },
                {
                title: (<span fieldid="badRatio">{json['20021DLANL-000051']}</span>),/* 国际化处理： 坏账比率(%)*/
                  dataIndex: "badRatio",
                  key: "badRatio",
                  render: (text, record, index) => {
                    let {ratioData}=this.state;
                      return (
                        <div className="textCenter">
                        <NCNumber 
                            fieldid="badRatio"
                            scale={2} 
                            value={text} 
                            min={0}
                            max={100}
                            onBlur={v => {}} 
                            onChange={(v) => {
                                
                                ratioData[index].badRatio=v;
                            }}
                        />
                        </div>
                    )}
                }
            ];
			this.setState({json:json},()=>{
			})
		}
		getMultiLang({moduleId:'20021DLANL',domainName:'gl',currentLocale:'simpchn',callback});
    }

    componentWillReceiveProps (nextProp) {
        let {loadData,isratioShow,ratioData}=this.props;
        let self=this;
        let { listItem,showModal }=self.state
    }

    //表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

    close() {
        this.props.handleClose();
    }

    confirm=()=>{
        let {ratioData} =this.state;
        return ratioData;
        //listItem.ass=assData;
        // this.props.onConfirm(ratioData);
    }
    //比率设置
    rateSetting=()=>{
        
    } 
   
    render() {
        let{isratioShow,ratioData}=this.props;
        let { loadData,assData} =this.state;
        // let columns10=[
        //     {
        //     title: (<span fieldid="accoutAgePeriod">{this.state.json['20021DLANL-000025']}</span>),/* 国际化处理： 账龄区间*/
        //       dataIndex: "accoutAgePeriod",
        //       key: "accoutAgePeriod",
        //       width: "30%",
        //       render:(text, record, index)=>{
        //           return <span fieldid="accoutAgePeriod">{text}</span>
        //       }
        //     },
        //     {
        //     title: (<span fieldid="badRatio">{this.state.json['20021DLANL-000051']}</span>),/* 国际化处理： 坏账比率(%)*/
        //       dataIndex: "badRatio",
        //       key: "badRatio",
        //       render: (text, record, index) => {
        //         let {ratioData}=this.state;
        //           return (
        //             <div className="textCenter">
        //             <NCNumber 
        //                 fieldid="badRatio"
        //                 scale={2} 
        //                 value={text} 
        //                 min={0}
        //                 max={100}
        //                 onBlur={v => {}} 
        //                 onChange={(v) => {
                            
        //                     ratioData[index].badRatio=v;
        //                 }}
        //             />
        //             </div>
        //         )}
        //     }
        // ];
        let columnsldad = this.columns10;
        const emptyFunc = () => <span>{this.state.json['20021DLANL-000047']}！</span>/* 国际化处理： 这里没有数据*/
        return (
            <div className="fl" fieldid="badAccount_table">
                <Table
                    columns={this.columns10?this.columns10:[]} 
                    data={ratioData}
                    emptyText={emptyFunc}
                />
            </div>
        )
    }
}
SearchModal.defaultProps = defaultProps12;
