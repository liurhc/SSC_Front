import React, { Component } from 'react';
import {high,base,ajax,deepClone,createPage,toast,getMultiLang,createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate} from './events';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCForm,NCButtonGroup:ButtonGroup,NCAffix,NCTooltip:Tooltip
} = base;
import { SimpleTable } from 'nc-report';
const {  NCFormItem:FormItem } = NCForm;
import { InputItem} from '../../../public/components/FormItems';
import QueryModal from './queryModal/querymodal.js';
import {handleQueryClick,handleVerifyDetails,handleVerifySum,handleRefresh} from './events/index.js';
import { tableDefaultData } from "../../../manageReport/defaultTableData";
import { setData } from '../../../manageReport/common/simbleTableData.js';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import reportPrint from '../../../public/components/reportPrint.js';
import reportSaveWidths from '../../../public/common/reportSaveWidths.js';
import {gl_pkreport_accountAgeAnalysis} from '../../../public/common/reportPkConst.js';
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less'
class VerifyBalances extends Component{
    constructor(props){
        super(props);
        this.state={
            accountType:'columnInfo',
            textAlginArr:[],
            flag:0,//切换表头用的
            showTable: false,//控制是否显示表格
            flow: false,//控制表头是否分级
            resourceDatas: [], // 原始数据
            dataout: deepClone(tableDefaultData),
            queryShow:true,//查询按钮默认显示
            listItem:{
                accountingbookValue:{display:'',value:''},
                verObj:{display:'',value:''},
                currency:{display:'',value:''},
                endDate:{display:'',value:''},
            },
            showModal:false,//查询条件对话框
            refersh:false,//刷新
            modalDefaultValue:{
                pk_org:'',//账簿对应的组织
                bizDate:'',//业务日期
                yearbegindate:'',//年初日期
                begindate:'',
                enddate:'',
                bizPeriod:'',//账簿对应的会计期间
                isShowUnit:false,//是否显示业务单元
                isOpenUpBusiUnit:'',//
                pk_accperiodscheme:'',//会计期间方案
                pk_accountingbook:{display:'',value:''},
                pk_currency:{display:'',value:''},
                appcode:this.props.getSearchParam("c")
            },//模态框里默认数据
            verifyBalancesData:[],
            detailOrSum:'sum',//明细汇总状态区分
            verBalanceVOData:{},
            resourveQuery:[],
            queryCondition:[],
            queryDataObj:{},//保存查询条件
            resourceData_sum:[],//查询出来的历史数据
            resourceData_detail:[],//明细数据
            resourceColumns_sum:[],//查询出来的历史数据列字段
            selectedData:{},//选中的数据,
            json:{}

        }   
        this.tableDefaultData=deepClone(tableDefaultData);    
    }

	componentWillMount() {   
		let callback= (json) =>{
			this.setState({json:json},()=>{
				initTemplate.call(this, this.props); 
			})
		}
		getMultiLang({moduleId:['20021DLANL','publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }


    //查询
    handleQuery=()=>{
        this.setState({
            showModal:true,
            refersh:false
        })
    }
    //关闭
    handleClose=()=>{
        this.setState({
            showModal:false
        })
    }
    //处理width
    changeWidth(arr){
        arr.map((item,index)=>{
            if(item.children){
                this.changeWidth(item.children);
            }else{
               item['width']=150; 
            }            
        })
        return arr;
    }
    //获取默认会计期间 会计期间方案
  getDefaultYearmouth=(pk_accountingbook,modalDefaultValue)=>{
    let self=this;
    let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
    let pk_accpont = {"pk_accountingbook":pk_accountingbook}
    ajax({
        url:url,
        data:pk_accpont,
        async:false,
        success: function(response){
            const { success } = response;
            //渲染已有账表数据遮罩
            if (success) {
                if(response.data){
                    modalDefaultValue.bizPeriod=response.data.bizPeriod;
                    modalDefaultValue.isShowUnit=response.data.isShowUnit;
                    modalDefaultValue.isOpenUpBusiUnit='Y';
                    modalDefaultValue.pk_accperiodscheme=response.data.pk_accperiodscheme;
                    modalDefaultValue.begindate=response.data.begindate;
                    modalDefaultValue.enddate=response.data.enddate;
                    modalDefaultValue.bizDate=response.data.bizDate;
                    modalDefaultValue.yearbegindate=response.data.yearbegindate;
                    modalDefaultValue.pk_currency.display= response.data.currinfo.display;
                    modalDefaultValue.pk_currency.value = response.data.currinfo.value;
                    modalDefaultValue.pk_org = response.data.unit.value;
                }
                self.setState({
                    modalDefaultValue,
                    dataout: self.state.dataout
                })
            }   
        }
    });
}
    componentDidMount(){
        let {modalDefaultValue}=this.state;
        let appcode=this.props.getSearchParam("c");    
        getDefaultAccountBook(appcode).then((defaultAccouontBook)=>{   
            if(defaultAccouontBook.value){
                modalDefaultValue.pk_accountingbook=defaultAccouontBook;
                this.getDefaultYearmouth(defaultAccouontBook.value,modalDefaultValue);
            }
        })
    }
    getHeadData=(listItem)=>{
       const headData=[
        {itemName:this.state.json['20021DLANL-000030'],itemType:'textInput',itemKey:'verObj'},/* 国际化处理： 核销对象:*/
        {itemName:this.state.json['20021DLANL-000031'],itemType:'textInput',itemKey:'currency'},/* 国际化处理： 币种:*/
        {itemName:this.state.json['20021DLANL-000032'],itemType:'textInput',itemKey:'endDate'},/* 国际化处理： 截止日期:*/
    ]
        return (
            headData.map((item,i)=>{
                let defValue=listItem[item.itemKey].value;

                switch (item.itemType) {
                    case 'textInput':
                        return (
                            // <div fieldid={item.itemKey}>
                                <FormItem
                                    fieldid={item.itemKey}
                                    inline={true}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={2} md={2} sm={2}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                    <Tooltip trigger="hover" placement="top" inverse overlay={defValue}>
                                        <span>
                                            <InputItem
                                                disabled={true}
                                                name={item.itemKey}
                                                type="customer"
                                                defaultValue={defValue}
                                            />
                                        </span>
                                    </Tooltip>
                                </FormItem>
                            // </div>
                        )
                    default:
                    break;
                }
            })
            
        )  
    }
    //刷新
    handleRefresh=()=>{
        let { resourveQuery,isQueryShow } = this.state;
        if(resourveQuery.pk_accountingbook){
        this.setState({
            isQueryShow:false
        },()=>{
            handleQueryClick.call(this,this.state,resourveQuery);
        })
        
        }
    }
    //获取当前选中行数据
    getSelectRowData=()=>{
        let selectRowData=this.refs.balanceTable.getRowRecord();
        return selectRowData;
    }
    //转换
    handleChange() {
    let self=this;
    let {resourceData_sum,resourceData_detail,flag,detailOrSum,accountType}=self.state;
    let data,balanceVODatas;
    if(detailOrSum=='sum'){
        data=resourceData_detail;
        balanceVODatas=data.detailVO;
    }else if(detailOrSum=='detail'){
        data=resourceData_sum;
        balanceVODatas=data.totalVO;
    }
    if(flag>=2){
        flag=0;
    }else{
        flag++;
    }
    if(flag==0){
        accountType='columnInfo';
        // ChangecolumnInfo=data.columnInfo;
    }else if(flag==1){
        accountType='columnInfo1';
        // ChangecolumnInfo=data.columnInfo1;
    }else if(flag==2){
        accountType='columnInfo2';
        // ChangecolumnInfo=data.columnInfo2;
    }
    let renderFirstData = {};
      renderFirstData.columnInfo = data.columnInfo;
      if(detailOrSum=='sum'){
      renderFirstData.data = data.detailVO;
      }else if(detailOrSum=='detail'){
        renderFirstData.data = data.totalVO;
      }
      renderFirstData.columnInfo1= data.columnInfo1;
      renderFirstData.columnInfo2 =data.columnInfo2;
      self.setState({
        accountType,flag
      },()=>{
          setData(self,renderFirstData,true);
      })
  }
    //直接输出
    handlePrint=()=>{
    let {resourceDatas,dataout,textAlginArr}=this.state;
    let dataArr=[];
    let emptyArr=[];
    let mergeInfo=dataout.data.mergeInfo;
    dataout.data.cells.map((item,index)=>{
        emptyArr=[];
        item.map((list,_index)=>{
            if(list){
                emptyArr.push(list.title);
            }
        })
        dataArr.push(emptyArr);
    })

    reportPrint(mergeInfo,dataArr,textAlginArr);
}
    //保存列宽
    handleSaveColwidth=()=>{
        let info=this.refs.balanceTable.getReportInfo();
        let {json}=this.state
        let callBack = this.refs.balanceTable.resetWidths
        reportSaveWidths(gl_pkreport_accountAgeAnalysis,info.colWidths,json, callBack);
    }
    //返回
    handleBack=()=>{
        window.history.go(-1);
    }
    GetRequest=(urlstr)=> {   
		var url = decodeURIComponent(urlstr)//(window.parent.location.href); //获取url中"?"符后的字串   
		var theRequest = new Object();   
		if (url.indexOf("?") != -1) {   
		   var str = url.substr(url.indexOf("?")+1);   
		   var strs = str.split("&");   
		   for(var i = 0; i < strs.length; i ++) {   
			  theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);   
		   }   
		}   
		return theRequest;   
	}
    //选中某一行
    getRow = (expanded, record) => {
		let copyData=deepClone(expanded);
		//表体数据增删获取key
        let { getrowkey,selectedRecord,selectedData } = this.state;
        selectedData=copyData;
		let getrow = expanded.key;
		this.setState({
            getrowkey: getrow,
            selectedRecord:record,
            selectedData
		});
	};
    render(){
        let{verifyBalancesData,listItem,queryShow,detailOrSum,modalDefaultValue,resourveQuery}=this.state;
        let {modal} = this.props;
        let { createModal } = modal;
        const loadQuery=[
            {
                itemName:this.state.json['20021DLANL-000006'],itemType:'refer',itemKey:'pk_accountingbook',isMustItem:true,/* 国际化处理： 核算账簿*/
                config:{refCode:"uapbd/refer/org/AccountBookTreeRef"}
            },
            {itemName:this.state.json['20021DLANL-000007'],itemType:'refer',itemKey:'pk_accasoa',isMustItem:true,/* 国际化处理： 核销科目*/
            config:{refCode:"uapbd/refer/fiacc/AccountDefaultGridTreeRef"}},
            {itemName:this.state.json['20021DLANL-000008'],itemType:'refer',itemKey:'pk_units',isMustItem:false,/* 国际化处理： 业务单元*/
            config:{refCode:"/uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef"}},
            {itemName:'',itemType:'checkbox',itemKey:'multBusi',
                itemChild:[{
                    label: this.state.json['20021DLANL-000009'],/* 国际化处理： 多业务单元合并*/
                    checked: false
                }]
            },
            {itemName:this.state.json['20021DLANL-000010'],itemType:'radio',itemKey:'analysisType',/* 国际化处理： 分析方式*/
                itemChild: [
                    {
                        label: this.state.json['20021DLANL-000011'],/* 国际化处理： 实际发生额账龄分析*/
                        value: 'real',
                        disabled:false,
                    },
                    {
                        label: this.state.json['20021DLANL-000012'],/* 国际化处理： 余额账龄分析*/
                        value: 'virtual',
                        disabled:false,
                    }
                ]
            },
            {itemName:this.state.json['20021DLANL-000013'],itemType:'Dbdate',itemKey:['startDate','endDate']},//begin_date,end_date（必传）/* 国际化处理： 日期范围*/
            {itemName:this.state.json['20021DLANL-000014'],itemType:'radio',itemKey:'verifyRange',/* 国际化处理： 核销范围*/
                itemChild: [
                    {
                        label: this.state.json['20021DLANL-000015'],/* 国际化处理： 最终核销日期*/
                        value: 'end',
                        disabled:false,
                    },
                    {
                        label: this.state.json['20021DLANL-000016'],/* 国际化处理： 截止日期*/
                        value: 'final',
                        disabled:false,
                    }
                ]
            },
            {itemName:this.state.json['20021DLANL-000017'],itemType:'refer',itemKey:'pk_currency',isMustItem:true,/* 国际化处理： 币种*/
            config:{refCode:"uapbd/refer/pubinfo/CurrtypeGridRef"},queryGridUrl:'/nccloud/gl/voucher/ref.do',refType:'grid'},
            {itemName:this.state.json['20021DLANL-000018'],itemType:'radio',itemKey:'dateType',/* 国际化处理： 分析日期*/
                itemChild: [
                    {
                        label: this.state.json['20021DLANL-000019'],/* 国际化处理： 凭证日期*/
                        value: 'voucherdate',
                        disabled:false,
                    },
                    {
                        label: this.state.json['20021DLANL-000020'],/* 国际化处理： 业务日期*/
                        value: 'businessDate',
                        disabled:false,
                    }
                ]
            },
                
            {itemName:this.state.json['20021DLANL-000021'],itemType:'radio',itemKey:'direct',/* 国际化处理： 分析方向*/
                itemChild: [
                    {
                        label: this.state.json['20021DLANL-000022'],/* 国际化处理： 借*/
                        value: 'debit',
                        disabled:false,
                    },
                    {
                        label: this.state.json['20021DLANL-000023'],/* 国际化处理： 贷*/
                        value: 'credit',
                        disabled:false,
                    },
                    {
                        label: this.state.json['20021DLANL-000024'],/* 国际化处理： 双向*/
                        value: 'both',
                        disabled:false,
                    }
                ]
            },
            {itemName:this.state.json['20021DLANL-000025'],itemType:'refer',itemKey:'accoutAgePeriod',isMustItem:true,/* 国际化处理： 账龄区间*/
            config:{refCode:"fipub/ref/pub/TimeControlRef"},queryGridUrl:'/nccloud/gl/voucher/ref.do',refType:'grid'},
            {itemName:this.state.json['20021DLANL-000026'],itemType:'checkbox',itemKey:'badAccount',/* 国际化处理： 坏账估算*/
                itemChild:[{
                    label: this.state.json['20021DLANL-000027'],/* 国际化处理： 是否显示*/
                    checked: false
                }]
            },
            {itemName:'',itemType:'checkbox',itemKey:'voucherRange',
                itemChild:[{
                    label: this.state.json['20021DLANL-000028'],/* 国际化处理： 包含未记账凭证*/
                    checked: false
                }]
            },
            {itemName:'',itemType:'checkbox',itemKey:'accountAge',
                itemChild:[{
                    label: this.state.json['20021DLANL-000029'],/* 国际化处理： 收款账龄分析*/
                    checked: false
                }]
            },
        ]
        return(
            <div className="manageReportContainer verifyDetails nc-bill-list" id="verifyDetails">
                <HeaderArea
                    title={this.state.json['20021DLANL-000053']}/* 国际化处理： 往来账龄分析*/

                    btnContent={
                        this.props.button.createButtonApp({
                            area: 'button_area',
                            onButtonClick: buttonClick.bind(this),
                        })
                    }
                />
                <QueryModal
                    modalDefaultValue={modalDefaultValue} 
                    resourveQuery={resourveQuery}
                    loadData={loadQuery} 
                    showOrHide = {this.state.showModal}
                    onConfirm = {handleQueryClick.bind(this,this.state)} 
                    handleClose={this.handleClose.bind(this)}
                />
                {/* //表头 */}
                <div className="nc-bill-search-area Mende-ncfrom-box-min nc-theme-form-label-c nc-theme-area-bgc">
                    <div fieldid="accouintAgeAnalysis_form-area">
                        <NCForm useRow={true}
                            submitAreaClassName='classArea'
                            showSubmit={false}　>
                            {this.getHeadData(listItem)}
                        </NCForm>
                    </div>
                </div>
                <div fieldid="accouintAgeAnalysis_report" className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data={this.state.dataout}
                    />
                </div>
            </div>
        )
        
    }
}
VerifyBalances = createPage({})(VerifyBalances);
ReactDOM.render(<VerifyBalances />, document.querySelector("#app"));
