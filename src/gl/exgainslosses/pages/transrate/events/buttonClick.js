import { base, ajax, toast, promptBox, cardCache } from 'nc-lightapp-front';
import { URL, pagecode, appcode } from '../../containers';
import reportPrint from '../../../../public/components/reportPrint.js';
import { setData } from '../../../../manageReport/common/simbleTableData.js';
import { pushToGenVoucher } from '../../../../public/common/voucherUtils';

export default function buttonClick(props, id, _this) {
    let { searchMap, rateTableData, step } = _this.state;
    switch (id) {
        //上一步
        case 'prev':
            step = step - 1;
            _this.setState({
                step
            });
            props.button.setButtonDisabled(['prev'], false);
            if(step === 0){
                // props.button.setButtonVisible('prev', false);
                props.button.setButtonDisabled(['prev'], true);
                props.button.setButtonVisible(['prev'], false);
                props.button.setButtonVisible(['print'],false)
            } 
            if(step === 1){
                props.button.setButtonDisabled(['next'], false);
                props.button.setButtonVisible(['print'],true)
            }
            break;
        //下一步
        case 'next':
            //第一步
            step = step + 1;
            // props.button.setButtonVisible('prev', true);
            props.button.setButtonDisabled(['prev'], false);
            props.button.setButtonVisible(['prev'], true);
            _this.setState({
                step
            });
            
            if(step === 1){
                props.button.setButtonVisible(['print'],true)
                searchMap.rate = rateTableData;
                ajax({
                    url: '/nccloud/gl/transrate/transratedataquery.do',
                    data: searchMap,
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            let rate;
                            if (data) {
                                _this.setState({
                                    transColumns: data.column,
                                    transTabledata: data.data
                                },()=>{
                                    let renderFirstData = {};
                                    renderFirstData.columnInfo = data.column;
                                    renderFirstData.data = data.data;
                                    if(data.data){
                                        setData(_this,renderFirstData);
                                    }
                                });
    
                            }
                        }
                    },
                    error: function (res) {
                        toast({ content: res.message, color: 'danger' });
                    }
                });
            //第二步
            }else if(step === 2){
                ajax({
                    url: '/nccloud/gl/transrate/generatevoucher.do',
                    data: searchMap,
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            let rate;
                            if (data) {
                                if(data.message){
                                    toast({ content: data.message, color: 'warning' });
                                }else{
                                    let param={
                                        voucher: data,
                                        titlename:_this.state.json['exgainslosses-000020'], /* 国际化处理： 制单*/
                                        backUrl: '/',
                                        backAppCode:_this.props.getUrlParam('backAppCode') || _this.props.getSearchParam('c'),
                                        backPageCode:_this.props.getUrlParam('backPageCode') || _this.props.getSearchParam('p')
                                    };
                                    pushToGenVoucher(_this,param)
    //                                let appCodeNumber=voucherRelatedApp(voucher_gen);
    //                                 if(data.gl_voucher){
    //                                     setDefData('gl_voucher', dataSource, data);
    //                                     _this.props.pushTo(
    //                                         '/list',
    //                                         { 
    //                                             ifshowQuery: true,
    //                                             n:  _this.state.json['exgainslosses-000020'], /* 国际化处理： 制单*/
    //                                             appcode:appCodeNumber.appcode, 
    //                                             // step: true,
    //                                             pagekey:'generate',
    //                                             backUrl:'/',
    //                                             backAppCode:_this.props.getUrlParam('backAppCode') || _this.props.getSearchParam('c'),
    //                                             backPageCode:_this.props.getUrlParam('backPageCode') || _this.props.getSearchParam('p')
    //                                         }
    //                                     );
    //                                 }else{
    //                                     setDefData('voucher_detail', 'gl.gl_voucher.voucher.detail', data);
    //                                     _this.props.pushTo('/Welcome', {
    //                                         status: 'edit',
    //                                         appcode: appCodeNumber.appcode,
    //                                         pagecode: '20021005card',
    //                                         pagekey: 'generate',
    //                                         ifshowQuery: true,
    //                                         n:  _this.state.json['exgainslosses-000020'],/* 国际化处理： 凭证生成*/
    //                                         backflag: 'back',
    //                                         backAppCode:_this.props.getUrlParam('backAppCode') || _this.props.getSearchParam('c'),
    //                                         backPageCode:_this.props.getUrlParam('backPageCode') || _this.props.getSearchParam('p')
    //                                     });
    //                                 }
                                }
                                //有数据才跳到凭证
                                
//                                 if (data.vouchers.gl_voucher){
//                                     setDefData('gl_voucher', dataSource, data.vouchers);//
//                                     _this.props.pushTo(
//                                         '/list',
//                                         { 
//                                             ifshowQuery: true,
//                                             n: _this.state.json['exgainslosses-000020'], /* 国际化处理： 制单*/
//                                             appcode:'20020PREPA', 
//                                             // step: true,
//                                             pagekey:'generate',
//                                             backUrl:'/',
//                                             backAppCode:_this.props.getUrlParam('backAppCode') || _this.props.getSearchParam('c'),
//                                             backPageCode:_this.props.getUrlParam('backPageCode') || _this.props.getSearchParam('p')
//                                         }
//                                     );
//                                 }else{
//                                     toast({ content: _this.state.json['exgainslosses-000021'], color: 'warning' });/* 国际化处理： 没有数据*/
//                                     return false;
//                                 }
                            }else{          
                                let msgRender = data.message && data.message.split("\n").map(item => <div>{item}</div>);
                                promptBox({ 
                                    color: 'warning', 
                                    content: msgRender, 
                                    noCancelBtn: true,
                                    beSureBtnClick: cancelBtnClica.bind(this,props)
                                });
                            }
                        }
                    },
                    error: function (res) {
                        toast({ content: res.message, color: 'danger' });
                        return false;
                    }
                });
                
            }
            break;
        case 'print':
       
            handlePrint(_this);
        break;
        //取消
        case 'cancel':
            // props.pushTo('/', {
            //     appcode,
            //     pagecode
            // });
            promptBox({
                color:"warning",
                title: _this.state.json['exgainslosses-000022'],/* 国际化处理： 取消？*/
                content: _this.state.json['exgainslosses-000023'],/* 国际化处理： 确定要取消吗*/
                cancelBtnClick: falsea(),
                beSureBtnClick: cancela.bind(this,props)
                });
            break;
    }

}
function falsea(){
    return false
}
function cancelBtnClica(props){
    props.button.setButtonDisabled(['next'],true)
    props.button.setButtonVisible(['print'],false)
}
function cancela(props){
    props.pushTo('/', {
        appcode,
        pagecode
    })
}


//直接输出
function handlePrint(self){
    let {resourceDatas,dataout,textAlginArr}=self.state;

    let dataArr=[];
    let emptyArr=[];
    let mergeInfo=dataout.data.mergeInfo;
    dataout.data.cells.map((item,index)=>{
        emptyArr=[];
        item.map((list,_index)=>{
            if(list){
                // if(list.title){
                //     list.title='';
                // }
                emptyArr.push(list.title);
            }
        })
        dataArr.push(emptyArr);
    })
    reportPrint(mergeInfo,dataArr,textAlginArr);
}
