import { ajax } from 'nc-lightapp-front';
import { URL, pagecode, appcode } from '../../containers';

export default function initTemplate(props) {
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do',
		data: {
			// pk_appregister: "0001Z31000000005BEGV",
			pagecode: pagecode,
			appcode: appcode	
		},
		success: (res) => {
			if (res.success) {
				if (res.data) {
					// props.meta.setMeta(meta1);
					
					props.button.setButtons(res.data);
					// props.button.setButtonVisible(['transfer','history','prev','save'], false);
					props.button.setButtonVisible(['transfer','history','save','prev'], false);
					//props.button.setButtonDisabled(['prev'], true);
				}
			}
		}
	});
}

