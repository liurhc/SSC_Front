import {asyncComponent} from 'nc-lightapp-front';
import ExchangeTransrate from '../main';

// import VoucherList from '../../gl_voucher/voucher_list/list';
// import Voucher from '../../gl_voucher/container/Welcome';
// import PeriodVoucher from '../../gl_voucher/container/PeriodVoucher/periodpage';
const transrate = asyncComponent(() => import(/* webpackChunkName: "gl/exgainslosses/pages/transrate" */'./transrate.js'));
// const VoucherList = asyncComponent(() => import(/* webpackChunkName: "gl/exgainslosses/voucher_list/list"*/'../../../gl_voucher/voucher_list/list'));
// const Voucher = asyncComponent(() => import(/* webpackChunkName: "gl/exgainslosses/container/Welcome"*/'../../../gl_voucher/container/Welcome'));
const PeriodVoucher = asyncComponent(() => import(/* webpackChunkName: "gl/exgainslosses/container/PeriodVoucher/periodpage" */'../../../gl_voucher/container/PeriodVoucher/periodpage'));
import Voucher from 'gl/voucher_card';
import VoucherList from 'gl/voucher_list';
// const edit11 = asyncComponent(() => import(/* webpackChunkName: "reva_demo/module/apply/card/soCard" */'../../apply/card'));

// const appHome = asyncComponent(() => import(/* webpackChunkName: "demo/module/so/js/AppHome" */'pages/app/home'));
// const appMain = asyncComponent(() => import(/* webpackChunkName: "demo/module/so/js/AppMain" */'pages/app/main'));

const TransferHistory = asyncComponent(() => import(/* webpackChunkName: "gl/exgainslosses/trransferHistory/main/index" */ /* webpackMode: "eager" */ '../../../gl_transfer/trransferHistory/main'));
//transrate: '/gl/exgainslosses/pages/transrate/index.html'
const routes = [
  {
    path: '/',
    component: ExchangeTransrate,
    exact: true,
  },
  {
    path: '/transrate',
    component: transrate,
    exact: true,
  },
  {
    path: '/list',
    component: VoucherList,
  },
  {
    path: '/Welcome',
    component: Voucher,
  },
  {
    path: '/periodpage',
    component: PeriodVoucher,
  },
  {
    path: '/trransferHistory',
    component: TransferHistory,
  }
];

export default routes;
