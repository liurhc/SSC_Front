//汇兑损益结转

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,getMultiLang,createPageIcon,viewModel} from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
const { NCTable, NCSelect, NCCheckbox, NCButton, NCStep, NCFormControl,NCNumber,NCIcon:Icon ,NCDiv} = base;
const NCOption = NCSelect.NCOption;
const NCSteps = NCStep.NCSteps;
import { buttonClick, initTemplate } from './events';
import ExgainslossesStep from '../../../public/components/ExgainslossesStep';
import { tableDefaultData } from "../../../manageReport/defaultTableData";
import '../containers/index.less';
import './index.less'
import HeaderArea from '../../../public/components/HeaderArea';
const API_URL = {
	rate: '/nccloud/gl/transrate/ratequery.do', //查询汇率清单
	query: '/nccloud/gl/transrate/transratedataquery.do', //结转数据查询
	check: '/nccloud/gl/voucher/queryBookCombineInfo.do' //查询核算账簿信息，用于显示集团本币和全局本币
}

class ExchangeTransrate extends Component {
	constructor(props) {
		super(props);
		this.tableId = 'transrateTable';
		this.state={
			json:{},
			textAlginArr:[],
			accountType:'columnInfo',
			dataout: tableDefaultData,
			searchMap: {},
			transColumns: [],
			rateTableData: [], //汇率表格数据
			transTabledata: [], //结转数据
			step: 0, //步骤
			showGroupRate: false, //是否显示集团本币列
			showGlobalRate: false, //是否显示全局本币列
		}
		
	}
	componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
				initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'exgainslosses',domainName:'gl',currentLocale:'simpchn',callback});
    }
	componentDidMount() {
		this.props.button.setButtonVisible(['print'],false)
		let exgainslossesParam = getGlobalStorage('sessionStorage', 'exgainslossesParam');
		this.setState({
			searchMap: JSON.parse(exgainslossesParam)
		},() => {
			this.checkColumn();
			this.queryRate();
		});
		
	}

	//汇率列
	createRateColumns = (columns) => {
		let result = [...columns];
		let { showGroupRate, showGlobalRate } = this.state;
		let groupCol = {
			title: (<div fieldid='groupamount'>{this.state.json['exgainslosses-000024']}</div>),/* 国际化处理： 集团本币*/
			children: [
				{
					title: (<div fieldid='grouprate'>{this.state.json['exgainslosses-000025']}</div>),/* 国际化处理： 当前汇率*/
					dataIndex: "grouprate",
					key: "grouprate",
					width: 120,
					render: (text, record, index) => {
						return <div fieldid='grouprate'>{Number(text.value).toFixed(text.scale)}</div>
					}
				},
				{
					title: <div fieldid='adjustgrouprate'>{this.state.json['exgainslosses-000026']}</div>,/* 国际化处理： 集团本币折本调整汇率*/
					dataIndex: "adjustgrouprate",
					key: "adjustgrouprate",
					width: 150,
					render: (text, record, index) => {
						let { rateTableData } = this.state;
						let value = rateTableData[index] && rateTableData[index].adjustgrouprate.value;
						value = text.edit ? value : Number(value).toFixed(text.scale);
						return <NCNumber
							fieldid='adjustgrouprate'
							value={value}
							scale={Number(text.scale)}
							// disabled={+value == 1}
							onChange={(val) => this.handleRateChange('adjustgrouprate', val, index)}
						/>
					}
				},
			]
		};
		let globalCol = {
			title: <div fieldid='globalrate'>{this.state.json['exgainslosses-000027']}</div>,/* 国际化处理： 全局本币*/
			children: [
				{
					title: <div fieldid="globalrate">{this.state.json['exgainslosses-000025']}</div>,/* 国际化处理： 当前汇率*/
					dataIndex: "globalrate",
					key: "globalrate",
					width: 120,
					render: (text, record, index) => {
						return <div fieldid='globalrate'>{Number(text.value).toFixed(text.scale)}</div>
					}
				},
				{
					title: <div fieldid='adjustglobalrate'>{this.state.json['exgainslosses-000028']}</div>,/* 国际化处理： 全局本币折本调整汇率*/
					dataIndex: "adjustglobalrate",
					key: "adjustglobalrate",
					width: 150,
					render: (text, record, index) => {
						let { rateTableData } = this.state;
						let value = rateTableData[index] && rateTableData[index].adjustglobalrate.value;
						value = text.edit ? value : Number(value).toFixed(text.scale);
						return <NCNumber
							fieldid='adjustglobalrate'
							value={value}
							scale={Number(text.scale)}
							// disabled={+value == 1}
							onChange={(val) => this.handleRateChange('adjustglobalrate', val, index)}
						/>
					}
				},
			]
		}
		if (showGroupRate) result.push(groupCol);
		if (showGlobalRate) result.push(globalCol);
		return result;
	}

	formatColumn = (columns) => {
		let result = [];
		columns && columns.map(item => {
			if (item.children){
				item.children.map(child => {
					child.width = 120;
				})
			}else{
				item.width = 120;
			}
			result.push(item);
		});
		return result;
	}

	formatData = (data) => {
		let result = [];
		data && data.map((item, index) => {
			item.rowKey = index;
			result.push(item);
		});
		if(data&&data.length>0){
			this.props.button.setButtonDisabled(['print','next'],false)
		}else{
			this.props.button.setButtonDisabled(['print','next'],true)
		}
		return result;
	}

	//查询汇率
	queryRate = () => {
		let { pk_accountingbook, period, pk_transrate } = this.state.searchMap;
		let params = { pk_accountingbook, period, pk_transrate };
		ajax({
			url: API_URL.rate,
			data: params,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let rate;
					if (data) {
						this.setState({
							rateTableData: data
						});
						
					}
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });
			}
		});
	}

	//查询核算账簿信息
	checkColumn = () => {
		ajax({
			url: API_URL.check,
			data: { pk_accountingbook: this.state.searchMap.pk_accountingbook[0]},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let rate;
					if (data) {
						this.setState({
							showGroupRate: data.NC001 ? true : false, //集团本币
							showGlobalRate: data.NC002 ? true : false //全局本币
						});
					}
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });
			}
		});
	}

	//查询接口
	getData = () => {
		let { searchMap, rateTableData } = this.state;
		ajax({
			url: API_URL.query,
			data: searchMap,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let rateTableData = [];
					if (data) {
						rateTableData = data.data

					}
					this.setState({
						transColumns: data.column,
						rateTableData
					})
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'danger' });
			}
		});
	};
	
	//修改调汇汇率
	handleRateChange = (key,val,index) => {
		let { rateTableData } = this.state;
		if (!isNaN(val) || val === '.'){ //只能输入小数
			rateTableData[index][key].value = val;
			rateTableData[index][key].edit = true;//记录下修改状态
		}
		this.setState({
			rateTableData
		});
	}

	render() {
		let { table, editTable, button, search, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let { createModal } = modal;
		let { createEditTable } = editTable;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		let { searchMap, transColumns, rateTableData,transTabledata,step } = this.state;
		let rateColumns = [
			{
				title: <div fieldid="num" className="mergecells">{this.state.json['exgainslosses-000029']}</div>,/* 国际化处理： 序号*/
				dataIndex: "rowKey",
				key: "rowKey",
				width: 80,
				render: (text, record, index) => {
					return <div fieldid="num">{index + 1}</div>
				}
			},
			{
				title: <div fieldid="currtypecode" className="mergecells">{this.state.json['exgainslosses-000030']}</div>,/* 国际化处理： 币种编码*/
				dataIndex: "pk_currtype",
				key: "pk_currtype",
				width: 100,
				render: (text, record, index) => {
					return <div fieldid="currtypecode">{text.display}</div>
				}
			},
			{
				title: <div fieldid="currtypename" className="mergecells">{this.state.json['exgainslosses-000031']}</div>,/* 国际化处理： 原币币种*/
				dataIndex: "currtypename",
				key: "currtypename",
				width: 120
			},
			{
				title: <div fieldid="localrate">{this.state.json['exgainslosses-000032']}</div>,/* 国际化处理： 组织本币*/
				children: [
					{
						title: this.state.json['exgainslosses-000025'],/* 国际化处理： 当前汇率*/
						dataIndex: "localrate",
						key: "localrate",
						width: 120,
						render: (text, record, index) => {
							return <div fieldid="localrate">{Number(text.value).toFixed(text.scale)}</div>
						}
					},
					{
						title: <div fieldid="adjustlocalrate">{this.state.json['exgainslosses-000033']}</div>,/* 国际化处理： 折本调整汇率*/
						dataIndex: "adjustlocalrate",
						key: "adjustlocalrate",
						width: 120,
						render: (text, record, index) => {
							let { rateTableData} = this.state;
							let value = rateTableData[index] && rateTableData[index].adjustlocalrate.value;
							value = text.edit ? value : Number(value).toFixed(text.scale);
							return <NCNumber
								fieldid="adjustlocalrate"
								value={value}
								scale={Number(text.scale)}
								// disabled={+value == 1}
								onChange={(val) => this.handleRateChange('adjustlocalrate', val, index)}
							/>
						}
					},
				]
			},
		];
let height=window.innerHeight-200
		return (
			<div className="nc-bill-list exchange" id="exchangeGainsLossesMaina">
				<HeaderArea
					title= {this.state.json['exgainslosses-000012']}
					searchContent={
						step == 1 ? <div className="mark nc-theme-warn-area-bgc nc-theme-warn-border-bc nc-theme-warn-font-c">
							<div className="mark_inco">
								<Icon type='uf-i-c-2' className="mark_inco_i"/>
							</div>															
								<div className="footer-container">{this.state.json['exgainslosses-000034']}</div> 												
						</div>:''
					}
					btnContent={
						this.props.button.createButtonApp({
							area: 'excae',
							onButtonClick: (props, key) => buttonClick(props, key, this),
							popContainer: document.querySelector('.header-button-area')
						})
					}
				/>
				<ExgainslossesStep step={step}/>
				
				{
					step == 1 ? 
						<div className="header-panel">
							<span className="label">{this.state.json['exgainslosses-000017']}</span>{/* 国际化处理： 会计期间*/}
							<span className="content">{searchMap.period}</span>
						</div>
					: ''
				}
				<div />
				<div className="nc-bill-table-area">
				{
					step == 0 ?
					//维护汇率清单
					<span className="firsta">
						<NCDiv fieldid='firststep' areaCode={NCDiv.config.TableCom}>
						<NCTable 
							columns={this.createRateColumns(rateColumns)}
							data={this.formatData(rateTableData)}
							rowKey='rowKey'
						/>
						</NCDiv>
					</span>
					: 
						step == 1 ?
						//损益计算与结果
						<span className="twoa">
							<NCDiv fieldid='twostep' areaCode={NCDiv.config.TableCom}>
							<NCTable
								columns={this.formatColumn(transColumns)}
								data={this.formatData(transTabledata)}
								scroll={{x: true, y:height}}
								rowKey='rowKey'
							/>
							</NCDiv>
						</span>
						: ''
				}
					
				</div>
				{/* <div className="mark">
					<div className="mark_inco">
						<Icon type='uf-i-c-2' className="mark_inco_i"/>
					</div>
					
					{
					step == 1 ? 	
						<div className="footer-container">提示：1：调整后余额金额 = 原币余额*调汇汇率—调整前本币余额；2：如果您想保留损益计算结果，请打印保留！</div> 
					: ''
					}
				</div>	 */}
			</div>
		);
	}
}

ExchangeTransrate = createPage({
	//initTemplate: initTemplate,
	// mutiLangCode: '2002'
})(ExchangeTransrate);

// ReactDOM.render(<ExchangeTransrate />, document.querySelector('#app'));
export default ExchangeTransrate;
