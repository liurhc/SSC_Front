import { base, ajax, toast, pageTo,cardCache,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
import { URL, pagecode, appcode } from '../../containers';
import cacheUtils from '../../../../gl_transfer/trransferHistory/utils/cacheUtils.js';

export default function buttonClick(props, id, _this) {
    let { selectData, searchMap1 } = _this.state;
    let { pk_accountingbook, pk_accountingbook_refname,period, includeuntally, isback, pk_unit} = searchMap1;
    switch (id) {
        //损益结转
        case 'transfer':
            if (selectData.length === 0){
                toast({color: 'warning',content: _this.state.json['exgainslosses-000000']});/* 国际化处理： 没有选择数据！*/
            }else{
                let pk_transrate = selectData.map(item => item.data.values.pk_transrate.value);
                    
                // props.linkTo(URL.transrate);//跳转至单页应用不要带参数，否则页面会显示不出来，参数放到sessionStorage中
                let searchParam = {
                    pk_accountingbook,
                    period,
                    includeuntally,
                    isback,
                    pk_transrate,
                };
                setGlobalStorage('sessionStorage','exgainslossesParam', JSON.stringify(searchParam));
                props.pushTo('/transrate', {})
            }
            break;
        //历史数据
        case 'history':
            if(pk_accountingbook.length>1){
                toast({ content: _this.state.json['exgainslosses-000001'], color: 'warning' })/* 国际化处理： 请选择一个核算账簿*/
                return
            }else{
                let pk_transrate = selectData.map(item => item.data.values.pk_transrate.value);                
                // props.linkTo(URL.transrate);//跳转至单页应用不要带参数，否则页面会显示不出来，参数放到sessionStorage中
                let searchParam = {
                    transferType: 'gl_transrate',
                    accountingbook:{
                        refpk: pk_accountingbook ? pk_accountingbook[0] : '',
                        refname: pk_accountingbook_refname ? pk_accountingbook_refname[0] : ''
                    },
                    period:{
                        refpk: period ? period : '',
                        refname: period ? period : ''
                    },
                    pk_unit: {
                        refpk: pk_unit ? pk_unit: '',
                        refname: pk_unit ? pk_unit: ''
                    },
                    includeUntallied: includeuntally,
                    isback,
                };
                // let historyData = {
                //     transferType: 'gl_transfer',
                //     accountingbook,
                //     pk_unit,
                //     period,
                //     pk_unitState,
                //     includeUntallied,
                //     transArr,
                //     selectedTrans,
                //     includeUntailiedDisabled
                // }
                cacheUtils.setSearchData(searchParam)
                props.pushTo('/trransferHistory', {               
                    
                })
            }
           
            break;
        
         
    }

}

