import { ajax,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
import { URL, pagecode, appcode } from '../../containers';

// let meta1 = 
// 	{
// 		"excTable": {
// 			"items": [
// 				{
// 					"itemtype": "refer",
// 					"label": this.state.json['exgainslosses-000002'],/* 国际化处理： 核算账簿*/
// 					"maxlength": "20",
// 					"disabled": true,
// 					"attrcode": "bookname",
// 					"visible": true
// 				},
// 				{
// 					"itemtype": "input",
// 					"label": this.state.json['exgainslosses-000003'],/* 国际化处理： 编码*/
// 					"maxlength": "20",
// 					"disabled": true,
// 					"attrcode": "transferno",
// 					"visible": true
// 				},
// 				{
// 					"itemtype": "input",
// 					"label": this.state.json['exgainslosses-000004'],/* 国际化处理： 说明*/
// 					"maxlength": "20",
// 					"disabled": true,
// 					"attrcode": "note",
// 					"visible": true
// 				},

// 				{
// 					"itemtype": "input",
// 					"label": this.state.json['exgainslosses-000005'],/* 国际化处理： 凭证类别*/
// 					"maxlength": "20",
// 					"disabled": true,
// 					"attrcode": "vouchertype",
// 					"visible": true
// 				},
// 				{
// 					"itemtype": "input",
// 					"label": this.state.json['exgainslosses-000006'],/* 国际化处理： 定义人*/
// 					"maxlength": "19",
// 					"disabled": true,
// 					"attrcode": "creator",
// 					"visible": true
// 				},
// 				{
// 					"itemtype": "input",
// 					"label": this.state.json['exgainslosses-000007'],/* 国际化处理： 附单据数*/
// 					"maxlength": "20",
// 					"disabled": true,
// 					"attrcode": "billnum",
// 					"visible": true
// 				}
// 			],
// 			"moduletype": "table",
// 			"pagination": false,
// 			"code": "excTable",
// 			"name": "excTable"
// 		}
// 	}
// let meta2 =
// 	{
// 		"filterTable": {
// 			"items": [
// 				{
// 					"itemtype": "refer",
// 					"label": this.state.json['exgainslosses-000003'],/* 国际化处理： 编码*/
// 					"maxlength": "20",
// 					"attrcode": "transferno",
// 					"visible": true
// 				}
// 			],
// 			"moduletype": "table",
// 			"pagination": false,
// 			"code": "filterTable",
// 			"name": "filterTable"
// 		}
// 	}

export default function initTemplate(props) {
	let page=this
	let meta1 = 
	{
		"excTable": {
			"items": [
				{
					"itemtype": "refer",
					"label": page.state.json['exgainslosses-000002'],/* 国际化处理： 核算账簿*/
					"maxlength": "20",
					"disabled": true,
					"attrcode": "bookname",
					"visible": true
				},
				{
					"itemtype": "input",
					"label": page.state.json['exgainslosses-000003'],/* 国际化处理： 编码*/
					"maxlength": "20",
					"disabled": true,
					"attrcode": "transferno",
					"visible": true
				},
				{
					"itemtype": "input",
					"label": page.state.json['exgainslosses-000004'],/* 国际化处理： 说明*/
					"maxlength": "20",
					"disabled": true,
					"attrcode": "note",
					"visible": true
				},

				{
					"itemtype": "input",
					"label": page.state.json['exgainslosses-000005'],/* 国际化处理： 凭证类别*/
					"maxlength": "20",
					"disabled": true,
					"attrcode": "vouchertype",
					"visible": true
				},
				{
					"itemtype": "input",
					"label": page.state.json['exgainslosses-000006'],/* 国际化处理： 定义人*/
					"maxlength": "19",
					"disabled": true,
					"attrcode": "creator",
					"visible": true
				},
				{
					"itemtype": "input",
					"label": page.state.json['exgainslosses-000007'],/* 国际化处理： 附单据数*/
					"maxlength": "20",
					"disabled": true,
					"attrcode": "billnum",
					"visible": true
				}
			],
			"moduletype": "table",
			"pagination": false,
			"code": "excTable",
			"name": "excTable"
		}
	}
	let meta2 =
	{
		"filterTable": {
			"items": [
				{
					"itemtype": "refer",
					"label": page.state.json['exgainslosses-000003'],/* 国际化处理： 编码*/
					"maxlength": "20",
					"attrcode": "transferno",
					"visible": true
				}
			],
			"moduletype": "table",
			"pagination": false,
			"code": "filterTable",
			"name": "filterTable"
		}
	}
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do',
		data: {
			// pk_appregister: "0001Z31000000005BEGV",
			pagecode: pagecode,
			appcode: appcode
		},
		success: (res) => {
			if (res.success) {
				if (res.data) {
					props.meta.setMeta(meta1);
					props.meta.addMeta(meta2);
					modifierMeta(props, meta1, page);
					props.button.setButtons(res.data);
					props.button.setButtonDisabled(['transfer'], true)
					props.button.setButtonVisible(['prev', 'next', 'print', 'cancel', 'return', 'save'], false);
				}
			}
		}
	});
}
//添加操作列
function modifierMeta(props, meta , page) {
	let porCol = {
		attrcode: 'opr',
		label: page.state.json['exgainslosses-000008'],/* 国际化处理： 操作*/
		visible: true,
		itemtype: 'customer',
		className: 'table-opr',
		width: 150,
		render(text, record, index) {
			return <a href="javascript:;" onClick={() => {
				let { pk_accountingbook, period, includeuntally, isback } = page.state.searchMap1;
				let searchParam = {
					pk_accountingbook,
					period,
					includeuntally,
					isback,
					pk_transrate: [record.values.pk_transrate.value],
				};
				setGlobalStorage('sessionStorage','exgainslossesParam', JSON.stringify(searchParam));
				// props.linkTo(URL.transrate);//跳转至单页应用不要带参数，否则会显示不出来，参数放到sessionStorage中
				props.pushTo('/transrate', {}) 
			}}>{page.state.json['exgainslosses-000009']}</a>/* 国际化处理： 生成*/
		}
	};
	if(meta['excTable'].items[meta['excTable'].items.length-1].attrcode!='opr'){
		meta['excTable'].items.push(porCol);
	}
	

	return meta;
}

