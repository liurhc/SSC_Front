//汇兑损益结转

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast ,cardCache,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const { NCTable, NCSelect, NCCheckbox, NCButton, NCDropdown,NCIcon:Icon,NCDiv } = base;
const NCOption = NCSelect.NCOption;
import { buttonClick, initTemplate } from './events';
//import AccountBookTreeRef from '../../../../uapbd/refer/org/AccountBookTreeRef/index.js';
// import AccPeriodDefaultTreeGridRef from '../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import { pagecode, appcode } from '../containers';
import '../containers/index.less';
import './index.less'
import cacheUtils from '../../../gl_transfer/trransferHistory/utils/cacheUtils.js';
import ReferLoader from '../../../public/ReferLoader/index.js'; 
import HeaderArea from '../../../public/components/HeaderArea';
let accbookRefCode = 'uapbd/refer/org/AccountBookTreeRef/index.js';
let { setDefData, getDefData } = cardCache;
class ExchangeGainsLosses extends Component {
	constructor(props) {
		super(props);
		this.tableId = 'excTable';
		this.filterTableId = 'filterTable';
		this.state={
			json:{},
			ref1: [], //核算账簿参照
			ref2: {}, //会计区间参照
			searchMap: {},
			searchMap1: { 
				pk_accountingbook: [], 
				period: "", 
				includeuntally: "N", 
				isback: "N" ,
				pk_accountingbook_refname: '',
				pk_unit:''
			},
			oTableData: [], //接口返回的原始数据
			filterBtnDisabled: true,//筛选按钮是否禁用
			dropdownShow: false,//下拉框显示
			selectData: [],//勾选数据
			selTransferno: [],//筛选表格勾选数据
			tableData: []
		}
		//initTemplate.call(this, props, this);
	}
	componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
				initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'exgainslosses',domainName:'gl',currentLocale:'simpchn',callback});
    }
	componentDidMount() {
		// let tempData = cacheUtils.getSearchData()
		let{ref1,searchMap, searchMap1}=this.state;
		let pk_accountingbook,pk_accountingbook_refname,value;
		let cachevalue=getDefData('value','gl.exgainslosses.main.value');
		let cachesearchMap=getDefData('searchMap','gl.exgainslosses.main.searchMap');
		let cachesearchMap1=getDefData('searchMap1','gl.exgainslosses.mian.searchMap1');
		if(cachevalue){
			// let pk_accountingbook = ref1.map(item => item.refpk);
			// let pk_accountingbook_refname = ref1.map(item => item.refname);
			// searchMap.pk_accountingbook = pk_accountingbook;
			// searchMap1.pk_accountingbook = pk_accountingbook;
			// searchMap1.pk_accountingbook_refname = pk_accountingbook_refname;
			// value =[{refpk:defaultAccountBook.value,refname:defaultAccountBook.display}];
			this.setState({
				ref1: cachevalue,
				searchMap:cachesearchMap,
				searchMap1:cachesearchMap1
			}, () => {
				this.getData();
			});
		}else{
			getDefaultAccountBook(appcode).then((defaultAccountBook)=>{
				// let defaultAccountBook = getDefaultAccountBook(appcode);
				if(defaultAccountBook && defaultAccountBook.value){
					pk_accountingbook = [defaultAccountBook.value];
					pk_accountingbook_refname = [defaultAccountBook.display];
					searchMap.pk_accountingbook = pk_accountingbook;
					searchMap1.pk_accountingbook = pk_accountingbook;
					searchMap1.pk_accountingbook_refname = pk_accountingbook_refname;
					value =[{refpk:defaultAccountBook.value,refname:defaultAccountBook.display}];
					this.setState({
						ref1: value,
						searchMap,
						searchMap1
					}, () => {
						this.getData();
					});
				}
				// let accountingbook=[{refname:defaultAccountBook.display,refpk:defaultAccountBook.value}];
				// this.setState({
				// 	ref1: accountingbook
				// })
			})
		}
	}

	formatData = (data) => {
		let newData = JSON.parse(JSON.stringify(data));
		let result = { rows: [] };
		newData && newData.map(item => {
			result.rows.push({
				values: item
			});
		});
		result.rows.map(item => {
			for(let key in item.values){
				item.values[key] = { 
					value: item.values[key] 
				}
			}
		});
		return result;
	}

	// formatData = (data) => {
	// 	let result = [];
	// 	data && data.map((item, index) => {
	// 		item.rowKey = index;
	// 		result.push(item);
	// 	});
	// 	return result;
	// }

	//查询接口
	getData = () => {
		let { searchMap, tableData, currentRow } = this.state;
		if(searchMap.pk_accountingbook && searchMap.pk_accountingbook.length>0&& searchMap.pk_accountingbook[0].length>0){
			ajax({
				url: '/nccloud/gl/transrate/transratedefquery.do',
				data: searchMap,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						let fdata = this.formatData(data);
						let uniqueData = this.unique(data,'transferno');//将返回数据去重放到筛选表格中
						this.props.editTable.setTableData(this.tableId, fdata);
						this.props.editTable.setTableData(this.filterTableId, this.formatData(uniqueData));
						this.setState({
							filterBtnDisabled: data[0] ? false : true,
							oTableData: data
						});
					}
				},
				error: function (res) {
					toast({ content: res.message, color: 'danger' });
				}
			});
	   }else{
		this.setState({
			filterBtnDisabled:true
		},()=>{
			this.props.editTable.setTableData(this.tableId, {rows:[]});
			setTimeout(
				()=>{this.props.editTable.setTableData(this.filterTableId, {rows:[]})}
			,0)}
		)
	   }
		if(searchMap.pk_accountingbook && searchMap.pk_accountingbook.length>0&& searchMap.pk_accountingbook[0].length>0){
			ajax({
				url: '/nccloud/gl/glpub/queryBizDate.do',
				data: {"pk_accountingbook":searchMap.pk_accountingbook[0]},
				success: (res) => {
					let period ={};
					period.pk_accperiodscheme = res.data.pk_accperiodscheme;
					period.refname=res.data.bizPeriod;
					period.refpk=res.data.bizPeriod;
					this.setState({ref2:period});
					 let { searchMap1 } = this.state;
                    searchMap1['period'] = period.refname;
				},
				error: function (res) {
					toast({ content: res.message, color: 'danger' });
				}
			});
		}

	};

	//数组中对象去重
	unique = (data,key) => {
		let hash = {};
		let newData = JSON.parse(JSON.stringify(data));
		return newData.reduce((item, next) => {
			hash[next[key]] ? '' : hash[next[key]] = true && item.push(next);
			return item
		}, []);
	}

	handleSearchReferChange = (ref,key,val) => {
		let { searchMap, searchMap1 } = this.state;
		searchMap1[key] = val.refname;
		this.setState({
			[ref]: val,
			searchMap1
		});
	}

	//单选
	onSelectedFn = (props, moduleId, record, index, status) => {
		let selData = props.editTable.getCheckedRows(moduleId);
		this.setState({
			selectData: selData
		},()=>{
		})
	}

	//点击筛选按钮
	handleFilterClick = () => {
		let { selTransferno, oTableData } = this.state;
		let data = [];
		selTransferno.map(val => {
			let rows = oTableData.filter(item => item.transferno === val);
			data = [...data, ...rows];
		});
		
		this.props.editTable.setTableData(this.tableId, this.formatData(data));
		this.setState({
			dropdownShow: false
		})
		// this.props.editTable.setFiltrateTableData(this.tableId, 'transferno', selTransferno, false);
	}

	//点击筛选表格选择框
	onFilterSelectedFn = (props, moduleId, record) => {
		let checkRows = props.editTable.getCheckedRows(moduleId);
		let selTransferno = checkRows.map(item => item.data.values.transferno.value);
		this.setState({
			selTransferno: selTransferno
		})
	}

	//筛选下拉框render
	filterDropRender = () =>{
		return <div>
			<div className="header-title">{this.state.json['exgainslosses-000010']}</div>{/* 国际化处理： 筛选*/}
			<div className="body-area">
				{this.props.editTable.createEditTable(this.filterTableId, {
					onSelected: this.onFilterSelectedFn,                        // 左侧选择列单个选择框回调
					onSelectedAll: this.onFilterSelectedFn,                  // 左侧选择列全选回调
					showCheck: true
				}
				)}
			</div>
			<div className="btn-area btnbtn">
				<NCButton colors="primary" onClick={this.handleFilterClick} className="choicetwo">{this.state.json['exgainslosses-000010']}</NCButton>{/* 国际化处理： 筛选*/}
				<NCButton onClick={() => {
					this.setState({
						dropdownShow: false
					})
				}} className="choicetwo">{this.state.json['exgainslosses-000011']}</NCButton>{/* 国际化处理： 取消*/}
			</div>
		</div>
	}
	selectedChange=(props,newVal,oldVal)=>{//选中行发生变化
		if(oldVal != 0){
			props.button.setButtonDisabled(['transfer'], false)
		}else{
			props.button.setButtonDisabled(['transfer'], true)
		}
	}
	render() {
		let { table, editTable, button, search, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let { createModal } = modal;
		let { createEditTable } = editTable;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		let { searchMap, searchMap1, tableData, selectData } = this.state;

		return (
			<div className="nc-bill-list" id="exchangeGainsLossesMain">
				<HeaderArea
					title={this.state.json['exgainslosses-000012']/* 国际化处理： 汇兑损益结转*/}
					searchContent={
						<div className="mark nc-theme-warn-area-bgc nc-theme-warn-border-bc nc-theme-warn-font-c">
							<div className="mark_inco">
								<Icon type='uf-i-c-2' className="mark_inco_i"/>
							</div>
							<div className="footer-container">{this.state.json['exgainslosses-000013']}</div>{/* 国际化处理： 提示,规则结转生成凭证后,如果后续业务变动有相关凭证录入系统,会导致这张结转凭证数据不准确*/}
						</div>
					}
					btnContent={
						<div>
							{this.props.button.createButtonApp({
								area: 'excae',
								onButtonClick: (props, key) => buttonClick(props, key, this),
								popContainer: document.querySelector('.header-button-area')
							})}
							<NCDropdown
								trigger={['click']}
								overlay={this.filterDropRender()}
								animation="slide-up"
								visible={this.state.dropdownShow}
								overlayClassName="exch-filter-dropdown-container nc-theme-area-bgc nc-theme-common-font-c"
							>
								<NCButton disabled={this.state.filterBtnDisabled} 
									onClick={() => {
										this.setState({
											dropdownShow: true
										})
									}} 
									className="choice choiceone">{this.state.json['exgainslosses-000010']}</NCButton>{/* 国际化处理： 筛选*/}
							</NCDropdown>
						</div>
					}
				/>
				<div className="transrate nc-theme-area-bgc nc-theme-form-label-c">
					<NCDiv fieldid='transrate' areaCode={NCDiv.config.FORM}>
					<div className="header-panel">
						<span className="redstar">*</span>
						<span className="label">{this.state.json['exgainslosses-000002']}</span>{/* 国际化处理： 核算账簿*/}
						<div className="content" style={{ width: 200 }}>
							<ReferLoader
								fieldid='pk_accountingbook'
								tag='AccountBookTreeRef'
								refcode = {accbookRefCode}
								value={this.state.ref1}
								onChange={(value) => {
									let { searchMap, searchMap1} = this.state;
									let pk_accountingbook = value.map(item => item.refpk);
									let pk_accountingbook_refname = value.map(item => item.refname);
									searchMap.pk_accountingbook = pk_accountingbook;
									searchMap1.pk_accountingbook = pk_accountingbook;
									searchMap1.pk_accountingbook_refname = pk_accountingbook_refname;
									setDefData('value','gl.exgainslosses.main.value',value);
									setDefData('searchMap','gl.exgainslosses.main.searchMap',searchMap);
									setDefData('searchMap1','gl.exgainslosses.mian.searchMap1',searchMap1);
									this.setState({
										ref1: value,
										searchMap,
										searchMap1
									}, () => {
										this.getData();
									});

								}}
								refType="tree"
								isMultiSelectedEnabled={true}
								disabledDataShow={true}
								queryCondition={{
									TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
									appcode
								}}
							/>
						</div>
						<span className="label">{this.state.json['exgainslosses-000017']}</span>{/* 国际化处理： 会计期间*/}
						<div className="content" style={{ width: 150 }}>
							<ReferLoader
								fieldid='period'
								tag='AccPeriodDefaultTreeGridRef'
								refcode='uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index.js'
								value={this.state.ref2}
								onChange={(value) => {
									value.pk_accperiodscheme=this.state.ref2.pk_accperiodscheme;
									this.handleSearchReferChange('ref2', 'period', value)
								}}
								refType="gridTree"
								isMultiSelectedEnabled={false}
								queryCondition={{
									pk_accperiodscheme : this.state.ref2.pk_accperiodscheme,
									GridRefActionExt: 'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
								}}
					/> 
						</div>
						<span className="label">
							<NCCheckbox 
								fieldid='includeuntally'
								colors="info"
								onChange={(v) => {
									searchMap1.includeuntally = v ? 'Y' : 'N';
									this.setState({
										searchMap1: searchMap1
									})
								}}
								checked={searchMap1.includeuntally === 'Y'}
							>{this.state.json['exgainslosses-000018']}</NCCheckbox>{/* 国际化处理： 包含未记账凭证*/}
						</span>
						<span className="label">
							<NCCheckbox 
								fieldid='isback'
								colors="info"
								onChange={(v) => {
									this.state.searchMap1.isback = v ? 'Y' : 'N';
									this.setState({
										searchMap1: this.state.searchMap1
									})
								}}
								checked={searchMap1.isback === 'Y'}
							>{this.state.json['exgainslosses-000019']}</NCCheckbox>{/* 国际化处理： 后台生成*/}
						</span>
						
					</div>
					</NCDiv>
					<div />
					<div className="nc-bill-table-area">
						{createEditTable(this.tableId, {
							// onAfterEvent: afterEventFn,                      // 控件的编辑后事件      
							onSelected: this.onSelectedFn,                        // 左侧选择列单个选择框回调
							onSelectedAll: this.onSelectedFn,                  // 左侧选择列全选回调
							// selectedChange: selectedChangeFn,                // 选择框有变动的钩子函数
							showIndex: true, //显示序号列
							showCheck: true, //显示复选框
							selectedChange: this.selectedChange.bind(this)
							}                                  
						)}

					</div>
				</div>
			</div>
		);
	}
}

ExchangeGainsLosses = createPage({
	// initTemplate: initTemplate,
	// mutiLangCode: '2002'
})(ExchangeGainsLosses);

//ReactDOM.render(<ExchangeGainsLosses />, document.querySelector('#app'));
export default ExchangeGainsLosses;
