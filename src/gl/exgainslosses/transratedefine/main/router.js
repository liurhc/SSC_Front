import { asyncComponent } from 'nc-lightapp-front';
import TransratedefineList from '../list/transratedefineList';

const TransratedefineCard = asyncComponent(() => import(/* webpackChunkName: "gl/exgainslosses/transratedefine/card/card" */ /* webpackMode: "eager" */  '../card/transratedefineCard'));

const routes = [
	{
		path: '/',
		component: TransratedefineList,
		exact: true
	},
	{
		path: '/list',
		component: TransratedefineList
	},
	{
		path: '/card',
		component: TransratedefineCard
	}
];

export default routes;
