import React, {Component} from 'react';
import {base,getMultiLang} from 'nc-lightapp-front';
import ReferLoader from '../../../../public/ReferLoader/index.js';
import './index.less'
const { NCModal, NCButton} = base;
class WorkbenchDetail extends Component {
    constructor(props) {
        super();
        this.state = {
            headRef: {},
            pk_accountingbookmap:'',
            appcode:props.appcode,
            json:{}
        }
    }

    componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{

			})
		}
        getMultiLang({moduleId:'20020EXCAD',domainName:'gl',currentLocale:'zh-CN',callback});
	}
    /**
     * 参照变更
     * @param {*} data 
     */
    headRefChange(data){
        // let refArr = [];
        // let refmap = {};
        // for (var i=0;i<data.length;i++) {
        //     refmap['pk'] = data[i].refpk;
        //     refmap['name'] = data[i].name;
        //     refArr.push(refmap);
        // }
        // this.setState({pk_accountingbookmap : refArr});
        this.setState({headRef : data});
        let refmap = "[";
        for (var i=0;i<data.length;i++) {
            refmap += "{'pk':'" + data[i].refpk + "','name':'" + data[i].refname + "'},";
        }
        refmap = refmap.substr(0, refmap.length-1);
        refmap += "]";
        this.setState({pk_accountingbookmap : refmap});
    }
    /**
     * 保存按钮点击
     */
    saveFormButtonClick() { 
        this.props.saveFormButtonClick(this.state.pk_accountingbookmap);
        //执行完后清空
        // this.setState({pk_accountingbookmap : ""});
    }
    /**
     * 取消按钮点击
     */
    cancelFormButtonClick() {
        //取消清空参照map
        // this.setState({pk_accountingbookmap : ""});
        this.props.onClose();
    };
    render(){
        return(
            <div>
                <NCModal fieldid='cp2' className='copyto' show={this.props.showFormModal} size="lg" id="copytoother">
                    <NCModal.Header>
                        {/* TODO：多语 */}
                        <NCModal.Title>{this.state.json['20020EXCAD-000010']}</NCModal.Title>{/* 国际化处理： 复制到*/}
                    </NCModal.Header>
                    <NCModal.Body>
                        {/* <ReferLoader
                            tag={'test'}
                            refcode={'uapbd/refer/org/AccountBookTreeRef/index.js'}
                            queryCondition={()=>{
                                return {
                                    "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    "appcode":  '20020EXCAD_CARD'
                                }
                            }}
                            value={this.state.headRef}
                            onChange={this.headRefChange.bind(this)}
                            isMultiSelectedEnabled={true}
                            showGroup={false}
                            showInCludeChildren={true}
                        /> */}
                        <ReferLoader
                            fieldid='accountbook_ref'
                            tag={'test'}
                            refcode={'uapbd/refer/org/AccountBookTreeRef/index.js'}
                            disabledDataShow={true}
                            queryCondition={()=>{
                                return {
                                    "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    "appcode": this.state.appcode,
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                            }}
                            value={this.state.headRef}
                            onChange={this.headRefChange.bind(this)}
                            isMultiSelectedEnabled={true}
                            showGroup={false}
                            showInCludeChildren={true}
                        />
                    </NCModal.Body>
                    <NCModal.Footer>
                        <NCButton
                            colors="primary"
                            onClick={this.saveFormButtonClick.bind(this)}
                            fieldid='confirm'
                        >
                            {/* TODO：多语 */}
                            {this.state.json['20020EXCAD-000007']}{/* 国际化处理： 保存*/}
                        </NCButton>
                        <NCButton 
                            fieldid='cancel'
                            onClick={this.cancelFormButtonClick.bind(this)}>
                            {/* TODO：多语 */}
                            {this.state.json['20020EXCAD-000008']}{/* 国际化处理： 取消*/}
                        </NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}
export default WorkbenchDetail;
