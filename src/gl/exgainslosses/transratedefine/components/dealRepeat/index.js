import React, {Component} from 'react';
import {base,getMultiLang} from 'nc-lightapp-front';
import AccountBookGridRef from '../../../../../uapbd/refer/org/AccountBookGridRef';
const { NCModal, NCButton, NCCheckbox } = base;
class WorkbenchDetail1 extends Component {
    constructor(props) {
        super();
        this.state = {
            showmodel:false,
            headRef: {},
            repeatmap:{},
            uncheckrepeat:{},
            json:{}
        }
    }

    componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{

			})
		}
        getMultiLang({moduleId:'20020EXCAD',domainName:'gl',currentLocale:'zh-CN',callback});
    }
    
    componentWillReceiveProps(nextProps){
        let repeatmap = nextProps.repeatmap;
        let uncheckrepeat = this.clone(repeatmap);
        this.setState({uncheckrepeat});
    }

    /**
     * 保存按钮点击
     */
    yes() { 
        //判断uncheckrepeat为空是没选还是全选
        if (!this.judgeObj(this.state.uncheckrepeat) && !this.judgeObj(this.state.repeatmap)) {
            this.props.yes(this.props.repeatmap);
        } else {

            this.props.yes(this.state.uncheckrepeat);
        }
        this.setState({uncheckrepeat: {}});
    }
    
    judgeObj (obj){
        for(var item in obj){
            return true;
        }
        return false;
    }

    /**
     * 取消按钮点击
     */
    no() {
        this.setState({uncheckrepeat: this.props.repeatmap});//此时都未选中
        this.props.no(this.state.uncheckrepeat);
        this.setState({uncheckrepeat: {}});
    };
    /**
     * 拿未选中的数据
     * @param {*} pk 
     * @param {*} name 
     */
    checkboxChange(pk, name) {

        var uncheckrepeat = this.judgeObj(this.state.uncheckrepeat) == true ? this.state.uncheckrepeat : this.clone(this.props.repeatmap);//存放未勾选的
        var repeatmap = this.state.repeatmap;
        if (uncheckrepeat[pk]) {
            delete uncheckrepeat[pk];
            this.setState({uncheckrepeat: uncheckrepeat});

            var map = {};
            map[pk] = pk;
            map[name] = name;
            repeatmap[pk] = map;
            this.setState({repeatmap: repeatmap});
            return;
        } else {

            var map = {};
            map[pk] = pk;
            map[name] = name;
            uncheckrepeat[pk] = map;
            if (repeatmap[pk]) {
                delete repeatmap[pk];
            }
    
            this.setState({uncheckrepeat: uncheckrepeat});
            this.setState({repeatmap: repeatmap});
        }

    }

    /**
     * 对象clone
     * @param {*} obj 
     */
    clone (obj) {
        if (null == obj || "object" != typeof obj) return obj;

        // Handle Date
        if (obj instanceof Date) {
            var copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }
        
        // Handle Array
        if (obj instanceof Array) {
            var copy = [];
            for (var i = 0; i < obj.length; ++i) {
            copy[i] = clone(obj[i]);
            }
            return copy;
        }
        
        // Handle Object
        if (obj instanceof Object) {
            var copy = {};
            for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = this.clone(obj[attr]);
            }
            return copy;
        }
    };

    showList() {
        var show = [];
        var  repeatmap = this.props.repeatmap;
        // var  repeatmap={};
        //         var map = {};
        //         map["pk"] = "1111";
        //         map["name"] = "22222";
        //         repeatmap['1111'] = map;
        //         var map1 = {};
        //         map1["pk"] = "3333";
        //         map1["name"] = "44444";
        //         repeatmap['3333'] = map1;

        if (repeatmap) {
       
            for (var i in repeatmap) {
                show.push((
                    <li>
                        <NCCheckbox onChange={this.checkboxChange.bind(this, repeatmap[i].pk, repeatmap[i].name)}>{repeatmap[i].name}</NCCheckbox>
                    </li>
                ))
               
            }
 
        }
        return <ul style={{fontSize:'15px',color:'#666', paddingLeft:'40px'}}>{show}</ul>;
    }

    close () {
        this.props.closedealRepeatModel();
    }
    

    render(){
        let checkboxMap = this.showList();
        return(
            <div>
                <NCModal fieldid='deal_repeat' show={this.props.showFormModal} 
                    size="lg" 
                    onHide={this.close.bind(this)}
                >
                {/* <NCModal show={true} size="lg"> */}
                    <NCModal.Header closeButton="true">
                        {/* TODO：多语 */}
                        <NCModal.Title>{this.state.json['20020EXCAD-000037']}：</NCModal.Title>{/* 国际化处理： 以下记录已存在*/}
                    </NCModal.Header>
                    <NCModal.Body>
                        <div className="demo-checkbox">
                            {checkboxMap}
                            
                        </div>
                        <div style={{textAlign:'center'}}>
                            {this.state.json['20020EXCAD-000047']}{/* 国际化处理：以上账簿存在相同转账编号的自定义转账定义，请选择需要覆盖的核算账簿*/}
                        </div>
                    </NCModal.Body>
                    <NCModal.Footer>
                        <NCButton
                            fieldid='confirm'
                            colors="primary"
                            onClick={this.yes.bind(this)}
                        >
                            {/* TODO：多语 */}
                            {this.state.json['20020EXCAD-000048']}{/* 国际化处理： 确定*/}
                        </NCButton>
                        <NCButton 
                            fieldid='cancel'
                            onClick={this.no.bind(this)}>
                            {/* TODO：多语 */}
                            {this.state.json['20020EXCAD-000049']}{/* 国际化处理： 取消*/}
                        </NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}
export default WorkbenchDetail1;
