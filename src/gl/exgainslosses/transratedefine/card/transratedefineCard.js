import {Component} from 'react';
import {createPage, base, cardCache,getMultiLang,createPageIcon} from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
const {NCSelect, NCIcon, NCBackBtn} = base;

import {initTemplate, buttonClick, query, afterEvent, listAfterEvent} from './events/index'
import Copyto from '../components/copyto'
import DealRepeat from '../components/dealRepeat'
import presetVar from './presetVar'
import listPresetVar from '../list/presetVar'
import requestApi from './requestApi'

import './index.less';
import {CardLayout, Form, Table, ModalArea,TopArea} from '../../../../fipub/public/components/layout/CardLayout';
import {Header, HeaderSearchArea, HeaderButtonArea, HeaderPaginationArea} from '../../../../fipub/public/components/layout/Header';



class TransratedefineCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paginationShowClassName: '',
            [presetVar.formField.pk_accsubjpl]: '',
            [presetVar.formField.pk_accsubjprofit]: '',
            [presetVar.formField.pk_accsubjloss]: '', 
            showDealRepeatModal:false,
            checkStatus: '',
            pk: '',
            repeatmap:'',
            pk_accountingbookmap:'',
            uncheckrepeatmap:{},
            json:{}       
        };
        this.pageParam = getDefData('pageParam', listPresetVar.dataSource);
    }
    componentDidMount() {
        
    }

    componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{
				initTemplate.call(this,this.props);
			})
		}
        getMultiLang({moduleId:'20020EXCAD',domainName:'gl',currentLocale:'zh-CN',callback});
        window.onbeforeunload = () => {
            let status = this.props.getUrlParam("status");
            if (status == 'edit' || status == 'add') {
                return this.state.json['20020EXCAD-000036']/* 国际化处理： 确定要离开吗？*/
            }
        }
	}
    searchClick(props,data,type) {
        query.queryData.call(this, props, data);
    }
    pageInfoClick(props,data){
        props.setUrlParam({id:data});
        props.cardPagination.setCardPaginationId({id:data});
        query.queryData(props, this.props.getUrlParam('id'));
    }
    closeFormModal(){
        this.setState({showFormModal:false});
    }
    closedealRepeatModel() {
        this.setState({showDealRepeatModal: false});
    }
    saveFormButtonClick(refmap) {
        this.setState({pk_accountingbookmap:refmap});//记录全部数据
        query.doCopyTo.call(this, this.props, refmap);
    }
    dealRepeat(refmap) {
        //解决state异步问题
        this.setState(({checkStatus:'N'}), () => {
            // var uncheckrepeatmap = this.getFinalDatas();
            var pk_accountingbookmap = this.state.pk_accountingbookmap;
            query.doCopyTo.call(this, this.props, pk_accountingbookmap, refmap);
        });
    }
    dealCancelCopy(){
        this.setState({showFormModal:false});
        this.setState({showDealRepeatModal:false});
        this.setState({pk_accountingbookmap:{}});//清空全部数据
    }
    /**
     * 取原总记录扣掉第二次不覆盖的
     */
    getFinalDatas() {
        var repeatmap = this.state.repeatmap;
        var pk_accountingbookmap = this.state.pk_accountingbookmap;
        var uncheckrepeatmap = this.status.uncheckrepeatmap;
        for (var i in repeatmap) {
            if (pk_accountingbookmap[i]) {
                uncheckrepeatmap[i] = i;
            }
        }
        return uncheckrepeatmap;
    }
    //获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp} = button;
		return (
			<div className="shoulder-definition-area">
			{this.props.getUrlParam("status") != "browse"?
				<div className="definition-icons">
				{createButtonApp({
                    area: presetVar.listShoulderButtonArea, 
                    buttonLimit: 3,
                    onButtonClick: buttonClick.bind(this)
                })}
            </div>:''}
			</div>
		)
    }
    updateButtonStatus(){
		//此处控制按钮的隐藏显示及启用状态
        let tableData = this.props.cardTable.getCheckedRows(presetVar.list);
        if(tableData && tableData.length>0){
            this.props.button.setButtonDisabled({
                DelLine: false
            });
        }else{
            this.props.button.setButtonDisabled({
                DelLine: true
            });
        }
		
    }
    render() {
        let multiLang = this.props.MutiInit.getIntl(2002); 
        const {cardTable, search, button, cardPagination, modal, form} = this.props;
        const {createModal} = modal;
        const {createForm} = form;
        
        const { createCardPagination } = cardPagination;
        const {createCardTable} = cardTable;
        const {createButtonApp} = button;
        const {NCCreateSearch} = search;
        let status=this.props.getUrlParam('status');
        let getCreateCardPagination=()=>{
            
         if(this.props.getUrlParam('status') == 'browse'){
                    {createCardPagination({
                        dataSource: listPresetVar.dataSource,
                        handlePageInfoChange:this.pageInfoClick.bind(this)
                    })}
            }   
        }
        return (
            <CardLayout>
                <TopArea>
                <Header
                    showGoback={status=='browse'}
                    onBackClick={()=>{buttonClick.call(this, this.props, 'Return')}}
                >
                    <HeaderButtonArea>
                        {createButtonApp({
                            area: presetVar.headButtonArea, 
                            buttonLimit: 3,
                            onButtonClick: buttonClick.bind(this)
                        })}
                    </HeaderButtonArea>
                    <HeaderPaginationArea>
                        {this.props.getUrlParam('status') == 'browse' ?
                            createCardPagination({
                                dataSource: listPresetVar.dataSource,
                                handlePageInfoChange:this.pageInfoClick.bind(this)
                            }):''
                        }
                    </HeaderPaginationArea>
                </Header>
                <Form>
                    {createForm(presetVar.formId, {
                        onAfterEvent: (props,moduleId,key,value, changedrows)=>{afterEvent.call(this, props,moduleId,key,value, changedrows)}
                    })}
                </Form>
                </TopArea>
                <Table>
                    {createCardTable(presetVar.list, {
                        adaptionHeight:true,
                        tableHead: this.getTableHead.bind(this),
                        onAfterEvent: listAfterEvent.bind(this),
                        showCheck: true,
                        selectedChange: this.updateButtonStatus.bind(this),
                        hideSwitch:()=>{return false;},
                        showIndex: true
                    })}
                </Table>
                <ModalArea>
                    <Copyto 
                        showFormModal={this.state.showFormModal}
                        onClose={this.closeFormModal.bind(this)}
                        saveFormButtonClick={this.saveFormButtonClick.bind(this)}
                        appcode={this.props.getSearchParam('c')}
                    />
                    <DealRepeat 
                       showFormModal={this.state.showDealRepeatModal}
                        yes={this.dealRepeat.bind(this)}
                        no={this.dealCancelCopy.bind(this)}
                        repeatmap={this.state.repeatmap}
                        closedealRepeatModel={this.closedealRepeatModel.bind(this)}
                    />
                </ModalArea>
            </CardLayout>
        )
    }
}
TransratedefineCard = createPage({mutiLangCode: '2002'})(TransratedefineCard);
export default TransratedefineCard;
