import {ajax } from 'nc-lightapp-front';
import pubUtils from '../../../public/common/pubUtil'
let requestDomain =  '';

let requestApiOverwrite = {
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/gl/transratedefine/cardquery.do',
            data: {...opt.data, ...pubUtils.getSysCode(opt.props)},
            success: opt.success
        });
    },
    // 保存
    save: (opt) => {
        ajax({
            url: '/nccloud/gl/transratedefine/save.do',
            data: {...opt.data, ...{['userjson']: JSON.stringify(pubUtils.getSysCode(opt.props))}},
            success: opt.success
        });
    },
    // 删除
    delete: (opt) => {
        ajax({
            url: '/nccloud/gl/transratedefine/delete.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 复制到账簿
    copyTo: (opt) => {
        ajax({
            url: '/nccloud/gl/transratedefine/copyto.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查询末级科目
    subRelationDataQuery: (opt) => {
        ajax({
            url: '/nccloud/gl/cashflow/SubCfQueryAccasoaAction.do',
            data: opt.data,
            async:false, 
            success: opt.success
        });
    },
}

export default  requestApiOverwrite;
