import {base} from 'nc-lightapp-front';
const {NCMessage} = base;
import {fixedTemplet} from './index'
import requestApi from '../requestApi'
import presetVar from '../presetVar'
export default function afterEvent(props,moduleId,key,value, changedrows) {
    if(moduleId == presetVar.formId){
        switch(key){
            // 损益科目不为空时清空收益科目、损失科目
            case presetVar.formField.pk_accsubjpl:
                if(value != null && value.value != null){
                    props.form.setFormItemsValue(presetVar.formId, {pk_accsubjprofit:{display:'',value:''},assprofit:{display:'',value:''},pk_accsubjloss:{},assloss:{display:'',value:''},asspl:{display:'',value:''}});
                    // this.setState({[presetVar.formField.pk_accsubjpl]:value.value});
                }else{
                    props.form.setFormItemsValue(presetVar.formId, {asspl:{display:'',value:''}});
                }
            break;
            // 收益科目不为空时清空损失科目
            case presetVar.formField.pk_accsubjprofit:
                if(value != null && value.value != null){
                    props.form.setFormItemsValue(presetVar.formId, {pk_accsubjpl:{display:'',value:''},asspl:{display:'',value:''},assprofit:{display:'',value:''}});
                    // this.setState({[presetVar.formField.pk_accsubjprofit]:value.value});
                }else{
                    props.form.setFormItemsValue(presetVar.formId, {assprofit:{display:'',value:''}});
                }
            break;
            // 损失科目不为空时清空损益科目、收益科目
            case presetVar.formField.pk_accsubjloss:
                if(value != null && value.value != null){
                    props.form.setFormItemsValue(presetVar.formId, {pk_accsubjpl:{display:'',value:''},asspl:{display:'',value:''},assloss:{display:'',value:''}});
                    // this.setState({[presetVar.formField.pk_accsubjloss]:value.value});
                }else{
                    props.form.setFormItemsValue(presetVar.formId, {assloss:{display:'',value:''}});
                }
            break;
        }
    }
}
