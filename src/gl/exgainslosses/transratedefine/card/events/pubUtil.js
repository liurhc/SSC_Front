import {listButtonClick} from './index'
import {getBusinessInfo} from 'nc-lightapp-front';
let pageStatusChange = function(props, status){
    if(status == 'add'){
        addOpr.call(this, props);
        this.setState({paginationShowClassName:'hide'});
        props.button.setButtonVisible({'Add':false,'Save': true, 'Cancel': true, 'Edit': false, 'Delete':false, 'AddLine':true, 'Copyto': false,'Refresh': false});
        props.form.setFormStatus(presetVar.formId, 'add');
        props.cardTable.setStatus(presetVar.list, 'edit');
    }else if(status == 'edit'){
        addOpr.call(this, props);
        this.setState({paginationShowClassName:'hide'});
        props.button.setButtonVisible({'Save': true, 'Cancel': true, 'Edit': false, 'Delete':false, 'AddLine':true, 'Return': false, 'CopyTo': false});
        props.form.setFormStatus(presetVar.formId, 'edit');
        props.cardTable.setStatus(presetVar.list, 'edit');
    }else if(status == 'browse'){
        this.setState({paginationShowClassName:''});
        props.button.setButtonVisible({'Add': true,'Save': false, 'Cancel': false, 'Edit': true, 'Delete':true, 'AddLine':false, 'Refresh': true, 'Copyto': true});
        props.form.setFormStatus(presetVar.formId, 'browse');
        props.cardTable.setStatus(presetVar.list, 'browse');
        removeOpr.call(this, props);
    }
    // props.setUrlParam({status:status})
}
let addOpr = function(props){
    let meta = props.meta.getMeta();
    let event = {
        label: this.state.json['20020EXCAD-000035'],/* 国际化处理： 操作*/
        attrcode: 'opr',
        itemtype: 'customer',
        visible: true,
        width: '150px',
        render: (text, record, index) =>{
            return props.button.createOprationButton(['DelLine'], {
                area: presetVar.listButtonArea,
                buttonLimit: 3,
                onButtonClick: (props, btnKey) => {
                    listButtonClick.call(this, props, btnKey, record, index);
                }
            });
        }
    };
    meta[presetVar.list].items.push(event);
}
let removeOpr = function(props){
    if(props.meta.getMeta()[presetVar.list].items[props.meta.getMeta()[presetVar.list].items.length-1].attrcode == 'opr'){
        props.meta.getMeta()[presetVar.list].items.pop();
    }
}

/**
 * 取得业务日期字符串（2001-01-01）
 */
let getSysDateStr = function(){ 
    // return '2018-07-26';
    let businessInfo = getBusinessInfo();
        let buziDate = businessInfo.businessDate;
        if(buziDate){
            return buziDate.split(' ')[0];
        }
    return undefined;
}
/**
 * 获取如下格式日期：（2001-01-01 hh:mm:ss）
 */
let getVersionDateStr = function(){ 
    // return '2018-07-26';
    let businessInfo = getBusinessInfo();
        let buziDate = businessInfo.businessDate;
        if(buziDate){
            return buziDate;
        }
    return undefined;
}
export default {pageStatusChange, addOpr, removeOpr, getSysDateStr , getVersionDateStr}
