import {ajax,getBusinessInfo,cardCache} from 'nc-lightapp-front';
import AssidModalRedner from '../../../../public/components/assidModalRedner'
import requestApi from '../requestApi'
import presetVar from '../presetVar'
import {listButtonClick, query, pubUtil} from './index'
import {commonApi} from '../../../../public/common/actions';
let {setDefData, getDefData } = cardCache;

let pk_accountingbook = '';
let getPretentAssData = function(props, key, index){
    let ref = {};
    if(pk_accountingbook){
        ajax({
            url:'/nccloud/gl/voucher/queryBookCombineInfo.do',
            data:{pk_accountingbook:pk_accountingbook},
            async:false, 
            success:(res) => {
                if((res.data || {}).unit){
                    ref.pk_org = res.data.unit.value;
                }
            }
        });
    }
    ref.pk_accountingbook=pk_accountingbook;
    if(key == 'assprofit'){
        ref.assid = props.form.getFormItemsValue(presetVar.formId, 'assprofit').value;
        ref.pk_accasoa= props.form.getFormItemsValue(presetVar.formId, 'pk_accsubjprofit').value;
    }else if(key == 'assloss'){
        ref.assid = props.form.getFormItemsValue(presetVar.formId, 'assloss').value;
        ref.pk_accasoa= props.form.getFormItemsValue(presetVar.formId, 'pk_accsubjloss').value;
    }else if(key == 'asspl'){
        ref.assid = props.form.getFormItemsValue(presetVar.formId, 'asspl').value;
        ref.pk_accasoa= props.form.getFormItemsValue(presetVar.formId, 'pk_accsubjpl').value;
    }else if(key == 'inass'){
        ref.assid= props.cardTable.getValByKeyAndIndex(presetVar.list, index, 'inass').value;
        ref.pk_accasoa= props.cardTable.getValByKeyAndIndex(presetVar.list, index, 'pk_accsubj').value;
    }else if(key == 'ass'){
        ref.assid= props.cardTable.getValByKeyAndIndex(presetVar.list, index, 'ass').value;
        ref.pk_accasoa= props.cardTable.getValByKeyAndIndex(presetVar.list, index, 'pk_accsubj').value;
    }
    return ref;
}

/**
 * 参照过滤设置
 * @param {*} meta 
 */
let initRefParam = function(meta){
    pk_accountingbook = this.pageParam.headRef.refpk;
    meta[presetVar.childFormId1].items.map((one)=>{
        if(one.attrcode =="pk_vouchertype"){
            let bizeInfo = getBusinessInfo();
            let pk_group = bizeInfo.groupId;
            let condition = {
                pk_group : pk_group,    
                pk_org : pk_accountingbook,
                isDataPowerEnable : 'Y',
                DataPowerOperationCode : 'fi'
            }
            one.queryCondition = () => {
				return condition;
			}
        }
    })
    meta[presetVar.childFormId2].items.map((one)=>{
        if(one.attrcode == presetVar.formField.pk_accsubjpl 
            || one.attrcode == presetVar.formField.pk_accsubjprofit 
            || one.attrcode == presetVar.formField.pk_accsubjloss){

                one.isAccountRefer = true;
                one.onlyLeafCanSelect=true;
                one.queryCondition = () => {
                return {
                    "pk_accountingbook": this.pageParam.headRef.refpk,
                    "versiondate": pubUtil.getVersionDateStr(),
                    "dateStr": pubUtil.getSysDateStr(),
                    "isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi'
                };
            }
        }
    })
    meta[presetVar.list].items.map((one)=>{
        if(one.attrcode == presetVar.listField.pk_accsubj){
            one.isAccountRefer = true;
            one.queryCondition = () => {
                return {
                    "pk_accountingbook": this.pageParam.headRef.refpk,
                    "versiondate": pubUtil.getVersionDateStr(),
                    "dateStr": pubUtil.getSysDateStr(),
                    "isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi'
                };
            }
            one.isMultiSelectedEnabled=!0;
        }
    })
}

let getassName = (data) => {
    let refStr = '';
    data && data.data && data.data.map((one)=>{
        if(one){
            let retStrOne = '[';
            retStrOne = retStrOne + one.checktypename;
            if(one.checkvaluecode){
                retStrOne = retStrOne + '=' + one.checkvaluecode;
            }
            retStrOne = retStrOne + ']';
            refStr = refStr + retStrOne;
        }
    })
    return refStr;
}

export default function (props) {

    props.createUIDom(
        {
            // pagecode: props.getUrlParam('pagecode'),
            // appcode: props.getUrlParam('appcode')
        },
        (data) => {
            let meta = data.template;
            window.setTimeout(() => {
                initRefParam.call(this, meta);
                // TODO：模板错误、重写模板
                meta[presetVar.childFormId2].items.map((one, index)=>{
                    if(one.attrcode == presetVar.formField.asspl
                        || one.attrcode == presetVar.formField.assloss
                        || one.attrcode == presetVar.formField.assprofit){
                        one = {
                            attrcode: one.attrcode,
                            label: one.label,
                            col: one.col,
                            itemtype: 'refer',
                            visible: one.visible,
                        }
                        meta[presetVar.childFormId2].items[index] = one;
                    }
                })
                // 转入辅助核算、转出辅助核算 重写render
                meta[presetVar.list].items.map((one, index)=>{
                    if(one.attrcode == presetVar.listField.inass || one.attrcode == presetVar.listField.ass){
                        one = {
                            label: one.label                        ,
                            attrcode: one.attrcode,
                            itemtype: 'refer',
                            visible: one.visible || false,
                            width: one.width || 150,
                            render: (text, record, index) =>{
                                return (
                                    <AssidModalRedner
                                        subjrelationset='subjrelationset'
                                        changeDisplay={getassName}
                                        getPretentAssData={()=>{return getPretentAssData(props, one.attrcode, index)}}
                                        doConfirm={(assid, data, display)=>{
                                            window.setTimeout(()=>{
                                                props.cardTable.setValByKeyAndIndex(presetVar.list, index, one.attrcode, {value: assid, display: display, isEdit: true});
                                                if(this.props.cardTable.setRowStatusByIndexs(presetVar.list, index) == '0'){
                                                    this.props.cardTable.setRowStatusByIndexs(presetVar.list, index, '1');
                                                }
                                            },10)
                                        }}
                                    />

                                );
                            }
                        }
                        meta[presetVar.list].items[index] = one;
                    }else if(one.attrcode == 'pk_currtype'){
                        one.isMultiSelectedEnabled = true;
                    }
                })
                props.meta.setMeta(meta);
                // 损益科目辅助核算重写render
                props.renderItem(
                    'form',
                    presetVar.childFormId2,
                    presetVar.formField.asspl,
                    <AssidModalRedner
                        subjrelationset='subjrelationset'
                        position="form" 
                        changeDisplay={getassName}
                        getPretentAssData={()=>{return getPretentAssData(props, presetVar.formField.asspl,'')}}
                        doConfirm={(assid, data, display)=>{
                            props.form.setFormItemsValue(presetVar.formId, {asspl:{value: assid, display: display}});
                        }}
                    />
                );
                // 收益科目辅助核算重写render
                props.renderItem(
                    'form',
                    presetVar.childFormId2,
                    presetVar.formField.assprofit,
                    <AssidModalRedner
                        subjrelationset='subjrelationset'
                        position="form"
                        changeDisplay={getassName}
                        getPretentAssData={()=>{return getPretentAssData(props, presetVar.formField.assprofit,'')}}
                        doConfirm={(assid, data, display)=>{
                            props.form.setFormItemsValue(presetVar.formId, {assprofit:{value: assid, display: display}});
                        }}
                    />
                );
                // 损失科目辅助核算重写render
                props.renderItem(
                    'form',
                    presetVar.childFormId2,
                    presetVar.formField.assloss,
                    <AssidModalRedner
                        subjrelationset='subjrelationset'
                        position="form"
                        changeDisplay={getassName}
                        getPretentAssData={()=>{return getPretentAssData(props, presetVar.formField.assloss,'')}}
                        doConfirm={(assid, data, display)=>{
                            props.form.setFormItemsValue(presetVar.formId, {assloss:{value: assid, display: display}});
                        }}
                    />
                );

                props.button.setButtons(data.button);
                let pageStatus = props.getUrlParam('status');
                pubUtil.pageStatusChange.call(this, props, pageStatus);
                if(pageStatus == 'browse'){
                    query.queryData(props, this.props.getUrlParam('id'));
                    // props.cardTable.setStatus(presetVar.list, 'browse');
                    // props.setUrlParam({status:'browse'})
                }else if(pageStatus == 'edit'){
                    query.queryData(props, this.props.getUrlParam('id'));
                    props.button.setButtonsVisible({'Add': false, 'Save': true, 'Cancel':true, 'Refresh':false, 'Copyto':false});
                }else if(pageStatus == 'add'){
                    props.form.setFormItemsValue(presetVar.formId,{'currtpropertyname':{value:'2',display:this.state.json['20020EXCAD-000024']}});/* 国际化处理： 对应币种*/
                    setTimeout(() => {
                        props.cardTable.addRow(presetVar.list);
                    }, 10);
                }
                props.form.setFormItemsDisabled(presetVar.formId,{'creator':true,'creationtime':true,'modifier':true,'modifiedtime':true});
                props.button.setButtonDisabled({ DelLine: true});
            }, 10);
            props.button.setButtonDisabled({ DelLine: true});
            
        }
    )
}
