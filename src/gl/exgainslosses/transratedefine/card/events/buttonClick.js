import {base,promptBox,toast,cardCache} from 'nc-lightapp-front';
const {NCMessage} = base;
import commonPubUtil from '../../../../public/common/pubUtil'
import requestApi from '../requestApi'
import presetVar from '../presetVar'
import listPresetVar from '../../list/presetVar'
import {listButtonClick, pubUtil} from './index'
import query from './query';
let { getCacheById, updateCache,addCache,getCurrentLastId,getNextId,deleteCacheById} = cardCache;

export default function buttonClick(props, actionId) {
    switch(actionId){
        case 'Add':
            props.setUrlParam({status:'add'})
            props.cardTable.setTableData(presetVar.list, {rows:[]});
            props.form.EmptyAllFormValue(presetVar.formId);
            let fromValue = props.form.getAllFormValue(presetVar.formId);
            if(fromValue.rows != null && fromValue.rows.length>0){
                for(let name in fromValue.rows[0].values){
                    fromValue.rows[0].values[name] = {};
                }
            }
            props.form.setAllFormValue({[presetVar.formId]: fromValue});
            props.cardTable.setStatus(presetVar.list, 'edit');
            props.form.setFormStatus(presetVar.formId, 'add');
            pubUtil.pageStatusChange.call(this, props, 'add');
            break;
        case 'Edit':
            props.setUrlParam({status:'edit'})
            props.cardTable.setStatus(presetVar.list, 'edit');
            props.form.setFormStatus(presetVar.formId, 'edit');
            pubUtil.pageStatusChange.call(this, props, 'edit');
            props.button.setButtonsVisible({'Add': false, 'Save': true, 'Cancel':true, 'Refresh':false, 'Copyto':false});
            break;
        case 'Cancel':
        promptBox({
            color:"warning",
            title: this.state.json['20020EXCAD-000000'],/* 国际化处理： 取消？*/
            content: this.state.json['20020EXCAD-000001'],/* 国际化处理： 确定要取消吗*/
            beSureBtnClick: cancelAction.bind(this,props)
            });
        break;
        case 'Save':
            props.cardTable.filterEmptyRows(presetVar.list); 
            let flag1 = props.form.isCheckNow(presetVar.formId);
            let flag2 = props.cardTable.getAllRows(presetVar.list);
            let checkData=[];
            if(flag2.length>0){
                for(let j=0;j<flag2.length;j++){
                    checkData.push(flag2[''+j]);
                }
                let check=props.cardTable.checkTableRequired(presetVar.list);
                if(check){
                    if(flag1 && flag2){
                        let data = {head:{}, body:{}, pageid: props.getUrlParam('pagecode')};
                        let list= props.cardTable.getAllData(presetVar.list);
                        if(list.rows && list.rows.length>0){
                            for(let x=0;x<list.rows.length;x++){
                                list.rows[x].values.inass.value= list.rows[x].values.inass.display;
                                list.rows[x].values.ass.value= list.rows[x].values.ass.display;
                            }
                        }
                        data.body[presetVar.list] = list;
                        let form=props.form.getAllFormValue(presetVar.formId);
                        form.rows[0].values.assprofit.value=form.rows[0].values.assprofit.display;
                        form.rows[0].values.assloss.value=form.rows[0].values.assloss.display;
                        form.rows[0].values.asspl.value=form.rows[0].values.asspl.display;
                        data.head[presetVar.formId] = form;
                        data.head[presetVar.formId].rows[0].values.pk_org = {value:this.pageParam["headRef"].refpk};
                        data.head[presetVar.formId].rows[0].values.pk_accountingbook = {value:this.pageParam["headRef"].refpk};
                        requestApi.save({
                            props:props,
                            data: data,
                            success: (data) => {
                                props.form.setAllFormValue({[presetVar.formId]: data.data.head[presetVar.formId] || {rows:[{values:{}}]}});
                                if(data.data.body != null){
                                    props.cardTable.setTableData(presetVar.list, data.data.body[presetVar.list] || {rows:[]});
                                }else{
                                    props.cardTable.setTableData(presetVar.list, {rows:[]});
                                }
                                let id=data.data.head[presetVar.formId].rows[0].values.pk_transrate.value;
                                if (props.getUrlParam('status') === 'edit') {
                                    updateCache(listPresetVar.pkname,id,data.data,presetVar.formId,listPresetVar.dataSource);
                                }else{
                                    addCache(id,data.data,presetVar.formId,listPresetVar.dataSource);
                                }
                                pubUtil.pageStatusChange.call(this, props, 'browse');
                                let multiLang = props.MutiInit.getIntl(2002);
                                toast({color: "success",content:this.state.json['20020EXCAD-000044']});
                                // NCMessage.create({content: multiLang && multiLang.get('2002-0001'), color: 'success', position: 'bottomRight'});
                                props.button.setButtonsVisible({'Add': true,'Edit': true, 'Delete': true, 'Save': false, 'Cancel':false, 'Refresh':false, 'Copyto':true});
                                props.setUrlParam({status:'browse',id:id});
                                this.forceUpdate();
                            }
                        })
                    }
                }
            }else{
                toast({color:"danger",content:this.state.json['20020EXCAD-000002']});/* 国际化处理： 分录不能为空！*/
            }
        break;
        case 'Delete':
        promptBox({
            color:"warning",
            title: this.state.json['20020EXCAD-000003'],/* 国际化处理： 删除*/
            content: this.state.json['20020EXCAD-000004'],/* 国际化处理： 确定要删除吗*/
            beSureBtnClick: deleteAction.bind(this,props)
            });
        break;
        case 'AddLine':
            props.cardTable.addRow(presetVar.list);
        break;
        case 'DelLine':
            let data = props.cardTable.getCheckedRows(presetVar.list);
            if(data.length>0){
                let index=[];
                for(let i=0;i<data.length;i++){
                    index.push(data[i].index);
                }
                props.cardTable.delRowsByIndex(presetVar.list, index);
                this.props.button.setButtonDisabled({
                    DelLine: true
                });
            }
        break;
        case 'Return':
            commonPubUtil.pushTo(props, presetVar.listURL,{
                appcode: props.getUrlParam('appcode'),
                pagecode: presetVar.listPageCode
            });
        break;
        case 'Copyto':
            this.setState({showFormModal:true});
            this.setState({checkStatus:'Y'});
            this.setState({pk: props.form.getAllFormValue('head').rows[0].values.pk_transrate.value});
        break;
        case 'Refresh':
            query.queryData(props, this.props.getUrlParam('id'));
            toast({color:"success",title:this.state.json['20020EXCAD-000045']})/* 国际化处理： 刷新成功*/
        break;
    }
}

function deleteAction(props,page){
    let id=props.getUrlParam('id');
    requestApi.delete({
        data: {pk_transrate: id},
        success: (data) => {
            let nextId = getNextId(id, listPresetVar.dataSource);
            deleteCacheById(listPresetVar.pkname,id,listPresetVar.dataSource);
            if(nextId){
                let nextCache = getCacheById(nextId, listPresetVar.dataSource);
                if(nextCache){
                    if(nextCache != null && nextCache.data != null){
                        props.form.setAllFormValue({[presetVar.formId]: nextCache.data.head[presetVar.formId] || {rows:[]}});
                        if(nextCache.data.body != null){
                            props.cardTable.setTableData(presetVar.list, nextCache.data.body[presetVar.list] || {rows:[]});
                        }else{
                            props.cardTable.setTableData(presetVar.list, {rows:[]});
                        }
                    }else{
                        props.form.EmptyAllFormValue(presetVar.formId);
                    }
                    this.props.setUrlParam({id:nextId});
                }else{
                    query.queryData(props, nextId);
                    this.props.setUrlParam({id:nextId});
                }
            }else{
                props.form.EmptyAllFormValue(presetVar.formId); 
                props.cardTable.setTableData(presetVar.list, {rows:[]});
            }
            pubUtil.pageStatusChange.call(this, props, 'browse');
            toast({color: "success",content:this.state.json['20020EXCAD-000046']});
        }
    })
}


function cancelAction(props,page){
    let status=props.getUrlParam('status');
    if(status=='edit'){
        let currId=props.getUrlParam('id');
        if(currId){
            let cardData = getCacheById(currId, listPresetVar.dataSource);
            props.setUrlParam({id:currId});
            if(cardData){
                loadData(props, cardData);
            }else{
                query.queryData(props, currId);
            }
        }else{
            props.form.EmptyAllFormValue(presetVar.formId); 
            props.cardTable.setTableData(presetVar.list, {rows:[]});
        }
    }else{
        let id = getCurrentLastId(listPresetVar.dataSource);
        if(id){
            let cardData = getCacheById(id, listPresetVar.dataSource);
            props.setUrlParam({id:id});
            if(cardData){
                loadData(props, cardData);
            }else{
                query.queryData(props, id);
            }
        }else{
            props.form.EmptyAllFormValue(presetVar.formId); 
            this.props.cardTable.setTableData(presetVar.list, {rows:[]});
        }
    }
    pubUtil.pageStatusChange.call(this, props, 'browse');
    props.setUrlParam({status:'browse'})
}

function loadData(props, data){
    if(data&&data.head){
        props.form.setAllFormValue({[presetVar.formId]:data.head[presetVar.formId] || {rows:[]}});
        if(data.body){
            props.cardTable.setTableData(presetVar.list, data.body[presetVar.list] || {rows:[]});
        }else{
            props.cardTable.setTableData(presetVar.list, {rows:[]});
        }
    }else{
        props.form.EmptyAllFormValue(presetVar.formId);
        props.cardTable.setTableData(presetVar.list, {rows:[]});
    }
}
