import {cardCache,toast} from "nc-lightapp-front";
import requestApi from '../requestApi'
import presetVar from '../presetVar'
let {setDefData, getDefData } = cardCache;

let queryData = function(props, queryKey){
    requestApi.query({
        props: props,
        data: {pk_transrate: queryKey},
        success: (data) => {
            if(data != null && data.data != null){
                props.form.setAllFormValue({[presetVar.formId]: data.data.head[presetVar.formId] || {rows:[]}});
                if(data.data.body != null){
                    props.cardTable.setTableData(presetVar.list, data.data.body[presetVar.list] || {rows:[]});
                }else{
                    props.cardTable.setTableData(presetVar.list, {rows:[]});
                }
            }else{
                props.form.EmptyAllFormValue(presetVar.formId);
                props.cardTable.setTableData(presetVar.list, {rows:[]});
            }
        }
    })
}
let doCopyTo = function (props, refmap, uncheckrepeatmap) {
    requestApi.copyTo({

        data: {
            pagecode: presetVar.cardPageCode,
            pageId: presetVar.pageId,
            pk_transrate: this.props.getUrlParam('id'),
            check: this.state.checkStatus,
            pk_accountingbookmap: refmap,
            ignore: uncheckrepeatmap
        },
        success: (data) => {
            //无覆盖数据或者二次处理
            if (data.data == undefined || data.data.length == 0) {
                toast({color:"success",content:"复制成功"})
                this.setState({showFormModal:false});
                this.setState({showDealRepeatModal:false});
                this.setState({pk_accountingbookmap:refmap});//清空全部数据
                return;
            }
            var mapAll = {};
            for (var i=0;i<data.data.length;i++) {
                var map = {};
                map["pk"] = data.data[i].pk;
                map["name"] = data.data[i].name;
                mapAll[data.data[i].pk] = map;
                // this.setState({tips_pk: data.data[i].pk});
                // this.setState({tips_name: data.data[i].name});
            }
            this.setState({repeatmap: mapAll});
            this.setState({showDealRepeatModal:true});
            // props.table.deleteTableRowsByIndex(presetVar.list, index);
        }
    })
}

export default {queryData, doCopyTo}
