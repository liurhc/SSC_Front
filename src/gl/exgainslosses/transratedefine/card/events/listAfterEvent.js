import {base,deepClone} from 'nc-lightapp-front';
const {NCMessage} = base;
import {fixedTemplet} from './index'
import requestApi from '../requestApi'
import presetVar from '../presetVar'
export default function listAfterEvent(props, moduleId, key, value, changedrows, index, record) {
    if(key == 'pk_accsubj'){
        if(value.length>0){
            let indexL = index;
            let copyData = record;
            // let copyData = this.props.cardTable.getClickRowIndex(presetVar.list);
           
            if(copyData){
                // 清空主键
                let oldPk_transratedetail=copyData.values.pk_transratedetail;
                // copyData.values.pk_transratedetail = {value:''};
                let datas=[];
                let codes=[];
                if(value && value.length>0){
                    for(let i=0;i<value.length;i++){
                        let ref=value[i];
                        let code=ref.code;
                        if(value.refcode){
                            code=value.refcode;
                        }
                        codes.push(code);
                    }
                }
                let data={
                    "pk_accasoa":codes,//科目编码
                    "pk_accountingbook":this.pageParam.headRef.refpk,//核算账簿
                    "flag":"org"
                }
                requestApi.subRelationDataQuery({
                    props: this.props,
                    data: data,
                    async:false, 
                    success: (res) => {
                        if(res.data){
                            if(res.data.length>0){
                                for(let j =0;j<res.data.length;j++){
                                    let accasoaVo=res.data[j];
                                    let clone = deepClone(record.values);
                                    clone.pk_accsubj={value:accasoaVo.pk_accasoa,display:accasoaVo.code};
                                    if(j==0){
                                        props.cardTable.setValByKeyAndIndex(moduleId,index,'pk_accsubj',{value:clone.pk_accsubj.value,display:clone.pk_accsubj.display});
                                        // props.cardTable.setValByKeyAndIndex(moduleId,index,'pk_transratedetail',{value:oldPk_transratedetail.value,display:oldPk_transratedetail.display});                                    
                                    }else{
                                        clone.pk_transratedetail = {value:'',diaplay:''};
                                        clone.ass = {value:'',diaplay:''};
                                        clone.inass = {value:'',diaplay:''};
                                        datas.push({values:clone});
                                    }
                                }
                                
                            }
                        }
                    }
                })
                // props.cardTable.delRowsByIndex(moduleId, index, false);
                if(datas.length>0){
                    props.cardTable.insertRowsAfterIndex(moduleId,datas,index);
                }
            }
        }else{
            props.cardTable.setValByKeyAndIndex(moduleId,index,"inass",{display:'',value:''});
            props.cardTable.setValByKeyAndIndex(moduleId,index,"ass",{display:'',value:''});
        }
    }
    if(key == 'pk_currtype'){
        if(value.length>1){
            let datas=[];
            for(let i=0;i<value.length;i++){
                let clone = deepClone(record.values);
                clone.pk_transratedetail={value:''};
                clone.pk_currtype={value:value[i].refpk,display:value[i].refcode};
                datas.push({values:clone});
            }
            props.cardTable.delRowsByIndex(moduleId, index, false);
            props.cardTable.insertRowsAfterIndex(moduleId,datas,index);
        }
    }
}
