import requestApi from '../requestApi'
import presetVar from '../presetVar'
export default function listButtonClick(props, actionId, data, index) {
    switch(actionId){
        case 'DelLine':
            props.cardTable.delRowsByIndex(presetVar.list, index);
            break;
    }
}
