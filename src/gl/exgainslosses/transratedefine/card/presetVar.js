/**页面全局变量 */
let currentVar = {
    pageId: '0001Z310000000008JWL',
    list: 'ratedetails',
    formId: 'vocherdef',
    childFormId1: 'vocherdef',
    childFormId2: 'inbook',
    headButtonArea: 'card_head',
    listButtonArea: 'card_body_inner',
    listShoulderButtonArea: 'card_body',
    // listURL: '/gl/exgainslosses/transratedefine/list/index.html',
    listURL: '/list',
    listPageCode: '20020EXCAD_LIST',
    /**
     * 表单项目
     */
    formField:{
        /**
         * 损益科目
         */
        pk_accsubjpl: 'pk_accsubjpl',
        /**
         * 收益科目
         */
        pk_accsubjprofit: 'pk_accsubjprofit',
        /**
         * 损失科目
         */
        pk_accsubjloss: 'pk_accsubjloss',
        /**
         * 损益科目辅助核算
         */
        asspl: 'asspl',
        /**
         * 收益科目辅助核算
         */
        assprofit: 'assprofit',
        /**
         * 损失科目辅助核算
         */
        assloss: 'assloss'
    },
    /**
     * 列表项目
     */
    listField:{
        /**
         * 会计科目
         */
        pk_accsubj: 'pk_accsubj',
        /**
         * 转入辅助核算
         */
        inass: 'inass',
        /**
         * 转出辅助核算
         */
        ass: 'ass',
    }
    
}
window.presetVar = {
    ...currentVar
};
export default currentVar
