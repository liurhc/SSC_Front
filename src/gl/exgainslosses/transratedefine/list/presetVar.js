/**页面全局变量 */
let currentVar = {
    pageId: 'yxysdy',
    list: 'vocherdef',
    headButtonArea: 'list_head',
    listButtonArea: 'list_inner',
    cardAppCode: '',
    cardPageCode: '20020EXCAD_CARD',
    // cardURL: '/gl/exgainslosses/transratedefine/card/index.html',
    cardURL: '/card',
    dataSource: 'gl.exgainslosses.transratedefine',
    pkname:'pk_transrate'
}
window.presetVar = {
    ...currentVar
};
export default currentVar
