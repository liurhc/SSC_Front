import {base, cardCache,toast} from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;

const {NCMessage} = base;
import pubUtil from '../../../../public/common/pubUtil'
import requestApi from '../requestApi'
import presetVar from '../presetVar'
import {query} from './index'
export default function buttonClick(props, actionId) {
    switch(actionId){
        case 'Add':
            if(this.state.headRef != null && this.state.headRef.refpk != null){
                setDefData('pageParam', presetVar.dataSource,{'headRef':this.state.headRef});
                pubUtil.pushTo(props, presetVar.cardURL,{
                    appcode: props.getSearchParam('c'),
                    pagecode: presetVar.cardPageCode,
                    status: 'add',
                    open_status: 'add'
                });            
            }else{
                let multiLang = this.props.MutiInit.getIntl(2002); 
                // 2002-0005: 请先选择
                toast({color:"warning",content:this.state.json['20020EXCAD-000043']})/* ?????? ????*/
                // NCMessage.create({content: this.state.json['20020EXCAD-000043'], color: 'warning', position: 'bottomRight'});
            }
        break;
        case 'Refresh':
        if(this.state.headRef != null && this.state.headRef.refpk != null){
            query.queryData.call(this, this.props, this.state.headRef);
            toast({color:"success",title:this.state.json['20020EXCAD-000045']})/* ?????? ????*/
        }
        break;
    }
}
