import {cardCache,toast} from 'nc-lightapp-front';
import requestApi from '../requestApi'
import presetVar from '../presetVar'
let {setDefData, getDefData } = cardCache;

let queryData = function(props, data){
    let { hasCacheData } = props.table;
	// if(!hasCacheData(presetVar.dataSource)){
        let cfQueryKey = getDefData('cfQueryKey', presetVar.dataSource);
        if(cfQueryKey != null && cfQueryKey.refpk != null){
            this.setState({ headRef:cfQueryKey});
            requestApi.query({
                data: {pk_accountingbook:cfQueryKey.refpk},
                success: (data) => {
                    this.pkList = [];
                    if(data.data != null && data.data[presetVar.list] != null){
                        data.data[presetVar.list].rows.map((one)=>{
                            if((((one || {}).values || {}).pk_transrate || {}).value != null){
                                this.pkList.push(one.values.pk_transrate.value);
                            }
                        })
                        data.data[presetVar.list].allpks=this.pkList;
                        props.table.setAllTableData(presetVar.list, data.data[presetVar.list]);
                        setDefData(presetVar.list, presetVar.dataSource, data.data);
                    }else{
                        props.table.setAllTableData(presetVar.list, {rows:[]});
                    }
                }
            })
        }else{
            if(data != null && data.refpk != null){
                requestApi.query({
                    data: {pk_accountingbook:data.refpk},
                    success: (data) => {
                        this.pkList = [];
                        if(data.data != null && data.data[presetVar.list] != null){
                            data.data[presetVar.list].rows.map((one)=>{
                                if((((one || {}).values || {}).pk_transrate || {}).value != null){
                                    this.pkList.push(one.values.pk_transrate.value);
                                }
                            })
                            data.data[presetVar.list].allpks=this.pkList;
                            props.table.setAllTableData(presetVar.list, data.data[presetVar.list]);
                            setDefData(presetVar.list, presetVar.dataSource, data.data);
                        }else{
                            props.table.setAllTableData(presetVar.list, {rows:[]});
                        }
                    }
                })
            }else{
                props.table.setAllTableData(presetVar.list, {rows:[]});
            }
        }
    // }
}

let doCopyTo = function (props, refmap, uncheckrepeatmap) {
    requestApi.copyTo({

        data: {
            pagecode: presetVar.cardPageCode,
            pageId: presetVar.pageId,
            pk_transrate: this.state.pk,
            check: this.state.checkStatus,
            pk_accountingbookmap: refmap,
            ignore: uncheckrepeatmap
        },
        success: (data) => {
            //无覆盖数据或者二次处理
            if (data.data == undefined || data.data.length == 0) {
                toast({color:"success",content:"复制成功"})
                this.setState({showFormModal:false});
                this.setState({showDealRepeatModal:false});
                this.setState({pk_accountingbookmap:refmap});//清空全部数据
                return;
            }
            var mapAll = {};
            for (var i=0;i<data.data.length;i++) {
                var map = {};
                map["pk"] = data.data[i].pk;
                map["name"] = data.data[i].name;
                mapAll[data.data[i].pk] = map;
                // this.setState({tips_pk: data.data[i].pk});
                // this.setState({tips_name: data.data[i].name});
            }
            this.setState({repeatmap: mapAll});
            this.setState({showDealRepeatModal:true});
            // props.table.deleteTableRowsByIndex(presetVar.list, index);
        }
    })
}

export default {queryData, doCopyTo}
