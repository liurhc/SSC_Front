
import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import listButtonClick from './listButtonClick'
import query from './query';

export { initTemplate, buttonClick, listButtonClick, query};
