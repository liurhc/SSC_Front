import {base, cardCache,toast} from 'nc-lightapp-front';
let {setDefData, getDefData } = cardCache;
const {NCMessage} = base;
import pubUtil from '../../../../public/common/pubUtil'
import requestApi from '../requestApi'
import presetVar from '../presetVar'
export default function listButtonClick(props, actionId, data, index) {
    let multiLang = props.MutiInit.getIntl(2002);
    switch(actionId){
        case 'DoubleClick':
            setDefData('pageParam', presetVar.dataSource,{'headRef':this.state.headRef});
            pubUtil.pushTo(props, presetVar.cardURL,{
                appcode: props.getSearchParam('c'),
                pagecode: presetVar.cardPageCode,
                status: 'browse',
                open_status: 'browse',
                id: data.pk_transrate.value
            });
        break;
        case 'Edit':
            setDefData('pageParam', presetVar.dataSource,{'headRef':this.state.headRef});
            pubUtil.pushTo(props, presetVar.cardURL,{
                appcode: props.getSearchParam('c'),
                pagecode: presetVar.cardPageCode,
                status: 'edit',
                open_status: 'edit',
                id: data.pk_transrate.value
            });
        break;
        case 'Delete':
            requestApi.delete({
                data: {pk_transrate: data.pk_transrate.value},
                success: (data) => {
                    props.table.deleteTableRowsByIndex(presetVar.list, index);
                    toast({color: "success",content:multiLang && multiLang.get('2002-0006')});
                    // NCMessage.create({content: multiLang && multiLang.get('2002-0006'), color: 'success', position: 'bottomRight'});
                }
            })
        break;
        case 'Copyto':
            this.setState({showFormModal:true});
            this.setState({checkStatus:'Y'});
            this.setState({pk: data.pk_transrate.value});
        break;
    }
}
