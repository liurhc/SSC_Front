import { ajax, cardCache, base } from 'nc-lightapp-front';
let { setDefData, getDefData } = cardCache;
import requestApi from '../requestApi'
import presetVar from '../presetVar'
import { fixedTemplet, listButtonClick, query } from './index'
import pubUtil from '../../../../public/common/pubUtil'

export default function (props) {
    props.createUIDom(
        {
            pagecode: props.getUrlParam('pagecode'),
            appcode: props.getUrlParam('appcode')
        },
        (data) => {
            let meta = data.template;
            // window.setTimeout(() => {
            let event = {
                label: this.state.json['20020EXCAD-000035'],/* 国际化处理： 操作*/
                attrcode: 'opr',
                width: '200px',
                fixed: 'right',
                itemtype: 'customer', 
                visible: true,
                // className: 'table-opr',
                render: (text, record, index) => {
                    return <div onClick={(e)=>{ e.stopPropagation()}}>
                        {
                            props.button.createOprationButton(['Edit', 'Delete', 'Copyto'], {
                                area: presetVar.listButtonArea,
                                onButtonClick: (props, btnKey,e) => {
                                    listButtonClick.call(this, props, btnKey, record, index);
                                }
                            })
                        }
                    </div>
                }
            };
            meta[presetVar.list].items.push(event);

            props.button.setPopContent('Delete', this.state.json['20020EXCAD-000041']);/* 国际化处理： 是否确定删除*/
            props.button.setButtons.bind(this)((data.button), () => {
                props.button.setButtonsVisible({ 'Add': true, 'Save': false, 'Cancel': false, 'Refresh': true });

            });
            meta[presetVar.list].items.map((item, key) => {
                //单据号添加下划线超链接
                if (item.attrcode == 'transferno') {
                    item.render = (text, record, index) => {
                        return (
                            <span
                                style={{  cursor: 'pointer',color: '#007ace'}}
                                onClick={() => {
                                    setDefData('pageParam', presetVar.dataSource,{'headRef':this.state.headRef});
                                    pubUtil.pushTo(props, presetVar.cardURL,{
                                        appcode: props.getSearchParam('c'),
                                        pagecode: presetVar.cardPageCode,
                                        status: 'browse',
                                        open_status: 'browse',
                                        id: record.pk_transrate.value
                                    });
                                }}
                            >
                                {record.transferno && record.transferno.value}
                            </span>
                        );
                    };
                }
            })
            props.meta.setMeta(meta);
            props.button.setButtonDisabled(['Add'], true);
            let pageParam = getDefData('pageParam', presetVar.dataSource);
            if (pageParam != null && pageParam.headRef != null && pageParam.headRef.refpk != null) {
                this.setState({ headRef: pageParam.headRef });
                query.queryData.call(this, this.props, pageParam.headRef);
                props.button.setButtonDisabled(['Add'], false);
            }
            // }, 100);
            if (data.context.defaultAccbookPk) {
                let dataPk = {
                    "refname": data.context.defaultAccbookName,
                    "refpk": data.context.defaultAccbookPk
                }
                this.setState({ headRef: dataPk });
                query.queryData.call(this, this.props, dataPk);
                props.button.setButtonDisabled(['Add'], false);
            }

        }
    )
}
