import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/gl/transratedefine/listquery.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 删除
    delete: (opt) => {
        ajax({
            url: '/nccloud/gl/transratedefine/delete.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 复制到账簿
    copyTo: (opt) => {
        ajax({
            url: '/nccloud/gl/transratedefine/copyto.do',
            data: opt.data,
            success: opt.success
        });
    }
}

export default  requestApiOverwrite;
