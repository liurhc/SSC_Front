import {Component} from 'react';
import {createPage, base,getMultiLang,cardCache,createPageIcon} from 'nc-lightapp-front';
const {NCSelect, NCIcon} = base;

import ReferLoader from '../../../public/ReferLoader/index.js';

import {initTemplate, buttonClick, query, listButtonClick} from './events/index'
import Copyto from '../components/copyto'
import DealRepeat from '../components/dealRepeat'

import presetVar from './presetVar'
import requestApi from './requestApi'
let {setDefData, getDefData } = cardCache;

import './index.less';
import {ListLayout, Table, ModalArea} from '../../../../fipub/public/components/layout/ListLayout';
import {Header, HeaderSearchArea, HeaderButtonArea} from '../../../../fipub/public/components/layout/Header';



class TransratedefineList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            headRef: {},
            showFormModal: false,
            showDealRepeatModal:false,
            checkStatus: '',
            pk: '',
            repeatmap:'',
            pk_accountingbookmap:'',
            uncheckrepeatmap:{},
            json:{}
        };
        this.pkList = [];
       
        // 页面初始化
    }

    componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{
				initTemplate.call(this,this.props);
			})
		}
		getMultiLang({moduleId:'20020EXCAD',domainName:'gl',currentLocale:'zh-CN',callback});
	}
    searchBtnClick(data){
        query.queryData.call(this, this.props, data);
    }
    headRefChange(data,props){
        
        this.setState({headRef : data});
        setDefData('cfQueryKey', presetVar.dataSource,data);
        query.queryData.call(this, this.props, data);
        if(data != null && data.refpk!= null){
            this.props.button.setButtonDisabled(['Add'], false)
        }else{
            this.props.button.setButtonDisabled(['Add'], true)
        }
    }
    closeFormModal(){
        this.setState({showFormModal:false});
    }
    saveFormButtonClick(refmap) {
        this.setState({pk_accountingbookmap:refmap});//记录全部数据
        query.doCopyTo.call(this, this.props, refmap);
    }
    dealRepeat(refmap) {
        //解决state异步问题
        this.setState(({checkStatus:'N'}), () => {
            // var uncheckrepeatmap = this.getFinalDatas();
            var pk_accountingbookmap = this.state.pk_accountingbookmap;
            query.doCopyTo.call(this, this.props, pk_accountingbookmap, refmap);
        });
    }
    /**
     * 取原总记录扣掉第二次不覆盖的
     */
    getFinalDatas() {
        var repeatmap = this.state.repeatmap;
        var pk_accountingbookmap = this.state.pk_accountingbookmap;
        var uncheckrepeatmap = this.status.uncheckrepeatmap;
        for (var i in repeatmap) {
            if (pk_accountingbookmap[i]) {
                uncheckrepeatmap[i] = i;
            }
        }
        return uncheckrepeatmap;
    }

    closedealRepeatModel() {
        this.setState({showDealRepeatModal: false});
    }

    render() {
        let multiLang = this.props.MutiInit.getIntl(2002); 
        const {table, search, button} = this.props;
        const {createSimpleTable} = table;
        const {createButtonApp, createOprationButton} = button;
        const {NCCreateSearch} = search;

        return (
            <ListLayout>
                <Header>
                    <HeaderSearchArea>
                        <ReferLoader
                            fieldid='accountbook_ref'
                            tag={'test'}
                            refcode={'uapbd/refer/org/AccountBookTreeRef/index.js'}
                            disabledDataShow={true}
                            queryCondition={()=>{
                                return {
                                    "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    "appcode": this.props.getSearchParam('c')
                                }
                            }}
                            value={this.state.headRef}
                            onChange={this.headRefChange.bind(this)}
                            isMultiSelectedEnabled={false}
                            showGroup={false}
                            showInCludeChildren={false}
                        />
                    </HeaderSearchArea>
                    <HeaderButtonArea>
                        {createButtonApp({
                            area: presetVar.headButtonArea, 
                            buttonLimit: 3,
                            onButtonClick: buttonClick.bind(this)
                        })}
                    </HeaderButtonArea>
                </Header>
                <Table>
                    {createSimpleTable(presetVar.list, {
                        dataSource: presetVar.dataSource,
					    pkname: presetVar.pkname,
                        onRowDoubleClick: (record, index)=>{listButtonClick.call(this, this.props, 'DoubleClick', record, index)},
                        showCheck: false,
                        showIndex: true
                    })}
                </Table>
                <ModalArea>
                    <Copyto
                        showFormModal={this.state.showFormModal}
                        onClose={this.closeFormModal.bind(this)}
                        saveFormButtonClick={this.saveFormButtonClick.bind(this)}
                        appcode={this.props.getSearchParam('c')}
                    />
                    <DealRepeat 
                        showFormModal={this.state.showDealRepeatModal}
                        yes={this.dealRepeat.bind(this)}
                        no={this.dealRepeat.bind(this)}
                        repeatmap={this.state.repeatmap}
                        closedealRepeatModel={this.closedealRepeatModel.bind(this)}
                    />
                </ModalArea>
            </ListLayout>
        )
    }
}
TransratedefineList = createPage({mutiLangCode: '2002'})(TransratedefineList);
export default TransratedefineList;
