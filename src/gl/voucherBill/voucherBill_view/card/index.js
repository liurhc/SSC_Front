import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
import { high, base, ajax, print, createPage, deepClone, toast, output, promptBox, cardCache, cacheTools, sum, getMultiLang, createPageIcon,useJS,viewModel} from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
import { isObj, convertCurrency } from '../../../public/components/oftenApi';
const {
	NCFormControl: FormControl,
	NCRow: Row,
	NCCol: Col,
	NCMessage: Message,
	NCTable: Table,
	NCNumber,
	NCForm: Form,NCDiv
} = base;
const { NCFormItem: FormItem } = Form;
import { InputItem } from '../../../public/components/FormItems';
const { NCUploader,ApproveDetail } = high;
import './index.less';
import { onButtonClick, initTemplate, pageInfoClick } from './events';
import ApprovalTrans from '../../../../uap/public/excomponents/approvalTrans';
import setScale from '../../../public/common/amountFormat.js';
import HeaderArea from '../../../public/components/HeaderArea';
let { getCurrentLastId, getCacheById, getNextId, deleteCacheById, updateCache } = cardCache;
let dataSource = 'gl.voucherBill.voucherBill_list.cache';
const BZCX="bzcx";//报账平台标志
const ZYCX="xycx";//共享稽核
useJS.setConfig({
	'sscrp/rppub/components/image/index': "../../../../sscrp/rppub/components/image/index.js",
})
//  @withShell
let VoucherBrowseCard=useJS(["sscrp/rppub/components/image/index"],function(image){
return class VoucherBrowseCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {},
			saveNumber: '', //保存新增标识
			update: false, //修改太页面标识
			isLoading: true,
			getrow: '',
			open: false, //footer默认不展示
			title: '',
			index: '', //摘要信息下标
			accIndex: '', //辅助核算下标
			assList: [], //根据会计科目和制单日期获取的辅助核算数据
			MsgModalShow: false, //控制更多模态框显隐
			MsgModalAll: false, //控制常用模态框显隐
			SelectModalShow: false,
			SaveModalShow: false,
			SubjectModalShow: false, //科目模态框
			AssistAccModalShow: false,
			ExplanationModalShow: false,
			SaveErrorModalShow: false,
			ProcessModalShow: false, //流程图模态框
			historyProcess: '',
			showUploader: false,
			checkValue: null,
			checkId: null,
			novAuto: true, //凭证号是否自动获取的，true是自动，false为手动填的
			scene: '', //场景
			compositedata: null, //指派相关
			compositedisplay: null, //指派相关
			transtype: 'VRD1',
			cardPaginationShow: false,

			approveshow: false,
			showApproveDetail: false,//审批详情的控制

			id: '',

			isadd: false, //根据核算账簿动态增列判断是否已经新增

			//保存数据控制
			checkForm: false,
			//参照依赖核算账簿值获取
			pk_accountingbook: {
				//核算账簿
				display: '',
				value: ''
			},
			// prepareddate: {
			// 	value: ''
			// },
			billdate: {
				value: ''
			},
			nov: {
				//凭证号
				value: ''
			},
			startdate: {
				//制单日期
			},
			attachment: {
				value: '0'
			}, //附单据数,
			period: {
				//会计期间
			},
			billmaker: {
				//制单人
			},
			approver: {
				//审核人
			},

			saveData: {
				details: []
			},
			headContent: [],
			footContent: [],
			evidenceColumns: [],
			evidenceData: {
				rows: [
					
				],
				index: 1,
				localdebitamountTotle: {
					//借方合计
					value: ''
				},
				localcreditamountTotle: {
					//贷方合计
					value: ''
				},

				groupdebitamountTotle: {
					//组织借方合计
					value: ''
				},
				groupcreditamountTotle: {
					//组织贷方合计
					value: ''
				},

				globaldebitamountTotle: {
					//集团借方合计
					value: ''
				},
				globalcreditamountTotle: {
					//集团贷方合计
					value: ''
				},
				newLine: {
					isEdit: true,
					excrate1: {
						value: ''
					},
					childform: [
						{
							p_location: {
								//所在地
								value: '',
								display: '',
								scale: -1
							},
							maxpledge: {
								//可质押价值
								value: '',
								display: '',
								scale: -1
							}
						}
					]
				}
			},
			placeholder: '',
			disabled: false,
			copyData: {}, //复制的数据
			copyRecord: '', //复制的key
			voucherStatus: 'view', //凭证单状态 update：修改态，save：保存态，view:浏览态
			copyStatus: '', //复制的凭证copy
			abandonShow: 'N', //Y是作废凭证，N取消作废
			isSingleItem: 'N',
			detailIndex: '',
			billstatus: '', //单据状态，待提交态不显示工作流
			opinion: '',
			billno: '', //得到单据号
			bill_type: '' //单据类型VRD1
		};
		this.formId = 'head';
		this.formIdtail = 'tail';
		this.dataSource = 'gl.voucherBill.voucherBill_list.cache';
		this.image=window['sscrp/rppub/components/image/index'];
	}
	handleProvinceChange = (e) => {
		this.state.voucherStatus = 'update';
		let self = this;
		let { title } = self.state;
		title = e + this.state.json['20020MANGE-000152'];/* 国际化处理： 账凭证*/
		self.setState({
			title
		});
	};

	searchById() {
		let self = this;
		let url = '/nccloud/platform/templet/querypage.do';
		let data = {
			pagecode: '20020MANGE_002'
		};
		ajax({
			url,
			data,
			success: function(response) {
				const { data, message, success } = response;
				if (response.success) {
					self.echoData(response.data);
				} else {
					toast({ content: message.message, color: 'warning' });
				}
			}
		});
	}
	//金额显示成千分位
	formatAcuracy(value, len) {
		if (value.value === null || value.value === undefined) {
			return value.value;
		}
		return this.commafy(this.formatDot(value, len));
	}

	changemoney(evidenceData) {
		let { json } = this.state;
		if (evidenceData.localdebitamountTotle.value == evidenceData.localcreditamountTotle.value) {
			if (evidenceData.localdebitamountTotle.value < 0) {
				return json['20020MANGE-000172'] + convertCurrency(json,Math.abs(evidenceData.localcreditamountTotle.value));
			} else {
				return convertCurrency(json,evidenceData.localcreditamountTotle.value);
			}
		}
	}

	formatDot(value, len) {
		let formatVal, dotSplit, val;
		val = (value.value || 0).toString();
		dotSplit = val.split('.');
		if (dotSplit.length > 2 || !value.value) {
			return value.value;
		}
		if (value.scale && value.scale != '-1') {
			len = value.scale;
		}
		len = len || 2;
		if (val.indexOf('.') > -1) {
			formatVal = val.substring(0, val.indexOf('.') + len + 1);
		} else {
			formatVal = val;
		}
		return formatVal;
	}
	//数字转换成千分位 格式
	commafy(num) {
		let pointIndex, intPart, pointPart;
		if (isNaN(num)) {
			return '';
		}
		num = num + '';
		if (/^.*\..*$/.test(num)) {
			pointIndex = num.lastIndexOf('.');
			intPart = num.substring(0, pointIndex);
			pointPart = num.substring(pointIndex + 1, num.length);
			intPart = intPart + '';
			let re = /(-?\d+)(\d{3})/;
			while (re.test(intPart)) {
				intPart = intPart.replace(re, '$1,$2');
			}
			num = intPart + '.' + pointPart;
		} else {
			num = num + '';
			let re = /(-?\d+)(\d{3})/;
			while (re.test(num)) {
				num = num.replace(re, '$1,$2');
			}
		}
		return num;
	}
	editeColums(data) {
		//整理表格保存数
		let { saveData } = this.state;
		let dataIndex = {
			title: (<span fieldid="key">{this.state.json['20020MANGE-000020']}</span>),/* 国际化处理： 序号*/
			key: 'key',
			attrcode:'key',
			dataIndex: 'key',
			width: '80px',
			render: (text, record, index) => {
				return <span fieldid='key'>{index + 1}</span>;
			}
		};
		data.map((item, index) => {
			item.title = item.label;
			item.dataIndex = item.attrcode;
			if (item.attrcode == 'detailindex') {
				item.width = '60px';
			} else if (item.attrcode == 'assid'){
				item.width = '200px';
			} else if (item.attrcode == 'pk_currtype'){
				item.width = '130px';
			} else {
				item.width = '180px';
			}
			item.render = (text, record, index) => {
				//根据字段类型，渲染不同的组件
				let self = this;
				let { rows } = self.state.evidenceData;
				let originData = self.findByKey(record.key, rows);

				let errorMsg, errorBorder;
				let { evidenceColumns } = self.state;
				let defaultValue = {};
				let { pk_accountingbook, voucherStatus } = self.state;
				if (text && text.value !== null) {
					//if(text[item.attrcode]){
					defaultValue = { refname: text.display, refpk: text.value };
					//}
				}
				if (item.attrcode != 'detailindex') {
					if (voucherStatus == 'update') {
						return <div className="textCenter">
						<FormControl
							fieldid={item.attrcode}
							value={text&&text.display&&text.display!='' ? text.display : (text&&text.value&&text.value!=''? text.value: <span>&nbsp;</span>)}
							onChange={(v) => {}}
						/>
						{errorMsg}
					</div>
					} else if (item.itemtype == 'number' && (text && (text.value || text.display))) {
						return <div fieldid={item.attrcode} style={{display:'inline-block', width:'100%', textAlign:'right'}}>{text&&text.display&&text.display!=''?text.display : (text&&text.value&&text.value!=''?text.value : <span>&nbsp;</span>)}</div>
					} else {
						return <div fieldid={item.attrcode}>{text&&text.display&&text.display!=''?text.display : (text&&text.value&&text.value!=''?text.value : <span>&nbsp;</span>)}</div>
					}
				} else {
					return <div fieldid={item.attrcode}>{index + 1}</div>;
				}
			};
		});
		return data;
	}
	//修改页面数据加载渲染
	echoData(data) {
		let self = this;
		let lastTotle = [];
		let {
			attachment,
			evidenceData,
			evidenceColumns,
			headContent,
			footContent,
			saveData,
			isNC01,
			isNC02
		} = self.state;
		let loadColums = [];
		data.details.items.map((item, i) => {
			//表体行数据
			if (item.visible) {
				loadColums.push(item);
			}
		});
		//根据列key 渲染加载的rows
		evidenceData.rows.map((item, index) => {
			loadColums.forEach(function(v, i, a) {
				let objData = {
					display: '',
					value: ''
				};
				item[v.attrcode] = objData;
			});
			item.pk_unit = {
				//添加业务单元字段
				display: '',
				value: ''
			};
			item.pk_accountingbook = {
				display: '',
				value: ''
			};
			item.quantity = {
				display: '',
				value: ''
			};
			item.price = {
				display: '',
				value: ''
			};
		});
		//新增行添加列信息
		loadColums.forEach(function(v, i, a) {
			let objData = {
				display: '',
				value: ''
			};
			evidenceData.newLine[v.attrcode] = objData;
			evidenceData.newLine.pk_unit = objData;
			evidenceData.newLine.quantity = objData;
			evidenceData.newLine.price = objData;
		});
		evidenceColumns = self.editeColums(loadColums);

		let group = evidenceColumns.find((item, i) => item.attrcode == 'groupdebitamount');
		let global = evidenceColumns.find((item, i) => item.attrcode == 'globaldebitamount');
		if (group && JSON.stringify(group) != '{}') {
			isNC01 = true;
		}
		if (global && JSON.stringify(global) != '{}') {
			isNC02 = true;
		}
		data.head.items.map((item, i) => {
			if (item.visible || item.attrcode == 'pk_accountingbook') {
				headContent.push(item);
			}
		});
		//修改模板数据为保存数据格式
		headContent.forEach(function(v, i, a) {
			let objData = {
				display: '',
				value: ''
			};
			if (v.attrcode == 'billdate') {
				objData.value = '';
			} else if (v.attrcode == 'billstatus') {
				objData.value = '1';
			} else if (v.attrcode == 'approvestatus') {
				objData.value = '-1';
			}
			saveData[v.attrcode] = objData;
		});
		footContent = data.tail.items;
		footContent.forEach(function(v, i, a) {
			let objData = {
				display: '',
				value: ''
			};
			saveData[v.attrcode] = objData;
		});
		self.setState({
			isNC01,
			isNC02,
			headContent,
			footContent,
			evidenceColumns,
			attachment,
			evidenceData
		});
	}

	isString(str) {
		return typeof str == 'string' && str.constructor == String;
	}

	isEmptyObject(obj) {
		for (var key in obj) {
			return false; //返回false，不为空对象
		}
		return true; //返回true，为空对象
	}

	handlelist = () => {
		window.location.href = './index.html?isChecked=' + 'true';
	};
	//刷新
	handleRefresh = () => {
		let localUrl = window.location.href;
		if (localUrl.indexOf('?') != -1) {
			let idName = localUrl.split('?')[1].split('=')[1];
			this.queryList(idName);
			setGlobalStorage('localStorage','localSearchId', idName);
		}
	};

	//首张->末张
	handlelistFirst = () => {
		let state = this.state.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20020MANGE-000030'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let searchData = JSON.parse(getGlobalStorage('localStorage','SearchData'));
		let searchId = searchData[0].pk_voucher.value;
		if (searchId) {
			this.queryList(searchId);
			setGlobalStorage('localStorage','localSearchId', searchId);
		}
	};
	handlelistPrevious = () => {
		let state = this.state.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20020MANGE-000030'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let isCopy = this.state.copyStatus;
		if (isCopy == 'copy') {
			let localUrl = window.location.href;
			if (localUrl.indexOf('?') != -1) {
				let idName = getGlobalStorage('localStorage','localSearchId');
				this.queryList(idName);
			}
			return;
		}
		let searchData = JSON.parse(getGlobalStorage('localStorage','SearchData'));
		let localUrl = window.location.href;
		if (localUrl.indexOf('?') != -1) {
			let idName = getGlobalStorage('localStorage','localSearchId');
			if (!idName) {
				idName = localUrl.split('?')[1].split('=')[1];
			}
			for (let i = 0, len = searchData.length; i < len; i++) {
				if (searchData[i].pk_voucher.value == idName) {
					if (i - 1 > 0 || i - 1 == 0) {
						let searchId = searchData[i - 1].pk_voucher.value;
						this.queryList(searchId);
						setGlobalStorage('localStorage','localSearchId', searchId);
					} else {
						Message.create({ content: this.state.json['20020MANGE-000031'], color: 'warning' });/* 国际化处理： 已经是第一条*/
						// toast({ content: '已经是第一条', color: 'warning' });
					}
				}
			}
		}
	};
	handlelistNext = () => {
		let state = this.state.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20020MANGE-000030'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let searchData = JSON.parse(getGlobalStorage('localStorage','SearchData'));
		let localUrl = window.location.href;
		if (localUrl.indexOf('?') != -1) {
			let idName = getGlobalStorage('localStorage','localSearchId');
			if (!idName) {
				idName = localUrl.split('?')[1].split('=')[1];
			}
			for (let i = 0, len = searchData.length; i < len; i++) {
				if (searchData[i].pk_voucher.value == idName) {
					if (i + 1 < len) {
						let searchId = searchData[i + 1].pk_voucher.value;
						this.queryList(searchId);
						setGlobalStorage('localStorage','localSearchId', searchId);
					} else {
						Message.create({ content: this.state.json['20020MANGE-000032'], color: 'warning' });/* 国际化处理： 已经是最后一条*/
					}
				}
			}
		}
	};
	handlelistEnd = () => {
		let state = this.state.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20020MANGE-000030'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let searchData = JSON.parse(getGlobalStorage('localStorage','SearchData'));
		let searchId = searchData[searchData.length - 1].pk_voucher.value;
		if (searchId) {
			this.queryList(searchId);
			setGlobalStorage('localStorage','localSearchId', searchId);
		}
	};
	addNewLine(record) {
		let { evidenceData } = this.state;
		let newkey = ++record;
		let cloneNewLine = deepClone(evidenceData.newLine);
		let newLine = Object.assign({}, cloneNewLine, {
			key: newkey
		});
		if (newkey <= evidenceData.rows.length) {
			evidenceData.rows.map((item, index) => {
				if (item.key >= newkey) {
					item.key++;
				}
			});
		}
		evidenceData.rows.splice(newkey - 1, 0, newLine);
	}

	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

	//格式化后台返回数据
	dataFormat = (data) => {
		let result = [];
		if (data) {
			if (data[0]) {
				data.map((item, index) => {
					item.key = index + 1;
					result.push(item);
				});
			}
		}
		return result;
	};

	//获取计算合计之后的数据
	getTableSumData = (data, amount, Totle) => {
		if (data.length != 0) {
			//需要合计的字段
			let { rows } = this.state.evidenceData;
			let newObj = this.state.evidenceData[Totle];
			let sumObj = {
				[amount]: []
			};
			for (let key in sumObj) {
				let arr = data.map((item) => {
					return item[key].value ? parseFloat(item[key].value) : 0;
				});
				newObj.value = arr.reduce((prev, curr) => {
					return prev + curr;
				});
			}
			return newObj;
		}
	};

	//主子表信息
	getData = (expanded, record) => {
	};
	getRow = (expanded, record) => {
		let copyData = deepClone(expanded);
		this.state.copyRecord = record;
		//表体数据增删获取key
		let { getrow } = this.state;
		getrow = expanded.key;
		this.setState({
			getrow: getrow
		});
	};

	componentWillMount() {
		let { id, bill_type, scene } = this.state;
		id = this.props.getUrlParam('bill_id');
		bill_type = this.props.getUrlParam('tradetype') ? this.props.getUrlParam('tradetype') : this.props.getUrlParam('bill_type');
		scene = this.props.getUrlParam('scene');
		if (scene && (scene === 'approve' || scene === 'approvesce')) {
			id = this.props.getUrlParam('id');
			bill_type = 'VRD1';
		}
		if(scene && scene != undefined){
			this.setState({scene:scene});
		}
		let callback = (json) => {
			this.setState({ 
				json: json,
				id,
				bill_type,
				transtype: bill_type 
			}, () => {
				initTemplate.call(this, this.props);
				this.searchById();
				//根据id 渲染数据
				if (id) {
					this.queryList(id);
					this.state.voucherStatus = 'view';
					this.props.setUrlParam(id);
				}else{
					//没有id时按照状态调整
					this.buttonStatus();
				}
			})
		}
		getMultiLang({ moduleId: ['20020MANGE','publiccomponents'], currentLocale: 'simpchn', domainName: 'gl', callback })
	}
	//详情页添加数量列
	addNumber = () => {
		let self = this;
		let { evidenceColumns } = self.state;
		let quantity = {
			title: self.state.json['20020MANGE-000037'],/* 国际化处理： 数量/单价*/
			key: 'quantity',
			dataIndex: 'quantity',
			width: '180px',
			render: (text, record, index) => {
				if(record.flag){
					return (
						<span>
							{record.debitquantity.value || ''}/{record.price.value || ''}
						</span>
					);
				} else {
					return (
						<span/>
					);
				} 
			}
		};
		//判断是否有数量这一列
		let originData = self.findByKey(quantity.key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			let cloneNewLine = deepClone(evidenceColumns);
			cloneNewLine.splice(5, 0, quantity);
			self.setState({
				evidenceColumns: cloneNewLine
			});
		}
	};
	queryList(id) {
		let self = this;
		let {
			pk_accountingbook,
			nov,
			startdate,
			attachment,
			evidenceData,
			saveData,
			update,
			abandonShow,
			billstatus,
			transtype
		} = self.state;
		let status = self.props.getUrlParam('status');
		let url = '/nccloud/gl/voucher/querybill.do';
		let data = {
			pk_voucher: id
		};
		ajax({
			url: url,
			data: data,
			success: function(response) {
				const { data, message, success } = response;
				if (success) {
					if(!data){
						toast({ content: self.state.json['20020MANGE-000185'], color: 'warning' });
						return;
					}

					let { NC001, NC002, isShowUnit, excrate1, excrate2,isShowNum } = response.data.paraInfo;
					let { voucher } = response.data;
					self.setState({ billno: voucher.billno });
					//  this.props.form.setAllFormValue({
					// 	[this.gl_brsettingFormId]:data.head[this.gl_brsettingFormId],
					// });
					if(voucher && voucher.details && voucher.details.length > 0){
						voucher.details.map((item, i) => {
							//渲染是否有动态新增列，有待优化
							item.key = item.detailindex.value;
							if (NC001) {
								item.groupType = NC001;
							}
							if (item.isShowNum && item.isShowNum.value) {
								//根据返回数据判断是否有数量列
								item.flag = true;
							}
							
							if (item.debitamount.value && item.debitamount.value != 0) {
								item.amount = {
									scale:item.debitamount && item.debitamount.scale||item.amount&&item.amount.scale,
									value: item.debitamount && item.debitamount.value||item.amount&&item.amount.value,
									editable: item.debitamount && item.debitamount.editable||item.amount&&item.amount.editable
								};
							} else {
								item.amount = {
									scale:item.creditamount && item.creditamount.scale||item.amount&&item.amount.scale,
									value: item.creditamount && item.creditamount.value||item.amount&&item.amount.value,
									editable: item.creditamount && item.creditamount.editable||item.amount&&item.amount.editable
								};
								item.debitquantity=item.creditquantity
							}
							if(item.localcreditamount.value && item.localcreditamount.value == 0){
								item.localcreditamount.value = null;
							}
							if(item.localdebitamount.value && item.localdebitamount.value == 0){
								item.localdebitamount.value = null;
							}
						});
					}else{
						voucher.details = [];
					}
					
					self.props.form.setFormStatus(self.formId, 'browse');
					self.props.form.setFormStatus(self.formIdtail, 'browse');
					//修改太页面参照设置依赖
					pk_accountingbook.value = voucher.pk_accountingbook.value;
					//对details按detailindex进行排序
					// if(voucher.details && voucher.details.length > 0){
					// 	self.selectSort(voucher.details);
					// }
					evidenceData.rows = voucher.details;
					evidenceData.index = voucher.details.length;
					saveData = voucher;
					saveData['paraInfo'] = response.data.paraInfo;
					billstatus = voucher.billstatus.value;
					evidenceData.localcreditamountTotle.value = voucher.totalcredit.display;
					evidenceData.localdebitamountTotle.value = voucher.totaldebit.display;
					saveData.period.display = voucher.year.value + '-' + voucher.period.value;
					transtype = voucher.pk_tradetypecode.value;
					update = true;
					// abandonShow=data.discardflag.value;

					evidenceData.localcreditamountTotle.value = voucher.totalcredit.value; //合计值获取
					evidenceData.localdebitamountTotle.value = voucher.totaldebit.value;
					evidenceData.groupcreditamountTotle.value = voucher.totalcreditgroup.value;
					evidenceData.groupdebitamountTotle.value = voucher.totaldebitgroup.value;
					evidenceData.globalcreditamountTotle.value = voucher.totalcreditglobal.value;
					evidenceData.globaldebitamountTotle.value = voucher.totaldebitglobal.value;

					self.setState(
						{
							pk_accountingbook,
							billstatus,
							evidenceData,
							update,
							saveData,
							abandonShow,
							transtype
						},
						() => {
							if(isShowNum){
								self.addNumber();
							}
							//表头表尾赋默认值
							self.props.form.setFormItemsValue(self.formId, saveData);
							//存在单据时调整按钮状态
							self.buttonStatus();
						}
					);
				} else {
					toast({ content: message.message, color: 'warning' });
				}
			}
		});
	}

	selectSort = (arr) =>{
		let min,temp;
		for(var i=0;i<arr.length-1;i++){
			min=i;
			for(var j=i+1;j<arr.length;j++){
				if(arr[j].key < arr[min].key){
					min = j;
				}
			}
			temp=arr[i];
			arr[i]=arr[min];
			arr[min]=temp;
  
		}
		return arr;
	}
	componentDidMount() {
		//用于接收跨域iframe传过来的数据
	}
	//渲染表头数据
	getHead = (data) => {
		const provinceData = [
			//单据状态
			{ display: this.state.json['20020MANGE-000153'], value: '1' },/* 国际化处理： 待提交*/
			{ display: this.state.json['20020MANGE-000154'], value: '2' },/* 国际化处理： 待审批*/
			{ display: this.state.json['20020MANGE-000155'], value: '3' }/* 国际化处理： 已完成*/
		];
		const billstatusData = [
			//审批状态
			{ display: this.state.json['20020MANGE-000007'], value: '-1' },/* 国际化处理： 自由*/
			{ display: this.state.json['20020MANGE-000156'], value: '0' },/* 国际化处理： 审批为通过*/
			{ display: this.state.json['20020MANGE-000157'], value: '1' },/* 国际化处理： 审批通过*/
			{ display: this.state.json['20020MANGE-000158'], value: '2' },/* 国际化处理： 审批进行中*/
			{ display: this.state.json['20020MANGE-000047'], value: '3' }/* 国际化处理： 提交*/
		];
		let self = this;
		let { evidenceColumns, saveData, evidenceData } = self.state;
		return data.length != 0 ? (
			<Form
				showSubmit={false}
				checkFormNow={this.state.checkForm}
				useRow={true}
			>
				{data.map((item, i) => {
					if (item.itemtype == 'combox') {
						return (
							<FormItem
								inline={true}
								showMast={true}
								labelXs={1}
								labelSm={1}
								labelMd={1}
								xs={2}
								md={2}
								sm={2}
								labelName={item.label}
								isRequire={false}
								//isViewMode
								method="blur"
							>
								{item.attrcode == 'billstatus' ? (
									<InputItem
										isViewMode
										name={item.attrcode}
										type="customer"
										defaultValue={
											saveData[item.attrcode].value ? (
												provinceData[saveData[item.attrcode].value - 1].display
											) : (
												provinceData[0].display
											)
										}
										onChange={(v) => {
											self.state.voucherStatus = 'update';
											let nameValue = saveData[item.attrcode];
											nameValue.value = v;
											self.setState({
												saveData
											});
										}}
									/>
								) : (
									<InputItem
										isViewMode
										name={item.attrcode}
										type="customer"
										defaultValue={
											saveData[item.attrcode].value ? (
												billstatusData[saveData[item.attrcode].value - 0 + 1].display
											) : (
												billstatusData[0].display
											)
										}
										onChange={(v) => {
											self.state.voucherStatus = 'update';
											let nameValue = saveData[item.attrcode];
											nameValue.value = v;
											self.setState({
												saveData
											});
										}}
									/>
								)}
							</FormItem>
						);
					} else {
						return (
							<FormItem
								inline={true}
								showMast={true}
								labelXs={1}
								labelSm={1}
								labelMd={1}
								xs={2}
								md={2}
								sm={2}
								labelName={item.label}
								isRequire={false}
								//isViewMode
								method="blur"
							>
								<InputItem
									isViewMode
									name={item.attrcode}
									type="customer"
									defaultValue={
										saveData[item.attrcode].display ? (
											saveData[item.attrcode].display
										) : (
											saveData[item.attrcode].value
										)
									}
									onChange={(v) => {
										self.state.voucherStatus = 'update';
										let nameValue = saveData[item.attrcode];
										nameValue.value = v;
										self.setState({
											saveData
										});
									}}
								/>
							</FormItem>
						);
					}
				})}
			</Form>
		) : null;
	};

	//动态渲染表格
	getTable = (evidenceColumns, evidenceRows) => {
		let tableArea= this.refs.tableArea ? document.getElementById('tableArea').offsetHeight : '400px';
			// let totalLine=document.getElementById('totalLine');
			// let totalHeight=0;
			// if(totalLine){
			// 	totalHeight=totalLine.offsetHeight
			// }else{
			// 	totalHeight=0;
			// }
			// this.tableHeight=tableArea-totalHeight;
			let appHeight=document.getElementById('app').offsetHeight;
			let voucherFormHeight=document.getElementById('voucherFormHeight')?document.getElementById('voucherFormHeight').offsetHeight:0;
			let totalLine=document.getElementById('totalLine');
			let totalHeight=0;
			if(totalLine){
				totalHeight=totalLine.offsetHeight;
			}else{
				totalHeight=0;
			}
			this.tableHeight=appHeight-voucherFormHeight-totalHeight-80+'px';
		return (
			<NCDiv fieldid="voucherBill" areaCode={NCDiv.config.TableCom}>
				<Table
					className="table-data-list"
					bordered
					onExpand={this.getData}
					rowClassName={(record,index,indent)=>{
						if(this.state.getrow-1==index){
							return 'selected'
						}else{
							return ''
						}
					}}
					onRowClick={this.getRow}
					// expandedRowRender={this.expandedRowRender}
					columns={evidenceColumns}
					data={evidenceRows}
					scroll={{
						x: true,//evidenceColumns.length > 8 ? 100 + (evidenceColumns.length - 8) * 15 + '%' : '100%',
						y: this.tableHeight
					}}
					bodyStyle={{height:this.tableHeight}}
				/>
			</NCDiv>
		);
	};

	//影像扫描
	receiptScan = () => {
		let saveData = this.state.saveData;

		var billInfoMap = {};
		//基础字段 单据pk,单据类型，交易类型，单据的组织
		billInfoMap.pk_billid = saveData.pk_voucher.value;
		billInfoMap.pk_billtype = saveData.pk_billtypecode.value;
		billInfoMap.pk_tradetype = saveData.pk_tradetypecode.value;
		billInfoMap.pk_org = saveData.pk_org.value;

		//影像所需 FieldMap
		billInfoMap.BillType = saveData.pk_tradetypecode.value;
		billInfoMap.BillDate = saveData.creationtime.value;
		billInfoMap.Busi_Serial_No = saveData.pk_voucher.value;
		billInfoMap.OrgNo = saveData.pk_org.value;
		billInfoMap.BillCode = saveData.billno.value;
		billInfoMap.OrgName = saveData.pk_org.display;
		billInfoMap.Cash = saveData.totaldebit.value;
		const { imageScan } = this.image;
		imageScan(billInfoMap, 'iweb');
	};
	

	//影像查看
	receiptCheck = () => {
		let saveData = this.state.saveData;

		var billInfoMap = {};
		//基础字段 单据pk,单据类型，交易类型，单据的组织
		billInfoMap.pk_billid = saveData.pk_voucher.value;
		billInfoMap.pk_billtype = saveData.pk_billtypecode.value;
		billInfoMap.pk_tradetype = saveData.pk_tradetypecode.value;
		billInfoMap.pk_org = saveData.pk_org.value;
		const { imageView  } = this.image;
		imageView(billInfoMap,  'iweb');
	};

	//新增
	handleSubmitAdd = () => {
		this.props.pushTo('/add', {
			status: 'add',
			id: this.state.id,
			appcode: this.props.getSearchParam('c'), //获取小应用编码
			tradetype: this.state.transtype //获取当前交易类型
		});
	};
	//修改单据
	handleUpdate = () => {
		this.props.pushTo('/add', {
			status: 'edit',
			bill_id: this.state.id,
			appcode: this.props.getSearchParam('c'), //获取小应用编码
			pagecode: '20020MANGE_003', //编辑态卡片页面编码
			tradetype: this.state.transtype //获取当前交易类型
		});
	};

	//复制
	handleCopy = () => {
		this.props.pushTo('/add', {
			status: 'copy',
			id: this.state.id,
			appcode: this.props.getSearchParam('c'), //获取小应用编码
			pagecode: '20020MANGE_003', //编辑态卡片页面编码
			tradetype: this.state.transtype //获取当前交易类型
		});
	};
	//删除
	handleDelete = () => {
		let self = this;
		var index1 = [];
		var id = self.state.id;
		index1.push(id);
		promptBox({
			'color': 'warning',
			'title': self.state.json['20020MANGE-000084'],/* 国际化处理： 删除*/
			'content': self.state.json['20020MANGE-000159'],/* 国际化处理： 确定要删除吗?*/
			beSureBtnClick: function() {
				let checkedObj3 = [];
				checkedObj3.push({
					pk_bill: id,
					billType: 'VR01',
					index: 0,
					pageId: self.props.getSearchParam('p')
				});
				ajax({
					url: '/nccloud/gl/voucherbill/delete.do',
					data: checkedObj3,
					success: (res) => {
						if (res) {
							toast({ content: self.state.json['20020MANGE-000079'], color: 'success' });/* 国际化处理： 删除成功*/
							var nextId = getNextId(id, dataSource);
							if(nextId){
								self.state.id = nextId;
							}
							deleteCacheById('pk_voucher', id, dataSource);
							var cardData = getCacheById(nextId, dataSource);
							if (cardData) {
								cardData = cardData.voucher;
								let response = cardData;
								let {
									pk_accountingbook,
									nov,
									startdate,
									attachment,
									evidenceData,
									saveData,
									update,
									abandonShow,
									billstatus
								} = self.state;
								response.details.map((item, i) => {
									//渲染是否有动态新增列，有待优化
									item.key = ++i;
									if (item.isShowNum && item.isShowNum.value) {
										//根据返回数据判断是否有数量列
										item.flag = true;
									}
									if (item.debitamount.value && item.debitamount.value != 0) {
										item.amount = {
											scale:item.debitamount && item.debitamount.scale||item.amount&&item.amount.scale,
											value: item.debitamount && item.debitamount.value||item.amount&&item.amount.value,
											editable: item.debitamount && item.debitamount.editable||item.amount&&item.amount.editable
										};
									} else {
										item.amount = {
											scale:item.creditamount && item.creditamount.scale||item.amount&&item.amount.scale,
											value: item.creditamount && item.creditamount.value||item.amount&&item.amount.value,
											editable: item.creditamount && item.creditamount.editable||item.amount&&item.amount.editable
										};
										item.debitquantity=item.creditquantity
									}
									if(item.localcreditamount.value && item.localcreditamount.value == 0){
										item.localcreditamount.value = null;
									}
									if(item.localdebitamount.value && item.localdebitamount.value == 0){
										item.localdebitamount.value = null;
									}
								});
								evidenceData.rows = response.details;
								evidenceData.index = response.details.length;
			
								saveData = cardData;
								evidenceData.localcreditamountTotle.value = response.totalcredit.display;
								evidenceData.localdebitamountTotle.value = response.totaldebit.display;
								saveData.period.display = response.year.value + '-' + response.period.value;
								update = true;
								evidenceData.localcreditamountTotle.value = response.totalcredit.value; //合计值获取
								evidenceData.localdebitamountTotle.value = response.totaldebit.value;
								evidenceData.groupcreditamountTotle.value = response.totalcreditgroup.value;
								evidenceData.groupdebitamountTotle.value = response.totaldebitgroup.value;
								evidenceData.globalcreditamountTotle.value = response.totalcreditglobal.value;
								evidenceData.globaldebitamountTotle.value = response.totaldebitglobal.value;
								self.setState(
									{
										evidenceData,
										update,
										saveData
									},
									() => {
										if(cardData.paraInfo.isShowNum){
											self.addNumber();
										}
										self.props.form.setFormItemsValue(self.formId, saveData);
										self.buttonStatus();
									}
								);
							} else {
								pageInfoClick(self.props, nextId, self);
							}
						}
					}
				});
			}
		});
	};
	//上传前检查
	beforeUpload(billId, fullPath, file, fileList) {
		const isLt20M = file.size / 1024 / 1024 < 20;
		if (!isLt20M) {
			toast({ content: this.state.json['20020MANGE-000149'], color: 'warning' });/* 国际化处理： 上传大小小于20M！*/
		}
		return isLt20M;
		// 备注： return false 不执行上传  return true 执行上传
	}
	//关闭
	onHide = () => {
		this.setState({
			showUploader: false
		});
	};
	//指派
	getAssginUsedr = (value) => {
		let self = this;
		let {
			evidenceData,
			saveData,
			update
		} = self.state;
		let url = '/nccloud/gl/voucher/sendApprovebill.do';
		saveData.assignObj = value;
		ajax({
			url: url,
			data: saveData,
			success: function(response) {
				if (response.success) {
					toast({ content: self.state.json['20020MANGE-000022'], color: 'success' });/* 国际化处理： 提交成功*/
					self.closeAssgin();
					//更新缓存
					updateCache('pk_voucher',self.state.id,response.data,self.formId,self.dataSource,response.data);
					let cloneVoucher = deepClone(response.data.voucher);
					cloneVoucher.paraInfo = response.data.paraInfo;
					response.data = cloneVoucher;
					response.data.details.map((item, i) => {
						//渲染是否有动态新增列，有待优化
						item.key = ++i;
						if (item.isShowNum && item.isShowNum.value) {
							//根据返回数据判断是否有数量列
							item.flag = true;
						}
						if (item.debitamount.value && item.debitamount.value != 0) {
							item.amount = {
								scale:item.debitamount && item.debitamount.scale||item.amount&&item.amount.scale,
								value: item.debitamount && item.debitamount.value||item.amount&&item.amount.value,
								editable: item.debitamount && item.debitamount.editable||item.amount&&item.amount.editable
							};
						} else {
							item.amount = {
								scale:item.creditamount && item.creditamount.scale||item.amount&&item.amount.scale,
								value: item.creditamount && item.creditamount.value||item.amount&&item.amount.value,
								editable: item.creditamount && item.creditamount.editable||item.amount&&item.amount.editable
							};
							item.debitquantity=item.creditquantity
						}
						if(item.localcreditamount.value && item.localcreditamount.value == 0){
							item.localcreditamount.value = null;
						}
						if(item.localdebitamount.value && item.localdebitamount.value == 0){
							item.localdebitamount.value = null;
						}
					});
					evidenceData.rows = response.data.details;
					evidenceData.index = response.data.details.length;

					saveData = response.data;
					evidenceData.localcreditamountTotle.value = response.data.totalcredit.display;
					evidenceData.localdebitamountTotle.value = response.data.totaldebit.display;
					saveData.period.display = response.data.year.value + '-' + response.data.period.value;
					update = true;

					evidenceData.localcreditamountTotle.value = response.data.totalcredit.value; //合计值获取
					evidenceData.localdebitamountTotle.value = response.data.totaldebit.value;
					evidenceData.groupcreditamountTotle.value = response.data.totalcreditgroup.value;
					evidenceData.groupdebitamountTotle.value = response.data.totaldebitgroup.value;
					evidenceData.globalcreditamountTotle.value = response.data.totalcreditglobal.value;
					evidenceData.globaldebitamountTotle.value = response.data.totaldebitglobal.value;
					self.setState({
						evidenceData,
						update,
						saveData
					},
					() => {
						if(response.data.paraInfo.isShowNum){
							self.addNumber();
						}
						self.props.form.setFormItemsValue(self.formId, saveData);
						self.buttonStatus();
					});
				} else {
					toast({ content: response.error.message, color: 'warning' });
				}
			}
		});
	};

	//指派关闭
	closeAssgin=()=>{
        if (this.state.compositedisplay) {
            this.setState({ compositedisplay: false });
        }
    }

	//打开审批详情
	openApprove = ()=>{
		this.setState({
			showApproveDetail: true
		})
	}
	//审批详情模态框控制
	closeApprove = () => {
		this.setState({
			showApproveDetail: false
		})
	}

	//联查凭证
	linkVoucher = () => {
		let that=this;
		let linkArray = [];
		//组装参数
		let linkObj = {
			pk_billtype: '',
			pk_group: '',
			pk_org: '',
			relationID: ''
		};
		linkObj.pk_billtype = this.state.transtype;
		linkObj.pk_group = this.props.form.getFormItemsValue(this.formId,'pk_group').value;
		linkObj.pk_org = this.props.form.getFormItemsValue(this.formId,'pk_org').value;
		linkObj.relationID = this.state.id;
		linkArray.push(linkObj);
		//将数据放到浏览器缓存里
		// cacheTools.set(this.props.getSearchParam('c') + '_LinkVouchar', linkArray);
		ajax({
			url: '/nccloud/gl/voucherbill/queryFipUrl.do',
			// data: { appCode: '10170410', pageCode: '10170410_1017041001' },
			data: linkArray,
			success: (res) => {
				if (res.success) {
					if (res.data.des) {
						// this.props.openTo(res.data, {
						// 	status: 'browse',
						// 	scene: this.props.getSearchParam('c') + '_LinkVouchar',
						// 	appcode: '10170410',
						// 	pagecode: '10170410_1017041001'
						// });
						//跳转到凭证界面
						if (res.data.pklist) {
							if (res.data.pklist.length == 1) {
								//单笔联查
								that.props.openTo(res.data.url, {
									status: 'browse',
									appcode: res.data.appcode,
									pagecode: res.data.pagecode,
									id: res.data.pklist[0],
									n: that.state.json['public-000231'], //'联查凭证'
									backflag: 'noback'
								});
								return;
							} else {
								//多笔联查
								cacheTools.set(res.data.cachekey, res.data.pklist);
								that.props.openTo(res.data.url, {
									status: 'browse',
									appcode: res.data.appcode,
									pagecode: res.data.pagecode,
									scene: res.data.appcode + res.data.src,
									n: that.state.json['public-000231'] //'联查凭证'
								});
								return;
							}
						}
					}else{
						//跳转到会计平台
						cacheTools.set(res.data.appcode + res.data.src, res.data.pklist);
						that.props.openTo(res.data.url, {
							status: 'browse',
							appcode: res.data.appcode,
							pagecode: res.data.pagecode,
							scene: res.data.appcode + res.data.src
						});
						return
					}
				}
			}
		});
	}
	//返回
	backToList = () => {
		this.props.pushTo('/list', { appcode: this.props.getSearchParam('c') });
	};
	//附件
	openFile = () => {
		let pk = this.state.id;
		let vbillcode = this.state.billno;
		if (this.state.showUploader == false) {
			this.setState(
				{ showUploader: true,
					checkId: pk,
					checkValue: vbillcode.value
			 });
		} else {
			this.setState(
				{ showUploader: false,
				checkId: pk,
				checkValue: vbillcode.value 
			});
		}
	};
	/**
 	  * 打印
      */
	handlePrint = () => {
		let index1 = [];
		let searchId = this.state.id;
		index1.push(searchId);
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/gl/voucherbill/print.do', //后台服务url
			{
				billtype: 'VR01', //单据类型
				appcode: this.props.getSearchParam('c'), //获取小应用编码
				nodekey: 'VRD1_CARD', //模板节点标识
				oids: index1 // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			},
			false
		);
	};

	/**
	 * 打印输出
	 */
	open = () => {
		let index1 = [];
		let searchId = this.state.id;
		index1.push(searchId);
		// 在你想要触发的事件里面调用output方法，必须传的参数有data， 选择传的参数有url（要打印调用的后台接口），callback（打印后的回调函数）
		output({
			url: '/nccloud/gl/voucherbill/printoutput.do',
			data: {
				outputType: 'output', //输出类型
				funcode: this.props.getSearchParam('c'), //获取小应用编码
				nodekey: 'VRD1_CARD', //模板节点标识
				oids: index1 // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			},
			callback: this.submit
		});
	};
	submit = () => {};

	//收回
	handleBack = () => {
		let self = this;
		let {
			evidenceData,
			saveData,
			update
		} = self.state;
		let url = '/nccloud/gl/voucher/unsendApprovebill.do';
		ajax({
			url: url,
			data: saveData,
			success: function(response) {
				if (response.success) {
					toast({ content: self.state.json['20020MANGE-000160'], color: 'success' });/* 国际化处理： 收回成功*/
					//更新缓存
					updateCache('pk_voucher',self.state.id,response.data,self.formId,self.dataSource,response.data.voucher);
					let cloneVoucher = deepClone(response.data.voucher);
					cloneVoucher.paraInfo = response.data.paraInfo;
					response.data = cloneVoucher;
					response.data.details.map((item, i) => {
						//渲染是否有动态新增列，有待优化
						item.key = ++i;
						if (item.isShowNum && item.isShowNum.value) {
							//根据返回数据判断是否有数量列
							item.flag = true;
						}
						if (item.debitamount.value && item.debitamount.value != 0) {
							item.amount = {
								scale:item.debitamount && item.debitamount.scale||item.amount&&item.amount.scale,
								value: item.debitamount && item.debitamount.value||item.amount&&item.amount.value,
								editable: item.debitamount && item.debitamount.editable||item.amount&&item.amount.editable
							};
						} else {
							item.amount = {
								scale:item.creditamount && item.creditamount.scale||item.amount&&item.amount.scale,
								value: item.creditamount && item.creditamount.value||item.amount&&item.amount.value,
								editable: item.creditamount && item.creditamount.editable||item.amount&&item.amount.editable
							};
							item.debitquantity=item.creditquantity
						}
						if(item.localcreditamount.value && item.localcreditamount.value == 0){
							item.localcreditamount.value = null;
						}
						if(item.localdebitamount.value && item.localdebitamount.value == 0){
							item.localdebitamount.value = null;
						}
					});
					evidenceData.rows = response.data.details;
					evidenceData.index = response.data.details.length;

					saveData = response.data;
					evidenceData.localcreditamountTotle.value = response.data.totalcredit.display;
					evidenceData.localdebitamountTotle.value = response.data.totaldebit.display;
					saveData.period.display = response.data.year.value + '-' + response.data.period.value;
					update = true;
					evidenceData.localcreditamountTotle.value = response.data.totalcredit.value; //合计值获取
					evidenceData.localdebitamountTotle.value = response.data.totaldebit.value;
					evidenceData.groupcreditamountTotle.value = response.data.totalcreditgroup.value;
					evidenceData.groupdebitamountTotle.value = response.data.totaldebitgroup.value;
					evidenceData.globalcreditamountTotle.value = response.data.totalcreditglobal.value;
					evidenceData.globaldebitamountTotle.value = response.data.totaldebitglobal.value;
					self.setState(
						{
							evidenceData,
							update,
							saveData
						},
						() => {
							if(response.data.paraInfo.isShowNum){
								self.addNumber();
							}
							self.props.form.setFormItemsValue(self.formId, saveData);
							self.buttonStatus();
						}
					);
				} else {
					toast({ content: response.error.message, color: 'warning' });
				}
			}
		});
	};

	//提交
	handleSubmit = () => {
		let self = this;
		let {
			evidenceData,
			saveData,
			update
		} = self.state;
		let url = '/nccloud/gl/voucher/sendApprovebill.do';
		ajax({
			url: url,
			data: saveData,
			success: function(response) {
				if (response.success) {
					if (
						response.data.workflow &&
						(response.data.workflow == 'approveflow' || response.data.workflow == 'workflow')
					) {
						self.setState({
							compositedata: response.data,
							compositedisplay: true
						});
					} else {
						toast({ content: self.state.json['20020MANGE-000022'], color: 'success' });/* 国际化处理： 提交成功*/
						//更新缓存
						updateCache('pk_voucher',self.state.id,response.data,self.formId,self.dataSource,response.data.voucher);
						let cloneVoucher = deepClone(response.data.voucher);
						cloneVoucher.paraInfo = response.data.paraInfo;
						response.data = cloneVoucher;
						response.data.details.map((item, i) => {
							//渲染是否有动态新增列，有待优化
							item.key = ++i;
							if (item.isShowNum && item.isShowNum.value) {
								//根据返回数据判断是否有数量列
								item.flag = true;
							}
							if (item.debitamount.value && item.debitamount.value != 0) {
								item.amount = {
									scale:item.debitamount && item.debitamount.scale||item.amount&&item.amount.scale,
									value: item.debitamount && item.debitamount.value||item.amount&&item.amount.value,
									editable: item.debitamount && item.debitamount.editable||item.amount&&item.amount.editable
								};
							} else {
								item.amount = {
									scale:item.creditamount && item.creditamount.scale||item.amount&&item.amount.scale,
									value: item.creditamount && item.creditamount.value||item.amount&&item.amount.value,
									editable: item.creditamount && item.creditamount.editable||item.amount&&item.amount.editable
								};
								item.debitquantity=item.creditquantity
							}
							if(item.localcreditamount.value && item.localcreditamount.value == 0){
								item.localcreditamount.value = null;
							}
							if(item.localdebitamount.value && item.localdebitamount.value == 0){
								item.localdebitamount.value = null;
							}
						});
						evidenceData.rows = response.data.details;
						evidenceData.index = response.data.details.length;

						saveData = response.data;
						evidenceData.localcreditamountTotle.value = response.data.totalcredit.display;
						evidenceData.localdebitamountTotle.value = response.data.totaldebit.display;
						saveData.period.display = response.data.year.value + '-' + response.data.period.value;
						update = true;
						evidenceData.localcreditamountTotle.value = response.data.totalcredit.value; //合计值获取
						evidenceData.localdebitamountTotle.value = response.data.totaldebit.value;
						evidenceData.groupcreditamountTotle.value = response.data.totalcreditgroup.value;
						evidenceData.groupdebitamountTotle.value = response.data.totaldebitgroup.value;
						evidenceData.globalcreditamountTotle.value = response.data.totalcreditglobal.value;
						evidenceData.globaldebitamountTotle.value = response.data.totaldebitglobal.value;
						self.setState(
							{
								evidenceData,
								update,
								saveData
							},
							() => {
								if(response.data.paraInfo.isShowNum){
									self.addNumber();
								}
								self.props.form.setFormItemsValue(self.formId, saveData);
								self.buttonStatus();
							}
						);
					}
				} else {
					toast({ content: response.error.message, color: 'warning' });
				}
			}
		});
	};
	//按钮状态统一处理方法
	buttonStatus = () => {
		let self = this;
		let { id,saveData } = self.state;
		if(id){
			self.props.button.setButtonVisible(['Edit','Delete','Commit','Uncommit','RelatedQuery','LinkVoucher'/*, 'LinkAprv'*/],false);
			self.props.button.setButtonVisible(['Add','Copy','Print','Output', 'AttachManage','ReceiptCheck','ReceiptScan'],true);
			self.setState({
				cardPaginationShow: true// 是否显示的判断依据条件??
			})
			let buttonArray=[];
			let billstatus = saveData.billstatus.value;
			let approvestatus = saveData.approvestatus.value;
			if(approvestatus == 1 || approvestatus == 2 || approvestatus == 3){
				buttonArray.push('Uncommit');
			}
			if(billstatus == 1){
				buttonArray.push('Edit');
				buttonArray.push('Delete');
				buttonArray.push('Commit');
			} else if(billstatus == 2){
				buttonArray.push('RelatedQuery');
				buttonArray.push('LinkAprv');
			} else if(billstatus == 3){
				buttonArray.push('RelatedQuery');
				buttonArray.push('LinkVoucher');
			} 
			self.props.button.setButtonVisible(buttonArray,true);
		} else {
			self.props.button.setButtonVisible(['Edit','Delete','Copy','Commit','Uncommit','More'],false);
			self.props.button.setButtonVisible(['Add'],true);
		}
	};

	render() {
		let self = this;
		let { evidenceData, showUploader, ProcessModalShow, historyProcess } = self.state;
		let { table, button, search, modal, form, cardPagination } = self.props;
		let { createCardPagination } = cardPagination;
		const { createForm } = form;
		let evidenceRows = evidenceData.rows;
		let { evidenceColumns, headContent, footContent, saveData } = self.state;
		let tableWidth=this.refs.tableArea ? document.getElementById('tableArea').offsetWidth:'98%';
		return (
			<div id="evidence" className="nc-bill-card nc-bill-list">
				<div id="voucherFormHeight">
					<HeaderArea
						title={self.state.json['20020MANGE-000051']/* 国际化处理： 凭证单*/}
						initShowBackBtn={self.state.scene && self.state.scene != undefined ? false : true}
						backBtnClick={this.backToList}
						btnContent={
							<div>
								{self.props.button.createButtonApp({
										area: self.state.scene?BZCX:'gl_voucher',
										onButtonClick: onButtonClick.bind(self),
										popContainer: document.querySelector('.header-button-area')
									})}
								
							</div>
						}
						pageBtnContent={
							self.state.cardPaginationShow ?
								createCardPagination({
									dataSource: dataSource,
									handlePageInfoChange: pageInfoClick.bind(this)
								})
							: ''
						}
					/>
					<div className="nc-bill-form-area">
						{createForm(
							self.formId,
							{
								//编辑后事件
								// onAfterEvent: afterEvent.bind(self),
								//onBeforeEvent:  self.bodyBeforeEvent.bind(self)添加过滤
							}
						)}
					</div>
				</div>
				<div className="table-area" ref="tableArea" id="tableArea">
					<div className="header table-header">
						<div className="title">{self.state.json['20020MANGE-000055']}</div>{/* 国际化处理： 分录信息*/}
					</div>
					{self.getTable(evidenceColumns, evidenceRows)}
					<NCDiv areaCode={NCDiv.config.BOTTOM}>
						<div className="total-line nc-bill-search-area nc-theme-area-split-bc nc-theme-area-bgc"  id="totalLine" style={{width:tableWidth}}>
							<span className="total-sum nc-theme-common-font-c">
								{self.state.json['20020MANGE-000056']+"："}<span className="total-number">{/* 国际化处理： 合计差额*/}
									{sum(
										evidenceData.localdebitamountTotle ?
										setScale(evidenceData.localdebitamountTotle.value,saveData.paraInfo&&saveData.paraInfo.orgscale?saveData.paraInfo.orgscale:'2') : '',
										-(evidenceData.localcreditamountTotle?
											setScale(evidenceData.localcreditamountTotle.value,saveData.paraInfo&&saveData.paraInfo.orgscale?saveData.paraInfo.orgscale:'2')
											: '')
									)}
								</span>
							</span>
							<span className="total-caps nc-theme-common-font-c">
								{self.state.json['20020MANGE-000057']+"："}<span className="total-caps-count">{this.changemoney(evidenceData)}</span>{/* 国际化处理： 大写合计*/}
							</span>

							<span className="total-item nc-theme-common-font-c">
								{self.state.json['20020MANGE-000058']+"："/* 国际化处理： 组织借方合计*/}
								<span className="total-number">{setScale(evidenceData.localdebitamountTotle.value,saveData.paraInfo&&saveData.paraInfo.orgscale?saveData.paraInfo.orgscale:'2')}</span>
							</span>

							<span className="total-item nc-theme-common-font-c">
								{self.state.json['20020MANGE-000059']+"："/* 国际化处理： 组织贷方合计*/}
								<span className="total-number">{setScale(evidenceData.localcreditamountTotle.value,saveData.paraInfo&&saveData.paraInfo.orgscale?saveData.paraInfo.orgscale:'2')}</span>
							</span>
						</div>
					</NCDiv>
				</div>

				{/* 附件*/}
				{/* 这里是附件上传组件的使用，需要传入三个参数 */}
				<div className="nc-faith-demo-div2">
				{showUploader && (
					<NCUploader
						billId={self.state.checkId}
						//billNo={self.state.checkValue}
						beforeUpload={self.beforeUpload}
						onHide={self.onHide}
					/>
				)}
				</div>
				{self.state.compositedisplay ? (
					<ApprovalTrans
						title={self.state.json['20020MANGE-000161']}/* 国际化处理： 指派*/
						data={self.state.compositedata}
						display={self.state.compositedisplay}
						getResult={self.getAssginUsedr}
						cancel={self.closeAssgin}
					/>
				) : (
					''
				)}
				{/* 审批详情 */}
				{<ApproveDetail 
					show={self.state.showApproveDetail}
					close={self.closeApprove}
					billtype={self.state.transtype}
					billid={self.state.id}
                 />}
			</div>
		);
	}
}
})
VoucherBrowseCard = createPage({
})(VoucherBrowseCard);
export default VoucherBrowseCard;
