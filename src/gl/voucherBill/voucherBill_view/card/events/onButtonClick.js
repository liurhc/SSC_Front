import { ajax, base, toast,cacheTools,print,withNav } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
// @withNav
export default function onButtonClick(props, id) {
    switch (id) {
        case 'OpGroup'://
            break;
        case 'Add'://新增
            this.handleSubmitAdd()
            break;
        case 'Edit'://修改
            this.handleUpdate()
            break;
        case 'Delete'://删除
            this.handleDelete()
            break;    
        case 'Copy'://复制
            this.handleCopy()
            break;
        case 'Commit'://提交
            this.handleSubmit()
            break;
        case 'commit'://提交
            break;
        case 'Uncommit'://收回
            this.handleBack()
            break;
        case 'MadeBill'://制单
            break;
        case 'More'://
            break;    
        case 'RelatedQuery'://
            break;
        case 'LinkAprv'://联查审批信息
            this.openApprove();
            break;
        case 'LinkVoucher'://联查凭证
            this.linkVoucher();
            break;
        case 'BillAssistant'://
            break;
        case 'AttachManage'://附件管理
            this.openFile()
            break;
        case 'Receipt'://
            break;
        case 'ReceiptScan'://影像扫描
            this.receiptScan()
            break;
        case 'ReceiptCheck': //影像查看
            this.receiptCheck()
            break;          
        case 'PrintGroup'://打印
            break;
        case 'Print'://打印
            this.handlePrint()
            break;
        case 'Output'://输出
            this.open()
            break;
        case 'Preview'://预览
            break;
        default:
        break;

    }
}
