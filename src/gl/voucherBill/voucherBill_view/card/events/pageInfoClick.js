import { ajax,cardCache } from 'nc-lightapp-front';
let dataSource = 'gl.voucherBill.voucherBill_list.cache';
let formId = 'head';
let pageId = '20020MANGE_003';
let pkname = 'pk_voucher';
export default function(props, pks, that) {
	if(that == undefined || that == null || that === "null")
	{
		that = this;
	}
	let { getCacheById, updateCache } = cardCache;
    props.setUrlParam(pks);

	if (pks == null) {
		let { pk_accountingbook, evidenceData, saveData, update } = that.state;
		//修改太页面参照设置依赖
		pk_accountingbook.value='';
		evidenceData.rows=[];
		evidenceData.index=0;
		saveData = {};

		evidenceData.localcreditamountTotle.value = ''; //合计值获取
		evidenceData.localdebitamountTotle.value = '';
		evidenceData.groupcreditamountTotle.value = '';
		evidenceData.groupdebitamountTotle.value = '';
		evidenceData.globalcreditamountTotle.value = '';
		evidenceData.globaldebitamountTotle.value = '';

		update = true;
		that.setState({
			pk_accountingbook,
			evidenceData,
			update,
			saveData,
			id:'',
		},()=>{
			//表头表尾赋默认值
			that.props.form.EmptyAllFormValue(formId);
			that.buttonStatus();
		});
		return;
	}
	let cardData = getCacheById(pks, dataSource);

    if(cardData){
        loadPageValue(cardData,props,that);
	}
	else
	{
		let pkArr = [];
		pkArr.push(pks); //后台使用数组解析数据，此处传数组
		let data = {
			pk_voucher:pkArr[0],
			pageid: pageId
		};
		ajax({
			url: '/nccloud/gl/voucher/querybill.do',
			data: data,
			success: (res) => {
				if (res.data) {
					loadPageValue(res.data,props,that);
					updateCache(pkname,pks,res.data,formId,dataSource);
				}
			}
		});
	}
}
function loadPageValue (cardData,props,that) {
	let self = that;
	let { pk_accountingbook, evidenceData, saveData, update, billstatus, transtype } = self.state;
	let voucher=cardData.voucher;
	let {NC001,isShowNum }=cardData.paraInfo;
	self.setState({
		billno:voucher.billno,
		id:voucher.pk_voucher.value
	});
	if(voucher && voucher.details && voucher.details.length > 0){
		voucher.details.map((item, i) => {
			//渲染是否有动态新增列，有待优化
			item.key = ++i;
			if (NC001) {
				item.groupType = NC001;
			}
			if (item.isShowNum && item.isShowNum.value) {
				//根据返回数据判断是否有数量列
				item.flag = true;
			}
			
			if (item.debitamount.value) {
				item.amount = {
					scale:item.debitamount && item.debitamount.scale||item.amount&&item.amount.scale,
					value: item.debitamount && item.debitamount.value||item.amount&&item.amount.value,
					editable: item.debitamount && item.debitamount.editable||item.amount&&item.amount.editable
				};
			} else {
				item.amount = {
					scale:item.creditamount && item.creditamount.scale||item.amount&&item.amount.scale,
					value: item.creditamount && item.creditamount.value||item.amount&&item.amount.value,
					editable: item.creditamount && item.creditamount.editable||item.amount&&item.amount.editable
				};
				item.debitquantity=item.creditquantity
			}
		});
	}else{
		voucher.details = [];
	}
	
	self.props.form.setFormStatus(self.formId, 'browse');
	self.props.form.setFormStatus(self.formIdtail, 'browse');
	//修改太页面参照设置依赖
	pk_accountingbook.value = voucher.pk_accountingbook.value;
	evidenceData.rows = voucher.details;
	evidenceData.index = voucher.details.length;
	saveData = voucher;
	saveData['paraInfo'] = cardData.paraInfo;
	billstatus = voucher.billstatus.value;
	saveData.period.display = voucher.year.value + '-' + voucher.period.value;
	transtype = voucher.pk_tradetypecode.value;
	update = true;

	evidenceData.localcreditamountTotle.value = voucher.totalcredit.value; //合计值获取
	evidenceData.localdebitamountTotle.value = voucher.totaldebit.value;
	evidenceData.groupcreditamountTotle.value = voucher.totalcreditgroup.value;
	evidenceData.groupdebitamountTotle.value = voucher.totaldebitgroup.value;
	evidenceData.globalcreditamountTotle.value = voucher.totalcreditglobal.value;
	evidenceData.globaldebitamountTotle.value = voucher.totaldebitglobal.value;

	self.setState(
		{
			pk_accountingbook,
			billstatus,
			evidenceData,
			update,
			saveData,
			transtype
		},
		() => {
			if(isShowNum){
				self.addNumber();
			}
			//表头表尾赋默认值
			self.props.form.setFormItemsValue(self.formId, saveData);
			//存在单据时调整按钮状态
			self.buttonStatus();
		}
	);
}
