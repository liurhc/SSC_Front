import { createPage, ajax, base, toast,cacheTools,print } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;

export default function (props) {
	let self=this;
	props.createUIDom(
		{
			pagecode: '20020MANGE_002',//卡片浏览态页面编码
			appcode:props.getUrlParam("appcode")//小应用编码 
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					// modifierMeta(props, meta);
					//	meta['head'].status = pageStatus;
					//	meta['tail'].status = pageStatus;
					props.meta.setMeta(meta);
					if(props.getUrlParam('status') != 'browse'){
						//props.cardTable.addRow(tableId);
					}
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
				if(data.context){
					//页面渲染时获取交易类型
					if(data.context.paramMap && data.context.paramMap.transtype){
						self.setState({transtype:data.context.paramMap.transtype});
					} else {
						if(props.getUrlParam("tradetype")){
							self.setState({transtype:props.getUrlParam("tradetype")});
						}
					}
				} else {
					if(props.getUrlParam("tradetype")){
						self.setState({transtype:props.getUrlParam("tradetype")});
					}
				}
			}   
		}
	)
}
