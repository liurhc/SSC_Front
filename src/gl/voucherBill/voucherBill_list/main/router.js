import {asyncComponent} from 'nc-lightapp-front';

const VoucherBillList = asyncComponent(() => import(/* webpackChunkName: "gl/voucherBill/voucherBill_list/list/list" */ /* webpackMode: "eager" */'../list'));
const VoucherEditCard = asyncComponent(() => import(/* webpackChunkName: "gl/voucherBill/voucherBill_insert/card/card" */ /* webpackMode: "eager" */'../../voucherBill_insert/card'));
const VoucherBrowseCard = asyncComponent(() => import(/* webpackChunkName: "gl/voucherBill/voucherBill_view/card/card" */ /* webpackMode: "eager" */'../../voucherBill_view/card'));

const routes = [
  {
    path: '/list',
    component: VoucherBillList,
    exact: true,
  },
  {
    path: '/add',
    component: VoucherEditCard,
  },
  {
    path: '/view',
    component: VoucherBrowseCard,
  }
];

export default routes;
