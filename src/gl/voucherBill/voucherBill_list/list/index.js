
//主子表列表
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, high, toast, cardCache, cacheTools, getMultiLang, promptBox, getBusinessInfo, createPageIcon } from 'nc-lightapp-front';
const { NCBreadcrumb } = base;
const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
const { NCUploader, ExcelImport, ApproveDetail } = high;
import { buttonClick, bodyButtonClick, controlButton, initTemplate, searchBtnClick, pageInfoClick, tableModelConfirm } from './events';
import ApprovalTrans from '../../../../uap/public/excomponents/approvalTrans';
import './index.less';
import HeaderArea from '../../../public/components/HeaderArea';
let { setDefData, getDefData } = cardCache;
class VoucherBillList extends Component {
	constructor(props) {
		super(props);
		this.moduleId = '2052';
		this.searchId = '200210VCR';
		this.tableId = 'details';
		this.state = {
			json: {},
			currentLocale: 'zh-CN',
			buttonDate: [],
			showUploader: false,
			checkValue: null,
			checkId: null,
			selectedPKS: [],//导出数据的主键pk 
			pageCode: '20020MANGE_001',//页面编码
			appCode: this.props.getSearchParam("c"),//小应用编码
			compositedata: null, //指派相关
			compositedisplay: null, //指派相关
			submitData: null,//指派时提交数据
			transtype: 'VRD1',
			selectIndex: '',
			showApproveDetail: false,//审批详情的控制
			id: null,
			pk_accasoa: null,
			pk_accountingbook: null,
			pk_org: null,
			begindate: '',
			enddate: '',
			periodDateArr: []
		};
		//initTemplate.call(this,this.props);
		this.dataSource = 'gl.voucherBill.voucherBill_list.cache';
		this.pkname = "pk_voucher";
		// this.state = { currentLocale: 'zh-CN',buttonDate: [] };
	}

	//导出格式化文件
	export = () => {
		let rows = this.props.table.getCheckedRows('details');
		let pk_bills = [];
		rows.forEach((ele, index) => {
			let pk = ele.data.values.pk_voucher;
			pk_bills.push(pk.value);
		});
		if (pk_bills.length == 0) {
			toast({ content: this.state.json['20020MANGE-000148'], color: 'warning' });/* 国际化处理： 未选中任何行，不允许导出操作！*/
			return;
		}
		this.setState({
			selectedPKS: pk_bills  //传递主键数组,之前nc需要导出的加主键
		}, () => {
			this.props.modal.show('exportFileModal');//不需要导出的只执行这行代码
		})
	}

	completeEvent() {
		let searchData = cacheTools.get('voucherBillSearchData');
		if (searchData && searchData.querycondition) {
			for (let item of searchData.querycondition.conditions) {
				if (item.field == 'billdate' || item.field == 'period') {
					// 时间类型特殊处理
					let time = [];
					time.push(item.value.firstvalue);
					time.push(item.value.secondvalue);
					this.props.search.setSearchValByField(this.searchId, item.field, {
						display: item.display,
						value: time
					});
				} else if (item.field == 'pk_org') {
					let pkOrg = item.value.firstvalue.split(",")[0];
					//根据所选组织查询账簿
					ajax({
						url: '/nccloud/gl/voucher/queryaccountingbookbyorg.do',
						data: { pk_org: pkOrg },
						async: false,
						success: (res) => {
							let { success, data } = res;
							let meta = this.props.meta.getMeta();
							if (success && data != undefined && JSON.stringify(data) != '{}') {
								meta[this.searchId].items.map((item) => {
									if (item.attrcode == 'details.pk_accasoa') {
										item.isAccountRefer = true;
										item.isShowHighFilter = false;
										item.onlyLeafCanSelect = true;
										item.isShowDisabledData = false;
										item.queryCondition = () => {
											return { pk_accountingbook: data.accountingbook.value };
										};

									} else if (item.attrcode == 'details.assid') {
										item.queryCondition = () => {
											return {
												pk_accasoa: null,
												pk_accountingbook: data.accountingbook.value,
												pk_org: pkOrg
											};
										};
									} else if (item.attrcode == "period") {
										let url1 = '/nccloud/gl/voucher/queryCombineInfoByOrgId.do';
										let pk_accpont = {
											"pk_org": pkOrg,
											"date": getBusinessInfo().businessDate,
											"pk_accountingbook": data.accountingbook.value
										}
										ajax({
											url: url1,
											data: pk_accpont,
											async: false,
											success: (res) => {
												let { success, data } = res;
												if (data && data.pk_accperiodscheme) {
													item.queryCondition = () => {
														return {
															pk_accperiodscheme: data.pk_accperiodscheme
														};
													};
												}
											}
										});
									}
									this.setState({
										pk_org: pkOrg,
										pk_accountingbook: data.accountingbook.value
									})
								});
								this.props.meta.setMeta(meta);
							} else if (JSON.stringify(data) == '{}') {
								// 当没有账簿时!!!
								meta[this.searchId].items.map((item) => {
									if (item.attrcode == 'details.assid') {
										item.queryCondition = () => {
											return {
												pk_accasoa: null,
												pk_accountingbook: null,
												pk_org: null
											};
										};
									} else if (item.attrcode == "period") {
										item.queryCondition = () => {
											return {
												pk_accperiodscheme: ""
											};
										};
									}
								});
							}
						}
					});
					this.props.search.setSearchValByField(this.searchId, item.field, {
						display: item.display,
						value: item.value.firstvalue
					});
				} else {
					this.props.search.setSearchValByField(this.searchId, item.field, {
						display: item.display,
						value: item.value.firstvalue
					});
				}
			}
			//this.props.search.setSearchValue(this.searchId, searchData.querycondition);
			this.props.search.setDisabledByField(this.searchId, 'details.pk_accasoa', false);
		}
	}

	componentWillMount() {
		let callback = (json) => {
			this.setState({ json: json }, () => {
				initTemplate.call(this, this.props);
			})
		}
		getMultiLang({ moduleId: ['20020MANGE', 'publiccomponents'], currentLocale: 'simpchn', domainName: 'gl', callback });
	}
	componentDidMount() {
		let src = this.props.getUrlParam('scene');
		if (src) {
			if (src == 'fip') {//会计平台联查
				//获取缓存中数据
				let checkedData = cacheTools.get('checkedData');
				let data = {
					pageid: '20020MANGE_001',
					fipLink: checkedData
				}
				if (checkedData) {
					ajax({
						url: '/nccloud/gl/voucherbill/linkBillQuery.do',
						data: data,
						success: (res) => {
							let { success, data } = res;
							if (success && data != undefined) {
								this.props.table.setAllTableData(this.tableId, data[this.tableId]);

							} else {
								this.props.table.setAllTableData(this.tableId, { rows: [] });
							}
						}
					});
				}
			}
		} else {
			let { hasCacheData } = this.props.table;
			//从缓存中取数据
			if (hasCacheData(this.dataSource)) { }
		}
	}

	//提交
	submitData = (submitData, isHeadBtn) => {
		ajax({
			url: '/nccloud/gl/voucher/sendApprovebills.do',
			data: submitData,
			success: (response) => {
				let { success, data } = response;
				if (success) {
					if (response.data.workflow && (response.data.workflow == 'approveflow' || response.data.workflow == 'workflow')) {
						this.setState({
							compositedata: response.data,
							compositedisplay: true,
							submitData: submitData
						});
					} else {
						if (data.grid) {
							let grid = data.grid;
							let updateValue = [];
							for (let key in grid) {
								updateValue.push({ index: key, data: { values: grid[key].values } });
							}
							this.props.table.updateDataByIndexs(this.tableId, updateValue);
						}
						controlButton(this.props, this.tableId);
						if (data.message) {
							toast({
								duration: 'infinity',
								color: data.PopupWindowStyle,
								content: data.message
							});
						}
					}
				}
			}
		});
	}

	//指派
	getAssginUsedr = (value) => {
		let url = '/nccloud/gl/voucher/sendApprovebills.do';
		let saveData = this.state.submitData;
		saveData[0].assignObj = value;
		let self = this;
		ajax({
			url: url,
			data: saveData,
			success: function (response) {
				let { success, data } = response;
				if (success) {
					if (data.grid) {
						let grid = data.grid;
						let updateValue = [];
						for (let key in grid) {
							updateValue.push({ index: key, data: { values: grid[key].values } });
						}
						self.props.table.updateDataByIndexs(self.tableId, updateValue);
					}
					controlButton(self.props, self.tableId);
					toast({ color: 'success', content: self.state.json['20020MANGE-000080'] });/* 国际化处理： 操作成功*/
					self.closeAssgin();
				}
			}
		});
	};

	//指派关闭
	closeAssgin = () => {
		if (this.state.compositedisplay) {
			this.setState({ compositedisplay: false });
		}
	}

	getButtonNames(codeId) {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button'
		} else {
			return 'secondary - button'
		}
	};

	setDefaultData = () => {
		let data = {
			pk_tradetypecode: 'VRD1'
		};
		let resdata;
		ajax({
			url: '/nccloud/gl/voucher/queryPageDefaultData.do',
			data: data,
			async: false,
			success: (res) => {
				let { success, data } = res;
				if (success && data != undefined) {
					this.props.search.setSearchValByField(this.searchId, 'pk_tradetypeid', { value: data.tradetype.value, display: data.tradetype.display });
				}
				resdata = res;
			}
		});
		return resdata;
	};

	beforeUpload(billId, fullPath, file, fileList) {
		const isLt20M = file.size / 1024 / 1024 < 20;
		if (!isLt20M) {
			toast({ content: this.state.json['20020MANGE-000149'], color: 'warning' });/* 国际化处理： 上传大小小于20M！*/
		}
		return isLt20M;
		// 备注： return false 不执行上传  return true 执行上传
	}
	//关闭
	onHide = () => {
		this.setState({
			showUploader: false
		})
	}
	//附件
	openfile = (props) => {
		let rows = props.table.getCheckedRows('details');
		let index1 = [];
		let pk;
		let vbillcode;
		rows.forEach((ele, index) => {
			pk = ele.data.values.pk_voucher;
			vbillcode = ele.data.values.billno;
			index1.push(pk.value);
		});
		if (index1.length == 0) {
			toast({ content: this.state.json['20020MANGE-000082'], color: 'warning' });/* 国际化处理： 请选择单据！*/
			return;
		}
		if (index1.length > 1) {
			toast({ content: this.state.json['20020MANGE-000083'], color: 'warning' });/* 国际化处理： 单据不能多选！*/
			return;
		}
		if (this.state.showUploader == false) {
			this.setState({ 
				showUploader: true,
				checkId: pk.value,
			checkValue: vbillcode.value
			 })
		} else {
			this.setState({ 
				showUploader: false,
				checkId: pk.value,
			checkValue: vbillcode.value
			 })
		}

		// this.setState({
		// 	checkId: pk.value,
		// 	checkValue: vbillcode.value
		// })

	}
	/* 高级清空按钮 */
	advSearchClearEve() {
		this.props.search.setSearchValByField(this.searchId, 'details.assid', {});
		let meta = this.props.meta.getMeta();
		meta[this.searchId].items.map((item) => {
			if (item.attrcode == 'details.assid') {
				item.queryCondition = () => {
					return {
						pk_accasoa: null,
						pk_accountingbook: null,
						pk_org: null
					};
				};
				window.localCheckedArray = [];
				window.localAssData = [];
				window.localPk_accountingbook = '';
			}
		})
	}
	/* 查询区编辑后事件 */
	onAfterEvent(field, val, id, item, index) {
		let self = this;
		if (field == 'pk_org') {//选择组织后事件
			if (val && val.length > 0) {
				this.props.search.setDisabledByField(this.searchId, 'details.pk_accasoa', false);
				this.props.search.setSearchValByField(this.searchId, 'details.pk_accasoa', {});
				this.props.search.setSearchValByField(this.searchId, 'details.assid', {});
				//根据所选组织查询账簿
				ajax({
					url: '/nccloud/gl/voucher/queryaccountingbookbyorg.do',
					data: { pk_org: val[0].refpk },
					async: false,
					success: (res) => {
						let { success, data } = res;
						let businessInfo = getBusinessInfo();
						let buziDate = businessInfo.businessDate;
						if (buziDate) {
							buziDate = buziDate.split(' ')[0];
						}
						if (success && data != undefined && JSON.stringify(data) != '{}') {
							let meta = this.props.meta.getMeta();
							meta[this.searchId].items.map((item) => {
								if (item.attrcode == 'details.pk_accasoa') {
									item.isAccountRefer = true;
									item.isShowHighFilter = false;
									item.onlyLeafCanSelect = true;
									item.isShowDisabledData = false;
									item.queryCondition = () => {
										return {
											pk_accountingbook: data.accountingbook.value,
											dateStr: buziDate,
											isDataPowerEnable: 'Y',
											DataPowerOperationCode: 'fi'
										};
									};
								} else if (item.attrcode == 'details.assid') {
									item.queryCondition = () => {
										return {
											pk_accasoa: null,
											pk_org: val[0].refpk,
											pk_accountingbook: data.accountingbook.value
										};
									};
									window.localCheckedArray = [];
									window.localAssData = [];
									window.localPk_accountingbook = '';
								} else if (item.attrcode == "period") {
									let url1 = '/nccloud/gl/voucher/queryCombineInfoByOrgId.do';
									let pk_accpont = {
										"pk_org": val[0].refpk,
										"date": getBusinessInfo().businessDate,
										"pk_accountingbook": data.accountingbook.value
									}
									ajax({
										url: url1,
										data: pk_accpont,
										async: false,
										success: (res) => {
											let { success, data } = res;
											if (data && data.pk_accperiodscheme) {
												item.queryCondition = () => {
													return {
														pk_accperiodscheme: data.pk_accperiodscheme
													};
												};
											}
										}
									});
								}
							});
							self.setState({
								pk_org: val[0].refpk,
								pk_accountingbook: data.accountingbook.value
							})
							this.props.meta.setMeta(meta);
						} else {

						}
					}
				});
			} else {
				self.setState({
					pk_accountingbook: null
				}, () => {
					let meta = this.props.meta.getMeta();
					meta[this.searchId].items.map((item) => {
						if (item.attrcode == 'details.assid') {
							item.queryCondition = () => {
								return {
									pk_accasoa: null,
									pk_accountingbook: null,
									pk_org: null
								};
							};
						} else if (item.attrcode == "period") {
							item.queryCondition = () => {
								return {
									pk_accperiodscheme: ""
								};
							};
						}
					});
					this.props.meta.setMeta(meta);
					this.props.search.setDisabledByField(this.searchId, 'details.pk_accasoa', true);
					this.props.search.setSearchValByField(this.searchId, 'details.pk_accasoa', {});
					this.props.search.setSearchValByField(this.searchId, 'details.assid', {});
				})
			}
		} else if (field == 'details.pk_accasoa') {
			if (val && val.refpk && val.refcode) {
				let meta = this.props.meta.getMeta();
				let businessInfo = getBusinessInfo();
				let currrentDate = businessInfo.businessDate.split(' ')[0];
				meta[this.searchId].items.map((item) => {
					if (item.attrcode == 'details.assid') {
						item.queryCondition = () => {
							return {
								pk_accountingbook: self.state.pk_accountingbook,
								pk_org: self.state.pk_org,
								pk_accasoa: val.refpk,
								prepareddate: currrentDate,
								assData: [],
								assid: null
							};
						};
					}
				});
				this.props.meta.setMeta(meta);
				this.props.search.setSearchValByField(this.searchId, 'details.assid', {});
			} else {
				let meta = this.props.meta.getMeta();
				meta[this.searchId].items.map((item) => {
					if (item.attrcode == 'details.assid') {
						item.queryCondition = () => {
							return {
								pk_accountingbook: self.state.pk_accountingbook,
								pk_org: self.state.pk_org,
								pk_accasoa: null
							};
						};
					}
				});
				this.props.meta.setMeta(meta);
				this.props.search.setSearchValByField(this.searchId, 'details.assid', {});
			}
		} else if (field == 'period') {
			let { begindate, enddate, periodDateArr } = self.state;
			let billdatedisplay = '';
			if (val && val.refpk && val.refcode) {
				if (periodDateArr.length > 0) {
					periodDateArr.forEach((item, index_) => {
						if (item.index == index) {
							periodDateArr.splice(index_, 1);
						}
					})
				}
				val.index = index;
				periodDateArr.push(val);
				// 只选择一个期间时
				if (periodDateArr.length == 1) {
					begindate = val.values.begindate.value;
					enddate = val.values.enddate.value;
					this.props.search.setSearchValByField(this.searchId, 'period', { value: [val.refname, val.refname], display: [val.refname, val.refname] });
				} else {
					let date0 = '', date1 = '';
					periodDateArr.forEach((item) => {
						if (item.index == '0') {
							date0 = item;
						} else if (item.index == '1') {
							date1 = item;
						}
					})
					// 第一个大于第二个
					if (self.dateCompare(date0.refname, date1.refname)) {
						begindate = val.values.begindate.value;
						enddate = val.values.enddate.value;
						periodDateArr.forEach((item, index_) => {
							if (item.index != index) {
								periodDateArr.splice(index_, 1);
							}
						})
						this.props.search.setSearchValByField(this.searchId, 'period', { value: [val.refname, val.refname], display: [val.refname, val.refname] });
					} else {
						begindate = date0.values.begindate.value;
						enddate = date1.values.enddate.value;
					}
				}
				billdatedisplay = begindate + '~' + enddate;
				this.props.search.setSearchValByField(this.searchId, 'billdate', { value: [begindate + " 00:00:00", enddate + " 23:59:59"], display: billdatedisplay });
				self.setState({
					begindate,
					enddate,
					periodDateArr
				});
			} else {
				periodDateArr.forEach((item, index_) => {
					if (item.index === index && JSON.stringify(val) == '{}') {
						periodDateArr.splice(index_, 1);
					}
				})
				if (periodDateArr.length > 0) {
					begindate = periodDateArr[0].values.begindate.value;
					enddate = periodDateArr[0].values.enddate.value;
				}
				billdatedisplay = begindate + '~' + enddate;
				this.props.search.setSearchValByField(this.searchId, 'billdate', { value: [begindate + " 00:00:00", enddate + " 23:59:59"], display: billdatedisplay });
				self.setState({
					begindate,
					enddate,
					periodDateArr
				});
			}
		}

	}
	/*比较日期大小 */
	dateCompare(date0, date1) {
		var oDate1 = new Date(date0);
		var oDate2 = new Date(date1);
		if (oDate1.getTime() > oDate2.getTime()) {
			return true;
		} else {
			return false;
		}
	}
	updateButtonStatus() {
		let checkedRows = this.props.table.getCheckedRows(this.tableId);
		if (checkedRows) {
			let dataArr = [];
			checkedRows.forEach((val) => {
				if (val.data.values.billstatus.value != '1') {
					dataArr.push(val.data.values.billstatus.value);
				}
			});
			if (dataArr.length > 0) {
				this.props.button.setButtonDisabled({
					Delete: true
				});
			}
		}
	}

	onSelectedFn = (props, moduleId, record, index, status) => {
		controlButton(props, this.tableId);
		this.setState({
			selectIndex: index
		});
	};

	onRowDblClick = (record) => {
		this.props.pushTo('/view', {
			status: 'browse',
			bill_id: record.pk_voucher.value,
			appcode: this.props.getSearchParam("c") //获取小应用编码
		});
	}

	onSelected() {
		let checkedRows = this.props.table.getCheckedRows(this.tableId);
		if (checkedRows) {
			let dataArr = [];
			checkedRows.forEach((val) => {
				if (val.data.values.billstatus.value != '1') {
					dataArr.push(val.data.values.billstatus.value);
				}
			});
			if (dataArr.length > 0) {
				this.props.button.setButtonDisabled({
					Delete: true
				});
			} else {
				this.props.button.setButtonDisabled({
					Delete: false
				});
			}
		}
	}
	//关闭审批详情模态框
	closeApprove = () => {
		this.setState({
			showApproveDetail: false
		})
	}

	render() {
		let { table, button, search, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let { showUploader, buttonDate, selectIndex } = this.state;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtonNames, createButtonApp } = button;
		let { createModal } = modal;
		return (<div className="nc-bill-list" id="ncbilllist">
			{createModal('tip')}
			<HeaderArea
				title={this.state.json['20020MANGE-000150']/* 国际化处理： 凭证单列表*/}
				btnContent={
					this.props.button.createButtonApp({
						area: 'list_head',
						buttonLimit: 8,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})
				}
			/>
			{createModal('importModal', {
				noFooter: true,
				className: 'import-modal'
			})}
			{/* 附件*/}
			{showUploader && <NCUploader
				billId={this.state.checkId}
				billNo={this.state.checkValue}
				beforeUpload={this.beforeUpload}
				onHide={this.onHide}
			/>}
			<div className="nc-bill-search-area">
				{NCCreateSearch(this.searchId, {
					clickSearchBtn: searchBtnClick.bind(this),
					onAfterEvent: this.onAfterEvent.bind(this),
					renderCompleteEvent: this.completeEvent.bind(this),
					advSearchClearEve: this.advSearchClearEve.bind(this)
				})}
			</div>
			<div className="nc-bill-table-area">
				{createSimpleTable(this.tableId, {
					handlePageInfoChange: pageInfoClick,
					tableModelConfirm: tableModelConfirm,
					onSelected: this.onSelectedFn.bind(this),
					onSelectedAll: controlButton.bind(this, this.props, this.tableId),
					onRowDoubleClick: this.onRowDblClick.bind(this),
					showCheck: true,
					showIndex: true,
					dataSource: this.dataSource,
					pkname: this.pkname
				})}
			</div>

			{<div className='header-button-area'>
				<ExcelImport
					{...Object.assign(this.props)}
					moduleName='gl'//模块名
					billType='VR01'//单据类型
					selectedPKS={this.state.selectedPKS}
					appcode={this.props.getSearchParam("c")}
					pagecode='20020MANGE_004'
				//referVO={{ignoreTemplate:true}}
				/>
			</div>}

			{this.state.compositedisplay ? (
				<ApprovalTrans
					title={this.state.json['20020MANGE-000161']}/* 国际化处理： 指派*/
					data={this.state.compositedata}
					display={this.state.compositedisplay}
					getResult={this.getAssginUsedr}
					cancel={this.closeAssgin}
				/>
			) : (
					''
				)}
			{<ApproveDetail
				show={this.state.showApproveDetail}
				close={this.closeApprove}
				billtype={this.state.transtype}
				billid={this.state.id}
			/>}
		</div>
		);
	}
}

VoucherBillList = createPage({
	//initTemplate: initTemplate
})(VoucherBillList);
export default VoucherBillList;
//ReactDOM.render(<VoucherBillList />, document.querySelector('#app'));
