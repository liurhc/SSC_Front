import { ajax, cacheTools, cardCache, toast } from 'nc-lightapp-front';

let { setDefData, getDefData } = cardCache;
let tableId = 'details';
let pkname = "pk_voucher";
let dataSource = 'gl.voucherBill.voucherBill_list.cache';
export default function(props,that) {
    let queryInfo = cacheTools.get('voucherBillSearchData');
    if(!queryInfo){
        return;
    }
    let pageInfo = props.table.getTablePageInfo(tableId);
    //辅助核算特殊处理
    let ass_value = null;
    let conditions = [];
    if(queryInfo.querycondition && queryInfo.querycondition.conditions && queryInfo.querycondition.conditions.length > 1){
        queryInfo.querycondition.conditions.forEach((item) => {
            if(item.field == 'details.assid'){
                if(item.value.firstvalue && item.value.firstvalue != "[]"){
                    ass_value = item.value.firstvalue;
                }
            }else if(item.field == 'period') {//会计期间与单据日期联动,只做前台展示,不做条件查询,单据日期作为条件进行查询

            }else {
                conditions.push(item);
            }
        });
    }
    if(conditions.length > 0){
        queryInfo.querycondition.conditions = conditions;
    }
    queryInfo.pageInfo = pageInfo;
    let data={
        queryInfo:queryInfo,
        pagecode: props.getSearchParam("p"),
        assid: ass_value
    };

    ajax({
        url: '/nccloud/gl/voucherbill/queryAll.do',//'/nccloud/reva/search/query.do',
        data: data,
        success:  (res)=> {
            let { success, data } = res;
				if (success && data != undefined) {
                    toast({content: that.state.json['20020MANGE-000182'],color:'success'});
                    setDefData(pkname, dataSource, data[tableId]);
					props.table.setAllTableData(tableId, data[tableId]);
				}else{
					props.table.setAllTableData(tableId, {rows:[]});
				}
        }
    });
}
