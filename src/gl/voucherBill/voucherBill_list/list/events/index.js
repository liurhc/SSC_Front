import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import bodyButtonClick from './bodyButtonClick';
import controlButton from './controlButton';
export { searchBtnClick, pageInfoClick,initTemplate,buttonClick,bodyButtonClick,controlButton };
