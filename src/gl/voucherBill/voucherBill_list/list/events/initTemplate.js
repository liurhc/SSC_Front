import { createPage, ajax, base, excelImportconfig,getBusinessInfo } from 'nc-lightapp-front';
import bodyButtonClick from './bodyButtonClick.js';
import Utils from '../../../../../uap/public/utils';
import controlButton from './controlButton';
let { NCPopconfirm, NCIcon } = base;

let searchId = '200210VCR';
let tableId = 'details';
let refPath = '../../../../uapbd/refer/org/FinanceOrgTreeRef/index.js'

export default function(props) {
	let self=this;
	let toastArr = [];
	let appcode = props.getSearchParam('c') ? props.getSearchParam('c') : props.getUrlParam('c');
	let excelimportconfig = excelImportconfig(props, "gl", "VR01", true, "",{"appcode":appcode,"pagecode":"20020MANGE_004"});
	props.createUIDom(
		{
			pagecode: props.getSearchParam("p"), //页面编码
			appcode:props.getSearchParam("c")    //小应用编码
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(self,props, meta, data);
					props.meta.setMeta(meta);
					
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('Delete_inner',self.state.json['20020MANGE-000159']) /* 国际化处理： 确认删除该信息吗？*//* 设置操作列上删除按钮的弹窗提示 */
					controlButton(props, tableId);
				}
				if(data.context){
					//页面渲染时获取交易类型
					self.setState({transtype:data.context.paramMap.transtype});
					if(data.context.pk_org){
						let pkOrg = data.context.pk_org;
						let orgName = data.context.org_Name;
						props.search.setSearchValByField(searchId, 'pk_org',{value:pkOrg,display:orgName});
						props.search.setSearchValByField(searchId, 'pk_org',{value:pkOrg,display:orgName},'normal');
						props.search.setSearchValByField(searchId, 'pk_org',{value:pkOrg,display:orgName},'super');
						props.search.setDisabledByField(searchId,'details.pk_accasoa',false);
						props.search.setSearchValByField(searchId,'details.pk_accasoa',{});
						//根据所选组织查询账簿
						ajax({
							url: '/nccloud/gl/voucher/queryaccountingbookbyorg.do',
							data: {pk_org:pkOrg},
							async: false,
							success: (res) => {
								let { success, data } = res;
								if (success && data != undefined && JSON.stringify(data) != '{}') {
									let meta = props.meta.getMeta();
									meta[searchId].items.map((item) => {
										if (item.attrcode == 'details.pk_accasoa') {
											item.isAccountRefer = true;
											item.isShowHighFilter = false;
											item.onlyLeafCanSelect = true;
											item.isShowDisabledData = false;
											item.queryCondition = () => {
												return {pk_accountingbook:data.accountingbook.value};
											};
											
										} else if (item.attrcode == 'details.assid') {
											item.queryCondition = () => {
												return {
													pk_accasoa: null,
													pk_accountingbook: data.accountingbook.value,
													pk_org:pkOrg
												};
											};
										} else if (item.attrcode == "period"){
											let url1 = '/nccloud/gl/voucher/queryCombineInfoByOrgId.do';
											let pk_accpont = {
												"pk_org": pkOrg,
												"date": getBusinessInfo().businessDate,
												"pk_accountingbook":data.accountingbook.value	
											}
											ajax({
												url: url1,
												data: pk_accpont,
												async: false,
												success: (res) => {
													let { success, data } = res;
													if(data && data.pk_accperiodscheme){
														item.queryCondition = () => {
															return {
																pk_accperiodscheme: data.pk_accperiodscheme
															};
														};
													}
												}
											});
										}
										self.setState({
											pk_org:pkOrg,
											pk_accountingbook: data.accountingbook.value
										})
									});
									props.meta.setMeta(meta);
								}else if( JSON.stringify(data) == '{}'){
									// 当没有账簿时!!!
									meta[searchId].items.map((item) => {
										if (item.attrcode == 'details.assid') {
											item.queryCondition = () => {
												return {
													pk_accasoa: null,
													pk_accountingbook: null,
													pk_org:null
												};
											};
										} else if (item.attrcode == "period"){
											item.queryCondition = () => {
												return {
													pk_accperiodscheme: ""
												};
											};
										}
									});	
								}
							}
						});
					}
				}
			}
		}
	)
	props.button.setUploadConfig("ImportData",excelimportconfig);
}

function modifierMeta(self,props, meta, data) {
	//修改列渲染样式
	// meta[searchId].items.map((ele) => {
	// 	ele.visible = true;
	// });

	meta[tableId].showindex = true;
    meta[tableId].status = 'browse';
    meta[tableId].pagination = true;
    //添加操作列
    meta[tableId].items.push({
        attrcode: 'opr',
        label: self.state.json['20020MANGE-000144'],/* 国际化处理： 操作*/
        itemtype:'customer',
        width: 200,
        fixed: 'right',
        className: 'table-opr',
        visible: true,
        render: (text, record, index) => {
			let buttonAry = [];
			if(record.billstatus.value == '1'){
				buttonAry = ["Edit_inner","Delete_inner","Copy_inner","Commit_inner"];
			} else {
				buttonAry = ["Copy_inner","Uncommit_inner", 'LinkAprv'];
			}
            
            return props.button.createOprationButton(buttonAry, {
                area: "list_inner",
                buttonLimit: 3,
				onButtonClick: (props, key) => bodyButtonClick(props, key, text, record, index, self)
            });
        }
    });
    
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		// if(item.attrcode != 'opr'){
		// 	item.width = 150;
		// }
		//列表数据切换到卡片浏览态处理逻辑
		if (item.attrcode == 'billno') {
			//item.width = 200;
			item.render = (text, record, index) => {
				return (
					<a
						style={{ textDecoration: 'none', cursor: 'pointer'}}
						//href={'#'}
						onClick={() => {
							props.pushTo('/view',{
								status:'browse',
								bill_id:record.pk_voucher.value,
								appcode:props.getSearchParam("c"), //获取小应用编码
								pagecode:'20020MANGE_002'
							});
						}}
					>{record.billno && record.billno.value}</a>
				);
			};
		}
		return item;
	});

	meta[searchId].items =meta[searchId].items.map((item, key) => {
		if(item.itemtype == 'refer'){
			//item.isMultiSelectedEnabled = true;
			item.queryCondition = () => {
				let org = getFirstOrgValue((props.search.getSearchValByField(searchId, 'pk_org') || {}).value.firstvalue);
				return {
					pk_org:org,
					isDataPowerEnable:true,//是否启用数据权限
					DataPowerOperationCode: 'fi'//使用权组
				};
			}
		}
		if (item.attrcode == 'pk_org') {
			item.isMultiSelectedEnabled = true;
			item.queryCondition = () => {
				return {
					isDataPowerEnable:true,//是否启用数据权限
					DataPowerOperationCode: 'fi',//使用权组
					AppCode: props.getSearchParam('c'),
					TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
				};
			}
		} else if(item.attrcode == 'pk_vouchertype'){
			item.isMultiSelectedEnabled = true;
			item.queryCondition = () => {
				return {
					GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
					isDataPowerEnable: 'Y',
					DataPowerOperationCode: 'fi',
					pk_org: self.state.pk_accountingbook
				};
			}
		}else if(item.attrcode == 'pk_tradetypeid'){
			item.queryCondition = () => {
				return { parentbilltype: 'VR01' };
			};
		}
		return item;
	});
	return meta;
}

//获取选中的业务组织的第一个
function getFirstOrgValue(orgValues) {
    let pkOrgValue = '';
    if (orgValues != null) {
        let orgArray = orgValues.split(',');
        if (orgArray != null && orgArray.length > 0) {
            pkOrgValue = orgArray[0];
        }
    }
    return pkOrgValue;
}
