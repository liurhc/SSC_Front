import {ajax,toast,cardCache,cacheTools} from 'nc-lightapp-front';
let {setDefData, getDefData,getCurrentLastId } = cardCache;
//点击查询，获取查询区数据

let searchId = '200210VCR';
let tableId = 'details';
let pkname = "pk_voucher";
let dataSource = 'gl.voucherBill.voucherBill_list.cache';
export default function clickSearchBtn(props,searchVal) {
    // if(!searchVal){
    //     return;
    // }
    // let flag = false;
    // searchVal.conditions.forEach((val) => {
    //     if(val.field == 'period'){
    //         val.value.firstvalue = val.display;
    //     }
    //     if(val.field == 'billdate'){
    //         flag = true;
    //     }
    // });
    // if (flag) { 
    //     searchVal.conditions.forEach((val, index) => {
    //         if(val.field == 'period'){  
    //             searchVal.conditions.splice(index, 1);
    //         }
    //     });
    //     flag = false;
    // }

    // if (flag) { 
    //     searchVal.conditions.forEach((val, index) => {
    //         if(val.field == 'period'){  
    //             searchVal.conditions.splice(index, 1);
    //         }
    //     });
    //     flag = false;
    // }
    
    let pageInfo = props.table.getTablePageInfo(tableId);
    let queryInfo = props.search.getQueryInfo(searchId);
    if(!queryInfo) {
        return;
    }
    cacheTools.set("voucherBillSearchData",queryInfo);
    //辅助核算特殊处理
    let ass_value = null;
    let conditions = [];
    if(queryInfo.querycondition && queryInfo.querycondition.conditions && queryInfo.querycondition.conditions.length > 1){
        queryInfo.querycondition.conditions.forEach((item) => {
            if(item.field == 'details.assid'){
                if(item.value.firstvalue && item.value.firstvalue != "[]"){
                    ass_value = item.value.firstvalue;
                }
            }else if(item.field == 'period') {//会计期间与单据日期联动,只做前台展示,不做条件查询,单据日期作为条件进行查询

            }else {
                conditions.push(item);
            }
        });
    }
    if(conditions.length > 0){
        queryInfo.querycondition.conditions = conditions;
    }
    queryInfo.pageInfo = pageInfo;
    let data={
        queryInfo:queryInfo,
        pagecode: props.getSearchParam("p"),
        assid: ass_value
    };

    ajax({
        url: '/nccloud/gl/voucherbill/queryAll.do',//'/nccloud/reva/search/query.do',
        data: data,
        success:  (res)=> {
            let { success, data } = res;
				if (success && data != undefined) {
                    toast({content: this.state.json['20020MANGE-000145']+data[tableId].allpks.length+this.state.json['20020MANGE-000146'],color:'success'});/* 国际化处理： 查询成功，共,条*/
                    props.table.setAllTableData(tableId, data[tableId]);
                    setDefData(tableId, dataSource, data);
				}else{
                    toast({content: this.state.json['20020MANGE-000147'],color:'warning'});/* 国际化处理： 未查询出符合条件的数据*/
					props.table.setAllTableData(tableId, {rows:[]});
				}
        }
    });

};
