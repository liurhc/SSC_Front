export default function controlButton(props,tableId) {
    //没有勾选数据时处理
    let rows = props.table.getAllTableData(tableId).rows;
    props.button.setButtonDisabled(['Delete', 'commit','uncomit','AttachManage','LinkVoucher','ExportData','Print','Output', 'ReceiptCheck','ReceiptScan'], true);
    props.button.setButtonDisabled(['Add', 'ImportData'], false);
    
    //当勾选选择数据后处理
    let selectRows=props.table.getCheckedRows(tableId);
    if(!selectRows||selectRows.length==0){
        props.button.setButtonDisabled(['Delete', 'Commit','Uncommit','AttachManage','LinkVoucher','ExportData','Print','Output', 'ReceiptCheck','ReceiptScan'], true);
    }else if(selectRows.length==1){
        props.button.setButtonDisabled(['ImportData','ExportData','Print','Output', 'AttachManage','ReceiptCheck','ReceiptScan'], false);
        let record=selectRows[0].data.values;
        let buttonArray=[];
        let billstatus = record.billstatus.value;
        let approvestatus = record.approvestatus.value;
        if(approvestatus == 1 || approvestatus == 2 || approvestatus == 3){
            buttonArray.push('Uncommit');
        }
        if(billstatus == 1){
            buttonArray.push('Delete');
            buttonArray.push('Commit');
        }else if(billstatus == 3){
            buttonArray.push('LinkVoucher');
        }
        props.button.setButtonDisabled(buttonArray, false);
    }else{
        props.button.setButtonDisabled(['Delete', 'Commit','Uncommit','AttachManage','LinkVoucher','ImportData','ExportData','Print','Output', 'ReceiptCheck','ReceiptScan'], false);
    }
}
