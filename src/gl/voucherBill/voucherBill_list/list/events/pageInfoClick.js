import {ajax} from 'nc-lightapp-front';

let searchId = '200210VCR';
let tableId = 'details';
let pageId = '20020MANGE_001';
export default function (props,config,pks) {
    let data = {
        "allpks": pks,
        "pageid": pageId
    };
    ajax({
        url:'/nccloud/gl/voucherbill/queryPage.do',
        data: data,
        success: function (res) {
            let {success,data} = res;
            if(success)
            {
                if(data)
                {
                    props.table.setAllTableData(tableId, res.data[tableId]);
                }
                else
                {
                    props.table.setAllTableData(tableId, {rows:[]});
                }
            }
        }
    });
}
