import { high, base, ajax, print, toast, output, cacheTools, promptBox, cardCache } from 'nc-lightapp-front';
import Utils from '../../../../../uap/public/utils';
import refresh from './refresh';
import {imageScan, imageView} from "../../../../../sscrp/public/common/components/sscImageMng";
import controlButton from './controlButton';
let {setDefData, getDefData,getCurrentLastId } = cardCache;
let tableid = 'details';
let appId = '20020MANGE';
let dataSource = 'gl.voucherBill.voucherBill_list.cache';
export default function buttonClick(props, id) {
	let { deleteCacheId } = props.table;
	switch (id) {
		//新增
		case 'Add':
			props.pushTo('/add', {
				status: 'add',
				appcode: props.getSearchParam('c'), //获取小应用编码
				pagecode: '20020MANGE_003', //编辑态卡片页面编码
				tradetype: this.state.transtype //获取当前交易类型
			});
			break;
		//提交
		case 'Commit':
			const cheDatas = props.table.getCheckedRows(tableid);
			if (cheDatas.length == 0) {
				toast({ content: this.state.json['20020MANGE-000081'], color: 'warning' });/* 国际化处理： 请先选择数据*/
				return;
			}
			let checkedObj = [];
			cheDatas.forEach((val) => {
				checkedObj.push({
					pk_bill: val.data.values.pk_voucher.value,
					ts: val.data.values.ts.value,
					billType: 'VR01',
					index: val.index,
					pageId: props.getSearchParam('p')
				});
			});
			this.submitData(checkedObj,true);
			break;
		//收回
		case 'Uncommit':
			const uncommitDatas = props.table.getCheckedRows(tableid);
			if (uncommitDatas.length == 0) {
				toast({ content: this.state.json['20020MANGE-000081'], color: 'warning' });/* 国际化处理： 请先选择数据*/
				return;
			}
			let checkedObj2 = [];
			uncommitDatas.forEach((val) => {
				checkedObj2.push({
					pk_bill: val.data.values.pk_voucher.value,
					ts: val.data.values.ts.value,
					billType: 'VR01',
					index: val.index,
					pageId: props.getSearchParam('p')
				});
			});
			ajax({
				url: '/nccloud/gl/voucher/unsendApprovebills.do',
				data: checkedObj2,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data.grid) {
							let grid = data.grid;
							let updateValue = [];
							for (let key in grid) {
								updateValue.push({ index: key, data: { values: grid[key].values } });
							}
							props.table.updateDataByIndexs(tableid, updateValue);
						}
						controlButton(props,tableid);
						if (data.message) {
							toast({
								duration: 'infinity',
								color: data.PopupWindowStyle,
								content:data.message
							});
						}
					}
				}
			});
			break;
		//附件上传
		case 'AttachManage':
			this.openfile(props);
			break;
		//导出
		case 'ExportData':
			this.export(props);
			break;
		//打印
		case 'Print':
			onPrint(props);
			break;
		//打印输出
		case 'Output':
			onOpen(props);
			break;
		//影像扫描
		case 'ReceiptScan':
			let rows = props.table.getCheckedRows('details');
			let index1 = [];
			let pk;
			let vbillcode;
			rows.forEach((ele, index) => {
				pk = ele.data.values.pk_voucher;
				vbillcode = ele.data.values.billno;
				index1.push(pk.value);
			});
			if (index1.length == 0) {
				toast({ content: this.state.json['20020MANGE-000082'], color: 'warning' });/* 国际化处理： 请选择单据！*/
				return;
			}
			if (index1.length > 1) {
				toast({ content: this.state.json['20020MANGE-000083'], color: 'warning' });/* 国际化处理： 单据不能多选！*/
				return;
			}
			var billInfoMap = {};
			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = pk.value;
			billInfoMap.pk_billtype = rows[0].data.values.pk_billtypecode.value;
			billInfoMap.pk_tradetype = rows[0].data.values.pk_tradetypecode.value;
			billInfoMap.pk_org = rows[0].data.values.pk_org.value;

			//影像所需 FieldMap
			billInfoMap.BillType = rows[0].data.values.pk_tradetypecode.value;
			billInfoMap.BillDate = rows[0].data.values.creationtime.value;
			billInfoMap.Busi_Serial_No = rows[0].data.values.pk_voucher.value;
			billInfoMap.OrgNo = rows[0].data.values.pk_org.value;
			billInfoMap.BillCode = rows[0].data.values.billno.value;
			billInfoMap.OrgName = rows[0].data.values.pk_org.display;
			billInfoMap.Cash = rows[0].data.values.totaldebit.value;

			imageScan(billInfoMap, 'iweb');
			break;
		//影像查看
		case 'ReceiptCheck':
			let rows2 = props.table.getCheckedRows('details');
			let index2 = [];
			let pk2;
			let vbillcode2;
			rows2.forEach((ele, index) => {
				pk2 = ele.data.values.pk_voucher;
				vbillcode2 = ele.data.values.billno;
				index2.push(pk2.value);
			});
			if (index2.length == 0) {
				toast({ content: this.state.json['20020MANGE-000082'], color: 'warning' });/* 国际化处理： 请选择单据！*/
				return;
			}
			if (index2.length > 1) {
				toast({ content: this.state.json['20020MANGE-000083'], color: 'warning' });/* 国际化处理： 单据不能多选！*/
				return;
			}
			var billInfoMap = {};
			//基础字段 单据pk,单据类型，交易类型，单据的组织
			billInfoMap.pk_billid = pk2.value;
			billInfoMap.pk_billtype = rows2[0].data.values.pk_billtypecode.value;
			billInfoMap.pk_tradetype = rows2[0].data.values.pk_tradetypecode.value;
			billInfoMap.pk_org = rows2[0].data.values.pk_org.value;

			imageView(billInfoMap,  'iweb');
			break;
		//删除
		case 'Delete':
			const selDatas = props.table.getCheckedRows(tableid);
			if (selDatas.length == 0) {
				toast({ content: this.state.json['20020MANGE-000081'], color: 'warning' });/* 国际化处理： 请先选择数据*/
				return;
			}
			promptBox({
                'color': 'warning',
                'title': this.state.json['20020MANGE-000084'],/* 国际化处理： 删除*//* 国际化处理： 提示*/
                'content': this.state.json['20020MANGE-000085'],/* 国际化处理： 确定要删除所选数据吗?*//* 国际化处理： 是否删除*/
                beSureBtnClick: () => {
					let checkedObj3 = [];
					selDatas.forEach((val) => {
						checkedObj3.push({
							pk_bill: val.data.values.pk_voucher.value,
							ts: val.data.values.ts.value,
							billType: 'VR01',
							index: val.index,
							pageId: props.getSearchParam('p')
						});
					});
					ajax({
						url: '/nccloud/gl/voucherbill/delete.do',
						data: checkedObj3,
						success: (res) => {
							let { success, data } = res;
							if (success) {
								if (data) {
									toast({
										duration:  'infinity',          
										color: data.PopupWindowStyle,         
										content: data.errMsg,   
									})
									//删除当前行数据
									props.table.deleteTableRowsByIndex(tableid, data.successIndexs);
									//删除缓存数据
									deleteCacheId(tableid, data.successPKs);
									//删行之后控制肩部按钮
									controlButton(props,tableid);
								}
							}
						}
					});
				}
			});
			break;
		//联查凭证
		case 'LinkVoucher':
			//获取选中的数据
			let linkData = props.table.getCheckedRows(tableid);
			if (linkData.length == 0) {
				toast({ content: this.state.json['20020MANGE-000081'], color: 'warning' });/* 国际化处理： 请先选择数据*/
				return;
			}
			let linkArray = [];
			//组装参数
			linkData.forEach((val) => {
				let linkObj = {
					pk_billtype: '',
					pk_group: '',
					pk_org: '',
					relationID: ''
				};
				linkObj.pk_billtype = val.data.values.pk_tradetypecode.value;
				linkObj.pk_group = val.data.values.pk_group.value;
				linkObj.pk_org = val.data.values.pk_org.value;
				linkObj.relationID = val.data.values.pk_voucher.value;
				linkArray.push(linkObj);
			});
			let that=this;
			let appcode=this.props.getSearchParam('c')
			//将数据放到浏览器缓存里
			//props.ViewModel.setData(appId + '_LinkVouchar',linkArray);
			// cacheTools.set(props.getSearchParam('c') + '_LinkVouchar', linkArray);
			ajax({
				url: '/nccloud/gl/voucherbill/queryFipUrl.do',
				// data: { appCode: '10170410', pageCode: '10170410_1017041001' },
				data:linkArray,
				success: (res) => {
					if (res.success) {
						if (res.data.des) {
							//跳转到凭证界面
							if (res.data.pklist) {
								if (res.data.pklist.length == 1) {
									//单笔联查
									that.props.openTo(res.data.url, {
										status: 'browse',
										appcode: res.data.appcode,
										pagecode: res.data.pagecode,
										id: res.data.pklist[0],
										n: that.state.json['public-000231'], //'联查凭证'
										backflag: 'noback'
									});
									return;
								} else {
									//多笔联查
									cacheTools.set(res.data.cachekey, res.data.pklist);
									that.props.openTo(res.data.url, {
										status: 'browse',
										appcode: res.data.appcode,
										pagecode: res.data.pagecode,
										scene: appcode + res.data.src,
										n: that.state.json['public-000231'] //'联查凭证'
									});
									return;
								}
							}
							// props.openTo(res.data, {
							// 	status: 'browse',
							// 	scene: props.getSearchParam('c') + '_LinkVouchar',
							// 	appcode: '10170410',
							// 	pagecode: '10170410_1017041001'
							// });
						}else{
							//跳转到会计平台
							cacheTools.set(appcode + res.data.src, res.data.pklist);
							props.openTo(res.data.url, {
								status: 'browse',
								appcode: res.data.appcode,
								pagecode: res.data.pagecode,
								scene: appcode + res.data.src
							});
							return
						}
					}
				}
			});
		//刷新
		case 'Refresh':
			refresh(props,this);
			// let queryInfo = cacheTools.get('voucherBillSearchData');
			// if(!queryInfo){
			// 	return;
			// }
			// let pageInfo = props.table.getTablePageInfo(tableid);
			// //辅助核算特殊处理
			// let ass_value = null;
			// let conditions = [];
			// if(queryInfo.querycondition && queryInfo.querycondition.conditions && queryInfo.querycondition.conditions.length > 1){
			// 	queryInfo.querycondition.conditions.forEach((item) => {
			// 		if(item.field == 'details.assid'){
			// 			ass_value = item.value.firstvalue;
			// 		}else if(item.field == 'period') {//会计期间与单据日期联动,只做前台展示,不做条件查询,单据日期作为条件进行查询
		
			// 		}else {
			// 			conditions.push(item);
			// 		}
			// 	});
			// }
			// if(conditions.length > 0){
			// 	queryInfo.querycondition.conditions = conditions;
			// }
			// queryInfo.pageInfo = pageInfo;
			// let data={
			// 	queryInfo:queryInfo,
			// 	pagecode: props.getSearchParam("p"),
			// 	assid: ass_value
			// };
		
			// ajax({
			// 	url: '/nccloud/gl/voucherbill/queryAll.do',//'/nccloud/reva/search/query.do',
			// 	data: data,
			// 	success:  (res)=> {
			// 		let { success, data } = res;
			// 			if (success && data != undefined) {
			// 				setDefData(pkname, dataSource, data[tableid]);
			// 				props.table.setAllTableData(tableid, data[tableid]);
			// 			}else{
			// 				props.table.setAllTableData(tableid, {rows:[]});
			// 			}
			// 	}
			// });

	}
}

/**
 * 打印
 */
export const onPrint = (props) => {
	let rows = props.table.getCheckedRows('details');
	let index1 = [];
	rows.forEach((ele, index) => {
		let pk = ele.data.values.pk_voucher;
		index1.push(pk.value);
	});
	if (index1.length == 0) {
		toast({ content: this.state.json['20020MANGE-000087'], color: 'warning' });/* 国际化处理： 未选中任何行，不允许操作打印！*/
		return;
	}
	print(
		'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
		'/nccloud/gl/voucherbill/print.do', //后台服务url
		{
			billtype: 'VR01', //单据类型
			appcode: props.getSearchParam('c'), //功能节点编码，即模板编码
			nodekey: 'VRD1_CARD', //模板节点标识
			oids: index1 // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
		}
	);
};

/**
 * 打印输出
 */
export const onOpen = (props) => {
	let rows = props.table.getCheckedRows('details');
	let index1 = [];
	rows.forEach((ele, index) => {
		let pk = ele.data.values.pk_voucher;
		index1.push(pk.value);
	});
	if (index1.length == 0) {
		toast({ content: this.state.json['20020MANGE-000088'], color: 'warning' });/* 国际化处理： 未选中任何行，不允许操作打印输出！*/
		return;
	}
	// 在你想要触发的事件里面调用output方法，必须传的参数有data， 选择传的参数有url（要打印调用的后台接口），callback（打印后的回调函数）
	output({
		url: '/nccloud/gl/voucherbill/printoutput.do',
		data: {
			outputType: 'output', //输出类型
			funcode: props.getSearchParam('c'), //功能节点编码，即模板编码
			nodekey: 'VRD1_CARD', //模板节点标识
			oids: index1 // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
		}
	});
};
