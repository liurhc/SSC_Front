import {ajax,toast} from 'nc-lightapp-front';
import refresh from './refresh';
let tableid = 'details';
import controlButton from './controlButton';
export default function(props, key, text, record, index,self) {
    let {deleteCacheId} = props.table;
	switch (key) {
        case 'Delete_inner':
            let checkedObj3 = [];
            checkedObj3.push({
                pk_bill: record.pk_voucher.value,
                ts: record.ts.value,
                billType: 'VR01',
                index: index,
                pageId: props.getSearchParam('p')
            });
            ajax({
                url: '/nccloud/gl/voucherbill/delete.do',
                data: checkedObj3,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        toast({ content: self.state.json['20020MANGE-000079'], color: "success" });/* 国际化处理： 删除成功*/
                        props.table.deleteTableRowsByIndex(tableid, index);
                        deleteCacheId(tableid,data.successPKs);
                        controlButton(props,tableid);
                    }
                }
            })
            break; 
        //复制                                             
        case 'Copy_inner':
            props.pushTo('/add',{
                status:"copy",
                id:record.pk_voucher.value,
                appcode:props.getSearchParam("c"),//获取小应用编码 
                pagecode:'20020MANGE_003',//编辑态卡片页面编码
                tradetype:record.pk_tradetypecode.value//获取当前交易类型 
            })                                                                          
            break; 
        //修改                                             
        case 'Edit_inner':
            props.pushTo('/add',{
                status:"edit",
                bill_id:record.pk_voucher.value,
                appcode:props.getSearchParam("c"),//获取小应用编码 
                pagecode:'20020MANGE_003',//编辑态卡片页面编码
                tradetype:record.pk_tradetypecode.value//获取当前交易类型
            })                                                                         
            break;
        //提交
        case 'Commit_inner':
            let checkedObj = [];
            checkedObj.push({
                pk_bill: record.pk_voucher.value,
                ts: record.ts.value,
                billType: 'VR01',
                index: record.numberindex.value - 1,
                pageId: props.getSearchParam('p')
            });
            self.submitData(checkedObj);
            break;
        //收回
        case 'Uncommit_inner':
            let checkedObj2 = [];
            checkedObj2.push({
                pk_bill: record.pk_voucher.value,
                ts: record.ts.value,
                billType: 'VR01',
                index: record.numberindex.value - 1,
                pageId: props.getSearchParam('p')
            });
            ajax({
				url: '/nccloud/gl/voucher/unsendApprovebills.do',
				data: checkedObj2,
				success: (res) => {
                    let { success, data } = res;
					if (success) {
						//toast({ color: 'success', content: self.state.json['20020MANGE-000080'] });/* 国际化处理： 操作成功*/
                        //更新当前行数据
                        if (data.grid) {
							let grid = data.grid;
							let updateValue = [];
							for (let key in grid) {
								updateValue.push({ index: key, data: { values: grid[key].values } });
							}
							props.table.updateDataByIndexs(tableid, updateValue);
						}
                        controlButton(props,tableid);
                        if (data.message) {
							toast({
								duration: 'infinity',
								color: data.PopupWindowStyle,
								content:data.message
							});
						}
					}
				}
			});
            break;
        //审批详情
        case 'LinkAprv':
            self.setState({
                id: record.pk_voucher.value,
                showApproveDetail: true
            });
            break;
		default:
			break;
	}
	return  true;
}
