import React, { Component } from 'react';
import { high, base, ajax, getMultiLang } from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCModal: Modal,
	NCForm: Form
} = base;
import amountconvert from '../../../amountConvert';
import { emptyEnter,countRow} from '../../../../gl_voucher/container/Welcome/voucherOftenApi.js'
const { Refer } = high;
import { CheckboxItem, RadioItem, TextAreaItem, ReferItem, NcnumberItem } from '../../../../public/components/FormItems';
import './index.less';
const { NCFormItem: FormItem } = Form;
const format = 'YYYY-MM-DD';
export default class ChangeNumberModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			checkFormNow: false, //控制表单form回调
			loadItem: [],
			json:{}
		};
	}

	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		multiSelect: {
			type: 'checkbox',
			param: 'key'
		}
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.show) {
			this.setState({
				loadItem: nextProps.loadForm
			});
		}
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				//initTemplate.call(this, this.props, this.state);
			})
		}
		getMultiLang({moduleId:'20020MANGE',domainName:'gl',currentLocale:'zh-CN',callback});
	}

	//点击保存,修改值传至父组件
	modalMessage = () => {
		let self = this;
		let { loadItem } = self.state;
		self.props.onConfirm(loadItem);
		self.setState({
			checkFormNow: false
		});
	};

	assureFormCallback = (isCheck, values, others) => {
		let self = this;
		self.props.onConfirm(loadItem);
		self.setState({
			checkFormNow: false
		});
	};

	countRow = (originData, countValue, key) => {
		//跟新计算后的值
		let keyString;
		if (key != 'amount') {
			keyString = key.substring(5);
		} else {
			keyString = 'amount';
		}
		for (let item in countValue) {
			if (
				originData.hasOwnProperty(item) ||
				item.indexOf('localamount') != -1 ||
				item.indexOf('groupamount') != -1 ||
				item.indexOf('globalamount') != -1
			) {
				originData[item] = {
					...originData[item],
					value: countValue[item]
				};
				if (item == 'localamount') {
					if (keyString != 'creditamount' && keyString != 'debitamount') {
						if (originData.localcreditamount.value) {
							originData.localcreditamount = {
								...originData.localcreditamount,
								value: countValue[item]
							};
						} else {
							originData.localdebitamount = {
								...originData.localdebitamount,
								value: countValue[item]
							};
						}
					} else {
						if (keyString == 'creditamount') {
							originData.localcreditamount = {
								...originData.localcreditamount,
								value: countValue[item]
							};
							originData.localdebitamount = {
								...originData.localdebitamount,
								value: ''
							};
						} else {
							originData.localdebitamount = {
								...originData.localdebitamount,
								value: countValue[item]
							};
							originData.localcreditamount = {
								...originData.localcreditamount,
								value: ''
							};
						}
					}
				}
				if (item == 'groupamount') {
					if (keyString != 'creditamount' && keyString != 'debitamount') {
						if (originData.groupcreditamount.value) {
							originData.groupcreditamount = {
								...originData.groupcreditamount,
								value: countValue[item]
							};
						} else {
							originData.groupdebitamount = {
								...originData.groupdebitamount,
								value: countValue[item]
							};
						}
					} else {
						if (keyString == 'creditamount') {
							originData.groupcreditamount = {
								...originData.groupcreditamount,
								value: countValue[item]
							};
							originData.groupdebitamount = {
								...originData.groupdebitamount,
								value: ''
							};
						} else {
							originData.groupcreditamount = {
								...originData.groupcreditamount,
								value: ''
							};
							originData.groupdebitamount = {
								...originData.groupdebitamount,
								value: countValue[item]
							};
						}
					}
				}
				if (item == 'globalamount') {
					if (keyString != 'creditamount' && keyString != 'debitamount') {
						if (originData.globalcreditamount.value) {
							originData.globalcreditamount = {
								...originData.globalcreditamount,
								value: countValue[item]
							};
						} else {
							originData.globaldebitamount = {
								...originData.globaldebitamount,
								value: countValue[item]
							};
						}
					} else {
						if (keyString == 'creditamount') {
							originData.globalcreditamount = {
								...originData.globalcreditamount,
								value: countValue[item]
							};
							originData.globaldebitamount = {
								...originData.globaldebitamount,
								value: ''
							};
						} else {
							originData.globalcreditamount = {
								...originData.globalcreditamount,
								value: ''
							};
							originData.globaldebitamount = {
								...originData.globaldebitamount,
								value: countValue[item]
							};
						}
					}
				}
			}
		}
	};

	loadRows = (loadItem) => {
		let { checkFormNow, banks, accountData } = this.state;
		let self = this;
		let originData = loadItem &&loadItem.length != 0 &&loadItem[0];
		let data = {
			amount:originData&& originData.amount && originData.amount.value, //原币
			localamount:
			originData&&(originData.localcreditamount && originData.localcreditamount.value) ||
			originData&&(originData.localdebitamount && originData.localdebitamount.value)
					? originData.localcreditamount.value || originData.localdebitamount.value
					: '', //组织本币
			groupamount:
			originData&&(originData.groupcreditamount && originData.groupcreditamount.value) ||
			originData&&(originData.groupdebitamount && originData.groupdebitamount.value)
					? originData.groupcreditamount.value || originData.groupdebitamount.value
					: '', //集团本币
			globalamount:
			originData&&(originData.globalcreditamount && originData.globalcreditamount.value) ||
			originData&&(originData.globaldebitamount && originData.globaldebitamount.value)
					? originData.globalcreditamount.value || originData.globaldebitamount.value
					: '', //全局本币
			pk_currtype: originData&&originData.pk_currtype ? originData.pk_currtype.value : '', //原币币种
			orgcurrtype: originData&&originData.orgcurrtype ? originData.orgcurrtype.value : '', //本币币种
			groupcurrtype: originData&&originData.groupcurrtype ? originData.groupcurrtype.value : '', //集团本币币种
			globalcurrtype: originData&&originData.globalcurrtype ? originData.globalcurrtype.value : '', //全局本币币种
			excrate1: originData&&originData.excrate1 ? originData.excrate1.value : '', //本币汇率
			excrate2: originData&&originData.excrate2 ? originData.excrate2.value : '', //集团本币汇率
			excrate3: originData&&originData.excrate3 ? originData.excrate3.value : '', //全局本币汇率
			price: originData&&originData.price ? originData.price.value : '', //单价
			quantity: originData&&originData.debitquantity ? originData.debitquantity.value : '' //数量分借贷这块后续修改
		};
		let currInfo = {
			roundtype: originData&&originData.roundtype ? originData.roundtype.value : '', //原币进舍规则
			orgroundtype: originData&&originData.orgroundtype ? originData.orgroundtype.value : '', //本币进舍规则
			grouproundtype: originData&&originData.grouproundtype ? originData.grouproundtype.value : '', //集团本币进舍规则
			globalroundtype: originData&&originData.globalroundtype ? originData.globalroundtype.value : '', //全局本币进舍规则
			pricescale: originData&&originData.pricescale ? originData.pricescale.value : '', //单价精度
			quantityscale:
				originData.debitquantity && originData.debitquantity.scale
					? originData.debitquantity.scale
					: originData.quantityscale && originData.quantityscale.value, //数量精度
			priceroundtype: originData&&originData.priceroundtype ? originData.priceroundtype.value : '', //单价进舍规则
			scale: originData&&originData.scale ? originData.scale : '', //原币精度
			excrate1scale: originData&&originData.excrate1scale ? originData.excrate1scale.value : '', //本币汇率
			excrate2scale: originData&&originData.excrate2scale ? originData.excrate2scale.value : '', //本币汇率
			excrate3scale:originData&&originData.excrate3scale ? originData.excrate3scale.value : '', //本币汇率
			orgscale: originData&&originData.orgscale ? originData.orgscale : '', //组织本币精度
			groupscale: originData&&originData.groupscale ? originData.groupscale : '', //集团本币精度
			globalscale: originData&&originData.globalscale ? originData.globalscale : '', //全局本币精度
			maxconverr: originData&&originData.maxconverr ? originData.maxconverr.value : '',
			NC001: originData&&originData.groupType ? originData.groupType : '', //集团本币计算方式
			NC002: originData&&originData.globalType ? originData.globalType : '', //全局本币计算方式
			orgmode: originData&&originData.orgmode ? originData.orgmode.value : '', //汇率计算方式 是否为除 true=除
			groupmode: originData&&originData.groupmode
				? originData.groupmode.value === undefined ? '' : originData.groupmode.value
				: '', //集团本币汇率计算方式 是否为除 true=除
			globalmode: originData&&originData.globalmode
				? originData.globalmode.value === undefined ? '' : originData.globalmode.value
				: '' //全局本币汇率计算方式 是否为除 true=除
		};
		return (
			<div>
				<div
					class='changeNumber'
					showSubmit={false}
					checkFormNow={this.state.checkFormNow}
					submitCallBack={this.assureFormCallback}
				>
					{loadItem &&
					loadItem.length != 0 &&
					'debitquantity' in loadItem[0] && (
					 <FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000062']/* 国际化处理： 数量:*/}
							method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
						>
							<NcnumberItem
								name="debitquantity"
								type="customer"
							//	scale={loadItem[0]['quantityscale'] ? Number(loadItem[0]['debitquantity'].scale?loadItem[0]['debitquantity'].scale:loadItem[0]['quantityscale'].value) : 2}
								// scale={loadItem[0]['debitquantity']&&loadItem[0]['debitquantity'].scale?Number(loadItem[0]['debitquantity'].scale):Number(loadItem[0]['quantityscale']?loadItem[0]['quantityscale'].value:'2')}
								scale={
									loadItem[0]['debitquantity'] && loadItem[0]['debitquantity'].scale ? (
										Number(loadItem[0]['debitquantity'].scale)
									) : (
										loadItem[0]['quantityscale'] && Number(loadItem[0]['quantityscale'].value)
									)
								}
								//	disabled={isChange}
								defaultValue={loadItem[0]['debitquantity']&&loadItem[0]['debitquantity'].value}
								placeholder={self.state.json['20020MANGE-000038']/* 国际化处理： 请输入数量*/}
								onBlur={(v) => {
									if (v) {
										let countValue = amountconvert(data, currInfo, 'debitquantity');
										countRow(originData, countValue, 'debitquantity');
										self.setState({
											loadItem
										});
									}
									//self.forceUpdate();
								}}
								onChange={(v) => {
									let { loadItem } = self.state;
									loadItem[0]['debitquantity'].value = v;
									self.setState({
										loadItem
									});
								}}
							/>
						</FormItem>
					)}
					{loadItem &&
					loadItem.length != 0 &&
					'price' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000063']/* 国际化处理： 单价:*/}
							method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="price"
								type="customer"
								//scale={loadItem[0]['pricescale'] ? Number(loadItem[0]['price'].scale?loadItem[0]['price'].scale:loadItem[0]['pricescale'].value) : 2}
								// scale={loadItem[0]['price']&&loadItem[0]['price'].scale?Number(loadItem[0]['price'].scale):Number(loadItem[0]['pricescale']?loadItem[0]['pricescale'].value:'2')}
								scale={
									loadItem[0]['price'] && loadItem[0]['price'].scale ? (
										Number(loadItem[0]['price'].scale)
									) : (
										loadItem[0]['pricescale'] && Number(loadItem[0]['pricescale'].value)
									)
								}
								//	disabled={isChange}
								defaultValue={loadItem[0]['price'] && loadItem[0]['price'].value}
								placeholder={self.state.json['20020MANGE-000039']/* 国际化处理： 请输入单价*/}
								onBlur={(v) => {
									if (v) {
										let countValue = amountconvert(data, currInfo, 'price');
										countRow(originData, countValue, 'price');
										self.setState({
											loadItem
										});
									}

									//self.forceUpdate();
								}}
								onChange={(v) => {
									let { loadItem } = self.state;
									loadItem[0]['price'].value = v;
									self.setState({
										loadItem
									});
									//self.forceUpdate();
								}}
							/>
						</FormItem>
					)}
					{loadItem &&
					loadItem.length != 0 &&
					'amount' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000064']/* 国际化处理： 原币金额:*/}
							method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="amount"
								type="customer"
								// scale={loadItem[0]['scale'] ? Number(loadItem[0]['scale']) : 2}
								scale={
									loadItem[0]['amount'] && loadItem[0]['amount'].scale ? (
										Number(loadItem[0]['amount'].scale)
									) : (
										Number(loadItem[0]['scale'])
									)
								}
								//	disabled={isChange}
								defaultValue={loadItem[0]['amount'] && loadItem[0]['amount'].value}
								placeholder={self.state.json['20020MANGE-000065']/* 国际化处理： 请输入金额*/}
								onBlur={(v) => {
									if (v) {
										let countValue = amountconvert(data, currInfo, 'amount');
										countRow(originData, countValue, 'amount');
										self.setState({
											loadItem
										});
									}
								}}
								onChange={(v) => {
									let { loadItem } = self.state;
									loadItem[0]['amount'].value = v;
									self.setState({
										loadItem
									});
								}}
							/>
						</FormItem>
					)}
					{loadItem &&
					loadItem.length != 0 &&
					'excrate1' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000066']/* 国际化处理： 组织本币汇率:*/}
							method="blur"
							//reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="excrate1"
								type="customer"
								// scale={loadItem[0]['excrate1scale'] ? Number(loadItem[0]['excrate1scale'].value) : 2}
								scale={
									loadItem[0]['excrate1'] && loadItem[0]['excrate1'].scale ? (
										Number(loadItem[0]['excrate1'].scale)
									) : (
										Number(loadItem[0]['excrate1scale'])&&Number(loadItem[0]['excrate1scale'].value)?Number(loadItem[0]['excrate1scale'].value):2
									)
								}
								disabled={excrateDis1(loadItem[0])}
								//	disabled={isChange}
								defaultValue={loadItem[0]['excrate1'] && loadItem[0]['excrate1'].value}
								placeholder={self.state.json['20020MANGE-000067']/* 国际化处理： 请输入本币汇率*/}
								onBlur={(v) => {
									if (v) {
										let countValue = amountconvert(data, currInfo, 'excrate1');
										countRow(originData, countValue, 'excrate1');
										self.setState({
											loadItem
										});
									}
								}}
								onChange={(v) => {
									let { loadItem } = self.state;
									loadItem[0]['excrate1'].value = v;
									self.setState({
										loadItem
									});
								}}
							/>
						</FormItem>
					)}
					{loadItem &&
					loadItem.length != 0 &&
					'localdebitamount' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000068']/* 国际化处理： 组织本币(借方):*/}
							method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="localdebitamount"
								type="customer"
								// scale={loadItem[0]['orgscale'] ? Number(loadItem[0]['orgscale']) : 2}
								scale={
									loadItem[0]['localdebitamount'] && loadItem[0]['localdebitamount'].scale ? (
										Number(loadItem[0]['localdebitamount'].scale)
									) : (
										Number(loadItem[0]['orgscale'])
									)
								}
								//	disabled={isChange}
								defaultValue={loadItem[0]['localdebitamount'] && loadItem[0]['localdebitamount'].value}
								placeholder={self.state.json['20020MANGE-000065']/* 国际化处理： 请输入金额*/}
								onBlur={(v) => {
									if(v){
										let countValue = amountconvert(data, currInfo, 'localdebitamount');
										countRow(originData, countValue, 'localdebitamount');
										self.setState({
											loadItem
										});
									}
								}}
								onKeyUp={(event) => {
									if (event.keyCode == '32' && originData.excrate1.value) {
										emptyEnter('localdebitamount', originData);
										self.setState({
											loadItem
										});
									}
								}}
								onChange={(v) => {
									if (v && v.indexOf(' ') == -1) {
										let { loadItem } = self.state;
										loadItem[0]['localdebitamount'].value = v;
										self.setState({
											loadItem
										});
									}
								}}
							/>
						</FormItem>
					)}
					{loadItem &&
					loadItem.length != 0 &&
					'localcreditamount' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000069']/* 国际化处理： 组织本币(贷方):*/}
							method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="amount"
								type="customer"
								// scale={loadItem[0]['orgscale'] ? Number(loadItem[0]['orgscale']) : 2}
								scale={
									loadItem[0]['localcreditamount'] && loadItem[0]['localcreditamount'].scale ? (
										Number(loadItem[0]['localcreditamount'].scale)
									) : (
										Number(loadItem[0]['orgscale'])
									)
								}
								//	disabled={isChange}
								defaultValue={loadItem[0]['localcreditamount'] && loadItem[0]['localcreditamount'].value}
								placeholder={self.state.json['20020MANGE-000065']/* 国际化处理： 请输入金额*/}
								onBlur={(v) => {
									if(v){
										let countValue = amountconvert(data, currInfo, 'localcreditamount');
										countRow(originData, countValue, 'localcreditamount');
										self.setState({
											loadItem
										});
									}
								}}
								onKeyUp={(event) => {
									if (event.keyCode == '32' && originData.excrate1.value) {
										emptyEnter('localcreditamount', originData);
										self.setState({
											loadItem
										});
									}
								}}
								onChange={(v) => {
									if (v && v.indexOf(' ') == -1) {
										let { loadItem } = self.state;
										loadItem[0]['localcreditamount'].value = v;
										self.setState({
											loadItem
										});
									}
								}}
							/>
						</FormItem>
					)}
					{loadItem &&
					loadItem.length != 0 &&
					'excrate2' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000070']/* 国际化处理： 集团本币汇率:*/}
							method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="excrate2"
								type="customer"
								// scale={loadItem[0]['excrate2scale'] ? Number(loadItem[0]['excrate2scale'].value) : 2}
								//	disabled={isChange}
								scale={
									loadItem[0]['excrate2'] && loadItem[0]['excrate2'].scale ? (
										Number(loadItem[0]['excrate2'].scale)
									) : (
										loadItem[0]['excrate2scale'].value ?
										Number(loadItem[0]['excrate2scale'].value):
										(
										loadItem[0]['excrate1'] && loadItem[0]['excrate1'].scale ? (
											Number(loadItem[0]['excrate1'].scale)
										) : (
											Number(loadItem[0]['excrate1scale'].value)
										)
										)
									)
								}
								disabled={excrateDis2(loadItem[0])}
								defaultValue={loadItem[0]['excrate2'] && loadItem[0]['excrate2'].value}
								placeholder={self.state.json['20020MANGE-000071']/* 国际化处理： 请输入集团本币汇率*/}
								onBlur={(v) => {
									if (v) {
										let countValue = amountconvert(data, currInfo, 'excrate2');
										countRow(originData, countValue, 'excrate2');
										self.setState({
											loadItem
										});
									}
								}}
								onChange={(v) => {
									let { loadItem } = self.state;
									loadItem[0]['excrate2'].value = v;
									self.setState({
										loadItem
									});
								}}
							/>
						</FormItem>
					)}

					{loadItem &&
					loadItem.length != 0 &&
					'groupdebitamount' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000072']/* 国际化处理： 集团本币(借方):*/}
							method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="groupdebitamount"
								type="customer"
								// scale={loadItem[0]['groupscale'] ? Number(loadItem[0]['groupscale']) : 2}
								//	disabled={isChange}
								scale={
									loadItem[0]['groupdebitamount'] && loadItem[0]['groupdebitamount'].scale ? (
										Number(loadItem[0]['groupdebitamount'].scale)
									) : (
										loadItem[0]['groupscale'] ?
										Number(loadItem[0]['groupscale']):
										(
											loadItem[0]['localdebitamount'] && loadItem[0]['localdebitamount'].scale ? (
												Number(loadItem[0]['localdebitamount'].scale)
											) : (
												Number(loadItem[0]['orgscale'])
											)
										)
									)
								}
								defaultValue={loadItem[0]['groupdebitamount'] && loadItem[0]['groupdebitamount'].value}
								placeholder={self.state.json['20020MANGE-000065']/* 国际化处理： 请输入金额*/}
								onBlur={(v) => {
									if(v){
										let countValue = amountconvert(data, currInfo, 'groupdebitamount');
										countRow(originData, countValue, 'groupdebitamount');
										self.setState({
											loadItem
										});
									}
								}}
								onKeyUp={(event) => {
									if (event.keyCode == '32' && originData.excrate2.value) {
										emptyEnter('groupdebitamount', originData);
										self.setState({
											loadItem
										});
									}
								}}
								onChange={(v) => {
									if (v && v.indexOf(' ') == -1) {
										let { loadItem } = self.state;
										loadItem[0]['groupdebitamount'].value = v;
										self.setState({
											loadItem
										});
									}
								}}
							/>
						</FormItem>
					)}

					{loadItem &&
					loadItem.length != 0 &&
					'groupcreditamount' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000073']/* 国际化处理： 集团本币(贷方):*/}
							method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="amount"
								type="customer"
								// scale={loadItem[0]['groupscale'] ? Number(loadItem[0]['groupscale']) : 2}
								//	disabled={isChange}
								scale={
									loadItem[0]['groupcreditamount'] && loadItem[0]['groupcreditamount'].scale ? (
										Number(loadItem[0]['groupcreditamount'].scale)
									) : (
										loadItem[0]['groupscale'] ?
										Number(loadItem[0]['groupscale']):
										(
											loadItem[0]['localcreditamount'] && loadItem[0]['localcreditamount'].scale ? (
												Number(loadItem[0]['localcreditamount'].scale)
											) : (
												Number(loadItem[0]['orgscale'])
											)
										)
									)
								}
								defaultValue={loadItem[0]['groupcreditamount'] && loadItem[0]['groupcreditamount'].value}
								placeholder={self.state.json['20020MANGE-000065']/* 国际化处理： 请输入金额*/}
								onBlur={(v) => {
									if(v){
										let countValue = amountconvert(data, currInfo, 'localdebitamount');
										countRow(originData, countValue, 'localdebitamount');
										self.setState({
											loadItem
										});
									}
								}}
								onKeyUp={(event) => {
									if (event.keyCode == '32' && originData.excrate2.value) {
										emptyEnter('groupcreditamount', originData);
										self.setState({
											loadItem
										});
									}
								}}
								onChange={(v) => {
									if (v && v.indexOf(' ') == -1) {
										let { loadItem } = self.state;
										loadItem[0]['groupcreditamount'].value = v;
										self.setState({
											loadItem
										});
									}
								}}
							/>
						</FormItem>
					)}

					{loadItem &&
					loadItem.length != 0 &&
					'excrate3' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000074']/* 国际化处理： 全局本币汇率:*/}
							//	method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="excrate3"
								type="customer"
								// scale={loadItem[0]['excrate3scale'] ? Number(loadItem[0]['excrate3scale'].value) : '2'}
								//	disabled={isChange}
								scale={
									loadItem[0]['excrate3'] && loadItem[0]['excrate3'].scale ? (
										Number(loadItem[0]['excrate3'].scale)
									) : (
										loadItem[0]['excrate3scale'].value ?
										Number(loadItem[0]['excrate3scale'].value):
										(
										loadItem[0]['excrate1'] && loadItem[0]['excrate1'].scale ? (
											Number(loadItem[0]['excrate1'].scale)
										) : (
											Number(loadItem[0]['excrate1scale'].value)
										)
										)
									)
								}
								disabled={excrateDis3(loadItem[0])}
								defaultValue={loadItem[0]['excrate3'] && loadItem[0]['excrate3'].value}
								placeholder={self.state.json['20020MANGE-000075']/* 国际化处理： 请输入全局本币汇率*/}
								onBlur={(v) => {
									if (v) {
										let countValue = amountconvert(data, currInfo, 'excrate3');
										countRow(originData, countValue, 'excrate3');
										self.setState({
											loadItem
										});
									}
								}}
								onChange={(v) => {
									let { loadItem } = self.state;
									loadItem[0]['excrate3'].value = v;
									self.setState({
										loadItem
									});
								}}
							/>
						</FormItem>
					)}

					{loadItem &&
					loadItem.length != 0 &&
					'globaldebitamount' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000076']/* 国际化处理： 全局本币(借方):*/}
							method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="amount"
								type="customer"
								// scale={loadItem[0]['globalscale'] ? Number(loadItem[0]['globalscale']) : 2}
								//	disabled={isChange}
								scale={
									loadItem[0]['globaldebitamount'] && loadItem[0]['globaldebitamount'].scale ? (
										Number(loadItem[0]['globaldebitamount'].scale)
									) : (
										loadItem[0]['globalscale'] ?
										Number(loadItem[0]['globalscale']):
										(
											loadItem[0]['localdebitamount'] && loadItem[0]['localdebitamount'].scale ? (
												Number(loadItem[0]['localdebitamount'].scale)
											) : (
												Number(loadItem[0]['orgscale'])
											)
										)
									)
								}
								defaultValue={loadItem[0]['globaldebitamount'] && loadItem[0]['globaldebitamount'].value}
								placeholder={self.state.json['20020MANGE-000065']/* 国际化处理： 请输入金额*/}
								onBlur={(v) => {
									if(v){
										let countValue = amountconvert(data, currInfo, 'globaldebitamount');
										countRow(originData, countValue, 'globaldebitamount');
										self.setState({
											loadItem
										});
									}
								}}
								onKeyUp={(event) => {
									if (event.keyCode == '32' && originData.excrate3.value) {
										emptyEnter('globaldebitamount', originData);
										self.setState({
											loadItem
										});
									}
								}}
								onChange={(v) => {
									if (v && v.indexOf(' ') == -1) {
										let { loadItem } = self.state;
										loadItem[0]['globaldebitamount'].value = v;
										self.setState({
											loadItem
										});
									}
								}}
							/>
						</FormItem>
					)}

					{loadItem &&
					loadItem.length != 0 &&
					'globalcreditamount' in loadItem[0] && (
						<FormItem
							showMast={true}
							inline={true}
							//	isRequire={this.state.assureInfo.contracttype.value == 1}
							labelName={self.state.json['20020MANGE-000077']/* 国际化处理： 全局本币(贷方):*/}
							method="blur"
							//	reg={/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/}
							//	errorMessage="请输入数量"
						>
							<NcnumberItem
								name="amount"
								type="customer"
								// scale={loadItem[0]['globalscale'] ? Number(loadItem[0]['globalscale']) : 2}
								//	disabled={isChange}
								scale={
									loadItem[0]['globalcreditamount'] && loadItem[0]['globalcreditamount'].scale ? (
										Number(loadItem[0]['globalcreditamount'].scale)
									) : (
										loadItem[0]['globalscale'] ?
										Number(loadItem[0]['globalscale']):
										(
											loadItem[0]['localcreditamount'] && loadItem[0]['localcreditamount'].scale ? (
												Number(loadItem[0]['localcreditamount'].scale)
											) : (
												Number(loadItem[0]['orgscale'])
											)
										)
									)
								}
								defaultValue={loadItem[0]['globalcreditamount'] && loadItem[0]['globalcreditamount'].value}
								placeholder={self.state.json['20020MANGE-000065']/* 国际化处理： 请输入金额*/}
								onBlur={(v) => {
									if(v){
										let countValue = amountconvert(data, currInfo, 'globalcreditamount');
										countRow(originData, countValue, 'globalcreditamount');
										self.setState({
											loadItem
										});
									}
								}}
								onKeyUp={(event) => {
									if (event.keyCode == '32' && originData.excrate3.value) {
										emptyEnter('globalcreditamount', originData);
										self.setState({
											loadItem
										});
									}
								}}
								onChange={(v) => {
									if (v && v.indexOf(' ') == -1) {
										let { loadItem } = self.state;
										loadItem[0]['globalcreditamount'].value = v;
										self.setState({
											loadItem
										});
									}
								}}
							/>
						</FormItem>
					)}
				</div>
			</div>
		);
	};

	render() {
		let self = this;
		let {
			show,
			title,
			content,
			closeButton,
			icon,
			isButtonWhite,
			isButtonShow
		} = this.props;
		const { loadItem } = self.state;

		return (
			<Modal
				show={show}
				className="simpleModal senior modal-use-form-componet"
				onHide={() => this.props.onCancel(true)}
			>
				<Modal.Header closeButton>
					<Modal.Title>{title}</Modal.Title>
				</Modal.Header>
				<Modal.Body>{loadItem &&
					loadItem.length != 0 &&self.loadRows(loadItem)}
				</Modal.Body>
				{isButtonShow && (
					<Modal.Footer>
						<Button className="button-primary" onClick={() => this.modalMessage()}>
							{this.state.json['20020MANGE-000061']/* 国际化处理： 确定*/}
						</Button>
						<Button
							onClick={() => {
								this.props.onCancel(false);
							}}
						>
							{this.state.json['20020MANGE-000027']/* 国际化处理： 取消*/}
						</Button>
					</Modal.Footer>
				)}
			</Modal>
		);
	}
}

export function excrateDis1(showRow) {
	if (showRow.pk_currtype && showRow.orgcurrtype) {
		if (showRow.pk_currtype.value == showRow.orgcurrtype.value) {
			return true;
		}
	}
	return false;
}

//当集团本币的计算方式是基于原币计算时，原币币种＝集团本币币种时，集团本币汇率默认为1，且不可修改；
//当集团本币的计算方式是基于组织本币计算时，组织本币币种＝集团本币币种时，集团本币汇率默认为1，且不可修改；
export function excrateDis2(showRow) {
	if (
		showRow.groupType &&
		showRow.groupType == 'raw_convert' &&showRow.pk_currtype&&showRow.groupcurrtype&&
		showRow.pk_currtype.value == showRow.groupcurrtype.value
	) {
		return true;
	} else if (
		showRow.groupType &&
		showRow.groupType == 'local_convert' &&showRow.orgcurrtype&&showRow.groupcurrtype&&
		showRow.orgcurrtype.value == showRow.groupcurrtype.value
	) {
		return true;
	} else {
		return false;
	}
}
//当全局本币的计算方式是基于原币计算时，原币币种＝全局本币币种时，全局本币汇率默认为1，且不可修改；
//当全局本币的计算方式是基于组织本币计算时，组织本币币种＝全局本币币种时，全局本币汇率默认为1，且不可修改；
export function excrateDis3(showRow) {
	if (
		showRow.globalType &&
		showRow.globalType == 'raw_convert' &&showRow.pk_currtype&&showRow.globalcurrtype&&
		showRow.pk_currtype.value == showRow.globalcurrtype.value
	) {
		return true;
	} else if (
		showRow.globalType &&
		showRow.globalType == 'local_convert' &&showRow.orgcurrtype&& showRow.globalcurrtype&&
		showRow.orgcurrtype.value == showRow.globalcurrtype.value
	) {
		return true;
	} else {
		return false;
	}
}
