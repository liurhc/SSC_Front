import { createPage, ajax, base, toast,cacheTools,print,getBusinessInfo } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;
let formId='head';
let formIdtail='tail'
export default function (props) {
	let that = this;
	props.createUIDom(
		{
			pagecode:'20020MANGE_003',//编辑态页面编码
			appcode:props.getSearchParam("c") //小应用编码
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					meta = modifierMeta(props, meta, that);
					props.meta.setMeta(meta);
					if(props.getUrlParam('status') != 'browse'){
						//props.cardTable.addRow(tableId);
					}
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
			}   
		}
	)
}
function modifierMeta(props, meta, that) {
	let status = props.getUrlParam('status');
	if(props.getUrlParam('status')=='add'){//新增的时候设置默认值
		that.initOrgInfo();
	}
	meta[formId].status = status;
	meta[formIdtail].status = status;
	meta[formId].items.map((item) => {
		if(item.attrcode == 'pk_org'){
			item.isMultiSelectedEnabled = false;
			item.queryCondition = () => {
                return {
					isDataPowerEnable:true,//是否启用数据权限
                    DataPowerOperationCode: 'fi',//使用权组
                    AppCode: props.getSearchParam('c'),
                    TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
                };
            }
        } else if (item.attrcode == 'pk_tradetypeid') {
			item.queryCondition = () => {
				return { parentbilltype: 'VR01' };
			};
		}
	});
	meta[formIdtail].items.map((item) => {
		item.disabled = true;
	});
	return meta;
}


