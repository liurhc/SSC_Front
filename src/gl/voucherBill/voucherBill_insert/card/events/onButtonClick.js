import { ajax, base, toast,cacheTools,print,withNav } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
let tableid = 'gl_brsetting';
// @withNav
export default function onButtonClick(props, id) {
    switch (id) {
        case 'SaveGroup'://           
            break;
        case 'Save'://保存
            this.handleSave()
            break;
        case 'SaveAdd'://保存新增
            this.handleSaveAdd()
            break;
        case 'Commit'://提交
            this.handleSubmit()
            break;    
        case 'Cancel'://取消
            this.cancel()
            break;
        case 'More'://更多

            break;
        case 'BillAssistant'://辅助功能

            break;
        case 'AttachManage'://附件管理
            this.templetFileManage()
            break;
        case 'Receipt'://影像

            break;
        case 'ReceiptCheck'://影像查看
            this.imageManagerSelect.bind(this,'imageshow')
            break;    
        case 'ReceiptScan'://影像扫描
            this.imageManagerSelect.bind(this,'imageupload')
            break;
        case 'Line1'://

            break;
        case 'AddLine'://增行
            this.handleAdd()
            break;
        case 'DelLine'://删行
            this.handleCancel()
            break;
        case 'Line2'://
            
            break;
        case 'CopyLine'://复制
            this.handleCopy()
            break;
        case 'CutLine'://剪切
            this.handleCut()
            break;    
        case 'PasteLine'://粘贴
            this.handlePaste()
            break;
        default:
        break;

    }
}
