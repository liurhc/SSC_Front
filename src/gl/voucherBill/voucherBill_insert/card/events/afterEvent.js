import { toast,ajax,promptBox,getBusinessInfo } from 'nc-lightapp-front';
let formId='head';
let formIdtail='tail'
import { isObj } from '../../../../public/components/oftenApi';

export default function afterEvent(props, moduleId, key,v, oldValue, i, s, g) {
	let self=this;
	let { saveData,pk_org,pk_unit_org}=self.state;
	if(key==='pk_org'){
		afterOrg(self,props,v,oldValue);
	} else if(key==='billdate') {
		let nameValue = saveData[key];
		nameValue.display = v.display;
		nameValue.value = v.value;
		self.setState({
			saveData
		});	
	} else if(key==='explanation') {
		let nameValue = saveData[key];
		nameValue.display = v.display === undefined ? "" : v.display;
		nameValue.value = v.value;
		self.setState({
			saveData
		});	
	} else if(key==='pk_tradetypeid') {
		let nameValue = saveData[key];
		nameValue.display = v.display === undefined ? "" : v.display;
		nameValue.value = v.value;
		self.setState({
			saveData
		});	
	} else if(key.indexOf('def') != -1) {
		let nameValue = saveData[key];
		nameValue.display = v.display === undefined ? "" : v.display;
		nameValue.value = v.value;
		self.setState({
			saveData
		});	
	} else if (key === 'pk_vouchertype'){
		let nameValue = saveData[key];
		nameValue.display = v.display === undefined ? "" : v.display;
		nameValue.value = v.value;
		self.setState({
			saveData
		});	
	}
}

function afterOrg(self,props,newValue,oldValue){
	let { saveData,evidenceData}=self.state;
	if(oldValue&&oldValue.value){//修改组织
		if(oldValue.value!=newValue.value){
			promptBox({
				color: 'warning',
				title:self.state.json['20020MANGE-000004'],/* 国际化处理： 确认修改*/
				content: self.state.json['20020MANGE-000005'],/* 国际化处理： 确定修改业务单元,这样会清空您录入的信息？*/
				beSureBtnClick: () => {
					clearData(self,saveData,evidenceData);
					changeOrg(self,props,newValue,saveData,evidenceData);
				},
				cancelBtnClick:()=>{
					props.form.setFormItemsValue(formId,saveData);
				}
			});
		}
	}else{
		changeOrg(self,props,newValue,saveData,evidenceData);
	}
	
}

function clearData(self,saveData,evidenceData){
	//清除表头数据
	for(let headkey in saveData){
		if(isObj(saveData[headkey])){
			saveData[headkey].display='';
			saveData[headkey].value='';
		}
	}
	//添加默认数据
	let billstatus = { display: self.state.json['20020MANGE-000006'], value: '1' } ;/* 国际化处理： 保存*/
	saveData.billstatus=billstatus;
	let approvestatus={ display: self.state.json['20020MANGE-000007'], value: '-1' };/* 国际化处理： 自由*/
	saveData.approvestatus=approvestatus;
	let pk_tradetypeid=self.state.tradetypeData;
	saveData.pk_tradetypeid=pk_tradetypeid;
	
	//清除表体数据
	let row = evidenceData.rows[0];
	// for(let i=0;i<rows.length;i++){
	for (let bodykey in row) {
		if (isObj(row[bodykey])) {
			row[bodykey].display = '';
			row[bodykey].value = '';
		}
	}
	let details=[];
	details.push[row];
	evidenceData.rows=details;
	// }
}

function changeOrg(self,props,newValue,saveData,evidenceData){
	saveData.pk_org={
		display:newValue.display,
		value:newValue.value
	}
	let { headFunc}=self.state;
	if(newValue.value){
		ajax({
			url:'/nccloud/gl/voucher/queryaccountingbookbyorg.do',
			data:{pk_org:newValue.value},
			success: function(response) {
				const { data, success } = response;
				if (success) {
					if (data && data.accountingbook) {
						saveData.pk_accountingbook={
							display:data.accountingbook.display,
							value:data.accountingbook.value
						}
						headFunc.pk_accountingbook=saveData.pk_accountingbook
					}
					let businessInfo = getBusinessInfo();
					let currrentDate = businessInfo.businessDate.split(' ')[0];
					// let billdateObj = { billdate: { value: currrentDate } };
					//props.form.setFormItemsValue(formId, billdateObj);
					saveData.billdate = {
						display: currrentDate,
						value: currrentDate
					}
					props.form.setFormItemsValue(formId,saveData);
					self.setState({
						saveData, evidenceData,headFunc,
						pk_org: newValue.value,
						pk_unit_org: saveData.pk_org,
						getrow:0
					},()=>{
						//self.setFormItemsDefault();
						self.querybook(
							{
								pk_org:newValue.value,
								billdate:currrentDate
							}, 
							'/nccloud/gl/voucher/queryCombineInfoByOrgId.do');
					})
				}
			}
		});
	}else{
		headFunc.pk_accountingbook=saveData.pk_accountingbook
		props.form.setFormItemsValue(formId,saveData);
		self.setState({
			saveData, evidenceData,headFunc,
			pk_org: '',
			pk_unit_org: saveData.pk_org,
			getrow:0
		})
	}
	
}