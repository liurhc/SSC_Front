import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
import { high, base, ajax, deepClone, toast, createPage, getBusinessInfo, promptBox, cardCache, sum, getMultiLang,createPageIcon } from 'nc-lightapp-front';
import { isObj, convertCurrency } from '../../../public/components/oftenApi';
import {GetChinese, RemoveChinese} from '../../../public/common/stringDeal.js';
let { getCurrentLastId, getCacheById, getNextId, deleteCacheById, addCache, updateCache } = cardCache;
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCAutoComplete: AutoComplete,
	NCDropdown: Dropdown,
	NCPanel: Panel,
	NCButtonGroup: ButtonGroup,
	NCMenu: Menu,
	NCForm: Form
} = base;
import {
	CheckboxItem,
	RadioItem,
	TextAreaItem,
	ReferItem,
	SelectItem,
	InputItem,
	DateTimePickerItem
} from '../../../public/components/FormItems';
const { Refer } = high;
const { NCFormItem: FormItem } = Form;
import amountconvert from '../../../voucherBill/amountConvert';
import {queryCurrinfo} from '../../../voucherBill/amountConvert/currInfo.js';
import ChangeNumberModal from '../../../voucherBill/voucherBill_insert/container/ChangeNumberModal';
import setScale from '../../../public/common/amountFormat.js';
import AssidModal from '../../../public/components/assidDbModal';
import ApprovalTrans from '../../../../uap/public/excomponents/approvalTrans';

const Option = Select.NCOption;
import createScript from '../../../public/components/uapRefer.js';
const { Item, Divider, SubMenu, ItemGroup } = Menu;
const format = 'YYYY-MM-DD';
import './index.less';
import { onButtonClick, initTemplate, afterEvent } from './events';

const detailExpend = [
	'bankaccount',
	'billtype',
	'checkstyle',
	'checkno',
	'checkdate',
	'verifyno',
	'verifydate',
	'cashflow'
];
class VoucherEditCard extends Component {
	constructor(props) {
		super(props);
		this.appId = '20020MANGE';
		this.state = {
			json: {},
			currentRowPrevass:[],//当前行的前一行的辅助核算
			saveNumber: '', //保存新增标识
			update: false, //修改太页面标识
			isLoading: true,
			getrow: '',
			open: false, //footer默认不展示
			title: ''/*'凭证单'*/,
			index: '', //摘要信息下标
			findRows: [], //修改汇率选定行数据
			accIndex: '', //辅助核算下标
			assList: [], //根据会计科目和制单日期获取的辅助核算数据
			freeValue: [], //存储分录自定义项
			MsgModalShow: false, //控制更多模态框显隐
			MsgModalAll: false, //控制常用模态框显隐
			SelectModalShow: false,
			SaveModalShow: false,
			SubjectModalShow: false, //科目模态框
			AssistAccModalShow: false,
			ExplanationModalShow: false,
			changeNumberShow: false, //修改汇率模态框
			SaveErrorModalShow: false,
			novAuto: true, //凭证号是否自动获取的，true是自动，false为手动填的
			compositedata: null, //指派相关
			compositedisplay: null, //指派相关
			transtype: 'VRD1',
			scene: '', //场景
			isFreevalueDefault:'N',
			pk_org: '',
			id: '',
			originLoadColums: [],
			pk_unit_org: {
				display: '',
				value: ''
			},

			headFunc: {
				checkNot: false, //是否保存模态框
				pk_accountingbook: {
					//核算账簿
					display: '',
					value: ''
				}
			}, //存储表头参照
			unitOrg: {
				//业务单元过滤
				value: ''
			},
			isadd: false, //根据核算账簿动态增列判断是否已经新增
			isNC01: false, //根据核算账簿动态增列判断是否已经新增集团借贷
			isNC02: false, //根据核算账簿动态增列判断是否已经新增全局借贷
			//保存数据控制
			checkForm: false,
			//参照依赖核算账簿值获取
			// pk_unit:{//业务单元
			// 	display:'',
			// 	value:''
			// },
			//参照依赖核算账簿值获取
			pk_accountingbook: {
				//核算账簿
				display: '',
				value: ''
			},
			billdate: {
				value: ''
			},
			nov: {
				//凭证号
				value: ''
			},
			startdate: {
				//制单日期
			},
			attachment: {
				value: '0'
			}, //附单据数,
			period: {
				//会计期间
			},
			billmaker: {
				//制单人
			},
			// approver: {
			// 	//审核人
			// },
			tradetypeData: {}, //交易类型
			saveData: {
				details: []
			},
			headContent: [],
			footContent: [],
			evidenceColumns: [],
			evidenceData: {
				defaultPkOrgInfo: {
					excrate1: {
						value: ''
					},
					excrate2: {
						value: ''
					},
					excrate3: {
						value: ''
					},
					excrate1scale: {
						value: '2'
					},
					excrate2scale: {
						value: '2'
					},
					excrate3scale: {
						value: '2'
					},
					globalscale: 2,
					groupscale: 2,
					orgscale: 2,
					scale: 2,
				},
				//表体数据
				rows: [
					{
						key: 1,
						checkedNumber: false,
						isEdit: true,
						excrate1: {
							value: ''
						},
						excrate2: {
							value: ''
						},
						excrate3: {
							value: ''
						},
						excrate1scale: {
							value: '2'
						},
						excrate2scale: {
							value: '2'
						},
						excrate3scale: {
							value: '2'
						},
						globalscale: 2,
						groupscale: 2,
						orgscale: 2,
						scale: 2,
						childform: []
					}
				],
				index: 1,
				localdebitamountTotle: {
					//借方合计
					value: ''
				},
				localcreditamountTotle: {
					//贷方合计
					value: ''
				},

				groupdebitamountTotle: {
					//组织借方合计
					value: ''
				},
				groupcreditamountTotle: {
					//组织贷方合计
					value: ''
				},

				globaldebitamountTotle: {
					//集团借方合计
					value: ''
				},
				globalcreditamountTotle: {
					//集团贷方合计
					value: ''
				},
				newLine: {
					isEdit: true,
					excrate1: {
						value: ''
					},
					excrate2: {
						value: ''
					},
					excrate3: {
						value: ''
					},
					childform: [
						{
							p_location: {
								//所在地
								value: '',
								display: '',
								scale: -1
							},
							maxpledge: {
								//可质押价值
								value: '',
								display: '',
								scale: -1
							}
						}
					]
				}
			},
			options: [],
			placeholder: '',
			disabled: false,
			copyData: {}, //复制的数据
			copyRecord: '', //复制的key
			voucherStatus: 'update', //凭证单状态 update：修改态，save：保存态，view:浏览态
			copyStatus: '', //复制的凭证copy
			abandonShow: 'N', //Y是作废凭证，N取消作废
			isSingleItem: 'N',
			detailIndex: '',
			btnHideshow: 'block', //按钮显示隐藏
			billno: '', //单据号
			bill_type: '' //单据类型
		};
		this.formId = 'head';
		this.formIdtail = 'tail';
		this.dataSource = 'gl.voucherBill.voucherBill_list.cache';
	}
	// 设置表头默认
	setFormItemsDefault = () => {
		let self = this;
		// 设置默认表头信息
		//pk_tradetypeid 交易类型  billstatus 单据状态 approvestatus审批状态
		let ctrltypedata = { billstatus: { display: self.state.json['20020MANGE-000006'], value: '1' } };/* 国际化处理： 保存*/
		self.props.form.setFormItemsValue(self.formId, ctrltypedata);
		self.props.form.setFormItemsValue(self.formIdtail, ctrltypedata);
		let balanceoridata = { approvestatus: { display: self.state.json['20020MANGE-000007'], value: '-1' } };/* 国际化处理： 自由*/
		self.props.form.setFormItemsValue(self.formId, balanceoridata);
		self.props.form.setFormItemsValue(self.formIdtail, balanceoridata);
		let pk_tradetypeid = { pk_tradetypeid: self.state.tradetypeData};
		self.props.form.setFormItemsValue(self.formId, pk_tradetypeid);
		let businessInfo = getBusinessInfo();
		let billdate = {billdate: {value: businessInfo.businessDate,display:businessInfo.businessDate}};
		self.props.form.setFormItemsValue(self.formId, billdate);
	}
	// 组织信息初始化
	initOrgInfo = () => {
		let self = this;
		self.setFormItemsDefault();
		let billdate = {billdate: {value: getBusinessInfo().businessDate}};
		let { saveData, pk_org, pk_unit_org }=self.state;
		// 获取默认业务单元
		let url = '/nccloud/platform/appregister/queryappcontext.do';
		let appCode = {
			appcode: self.props.getSearchParam('c')
		}
		ajax({
			url,
			data: appCode,
			success: function(response) {
				const { data, error, success } = response;
				let _data = data;
				if (success && _data.pk_org) {
					let orgData = {pk_org: _data.pk_org};
					pk_org = _data.pk_org;
					pk_unit_org.display = _data.org_Name;
					pk_unit_org.value = _data.pk_org;
					let url = '/nccloud/gl/voucher/queryaccountingbookbyorg.do';
					ajax({
						url,
						data: orgData,
						success: function(response) {
							const { data, error, success } = response;
							if (success && JSON.stringify(data) != '{}') {
								let { headFunc } = self.state;
								let { pk_accountingbook} = headFunc;
								let url1 = '/nccloud/gl/voucher/queryCombineInfoByOrgId.do';
								let pk_accpont = {
									"pk_org": _data.pk_org,
									"date": billdate.value	
								}
								if (data && data.accountingbook) {
									saveData['pk_accountingbook'] = data.accountingbook;
									saveData['pk_org'] = {display:_data.org_Name,value:_data.pk_org}
									pk_accountingbook.value = data.accountingbook.value;
									pk_accountingbook.display = data.accountingbook.display;
								}
								let defaultorg = { pk_org: { display: pk_unit_org.display, value: pk_unit_org.value } };
								self.props.form.setFormItemsValue(self.formId, defaultorg);
								self.ifCheck(pk_accpont, url1);
								self.setState({
									pk_org,
									pk_unit_org,
									saveData,
									headFunc
								});					
							}
						}
					})
				}
			}
		});							
	};
	
	handleProvinceChange = (e) => {
		this.state.voucherStatus = 'update';
		let self = this;
	};

	searchById() {
		let self = this;
		let url = '/nccloud/platform/templet/querypage.do';
		let data = {
			pagecode: '20020MANGE_003'
		};
		ajax({
			url,
			data,
			success: function(response) {
				const { data, message, success } = response;
				if (success) {
					self.echoData(data);
					self.props.button.setButtonDisabled(['DelLine','CopyLine','CutLine','PasteLine'],true);
				} else {
					toast({ content: message.message, color: 'warning' });
				}
			}
		});
	}

	//金额显示成千分位
	formatAcuracy(value, len) {
		if (value.value === null || value.value === undefined) {
			return value.value;
		}
		return this.commafy(this.formatDot(value, len));
	}

	changemoney(evidenceData) {
		let { json } = this.state;
		if (evidenceData.localdebitamountTotle.value == evidenceData.localcreditamountTotle.value) {
			if (evidenceData.localdebitamountTotle.value < 0) {
				return json['20020MANGE-000172'] + convertCurrency(json,Math.abs(evidenceData.localcreditamountTotle.value));
			} else {
				return convertCurrency(json,evidenceData.localcreditamountTotle.value);
			}
		}
	}

	formatDot(value, len) {
		let formatVal, dotSplit, val;
		val = (value.value || 0).toString();
		dotSplit = val.split('.');
		if (dotSplit.length > 2 || !value.value) {
			return value.value;
		}
		if (value.scale && value.scale != '-1') {
			len = value.scale;
		}
		len = len || 2;
		if (val.indexOf('.') > -1) {
			formatVal = val.substring(0, val.indexOf('.') + len + 1);
		} else {
			formatVal = val;
		}
		return formatVal;
	}
	//数字转换成千分位 格式
	commafy(num) {
		let pointIndex, intPart, pointPart;
		if (isNaN(num)) {
			return '';
		}
		num = num + '';
		if (/^.*\..*$/.test(num)) {
			pointIndex = num.lastIndexOf('.');
			intPart = num.substring(0, pointIndex);
			pointPart = num.substring(pointIndex + 1, num.length);
			intPart = intPart + '';
			let re = /(-?\d+)(\d{3})/;
			while (re.test(intPart)) {
				intPart = intPart.replace(re, '$1,$2');
			}
			num = intPart + '.' + pointPart;
		} else {
			num = num + '';
			let re = /(-?\d+)(\d{3})/;
			while (re.test(num)) {
				num = num.replace(re, '$1,$2');
			}
		}
		return num;
	}
	loadCurryData = (response) => {
		let self = this;
		let { saveData, evidenceData } = self.state;
		this.updateRows(evidenceData, response.data);
		self.setState({
			evidenceData,
			saveData
		});
	};
	getCurry(book, date, values, type, key) {
		var self = this;
		let url = '/nccloud/gl/voucher/voucherValueChange.do';
		let data = {
			key: type,
			value: values.value,
			pk_accountingbook: book,
			prepareddate: date
		};
		ajax({
			url,
			data,
			success: function(response) {
				if (response.data) {
					if (type == 'pk_vouchertype') {
						self.loadCurryData(response);
					} else {
						self.updateRowScale(response, key);
					}
				}
			}
		});
	}

	//跟新计算后的值
	countRow = (originData, countValue, key) => {
		//跟新计算后的值
		let keyString;
		if (key != 'amount') {
			if (key == 'globalcreditamount' || key == 'globaldebitamount') {
				keyString = key.substring(6);
			} else {
				keyString = key.substring(5);
			}
		} else {
			keyString = 'amount';
		}
		for (let item in countValue) {
			if (
				originData.hasOwnProperty(item) ||
				item.indexOf('localamount') != -1 ||
				item.indexOf('groupamount') != -1 ||
				item.indexOf('globalamount') != -1 ||
				item.indexOf('quantity') != -1 ||
				item.indexOf('price') != -1
			) {
				if (item == 'quantity' && originData.debitquantity) {
					//数量特殊处理
					originData.debitquantity = {
						...originData.debitquantity,
						value: countValue.quantity
					};
				} else {
					originData[item] = {
						...originData[item],
						value: countValue[item]
					};
				}
				if (item == 'localamount') {
					if (keyString != 'creditamount' && keyString != 'debitamount') {
						if (originData.localcreditamount.value) {
							originData.localcreditamount = {
								...originData.localcreditamount,
								value: countValue[item]
							};
						} else {
							originData.localdebitamount = {
								...originData.localdebitamount,
								value: countValue[item]
							};
						}
					} else {
						if (keyString == 'creditamount') {
							originData.localcreditamount = {
								...originData.localcreditamount,
								value: countValue[item]
							};
							originData.localdebitamount = {
								...originData.localdebitamount,
								value: ''
							};
						} else {
							originData.localdebitamount = {
								...originData.localdebitamount,
								value: countValue[item]
							};
							originData.localcreditamount = {
								...originData.localcreditamount,
								value: ''
							};
						}
					}
				}
				if (item == 'groupamount') {
					if (keyString != 'creditamount' && keyString != 'debitamount') {
						if (originData.groupcreditamount&&originData.groupcreditamount.value) {
							originData.groupcreditamount = {
								...originData.groupcreditamount,
								value: countValue[item]
							};
							originData.groupdebitamount = {
								...originData.groupdebitamount,
								value: ''
							};
						} else {
							originData.groupdebitamount = {
								...originData.groupdebitamount,
								value: countValue[item]
							};
							originData.groupcreditamount = {
								...originData.groupcreditamount,
								value: ''
							};
						}
					} else {
						if (keyString == 'creditamount') {
							originData.groupcreditamount = {
								...originData.groupcreditamount,
								value: countValue[item]
							};
							originData.groupdebitamount = {
								...originData.groupdebitamount,
								value: ''
							};
						} else {
							originData.groupcreditamount = {
								...originData.groupcreditamount,
								value: ''
							};
							originData.groupdebitamount = {
								...originData.groupdebitamount,
								value: countValue[item]
							};
						}
					}
				}
				if (item == 'globalamount') {
					if (keyString != 'creditamount' && keyString != 'debitamount') {
						if (originData.globalcreditamount&&originData.globalcreditamount.value) {
							originData.globalcreditamount = {
								...originData.globalcreditamount,
								value: countValue[item]
							};
							originData.globaldebitamount = {
								...originData.globaldebitamount,
								value: ''
							};
						} else {
							originData.globaldebitamount = {
								...originData.globaldebitamount,
								value: countValue[item]
							};
							originData.globalcreditamount = {
								...originData.globalcreditamount,
								value: ''
							};
						}
					} else {
						if (keyString == 'creditamount') {
							//全局特殊处理
							originData.globalcreditamount = {
								...originData.globalcreditamount,
								value: countValue[item]
							};
							originData.globaldebitamount = {
								...originData.globaldebitamount,
								value: ''
							};
						} else {
							originData.globalcreditamount = {
								...originData.globalcreditamount,
								value: ''
							};
							originData.globaldebitamount = {
								...originData.globaldebitamount,
								value: countValue[item]
							};
						}
					}
				}
			}
		}
	};

	//凭证分录等号操作
	equal = (code, originData) => {
		let {
			rows,
			localcreditamountTotle,
			localdebitamountTotle,
			globalcreditamountTotle,
			globaldebitamountTotle,
			groupcreditamountTotle,
			groupdebitamountTotle
		} = this.state.evidenceData;
		switch (code) {
			case 'localcreditamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[code] = {
						...originData[code],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: localdebitamountTotle.value - localcreditamountTotle.value
					};
				} else {
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: localdebitamountTotle.value - localcreditamountTotle.value
					};
				}
				break;
			case 'localdebitamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[code] = {
						...originData[code],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: localcreditamountTotle.value - localdebitamountTotle.value
					};
				} else {
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: localcreditamountTotle.value - localdebitamountTotle.value
					};
				}
				break;

			case 'groupcreditamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[code] = {
						...originData[code],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: groupdebitamountTotle.value - groupcreditamountTotle.value
					};
				} else {
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: groupdebitamountTotle.value - groupcreditamountTotle.value
					};
					originData.groupdebitamount = {
						...originData.groupdebitamount,
						value: ''
					};
				}
				break;
			case 'groupdebitamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[code] = {
						...originData[code],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: groupcreditamountTotle.value - groupdebitamountTotle.value
					};
				} else {
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: groupcreditamountTotle.value - groupdebitamountTotle.value
					};
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: ''
					};
				}
				break;
			case 'globalcreditamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[code] = {
						...originData[code],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: globaldebitamountTotle.value - globalcreditamountTotle.value
					};
				} else {
					this.totleCount(code, 'localdebitamountTotle');
					originData.globaldebitamount = {
						...originData.globaldebitamount,
						value: ''
					};
					originData[code] = {
						...originData[code],
						value: globaldebitamountTotle.value - globalcreditamountTotle.value
					};
				}
				break;
			case 'globaldebitamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[item.attrcode] = {
						...originData[item.attrcode],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: globalcreditamountTotle.value - globaldebitamountTotle.value
					};
				} else {
					this.totleCount(code, 'localdebitamountTotle');
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: ''
					};
					originData[code] = {
						...originData[code],
						value: globalcreditamountTotle.value - globaldebitamountTotle.value
					};
				}
				break;
			default:
				break;
		}
	};

	//凭证分录录入空格操作
	emptyEnter = (code, originData) => {
		switch (code) {
			case 'localcreditamount':
				if (originData[code].value) {
					if (originData.groupdebitamount) {
						originData.groupdebitamount = {
							//集团空格
							...originData.groupdebitamount,
							value: originData.groupcreditamount && originData.groupcreditamount.value
						};
					}
					if (originData.groupcreditamount) {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: ''
						};
					}
	
					if (originData.globaldebitamount) {
						originData.globaldebitamount = {
							//全局
							...originData.globaldebitamount,
							value: originData.globalcreditamount && originData.globalcreditamount.value
						};
					}
	
					if (originData.globalcreditamount) {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: ''
						};
					}
	
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: originData[code].value
					};
					originData[code] = {
						...originData[code],
						value: ''
					};
				} else {
					if (originData.groupcreditamount) {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: originData.groupdebitamount && originData.groupdebitamount.value
						};
					}
					if (originData.groupdebitamount) {
						originData.groupdebitamount = {
							//集团空格
							...originData.groupdebitamount,
							value: ''
						};
					}
	
					if (originData.globalcreditamount) {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: originData.globaldebitamount && originData.globaldebitamount.value
						};
					}
	
					if (originData.globaldebitamount) {
						originData.globaldebitamount = {
							//全局
							...originData.globaldebitamount,
							value: ''
						};
					}
	
					originData[code] = {
						...originData[code],
						value: originData.localdebitamount.value
					};
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: ''
					};
				}
				break;
			case 'localdebitamount':
				if (originData[code].value) {
					if (originData.groupcreditamount) {
						//组织集团缺乏判断后续添加
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: originData.groupdebitamount && originData.groupdebitamount.value
						};
					}
	
					if (originData.groupdebitamount) {
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: ''
						};
					}
	
					if (originData.globalcreditamount) {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: originData.globaldebitamount && originData.globaldebitamount.value
						};
					}
	
					if (originData.globaldebitamount) {
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: ''
						};
					}
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: originData[code].value
					};
					originData[code] = {
						...originData[code],
						value: ''
					};
				} else {
					if (originData.groupdebitamount) {
						originData.groupdebitamount = {
							//集团空格
							...originData.groupdebitamount,
							value: originData.groupcreditamount && originData.groupcreditamount.value
						};
					}
					if (originData.groupcreditamount) {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: ''
						};
					}
	
					if (originData.globaldebitamount) {
						originData.globaldebitamount = {
							//全局
							...originData.globaldebitamount,
							value: originData.globalcreditamount && originData.globalcreditamount.value
						};
					}
					if (originData.globalcreditamount) {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: ''
						};
					}
	
					originData[code] = {
						...originData[code],
						value: originData.localcreditamount.value
					};
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: ''
					};
				}
				break;
	
			case 'groupcreditamount':
				if (originData[code].value) {
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: originData.localcreditamount && originData.localcreditamount.value
					};
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: ''
					};
					originData.globaldebitamount = {
						...originData.globaldebitamount,
						value: originData.globalcreditamount && originData.globalcreditamount.value
					};
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: ''
					};
					originData.groupdebitamount = {
						...originData.groupdebitamount,
						value: originData[code].value
					};
					originData[code] = {
						...originData[code],
						value: ''
					};
				} else {
					originData[code] = {
						...originData[code],
						value: originData.groupdebitamount && originData.groupdebitamount.value
					};
					originData.groupdebitamount = {
						...originData.groupdebitamount,
						value: ''
					};
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: originData.localdebitamount && originData.localdebitamount.value
					};
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: ''
					};
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: originData.globaldebitamount && originData.globaldebitamount.value
					};
					originData.globaldebitamount = {
						...originData.globaldebitamount,
						value: ''
					};
				}
				break;
			case 'groupdebitamount':
				if (originData[code].value) {
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: originData[code].value
					};
					originData[code] = {
						...originData[code],
						value: ''
					};
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: originData.localdebitamount && originData.localdebitamount.value
					};
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: ''
					};
					originData.globalcreditamount = {
						//全局
						...originData.globalcreditamount,
						value: originData.globaldebitamount && originData.globaldebitamount.value
					};
					originData.globaldebitamount = {
						...originData.globaldebitamount,
						value: ''
					};
				} else {
					originData[code] = {
						...originData[code],
						value: originData.groupcreditamount && originData.groupcreditamount.value
					};
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: ''
					};
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: originData.localcreditamount && originData.localcreditamount.value
					};
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: ''
					};
					originData.globaldebitamount = {
						...originData.globaldebitamount,
						value: originData.globalcreditamount && originData.globalcreditamount.value
					};
	
					originData.globalcreditamount = {
						//全局
						...originData.globalcreditamount,
						value: ''
					};
				}
	
				break;
			case 'globalcreditamount':
				if (originData[code].value) {
					originData.globaldebitamount = {
						...originData.globaldebitamount,
						value: originData[code].value
					};
					originData[code] = {
						...originData[code],
						value: ''
					};
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: originData.localcreditamount && originData.localcreditamount.value
					};
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: ''
					};
					originData.groupdebitamount = {
						...originData.groupdebitamount,
						value: originData.groupcreditamount && originData.groupcreditamount.value
					};
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: ''
					};
				} else {
					originData[code] = {
						...originData[code],
						value: originData.globaldebitamount && originData.globaldebitamount.value
					};
					originData.globaldebitamount = {
						...originData.globaldebitamount,
						value: ''
					};
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: originData.localdebitamount && originData.localdebitamount.value
					};
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: ''
					};
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: originData.groupdebitamount && originData.groupdebitamount.value
					};
					originData.groupdebitamount = {
						...originData.groupdebitamount,
						value: ''
					};
				}
				break;
	
			case 'globaldebitamount':
				if (originData[code].value) {
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: originData[code].value
					};
					originData[code] = {
						...originData[code],
						value: ''
					};
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: originData.localdebitamount && originData.localdebitamount.value
					};
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: ''
					};
					originData.groupcreditamount = {
						//全局
						...originData.groupcreditamount,
						value: originData.groupdebitamount && originData.groupdebitamount.value
					};
					originData.groupdebitamount = {
						...originData.groupdebitamount,
						value: ''
					};
				} else {
					originData[code] = {
						...originData[code],
						value: originData.globalcreditamount && originData.globalcreditamount.value
					};
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: ''
					};
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: originData.localcreditamount && originData.localcreditamount.value
					};
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: ''
					};
					originData.groupdebitamount = {
						...originData.groupdebitamount,
						value: originData.groupcreditamount && originData.groupcreditamount.value
					};
					originData.groupcreditamount = {
						//全局
						...originData.groupcreditamount,
						value: ''
					};
				}
				break;
	
			default:
				break;
		}
	};

	//根据模板数据渲染表格列
	editeColums(data) {
		let dataIndex = {
			title: this.state.json['20020MANGE-000020'],/* 国际化处理： 序号*/
			key: 'key',
			dataIndex: 'key',
			width: '80px',
			render: (text, record, index) => {
				return <span>{index + 1}</span>;
			}
		};
		data.map((item, index) => {
			//if(!item.visible) return;
			item.title = item.label;
			item.dataIndex = item.attrcode;
			if (item.attrcode == 'detailindex') {
				item.width = '60px';
			} else if (item.attrcode == 'assid'){
				item.width = '200px';
			} else if (item.attrcode == 'pk_currtype'){
				item.width = '130px';
			} else {
				item.width = '180px';
			}
			item.render = (text, record, index) => {
				//根据字段类型，渲染不同的组件
				let self = this;
				let { rows } = self.state.evidenceData;
				//let record = self.findByKey(record.key, rows);
				//let originData = record;
				let errorMsg, errorBorder;
				let { evidenceColumns } = self.state;
				let { data, currInfo } = self.exportData(record);
				let defaultValue = {};
				let { billdate, saveData, isNC01, isNC02 } = self.state;
				if (text && text.value !== null) {
					defaultValue = { refname: text.display, refpk: text.value };
				}
				switch (item.itemtype) {
					case 'input':
						// if (item.attrcode == 'explanation') {
						// 	let { value } = record && record.explanation ? record.explanation : {display:'',value:''};
						// 	let { options, placeholder, disabled } = self.state;
						// 	return (
						// 		<AutoComplete
						// 			value={value}
						// 			disabled={disabled}
						// 			options={options}
						// 			placeholder={this.state.json['20020MANGE-000181']}/* 国际化处理： 请输入摘要*/
						// 			maxlength="200"
						// 			onKeyDown={(event) => {}}
						// 			onClick={(value) => {}}
						// 			// onValueChange={(value) => {
						// 			// 	let { rows } = this.state.evidenceData;
						// 			// 	let originData = this.findByKey(record.key, rows);
						// 			// 	if (originData) {
						// 			// 		originData[item.attrcode] = {
						// 			// 			value: value
						// 			// 		};
						// 			// 	}
						// 			// }}
						// 		/>
						// 	);
						// } else 
						if (item.attrcode == 'assid') {
							return (
								<div  className="refer-input-wraper" >
									<FormControl
										value={record.assid ? record.assid.display : ''}
										disabled={true}
										onFocus={this.focus}
										onChange={(v) => {
											let cloneNewLine = deepClone(evidenceColumns);
											cloneNewLine.splice(4, 0, number1);
											this.setState({
												evidenceColumns: cloneNewLine
											});
										}}
									/>
									<i
										className="iconfont icon-canzhaozuixin"
										onClick={this.handleAssistAcc.bind(this, record.key)}
									/>
								</div>
							);
						} else {
							return(
								<FormControl 
									name={item.attrcode}
									className = "input_new"
									type="customer"
									defaultValue={record[item.attrcode].value}
									value = {this.state.evidenceData.rows[index][item.attrcode].value}
									onFocus= {(e)  => {
										e.target.addEventListener(
											"compositionstart",
											e => {
													e.target.isFlag = '1';
											},
											false
										);
										e.target.addEventListener(
											"compositionend",
											e => {
											e.target.isFlag = '0';
											let v = e.target.defaultValue;
											// e.target && e.target.blur();
											if(v&&v.length>0){
												if(GetChinese(v).length>0&&2*(GetChinese(v).length)>Number(item.maxlength)){
													toast({ content:this.state.json['20020MANGE-000183'] +  item.maxlength, color: 'danger',mark:'search_data' });//输入的数字长度不能超过用户设置的长度
														v=v.slice(0,Number(item.maxlength/2));
														// return false;
												}else if(GetChinese(v).length>0&&2*(GetChinese(v).length)+RemoveChinese(v).length>Number(item.maxlength)){
													toast({ content:this.state.json['20020MANGE-000183'] +  item.maxlength, color: 'danger',mark:'search_data' });//输入的数字长度不能超过用户设置的长度
														let chineseData=GetChinese(v);
													if(2*(chineseData.length)<10){
														let chartData= RemoveChinese(v);
														if(chartData.length>0){
															v=chineseData.slice(0,Number(item.maxlength/2))+chartData.slice(0,item.maxlength-2*(chineseData.length));
														}else{
														v=v.slice(0,Number(item.maxlength/2));
														}
													}else{
														v=v.slice(0,Number(item.maxlength/2));
													}
														// return false;
												}else if(RemoveChinese(v).length>Number(item.maxlength)){
													toast({ content:this.state.json['20020MANGE-000183'] +  item.maxlength, color: 'danger',mark:'search_data' });//输入的数字长度不能超过用户设置的长度
														v=v.slice(0,Number(item.maxlength));
														// return false;
												}
											}
											record[item.attrcode] = {
												value: v
											};
											this.setState({
												voucherStatus: 'update',
												evidenceData: this.state.evidenceData
											});
										}
										)
									}}
									onChange={(v,e) => {
										
										if(e.target.isFlag=='0'||!e.target.isFlag){
										if(v && v.length > 0){
                                            if(2 * (GetChinese(v).length) > Number(item.maxlength)){
												toast({ content:this.state.json['20020MANGE-000183'] +  item.maxlength, color: 'danger',mark:'search_data' });/*国际化处理：提示.最大录入长度为: */
                                                v = v.slice(0,Number(item.maxlength / 2));
                                            }else if(2*(GetChinese(v).length) + RemoveChinese(v).length > Number(item.maxlength)){
                                                toast({ content:this.state.json['20020MANGE-000183'] +  item.maxlength, color: 'danger',mark:'search_data' });/*国际化处理：提示.最大录入长度为: */
												// v = RemoveChinese(v);
												let chineseData=GetChinese(v);
													if(2*(chineseData.length)<10){
														let chartData= RemoveChinese(v);
														if(chartData.length>0){
															v=chineseData.slice(0,Number(item.maxlength/2))+chartData.slice(0,item.maxlength-2*(chineseData.length));
														}else{
														v=v.slice(0,Number(item.maxlength/2));
														}
													}else{
														v=v.slice(0,Number(item.maxlength/2));
													}
                                            }else if(RemoveChinese(v).length > Number(item.maxlength)){
                                                toast({ content:this.state.json['20020MANGE-000183'] +  item.maxlength, color: 'danger',mark:'search_data' });/*国际化处理：提示.最大录入长度为: */
												v = v.slice(0,Number(item.maxlength));
                                            }
										}
									}
										record[item.attrcode] = {
											value: v
										};
										this.setState({
											voucherStatus: 'update',
											evidenceData: this.state.evidenceData
										});
									}}
								/>
							)
						}
						break;
					case 'refer':
						let referUrl = item.refcode + '.js';
						let pk_accountingbookValue = self.state.headFunc.pk_accountingbook.value;
						let datestr = self.props.form.getFormItemsValue(this.formId,'billdate');
						if(datestr && datestr.value){
							datestr = datestr.value.split(' ')[0];
						} else {
							let businessInfo = getBusinessInfo();
							let buziDate = businessInfo.businessDate;
							if (buziDate) {
								datestr = buziDate.split(' ')[0];
							}
						}
						if (!self.state[item.attrcode]) {
							{
								createScript.call(self, referUrl, item.attrcode);
							}
							return <div />;
						} else {
							return self.state[item.attrcode] ? (
								self.state[item.attrcode]({
									value: { refname: text && text.display ? text.display : '', refpk: text && text.value ? text.value : ''},
									onlyLeafCanSelect : item.attrcode == 'pk_accasoa' ? true : false,
									queryCondition: () => {
										if (item.attrcode == 'pk_accasoa') {
											return {
												pk_accountingbook: pk_accountingbookValue,
												dateStr: datestr,
												isDataPowerEnable: 'Y',
												DataPowerOperationCode: 'fi'
											};
										} else {
											return {
												pk_org:self.state.pk_org,
												pk_defdoclist:item.pk_defdoclist,
												DataPowerOperationCode:item.dataPowerOperationCode?item.dataPowerOperationCode:'fi',
												isDataPowerEnable:item.isDataPowerEnable
											};
										}
									},
									onFocus: (e) => {
										let { prepareddate, evidenceData, headFunc, freeValue } = this.state;
										if (item.attrcode == 'pk_accasoa') {
											if (!headFunc.pk_accountingbook.value) {
												toast({ content: this.state.json['20020MANGE-000180'] + this.state.json['20020MANGE-000040'], color: 'warning' });/* 国际化处理： 请先选择业务单元*/
												return false;
											}
										}
										this.getRow(evidenceData.rows[index],index);
									},
									onChange: (v) => {
										let { billdate, evidenceData, headFunc } = this.state;
										let originData = this.findByKey(record.key, evidenceData.rows);
										if (originData) {
											originData[item.attrcode] = {
												display: item.attrcode == 'pk_accasoa' ? (v.dispname ? v.dispname : (v.nodeData ? v.nodeData.dispname : '')) : v.refname,
												value: v.refpk
											};
										}
										//根据会计科目请求辅助核算
										if (item.attrcode == 'pk_accasoa') {
											if (v) {
												if (v && (evidenceData.rows[index].assid.value || (evidenceData.rows[index].ass && evidenceData.rows[index].ass.length != 0))) {
													//切换会计科目清空辅助核算
													evidenceData.rows[index].ass = [];
													evidenceData.rows[index].assid = {
														...evidenceData.rows[index].assid,
														display: '',
														value: ''
													};
												}
												if(v && v.nodeData){
													this.ifNumber(v.nodeData.unit, index);
												} else {
													this.ifNumber(false,index);
												}
												let queryArry = [];
												queryArry.push(v.refpk);
												//切换科目后处理币种，精度
												this.getCurry(
													pk_accountingbookValue,
													billdate.value,
													{ value: queryArry },
													'pk_accasoa',
													index
												);
											}
										}

										//根据币种请求汇率
										if (item.attrcode == 'pk_currtype') {
											let url = '/nccloud/gl/voucher/ratequerybyorg.do';
											let pk_currtype = {
												pk_org: self.state.pk_org,
												pk_currtype: v.refpk,
												prepareddate: billdate.value
											};
											if (headFunc.pk_accountingbook.value && v.refpk && billdate.value) {
												this.queryRate(pk_currtype, url, originData);
											}
										}
										this.setState({
											voucherStatus: 'update',
											evidenceData
										});
									}
								})
							) : (
								<div />
							);
						}
						break;
					case 'ass':
						return (
							<div>
								<FormControl
									//	value={record.assid.display||''}
									disabled={true}
									onFocus={this.focus}
									onChange={(v) => {
										let cloneNewLine = deepClone(evidenceColumns);
										cloneNewLine.splice(4, 0, number1);
										this.setState({
											voucherStatus: 'update',
											evidenceColumns: cloneNewLine
										});
									}}
								/>

								<i
									className="iconfont icon-gengduo"
									onClick={this.handleAssistAcc.bind(this, record.key)}
								/>
							</div>
						);
					case 'number':
						if (item.attrcode == 'amount') {
							return (
								<div className="refer-input-wraper" >
									<NCNumber
										scale={record.amount&&record.amount.scale ? Number(record.amount.scale) : Number(record.scale)}
										value={text?text.value:''}
										onFocus={() => {
											let _this = this;
											let { prepareddate, evidenceData, headFunc, freeValue } = _this.state;
										}}
										placeholder={this.state.json['20020MANGE-000021']}/* 国际化处理： 请输入数字*/
										onKeyDown={(event) => {
											
										}}
										onBlur={(v) => {
											if (v) {
												let countValue = amountconvert(data, currInfo, 'amount');
												if (countValue.message) {
													toast({ content: countValue.message, color: 'warning' });
												}
												this.countRow(record, countValue, 'amount');
											}
											this.totleCount(item.attrcode, 'localdebitamountTotle');
										}}
										onChange={(v) => {
											if (record) {
												record[item.attrcode].value = v;
											}
											this.setState({
												evidenceData: this.state.evidenceData
											});
										}}
									/>
									<i
										className="iconfont icon-canzhaozuixin"
										onClick={this.changeNumberClick.bind(this, record.key)}
									/>
								</div>
							);
						} else if (item.attrcode == 'localdebitamount' || item.attrcode == 'localcreditamount') {
							let debitScale = record.localdebitamount&&record.localdebitamount.scale ? Number(record.localdebitamount.scale) :  Number(record.orgscale),
							creditScale = record.localcreditamount&&record.localcreditamount.scale ? Number(record.localcreditamount.scale) :  Number(record.orgscale)
							let scale = item.attrcode == 'localdebitamount' ? debitScale : creditScale;
							return (
								<div className="textCenter">
									<NCNumber
										scale={scale}
										placeholder={this.state.json['20020MANGE-000021']}/* 国际化处理： 请输入数字*/
										value={text && text.value && text.value != '0' ? text.value : ''}
										onFocus={(v) => {
											record[item.attrcode].visible = true;
										}}
										onBlur={(v) => {
											record[item.attrcode].visible = false;
											if (v && v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
												data.localamount = item.attrcode == 'localdebitamount' ? record.localdebitamount.value : record.localcreditamount.value;
												let countValue = amountconvert(data, currInfo, item.attrcode);
												if (countValue.message) {
													toast({
														content: countValue.message,
														color: 'warning',
														duration: 5
													});
												}
												this.countRow(record, countValue, item.attrcode);
											}
											this.totleCount(item.attrcode, item.attrcode + 'Totle');
										}}
										onKeyUp={(event) => {
											if (
												event.keyCode == '187' ||
												(event.keyCode == 61 && record.excrate1.value)
											) {
												this.equal(item.attrcode, record);
												// let { data, currInfo } = this.exportData(record);
												// let countValue = amountconvert(data, currInfo, item.attrcode);
												// this.countRow(record, countValue,  item.attrcode);
												this.totleCount(item.attrcode, 'localdebitamountTotle');
											} else if (event.keyCode == '32' && record.excrate1.value) {
												this.emptyEnter(item.attrcode, record);
												this.totleCount(item.attrcode, 'localdebitamountTotle');
											}
										}}
										onChange={(v) => {
											let self = this;
											let { evidenceData } = self.state;
											if (v && v !== '=' && v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
												if (record) {
													record[item.attrcode].value = v
												}
												this.setState({
													evidenceData
												});
											}
										}}
									/>
								</div>
							);
						} else if (item.attrcode == 'detailindex') {
							return <span>{index + 1}</span>;
						} else if (item.attrcode == 'groupdebitamount' || item.attrcode == 'groupcreditamount' || item.attrcode == 'globalcreditamount' || item.attrcode == 'globaldebitamount') {
							let groupDebitScale = record.groupdebitamount&&record.groupdebitamount.scale ? Number(record.groupdebitamount.scale) :  (record.groupscale ? Number(record.groupscale) : (record.localdebitamount&&record.localdebitamount.scale ? Number(record.localdebitamount.scale) :  Number(record.orgscale))),
							groupCreditScale = record.groupcreditamount&&record.groupcreditamount.scale ? Number(record.groupcreditamount.scale) :  (record.groupscale ? Number(record.groupscale) : (record.localcreditamount&&record.localcreditamount.scale ? Number(record.localcreditamount.scale) :  Number(record.orgscale))),
							globalCreditScale = record.globalcreditamount&&record.globalcreditamount.scale ? Number(record.globalcreditamount.scale) : (record.globalscale ? Number(record.globalscale): (record.localcreditamount&&record.localcreditamount.scale ? Number(record.localcreditamount.scale) :  Number(record.orgscale))),
							globalDebitScale = record.globaldebitamount&&record.globaldebitamount.scale ? Number(record.globaldebitamount.scale) :  (record.globalscale ? Number(record.globalscale) : (record.localdebitamount&&record.localdebitamount.scale ? Number(record.localdebitamount.scale) :  Number(record.orgscale)));
							let scale = '';
							if (item.attrcode == 'groupdebitamount') {
								scale = groupDebitScale;
							} else if (item.attrcode == 'groupcreditamount') {
								scale = groupCreditScale;
							} else if (item.attrcode == 'globalcreditamount') {
								scale = globalCreditScale;
							} else if (item.attrcode == 'globaldebitamount') {
								scale = globalDebitScale;
							}
							return (
								<div className="textCenter">
									<NCNumber
										scale={scale}
										value={text && text.value && text.value != '0' ? text.value : ''}
										placeholder={this.state.json['20020MANGE-000021']}/* 国际化处理： 请输入数字*/
										onBlur={(v) => {
										}}
										onChange={(v) => {
											record[item.attrcode].value = v;
											this.setState({
												voucherStatus: 'update',
												evidenceData: this.state.evidenceData
											});
										}}
									/>
								</div>
							);
						} else {
							return (
								<div className="refer-input-wraper" >
									<NCNumber
										scale={item.scale?item.scale:2}
										maxlength={item.maxlength}
										value={text && text.value ? text.value : ''}
										onFocus={this.focus}
										placeholder={true ? this.state.json['20020MANGE-000021'] : ''}/* 国际化处理： 请输入数字*/
										onBlur={(v) => {
										}}
										onChange={(v) => {
											record[item.attrcode].value = v;
											this.setState({
												voucherStatus: 'update',
												evidenceData: this.state.evidenceData
											});
										}}
									/>
								</div>
							)
						}
						break;
					case 'num':
						return (
							<div className="textCenter">
								<FormControl
									onFocus={this.focus}
									// value={text[item.attrcode].value || ''}
									onChange={(v) => {
										let { rows } = this.state.evidenceData;
										let originData = this.findByKey(record.key, rows);
										if (originData) {
											originData.localcreditamount = {
												value: Number(v)
											};
											originData.localdebitamount = {
												value: ''
											};
											originData.amount = {
												value: Number(v)
											};
										}
									}}
								/>
								{errorMsg}
							</div>
						);

					case 'textField':
						return (
							<div className="textCenter">
								<FormControl
									onFocus={this.focus}
									value={text[item.attrcode] ? text[item.attrcode].value : ''}
									onChange={(v) => {
										let { rows } = this.state.evidenceData;
										let originData = this.findByKey(record.key, rows);
										if (originData) {
											originData.localcreditamount = {
												value: v
											};
											originData.localdebitamount = {
												value: ''
											};
											originData.amount = {
												value: v
											};
										}
									}}
								/>
								{errorMsg}
							</div>
						);
					default:
						return <div />;
						break;
				}
			};
		});
		return data;
	}

	//表格参数整理
	exportData = (originData) => {
		let data = {
			amount: originData.amount ? originData.amount.value : '', //原币
			localamount:
			originData.localcreditamount&&originData.localcreditamount.value ||  originData.localdebitamount&&originData.localdebitamount.value
					? originData.localcreditamount.value || originData.localdebitamount.value
					: '', //组织本币
			groupamount:
			originData.groupcreditamount&&originData.groupcreditamount.value || originData.groupdebitamount&&originData.groupdebitamount.value
					? originData.groupcreditamount.value || originData.groupdebitamount.value
					: '', //集团本币
			globalamount:
			originData.globalcreditamount&&originData.globalcreditamount.value || originData.globaldebitamount&&originData.globaldebitamount.value
					? originData.globalcreditamount.value || originData.globaldebitamount.value
					: '', //全局本币
			pk_currtype: originData.pk_currtype ? originData.pk_currtype.value : '', //原币币种
			orgcurrtype: originData.orgcurrtype ? originData.orgcurrtype.value : '', //本币币种
			groupcurrtype: originData.groupcurrtype ? originData.groupcurrtype.value : '', //集团本币币种
			globalcurrtype: originData.globalcurrtype ? originData.globalcurrtype.value : '', //全局本币币种
			excrate1: originData.excrate1 ? originData.excrate1.value : '', //本币汇率
			excrate2: originData.excrate2 ? originData.excrate2.value : '', //集团本币汇率
			excrate3: originData.excrate3 ? originData.excrate3.value : '', //全局本币汇率
			price: originData.price ? originData.price.value : '', //单价
			pk_accountingbook: originData.pk_accountingbook ? originData.pk_accountingbook.value : '', //账簿
			pk_org: originData.pk_org ? originData.pk_org.value : '',
			prepareddate: originData.billdate ? originData.billdate.value : '', //制单日期
			quantity: originData.debitquantity ? originData.debitquantity.value : '' //数量分借贷，后续处理
		};
		let currInfo = {
			roundtype: originData.roundtype ? originData.roundtype.value : '', //原币进舍规则
			orgroundtype: originData.orgroundtype ? originData.orgroundtype.value : '', //本币进舍规则
			grouproundtype: originData.grouproundtype ? originData.grouproundtype.value : '', //集团本币进舍规则
			globalroundtype: originData.globalroundtype ? originData.globalroundtype.value : '', //全局本币进舍规则
			pricescale: originData.pricescale ? originData.pricescale.value : '', //单价精度
			priceroundtype: originData.priceroundtype ? originData.priceroundtype.value : '', //单价进舍规则
			scale: originData.scale ? originData.scale : '', //原币精度
            quantityscale: (this.props.getUrlParam('status') == 'copy' || this.props.getUrlParam('status') == 'edit') 
				? (originData.localcreditamount&&originData.localcreditamount.value
					? (originData.creditquantity&&originData.creditquantity.scale ? originData.creditquantity.scale : '') 
					: (originData.debitquantity&&originData.debitquantity.scale?originData.debitquantity.scale:'')
				) 
				: (originData.quantityscale ? originData.quantityscale.value: ''), //数量精度
			excrate1scale: originData.excrate1scale ? originData.excrate1scale.value : '', //本币汇率
			excrate2scale: originData.excrate2scale ? originData.excrate2scale.value : '', //本币汇率
			excrate3scale: originData.excrate3scale ? originData.excrate3scale.value : '', //本币汇率
			orgscale: originData.orgscale ? originData.orgscale : '', //组织本币精度
			groupscale: originData.groupscale ? originData.groupscale : '', //集团本币精度
			globalscale: originData.globalscale ? originData.globalscale : '', //全局本币精度
			maxconverr: originData.maxconverr ? originData.maxconverr.value : '',
			NC001: originData.groupType ? originData.groupType : '', //集团本币计算方式
			NC002: originData.globalType ? originData.globalType : '', //全局本币计算方式
			orgmode: originData.orgmode ? originData.orgmode.value : '', //汇率计算方式 是否为除 true=除
			groupmode: originData.groupmode
				? originData.groupmode.value === undefined ? '' : originData.groupmode.value
				: '', //集团本币汇率计算方式 是否为除 true=除
			globalmode: originData.globalmode
				? originData.globalmode.value === undefined ? '' : originData.globalmode.value
				: '' //全局本币汇率计算方式 是否为除 true=除
		};

		return { data, currInfo };
	};

	//修改页面数据加载渲染
	echoData(data) {
		let self = this;
		let {
			freeValue,
			attachment,
			evidenceData,
			evidenceColumns,
			headContent,
			footContent,
			saveData,
			tradetypeData,
			billdate,
			isNC01,
			isNC02,
			originLoadColums
		} = self.state;
		let loadColums = data.details.items;
		//根据列key 渲染加载的rows
		evidenceData.rows.map((item, index) => {
			loadColums.forEach(function(v, i, a) {
				if (v.attrcode == 'explanation') {
					v.itemType = 'explanation';
				}
				if (detailExpend.includes(v.attrcode)) {
					//结算 往来核销 现金流量
					freeValue.push(v);
				}
				if (v.attrcode.indexOf('freevalue') != -1) {
					//分录自定义项
					freeValue.push(v);
				}
				// 当前行添加信息
				item[v.attrcode] = {
					display: '',
					value: ''
				};
				//新增行添加列信息
				evidenceData.newLine[v.attrcode] = {
					display: '',
					value: ''
				};
			});
		});
		originLoadColums = deepClone(loadColums);
		loadColums = loadColums.filter(function(v, i, a) {
			return v.attrcode.indexOf('freevalue') == -1 && !detailExpend.includes(v.attrcode) && v.visible == true;
		});
		evidenceColumns = self.editeColums(loadColums);
		let group = evidenceColumns.find((item, i) => item.attrcode == 'groupdebitamount');
		let global = evidenceColumns.find((item, i) => item.attrcode == 'globaldebitamount');
		if (group && JSON.stringify(group) != '{}') {
			isNC01 = true;
		}
		if (global && JSON.stringify(global) != '{}') {
			isNC02 = true;
		}
		headContent = [];
		data.head.items.map((item, i) => {
			if (item.visible || item.attrcode == 'pk_accountingbook' || item.attrcode == 'year') {
				headContent.push(item);
			}
		});

		//修改模板数据为保存数据格式
		headContent.forEach(function(v, i, a) {
			let objData = {
				display: '',
				value: ''
			};
			if (v.attrcode == 'pk_tradetypeid') {
				objData = {
					display: tradetypeData.display,
					value: tradetypeData.value
				};
			}
			if (v.attrcode == 'billdate') {
				let businessInfo = getBusinessInfo()
				let buziDate = businessInfo.businessDate;
				let datestr = buziDate.split(' ')[0];
				objData.value = datestr;
				billdate.value = datestr;
			} else if (v.attrcode == 'billstatus') {
				objData.value = '1';
			} else if (v.attrcode == 'approvestatus') {
				objData.value = '-1';
			}
			saveData[v.attrcode] = objData;
		});
		footContent = data.tail.items;
		footContent.forEach(function(v, i, a) {
			let objData = {
				display: '',
				value: ''
			};
			saveData[v.attrcode] = objData;
		});
		
		self.setState({
			isNC01,
			isNC02,
			freeValue,
			evidenceColumns,
			evidenceData,
			originLoadColums
		});
	}

	//指派
	getAssginUsedr = (value) => {
		let self = this;
		let {
			saveData
		} = self.state;
		let url = '/nccloud/gl/voucher/sendApprovebill.do';
		saveData.assignObj = value;
		ajax({
			url: url,
			data: saveData,
			success: function(response) {
				if (response.success) {
					toast({ content: self.state.json['20020MANGE-000022'], color: 'success' });/* 国际化处理： 提交成功*/
					self.state.id = response.data.voucher.pk_voucher.value;
					let resdata = response.data.voucher;
					resdata.paraInfo = response.data.paraInfo;
					updateCache('pk_voucher',self.state.id,response.data,self.formId,self.dataSource,resdata);
					self.props.pushTo('/view', {
						status: 'browse',
						bill_id: self.state.id,
						appcode: self.props.getSearchParam('c'), //获取小应用编码
						scene: self.state.scene
					});
				} else {
					toast({ content: response.error.message, color: 'warning' });
				}
			}
		});
	}
	//提交
	submitData = (submitData) => {
		let self = this;
		let url = '/nccloud/gl/voucher/sendApprovebill.do';
		let data = submitData;
		ajax({
			url,
			data,
			success: function(response) {
				if (response.success) {
					if (response.data.workflow && (response.data.workflow == 'approveflow' || response.data.workflow == 'workflow')) {
						self.setState({
							compositedata: response.data,
							compositedisplay: true,
							saveData:data
						});
					} else {
						toast({ content: self.state.json['20020MANGE-000022'], color: 'success' });/* 国际化处理： 提交成功*/
						self.state.id = response.data.voucher.pk_voucher.value;
						let resdata = response.data.voucher;
						resdata.paraInfo = response.data.paraInfo;
						updateCache('pk_voucher',self.state.id,response.data,self.formId,self.dataSource,resdata);
						
						self.props.pushTo('/view', {
							status: 'browse',
							bill_id: self.state.id,
							appcode: self.props.getSearchParam('c'), //获取小应用编码
							scene: self.state.scene
						});
					}
				} else {
					toast({ content: response.error.message, color: 'warning' });
				}
			}
		});
	};

	//指派关闭
	closeAssgin=()=>{
        if (this.state.compositedisplay) {
            this.setState({ compositedisplay: false });
        }
    }

	//提交
	handleSubmit = () => {
		let self = this;
		self.executeSubmit('submit'); //先保存
	};
	//保存新增
	handleSaveAdd = () => {
		let self = this;
		let { nov, startdate, attachment, evidenceData, saveData, update, saveNumber,getrow } = self.state;
		let url = '/nccloud/gl/voucher/insertbill.do';
		saveData = self.insertDetails();
		let formdata=this.props.form.getAllFormValue(['head']);
		// saveData.billstatus=formdata.head.rows[0].values.billstatus;
		// saveData.approvestatus=formdata.head.rows[0].values.approvestatus;
		let newData=formdata.head.rows[0].values;
		for(let key in newData){
			if(newData[key].value!=null){
				saveData[key]=newData[key]
			}
		}
		// newData.details=saveData.details;
		//表头校验
		let ischeck = this.props.form.isCheckNow(self.formId);
		if(!ischeck){
			return;
		}
		// let headIsNoExistStr = self.headIsNoExist();
		// if (headIsNoExistStr != '') {
		// 	toast({ content: headIsNoExistStr, color: 'danger' });
		// 	return;
		// }
		// 表体校验
		if (evidenceData.rows.length == 0) {
			toast({ content: self.state.json['20020MANGE-000174'], color: 'danger' });
			return;
		}
		let bodyIsNoExistStr = self.bodyIsNoExist(saveData);
		if (bodyIsNoExistStr != '') {
			toast({ content: bodyIsNoExistStr, color: 'danger' });
			self.setState({
				checkForm: false,
				isLoading: false
			});
			return;
		}
		let data = saveData;
		ajax({
			url,
			data,
			success: function(response) {
				if (response.success) {
					toast({ content: self.state.json['20020MANGE-000026'], color: 'success' });/* 国际化处理： 保存成功*/
					self.state.id = response.data.voucher.pk_voucher.value;
					let resdata = response.data.voucher;
					resdata.paraInfo = response.data.paraInfo;
					let billId = self.props.getUrlParam('bill_id');
					if(billId){
						updateCache('pk_voucher',self.state.id,response.data,self.formId,self.dataSource,resdata);
					}else{
						addCache(self.state.id,response.data,self.formId,self.dataSource,resdata);
					}
					self.props.form.EmptyAllFormValue(self.formId);
					self.props.form.EmptyAllFormValue(self.formIdtail);					
					evidenceData.rows=[];
					evidenceData.localcreditamountTotle = {value:""};
					evidenceData.localdebitamountTotle = {value:""};
					evidenceData.groupdebitamountTotle = {value:""};
					evidenceData.groupcreditamountTotle = {value:""};
					evidenceData.globaldebitamountTotle = {value:""};
					evidenceData.globalcreditamountTotle = {value:""};
					evidenceData.index=0;
					saveData={};					
					let data = {
						pk_tradetypecode: response.data.voucher.pk_tradetypecode.value
					};
					ajax({
						url: '/nccloud/gl/voucher/queryPageDefaultData.do',
						data: data,
						success: (res) => {
							let { success, data } = res;
							if (success && data != undefined) {
								self.setState({ 
									tradetypeData: { 
										value: data.tradetype.value, 
										display: data.tradetype.display
									},
									getrow: 0,
									saveData,
									evidenceData,
									headFunc: {
										checkNot: false, //是否保存模态框
										pk_accountingbook: {
											//核算账簿
											display: '',
											value: ''
										}
									}
								}, () => {
									self.searchById();
									//新增一行
									self.handleAdd();
									//初始化组织信息
									self.initOrgInfo();
									self.setState({
										getrow:''
									})
								});
							}
						}
					});
				} else {
					toast({ content: response.error.message, color: 'warning' });
				}
			}
		});
	};
	//保存
	handleSave = () => {
		this.executeSubmit();
	};
	//返回
	handleBack = () => {
		this.props.pushTo('/list', { appcode: this.props.getSearchParam('c') });
	};
	//取消
	cancel = () => {
		let that = this;
		promptBox({
			'color':"warning",
			'title': that.state.json['20020MANGE-000027'],/* 国际化处理： 取消*/
            'content': that.state.json['20020MANGE-000028'],/* 国际化处理： 确定要取消吗?*/
			beSureBtnClick:function(){
				var id = that.state.id;
				if(id === "undefined" || id == undefined)
				{
					id = getCurrentLastId(that.dataSource);
				}
				that.props.pushTo('/view', {
					status: 'browse',
					bill_id: id,
					appcode: that.props.getSearchParam('c'), //获取小应用编码
					scene: that.state.scene
				});
			}
		});
	};

	isString(str) {
		return typeof str == 'string' && str.constructor == String;
	}

	isEmptyObject(obj) {
		for (var key in obj) {
			return false; //返回false，不为空对象
		}
		return true; //返回true，为空对象
	}

	//saveData里拼接details数据
	insertDetails = () => {
		let { update, nov, attachment, saveData, evidenceData } = this.state;
		//let cloneNewLine = deepClone(evidenceData.rows);
		let cloneNewLine = [];
		if(evidenceData.rows){
			evidenceData.rows.forEach((val) => {
				cloneNewLine.push(val);
			})
		}
		if (!update) {
			let newclone = cloneNewLine
			newclone.forEach(function(v, i, a) {
				let ass = [];
				v.detailindex.value = ++i;
				//v.explanation.value=v.explanation.display;
				v.ass = v.childform;
				delete v.childform;
				delete v.checkedNumber;
				//delete v.key;
				//delete v.isEdit;
			});
			nov.value = Number(nov.value);
			attachment.value = Number(attachment.value);
			saveData.details = newclone;
		} else {
			if (saveData.period.value && saveData.period.value.indexOf('-') != -1) {
				let newValue = saveData.period.value.split('-')[1];
				saveData.period.value = newValue;
			}
			let newclone = cloneNewLine
			newclone.forEach(function(v, i, a) {
				v.detailindex.value = ++i;
			});
			saveData.details = newclone;
		}
		return saveData;
	};
	// 表头检验函数
	headIsNoExist = ()=> {
		let self = this;
		let { saveData } = this.state;
		saveData = self.insertDetails();
		let noExistStr = '', str = '';
		if (saveData.pk_org.value == '' || saveData.pk_org.value == null) {
			let span = self.state.json['20020MANGE-000040'] + '; ';
			str += span;
		}
		if (saveData.pk_tradetypeid.value == '' || saveData.pk_tradetypeid.value == null) {
			let span = self.state.json['20020MANGE-000092'] + '; ';
			str += span;
		}
		if (saveData.billdate.value == '' || saveData.billdate.value == null) {
			let span = self.state.json['20020MANGE-000094'] + '; ';
			str += span;
		}
		if (str != '') {
			noExistStr = self.state.json['20020MANGE-000173'] + ': ' + str + self.state.json['20020MANGE-000179'];
		}
		return noExistStr;
	};
	// 表体校验
	bodyIsNoExist = (saveData) =>{
		let self = this;
		let { evidenceData } = this.state; 
		// saveData = self.insertDetails();
		let { localcreditamountTotle, localdebitamountTotle } = evidenceData;
		if(evidenceData.rows.length > 0) {
			let noExistStr = '', str = '', deteleDetails = [];
			let cloneSaveDataDetails = [];
			if(saveData.details && saveData.details.length > 0){
				saveData.details.forEach((val) => {
					cloneSaveDataDetails.push(val);
				})
			}
			//cloneSaveDataDetails = deepClone(saveData.details);
	 		saveData.details.forEach((value, index)=>{
				let itemCom = '', item = self.state.json['20020MANGE-000054'] + value.detailindex.value + self.state.json['20020MANGE-000025'] + ': ';
				// 对必填项进行过滤,全部满足不进行校验
				if ((value.explanation.value == '' || value.explanation.value == undefined)
				&&(value.pk_accasoa.value == '' || value.pk_accasoa.value == undefined)
				&&(value.pk_currtype.value == '' || value.pk_currtype.value == undefined)
				&&(value.amount.value == '' || value.amount.value == undefined)) {
					deteleDetails.push(value);
					return;
				}
				if (value.explanation.value == '' || value.explanation.value == undefined) {
					itemCom += self.state.json['20020MANGE-000176'] + '； ';
				}
				if (value.pk_accasoa.value == '' || value.pk_accasoa.value == undefined) {
					itemCom += self.state.json['20020MANGE-000101'] + '； ';
				}
				if (value.pk_currtype.value == '' || value.pk_currtype.value == undefined) {
					itemCom += self.state.json['20020MANGE-000102'] + '； ';
				}
				if (value.amount.value == '' || value.amount.value == undefined) {
					itemCom += self.state.json['20020MANGE-000177'] + '； ';
				}
				if (value.flag == true && value.debitquantity.value == '') {
					itemCom += self.state.json['20020MANGE-000062'] + '； ';
				}
				if (value.flag == true && value.price.value == '') {
					itemCom += self.state.json['20020MANGE-000063'] + '； ';
				}
				if (itemCom != '') {
					item += itemCom;
					str += '<div>'+ item +'</div>';
				}
			})
			// 过滤空白行
			deteleDetails.forEach((value, index)=>{
				cloneSaveDataDetails.forEach((value1, index1)=>{
					if (value.detailindex.value == value1.detailindex.value) {
						cloneSaveDataDetails.splice(index1, 1);
					}
				})
			})
			// 必填字段全部为空时，进行校验
			if (cloneSaveDataDetails.length == 0) {
				saveData.details.forEach((value, index)=>{
					let itemCom = '', item = self.state.json['20020MANGE-000054'] + value.detailindex.value + self.state.json['20020MANGE-000025'] + ': ';
					if (value.explanation.value == '' || value.explanation.value == undefined) {
						itemCom += self.state.json['20020MANGE-000176'] + '； ';
					}
					if (value.pk_accasoa.value == '' || value.pk_accasoa.value == undefined) {
						itemCom += self.state.json['20020MANGE-000101'] + '； ';
					}
					if (value.pk_currtype.value == '' || value.pk_currtype.value == undefined) {
						itemCom += self.state.json['20020MANGE-000102'] + '； ';
					}
					if (value.amount.value == '' || value.amount.value == undefined) {
						itemCom += self.state.json['20020MANGE-000177'] + '； ';
					}
					if (itemCom != '') {
						item += itemCom;
						str += '<div>'+ item +'</div>';
					}
				})
			}
			saveData.details = cloneSaveDataDetails;
			if (str != '') {
				noExistStr = '<div>' + self.state.json['20020MANGE-000178'] + '</div>' + str;
			}
			if (localcreditamountTotle.value != localdebitamountTotle.value) {
				noExistStr += '<div>' + self.state.json['20020MANGE-000024'] + '</div>';
			}
			return noExistStr;
		}
	};
	executeSubmit(submit) {
		let self = this;
		let { nov, startdate, attachment, evidenceData, saveData, update, saveNumber } = self.state;
		let url = '/nccloud/gl/voucher/insertbill.do';
		saveData = self.insertDetails();
		let formdata=this.props.form.getAllFormValue(['head']);
		// saveData.billstatus=formdata.head.rows[0].values.billstatus;
		// saveData.approvestatus=formdata.head.rows[0].values.approvestatus;
		let newData=formdata.head.rows[0].values;
		for(let key in newData){
			if(newData[key].value!=null){
				saveData[key]=newData[key]
			}
		}
		// newData.details=saveData.details;
		// 表头校验
		let ischeck = this.props.form.isCheckNow(self.formId);
		if(!ischeck){
			return;
		}
		// let headIsNoExistStr = self.headIsNoExist();
		// if (headIsNoExistStr != '') {
		// 	toast({ content: headIsNoExistStr, color: 'danger' });
		// 	return;
		// }
		// 表体校验
		if (evidenceData.rows.length == 0) {
			toast({ content: self.state.json['20020MANGE-000174'], color: 'danger' });
			return;
		}
		let bodyIsNoExistStr = self.bodyIsNoExist(saveData);
		if (bodyIsNoExistStr != '') {
			toast({ content: bodyIsNoExistStr, color: 'danger' });
			self.setState({
				checkForm: false,
				isLoading: false
			});
			return;
		}
		let data = saveData;
		ajax({
			url,
			data,
			success: function(response) {
				if (response.success) {
					// //更新页面保存后的数据
					// if(response.data.voucher && response.data.voucher.details && response.data.voucher.details.length > 0){
					// 	response.data.voucher.totalcredit.scale = response.data.voucher.details[0].debitamount.scale;
					// 	response.data.voucher.totaldebit.scale = response.data.voucher.details[0].debitamount.scale;
					// }
					self.state.id = response.data.voucher.pk_voucher.value;
					let resdata = response.data.voucher;
					resdata.paraInfo = response.data.paraInfo;
					let billId = self.props.getUrlParam('bill_id');
					if(billId){
						updateCache('pk_voucher',self.state.id,response.data,self.formId,self.dataSource,resdata);
					}else{
						addCache(self.state.id,response.data,self.formId,self.dataSource,resdata);
					}
					if (submit != 'submit') {
						toast({ content: self.state.json['20020MANGE-000026'], color: 'success' });/* 国际化处理： 保存成功*/
						self.props.pushTo('/view', {
							status: 'browse',
							bill_id: self.state.id,
							appcode: self.props.getSearchParam('c'), //获取小应用编码
							scene: self.state.scene
						});
					} else {
						promptBox({
							color: 'warning',
							content: self.state.json['20020MANGE-000029'],/* 国际化处理： 保存成功！ 是否要提交？*/
							beSureBtnClick: self.submitData.bind(self,resdata),
							cancelBtnClick: () => {
								self.props.pushTo('/view', {
									status: 'browse',
									bill_id: self.state.id,
									appcode: self.props.getSearchParam('c'), //获取小应用编码
									scene: self.state.scene
								});
							}
						});
					}
				} else {
					toast({ content: response.error.message, color: 'warning' });
				}
			}
		});
	}

	//表单编辑前事件
	bodyBeforeEvent = (props, moduleId, key, value, index, record) => {
		let { pk_org, headFunc } = this.state;
		let meta = props.meta.getMeta();
		meta[moduleId].items.forEach((item) => {
			if(item.itemtype == 'refer'){
				item.queryCondition = () => {
					return {
						pk_org: item.attrcode == 'pk_org' ? null : pk_org,
						isDataPowerEnable: 'Y',
						DataPowerOperationCode: 'fi'
					};
				}
			}
			if(item.attrcode == 'pk_vouchertype'){
				item.queryCondition = () => {
					return {
						GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
						isDataPowerEnable: 'Y',
						DataPowerOperationCode: 'fi',
						pk_org: headFunc.pk_accountingbook.value
					};
				}
			}
			if(item.attrcode == 'pk_org'){
				item.isMultiSelectedEnabled = false;
				item.queryCondition = () => {
	                return {
						isDataPowerEnable:true,//是否启用数据权限
	                    DataPowerOperationCode: 'fi',//使用权组
	                    AppCode: props.getSearchParam('c'),
	                    TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
	                };
	            }
	        } 
			if(item.attrcode == 'pk_tradetypeid') {
				item.queryCondition = () => {
					return { 
						parentbilltype: 'VR01',
						pk_org: pk_org
					};
				};
			}
		});
		props.meta.setMeta(meta);
		return true;
	};
	handlelist = () => {
		window.location.href = './index.html?isChecked=' + 'true';
	};
	//刷新
	handleRefresh = () => {
		let localUrl = window.location.href;
		if (localUrl.indexOf('?') != -1) {
			let idName = localUrl.split('?')[1].split('=')[1];
			this.queryList(idName);
			localStorage.setItem('localSearchId', idName);
		}
	};

	//首张->末张
	handlelistFirst = () => {
		let state = this.state.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20020MANGE-000030'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let searchData = JSON.parse(localStorage.getItem('SearchData'));
		let searchId = searchData[0].pk_voucher.value;
		if (searchId) {
			this.queryList(searchId);
			localStorage.setItem('localSearchId', searchId);
		}
	};
	handlelistPrevious = () => {
		let state = this.state.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20020MANGE-000030'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let isCopy = this.state.copyStatus;
		if (isCopy == 'copy') {
			let localUrl = window.location.href;
			if (localUrl.indexOf('?') != -1) {
				let idName = localStorage.getItem('localSearchId');
				this.queryList(idName);
			}
			return;
		}
		let searchData = JSON.parse(localStorage.getItem('SearchData'));
		let localUrl = window.location.href;
		if (localUrl.indexOf('?') != -1) {
			let idName = localStorage.getItem('localSearchId');
			if (!idName) {
				idName = localUrl.split('?')[1].split('=')[1];
			}
			for (let i = 0, len = searchData.length; i < len; i++) {
				if (searchData[i].pk_voucher.value == idName) {
					if (i - 1 > 0 || i - 1 == 0) {
						let searchId = searchData[i - 1].pk_voucher.value;
						this.queryList(searchId);
						localStorage.setItem('localSearchId', searchId);
					} else {
						Message.create({ content: this.state.json['20020MANGE-000031'], color: 'warning' });/* 国际化处理： 已经是第一条*/
						// toast({ content: '已经是第一条', color: 'warning' });
					}
				}
			}
		}
	};
	handlelistNext = () => {
		let state = this.state.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20020MANGE-000030'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let searchData = JSON.parse(localStorage.getItem('SearchData'));
		let localUrl = window.location.href;
		if (localUrl.indexOf('?') != -1) {
			let idName = localStorage.getItem('localSearchId');
			if (!idName) {
				idName = localUrl.split('?')[1].split('=')[1];
			}
			for (let i = 0, len = searchData.length; i < len; i++) {
				if (searchData[i].pk_voucher.value == idName) {
					if (i + 1 < len) {
						let searchId = searchData[i + 1].pk_voucher.value;
						this.queryList(searchId);
						localStorage.setItem('localSearchId', searchId);
					} else {
						Message.create({ content: this.state.json['20020MANGE-000032'], color: 'warning' });/* 国际化处理： 已经是最后一条*/
					}
				}
			}
		}
	};
	handlelistEnd = () => {
		let state = this.state.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20020MANGE-000030'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let searchData = JSON.parse(localStorage.getItem('SearchData'));
		let searchId = searchData[searchData.length - 1].pk_voucher.value;
		if (searchId) {
			this.queryList(searchId);
			localStorage.setItem('localSearchId', searchId);
		}
	};
	addNewLine(record) {
		let { evidenceData, saveData} = this.state;
		let { defaultPkOrgInfo } = evidenceData
		let originData = this.findByKey(record, evidenceData.rows);
		let newkey = ++record;
		//新增分录行带出默认行信息 摘要 币种 汇率 组织集团依赖
		evidenceData.newLine.explanation = { display: '', value: '' };
		evidenceData.newLine.pk_unit = defaultPkOrgInfo&&defaultPkOrgInfo.pk_unit ? defaultPkOrgInfo.pk_unit : { display: '', value: '' };
		evidenceData.newLine.pk_unit_v = defaultPkOrgInfo&&defaultPkOrgInfo.pk_unit_v ? defaultPkOrgInfo.pk_unit_v : { display: '', value: '' };
		evidenceData.newLine.pk_currtype = defaultPkOrgInfo&&defaultPkOrgInfo.pk_currtype ? defaultPkOrgInfo.pk_currtype : { display: '', value: '' };
		evidenceData.newLine.excrate2 = defaultPkOrgInfo&&defaultPkOrgInfo.excrate2 ? defaultPkOrgInfo.excrate2 : { display: '', value: '' };
		evidenceData.newLine.excrate3 = defaultPkOrgInfo&&defaultPkOrgInfo.excrate3 ? defaultPkOrgInfo.excrate3 : { display: '', value: '' };
		evidenceData.newLine.excrate1 = defaultPkOrgInfo&&defaultPkOrgInfo.excrate1 ? defaultPkOrgInfo.excrate1 : { display: '', value: '' };
		evidenceData.newLine.groupType = defaultPkOrgInfo&&defaultPkOrgInfo.groupType ? defaultPkOrgInfo.groupType : '';
		evidenceData.newLine.globalType = defaultPkOrgInfo &&defaultPkOrgInfo.globalType? defaultPkOrgInfo.globalType : '';
		evidenceData.newLine.scale =  defaultPkOrgInfo&&defaultPkOrgInfo.scale?defaultPkOrgInfo.scale:(saveData&&saveData.paraInfo? saveData.paraInfo.scale : '');
		evidenceData.newLine.orgscale = defaultPkOrgInfo&&defaultPkOrgInfo.orgscale?defaultPkOrgInfo.orgscale:(saveData &&saveData.paraInfo? saveData.paraInfo.orgscale : '');
		evidenceData.newLine.groupscale = defaultPkOrgInfo&&defaultPkOrgInfo.groupscale?defaultPkOrgInfo.groupscale:(saveData &&saveData.paraInfo? saveData.paraInfo.groupscale : '');
		evidenceData.newLine.globalscale = defaultPkOrgInfo&&defaultPkOrgInfo.globalscale?defaultPkOrgInfo.globalscale:(saveData &&saveData.paraInfo? saveData.paraInfo.globalscale : '');
		evidenceData.newLine.pricescale = defaultPkOrgInfo &&defaultPkOrgInfo.pricescale? defaultPkOrgInfo.pricescale : '';
		let cloneNewLine = deepClone(evidenceData.newLine);
		let newLine = Object.assign({}, cloneNewLine, {
			key: newkey
		});
		if (newkey <= evidenceData.rows.length) {
			evidenceData.rows.map((item, index) => {
				if (item.key >= newkey) {
					item.key++;
				}
			});
		}
		evidenceData.rows.splice(newkey - 1, 0, newLine);
		this.setState({
			evidenceData
		});
	}

	//原币金额求和
	onInputChange = (record, index, name) => {
		let { rows } = this.state.evidenceData;
		let originData = this.findByKey(record, rows);
		if (originData) {
			originData[name] = {
				value: v
			};
		}
		this.setState({
			evidenceData: this.state.evidenceData
		});
	};

	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

	//格式化后台返回数据
	dataFormat = (data) => {
		let result = [];
		if (data) {
			if (data[0]) {
				data.map((item, index) => {
					item.key = index + 1;
					result.push(item);
				});
			}
		}
		return result;
	};

	//获取计算合计之后的数据
	getTableSumData = (data, amount, Totle) => {
		if (data.length != 0) {
			//需要合计的字段
			let { rows } = this.state.evidenceData;
			let newObj = this.state.evidenceData[Totle];
			let sumObj = {
				[amount]: []
			};
			for (let key in sumObj) {
				let arr = data.map((item) => {
					return item[key].value ? parseFloat(item[key].value) : 0;
				});
				//let totleValue= sum(arr.slice(0))
				newObj.value =sum(...arr).replace(/,/g, '')  
				// arr.reduce((prev, curr) => {
				// 	return  prev + curr;
				// });
			}
			return newObj;
		}
	};

	//主子表信息
	getData = (expanded, record) => {
	};
	hasClass = (obj, cls) => { 
        return obj.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)')); 
    } 
        
    addClass = (obj, cls) => { 
        if (!this.hasClass(obj, cls)) obj.className += " " + cls; 
    } 
        
    removeClass = (obj, cls) => { 
        if (this.hasClass(obj, cls)) { 
            var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)'); 
            obj.className = obj.className.replace(reg, ' '); 
        } 
    }
	getRow = (expanded, record) => {
		var rows = document.getElementsByClassName('u-table-row');
		if (rows.length > 0) {
			for(var i = 0; i < rows.length; i++) {
				this.removeClass(rows[i], 'selected')
			}
			this.addClass(rows[record], 'selected')
		}
		let copyData = deepClone(expanded); // /this.state.copyData=copyData;
		this.state.copyRecord = record;
		//表体数据增删获取key
		let { getrow } = this.state;
		getrow = expanded.key;
		this.setState({
			getrow: getrow
		});
		this.props.button.setButtonDisabled(['AddLine','DelLine','CopyLine','CutLine','PasteLine'],false);
	};
	//复制
	handleCopy = () => {
		//根据key去evidenceData取出对应行的数据
		let { evidenceData, copyRecord } = this.state;
		this.state.copyData = evidenceData.rows[copyRecord];
		if(this.state.copyData && this.state.copyData.pk_detail){
			if (this.state.copyData.pk_detail) {
				this.state.copyData.pk_detail.display = null;
				this.state.copyData.pk_detail.value = null;
			}
		}
	};
	//剪切
	handleCut = () => {
		let { evidenceData, copyRecord } = this.state;
		this.state.copyData = evidenceData.rows[copyRecord];
		this.state.copyOrCut = 'cut';
		let record = ++copyRecord;
		let { rows } = this.state.evidenceData;
		let originData = this.findByKey(record, rows);
		if (originData) {
			rows = rows.filter(function(v, i, a) {
				return v.key != originData.key;
			});
			this.state.evidenceData.rows = rows;
		}
		if (record <= rows.length) {
			rows.map((item, index) => {
				if (item.key >= record) {
					item.key--;
				}
			});
		}
		this.setState({
			evidenceData: this.state.evidenceData
		});
	};
	//粘贴
	handlePaste = () => {
		let { evidenceData, copyData, copyRecord } = this.state;
		let newkey = ++copyRecord;
		var deepCopyData = deepClone(copyData);
		let newLine = Object.assign({}, deepCopyData, {
			key: newkey
		});
		if (newkey <= evidenceData.rows.length) {
			evidenceData.rows.map((item, index) => {
				if (item.key >= newkey) {
					item.key++;
				}
			});
		}
		evidenceData.rows.splice(newkey - 1, 0, newLine);
		this.setState({
			evidenceData
		});
		this.totleCount();
	};

	// expandedRowRender = (record) => {
	// 	return <FormItemTab tabs={[]} />;
	// };

	//合计计算
	totleCount = (userdata, Totle) => {
		let _this = this;
		let { saveData } = _this.state;
		let { rows } = _this.state.evidenceData;
		let { evidenceData } = _this.state;
		let expenseData = rows.map((e) => {
			return e;
		});
		expenseData = _this.dataFormat(expenseData);
		let sumData = _this.getTableSumData(expenseData, 'localcreditamount', 'localcreditamountTotle'); //计算组织合计数据
		let sumDataother = _this.getTableSumData(expenseData, 'localdebitamount', 'localdebitamountTotle'); //计算合计数据
		let groupDdata = _this.getTableSumData(expenseData, 'groupdebitamount', 'groupdebitamountTotle'); //计算集团合计数据
		let groupCdata = _this.getTableSumData(expenseData, 'groupcreditamount', 'groupcreditamountTotle'); //计算合计数据
		let globalDdata = _this.getTableSumData(expenseData, 'globaldebitamount', 'globaldebitamountTotle'); //计算全局合计数据
		let globalCdata = _this.getTableSumData(expenseData, 'globalcreditamount', 'globalcreditamountTotle'); //计算合计数据
		saveData.totalcredit = sumData;
		saveData.totaldebit = sumDataother;
		saveData.totalcreditgroup = groupCdata;
		saveData.totaldebitgroup = groupDdata;
		saveData.totaldebitglobal = globalDdata;
		saveData.totalcreditglobal = globalCdata;
		if (sumData) {
			_this.setState({
				evidenceData,
				saveData
			});
			//this.forceUpdate()
		}
	};

	//日期控制
	handleInputChange = (obj, type, val) => {
		this.state.voucherStatus = 'update';
		let { saveData } = this.state;
		let nameValue = saveData[obj];
		// if (type === 'begindate' || type === 'enddate') {
		// 	val = moment(val);
		// }
		nameValue.value = val;
	};

	//删除
	handleCancel = () => {
		let record = this.state.getrow;
		let { rows } = this.state.evidenceData;
		if (record == 0) {
			record++;
		}
		let originData = this.findByKey(record, rows);
		let self = this;
		if (originData) {
			rows = rows.filter(function(v, i, a) {
				if ( v.key == originData.key) {
					self.ifNumber(false, i);
				}
				return v.key != originData.key;
			});
			this.state.evidenceData.rows = rows;
		}
		// if (record <= rows.length) {
			rows.map((item, index) => {
				if (item.key >= record) {
					item.key--;
				}
			});
			record--;
		// }
		this.setState({
			evidenceData: this.state.evidenceData,
			getrow: record
		});
		this.totleCount();
		var rows_ = document.getElementsByClassName('u-table-row');
		var record_ = record - 1;	
		if (rows_.length > 0 && record_ >= 0) {
			for(var i = 0; i < rows_.length; i++) {
				this.removeClass(rows_[i], 'selected')
			}
			this.addClass(rows_[record_], 'selected')
		}
	};
	handleAdd = () => {
		let record = this.state.getrow;
		let { evidenceData } = this.state;
		this.addNewLine(record !== '' ? record : evidenceData.index);
	};
	//常用按钮点击弹出模态框
	handleAllways = () => {
		this.state.voucherStatus = 'update';
		this.setState({
			saveNumber: '2',
			isLoading: true,
			checkForm: true
		});
	};
	changeNumberClick = (e) => {
		let self = this;
		let { evidenceData, findRows, saveData, isNC01, isNC02, originLoadColums} = self.state;
		//let originData = deepClone(this.findByKey(e, evidenceData.rows)); //避免修改原对象数组，对其他产生影响
		let originData = this.findByKey(e, evidenceData.rows);
		if (!originData.flag) {
			//传入子组件字段控制
			delete originData.debitquantity;
			delete originData.price;
		}
		let groupdebitamount = deepClone(originData.groupdebitamount);
		let groupcreditamount = deepClone(originData.groupcreditamount);
		let globalcreditamount = deepClone(originData.globalcreditamount);
		let globaldebitamount = deepClone(originData.globaldebitamount);
		delete originData.groupdebitamount;
		delete originData.groupcreditamount;
		delete originData.globalcreditamount;
		delete originData.globaldebitamount;
		originLoadColums.forEach(function(v, i, a) {
			if (v.attrcode == 'groupdebitamount' && v.visible) {
				originData.groupdebitamount = groupdebitamount;
			} else if (v.attrcode == 'groupcreditamount' && v.visible) {
				originData.groupcreditamount = groupcreditamount;
			} else if (v.attrcode == 'globalcreditamount' && v.visible) {
				originData.globalcreditamount = globalcreditamount;
			} else if (v.attrcode == 'globaldebitamount' && v.visible) {
				originData.globaldebitamount = globaldebitamount;				
			}
		})
		if ('excrate2' in originData && !originData.groupdebitamount && !originData.groupcreditamount) {
			//传入子组件字段控制
			delete originData.excrate2;
		}
		if ('excrate3' in originData && !originData.globaldebitamount && !originData.globalcreditamount ) {
			//传入子组件字段控制
			delete originData.excrate3;
		}
		if (findRows.length == 0) {
			findRows.push(originData);
		} else {
			findRows = [];
			findRows.push(originData);
		}
		let changeNumberShow = true;
		this.setState({
			changeNumberShow,
			index: e,
			findRows
		});
	};

	//辅助核算点击弹出模态框
	handleAssistAcc = (index) => {
		//ass
		let { isFreevalueDefault, saveData, unitOrg, evidenceData,currentRowPrevass, headFunc } = this.state;
		let prepareddate = saveData.billdate.value;
		let pk_accountingbook = headFunc.pk_accountingbook.value;
		let pk_unit = unitOrg.value;
		let pk_accasoa = '';
		let rows = evidenceData.rows;
		if (isFreevalueDefault && index != '1' && rows[index - 2] && rows[index - 2].ass && rows[index - 2].ass.length != 0) {
			currentRowPrevass=rows[index - 2].ass;
		}else{
			currentRowPrevass=[];
		}
		for (let i = 0, len = rows.length; i < len; i++) {
			if (rows[i].key == index) {
				pk_accasoa = this.state.evidenceData.rows[i].pk_accasoa.value;
			}
		}
		if (!pk_accountingbook) {
			toast({ content: this.state.json['20020MANGE-000034'], color: 'warning' });/* 国际化处理： 请选择核算账簿*/
			return false;
		}
		if (!pk_accasoa) {
			toast({ content: this.state.json['20020MANGE-000035'], color: 'warning' });/* 国际化处理： 请选择会计科目*/
			return false;
		}
		if (!prepareddate) {
			toast({ content: this.state.json['20020MANGE-000036'], color: 'warning' });/* 国际化处理： 请选择制单日期*/
			return false;
		}
		this.setState({
			evidenceData,
			getrow: index,
			AssistAccModalShow: true,
			currentRowPrevass
		});
	};
	//修改汇率和原币值
	changeUnit = (e) => {
		let { evidenceData } = this.state;
		let { rows } = evidenceData;
		rows = rows.map((t) => {
			return t.key === e[0].key ? e[0] : t;
		});
		this.setState(
			{
				evidenceData,
				changeNumberShow: false
			}
		);
	};

	// 辅助核算点确定后续操作
	handleAssistAccModal = (MsgModalAll, assDatas) => {
		let self = this;
		let { getrow, evidenceData,saveData } = self.state;
		let { rows } = self.state.evidenceData;
		if (Array.isArray(assDatas)) {
			let newData = assDatas.map((item,i) => {
				let assid = {
					display: item.assname,
					value: item.assid
				};
				let	rowsData;
				if(i=='0'){
					rowsData = evidenceData.rows.find((rowKey) => item.assid == rowKey.assid.value&&getrow==rowKey.key)||evidenceData.rows[getrow-1];
				}
				let  addRows=rowsData?rowsData:evidenceData.rows[getrow-1];
				let quantityscale={
					value:item.quantityscale?item.quantityscale:''
				}
				let pk_detail={
					value:'',
					display:'',
					scale:''
				};
				let amount={
					value:'',
					display:'',
					scale:''
				};
				let debitquantity={
					value:'',
					display:'',
					scale:''
				};
				let price={
					value:'',
					display:'',
					scale:''
				};
				let localdebitamount={
					value:'',
					display:'',
					scale:''
				};
				let localcreditamount={
					value:'',
					display:'',
					scale:''
				};
				let groupcreditamount={
					value:'',
					display:'',
					scale:''
				};
				let groupdebitamount={
					value:'',
					display:'',
					scale:''
				};
				let globalcreditamount={
					value:'',
					display:'',
					scale:''
				};
				let globaldebitamount={
					value:'',
					display:'',
					scale:''
				}
				self.addRowClear(
					pk_detail,amount,debitquantity,price,localdebitamount,localcreditamount,groupcreditamount,
					groupdebitamount,globalcreditamount,globaldebitamount,rowsData,addRows
				)
				if(item.quantityscale){//覆盖数量精度
					debitquantity.scale=item.quantityscale
				}
				return {
					...addRows,
					ass: JSON.stringify(item.assData) != '{}' ? item.assData : null, //辅助核算为{}  传 null
					quantityscale:item.quantityscale?quantityscale:'',
					
					assid:assid,
					pk_detail,//清空组件
					amount,debitquantity,price,groupcreditamount,localcreditamount, globalcreditamount, globaldebitamount,
					localdebitamount,groupdebitamount,assid
				};
			});
			newData.unshift(getrow - 1, 1);
			Array.prototype.splice.apply(rows, newData);
			rows.forEach(function(item, index) {
				item.detailindex = {//多选重置
					value: (index + 1).toString()
				};
				item.key = ++index;
			});
			saveData.details=rows;
			self.setState({
				saveData,
				AssistAccModalShow: false,
				evidenceData: self.state.evidenceData
			});
		} else {
			rows.forEach(function(item, index) {
				if (item.key == accIndex) {
					item.assid.display = '';
					item.assid.value = '';
				}
			});
			self.setState({
				AssistAccModalShow: false,
				evidenceData: self.state.evidenceData
			});
		}
	};

		//科目辅助核算拆行更新字段值
		addRowClear=(pk_detail,amount,debitquantity,price,localdebitamount,localcreditamount,groupcreditamount,
			groupdebitamount,globalcreditamount,globaldebitamount,rowsData,addRows
		)=>{
			 //分录主键,新增行清空
				pk_detail.display=rowsData?addRows.pk_detail&&addRows.pk_detail.display:'';
				pk_detail.value=rowsData?addRows.pk_detail&&addRows.pk_detail.value:'';
				pk_detail.scale=rowsData?addRows.pk_detail&&addRows.pk_detail.scale:'';
			//分录原币,新增行清空
				amount.display=rowsData?addRows.amount&&addRows.amount.display:'';
				amount.value=rowsData?addRows.amount&&addRows.amount.value:'';
				amount.scale=addRows.amount&&addRows.amount.scale
			//分录原币,新增行清空
				debitquantity.display=rowsData?addRows.debitquantity&&addRows.debitquantity.display:'';
				debitquantity.value=rowsData?addRows.debitquantity&&addRows.debitquantity.value:'';
				debitquantity.scale=addRows.debitquantity&&addRows.debitquantity.scale
			//分录原币,新增行清空
				price.display=rowsData?addRows.price&&addRows.price.display:'';
				price.value=rowsData?addRows.price&&addRows.price.value:'';
				price.scale=addRows.price&&addRows.price.scale
			//分录原币,新增行清空
				localdebitamount.display=rowsData?addRows.localdebitamount&&addRows.localdebitamount.display:'';
				localdebitamount.value=rowsData?addRows.localdebitamount&&addRows.localdebitamount.value:'';
				localdebitamount.scale=addRows.localdebitamount&&addRows.localdebitamount.scale
			//分录原币,新增行清空
				localcreditamount.display=rowsData?addRows.localcreditamount&&addRows.localcreditamount.display:'';
				localcreditamount.value=rowsData?addRows.localcreditamount&&addRows.localcreditamount.value:'';
				localcreditamount.scale=addRows.localcreditamount&&addRows.localcreditamount.scale
			//分录原币,新增行清空
				groupcreditamount.display=rowsData?addRows.groupcreditamount&&addRows.groupcreditamount.display:'';
				groupcreditamount.value=rowsData?addRows.groupcreditamount&&addRows.groupcreditamount.value:'';
				groupcreditamount.scale=addRows.groupcreditamount&&addRows.groupcreditamount.scale
			//分录原币,新增行清空
				groupdebitamount.display=rowsData?addRows.groupdebitamount&&addRows.groupdebitamount.display:'';
				groupdebitamount.value=rowsData?addRows.groupdebitamount&&addRows.groupdebitamount.value:'';
				groupdebitamount.scale=addRows.groupdebitamount&&addRows.groupdebitamount.scale
			//分录原币,新增行清空
				globalcreditamount.display=rowsData?addRows.globalcreditamount&&addRows.globalcreditamount.display:'';
				globalcreditamount.value=rowsData?addRows.globalcreditamount&&addRows.globalcreditamount.value:'';
				globalcreditamount.scale=addRows.globalcreditamount&&addRows.globalcreditamount.scale
			//分录原币,新增行清空
				globaldebitamount.display=rowsData?addRows.globaldebitamount&&addRows.globaldebitamount.display:'';
				globaldebitamount.value=rowsData?addRows.globaldebitamount&&addRows.globaldebitamount.value:'';
				globaldebitamount.scale=addRows.globaldebitamount&&addRows.globaldebitamount.scale
		}

	// 编辑复制获取数据后的渲染
	dataRender(response) {
		let self = this;
		let {
			headFunc,
			billdate,
			evidenceData,
			saveData,
			update,
			pk_org
		} = self.state;
		let { pk_accountingbook } = headFunc;
		let { isShowNum } = response.data.paraInfo;
		let { voucher } = response.data;
		self.setState({ billno: voucher.billno });
		if(voucher && voucher.details && voucher.details.length > 0){
			voucher.details.map((item, i) => {
				item.pk_accountingbook=voucher.pk_accountingbook;// >?????逻辑错误?
				item.billdate=voucher.billdate;// >?????逻辑错误?
				//渲染是否有动态新增列，有待优化
				item.key = ++i;
				if (item.isShowNum && item.isShowNum.value) {
					//根据返回数据判断是否有数量列
					item.flag = true;
				}
				if (item.debitamount.value && item.debitamount.value != 0) {
					item.amount = {
						scale:item.debitamount && item.debitamount.scale||item.amount&&item.amount.scale,
						value: item.debitamount && item.debitamount.value||item.amount&&item.amount.value,
					};
					item.debitamount.value = '';
				} else {
					item.amount = {
						scale:item.creditamount && item.creditamount.scale||item.amount&&item.amount.scale,
						value: item.creditamount && item.creditamount.value||item.amount&&item.amount.value,
					};
					item.creditamount.value = '';
					item.debitquantity=item.creditquantity
				}
				if(item.localcreditamount.value && item.localcreditamount.value == 0){
					item.localcreditamount.value = null;
				}
				if(item.localdebitamount.value && item.localdebitamount.value == 0){
					item.localdebitamount.value = null;
				}
			});
		}else{
			voucher.details = [];
		}
		
	
		//修改太页面参照设置依赖
		pk_accountingbook.value = voucher.pk_accountingbook.value;
		billdate.value = voucher.billdate.value;//???
		evidenceData.rows = voucher.details;
		// 设置当前组织默认的币种精度以及新增行等等
		self.updateRowsCopyEdit(evidenceData, response.data.paraInfo);
		let copeParaInfo = deepClone(response.data.paraInfo);
		itemUpdate(evidenceData.defaultPkOrgInfo, copeParaInfo);
		evidenceData.index = voucher.details.length;
		saveData = voucher;
		pk_org = voucher.pk_org.value;
		//表头表尾赋默认值
		self.props.form.setFormItemsValue(self.formId, saveData);
		self.props.form.setFormStatus(self.formId, status);
		self.props.form.setFormStatus(self.formIdtail, status);
		evidenceData.localcreditamountTotle.value = voucher.totalcredit.value; //合计值获取
		evidenceData.localdebitamountTotle.value = voucher.totaldebit.value;
		evidenceData.groupcreditamountTotle.value = voucher.totalcreditgroup.value;
		evidenceData.groupdebitamountTotle.value = voucher.totaldebitgroup.value;
		evidenceData.globalcreditamountTotle.value = voucher.totalcreditglobal.value;
		evidenceData.globaldebitamountTotle.value = voucher.totaldebitglobal.value;
		saveData['paraInfo'] = response.data.paraInfo;
		saveData.period.display = voucher.year.value + '-' + voucher.period.value;
		update = true;
		
		self.setState({
			headFunc,
			evidenceData,
			update,
			saveData,
			pk_org
		}, () => {
			if(isShowNum){
				self.ifNumber();
			}
		});
	}
	//复制
	handleBillCopy() {
		var self = this;
		let data = { 
			pk: self.props.getUrlParam('id'), 
			pagecode: self.props.getUrlParam('pagecode') 
		};
		let url = '/nccloud/gl/voucherbill/copycard.do';
		if (!data.pk && !data.pagecode) {
			return
		}
		ajax({
			url: url,
			data: data,
			success: (response) => {
				const { data, message, success } = response;
				if (success && data) {
					self.dataRender(response);
				} else {
					toast({ content: message, color: 'warning' });
				}
			}
		});
	}
	// 页面信息初始化
	initInfo = (param)=> {
		let self = this;
		let status = self.props.getUrlParam('status');
		if (status == 'add') {
			let data = {
				pk_tradetypecode: param.bill_type
			};
			ajax({
				url: '/nccloud/gl/voucher/queryPageDefaultData.do',
				data: data,
				success: (res) => {
					let { success, data } = res;
					if (success && data != undefined) {
						param.tradetypeData = { 
							value: data.tradetype.value, 
							display: data.tradetype.display 
						}
						self.setState(param, () => {
							initTemplate.call(self, self.props);
							self.searchById();
						});
					}
				}
			});
		} else if (status == 'edit') {
			self.setState(param, () =>{
				initTemplate.call(self, self.props);
				self.searchById();
				//根据id 渲染数据
				if (param.id) {
					self.queryList(param.id);
					self.state.voucherStatus = 'update';
				}
			});
		} else if (status == 'copy') {
			self.setState(param, () =>{
				initTemplate.call(self, self.props);
				self.searchById();
				self.handleBillCopy();
			});
		} else {
			self.setState(param, () =>{
				initTemplate.call(self, self.props);
				self.searchById();
 			});
		}
	}

	componentWillMount() {
		let self = this;
		//关闭浏览器
        window.onbeforeunload = () => {
			let status = self.props.getUrlParam('status');
	        if (status == 'edit' || status == 'add' || status == 'copy') {
	        	return ''
	        }
        }
		let callback = (json) => {
			let self = this;
			let { id, bill_type, scene } = self.state;
			id = self.props.getUrlParam('bill_id') || self.props.getUrlParam('id');
			bill_type = self.props.getUrlParam('tradetype') ? self.props.getUrlParam('tradetype') : self.props.getUrlParam('bill_type');
			let getScene = self.props.getUrlParam('scene'), setScene = getScene ? getScene : '';
			let paramData = { 
				json: json,
				id,
				bill_type,
				transtype: bill_type,
				scene: setScene
			};
			self.initInfo(paramData);
		}
		getMultiLang({ moduleId: ['20020MANGE','publiccomponents'], currentLocale: 'simpchn', domainName: 'gl', callback })
		
		// document.addEventListener(
		// 	'keydown',
		// 	function(e) {
		// 		if (e.keyCode == 83 && e.ctrlKey) {
		// 			e.preventDefault();
		// 			self.handleSave();
		// 		}

		// 		if (e.keyCode == 83 && e.altKey) {
		// 			e.preventDefault();
		// 			self.handleAllways();
		// 		}

		// 		if (e.keyCode == 73 && e.ctrlKey) {
		// 			e.preventDefault();
		// 			//增行')
		// 			self.handleAdd();
		// 		}

		// 		if (e.keyCode == 68 && e.ctrlKey) {
		// 			e.preventDefault();
		// 			//'删行')
		// 			self.handleCancel();
		// 		}

		// 		if (e.keyCode == 67 && e.altKey) {
		// 			e.preventDefault();
		// 			//复制')
		// 			self.handleCopyCopy();
		// 		}

		// 		if (e.keyCode == 86 && e.altKey) {
		// 			e.preventDefault();
		// 			//粘贴')
		// 			self.handlePaste();
		// 		}
		// 	},
		// 	false
		// );
	}
	queryList(id) {
		let self = this;
		let url = '/nccloud/gl/voucher/querybill.do';
		let data = {
			pk_voucher: id
		};
		ajax({
			url,
			data,
			success: function(response) {
				const { data, message, success } = response;
				if (success && data) {
					self.dataRender(response);
				} else {
					toast({ content: message, color: 'warning' });
				}
			}
		});
	}
	
	referValue = (value, index) => {
	};

	focus = () => {
	};

	//币种请求成功更新分录行精度汇率
	modifyRows = (originData, response) => {
		let {
			maxconverr,
			scale,
			NC001,
			NC002,
			excrate1,
			excrate2,
			excrate3,
			globalroundtype,
			grouproundtype,
			globalscale,
			groupscale,
			orgroundtype,
			orgscale,
			excrate1scale,
			excrate2scale,
			excrate3scale,
			roundtype,
			pricescale,
			priceroundtype,
			orgmode,
			groupmode,
			globalmode
		} = response;
		if (NC001) {
			originData.groupType = NC001;
		}
		if (NC002) {
			originData.globalType = NC002;
		}
		if (excrate1) {
			//根据核算账簿赋值默认汇率，默认币种，默认业务单元
			originData.excrate1 = {
				value: excrate1
			};
		}
		if (excrate2) {
			//根据核算账簿赋值默认汇率，默认币种，默认业务单元
			originData.excrate2 = {
				value: excrate2
			};
		}
		if (excrate3) {
			originData.excrate3 = {
				value: excrate3
			};
		}
		if (excrate1scale) {
			originData.excrate1scale = {
				value: excrate1scale
			};
		}
		if (excrate2scale) {
			originData.excrate2scale = {
				value: excrate2scale
			};
		}
		if (excrate3scale) {
			originData.excrate3scale = {
				value: excrate3scale
			};
		}
		if (maxconverr) {
			originData.maxconverr = {
				value: maxconverr
			};
		}
		if (orgroundtype) {
			originData.orgroundtype = {
				value: orgroundtype
			};
		}
		if (grouproundtype) {
			originData.grouproundtype = {
				value: grouproundtype
			};
		}
		if (globalroundtype) {
			originData.globalroundtype = {
				value: globalroundtype
			};
		}
		if (globalscale) {
			originData.globalscale = globalscale;
		}
		if (groupscale) {
			originData.groupscale = groupscale;
		}
		if (orgscale) {
			originData.orgscale = orgscale;
		}
		if (scale) {
			originData.scale = scale;
		}
		if (roundtype) {
			originData.roundtype = {
				value: roundtype
			};
		}
		if (pricescale) {
			originData.pricescale = {
				value: pricescale
			};
		}
		if (priceroundtype) {
			originData.priceroundtype = {
				value: priceroundtype
			};
		}
		originData.orgmode = {
			value: orgmode
		};
		if (groupmode) {
			originData.groupmode = {
				value: groupmode
			};
		}
		if (globalmode) {
			originData.globalmode = {
				value: globalmode
			};
		}
	};

	//根据币种获取汇率
	queryRate = (data, url, originData) => {
		let self = this;
		let { evidenceColumns } = self.state;
		ajax({
			url,
			data: data,
			success: function(response) {
				// scale roundtype excrate2 excrate3 excrate4 pricescale priceroundtype orgmode groupmode globalmode
				let {
					maxconverr,
					scale,
					NC001,
					NC002,
					excrate1,
					excrate2,
					excrate3,
					roundtype,
					pricescale,
					priceroundtype,
					orgmode,
					groupmode,
					globalmode
				} = response.data;
				self.modifyRows(originData, response.data);
				if (originData.amount.value) {
					//切换汇率请求同步跟进原币计算
					let { data, currInfo } = self.exportData(originData);
					let countValue = amountconvert(data, currInfo, 'amount');
					self.countRow(originData, countValue, 'amount');
					self.totleCount();
				}
			}
		});
	};

	//切换核算账簿后续操作
	changePK = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
		let { billdate, saveData } = self.state;
		let pk_accpont = {
			pk_accountingbook: saveData.pk_accountingbook.value,
			date: billdate.value
		};
		this.ifCheck(pk_accpont, url);
	};

	//是否二级核算请求
	ifCheck = (data, url, clearRows, clearOrg) => {
		let self = this;
		//根据核算账簿动态加载全局集团列 精度 汇率加载默认值
		let { evidenceData, pk_unit_org } = self.state;
		if (clearRows) {
			//清空整张凭证
			let rows = evidenceData.rows;
			for (let i = 0, len = rows.length; i < len; i++) {
				// 数量、单价隐藏
				this.ifNumber(false, i);
				// 精度设置
				if (clearOrg == 'clearOrg') {
					rows[i].globalscale = 2;
					rows[i].groupscale = 2;
					rows[i].orgscale = 2;
					rows[i].scale = 2;
				}
				for (let k in rows[i]) {
					if (isObj(rows[i][k])) {
						rows[i][k].value = '';
						rows[i][k].display = '';
						if (clearOrg == 'clearOrg') {
							if (k == 'excrate1scale' || k == 'excrate2scale' || k == 'excrate3scale') {
								rows[i][k].value = '2';
							}
						}
					}
				}
			}
			//清空表头
			self.props.form.EmptyAllFormValue(self.formId);
			self.setFormItemsDefault();
			let pk_accountingbook = { pk_org: { display: pk_unit_org.display, value: pk_unit_org.value } };//此处清空是否也要放在setFormItemsDefault中
			self.props.form.setFormItemsValue(self.formId, pk_accountingbook);
			self.setState(
				{
					evidenceData
				},
				() => {
					if (clearOrg != 'clearOrg') {
						self.querybook(data, url);
					}
				}
			);
		} else {
			self.querybook(data, url);
		}
	};

	querybook = (data, url) => {
		let self = this;
		//根据核算账簿动态加载全局集团列 精度 汇率加载默认值
		let { evidenceColumns, isadd, evidenceData, unitOrg, saveData } = self.state;
		ajax({
			url,
			data: data,
			success: function(response) {
				let cloneNewLine = deepClone(evidenceColumns);
				if (response.data) {
					saveData['paraInfo'] = response.data;
					let { isShowUnit, currinfo, excrate1, excrate2, excrate3, unit, NC001, NC002 } = response.data;
					if (unit) {
						unitOrg.value = unit.value;
					}

					self.updateRows(evidenceData, response.data);
					evidenceData.rows.forEach(function(item, index) {
						if (NC001) {
							item.groupType = NC001;
							evidenceData.newLine.groupType = NC001;
						}
						if (NC002) {
							item.globalType = NC002;
							evidenceData.newLine.globalType = NC002;
						}
						if (excrate1) {
							//根据核算账簿赋值默认汇率，默认币种，默认业务单元
							item.excrate1 = {
								value: excrate1
							};
							//新增行添加默认币种，业务单元
							evidenceData.newLine.excrate1 = {
								value: excrate1
							};
						}
						if (excrate2) {
							//根据核算账簿赋值默认汇率，默认币种，默认业务单元
							item.excrate2 = {
								value: excrate2
							};
							//新增行添加默认币种，业务单元
							evidenceData.newLine.excrate2 = {
								value: excrate2
							};
						}
						if (excrate3) {
							//根据核算账簿赋值默认汇率，默认币种，默认业务单元
							item.excrate3 = {
								value: excrate3
							};
							//新增行添加默认币种，业务单元
							evidenceData.newLine.excrate3 = {
								value: excrate3
							};
						}
						if (currinfo) {
							item.pk_currtype = {
								display: currinfo.display,
								value: currinfo.value
							};
							evidenceData.newLine.pk_currtype = {
								display: currinfo.display,
								value: currinfo.value
							};
						}
						if (unit) {
							item.pk_unit = {
								display: unit.display,
								value: unit.value
							};
							evidenceData.newLine.pk_unit = {
								display: unit.display,
								value: unit.value
							};
						}
						
						// 设置当前组织默认的币种精度等等
						var copyitem = deepClone(item);
						evidenceData.defaultPkOrgInfo = copyitem;
						// 修改原来的相应参数
						let { data, currInfo } = self.exportData(item);
						let countValue = amountconvert(data, currInfo, 'amount');
						self.countRow(item, countValue, 'amount');
					});
					if (!isShowUnit) {
						if (isadd) {//???是否走此代码
							cloneNewLine = cloneNewLine.filter(function(v, i, a) {
								return v.key != number.key;
							});
						}
					}
					
					self.setState(
						{
							evidenceData,
							saveData,
							evidenceColumns: cloneNewLine
						}
					);
				}
			}
		});
	};

	//是否有数量列
	ifNumber = (data, index1) => {
		let self = this;
		let { evidenceColumns, evidenceData } = self.state;
		data && (evidenceData.rows[index1].flag = true);
		!data && data != undefined && (evidenceData.rows[index1].flag = false);
		if (!data && data != undefined) {
			if (evidenceData.rows[index1].creditquantity && evidenceData.rows[index1].creditquantity.value != '') {
				evidenceData.rows[index1].creditquantity = {
					value: '',
					display: ''
				};
				evidenceData.rows[index1].price = {
					value: '',
					display: ''
				};
			} 
			if (evidenceData.rows[index1].debitquantity && evidenceData.rows[index1].debitquantity.value != '') {
				evidenceData.rows[index1].debitquantity = {
					value: '',
					display: ''
				};
				evidenceData.rows[index1].price = {
					value: '',
					display: ''
				};
			}
		}
		let quantity = {
			title: self.state.json['20020MANGE-000037'],/* 国际化处理： 数量/单价*/
			key: 'quantity',
			dataIndex: 'quantity',
			width: '180px',
			render: (text, record, index) => {
				//let self = this;
				let { pk_accountingbook, prepareddate, evidenceColumns, voucherView } = self.state;
				//let editable=record[item.attrcode].editable ? record[item.attrcode].editable:false
				let { rows } = self.state.evidenceData;
				let originData = self.findByKey(record.key, rows);
				let { data, currInfo } = self.exportData(originData);
				return record.flag ? (
					<div className="number">
								<NCNumber
									className="count"
									//disabled={editable}
									// scale={originData.quantityscale ? Number(originData.quantityscale.value) : 2}
									scale={record.debitquantity&&record.debitquantity.scale ? Number(record.debitquantity.scale) : record.quantityscale&&Number(record.quantityscale.value)}
									value={record.debitquantity.value || ''}
									placeholder={self.state.json['20020MANGE-000038']}/* 国际化处理： 请输入数量*/
									//disabled={record.debitquantity.value!='0'?false:true}
									onKeyUp={(v) => {}}
									onBlur={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (v) {
											let countValue = amountconvert(data, currInfo, 'debitquantity');
											self.countRow(originData, countValue, 'debitquantity');
										}
									}}
									onChange={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (originData) {
											originData.debitquantity = {
												...originData.debitquantity,
												value: v
											};
										}
										self.setState(
											{
												evidenceData
											}
										);
									}}
								/>
								<NCNumber
									className="price"
									//disabled={editable}
									scale={record.price&&record.price.scale? Number(record.price.scale) : record.pricescale&&Number(record.pricescale.value)}
									value={record.price.value || ''}
									placeholder={self.state.json['20020MANGE-000039']}/* 国际化处理： 请输入单价*/
									onKeyUp={(v) => {}}
									onBlur={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (v) {
											let countValue = amountconvert(data, currInfo, 'price');
											self.countRow(originData, countValue, 'price');
										}
									}}
									onChange={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (originData) {
											originData.price = {
												...originData.price,
												value: v
											};
										}
										self.setState({
											//evidenceColumns:cloneNewLine,
											evidenceData
										});
									}}
								/>
							</div>
				) : (
					<span />
				);
			}
		};
		//判断是否有数量这一列
		let num = false;
		for (let v of evidenceData.rows) {
			if (v.flag == true) {
				num = true;
				break;
			}
		}
		let originData = self.findByKey(quantity.key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			if (num) {
				let cloneNewLine = deepClone(evidenceColumns);
				cloneNewLine.splice(5, 0, quantity);
				self.setState({
					evidenceColumns: cloneNewLine
				});
			}
		} else {
			if (!num) {
				evidenceColumns = evidenceColumns.filter(function(v, i, a) {
					return v.key != 'quantity';
				});
				self.setState({
					evidenceColumns
				});
			}
			self.setState({
				evidenceData
			});
		}
	};
	updateRowScale = (response, key) => {
		//会计科目选中更新精度设置
		let { evidenceData } = this.state;
		if (Array.isArray(response.data)) {
			if (response.data.length == 1) {
				let {
					currinfo,
					excrate1,
					excrate2,
					excrate3,
					excrate1scale,
					excrate2scale,
					excrate3scale,
					NC001,
					NC002,
					scale,
					globalCurrinfo,
					globalmode,
					globalroundtype,
					globalscale,
					groupCurrinfo,
					groupmode,
					orgmode,
					grouproundtype,
					groupscale,
					orgroundtype,
					orgscale,
					quantityscale,
					pricescale,
				} = response.data[0];
				if (scale) {
					evidenceData.rows[key].scale = scale;
				}
				if (NC001) {
					evidenceData.rows[key].groupType = NC001;
				}
				if (NC002) {
					evidenceData.rows[key].globalType = NC002;
				}
				if (excrate1||excrate1=='0') {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].excrate2 = {
						value: excrate1
					};
				}
				if (excrate2||excrate2=='0') {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].excrate2 = {
						value: excrate2
					};
				}
				if (excrate3||excrate3=='0') {
					evidenceData.rows[key].excrate3 = {
						value: excrate3
					};
				}
				if (excrate1scale) {
					evidenceData.rows[key].excrate1scale = {
						value: excrate1scale
					};
				}
				if (excrate2scale) {
					evidenceData.rows[key].excrate2scale = {
						value: excrate2scale
					};
				}
				if (excrate3scale) {
					evidenceData.rows[key].excrate3scale = {
						value: excrate3scale
					};
				}
				if (currinfo) {
					evidenceData.rows[key].pk_currtype = {
						display: currinfo.display,
						value: currinfo.value
					};
				}
				if (globalCurrinfo) {
					evidenceData.rows[key].globalCurrinfo = {
						display: globalCurrinfo.display,
						value: globalCurrinfo.value
					};
				}
				if (groupCurrinfo) {
					evidenceData.rows[key].groupCurrinfo = {
						display: groupCurrinfo.display,
						value: groupCurrinfo.value
					};
				}
				if (globalmode) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].globalmode = {
						value: globalmode
					};
				}
				if (globalroundtype) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].globalroundtype = {
						value: globalroundtype
					};
				}
				if (globalscale) {
					evidenceData.rows[key].globalscale = globalscale;
				}
				if (groupmode) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].groupmode = {
						value: groupmode
					};
				}
				if (grouproundtype) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].grouproundtype = {
						value: grouproundtype
					};
				}
				if (groupscale) {
					evidenceData.rows[key].groupscale = groupscale;
				}
				if (orgroundtype) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].orgroundtype = {
						value: orgroundtype
					};
				}
				if (orgscale) {
					evidenceData.rows[key].orgscale = orgscale;
				}
				if(quantityscale){
					evidenceData.rows[key].quantityscale = {
						value: quantityscale
					};
				}
				if(pricescale){
					evidenceData.rows[key].pricescale = {
						value: pricescale
					};
				}
				evidenceData.rows[key].orgmode = {
					value: orgmode
				};
				evidenceData.rows[key].groupmode = {
					value: groupmode
				};
				evidenceData.rows[key].globalmode = {
					value: globalmode
				};
			} else {
				response.data.map((item, index) => {
					if (JSON.stringify(item) != '{}') {
						evidenceData.rows.forEach((content, rIndex) => {
							if (key == rIndex) {
								content.scale = item.scale;
								content.groupType = item.NC001;
								content.globalType = item.NC002;
								content.excrate1 = {
									value: item.excrate1
								};
								content.excrate2 = {
									value: item.excrate2
								};
								content.excrate3 = {
									value: item.excrate3
								};
								content.excrate1scale = {
									value: item.excrate1scale
								};
								content.excrate2scale = {
									value: item.excrate2scale
								};
								content.excrate3scale = {
									value: item.excrate3scale
								};
								content.currinfo = {
									display:  item.currinfo&&item.currinfo.display,
									value: item.currinfo&&item.currinfo.value
								};
								content.groupCurrinfo = {
									display: item.groupCurrinfo ? item.groupCurrinfo.display : '',
									value: item.groupCurrinfo ? item.groupCurrinfo.value : ''
								};
								content.globalCurrinfo = {
									display: item.globalCurrinfo ? item.globalCurrinfo.display : '',
									value: item.globalCurrinfo ? item.globalCurrinfo.value : ''
								};
								content.globalmode = {
									value: item.globalmode
								};
								content.globalroundtype = {
									value: item.globalroundtype
								};
								content.globalscale = item.globalscale;
								content.groupmode = {
									value: item.groupmode
								};
								content.grouproundtype = {
									value: item.grouproundtype
								};
								content.groupscale = item.groupscale;

								content.quantityscale = {
									value: item.quantityscale
								};
								content.orgroundtype = {
									value: item.orgroundtype
								};
								content.orgmode={
									value: item.orgmode
								};
								content.groupmode={
									value: item.groupmode
								};
								content.globalmode={
									value: item.globalmode
								};
								content.orgscale = item.orgscale;
							}
						});
					}
					key++;
				});
			}
		}
		this.setState({
			evidenceData
		});
	};

	//更新分录行精度
	updateRows = (evidenceData, response) => {
		evidenceData.rows.forEach(function(item, index) {
			if (!item.pk_accasoa.value) {
				//会计科目优先级最高
				itemUpdate(item, response);
			}
		});
		//新增行赋值
		itemUpdate(evidenceData.newLine, response);
	};
	//更新分录行精度
	updateRowsCopyEdit = (evidenceData, response) => {
		evidenceData.rows.forEach(function(item, index) {
			itemUpdate(item, response, 'copyEdit');
		});
		//新增行赋值
		itemUpdate(evidenceData.newLine, response);
	};

	//动态渲染表格
	getTable = (evidenceColumns, evidenceRows) => {
		return (
			<div class='voucherTable'>
				<Table
					className='table-data-list'
					bordered
					//onExpand={this.getData}
					onRowClick={this.getRow}
					
					// expandedRowRender={this.expandedRowRender}
					columns={evidenceColumns}
					data={evidenceRows}
					scroll={{
						x: evidenceColumns.length > 8 ? 100 + (evidenceColumns.length - 8) * 15 + '%' : '100%',
						y: 230
					}}
				/>
			</div>
		);
	};

	render() {
		const provinceData = [
			//单据状态
			{ display: this.state.json['20020MANGE-000006'], value: '1' },/* 国际化处理： 保存*/
			{ display: this.state.json['20020MANGE-000047'], value: '2' },/* 国际化处理： 提交*/
			{ display: this.state.json['20020MANGE-000048'], value: '3' }/* 国际化处理： 完成*/
		];
		const provinceOptions = provinceData.map((province) => <Option key={province}>{province}</Option>);

		let self = this;

		let {
			options,
			placeholder,
			disabled,
			evidenceData,
			MsgModalShow,
			MsgModalAll,
			SelectModalShow,
			SaveModalShow,
			SubjectModalShow,
			AssistAccModalShow,
			ExplanationModalShow,
			changeNumberShow,
			update,
			SaveErrorModalShow,
			currentRowPrevass
		} = self.state;
		let {
			evidenceColumns,
			headContent,
			footContent,
			findRows,
			errorFlag,
			saveData,
			voucherStatus,
			getrow,
			headFunc,
			billdate,
			unitOrg,
			pk_org
		} = this.state;
		let { table, button, search, modal, form } = self.props;
		const { createForm } = form;
		let evidenceRows = evidenceData.rows;
		let apportionmentValue = {
			amount:
				getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].amount
					? evidenceData.rows[getrow - 1].amount.value
					: '',
			pk_accasoa:
				getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].pk_accasoa
					? evidenceData.rows[getrow - 1].pk_accasoa.value
					: '',
			pk_accountingbook: saveData.pk_accountingbook
				? saveData.pk_accountingbook.value
				: headFunc.pk_accountingbook.value,
			prepareddate: saveData.billdate ? saveData.billdate.value : billdate.value,
			pk_unit:
				getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].pk_unit
					? evidenceData.rows[getrow - 1].pk_unit.value
					: unitOrg.value
		};

		if (saveData.billstatus) {
			const billstatus = saveData.billstatus.value;
		} else {
			const billstatus = '';
		}

		return (
			<div id="evidence">
				<div className="header">
					{/* <i className="back-btn iconfont icon-fanhuishangyiji" onClick={this.handleBack} /> */}
					{createPageIcon()}
					<div className="title">
						{this.state.json['20020MANGE-000051']}
					</div>{/* 国际化处理： 凭证单*/}
					<div className="btn-group">
						{this.props.button.createButtonApp({
							area: 'gl_voucher',
							onButtonClick: onButtonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="form-area">
					{createForm(this.formId, {
						//编辑后事件
						onAfterEvent: afterEvent.bind(this),
						onBeforeEvent:  this.bodyBeforeEvent.bind(this)//添加过滤
					})}
				</div>
				<div className="table-area">
					<div className="header table-header">
						<div className="title">{this.state.json['20020MANGE-000055']}</div>{/* 国际化处理： 分录信息*/}
						<div className="btn-group">
							{voucherStatus == 'update' ? (
								this.props.button.createButtonApp({
									area: 'detail',
									onButtonClick: onButtonClick.bind(this),
									popContainer: document.querySelector('.edit')
								})
							) : null}
						</div>
					</div>
					<div className="autoheight">
					{this.getTable(evidenceColumns, evidenceRows)}

					<div className="total-line">
					<span className="total-sum">
							{this.state.json['20020MANGE-000056']+"："}<span className="total-number">{/* 国际化处理： 合计差额*/}
								{sum(
									 evidenceData.localdebitamountTotle ?
									 setScale(evidenceData.localdebitamountTotle.value,pk_org&&saveData.paraInfo&&saveData.paraInfo.orgscale?saveData.paraInfo.orgscale:'2') : '',
									-(evidenceData.localcreditamountTotle?
										setScale(evidenceData.localcreditamountTotle.value,pk_org&&saveData.paraInfo&&saveData.paraInfo.orgscale?saveData.paraInfo.orgscale:'2')
										: '')
								)}
							</span>
						</span>
						<span className="total-caps">
							{this.state.json['20020MANGE-000057']+"："}<span className="total-caps-count">{this.changemoney(evidenceData)}</span>{/* 国际化处理： 大写合计*/}
						</span>

						<span className="total-item">
							{this.state.json['20020MANGE-000058']+"："/* 国际化处理： 组织借方合计*/}
							<span className="total-number">{setScale(evidenceData.localdebitamountTotle.value,pk_org&&saveData.paraInfo&&saveData.paraInfo.orgscale?saveData.paraInfo.orgscale:'2')}</span>
						</span>

						<span className="total-item">
							{this.state.json['20020MANGE-000059']+"："/* 国际化处理： 组织贷方合计*/}
							<span className="total-number">{setScale(evidenceData.localcreditamountTotle.value,pk_org&&saveData.paraInfo&&saveData.paraInfo.orgscale?saveData.paraInfo.orgscale:'2')}</span>
						</span>
					</div>
					</div>
				</div>
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={this.state.json['20020MANGE-000161']}/* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.closeAssgin}
					/>
				) : (
					''
				)}				
				<AssidModal
					showOrHide={AssistAccModalShow}
					voucherType={'voucherBill'}
					assData={
						getrow && evidenceData.rows[getrow - 1] &&evidenceData.rows[getrow - 1].ass&& evidenceData.rows[getrow - 1].ass.length!=0 ? (
							evidenceData.rows[getrow - 1].ass
						) : (
							currentRowPrevass
						)
					}
					assid={
						getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].assid ? (
							evidenceData.rows[getrow - 1].assid.value
						) : (
							''
						)
					}
					pk_accasoa={apportionmentValue.pk_accasoa}
					prepareddate={apportionmentValue.prepareddate}
					pk_accountingbook={apportionmentValue.pk_accountingbook}
					pk_org={apportionmentValue.pk_unit}
					onConfirm={(assDatas) => {
						this.handleAssistAccModal(AssistAccModalShow, assDatas);
					}}
					handleClose={() => {
						let AssistAccModalShow = false;
						this.setState({ AssistAccModalShow });
					}}
				/>
				<ChangeNumberModal
					show={changeNumberShow}
					title={this.state.json['20020MANGE-000052']}/* 国际化处理： 修改汇率*/
					loadForm={findRows}
					onConfirm={(e) => {
						this.changeUnit(e);
					}}
					onCancel={() => {
						let changeNumberShow = false;
						this.setState({ changeNumberShow });
					}}
				/>
			</div>
		);
	}
}

function itemUpdate(item, response, param) {
	let {
		isShowUnit,
		currinfo,
		groupCurrinfo,
		globalCurrinfo,
		excrate2,
		excrate3,
		excrate1,
		excrate2scale,
		excrate3scale,
		excrate1scale,
		unit,
		NC001,
		NC002,
		unit_v,
		groupscale, // 集团精度
		scale, // 默认币种精度
		globalscale, // 全局精度
		orgscale, // 组织精度
		bizDate,
		orgmode, //汇率计算方式 是否为除 true=除
		groupmode, //集团本币汇率计算方式 是否为除 true=除
		globalmode, //全局本币汇率计算方式 是否为除 true=除
		roundtype, //原币进舍规则
		orgroundtype, //本币进舍规则
		grouproundtype, //集团本币进舍规则
		globalroundtype, //全局本币进舍规则
		pricescale, //单价精度
		priceroundtype //单价进舍规则
	} = response;
	if (scale) {
		item.scale = scale;
	}
	if (orgscale) {
		item.orgscale = orgscale;
	}
	if (groupscale) {
		item.groupscale = groupscale;
	}
	if (globalscale) {
		item.globalscale = globalscale;
	}
	if (NC001) {
		item.groupType = NC001;
	}
	if (NC002) {
		item.globalType = NC002;
	}
	if (param == 'copyEdit') {
		if (item.excrate1||item.excrate1=='0') {
			item.excrate1scale = {
				value: item.excrate1.scale
			};
			item.excrate1 = {
				value: item.excrate1.value
			};
		}
		if (item.excrate2||item.excrate2=='0') {
			item.excrate2scale = {
				value: item.excrate2.scale
			};
			item.excrate2 = {
				value: item.excrate2.value
			};
		}
		if (item.excrate3||item.excrate3=='0') {
			item.excrate3scale = {
				value: item.excrate3.scale
			};
			item.excrate3 = {
				value: item.excrate3.value
			};
		}
	} else {
		if (excrate1||excrate1=='0') {
			//根据核算账簿赋值默认汇率，默认币种，默认业务单元
			item.excrate1 = {
				value: excrate1
			};
		}
		if (excrate2||excrate3=='0') {
			//根据核算账簿赋值默认汇率，默认币种，默认业务单元
			item.excrate2 = {
				value: excrate2
			};
		}
		if (excrate3||excrate3=='0') {
			item.excrate3 = {
				value: excrate3
			};
		}
		if (excrate1scale) {
			item.excrate1scale = {
				value: excrate1scale
			};
		}
		if (excrate2scale) {
			item.excrate2scale = {
				value: excrate2scale
			};
		}
		if (excrate3scale) {
			item.excrate3scale = {
				value: excrate3scale
			};
		}
	}
	item.orgmode = {
		value: orgmode
	};
	item.groupmode = {
		value: groupmode
	};
	item.globalmode = {
		value: groupmode
	};

	if (roundtype) {
		//原币进舍规则
		item.roundtype = {
			value: roundtype
		};
	}
	if (orgroundtype) {
		//本币进舍规则
		item.orgroundtype = {
			value: orgroundtype
		};
	}
	if (grouproundtype) {
		//集团本币进舍规则
		item.grouproundtype = {
			value: grouproundtype
		};
	}
	if (globalroundtype) {
		//全局本币进舍规则
		item.globalroundtype = {
			value: globalroundtype
		};
	}
	if (pricescale) {
		//单价精度
		item.pricescale = {
			value: pricescale
		};
	}
	if (priceroundtype) {
		//单价进舍规则
		item.priceroundtype = {
			value: priceroundtype
		};
	}
	if (param == 'copyEdit') {
		//设置原币
		if (item.pk_currtype) {
			item.pk_currtype = {
				display: item.pk_currtype.display,
				value: item.pk_currtype.value
			};
		}
		if (currinfo) {
			//组织本币币种
			item.orgcurrtype = {
				display: currinfo.display,
				value: currinfo.value
			};
		}
	} else {
		if (currinfo) {
			//设置原币和组织本币币种，默认原币等于组织本币币种
			item.pk_currtype = {
				display: currinfo.display,
				value: currinfo.value
			};
			item.orgcurrtype = {
				display: currinfo.display,
				value: currinfo.value
			};
		}
	}
	if (groupCurrinfo) {
		//集团币种
		item.groupcurrtype = {
			display: groupCurrinfo.display,
			value: groupCurrinfo.value
		};
	}
	if (globalCurrinfo) {
		//全局币种
		item.globalcurrtype = {
			display: globalCurrinfo.display,
			value: globalCurrinfo.value
		};
	}
	if (unit) {
		item.pk_unit = {
			display: unit.display,
			value: unit.value
		};
	}
	if (unit_v) {
		item.pk_unit_v = {
			display: unit_v.display,
			value: unit_v.value
		};
	}
}

VoucherEditCard = createPage({
})(VoucherEditCard);
export default VoucherEditCard;
