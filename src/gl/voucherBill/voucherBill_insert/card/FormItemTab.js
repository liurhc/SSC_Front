import React, { Component } from 'react';
import  Form from 'bee-form';
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../public/components/FormItems';
const { FormItem } = Form;
export default class FormItemTab extends  Component {
	constructor(props) {
		super(props);
		this.state={
			// tabs: [],
		}
	
	}


	render() {
		// return <h2>jjjjjjj</h2>
		
		let self = this;
		let { tabs} = this.props;
		return (
			<Form useRow={true}
				 submitAreaClassName='classArea'
				 showSubmit={false}　>
				 <FormItem inline={true}
                     showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2} 
                     style={{height:35}}
					 labelName={this.state.json['20020MANGE-000008']}/* 国际化处理： 实际使用可质押价值本币：*/
					 method="change">
					 <InputItem name="usinglcamount"
						 // defaultValue={tabs.usinglcamount.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000009']}/* 国际化处理： 经办人：*/
					 method="change">
					 <InputItem name="dealer"
						 type="customer"
						 // defaultValue={tabs.dealer.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000010']}/* 国际化处理： 日期：*/
					 method="change">
					 <InputItem name="makedate"
						 type="customer"
						 // defaultValue={tabs.makedate.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000011']}/* 国际化处理： 质押协议号：*/
					 method="change">
					 <InputItem name="pledgepno"
						 type="customer"
						 // defaultValue={tabs.pledgepno.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000012']}/* 国际化处理： 规格型号：*/
					 method="change">
					 <InputItem name="p_specno"
						 type="customer"
						 // defaultValue={tabs.p_specno.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000013']}/* 国际化处理： 数量：*/
					 method="change">
					 <InputItem name="p_count"
						 type="customer"
						 // defaultValue={tabs.p_count.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000014']}/* 国际化处理： 单位：*/
					 method="change">
					 <InputItem name="p_unit"
						 type="customer"
						 // defaultValue={tabs.p_unit.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2} xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000015']}/* 国际化处理： 单价：*/
					 method="change">
					 <InputItem name="p_price"
						 type="customer"
						 // defaultValue={tabs.p_price.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000016']}/* 国际化处理： 质量：*/
					 method="change">
					 <InputItem name="p_quality"
						 type="customer"
						 // defaultValue={tabs.p_quality.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000017']}/* 国际化处理： 状况：*/
					 method="change">
					 <InputItem name="p_status"
						 type="customer"
						 // defaultValue={tabs.p_status.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000018']}/* 国际化处理： 所在地：*/
					 method="change">
					 <InputItem name="p_location"
						 type="customer"
						 // defaultValue={tabs.p_location.value}
						 defaultValue={''}
					 />
				 </FormItem>
				 <FormItem inline={true}
					 showMast={false} labelXs={2} labelSm={2} labelMd={2}  xs={2} md={2} sm={2}
					 labelName={this.state.json['20020MANGE-000019']}/* 国际化处理： 可质押价值：*/
					 method="change">
					 <InputItem name="maxpledge"
						 type="customer"
						 // defaultValue={tabs.maxpledge.value}
						 defaultValue={''}
					 />
				 </FormItem>
			 </Form>
		);
	}
}
