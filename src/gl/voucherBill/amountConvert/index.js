import setScale from '../../public/common/amountFormat.js';
import {Subtr,accMul,accDiv} from '../../public/common/method.js';
import {getCurrInfo} from './currInfo.js';
/**
 * 金额换算公式 VoucherModel L2696
 */
let local_convert='local_convert';//根据本币计算
let raw_convert='raw_convert';//根据原币计算
export default function(data,currInfo,key){
     if(data.pk_accountingbook&&data.pk_accountingbook!=''){
        let copyCurrInfo = Object.assign({},currInfo);
        currInfo=getCurrInfo(data,currInfo);
        currInfo.quantityscale = copyCurrInfo.quantityscale;
     }
        switch(key){
            case 'amount':
            case 'creditamount':
            case 'debitamount':
                return  changeAmount(data,currInfo);
            case 'localamount':
            case 'localdebitamount':
            case 'localcreditamount':
                return  changeLocalamount(data,currInfo);
            case 'groupamount':
            case 'groupdebitamount':
            case 'groupcreditamount':
                return changeGroupAmount(data,currInfo);
            case 'globalamount':
            case 'globaldebitamount':
            case 'globalcreditamount':
                return changeGlobalAmount(data,currInfo);
            case 'price':
                return changePrice(data, currInfo);
            case 'debitquantity':
            case 'creditquantity':
                return changeQuantity(data, currInfo);
            case 'excrate1':
                return changeexcrate1(data, currInfo);
            case 'excrate2':
                return changeexcrate2(data, currInfo);
            case 'excrate3':
                return changeexcrate3(data, currInfo);
            default:
                break;
        }

}
function changeAmount(data, currInfo) {//修改原币
    let caldata = {
        amount: data.amount
    };
    //正算金额
    //计算组织本币
    let orgdata = getLocalAmount(data.amount, data, currInfo);
    //计算集团本币
    let groupdata = getGroupAmount(caldata.amount, orgdata.localamount, data, currInfo);
    //计算全局本币
    let globaldata = getGlobalAmount(caldata.amount, orgdata.localamount, data, currInfo);
    //数量和单价
    let pqdata = calPriceAndQuantity(caldata.amount, data);
    //处理数据格式
    return dealScale(Object.assign({},caldata, orgdata, groupdata, globaldata, pqdata),currInfo);
}

function changeLocalamount(data, currInfo) {
    //1.如果组织本币=原币，组织本币变化，原币随之变化
    //2.如果组织本币<>原币，组织本币变化，原币、汇率不变化，校验折算误差
    //3.如果原币未录入值，先录组织本币，自动计算原币
    let caldata = {
        localamount: data.localamount
    };
    if(data.pk_currtype==data.orgcurrtype){
        caldata.amount=data.localamount;
        caldata.excrate1=1;
         //数量和单价
        let pqdata = calPriceAndQuantity(caldata.amount, data);
        Object.assign(caldata, pqdata)
    }else{
        if(data.amount&&data.amount-0!=0){
            caldata.amount=data.amount;
            caldata.excrate1=data.excrate1;
            //校验折算误差
            let orgdata = getLocalAmount(data.amount, data, currInfo);
            let calocalamount=setScale(orgdata.localamount,currInfo.orgscale,currInfo.orgroundtype)
            let short=Math.abs(Subtr(calocalamount,data.localamount));
            if(Subtr(short,currInfo.maxconverr)>0){
                //caldata.message=this.state.json['20020MANGE-000000']+data.localamount+this.state.json['20020MANGE-000001']+calocalamount+this.state.json['20020MANGE-000002']+currInfo.maxconverr+"。"/* 国际化处理： 组织本币金额,与折算金额,的差额超出原币币种的最大折算误差*/
            }
        }else{ 
            //计算原币
            let occdata = getAmount(data.localamount, data, currInfo);
            caldata.amount=occdata.amount;
            caldata.excrate1=occdata.excrate1;
        }
        //数量和单价
        let pqdata = calPriceAndQuantity(caldata.amount, data);
        Object.assign(caldata, pqdata)
    } 

    //计算集团本币
    let groupdata = getGroupAmount(caldata.amount, caldata.localamount, data, currInfo);
    //计算全局本币
    let globaldata = getGlobalAmount(caldata.amount, caldata.localamount, data, currInfo);
    return dealScale(Object.assign(caldata, groupdata, globaldata), currInfo);
}

function changeGroupAmount(data,currInfo){
    return data;
}

function changeGlobalAmount(data,currInfo){
    return data;
}

function changePrice(data,currInfo){
    let caldata = {
        price: data.price
    }
    if (data.quantity - 0 != 0) {
        //正算金额
        caldata.amount = accMul(caldata.price,data.quantity);
        caldata.quantity = data.quantity;
    } else if (data.amount - 0 != 0) {
        //反算数量
        caldata.quantity = accDiv(data.amount,data.price);
        caldata.amount = data.amount;
    }

    //正算金额
    //计算组织本币
    let orgdata = getLocalAmount(caldata.amount, data, currInfo);
    //计算集团本币
    let groupdata = getGroupAmount(caldata.amount, orgdata.localamount, data, currInfo);
    //计算全局本币
    let globaldata = getGlobalAmount(caldata.amount, orgdata.localamount, data, currInfo);

    return dealScale(Object.assign(caldata, orgdata, groupdata, globaldata),currInfo);
}

function changeQuantity(data,currInfo){
    let caldata={
        quantity:data.quantity
    }

    if(data.price-0!=0){
        //正算金额
        caldata.amount = accMul(data.price,caldata.quantity);
        caldata.price = data.price;
    }else if(data.amount-0!=0){
        //反算单价
        caldata.price =accDiv(data.amount,caldata.quantity);
        caldata.amount=data.amount;
    }

    //正算金额
    //计算组织本币
    let orgdata = getLocalAmount(caldata.amount, data, currInfo);
    //计算集团本币
    let groupdata = getGroupAmount(caldata.amount, orgdata.localamount, data, currInfo);
    //计算全局本币
    let globaldata = getGlobalAmount(caldata.amount, orgdata.localamount, data, currInfo);

    return dealScale(Object.assign(caldata, orgdata, groupdata, globaldata),currInfo);
}

function changeexcrate1(data,currInfo){
    let caldata={
        excrate1:data.excrate1
    }

    if(data.amount-0!=0){
        let orgdata=getLocalAmount(data.amount,data,currInfo);
        caldata.amount=data.amount;
        caldata.excrate1=orgdata.excrate1;
        caldata.localamount=orgdata.localamount;
    }else if(data.localamount-0!=0){
        let occdata=getAmount(data.localamount,data,currInfo);
        caldata.amount=occdata.amount;
        caldata.excrate1=occdata.excrate1;
        caldata.localamount=data.localamount;
    }

    //计算集团本币
    let groupdata = getGroupAmount(caldata.amount, caldata.localamount, data, currInfo);
    //计算全局本币
    let globaldata = getGlobalAmount(caldata.amount, caldata.localamount, data, currInfo);
    //数量和单价
    let pqdata = calPriceAndQuantity(caldata.amount, data);
    //处理数据格式
    return dealScale(Object.assign(caldata, groupdata, globaldata, pqdata),currInfo);
}

function changeexcrate2(data,currInfo){
    return dealScale(getGroupAmount(data.amount,data.localamount,data,currInfo),currInfo);
}

function changeexcrate3(data,currInfo){
    return dealScale(getGlobalAmount(data.amount,data.localamount,data,currInfo),currInfo);
}

//根据来源金额，汇率1，目的金额 正算
function calDesAmount(srcAmount,desAmount,rate,ismode){
    let data={
        srcAmount:srcAmount,
        desAmount:desAmount,
        rate:rate
    }
    if(rate&&rate-0!=0){
        //正算
        if(ismode){
            data.desAmount=accDiv(srcAmount,rate);
        }else{
            data.desAmount=accMul(srcAmount,rate);
        }
    }else if(rate-0==1){
        data.desAmount=srcAmount;
    }else if(srcAmount-0==0){
        data.desAmount=0;
    }else if(rate-0==0&&desAmount-0!=0){
        //反算汇率
        if(ismode){
            data.rate=accDiv(srcAmount,desAmount);
        }else{
            data.rate=accDiv(desAmount,srcAmount);
        }
    }
    return data;
}

//根据 目的金额，汇率 来源金额 反算
function calSrcAmount(srcAmount,desAmount,rate,ismode){
    let data={
        srcAmount:srcAmount,
        desAmount:desAmount,
        rate:rate
    }

    if(rate&&rate-0!=0){
        if(ismode){
            data.srcAmount=accMul(desAmount,rate);
        }else{
            data.srcAmount=accDiv(desAmount,rate);
        }
    }else if(rate-0==1){
        data.srcAmount=desAmount;
    }else if(desAmount-0==0){
        data.srcAmount=0;
    }else if(rate-0==0&&srcAmount-0!=0){
        //反算汇率
        if(ismode){
            data.rate=accDiv(srcAmount,desAmount);
        }else{
            data.rate=accDiv(desAmount,srcAmount);
        }
    }
    return data;
}

//计算原币 
function getAmount(localamount,data,currInfo){
    let caldata={};
    if(data.pk_currtype==data.orgcurrtype){
        //原币==组织本币
        caldata.excrate1=1;
        caldata.amount=localamount;        
    }else{
        //计算组织本币
        let orgdata=calSrcAmount(data.amount,localamount,data.excrate1,currInfo.orgmode);
        caldata.excrate1=orgdata.rate;
        caldata.amount=orgdata.srcAmount;
    }
    return caldata;
}

//计算组织本币
function getLocalAmount(amount,data,currInfo){
    let caldata={};
    if(data.pk_currtype==data.orgcurrtype){
        //原币==组织本币
        caldata.excrate1=1;
        caldata.localamount=amount;        
    }else{
        //计算组织本币
        let orgdata=calDesAmount(amount,data.localamount,data.excrate1,currInfo.orgmode);
        caldata.excrate1=orgdata.rate;
        caldata.localamount=orgdata.desAmount;
    }
    return caldata;
}

//计算集团本币
function getGroupAmount(amount,localamount,data,currInfo){
    let caldata={};
    if(raw_convert==currInfo.NC001){
        if(data.pk_currtype==data.groupcurrtype){
            caldata.excrate2=1;
            caldata.groupamount=amount;
        }else{
            let groupdata=calDesAmount(amount,data.groupamount,data.excrate2,currInfo.groupmode);
            caldata.excrate2=groupdata.rate;
            caldata.groupamount=groupdata.desAmount;
        }            
    }else if(local_convert==currInfo.NC001){
        if(data.orgcurrtype==data.groupcurrtype){
            caldata.excrate2=1;
            caldata.groupamount=localamount;
        }else{
            let groupdata=calDesAmount(localamount,data.groupamount,data.excrate2,currInfo.groupmode);
            caldata.excrate2=groupdata.rate;
            caldata.groupamount=groupdata.desAmount;
        }            
    }
    return caldata;
}

//计算全局本币
function getGlobalAmount(amount,localamount,data,currInfo){
    let caldata={};
    if(raw_convert==currInfo.NC002){
        if(data.pk_currtype==data.globalcurrtype){
            caldata.excrate3=1;
            caldata.globalamount=amount;
        }else{
            let globaldata=calDesAmount(amount,data.globalamount,data.excrate3,currInfo.globalmode);
            caldata.excrate3=globaldata.rate;
            caldata.globalamount=globaldata.desAmount;
        }
    }else if(local_convert==currInfo.NC002){
        if(data.orgcurrtype==data.globalcurrtype){
            caldata.excrate3=1;
            caldata.globalamount=localamount;
        }else{
            let groupdata=calDesAmount(localamount,data.globalamount,data.excrate3,currInfo.globalmode);
            caldata.excrate3=groupdata.rate;
            caldata.globalamount=groupdata.desAmount;
        }        
    }
    return caldata;
}

function calPriceAndQuantity(amount,data){
    let caldata={amount:amount};
    //数量和单价
    if(data.quantity&&data.quantity-0!=0){
        //数量不为0，反算单价
        //TODO 取单价的精度 这里可以使用缓存
        caldata.price=accDiv(amount,data.quantity);
        caldata.quantity=data.quantity;
    }else if(data.price&&data.price-0!=0){
        //反算数量
        caldata.price=data.price;
        caldata.quantity=accDiv(amount,data.price);
    }
    return caldata;
}

function dealScale(caldata,currInfo){
    if(caldata.amount){
        caldata.amount=setScale(caldata.amount,currInfo.scale,currInfo.roundtype);
    }
    if(caldata.localamount){
        caldata.localamount=setScale(caldata.localamount,currInfo.orgscale,currInfo.orgroundtype);
    }
    if(caldata.groupamount){
        caldata.groupamount=setScale(caldata.groupamount,currInfo.groupscale,currInfo.grouproundtype);
    }
    if(caldata.globalamount){
        caldata.globalamount=setScale(caldata.globalamount,currInfo.globalscale,currInfo.globalroundtype);
    }
    if(caldata.price){
        caldata.price=setScale(caldata.price,currInfo.pricescale,currInfo.priceroundtype);
    }
    if(caldata.quantity){
        caldata.quantity=setScale(caldata.quantity,currInfo.quantityscale,'4');
    }
    if(caldata.excrate1){
        caldata.excrate1=setScale(caldata.excrate1,currInfo.excrate1scale,'4');
    }
    if(caldata.excrate2){
        caldata.excrate2=setScale(caldata.excrate2,currInfo.excrate2scale,'4');
    }
    if(caldata.excrate3){
        caldata.excrate3=setScale(caldata.excrate3,currInfo.excrate3scale,'4');
    }
    return caldata;
}

//export default amountConvert
