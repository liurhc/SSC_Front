import React, { Component } from 'react';
import {high,base,ajax,createPage, deepClone, toast, getMultiLang, createPageIcon } from 'nc-lightapp-front';
const { NCFormControl: FormControl, NCRadio:Radio,NCRow:Row,NCCol:Col,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCDiv
} = base;

import createScript from '../../../public/components/uapRefer.js';

import './index.less'
import { conf } from '../../../../uapbd/refer/org/BusinessUnitTreeRef/index';
import SubjectVersion from "../../../manageReport/common/modules/subjectVersion";

import {handleValueChange} from "../../../manageReport/common/modules/handleValueChange";
import {searchById} from "../../../manageReport/common/modules/createPureBtn";
import '../../../public/reportcss/firstpage.less'
import {businessUnit, createReferFn} from "../../../manageReport/common/modules/modules";
import HeaderArea from '../../../public/components/HeaderArea';
import { getTableHeight } from '../../../public/common/method.js';
const emptyCell = <span>&nbsp;</span>
class Reckon extends Component{
    constructor(props){
        super(props);
        this.state = {
            json: {},
            browserStyle: false,
            definer:'',
            user:{
                display: '',
                value: ''
            },
            choseRow: -1,
            assvo: [1, 2, 3],
            reportcode:'',
            reportname:'',
            comment:'',
            tableData: [
                { key: 1 }
            ],
            period_pk: {
                refname: "",
                refpk: "",
                tableData: []
            },
            remeber: {
                refpk:""
            },
            pk_accountingbook: {
                display:'',
                value:''
            },
            loadData: {
                refpk: '',
                pk_multicol: '',
                multicolname: ''
            },
            refUrl: {
                accBook:'uapbd/refer/org/AccountBookAllTreeRef/index'
            },
            columnsData:[

            ],
            selectedValue: {
                applyrange: {
                    "display": '',/* 国际化处理： 公用*/
                    "value": "C",
                    "scale": null,
                    "refer": true
                },
                balanceorient:'1'
            },
            checkData: {
                subjversion: null
            },
            headData: {
                reportcode:'',
                reportname:'',
                comment:''
            },
            subjversion: null,
            footData: {
                "containsub": {
                    "display": '',/* 国际化处理： 否*/
                    "value": "N",
                    "scale": null
                },
                "subextend": "N",
                "colsummaryflag": "N",
                "rowsummaryflag": "N"
            },
            saveData: {
                pk_accountingbook: '',
                reportitem: [],
                reportcode: '',
                reportname: '',
                comment: '',
                subjversion: null,
                balanceorient: '1',
                applyrange: {
                    "display": '',/* 国际化处理： 公用*/
                    "value": "C",
                    "scale": null
                },  
                containsub: {
                    "display": '',/* 国际化处理： 否*/
                    "value": "N",
                    "scale": null
                },
                subextend: 'N',
                colsummaryflag: 'N',
                rowsummaryflag: 'N',
            },
            versionDateArr: [],
            versiondate: '',
            busiDate: [],
            isversiondate: false, //科目版本复选框
            // columns: []
        };

        this.getMaindata = this.getMaindata.bind(this);
        this.userMan = this.userMan.bind(this);
        this.tableInChange = this.tableInChange.bind(this);
        this.headData = this.headData.bind(this);
        this.footCheck = this.footCheck.bind(this);
        this.footSubCheck = this.footSubCheck.bind(this);
        this.renderRef = this.renderRef.bind(this);
        this.saveData = this.saveData.bind(this);
        this.handleValueChange = handleValueChange.bind(this);
        this.searchById = searchById.bind(this);
    }
    headData(val, name){
        //('headData:::', val)
        if(val.length <= 500){
            this.state.headData[name] = val;
            this.setState({
                headData:this.state.headData
            })
        }else{
            toast({
                content: this.state.json['2002308005-000026'],/* 国际化处理： 最多只能输入500个字符！*/
                color: 'warning'
            })
        }

    }
    componentWillMount() {
        //('WillMount>>>>', this.props.getUrlParam('user'))
        let callback= (json) =>{
            this.setState({
                json:json,
                selectedValue: {
                    applyrange: {
                        "display": json['2002308005-000020'],/* 国际化处理： 公用*/
                        "value": "C",
                        "scale": null,
                        "refer": true
                    },
                    balanceorient:'1'
                },
                footData: {
                    "containsub": {
                        "display": json['2002308005-000021'],/* 国际化处理： 否*/
                        "value": "N",
                        "scale": null
                    },
                    "subextend": "N",
                    "colsummaryflag": "N",
                    "rowsummaryflag": "N"
                },
                applyrange: {
                    "display": json['2002308005-000020'],/* 国际化处理： 公用*/
                    "value": "C",
                    "scale": null
                },
                containsub: {
                    "display": json['2002308005-000021'],/* 国际化处理： 否*/
                    "value": "N",
                    "scale": null
                },
            })
        }
        getMultiLang({moduleId:'2002308005',domainName:'gl',currentLocale:'simpchn',callback});
        this.getMaindata()
    }
    componentDidMount(){
        let selectUserPeople = this.props.getUrlParam('user') && JSON.parse(this.props.getUrlParam('user'));
        //('dMount:::1di', this.props.getUrlParam('look'), selectUserPeople);
        //('didMount:::2', this.props.getUrlParam('look') == 'false' && !this.state.browserStyle)
        let dataParam = JSON.parse(this.props.getUrlParam('dateData'));
        let lengthParam = dataParam.versionDateArr.length;
        let selectPeopleUser={};
        let newSelectPeopleUser = [];
        if(this.props.getUrlParam('user')){
            selectPeopleUser =  JSON.parse(this.props.getUrlParam('user'));
            selectPeopleUser.display.split(',').map((item, index) => {
                let cellObj = {};
                cellObj.refname = item;
                cellObj.refpk = selectPeopleUser.value.split(',')[index];
                newSelectPeopleUser.push(cellObj);
            })
        }
        this.setState({
            versionDateArr: dataParam.versionDateArr,
            busiDate: dataParam.busiDate,
            versiondate: dataParam.versionDateArr[lengthParam-1],
            selectUsers: newSelectPeopleUser
        });
        let appceod = this.props.getSearchParam('c');
        this.searchById('2002308005PAGE',appceod);
        if(this.props.getUrlParam('look') == 'false' && !this.state.browserStyle){
            this.props.button.setButtonVisible([ 'moveUp', 'movedown', 'save', 'back'], false)
        }else{
            this.props.button.setButtonVisible([ 'alter', 'secondDelete'], false)
        }
        //('v:::::::>>>>', this.state.columnsData)
    }
    initTabeData = () => {
        this.state.columnsData.length>0 && this.state.columnsData[0].selected === 'Y' &&
        this.state.columnsData.map((item, index) => {
            if(item.pk_extendreportprop != "zb00Z300000000000001" && item.pk_extendreportprop != "km00Z300000000000001"){//核算账簿，会计科目
                item.selected ='N';
                item.disabled = true
            }
        })
        this.setState({
            columnsData: {...this.state.columnsData}
        })
    }
    tableInChange(index, valName, value, record) {
        //('tableInChange>', index, valName, value, record)
        if(index == 1 && valName == 'selected') return;
        if(valName == 'selected'){
            if(record.pk_extendreportprop === "zb00Z300000000000001" && value){
                this.state.columnsData.map((item, index) => {
                    if(item.pk_extendreportprop != "zb00Z300000000000001" && item.pk_extendreportprop != "km00Z300000000000001"){//核算账簿，会计科目
                        item.selected ='N';
                        item.disabled = true
                    }

                })
                //browserStyle
            }else if(record.pk_extendreportprop === "zb00Z300000000000001" && !value){
                this.state.columnsData.map((item, index) => {
                    if(item.pk_extendreportprop != "zb00Z300000000000001" && item.pk_extendreportprop != "km00Z300000000000001"){
                        item.disabled = false
                    }

                })
            }
            // this.setState({
            //     browserStyle: false
            // })
        }
        let numVal = 0;
        this.state.columnsData.forEach((item, num) => {
            if(this.state.columnsData[num][valName] == 'Y'){
                numVal++;
            }
        })
        //(numVal);
        if(numVal ==2 && value) {
            if(valName == 'rowselected'){
                toast({content: this.state.json['2002308005-000027'], color: 'warning'});/* 国际化处理： 行标题 最多只能选择两个*/
                return;
            }else if(valName == 'colselected'){
                toast({content: this.state.json['2002308005-000028'], color: 'warning'});/* 国际化处理： 列标题 最多只能选择两个*/
                return;
            }
        }
        this.state.columnsData[index][valName] = this.state.columnsData[index][valName] == 'Y' ? 'N' : 'Y';
        if ( valName == 'rowselected' ) this.state.columnsData[index]['colselected']  = 'N';
        if (valName == 'colselected') this.state.columnsData[index]['rowselected']  = 'N';
        this.setState({
            columnsData: this.state.columnsData
        })
    }
    //record.pk_extendreportprop, record.refpath, record.analyvalue, record.key, record
    renderAccountRefer(record, fieldid){
        if(record.refpath){
            return this.renderRef(record, fieldid);
        }
    }
    renderRef(record, fieldid) {
        let param = record.classid || record.pk_extendreportprop;
        let refUrl = record.refpath;
        let book = '';
        let ifEdit = false;
        if(this.props.getUrlParam('look') == 'false' && !this.state.browserStyle){
            if(record.selectCells || record.assvo){
                record.assvo.map((item, index) => {
                    book += item.checkvaluename + ' '
                })
            }
        }else{
            if(!this.state[param]) {
                {createScript.call(this, refUrl+'.js', param)}
            } else {
                let queryCondition = {}
                if(record.caption === this.state.json['2002308005-000029']){/* 国际化处理： 会计科目*/
                    let dateParam = JSON.parse(this.props.getUrlParam('dateData'));
                    //('renderRef::::', record, dateParam)
                    queryCondition = {
                        "pk_accountingbook": this.props.getUrlParam('pk_accountingbook'),
                        "dateStr": this.state.isversiondate ? this.state.versiondate :dateParam.busiDate
                    }
                } else if(record.caption === this.state.json['2002308005-000030']){/* 国际化处理： 核算账簿*/
                    disabledDataShow:true,
                        queryCondition = {
                            "pk_accountingbook": this.props.getUrlParam('pk_accountingbook'),
                            "TreeRefActionExt":"nccloud.web.gl.ref.AccountBookRefSqlBuilder",
                            "appcode": this.props.getUrlParam('appcode')
                        }
                }else {
                    queryCondition = {
                        "pk_accountingbook": this.props.getUrlParam('pk_accountingbook')
                    }
                }
                if(this.props.getUrlParam('look') == 'false' && !this.state.browserStyle) {
                    ifEdit = true;
                }
                record.assvo && record.assvo.map((item, index) => {
                    item.refname = item.checkvaluecode
                })
                //('assvoassvo::', record)
                let options = {
                    fieldid: fieldid,
                    queryCondition:() => {
                        return {
                            ...queryCondition,
                            "isDataPowerEnable": 'Y',
                            "DataPowerOperationCode" : 'fi',
                            "pk_org": this.props.getUrlParam('pk_org'),
                            "dateStr": this.state.isversiondate ? this.state.versiondate : this.state.busiDate
                        }
                    },
                    "isShowDisabledData": true,
                    disabled: ifEdit,
                    value: record.selectCells || record.assvo,
                    isMultiSelectedEnabled: true,
                    onChange:(v) => {
                        //('onChange::::',v, record);

                        let namesingle = [];
                        let refpksingle = [];
                        v.map( (itemCell, index) => {
                            namesingle.push(itemCell.refname);
                            refpksingle.push(itemCell.refpk);
                        })
                        record.analyvalue = {}
                        record.analyvalue['display'] = namesingle.join(',');
                        record.analyvalue['value'] = refpksingle.join(',');
                        record.selectCells = v;
                        this.setState({})
                    }
                };
                let newOptions = {}
                if(record.classid && record.classid.length === 20){
                    newOptions = {
                        ...options,
                        "pk_defdoclist":record.classid,
                    }
                }else if(record.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'){
                    newOptions = {
                        ...options,
                        "unitValueIsNeeded":false,
                        "isShowDimission":true,
                        queryCondition:() => {
                            return {
                                ...queryCondition,
                                "isDataPowerEnable": 'Y',
                                "DataPowerOperationCode" : 'fi',
                                "pk_org": this.props.getUrlParam('pk_org'),
                                "busifuncode":"all",
                                "dateStr": this.state.isversiondate ? this.state.versiondate : this.state.busiDate,
                                isShowDimission:true//显示离职人员
                            }
                        },
                        isShowUnit:true,
                        "isShowDisabledData": true,
                        unitProps:{
                            refType: 'tree',
                            refName: this.state.json['2002308005-000031'],/* 国际化处理： 业务单元*/
                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                            rootNode:{refname:this.state.json['2002308005-000031'],refpk:'root'},/* 国际化处理： 业务单元*/
                            placeholder:this.state.json['2002308005-000031'],/* 国际化处理： 业务单元*/
                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                            treeConfig:{name:[this.state.json['2002308005-000005'], this.state.json['2002308005-000006']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                            isMultiSelectedEnabled: false,
                            //unitProps:unitConf,
                            isShowUnit:false
                        },
                        unitCondition:{
                            pk_financeorg: this.props.getUrlParam('pk_org'),
                            'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                        },
                    }
                }else{
                    newOptions = {...options}
                }

                //('dateStr:', queryCondition);
                book =  (
                    <Row>
                        <Col xs={12} md={12}>
                            {this.state[param]?(this.state[param])(newOptions):<div/>}
                        </Col>
                    </Row>
                );
            }
        }

        return book;
    }
    getMaindata() {
        let sendData = { "pk_accountingbook": this.props.getUrlParam('pk_accountingbook') }
        sendData.pk_extendreport = this.props.getUrlParam('pk_extendreport') || null;
        let url ='/nccloud/gl/accountrep/assanalysisreportquery.do';
        ajax({
            url,
            data: sendData,
            success: (response) => {
                //('response.data', response.data);
                response.data.reportitem.forEach((item, index) => item.key = index)
                if(!this.props.getUrlParam('pk_extendreport')) {
                    this.state.columnsData = response.data.reportitem;
                    this.state.columnsData[1].selected = 'Y';
                    //(this.state.columnsData[1]);
                    this.setState({
                        columnsData: this.state.columnsData
                    })
                } else {
                    this.state.selectedValue.balanceorient = response.data.balanceorient;
                    this.state.selectedValue.applyrange = response.data.applyrange;
                    this.state.selectedValue.applyrange.refer = response.data.applyrange.value == 'P' ? false: true;
                    this.state.headData['reportcode'] = response.data.reportcode;
                    this.state.headData['reportname'] = response.data.reportname;
                    this.state.headData['comment'] = response.data.comment;
                    this.state.footData['containsub'] = response.data.containsub;
                    this.state.footData['subextend'] = response.data.subextend;
                    this.state.footData['colsummaryflag'] = response.data.colsummaryflag;
                    this.state.checkData.subjversion = response.data.subjversion;
                    this.state.footData.colsummaryflag = response.data.colsummaryflag;
                    this.state.footData.rowsummaryflag = response.data.rowsummaryflag;
                    response.data.reportitem[0].selected === 'Y' && response.data.reportitem.map((item,index) => {
                        if(item.pk_extendreportprop != "zb00Z300000000000001" && item.pk_extendreportprop != "km00Z300000000000001"){//核算账簿，会计科目
                            item.selected ='N';
                            item.disabled = true
                        }
                    });
                    //selected
                    this.setState({
                        columnsData: response.data.reportitem,
                        selectedValue: this.state.selectedValue,
                        headData: this.state.headData,
                        checkData: this.state.checkData,
                        footData: this.state.footData,
                        isversiondate: response.data.subjversion != null
                    })
                    let versiondateArr = this.props.getUrlParam('versiondate');
                    if (response.data.subjversion == null && JSON.parse(versiondateArr)) {
                        JSON.parse(versiondateArr).map((item) => {
                            this.setState({
                                versiondate: item
                            });
                        })
                    } else {
                        this.setState({
                            versiondate: response.data.subjversion
                        });
                    }
                    //('this.state.columnsData', this.state.columnsData);
                }
            }
        })
    }
    balanceorientChange(val) {
        if(this.props.getUrlParam('look') == 'false' && !this.state.browserStyle) return;
        this.state.selectedValue.balanceorient = val;
        this.setState({
            selectedValue: this.state.selectedValue
        })
    }
    userMan(val) {
        if(this.props.getUrlParam('look') == 'false' && !this.state.browserStyle) return;
        this.state.selectedValue.applyrange.value = val;
        this.state.selectedValue.applyrange.refer = val == "P" ? false: true;
        this.state.selectedValue.applyrange.display = val == "P" ? this.state.json['2002308005-000032']: this.state.json['2002308005-000020'];/* 国际化处理： 指定,公用*/
        this.setState({
            selectedValue: this.state.selectedValue
        })
    }
    footCheck(val, item) {
        //('footCheck>>>', val, item)
        this.state.footData[item] = val ? 'Y':'N';
        if (val && item === 'subextend') {// 勾选'是否展开下级'，则自动勾选'辅助核算是否包含下级'
            this.state.footData.containsub.value ='Y';
            this.setState({
                footData: this.state.footData
            })
        }
        this.setState({
            footData: this.state.footData
        })
    }
    footSubCheck(val) {
        this.state.footData.containsub.value = val ? 'Y':'N';
        if(!val){
            this.state.footData['subextend'] = 'N';
            this.setState({
                footData: this.state.footData
            })
        }
        this.setState({
            footData: this.state.footData
        })
    }
    saveData() {
        let url = '/nccloud/gl/accountrep/assanalysisreportsave.do';
        this.state.saveData.reportitem = [];
        this.state.columnsData.forEach((item) => {
            if (item.selected == 'Y') this.state.saveData.reportitem.push(item);
        })
        if(this.props.getUrlParam('pk_extendreport')) this.state.saveData.pk_extendreport = this.props.getUrlParam('pk_extendreport');

        this.state.saveData.pk_accountingbook = this.props.getUrlParam('pk_accountingbook');
        this.state.saveData.reportcode = this.state.headData.reportcode;
        this.state.saveData.reportname = this.state.headData.reportname;
        this.state.saveData.comment = this.state.headData.comment;
        this.state.saveData.balanceorient = this.state.selectedValue.balanceorient;
        this.state.saveData.applyrange = this.state.selectedValue.applyrange;
        this.state.saveData.containsub = this.state.footData.containsub;
        this.state.saveData.subextend = this.state.footData.subextend;
        this.state.saveData.colsummaryflag = this.state.footData.colsummaryflag;
        this.state.saveData.rowsummaryflag = this.state.footData.rowsummaryflag;
        this.state.saveData.subjversion = this.state.isversiondate ? this.state.versiondate : null;
        if(this.state.saveData.applyrange.value == 'P') {
            this.state.saveData.definer = this.state.definer
            if (this.state.selectUsers.length == 0) {
                toast({ content: this.state.json['2002308005-000045'], color: 'warning' });/* 国际化处理： 请选择使用人*/
                return;
            }
            let userAll = {};
            let userAllArr = [];
            this.state.selectUsers.map((itme) => {
                userAllArr.push(itme.refpk);
            });
            userAll.value = userAllArr.join(',');
            this.state.saveData.user = userAll;
        }
        this.setState({
            saveData: this.state.saveData
        })
        //(' saveData >>>>',this.state.saveData);
        ajax({
            url,
            data: this.state.saveData,
            success: (response) => {
                //(response.data);
                toast({color: 'success'});
                setTimeout(()=>{
                    this.props.linkTo('/gl/accountrep/pages/first/index.html', {
                        oldRef: this.props.getUrlParam('selectRef')
                    })
                }, 1000)
            }
        });
    }
    moveUp = () => {
        let remNum = this.state.choseRow;
        if(remNum == -1 || remNum == 0) return;
        let coverItem = {};
        coverItem = this.state.columnsData[remNum-1];
        this.state.columnsData[remNum-1] = this.state.columnsData[remNum];
        this.state.columnsData[remNum] = coverItem;
        this.setState({
            columnsData: this.state.columnsData,
            choseRow: remNum-1
        })
    }
    moveDown = () => {
        let remNum = this.state.choseRow;
        let length = this.state.columnsData.length-1;
        if(remNum == -1 || remNum == length) return;
        let coverItem = {};
        coverItem = this.state.columnsData[remNum+1];
        this.state.columnsData[remNum+1] = this.state.columnsData[remNum];
        this.state.columnsData[remNum] = coverItem;
        this.setState({
            columnsData: this.state.columnsData,
            choseRow: remNum+1
        })
    }

    handleLoginBtn = (obj, btnName) => {
        //('handleLoginBtn>>>', obj, btnName)
        if(btnName === 'moveUp'){//上移
            this.moveUp()
        }else if(btnName === 'movedown'){//下移
            this.moveDown()
        }else if(btnName === 'save'){//保存
            this.saveData()
        }else if(btnName === 'back'){//返回
            this.props.linkTo('/gl/accountrep/pages/first/index.html', {
                oldRef: this.props.getUrlParam('selectRef')
            })
        }else if(btnName === 'alter') {
            this.props.button.setButtonVisible([ 'moveUp', 'movedown', 'save', 'back'], true)
            this.props.button.setButtonVisible([ 'alter', 'secondDelete'], false);
            this.setState({
                browserStyle: true
            })
        }
    }
    handleBackClick = () => {
        //('handleBackClick;')
        this.props.linkTo('/gl/accountrep/pages/first/index.html', {
            oldRef: this.props.getUrlParam('selectRef')
        })
    }
    renderRight = (item) => {
        if(item.pk_accasoa){
           return <div><span>{item.checkvaluename || item.refname}</span></div>
        }else{
           return <div>
                <span>{item.checkvaluecode || item.refcode}</span>
                <span>{item.checkvaluename || item.refname}</span>
            </div>
        }
    }
    render() {
        this.columns = [
            {
                title: (<div fieldid='selected'>{this.state.json['2002308005-000022']}</div>),/* 国际化处理： 选择*/
                dataIndex: 'selected',
                key: 'selected',
                width: 80 ,
                render:(text, record) => {
                    //('suanzhe::', text, record, this.props.getUrlParam('look') == 'false', !this.state.browserStyle)
                    return (
                        <div fieldid='selected'>
                            <Checkbox
                                disabled={
                                    this.props.getUrlParam('look') == 'false' && !this.state.browserStyle ? true : record.disabled
                                }
                                checked = {text === 'Y' ? true : false}
                                onChange = {(value) => {
                                    this.tableInChange(record.key, 'selected', value, record)
                                }}
                            />
                        </div>
                    )
                }
            },
            {
                // id: '123',
                title: (<div fieldid='caption'>{this.state.json['2002308005-000023']}</div>),/* 国际化处理： 分析项目*/
                dataIndex: 'caption',
                key: 'caption',
                width: 80,
                render: (text, record, index) => <div fieldid='caption'>{text ? text : emptyCell}</div>
            },
            {
                // id: '123',
                title: (<div fieldid='type'>{this.state.json['2002308005-000024']}</div>),/* 国际化处理： 属性*/
                dataIndex: 'type',
                key: 'type',
                width: 80,
                render: (text, record, index) => <div fieldid='type'>{text ? text : emptyCell}</div>
            },
            {
                // id: '123',
                title: (<div fieldid='rowselected'>{this.state.json['2002308005-000007']}</div>),/* 国际化处理： 行标题*/
                dataIndex: 'rowselected',
                key: 'rowselected',
                width: 80 ,
                render:(text, record)=>{
                    return (
                        <div fieldid='rowselected'>
                            <Checkbox
                                disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle}
                                checked = {record.rowselected === 'Y' ? true : false}
                                onChange = {(value) => this.tableInChange(record.key, 'rowselected', value)}
                            />
                        </div>
                    )
                }
            },
            {
                // id: '123',
                title: (<div fieldid='colselected'>{this.state.json['2002308005-000008']}</div>),/* 国际化处理： 列标题*/
                dataIndex: 'colselected',
                key: 'colselected',
                width: 80 ,
                render:(text, record) => {
                    return (
                        <div fieldid='colselected'>
                            <Checkbox
                                disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle}
                                checked = {record.colselected === 'Y' ? true : false}
                                onChange = {(value) => this.tableInChange(record.key, 'colselected', value)}
                            />
                        </div>
                    )
                }
            },
            {
                // id: '123',
                title: (<div fieldid='user'>{this.state.json['2002308005-000025']}</div>),/* 国际化处理： 分析选择*/
                dataIndex: 'user',
                key: 'user',
                width: 80 ,
                render: (text, record, index) => {//.pk_extendreportprop, record.refpath, record.analyvalue, index, record
                    // this.renderRef(record, index)
                    let renEle = this.renderAccountRefer(record, 'user');
                    return renEle;
                }
            }
        ]
        //('DidMounthhhhh::', this.props.getUrlParam('user'))
        //('renderrr:::', this.state, this.props.getUrlParam('look') );

        //('newSelectPeopleUser>', newSelectPeopleUser);
        const { DragWidthCom, getUrlParam } = this.props;
        let { period_pk } = this.state;
        let { pk_accountingbook, periodData }=this.state;
        let referUrl = 'uap/refer/riart/userDefaultRefer/index.js';
        let mybook;
        //{getUrlParam('look') == 'false'}
		if(!this.state['myattrcode']) {
			{createScript.call(this, referUrl, 'myattrcode')}
		} else {
			mybook = (
				<div>
                    {this.state['myattrcode']?(this.state['myattrcode'])(
                        {
                            fieldid: 'userAll',
                            value: this.state.selectUsers,
                            disabled: this.state.selectedValue.applyrange.refer,
                            queryCondition: {
                                "user_Type":"4",
                                "GridRefActionExt":"nccloud.web.gl.ref.OperatorRefSqlBuilder",
                                "pk_accountingbook": this.props.getUrlParam('pk_accountingbook')
                            },
                            onChange:(v) => {
                                //('onChange>>::',v);
                                let userAll = {
                                    display: '',
                                    value: ''
                                };
                                let definerName = '';
                                userAll.display = '';
                                let Vlength1 = v.length-1;
                                if (v.length != 0) {
                                    v.forEach((item, index) => {
                                        definerName += Vlength1 == index ? `${item.refname}` : `${item.refname},`;
                                        userAll.display += Vlength1 == index ? `${item.refname}` : `${item.refname},`;
                                        userAll.value += Vlength1 == index ? `${item.refpk}` : `${item.refpk},`;
                                    })
                                }
                                this.setState({
                                    definer: definerName,
                                    user: userAll,
                                    selectUsers: v
                                })
                                //(this.state.definer);
                            }
                        }
                    ):<div/>}
                </div>
			);
        }
        let centerConHeight = getTableHeight(176)
        let centerTableHeight = getTableHeight(216)
        return (
            <div className="nc-bill-tree-table reckoning accountrep">
                <HeaderArea 
                    title = {this.state.json['2002308005-000019']} /* 国际化处理： 辅助分析设置*/
                    initShowBackBtn = {true}
                    backBtnClick = {this.handleBackClick}
                    btnContent = {this.props.button.createButtonApp({
                        area: 'secondpage',
                        buttonLimit: 3,
                        onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                    })}
                />
                <NCDiv fieldid="accountrep" areaCode={NCDiv.config.FORM} className="mainBody">
                    <div className="main-bottom m-analysis-main-bottom nc-theme-area-bgc">
                        <div className={"m-analysis-main-bottom-div"}>
                            <span style={{color: 'red'}}>*</span>
                            <span className="main-bottom-span nc-theme-common-font-c">{this.state.json['2002308005-000005']}：</span>{/* 国际化处理： 编码*/}
                            {
                                this.props.getUrlParam('look') == 'false' && !this.state.browserStyle
                                ?
                                    <div className="disabledCell" fieldid='reportcode'>{this.state.headData.reportcode}</div>
                                :
                                <FormControl
                                    fieldid='reportcode'
                                    disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle}
                                    // className="demo-input  m-main-bottom-input"
                                    value={this.state.headData.reportcode}
                                    onChange={(val)=> this.headData(val,'reportcode')}
                                    size="sm"
                                />
                            }
                        </div>
                        <div className={"m-analysis-main-bottom-div"}>
                            <span style={{color: 'red'}}>*</span>
                            <span className="main-bottom-span nc-theme-common-font-c">{this.state.json['2002308005-000006']}：</span>{/* 国际化处理： 名称*/}
                            {
                                this.props.getUrlParam('look') == 'false' && !this.state.browserStyle
                                ?
                                    <div className='disabledCell' fieldid='reportname'>{this.state.headData.reportname}</div>
                                :
                                    <FormControl
                                        fieldid='reportname'
                                        disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle}
                                        // className="demo-input  m-main-bottom-input"
                                        value={this.state.headData.reportname}
                                        onChange={(val)=> this.headData(val,'reportname')}
                                        size="sm"
                                    />
                            }
                        </div>

                        {/*启用科目版本：*/}
                        <div className={"m-analysis-main-bottom-div"}>
                        {/* {
                            this.props.getUrlParam('look') == 'false' && !this.state.browserStyle ?
                            <div className={"m-analysis-main-bottom-div"}>
                                <span className="main-bottom-span">{this.state.json['2002308005-000033']}：</span>
                                <div style={{
                                    margin: '2px -6px 0 0px'
                                }}>
                                    <Checkbox
                                        colors="dark"
                                        id='isversiondate'
                                        disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle ? true : false}
                                        checked={this.state.isversiondate}
                                        onChange={
                                            (value) => this.handleValueChange('isversiondate', value)
                                        }
                                    />
                                </div>
                                <div className="disabledCell">{this.state.versiondate}</div>
                            </div>
                            : */}
                            <NCDiv fieldid='versiondate' areaCode={NCDiv.config.Area} style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <label className='nc-theme-common-font-c' style={{display:'inlineBlock', width: '170px'}}>
                                    {this.state.json['2002308005-000033']}：
                                </label>{/* 国际化处理： 启用科目版本*/}
                                <div style={{
                                    margin: '2px -6px 0 0px'
                                }}>
                                    <Checkbox
                                        colors="dark"
                                        disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle ? true : false}
                                        checked={this.state.isversiondate}
                                        onChange={
                                            (value) => this.handleValueChange('isversiondate', value)
                                        }
                                    />
                                </div>
                                {this.props.getUrlParam('look') == 'false' && !this.state.browserStyle ? 
                                    <div className="disabledCell">{this.state.versiondate}</div>
                                    :
                                    <Select
                                        showClear={false}
                                        showClear={false}
                                        fieldid='versiondate'
                                        value={this.state.versiondate}
                                        onChange={
                                            (value) => {
                                                this.handleValueChange('versiondate', value)
                                            }
                                        }
                                        disabled={!this.state.isversiondate}
                                    >
                                        {JSON.parse(this.props.getUrlParam('versiondate')) && JSON.parse(this.props.getUrlParam('versiondate')).map((item) => {
                                            return (
                                                <Select.NCOption
                                                    value={item}
                                                    key={item}
                                                >
                                                    {item}
                                                </Select.NCOption>
                                            )
                                        })}
                                    </Select>
                                }
                                {/* <Select
                                    size="lg"
                                    fieldid='versiondate'
                                    value={this.state.versiondate}
                                    onChange={
                                        (value) => {
                                            this.handleValueChange('versiondate', value)
                                        }
                                    }
                                    disabled={!this.state.isversiondate}
                                >
                                    {JSON.parse(this.props.getUrlParam('versiondate')) && JSON.parse(this.props.getUrlParam('versiondate')).map((item) => {
                                        return (
                                            <Select.NCOption
                                                value={item}
                                                key={item}
                                            >
                                                {item}
                                            </Select.NCOption>
                                        )
                                    })}
                                </Select> */}
                            </NCDiv>
                        {/* } */}
                        </div>

                        <div
                            className={"m-analysis-main-bottom-div reMarkContainer"}
                        >
                            <label className="main-bottom-span nc-theme-common-font-c">{this.state.json['2002308005-000034']}：</label>{/* 国际化处理： 备注*/}
                            {
                                this.props.getUrlParam('look') == 'false' && !this.state.browserStyle
                                ?
                                    <div fieldid='comment' className="disabledCell">{this.state.headData.comment}</div>
                                :
                                    <FormControl
                                        style={{
                                            minHeight: '30px'
                                        }}
                                        fieldid='comment'
                                        rows="5"
                                        maxlength="200"
                                        componentClass='textarea'
                                        disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle}
                                        // className="demo-input  m-main-bottom-input"
                                        value={this.state.headData.comment}
                                        onChange={(val)=> this.headData(val,'comment')}
                                        size="sm"
                                    />
                            }

                        </div>
                    </div>

                </NCDiv>
                <div className="tree-table"  style={{height: centerConHeight}}>
                    <DragWidthCom
                        // style={{height: '96%'}}
                        defLeftWid='85%'
                        leftDom = {
                            <NCDiv fieldid="analysis" areaCode={NCDiv.config.TableCom}>
                                <Table
                                    // style={{height: '100%'}}
                                    columns={this.columns}
                                    data={this.state.columnsData}
                                    useFixedHeader={true}
                                    scroll= {{ x: true, y: centerTableHeight}}
                                    onRowClick = {(record,index,indent)=>{
                                        //('onClick:::::',record, index, indent)
                                        this.setState({
                                            choseRow: index
                                        })
                                        if(record.pk_extendreportprop === "zb00Z300000000000001" && record.selected === "Y"){
                                            this.state.columnsData.map((item, indx) => {
                                                if(item.pk_extendreportprop === "km00Z300000000000001" || item.pk_extendreportprop === 'zb00Z300000000000001'){
                                                    item.selected = 'Y';
                                                    item.disabled = false
                                                }else{
                                                    item.selected = 'N';
                                                    item.disabled = true
                                                }
                                            })
                                            let newColumnsData = deepClone(this.state.columnsData);
                                            this.setState({
                                                columnsData: newColumnsData
                                            })
                                        }
                                        //(this.state.choseRow)
                                        if(record.assvo || (record.selectCells && record.selectCells.length>0)){
                                            this.setState({
                                                assvo: record.assvo || record.selectCells
                                            })
                                        }else{
                                            this.setState({
                                                assvo: []
                                            })
                                        }
                                    }}
                                />
                            </NCDiv>
                        }
                        rightDom = {
                            <div>
                                {
                                    this.state.assvo.map((item) => {
                                        return this.renderRight(item)
                                    })
                                }
                            </div>
                        }
                    />

                    
                </div>
                <div className={"m-warp-bottom-box nc-theme-area-bgc nc-theme-area-split-bc"}>
                    <div className={"warp-bottom-top"}>
                        <div className={"ddbt-box"}>
                            <div className={"title-box nc-theme-common-font-c"}>{this.state.json['2002308005-000035']} :</div>{/* 国际化处理： 余额合计默认方向*/}
                            <Radio.NCRadioGroup
                                disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle}
                                name="lower"
                                selectedValue={this.state.selectedValue.balanceorient}
                                onChange={this.balanceorientChange.bind(this)}
                            >
                                <Radio value="1" clasName={"radio-left"}>{this.state.json['2002308005-000036']}</Radio>{/* 国际化处理： 借方*/}
                                <Radio value="-1" >{this.state.json['2002308005-000037']}</Radio>{/* 国际化处理： 贷方*/}
                            </Radio.NCRadioGroup>
                        </div>
                        <div  className={"ar-box"} style={{width: '40%'}}>
                            <div className={"title-box nc-theme-common-font-c"}>{this.state.json['2002308005-000038']}:</div>{/* 国际化处理： 应用范围*/}
                            <Radio.NCRadioGroup
                                name="lower"
                                selectedValue={this.state.selectedValue.applyrange.value}
                                onChange={this.userMan.bind(this)}
                            >
                                <Radio value="C" clasName={"radio-left"} >{this.state.json['2002308005-000039']}</Radio>{/* 国际化处理： 账簿公用*/}
                                <Radio value="P" >{this.state.json['2002308005-000010']}</Radio>{/* 国际化处理： 使用人*/}

                            </Radio.NCRadioGroup>
                            <div style={{ width:'50%'}}>
                                {mybook}
                            </div>
                        </div>
                    </div>

                    <div  className='bottomContainer'>
                        <div className='bottomContainerLeft nc-theme-common-font-c'>
                            {this.state.json['2002308005-000040']}：{/* 国际化处理： 展开方式*/}
                        </div>
                        <div className='bottomContainerRight'>
                            <Checkbox
                                disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle}
                                checked={ this.state.footData.containsub.value == 'Y'}
                                onChange={(val)=> this.footSubCheck(val)}
                            >
                                {this.state.json['2002308005-000041']} {/* 国际化处理： 辅助核算是否包含下级*/}
                            </Checkbox>
                            <Checkbox
                                disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle}
                                checked={ this.state.footData.subextend == 'Y'}
                                onChange={(val)=> this.footCheck(val, 'subextend')}
                            >
                                {this.state.json['2002308005-000042']}  {/* 国际化处理： 是否展开下级*/}
                            </Checkbox>
                            <Checkbox
                                disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle}
                                checked={ this.state.footData.rowsummaryflag == 'Y'}
                                onChange={(val)=> this.footCheck(val, 'rowsummaryflag')}
                            >
                                {this.state.json['2002308005-000043']} {/* 国际化处理： 显示合计行*/}
                            </Checkbox>
                            <Checkbox
                                disabled={this.props.getUrlParam('look') == 'false' && !this.state.browserStyle}
                                checked={ this.state.footData.colsummaryflag == 'Y'}
                                onChange={(val)=> this.footCheck(val, 'colsummaryflag')}
                            >
                                {this.state.json['2002308005-000044']} {/* 国际化处理： 显示合计列*/}
                            </Checkbox>
                        </div>

                    </div>

                </div>
            </div>
        )
    }
}

let initTemplate = (props) => {
}

Reckon = createPage({
    initTemplate: initTemplate
})(Reckon);

ReactDOM.render(<Reckon />,document.querySelector('#app'));
