import React, { Component } from 'react';
import {high,base,ajax,createPage, deepClone, promptBox, getMultiLang, createPageIcon } from 'nc-lightapp-front';
const { NCTable:Table, NCDiv } = base;

import createScript from '../../../public/components/uapRefer.js';
//import AccperiodMonthTreeGridRef from '../../../../uapbd/refer/org/AccountBookTreeRef';

//import {handleQueryClick,handleVerifyDetails,handleVerifySum,handleRefresh} from './events/index.js';
import './index.less'
import { conf } from '../../../../uapbd/refer/org/BusinessUnitTreeRef/index';
import {
    businessUnit,
    createReferFn,
    getReferDetault
} from "../../../manageReport/common/modules/modules";
import {searchById} from "../../../manageReport/common/modules/createPureBtn";
import '../../../public/reportcss/firstpage.less'
import {handleValueChange} from "../../../manageReport/common/modules/handleValueChange";
import {toast} from "../../../public/components/utils";
import HeaderArea from '../../../public/components/HeaderArea';
import { getTableHeight } from '../../../public/common/method.js'
const emptyCell = <span>&nbsp;</span>
class Reckon extends Component{
    constructor(props){
        super(props);
        this.state = {
            appcode: "2002308005",
            json: {},
            deletModal: false,//点击删除时控制确认弹框是否显示
            checkedAll: false,//全部选中复选框
            cellsChecked: false,//每一行的复选框
            tableData: [],
            period_pk: {
                refname: "",
                refpk: "",
                tableData: []
            },
            remeber: {
                refpk:""
            },
            pk_accountingbook: {
                display:'',
                value:''
            },
            loadData: {},
            refUrl: {
                accBook:'uapbd/refer/org/AccountBookTreeRef/index'
            },
            Columns:[]
        }
        this.selectNum = 0;
        this.getMaindata = this.getMaindata.bind(this);
        this.searchById = searchById.bind(this);
        this.handleValueChange = handleValueChange.bind(this);
    }
    getMaindata() {
        let url ='/nccloud/gl/accountrep/assanalysisreportquery.do';
        ajax({
            url,
            data: {
                "pk_accountingbook": "1001A3100000000000PE"
            },
            success: (response) => {
                //(response.data);
            }
        });
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json})
        }
        getMultiLang({moduleId:'2002308005',domainName:'gl',currentLocale:'simpchn',callback});
        this.getMaindata()
    }
    componentDidMount() {
        //('jjjjj>>>,',this.state,this.props.getSearchParam('c'), this.props,this.props.getUrlParam('hhahaah'), window.location.href,this.props.getUrlParam('c'))

        let appceod = this.props.getSearchParam('c');
        this.state.appcode = appceod;
        this.searchById('2002308005PAGE',appceod)
        this.props.button.setDisabled({
            add: true, browse: true, delete: true,
        });
        this.getParam();
    }

    getDefaultRefer = () => {
        setTimeout(() => getReferDetault(this, false,{
            businessUnit: businessUnit ,
            selfFunction: this.getInfoByBook
        }, 'accountrep'),0);
    }

    getParam = () => {
        //('getParam>>>', this.props.getUrlParam);
        let data = this.props.getUrlParam && this.props.getUrlParam('oldRef');
        //('datata:::',data);
        if(data){//如果data存在，那就是返回到首页的
            this.props.button.setDisabled({
                add: false, browse: false, delete: false,
            })
            let newData = JSON.parse(data);
            //('newData::', newData);
            this.handleValueChange('accountingbook', newData)
            businessUnit(this, newData, {
                getCheckContent: () => this.getInfoByBook()
            },'accountrep')
        }else{//否则就是直接进的首页
            this.getDefaultRefer()
        }
    }

    getInfoByBook = () => {
        //('getInfoByBook>>>', this.state.accountingbook)
        let url ='/nccloud/gl/accountrep/assanalysisreportquery.do';
        if (!this.state.accountingbook.refpk){
            this.getParam();
            toast({
                color: 'success'
            })
            return;
        }
        ajax({
            url,
            data: {
                "pk_accountingbook": this.state.accountingbook.refpk
            },
            success: (response) => {
                let arrTable=[];
                //('mybook', response.data);
                if(!response.data) {
                    this.setState({
                        tableData: arrTable
                    })
                    return;
                }
                response.data.forEach((item, index) => {
                    let initTable = {
                        "pk_extendreport": item.pk_extendreport,
                        "containsub": item.containsub.display,
                        "reportname": item.reportname,
                        "colitem": item.colitem,
                        "applyrange": item.applyrange.display,
                        "pk_accountingbook": item.pk_accountingbook,
                        "reportcode": item.reportcode,
                        "user": item.user,
                        "rowitem": item.rowitem,
                        "key": index
                    }
                    arrTable.push(initTable);
                })
                this.setState({
                    tableData: arrTable
                })
            }
        });
    }
    add = () => {
        if(!this.state.accountingbook.refpk){
            toast({content: this.state.json['2002308005-000000'], color: 'warning'});/* 国际化处理： 请先选择核算账簿*/
            return;
        }

        this.props.linkTo('/gl/accountrep/pages/end/index.html', {
            pk_accountingbook : this.state.accountingbook.refpk,
            dateData: JSON.stringify({
                busiDate: this.state.busiDate,
                versionDateArr: this.state.versionDateArr,
                pk_org: this.state.unitValueParam,// this.state.accountingbook.nodeData ? this.state.accountingbook.nodeData.pk_corp : this.state.accountingbook.pk_corp,
            }),
            appcode: this.state.appcode,
            pk_org: this.state.unitValueParam, //this.state.accountingbook.nodeData ? this.state.accountingbook.nodeData.pk_corp : this.state.accountingbook.pk_corp,
            selectRef: JSON.stringify(this.state.accountingbook),
            versiondate: JSON.stringify(this.state.versionDateArr)
        })
    }
    edit = (record) => {
        this.props.linkTo('/gl/accountrep/pages/end/index.html', {
            pk_accountingbook : record.pk_accountingbook,
            pk_extendreport : record.pk_extendreport,
            dateData: JSON.stringify({
                busiDate: this.state.busiDate,
                versionDateArr: this.state.versionDateArr,
                pk_org: this.state.unitValueParam,// this.state.accountingbook.nodeData ? this.state.accountingbook.nodeData.pk_corp : this.state.accountingbook.pk_corp,
            }),
            appcode: this.state.appcode,
            pk_org: this.state.unitValueParam,// this.state.accountingbook.nodeData ? this.state.accountingbook.nodeData.pk_corp : this.state.accountingbook.pk_corp,
            selectRef: JSON.stringify(this.state.accountingbook),
            versiondate: JSON.stringify(this.state.versionDateArr),
            user: JSON.stringify(record.user)
        })
    }
    browse = (record) => {
        //('recdddd:::', record);
        if(!this.state.loadData.pk_extendreport && !record) {
            toast({content: this.state.json['2002308005-000001'], color: 'warning'});/* 国际化处理： 请先选择核算账簿具体项*/
            return;
        }
        this.props.linkTo('/gl/accountrep/pages/end/index.html', {
            pk_accountingbook : this.state.loadData.pk_accountingbook ? this.state.loadData.pk_accountingbook: record.pk_accountingbook,
            pk_extendreport : this.state.loadData.pk_extendreport ? this.state.loadData.pk_extendreport: record.pk_extendreport,
            look: 'false',
            dateData: JSON.stringify({
                busiDate: this.state.busiDate,
                versionDateArr: this.state.versionDateArr,
                pk_org: this.state.unitValueParam,// this.state.accountingbook.nodeData ? this.state.accountingbook.nodeData.pk_corp : this.state.accountingbook.pk_corp,
            }),
            appcode: this.state.appcode,
            pk_org: this.state.unitValueParam,//this.state.accountingbook.nodeData ? this.state.accountingbook.nodeData.pk_corp : this.state.accountingbook.pk_corp,
            selectRef: JSON.stringify(this.state.accountingbook),
            versiondate: JSON.stringify(this.state.versionDateArr),
            user: JSON.stringify(record.user)
        })
    }

    delete = (record) => {
        //('deleted::', record);
        let url ='/nccloud/gl/accountrep/assanalysisreportdelete.do';
        ajax({
            url,
            data: {
                "pk_extendreport": record.pk_extendreport
            },
            success: (response) => {
                //(response.data);
                let url ='/nccloud/gl/accountrep/assanalysisreportquery.do';
                ajax({
                    url,
                    data: {
                        "pk_accountingbook": this.state.accountingbook.refpk
                    },
                    success: (response) => {
                        let arrTable=[];
                        //('mybook', response.data);
                        if(!response.data) {
                            this.setState({
                                tableData: [],
                                deletModal: false
                            })
                            return;
                        }
                        response.data.forEach((item, index) => {
                            let initTable = {
                                "pk_extendreport": item.pk_extendreport,
                                "containsub": item.containsub.display,
                                "reportname": item.reportname,
                                "colitem": item.colitem,
                                "applyrange": item.applyrange.display,
                                "pk_accountingbook": item.pk_accountingbook,
                                "reportcode": item.reportcode,
                                "user": item.user,
                                "rowitem": item.rowitem,
                                "key": index
                            }
                            arrTable.push(initTable);
                        })
                        this.setState({
                            tableData: arrTable,
                            deletModal: false
                        })
                    }
                });
            }
        });
    }
    handleLoginBtn = (obj, btnName) => {
        //('handleLoginBtn>>>', obj, btnName)
        if(btnName === 'add'){//新增
            this.add()
        }else if(btnName === 'edit'){//修改
            this.edit()
        }else if(btnName === 'browse'){//浏览
            this.browse()
        }else if(btnName === 'refresh'){//刷新
            this.getInfoByBook();
        }else if(btnName === 'print'){//打印
            //('print>>', btnName)
        }
    }
    newDeleted = (record) => {//删除
        //('ddddd::', record);
        promptBox({
            color: 'warning',
            title: this.state.json['2002308005-000002'],/* 国际化处理： 注意*/
            content: `${this.state.json['2002308005-000014']}${record.reportname}${this.state.json['2002308005-000015']}？`,             // 提示内容,非必输/* 国际化处理： 确定要删除,吗*/
            noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
            noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
            beSureBtnName: this.state.json['2002308005-000003'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
            cancelBtnName: this.state.json['2002308005-000004'],           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
            beSureBtnClick: () => this.delete(record),   // 确定按钮点击调用函数,非必输
        })
    }
    render() {
        //('render:::', this.state)
        const columns = [
            {
                title: (<div fieldid='reportcode'>{this.state.json['2002308005-000005']}</div>),/* 国际化处理： 编码*/
                dataIndex: 'reportcode',
                key: 'reportcode',
                render: (text, record) => {
                    return(
                        <div
                            fieldid='reportcode'
                            style={{color:'#007ACE', cursor: 'pointer'}}
                            onClick={() => this.browse(record)}
                        >
                            {text ? text : emptyCell}
                        </div>
                    )
                }
            },
            { 
                // id: '123', 
                title: (<div fieldid='reportname'>{this.state.json['2002308005-000006']}</div>), 
                dataIndex: 'reportname', 
                key: 'reportname',
                render: (text, record, index) => <div fieldid='reportname'>{text ? text : emptyCell}</div>
            },/* 国际化处理： 名称*/
            { 
                // id: '123', 
                title: (<div fieldid='rowitem'>{this.state.json['2002308005-000007']}</div>), 
                dataIndex: 'rowitem', 
                key: 'rowitem',
                render: (text, record, index) => <div fieldid='rowitem'>{text ? text : emptyCell}</div>
            },/* 国际化处理： 行标题*/
            { 
                // id: '123', 
                title: (<div fieldid='colitem'>{this.state.json['2002308005-000008']}</div>), 
                dataIndex: 'colitem', 
                key: 'colitem',
                render: (text, record, index) => <div fieldid='colitem'>{text ? text : emptyCell}</div>
            },/* 国际化处理： 列标题*/
            { 
                // id: '123', 
                title: (<div fieldid='containsub'>{this.state.json['2002308005-000009']}</div>), 
                dataIndex: 'containsub', 
                key: 'containsub',
                render: (text, record, index) => <div fieldid='containsub'>{text ? text : emptyCell}</div>
            },/* 国际化处理： 辅助核算包含下级*/
            {
                // id: '123',
                title: (<div fieldid='user'>{this.state.json['2002308005-000010']}</div>),
                dataIndex: 'user',
                key: 'user',
                render: (text, record) => {
                    //('userrrrr::', text, record);
                    return <div fieldid='user'>{text.display ? text.display : emptyCell}</div>
                }
            },/* 国际化处理： 使用人*/
            { 
                // id: '123', 
                title: (<div fieldid='applyrange'>{this.state.json['2002308005-000011']}</div>), 
                dataIndex: 'applyrange', 
                key: 'applyrange',
                render: (text, record, index) => <div fieldid='applyrange'>{text ? text : emptyCell}</div>
            },/* 国际化处理： 使用范围*/
            {
                title: (<div fieldid='opr'>{this.state.json['2002308005-000012']}</div>),/* 国际化处理： 操作*/
                dataIndex: 'oparation',
                key: 'oparation',
                render: (text, record) => {
                    //(this.state.json['2002308005-000013'], text, record)/* 国际化处理： 操作：*/
                    return <div fieldid='opr'>
                        <span
                            style={{color:'#007ACE', cursor: 'pointer'}}
                            onClick={ () => this.edit(record)}>
                            {this.state.json['2002308005-000016']}{/* 国际化处理： 修改*/}
                        </span>
                        <span
                            style={{color:'#007ACE', cursor: 'pointer', margin: '10px'}}
                            onClick={ () => this.browse(record)}>
                            {this.state.json['2002308005-000017']}{/* 国际化处理： 浏览*/}
                        </span>
                        <span
                            style={{color:'#007ACE', cursor: 'pointer'}}
                            onClick={ () => {
                                this.newDeleted(record)
                            }}>
                            {this.state.json['2002308005-000018']}{/* 国际化处理： 删除*/}
                        </span>
                    </div>
                }
            }
        ];
        let { period_pk }=this.state;
        return (
            <div className="reckoning accountrep">
                <HeaderArea 
                    title = {this.state.json['2002308005-000019']} /* 国际化处理： 辅助分析设置*/
                    showBorderBottom = {false}
                    searchContent = {
                        <div className={"serrch-ref"}>
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: false,
                                        "isShowDisabledData": true,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit ,
                                        selfFunction: this.getInfoByBook
                                    },
                                    'accountrep'
                                )
                            }
                        </div>
                    }
                    btnContent = {this.props.button.createButtonApp({
                        area: 'btnarea',
                        buttonLimit: 3,
                        onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName)
                    })}
                />
                <div className="mainBody">
                    <NCDiv fieldid="accountrep" areaCode={NCDiv.config.TableCom} style={{width: "100%", height: '86vh'}}>
                        <Table
                            bodyStyle={{height:getTableHeight(80)}}
                            scroll={{ x: true, y: getTableHeight(80) }}                            
                            columns={columns}
                            data={this.state.tableData}
                            onRowDoubleClick = {(record) => {
                                //('onRowDoubleClick>', record);
                                this.browse(record);
                            }}
                            onRowClick = {(record,index) => {
                                //('click:::',record, index);
                                this.props.button.setDisabled({
                                    browse: false, delete: false,
                                })
                                this.state.loadData.pk_accountingbook = record.pk_accountingbook;
                                this.state.loadData.pk_extendreport = record.pk_extendreport;
                                this.state.loadData.reportname = record.reportname;
                                this.state.loadData.multicolname = record.multicolname;
                                this.setState({
                                    loadData: this.state.loadData
                                })
                            }}
                        />
                    </NCDiv>
				</div>
            </div>
        )
    }
}

let initTemplate = (props) => {
  
}

Reckon = createPage({
    initTemplate: initTemplate
})(Reckon);

ReactDOM.render(<Reckon />,document.querySelector('#app'));
