import setScale from '../../public/common/amountFormat.js';
import {Subtr,accMul,accDiv} from '../../public/common/method.js';
import {getCurrInfo} from './currInfo.js';
import { cardCache } from 'nc-lightapp-front';
let { getDefData } = cardCache;
import {mutilangJson_card} from '../../public/components/constJSON';
/**
 * 金额换算公式 VoucherModel L2696
 */
let local_convert='local_convert';//根据本币计算
let raw_convert='raw_convert';//根据原币计算
// data: {
//     amount:,//原币
//     localamount,//组织本币
//     groupamount,//集团本币
//     globalamount,//全局本币
//     pk_currtype,//原币币种
//     orgcurrtype,//本币币种
//     groupcurrtype,//集团本币币种
//     globalcurrtype,//全局本币币种
//     excrate2,//本币汇率
//     excrate3,//集团本币汇率
//     excrate4,//全局本币汇率
//     price,//单价
//     quantity,//数量 
//    pk_accountingbook,//账簿
//    prepareddate//制单日期
//  }
// currInfo: {
//     scale,//原币精度 
//     orgscale,//组织本币精度
//     groupscale,//集团本币精度
//     globalscale,//全局本币精度
//     NC001,//集团本币计算方式
//     NC002,//全局本币计算方式
//     orgmode,//汇率计算方式 是否为除 true=除
//     groupmode,//集团本币汇率计算方式 是否为除 true=除
//     globalmode,//全局本币汇率计算方式 是否为除 true=除
//     roundtype,//原币进舍规则
//     orgroundtype,//本币进舍规则
//     grouproundtype,//集团本币进舍规则
//     globalroundtype,//全局本币进舍规则
//     pricescale,//单价精度
//     priceroundtype,//单价进舍规则
//     maxconverr,//最大折算误差 原币和组织本币
//     quantityscale,//数量精度
//     excrate2scale,//本币汇率精度
//     excrate3scale,//集团本币汇率精度
//     excrate4scale,//全局本币汇率精度
// }
 export default function(data,currInfo,key){
     if(data.pk_accountingbook&&data.pk_accountingbook!=''){
        currInfo=getCurrInfo(data,currInfo);
     }
     for(let key in data){
        if(key=='pk_accountingbook'||key=='prepareddate'||key=='pk_currtype'
            ||key=='orgcurrtype'||key=='groupcurrtype'||key=='globalcurrtype')
            continue;
        if(!data[key]||data[key]=='')
            data[key]=0;
     }
     switch (key) {
         case 'amount':
         case 'creditamount':
         case 'debitamount':
             return changeAmount(data, currInfo);
         case 'localamount':
         case 'localdebitamount':
         case 'localcreditamount':
             return changeLocalamount(data, currInfo);
         case 'groupamount':
         case 'groupdebitamount':
         case 'groupcreditamount':
             return changeGroupAmount(data, currInfo);
         case 'globalamount':
         case 'globaldebitamount':
         case 'globalcreditamount':
             return changeGlobalAmount(data, currInfo);
         case 'price':
             return changePrice(data, currInfo);
         case 'debitquantity':
         case 'creditquantity':
             return changeQuantity(data, currInfo);
         case 'excrate2':
             return changeExcrate2(data, currInfo);
         case 'excrate3':
             return changeExcrate3(data, currInfo);
         case 'excrate4':
             return changeExcrate4(data, currInfo);
         default:
             break;
     }

}
function changeAmount(data, currInfo) {//修改原币
    if(!data.amount||data.amount==0)
        return {};
    let caldata = {
        amount: data.amount
    };
    //正算金额
    //计算组织本币
    let orgdata = getLocalAmount(data.amount, data, currInfo);
    //计算集团本币
    let groupdata = getGroupAmount(caldata.amount, orgdata.localamount, data, currInfo);
    //计算全局本币
    let globaldata = getGlobalAmount(caldata.amount, orgdata.localamount, data, currInfo);
    //数量和单价
    let pqdata = calPriceAndQuantity(caldata.amount, data);
    //处理数据格式
    return dealScale(Object.assign({},caldata, orgdata, groupdata, globaldata, pqdata),currInfo);
}

function changeLocalamount(data, currInfo) {
    //1.如果组织本币=原币，组织本币变化，原币随之变化
    //2.如果组织本币<>原币，组织本币变化，原币、汇率不变化，校验折算误差
    //3.如果原币未录入值，先录组织本币，自动计算原币
    let inlt = getDefData('mutilangInlt_card', mutilangJson_card);
    let caldata = {
        localamount: data.localamount
    };
    if(data.pk_currtype==data.orgcurrtype){
        caldata.amount=data.localamount;
        caldata.excrate2=1;
         //数量和单价
        let pqdata = calPriceAndQuantity(caldata.amount, data);
        Object.assign(caldata, pqdata)
    }else{
        if(data.amount&&data.amount-0!=0){
            caldata.amount=data.amount;
            caldata.excrate2=data.excrate2;
            //校验折算误差
            let orgdata = getLocalAmount(data.amount, data, currInfo);
            let calocalamount=setScale(orgdata.localamount,currInfo.orgscale,currInfo.orgroundtype)
            let short=Math.abs(Subtr(calocalamount,data.localamount));
            if(Subtr(short,currInfo.maxconverr)>0){
               caldata.message=inlt.get('20021005card-000000',{n1:data.localamount,n2:calocalamount})/* 国际化处理： 组织本币金额,与折算金额,的差额超出原币币种的最大折算误差*/
            }
        }else{ 
            //计算原币
            let occdata = getAmount(data.localamount, data, currInfo);
            caldata.amount=occdata.amount;
            caldata.excrate2=occdata.excrate2;
        }

        //数量和单价
        let pqdata = calPriceAndQuantity(caldata.amount, data);

        Object.assign(caldata, pqdata)
    } 
    caldata.amount=setScale(caldata.amount,currInfo.scale,currInfo.roundtype)
    //计算集团本币
    let groupdata = getGroupAmount(caldata.amount, caldata.localamount, data, currInfo);
    //计算全局本币
    let globaldata = getGlobalAmount(caldata.amount, caldata.localamount, data, currInfo);


    return dealScale(Object.assign(caldata, groupdata, globaldata), currInfo);

}

function changeGroupAmount(data,currInfo){
    return data;
    // let caldata={
    //     groupamount:data.groupamount
    // };

    // if(raw_convert==currInfo.NC001){
    //     if(data.pk_currtype==data.groupcurrtype){
    //         caldata.excrate3=1;
    //         caldata.amount=caldata.groupamount;
    //     }else{
    //         let groupdata=calSrcAmount(data.amount,caldata.groupamount,data.excrate3,currInfo.groupmode);
    //         caldata.excrate3=groupdata.excrate3;
    //         caldata.amount=groupdata.amount;
    //     }
    //     //计算组织本币
    //     let orgdata = getLocalAmount(caldata.amount, data, currInfo);
    //     //计算全局本币
    //     let globaldata = getGlobalAmount(caldata.amount, caldata.localamount, data, currInfo);

    //     Object.assign(caldata, orgdata, globaldata);
    // }else if(local_convert==currInfo.NC001){
    //     if(data.orgcurrtype==data.groupcurrtype){
    //         caldata.excrate3=1;
    //         caldata.localamount=caldata.groupamount;
    //     }else{
    //         let groupdata=calSrcAmount(localamount,data.groupamount,data.excrate3,currInfo.groupmode);
    //         caldata.excrate3=groupdata.excrate3;
    //         caldata.localamount=groupdata.localamount;
    //     }
    //     //计算原币
    //     let occdata = getAmount(caldata.localamount, data, currInfo);
    //     //计算全局本币
    //     let globaldata = getGlobalAmount(caldata.amount, caldata.localamount, data, currInfo);

    //     Object.assign(caldata, occdata, globaldata);
    // }
    //  //数量和单价
    //  let pqdata = calPriceAndQuantity(caldata.amount, data);

    //  return dealScale(Object.assign(caldata, pqdata),currInfo);
}

function changeGlobalAmount(data,currInfo){
    return data;
//     let caldata={
//         globalamount:data.globalamount
//     };

//     if(raw_convert==currInfo.NC002){
//         if(data.pk_currtype==data.globalcurrtype){
//             caldata.excrate4=1;
//             caldata.amount=caldata.globalamount;
//         }else{
//             let globaldata=calSrcAmount(data.amount,caldata.globalamount,data.excrate4,currInfo.globalmode);
//             caldata.excrate4=globaldata.excrate4;
//             caldata.amount=globaldata.amount;
//         }

//         //计算组织本币
//         let orgdata = getLocalAmount(caldata.amount, data, currInfo);
//         //计算集团本币
//         let groupdata = getGroupAmount(caldata.amount, caldata.localamount, data, currInfo);

//         Object.assign(caldata, orgdata,groupdata);
//     }else if(local_convert==currInfo.NC001){
//         if(data.orgcurrtype==data.globalcurrtype){
//             caldata.excrate4=1;
//             caldata.localamount=caldata.globalamount;
//         }else{
//             let groupdata=calSrcAmount(data.localamount,caldata.globalamount,data.excrate3,currInfo.globalmode);
//             caldata.excrate4=groupdata.excrate4;
//             caldata.localamount=groupdata.localamount;
//         }
        
//         //计算原币
//         let occdata = getAmount(caldata.localamount, data, currInfo);
//         //计算集团本币
//         let groupdata = getGroupAmount(caldata.amount, caldata.localamount, data, currInfo);

//         Object.assign(caldata, occdata,groupdata);
//     }

//    //数量和单价
//    let pqdata = calPriceAndQuantity(caldata.amount, data);

//    return dealScale(Object.assign(caldata, pqdata),currInfo);
}

function changePrice(data,currInfo){
    let caldata = {
        price: data.price
    }
    if (data.quantity - 0 != 0) {
        //正算金额
        caldata.amount = accMul(caldata.price,data.quantity);
        caldata.quantity = data.quantity;
    } else if (data.amount - 0 != 0) {
        //反算数量
        caldata.quantity = accDiv(data.amount,data.price);
        caldata.amount = data.amount;
    }

    //正算金额
    //计算组织本币
    let orgdata = getLocalAmount(caldata.amount, data, currInfo);
    //计算集团本币
    let groupdata = getGroupAmount(caldata.amount, orgdata.localamount, data, currInfo);
    //计算全局本币
    let globaldata = getGlobalAmount(caldata.amount, orgdata.localamount, data, currInfo);

    return dealScale(Object.assign(caldata, orgdata, groupdata, globaldata),currInfo);
}

function changeQuantity(data,currInfo){
    let caldata={
        quantity:data.quantity
    }

    if(data.price-0!=0){
        //正算金额
        caldata.amount = accMul(data.price,caldata.quantity);
        caldata.price = data.price;
    }else if(data.amount-0!=0){
        //反算单价
        if(caldata.quantity-0==0){
            caldata.price=0;
        }else{
            caldata.price =accDiv(data.amount,caldata.quantity);
        }
        caldata.amount=data.amount;
    }

    //正算金额
    //计算组织本币
    let orgdata = getLocalAmount(caldata.amount, data, currInfo);
    //计算集团本币
    let groupdata = getGroupAmount(caldata.amount, orgdata.localamount, data, currInfo);
    //计算全局本币
    let globaldata = getGlobalAmount(caldata.amount, orgdata.localamount, data, currInfo);

    return dealScale(Object.assign(caldata, orgdata, groupdata, globaldata),currInfo);
}

function changeExcrate2(data,currInfo){
    let caldata={
        excrate2:data.excrate2
    }

    if(data.amount-0!=0){
        let orgdata=getLocalAmount(data.amount,data,currInfo);
        caldata.amount=data.amount;
        caldata.excrate2=orgdata.excrate2;
        caldata.localamount=orgdata.localamount;
    }else if(data.localamount-0!=0){
        let occdata=getAmount(data.localamount,data,currInfo);
        caldata.amount=occdata.amount;
        caldata.excrate2=occdata.excrate2;
        caldata.localamount=data.localamount;
    }

    //计算集团本币
    let groupdata = getGroupAmount(caldata.amount, caldata.localamount, data, currInfo);
    //计算全局本币
    let globaldata = getGlobalAmount(caldata.amount, caldata.localamount, data, currInfo);
    //数量和单价
    let pqdata = calPriceAndQuantity(caldata.amount, data);
    //处理数据格式
    return dealScale(Object.assign(caldata, groupdata, globaldata, pqdata),currInfo);
}

function changeExcrate3(data,currInfo){
    return dealScale(getGroupAmount(data.amount,data.localamount,data,currInfo),currInfo);
}

function changeExcrate4(data,currInfo){
    return dealScale(getGlobalAmount(data.amount,data.localamount,data,currInfo),currInfo);
}

//根据来源金额，汇率1，目的金额 正算
function calDesAmount(srcAmount,desAmount,rate,ismode){
    let data={
        srcAmount:srcAmount,
        desAmount:desAmount,
        rate:rate
    }
    if(rate&&rate-0!=0){
        //正算
        if(ismode){
            data.desAmount=accDiv(srcAmount,rate);
        }else{
            data.desAmount=accMul(srcAmount,rate);
        }
    }else if(rate-0==1){
        data.desAmount=srcAmount;
    }else if(srcAmount-0==0){
        data.desAmount=0;
    }else if(rate-0==0&&desAmount-0!=0){
        //反算汇率
        if(ismode){
            data.rate=accDiv(srcAmount,desAmount);
        }else{
            data.rate=accDiv(desAmount,srcAmount);
        }
    }else if(rate-0==0){
        data.desAmount=0;
    }
    return data;
}

//根据 目的金额，汇率 来源金额 反算
function calSrcAmount(srcAmount,desAmount,rate,ismode){
    let data={
        srcAmount:srcAmount,
        desAmount:desAmount,
        rate:rate
    }

    if(rate&&rate-0!=0){
        if(ismode){
            data.srcAmount=accMul(desAmount,rate);
        }else{
            data.srcAmount=accDiv(desAmount,rate);
        }
    }else if(rate-0==1){
        data.srcAmount=desAmount;
    }else if(desAmount-0==0){
        data.srcAmount=0;
    }else if(rate-0==0&&srcAmount-0!=0){
        //反算汇率
        if(ismode){
            data.rate=accDiv(srcAmount,desAmount);
        }else{
            data.rate=accDiv(desAmount,srcAmount);
        }
    }else if(rate-0==0){
        data.srcAmount=0;
    }
    return data;
}

//计算原币 
function getAmount(localamount,data,currInfo){
    let caldata={};
    if(data.pk_currtype==data.orgcurrtype){
        //原币==组织本币
        caldata.excrate2=1;
        caldata.amount=localamount;        
    }else{
        //计算组织本币
        let orgdata=calSrcAmount(data.amount,localamount,data.excrate2,currInfo.orgmode);
        caldata.excrate2=orgdata.rate;
        caldata.amount=orgdata.srcAmount;
    }
    return caldata;
}

//计算组织本币
function getLocalAmount(amount,data,currInfo){
    let caldata={};
    if(data.pk_currtype==data.orgcurrtype){
        //原币==组织本币
        caldata.excrate2=1;
        caldata.localamount=amount;        
    }else{
        //计算组织本币
        let orgdata=calDesAmount(amount,data.localamount,data.excrate2,currInfo.orgmode);
        caldata.excrate2=orgdata.rate;
        caldata.localamount=orgdata.desAmount;
    }
    return caldata;
}

//计算集团本币
function getGroupAmount(amount,localamount,data,currInfo){
    let caldata={};
    if(raw_convert==currInfo.NC001){
        if(data.pk_currtype==data.groupcurrtype){
            caldata.excrate3=1;
            caldata.groupamount=amount;
        }else{
            let groupdata=calDesAmount(amount,data.groupamount,data.excrate3,currInfo.groupmode);
            caldata.excrate3=groupdata.rate;
            caldata.groupamount=groupdata.desAmount;
        }            
    }else if(local_convert==currInfo.NC001){
        if(data.orgcurrtype==data.groupcurrtype){
            caldata.excrate3=1;
            caldata.groupamount=localamount;
        }else{
            let groupdata=calDesAmount(localamount,data.groupamount,data.excrate3,currInfo.groupmode);
            caldata.excrate3=groupdata.rate;
            caldata.groupamount=groupdata.desAmount;
        }            
    }
    return caldata;
}

//计算全局本币
function getGlobalAmount(amount,localamount,data,currInfo){
    let caldata={};
    if(raw_convert==currInfo.NC002){
        if(data.pk_currtype==data.globalcurrtype){
            caldata.excrate4=1;
            caldata.globalamount=amount;
        }else{
            let globaldata=calDesAmount(amount,data.globalamount,data.excrate4,currInfo.globalmode);
            caldata.excrate4=globaldata.rate;
            caldata.globalamount=globaldata.desAmount;
        }
    }else if(local_convert==currInfo.NC002){
        if(data.orgcurrtype==data.globalcurrtype){
            caldata.excrate4=1;
            caldata.globalamount=localamount;
        }else{
            let groupdata=calDesAmount(localamount,data.globalamount,data.excrate4,currInfo.globalmode);
            caldata.excrate4=groupdata.rate;
            caldata.globalamount=groupdata.desAmount;
        }        
    }
    return caldata;
}

function calPriceAndQuantity(amount,data){
    let caldata={amount:amount};
    //数量和单价
    if(data.quantity&&data.quantity-0!=0){
        //数量不为0，反算单价
        //TODO 取单价的精度 这里可以使用缓存
        caldata.price=accDiv(amount,data.quantity);
        caldata.quantity=data.quantity;
    }else if(data.price&&data.price-0!=0){
        //反算数量
        caldata.price=data.price;
        caldata.quantity=accDiv(amount,data.price);
    }
    return caldata;
}

function dealScale(caldata,currInfo){
    if(caldata.amount){
        caldata.amount=setScale(caldata.amount,currInfo.scale,currInfo.roundtype);
    }
    if(caldata.localamount){
        caldata.localamount=setScale(caldata.localamount,currInfo.orgscale,currInfo.orgroundtype);
    }
    if(caldata.groupamount){
        caldata.groupamount=setScale(caldata.groupamount,currInfo.groupscale,currInfo.grouproundtype);
    }
    if(caldata.globalamount){
        caldata.globalamount=setScale(caldata.globalamount,currInfo.globalscale,currInfo.globalroundtype);
    }
    if(caldata.price){
        caldata.price=setScale(caldata.price,currInfo.pricescale,currInfo.priceroundtype);
    }
    if(caldata.quantity){
        caldata.quantity=setScale(caldata.quantity,currInfo.quantityscale,'4');
    }
    if(caldata.excrate2){
        caldata.excrate2=setScale(caldata.excrate2,currInfo.excrate2scale,'4');
    }
    if(caldata.excrate3){
        caldata.excrate3=setScale(caldata.excrate3,currInfo.excrate3scale,'4');
    }
    if(caldata.excrate4){
        caldata.excrate4=setScale(caldata.excrate4,currInfo.excrate4scale,'4');
    }
    return caldata;
}

//export default amountConvert
