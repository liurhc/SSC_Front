import { ajax,cardCache } from 'nc-lightapp-front';
let { setDefData, getDefData } = cardCache;
import {currInfoDataSource} from '../../public/components/constJSON';
function getCurrInfo(data,currInfo){
    //金额换算
    let currdata=queryCurrinfo(data.pk_accountingbook,data.pk_currtype,data.prepareddate);
    if (currdata) {
        currInfo.scale = currdata.scale;//原币精度
        currInfo.orgscale = currdata.orgscale;//组织本币精度
        currInfo.groupscale = currdata.groupscale;//集团本币精度
        currInfo.globalscale = currdata.globalscale;//全局本币精度
        currInfo.NC001 = currdata.NC001;//集团本币计算方式
        currInfo.NC002 = currdata.NC002;//全局本币计算方式
        currInfo.orgmode = currdata.orgmode;//汇率计算方式 是否为除 true=除
        currInfo.groupmode = currdata.groupmode;//集团本币汇率计算方式 是否为除 true=除
        currInfo.globalmode = currdata.globalmode;//全局本币汇率计算方式 是否为除 true=除
        currInfo.roundtype = currdata.roundtype;//原币进舍规则
        currInfo.orgroundtype = currdata.orgroundtype;//本币进舍规则
        currInfo.grouproundtype = currdata.grouproundtype;//集团本币进舍规则
        currInfo.globalroundtype = currdata.globalroundtype;//全局本币进舍规则
        currInfo.pricescale = currdata.pricescale;//单价精度
        currInfo.priceroundtype = currdata.priceroundtype;//单价进舍规则
        currInfo.maxconverr = currdata.maxconverr;//最大折算误差 原币和组织本币
        // currInfo.quantityscale=quantityscale;//数量精度
        currInfo.excrate2scale = currdata.excrate2scale;//本币汇率精度
        currInfo.excrate3scale = currdata.excrate3scale;//集团本币汇率精度
        currInfo.excrate4scale = currdata.excrate4scale;//全局本币汇率精度
        data.orgcurrtype = currdata.orgcurrtype;
        data.groupcurrtype = currdata.groupcurrtype;
        data.globalcurrtype = currdata.globalcurrtype;
        if(!data.excrate2||data.excrate2==''){
            data.excrate2=currdata.excrate2;
        }
        if(!data.excrate3||data.excrate3==''){
            data.excrate3=currdata.excrate3;
        }
        if(!data.excrate4||data.excrate4==''){
            data.excrate4=currdata.excrate4;
        }
    }
    
    return currInfo;
}

function queryCurrinfo(pk_accountingbook,pk_currtype,date){
    //将币种做缓存
    let url='/nccloud/gl/glpub/ratequery.do';
    let param = {
        "pk_accountingbook": pk_accountingbook,
        "pk_currtype": pk_currtype,
        "prepareddate": date
    };
    let key=pk_accountingbook+pk_currtype+date;
    let currdata=getDefData(key,currInfoDataSource);
    if(!currdata){
        ajax({
            url: url,
            data: param,
            async: false,
            success: function (response) {
                //加入缓存
                setDefData(key, currInfoDataSource, response.data);
                currdata=response.data;
            }
        });
    }
    return currdata
}


export { getCurrInfo,queryCurrinfo };
