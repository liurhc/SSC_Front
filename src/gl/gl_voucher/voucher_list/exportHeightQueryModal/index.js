import React, { Component } from 'react';
import { cardCache, getMultiLang,base } from 'nc-lightapp-front';
import SearchModal from '../list/searchModal/searchModal';
import { searchBtnClick, onAfterEvent, initTemplate } from '../list/events';
import loadQuery from './queryJson.js';
import { dataSourceHeightQuery } from '../../../public/components/constJSON';
import { dataSourceValue } from '../../../public/components/constJSON';
let { getDefData } = cardCache;
const {NCButton} = base;
import transferFormat from '../list/events/transferFormat';
export default class ExportHeightQueryModal extends Component {
    constructor(props) {
        super(props)
        this.searchId = this.props.searchId;
        //this.searchId = "20021005query"
        this.state = {
            clickPlanEve:false,
            queryList: {},
            pk_accountingbook: "",
            SearchModalShow: false,//this.props.SearchModalShow
        }
        this.addAdvBody = this.addAdvBody.bind(this)
    }
    onRef = (ref) => {
        this.child = ref;
    };
    addAdvBody = () => {
        let { queryList, pk_accountingbook, SearchModalShow } = this.state;
        const emptyFunc = () => <span>{this.props.stateJson['20021005list-000054']}</span>;/* 国际化处理： 这里没有数据*/
        let checkQuery = {};
        if (getDefData('heightQuery', dataSourceHeightQuery) && getDefData('heightQuery', dataSourceHeightQuery).length != '0') {
            checkQuery = getDefData('heightQuery', dataSourceHeightQuery);//dataSourceHeight
        } else {
            checkQuery.pk_accountingbook = pk_accountingbook
        }
        let voucherState = this.props.getUrlParam('type');
        const loadQuery = [
            //多选的有 核算账簿、摘要、科目编码、对方科目、币种、业务单元
            {
                itemName: this.props.stateJson['20021005list-000001'],/* 国际化处理： 财务核算账簿*/
                itemType: 'refer',
                itemKey: 'pk_accountingbook',
                isMultiSelectedEnabled: true,
                config: { refCode: 'uapbd/refer/org/AccountBookTreeRef' },
                queryGridUrl: '',
                refType: 'grid',
                showMust: true
            },
            {
                itemName: this.props.stateJson['20021005list-000075'],/* 国际化处理： 业务单元*/
                itemType: 'refer',
                itemKey: 'pk_unit',
                isMultiSelectedEnabled: true,
                config: { refCode: 'uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            {
                itemName: this.props.stateJson['20021005list-000076'],/* 国际化处理： 会计期间*/
                itemType: 'refer',
                itemKey: 'period',
                isMultiSelectedEnabled: true,
                config: { refCode: 'uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            { itemName: this.props.stateJson['20021005list-000077'], itemType: 'date', itemKey: 'prepareddate' },/* 国际化处理： 日期*/
            {
                itemName: this.props.stateJson['20021005list-000003'],/* 国际化处理： 凭证类别*/
                itemType: 'refer',
                itemKey: 'pk_vouchertype',
                isMultiSelectedEnabled: false,
                config: { refCode: 'uapbd/refer/fiacc/VoucherTypeDefaultGridRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            { itemName: this.props.stateJson['20021005list-000004'], itemType: 'textInput', itemKey: 'num' },/* 国际化处理： 凭证号*/
            {
                itemName: this.props.stateJson['20021005list-000078'],/* 国际化处理： 制单系统*/
                itemType: 'refer',
                itemKey: 'pk_system',
                isMultiSelectedEnabled: false,
                config: { refCode: 'gl/refer/voucher/CtlSysGridRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            { itemName: this.props.stateJson['20021005list-000079'], itemType: 'textInput', itemKey: 'attachment' },/* 国际化处理： 附单数据*/
            {
                itemName: this.props.stateJson['20021005list-000005'], itemType: 'refer', itemKey: 'explanation', isMultiSelectedEnabled: true,/* 国际化处理： 摘要*/
                config: { refCode: 'fipub/ref/pub/SummaryRef' },
            },
            {
                itemName: this.props.stateJson['20021005list-000080'],/* 国际化处理： 币种*/
                itemType: 'refer',
                itemKey: 'pk_currtype',
                isMultiSelectedEnabled: true,
                config: { refCode: 'uapbd/refer/pubinfo/CurrtypeGridRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            {
                itemName: this.props.stateJson['20021005list-000081'],/* 国际化处理： 科目编码*/
                itemType: 'refer',
                itemKey: 'accountingcode',
                isMultiSelectedEnabled: true,
                config: { refCode: 'uapbd/refer/fiacc/AccountDefaultGridTreeRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            {
                itemName: this.props.stateJson['20021005list-000082'],/* 国际化处理： 对方科目*/
                itemType: 'refer',
                itemKey: 'oppositesubj',
                isMultiSelectedEnabled: true,
                config: { refCode: 'uapbd/refer/fiacc/AccountDefaultGridTreeRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            {
                itemName: this.props.stateJson['20021005list-000003'],/* 国际化处理： 凭证类别*/
                itemType: 'checkbox',
                itemKey: 'gl_Checkbox',
                itemChild: [
                    {
                        label: this.props.stateJson['20021005list-000083'],/* 国际化处理： 正常凭证*/
                        itemKey: 'normalflag',
                        checked: true
                    },
                    {
                        label: this.props.stateJson['20021005list-000084'],/* 国际化处理： 错误凭证*/
                        itemKey: 'errorflag',
                        checked: true
                    },
                    {
                        label: this.props.stateJson['20021005list-000085'],/* 国际化处理： 暂存凭证*/
                        itemKey: 'tempsaveflag',
                        checked: true
                    },
                    {
                        label: this.props.stateJson['20021005list-000086'],/* 国际化处理： 作废凭证*/
                        itemKey: 'discardflag',
                        checked: true
                    }
                ]
            },
            {
                itemName: this.props.stateJson['20021005list-000087'],/* 国际化处理： 本币类型*/
                itemType: 'radio',
                itemKey: 'amountType',
                itemChild: [
                    {
                        label: this.props.stateJson['20021005list-000088'],/* 国际化处理： 组织本币*/
                        value: '1',
                        disabled: false
                    },
                    {
                        label: this.props.stateJson['20021005list-000089'],/* 国际化处理： 集团本币*/
                        value: '2',
                        disabled: true
                    },
                    {
                        label: this.props.stateJson['20021005list-000090'],/* 国际化处理： 全局本币*/
                        value: '3',
                        disabled: true
                    }
                ]
            },
            {
                itemName: this.props.stateJson['20021005list-000091'],/* 国际化处理： 原币*/
                itemType: 'mix',
                itemKey: 'oriamount',
                itemChildOne: [
                    {
                        label: this.props.stateJson['20021005list-000092'],/* 国际化处理： 双向*/
                        value: '0'
                    },
                    {
                        label: this.props.stateJson['20021005list-000093'],/* 国际化处理： 借*/
                        value: '1'
                    },
                    {
                        label: this.props.stateJson['20021005list-000094'],/* 国际化处理： 贷*/
                        value: '2'
                    }
                ],
                itemChildTwo: [
                    {
                        label: this.props.stateJson['20021005list-000095'],/* 国际化处理： 且*/
                        value: 'and'
                    },
                    {
                        label: this.props.stateJson['20021005list-000096'],/* 国际化处理： 或*/
                        value: 'or'
                    }
                ]
            },
            {
                itemName: this.props.stateJson['20021005list-000097'],/* 国际化处理： 本币*/
                itemType: 'mix',
                itemKey: 'amount',
                itemChildOne: [
                    {
                        label: this.props.stateJson['20021005list-000092'],/* 国际化处理： 双向*/
                        value: '0'
                    },
                    {
                        label: this.props.stateJson['20021005list-000093'],/* 国际化处理： 借*/
                        value: '1'
                    },
                    {
                        label: this.props.stateJson['20021005list-000094'],/* 国际化处理： 贷*/
                        value: '2'
                    }
                ],
                itemChildTwo: [
                    {
                        label: this.props.stateJson['20021005list-000095'],/* 国际化处理： 且*/
                        value: 'and'
                    },
                    {
                        label: this.props.stateJson['20021005list-000096'],/* 国际化处理： 或*/
                        value: 'or'
                    }
                ]
            },
            {
                itemName: this.props.stateJson['20021005list-000151'],/* 国际化处理： 数量*/
                itemType: 'mix',
                itemKey: 'quantity',
                itemChildOne: [
                    {
                        label: this.props.stateJson['20021005list-000092'],/* 国际化处理： 双向*/
                        value: '0'
                    },
                    {
                        label: this.props.stateJson['20021005list-000093'],/* 国际化处理： 借*/
                        value: '1'
                    },
                    {
                        label: this.props.stateJson['20021005list-000094'],/* 国际化处理： 贷*/
                        value: '2'
                    }
                ],
                itemChildTwo: [
                    {
                        label: this.props.stateJson['20021005list-000095'],/* 国际化处理： 且*/
                        value: 'and'
                    },
                    {
                        label: this.props.stateJson['20021005list-000096'],/* 国际化处理： 或*/
                        value: 'or'
                    }
                ]
            },

            // {itemName:'摘要',itemType:'refer',itemKey:'explanation',
            // config:{refCode:"uapbd/refer/org/AccountBookByFinanceOrgRef"},queryGridUrl:'',refType:'grid'},
            {
                itemName: this.props.stateJson['20021005list-000098'],/* 国际化处理： 凭证状态*/
                itemType: 'select',
                itemKey: 'voucherState',
                itemChild: [
                    { label: this.props.stateJson['20021005list-000099'], value: '0' },/* 国际化处理： 全部凭证*/
                    { label: this.props.stateJson['20021005list-000100'], value: '1' },/* 国际化处理： 已记账*/
                    { label: this.props.stateJson['20021005list-000101'], value: '2' },/* 国际化处理： 待记账*/
                    {
                        label: this.props.stateJson['20021005list-000102'],/* 国际化处理： 未记账*/
                        value: '3'
                    },
                    { label: this.props.stateJson['20021005list-000103'], value: '4' },/* 国际化处理： 已审核*/
                    { label: this.props.stateJson['20021005list-000104'], value: '5' },/* 国际化处理： 待审核*/
                    { label: this.props.stateJson['20021005list-000105'], value: '6' },/* 国际化处理： 未审核*/
                    { label: this.props.stateJson['20021005list-000106'], value: '7' },/* 国际化处理： 已签字*/
                    {
                        label: this.props.stateJson['20021005list-000107'],/* 国际化处理： 待签字*/
                        value: '8'
                    },
                    { label: this.props.stateJson['20021005list-000108'], value: '9' }/* 国际化处理： 未签字*/
                ]
            },


            // {itemName:'会计科目',itemType:'refer',itemKey:'accountingcode',
            // config:{refCode:"uapbd/refer/fiacc/AccountDefaultModelTreeRef"},queryGridUrl:'',refType:'grid'},

            {
                itemName: this.props.stateJson['20021005list-000109'],/* 国际化处理： 差异凭证*/
                itemType: 'select',
                itemKey: 'difflag',
                itemChild: [
                    {
                        label: this.props.stateJson['20021005list-000099'],/* 国际化处理： 全部凭证*/
                        value: '0'
                    },
                    {
                        label: this.props.stateJson['20021005list-000109'],/* 国际化处理： 差异凭证*/
                        value: '1'
                    },
                    {
                        label: this.props.stateJson['20021005list-000110'],/* 国际化处理： 非差异凭证*/
                        value: '2'
                    }
                ]
            },
            {
                itemName: this.props.stateJson['20021005list-000111'],/* 国际化处理： 制单人*/
                itemType: 'refer',
                itemKey: 'pk_prepared',
                isMultiSelectedEnabled: false,
                //\uap\demo\referDemo\main\index.js
                //uap\refer\riart\userRefer
                //gl.refer.voucher.OperatorDefaultRef
                config: { refCode: 'gl/refer/voucher/OperatorDefaultRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            {
                itemName: this.props.stateJson['20021005list-000112'],/* 国际化处理： 出纳人*/
                itemType: 'refer',
                itemKey: 'pk_casher',
                isMultiSelectedEnabled: false,
                config: { refCode: 'gl/refer/voucher/OperatorDefaultRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            {
                itemName: this.props.stateJson['20021005list-000113'],/* 国际化处理： 审核人*/
                itemType: 'refer',
                itemKey: 'pk_checked',
                isMultiSelectedEnabled: false,
                config: { refCode: 'gl/refer/voucher/OperatorDefaultRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            {
                itemName: this.props.stateJson['20021005list-000114'],/* 国际化处理： 记账人*/
                itemType: 'refer',
                itemKey: 'pk_manager',
                isMultiSelectedEnabled: false,
                config: { refCode: 'gl/refer/voucher/OperatorDefaultRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            {
                itemName: this.props.stateJson['20021005list-000115'],/* 国际化处理： 折算来源*/
                itemType: 'select',
                itemKey: 'convertSource',
                itemChild: [
                    {
                        label: this.props.stateJson['20021005list-000099'],/* 国际化处理： 全部凭证*/
                        value: '0'
                    },
                    {
                        label: this.props.stateJson['20021005list-000116'],/* 国际化处理： 折算生成*/
                        value: '1'
                    },
                    {
                        label: this.props.stateJson['20021005list-000117'],/* 国际化处理： 非折算生成*/
                        value: '2'
                    }
                ]
            },
            {
                itemName: this.props.stateJson['20021005list-000118'],/* 国际化处理： 折算状态*/
                itemType: 'select',
                itemKey: 'convertState',
                itemChild: [
                    {
                        label: this.props.stateJson['20021005list-000099'],/* 国际化处理： 全部凭证*/
                        value: '0'
                    },
                    {
                        label: this.props.stateJson['20021005list-000119'],/* 国际化处理： 已折算*/
                        value: '1'
                    },
                    {
                        label: this.props.stateJson['20021005list-000120'],/* 国际化处理： 未折算*/
                        value: '2'
                    }
                ]
            },
            {
                itemName: this.props.stateJson['20021005list-000152'],/* 国际化处理： 冲销类凭证*/
                itemType: 'select',
                itemKey: 'offervoucher',
                itemChild: [
                    {
                        label: this.props.stateJson['20021005list-000156'],/* 国际化处理： 包含冲销凭证*/
                        value: '0'
                    },
                    {
                        label: this.props.stateJson['20021005list-000157'],/* 国际化处理： 仅显示冲销凭证*/
                        value: '1'
                    }
                ]
            },
            {
                itemName: this.props.stateJson['20021005list-000153'],/* 国际化处理： 结算方式*/
                itemType: 'refer',
                itemKey: 'checkstyle',
                isMultiSelectedEnabled: true,
                config: { refCode: 'uapbd/refer/sminfo/BalanceTypeGridRef' },
                queryGridUrl: '',
                refType: 'grid'
            },
            {
                itemName: this.props.stateJson['20021005list-000154'],/* 国际化处理： 票据号*/
                itemType: 'mix',
                itemKey: 'checkno',
                itemChildOne: [
                    {
                        label: this.props.stateJson['20021005list-000092'],/* 国际化处理： 双向*/
                        value: '0'
                    },
                    {
                        label: this.props.stateJson['20021005list-000093'],/* 国际化处理： 借*/
                        value: '1'
                    },
                    {
                        label: this.props.stateJson['20021005list-000094'],/* 国际化处理： 贷*/
                        value: '2'
                    }
                ],
                itemChildTwo: [
                    {
                        label: this.props.stateJson['20021005list-000095'],/* 国际化处理： 且*/
                        value: 'and'
                    },
                    {
                        label: this.props.stateJson['20021005list-000096'],/* 国际化处理： 或*/
                        value: 'or'
                    }
                ]
            },
            {
                itemName: this.props.stateJson['20021005list-000155'],/* 国际化处理： 票据日期*/
                itemType: 'mix',
                itemKey: 'checkdate',
                itemChildOne: [
                    {
                        label: this.props.stateJson['20021005list-000092'],/* 国际化处理： 双向*/
                        value: '0'
                    },
                    {
                        label: this.props.stateJson['20021005list-000093'],/* 国际化处理： 借*/
                        value: '1'
                    },
                    {
                        label: this.props.stateJson['20021005list-000094'],/* 国际化处理： 贷*/
                        value: '2'
                    }
                ],
                itemChildTwo: [
                    {
                        label: this.props.stateJson['20021005list-000095'],/* 国际化处理： 且*/
                        value: 'and'
                    },
                    {
                        label: this.props.stateJson['20021005list-000096'],/* 国际化处理： 或*/
                        value: 'or'
                    }
                ]
            },

        ];
        return (
            <div className='voucherHeightSearch'>
                {/*  */}
                <SearchModal
                    clickPlanEve={this.state.clickPlanEve}
                    queryValue={(JSON.stringify(queryList) != '{}'&&queryList)? queryList : checkQuery}
                    hideOrShow={SearchModalShow}
                    voucherState={voucherState}
                    loadData={loadQuery}
                    json={this.props.stateJson}
                    onConfirm={(e) => {
                        this.setState({ queryList: e });
                    }}
                    onRef={this.onRef}
                    dataRows={(e) => this.setState({ dataRows: e })}
                    ifAdd={(e) => {
                        this.setState({ addContent: e });
                    }}
                    // showorhidebtn={this.props.showorhidebtn}
                    searchId={this.props.searchId}
                    display={this.props.display}
                    desbilltype={this.props.desbilltype}
					srcbilltype={this.props.srcbilltype}
                />
                {/* <NCButton fieldid="saveSearchPlan_btn" colors="primary" onClick={this.saveSearchPlanFun.bind(this)} style={{display:this.props.display,left:-10}} className="cleardata clear">保存查询方案</NCButton> */}
                {this.searchId=="pullgetsearch"?
                <p className="pullgetsearchTitle nc-theme-form-label-c">
                    <label>{this.props.stateJson['20021005list-000998']}</label><span>{this.props.srcbilltype?this.props.srcbilltype.display:''}</span>
                    <label>{this.props.stateJson['20021005list-000999']}</label><span>{this.props.desbilltype?this.props.desbilltype.display:''}</span>
                    <NCButton fieldid="clearData" onClick={this.clear.bind(this)} className="cleardata clear">{this.props.stateJson['20021005list-000997']}</NCButton>
                </p>
                :<div/>}
                {/* <NCButton shape="border" colors="primary" style={{display:this.props.display}} onClick={searchBtnClick.bind(this,this.state,true,this.props.querydata,this.props.props)} className="searchbtn" >确定</NCButton> */}
            </div>

        );
    };
    
    saveSearchPlan = () => {
        let listdata = this.child.confirm();
        return [listdata];
    };
    clickPlanEve = (data) => {
        let querydata=data.conditionobj4web?data.conditionobj4web.nonpublic[0]:{'presetScheme':true,pk_accountingbook:this.state.pk_accountingbook};//presetScheme 点击预置方案
        this.queryList = querydata;
        this.setState({
            clickPlanEve:true,
            queryList: querydata
        });
    };
    componentWillReceiveProps(nextProp) {
        let { queryList, pk_accountingbook, SearchModalShow } = nextProp
        this.setState({
            queryList, pk_accountingbook, SearchModalShow
        })

    }
    onRef = (ref) => {
        this.child = ref;
    };
    componentDidMount() {
        this.props.onRef(this)
    }
    componentDidUpdate(){
        // this.getResultData()
    }
    getsearchData = () => {
        let checkQuery = {};
        if(this.queryList){//查询方案
			checkQuery=this.queryList; 
		}else{
			//高级查询确定按钮，获取查询条件
			checkQuery=this.child.confirm(); 
		}
        // if (getDefData('heightQuery', dataSourceHeightQuery) && getDefData('heightQuery', dataSourceHeightQuery).length != '0') {
        //     checkQuery = getDefData('heightQuery', dataSourceHeightQuery);
        //     // this.props.getSearchData(checkQuery)
        //     return checkQuery
        // }
        checkQuery=transferFormat(JSON.parse(JSON.stringify(checkQuery)));
        return checkQuery;
    }
    getResultData = () => {
        searchBtnClick(this,this.state,true,this.props.querydata,this.props.props);
        setTimeout(() => {
            let resultQuery = {};
            if (getDefData('result', 'resultData') && getDefData('result', 'resultData').length != '0') {
                resultQuery = getDefData('result', 'resultData');
                //this.props.getResultData(resultQuery)
                return resultQuery;
            }
        }, 0);
    }
    saveSearchPlanFun=()=>{
        // let listdata = this.child.confirm();
        // return [listdata];
        this.props.props.search.openSavePlanWindow('pullgetsearch')
    }
    clear=()=>{
        this.child.clear();
    }
    openHightQuery=()=>{
        this.setState({
            SearchModalShow:true
        },()=>{
            this.props.props.search.openAdvSearch(this.searchId, true)
        })
        
    }
    render() {
        let { NCCreateSearch } = this.props;
        return (<div className="nc-bill-search-area">
            {NCCreateSearch(this.searchId, {
                hideBtnArea:this.searchId=="pullgetsearch"?true:false,
                onlyShowSuperBtn:this.searchId=="pullgetsearch"?true:false,
                clickSearchBtn: searchBtnClick.bind(this, this.state, false),
                onAfterEvent: onAfterEvent.bind(this.props.parentThis),
                showAdvBtn: true,
                searchBtnName:this.searchId=="pullgetsearch"?this.props.stateJson['20021005list-000047']:this.props.stateJson['20021005list-000143'],//确定 查询
                hideSearchCondition: true, //隐藏候选条件
                isSynInitAdvSearch:true,//渲染右边面板自定义区域
                oid: this.props.oid, //查询模板oid
                replaceRightBody: this.addAdvBody,
                saveSearchPlan: this.saveSearchPlan, //保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
                clickPlanEve: this.clickPlanEve, //点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                // onlyShowAdvArea: this.props.onlyShowAdvArea,
            })}
        </div>)

    }
}