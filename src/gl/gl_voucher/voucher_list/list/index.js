//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, high, cardCache,cacheTools,getMultiLang,createPageIcon,useJS  } from 'nc-lightapp-front';
import SearchModal from './searchModal/searchModal';
import ExportHeightQueryModal from '../exportHeightQueryModal';
import createScript from '../../../public/components/uapRefer.js';
let { NCRow: Row, NCCol: Col, NCCheckbox: Checkbox, NCDatePicker: DatePicker,NCHotKeys:HotKeys,NCDiv} = base;
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	tableModelConfirm,
	scan,
	controlButton,
	dealOperate,
	operate,
	onAfterEvent
} from './events';
import './index.less';
import Exgianstep from '../../../public/components/ExgainslossesStep';
//import CashflowTreeRef from '../../../../uapbd/refer/fiacc/CashflowTreeRef';
//import { ExcelImport } from '../../../../uap/public/excelImport';
//import EmptyQuery from './emptyQuery';
import { quicktallyItem, quicktallyComfirm } from './quickTally';
import { exportXMLItem, exportXMLComfirm } from './exportXML';
import { createHint } from './batchHint';
import { createPrint, printButton } from './print';
import { createSpaceQuery,clearContent } from './spacequery';
import Iframe from '../../../public/components/Iframe';
import {
	dataSource,
	dataSourceCheck,
	dataSourceTable,
	dataSourceDetail,
	dataSourceNumber,
	dataSourceAppCode,
	dataSourcePageCode,
	mutilangJson_list,
	dataSourceSaveNext,
	dataSourcetableclick,
	listCacheKey,linkToCacheTool
} from '../../../public/components/constJSON';
let { setDefData, getDefData } = cardCache;
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};

useJS.setConfig({
	'sscrp/rppub/components/image/index': "../../../../sscrp/rppub/components/image/index.js",
	'uap/common/components/printOnClient/index': "../../../../uap/common/components/printOnClient/index.js"
})
//  @withShell
let page=useJS(["sscrp/rppub/components/image/index","uap/common/components/printOnClient/index",],function(image,print){

 return class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			SearchModalShow:true,//高级查询面板显示隐藏
			defaultAccbookPk:'',//凭证类别过滤
			printQuery: [],
			selectVoucher: [], //选中的凭证
			selectIndex: '',
			showPrintModal: false,
			showModal: false,
			dataRows: [], //辅助核算列表数据
			checkedRefer: [],
			checkedArray: [ false, false, false ],
			pk_accountingbook: '',
			queryList: {},
			formLoad: '',
			showQuery: false,
			showstep: false,
			addContent: false, //是否追加查询结果
			exportxml: {
				type: '1',
				filepath: 'C:\\Users\\Administrator\\Desktop\\voucher.xml',
				localflag: true, //导出至本机 路径是否可编辑
				showInput: false
			},
			spaceQueryText: '', //空号查询结果
			spacequery_clearBtn_disabled:true,//清除按钮禁用
			signData:{},
			json:{},
			inlt:null
		};
		this.image=window['sscrp/rppub/components/image/index'];
		this.printOnClient=print;
		this.moduleId = '2002';
		this.searchId = '20021005query';
		this.tableId = 'gl_voucher';
		this.ViewModel = this.props.ViewModel;
		this.pk_acc = {}; //查询带默认账簿到凭证卡片
		this.showUntally={
			num:0,
			time:''
		};
	}

	componentWillMount() {
		let callback=(json,status,inlt)=>{
			initTemplate.call(this, this.props, json, inlt);
			setDefData('mutilangJson_list', mutilangJson_list, json);
			setDefData('mutilangInlt_list', mutilangJson_list, inlt);
			this.setState({ json, inlt })
		}
		getMultiLang({moduleId:'20021005list',domainName:'gl',callback});
	}

	onRef = (ref) => {
		this.child = ref;
	};

	componentDidMount() {
		if (this.props.getUrlParam('type') == 'check' || this.props.getUrlParam('type') == 'query') {
			scan(this);//扫码抢扫码
		}
	}
	caiHover=(index,type,e)=>{
		// let line_cai=document.getElementsByClassName('changeAnimation');
		// let line_caiyu=document.getElementsByClassName('line_caiyu');
		// for(let i=0;i<line_cai.length;i++){
		// 	line_cai[i].style.animation='changeMax 1s';
		// }
		// for(let ii=0;ii<line_caiyu.length;ii++){
		// 	line_caiyu[ii].style={'margin-left':'20px'};
		// }
	}
	caiOut=(index,type,e)=>{
		// let line_cai=document.getElementsByClassName('changeAnimation');
		// let line_caiyu=document.getElementsByClassName('line_caiyu');
		// for(let i=0;i<line_cai.length;i++){
		// 	line_cai[i].style.animation='changeMin 1s';
		// }
		// for(let ii=0;ii<line_caiyu.length;ii++){
		// 	line_caiyu[ii].style={'margin-left':'10px'};
		// }
		
	}
	//列表双击表体
	dbClick = (props, record, index, a) => {
		let self = this;
		let yieldCode = self.props.getUrlParam('pagecode');
		let card = yieldCode ? yieldCode.replace(/list/g, 'card') : '20021005card';
		let pagekey = self.props.getUrlParam('pagekey');
		let ifstep = self.props.getUrlParam('step');
		let ifshow = self.props.getUrlParam('ifshowQuery');
		let srcLink = self.props.getUrlParam('scene');
		let type=self.props.getUrlParam('type');
		let backUrl=ifshow&&ifshow!=''? '/list':'/';
		let backAppCode = self.props.getUrlParam('backAppCode') || getDefData('appcode', dataSourceAppCode);
		let listback=backAppCode||self.props.getUrlParam('listback');
		if(cacheTools.get(linkToCacheTool)!=null)//外系统多条联查时opento返回列表
			backUrl='/';
		if (record.pk_voucher.value) {
			//缓存跳转标识,返回添加背景色
			setDefData('tableIndex', dataSourcetableclick, index);
			let param = {
				appcode: self.props.getUrlParam('appcode') || self.props.getSearchParam('c'),
				pagecode: card,
				status: 'browse',
				n:self.props.getUrlParam('n'),
				step: ifstep ? ifstep : false,
				type:type?type:'',
				ifshowQuery: ifshow ? ifshow : false,
				id: record.pk_voucher.value,
				backUrl:backUrl
			};
			if (pagekey && pagekey != '') {
				param.pagekey = pagekey;
			}
			if(srcLink){
				param.scene=srcLink;
			}
			props.pushTo('/Welcome', param);
		} else {
			let url = '/nccloud/gl/voucher/listtovoucher.do';
			if(!record.hasOwnProperty('detailMDVOs')){
				let linkData = getDefData('gl_voucher', dataSource);
				record=linkData[this.tableId].rows[index].values;
			}
			ajax({
				url,
				data: record,
				success: function(res) {
					//self.ViewModel.setData('gl_voucher', res.data);
					setDefData('number', dataSourceNumber, index);
					setDefData('voucher_detail', dataSourceDetail, res.data);
					self.ViewModel.setData('gl_voucher_context', self.props.getSearchParam('c'));
					let yieldCode = self.props.getUrlParam('pagecode');
					let card = yieldCode ? yieldCode.replace(/list/g, 'card') : '20021005card';
					let status = self.props.getUrlParam('status');
					let pagekey= self.props.getUrlParam('pagekey');//会计平台预览pagekey=‘pre’
					if(card&&card.indexOf('link')!=-1){
						status='browse'
					}
					let urlParam={
						appcode: self.props.getUrlParam('appcode'),
						pagekey: (yieldCode&&yieldCode.indexOf('_')!=-1&&pagekey!='pre')?'':pagekey||'generate',
						pagecode: card,
						n:self.props.getUrlParam('n'),
						step: ifstep ? ifstep : false,
						ifshowQuery: ifshow ? ifshow : false,
						status: status||'edit',
						backUrl: srcLink&&srcLink.indexOf('Preview')!=-1 ?'/':'/list',
						listback:listback
					}
					if(srcLink){
						urlParam.scene=srcLink;
					}
					props.pushTo('/Welcome', urlParam);
				}
			});
		}
	};

	// /* 编辑后事件 */
	// onAfterEvent(field, val) {
	// 	if (field == 'pk_accountingbook') {
	// 		let defaultAccbookvalue='';
	// 		if(val.length>0){
	// 			defaultAccbookvalue=val[0].refpk;
	// 		}
	// 		this.setState({
	// 			defaultAccbookPk:defaultAccbookvalue
	// 		})
	// 		// //liuh 注释
	// 		// let meta = this.props.meta.getMeta();
	// 		// let searchId = '20021005query';
	// 		// let items = meta[searchId].items;
	// 		// for (let i = 0; i < items.length; i++) {
	// 		// 	if (items[i].attrcode == 'pk_vouchertype') {
	// 		// 		items[i].queryCondition = () => {
	// 		// 			return {
	// 		// 				GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
	// 		// 				pk_org: val.refpk,
	// 		// 				isDataPowerEnable: 'Y',
	// 		// 				DataPowerOperationCode: 'fi'
	// 		// 			};
	// 		// 		};
	// 		// 	}
	// 		// }
	// 	}
	// }

	// addAdvBody = () => {
	// 	let {dataRows, queryList, addContent,pk_accountingbook,SearchModalShow } = this.state;
	// 	const emptyFunc = () => <span>{this.state.json['20021005list-000054']}</span>;/* 国际化处理： 这里没有数据*/
	// 	let checkQuery = {};
	// 	// if (getDefData('heightQuery', dataSourceHeight) && getDefData('heightQuery', dataSourceHeight).length != '0') {
	// 	if (getDefData('heightQuery', dataSourceHeightQuery) && getDefData('heightQuery', dataSourceHeightQuery).length != '0') {
	// 		checkQuery = getDefData('heightQuery', dataSourceHeightQuery);//dataSourceHeight
	// 		// this.setState({
	// 		//     queryList:checkQuery
	// 		// })
	// 	}else{
	// 		checkQuery.pk_accountingbook=pk_accountingbook
	// 	}
	// 	let voucherState=this.props.getUrlParam('type');
	// 	return (
	// 		<SearchModal
	// 			queryValue={JSON.stringify(queryList) != '{}' ? queryList : checkQuery}
	// 			hideOrShow={SearchModalShow}
	// 			voucherState={voucherState}
	// 			loadData={loadQuery(this)}
	// 			json={this.state.json}
	// 			onConfirm={(e) => {
	// 				this.setState({ queryList: e });
	// 			}}
	// 			onRef={this.onRef}
	// 			dataRows={(e) => this.setState({ dataRows: e })}
	// 			ifAdd={(e) => {
	// 				this.setState({ addContent: e });
	// 			}}
	// 		/>
	// 	);
	// };

	// saveSearchPlan = () => {
	// 	let self = this;
	// 	let listdata=this.child.confirm(); 
	// 	return [ listdata ];
	// 	// return [ self.state.queryList ];
	// };

	replaceAdvBtnEve = () => {
		let { showModal } = this.state;
		this.setState({
			showModal: true
		});
	};

	replaceAdvBody = () => {
		return <div>111</div>;
	};

	// //点击高级面板中的查询方案事件 ,返回查询方案信息，用于业务组为自定义查询区赋值
	// clickPlanEve = (data) => {
	// 	this.queryList=data.conditionobj4web.nonpublic[0];
	// 	this.setState({
	// 		queryList: data.conditionobj4web.nonpublic[0]
	// 	});
	// };

	appSureBtnClick = () => {
		let { formLoad } = this.state;
		formLoad = '';
		this.setState({
			formLoad
		});
	};

	backClick = () => {
		//联查列表到卡片后需缓存appcode pagecode，返回初始页面;

		let backUrl = this.props.getUrlParam('backUrl');
		let backAppCode = this.props.getUrlParam('backAppCode') || getDefData('appcode', dataSourceAppCode);
		let backPageCode = this.props.getUrlParam('backPageCode') || getDefData('pagecode', dataSourcePageCode);
		//	window.history.go(-1);
		//列表返回清空保存成功缓存数据
		let linkData = getDefData('gl_voucher', dataSource);
		// let crossData = getDefData('checkedData', dataSource);
		let crossData = getDefData(listCacheKey, dataSource);
		let isMuti = this.props.getUrlParam('isMuti');
		if(linkData){
			setDefData('gl_voucher', dataSource,'')
		}
		if(crossData&&crossData.length!='0'){
			//setDefData('checkedData', dataSourceCheck,[])
			setDefData(listCacheKey, dataSourceCheck,[])
		}
		let appcode = this.props.getUrlParam('appcode') || this.props.getSearchParam('appcode')|| this.props.getSearchParam('c');
		this.props.pushTo(backUrl || '/', {
			appcode: backAppCode,
			pagecode: backPageCode,
			fromapp: appcode,//返回会计平台时使用参数
			isMuti:isMuti?isMuti:''//会计平台用
		});
	};

	onSelectedFn = (props, moduleId, record, index, status) => {
		controlButton(props, 'gl_voucher');
		this.setState({
			selectIndex: index
		});
	};

	
	onTitleClick = (event) => {
		//if(!!window.ActiveXObject || "ActiveXObject" in window){//IE11
			if(this.showUntally.num>=6)
				return;
			if(this.showUntally.time){
				let currTime=new Date().getTime();
				let time=currTime-this.showUntally.time;
				if(time<500){
					this.showUntally.num=this.showUntally.num+1;
					this.showUntally.time=currTime;
				}else{
					this.showUntally.num=1;
					this.showUntally.time=currTime;
				}
			}else{
				//第一次
				this.showUntally.num=this.showUntally.num+1;
				this.showUntally.time=new Date().getTime();
			}
			if(this.showUntally.num>=6){
				this.showProBtn();
			}
		// }else{
		// 	if (event.detail >= 6) {
		// 		this.showProBtn();
		// 	}
		// }
	};

	showProBtn = () => {
		this.props.button.setButtonVisible('untally', true);
	};
	//空号查询框关闭
	closeBtnClick=()=>{
		this.props.modal.close('spacequery');
	}
	render() {
		let showstep = this.props.getUrlParam('step');
		let showQuery = this.props.getUrlParam('ifshowQuery');
		let scene = this.props.getUrlParam('scene');
		let { table, search, modal, form } = this.props;
		let { createSimpleTable } = table;
		const { createModal } = modal;
		const {ExcelImport}=high;
		let { NCCreateSearch } = search;
		let { createForm } = form;
		let pagetype = this.props.getUrlParam('type');
		// let buttonType = this.props.getUrlParam('pagekey'); //生成凭证控制按钮显影
		let yieldCode = this.props.getUrlParam('pagecode'); //会计平台联查
		let backAppCode = this.props.getUrlParam('backAppCode') || getDefData('appcode', dataSourceAppCode);
		let appname =this.props.getUrlParam('n')|| this.props.getSearchParam('n') || this.state.json['20021005list-000042'];/* 国际化处理： 凭证维护*/
		let listback=backAppCode||this.props.getUrlParam('listback');
		let appcode=this.props.getUrlParam('c') || this.props.getSearchParam('c');
		const { createBillHeadInfo } = this.props.BillHeadInfo
		return (
			<HotKeys
				// keyMap={{
				// 	'add': 'ctrl+/'
				// }}
				// handlers={{'add': (e)=>{
				// 	e.preventDefault()
				// 	buttonClick.call(this,this.props,'add')},
				// //	'saveadd':onButtonClick.bind(this, status, this.props, this.state,this.props.buttonload,'saveadd')
				// }}
				focused = {true}
				attach = {document.body}
				className='header-button-area'
			>
				<div className="nc-bill-list" id="list_all">
					<NCDiv areaCode={NCDiv.config.HEADER}>
						<div className="nc-bill-header-area">
						{/* {listback ? (
							<i className="iconfont icon-fanhuishangyiji back-btn" onClick={this.backClick.bind(this)} />
						) : null}

						<div className="header-title-search-area" fieldid="header_area">
							{createPageIcon()}
							<h2 fieldid={`${appname}`} className="title-search-detail" onClick={this.onTitleClick}>
								{appname}
							</h2>
						</div> */}
							<div className="header-title-search-area">
								{createBillHeadInfo(
									{
										title: appname,//标题
										titleClick: this.onTitleClick,
										initShowBackBtn: listback ? true : false,
										backBtnClick: this.backClick.bind(this)
									}
								)}
							</div>
							{pagetype && (pagetype == 'check' || pagetype == 'query') ? (
								<div className="header-special-form">
								{createForm('scan')}
							</div>
							) : null}
							<div className="header-button-area">
								{this.props.button.createButtonApp({
									// area: buttonType ? buttonType : 'gl_voucher',
									area:'gl_voucher',
									buttonLimit: 3,
									ignoreHotkeyCode: ['delete', 'abandon'],
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
								{/* 导入导出 */}
								{createModal('importModal', {
									noFooter: true,
									className: 'import-modal'
								})}
								<ExcelImport
									{...Object.assign(this.props)}
									moduleName="gl" //模块名
									billType="C0" //单据类型
									selectedPKS={this.state.selectVoucher}
									appcode={appcode}
									pagecode={yieldCode || '20021005list'}
									referVO={{ ignoreTemplate: true }}
								/>
							</div>
							{createModal('printService', {
								className: 'print-service'
							})}
							<Iframe />
							{/* <iframe id="printServiceframe" name="printServiceframe" style={{ display: 'none' }}></iframe> */}
						</div>
					</NCDiv>
					{showstep ? <Exgianstep step={2} /> : null}
					{!showQuery&&!scene ? (
						<ExportHeightQueryModal
							onRef={this.onRef}
							oid={this.props.meta.oid}
							queryList={this.state.queryList}
							pk_accountingbook={this.state.pk_accountingbook}
							SearchModalShow={this.state.SearchModalShow}
							
							NCCreateSearch={NCCreateSearch}
							getUrlParam={this.props.getUrlParam}
							stateJson={this.state.json}
							table={{setAllTableData:this.props.table.setAllTableData,getAllTableData:this.props.table.getAllTableData,getCheckedRows:this.props.table.getCheckedRows}}
							button={{setButtonDisabled:this.props.button.setButtonDisabled}}
							that={this.props}
							parentThis={this}
							onlyShowAdvArea={false}
							searchId={"20021005query"}
							display={"none"}
						/>
					) : null} 
					<div className="nc-bill-table-area">
						{createSimpleTable(this.tableId, {
							dataSource: dataSourceTable,
							pkname:'pk_voucher',
							// handlePageInfoChange: pageInfoClick,
							tableModelConfirm: tableModelConfirm,
							onRowDoubleClick: this.dbClick.bind(this, this.props),
							onSelected: this.onSelectedFn.bind(this),
							onSelectedAll: controlButton.bind(this, this.props, 'gl_voucher'),
							showCheck: true,
							showIndex: true,
							setCellClass: this.setCellClass,
							componentInitFinished:()=>{
								//缓存数据赋值成功的钩子函数
								//若初始化数据后需要对数据做修改，可以在这里处理
								let linkData = getDefData('gl_voucher', dataSource);
								// let saveNext = getDefData('saveNext', dataSourceSaveNext);
								// let cardBack = this.props.getUrlParam('cardBack'); //卡片返回列表避免单个继续跳转卡片
								let tableIndex=getDefData('tableIndex', dataSourcetableclick)
								controlButton(this.props, 'gl_voucher');
								if (linkData&&linkData[this.tableId]) {//联查判断,避免先生成，联查取缓存数据
									//生成凭证跳转赋值
									if (linkData[this.tableId].rows && linkData[this.tableId].rows.length >= 1) {
										//保存下一张批量删除数据
										this.props.table.setAllTableData(this.tableId, linkData[this.tableId]);
										if(typeof tableIndex !='undefined'){
											this.props.table.focusRowByIndex(this.tableId, tableIndex)
										}
									}else{
										//卡片返回更新列表数据
										this.props.table.setAllTableData(this.tableId, linkData[this.tableId]);
									}

								}
							}
						
						})}
					</div>

					{this.state.json['20021005list-000046']?(
						createModal('quickTally', {
						title: this.state.json['20021005list-000046'], // 弹框表头信息/* 国际化处理： 快速记账*/
						content: quicktallyItem(this), //弹框内容，可以是字符串或dom
						beSureBtnClick: quicktallyComfirm.bind(this), //点击确定按钮事件
						userControl: false, // 点确定按钮后，是否自动取消弹出框.true:手动关。false:自动关
						className: 'junior modal-createform', //  模态框大小 sm/lg/xlg
						rightBtnName: this.state.json['20021005list-000044'], //左侧按钮名称,默认取消/* 国际化处理： 取消*/
						leftBtnName: this.state.json['20021005list-000047'] //右侧按钮名称， 默认确认/* 国际化处理： 确定*/
					})):null}

					{this.state.json['20021005list-000048']?(
						createModal('exportXML', {
						title: this.state.json['20021005list-000048'], // 弹框表头信息/* 国际化处理： 导出XML*/
						content: exportXMLItem.call(this), //弹框内容，可以是字符串或dom
						beSureBtnClick: exportXMLComfirm.bind(this), //点击确定按钮事件
						userControl: false, // 点确定按钮后，是否自动取消弹出框.true:手动关。false:自动关
						className: 'senior', //  模态框大小 sm/lg/xlg
						rightBtnName: this.state.json['20021005list-000044'], //左侧按钮名称,默认取消/* 国际化处理： 取消*/
						leftBtnName: this.state.json['20021005list-000047'] //右侧按钮名称， 默认确认/* 国际化处理： 确定*/
					})):null}

					{this.state.json['20021005list-000049']?(
						createModal('batchhint', {
						size:'xlg',
						title: this.state.json['20021005list-000049'], // 弹框表头信息/* 国际化处理： 提示*/
						content: createHint(this), //弹框内容，可以是字符串或dom
						rightBtnName: this.state.json['20021005list-000044'] //左侧按钮名称,默认取消/* 国际化处理： 取消*/
					})):null}


					{this.state.json['20021005list-000050']?(
						createModal('print', {
						title: this.state.json['20021005list-000050'], // 弹框表头信息/* 国际化处理： 打印*/
						content: createPrint.call(this,this), //弹框内容，可以是字符串或dom
						//beSureBtnClick: confirmPrint.bind(this, this.props, -1), //点击确定按钮事件
						userControl: true,
						size:'sm',
						//cancelBtnClick: closePrint.bind(this),
						className: 'modal-h-auto modal-createform',
						showCustomBtns:true,
						customBtns:printButton(this,this.props, -1)
					})):null}

					{this.state.json['20021005list-000043']?(
						createModal('spacequery', {
						size:'lg',
						title: this.state.json['20021005list-000043'], // 弹框表头信息/* 国际化处理： 空号查询*/
						content: createSpaceQuery(this), //弹框内容，可以是字符串或dom
						noFooter : true,//底部是否有按钮
						className:"spacequery"
					})):null}

					{this.state.json['20021005list-000051']?(
						createModal('signModal', {
						title: this.state.json['20021005list-000051'],/* 国际化处理： 签字*/
						content: (
							<div>
								<Row>
									<Col sm={4} md={4} xs={4}>
										<span className="item-title">{this.state.json['20021005list-000055']/* 国际化处理： 签字日期*/}</span>
									</Col>
									<Col sm={8} md={8} xs={8}>
										<DatePicker 
											value={this.state.signData.date}
											onChange={(v)=>{
												
												let {signData}=this.state;
												signData.date=v
												this.setState({
													signData
												})
											}}
										/>
									</Col>
								</Row>
							</div>
						),
						size: 'sm',
						beSureBtnClick: dealOperate.bind(this,this,this.props,'/nccloud/gl/voucher/sign.do','sign')
					})):null}
					{this.state.json['20021005list-000051']?(
						createModal('signRowModal', {
						title: this.state.json['20021005list-000051'],/* 国际化处理： 签字*/
						content: (
							<div>
								<Row>
									<Col sm={4} md={4} xs={4}>
										<span className="item-title">{this.state.json['20021005list-000055']/* 国际化处理： 签字日期*/}</span>
									</Col>
									<Col sm={8} md={8} xs={8}>
										<DatePicker 
											value={this.state.signData.date}
											onChange={(v)=>{
												
												let {signData}=this.state;
												signData.date=v
												this.setState({
													signData
												})
											}}
										/>
									</Col>
								</Row>
							</div>
						),
						size: 'sm',
						beSureBtnClick: operate.bind(this,this,this.props,'sign',this.state.signData.record,this.state.signData.index,true)
					})):null}
				</div>
			</HotKeys>		
			);
	}

	setCellClass = (index, record, key) => {
		if (record.tempsaveflag.value) {
			//暂存
			return 'voucher_temp';
		} else if (record.discardflag.value) {
			//作废
			return 'voucher_abandon';
		}  else if (record.isOffer.value) {
			//冲销
			return 'voucher_offer';
		} else if (record.pk_manager.value && record.pk_manager.value != '') {
			//记账
			return 'voucher_tally';
		}else if (record.errmessage.value && record.errmessage.value != '') {
			//错误
			return 'voucher_error';
		}
	};
}
})
page.defaultProps = defaultProps12;

page = createPage({
	mutiLangCode: '2002'
})(page);

export default page;
