/**
 * 列表批量提示框
 */
let areacode='batchhint';

function hintItem(){
    return{
        code:areacode,
        moduletype:'table',
        name:this.state.json['20021005list-000000'],/* 国际化处理： 批量提示*/
        pagination:false,
        status:'browse',
        items:[
            {
                "position": "1",
                "visible": true,
                "metadataProperty": "uap.accountingbook.name",
                "maxlength": "20",
                "refcode": "uapbd/refer/org/AccountBookTreeRef/index",
                "itemtype": "refer",
                "isDataPowerEnable": true,
                "label": this.state.json['20021005list-000001'],/* 国际化处理： 财务核算账簿*/
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "pk_accountingbook"
            },
            {
                "position": "2",
                "visible": true,
                "maxlength": "20",
                "itemtype": "datepicker",
                "isDataPowerEnable": true,
                "label": this.state.json['20021005list-000002'],/* 国际化处理： 制单日期*/
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "prepareddate"
            },
            {//凭证类别
                "position": "1",
                "visible": true,
                "metadataProperty": "uap.vouchertype.name",
                "maxlength": "20",
                "refcode": "uapbd/refer/fiacc/VoucherTypeDefaultGridRef/index",
                "itemtype": "refer",
                "isDataPowerEnable": true,
                "label": this.state.json['20021005list-000003'],/* 国际化处理： 凭证类别*/
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "pk_vouchertype"
            },
            {
                "position": "3",
                "visible": true,
                "maxlength": "20",
                "itemtype": "number",
                "isDataPowerEnable": true,
                "label": this.state.json['20021005list-000004'],/* 国际化处理： 凭证号*/
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "num"
            },
            {
                "position": "4",
                "visible": true,
                "maxlength": "20",
                "itemtype": "input",
                "isDataPowerEnable": true,
                "label": this.state.json['20021005list-000005'],/* 国际化处理： 摘要*/
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "explanation"
            },
            {
                "position": "4",
                "visible": true,
                "maxlength": "20",
                "itemtype": "input",
                "isDataPowerEnable": true,
                "label": this.state.json['20021005list-000006'],/* 国际化处理： 结果*/
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "result"
            },
            {
                "position": "4",
                "visible": true,
                "maxlength": "20",
                "itemtype": "input",
                "isDataPowerEnable": true,
                "label": this.state.json['20021005list-000007'],/* 国际化处理： 原因*/
                "color": "#6E6E77",
                "isrevise": false,
                "attrcode": "reason"
            }
        ]
    }    
}

function createHint(_this){
    let { table } = _this.props;
    let { createSimpleTable } = table;
    return(
        <div className="nc-bill-table-area">
            {createSimpleTable(areacode, {
                adaptionHeight:false
            //    height:"500px"
            })}
        </div>
    )
}

export {hintItem,createHint}
