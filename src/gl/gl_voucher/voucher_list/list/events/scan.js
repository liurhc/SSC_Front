/**
 * 扫码
 */
import { toast, ajax, cardCache } from 'nc-lightapp-front';
import {
    dataSourceTable,
    tableId
} from '../../../../public/components/constJSON';
let { setDefData } = cardCache;
import controlButton from './controlButton';

let scanid = "scan";
export default function scan(_this) {
    let code = '';
    window.addEventListener('keypress', function (e) {
        code += String.fromCharCode(e.which);
        if (e.which == 13) {
            let isserial = _this.props.form.getFormItemsValue(scanid, 'isserial');
            if (isserial && isserial.display) {
                let value = code.trim();//去掉空格
                if(!value||value.length<20)
                    return;
                // _this.props.form.setFormItemsValue(scanid,{scan:{display:value,value:value}});
                ajax({
                    url: "/nccloud/gl/voucher/scan.do",
                    data: {
                        billPk: value
                    },
                    success: function (res) {
                        let { success, data } = res;
                        if (success) {
                            if (data) {
                                if (isserial.value) {
                                    //如果是连续扫码，则追加在现有单据后面
                                    if (data['gl_voucher'] && data['gl_voucher'].rows.length > 0) {
                                        let alldatas = _this.props.table.getAllTableData('gl_voucher');
                                        if (alldatas && alldatas.rows.length > 0) {
                                            //页面上已有数据
                                            let allpks = alldatas.allpks;
                                            data['gl_voucher'].rows.map((row) => {
                                                if (allpks.join(',').indexOf(row.values.pk_voucher.value) == -1) {
                                                    allpks.push(row.values.pk_voucher.value);
                                                    alldatas.rows.push(row);
                                                }
                                            })
                                            _this.props.table.setAllTableData('gl_voucher', alldatas);
                                            setDefData(tableId, dataSourceTable, alldatas);
                                        } else {
                                            if (data['gl_voucher'] && data['gl_voucher'].rows.length > 0) {
                                                let allpks = [];
                                                data['gl_voucher'].rows.map((row) => {
                                                    allpks.push(row.values.pk_voucher.value);
                                                })
                                                data['gl_voucher'].allpks = allpks;
                                            }
                                            _this.props.table.setAllTableData('gl_voucher', data['gl_voucher']);
                                            setDefData(tableId, dataSourceTable, data[tableId]);
                                        }
                                    }
                                } else {
                                    //不是连续扫码
                                    if (data['gl_voucher'] && data['gl_voucher'].rows.length > 0) {
                                        let allpks = [];
                                        data['gl_voucher'].rows.map((row) => {
                                            allpks.push(row.values.pk_voucher.value);
                                        })
                                        data['gl_voucher'].allpks = allpks;
                                    }
                                    _this.props.table.setAllTableData('gl_voucher', data['gl_voucher']);
                                    setDefData(tableId, dataSourceTable, data[tableId]);
                                    if (data['gl_voucher'] && data['gl_voucher'].rows.length == 1) {
                                        let pk_voucher = data['gl_voucher'].rows[0].values.pk_voucher.value;
                                        let ifstep = _this.props.getUrlParam('step');
                                        let ifshow = _this.props.getUrlParam('ifshowQuery');
                                        let type = _this.props.getUrlParam('type');
                                        let param = {
                                            appcode: _this.props.getUrlParam('appcode') || _this.props.getSearchParam('c'),
                                            pagecode: '20021005card',
                                            status: 'browse',
                                            n: _this.props.getUrlParam('n') || _this.props.getSearchParam('n'),
                                            step: ifstep ? ifstep : false,
                                            type: type ? type : '',
                                            ifshowQuery: ifshow ? ifshow : false,
                                            id: pk_voucher,
                                            backUrl: '/'
                                        };
                                        _this.props.pushTo('/Welcome', param);
                                    }
                                }
                                controlButton(_this.props, tableId);
                            } else {
                                toast({ content: _this.state.json['20021005list-000023'], color: 'warning', position: 'bottom' });/* 国际化处理： 未找到符合条件的凭证！*/
                            }
                        }
                        code = "";
                    }
                })
            }
        }

    })
}

// export default function scan(props, key) {
//     
//     if ("isserial" == key)
//         return;
//     let scanid="scan";
//     let scanvalue = props.form.getFormItemsValue(scanid, 'scan');
//     let isserial = props.form.getFormItemsValue(scanid, 'isserial');
//     if (scanvalue&&scanvalue.value&& isserial) {
//         ajax({
//             url: "/nccloud/gl/voucher/scan.do",
//             data: {
//                 billPk: scanvalue.value
//             },
//             success: function (res) {
//                 let { success, data } = res;
//                 if (success) {
//                     if(data){  
//                         if (isserial.value) {
//                             //如果是连续扫码，则追加在现有单据后面
//                             props.table.addRow('gl_voucher', data['gl_voucher']);
//                         }else                   
//                             props.table.setAllTableData('gl_voucher', data['gl_voucher']);
//                     }else{
//                         toast({ content: "未找到符合条件的凭证！", color: 'warning', position: 'bottom' });
//                     }                    
//                 }
//             },
//             error: (res) => {
//                 toast({ content: "未找到符合条件的凭证！", color: 'warning', position: 'bottom' });
//             }
//         })
//     }
// }
