import { ajax, toast, deepClone, cardCache,createPage,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
import {
	dataSourceTable,
	tableId,
	dataSourceNormal,
	dataSourceNormalQuery,
	dataSourceHeightQuery,
	dataSourceValue,
	query_accountingbook
} from '../../../../public/components/constJSON';
let { setDefData, getDefData } = cardCache;
import controlButton from './controlButton';
import transferFormat from './transferFormat';
//点击查询，获取查询区数据
export default function clickSearchBtn(state,flag,props,searchVal,querydata) {
	let _this=this;
	let assvos = [];
	if (searchVal && searchVal.conditions && searchVal.conditions.length > 0) {
		let searchValData = searchVal.conditions;
		let data = {
			assvos: []
		};
		//简单查询数据存储
		setDefData('normalQuery', dataSourceNormalQuery, searchValData);
		for (let i = 0; i < searchValData.length; i++) {
			if ('prepareddate' == searchValData[i].field) {
				data['prepareddate_start'] = searchValData[i].value.firstvalue;
				data['prepareddate_end'] = searchValData[i].value.secondvalue;
			} else if ('pk_accountingbook' == searchValData[i].field) {
				let pk_accountingbooks = searchValData[i].value.firstvalue;
				let pk_accountingbooksDisplay = searchValData[i].display;
				data['pk_accountingbook'] = pk_accountingbooks.split(',');
				let pk_acc = {
					display: pk_accountingbooksDisplay.split(',')[0],
					value: data['pk_accountingbook'][0]
				};
				setDefData('normalQuery', query_accountingbook, pk_acc);
			} else if ('num' == searchValData[i].field) {
				data['num_start'] = searchValData[i].value.firstvalue;
				data['num_end'] = searchValData[i].value.firstvalue;
			} else {
				data[searchValData[i].field] = searchValData[i].value.firstvalue;
			}
		}
		data.assvos=[];
		data.pageInfo = props.table.getTablePageInfo(tableId);
		
		setDefData('realQuery', dataSourceNormal, data);
		ajax({
			url: '/nccloud/gl/voucher/voucherQuery.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						//  getInsertData(data);//列表数据存储pk_voucher
						let message=_this.props.parentThis.state.inlt&&_this.props.parentThis.state.inlt.get('20021005list-000024', {number: data[tableId].allpks.length});
						toast({ color: 'success', content:message});/* 国际化处理： 查询成功，共{number}条*/
						// if (state.addContent) {//普通查询不要判断这个
						// 	this.props.table.addRow(tableId, data[tableId]);
						// } else {
							_this.props.table.setAllTableData(tableId, data[tableId]);
							// setDefData('loadTable', dataSourceTable, data[tableId]);
							setDefData(tableId, dataSourceTable, data[tableId]);
						// }
					} else {
						toast({ color: 'warning', content: _this.props.stateJson['20021005list-000026'] });/* 国际化处理： 未查询出符合条件的数据*/
						_this.props.table.setAllTableData(tableId, { rows: [],allpks:[] });
					}
					controlButton(this.props,tableId);
				}
			}
		});
		
	} else {
		let listdata={};
		// if(this.queryList){//查询方案
		// 	listdata=this.queryList; 
		// }else{
			//高级查询确定按钮，获取查询条件
			listdata=this.child.confirm(); 
			if(listdata.errorFlag){
				return true;
			}
		// }
		
		let heightQueryListData=deepClone(listdata);
		let dataQuery = deepClone(listdata);
		//高级查询查询存储
		// setDefData('heightQuery', dataSourceHeightQuery, state.queryList);
		setDefData('heightQuery', dataSourceHeightQuery, heightQueryListData);
		dataQuery=transferFormat(dataQuery);
		if(_this.searchId!="pullgetsearch"){
			// dataQuery.pageInfo = props.table.getTablePageInfo(tableId);//去掉分页
			setDefData('realQuery', dataSourceValue, dataQuery);
			let queryData = getDefData('realQuery', dataSourceValue);
			// cacheTools.set("VoucherSearchData",dataQuery);
			ajax({
				url: '/nccloud/gl/voucher/voucherQuery.do',
				data: dataQuery,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data) {
							setDefData('result', 'resultData', data);
							let message=_this.props.parentThis.state.inlt&&_this.props.parentThis.state.inlt.get('20021005list-000024', {number: data[tableId].allpks.length});
							toast({ color: 'success', content: message});/* 国际化处理： 查询成功，共{number}条*/
							if (state.addContent) {//高级查询里勾选了追加结果的复选框
								//获取缓存数据，将现有数据和缓存数据过滤
								let cacheData=getDefData(tableId, dataSourceTable);
								let allpks=cacheData.allpks;
								let cacheDataPKArr=[];
								cacheData.rows.map((item,index)=>{
									cacheDataPKArr.push(item.values.pk_voucher.value);
								})
								data[tableId].rows.map((list,index)=>{
									if(cacheDataPKArr.join(',').indexOf(list.values.pk_voucher.value)==-1){
										cacheData.rows.push(list);
									}
								})
								// let resultData=Array.from(new Set([...cacheData.rows,...data[tableId].rows]));
								// cacheData.rows=resultData;
								_this.props.table.setAllTableData(tableId, cacheData);
								setDefData(tableId, dataSourceTable, cacheData);
								// this.props.table.addRow(tableId, data[tableId]);
							} else {
								_this.props.table.setAllTableData(tableId, data[tableId]);
								// setDefData('loadTable', dataSourceTable, data[tableId]);
								setDefData(tableId, dataSourceTable, data[tableId]);
							}
						} else {
							toast({ color: 'warning', content: _this.props.stateJson['20021005list-000026'] });/* 国际化处理： 未查询出符合条件的数据*/
							_this.props.table.setAllTableData(tableId, { rows: [] });
						}
						controlButton(_this.props,tableId);
					}
				}
			});
			_this.setState({clickPlanEve:false})
		}else{
			_this.props.getsynctreedomData();
		}
		
		
	}
}
export function getInsertData(param) {
	let InsertData = JSON.stringify(param.gl_voucher.rows);
	let searchData = JSON.parse(getGlobalStorage('localStorage','SearchData'));
	if (searchData) {
		removeGlobalStorage('localStorage', 'SearchData')
		setGlobalStorage('localStorage','SearchData', InsertData);
	} else {
		setGlobalStorage('localStorage','SearchData', InsertData);
	}
}

export function isObj(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'Object';
}
