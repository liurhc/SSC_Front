// 外系统联查及生成凭证列表参数定义
// cacheTools.set(‘checkedData’,{id:[..,..]});//会计平台联查传pk数组 打开新页签
// setDefData('gl_voucher',dataSource,res.data.grid);//生成凭证，传整张凭证
// setDefData('checkedData', dataSource, [..,..]);//周期凭证、自定义转账、汇兑损益 联查凭证 单页联查
// {
//     status:'browse',  //浏览态 ('edit'编辑态,'browse'浏览态')
//     scene:'fip',//只有会计平台联查时传了
//     n:'****',//标题区显示名称
//     appcode:'20020PREPA',//应用编码
//     pagecode:'20021005list',//会计平台传递20021005list_link
//     pagekey:'link',//联查 ('link'联查,‘generate’生成,‘pre’会计平台预览凭证) 这个决定按钮的显示隐藏
//     backPageCode:'...',//返回的应用的页面编码
//     backAppCode:'...',//返回的应用编码，需要返回的传，不需要返回的不传
//     ifshowQuery:true,//是否显示查询区，true不显示，false显示，默认显示
//     step:false,//是否显示步骤区 false不显示，true显示，默认不显示
// }
import { ajax, toast, cacheTools, cardCache,excelImportconfig } from 'nc-lightapp-front';
import { quickTallyMeta } from '../quickTally';
import { hintItem } from '../batchHint';
import { printItem } from '../print';
import { setSpaceQueryDefault } from '../spacequery';
import controlButton from './controlButton';
import {
	dataSource,
	dataSourceTable,
	tableId,
	dataSourceNormal,
	dataSourceNumber,
	dataSourceDetail,
	successStatus,
	dataSourceAppCode
} from '../../../../public/components/constJSON';
import {toVoucherList,robotInShort} from './toVoucherList';
let { setDefData, getDefData } = cardCache;

//let linkNumber=getDefData('number',dataSourceNumber)
let searchId = '20021005query';
let tablebuttonId='gl_voucher_inner';
//let pageId = '20021005list';
export default function(props) {

	let sign=robotInShort(props);
	if(!sign)//如果从列表直接跳转到卡片则返回false，不继续向下走
		return;
	let appcode = props.getUrlParam('appcode') || props.getSearchParam('appcode');
	let ifshow = this.props.getUrlParam('ifshowQuery');
	
	let voucherType = props.getUrlParam('pagekey'); //区分联查 生成 和直接凭证列表跳卡片状态
	 let { hasCacheData } = props.table;
	let _this = this;
	let yieldCode = _this.props.getUrlParam('pagecode') || _this.props.getSearchParam('pagecode');

	let excelconfig = excelImportconfig(props, 'gl', 'C0');

	let pageId = yieldCode ? yieldCode : '20021005list';
	props.createUIDom(
		{
			pagecode: pageId, //页面id
			appcode: appcode //小应用id
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					_this.setState({
						defaultAccbookPk: data.context.defaultAccbookPk,
						pk_accountingbook: {
							display: data.context.defaultAccbookName,
							value: data.context.defaultAccbookPk
						}
					});
					meta = modifierMeta(_this, props, meta, data.context);
					props.meta.oid = meta[searchId] && meta[searchId].oid;
					props.meta.setMeta(meta);

					toVoucherList(props);//联查、生成凭证跳转

					let saveStatus = getDefData('saveSuccess', successStatus);
					let linkNumber = getDefData('number', dataSourceNumber);

					if (voucherType && saveStatus) {
						//保存成功返回列表删除成功行数据
						props.table.deleteTableRowsByIndex(tableId, linkNumber);
					}

					if (!ifshow) {
						setDefaultValue(_this, props, data.context);
					}
					//用户查询要覆盖系统预置，后续用promise 处理
					if (
						meta[searchId] &&
						getDefData('normalQuery', dataSourceNormal) &&
						getDefData('normalQuery', dataSourceNormal).length != '0'
					) {
						let checkQuery = getDefData('normalQuery', dataSourceNormal);
						for (let i = 0, len = checkQuery.length; i < len; i++) {
							props.search.setSearchValByField(searchId, checkQuery[i].field, {
								display: checkQuery[i].display,
								value: checkQuery[i].value.firstvalue
							});
						}
					}
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setButtonVisible('untally', false);
					props.button.setUploadConfig('import', excelconfig);
					props.button.setPopContent('delete', _this.state.json['20021005list-000021']);/* 国际化处理： 确认要删除吗？*/
					controlButton(props, tableId);
				}
			}
		}
	);

	//从卡片跳回列表之后，根据之前的查询条件重新查询
	if (!hasCacheData(dataSourceTable)) {
		let voucherdata = cacheTools.get('VoucherSearchData');
		if (voucherdata) {
			ajax({
				url: '/nccloud/gl/voucher/voucherQuery.do',
				data: voucherdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data) {
							props.table.setAllTableData(tableId, data[tableId]);
						} else {
							props.table.setAllTableData(tableId, { rows: [] });
						}
					}
				}
			});
		}
	}
}

function loadList(pks, status, props) {
	ajax({
		url: '/nccloud/gl/voucher/query.do',
		data: {
			pk_voucher: pks,
			voucherStatus: status,
			type: 'list'
		},
		success: (res) => {
			let { success, data } = res;
			if (success){
				if(data){
					setDefData('gl_voucher', dataSource,data);//缓存列表数据，下次返回避免请求，选中行标识避免丢失
					props.table.setAllTableData(tableId, data[tableId]);
				}else{
					props.table.setAllTableData(tableId, { rows: [] });
				}
			}
		}
	});
}

function setDefaultValue(_this, props, context) {
	let appcode = props.getUrlParam('appcode') || props.getSearchParam('c');
	if (context.defaultAccbookPk && context.defaultAccbookPk != '') {
		props.form.setFormItemsValue('quickTally', {
			pk_accountingbook: {
				display: context.defaultAccbookName,
				value: context.defaultAccbookPk
			}
		});
		//设置默认的会计期间
		ajax({
			url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
			data: {
				pk_accountingbook: context.defaultAccbookPk
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let period = data.bizPeriod;
					props.form.setFormItemsValue('quickTally', { period: { display: period, value: period } });
					let quicktally={
						pk_accountingbook: {display: context.defaultAccbookName,value: context.defaultAccbookPk},
						period: { display: period, value: period }
					}
					setDefData('quickTally', dataSource,quicktally);
					setSpaceQueryDefault.call(_this, context, data, appcode);
				}
			}
		});
	} else {
		setSpaceQueryDefault.call(_this, context, undefined, appcode);
	}
}

function modifierMeta(_this, props, meta, context) {
	if (props.getUrlParam('type') == 'check' || props.getUrlParam('type') == 'query') meta['scan'].status = 'edit';

	let appcode = props.getUrlParam('c') || props.getSearchParam('c');
	// let buttonType = props.getUrlParam('pagekey');

	if (meta[searchId]) {
		meta[searchId].items = meta[searchId].items.map((item, key) => {
			item.visible = true;
			item.col = '3';
			if (item.attrcode == 'pk_accountingbook') {
				if (context.defaultAccbookPk && context.defaultAccbookPk != '') {
					item.initialvalue = {
						display: context.defaultAccbookName,
						value: context.defaultAccbookPk
					};
				}
				item.isMultiSelectedEnabled = true;
				item.disabledDataShow = true;
				item.queryCondition = () => {
					return {
						TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
						appcode: appcode
					};
				};
			}
			if (item.attrcode == 'pk_vouchertype') {
				//设置凭证类别过滤
				item.queryCondition = () => {
					return {
						// pk_org: context.defaultAccbookPk,
						pk_org: _this.state.defaultAccbookPk,
						isDataPowerEnable: 'Y',
						DataPowerOperationCode: 'fi',
						GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder'
					};
				};
			}
			return item;
		});
	}

	let material_event = {
		label: _this.state.json['20021005list-000022'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: '150px',
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let buttonAry = getOprButtonArray(props, record, index);
			// if (buttonType && buttonType.indexOf('link') != -1) {
			// 	buttonAry = [];
			// }
			return props.button.createOprationButton(buttonAry, {
				// area: buttonType ? buttonType + '_inner' : 'gl_voucher_inner',
				area:tablebuttonId,
				//buttonLimit: 3,
				onButtonClick: (props, key) => oprButtonClick(_this, props, key, record, index)
			});
		}
	};
	let defaultColumns = {
		label:'',
		itemtype: 'customer',
		attrcode: 'flag',
		id:'line_flag',
		className:'line_flag',
		width: '10px',
			fixed: "left",
			visible:true,
		render: (text, record, index) => {
			if (record.free8 && record.free8.value) {
				if (record.free8.value == '1') {
					return (
						<div id="linecount_div">
							<div className='hot_caiyu' index={index} onMouseOver={_this.caiHover.bind(_this, index, "line_cai")} onMouseOut={_this.caiOut.bind(_this, index, "line_cai")}>
								{/* <div id="color_cai"></div> */}
								<span id={"line_cai" + index} className={"change_span line_cai line_cai" + index}>财</span>
							</div>
						</div>);
				} else if (record.free8.value == '2') {//预
					return (
						<div id="linecount_div">
							<div className='hot_caiyu' index={index} onMouseOver={_this.caiHover.bind(_this, index, "line_yu")} onMouseOut={_this.caiOut.bind(_this, index, "line_yu")}>
								{/* <div id="color_yu"></div> */}
								<span id={"line_yu" + index} className={"change_span line_yu line_yu" + index}>预</span>
							</div>
						</div>);
				} else if (record.free8.value == '3') {//财+预
					return (
						<div id="linecount_div">
							<div className='hot_caiyu' index={index} onMouseOver={_this.caiHover.bind(_this, index, "line_cai")} onMouseOut={_this.caiOut.bind(_this, index, "line_cai")}>
								{/* <div id="color_caiyu"></div> */}
								<span id={"line_cai" + index} className={"changeAnimation change_span line_cai line_cai" + index}>财</span>
								<span id={"line_yu" + index} className={"changeAnimation change_span line_caiyu line_yu line_yu" + index}>预</span>
							</div>
						</div>
					);
				}

			} else {
				return (
					<div id="linecount_div"></div>
				);
			}
		}
	};
	//if (buttonType) {
	if (meta[tableId])
		meta[tableId].pagination = false; //去掉分页器
	//}
	meta[tableId] && meta[tableId].items && meta[tableId].items.push(material_event);
	meta[tableId] && meta[tableId].items && meta[tableId].items.unshift(defaultColumns);
	meta['quickTally'] = quickTallyMeta.call(_this,appcode); //快速记账
	meta['batchhint'] = hintItem.call(_this); //批量提示
	meta['print'] = printItem.call(_this); //打印
	//props.form.setFormItemsValue('print',{'printsubjlevel':{display: '不汇总', value: '0'}});
	//meta['spacequery'] = spaceQueryItems(); //空号查询
	return meta;
}

function getOprButtonArray(props, record, index) {
	//可以放在操作列上的按钮
	let btnArry = [];

	if (record.pk_casher && record.pk_casher.value && record.pk_casher.value != '~') {
		btnArry.push('unsign');
	} else {
		btnArry.push('sign');
	}

	if (record.pk_checked && record.pk_checked.value && record.pk_checked.value != '~') {
		btnArry.push('uncheck');
	} else {
		btnArry.push('check');
	}

	if (record.pk_manager && record.pk_manager.value && record.pk_manager.value != 'N/A') {
		btnArry.push('untally');
	} else {
		btnArry.push('tally');
	}

	if (
		!(record.pk_casher && record.pk_casher.value && record.pk_casher.value != '~') &&
		!(record.pk_checked && record.pk_checked.value && record.pk_checked.value != '~') &&
		!(record.pk_manager && record.pk_manager.value && record.pk_manager.value != 'N/A')
	) {
		if (record.discardflag && record.discardflag.value) {
			btnArry.push('unabandon');
		} else {
			btnArry.push('abandon');
			btnArry.push('edit');
		}
		btnArry.push('delete');
	}

	return btnArry;
}

function oprButtonClick(_this, props, key, record, index, singleflag) {
	switch (key) {
		case 'edit':
			if (record.pk_voucher && record.pk_voucher.value && record.pk_voucher.value != '') {
				props.pushTo('/Welcome', {
					status: 'edit',
					pagecode: '20021005card',
					id: record.pk_voucher.value
				});
			} else {
				let url = '/nccloud/gl/voucher/listtovoucher.do';
				if(!record.hasOwnProperty('detailMDVOs')){
					let linkData = getDefData('gl_voucher', dataSource);
					record=linkData[tableId].rows[index].values;
				}
				ajax({
					url,
					data: record,
					success: function(res) {
						let ifstep = _this.props.getUrlParam('step');
						let ifshow = _this.props.getUrlParam('ifshowQuery');
						setDefData('number', dataSourceNumber, index);
						setDefData('voucher_detail', dataSourceDetail, res.data);
						_this.ViewModel.setData('gl_voucher_context', _this.props.getSearchParam('c'));
						let yieldCode = _this.props.getUrlParam('pagecode');
						let card = yieldCode ? yieldCode.replace(/list/g, 'card') : '20021005card';
						let status = _this.props.getUrlParam('status');
						let pagekey = _this.props.getUrlParam('pagekey'); //会计平台预览pagekey=‘pre’
						let backAppCode = _this.props.getUrlParam('backAppCode') || getDefData('appcode', dataSourceAppCode);
						let listback=backAppCode||_this.props.getUrlParam('backAppCode');
						let param = {
							appcode: props.getUrlParam('appcode'),
							pagekey:
								yieldCode && yieldCode.indexOf('_') != -1 && pagekey != 'pre'
									? ''
									: pagekey || 'generate',
							pagecode: card,
							n: _this.props.getUrlParam('n'),
							step: ifstep ? ifstep : false,
							ifshowQuery: ifshow ? ifshow : false,
							status: status || 'edit',
							backUrl: '/list',
							listback:listback,
							backflag: pagekey == 'pre' ? 'noback' : 'back' //如果是预览的则没有返回按钮
						};
						if (singleflag) {
							if(_this.props.getUrlParam('backAppCode')){
								param.backAppCode = _this.props.getUrlParam('backAppCode');
								param.backPageCode = _this.props.getUrlParam('backPageCode');
								param.backUrl = '/';
							}else{
								param.backflag='noback';
							}
						}
						let scene=_this.props.getUrlParam('scene');
						if(scene)
							param.scene=scene;
						props.pushTo('/Welcome', param);
					}
				});
			}
			break;
		case 'printvoucher':
			props.ViewModel.setData('selectVoucher', [ record.pk_voucher.value ]);
			props.modal.show('print');
			break;
		case 'sign':
			sign(_this, props, record, index);
			break;
		default:
			operate(_this, props, key, record, index);
			break;
	}
}

function sign(_this, props, record, index) {
	let accountingbooks = [];
	accountingbooks.push(record.pk_accountingbook.value);
	ajax({
		url: '/nccloud/gl/voucher/signdatetype.do',
		data: accountingbooks,
		success: (res) => {
			if (res.data) {
				//需要弹框
				let signData = {
					record: record,
					index: index
				};
				_this.setState({
					signData
				});
				props.modal.show('signRowModal');
			} else {
				operate(_this, props, 'sign', record, index);
			}
		}
	});
}

export function operate(_this, props, key, record, index,ishandle) {
	let data = {
		type: 'list'
	};
	if ('sign' == key) {
		if(!_this.state.signData.date&&ishandle){
			toast({ content: _this.state.json['20021005list-000165']/*"请选择签字日期！"*/, color: 'warning' });
			return;
		}
		if (record) {
			data.date = _this.state.signData.date;
			data.pk_voucher = [ record.pk_voucher.value ];
			data.ts = record.ts.value;
		} else {
			data.date = _this.state.signData.date;
			data.pk_voucher = [ _this.state.signData.record.pk_voucher.value ];
			data.ts = _this.state.signData.record.ts.value;
			index = _this.state.signData.index;
		}
	} else {
		data.pk_voucher = [ record.pk_voucher.value ];
		data.ts = record.ts.value;
	}
	ajax({
		url: '/nccloud/gl/voucher/' + key + '.do',
		data: data,
		async: true,
		success: (res) => {
			if (key != 'delete') {
				if (res.success) {
					let { error, success ,warn} = res.data;
					if (error) {
						//将错误信息返回
						toast({ content: error[data.pk_voucher], color: 'danger' });
					} else {
						if(warn){
							toast({ content: warn[data.pk_voucher], color: 'warning' });
						}
						if (success['gl_voucher']) 
							dealSuccess(_this,props, key, index, success['gl_voucher'].rows[0].values);
					}
				}
			} else {
				if (res.data) {
					let alldata = props.table.getAllTableData(tableId).rows;
					let allindex = [];
					for (let i = 0; i < alldata.length; i++) {
						allindex[alldata[i].values.pk_voucher.value] = i;
					}
					let delindex = [];
					res.data.success.forEach((pk) => {
						if (allindex.hasOwnProperty(pk)) {
							delindex.push(allindex[pk]);
						}
					});
					props.table.deleteTableRowsByIndex(tableId, delindex);
				}

				if (res.data && res.data.warn) {
					toast({ content: res.data.warn, color: 'warning' });
				} else 
					toast({ content: _this.state.json['20021005list-000012'], color: 'success' });/* 国际化处理： 删除成功*/
			}
		},
		error: (res) => {
			toast({ content: res.message, color: 'danger' });
		}
	});
}

function dealSuccess(_this,props, key, index, data) {
	switch (key) {
		case 'delete':
			toast({ content: _this.state.json['20021005list-000012'], color: 'success' });/* 国际化处理： 删除成功*/
			return;
		case 'abandon':
			props.table.setValByKeyAndIndex(tableId, index, 'discardflag', { value: true, display: true, scale: 0 });
			props.table.setValByKeyAndIndex(tableId, index, 'pk_prepared', data.pk_prepared);
			props.table.setValByKeyAndIndex(tableId, index, 'remark', data.remark);
			toast({ content: _this.state.json['20021005list-000013'], color: 'success' });/* 国际化处理： 作废成功*/
			break;
		case 'unabandon':
			props.table.setValByKeyAndIndex(tableId, index, 'discardflag', { value: false, display: false, scale: 0 });
			props.table.setValByKeyAndIndex(tableId, index, 'remark', data.remark);
			toast({ content: _this.state.json['20021005list-000014'], color: 'success' });/* 国际化处理： 取消作废成功*/
			break;
		case 'check':
			//审核
			props.table.setValByKeyAndIndex(tableId, index, 'pk_checked', data.pk_checked);
			toast({ content: _this.state.json['20021005list-000015'], color: 'success' });/* 国际化处理： 审核成功*/
			break;
		case 'uncheck':
			//取消审核
			props.table.setValByKeyAndIndex(tableId, index, 'pk_checked', { value: null, display: null, scale: 0 });
			toast({ content: _this.state.json['20021005list-000016'], color: 'success' });/* 国际化处理： 取消审核成功*/
			break;
		case 'tally':
			//记账
			props.table.setValByKeyAndIndex(tableId, index, 'pk_manager', data.pk_manager);
			toast({ content: _this.state.json['20021005list-000017'], color: 'success' });/* 国际化处理： 记账成功*/
			break;
		case 'untally':
			//取消记账
			props.table.setValByKeyAndIndex(tableId, index, 'pk_manager', { value: null, display: null, scale: 0 });
			toast({ content: _this.state.json['20021005list-000018'], color: 'success' });/* 国际化处理： 取消记账成功*/
			break;
		case 'sign':
			//签字
			props.table.setValByKeyAndIndex(tableId, index, 'pk_casher', data.pk_casher);
			toast({ content: _this.state.json['20021005list-000019'], color: 'success' });/* 国际化处理： 签字成功*/
			break;
		case 'unsign':
			//取消签字
			props.table.setValByKeyAndIndex(tableId, index, 'pk_casher', { value: null, display: null, scale: 0 });
			toast({ content: _this.state.json['20021005list-000020'], color: 'success' });/* 国际化处理： 取消签字成功*/
			break;
	}
	props.table.setValByKeyAndIndex(tableId, index, 'ts', data.ts);
}
