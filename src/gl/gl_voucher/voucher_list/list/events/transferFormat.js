export default function transferFormat(dataQuery){
    let assvos = [];
    dataQuery.dataRows&&dataQuery.dataRows.forEach((item, i) => {
        let obj = {
            m_pk_checktype: item.pk_accassitem,
            // m_pk_checkvalue: item.checkvaluename?item.checkvaluename:null
            m_pk_checkvalue: item.pk_Checkvalue?item.pk_Checkvalue:null
            
        };
        assvos.push(obj);
    // }
});
    if (dataQuery.start_data) {
        dataQuery.prepareddate_start = dataQuery.start_data[0];
        dataQuery.prepareddate_end = dataQuery.start_data[1];
        delete dataQuery.start_data;
    }
    for (let item in dataQuery) {
        if (!dataQuery[item]) {
            dataQuery[item] = null;
        } else {
            if (dataQuery[item] instanceof Array) {
                let queryArray = [];
                dataQuery[item].map((e, i) => {
                    if (item == 'oppositesubj' || item == 'accountingcode') {
                        if(e.refcode){
                            queryArray.push(e.refcode);
                        }else{
                            queryArray=null;
                        }
                        
                    }else if(item=='explanation'){//摘要取显示值refname
                        if(e.refname||e.value){
                            queryArray.push(e.refname?e.refname:e.value);
                        }else{
                            queryArray=null;
                        }
                    } else {
                        if(e.refpk||e.value){
                            queryArray.push(e.refpk?e.refpk:e.value);
                        }else{
                            queryArray=null;
                        }
                        
                    }
                });
                if (item != 'pk_vouchertype') {
                    dataQuery[item] = queryArray;
                    if (item == 'period') {
                        //会计期间保存数据整理

                        queryArray[0] = dataQuery['period_start']?dataQuery['period_start'].refname:'';
                        queryArray[1] = dataQuery['period_end']?dataQuery['period_end'].refname:'';
                    }
                } else {
                    dataQuery[item] = queryArray?queryArray[0]:'';
                }
            }
            if (isObj(dataQuery[item]) && item != 'period_start' && item != 'period_end') {
                if (dataQuery[item].value) {
                    if(item=='pk_accountingbook'){
                        let accountingbooks =[];
                        accountingbooks.push(dataQuery[item].value)
                        dataQuery[item]=accountingbooks;
                    }else{
                        dataQuery[item] = dataQuery[item].value;
                    }
                } else {
                    if(item != 'amount' && item != 'oriamount'&& item != 'quantity'&& item != 'checkno'&& item != 'checkdate'){
                        dataQuery[item] = null;
                    }
                }
            }
        }
    }
    //会计期间开始期间结束期间字段处理
    for (let item in dataQuery) {
        if (isObj(dataQuery[item])) {
            if (dataQuery[item].value) {
                dataQuery[item] = dataQuery[item].value;
            } else {
                if(item != 'amount' && item != 'oriamount'&& item != 'quantity'&& item != 'checkno'&& item != 'checkdate'){
                    dataQuery[item] = null;
                }
            }
        }
    }
    if (assvos.length != 0) {
        dataQuery.assvos = assvos;
    }
    if(dataQuery.selectPretype=='0'){//选中的会计期间
        dataQuery.prepareddate_start=null;
        dataQuery.prepareddate_end=null;
        
    }
    if(dataQuery.selectDatetype=='1'){//选中的日期
        dataQuery.period=null
    }
    return dataQuery;
}
export function isObj(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'Object';
}