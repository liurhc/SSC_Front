import {ajax,cardCache} from 'nc-lightapp-front';
let { setDefData } = cardCache;
import { dataSourceTable,tableId} from '../../../../public/components/constJSON';

export default function (props,config,pks,total) {
    let data = {
        pk_vouchers: pks
    };
    ajax({
        url: '/nccloud/gl/voucher/voucherQuery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    //缓存
                    if (data) {
                        props.table.setAllTableData(tableId, data[tableId]);
                        setDefData(tableId, dataSourceTable, data[tableId]);
                    } else {
                        props.table.setAllTableData(tableId, { rows: [] });
                    }
                }
            }
    });
}