export default function controlButton(props,tableId) {
    //当有数据时，打印、输出可用，否则置灰
    let rows = props.table.getAllTableData(tableId).rows;
    if (rows.length == 0) {
        props.button.setButtonDisabled(['printvoucher', 'printlist','printbill','output','outputlist','outputbill','delete', 'abandon','unabandon'], true);
    }else{
        props.button.setButtonDisabled(['printvoucher', 'printlist','printbill','output','outputlist','outputbill'], false);
    }

    //当勾选选择数据后，表头删除、作废/取消作废可用，否则置灰
    let selectRows=props.table.getCheckedRows(tableId);
    if(!selectRows||selectRows.length==0){
        props.button.setButtonDisabled(['delete', 'abandon','unabandon','sign','unsign','check','uncheck','tally','untally'], true);
    }else if(selectRows.length==1){
        let record=selectRows[0].data.values;
        let buttonArray=[];
        if(record.pk_casher && record.pk_casher.value && record.pk_casher.value != '~'){
            buttonArray.push('unsign');
        }else{
            buttonArray.push('sign');
        }
        if (record.pk_checked && record.pk_checked.value && record.pk_checked.value != '~') {
            buttonArray.push('uncheck');
        } else {
            buttonArray.push('check');
        }
    
        if (record.pk_manager && record.pk_manager.value && record.pk_manager.value != 'N/A') {
            buttonArray.push('untally');
        } else {
            buttonArray.push('tally');
        }
        if (
            !(record.pk_casher && record.pk_casher.value && record.pk_casher.value != '~') &&
            !(record.pk_checked && record.pk_checked.value && record.pk_checked.value != '~') &&
            !(record.pk_manager && record.pk_manager.value && record.pk_manager.value != 'N/A')
        ) {
            if (record.discardflag && record.discardflag.value) {
                buttonArray.push('unabandon');
            } else {
                buttonArray.push('abandon');
            }
            buttonArray.push('delete');
        }
        props.button.setButtonDisabled(buttonArray, false);
    }else{
        props.button.setButtonDisabled(['delete', 'abandon','unabandon','sign','unsign','check','uncheck','tally','untally'], false);
    }
}