import { ajax, toast, promptBox, print, cardCache, output } from 'nc-lightapp-front';
import { dataSourceNormal,dataSource } from '../../../../public/components/constJSON';
let { getDefData } = cardCache;
import controlButton from './controlButton';

let tableid = 'gl_voucher';
export default function buttonClick(props, id) {
	switch (id) {
		case 'saveall': //保存全部
			let alldata = {};
			let rows = props.table.getCheckedRows(tableid);
			if (rows.length > 0) {
				let linkData = getDefData('gl_voucher', dataSource);
				rows.forEach((row) => {
					if(!row.data.values.hasOwnProperty('detailMDVOs')){
						alldata[row.index] = linkData[tableid].rows[row.index].values;
					}else{
						alldata[row.index]=row.data.values;
					}
				});
				ajax({
					url: '/nccloud/gl/voucher/saveall.do',
					data: alldata,
					success: (res) => {
						let { success, data } = res;
						if (success) {
							if (data) {
								let rowindex = [];
								for (let index in data) {
									rowindex.push(index);
								}
								props.table.deleteTableRowsByIndex(tableid, rowindex);
							}
						}
					},
					error: (res) => {
						toast({ content: res.message, color: 'warning' });
					}
				});
			} else {
				toast({ content: this.state.json['20021005list-000008'], color: 'warning' });/* 国际化处理： 请选择一行保存！*/
			}
			break;
		case 'add':
			props.pushTo('/Welcome', {
				pagecode: '20021005card',
				status: 'add',
				//modifyPk: JSON.stringify(this.pk_acc)
			});
			break;
		case 'adjustAdd':
			props.pushTo('/Welcome', {
				pagecode: '20021005card',
				status: 'add',
				//modifyPk: JSON.stringify(this.pk_acc),
				adjust: '1'
			});
			break;
		case 'delete':
			let delUrl = '/nccloud/gl/voucher/delete.do';
			promptBox({
				color: 'warning',
				title:this.state.json['20021005list-000009'],/* 国际化处理： 删除*/
				content: this.state.json['20021005list-000010'],/* 国际化处理： 确定要删除所选数据吗?*/
				beSureBtnClick: dealOperate.bind(this,this, props, delUrl, id)
			});
			//dealOperate(props, delUrl, id);
			break;
		case 'query':
			this.setState({
				showModal: true
			});
			break;
		case 'refresh':
			//刷新
			let data = getDefData('realQuery', dataSourceNormal);
			if (data) {
				ajax({
					url: '/nccloud/gl/voucher/voucherQuery.do',
					data: data,
					success: (res) => {
						let { success, data } = res;
						toast({ content: this.state.json['20021005list-000160'], color: 'success' });
						if (success) {
							if (data) {
								this.props.table.setAllTableData(tableid, data[tableid]);
							} else {
								this.props.table.setAllTableData(tableid, { rows: [] });
							}
						}
					}
				});
			}
			break;
		case 'abandon':
			//作废
			let abnUrl = '/nccloud/gl/voucher/abandon.do';
			dealOperate(this,props, abnUrl, id);
			break;
		case 'unabandon':
			//取消作废
			let unabnUrl = '/nccloud/gl/voucher/unabandon.do';
			dealOperate(this,props, unabnUrl, id);
			break;
		case 'check':
			//审核
			let checkUrl = '/nccloud/gl/voucher/check.do';
			dealOperate(this,props, checkUrl, id);
			break;
		case 'uncheck':
			//取消审核
			let uncheckUrl = '/nccloud/gl/voucher/uncheck.do';
			dealOperate(this,props, uncheckUrl, id);
			break;
		case 'tally':
			//记账
			let tallyUrl = '/nccloud/gl/voucher/tally.do';
			dealOperate(this,props, tallyUrl, id);
			break;
		case 'untally':
			//取消记账
			let untallyUrl = '/nccloud/gl/voucher/untally.do';
			dealOperate(this,props, untallyUrl, id);
			break;
		case 'quicktally':
			let quicktally=getDefData('quickTally', dataSource);
			props.form.setFormItemsValue('quickTally', quicktally);
			props.modal.show('quickTally');
			break;
		case 'sign':
			//签字
			batchsign(this,props);
			// let signUrl = '/nccloud/gl/voucher/sign.do';
			// dealOperate(props, signUrl, id);
			break;
		case 'unsign':
			//取消签字
			let unsignUrl = '/nccloud/gl/voucher/unsign.do';
			dealOperate(this,props, unsignUrl, id);
			break;
		case 'spacequery':
			//空号查询
			// props.modal.show('numberQueryModal');
			props.modal.show('spacequery');
			break;
		case 'import':
			break;
		case 'export':
			//导出
			let exportdata = [];
			props.table.getCheckedRows(tableid).forEach((val) => {
				exportdata.push(val.data.values.pk_voucher.value);
			});
			this.setState(
				{
					selectVoucher: exportdata //传递主键数组
				},
				() => {
					props.modal.show('exportFileModal');
				}
			);
			// props.modal.show('exportFileModal');
			break;
		case 'exportXML':
			//导出XML
			props.modal.show('exportXML');
			break;
		case 'printvoucher':
			//打印凭证
			const selectedData = props.table.getCheckedRows(tableid);
			let dataArr = [];
			selectedData.forEach((val) => {
				dataArr.push(val.data.values.pk_voucher.value);
			});
			this.props.ViewModel.setData('selectVoucher', dataArr);
			let meta = props.meta.getMeta();
			meta['print'].items.map((item, index) => {
				if (item.attrcode == 'printemp') {
					item.visible = true;
				}
			});
			props.meta.setMeta(meta);
			props.modal.show('print');
			
			break;
		case 'printlist':
			//打印列表
			let oids = [];
			let allrows = props.table.getAllTableData(tableid).rows;
			allrows.forEach((val) => {
				oids.push(val.values.pk_voucher.value);
			});
			
			print(
				'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
				'/nccloud/gl/voucher/printlist.do', //后台服务url
				{
					billtype: 'C0', //单据类型
					appcode: '20020VSIGN', //功能节点编码，即模板编码
					nodekey: '20020VSIGN1', //模板节点标识
					oids: oids
				},
				false
			);
			break;
		case 'printbill':
			//打印清单
			const selectedData2 = props.table.getCheckedRows(tableid);
			let dataArr2 = [];
			selectedData2.forEach((val) => {
				dataArr2.push(val.data.values.pk_voucher.value);
			});
			print(
				'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
				'/nccloud/gl/voucher/printlist.do', //后台服务url
				{
					billtype: 'C0', //单据类型
					appcode: '20020VSIGN', //功能节点编码，即模板编码
					nodekey: '20020VSIGN1', //模板节点标识
					oids: dataArr2
				},
				false
			);
			break;
		case 'output':
			let outputdata = {
				printsubjlevel: '0', //printsubjlevel：科目汇总到第几级，不汇总是0
				printasslevel: '0', //printasslevel：按辅助项汇总 勾选-1 不勾选0
				printmode: '0', //printmode：代账单格式 0不勾选 1勾选
				selectIndex: '-1' //selectIndex: 选中第几行分录 从0开始
			};
			let ouputArr = [];
			props.table.getCheckedRows(tableid).forEach((val) => {
				ouputArr.push(val.data.values.pk_voucher.value);
			});
			output({
				url: '/nccloud/gl/voucher/outputvoucher.do',
				data: {
					appcode: '20020PREPA', //小应用编码
					oids: ouputArr,
					userjson: JSON.stringify(outputdata),
					outputType: 'output'
				}
			});
			break;
		case 'outputbill':
			//输出清单
			let outputBillArr = [];
			props.table.getCheckedRows(tableid).forEach((val) => {
				outputBillArr.push(val.data.values.pk_voucher.value);
			});
			output({
				url: '/nccloud/gl/voucher/outputlist.do',
				data: {
					appcode: '20020VSIGN', //小应用编码
					nodekey: '20020VSIGN1', //模板节点标识
					oids: outputBillArr,
					outputType: 'output'
				}
			});
			break;
		case 'outputlist':
			output({
				url: '/nccloud/gl/voucher/outputlist.do',
				data: {
					appcode: '20020VSIGN', //小应用编码
					nodekey: '20020VSIGN1', //模板节点标识
					oids: props.table.getAllTableData(tableid).allpks,
					outputType: 'output'
				}
			});
			break;
	}
}

function batchsign(_this,props){
	const selectedData = props.table.getCheckedRows(tableid);
	if (selectedData.length == 0) return;
	let accountingbooks = [];
	selectedData.forEach((val) => {
		accountingbooks.push(val.data.values.pk_accountingbook.value);
	});
	ajax({
		url: '/nccloud/gl/voucher/signdatetype.do',
		data: accountingbooks,
		success: (res) => {
			if(res.data){
				//需要弹框
				props.modal.show('signModal');
			}else{
				let signUrl = '/nccloud/gl/voucher/sign.do';
				dealOperate(_this,props, signUrl, 'sign');
			}
		}
	})
}

export function dealOperate(_this,props, opurl, id) {
	const selectedData = props.table.getCheckedRows(tableid);
	if (selectedData.length == 0) return;
	let indexArr = [];
	let dataArr = [];
	let indexObj = {};
	let allindex=[];
	if(id=='delete'){
		let alldata=props.table.getAllTableData(tableid).rows;
		for(let i=0;i<alldata.length;i++){
			allindex[alldata[i].values.pk_voucher.value] = i;
		}
	}
	//只传ts和Pk_voucher主键
	selectedData.forEach((val) => {
		dataArr.push(val.data.values.pk_voucher.value);
		indexArr.push(val.index);
		indexObj[val.data.values.pk_voucher.value] = val.index;
	});
	let data={
		pk_voucher: dataArr,
		type: 'list'
	}
	if(id=='sign'&&this){
		if(this.state.signData.date)
			data.date=this.state.signData.date;
		else{
			toast({ content: this.state.json['20021005list-000165']/*"请选择签字日期！"*/, color: 'warning' });
			return;
		}
	}
	ajax({
		url: opurl,
		data: data,
		async: true,
		success: (res) => {
			let { data } = res;
			if (data && (data.error || data.warn) ) {//有错误信息或者警告信息
				if (id == 'abandon'||id == 'unabandon'||
					id == 'check' || id == 'uncheck' ||
					id == 'sign' || id == 'unsign' ||
					id == 'tally' || id == 'untally') {
						
					props.modal.show('batchhint');
					let errMessageArr = [];
					selectedData.forEach((val) => {
						let pk_voucher=val.data.values.pk_voucher.value;
						if(data.error&&data.error.hasOwnProperty(pk_voucher)){
							let err = val.data.values;
							err.result={display: _this.state.json['20021005list-000011'], value: _this.state.json['20021005list-000011'] }/* 国际化处理： 失败,失败*/
							err.reason={
								display: data.error[pk_voucher],
								value: data.error[pk_voucher]
							};
							errMessageArr.push({values:err})
						}else if (data.warn&&data.warn.hasOwnProperty(pk_voucher)) {
							let warn = val.data.values;
							warn.result = {display:_this.state.json['20021005list-000164'], value: _this.state.json['20021005list-000164']};
							warn.reason = {
								display: data.warn[pk_voucher],
								value: data.warn[pk_voucher]
							};
							errMessageArr.push({ values: warn });
						}
					});
					props.table.setAllTableData('batchhint', { rows: errMessageArr });
					refreshTable(props,data);
					return;
				} else {
					toast({ content: data.error, color: 'danger', position: 'bottom' });
				}
			}
			if (res.success&&data) {
				if (id == 'delete') {
					if (data && data.warn && data.warn.length > 0) {
						toast({ content: data.warn, color: 'warning' });
					}
					let delindex=[];
					data.success.forEach((pk)=>{
						if(allindex.hasOwnProperty(pk)){
							delindex.push(allindex[pk]);
						}
					});
					props.table.deleteTableRowsByIndex(tableid, delindex);
					
					controlButton(props,tableid);
					toast({ content: _this.state.json['20021005list-000012'], color: 'success', position: 'bottom' });/* 国际化处理： 删除成功*/
				} else {
					refreshTable(props,data);
					dealSuccess(_this,props, data, indexObj, id);
				}
			}
		},
		error: (res) => {
			//props.modal.show('batchHint');
			toast({ content: res.message, color: 'danger' });
		}
	});
}

export function refreshTable(props,data){
	if(data.success&&data.success.hasOwnProperty('gl_voucher')){
		let succrows=data.success['gl_voucher'].rows;
		let voucherRow={};
		succrows.forEach((row)=>{
			voucherRow[row.values.pk_voucher.value]=row;
		});
	
		let tabledata=props.table.getAllTableData(tableid);
		for(let i=0;i<tabledata.rows.length;i++){
			let row=tabledata.rows[i];
			if(voucherRow.hasOwnProperty(row.values.pk_voucher.value)){
				tabledata.rows[i].values=voucherRow[row.values.pk_voucher.value].values;
			}
		}
		delete tabledata.pageInfo;
		
		props.table.setAllTableData(tableid,tabledata);
	}
}

export function dealSuccess(_this,props, data, indexObj, id) {
	switch (id) {
		case 'delete':
			let succdatas = data.success['gl_voucher'].rows;
			let deleteRows = [];
			succdatas.forEach((val) => {
				deleteRows.push(indexObj[val.values.pk_voucher.value]);
			});
			props.table.deleteTableRowsByIndex(tableid, deleteRows);
			toast({ content: _this.state.json['20021005list-000012'], color: 'success' });/* 国际化处理： 删除成功*/
			break;
		case 'abandon':
			toast({ content: _this.state.json['20021005list-000013'], color: 'success' });/* 国际化处理： 作废成功*/
			break;
		case 'unabandon':
			toast({ content: _this.state.json['20021005list-000014'], color: 'success' });/* 国际化处理： 取消作废成功*/
			break;
			break
		case 'check':
			//审核
			toast({ content: _this.state.json['20021005list-000015'], color: 'success' });/* 国际化处理： 审核成功*/
			break;
		case 'uncheck':
			//取消审核
			toast({ content: _this.state.json['20021005list-000016'], color: 'success' });/* 国际化处理： 取消审核成功*/
			break;
		case 'tally':
			//记账
			toast({ content: _this.state.json['20021005list-000017'], color: 'success' });/* 国际化处理： 记账成功*/
			break;
		case 'untally':
			//取消记账
			toast({ content: _this.state.json['20021005list-000018'], color: 'success' });/* 国际化处理： 取消记账成功*/
			break;
		case 'sign':
			//签字
			toast({ content: _this.state.json['20021005list-000019'], color: 'success' });/* 国际化处理： 签字成功*/
			break;
		case 'unsign':
			//取消签字
			toast({ content: _this.state.json['20021005list-000020'], color: 'success' });/* 国际化处理： 取消签字成功*/
			break;
	}
}
