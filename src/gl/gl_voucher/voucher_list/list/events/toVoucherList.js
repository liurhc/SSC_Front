// 外系统联查、生成凭证到列表
import { cardCache,cacheTools,ajax ,getBusinessInfo} from 'nc-lightapp-front';
const { setDefData,getDefData } = cardCache;
import { 
    tableId,dataSource,dataSourceTable,
    dataSourceAppCode,dataSourcePageCode,
    listCacheKey,linkToCacheTool
} from '../../../../public/components/constJSON';
import controlButton from './controlButton';

export function toVoucherList(props){
    //支持小友机器人
    let robotParam=props.getSearchParam('param');
    let appcode = props.getUrlParam('appcode') || props.getSearchParam('appcode')|| props.getSearchParam('c');
    let pagecode = props.getUrlParam('pagecode') || props.getSearchParam('pagecode')|| props.getSearchParam('p');
	if(robotParam){
        let jsonParam=JSON.parse(robotParam);
        let pagetype = props.getUrlParam('type');
        if('check'==pagetype||'tally'==pagetype){
            //当前业务日期对应年的待记账审核数据查出来
            let param={};
            param.pageInfo=props.table.getTablePageInfo(tableId);
            param.type=pagetype;
            param.busidate=getBusinessInfo().businessDate;
            param.botparam=jsonParam;
            ajax({
                url: '/nccloud/gl/voucher/botQueryVoucher.do',
                data: param,
                success: (res) => {
                    let { success, data } = res;
                    if (success && data) {
                        props.table.setAllTableData(tableId, data[tableId]);
                        setDefData(tableId, dataSourceTable, data[tableId]);
                        props.table.selectAllRows(tableId,true);
                        controlButton(props,tableId)
                    }
                }
            });
            return true;
        }else if(jsonParam.entity!='openPoint'){//openPoint是制单
            //一句话生成凭证
            props.pushTo('/Welcome', {
                appcode: appcode,
                pagecode: '20021005card',
                status: 'edit'});
            return false;
        }
		
    }

    let scene = props.getUrlParam('scene');//会计平台及单据联查会传
    if (scene && scene.indexOf('_Preview2019') != -1) {
        //业务单据预览
        let previewData = cacheTools.get(scene);
        if (previewData) {
            ajax({
                url: '/nccloud/fip/generate/previewSrcBill.do',
                data: previewData,
                success: (res) => {
                    let { success, data } = res;
                    if (success && data) {
                        if (data[tableId]) {
                            props.table.setAllTableData(tableId, data[tableId]);
                        }
                    }
                }
            });
        }
    } else if (scene && scene.indexOf('_MadeBillGL') != -1) {
        //业务单据制单
        let madeBillData = cacheTools.get(scene);
        if(madeBillData){
            ajax({
                url: '/nccloud/fip/generate/generateBillForGL.do',
                data: madeBillData,
                success: (res) => {
                    let { success, data } = res;
                    if (success && data) {
                        if(data.grid){
                            props.table.setAllTableData(tableId, data.grid[tableId]);
                        }
                    }
                }
            });
        }
    } else {//联查生成凭证
        //如果需要返回将 返回的backAppCode和backPageCode缓存起来
        let backAppCode = props.getUrlParam('backAppCode');
        if (backAppCode) {
            setDefData('appcode', dataSourceAppCode, backAppCode);
        }
        let backPageCode = props.getUrlParam('backPageCode');
        if (backPageCode) {
            setDefData('pagecode', dataSourcePageCode, backPageCode);
        }
        // let pagekey = props.getUrlParam('pagekey'); //联查还是生成
        if ((scene && scene.indexOf('_LinkVouchar')!=-1)||cacheTools.get(linkToCacheTool)) {
            //联查凭证
            let pk_vouchers = getDefData(listCacheKey, dataSource);//pushTo联查
            if (!pk_vouchers || pk_vouchers.length == 0) {
                let vouchers = cacheTools.get(linkToCacheTool);//openTo联查(也包括会计平台及单据联查批量的)
                if(vouchers.hasOwnProperty("id")){//单据生成联查目标
                    pk_vouchers = vouchers.id;
                }else{//单据批量联查
                    pk_vouchers=vouchers;
                }
            }
            if (pk_vouchers && pk_vouchers.length > 0) {
                ajax({
                    url: '/nccloud/gl/voucher/query.do',
                    data: {
                        pk_voucher: pk_vouchers,
                        type: 'list'
                    },
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            if (data) {
                                setDefData(listCacheKey, dataSource, data);//缓存列表数据，下次返回避免请求，选中行标识避免丢失
                                props.table.setAllTableData(tableId, data[tableId]);
                            } else {
                                setDefData(listCacheKey, '');
                                props.table.setAllTableData(tableId, { rows: [] });
                            }
                        }
                    }
                });
            }
        } else {
            //pushTo生成凭证
            let voucherData = getDefData(listCacheKey, dataSource);
            if (voucherData) {
                let rows = voucherData[tableId].rows;
                let newrows = [];
                for (let i = 0; i < rows.length; i++) {
                    let newrow = {};
                    let newvalues = {};
                    let row = rows[i];
                    for (let key in row.values) {
                        if (key != 'detailMDVOs') {
                            newvalues[key] = row.values[key];
                        }
                    }
                    newrow.values = newvalues;
                    newrows.push(newrow);
                }//效率问题：把detailMDVOs去掉再放进表格里
                // console.time("setAllTableData");
                props.table.setAllTableData(tableId, { rows: newrows });
                // console.timeEnd("setAllTableData");
            }
        }
    }
    return true;
}

export function robotInShort(props){
     //小友机器人一句话生成凭证
     let robotParam=props.getSearchParam('param');
     let appcode = props.getUrlParam('appcode') || props.getSearchParam('appcode')|| props.getSearchParam('c');
     if(robotParam){
        let jsonParam=JSON.parse(robotParam);
        // if(jsonParam.entity!='openPoint'){//openPoint是制单
        if(jsonParam.pointcode=='20020PREPA'){//20020PREPA是制单
            //一句话生成凭证
            props.pushTo('/Welcome', {
                appcode: appcode,
                pagecode: '20021005card',
                status: 'edit'});
            return false;
        }
     }
     return true;
}