import { base, ajax } from 'nc-lightapp-front';
const { NCFormControl, NCButton } = base;
import './index.less';
/**
 * 空号查询
 */
let areacode = 'spacequery';

function createSpaceQuery(_this) {
	let { search } = _this.props;
	let { NCCreateSearch } = search;
	let spacequery_clearBtn_disabled=_this.state.spacequery_clearBtn_disabled?'spacequery_clearBtn spacequery_clearBtn_disabled':'spacequery_clearBtn';
	let btndisabled=_this.state.spacequery_clearBtn_disabled?true:false;
	return (
		<div className="space-query-modal">
			{NCCreateSearch(areacode, {
				clickSearchBtn: spaceQuery.bind(_this),
				onAfterEvent: onAfterEvent.bind(_this),
				showAdvBtn: false,//是否显示高级按钮
				// showClearBtn:false,//是否显示清空按钮
				// clickClearBtnEve: clearContent.bind(_this)
			})}
			<button fieldid="clearContent" className={spacequery_clearBtn_disabled} disabled={btndisabled} onClick={clearContent.bind(_this)}>{_this.state.json['20021005list-000045']/* 国际化处理： 清除*/}</button>
			<div className="content-area" contenteditable="true">
				<pre>{_this.state.spaceQueryText}</pre>
			</div>
		</div>
	);
}

function clearContent() {
	this.setState({
		spaceQueryText: '',
		spacequery_clearBtn_disabled:true
	});
}

function spaceQuery(props, searchVal) {
	if (searchVal && searchVal.conditions && searchVal.conditions.length > 0) {
		let searchValData = searchVal.conditions;
		let data = {};
		searchValData.map((val) => {
			if (val.field == 'period') {
				data[val.field] = val.display;
			} else {
				data[val.field] = val.value.firstvalue;
			}
		});
		ajax({
			url: '/nccloud/gl/voucher/queryBreakNo.do',
			data: data,
			success: (res) => {
				let message = this.state.spaceQueryText + res.data.message;
				this.setState({
					spaceQueryText: message,
					spacequery_clearBtn_disabled:false
				});
			}
		});
	}
}

function onAfterEvent(key, val, areaId) {
	if ('pk_accountingbook' == key && val) {
		let pk_accountingbook = val.refpk;
		if (pk_accountingbook && pk_accountingbook != '') {
			ajax({
				url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
				data: {
					pk_accountingbook: pk_accountingbook
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						let period = data.bizPeriod;
						this.props.search.setSearchValByField(areacode, 'period', { display: period, value: period });

						let pk_accperiodscheme = data.pk_accperiodscheme;
						let meta = this.props.meta.getMeta();
						meta[areaId].items.map((item, key) => {
							if (item.attrcode == 'period') {
								//设置会计期间参照过滤
								item.queryCondition = () => {
									return {
										pk_accperiodscheme: pk_accperiodscheme,
										GridRefActionExt: 'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
									};
								};
							}
							if (item.attrcode == 'pk_vouchertype') {
								//设置凭证类别过滤
								item.queryCondition = () => {
									return {
										pk_org: pk_accountingbook,
										isDataPowerEnable: 'Y',
										DataPowerOperationCode: 'fi',
										GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder'
									};
								};
							}
						});
						this.props.meta.setMeta(meta);
					}
				}
			});
		} else {
			this.props.search.setSearchValue(areacode, { period: { display: '', value: '' } });
			this.props.search.setSearchValue(areacode, { pk_vouchertype: { display: '', value: '' } });
		}
	}
}

function setSpaceQueryDefault(context, data, appcode) {
	let meta = this.props.meta.getMeta();
	if (meta[areacode]) {
		meta[areacode].items.map((item, key) => {
			if (item.attrcode == 'pk_accountingbook') {
				if (context.defaultAccbookPk && context.defaultAccbookPk != '') {
					item.initialvalue = {
						display: context.defaultAccbookName,
						value: context.defaultAccbookPk
					};
				}
				item.disabledDataShow = true,
					item.queryCondition = () => {
						return {
							TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
							appcode: appcode
						};
					};
			}
			if (context.defaultAccbookPk && context.defaultAccbookPk != '') {
				if (item.attrcode == 'period') {
					//设置会计期间参照过滤
					if (data)
						item.initialvalue = {
							display: data.bizPeriod,
							value: data.bizPeriod
						};
					item.queryCondition = () => {
						return {
							pk_accperiodscheme: data.pk_accperiodscheme,
							GridRefActionExt: 'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
						};
					};
				}
				if (item.attrcode == 'pk_vouchertype') {
					//设置凭证类别过滤
					item.queryCondition = () => {
						return {
							pk_org: context.defaultAccbookPk,
							isDataPowerEnable: 'Y',
							DataPowerOperationCode: 'fi',
							GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder'
						};
					};
				}
			}
		});
		this.props.meta.setMeta(meta);
	}
}

export { createSpaceQuery, setSpaceQueryDefault,clearContent };
