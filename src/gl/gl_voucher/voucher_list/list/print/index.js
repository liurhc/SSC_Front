import { toast, output ,ajax,cardCache,base} from 'nc-lightapp-front';
import {mutilangJson_list} from '../../../../public/components/constJSON';
let { getDefData } = cardCache;
let { NCButton} = base;
/**
 * 打印
 */
let areacode = 'print';
function printItem() {
	let json=getDefData('mutilangJson_list', mutilangJson_list);
	if(!json||json.length==0)
		return <div></div>
	return {
		areastatus: 'edit',
		code: 'print',
		moduletype: 'form',
		name: json['20021005list-000050'],/* 国际化处理： 打印*/
		status: 'edit',
		items: [
			{
				attrcode: 'printsubjlevel',
				colnum: '1',
				color: '#6E6E77',
				isDataPowerEnable: true,
				itemtype: 'select',
				label: json['20021005list-000056'],/* 国际化处理： 科目汇总级次*/
				maxlength: '20',
				position: '1',
				visible: true,
				options: [
					{ display: json['20021005list-000057'], value: '0' },/* 国际化处理： 不汇总*/
					{ display: json['20021005list-000058'], value: '1' },/* 国际化处理： 1级*/
					{ display: json['20021005list-000059'], value: '2' },/* 国际化处理： 2级*/
					{ display: json['20021005list-000060'], value: '3' },/* 国际化处理： 3级*/
					{ display: json['20021005list-000061'], value: '4' },/* 国际化处理： 4级*/
					{ display: json['20021005list-000062'], value: '5' },/* 国际化处理： 5级*/
					{ display: json['20021005list-000063'], value: '6' },/* 国际化处理： 6级*/
					{ display: json['20021005list-000064'], value: '7' },/* 国际化处理： 7级*/
					{ display: json['20021005list-000065'], value: '8' },/* 国际化处理： 8级*/
					{ display: json['20021005list-000066'], value: '-100' }/* 国际化处理： 按科目设置*/
				],
				// initialValue: { display: '不汇总', value: '0' }
			},
			{
				attrcode: 'printasslevel',
				colnum: '1',
				color: '#6E6E77',
				isnextrow: true,
				isDataPowerEnable: true,
				itemtype: 'checkbox_switch',
				label: json['20021005list-000067'],/* 国际化处理： 按辅助项汇总*/
				maxlength: '20',
				position: '2',
				visible: true
			},
			{
				attrcode: 'printmode',
				colnum: '1',
				color: '#6E6E77',
				isnextrow: true,
				isDataPowerEnable: true,
				itemtype: 'checkbox_switch',
				label: json['20021005list-000068'],/* 国际化处理： 代账单格式*/
				maxlength: '20',
				position: '2',
				visible: true
			},
			{
				attrcode: 'printemp',
				colnum: '1',
				color: '#6E6E77',
				isnextrow: true,
				required: true,
				isDataPowerEnable: true,
				itemtype: 'refer',
				label: json['20021005list-000069'],/* 国际化处理： 选择模板*/
				maxlength: '20',
				position: '1',
				visible: true,
				refcode: 'gl/refer/voucher/PrintTemplateGridRef/index',
				queryCondition: () => {
					return {
						appcode: '20020PREPA'
					};
				}
			}
		]
	};
}

function createPrint(_this) {
	let self=this;
	let { form } = self.props;
	let { createForm } = form;
	let json=getDefData('mutilangJson_list', mutilangJson_list);
	if(!json||json.length==0){
		return <div/>
	} else {
		let ctrltypedata = { 'printsubjlevel': { display: json['20021005list-000057'], value: '0' } };
		self.props.form.setFormItemsValue('print', ctrltypedata);
		return (
			<div className="nc-theme-form-label-c">
				{createForm(
					areacode,
					{
						onAfterEvent: printAfterEvent.bind(_this)
					}
				)}
			</div>
		)
	}
}

function printAfterEvent(props,tableid,key,refValue){
	switch (key){
		case 'printemp':
			if(refValue&&refValue.refpk&&refValue.refpk!=''){
				if(refValue.refcode=='20020PREPA20_nccloud'){//平行记账模版
					this.isParallelPrint=true;
					break;
				}
			}
			this.isParallelPrint=false;
			break;
	}
}

function printButton(_this,props,selectIndex,voucher){
	let json=getDefData('mutilangJson_list', mutilangJson_list);
	return (
		<div>
			<NCButton shape="border" colors="primary" onClick={ confirmPrint.bind(_this,props,selectIndex,voucher,false) }>{json['20021005list-000161']/**预览 */}</NCButton>
			<NCButton shape="border" colors="primary" onClick={ confirmPrint.bind(_this,props,selectIndex,voucher,true) }>{json['20021005list-000162']/**打印 */}</NCButton>
			<NCButton shape="border" colors="primary" onClick={ closePrint.bind(_this)}>{json['20021005list-000163']/**取消 */}</NCButton>
		</div>
	)
}

function confirmPrint(props,selectIndex,voucher,isPrint=false) {
	const {printPreview,printerView} = this.printOnClient;
	let json=getDefData('mutilangJson_list', mutilangJson_list);
	let printmode = props.form.getFormItemsValue(areacode, 'printmode').value;
	if (printmode && selectIndex == '-1') {
		toast({ content: json['20021005list-000070'], color: 'warning' });/* 国际化处理： 请选择需要打印的分录！*/
		return;
	}
	let selectVoucher=[]
	if(voucher){
		if(voucher==''){
			toast({ content: json['20021005list-000071'], color: 'warning' });/* 国际化处理： 该凭证不能打印！*/
			return;
		}
		selectVoucher.push(voucher);
	}else{
		selectVoucher = props.ViewModel.getData('selectVoucher');
		if (selectVoucher.length == 0) {
			toast({ content: json['20021005list-000072'], color: 'warning' });/* 国际化处理： 请选择需要打印的凭证！*/
			return;
		}
	}
	
	let printemp = props.form.getFormItemsValue(areacode, 'printemp').value;
	if (printemp=='') {
		toast({ content: json['20021005list-000073'], color: 'warning' });/* 国际化处理： 请选择模板！*/
		return;
	}
	let printsubjlevel = props.form.getFormItemsValue(areacode, 'printsubjlevel').value;
	let printasslevel = props.form.getFormItemsValue(areacode, 'printasslevel').value;
	let printdata = {
		printsubjlevel: printsubjlevel, //printsubjlevel：科目汇总到第几级，不汇总是0
		printasslevel: printasslevel ? '-1' : '0', //printasslevel：按辅助项汇总 勾选-1 不勾选0
		printmode: printmode ? '1' : '0', //printmode：代账单格式 0不勾选 1勾选
		selectIndex: selectIndex, //selectIndex: 选中第几行分录 从0开始
		isParallelPrint: true//this.isParallelPrint
	};
	let appcode= props.getUrlParam('appcode')||props.getUrlParam('c')||props.getSearchParam('c');
	//校验单据号断号
	ajax({
		url: '/nccloud/gl/voucher/checkprint.do',
		data: selectVoucher,
		success: (res) => {
			let {data}=res;
			if(data){
				if(data.failure){
					if(selectVoucher.length==1){
						toast({ content: json['20021005list-000074'], color: 'warning' });/* 国际化处理： #本类凭证存在断号，取消打印#*/
						return;
					}else{
						props.modal.show('batchhint');
						let rows=data.failure.gl_voucher.rows;
						let errMessageArr = [];
						rows.forEach((val) => {
								let err = val.values;
								err.result = { display: json['20021005list-000011'], value: json['20021005list-000011'] };/* 国际化处理： 失败,失败*/
								err.reason = {
									display: json['20021005list-000074'],/* 国际化处理： #本类凭证存在断号，取消打印#*/
									value: json['20021005list-000074']/* 国际化处理： #本类凭证存在断号，取消打印#*/
								};
								errMessageArr.push({ values: err });
							
						});
						props.table.setAllTableData('batchhint', { rows: errMessageArr });
					}
				}

				if (data.success && data.success.length > 0) {
					if (isPrint) {
						//打印并预览
						printerView(
							props,
							'/nccloud/gl/voucher/printNoPreview.do', //后台服务url
							{
								billtype: 'C0', //单据类型
								appcode: appcode, //功能节点编码，即模板编码
								nodekey: '',     //模板节点标识
								printTemplateID: printemp,
								oids: data.success, // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
								userjson: JSON.stringify(printdata),
								type:'1',
								totalPage:5,
								// ip:'10.11.117.15',
								// port:'80',
								//socketIp:'10.11.115.124',
								//download:'preview',
								//socketIp:'10.11.115.147',
								realData:"true"
							},
							false
						);
					} else {
						//预览
						printPreview(
							props,
							'/nccloud/gl/voucher/printNoPreview.do', //后台服务url
							{
								billtype: 'C0', //单据类型
								appcode: appcode, //功能节点编码，即模板编码
								nodekey: '',     //模板节点标识
								printTemplateID: printemp,
								oids: data.success, // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
								userjson: JSON.stringify(printdata),
								type:'1',
								totalPage:5,
								// ip:'10.11.117.15',
								// port:'80',
								//socketIp:'10.11.115.124',
								//download:'preview',
								//socketIp:'10.11.115.147',
								realData:"true"
							},
							false
						);
					}
				}
			}
		}
	});
	props.modal.close('print');
}

// function printAfterEvent(props,areacode,key,finalvalue,oldvalue,val){
//     if('printemp'==key){
//         let value=val.refcode;
//         let display=finalvalue.display;
//         props.form.setFormItemsValue(areacode,{'printemp':{display:display,value:value}})
//     }
// }

function closePrint() {
	this.props.modal.close('print');
}

/**凭证输出 */
function confrimOutPut(selectIndex) {
	let printmode = this.props.form.getFormItemsValue(areacode, 'printmode').value;
	if (printmode && selectIndex == '-1') {
		toast({ content: json['20021005list-000070'], color: 'warning' });/* 国际化处理： 请选择需要打印的分录！*/
		return;
	}
	let selectVoucher = this.props.ViewModel.getData('selectVoucher');
	if (selectVoucher.length == 0) {
		toast({ content: json['20021005list-000072'], color: 'warning' });/* 国际化处理： 请选择需要打印的凭证！*/
		return;
	}

	let printsubjlevel = this.props.form.getFormItemsValue(areacode, 'printsubjlevel').value;
	let printasslevel = this.props.form.getFormItemsValue(areacode, 'printasslevel').value;
	let printdata = {
		printsubjlevel: printsubjlevel, //printsubjlevel：科目汇总到第几级，不汇总是0
		printasslevel: printasslevel ? '-1' : '0', //printasslevel：按辅助项汇总 勾选-1 不勾选0
		printmode: printmode ? '1' : '0', //printmode：代账单格式 0不勾选 1勾选
		selectIndex: selectIndex //selectIndex: 选中第几行分录 从0开始
	};

	output({
		url: '/nccloud/gl/voucher/outputlist.do',
		data: {
			funcode: '20020PREPA', //小应用编码
			oids: selectVoucher,
			userjson: JSON.stringify(printdata)
		}
	});
}

export { printItem, createPrint, confirmPrint, closePrint, confrimOutPut ,printButton};
