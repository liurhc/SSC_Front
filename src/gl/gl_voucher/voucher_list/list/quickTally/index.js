import { ajax, toast ,cardCache} from 'nc-lightapp-front';
import './index.less';
let { setDefData } = cardCache;
import {dataSource} from '../../../../public/components/constJSON';
/**
 * 快速记账
 */
let areacode = 'quickTally';
function quickTallyMeta(appcode) {
	return {
		areastatus: 'edit',
		code: 'quickTally',
		moduletype: 'form',
		name: this.state.json['20021005list-000046'],/* 国际化处理： 快速记账*/
		status: 'edit',
		items: [
			{
				attrcode: 'pk_accountingbook',
				colnum: '1',
				color: '#6E6E77',
				isDataPowerEnable: true,
				itemtype: 'refer',
				label: this.state.json['20021005list-000001'],/* 国际化处理： 财务核算账簿*/
				maxlength: '20',
				position: '1',
				visible: true,
				refcode: 'uapbd/refer/org/AccountBookTreeRef/index',
				metadataProperty: 'uap.accountingbook.name',
				queryCondition: () => {
					return {
						TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
						appcode: appcode
					};
				}
			},
			{
				attrcode: 'period',
				colnum: '1',
				color: '#6E6E77',
				isDataPowerEnable: true,
				itemtype: 'refer',
				label: this.state.json['20021005list-000076'],/* 国际化处理： 会计期间*/
				maxlength: '20',
				position: '1',
				visible: true,
				refcode: 'uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef/index'
			}
		]
	};
}
function quicktallyItem(_this) {
	let { form } = _this.props;
	let { createForm } = form;
	return createForm(areacode, {
		onAfterEvent: onAfterEvent.bind(this)
	});
}

function onAfterEvent(props, areaId, itemId, finalValue, oldValue, val) {
	if ('pk_accountingbook' == itemId) {
		let pk_accountingbook = finalValue.value;
		if (pk_accountingbook && pk_accountingbook != '') {
			ajax({
				url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
				data: {
					pk_accountingbook: pk_accountingbook
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						let period = data.bizPeriod;
						props.form.setFormItemsValue(areacode, { period: { display: period, value: period } });
						let quicktally={
							pk_accountingbook:finalValue,
							period:{ display: period, value: period }
						}
						setDefData('quickTally', dataSource,quicktally);
						//设置会计期间参照过滤
						let pk_accperiodscheme = data.pk_accperiodscheme;
						let meta = props.meta.getMeta();
						meta[areaId].items.map((item, key) => {
							if (item.attrcode == 'period') {
								item.queryCondition = () => {
									return {
										pk_accperiodscheme: pk_accperiodscheme,
										GridRefActionExt: 'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
									};
								};
							}
						});
						props.meta.setMeta(meta);
					}
				}
			});
		} else {
			props.form.setFormItemsValue(areacode, { period: { display: '', value: '' } });
		}
	}
}

function quicktallyComfirm() {
	let pk_accountingbook = this.props.form.getFormItemsValue(areacode, 'pk_accountingbook').value;
	let yearmth = this.props.form.getFormItemsValue(areacode, 'period').display;
	if (!pk_accountingbook || pk_accountingbook == '' || !yearmth || yearmth == '') {
		toast({ content: this.state.json['20021005list-000121'], color: 'warning' });/* 国际化处理： 请选择账簿和期间！*/
		return;
	}
	ajax({
		url: '/nccloud/gl/voucher/quicktally.do',
		data: {
			pk_accountingbook: pk_accountingbook,
			year: yearmth.split('-')[0],
			period: yearmth.split('-')[1]
		},
		success: (res) => {
			let { success } = res;
			if (success) {
				toast({ content: this.state.json['20021005list-000122'], color: 'success', position: 'bottom' });/* 国际化处理： 快速记账成功！*/
			}
		}
	});
}

export { quicktallyItem, quicktallyComfirm, quickTallyMeta };
