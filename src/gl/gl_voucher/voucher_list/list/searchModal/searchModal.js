import React, { Component } from 'react';
import { high, base, ajax, promptBox, cardCache, getBusinessInfo } from 'nc-lightapp-front';
import {
	CheckboxItem,
	RadioItem,
	SelectItem
} from '../../../../public/components/FormItems';
import { toast } from 'nc-lightapp-front';
import createScript from '../../../../public/components/uapRefer.js';
import Explantion from '../../../../public/components/Explanation';
import { GetChinese, RemoveChinese } from '../../../../public/common/stringDeal.js';
import AccPeriodDefaultTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
const format = 'YYYY-MM-DD';
const timeFormat = 'YYYY-MM-DD HH:mm:ss';
import {localToEast8,east8ToLocal} from '../../../../public/common/dateUtil.js'
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	AutoComplete,
	NCDropdown: Dropdown,
	NCPanel: Panel,
	NCModal: Modal,
	NCForm,
	NCRangePicker: RangePicker,
	NCDiv
} = base;
import {
	dataSource,
	dataSourceTable,
	dataSourceHeightQuery,
	tableId,
	dataSourceNormal,
	dataSourceHeight
} from '../../../../public/components/constJSON';
import './index.less';
//const { NCRangePicker:RangePicker } = DatePicker;
const { NCFormItem: FormItem } = NCForm;
const columns101 = [];
const data12 = [
	{
		key: 1,
		checkedNumber: false,
		isEdit: true,
		excrate2: {
			value: ''
		},
		excrate3: {
			value: ''
		},
		excrate4: {
			value: ''
		},
		childform: []
	}
];
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};
const config = {
	"isDataPowerEnable": 'Y',
	"DataPowerOperationCode": 'fi'
};
let { setDefData, getDefData } = cardCache;

/* 检测类型是否为数组 */
export function isArray(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'Array';
}
export default class SearchModal extends Component {
	constructor(props) {
		super(props);


		this.state = {
			json: props.json,
			loadData: [], //查询模板加载数据
			listItem: {}, //模板数据对应值
			presetSchemeItem:{},//预置方案
			dataRows: [], //辅助核算列表数据
			commonDate: getBusinessInfo().businessDate.split(' ')[0],
			pk_accperiodscheme: '',
			queryAccount: '',
			unitOrg: '',
			accountingbook_org: '',//核算账簿对应的组织
			queryCondition: {
				pk_accountingbook: {
					value: ''
				}
			},
			SelectedAssData: [],//选中的数据
			checkedAll: false,//辅助项默认不选中
			checkedArray: [],//辅助项选中状态
			columns10: this.columns10,//渲染辅助核算项
			selectPretype: '0', //默认选择会计期间
			preAble: false,
			hideUnit: true, //控制业务单元显隐
			dataAble: true,
			checkQuery: false,
			selectDatetype: '', //日期选中控制
		};
		this.clear = this.clear.bind(this)
	}

	componentWillMount() { }

	componentWillReceiveProps(nextProp) {
		let self = this;
		let { listItem, presetSchemeItem,loadData, queryAccount, queryCondition, commonDate, dataRows,checkedAll,checkedArray, selectPretype, selectDatetype, preAble, dataAble } = self.state;
		commonDate = getBusinessInfo().businessDate.split(' ')[0];
		// queryCondition.pk_accountingbook.value=nextProp.queryValue.pk_accountingbook.value;
		if(nextProp.queryValue&&nextProp.queryValue.presetScheme){
			// loadData=[];
			// this.clear();
			self.setState({
				listItem:presetSchemeItem
			},()=>{
				if(presetSchemeItem.pk_accountingbook&&presetSchemeItem.pk_accountingbook[0].value){
					self.queryListAccount({ pk_accountingbook: presetSchemeItem.pk_accountingbook[0].value });
					self.queryBookCombineInfo([{ "refpk": presetSchemeItem.pk_accountingbook[0].value }]);
				}
			})
		}else{
			if (nextProp.hideOrShow) {
				if (loadData.length == 0) {
					nextProp.loadData.forEach((item, i) => {
						let key = '';
						if (item.itemType == 'refer') {
							if (item.itemKey == 'period') {
								key = [];
							} else if (item.itemKey == 'pk_accountingbook' || item.itemKey == 'pk_unit' || item.itemKey == 'explanation' ||
								item.itemKey == 'oppositesubj' || item.itemKey == 'accountingcode' || item.itemKey == 'pk_currtype' || item.itemKey == 'checkstyle') {
								key = [{
									display: '',
									value: ''
								}];
							} else {
								key = {
									display: '',
									value: ''
								};
							}
						} else {
							if (item.itemType == 'mix') {
								key = {
									direction: '', //0=双向，1=借，2=贷
									firstAmount: '',
									andOr: '',
									secondAmount: ''
								};
							}if(item.itemType=='checkbox'&&item.itemKey=='gl_Checkbox'){
								item.itemChild.map((k,index)=>{
									listItem[k.itemKey]=k.checked;
								})
							} else {
								key = {
									display: '',
									value: ''
								};
							}
						}

						//区间字段特殊处理
						listItem.prepareddate_start = '';
						listItem.prepareddate_end = '';
						listItem.period_start = '';
						listItem.period_end = '';
						listItem.attachment_start = '';
						listItem.attachment_end = '';
						listItem.num_start = '';
						listItem.num_end = '';

						//下拉初始化默认值
						let defautItem = item.itemKey;
						switch (defautItem) {
							case 'voucherState':
								if (nextProp.voucherState == 'sign') {//签字
									key = '8'
								} else if (nextProp.voucherState == 'check') {//审核
									key = '5';
								} else if (nextProp.voucherState == 'tally') {//记账
									key = '2';
								} else {
									key = '0';
								}

								break;
							case 'convertState':
								key = '0';
								break;
							case 'convertSource':
								key = '0';
								break;
							case 'difflag':
								key = '0';
								break;
							case 'convertSource':
								key = '0';
								break;
							case 'amountType':
								key = '1';
								break;
							case 'oriamount':
								key.direction = '0';
								key.andOr = 'and';
								break;
							case 'amount':
								key.direction = '0';
								key.andOr = 'and';
								break;
							case 'quantity':
								key.direction = '0';
								key.andOr = 'and';
								break;
							case 'checkno':
								key.direction = '0';
								key.andOr = 'and';
								break;
							case 'checkdate':
								key.direction = '0';
								key.andOr = 'and';
								break;
							case 'offervoucher':
								key = '0';
								break;
							case 'period':
								key = [];
								break;
							case 'pk_accountingbook':
								key = [{
									display: nextProp.queryValue ? nextProp.queryValue.pk_accountingbook.display : '',
									value: nextProp.queryValue ? nextProp.queryValue.pk_accountingbook.value : ''
								}];
								break;
							default:
								// key = {
								// 	display: '',
								// 	value: ''
								// };
								break;
						}
						listItem[item.itemKey] = key;
					});

					let cacheListItem = getDefData('heightQuery', dataSourceHeightQuery);
					if (cacheListItem) {
						let cachePk_accountingbook = cacheListItem.pk_accountingbook[0].refpk ? cacheListItem.pk_accountingbook[0].refpk : cacheListItem.pk_accountingbook[0].value;
						queryCondition.pk_accountingbook.value = cachePk_accountingbook;
						queryAccount=cachePk_accountingbook;
						self.queryBookCombineInfo([{ "refpk": cachePk_accountingbook }]);
						if (cacheListItem.dataRows && cacheListItem.dataRows.length > 0) {
							dataRows = cacheListItem.dataRows;
							self.queryListAccount({ pk_accountingbook: cachePk_accountingbook }, dataRows, cacheListItem);
						} else {
							self.queryListAccount({ pk_accountingbook: cachePk_accountingbook }, '', cacheListItem);
						}
						selectPretype = cacheListItem.selectPretype;
						if (selectPretype == '') {
							preAble = true;
						}
						selectDatetype = cacheListItem.selectDatetype;
						if (selectDatetype != '') {
							dataAble = false;
						}
						presetSchemeItem=JSON.parse(JSON.stringify(listItem));
						self.setState({
							selectPretype, selectDatetype, preAble, dataAble,
							loadData: nextProp.loadData,
							listItem: cacheListItem,presetSchemeItem,
							commonDate, queryCondition, dataRows,
							queryAccount
						})
					} else {
						queryCondition.pk_accountingbook.value = nextProp.queryValue.pk_accountingbook.value;
						if (nextProp.queryValue.pk_accountingbook.value) {
							queryAccount=nextProp.queryValue.pk_accountingbook.value;
							self.queryListAccount({ pk_accountingbook: nextProp.queryValue.pk_accountingbook.value });
							self.queryBookCombineInfo([{ "refpk": nextProp.queryValue.pk_accountingbook.value }]);
						}
						presetSchemeItem=JSON.parse(JSON.stringify(listItem));
						self.setState(
							{
								loadData: nextProp.loadData,
								listItem, commonDate, queryCondition,
								presetSchemeItem,
								queryAccount
							}
						);
					}

				} else {
					if (nextProp.queryValue && nextProp.queryValue.pk_accountingbook) {
						for (let e in nextProp.queryValue) {
							if (nextProp.queryValue[e] instanceof Array && e != 'start_data' && e != 'period') {
							}
						}
						let propValue;
						if (nextProp.queryValue.pk_accountingbook.display instanceof Array && nextProp.queryValue.pk_accountingbook.display.length > 0) {
							propValue = nextProp.queryValue.pk_accountingbook.display[0].refpk;
						} else {
							propValue = nextProp.queryValue.pk_accountingbook.value||(nextProp.queryValue.pk_accountingbook.length?(nextProp.queryValue.pk_accountingbook[0].value||nextProp.queryValue.pk_accountingbook[0].refpk):'');
						}
						if (propValue != queryAccount) {
							if(nextProp.searchId == 'pullgetsearch'){
								listItem.pk_accountingbook=[{display:nextProp.queryValue.pk_accountingbook.display,value:nextProp.queryValue.pk_accountingbook.value}];
							}
								//根据查询方案加载辅助核算
							self.setState(
								{
									listItem,
									queryAccount: propValue,
									commonDate
								},
								() => {
									let data = { pk_accountingbook: propValue };
									if (data.pk_accountingbook) {
										self.queryListAccount(data);
									}
								}
							);
						}
						if (nextProp.queryValue.pk_accountingbook.length >= 0&&nextProp.clickPlanEve) {//点击了查询方案走这
							listItem = nextProp.queryValue;
							dataRows=nextProp.queryValue.emptyDataRows;//取查询方案里保存的辅助核算
							checkedAll=nextProp.queryValue.checkedAll||false;
							checkedArray=nextProp.queryValue.checkedArray||[];
							self.setState({
								listItem,dataRows,checkedAll,checkedArray
							});
						}
					} else {
						self.setState({
							commonDate
						})
					}
				}
			}
		}
	}

	//  shouldComponentUpdate(nextProps, nextState) {
	//      if(nextState.loadData.length!=this.state.loadData.length){
	//          return true
	//      }
	//      if(JSON.stringify(nextState.listItem)!="{}"){
	//         return true
	//     }
	//      return false
	//  }

	componentDidMount() {
		let { columns10 } = this.state;
		this.props.onRef(this);
		let data1 = [
			{
				itemtype: 'refer',
				col: '4',
				visible: true,
				label: this.state.json['20021005list-000038'],/* 国际化处理： 核算内容*/
				maxlength: '2',
				refcode: '../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef/index.js',
				attrcode: 'pk_accasoa',
				key: 1
			}
		];
	}
	//表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function (v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}
	//供父组件调用此方法，获取查询条件和辅助核算项
	confirm = () => {
		let { listItem, dataRows, checkedAll,checkedArray, SelectedAssData, selectPretype, selectDatetype } = this.state;
		SelectedAssData = [];
		let flag=false;
		if(JSON.stringify(listItem)!='{}'){
			if (listItem.pk_accountingbook.length == 0 || (!listItem.pk_accountingbook[0].refpk && !listItem.pk_accountingbook[0].value)) {
				flag=true;
				toast({ content: this.state.json['20021005list-000129'], color: 'warning' });/* 国际化处理： 请选择核算账簿，核算账簿不能为空*/
			}
			if (!flag&&selectPretype == '0' && (!listItem.period_start.refpk || !listItem.period_end.refpk)) {//会计期间必输
				flag=true;
				toast({ content: this.state.json['20021005list-000158'], color: 'warning' });/* 国际化处理： 请选择会计期间，会计期间不能为空*/
			}
			if (!flag&&selectDatetype == '1' && (!listItem.prepareddate_start || !listItem.prepareddate_end)) {//日期必输
				flag=true;
				toast({ content: this.state.json['20021005list-000159'], color: 'warning' });/* 国际化处理： 请选择日期，日期不能为空*/
			}
			for (var i = 0; i < checkedArray.length; i++) {
				if (checkedArray[i] == true) {
					SelectedAssData.push(dataRows[i]);
				}
			}
			listItem.dataRows = SelectedAssData;//dataRows;//辅助核算信息
			listItem.emptyDataRows = dataRows;
			listItem.checkedAll=checkedAll;
			listItem.checkedArray=checkedArray;
		}
		// this.setState(
		// 	{
		// 		showModal: false
		// 	});
		listItem.errorFlag=flag;//报错标识
		return listItem;
	};

	queryListAccount = (dataCount, cacheDataRows, cacheListItem,flag) => {
		let preLineAss = {};
		ajax({
			url: '/nccloud/gl/voucher/queryAllAssItem.do',
			data: dataCount,
			success: (res) => {
				let {
					loadData,
					dataRows,
					queryCondition,
					pk_accperiodscheme,
					commonDate,
					listItem,
					unitOrg, accountingbook_org,
					hideUnit, checkedArray,checkedAll
				} = this.state;
				checkedArray=[];
				checkedAll=false;
				let { success, data } = res;
				if (success) {
					// dataRows=data;
					if (data.assvo && data.assvo.length > 0) {
						if (cacheDataRows) {
							cacheDataRows.forEach((ass) => {
								preLineAss[ass.classid] = {
									pk_Checkvalue: ass.pk_Checkvalue,
									checkvaluecode: ass.checkvaluecode,
									checkvaluename: ass.checkvaluename
								};
							});
						}
						data.assvo.map((item, i) => {
							checkedArray.push(false);
							item.key = ++i;
							item.value = {
								display: '',
								value: ''
							};
							if (cacheDataRows && preLineAss.hasOwnProperty(item.classid)) {
								item.pk_Checkvalue = preLineAss[item.classid].pk_Checkvalue;
								item.checkvaluecode = preLineAss[item.classid].checkvaluecode;
								item.checkvaluename = preLineAss[item.classid].checkvaluename;
							}
						});
					}
					queryCondition.pk_accountingbook.value = dataCount.pk_accountingbook;
					pk_accperiodscheme = data.pk_accperiodscheme;
					commonDate = data.bizDate;
					if (!cacheListItem) {//没有缓存高级查询条件的话取默认日期
						//默认会计期间和日期赋值
						listItem.period_start = { refname: data.bizPeriod, refpk: data.bizPeriod };
						listItem.period_end = { refname: data.bizPeriod, refpk: data.bizPeriod };
						listItem.prepareddate_start = data.begindate;
						listItem.prepareddate_end = data.enddate;
					}
					// if (data.isShowUnit) {
					hideUnit = false;
					unitOrg = data.unit.value;
					accountingbook_org = data.unit.value;
					// }

					if (data.NC001 || data.NC002) {
						//集团全局radio 可用性控制
						loadData.map((item, i) => {
							if (item.itemType == 'radio') {
								if (data.NC001) {
									item.itemChild.map((e, k) => {
										if (e.value == '2') {
											e.disabled = false;
										}
									});
								}
								if (data.NC002) {
									item.itemChild.map((e, k) => {
										if (e.value == '3') {
											e.disabled = false;
										}
									});
								}
							}
						});
					}
					this.setState(
						{
							loadData,
							dataRows: data.assvo ? data.assvo : [],
							pk_accperiodscheme,
							commonDate,
							listItem,
							unitOrg, accountingbook_org,
							queryCondition,
							hideUnit,checkedArray,checkedAll
						});
				}
			}
		});
	};
	queryBookCombineInfo = (value) => {
		let pk_accpont = {
			"pk_accountingbook": value[0] && value[0].refpk
		};
		ajax({
			url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
			data: pk_accpont,
			success: (response) => {
				let { data } = response;

				this.setState({
					isShowUnit: data.isShowUnit,
					unitValueParam: data.unit.value
				})
			}
		})
	}
	clear = () => {                     //会计平台使用
		//清空高级查询条件
		let { listItem, dataRows } = this.state;
		for (let k in listItem) {
			if (listItem[k] && typeof (listItem[k]) == 'object') {//对象
				if (listItem[k].constructor === Array || (listItem[k] instanceof Array)) {//数组
					listItem[k] = [{ "display": "", "value": "" }];
				}else {
					if (listItem[k].direction) {
						listItem[k] = {
							direction: '', //0=双向，1=借，2=贷
							firstAmount: '',
							andOr: '',
							secondAmount: ''
						}
					} else {
						listItem[k] = { "display": "", "value": "" };
					}

				}
			} else {
				listItem[k] = '';
			}
			if(k=='discardflag'||k=='errorflag'||k=='normalflag'||k=='tempsaveflag'){//凭证类别默认选中
				listItem[k]=true;
			}
		}
		dataRows = [];
		this.setState({ listItem, dataRows });
	}
	queryList = (data) => {
		let self = this;
		let {
			listItem,
			queryCondition,
			unitOrg, accountingbook_org,
			pk_accperiodscheme,
			commonDate,
			selectPretype,
			selectDatetype,
			preAble,
			dataAble,
			hideUnit, dataRows
		} = self.state;
		let { queryValue } = self.props;
		let mixcount = 0; // 样式用
		return data.length != 0 ? (
			data.map((item, i) => {
				let defaultValue;
				let DBValue = [];
				if (item.isMultiSelectedEnabled) {
					if (item.itemKey == 'pk_accountingbook' || item.itemKey == 'pk_unit' || item.itemKey == 'explanation' ||
						item.itemKey == 'oppositesubj' || item.itemKey == 'pk_currtype' || item.itemKey == 'checkstyle') {
						if (Array.isArray(listItem.pk_accountingbook)) {       //防止报错
							listItem[item.itemKey].map((item, index) => {
								DBValue[index] = { refname: item.refname ? item.refname : item.display, refpk: item.refpk ? item.refpk : item.value };
							})
						} else {
							DBValue = { refname: item.refname ? item.refname : item.display, refpk: item.refpk ? item.refpk : item.value };
						}
						defaultValue = DBValue;
					} else if (item.itemKey == 'accountingcode') {
						if (Array.isArray(listItem.pk_accountingbook)) {
							listItem[item.itemKey].map((item, index) => {
								item.refname = item.refnamecode;
								DBValue[index] = item;
							})
						} else {
							DBValue = item
						}
						defaultValue = DBValue;
					} else if (listItem[item.itemKey] && listItem[item.itemKey].display instanceof Array) {
						defaultValue = listItem[item.itemKey].display;
					} else {
						defaultValue = [
							{
								refname: listItem[item.itemKey] ? listItem[item.itemKey].display : '',
								refpk: listItem[item.itemKey] ? listItem[item.itemKey].value : ''
							}
						];
					}
				} else {
					defaultValue = {
						refname: listItem[item.itemKey] ? listItem[item.itemKey].display : '',
						refpk: listItem[item.itemKey] ? listItem[item.itemKey].value : ''
					};
				}
				switch (item.itemType) {
					case 'refer':
						if (item.itemKey == 'period') {
							//会计期间区间特殊处理
							return (
								<div className="period u-col-md-12 u-col-sm-12 u-col-xs-12 nc-theme-pop-header-bgc nc-theme-area-split-bc">
									<div class="u-col-md-2 u-col-sm-2 u-col-xs-2">
										<label class="u-label nc-theme-common-font-c">{this.state.json['20021005list-000132']/* 国际化处理： 期间或日期*/}</label>
									</div>
									<div class="u-col-md-3 u-col-sm-3 u-col-xs-3">
										<NCRadio.NCRadioGroup
											fieldid="period"
											selectedValue={selectPretype}
											onChange={(v) => {
												selectPretype = v;
												selectDatetype = '';
												preAble = false;
												dataAble = true;
												listItem.selectPretype = v;
												listItem.selectDatetype = '';
												this.setState({
													listItem,
													selectPretype,
													selectDatetype,
													preAble,
													dataAble
												});
											}}
										>
											<NCRadio value="0">{this.state.json['20021005list-000076']/* 国际化处理： 会计期间*/}</NCRadio>
										</NCRadio.NCRadioGroup>
									</div>
									<div class="u-col-md-4 u-col-sm-4 u-col-xs-4">
										<AccPeriodDefaultTreeGridRef
											fieldid="AccPeriodDefaultTreeGridRef_start"
											disabled={preAble}
											value={listItem[item.itemKey + '_start']}
											queryCondition={{
												// GridRefActionExt: 'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder', //过滤A的时间
												pk_accperiodscheme: this.state.pk_accperiodscheme
											}}
											onChange={(v) => {
												listItem[item.itemKey + '_start'] = v;
												if (v && v.values && v.values.begindate) {
													listItem['prepareddate_start'] = v.values.begindate.value; //期间赋值给日历
												} else {
													listItem['prepareddate_start'] = '';
												}

												this.setState(
													{
														listItem
													});
											}}
										/>
									</div>
									<span className="to nc-theme-common-font-c">~</span>
									<div class="u-col-md-4 u-col-sm-4 u-col-xs-4">
										<AccPeriodDefaultTreeGridRef
											fieldid="AccPeriodDefaultTreeGridRef_end"
											disabled={preAble}
											value={listItem[item.itemKey + '_end']}
											queryCondition={{
												"includeAdj": "Y",//需要显示调整期
												pk_accperiodscheme: this.state.pk_accperiodscheme
											}}
											onChange={(v) => {
												listItem[item.itemKey + '_end'] = v;
												if (v && v.values && v.values.enddate && v.values.enddate.value) {
													listItem['prepareddate_end'] = v.values.enddate.value; //期间赋值给日历
												} else {//调整期
													if (v && v.refname) {
														let url = '/nccloud/gl/glpub/queryDateByPeriod.do';
														let data = {
															pk_accountingbook: listItem['pk_accountingbook'][0].value ? listItem['pk_accountingbook'][0].value : listItem['pk_accountingbook'][0].refpk,
															period: v.refname
														}
														ajax({
															url,
															data,
															success: (response) => {
																let { success, data } = response;
																if (success) {
																	listItem['prepareddate_end'] = data.enddate;
																	this.setState({
																		listItem
																	})
																}
															},
															error: (error) => {
																toast({ content: error.message, color: 'warning' })
															}
														})
													} else {
														listItem['prepareddate_end'] = '';
													}
												}

												this.setState(
													{
														listItem
													});
											}}
										/>
									</div>
								</div>
							);
						} else {

							if (item.itemKey != 'pk_unit') {
								let referUrl = item.config.refCode + '/index.js';
								if (!self.state[item.itemKey]) {
									{
										createScript.call(self, referUrl, item.itemKey);
									}
								} else {
									return (
										<div className="nomal-item">
											<div class="u-col-md-2 u-col-sm-2 u-col-xs-2">
												<label class="u-label nc-theme-common-font-c">{item.showMust ? <span class='u-must'>*</span> : <span />}
													{item.itemName}</label>
											</div>
											<div class="u-col-md-4 u-col-sm-4 u-col-xs-4">
												{self.state[item.itemKey] ? (
													self.state[item.itemKey]({
														isMultiSelectedEnabled: item.isMultiSelectedEnabled,
														value: defaultValue,
														// disabledDataShow:true,
														// value:{refname:'法人公司-0000,用友股份-美元账簿类型',refpk:"1001A3100000000008MT,1001Z310000000008X05"},
														queryCondition: () => {
															if (item.itemKey == 'pk_vouchertype') {
																//凭证类别
																return {
																	GridRefActionExt:
																		'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
																	isDataPowerEnable: 'Y',
																	DataPowerOperationCode: 'fi',
																	pk_org: queryCondition.pk_accountingbook.value
																};
															} else if (item.itemKey === 'pk_accountingbook') {//财务核算账簿
																if (this.props.searchId == 'pullgetsearch') {//会计平台拉式取数的话，账簿就不加过滤，fuqi说要加
																	return {
																		"isDataPowerEnable": "Y",
																		"TreeRefActionExt":"nccloud.web.gl.ref.AccountBookRefSqlBuilder",
																		"appcode": "20020PREPA",
																		"isShowDisabledData": "Y",
																		"scene":'fip'
																	};
																} else {
																	return {
																		"isDataPowerEnable": "Y",
																		"TreeRefActionExt": "nccloud.web.gl.ref.AccountBookRefSqlBuilder",
																		"appcode": "20020PREPA",
																		"isShowDisabledData": "Y"
																	};
																}
															} else if (item.itemKey == 'pk_system') {
																//制单系统
																return {
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value,
																	// showAllSys: 'Y',
																	isDataPowerEnable: 'Y',
																	DataPowerOperationCode: 'fi'
																};
															} else if (item.itemKey == 'pk_unit') {
																//业务单元
																return {
																	isDataPowerEnable: 'Y',
																	DataPowerOperationCode: 'fi',
																	VersionStartDate: commonDate, //当前业务日期
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value,
																	TreeRefActionExt:
																		'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
																};
															} else if (item.itemKey == 'oppositesubj') {
																//对方科目
																return {
																	isMultiSelectedEnabled: true,
																	isShowHighFilter: false,
																	isDataPowerEnable: 'Y',
																	DataPowerOperationCode: 'fi',
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value,
																	dateStr: commonDate
																};
															} else if (item.itemKey == 'accountingcode') {
																//科目编码
																return {
																	isMultiSelectedEnabled: true,
																	isShowHighFilter: false,
																	isDataPowerEnable: 'Y',
																	DataPowerOperationCode: 'fi',
																	dateStr: commonDate, //当前业务日期
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value
																};
															} else if (item.itemKey == 'pk_currtype') {
																//币种
																return {
																	isDataPowerEnable: 'Y',
																	DataPowerOperationCode: 'fi',
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value
																};
															} else if (item.itemKey == 'explanation') {
																//摘要
																return {
																	isDataPowerEnable: 'Y',
																	DataPowerOperationCode: 'fi',
																	"pk_org": self.state.accountingbook_org,
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value
																};
															} else if (item.itemKey == 'pk_casher') {
																//出纳人
																return {
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value,
																	user_Type: '2',
																	GridRefActionExt:
																		'nccloud.web.gl.ref.OperatorRefSqlBuilder'
																};
															} else if (item.itemKey == 'pk_checked') {
																//审核人
																return {
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value,
																	user_Type: '1',
																	GridRefActionExt:
																		'nccloud.web.gl.ref.OperatorRefSqlBuilder'
																};
															} else if (item.itemKey == 'pk_manager') {
																//记账人
																return {
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value,
																	user_Type: '3',
																	GridRefActionExt:
																		'nccloud.web.gl.ref.OperatorRefSqlBuilder'
																};
															} else if (item.itemKey == 'pk_prepared') {
																//制单人
																return {
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value,
																	user_Type: '0',
																	GridRefActionExt:
																		'nccloud.web.gl.ref.OperatorRefSqlBuilder'
																};
															}
														},

														disabledDataShow:
															item.itemKey == 'accountingcode' || item.itemKey == 'pk_accountingbook' ||
																item.itemKey == 'oppositesubj' ||
																item.itemKey == 'pk_vouchertype'
																? true
																: false,
														onFocus: (v) => {
															if (item.itemKey == 'accountingcode' || item.itemKey == 'oppositesubj') {
																if (listItem.pk_accountingbook.length == 0) {
																	toast({ content: this.state.json['20021005list-000130'], color: 'warning' });/* 国际化处理： 请先选择核算账簿，核算账簿不能为空*/
																	return false;
																} else {
																	if (!listItem.pk_accountingbook[0].value && !listItem.pk_accountingbook[0].refpk) {
																		toast({ content: this.state.json['20021005list-000130'], color: 'warning' });/* 国际化处理： 请先选择核算账簿，核算账簿不能为空*/
																		return false;
																	}
																}
															}
														},
														onChange: (v) => {
															// if (v.length == 0) {
															// 	promptBox({
															// 		color: 'warning',
															// 		content: '必须选择核算账簿,请输入核算账簿！'
															// 	});
															// }

															if (v && v.length != 0) {
																if (item.itemKey == 'pk_accountingbook') {
																	let data = { pk_accountingbook: v[0].refpk };
																	listItem['pk_unit'] = [{ display: '', value: '' }];
																	this.queryListAccount(data);
																	this.queryBookCombineInfo(v)
																}
															}
															if (item.itemKey == 'accountingcode') {//科目编码 显示编码
																if (item.isMultiSelectedEnabled) {
																	v.map((item, index) => {
																		// item.refname=item.refcode;
																		item.refnamecode = item.refcode;
																	})
																	listItem[item.itemKey] = v;
																} else {
																	listItem[item.itemKey].value = v.refpk;
																	listItem[item.itemKey].display = v.refcode;
																}
															} else if (item.itemKey == 'pk_system') {//制单系统
																if (item.isMultiSelectedEnabled) {
																	v.refpk = v.refcode;
																	listItem[item.itemKey] = v;
																} else {
																	listItem[item.itemKey].value = v.refcode;
																	listItem[item.itemKey].display = v.refname;
																}
															} else {
																if (item.isMultiSelectedEnabled) {
																	listItem[item.itemKey] = v;
																} else {
																	listItem[item.itemKey].value = v.refpk;
																	listItem[item.itemKey].display = v.refname;
																}
															}
															self.setState({
																listItem
															});
														}
													})
												) : (
														<div />
													)}
											</div>
										</div>
									);
								}
							} else {
								if (this.state.isShowUnit) {
									let referUrl = item.config.refCode + '/index.js';
									if (!self.state[item.itemKey]) {
										{
											createScript.call(self, referUrl, item.itemKey);
										}
									} else {
										return (
											<div className="nomal-item">
												<div class="u-col-md-2 u-col-sm-2 u-col-xs-2">
													<label class="u-label nc-theme-common-font-c">

														{item.itemName}
													</label>
												</div>
												<div class="u-col-md-4 u-col-sm-4 u-col-xs-4">
													{self.state[item.itemKey] ? (
														self.state[item.itemKey]({
															isMultiSelectedEnabled: item.isMultiSelectedEnabled,
															value: defaultValue,
															// value:{refname:'法人公司-0000,用友股份-美元账簿类型',refpk:"1001A3100000000008MT,1001Z310000000008X05"},
															queryCondition: () => {
																//业务单元
																return {
																	isDataPowerEnable: 'Y',
																	DataPowerOperationCode: 'fi',
																	VersionStartDate: commonDate, //当前业务日期
																	pk_accountingbook:
																		queryCondition.pk_accountingbook.value,
																	TreeRefActionExt:
																		'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
																};
															},

															disabledDataShow: true,

															onChange: (v) => {
																if (item.isMultiSelectedEnabled) {
																	listItem[item.itemKey] = v;
																	if (v.length > 0) {
																		unitOrg = v[0].refpk;
																	} else {
																		unitOrg = accountingbook_org;
																	}

																} else {
																	listItem[item.itemKey].value = v.refpk;
																	listItem[item.itemKey].display = v.refname;
																	if (v.refpk) {
																		unitOrg = v.refpk;
																	} else {
																		unitOrg = accountingbook_org;
																	}
																}
																//辅助核算项的值
																dataRows.map((item, index) => {
																	item.checkvaluename = null;
																	item.pk_Checkvalue = null;
																	item.checkvaluecode = null;
																})
																self.setState(
																	{
																		listItem, unitOrg, dataRows
																	});
																// }
															}
														})
													) : (
															<div />
														)}
												</div>
											</div>
										);
									}
								}
							}


						}
						break;
					case 'date':
						return (
							<div className="date u-col-md-12 u-col-sm-12 u-col-xs-12 nc-theme-pop-header-bgc nc-theme-area-split-bc">
								<div class="u-col-md-2 u-col-sm-2 u-col-xs-2" />
								<div class="u-col-md-3 u-col-sm-3 u-col-xs-3">
									<NCRadio.NCRadioGroup
										selectedValue={selectDatetype}
										onChange={(v) => {
											selectDatetype = v;
											selectPretype = '';
											dataAble = false;
											preAble = true;
											listItem.selectPretype = '';
											listItem.selectDatetype = v;
											this.setState({
												listItem,
												selectDatetype,
												selectPretype,
												dataAble,
												preAble
											});
										}}
									>
										<NCRadio value="1">{item.itemName}</NCRadio>
									</NCRadio.NCRadioGroup>
								</div>
								<div class="u-col-md-4 u-col-sm-4 u-col-xs-4">
									<DatePicker
										isRequire={true}
										disabled={dataAble}
										placeholder={this.state.json['20021005list-000123']}/* 国际化处理： 选择日期*/
										value={listItem[item.itemKey + '_start']}
										onChange={(v) => {
											if (v) {
												listItem[item.itemKey + '_start'] = v;
											}
											self.setState(
												{
													listItem
												});
										}}
									/>
								</div>
								<span className="to nc-theme-common-font-c">~</span>
								<div class="u-col-md-4 u-col-sm-4 u-col-xs-4">
									<DatePicker
										isRequire={true}
										disabled={dataAble}
										placeholder={this.state.json['20021005list-000123']}/* 国际化处理： 选择日期*/
										value={listItem[item.itemKey + '_end']}
										onChange={(v) => {
											if (v) {
												listItem[item.itemKey + '_end'] = v;
											}
											self.setState(
												{
													listItem
												});
										}}
									/>
								</div>
							</div>
						);
						break;
					case 'textInput':
						if (item.itemKey == 'attachment' || item.itemKey == 'num') {
							return (
								<div class="u-col-md-6 u-col-sm-6 u-col-xs-6">
									<div class="u-col-md-4 u-col-sm-4 u-col-xs-4">
										<label class="u-label nc-theme-common-font-c">{item.itemName}</label>
									</div>
									<div class="u-col-md-4 u-col-sm-4 u-col-xs-4 ml-15">
										<NCNumber
											value={listItem[item.itemKey + '_start']}
											onChange={(v) => {
												listItem[item.itemKey + '_start'] = v;
												this.setState(
													{
														listItem
													});
											}}
										/>
									</div>
									<span className="to nc-theme-common-font-c">~</span>

									<div class="u-col-md-4 u-col-sm-4 u-col-xs-4">
										<NCNumber
											value={listItem[item.itemKey + '_end']}
											onChange={(v) => {
												listItem[item.itemKey + '_end'] = v;
												this.setState(
													{
														listItem
													});
											}}
										/>
									</div>
								</div>
							);
						} else {
							return (
								<div>
									<div class="u-col-md-2 u-col-sm-2 u-col-xs-2">
										<label class="u-label nc-theme-common-font-c">{item.itemName}</label>
									</div>
									<div class="u-col-md-4 u-col-sm-4 u-col-xs-4">
										<NCNumber
											value={listItem[item.itemKey]}
											onChange={(v) => {
												listItem[item.itemKey] = v;
												this.setState(
													{
														listItem
													});
											}}
										/>
									</div>
								</div>
							);
						}
						break;

					case 'select':
						return (
							<div>
								<div class="u-col-md-2 u-col-sm-2 u-col-xs-2">
									<label class="u-label nc-theme-common-font-c">{item.itemName}</label>
								</div>
								<div class="u-col-md-4 u-col-sm-4 u-col-xs-4">
									<SelectItem
										name={item.itemKey}
										defaultValue={listItem[item.itemKey] ? listItem[item.itemKey] : ''}
										items={() => {
											return item.itemChild;
										}}
										onChange={(v) => {
											listItem[item.itemKey] = v;
											this.setState({
												listItem
											});
										}}
									/>
								</div>
							</div>
						);
						break;
					case 'checkbox':
						if(item.itemKey=='gl_Checkbox'){
							item.itemChild.map((k,index)=>{
								k.checked=(listItem[k.itemKey]!=undefined&&listItem[k.itemKey]!='undefined')?listItem[k.itemKey]:k.checked;
							})
						}
						return (
							<div className="u-col-md-12 u-col-sm-12 u-col-xs-12 checkbox nc-theme-pop-header-bgc nc-theme-area-split-bc">
								<div class="u-col-md-2 u-col-sm-2 u-col-xs-2">
									<label class="u-label nc-theme-common-font-c">{item.itemName}</label>
								</div>
								<CheckboxItem
									defaultValue={listItem[item.itemKey].value} 
									boxs={() => {
										return item.itemChild;
									}}
									onChange={(v) => {
										v.map((item, index) => {
											listItem[item.itemKey] = item.checked;
										});
										this.setState(
											{
												listItem
											});
									}}
								/>
							</div>
						);
					case 'radio':
						return (
							<div className="u-col-md-12 u-col-sm-12 u-col-xs-12 radio nc-theme-pop-header-bgc nc-theme-area-split-bc">
								<div class="u-col-md-2 u-col-sm-2 u-col-xs-2">
									<label class="u-label nc-theme-common-font-c">{item.itemName}</label>
								</div>
								<RadioItem
									name={item.itemKey}
									type="customer"
									defaultValue={listItem[item.itemKey] ? listItem[item.itemKey] : '1'}
									items={() => {
										return item.itemChild;
									}}
									onChange={(v) => {
										listItem[item.itemKey] = v;
										this.setState({
											listItem
										});
									}}
								/>
							</div>
						);
						break;
					case 'mix':
						++mixcount;
						return (
							<div className={`u-col-md-12 u-col-sm-12 u-col-xs-12 mix mix${mixcount} nc-theme-pop-header-bgc nc-theme-area-split-bc`}>
								{item.itemKey != 'checkno' && item.itemKey != 'checkdate' ?//非票据号票据
									<div className="u-col-md-2 u-col-sm-2 u-col-xs-2">
										<SelectItem
											name={item.itemKey}
											defaultValue={listItem[item.itemKey] ? listItem[item.itemKey].direction : ' '}
											items={() => {
												return item.itemChildOne;
											}}
											onChange={(v) => {
												listItem[item.itemKey].direction = v;

												this.setState({
													listItem
												});
											}}
										/>
									</div>
									: ''}
								<div className="u-col-md-2 u-col-sm-2 u-col-xs-2 text-center">
									<label class="u-label nc-theme-common-font-c">{item.itemName}>=</label>
								</div>
								<div className="u-col-md-2 u-col-sm-2 u-col-xs-2 mr-10">
									{item.itemKey == 'checkno' ?
										<FormControl
											value={listItem[item.itemKey] ? listItem[item.itemKey].firstAmount : ''}
											onChange={(v) => {
												listItem[item.itemKey].firstAmount = v;
												this.setState(
													{
														listItem
													}
												);
											}}
										/>
										:
										item.itemKey == 'checkdate' ?
											<DatePicker
												// isRequire={true}
												disabled={false}
												placeholder={this.state.json['20021005list-000123']}/* 国际化处理： 选择日期*/
												value={listItem[item.itemKey] ? listItem[item.itemKey].firstAmount : ''}
												onChange={(v) => {
													listItem[item.itemKey].firstAmount = v;
													this.setState(
														{
															listItem
														}
													);
												}}
											/>
											: <NCNumber
												value={listItem[item.itemKey] ? listItem[item.itemKey].firstAmount : ''}
												scale={item.itemKey == 'quantity' ? '' : 4}
												onChange={(v) => {
													listItem[item.itemKey].firstAmount = v;
													this.setState(
														{
															listItem
														}
													);
												}}
											/>}
								</div>
								<div className="u-col-md-2 u-col-sm-2 u-col-xs-2">
									<SelectItem
										name={item.itemKey}
										defaultValue={listItem[item.itemKey] ? listItem[item.itemKey].andOr : ' '}
										items={() => {
											return item.itemChildTwo;
										}}
										onChange={(v) => {
											listItem[item.itemKey].andOr = v;
											this.setState({
												listItem
											});
										}}
									/>
								</div>
								<div className="u-col-md-1 u-col-sm-1 u-col-xs-1 text-center nc-theme-common-font-c">{'<='}</div>
								<div className="u-col-md-2 u-col-sm-2 u-col-xs-2">
									{item.itemKey == 'checkno' ?
										<FormControl
											value={listItem[item.itemKey] ? listItem[item.itemKey].secondAmount : ''}
											onChange={(v) => {
												listItem[item.itemKey].secondAmount = v;
												this.setState(
													{
														listItem
													}
												);
											}}
										/>
										: item.itemKey == 'checkdate' ?
											<DatePicker
												// isRequire={true}
												disabled={false}
												placeholder={this.state.json['20021005list-000123']}/* 国际化处理： 选择日期*/
												value={listItem[item.itemKey] ? listItem[item.itemKey].secondAmount : ''}
												onChange={(v) => {
													listItem[item.itemKey].secondAmount = v;
													this.setState(
														{
															listItem
														}
													);
												}}
											/>
											: <NCNumber
												value={listItem[item.itemKey] ? listItem[item.itemKey].secondAmount : ''}
												scale={item.itemKey == 'quantity' ? '' : 4}
												onChange={(v) => {
													listItem[item.itemKey].secondAmount = v;
													this.setState(
														{
															listItem
														}
													);
												}}
											/>
									}
								</div>
							</div>
						);
					case 'autoInput':
						return (
							<div>
								<div className="u-col-md-2 u-col-sm-2 u-col-xs-2">
									<label class="u-label nc-theme-common-font-c">{item.itemName}</label>
								</div>
								<div className="u-col-md-4 u-col-sm-4 u-col-xs-4 explantion">
									<Explantion
										pk_unit={unitOrg}
										value={(v) => {
											listItem[item.itemKey] = v;
											this.setState(
												{
													listItem
												});
										}}
									/>
								</div>
							</div>
						);
						break;
					default:
						return <div />;
						break;
				}
			})
		) : (
				<div />
			);
	};
	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		let selIds = [];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray,
		});
	};
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		self.setState({
			checkedAll: allFlag,
			checkedArray: checkedArray,
		});
	};

	renderColumnsMultiSelect(columns) {
		const { data, checkedArray } = this.state;
		const { multiSelect } = this.props;
		let select_column = {};
		let indeterminate_bool = false;
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
						<Checkbox
							className="table-checkbox"
							checked={this.state.checkedAll}
							indeterminate={indeterminate_bool && !this.state.checkedAll}
							onChange={this.onAllCheckChange}
						/>
					),
					key: 'checkbox',
					dataIndex: 'checkbox',
					width: '50',
					render: (text, record, index) => {
						return (
							<Checkbox
								className="table-checkbox"
								checked={this.state.checkedArray[index]}
								onChange={this.onCheckboxChange.bind(this, text, record, index)}
							/>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}

	onChangeincludetranfer = () => {
		let { checkQuery } = this.state;
		checkQuery = !checkQuery;
		this.setState(
			{
				checkQuery
			},
			() => {
				this.props.ifAdd(checkQuery);
			}
		);
	};

	render() {
		let data1 = [
			{
				itemtype: 'refer',
				col: '4',
				name: this.state.json['20021005list-000131'],/* 国际化处理： 借款项目*/
				visible: true,
				key: 'pk_accasoa',
				label: '22',
				maxlength: '2',
				refcode: '../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef/index.js',
				attrcode: 'pk_accasoakkk'
			}
		];
		let { loadData, data, dataRows } = this.state;
		const columns10 = [
			{
				title: this.state.json['20021005list-000039'],/* 国际化处理： 核算类型*/
				dataIndex: "checktypename",
				key: "checktypename",
				width: '150',
				render: (text, record, index) => {
					return <span>{record.name}</span>;
				}
			},
			{
				title: this.state.json['20021005list-000038'],/* 国际化处理： 核算内容*/
				dataIndex: "checkvaluename",
				key: "checkvaluename",
				width: '300',
				render: (text, record, index) => {
					let { dataRows } = this.state;
					let defaultValue = [];
					if (dataRows[index]["checkvaluename"]) {
						dataRows[index]["checkvaluename"].split(",").map((item, _index) => {
							defaultValue[_index] = { refname: item, refpk: "" };
						});
						dataRows[index]["pk_Checkvalue"].split(",").map((list, __index) => {
							defaultValue[__index].refpk = list;
						});
					} else {
						defaultValue = [{ refname: "", refpk: "" }];
					}
					if (record.refnodename) {
						let referUrl = record.refnodename + '.js';
						if (!this.state[record.pk_accassitem]) {
							{ createScript.call(this, referUrl, record.pk_accassitem) }
							return <div />
						} else {
							if (record.classid == 'b26fa3cb-4087-4027-a3b6-c83ab2a086a9' || record.classid == '40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
								return (
									this.state[record.pk_accassitem] ?
										(this.state[record.pk_accassitem])(
											{
												value: defaultValue,
												isShowUnit: true,
												unitProps: {
													refType: 'tree',
													refName: this.state.json['20021005list-000075'],/* 国际化处理： 业务单元*/
													refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
													rootNode: { refname: this.state.json['20021005list-000075'], refpk: 'root' },/* 国际化处理： 业务单元*/
													placeholder: this.state.json['20021005list-000075'],/* 国际化处理： 业务单元*/
													queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
													treeConfig: { name: [this.state.json['20021005list-000124'], this.state.json['20021005list-000125']], code: ['refcode', 'refname'] },/* 国际化处理： 编码,名称*/
													isMultiSelectedEnabled: false,
													//unitProps:unitConf,
													isShowUnit: false
												},
												unitCondition: {
													pk_financeorg: this.state.unitOrg,
													'TreeRefActionExt': 'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
												},
												isMultiSelectedEnabled: true,
												isShowDisabledData: true,
												disabledDataShow: true,
												"unitValueIsNeeded": false,
												"isShowDimission":true,
												queryCondition: () => {
													config.isShowDimission=true;
													return Object.assign({
														"busifuncode": "all",
														"pk_org": this.state.unitOrg
													}, config)
												},
												onChange: (v) => {
													let { dataRows } = this.state;
													let originData = this.findByKey(record.key, dataRows);
													let refnameArr = [], refpkArr = [], refcodeArr = [];
													if (originData) {
														v.map((arr, index) => {
															refnameArr.push(arr.refname);
															refpkArr.push(arr.refpk);
															refcodeArr.push(arr.refcode);

														})
														originData.checkvaluename = (v.length > 0) ? refnameArr.join() : null;
														originData.pk_Checkvalue = (v.length > 0) ? refpkArr.join() : null;
														originData.checkvaluecode = (v.length > 0) ? refcodeArr.join() : null;
													}
													this.setState({
														dataRows
													})
												}
											}) : <div />
									// </FormItem>
								);
							} else {
								return (
									this.state[record.pk_accassitem] ? (this.state[record.pk_accassitem])(
										{
											value: defaultValue,
											isMultiSelectedEnabled: true,
											isShowDisabledData: true,
											disabledDataShow: true,
											queryCondition: () => {
												if (record.classid && record.classid.length == 20) {//classid的长度等于20的话过滤条件再加一个pk_defdoclist
													return Object.assign({
														"pk_org": this.state.unitOrg,
														"pk_defdoclist": record.classid
													}, config)
												} else {
													return Object.assign({
														"pk_org": this.state.unitOrg
													}, config)
												}
											},
											onChange: (v) => {
												let { dataRows } = this.state;
												let originData = this.findByKey(record.key, dataRows);
												let refnameArr = [], refpkArr = [], refcodeArr = [];
												if (originData) {
													v.map((arr, index) => {
														refnameArr.push(arr.refname);
														refpkArr.push(arr.refpk);
														refcodeArr.push(arr.refcode);

													})
													originData.checkvaluename = (v.length > 0) ? refnameArr.join() : null;
													originData.pk_Checkvalue = (v.length > 0) ? refpkArr.join() : null;
													originData.checkvaluecode = (v.length > 0) ? refcodeArr.join() : null;
												}
												this.setState({
													dataRows
												})
											}
										}) : <div />
									// {/* </FormItem> */}
								);
							}
						}
					} else {//不是参照的话要区分日期、字符、数值
						if (record.classid == 'BS000010000100001033') {//日期
							return (
								<DatePicker
									//name={item.itemKey}
									format={format}
									type="customer"
									isRequire={true}
									placeholder={this.state.json['20021005list-000123']}/* 国际化处理： 选择日期*/
									value={defaultValue[0].refname}
									onChange={(v) => {
										let { dataRows } = this.state;
										let originData = this.findByKey(record.key, dataRows);
										if (originData) {
											let assArr = [];
											assArr.push(v);
											originData.checkvaluename = v ? assArr.join() : null;
											originData.pk_Checkvalue = v ? assArr.join() : null;
											originData.checkvaluecode = v ? assArr.join() : null;
										}
										this.setState({
											dataRows
										})
									}}

								/>
							)
						} else if (record.classid == 'BS000010000100001034') {//日期时间
							return (
								<DatePicker
									//name={item.itemKey}
									showTime={true}
									format={timeFormat}
									type="customer"
									isRequire={true}
									placeholder={this.state.json['20021005list-000123']}/* 国际化处理： 选择日期*/
									value={defaultValue[0].refname}
									onChange={(v) => {
										let { dataRows } = this.state;
										let originData = this.findByKey(record.key, dataRows);
										if (originData) {
											let assArr = [];
											assArr.push(v);
											originData.checkvaluename = v ? assArr.join() : null;
											originData.pk_Checkvalue = v ? assArr.join() : null;
											originData.checkvaluecode = v ? assArr.join() : null;
										}
										this.setState({
											dataRows
										})
									}}

								/>
							)
						} else if (record.classid == 'BS000010000100001031' || record.classid == 'BS000010000100001004') {//数值 整数
							return (
								<NCNumber
									scale={Number(record.digits ? record.digits : '0')}
									value={defaultValue[0].refname}
									maxlength={Number(record.inputlength) + Number(record.digits ? record.digits : '0')}
									placeholder={this.state.json['20021005list-000126']}/* 国际化处理： 请输入数字*/
									onChange={(v) => {
										if (v.indexOf('.') != -1) {
											if (v && v.slice(0, v.indexOf('.')).length > Number(record.inputlength)) {
												toast({ content: this.state.json['20021005list-000150'] + record.inputlength, color: 'warning' });//输入的数字长度不能超过用户设置的长度
												v = v.slice(0, Number(record.inputlength));
											}
										} else {
											if (v && v.length > Number(record.inputlength)) {
												toast({ content: this.state.json['20021005list-000150'] + record.inputlength, color: 'warning' });//输入的数字长度不能超过用户设置的长度
												v = v.slice(0, Number(record.inputlength));
											}
										}
										let { dataRows } = this.state;
										let originData = this.findByKey(record.key, dataRows);
										if (originData) {
											let assArr = [];
											assArr.push(v);
											originData.checkvaluename = v ? assArr.join() : null;
											originData.pk_Checkvalue = v ? assArr.join() : null;
											originData.checkvaluecode = v ? assArr.join() : null;
										}
										this.setState({
											dataRows
										})
									}}
								/>
							)
						} else if (record.classid == 'BS000010000100001032') {//布尔
							return (
								<SelectItem name={record.checktypecode}
									defaultValue={defaultValue[0].refname}
									items={
										() => {
											return ([{
												label: this.state.json['20021005list-000127'],/* 国际化处理： 是*/
												value: 'Y'
											}, {
												label: this.state.json['20021005list-000128'],/* 国际化处理： 否*/
												value: 'N'
											}])
										}
									}
									onChange={(v) => {
										let { dataRows } = this.state;
										let originData = this.findByKey(record.key, dataRows);
										if (originData) {
											let assArr = [];
											assArr.push(v);
											originData.checkvaluename = v ? assArr.join() : null;
											originData.pk_Checkvalue = v ? assArr.join() : null;
											originData.checkvaluecode = v ? assArr.join() : null;
										}
										this.setState({
											dataRows
										})
									}}
								/>
								//   {/* </FormItem> */}
							)
						} else {//字符
							return (
								<FormControl
									value={defaultValue[0].refname}
									onFocus={(e) => {
										e.target.addEventListener(
											"compositionstart",
											e => {
												e.target.isFlag = '1';
											},
											false
										);
										e.target.addEventListener(
											"compositionend",
											e => {
												e.target.isFlag = '0';
												let v = e.target.defaultValue;
												// e.target && e.target.blur();
												if (v && v.length > 0) {
													if (GetChinese(v).length > 0 && 2 * (GetChinese(v).length) > Number(record.inputlength)) {
														toast({ content: this.state.json['20021005list-000150'] + record.inputlength, color: 'warning' });//输入的数字长度不能超过用户设置的长度
														v = v.slice(0, Number(record.inputlength / 2));
														// return false;
													} else if (GetChinese(v).length > 0 && 2 * (GetChinese(v).length) + RemoveChinese(v).length > Number(record.inputlength)) {
														toast({ content: this.state.json['20021005list-000150'] + record.inputlength, color: 'warning' });//输入的数字长度不能超过用户设置的长度
														let chineseData = GetChinese(v);
														if (2 * (chineseData.length) < 10) {
															let chartData = RemoveChinese(v);
															if (chartData.length > 0) {
																v = chineseData.slice(0, Number(record.inputlength / 2)) + chartData.slice(0, record.inputlength - 2 * (chineseData.length));
															} else {
																v = v.slice(0, Number(record.inputlength / 2));
															}
														} else {
															v = v.slice(0, Number(record.inputlength / 2));
														}
														// return false;
													} else if (RemoveChinese(v).length > Number(record.inputlength)) {
														toast({ content: this.state.json['20021005list-000150'] + record.inputlength, color: 'warning' });//输入的数字长度不能超过用户设置的长度
														v = v.slice(0, Number(record.inputlength));
														// return false;
													}
												}
												let { dataRows } = this.state;
												let originData = this.findByKey(record.key, dataRows);
												if (originData) {
													let assArr = [];
													assArr.push(v);
													originData.checkvaluename = v ? assArr.join() : null;
													originData.pk_Checkvalue = v ? assArr.join() : null;
													originData.checkvaluecode = v ? assArr.join() : null;
												}
												this.setState({
													dataRows
												})
											}
										)
									}}

									onChange={(v, e) => {
										if (e.target.isFlag == '0' || !e.target.isFlag) {

											if (v && v.length > 0) {
												if (GetChinese(v).length > 0 && 2 * (GetChinese(v).length) > Number(record.inputlength)) {
													toast({ content: this.state.json['20021005list-000150'] + record.inputlength, color: 'warning' });//输入的数字长度不能超过用户设置的长度
													v = v.slice(0, Number(record.inputlength / 2));
													// return false;
												} else if (GetChinese(v).length > 0 && 2 * (GetChinese(v).length) + RemoveChinese(v).length > Number(record.inputlength)) {
													toast({ content: this.state.json['20021005list-000150'] + record.inputlength, color: 'warning' });//输入的数字长度不能超过用户设置的长度

													let chineseData = GetChinese(v);
													if (2 * (chineseData.length) < 10) {
														let chartData = RemoveChinese(v);
														if (chartData.length > 0) {
															v = chineseData.slice(0, Number(record.inputlength / 2)) + chartData.slice(0, record.inputlength - 2 * (chineseData.length));
														} else {
															v = v.slice(0, Number(record.inputlength / 2));
														}
													} else {
														v = v.slice(0, Number(record.inputlength / 2));
													}
													// return false;
												} else if (RemoveChinese(v).length > Number(record.inputlength)) {
													toast({ content: this.state.json['20021005list-000150'] + record.inputlength, color: 'warning' });//输入的数字长度不能超过用户设置的长度
													v = v.slice(0, Number(record.inputlength));
													// return false;
												}
											}
										}
										let { dataRows } = this.state;
										let originData = this.findByKey(record.key, dataRows);
										if (originData) {
											let assArr = [];
											assArr.push(v);
											originData.checkvaluename = v ? assArr.join() : null;
											originData.pk_Checkvalue = v ? assArr.join() : null;
											originData.checkvaluecode = v ? assArr.join() : null;
										}
										this.setState({
											dataRows
										})
									}}
								/>
							)
						}

					}
				}
			}
		];
		let columnsldad = this.renderColumnsMultiSelect(columns10);
		const emptyFunc = () => <span>{this.state.json['20021005list-000054']}</span>;/* 国际化处理： 这里没有数据*/
		return (
			<div className="right_query">
				<div className="query_head nc-theme-area-split-bc">
					{/* <span className="title">凭证查询</span>
					<Checkbox
						className="mycheck"
						checked={this.state.checkQuery}
						onChange={this.onChangeincludetranfer.bind(this)}
					>
						将查询结果追加到已有结果后面
					</Checkbox> */}
					{/* <Button>清空值</Button> */}
				</div>
				<div className="query_body">
					<div class="query_form">
						{this.props.hideOrShow&&loadData.length > 0 ? this.queryList(loadData) : ''}
					</div>
					{loadData.length > 0 ?
						<NCDiv fieldid="AuxiliaryAccounting" areaCode={NCDiv.config.TableCom}>
							<Table
								className="query_table nc-theme-area-split-bc"
								columns={columnsldad}
								data={dataRows}
								emptyText={emptyFunc}
								scroll={{ x: true, y: 124 }}

							/>
						</NCDiv>
						: ''}
				</div>
				{/* <Button onClick={this.clear} style={{display:this.props.display}} className="cleardata clear">清空值</Button> */}
			</div>
		);
	}
}
SearchModal.defaultProps = defaultProps12;
