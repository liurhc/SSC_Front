import { base, ajax, toast, formDownload } from 'nc-lightapp-front';
const { NCRadio } = base;
import './index.less';
/**
 * 导出xml
 */
function exportXMLItem() {
	let _this=this;
	return (
		<div className="exportXML">
			<NCRadio.NCRadioGroup
				name="ordertype"
				selectedValue={_this.state.exportxml.type}
				onChange={(v) => {
					let exportdata = _this.state.exportxml;
					exportdata.type = v;
					exportdata.localflag = v == '1';
					_this.setState({
						exportxml: exportdata
					});
				}}
			>
				<NCRadio value="0">
					{_this.state.json['20021005list-000029']/* 国际化处理： 导出至服务器*/} <span> .\voucher\voucher.xml</span>
				</NCRadio>
				<div style={{ height: 8 }} />
				<NCRadio value="1">{_this.state.json['20021005list-000030']/* 国际化处理： 导出至本机*/}</NCRadio>
			</NCRadio.NCRadioGroup>
			<p className="tip-box">
				<i className="iconfont icon-tixing" />
				<div className="tip">
					{_this.state.json['20021005list-000031']}<br/>{this.state.json['20021005list-000032']/* 国际化处理：说明：本功能可以把选中的凭证以 *.XML 文件的格式导出。注意：由于大小的限制，一次导出过多的凭证会有问题，所以凭证过多时请分段导出。*/}
				</div>
			</p>
		</div>
	);
}

function exportXMLComfirm() {
	let selectedData = this.props.table.getCheckedRows('gl_voucher');
	if(selectedData.length==0){
		toast({ content: this.state.json['20021005list-000027'], color: 'warning'});/* 国际化处理： 请选择凭证！*/
		return;
	}
	let dataArr = [];
	selectedData.forEach((val) => {
		dataArr.push(val.data.values.pk_voucher.value);
	});
	let pk_vouchers = dataArr;
	let type = this.state.exportxml.type;
	let param = {
		pk_vouchers: pk_vouchers,
		type: type
	};
	let _this=this;
	if (type == 0) {
		ajax({
			url: '/nccloud/gl/voucher/exportXML.do',
			params: param,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					toast({ content: _this.state.json['20021005list-000028'], color: 'success', position: 'bottom' });/* 国际化处理： 导出成功*/
				}
			}
		});
	} else {
		formDownload({
			params: param,
			url: '/nccloud/gl/voucher/exportXML.do',
			enctype: 2
		});
	}
}

export { exportXMLItem, exportXMLComfirm };
