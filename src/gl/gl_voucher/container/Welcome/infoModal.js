import React, { Component } from 'react';
import { high, base, ajax ,getMultiLang} from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCCol: Col,
	NCForm: Form,
	NCTZDatePickClientTime
} = base;
import createScript from '../../../public/components/uapRefer.js';
import {
	CheckboxItem,
	RadioItem,
	TextAreaItem,
	ReferItem,
	SelectItem,
	InputItem,
	DateTimePickerItem
} from '../../../public/components/FormItems';
import { isObj } from './events/buttonClick.js';

const { NCFormItem: FormItem } = Form;
export default class FormItemTab extends Component {
	constructor(props) {
		super(props);
		this.state = {
			listItem: {
				bankaccount: { display: '', value: '' },
				billtype: { display: '', value: '' },
				checkstyle: { display: '', value: '' },
				checkno: { display: '', value: '' },
				checkdate: { display: '', value: '' }
			}, //模板数据对应值
			openindex: '',
			settle: false,
			verify: false,
			querydata: {},
			pk_org: '',
			json:{}
		};
		this.tabs=[]
	}


	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				this.tabs=[
					{
						attrcode: 'bankaccount',
						label: this.state.json['20021005card-000290'],/* 国际化处理： 银行账户*/
						refcode: 'uapbd/refer/sminfo/BankaccSubGridTreeRef/index',
						itemtype: 'refer'
					},
					{
						attrcode: 'billtype',
						label: this.state.json['20021005card-000291'],/* 国际化处理： 票据类型*/
						refcode: 'uapbd/refer/sminfo/NotetypeDefaultGridRef/index',
						itemtype: 'refer'
					},
					{
						attrcode: 'checkstyle',
						label: this.state.json['20021005card-000292'],/* 国际化处理： 结算方式*/
						refcode: 'uapbd/refer/sminfo/BalanceTypeGridRef/index',
						itemtype: 'refer'
					},
					{ attrcode: 'checkno', label: this.state.json['20021005card-000293'], refcode: null, itemtype: 'number' },/* 国际化处理： 票据号*/
					{ attrcode: 'checkdate', label: this.state.json['20021005card-000294'], refcode: null, itemtype: 'datepicker' }/* 国际化处理： 结算业务日期*/
				]

				this.forceUpdate();
				//initTemplate.call(this, this.props);
				// this.loadDept();
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}

	// shouldComponentUpdate (nextProps, nextState) {
	// //    if(nextProps.freeKey==nextState.openindex&&nextProps.querydata.pk_accasoa==nextState.querydata.pk_accasoa){
	// //         return false;
	// //    }
	// }

	componentDidMount() {
		let self = this;
		let nextProp=self.props;
		let { listItem, tabs, pk_org } = self.state;
		if (nextProp.defaultValue&&nextProp.defaultValue.expand) {
				for (let e in nextProp.defaultValue.expand) {
					if (listItem.hasOwnProperty(e)) {
						listItem[e] = JSON.parse(JSON.stringify(nextProp.defaultValue.expand[e]));
					}
					if(e=='checkdate'){
						listItem[e].value= nextProp.defaultValue.expand[e]&&nextProp.defaultValue.expand[e].value?nextProp.defaultValue.expand[e].value:nextProp.defaultValue.prepareddate.value
					}
				}
			}
			if (nextProp.pk_org) {
				pk_org = nextProp.pk_org;
			}
			self.props.OnChange(self);
			self.setState(
				{
					listItem,
					pk_org,
				},
				// () => {
				// 	this.props.OnChange('checkdate', listItem['checkdate'].value);
				// }
			);
			
	}

	queryList = (data) => {
		let self = this;
		let { voucherView } = self.props;
		let { listItem } = self.state;
		return data.length != 0 ? (
			data.map((item, i) => {
				switch (item.itemtype) {
					case 'refer':
						let referUrl = item.refcode + '.js';
						if (!self.state[item.attrcode]) {
							{
								createScript.call(self, referUrl, item.attrcode);
							}
						} else {
							return (
								<div className="modal-form-item">
									<div className="item-title">{item.label}</div>
									<div className="item-content">
										{self.state[item.attrcode] && !voucherView ? (
											self.state[item.attrcode]({
												value: {
													refname: listItem[item.attrcode].display,
													refpk: listItem[item.attrcode].value
												},
												queryCondition: () => {
													return {
														pk_org: self.state.pk_org
													};
												},
												onChange: (v) => {
													// if (item.attrcode != 'pk_accountingbook' && !listItem.pk_accountingbook.value) {
													//     toast({ content: '请先选择核算账簿', color: 'warning' });
													//     return false
													// }
													listItem[item.attrcode].display = v.refname;
													listItem[item.attrcode].value = v.refpk;
													// listItem[item.attrcode].display = v.refname
													this.setState(
														{
															listItem
														},
														// () => {
														// 	this.props.OnChange(item.attrcode, v, self.props.freeKey);
														// }
													);
												}
											})
										) : (
											<span />
										)}
									</div>
								</div>
							);
						}
						break;
					case 'datepicker':
						// let  date=listItem[item.attrcode];
						//    let newDate = [];
						//    if (date && Object.prototype.toString.call(date) == '[object Array]') {
						// 	   if (date.length > 1) {
						// 		   date.map((val) => {
						// 			  // newDate.push(moment(val));
						// 		   });
						// 	   }
						//    }
						//    newDate.length == 0 ? (newDate = '') : newDate;
						return (
							<div className="modal-form-item">
								<div className="item-title">{item.label}</div>
								<div className="item-content">
									{!voucherView ? (
										<NCTZDatePickClientTime
											format={"YYYY-MM-DD"}
											name={item.attrcode}
											showClear={true}
											placeholder={data.label}
											isRequire={true}
											value={listItem[item.attrcode].value}
											showTime={false}
											onChange={(v) => {
												listItem[item.attrcode].value = v;
												this.setState(
													{
														listItem
													},
													// () => {
													// 	this.props.OnChange(item.attrcode, v, self.props.freeKey);
													// }
												);
											}}
											// placeholder={dateInputPlaceholder}
										/>
									) : (
										<span />
									)}
								</div>
							</div>
						);
						break;
					case 'input':
						return (
							<div className="modal-form-item">
								<div className="item-title">{item.label}</div>
								<div className="item-content">
									{!voucherView ? (
										<FormControl
											value={listItem[item.attrcode].value}
											disabled={item.attrcode == 'cashflow' ? true : false}
											onChange={(v) => {
												if (v) {
													listItem[item.attrcode].value = v;
													this.setState(
														{
															listItem
														},
														// () => {
														// 	this.props.OnChange(item.attrcode, v, self.props.freeKey);
														// }
													);
												}
											}}
										/>
									) : (
										<span>{listItem[item.attrcode].value}</span>
									)}
								</div>
							</div>
						);
						break;
					case 'number':
						return (
							<div className="modal-form-item">
								<div className="item-title">{item.label}</div>
								<div className="item-content">
									{!voucherView ? (
										<FormControl
											value={listItem[item.attrcode].value}
											onChange={(v) => {
												if (v) {
													listItem[item.attrcode].value = v;
													this.setState(
														{
															listItem
														},
														// () => {
														// 	this.props.OnChange(item.attrcode, v, self.props.freeKey);
														// }
													);
												}
											}}
										/>
									) : (
										<span>{listItem[item.attrcode].value}</span>
									)}
								</div>
							</div>
						);
						break;
					default:
						break;
				}
			})
		) : (
			<Col />
		);
	};

	render() {
		let self = this;
		let { freeKey } = self.props;
		return this.queryList(self.tabs);
	}
}
