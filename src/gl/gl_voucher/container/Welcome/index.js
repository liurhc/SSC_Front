import React, { Component } from 'react';
import { createPage, high, base, ajax, deepClone, toast, cardCache, sum ,getMultiLang,DongbaToLocalTime,useJS,viewModel} from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
//import SubjectModal from '../SubjectModal'; //会计科目
import VrifyModal from '../vrifyModal'; //即时核销模态框
import VrifyRefModal from '../VrifyRefModal'; //参照核销模态框
import LinkModal from '../LinkModal'; //联查余额功能添加
import ChangeNumberModal from '../ChangeNumberModal'; //修改汇率
import ApportionmentModal from '../ApportionmentModal'; //修改汇率
import SaveErrorModal from '../SaveErrorModal'; //分录标错
import FormItemTab from './FormItemTab.js'; //辅助核算展开
import InfoModal from './infoModal'; //补录信息
import Rate from './rate.js';
import HeadButtom from './headButtom.js';
import AssidModal from '../../../public/components/assidDbModal';
import ReferLoader from '../../../public/ReferVoucherLoader';
import moment from 'moment';
import setScale from '../../../public/common/amountFormat.js';
import {Subtr,accMul,accDiv} from '../../../public/common/method.js';
import { TableColSetting, removeTableListener } from './events/hotkey/contextMenu/index.js';
import {
	dataSourceTable,
	tableId,
	dataSource,
	dataSourceCoord,
	dataSourceDetail,
	successStatus,
	dataSourceNumber,
	oldSource,
	dataSourceSaveNext,
	mutilangJson_list,
	mutilangJson_card,
	mergerequest_card,
	query_accountingbook
} from '../../../public/components/constJSON';
import vouchersaveUpdateCache from './events/vouchersaveUpdateCache.js';
import { handleButtonClick, afterEvent, initTemplate,onEnterAction,onLastFormEnter,onEqual } from './events';
import Checkbox from './checkbox.js';
import { printItem } from '../../voucher_list/list/print';
import amountconvert from '../../amountConvert';
import {queryCurrinfo} from '../../amountConvert/currInfo.js';
import { emptyEnter,buttonStatusDetails,countRow ,memoryHistry,throttle,cutData,updateDirection,fmoney} from "./voucherOftenApi";
import { isObj, convertCurrency } from '../../../public/components/oftenApi';
import AssiBalance from '../../../manageReport/journal/content/export';
import './index.less';
import { totalLine } from './component';
//引入高级组件
//import { Enhance } from './enhanceComponent.js';
let { setDefData, getDefData,addCache,getCurrentLastId,updateCache ,deleteCacheById,getNextId} = cardCache;
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCTZDatePickClientTime,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox11,
	NCNumber,
	NCTooltip,
	NCAutoComplete: AutoComplete,
	NCDropdown: Dropdown,
	NCPanel: Panel,
	NCForm: Form,
	NCButtonGroup: ButtonGroup,
	NCHotKeys:HotKeys,
	NCDiv
} = base;
const { Refer, NCUploader, Inspection ,ApproveDown} = high;
const Option = Select.Option;
const { NCFormItem: FormItem } = Form;
const format = 'YYYY-MM-DD';
const detailExpend = [
	'bankaccount',
	'billtype',
	'checkstyle',
	'checkno',
	'checkdate',
	'verifyno',
	'verifydate',
	'cashflow'
];
const settkleArray = [ 'bankaccount', 'billtype', 'checkstyle', 'checkno', 'checkdate' ];
const verifyArray = [ 'verifyno', 'verifydate' ];

useJS.setConfig({
	'sscrp/rppub/components/image/index': "../../../../sscrp/rppub/components/image/index.js",
	'uap/common/components/printOnClient/index': "../../../../uap/common/components/printOnClient/index.js"
})
//  @withShell
let Welcome=useJS(["sscrp/rppub/components/image/index","uap/common/components/printOnClient/index",],function(image,print){

 
//分录单元格 表头字段 可编辑新控制
// 增删按钮控制 editable true 为可编辑 false 不可编辑
///表头 分录	 editable false 为可编辑 true 不可编辑
return  class Welcome extends Component {
	static defaultProps = {
		multiSelect: {
			type: 'checkbox',
			param: 'key'
		}
	};
	constructor(props) {
		//每定义一个state 都要添加备注
		super(props);
		this.state = {
			verifyRefData:{},//参照核销数据
			currentRowPrevass:[],//当前行的前一行的辅助核算
			attachModal: '', //赋单数据录入弹框标识
			show: true,
			showUploaderTable: false, //分录附件上传
			showViewLoaderDetail:false,//浏览太分录附件
			target: null,
			referDom: '',
			referDom2: '',
			showAss: false, //联查序时账
			headFunc: {
				pk_accountingbook: {
					//核算账簿
					display: '',
					value: ''
				}
			}, //存储表头参照

			checkedAll: false, //分录复选框
			checkedArray: [ false ],
			lastMessage: [],
			unitOrg: {
				//业务单元过滤
				value: ''
			},
			errorFlag: '',
			isFreevalueDefault: false, //是否带出辅助核算
			saveNumber: '', //保存新增标识
			update: false, //修改太页面标识
			getrow: '', //获取指定行的Key
			expandRow: [], //展开子表数据
			index: '', //摘要信息下标
			findRows: [], //修改汇率选定行数据
			accIndex: '', //辅助核算下标
			assList: [], //根据会计科目和制单日期获取的辅助核算数据
			freeValue: [], //存储分录自定义项
			MsgModalAll: false, //控制常用模态框显隐
			SubjectModalShow: false, //科目模态框
			AssistAccModalShow: false, //辅助核算模态框
			changeNumberShow: false, //修改汇率模态框
			apportionmentShow: false, //快速分摊模态框
			VrifyModalShow: false, //及时核销模态框
			rtverifyData:{},//及时核销数据
			VrifyRefModalShow: false, //及时核销模态框
			LinkModalShow: false, //联查余额模态框
			Inspectionshow: false, //联查预算模态框
			SaveErrorModalShow: false, //标错模态框
			sourceData: '', //联查序时账数据
			sequenceData: '',
			id: '',
			isadd: false, //根据核算账簿动态增列判断是否已经新增
			isNC01: false, //根据核算账簿动态增列判断是否已经新增集团借贷
			isNC02: false, //根据核算账簿动态增列判断是否已经新增全局借贷
			checkForm: false, //保存数据控制
			//参照依赖核算账簿值获取
			prepareddate: {
				value: ''
			},
			nov: {
				//凭证号
				value: ''
			},
			startdate: {
				//制单日期
			},
			attachment: {
				//附单据数,
				value: ''
			},
			period: {
				//会计期间
			},
			billmaker: {
				//制单人
			},
			approver: {
				//审核人
			},
			saveData: {
				//保存数据
				showUnit: false,
				details: [],
				voucherkind: {
					//调整期标识
					value: ''
				}
			},
			headContent: [], //表头数据
			footContent: [], //表尾数据
			evidenceColumns: [], //表体显示列
			evidenceData: {
				//表体数据
				rows: [
					{
						key: 1,
						checkedNumber: false,
						isEdit: true,
						excrate2: {
							value: ''
						},
						excrate3: {
							value: ''
						},
						excrate4: {
							value: ''
						},
						childform: []
					}
				],
				index: 1,
				localdebitamountTotle: {
					//借方合计
					value: ''
				},
				localcreditamountTotle: {
					//贷方合计
					value: ''
				},

				groupdebitamountTotle: {
					//组织借方合计
					value: ''
				},
				groupcreditamountTotle: {
					//组织贷方合计
					value: ''
				},

				globaldebitamountTotle: {
					//集团借方合计
					value: ''
				},
				globalcreditamountTotle: {
					//集团贷方合计
					value: ''
				},
				totalamount:{//合计行数据
					totalfinacredit:'',
					totalfinacdebit:'',
					totalbugetcredit:'',
					totalbugetdebit:'',
					totaldebit:'',
					totalcredit:''
				},
				newLine: {
					isEdit: true,
					excrate2: {
						value: ''
					},
					excrate3: {
						value: ''
					},
					excrate4: {
						value: ''
					},
					childform: []
				}
			},
			//options: [ '1常用摘要', '2常用摘要', '3常用摘要' ],
			placeholder: '',
			disabled: false, //可编辑性控制
			copyRecord: '', //复制的key
			voucherStatus: 'save', //凭证单状态 update：修改态，save：保存态
			voucherView: false, //凭证编辑太和修改太唯一标识
			copyStatus: '', //复制的凭证copy
			abandonShow: 'N', //Y是错误
			detailIndex: '', //标错时判断行号
			accountData: [], //会计科目数据
			isNumChange: false,
			json:{},
			inlt:null
		};
		this.amount='';
		this.localcreditamount='';
		this.localdebitamount='';
		this.voucherRows=[];//存储数组数量
		//this.tableDetailbottom='';//滚动底部标识
		//this.tableDetailtop='';//滚动顶部部标识
		this.apportStatus=false;//分摊成功标识 处理分摊组织本币数据
		this.copyData = []; //复制的数据
		this.ViewModel = this.props.ViewModel;
		this.formId = 'head';
		this.dateDisplay=[false];
		this.defaultAccount = {
			display: '',
			value: ''
		};
		this.offsetButton=false;//冲销标识按钮
		this.voucherkind = ''; //调整期凭证标识
		this.selectedIndex = '0';
		//this.formIdtail = 'tail';
		// this.clickhandle = this.clickhandle.bind(this);
		this.tableHeight=40;
		this.datavoucher='';//即时核销弹出框特殊处理
		this.detailvalue='';
		this.moduleId='voucherTable';
		this.props.controlAutoFocus(true);
		this.image=window['sscrp/rppub/components/image/index'];
		this.printOnClient=print;
		this.removeTableListener = removeTableListener.bind(this);
    this.TableColSetting = TableColSetting.bind(this);
	}
	
	// clickhandle() {
	// 	this.setState({
	// 		showAss: !this.state.showAss
	// 	});
	// }

	componentWillReceiveProps(nextProps) {
		let self = this;
		if (nextProps.datavoucher && nextProps.datavoucher.paraInfo) {
			let { voucher, paraInfo } = nextProps.datavoucher;
			self.loadVoucher(voucher, paraInfo);
		}
	}

	//保存新增加载上一张的核算账簿和凭证类别
	loadValue = () => {
		let value = JSON.parse(getGlobalStorage('sessionStorage', 'loadValue'));
		let { pk_accountingbook, num, pk_vouchertype, ...others } = this.state.saveData;
		if (value && value.length != 0) {
			num.value = value[0].num.value;
			pk_accountingbook.display = value[0].pk_accountingbook.display;
			pk_accountingbook.value = value[0].pk_accountingbook.value;
			pk_vouchertype.display = value[0].pk_vouchertype.display;
			pk_vouchertype.value = value[0].pk_vouchertype.value;
			//保存新增带出核算账簿
			this.props.form.setFormItemsValue(this.formId, {
				pk_accountingbook: { value: pk_accountingbook.value, display: pk_accountingbook.display }
			});
			let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
			let pk_accpont = {
				pk_accountingbook: value[0].pk_accountingbook.value,
				isVoucher:true
			};
			this.ifCheck(pk_accpont, url);
			this.setState({
				headFunc: {
					//表体参照依赖
					...others,
					pk_accountingbook: {
						//核算账簿
						display: value[0].pk_accountingbook.display,
						value: value[0].pk_accountingbook.value
					}
				},
				saveData: this.state.saveData
			});
		}
	};


	//react 组件生命周期
	componentWillMount() {
		let callback = (json,status,inlt) => {
			initTemplate.call(this, this.props, json, inlt);
			let listjson = getDefData('mutilangJson_list', mutilangJson_list);
			if (!listjson || listjson.lenght == 0) {
				setDefData('mutilangJson_list', mutilangJson_list, json);
			}
			setDefData('mutilangInlt_card', mutilangJson_card, inlt);
			this.setState({ json: json, inlt: inlt });
		}
		getMultiLang({moduleId:['20021005card','publiccomponents','20021005list'],domainName:'gl',currentLocale:'zh-CN',callback});

		window.onbeforeunload = () => {
			let propsType = this.state.voucherView;
			if (!propsType) {
				return ''
			}
		}
	}

	voucherPageButton=()=>{
		let self=this;
		self.props.form.setFormItemsDisabled(self.formId, {
			pk_vouchertype: true,
			prepareddate: true,
			attachment:  true,
			isdifflag:  true,
			quantityflag:true,
			num:true
		});

	}
//再次触发鼠标点击事件后移除自定义右键菜单
removeCustomRightMenu = (ev, flag) => {
	//点击菜单
	let target = ev.target;
	//当鼠标点击的不是列宽设置外的菜单，都移除右键自定义菜单
	if (!target.classList.contains('columnWidthSetting') || flag) {
		let mouseRightMenu = document.getElementsByClassName('nc-table-setColumnMenu-wrap')[0];
		if (mouseRightMenu) {
			mouseRightMenu.parentNode.removeChild(mouseRightMenu);
		}
	}
}
	componentDidMount() {
		let self = this;
		let { voucherView, saveData, voucherStatus,evidenceData } = self.state;
		let { discardflag, pk_casher, pk_checked ,pk_accountingbook} = saveData;
		let tableArea=document.getElementById('tableArea');
		this.TableColSetting(tableArea, '', '', this.removeCustomRightMenu);
	}
	componentWillUnmount() {
    this.removeTableListener(this.removeCustomRightMenu);
  }
	caiHover=(index,type,e)=>{
		// let line_cai=document.getElementsByClassName('change_span');
		// for(let i=0;i<line_cai.length;i++){
		// 	line_cai[i].style.left='6px';
		// }
	}
	caiOut=(index,type,e)=>{
		// let line_cai=document.getElementsByClassName('change_span');
		// for(let i=0;i<line_cai.length;i++){
		// 	line_cai[i].style.left='-20px';
		// }
		
	}
	componentWillUnmount() {
		setDefData('oldValue', oldSource, null);
		//document.body.removeEventListener("keydown")
		//document.body.removeEventListener("keydown")
	}

	//自定义类型方法添加备注

	//键盘事件
	expandNode = () => {
		let { getrow, expandRow,evidenceData } = this.state;
		if(getrow){
			if (!(expandRow.indexOf(Number(getrow))!='-1')) {
				let record = this.findByKey(getrow, evidenceData.rows);
				this.getData(true, record)
				expandRow.push(Number(getrow));
			}else{
				expandRow=expandRow.filter((item,i)=>{
					return item!=Number(getrow)
				})
			}
		}
	
		this.setState({
			expandRow
		});
	};

	updateRowScale = (response, key) => {
		//会计科目选中更新精度设置
		let { evidenceData } = this.state;
		if (Array.isArray(response.data)) {
			if (response.data.length == 1&&JSON.stringify(response.data[0]!=='{}')) {
				let {
					currinfo,
					excrate2,
					excrate3,
					excrate4,
					excrate2scale,
					excrate3scale,
					excrate4scale,
					NC001,
					NC002,
					scale,
					globalCurrinfo,
					globalmode,
					globalroundtype,
					globalscale,
					groupCurrinfo,
					groupmode,
					orgmode,
					grouproundtype,
					groupscale,
					orgroundtype,
					orgscale,
					quantityscale,
				} = response.data[0];
				if (scale) {
					evidenceData.rows[key].scale = scale;
				}
				if (NC001) {
					evidenceData.rows[key].groupType = NC001;
				}
				if (NC002) {
					evidenceData.rows[key].globalType = NC002;
				}
				if (excrate2||excrate2=='0') {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].excrate2 = {
						value: excrate2
					};
				}
				if (excrate3||excrate3=='0') {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].excrate3 = {
						value: excrate3
					};
				}
				if (excrate4||excrate4=='0') {
					evidenceData.rows[key].excrate4 = {
						value: excrate4
					};
				}
				if (excrate2scale) {
					evidenceData.rows[key].excrate2scale = {
						value: excrate2scale
					};
				}
				if (excrate3scale) {
					evidenceData.rows[key].excrate3scale = {
						value: excrate3scale
					};
				}
				if (excrate4scale) {
					evidenceData.rows[key].excrate4scale = {
						value: excrate4scale
					};
				}
				if (currinfo) {
					evidenceData.rows[key].pk_currtype = {
						display: currinfo.display,
						value: currinfo.value
					};
				}
				if (globalCurrinfo) {
					evidenceData.rows[key].globalCurrinfo = {
						display: globalCurrinfo.display,
						value: globalCurrinfo.value
					};
				}
				if (groupCurrinfo) {
					evidenceData.rows[key].groupCurrinfo = {
						display: groupCurrinfo.display,
						value: groupCurrinfo.value
					};
				}
				if (globalmode) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].globalmode = {
						value: globalmode
					};
				}
				if (globalroundtype) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].globalroundtype = {
						value: globalroundtype
					};
				}
				if (globalscale) {
					evidenceData.rows[key].globalscale = globalscale;
				}
				if (groupmode) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].groupmode = {
						value: groupmode
					};
				}
				if (grouproundtype) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].grouproundtype = {
						value: grouproundtype
					};
				}
				if (groupscale) {
					evidenceData.rows[key].groupscale = groupscale;
				}
				if (orgroundtype) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].orgroundtype = {
						value: orgroundtype
					};
				}
				if (orgscale) {
					evidenceData.rows[key].orgscale = orgscale;
				}
				if(quantityscale){
					evidenceData.rows[key].quantityscale = {
						value: quantityscale
					};
				}
				evidenceData.rows[key].orgmode = {
					value: orgmode
				};
				evidenceData.rows[key].groupmode = {
					value: groupmode
				};
				evidenceData.rows[key].globalmode = {
					value: globalmode
				};
			} else {
				response.data.map((item, index) => {
					// let {  excrate2, excrate3, excrate4,currinfo,globalCurrinfo,groupCurrinfo,
					// 	NC001, NC002, scale,
					// 	globalmode,globalroundtype,globalscale,
					// 	groupmode,grouproundtype,groupscale,
					//    orgroundtype,orgscale} = item;
					if (JSON.stringify(item) != '{}') {
						evidenceData.rows.forEach((content, rIndex) => {
							if (key == rIndex) {
								if(item.scale){
									content.scale = item.scale
								}
								if(item.NC001){
									content.groupType = item.NC001
								}
								if(item.NC002){
									content.globalType = item.NC002
								}
								if(item.excrate2){
									content.excrate2.value= item.excrate2
								}
								if(item.excrate3){
									content.excrate3.value= item.excrate3
								}
								if(item.excrate4){
									content.excrate4.value= item.excrate4
								}
								if(item.excrate2scale){
									if(content.excrate2scale){
										content.excrate2scale.value=item.excrate2scale
									}
									if(content.excrate2){
										content.excrate2.scale=item.excrate2scale
									}
									
								}
								if(item.excrate3scale){
									if(content.excrate3scale){
										content.excrate3scale.value=item.excrate3scale
									}
									if(content.excrate3){
										content.excrate3.scale=item.excrate3scale
									}	
									
								}
								if(item.excrate4scale){
									if(content.excrate4scale){
										content.excrate4scale.value=item.excrate4scale
									}
									if(content.excrate4){
										content.excrate4.scale=item.excrate4scale
									}	
								}
								if(item.currinfo&&item.currinfo.display){
									content.currinfo = {
										display:  item.currinfo&&item.currinfo.display,
										value: item.currinfo&&item.currinfo.value
									};
								}
								if(item.groupCurrinfo){
									content.groupCurrinfo = {
										display: item.groupCurrinfo ? item.groupCurrinfo.display : '',
										value: item.groupCurrinfo ? item.groupCurrinfo.value : ''
									};
								}

								if(content.globalCurrinfo){
									content.globalCurrinfo = {
										display: item.globalCurrinfo ? item.globalCurrinfo.display : '',
										value: item.globalCurrinfo ? item.globalCurrinfo.value : ''
									};
								}
							
								if(item.globalmode){
									content.globalmode={
										value:item.globalmode
									}
								}

								if(item.globalroundtype){
									content.globalroundtype={
										value:item.globalroundtype
									}
								}
								
								if(item.globalscale){
									content.globalscale = item.globalscale;
								}
								
								if(item.orgscale){
									content.orgscale = item.orgscale;
								}
								if(item.groupmode){
									content.groupmode={
										value:item.groupmode
									}
								}

								if(item.grouproundtype){
									content.grouproundtype={
										value :item.grouproundtype
									}
								}
								if(item.groupscale){
									content.groupscale = item.groupscale;
								}
								
								if(item.quantityscale){
									content.quantityscale={
										value:item.quantityscale
									}
								}
								
								if( item.orgroundtype){
									content.orgroundtype={
										value:item.orgroundtype
									}
								}
								if(item.orgmode){
									content.orgmode={
										value:item.orgmode
									}
								}
								
							}
						});
					}
					key++;
				});
			}
		}
		this.setState({
			evidenceData
		},()=>{
			//切换科目同步跟新原币 组织 集团本币
			let { data, currInfo } = this.exportData(evidenceData.rows[key]);
			let countValue = amountconvert(data, currInfo, 'amount');
			countRow(evidenceData.rows[key], countValue, 'amount');
			this.totleCount();
		});
	};

	loadCurryData = (response) => {
		let self = this;
		let { saveData, evidenceData } = self.state;
		let { pk_currtype, excrate2, excrate3, excrate4, NC001, NC002, scale, num } = response.data;
		saveData.num = {
			display: num.toString(),
			value: num.toString()
		};
		//凭证号赋默认值
		self.props.form.setFormItemsValue(self.formId, { num: { value: response.data.num } });
		this.updateRows(evidenceData, response.data);
		self.setState({
			evidenceData,
			saveData
		});
	};

	getCurry(book, date, values, type, key) {
		var self = this;
		let url = '/nccloud/gl/voucher/voucherValueChange.do';
		let data = {
			key: type,
			value: values.value,
			pk_accountingbook: book,
			prepareddate: date
		};
		ajax({
			url,
			data,
			success: function(response) {
				if (response.data) {
					if (type == 'pk_vouchertype') {
						self.loadCurryData(response);
					} else {
						self.updateRowScale(response, key);
					}
				}
			}
		});
	}

	//根据凭证类别获取凭证号
	getNov(book, date, type) {
		var self = this;
		let url = '/nccloud/gl/voucher/getvoucherno.do';
		let data = {
			pk_accountingbook: book,
			prepareddate: date,
			pk_vouchertype: type
		};
		ajax({
			url,
			data,
			success: function(response) {
				if (response.data) {
					let { saveData } = self.state;
					//let { num }=response.data
					saveData.num = {
						display: response.data.num.toString(),
						value: response.data.num.toString()
					};
					//凭证号赋默认值
					self.props.form.setFormItemsValue(self.formId, { num: { value: response.data.num } });
					self.setState({
						saveData
					});
				}
			}
		});
	}
	
	//凭证分录等号
	// equal = (code, originData) => {
	// 	let {
	// 		rows,
	// 		localcreditamountTotle,
	// 		localdebitamountTotle,
	// 		globalcreditamountTotle,
	// 		globaldebitamountTotle,
	// 		groupcreditamountTotle,
	// 		groupdebitamountTotle
	// 	} = this.state.evidenceData;
	// 	switch (code) {
	// 		case 'localcreditamount':
	// 			if (originData[code].value && originData[code].value != 0) {
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: ''
	// 				};
	// 				originData.amount={//将原币清空，根据组织本币算原币
	// 					...originData.amount,
	// 					value:''
	// 				}
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData[code] = {
	// 					...originData[code],
	// 					value:Subtr(localdebitamountTotle.value , localcreditamountTotle.value)
	// 				};
	// 			} else {
	// 				originData.localdebitamount = {
	// 					...originData.localdebitamount,
	// 					value: ''
	// 				};
	// 				originData.amount={//将原币清空，根据组织本币算原币
	// 					...originData.amount,
	// 					value:''
	// 				}
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData[code] = {
	// 					...originData[code],
	// 					value:Subtr(localdebitamountTotle.value - localcreditamountTotle.value)
	// 				};
	// 			}
	// 			break;
	// 		case 'localdebitamount':
	// 			if (originData[code].value && originData[code].value != 0) {
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: ''
	// 				};
	// 				originData.amount={
	// 					...originData.amount,
	// 					value:''
	// 				}
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData[code] = {
	// 					...originData[code],
	// 					value:Subtr(localcreditamountTotle.value ,localdebitamountTotle.value)
	// 				};
	// 			} else {
	// 				originData.localcreditamount = {
	// 					...originData.localcreditamount,
	// 					value: ''
	// 				};
	// 				originData.amount={//将原币清空，根据组织本币算原币
	// 					...originData.amount,
	// 					value:''
	// 				}
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: Subtr(localcreditamountTotle.value , localdebitamountTotle.value)
	// 				};
	// 			}
	// 			break;

	// 		case 'groupcreditamount':
	// 			if (originData[code].value && originData[code].value != 0) {
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: ''
	// 				};
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: Subtr(groupdebitamountTotle.value , groupcreditamountTotle.value)
	// 				};
	// 			} else {
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: Subtr(groupdebitamountTotle.value , groupcreditamountTotle.value)
	// 				};
	// 				originData.groupdebitamount = {
	// 					...originData.groupdebitamount,
	// 					value: ''
	// 				};
	// 			}
	// 			break;
	// 		case 'groupdebitamount':
	// 			if (originData[code].value && originData[code].value != 0) {
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: ''
	// 				};
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData[code] = {
	// 					...originData[code],
	// 					value:Subtr(groupcreditamountTotle.value , groupdebitamountTotle.value)
	// 				};
	// 			} else {
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData[code] = {
	// 					...originData[code],
	// 					value:Subtr(groupcreditamountTotle.value ,groupdebitamountTotle.value)
	// 				};
	// 				originData.groupcreditamount = {
	// 					...originData.groupcreditamount,
	// 					value: ''
	// 				};
	// 			}
	// 			break;
	// 		case 'globalcreditamount':
	// 			if (originData[code].value && originData[code].value != 0) {
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: ''
	// 				};
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: Subtr(globaldebitamountTotle.value , globalcreditamountTotle.value)
	// 				};
	// 			} else {
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData.globaldebitamount = {
	// 					...originData.globaldebitamount,
	// 					value: ''
	// 				};
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: Subtr(globaldebitamountTotle.value , globalcreditamountTotle.value)
	// 				};
	// 			}
	// 			break;
	// 		case 'globaldebitamount':
	// 			if (originData[code].value && originData[code].value != 0) {
	// 				originData[item.attrcode] = {
	// 					...originData[item.attrcode],
	// 					value: ''
	// 				};
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: Subtr(globalcreditamountTotle.value , globaldebitamountTotle.value)
	// 				};
	// 			} else {
	// 				this.totleCount(code, 'localdebitamountTotle');
	// 				originData.globalcreditamount = {
	// 					...originData.globalcreditamount,
	// 					value: ''
	// 				};
	// 				originData[code] = {
	// 					...originData[code],
	// 					value: Subtr(globalcreditamountTotle.value , globaldebitamountTotle.value)
	// 				};
	// 			}
	// 			break;
	// 		default:
	// 			break;
	// 	}
	// };

	//参数抽取

	exportData = (originData) => {
		let data = {
			amount: originData.amount ? originData.amount.value : '', //原币
			localamount:
			originData.localcreditamount&&originData.localcreditamount.value ||  originData.localdebitamount&&originData.localdebitamount.value
					? originData.localcreditamount.value || originData.localdebitamount.value
					: '', //组织本币
			groupamount:
			originData.groupcreditamount&&originData.groupcreditamount.value || originData.groupdebitamount&&originData.groupdebitamount.value
					? originData.groupcreditamount.value || originData.groupdebitamount.value
					: '', //集团本币
			globalamount:
			originData.globalcreditamount&&originData.globalcreditamount.value || originData.globaldebitamount&&originData.globaldebitamount.value
					? originData.globalcreditamount.value || originData.globaldebitamount.value
					: '', //全局本币
			pk_currtype:  originData.pk_currtype&&originData.pk_currtype.value, //原币币种
			orgcurrtype: originData.orgcurrtype ? originData.orgcurrtype.value : '', //本币币种
			groupcurrtype: originData.groupcurrtype ? originData.groupcurrtype.value : '', //集团本币币种
			globalcurrtype: originData.globalcurrtype ? originData.globalcurrtype.value : '', //全局本币币种
			excrate2: originData.excrate2 ? originData.excrate2.value : '', //本币汇率
			excrate3: originData.excrate3 ? originData.excrate3.value : '', //集团本币汇率
			excrate4: originData.excrate4 ? originData.excrate4.value : '', //全局本币汇率
			price: originData.price ? originData.price.value : '', //单价
			pk_accountingbook: originData.pk_accountingbook ? originData.pk_accountingbook.value : '', //账簿
			prepareddate: originData.prepareddate ? originData.prepareddate.value : '', //制单日期
			quantity: originData.debitquantity ? originData.debitquantity.value : '' //数量分借贷，后续处理
		};
		let currInfo = {
			roundtype: originData.roundtype ? originData.roundtype.value : '', //原币进舍规则
			orgroundtype: originData.orgroundtype ? originData.orgroundtype.value : '', //本币进舍规则
			grouproundtype: originData.grouproundtype ? originData.grouproundtype.value : '', //集团本币进舍规则
			globalroundtype: originData.globalroundtype ? originData.globalroundtype.value : '', //全局本币进舍规则
			pricescale: originData.pricescale ? originData.pricescale.value : originData.price&&originData.price.scale, //单价精度
			quantityscale: originData.debitquantity&&originData.debitquantity.scale ?originData.debitquantity.scale : originData.quantityscale&&originData.quantityscale.value, //数量精度
			priceroundtype: originData.priceroundtype ? originData.priceroundtype.value : '', //单价进舍规则
			scale: originData.scale ? originData.scale : '', //原币精度
			excrate2scale: originData.excrate2scale ? originData.excrate2scale.value : '', //本币汇率
			excrate3scale: originData.excrate3scale ? originData.excrate3scale.value : '', //本币汇率
			excrate4scale: originData.excrate4scale ? originData.excrate4scale.value : '', //本币汇率
			orgscale: originData.orgscale ? originData.orgscale : '', //组织本币精度
			groupscale: originData.groupscale ? originData.groupscale : '', //集团本币精度
			globalscale: originData.globalscale ? originData.globalscale : '', //全局本币精度
			maxconverr: originData.maxconverr ? originData.maxconverr.value : '',
			NC001: originData.groupType ? originData.groupType : '', //集团本币计算方式
			NC002: originData.globalType ? originData.globalType : '', //全局本币计算方式
			orgmode: originData.orgmode ? originData.orgmode.value : '', //汇率计算方式 是否为除 true=除
			groupmode: originData.groupmode
				? originData.groupmode.value === undefined ? '' : originData.groupmode.value
				: '', //集团本币汇率计算方式 是否为除 true=除
			globalmode: originData.globalmode
				? originData.globalmode.value === undefined ? '' : originData.globalmode.value
				: '' //全局本币汇率计算方式 是否为除 true=除
		};

		return { data, currInfo };
	};



	countCashFlow=(record)=>{
		if(record.cashflow&&record.cashflow.length>0){
			let cashflowname='';
			for(let i=0;i<record.cashflow.length;i++){
				let cashflow=record.cashflow[i];
				cashflow.pk_accasoa={
					display:record.pk_accasoa.display,
					value:record.pk_accasoa.value
				}
				if(record.localcreditamount&&record.localcreditamount.value){	
					cashflow.direct.value=this.state.json['20021005card-000010'];/* 国际化处理： 贷*/
					cashflow.creditamount={
						display:record.amount.value,
						value:record.amount.value
					}
					cashflow.debitamount={
						display:'0',value:'0'
					}
					cashflow.localamount.value=record.localcreditamount.value;
					cashflow.localamount.display=record.localcreditamount.value;
					cashflow.groupamount.value=record.groupcreditamount.value;
					cashflow.groupamount.display=record.groupcreditamount.value;
					cashflow.globalamount.value=record.globalcreditamount.value;
					cashflow.globalamount.display=record.globalcreditamount.value;
				}else{
					cashflow.localamount.value=record.localdebitamount.value;
					cashflow.localamount.display=record.localdebitamount.value;
					cashflow.groupamount.value=record.groupdebitamount.value;
					cashflow.groupamount.display=record.groupdebitamount.value;
					cashflow.globalamount.value=record.globaldebitamount.value;
					cashflow.globalamount.display=record.globaldebitamount.value;
					cashflow.debitamount={
						display:record.amount.value,
						value:record.amount.value
					}
					cashflow.creditamount={
						display:'0', value:'0'
					}
				}
				cashflow.assid={
					display:record.assid.display,
					value:record.assid.display
				}
				cashflow.occamount.value=record.amount.value;
				cashflow.occamount.display=record.amount.value;
				cashflow.localcreditamount.value=record.localcreditamount.value;
				cashflow.localdebitamount.value=record.localdebitamount.value;
				cashflow.groupcreditamount.value=record.groupcreditamount.value;
				cashflow.groupdebitamount.value=record.groupdebitamount.value;
				cashflow.globalcreditamount.value=record.globalcreditamount.value;
				cashflow.globaldebitamount.value=record.globaldebitamount.value;

				if(cashflow.pk_cashflow_main&&cashflow.pk_cashflow_main.value&&cashflow.pk_cashflow_main.value!=''){
					cashflowname=cashflowname+'【' +cashflow.pk_cashflow_main.display +'：' +
						(cashflow.direct.value=='贷'?cashflow.localcreditamount.value:
						cashflow.localdebitamount.value)+'】';
				}
				if(cashflow.pk_cashflow_ass&&cashflow.pk_cashflow_ass.value&&cashflow.pk_cashflow_ass.value!=''){
					cashflowname=cashflowname+'【' +cashflow.pk_cashflow_ass.display +'：' +
						(cashflow.direct.value=='贷'?cashflow.localcreditamount.value:
						cashflow.localdebitamount.value)+'】';
				}
			}
			record.expand.cashflowname={display:cashflowname,value:cashflowname};			
		}
	}

	isQuire=(item)=>{
		if(item.required){
			return <div><span class='requireItem' fieldid={item.attrcode}>*</span>{item.label}</div>
		}else{
			return <span fieldid={item.attrcode}>{item.label}</span>
		}
	}

	//根据模板数据渲染表格列

	editeColumns(loadColums) {
		let openRowCol = {
			title: '',
			key: 'open',
			dataIndex: 'open',
			width: '50px',
			fixed: "right",
			render: (text, record, index) => {
				let errorTip = <div>{record.errmessage ? record.errmessage.value : ''}</div>;
				let checkTip = <div>{record.checkmessage ? record.checkmessage.value : ''}</div>;
				return [
					record.checkmessage && record.checkmessage.value && (
						<NCTooltip trigger="hover" placement={'top'} inverse overlay={checkTip}>
							<i className="iconfont icon-zhuyi1" />
						</NCTooltip>
					),
					record.errmessage &&record.errmessage.value&& (
						<NCTooltip trigger="hover" placement={'top'} inverse overlay={errorTip}>
							<i className="iconfont icon-shibai" />
						</NCTooltip>
					)
				];
			}
		};
		let dataFirst = {
		title: (<span fieldid="key">{this.state.json['20021005card-000065']}</span>),/* 国际化处理： 行号*/
			attrcode:'key',
			key: 'key',
			dataIndex: 'key',
			width: '80px',
			fixed: "left",
			render: (text, record, index) => <span class='indexCode' fieldid="key">{record.key}</span>
		};
		loadColums.map((item, index) => {
			item.title =this.isQuire(item);
			item.dataIndex = item.attrcode;
			// item.width =item.width;
			item.render = (text, record, index) => {
				//根据字段类型，渲染不同的组件
				let self = this;
				let { rows } = self.state.evidenceData;
			//	let originData = self.findByKey(record.key, rows);
				let errorMsg, errorBorder;
				let defaultValue = {};
				let { pk_accountingbook, prepareddate, saveData, voucherView, evidenceColumns } = self.state;
				let { data, currInfo } = self.exportData(record);
				if (text && text.value !== null) {
					defaultValue = { refname: text.display, refpk: text.value };
				}
				switch (item.itemtype) {
					case 'input':
						 if (item.attrcode == 'assid') {//辅助核算
							return !voucherView ? (
								<HotKeys
									keyMap={{'assidNode': 'f2'}}
									attach={false}
									handlers={{
										'assidNode': (e) => {
											if (!record[item.attrcode].editable) {
												this.handleAssistAcc.call(this, record.key)
												e.stopPropagation();
											}
										}
									}}
								>
									<div fieldid={item.attrcode}
										className="refer-input-wraper voucher_assid"
										id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`}//tabindex="0" 
										onKeyDown={(event) => {
											if (event.keyCode == '13' || event.key === "Enter" || event.keyCode == '38' || event.key === "ArrowUp" || event.keyCode == '40' || event.key === "ArrowDown") {// 40 ArrowDown 38 ArrowUp
												onEnterAction(event, this, item.attrcode, `hot-key-${this.moduleId}-${item.attrcode}-${index}`, index, record)
											}
										}}
									>
										<FormControl
											fieldid={item.attrcode}
											value={record.assid ? record.assid.display : ''}
											// disabled={true}//禁用状态不会触发onenter
											onChange={(v) => {
												let cloneNewLine = deepClone(evidenceColumns);
												cloneNewLine.splice(4, 0, number1);
												this.setState({
													evidenceColumns: cloneNewLine
												});
											}}
										/>

										{!record[item.attrcode].editable ? (
											<i
												className="iconfont icon-canzhaozuixin"
												onClick={this.handleAssistAcc.bind(this, record.key)}
											/>
										) : null}
									</div>
								</HotKeys>
							) : (
								<NCTooltip trigger="hover" placement={'top'} inverse overlay={record.assid ? record.assid.display : ''}>
									<div class='accasoaClass' fieldid={item.attrcode}>{record.assid&&record.assid.display?record.assid.display : <span>&nbsp;</span>}</div>
								</NCTooltip>
							);
						}else{
							return <div id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`}>
							{ !voucherView ? (
							<FormControl
							fieldid={item.attrcode}
							onEnter={(event)=>{
								onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
							}}
								value={text&&text.value && text.value != '0' ? text.value : ''}
								disabled={
									record[item.attrcode]&&record[item.attrcode].editable ? record[item.attrcode].editable : false
								}
								onChange={(v) => {
									let { evidenceData } = this.state;
									if (record) {
										record[item.attrcode]={
											value:v
										}
									}
									this.setState({
										evidenceData
									});
								}}
							/>):(
								<div fieldid={item.attrcode}>{text&&text.value?text.value:<span>&nbsp;</span>}</div>
							)}</div>
						}
						break;

					case 'refer':
						let referUrl = item.refcode + '.js';
						let editable = record[item.attrcode]&&record[item.attrcode].editable ? record[item.attrcode].editable : false;
						if (item.attrcode != 'pk_accasoa') {
							return !voucherView ? (
								<div id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`} fieldid={item.attrcode}>
								<ReferLoader
									onEnter={(event)=>{
										onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
									}}
									fieldid={item.attrcode}
									tag={item.attrcode}
									refcode={referUrl}
									inputDefer={100}
									disabled={editable}
									value={{ refname: text?text.display:'', refpk: text?text.value:'' }}
									queryCondition={() => {
										return {
											pk_accountingbook: self.state.headFunc.pk_accountingbook.value,
											pk_org: self.state.unitOrg.value
										};
									}}
									onFocus={(v) => {
										let _this = this;
										let { prepareddate, evidenceData, headFunc, freeValue } = _this.state;
										if (!headFunc.pk_accountingbook.value) {
											toast({ content: _this.state.json['20021005card-000220'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
											return false;
										}
										if (item.attrcode == 'explanation' || item.attrcode == 'pk_currtype') {
											//摘要最后一行新增空行
											if (record.key == evidenceData.rows.length&&!evidenceData.rows[index].explanation.value) {
												//在最后一行点击，自动新增一行\
												if (item.attrcode == 'explanation' && index >= 1) {
													evidenceData.rows[index].explanation.display =
														evidenceData.rows[index - 1].explanation.display;
													evidenceData.rows[index].explanation.value =
														evidenceData.rows[index - 1].explanation.value;
												}
												// if (item.attrcode == 'pk_unit' && index >= 1) {
												// 	evidenceData.rows[index].pk_unit.display =
												// 		evidenceData.rows[index - 1].pk_unit.display;
												// 	evidenceData.rows[index].pk_unit.value =
												// 		evidenceData.rows[index - 1].pk_unit.value;
												// }
												// if (item.attrcode == 'pk_unit_v' && index >= 1) {
												// 	evidenceData.rows[index].pk_unit_v.display =
												// 		evidenceData.rows[index - 1].pk_unit_v.display;
												// 	evidenceData.rows[index].pk_unit_v.value =
												// 		evidenceData.rows[index - 1].pk_unit_v.value;
												// }
												if(!this.offsetButton&&!(saveData.detailmodflag && saveData.detailmodflag.value === false)){
													this.addNewLine(rows.length);//核销和汇兑损益跳转无法自动怎行
												}
												
											}
										}
										if(item.attrcode == 'explanation'){//键盘焦点问题
											_this.getRow({key:index+1},index)
										}
									}}
									onChange={(v) => {
										let { prepareddate, evidenceData, headFunc } = this.state;
										//let originData = this.findByKey(record.key, evidenceData.rows);
										if (record) {
											record[item.attrcode] = {
												display: v.refname,
												value: v.refpk
											};
										}
										//根据币种请求汇率
										if (item.attrcode == 'pk_currtype') {
											let url = '/nccloud/gl/glpub/ratequery.do';
											let pk_currtype = {
												pk_accountingbook: headFunc.pk_accountingbook.value,
												pk_currtype: v.refpk,
												prepareddate: prepareddate.value
											};
											if (headFunc.pk_accountingbook.value && v.refpk && prepareddate.value) {
												this.queryRate(pk_currtype, url, record);
											}
											if(record.cashflow&&record.cashflow.length!='0'){//跟新现金流量
												record.cashflow[0]['pk_currtype'].value= v.refpk;
												record.cashflow[0]['pk_currtype'].display= v.refname;
											}
										}
										//摘要联动
										let saveDataObj = JSON.parse(JSON.stringify(this.state.saveData));
										if (saveDataObj.explanation) {
											saveDataObj.explanation.display = evidenceData.rows[0].explanation.display;
											saveDataObj.explanation.value = evidenceData.rows[0].explanation.value;
										}
										this.setState({
											evidenceData,
											saveData: saveDataObj
										});
										

									}}
								/>
								</div>
							) : (
								<NCTooltip trigger="hover" placement="top" inverse={false} overlay={text?text.display:''}>
									<div class='accasoaClass' fieldid={item.attrcode}>{text?text.display:<span>&nbsp;</span>}</div>
								</NCTooltip>
								// <p class='accasoaClass'><span>{text?text.display:''}</span></p>
							);
						} else {
							//会计科目参照特殊处理
							return !voucherView ? (
								<div id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`} fieldid={item.attrcode}>
								<ReferLoader
									onEnter={(event)=>{
										onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
									}}
									fieldid={item.attrcode}
									tag={item.attrcode}
									refcode={referUrl}
									inputDefer={100}
									disabled={editable}
									isMultiSelectedEnabled={true}
									isShowHighFilter={false}
									onlyLeafCanSelect={true}
									isShowDisabledData={false}
									value={[ { ...text.text,refname: text.display, refpk: text.value } ]}
									onFocus={async () => {
										return new Promise(resolve => {
											let _this = this;
											let { prepareddate, evidenceData, headFunc, freeValue, getrow } = _this.state;
									
											if (!headFunc.pk_accountingbook.value) {
												toast({ content: _this.state.json['20021005card-000220'], color: 'warning' }); /* 国际化处理： 请先选择核算账簿*/
												resolve(false);
											}
									
											if (item.attrcode == 'pk_accasoa') {
												//摘要最后一行新增空行
												if (record.key == evidenceData.rows.length) {
													//在最后一行点击，自动新增一行
													// if (!evidenceData.rows[index].explanation.display&&index >= 1) {
													// 	evidenceData.rows[index].explanation.display = evidenceData.rows[index - 1].explanation.display;
													// 	evidenceData.rows[index].explanation.value = evidenceData.rows[index - 1].explanation.value;
													// }
													// if (item.attrcode == 'pk_unit' && index >= 1) {
													// 	evidenceData.rows[index].pk_unit.display = evidenceData.rows[index - 1].pk_unit.display;
													// 	evidenceData.rows[index].pk_unit.value = evidenceData.rows[index - 1].pk_unit.value;
													// }
													// if (item.attrcode == 'pk_unit_v' && index >= 1) {
													// 	evidenceData.rows[index].pk_unit_v.display = evidenceData.rows[index - 1].pk_unit_v.display;
													// 	evidenceData.rows[index].pk_unit_v.value = evidenceData.rows[index - 1].pk_unit_v.value;
													// }
													if (!this.offsetButton && !(saveData.detailmodflag && saveData.detailmodflag.value === false)) {
														//冲销编辑太不自动曾航 //resolve  setState组件冲突
														this.addNewLine(rows.length);
													}
													this.getRow({key:index+1},index);
												}
												this.setState(
													{
														getrow: record.key //点击科目参照设置选中行
													},
													() => {
														resolve(true);
													}
												);
											} else {
												resolve(true);
											}
										});
									}}

									queryCondition={() => {
										return {
											pk_accountingbook: self.state.headFunc.pk_accountingbook.value,
											dateStr: prepareddate.value?prepareddate.value.split(' ')[0]:prepareddate.value,
											isDataPowerEnable: 'Y',
											DataPowerOperationCode: 'fi'
										};
									}}
									onChange={(v) => {
										let _this = this;
										let { prepareddate, evidenceData, headFunc, freeValue ,getrow} = _this.state;
										//切换会计科目修改币种汇率
										if (v && v.length != 0) {
											let queryArry = [];
											if(v.length=='1'){//针对一行特殊处理
												if (record) {
													record[item.attrcode] = {
														accproperty : v[0].nodeData.accproperty,
														parallelaccounts: v[0].nodeData.parallelaccounts,
														nparallelaccounts: v[0].nodeData.nparallelaccounts,
														cashtype: v[0].cashtype&&v[0].cashtype || v[0].nodeData&&v[0].nodeData.cashtype,
														display: v[0].dispname&&v[0].dispname || v[0].nodeData&&v[0].nodeData.dispname,
														value: v[0].refpk,
														text:v[0]
													};
													record.expand={}//切换会计科目跟新子表展开数据
												}
												if (
													v[0].unit != null ||
													( v[0].nodeData && JSON.stringify( v[0].nodeData) != '{}')
												) {
													_this.ifNumber( v[0].unit ||  v[0].nodeData.unit, evidenceData.rows[index]);
												}
												queryArry.push(v[0].refpk);
												evidenceData.rows.forEach(function(item, i) {
													item.detailindex = {//多选重置 现金流量多选
														value: (i + 1).toString()
													};
													item.key = ++i;
												});
											}else{//多行拆分处理
												let newData = v.map((item,i) => {
													let pk_accasoa = {
														accproperty : item.nodeData.accproperty,
														parallelaccounts: item.nodeData.parallelaccounts,
														nparallelaccounts: item.nodeData.nparallelaccounts,
														cashtype: item.cashtype&&item.cashtype || item.nodeData&&item.nodeData.cashtype,
														display: item.dispname&&item.dispname || item.nodeData&&item.nodeData.dispname,
														value: item.refpk,
														text:item
													};
													let	rowsData = evidenceData.rows.find((rowKey) => item.refpk == rowKey.pk_accasoa.value&&getrow==rowKey.key);
													let addRows=rowsData?rowsData:evidenceData.rows[index];
													let pk_detail={ 
														value:'',
														display:'',
														scale:''
													};
													let amount={
														value:'',
														display:'',
														scale:''
													};
													let debitquantity={
														value:'',
														display:'',
														scale:''
													};
													let price={
														value:'',
														display:'',
														scale:''
													};
													let localdebitamount={
														value:'',
														display:'',
														scale:''
													};
													let localcreditamount={
														value:'',
														display:'',
														scale:''
													};
													let groupcreditamount={
														value:'',
														display:'',
														scale:''
													};
													let groupdebitamount={
														value:'',
														display:'',
														scale:''
													};
													let globalcreditamount={
														value:'',
														display:'',
														scale:''
													};
													let globaldebitamount={
														value:'',
														display:'',
														scale:''
													}
													_this.addRowClear(//是否有科目添加上面赋值问题，有待完善
														pk_detail,amount,debitquantity,price,localdebitamount,localcreditamount,groupcreditamount,
														groupdebitamount,globalcreditamount,globaldebitamount,rowsData,addRows
													)
													// let pk_detail={//分录主键,新增行清空
													// 		display:i=='0'?evidenceData.rows[index].pk_detail&&evidenceData.rows[index].pk_detail.display:'',
													// 		value:i=='0'?evidenceData.rows[index].pk_detail&&evidenceData.rows[index].pk_detail.value:'',
													// 		scale:i=='0'?evidenceData.rows[index].pk_detail&&evidenceData.rows[index].pk_detail.scale:''
													// 	}
													let assid={
														display:rowsData?addRows.assid&&addRows.assid.display:'',
														value:rowsData?addRows.assid&&addRows.assid.value:'',
														scale:rowsData?addRows.assid&&addRows.assid.scale:''
													}
													let ass=rowsData?addRows.ass:[];
													//多选数量列控制
													if (
														item.unit != null ||
														(item.nodeData && JSON.stringify(item.nodeData) != '{}')
													) {
														_this.ifNumber(item.unit || item.nodeData.unit, addRows);
													}
													queryArry.push(item.refpk);
													return {
														...addRows,
														pk_accasoa,
														cashflow:Array.isArray(addRows.cashflow)&&addRows.cashflow.length!='0'?[//需要循环更新会计科目字段
															{...addRows.cashflow[0],
															pk_accasoa:pk_accasoa}
														]:null,
														pk_detail,
														amount,debitquantity,price,groupcreditamount,localcreditamount, globalcreditamount, globaldebitamount,
														localdebitamount,groupdebitamount,assid,ass
													};
												});
												newData.unshift(index, 1);
												Array.prototype.splice.apply(evidenceData.rows, newData);
												evidenceData.rows.forEach(function(item, i) {
													item.detailindex = {//多选重置 现金流量多选
														value: (i + 1).toString()
													};
													item.key = ++i;
												});
											}
											_this.getCurry(
												headFunc.pk_accountingbook.value,
												prepareddate.value,
												{ value: queryArry },
												'pk_accasoa',
												index
											);
										} else {
											evidenceData.rows[index][item.attrcode] = {
												...evidenceData.rows[index][item.attrcode],
												display: '',
												value: ''
											};
										}
										if (v&& v.length ==1&&evidenceData.rows[index].ass && evidenceData.rows[index].ass.length != '0') {
											//切换会计科目清空辅助核算
											evidenceData.rows[index].ass = [];
											evidenceData.rows[index].assid = {
												...evidenceData.rows[index].assid,
												display: '',
												value: ''
											};
										}

										//如果是非现金类科目，将现金流量清空
										if(Array.isArray(evidenceData.rows[index].cashflow)&&evidenceData.rows[index].cashflow.length!='0'){
											if(v&&v.length>0&&(v[0].cashtype=='1'||v[0].cashtype=='2'||v[0].cashtype=='3')){
												evidenceData.rows[index].cashflow=[];
												evidenceData.rows[index].cashflowname={value:'',display:''};
												evidenceData.rows[index].expand.cashflowname={value:'',display:''};
											}else{
												//将科目更新
												this.countCashFlow(evidenceData.rows[index]);
											}
										}
										//每一行的checkbox
										let checkAfter=[];
										evidenceData.rows.forEach((item, index) => {
											if(index==getrow-1){
												checkAfter.push(true);
											}else{
												checkAfter.push(false);
											}
										})
										_this.setState({
											expandRow:[],
											evidenceData,
											checkedArray:checkAfter
										});
									}}
								/>
								</div>
							) : (
								<div class='accasoaClass' fieldid={item.attrcode}>{text&&text.display?text.display:<span>&nbsp;</span>}</div>
							);
						}
						break;
					case 'number':
						if (item.attrcode == 'amount') {
							let { checkedNumber } = record;
							return !voucherView ? (
								<HotKeys
									keyMap={{
										'amountNode': 'f2'
									  }}
									attach={false}
									handlers={{'amountNode':this.changeNumberClick.bind(this, record.key)}}
								>
								<div className="refer-input-wraper" id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`}>
									<NCNumber
									renderInTable={true}
									onKeyDown={(event)=>{
										if(event.keyCode=='38'|| event.key === "ArrowUp"||event.keyCode=='40'|| event.key === "ArrowDown"){// 40 ArrowDown 38 ArrowUp
											onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
										}
									}}
										onEnter={(event)=>{
											onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
										}}
										fieldid={item.attrcode}
										scale={record.amount&&record.amount.scale ? Number(record.amount.scale) : Number(record.scale)}
										disabled={
											record[item.attrcode]&&record[item.attrcode].editable ? record[item.attrcode].editable : false
										}
										value={text?text.value:''}
										onFocus={() => {
											let _this = this;
											let { prepareddate, evidenceData, headFunc, freeValue } = _this.state;
											//摘要最后一行新增空行
											if (record.key == evidenceData.rows.length) {
												//在最后一行点击，自动新增一行
												if (index >= 1) {
													evidenceData.rows[index].explanation.display =
														evidenceData.rows[index - 1].explanation.display;
													evidenceData.rows[index].explanation.value =
														evidenceData.rows[index - 1].explanation.value;
												}
												if(!this.offsetButton && !(saveData.detailmodflag && saveData.detailmodflag.value === false)){
													this.addNewLine(rows.length);
												}
											}
										}}
										placeholder={checkedNumber ? this.state.json['20021005card-000066'] : ''}/* 国际化处理： 请输入数字*/
										onKeyUp={(event) => {
										}}
										onBlur={(v) => {
											if (v) {
												if(record.memoryValue&&v==record.memoryValue.amount&&record.localdebitamount.value){
													return false; 
												}
												let countValue = amountconvert(data, currInfo, 'amount');
												if (countValue.message) {
													toast({ content: countValue.message, color: 'warning' });
												}
												countRow(record, countValue, 'amount');
												memoryHistry(record)
												//更新现金流量cashflow
												if(record.cashflow&&record.cashflow.length!=0){
													this.countCashFlow(record);
												}

											}
											this.totleCount(item.attrcode, 'localdebitamountTotle');
										}}
										onChange={(v) => {
											// let flag = this.numCheck(v, 4);//录入校验
											// if (!flag) {
											// 	originData.checkedNumber = true;
											// 	// this.setState({
											// 	// 	evidenceData
											// 	// });
											// 	return false;
											// }
											//let { rows } = this.state.evidenceData;
											//let originData = this.findByKey(record.key, rows);
											if (record) {
												record[item.attrcode].value=v
											}
											this.setState({
												evidenceData: this.state.evidenceData
											});
										}}
									/>

									{!(record[item.attrcode]&&record[item.attrcode].editable) ? (
										<i
											className="iconfont icon-canzhaozuixin"
											onClick={this.changeNumberClick.bind(this, record.key)}
										/>
									) : null}
								</div>
								</HotKeys>
							) : (
								<p class='curryClass'><div fieldid={item.attrcode}>{text?text.value&&self.commafy(text.value) : <span>&nbsp;</span>}</div></p>
							);
						} else if (item.attrcode == 'localdebitamount') {
							return !voucherView ? (
								<div className="textCenter" id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`} fieldid={item.attrcode}>
									<NCNumber
									renderInTable={true}
									onKeyDown={(event)=>{
										if(event.keyCode=='38'|| event.key === "ArrowUp"||event.keyCode=='40'|| event.key === "ArrowDown"){// 40 ArrowDown 38 ArrowUp
											onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
										}
									}}
										onEnter={(event)=>{
											onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
										}}
										fieldid={item.attrcode}
										scale={record.localdebitamount&&record.localdebitamount.scale ? Number(record.localdebitamount.scale) :  Number(record.orgscale)}
										disabled={
											record[item.attrcode]&&record[item.attrcode].editable ? record[item.attrcode].editable : false
										}
										value={text.value && text.value != '0' ? text.value : ''}
										onFocus={(v) => {
											record[item.attrcode].visible = true;
											this.getRow({key:index+1},index)
										}}
										onBlur={(v) => {
											record[item.attrcode].visible = false;
											if (v && v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
												if(record.memoryValue&&v==record.memoryValue.localdebitamount || saveData.pk_system.value=='EGL'){
													return false; 
												}
												data.localamount = record.localdebitamount.value;
												let countValue = amountconvert(data, currInfo, 'localdebitamount');
												if (countValue.message) {
													toast({
														content: countValue.message,
														color: 'warning',
														duration: 5
													});

												}
												countRow(record, countValue, 'localdebitamount');
												memoryHistry(record)//记录之前值防止反算问题
												if(record.cashflow&&record.cashflow.length!=0){
													this.countCashFlow(record);
												}
											}
											this.totleCount(item.attrcode, item.attrcode + 'Totle');
										}}
										onKeyUp={(event) => {
											let {
												localcreditamountTotle,
												localdebitamountTotle
											} = this.state.evidenceData;
											let originData = this.findByKey(record.key, rows);
											if (
												event.keyCode == '187' ||
												(event.keyCode == '61' && originData.excrate2.value)//兼容火狐
											) {
												onEqual.call(this,item.attrcode,record);
												// this.equal(item.attrcode, originData);
												// let { data, currInfo } = this.exportData(record);//键盘输入等号，直接保存
												// let countValue = amountconvert(data, currInfo, item.attrcode);
												// countRow(record, countValue,  item.attrcode);
												// memoryHistry(record)//记录之前值防止反算问题
												// if(record.cashflow&&record.cashflow.length!=0){
												// 	this.countCashFlow(record);
												// }
												this.totleCount(item.attrcode, 'localdebitamountTotle');
											} else if (event.keyCode == '32') {// && originData.excrate2.value  why add
												emptyEnter(item.attrcode, originData);
												updateDirection(originData)
												if(record.cashflow&&record.cashflow.length!=0){
													this.countCashFlow(record);
												}
												this.totleCount(item.attrcode, 'localdebitamountTotle');
											}
										}}
										onChange={(v) => {
											let self = this;
											let { evidenceData } = self.state;
										
											//if (v !==  && v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
												if (record) {
													record[item.attrcode].value = v
												}
												this.setState({
													evidenceData
												});
											//}
										}}
									/>
								</div>
							) : (
								<div class='curryClass' fieldid={item.attrcode}>
									{text.value?self.commafy(text.value):<span>&nbsp;</span>}
									<Rate rate={record.excrate2.value || ''} ifShow={record[item.attrcode]} json={self} />
								</div>
							);
						} else if (item.attrcode == 'localcreditamount') {
							return !voucherView ? (
								<div className="textCenter" id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`} fieldid={item.attrcode}>
									<NCNumber
									renderInTable={true}
									onKeyDown={(event)=>{
										if(event.keyCode=='38'|| event.key === "ArrowUp"||event.keyCode=='40'|| event.key === "ArrowDown"){// 40 ArrowDown 38 ArrowUp
											onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
										}
									}}
										onEnter={(event)=>{
											onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
										}}
										fieldid={item.attrcode}
										scale={record.localcreditamount&&record.localcreditamount.scale ? Number(record.localcreditamount.scale) :  Number(record.orgscale)}
									//	scale={originData.orgscale ? Number(originData.orgscale) : 2}
										disabled={
											record[item.attrcode]&&record[item.attrcode].editable ? record[item.attrcode].editable : false
										}
										value={text.value && text.value != '0' ? text.value : ''}
										onFocus={(v) => {
											record[item.attrcode].visible = true;
											this.getRow({key:index+1},index)
										}}
										onBlur={(v) => {
											record[item.attrcode].visible = false;
											if (v&&v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
												if(record.memoryValue&&v==record.memoryValue.localcreditamount || saveData.pk_system.value=='EGL'){//汇兑损益特殊处理
													return false; 
												}
												data.localamount = record.localcreditamount.value;
												let countValue = amountconvert(data, currInfo, 'localcreditamount');
												if (countValue.message) {
													toast({
														content: countValue.message,
														color: 'warning',
														duration: 5
													});
												}
												countRow(record, countValue, 'localcreditamount');
												memoryHistry(record)
												if(record.cashflow&&record.cashflow.length!=0){
													this.countCashFlow(record);
												}
											}
											this.totleCount(item.attrcode, item.attrcode + 'Totle');
										}}
										onKeyUp={(event) => {
											let {
												localcreditamountTotle,
												localdebitamountTotle
											} = this.state.evidenceData;
											//	let originData = this.findByKey(record.key, rows);
											if (
												event.keyCode == '187' ||
												(event.keyCode == '61' && record.excrate2.value)
											) {
												onEqual.call(this,item.attrcode,record);
												// this.equal(item.attrcode, record);
												// let { data, currInfo } = this.exportData(record);//键盘输入等号，直接保存
												// let countValue = amountconvert(data, currInfo, item.attrcode);
												// countRow(record, countValue,  item.attrcode);
												this.totleCount(item.attrcode, 'localdebitamountTotle');
											} else if (event.keyCode == '32') {// && record.excrate2.value why add
												emptyEnter(item.attrcode, record);
												updateDirection(record)//跟新借贷方向
												this.totleCount(item.attrcode, 'localdebitamountTotle');
											}
										}}
										onChange={(v) => {
											let { evidenceData } = this.state;
											//if ( v !==  && v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
												if (record) {
													record[item.attrcode].value=v
												}
												this.setState({
													evidenceData
												});
											//}
										}}
									/>
								</div>
							) : (
								<div class='curryClass' fieldid={item.attrcode}>
									{text.value?self.commafy(text.value):<span>&nbsp;</span>}
									<Rate rate={record.excrate2.value || ''} ifShow={record[item.attrcode]} json={self}/>
								</div>
							);
						}
						break;
					case 'num':
						return (
							<div className="textCenter" id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`} fieldid={item.attrcode}>
								<FormControl
									onEnter={(event)=>{
										onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
									}}
									fieldid={item.attrcode}
									value={text[item.key].value || ''}
									onChange={(v) => {
										let { rows } = this.state.evidenceData;
										let originData = this.findByKey(record.key, rows);
										if (originData) {
											originData.localcreditamount = {
												value: Number(v)
											};
											originData.localdebitamount = {
												value: ''
											};
											originData.amount = {
												value: Number(v)
											};
										}
										// this.setState({
										// 	evidenceData: this.state.evidenceData
										// });
										//this.totleCount('localcreditamount','creTotle');
									}}
								/>
								{errorMsg}
							</div>
						);

					case 'datepicker':
						return (
							<div className="textCenter" id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`}>
									{!voucherView ? (
										<div fieldid={item.attrcode}>
										<NCTZDatePickClientTime
											format={"YYYY-MM-DD"}
											onEnter={(event)=>{
												onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
											}}
											fieldid={item.attrcode}
											value={text&&text.value && text.value != '0' ? text.value : ''}
											disabled={
												record[item.attrcode]&&record[item.attrcode].editable ? record[item.attrcode].editable : false
											}
											// open={self.dateDisplay[index]}
											// onClick={(v)=>{
											// 	self.dateDisplay[index]=!self.dateDisplay[index];
											// }}
											// onOpenChange={(v)=>{
											// 	self.dateDisplay[index]=v;
											// }}
											onChange={(v) => {
												let { evidenceData } = this.state;
												if (record) {
													record[item.attrcode].value=v
													record[item.attrcode].display=v
													if(record.expand&&JSON.stringify(record.expand) != '{}'&&record.expand.verifydate){
														record.expand.verifydate.value=v
														record.expand.verifydate.display=v
													}
												}
												this.setState({
													evidenceData
												});
											}}
										/>
										</div>
									) : (
										<div fieldid={item.attrcode}>
											{text&&text.value?DongbaToLocalTime(moment(text.value)).format('YYYY-MM-DD'):<span>&nbsp;</span>}
										</div>
									)}
							</div>
						);
					default:
						break;
				}
			};
		});
		// data.unshift(dataFirst);
		// data.unshift(openRowCol);
	    let dataCloums=[dataFirst].concat(loadColums);
		return dataCloums;
	}

	//修改页面数据加载渲染
	echoData(data) {
		let self = this;
		let lastTotle = [];
		let {
			freeValue,
			attachment,
			evidenceData,
			evidenceColumns,
			headContent,
			footContent,
			saveData,
			prepareddate
		} = self.state;
		let loadColums = data.template.gl_voucher_detail.items;
		headContent = data.template.head.items;
		footContent = data.template.tail.items;

		for (let i = 0, len = evidenceData.rows.length; i < len; i++) {
			//高效率循环
			let item = evidenceData.rows[i];
			loadColums.forEach(function(v, k, a) {
				let objData = {
					display: '',
					value: '',
					editable: false
				};
				if (v.attrcode != 'cashflow') {
					//暂时先不保存现金流量
					item[v.attrcode] = objData;
				}
			});
			item.detailindex = {
				value: (i + 1).toString()
			};
			item.pk_unit = {
				//添加业务单元字段
				display: '',
				value: ''
			};
			item.groupdebitamount = {
				//添加集团本币借方发生额
				display: '',
				value: ''
			};
			item.groupcreditamount = {
				//添加集团本币贷方发生额
				display: '',
				value: ''
			};
			item.globaldebitamount = {
				//添加全局本币借方发生额
				display: '',
				value: ''
			};
			item.globalcreditamount = {
				//添加全局本币贷方发生额
				display: '',
				value: ''
			};
			item.debitquantity = {
				//数量
				display: '',
				value: ''
			};
			item.price = {
				//单价
				display: '',
				value: ''
			};
		}

		
		//新增行添加列信息
		loadColums.forEach(function(v, i, a) {
			if (detailExpend.includes(v.attrcode)) {
				//结算 往来核销 现金流量
				freeValue.push(v);
			}
			if (v.attrcode.indexOf('freevalue') != -1 && v.visible) {
				//分录自定义项
				freeValue.push(v);
			}
			let objnewData = {
				display: '',
				value: '',
				editable: false
			};
			if (v.attrcode != 'cashflow') {
				//暂时先不保存现金流量
				evidenceData.newLine[v.attrcode] = objnewData;
			}
			evidenceData.newLine.pk_unit = objnewData;
			evidenceData.newLine.groupdebitamount = objnewData;
			evidenceData.newLine.groupcreditamount = objnewData;
			evidenceData.newLine.globaldebitamount = objnewData;
			evidenceData.newLine.globalcreditamount = objnewData;
			evidenceData.newLine.debitquantity = objnewData;
			evidenceData.newLine.price = objnewData;
			evidenceData.newLine.detailindex = {
				value: ''
			};
		});

		loadColums = loadColums.filter(function(v, i, a) {
			return v.attrcode!='pk_unit_v'&&v.attrcode.indexOf('freevalue') == -1 && !detailExpend.includes(v.attrcode) && v.visible == true;
		});
		evidenceColumns = self.editeColumns(loadColums);
		headContent.forEach(function(v, i, a) {
			//修改模板数据为保存数据格式
			saveData[v.attrcode] =v.initialvalue?  {
				display: v.initialvalue.display,
				value: v.initialvalue.value
			}:{
				display: '',
				value: ''
			};
		});

		footContent.forEach(function(v, i, a) {
			saveData[v.attrcode] =  {
				display: '',
				value: ''
			};
		});

		self.setState({
			freeValue,
			headContent,
			footContent,
			evidenceColumns,
			attachment,
			saveData,
			evidenceData
		});
	}

	
	//新增行操作
	addNewLine(record) {
		// if(this.tableHeight=='350'){//动态计算表格高度,产生滚动条
		// 	let tableArea_H = this.refs.tableArea ? getComputedStyle(this.refs.tableArea, null).height : '400px';
		// 	let height = tableArea_H.replace('px', '');
		// 	this.tableHeight=height;
		// }else{
			// let tableArea= this.refs.tableArea ? document.getElementById('tableArea').offsetHeight : '400px';
			// let totalHeight=document.getElementById('totalLine').offsetHeight;
			// this.tableHeight=tableArea-totalHeight;
		// }
		let { evidenceData, saveData, checkedArray } = this.state;
		checkedArray = [];
		let originData = this.findByKey(record, evidenceData.rows);
		let newkey = ++record;
		//新增分录行带出上一行信息 摘要 币种 汇率 组织集团依赖
		if (originData) {
			evidenceData.newLine.explanation =  originData&&originData.explanation ? originData.explanation : { display: '', value: '' };
			evidenceData.newLine.explanation.editable=false;
			evidenceData.newLine.pk_unit = originData&&originData.pk_unit ? originData.pk_unit : { display: '', value: '' };
			evidenceData.newLine.pk_unit.editable=false;
			evidenceData.newLine.pk_unit_v = originData&&originData.pk_unit_v ? originData.pk_unit_v : { display: '', value: '' };
			evidenceData.newLine.pk_unit_v.editable=false;
			evidenceData.newLine.pk_currtype = originData&&originData.pk_currtype ? originData.pk_currtype : { display: '', value: '' };
			evidenceData.newLine.pk_currtype.editable=false;
			evidenceData.newLine.busidate = originData&&originData.busidate ? originData.busidate : { display: '', value: '' };
			evidenceData.newLine.busidate.editable=false;
			evidenceData.newLine.excrate2 = originData&&originData.excrate2 ? originData.excrate2 : { display: '', value: '' };
			evidenceData.newLine.excrate2.editable=false;
			evidenceData.newLine.excrate3 = originData&&originData.excrate3 ? originData.excrate3 : { display: '', value: '' };
			evidenceData.newLine.excrate3.editable=false;
			evidenceData.newLine.excrate4 = originData&&originData.excrate4 ? originData.excrate4 : { display: '', value: '' };
			evidenceData.newLine.excrate4.editable=false;
			evidenceData.newLine.groupType = originData&&originData.groupType ? originData.groupType : '';
			evidenceData.newLine.globalType = originData &&originData.globalType? originData.globalType : '';
			evidenceData.newLine.scale =  originData&&originData.scale?originData.scale:(saveData&&saveData.paraInfo? saveData.paraInfo.scale : '');
			evidenceData.newLine.orgscale = originData&&originData.orgscale?originData.orgscale:(saveData &&saveData.paraInfo? saveData.paraInfo.orgscale : '');
			evidenceData.newLine.groupscale = originData&&originData.groupscale?originData.groupscale:(saveData &&saveData.paraInfo? saveData.paraInfo.groupscale : '');
			evidenceData.newLine.globalscale = originData&&originData.globalscale?originData.globalscale:(saveData &&saveData.paraInfo? saveData.paraInfo.globalscale : '');
			evidenceData.newLine.pricescale =  originData&&originData.pricescale?originData.pricescale:(saveData&&saveData.paraInfo? saveData.paraInfo.pricescale : '');//数量精度依赖上一行
			evidenceData.newLine.detailindex.value = record.toString();
		}
		//联查余额添加核算账簿和会计期间字段
		evidenceData.newLine.pk_accountingbook = saveData.pk_accountingbook
			? saveData.pk_accountingbook
			: { display: '', value: '' };
		evidenceData.newLine.prepareddate = saveData.prepareddate ? saveData.prepareddate : { display: '', value: '' };

		let cloneNewLine = deepClone(evidenceData.newLine);
		let newLine = Object.assign({}, cloneNewLine, {
			key: newkey
		});
		if (newkey <= evidenceData.rows.length) {
			evidenceData.rows.map((item, index) => {
				if (item.key >= newkey) {
					item.key++;
				}
			});
		}
		evidenceData.rows.splice(newkey - 1, 0, newLine);
		evidenceData.rows.forEach((item, i) => {
			checkedArray.push(false);
		});
		this.dateDisplay=checkedArray;
		saveData.details=evidenceData.rows;//跟新saveData details 同步跟新现金流量
		this.setState({
			checkedArray,
			evidenceData,
			expandRow:[]
		},()=>{
			let tableScroll=document.getElementsByClassName('u-table-scroll')[0];
			let tr=tableScroll.querySelector('tbody.u-table-tbody tr:first-child');
			let tableHeight=0;
			if(tr.scrollHeight){
				tableHeight=tr.scrollHeight;
			}
			let tableFixedRight=document.getElementsByClassName('u-table-fixed-right')[0];
			let tableBodyScroll=tableFixedRight.querySelector('.u-table-body-inner');
			tableBodyScroll.scrollTop=tableHeight?tableHeight+tableScroll.offsetHeight:tableScroll.offsetHeight;
		});
	}

	//表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rt = rows.find((item) => item.key == key);
		return rt;
	}

	//格式化后台返回数据
	dataFormat = (data) => {
		let result = [];
		if (data) {
			if (data[0]) {
				data.map((item, index) => {
					item.key = index + 1;
					result.push(item);
				});
			}
		}
		return result;
	};

	//获取计算合计之后的数据
	getTableSumData = (data, amount, Totle) => {
		if (data.length != 0) {
			//需要合计的字段
			let { rows } = this.state.evidenceData;
			let newObj = this.state.evidenceData[Totle];
			let sumObj = {
				[amount]: []
			};
			for (let key in sumObj) {
				let arr = data.map((item) => {
					return item[key]&&item[key].value ? parseFloat(item[key].value) : 0;
				});
				//let totleValue= sum(arr.slice(0))
				newObj.value =sum(...arr).replace(/,/g, '')  
				// arr.reduce((prev, curr) => {
				// 	return  prev + curr;
				// });
			}
			return newObj;
		}
	};

	
	//主子表信息 点击展开触发
	getData = (expanded, record) => {
		let self = this;
		let { expandRow, getrow, voucherView, headFunc, evidenceData, prepareddate } = self.state;
		if (expanded) {
			let originData = self.findByKey(record.key, evidenceData.rows);
			if (record.pk_accasoa.value && headFunc.pk_accountingbook.value && prepareddate.value) {
				let url = '/nccloud/gl/voucher/detailExpend.do';
				let data = {
					pk_accountingbook: headFunc.pk_accountingbook.value,
					pk_accasoa: record.pk_accasoa.value,
					prepareddate: prepareddate.value
				};
				ajax({
					url,
					data: data,
					async: false,
					success: function(response) {
						let { freeValue ,saveData} = self.state;
						let settle = response.data.settle; //是否显示结算信息
						let verify = response.data.verify; //往来核销信息是否显示
						if (!settle) {
							freeValue = freeValue.filter((v, i, a) => {
								return !settkleArray.includes(v.attrcode);
							});
						}
						if (!verify) {
							freeValue = freeValue.filter((v, i, a) => {
								return !verifyArray.includes(v.attrcode);
							});
						}
						originData.childform = freeValue;
						if (originData.expand && JSON.stringify(originData.expand) != '{}') {
							originData.childform.map((e, i) => {
								if (e.attrcode == 'cashflow') {
									originData.expand['cashflowname'] = {
										value: originData.expand.cashflowname?originData.expand.cashflowname.value:'',
										display: originData.expand.cashflowname?originData.expand.cashflowname.display:''
									};
								} else {
									originData.expand[e.attrcode] = {
										...originData.expand[e.attrcode],
										value:  originData.expand[e.attrcode]&&originData.expand[e.attrcode].value,
										display:  originData.expand[e.attrcode]&&originData.expand[e.attrcode].display
									};
								}
							});
						} else {
							originData.expand = {}; //新增凭证添加expand字段赋默认值
							originData.childform.map((e, i) => {
								if (e.attrcode == 'cashflow') {
									originData.expand['cashflowname'] = {
										value: '',
										display: ''
									};
								} else {
									if(e.attrcode=='checkdate'||e.attrcode=='verifydate'){
										originData.expand[e.attrcode] = {
											...originData.expand[e.attrcode],
											value: saveData.prepareddate.value,
											display: ''
										};
										originData[e.attrcode] = {
											...originData[e.attrcode],
											value: saveData.prepareddate.value,
											display: ''
										};
									}else{
										originData.expand[e.attrcode] = {
											...originData.expand[e.attrcode],
											value: '',
											display: ''
										};
									}
								}
							});
						}
						self.setState(
							{
								evidenceData
							}
						);
					}
				});
			}
		}
		if (expandRow.includes(record.key)) {
			expandRow = expandRow.filter(function(v, i, a) {
				return v != record.key;
			});
		} else {
			expandRow.push(record.key);
		}
		this.setState({
			expandRow
		});
	};
	//表体点击行事件
	getRow = (expanded, record) => {
		let { getrow, saveData, evidenceData: { rows }, voucherView,checkedArray } = this.state;
		//复选框跟新
		 let checkArrayClone= this.Changecheck(record,checkedArray);
		//点选复选框表体按钮可用
		//if(!!!getrow){
			buttonStatusDetails(this.props,true,this.state.saveData)
	//	}
		//现金流量更新选中行
		this.selectedIndex=record;
		//分录行点击校验
		if (
			getrow &&
			getrow != expanded.key &&
			!voucherView &&
			expanded.key != 1 &&rows[getrow - 1]&&rows[getrow - 1].pk_accasoa&&
			rows[getrow - 1].pk_accasoa.value
		) {
			//只有会计科目有值才发校验请求
			let self = this;
			let cutRowData=[];
			cutData(cutRowData,rows)
			saveData.details = cutRowData;
			saveData['detailindex'] = {
				value: record
			};
			let url = '/nccloud/gl/voucher/checkdetail.do';
			ajax({
				url,
				data: saveData,
				success: function(res) {
					let { evidenceData } = self.state;
					let { rows } = evidenceData;
					let originData = self.findByKey(getrow - 1, rows);
					if (res.data.message) {
						originData.showCheck = true;
						originData['checkmessage'] = {
							value: res.data.message
						};
					} else {
						originData.showCheck = false;
						originData['checkmessage'] = {
							value: ''
						};
					}
					self.setState({
						evidenceData
					});
				}
			});
		}
		//跟新选中行
		getrow = expanded.key;
		this.setState({
			checkedArray:checkArrayClone,
			getrow: getrow.toString()
		});
	};

	//表格复选框修改
	Changecheck = (index,checkedArray) => {
		let self = this;
		let checkedArraynew = checkedArray.concat();
		if (!checkedArraynew[index]) {
			checkedArraynew[index] = !checkedArray[index];
		}
		// console.time('aaa')
		for (var i = 0,len=checkedArray.length; i < len; i++) {
			if (i != index) {
				checkedArraynew[i] = false;
			}
		}
		// console.timeEnd('aaa')
		return checkedArraynew
	};



	//子表展开数据展示
	expandedRowRender = (record, index, indent) => {
		let self = this;
		let { voucherView, headFunc, prepareddate, evidenceData ,saveData} = self.state;
		return (
			<FormItemTab
				voucherView={voucherView}
				defaultValue={record.expand}
				style={{height:99}}
				pk_org={record.pk_unit.value}
				prepareddate={saveData.prepareddate.value}
				tabs={record.childform}
				propsForm={self.props}
				freeKey={record.key}
				OnChange={(i, v, e) => {
					let { rows } = this.state.evidenceData;
					let originData = this.findByKey(e, rows);
					if (originData&&originData.expand) {
						if (i == 'cashflow') {
							//现金流量赋值
							originData.expand.cashflowname.value = v;
							originData.cashflowname.value = v;
						} else {
								if (isObj(v)) {
									originData.expand[i].value = v.refpk;
									originData.expand[i].display = v.refname;
									if(originData[i]){
										originData[i].value = v.refpk; //同时在expand　外赋值便于后端取 区别新增和修改
										originData[i].display = v.refname;
									}else{
										originData[i]={
											value:v.refpk,
											display:v.refname
										}
									}
								} else {
									originData.expand[i].value = v;
									if(originData[i]){
										originData[i].value = v;
									}else{
										originData[i]={
											value:v
										}
									}
									if(i=='verifydate'){
										originData.busidate={value:v,display:v}
									}
								}
						}
					}
					this.setState({
						evidenceData
					});
				}}
			/>
		);
	};

	

	//合计计算
	totleCount = (userdata, Totle) => {
		let _this = this;
		let { rows } = _this.state.evidenceData;
		let { evidenceData,isNC01,isNC02 } = _this.state;
		// let expenseData = rows.map((e) => {
		// 	return e;
		// });

		let expenseData = _this.dataFormat(rows);
		let sumData = _this.getTableSumData(expenseData, 'localcreditamount', 'localcreditamountTotle'); //计算组织合计数据
		_this.getTableSumData(expenseData, 'localdebitamount', 'localdebitamountTotle'); //计算合计数据
		//判断是否有集团全局列
		if(isNC01){
			_this.getTableSumData(expenseData, 'groupdebitamount', 'groupdebitamountTotle'); //计算集团合计数据
			_this.getTableSumData(expenseData, 'groupcreditamount', 'groupcreditamountTotle'); //计算合计数据
		}
		if(isNC02){
			_this.getTableSumData(expenseData, 'globaldebitamount', 'globaldebitamountTotle'); //计算全局合计数据
			_this.getTableSumData(expenseData, 'globalcreditamount', 'globalcreditamountTotle'); //计算合计数据
		}
		
		if (sumData) {
			_this.setState({
				evidenceData
			});
			//this.forceUpdate()
		}
	};


	//点击原币弹出修改汇率 精度模态框
	changeNumberClick = (e) => {
		let self = this;
		let { evidenceData, findRows ,saveData} = self.state;
		let originData = deepClone(this.findByKey(e, evidenceData.rows)); //避免修改原对象数组，对其他产生影响
		let paraInfo=saveData.paraInfo;
		if(!originData.amount.value){
			let newcurrinfo=queryCurrinfo(saveData.pk_accountingbook.value||originData.pk_accountingbook.value,originData.pk_currtype.value,saveData.prepareddate.value)
			if(originData.excrate2){
				originData.excrate2.value=newcurrinfo.excrate2
			}
			if(originData.excrate3){
				originData.excrate3.value=newcurrinfo.excrate3
			}
			if(originData.excrate4){
				originData.excrate4.value=newcurrinfo.excrate4
			}

		}

		if (!originData.flag) {
			//传入子组件字段控制
			delete originData.debitquantity;
			delete originData.price;
		}
		// if (originData.excrate3 && !originData.excrate3.value) {
		// 	//传入子组件字段控制
		// 	delete originData.excrate3;
		// }
		// if (originData.excrate4 && !originData.excrate4.value) {
		// 	//传入子组件字段控制
		// 	delete originData.excrate4;
		// }

		// if (!originData.groupType) {
		// 	delete originData.groupdebitamount;
		// 	delete originData.groupcreditamount;
		// }
		// if (!originData.globalType) {
		// 	delete originData.globaldebitamount;
		// 	delete originData.globalcreditamount;
		// }
		//if (!originData.groupType&&originData.groupdebitamount&&!originData.groupdebitamount.value) {
		if(!(paraInfo&&paraInfo.NC001)){
			delete originData.excrate3;	
			delete originData.groupdebitamount;
			delete originData.groupcreditamount;
		}
		//if (!originData.globalType&&originData.globaldebitamount&&!originData.globaldebitamount.value) {
		if(!(paraInfo&&paraInfo.NC002))	{
			delete originData.excrate4;		
		  delete originData.globaldebitamount;
			delete originData.globalcreditamount;
		}

		if (findRows.length == 0) {
			findRows.push(originData);
		} else {
			findRows = [];
			findRows.push(originData);
		}
		let changeNumberShow = true;
		this.setState({
			changeNumberShow,
			findRows
		});
	};
	//辅助核算点击弹出模态框
	handleAssistAcc = (index) => {
		//ass
		let { isFreevalueDefault, saveData, unitOrg, evidenceData,currentRowPrevass } = this.state;
		let prepareddate = saveData.prepareddate.value;
		let pk_accountingbook = saveData.pk_accountingbook.value;
		let pk_unit = unitOrg.value;
		let pk_accasoa = '';
		let rows = evidenceData.rows;

		if (isFreevalueDefault && index != '1' && rows[index - 2].ass && rows[index - 2].ass.length != 0) {
			// rows[index - 1]['ass'] = rows[index - 2].ass;
			currentRowPrevass=rows[index - 2].ass;
		}else{
			currentRowPrevass=[];
		}
		for (let i = 0, len = rows.length; i < len; i++) {
			if (rows[i].key == index) {
				pk_accasoa = this.state.evidenceData.rows[i].pk_accasoa.value;
			}
		}
		if (!pk_accountingbook) {
			toast({ content: this.state.json['20021005card-000068'], color: 'warning' });/* 国际化处理： 请选择核算账簿*/
			return false;
		}
		if (!pk_accasoa) {
			toast({ content: this.state.json['20021005card-000069'], color: 'warning' });/* 国际化处理： 请选择会计科目*/
			return false;
		}
		if (!prepareddate) {
			toast({ content: this.state.json['20021005card-000070'], color: 'warning' });/* 国际化处理： 请选择制单日期*/
			return false;
		}
		this.setState({
			evidenceData,
			getrow: index,
			AssistAccModalShow: true,
			currentRowPrevass
		});
	};


	//修改汇率和原币值
	changeUnit = (e) => {
		let { evidenceData } = this.state;
		let { rows } = evidenceData;
		rows = rows.map((t) => {
			return t.key === e[0].key ? e[0] : t;
		});
		evidenceData.rows = rows;
		this.setState(
			{
				evidenceData,
				changeNumberShow: false
			}
		);
	};

	
	//分摊规则请求
	ApportAction = (MsgModalAll, e) => {
		let { saveData, evidenceData, nov, attachment, getrow, update} = this.state;
		let newclone = deepClone(evidenceData.rows);
		// let newclone = cloneNewLine.filter(function(v, i, a) {
		// 	return !(v.pk_accasoa.value == '' && v.explanation.value == '');
		// });
		// let newclone = cloneNewLine.filter(function(v, i, a) {
		// 	//分录行过滤数据
		// 	return (
		// 		(v.localcreditamount.value && v.localcreditamount.value != '0') ||
		// 		(v.localdebitamount.value && v.localdebitamount.value != '0') ||
		// 		(v.debitquantity && v.debitquantity.value && v.debitquantity.value != '0')
		// 	);
		// });
		nov.value = Number(nov.value);
		attachment.value = Number(attachment.value);
		saveData.details = newclone;
		saveData.selectIndex = (getrow - 1).toString();
		saveData.apportionAmount = e.apportionAmount.toString();
		saveData.pk_apprule = e.pk_apprule;
		let url = '/nccloud/gl/voucher/apportion.do';
		ajax({
			url,
			data: saveData,
			success: (response) => {
				if(response.data){
					let {voucher, paraInfo }=response.data
					this.apportStatus=true;
					this.loadVoucher(voucher, paraInfo);
				}
			}
		});
		this.setState(
			{
				apportionmentShow: false
			}
		);
	};

	
	//科目辅助核算拆行更新字段值
	addRowClear=(pk_detail,amount,debitquantity,price,localdebitamount,localcreditamount,groupcreditamount,
		groupdebitamount,globalcreditamount,globaldebitamount,rowsData,addRows
	)=>{
		 //分录主键,新增行清空
			pk_detail.display=rowsData?addRows.pk_detail&&addRows.pk_detail.display:'';
			pk_detail.value=rowsData?addRows.pk_detail&&addRows.pk_detail.value:'';
			pk_detail.scale=rowsData?addRows.pk_detail&&addRows.pk_detail.scale:'';
		//分录原币,新增行清空
			amount.display=rowsData?addRows.amount&&addRows.amount.display:'';
			amount.value=rowsData?addRows.amount&&addRows.amount.value:'';
			amount.scale=addRows.amount&&addRows.amount.scale
		//分录原币,新增行清空
			debitquantity.display=rowsData?addRows.debitquantity&&addRows.debitquantity.display:'';
			debitquantity.value=rowsData?addRows.debitquantity&&addRows.debitquantity.value:'';
			debitquantity.scale=addRows.debitquantity&&addRows.debitquantity.scale
		//分录原币,新增行清空
			price.display=rowsData?addRows.price&&addRows.price.display:'';
			price.value=rowsData?addRows.price&&addRows.price.value:'';
			price.scale=addRows.price&&addRows.price.scale
		//分录原币,新增行清空
			localdebitamount.display=rowsData?addRows.localdebitamount&&addRows.localdebitamount.display:'';
			localdebitamount.value=rowsData?addRows.localdebitamount&&addRows.localdebitamount.value:'';
			localdebitamount.scale=addRows.localdebitamount&&addRows.localdebitamount.scale
		//分录原币,新增行清空
			localcreditamount.display=rowsData?addRows.localcreditamount&&addRows.localcreditamount.display:'';
			localcreditamount.value=rowsData?addRows.localcreditamount&&addRows.localcreditamount.value:'';
			localcreditamount.scale=addRows.localcreditamount&&addRows.localcreditamount.scale
		//分录原币,新增行清空
			groupcreditamount.display=rowsData?addRows.groupcreditamount&&addRows.groupcreditamount.display:'';
			groupcreditamount.value=rowsData?addRows.groupcreditamount&&addRows.groupcreditamount.value:'';
			groupcreditamount.scale=addRows.groupcreditamount&&addRows.groupcreditamount.scale
		//分录原币,新增行清空
			groupdebitamount.display=rowsData?addRows.groupdebitamount&&addRows.groupdebitamount.display:'';
			groupdebitamount.value=rowsData?addRows.groupdebitamount&&addRows.groupdebitamount.value:'';
			groupdebitamount.scale=addRows.groupdebitamount&&addRows.groupdebitamount.scale
		//分录原币,新增行清空
			globalcreditamount.display=rowsData?addRows.globalcreditamount&&addRows.globalcreditamount.display:'';
			globalcreditamount.value=rowsData?addRows.globalcreditamount&&addRows.globalcreditamount.value:'';
			globalcreditamount.scale=addRows.globalcreditamount&&addRows.globalcreditamount.scale
		//分录原币,新增行清空
			globaldebitamount.display=rowsData?addRows.globaldebitamount&&addRows.globaldebitamount.display:'';
			globaldebitamount.value=rowsData?addRows.globaldebitamount&&addRows.globaldebitamount.value:'';
			globaldebitamount.scale=addRows.globaldebitamount&&addRows.globaldebitamount.scale
	}



	// 辅助核算点确定后续操作
	handleAssistAccModal = (MsgModalAll, assDatas) => {
		let self = this;
		let { getrow, evidenceData,saveData } = self.state;
		let { rows } = self.state.evidenceData;
		if (Array.isArray(assDatas)) {
			let newData = assDatas.map((item,i) => {
				let assid = {
					display: item.assname,
					value: item.assid
				};
				let	rowsData;
				if(i=='0'){
					rowsData = evidenceData.rows.find((rowKey) => item.assid == rowKey.assid.value&&getrow==rowKey.key)||evidenceData.rows[getrow-1];
				}
				let  addRows=rowsData?rowsData:evidenceData.rows[getrow-1];
				let quantityscale={
					value:item.quantityscale?item.quantityscale:''
				}
				//debitquantity globalcreditamount globaldebitamount groupcreditamount localdebitamount
				//groupdebitamount localcreditamount price
				let pk_detail={
					value:'',
					display:'',
					scale:''
				};
				let amount={
					value:'',
					display:'',
					scale:''
				};
				let debitquantity={
					value:'',
					display:'',
					scale:''
				};
				let price={
					value:'',
					display:'',
					scale:''
				};
				let localdebitamount={
					value:'',
					display:'',
					scale:''
				};
				let localcreditamount={
					value:'',
					display:'',
					scale:''
				};
				let groupcreditamount={
					value:'',
					display:'',
					scale:''
				};
				let groupdebitamount={
					value:'',
					display:'',
					scale:''
				};
				let globalcreditamount={
					value:'',
					display:'',
					scale:''
				};
				let globaldebitamount={
					value:'',
					display:'',
					scale:''
				}
				self.addRowClear(
					pk_detail,amount,debitquantity,price,localdebitamount,localcreditamount,groupcreditamount,
					groupdebitamount,globalcreditamount,globaldebitamount,rowsData,addRows
				)
				if(item.quantityscale){//覆盖数量精度
					debitquantity.scale=item.quantityscale
					addRows.quantity={value:item.quantityscale}
				}

				return {
					...addRows,
					ass: JSON.stringify(item.assData) != '{}' ? item.assData : null, //辅助核算为{}  传 null
					//quantityscale:item.quantityscale?quantityscale:'',
					cashflow:Array.isArray(addRows.cashflow)&&addRows.cashflow.length!='0'?
					addRows.cashflow.map((item,i)=>{
						item.assid=assid
						return item
					}):null,
					assid:assid,
					pk_detail,//清空组件
					amount,debitquantity,price,groupcreditamount,localcreditamount, globalcreditamount, globaldebitamount,
					localdebitamount,groupdebitamount,assid
				};
			});
			newData.unshift(getrow - 1, 1);
			Array.prototype.splice.apply(rows, newData);
			rows.forEach(function(item, index) {
				item.detailindex = {//多选重置 现金流量多选
					value: (index + 1).toString()
				};
				item.key = ++index;
			});
			saveData.details=rows;
			self.setState({
				saveData,
				AssistAccModalShow: false,
				evidenceData: self.state.evidenceData
			});
		} else {
			rows.forEach(function(item, index) {
				if (item.key == accIndex) {
					item.assid.display = '';
					item.assid.value = '';
				}
			});
			self.setState({
				AssistAccModalShow: false,
				evidenceData: self.state.evidenceData
			});
		}
		// if(Array.isArray(assDatas.selectValues[i])){
		// 	assDatas.selectValues[i].map((e,i)=>{
		// 		selectpk+=e.refpk+',';
		// 		selectcode+=e.refcode+',';
		// 		selectname+=e.refname+',';
		// 	})
		// }
		// selectpk=selectpk.substring(0,selectpk.length-1);
		// selectcode=selectcode.substring(0,selectcode.length-1)
		// selectname=selectname.substring(0,selectname.length-1)
	};

	//详情页添加数量列
	addNumber = (evidenceColumns) => {
		let self = this;
		let quantity = {
			title: (<span fieldid="quantity">{self.state.json['20021005card-000071']}</span>),/* 国际化处理： 数量/单价*/
			key: 'quantity',
			dataIndex: 'quantity',
			width: '170px',
			render: (text, record, index) => {
				let { pk_accountingbook, prepareddate, voucherView } = self.state;
				let { rows } = self.state.evidenceData;
				let originData = self.findByKey(record.key, rows);
				let { data, currInfo } = self.exportData(originData);
				let attrcode = 'quantity';
				let attrcode1 = 'price';
				if (record.flag) {
					if (!voucherView) {
						return (
							<div className="number">
							<div id={`hot-key-${this.moduleId}-${attrcode}-${index}`}>
								<NCNumber
									renderInTable={true}
									onEnter={(event)=>{
										onEnterAction(event,this,attrcode,`hot-key-${this.moduleId}-${attrcode}-${index}`,index,record)
									}}
									fieldid="count"
									className="count"
									disabled={
										record['debitquantity']&&record['debitquantity'].editable ? record['debitquantity'].editable : false
									}
									scale={record.debitquantity&&record.debitquantity.scale ? Number(record.debitquantity.scale) : (record.quantityscale&&record.quantityscale.value? Number(record.quantityscale.value):2)}
									value={record.debitquantity&&record.debitquantity.value || ''}
									placeholder={self.state.json['20021005card-000043']}/* 国际化处理： 请输入数量*/
									//disabled={record.debitquantity.value!='0'?false:true}
								//	onKeyUp={(v) => {}}
									onBlur={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (v) {
											let countValue = amountconvert(data, currInfo, 'debitquantity');
											countRow(originData, countValue, 'debitquantity');
										}
										self.setState({
											evidenceData
										});
									}}
									onChange={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (originData) {
											originData.debitquantity = {
												...originData.debitquantity,
												value: v
											};
										}
										self.setState(
											{
												evidenceData
											}
										);
									}}
								/>
								</div>
								{/* <span>{data.name.text}</span> */}
								<div id={`hot-key-${this.moduleId}-${attrcode1}-${index}`}>
								<NCNumber
									renderInTable={true}
									onEnter={(event)=>{
										onEnterAction(event,this,attrcode1,`hot-key-${this.moduleId}-${attrcode1}-${index}`,index,record)
									}}
									fieldid="price"
									className="price"
									scale={record.price&&record.price.scale? Number(record.price.scale) : (record.pricescale&&Number(record.pricescale.value))||record.pricescale}
									disabled={record['price']&&record['price'].editable ? record['price'].editable : false}
									value={record.price&&record.price.value || ''}
									placeholder={self.state.json['20021005card-000045']}/* 国际化处理： 请输入单价*/
								//	onKeyUp={(v) => {}}
									onBlur={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (v) {
											let countValue = amountconvert(data, currInfo, 'price');
											countRow(originData, countValue, 'price');
											this.totleCount('price', 'localdebitamountTotle');
										}
										self.setState({
											evidenceData
										});
									}}
									onChange={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (originData) {
											originData.price = {
												...originData.price,
												value: v
											};
										}
										self.setState({
											//evidenceColumns:cloneNewLine,
											evidenceData
										});
									}}
								/>
								</div>
							</div>
						);
					} else {
						return (
							<div fieldid="price">
								{record.debitquantity.value || ''}/{record.price.value || ''}
							</div>
						);
					}
				} else {
					return <div fieldid="price"><span>&nbsp;</span></div>;
				}
			}
		};
		//判断是否有数量这一列的
		let originData = self.findByKey(quantity.key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			//let cloneNewLine = deepClone(evidenceColumns);
			let cloneNewLine=evidenceColumns;
			for(let i=0,len=cloneNewLine.length;i<len;i++){
				if(cloneNewLine[i].attrcode=='pk_currtype'){
					cloneNewLine.splice(i+1, 0, quantity);
					break;
				}
			}
			evidenceColumns = cloneNewLine;
		}
		return evidenceColumns;
	};

	//详情页添加业务单元列

	addUnit = (evidenceColumns) => {
		let self = this;
		let number = {
		title: (<span fieldid="pk_unit" >{self.state.json['20021005card-000072']}</span>),/* 国际化处理： 业务单元*/
			attrcode :'pk_unit',
			key: 'pk_unit',
			dataIndex: 'pk_unit',
			width: '170px',
			render: (text, record, index) => {
				//"uapbd/refer/org/BusinessUnitTreeRef"
				let { evidenceColumns, voucherView } = self.state;
				let referUrl = 'uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef/index.js';
				let attrcode = 'pk_unit';
				let editable =
					record['pk_unit'] && record['pk_unit'].editable ? record['pk_unit'].editable : false;
				return !voucherView ? (
					<div id={`hot-key-${this.moduleId}-${attrcode}-${index}`}>
					<ReferLoader
						onEnter={(event)=>{
							onEnterAction(event,this,attrcode,`hot-key-${this.moduleId}-${attrcode}-${index}`,index,record)
						}}
						fieldid="pk_unit"
						tag={attrcode}
						refcode={referUrl}
						disabled={editable}
						value={{ refname: (text && text.display) || '', refpk: (text && text.value) || '' }}
						queryCondition={() => {
							return {
								pk_accountingbook: self.state.headFunc.pk_accountingbook.value,
								VersionStartDate: self.state.prepareddate.value,
								TreeRefActionExt: 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder',
								isDataPowerEnable: 'Y',
								DataPowerOperationCode: 'fi'
							};
						}}
						onChange={(v) => {
							let { rows } = this.state.evidenceData;
							let originData = this.findByKey(record.key, rows);
							if (originData && originData.pk_unit_v.value) {
								//切换业务单元清空辅助核算
								if (originData.ass && originData.ass.length != '0') {
									originData.ass = [];
									originData.assid = {
										...originData.assid,
										display: '',
										value: ''
									};
									if(originData.cashflow&&originData.cashflow.length!='0'){//同步清空现金流量值
										originData.cashflow[0].assid.value='';
										originData.cashflow[0].assid.display='';
									}
								}
							}
							if (originData) {
								originData.pk_unit_v = {
									display: v.refname ? v.refname : '',
									value: v.values ? v.values.pk_vid.value : ''
								};
								originData.pk_unit = {
									display: v.refname ? v.refname : '',
									value: v.refpk ? v.refpk : ''
								};
								if(originData.cashflow&&originData.cashflow.length!='0'){//同步清空现金流量值
									originData.cashflow[0].pk_unit.value= v.refpk ? v.refpk : '';
									originData.cashflow[0].pk_unit.display=v.refname ? v.refname : '';
								}
							}
							this.setState({
								evidenceData: this.state.evidenceData
							});
						}}
					/>
					</div>
				) : (
					<p class='accasoaClass'><span fieldid="pk_unit">{text ? text.display : ''}</span></p>
				);
			}
		};
		let originData = self.findByKey(number.key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			//let cloneNewLine = deepClone(evidenceColumns);
		//	let cloneNewLine=evidenceColumns
			evidenceColumns.splice(2, 0, number);
		//	evidenceColumns = cloneNewLine;
		}
		return evidenceColumns;
	};

	//详情页添加集团列
	addGroup = (evidenceColumns) => {
		let self = this;
		let addGroup = [
			{
			title:(<span fieldid="groupdebitamount">{self.state.json['20021005card-000268']}</span>),/* 国际化处理： 集团本币(借方)*/
				key: 'groupdebitamount',
				attrcode: 'groupdebitamount',
				itemType: 'amount-rate'
			},
			{
			title:(<span fieldid="groupcreditamount">{ self.state.json['20021005card-000269']}</span>),/* 国际化处理： 集团本币(贷方)*/
				itemType: 'amount-rate',
				key: 'groupcreditamount',
				attrcode: 'groupcreditamount'
			}
		];
		let addCurryLine = self.addCurry(addGroup)
		let originData = self.findByKey(addCurryLine[0].key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			let cloneNewLine = evidenceColumns.concat(addCurryLine);
			evidenceColumns = cloneNewLine;
		}
		return evidenceColumns;
	};

	//详情页添加全局列
	addGlobalLine = (evidenceColumns) => {
		let self = this;
		let addGlobal = [
			{
				title:(<span fieldid="globaldebitamount">{  self.state.json['20021005card-000270']}</span>),/* 国际化处理： 全局本币(借方)*/
				key: 'globaldebitamount',
				attrcode: 'globaldebitamount',
				itemType: 'amount-rate'
			},
			{
				title:(<span fieldid="globalcreditamount">{  self.state.json['20021005card-000271']}</span>),/* 国际化处理： 全局本币(贷方)*/
				itemType: 'amount-rate',
				key: 'globalcreditamount',
				attrcode: 'globalcreditamount'
			}
		];
		let addAllLine = self.addGlobal(addGlobal);
		let originData = self.findByKey(addAllLine[0].key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			let cloneNewLine = evidenceColumns.concat(addAllLine);
			evidenceColumns = cloneNewLine;
		}
		return evidenceColumns;
	};

	//凭证成功返回数据操作
	loadVoucher(voucher, paraInfo, propsType, pageType) {
		let self = this;
		let {
			checkedArray,
			nov,
			isadd,
			startdate,
			attachment,
			evidenceData,
			saveData,
			update,
			abandonShow,
			prepareddate,
			isNC01,
			isNC02,
			headFunc,
			unitOrg,
			freeValue,
			evidenceColumns,
			voucherStatus,
			expandRow,isFreevalueDefault
		} = self.state;
		let { pk_accountingbook } = headFunc;
		let generKey= self.props.getUrlParam('pagekey');
		checkedArray = []; //复选框先置空
		let { NC001, NC002, isShowUnit, excrate2, excrate3, unit, isShowNum ,isEditVoucherNO,pricescale,isAttachmentMust} = paraInfo;
		isFreevalueDefault=paraInfo.isFreevalueDefault;
		let finalColums = evidenceColumns;
		if (voucher.details && voucher.details.length != 0) {
			voucher.details.forEach((item, i) => {
				//渲染是否有动态新增列，有待优化
				checkedArray.push(false);
				this.dateDisplay.push(false);
				item.key = ++i;
			//	itemUpdate(item, paraInfo,true);//第三个参数，避免覆盖默认币种
				if (item.isShowNum && item.isShowNum.value) {
					//根据返回数据判断是否有数量单价
					item.flag = true;
					//evidenceData.newline[flag]=true;
				}
				if(pricescale){
					item.pricescale=pricescale
				}
				if (item.direction && item.direction.value && item.direction.value == 'D') {
				//	if(item.amount&&item.amount&&)
					if(!(item.amount&&item.amount.value)){
						item.amount = {
							scale:item.debitamount && item.debitamount.scale||item.amount&&item.amount.scale,
							value: item.debitamount && item.debitamount.value||item.amount&&item.amount.value,
							editable: item.debitamount && item.debitamount.editable||item.amount&&item.amount.editable
						};
					}
				} else {
					if(!(item.amount&&item.amount.value)){
						item.amount = {
							scale:item.creditamount && item.creditamount.scale||item.amount&&item.amount.scale,
							value: item.creditamount && item.creditamount.value||item.amount&&item.amount.value,
							editable: item.creditamount && item.creditamount.editable||item.amount&&item.amount.editable
						};
					}
					item.debitquantity=item.creditquantity&&item.creditquantity.value?item.creditquantity:item.debitquantity; //liuhuit
				}

				if (item.settle && !item.settle.value) {
					freeValue = freeValue.filter((v, i, a) => {
						return !settkleArray.includes(v.attrcode);
					});
				}
				if (item.verify && !item.verify.value) {
					freeValue = freeValue.filter((v, i, a) => {
						return !verifyArray.includes(v.attrcode);
					});
				}
				item.childform = freeValue;
			});
		}

		if (unit) {
			unitOrg.value = unit.value;
		}
		if (isShowUnit) {
			isadd = true;
			//根据返回数据判断是否业务单元
			finalColums = self.addUnit(evidenceColumns);
		}
		if (isShowNum) {
			//有待完善
			//根据返回数据判断是否数量列
			finalColums = self.addNumber(finalColums);
		}else{
			finalColums=finalColums.filter(item=>{
				return item.key!='quantity'
			})//去掉数量列
		}

		if (NC001) {
			//根据返回数据判断是否集团
			isNC01 = true;
			finalColums = self.addGroup(finalColums);
			//	self.addGroup(NC001);
		}

		if (NC002) {
			//根据返回数据判断是否集团
			isNC02 = true;
			finalColums = self.addGlobalLine(finalColums);
			//	self.addGroup(NC001);
		}
	
		//修改太页面参照设置依赖
		pk_accountingbook.value = voucher.pk_accountingbook.value;
		if(voucher.prepareddate.value.indexOf(' ')!=-1){
			prepareddate.value = voucher.prepareddate.value.split(' ')[0];
		}else{
			prepareddate.value = voucher.prepareddate.value;
		}
		
		if (voucher.details && voucher.details.length != 0) {
			evidenceData.rows = voucher.details;
			evidenceData.index = voucher.details.length;
		} else {
			evidenceData.rows = [];
		}
		
		
		saveData = voucher;
		//表头信息编辑性控制
		self.props.form.setFormItemsDisabled(self.formId, {
			pk_accountingbook:generKey=='generate'?true:(saveData.pk_accountingbook&&saveData.pk_accountingbook.value&&pageType =='update')?true:false,//生成凭证账簿不可编辑
			pk_vouchertype: voucher.pk_vouchertype.editable ? voucher.pk_vouchertype.editable : false,
			prepareddate: voucher.prepareddate.editable||voucher.voucherkind.value=='1'? true : false,
			attachment: voucher.attachment.editable ? voucher.attachment.editable : false,
			isdifflag: voucher.convertflag && voucher.convertflag.value ? true : false,
			num:!isEditVoucherNO?true:false
		});
		self.props.executeAutoFocus();
		  self.props.form.setItemsVisible (self.formId, { 
		 	 	adjustperiod:voucher.voucherkind&&voucher.voucherkind.value=='1'? true : false});
		self.props.form.setFormStatus(self.formId, propsType ? 'browse' : 'edit');
		if(self.props.getUrlParam('pagekey')=='pre'){//预览凭证凭证号清空
			saveData.num.value='';
		}

		// //表头表尾赋默认值 此方法有bug
		self.props.form.setFormItemsValue(self.formId, saveData);
		//self.props.form.setFormItemsValue(self.formIdtail, saveData)
		evidenceData.localcreditamountTotle.value = voucher.totalcredit&&voucher.totalcredit.value; //合计值获取
		evidenceData.localdebitamountTotle.value = voucher.totaldebit&&voucher.totaldebit.value;
		evidenceData.groupcreditamountTotle.value = voucher.totalcreditgroup&&voucher.totalcreditgroup.value;
		evidenceData.groupdebitamountTotle.value = voucher.totaldebitgroup&&voucher.totaldebitgroup.value;
		evidenceData.globalcreditamountTotle.value = voucher.totalcreditglobal&&voucher.totalcreditglobal.value;
		evidenceData.globaldebitamountTotle.value = voucher.totaldebitglobal&&voucher.totaldebitglobal.value;
		saveData['paraInfo'] = paraInfo;
		saveData.period.value = voucher.year&&voucher.year.value + '-' + voucher.year&&voucher.period.value;
		//update = true;

		if (voucher.errmessage&&voucher.errmessage.value) {
			abandonShow = 'Y'; //显示标错标志
		}
		// abandonShow=voucher.discardflag.value;
		//凭证标错信息
		// saveData.errmessage={value:''};
		// saveData.errmessageeh={value:''};
		if(expandRow&&expandRow.length!=0){//分录展开控制，保存前展开，保存成功统一折叠处理
			expandRow=[];
		}

		self.setState(
			{
				voucherStatus: pageType,
				expandRow,
				checkedArray,
				isadd,
				isNC01,
				isNC02,
				attachModal:isAttachmentMust,
				voucherView: propsType, //凭证状态控制
				headFunc,
				evidenceData,
				unitOrg,
				evidenceColumns: finalColums,
				update: propsType,
				saveData,
				abandonShow,
				isFreevalueDefault
			},
			() => {
				this.buttonStatus();
				this.totleCount();
				if(this.apportStatus){
					this.updateLocalValue()
				}
			}
		);
	}

	updateLocalValue=()=>{//分摊重新换算组织本币值
		let _this=this;
		let { evidenceData }=_this.state;
		evidenceData.rows.forEach((e,i)=>{
			let { data, currInfo } = _this.exportData(e);
			let countValue = amountconvert(data, currInfo, 'amount');
			countRow(e, countValue, 'amount');
		})
		_this.totleCount();
	}


	//按钮状态统一处理方法
	buttonStatus = () => {
		let self = this;
		let { voucherView, saveData, headFunc } = self.state;
		let { discardflag, pk_casher, pk_checked, pk_manager ,offervoucher,voucherkind,tempsaveflag,paraInfo} = saveData;
		
		if (voucherView) {
			//不同状态控制按钮显示隐藏
			self.props.button.setButtonVisible(
				[
					'save',
					'saveadd',
					'temp',
					'cancel',
					'errcorr',
					'signmistake',
					'addline',
					'deline',
					'copyline',
					'cutline',
					'pasteline',
					'verifyref',
					'apportion',
					'loadtmp',
					'savenext',
					'parallel',
					//'more'
				],
				false
			);

			self.props.button.setButtonVisible(
				{'add':true,'location':saveData.pk_accountingbook.value?true:false,'keyboard':true,
				 'tolist':saveData.pk_accountingbook.value?true:false,
					'delete':saveData.pk_accountingbook.value?true:false,
					'copy':saveData.pk_accountingbook.value?true:false,
					'verify':saveData.pk_accountingbook.value?true:false,
					'edit':saveData.pk_accountingbook.value?true:false,
					'showsum':saveData.pk_accountingbook.value?true:false,
					'attachment':saveData.pk_accountingbook.value?true:false,
					'bodyattach':saveData.pk_accountingbook.value?true:false,
					'adjustAdd':saveData.pk_accountingbook.value?true:false,
					'managetmp':saveData.pk_accountingbook.value?true:false,
					'abandon':saveData.pk_accountingbook.value?true:false,
					'unabandon':saveData.pk_accountingbook.value?true:false,
					'convert':saveData.pk_accountingbook.value?true:false,
					'viewscan':saveData.pk_accountingbook.value?true:false,
					'viewlook':saveData.pk_accountingbook.value?true:false,
					'cashflow':saveData.pk_accountingbook.value?true:false,
					'refresh':saveData.pk_accountingbook.value?true:false,
					'linkbill':saveData.pk_accountingbook.value?true:false,
					'linkdes':saveData.pk_accountingbook.value?true:false,
					'linksrc':saveData.pk_accountingbook.value?true:false,
					'print':saveData.pk_accountingbook.value?true:false,
					'output':saveData.pk_accountingbook.value?true:false,
					'linkoffersrc':saveData.pk_accountingbook.value?true:false,
					'linkofferdes':saveData.pk_accountingbook.value?true:false,
				}
			
			);

			if (discardflag && discardflag.value) {
				//作废取消作废
				self.props.button.setButtonVisible({'abandon' : false,'unabandon':true,'edit':false,'delete':false });
			} else {
				self.props.button.setButtonVisible({'unabandon':false,'abandon':true});
			}

			if (pk_casher && pk_casher.value && pk_casher.value != '~') {
				//签字取消签字
				self.props.button.setButtonVisible({'sign' : false,'unsign' : true ,'edit':false,'delete':false});
			} else {
				self.props.button.setButtonVisible({'unsign' : false,'sign' : true});
			}

			if (pk_checked && pk_checked.value && pk_checked.value != '~') {
				//审核
				self.props.button.setButtonVisible(
					[ 'check', 'signmistake', 'savemistake', 
					'signmistakeb', 'viewscan',"edit",'delete',
				 ],
					false
				);
				self.props.button.setButtonVisible(
					[ 'uncheck'],true
				);
				//return false//审核，签字，记账按钮覆盖问题  放开冲销返回按钮有问题
			} else {
				self.props.button.setButtonVisible([ 'uncheck' ], false);
				self.props.button.setButtonVisible([ 'signmistake', 'savemistake', 'signmistakeb' ,'check'], true);
				// 显示‘审核’;
				// 显示‘凭证标错’‘保存标错’;
				// 显示‘分录标错’
			}
			if (pk_manager && pk_manager.value && pk_manager.value != 'N/A') {
				//记账
				self.props.button.setButtonVisible([ 'untally' ], true);
				self.props.button.setButtonVisible([ 'tally','edit','delete' ], false);
			} else {
				self.props.button.setButtonVisible([ 'untally' ], false);
				self.props.button.setButtonVisible([ 'tally' ], true);
			}
			if (
				!(discardflag && discardflag.value) &&
				!(pk_casher && pk_casher.value && pk_casher.value != '~') &&
				!(pk_checked && pk_checked.value && pk_checked.value != '~') &&
				!(pk_manager && pk_manager.value && pk_manager.value != 'N/A')
			) {
				self.props.button.setButtonVisible([ 'delete' ], true);
			}

			if(offervoucher&&offervoucher.value&&offervoucher.value!=''){//冲销
				self.props.button.setButtonVisible([ 'redoffset' ,'blueoffset','offset'], false);
			}else{
				self.props.button.setButtonVisible([ 'redoffset' ,'blueoffset','offset',
				'transfer','bodyattach','attachment'], true);
			}

			if(voucherkind&&voucherkind.value=='1'){//调整期凭证
				self.props.button.setButtonVisible( {'errcorr':false,'temp':false ,'convert':false,
				'linkconvert':false,'linksrc':false,'linkdes':false,
				'linkoffset':false,'linkoffersrc':false,'linkofferdes':false,'location':false,'verify':false,'linkbill':false,'print':true,'output':true,});
			}
			if(tempsaveflag&&tempsaveflag.value){//暂存
				 self.props.button.setButtonVisible({'verify': true});
			}

			if(!saveData.pk_voucher||!saveData.pk_voucher.value||saveData.pk_voucher.value==''){
				//预览的凭证
				self.props.button.setButtonVisible(['viewlook','linkbill','attachment','bodyattach'],false);
			}
		} else {
			if(self.offsetButton){//冲销成功页面为编辑太，需在编辑太特殊处理
				self.props.button.setButtonVisible(
					[
						'tolist',
						'delete',
						'copy',
						'verify',
						'edit',
						'add',
						'transfer',
						'bodyattach',
						'attachment',
						'adjustAdd',
						'abandon',
						'unabandon',
						'convert',
						'viewscan',
						'viewlook',
						'refresh',
						'redoffset' ,'blueoffset','offset','more'
					],
					false
				);
				if(paraInfo&&paraInfo.GLGOVPermission){
					self.props.button.setButtonVisible(['parallel'],true);
				}
				self.props.button.setButtonVisible(
					[
						'save',
						'saveadd',
						//'transfer',
						'cancel'
					],
					true
				);
			}else{
				self.props.button.setButtonVisible(
					[
						'tolist',
						'delete',
						'copy',
						'verify',
						'edit',
						'add',
						'transfer',
						'bodyattach',
						'attachment',
						'adjustAdd',
						'abandon',
						'linkbill',
						'unabandon',
						'convert',
						'viewscan',
						'viewlook',
						'refresh',
						'linkconvert',
						'location',
						'more'
					],
					false
				);
				self.props.button.setButtonVisible(
					[
						'save',
						'saveadd',
						'temp',
						'cancel',
						'errcorr',
						'signmistake',
						'managetmp',
						'addline',
						'deline',
						'copyline',
						'cutline',
						'cashflow',
						'pasteline',
						'verifyref',
						'apportion',
						'loadtmp',
						'savenext'
					],
					true
				);
				if(paraInfo&&paraInfo.GLGOVPermission&&paraInfo.isParallel){
					self.props.button.setButtonVisible(['parallel'],true);
				}else{
					self.props.button.setButtonVisible(['parallel'],false);
				}
				self.props.button.setButtonDisabled(
					['addline' ],
					saveData.detailmodflag && saveData.detailmodflag.value === false ? true :false
				);
				if(voucherkind&&voucherkind.value=='1'){//调整期凭证
					self.props.button.setButtonVisible([ 'errcorr','temp' ,'convert','linkconvert','linksrc','linkdes',
					'linkoffset','linkoffersrc','linkofferdes','location','verify','linkbill','print','output'], false);
				}
			}
		}
	};

	//根据id 查询详细信息
	queryList(id, url, type) {
		let self = this;
		let propsType = type ? type : self.props.getUrlParam('status');
		let data = id;
		ajax({
			url,
			data,
			success: function(response) {
				if (response.data) {
					let { voucher, paraInfo } = response.data;
					if (voucher && paraInfo) {
						//存储数据，取消拿到回调值
						let oldValueData = JSON.stringify(response.data);
						setDefData('oldValue', oldSource, oldValueData);
						//self.ViewModel.setData('oldValue', voucher);
						self.loadVoucher(voucher, paraInfo, propsType == 'browse' ? true : false, 'update');
						toast({ content: self.state.json['20021005card-000298'], color: 'success', position: 'bottom' });
					} else {
						self.props.form.EmptyAllFormValue('head');
						self.props.form.setFormStatus('head', 'browse');
						self.cleanVoucher();
						toast({ content: self.state.json['20021005card-000315'], color: 'warning', position: 'bottom' });
						//window.location.refresh();
					}
				}
			}
		});
	}


	//正则校验只能输入数字
	numCheck = (num, scale) => {
		let reg = new RegExp(`^\\d+\\.?\\d{0,${scale === -1 || !scale ? this.scale : scale}}$`, 'g');
		let flag = reg.test(num);
		return flag || num === '';
	};

	//币种请求成功更新分录行精度汇率
	modifyRows = (originData, response) => {
		let {
			maxconverr,
			scale,
			NC001,
			NC002,
			excrate2,
			globalroundtype,
			grouproundtype,
			globalscale,
			groupscale,
			orgroundtype,
			orgscale,
			excrate3,
			excrate4,
			excrate2scale,
			excrate3scale,
			excrate4scale,
			roundtype,
			pricescale,
			priceroundtype,
			orgmode,
			groupmode,
			globalmode
		} = response;
		if (NC001) {
			originData.groupType = NC001;
		}
		if (NC002) {
			originData.globalType = NC002;
		}
		if (excrate2||excrate2=='0') {
			//根据核算账簿赋值默认汇率，默认币种，默认业务单元
			originData.excrate2 = {
				value: excrate2
			};
		}
		if (excrate3||excrate3=='0') {
			//根据核算账簿赋值默认汇率，默认币种，默认业务单元
			originData.excrate3 = {
				value: excrate3
			};
		}
		if (excrate4||excrate4=='0') {
			originData.excrate4 = {
				value: excrate4
			};
		}
		if (excrate2scale) {
			originData.excrate2scale = {
				value: excrate2scale
			};
		}
		if (excrate3scale) {
			originData.excrate3scale = {
				value: excrate3scale
			};
		}
		if (excrate4scale) {
			originData.excrate4scale = {
				value: excrate4scale
			};
		}
		if (maxconverr) {
			originData.maxconverr = {
				value: maxconverr
			};
		}
		if (orgroundtype) {
			originData.orgroundtype = {
				value: orgroundtype
			};
		}
		if (grouproundtype) {
			originData.grouproundtype = {
				value: grouproundtype
			};
		}
		if (globalroundtype) {
			originData.globalroundtype = {
				value: globalroundtype
			};
		}
		if (globalscale) {
			originData.globalscale = globalscale;
		}
		if (groupscale) {
			originData.groupscale = groupscale;
		}
		if (orgscale) {
			originData.orgscale = orgscale;
		}
		if (scale) {
			originData.amount.scale=scale;//更新原币内部scale
			originData.scale = scale;
		}
		if (roundtype) {
			originData.roundtype = {
				value: roundtype
			};
		}
		if (pricescale) {
			originData.pricescale = {
				value: pricescale
			};
		}
		if (priceroundtype) {
			originData.priceroundtype = {
				value: priceroundtype
			};
		}
		originData.orgmode = {
			value: orgmode
		};
		originData.groupmode = {
			value: groupmode
		};
		originData.globalmode = {
			value: globalmode
		};
	};

	//根据币种获取汇率
	queryRate = (data, url, originData) => {
		let self = this;
		let { evidenceColumns } = self.state;
		ajax({
			url,
			data: data,
			success: function(response) {
				// scale roundtype excrate2 excrate3 excrate4 pricescale priceroundtype orgmode groupmode globalmode
				let {
					maxconverr,
					scale,
					NC001,
					NC002,
					excrate2,
					excrate3,
					excrate4,
					roundtype,
					pricescale,
					priceroundtype,
					orgmode,
					groupmode,
					globalmode
				} = response.data;
				self.modifyRows(originData, response.data);
				if (originData.amount.value) {
					//切换汇率请求同步跟进原币计算
					let { data, currInfo } = self.exportData(originData);
					let countValue = amountconvert(data, currInfo, 'amount');
					countRow(originData, countValue, 'amount');
					memoryHistry(originData)
					self.totleCount();
					//更新原数据信息1
					if(self.voucherRows&&self.voucherRows.length!='0'){
						self.updateSaveRows(originData)
					}
				}
			}
		});
	};

	updateSaveRows=(originData)=>{
		let self=this;
		let totleArray=self.voucherRows;
		totleArray.forEach((item,i)=>{
			if(item.key==originData.key){
				item=originData
				return false
			}
		})
		self.voucherRows=totleArray;
	}


	//动态新增集团和全局列
	addCurry = (loadColums) => {
		loadColums.map((item, index) => {
			item.dataIndex = item.name;
			item.width = '170px';
			item.render = (text, record, index) => {
				//根据字段类型，渲染不同的组件
				let self = this;
				let { pk_accountingbook, prepareddate, evidenceColumns, voucherView } = self.state;
				let { rows } = self.state.evidenceData;
			//	let originData = self.findByKey(record.key, rows);
				let errorMsg, errorBorder;
				let defaultValue = {};
				let { data, currInfo } = self.exportData(record);
				if (text.value !== null) {
					//defaultValue = { refname: text[item.key].display, refpk: text[item.key].value };
				}
				switch (item.itemType) {
					case 'amount-rate':
						return !voucherView ? (
							<div className="textCenter" id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`} fieldid={item.key}>
								<NCNumber
									renderInTable={true}
									onKeyDown={(event)=>{
										if(event.keyCode=='38'|| event.key === "ArrowUp"||event.keyCode=='40'|| event.key === "ArrowDown"){// 40 ArrowDown 38 ArrowUp
											onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
										}
									}}
									onEnter={(event)=>{
										onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
									}}
									fieldid={item.key}
									scale={record[item.key]&&record[item.key].scale ? Number(record[item.key].scale) : Number(record.groupscale)}
									disabled={record[item.key]&&record[item.key].editable ? record[item.key].editable : false}
									value={
										text[item.key]&&text[item.key].value && text[item.key].value != '0' ? text[item.key].value : ''
									}
									onFocus={(v) => {
										record[item.key].visible = true;
										self.getRow({key:index+1},index)
									}}
									onBlur={(v) => {
										record[item.key].visible = false;
										if (v &&v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
											let countValue = amountconvert(data, currInfo, item.key);
											countRow(record, countValue, item.key);
										}
									}}
									onKeyUp={(event) => {
										let {
											rows,
											groupcreditamountTotle,
											groupdebitamountTotle,
											globalcreditamountTotle,
											globaldebitamountTotle
										} = this.state.evidenceData;
										let originData = this.findByKey(record.key, rows);
										if (event.keyCode == '187' || event.keyCode == '61') {
											onEqual.call(this,item.attrcode,record);
											// this.equal(item.attrcode, originData);
											// //let { data, currInfo } = this.exportData(originData);
											// //let countValue = amountconvert(data, currInfo, item.attrcode);
											// //countRow(originData, countValue,  item.attrcode);
											this.totleCount(item.attrcode, 'localdebitamountTotle');
										} else if (event.keyCode == '32' && originData.excrate3.value) {
											emptyEnter(item.attrcode, originData);
											if (record.cashflow && record.cashflow.length != 0) {
												this.countCashFlow(record);
											}
											this.totleCount(item.attrcode, 'localdebitamountTotle');
										}
									}}
									onChange={(v) => {
										let { localcreditamountTotle, localdebitamountTotle } = this.state.evidenceData;
									//	if( v !== '=' && v.indexOf('=') == -1 && v.indexOf(' ') == -1){
											let originData = this.findByKey(record.key, rows);
											originData[item.attrcode]={
												...originData[item.attrcode],
												value:v
											}
											this.setState({
												evidenceData: this.state.evidenceData
											});
									//	}
									}}
								/>
							</div>
						) : (
							<div class='curryClass' fieldid={item.key}>
								{text[item.key].value?self.commafy(text[item.key].value):<span>&nbsp;</span>}
								<Rate rate={text.excrate3 ? text.excrate3.value : ''} ifShow={text[item.key]} json={self} />
							</div>
						);
					default:
						break;
				}
			};
		});
		return loadColums;
	};
	//动态新增集团和全局列
	addGlobal = (loadColums) => {
		loadColums.map((item, index) => {
			item.dataIndex = item.name;
			item.width = '170px';
			item.render = (text, record, index) => {
				//根据字段类型，渲染不同的组件
				let self = this;
				let { pk_accountingbook, prepareddate, evidenceColumns, voucherView } = self.state;
				let { rows } = self.state.evidenceData;
			//	let originData = self.findByKey(record.key, rows);
			
				let errorMsg, errorBorder;
				let defaultValue = {};
				let { data, currInfo } = self.exportData(record);
				if (text.value !== null) {
					//	defaultValue = { refname: text[item.key].display, refpk: text[item.key].value };
				}
				switch (item.itemType) {
					case 'amount-rate':
						return !voucherView ? (
							<div className="textCenter" id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`} fieldid={item.key}>
								<NCNumber
									renderInTable={true}
									onKeyDown={(event)=>{
										if(event.keyCode=='38'|| event.key === "ArrowUp"||event.keyCode=='40'|| event.key === "ArrowDown"){// 40 ArrowDown 38 ArrowUp
											onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
										}
									}}
									onEnter={(event)=>{
										onEnterAction(event,this,item.attrcode,`hot-key-${this.moduleId}-${item.attrcode}-${index}`,index,record)
									}}
									fieldid={item.key}
									scale={record[item.key]&&record[item.key].scale? Number(record[item.key].scale) :  Number(record.globalscale)}
									disabled={record[item.key]&&record[item.key].editable ? record[item.key].editable : false}
									value={
										text[item.key]&&text[item.key].value && text[item.key].value != '0' ? text[item.key].value : ''
									}
									onFocus={(v) => {
										record[item.key].visible = true;
										self.getRow({key:index+1},index)
									}}
									onBlur={(v) => {
										record[item.key].visible = false;
										if (v &&v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
											let countValue = amountconvert(data, currInfo, item.key);
											countRow(record, countValue, item.key);
										}
									}}
									onKeyUp={(event) => {
										let {
											rows,
											localcreditamountTotle,
											localdebitamountTotle,
											groupcreditamountTotle,
											groupdebitamountTotle,
											globalcreditamountTotle,
											globaldebitamountTotle
										} = this.state.evidenceData;
										let originData = this.findByKey(record.key, rows);
										if (event.keyCode == '187' || event.keyCode == '61') {
											onEqual.call(this,item.attrcode,record);
											// this.equal(item.attrcode, originData);
											// //let { data, currInfo } = this.exportData(originData);
											// //let countValue = amountconvert(data, currInfo, item.attrcode);
											// //countRow(originData, countValue,  item.attrcode);
											this.totleCount(item.attrcode, 'localdebitamountTotle');
										} else if (event.keyCode == '32' && originData.excrate3.value) {
											emptyEnter(item.attrcode, originData);
											if (record.cashflow && record.cashflow.length != 0) {
												this.countCashFlow(record);
											}
											this.totleCount(item.attrcode, 'localdebitamountTotle');
										}
									}}
									onChange={(v) => {
										let { localcreditamountTotle, localdebitamountTotle } = this.state.evidenceData;
									//	if ( v !== '=' && v.indexOf('=') == -1 && v.indexOf(' ') == -1){
											let originData = this.findByKey(record.key, rows);
											originData[item.attrcode]={
												...originData[item.attrcode],
												value:v
											}
											this.setState({
												evidenceData: this.state.evidenceData
											});
									//	}
										
									}}
								/>
								{/* {errorMsg} */}
							</div>
						) : (
							<div class='curryClass' fieldid={item.key}>
								{text[item.key].value?self.commafy(text[item.key].value):<span>&nbsp;</span>}
								<Rate rate={text.excrate4 ? text.excrate4.value : ''} ifShow={text[item.key]} json={self}/>
							</div>
						);
					default:
						break;
				}
			};
		});
		return loadColums;
	};

	querybook = (dataAjax, url) => {
		let self = this;
		//根据核算账簿动态加载全局集团列 精度 汇率加载默认值
		let {
			evidenceColumns,
			isadd,
			evidenceData,
			isNC01,
			isNC02,
			unitOrg,
			groupScale,
			globalScale,
			orgScale,
			saveData,
			headFunc,
			expandRow,
			prepareddate,
			attachModal,isFreevalueDefault
		} = self.state;
		let number = {
			//业务单元列
		title: (<span fieldid="pk_unit">{self.state.json['20021005card-000072']}</span>),/* 国际化处理： 业务单元*/
			attrcode :'pk_unit',
			key: 'pk_unit',
			dataIndex: 'pk_unit',
			width: '170px',
			render: (text, record, index) => {
				//"uapbd/refer/org/BusinessUnitTreeRef"
				let self = this;
				let { voucherView } = self.state;
				let referUrl = 'uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef/index.js';
				let attrcode = 'pk_unit';
				let editable =
					record['pk_unit'] && record['pk_unit'].editable ? record['pk_unit'].editable : false;
				return !voucherView ? (
					<div id={`hot-key-${this.moduleId}-${attrcode}-${index}`} fieldid={attrcode}>
					<ReferLoader
						onEnter={(event)=>{
							onEnterAction(event,this,attrcode,`hot-key-${this.moduleId}-${attrcode}-${index}`,index,record)
						}}
						fieldid={attrcode}
						tag={attrcode}
						refcode={referUrl}
						disabled={editable}
						value={{ refname: text.display, refpk: text.value }}
						queryCondition={() => {
							return {
								pk_accountingbook: self.state.headFunc.pk_accountingbook.value,
								VersionStartDate: self.state.prepareddate.value,
								TreeRefActionExt: 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder',
								isDataPowerEnable: 'Y',
								DataPowerOperationCode: 'fi'
							};
						}}
						onChange={(v) => {
							let { rows } = this.state.evidenceData;
							let originData = this.findByKey(record.key, rows);
							if (originData && originData.pk_unit_v.value) {
								//切换业务单元清空辅助核算
								if (originData.ass && originData.ass.length != '0') {
									originData.ass = [];
									originData.assid = {
										...originData.assid,
										display: '',
										value: ''
									};
									if(originData.cashflow&&originData.cashflow.length!='0'){//同步清空现金流量值
										originData.cashflow[0].assid.value='';
										originData.cashflow[0].assid.display='';
									}
								}
							}
							if (originData) {
								originData.pk_unit_v = {
									display: v.refname ? v.refname : '',
									value: v.values ? v.values.pk_vid.value : ''
								};
								originData.pk_unit = {
									display: v.refname?v.refname:'',
									value: v.refpk ? v.refpk : ''
								};
								if(originData.cashflow&&originData.cashflow.length!='0'){//同步清空现金流量值
									originData.cashflow[0].pk_unit.value=v.refpk ? v.refpk : '';
									originData.cashflow[0].pk_unit.display= v.refname ? v.refname : '';
								}
							}
							this.setState({
								evidenceData: this.state.evidenceData
							});
						}}
					/>
					</div>
				) : (
					<div class='accasoaClass' fieldid={attrcode}>{text&&text.display?text.display:<span>&nbsp;</span>}</div>
				);
			}
		};
		let addGroup = [
			{
				title:(<span fieldid="groupdebitamount">{ self.state.json['20021005card-000268']}</span>),/* 国际化处理： 集团本币(借方)*/
				key: 'groupdebitamount',
				attrcode: 'groupdebitamount',
				itemType: 'amount-rate'
			},
			{
				title: (<span fieldid="groupcreditamount">{self.state.json['20021005card-000269']}</span>),/* 国际化处理： 集团本币(贷方)*/
				itemType: 'amount-rate',
				key: 'groupcreditamount',
				attrcode: 'groupcreditamount'
			}
		];
		let addGlobal = [
			{
				title: (<span fieldid="globaldebitamount">{self.state.json['20021005card-000270']}</span>),/* 国际化处理： 全局本币(借方)*/
				key: 'globaldebitamount',
				attrcode: 'globaldebitamount',
				itemType: 'amount-rate'
			},
			{
				title: (<span fieldid="globalcreditamount">{self.state.json['20021005card-000271']}</span>),/* 国际化处理： 全局本币(贷方)*/
				itemType: 'amount-rate',
				key: 'globalcreditamount',
				attrcode: 'globalcreditamount'
			}
		];
		let addAll = [
			{
				title: (<span fieldid="groupdebitamount">{self.state.json['20021005card-000268']}</span>),/* 国际化处理： 集团本币(借方)*/
				key: 'groupdebitamount',
				itemType: 'amount-rate',
				attrcode: 'groupdebitamount'
			},
			{
				title: (<span fieldid="groupcreditamount">{self.state.json['20021005card-000269']}</span>),/* 国际化处理： 集团本币(贷方)*/
				itemType: 'amount-rate',
				key: 'groupcreditamount',
				attrcode: 'groupcreditamount'
			},
			{
				title: (<span fieldid="globaldebitamount">{self.state.json['20021005card-000270']}</span>),/* 国际化处理： 全局本币(借方)*/
				key: 'globaldebitamount',
				itemType: 'amount-rate',
				attrcode: 'globaldebitamount'
			},
			{
				title: (<span fieldid="globalcreditamount">{self.state.json['20021005card-000271']}</span>),/* 国际化处理： 全局本币(贷方)*/
				itemType: 'amount-rate',
				key: 'globalcreditamount',
				attrcode: 'globalcreditamount'
			}
		];

		ajax({
			url,
			data: dataAjax,
			success: function(response) {
				let cloneNewLine = deepClone(evidenceColumns);
				if (response.data) {
					let {
						isShowUnit,
						currinfo,
						groupCurrinfo,
						globalCurrinfo,
						excrate2,
						excrate3,
						excrate4,
						unit,
						NC001,
						NC002,
						unit_v,
						groupscale, // 集团精度
						scale, // 默认币种精度
						globalscale, // 全局精度
						orgscale, // 组织精度
						pk_vouchertype,
						num,
						isEditVoucherNO,
						isAttachmentMust,
						isFreevalueDefault, //是否带出辅助核算
						bizDate,
						orgmode, //汇率计算方式 是否为除 true=除
						groupmode, //集团本币汇率计算方式 是否为除 true=除
						globalmode, //全局本币汇率计算方式 是否为除 true=除
						roundtype, //原币进舍规则
						orgroundtype, //本币进舍规则
						grouproundtype, //集团本币进舍规则
						globalroundtype, //全局本币进舍规则
						pricescale, //单价精度
						priceroundtype, //单价进舍规则
						errMessage
					} = response.data;
					isFreevalueDefault=response.data.isFreevalueDefault;
					self.props.controlAutoFocus(true);
					self.props.form.setFormItemsValue(self.formId, {
						prepareddate: { value: bizDate },
						pk_vouchertype: { value: pk_vouchertype?pk_vouchertype.value:'', display: pk_vouchertype?pk_vouchertype.display:'' },
						num: { value: num }
					});

					if(errMessage){
						toast({ content: errMessage, color: 'warning' });
					}
					
					//保存新增重新设置字段可编辑性
					self.props.form.setFormItemsDisabled(self.formId, {
						pk_accountingbook:false,
						pk_vouchertype: false,
						prepareddate: self.voucherkind=='1'?true:false,
						attachment:  false,
						isdifflag:  false,
						quantityflag:false,
						num:!isEditVoucherNO?true:false
					});
					self.props.executeAutoFocus();
					attachModal = isAttachmentMust;
					prepareddate.value = bizDate;
					if (self.voucherkind) {
						saveData.voucherkind.value = self.voucherkind;
					}
					saveData.prepareddate.value = bizDate;
					saveData.num.value = num;
					saveData.pk_vouchertype.display = pk_vouchertype.display;
					saveData.pk_vouchertype.value = pk_vouchertype.value;
					saveData.pk_accountingbook.value = dataAjax.pk_accountingbook;
					saveData.showUnit = isShowUnit;
					saveData['paraInfo'] = response.data;
					headFunc.pk_accountingbook.value = dataAjax.pk_accountingbook;
					if (evidenceData.rows && evidenceData.rows.length > 0 && evidenceData.rows[0]["pk_accountingbook"]){
						evidenceData.rows[0]["pk_accountingbook"] = {//第一行分录添加账簿
							value: dataAjax.pk_accountingbook
						}
					}
					if (unit) {
						unitOrg.value = unit.value;
					}
					if (isShowUnit) {
						//业务单元列控制
						if (!isadd) {
							cloneNewLine.splice(2, 0, number);
							isadd = true;
						}
						self.updateRows(evidenceData, response.data);
					}
					if (!isShowUnit) {
						self.updateRows(evidenceData, response.data);
						if (isadd) {
							cloneNewLine = cloneNewLine.filter(function(v, i, a) {
								return v.key != number.key;
							});
							isadd = false;
						}
					}
					if (NC001) {
						//集团
						if (!isNC01) {
							let addCurryLine = self.addCurry(addGroup);
							cloneNewLine = cloneNewLine.concat(addCurryLine);
							isNC01 = true;
						}
					}
					if (!NC001) {
						if (isNC01) {
							cloneNewLine = cloneNewLine.filter(
								(item) => !addGroup.map((item) => item.key).includes(item.key)
							);
							isNC01 = false;
						}
					}
					if (NC002) {
						//全局
						if (!isNC02) {
							let addGlobalLine = self.addGlobal(addGlobal);
							cloneNewLine = cloneNewLine.concat(addGlobalLine);
							isNC02 = true;
						}
					}
					if (!NC002) {
						if (isNC02) {
							cloneNewLine = cloneNewLine.filter(
								(item) => !addGlobal.map((item) => item.key).includes(item.key)
							);
							isNC02 = false;
						}
					}
					//数量列 切换核算账簿清除
					cloneNewLine = cloneNewLine.filter(function(v, i, a) {
						return v.key != 'quantity';
					});
					if(expandRow&&expandRow.length!=0){//分录展开控制，保存前展开，保存成功统一折叠处理
						expandRow=[];
					}
					self.setState(
						{
							isadd,
							isNC01,
							isNC02,
							isFreevalueDefault,
							headFunc,
							expandRow,
							prepareddate,
							evidenceData,
							evidenceColumns: cloneNewLine,
							saveData,
							attachModal
						},
						() => {
							self.buttonStatus()
						}
					);
				}
			}
		});
	};

	//是否二级核算请求
	ifCheck = (dataAjax, url, clearRows) => {
		let self = this;
		//根据核算账簿动态加载全局集团列 精度 汇率加载默认值
		let {
			evidenceColumns,
			isadd,
			evidenceData,
			isNC01,
			isNC02,
			unitOrg,
			groupScale,
			globalScale,
			orgScale,
			saveData,
			headFunc,
			prepareddate,
			attachModal
		} = self.state;
		if (clearRows) {
			for (let k in saveData) {
				//清空保存数据
				if (isObj(saveData[k])) {
					saveData[k].value = '';
					saveData[k].display = '';
				}
			}
			if (evidenceData.localcreditamountTotle.value) {
				//清空合计值
				evidenceData.localcreditamountTotle.value = '';
			}
			if (evidenceData.localdebitamountTotle.value) {
				evidenceData.localdebitamountTotle.value = '';
			}
			
			let rows = [ ];

			if(evidenceData.rows&&evidenceData.rows.length!='0'){
				rows = [ evidenceData.rows[0] ]
			}
			for (let i = 0, len = rows.length; i < len; i++) {
				//清空分录
				for (let k in rows[i]) {
					if(k=='expand'||k=='memoryValue'){//清空展开字段
						 rows[i][k]={}
					}
					if (isObj(rows[i][k])) {
						rows[i][k].value = '';
						rows[i][k].display = '';
						if (rows[i][k].editable) {
							rows[i][k].editable = false;
						}
						if(rows[i][k].scale){
							rows[i][k].scale=''
						}
					}
					if(Array.isArray(rows[i][k])){//清空辅助字段
						rows[i][k]=[];
					}
				}
			}
			evidenceData.rows = rows;
			evidenceData.index=rows.length;
			self.setState(
				{
					saveData,
					voucherStatus: 'save',
					evidenceData
				},
				() => {
					if(dataAjax.pk_accountingbook){
						self.querybook(dataAjax, url);
					}else{
						//清空表头form数据
						self.props.form.setFormItemsValue(self.formId, saveData);
						self.props.button.setButtonDisabled(//情况核算账簿增行按钮至灰
							['addline' ],
							true
						);
					}
				}
			);
		} else {
			self.querybook(dataAjax, url);
		}
	};

	//更新分录行精度
	updateRows = (evidenceData, response) => {
		evidenceData.rows.forEach(function(item, index) {
			if (!item.pk_accasoa.value) {
				//会计科目优先级最高
				itemUpdate(item, response);
			}
		});
		//新增行赋值
		itemUpdate(evidenceData.newLine, response);
	};

	//是否有数量列
	ifNumber = (data, rowsData) => {
		let self = this;
		let { evidenceColumns, evidenceData } = self.state;
		data && (rowsData.flag = true);
		!data && (rowsData.flag = false);
		let quantity = {
			title: (<span fieldid="quantity">{self.state.json['20021005card-000071']}</span>),/* 国际化处理： 数量/单价*/
			key: 'quantity',
			dataIndex: 'quantity',
			width: '170px',
			render: (text, record, index) => {
				let { pk_accountingbook, prepareddate, voucherView } = self.state;
				//let editable=record[item.attrcode].editable ? record[item.attrcode].editable:false
				let { rows } = self.state.evidenceData;
				let attrcode = 'quantity';
				let attrcode1 = 'price';
			//	let originData = self.findByKey(record.key, rows);
				let { data, currInfo } = self.exportData(record);
				if (record.flag) {
					if (!voucherView) {
						return (
							<div className="number" fieldid="quantity">
							<div id={`hot-key-${this.moduleId}-${attrcode}-${index}`} >
								<NCNumber
									renderInTable={true}
									onEnter={(event)=>{
										onEnterAction(event,this,attrcode,`hot-key-${this.moduleId}-${attrcode}-${index}`,index,record)
									}}
									fieldid="quantity"
									className="count"
									//disabled={editable}
									scale={record.debitquantity&&record.debitquantity.scale ? Number(record.debitquantity.scale) : record.quantityscale&&Number(record.quantityscale.value)}
									value={record.debitquantity&&record.debitquantity.value || ''}
									placeholder={self.state.json['20021005card-000043']}/* 国际化处理： 请输入数量*/
									//disabled={record.debitquantity.value!='0'?false:true}
									onKeyUp={(v) => {}}
									onBlur={(v) => {
										let { evidenceData } = self.state;
										//let originData = self.findByKey(record.key, evidenceData.rows);
										if (v) {
											if(record.memoryValue&&v==record.memoryValue.debitquantity){
												return false; 
											}
											let countValue = amountconvert(data, currInfo, 'debitquantity');
											countRow(record, countValue, 'debitquantity');
											memoryHistry(record)
											this.totleCount('debitquantity', 'localdebitamountTotle');
										}
										self.setState({
											evidenceData
										});
									}}
									onChange={(v) => {
										let { evidenceData } = self.state;
										//let originData = self.findByKey(record.key, evidenceData.rows);
										if (record) {
											record.debitquantity = {
												...record.debitquantity,
												value: v
											};
										}
										self.setState(
											{
												evidenceData
											}
										);
									}}
								/>
								</div>
								{/* <span>{data.name.text}</span> */}

								<span className="space" />
								<div id={`hot-key-${this.moduleId}-${attrcode1}-${index}`} fieldid="price">
								<NCNumber
									renderInTable={true}
									onEnter={(event)=>{
										onEnterAction(event,this,attrcode1,`hot-key-${this.moduleId}-${attrcode1}-${index}`,index,record)
									}}
									fieldid="price"
									className="price"
									//disabled={editable}
									scale={record.price&&record.price.scale? Number(record.price.scale) : (record.pricescale&&Number(record.pricescale.value))||Number(record.pricescale)}
									value={record.price&&record.price.value || ''}
									placeholder={self.state.json['20021005card-000045']}/* 国际化处理： 请输入单价*/
									onKeyUp={(v) => {}}
									onBlur={(v) => {
										let { evidenceData } = self.state;
										//let originData = self.findByKey(record.key, evidenceData.rows);
										if (v) {
											if(record.memoryValue&&v==record.memoryValue.price){
												return false; 
											}
											let countValue = amountconvert(data, currInfo, 'price');
											countRow(record, countValue, 'price');
											memoryHistry(record)
											this.totleCount('price', 'localdebitamountTotle');
										}
										self.setState({
											evidenceData
										});
									}}
									onChange={(v) => {
										let { evidenceData } = self.state;
										if (record) {
											record.price = {
												...record.price,
												value: v
											};
										}
										self.setState({
											//evidenceColumns:cloneNewLine,
											evidenceData
										});
									}}
								/>
								</div>
							</div>
						);
					} else {
						return (
							<div fieldid="debitquantity">
								{record.debitquantity.value || ''}/{record.price.value || ''}
							</div>
						);
					}
				} else {
					return <span />;
				}
			}
		};
		//判断是否有数量这一列
		let num = false;
		for (let v of evidenceData.rows) {
			if (v.flag == true) {
				num = true;
				break;
			}
		}

		// evidenceColumns.forEach(item=>{
		// 	if(item.key=='quantity'){

		// 	}
		// })
		let originData = self.findByKey(quantity.key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			if (num) {
				let cloneNewLine = deepClone(evidenceColumns);
				for(let i=0,len=cloneNewLine.length;i<len;i++){
					if(cloneNewLine[i].attrcode=='pk_currtype'){
						cloneNewLine.splice(i+1, 0, quantity);
						break;
					}
				}
			//	cloneNewLine.splice(5, 0, quantity);
				self.setState({
					evidenceColumns: cloneNewLine
				});
			}
		} else {
			if (!num) {
				evidenceColumns = evidenceColumns.filter(function(v, i, a) {
					return v.key != 'quantity';
				});
				self.setState({
					evidenceColumns
				});
			}
			self.setState({
				evidenceData
			});
		}
	};

	//凭证分录添加复选框
	onAllCheckChange = () => {
		let self = this;
		self.selectedIndex = '1';
		let checkedArray = [];
		//let listData = self.state.data.concat();
		let selIds = [];
		// let id = self.props.multiSelect.param;
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		},()=>{
			let { checkedAll,getrow }=self.state;
			if(checkedAll){
				getrow='1';
				buttonStatusDetails(self.props,true,self.state.saveData)//点选复选框表体按钮可用
			}else{
				getrow='';
				buttonStatusDetails(self.props,false,self.state.saveData)//点选复选框表体按钮可用
			}
			self.setState({
				getrow
			})
		});
	};
	//凭证分录复选框点击
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let rowKey='';
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (!checkedArray[i]) {
				allFlag = false;
				break;
			} else {
				allFlag = true;
			}
		}
		if(checkedArray.find(item=>item)){
			buttonStatusDetails(self.props,true,self.state.saveData)//点选复选框表体按钮可用
		}else{
			buttonStatusDetails(self.props,false,self.state.saveData)//点选复选框表体按钮可用
		}
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (checkedArray[i]) {//分录标识
				rowKey=i+1;
				break;
			}
		}
		self.setState(
			{
				getrow: rowKey,
				checkedAll: allFlag,
				checkedArray: checkedArray
			},
			() => {
				let { checkedArray, evidenceData } = this.state;
				let checkArr = {};
				if (evidenceData.rows && evidenceData.rows.length != '0') {
					for (let i = 0; i < evidenceData.rows.length; i++) {
						if (checkedArray[i]) {
							this.selectedIndex = i.toString();
							break;
						}
					}
				}
			}
		);
		event.stopPropagation();
	};

	//表格添加复选框
	renderColumnsMultiSelect(columns) {
		const { data, checkedArray,voucherView } = this.state;
		const { multiSelect } = this.props;

		let select_column = {};
		let indeterminate_bool = false;
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let openRowCol = {
				title: '',
				attrcode: 'open',
				key: 'open',
				dataIndex: 'open',
				width: '100px',
				fixed: "right",
				render: (text, record, index) => {
					let errorTip = <div>{record.errmessage ? record.errmessage.value : ''}</div>;
					let checkTip = <div>{record.checkmessage ? record.checkmessage.value : ''}</div>;
					return [
						record.checkmessage && record.checkmessage.value && (
							<NCTooltip trigger="hover" placement={'top'} inverse overlay={checkTip}>
								<i className="iconfont icon-zhuyi1" />
							</NCTooltip>
						),
						record.errmessage &&record.errmessage.value&& (
							<NCTooltip trigger="hover" placement={'top'} inverse overlay={errorTip}>
								<i className="iconfont icon-shibai" />
							</NCTooltip>
						)
					];
				}
			};
			let defaultColumns = [
				{
					title: (
						<Checkbox
							className="table-checkbox"
							checked={this.state.checkedAll}
							indeterminate={indeterminate_bool && !this.state.checkedAll}
							onChange={this.onAllCheckChange}
						/>
					),
					key: 'checkbox',
					attrcode: 'checkbox',
					dataIndex: 'checkbox',
					width: '60px',
					fixed: "left",
					render: (text, record, index) => {
						let checkTip = <div>{record.checkmessage ? record.checkmessage.value : ''}</div>;
						let isFincc=true;
						let {evidenceData}=this.state;
						let evidenceRows=evidenceData.rows;
						let isShow=this.state.saveData&&this.state.saveData.paraInfo?this.state.saveData.paraInfo.isParallel:false;
						if(evidenceRows[index]&&evidenceRows[index].pk_accasoa&&evidenceRows[index].pk_accasoa.value&&evidenceRows[index].pk_accasoa.value!=''){
							let accproperty=evidenceRows[index].pk_accasoa.accproperty?evidenceRows[index].pk_accasoa.accproperty:
													(evidenceRows[index].accsubjaccproperty?evidenceRows[index].accsubjaccproperty.value:'0');
							if(accproperty=='1')
								isFincc=false;
						}else{
							isShow=false;
						}
						return (
							<div id="linecount_div">
								{isShow ?
									(isFincc ?
										<div className='hot_caiyu' index={index} onMouseOver={this.caiHover.bind(this,index,"line_cai")} onMouseOut={this.caiOut.bind(this,index,"line_cai")}>
											{/* <div id="color_cai" index={index}></div> */}
											<span id={"line_cai"+index} className={"change_span line_cai line_cai"+index}>财</span>
										</div>
										:
										<div className='hot_caiyu' index={index} onMouseOver={this.caiHover.bind(this,index,"line_yu")} onMouseOut={this.caiOut.bind(this,index,"line_yu")}>
											{/* <div id="color_yu" index={index}></div> */}
											<span id={"line_yu"+index} className={"change_span line_yu line_yu"+index}>预</span>
										</div>
									)
								: ""}
								<Checkbox
									checked={this.state.checkedArray[index]}
									onChange={this.onCheckboxChange.bind(this, text, record, index)}
								/>
						</div>
							// {/* {!record.showCheck ? (
							// 	<NCTooltip trigger="hover" placement={'top'} inverse overlay={checkTip}>
							// 		<i className="iconfont icon-zhuyi1" />
							// 	</NCTooltip>
							// ) : (
							// 	''
							// )} */}
						);
					}
				}
			];
			let emptyColumn = {
				title: '',/* 国际化处理： 行号*/
					key: 'empty',
					dataIndex: 'empty',
					width: '0'
				};
			columns = defaultColumns.concat(columns);
			// columns.push(emptyColumn);
			columns.push(openRowCol);
		}
		return columns;
	}

	//动态渲染表格
	getTable = (evidenceColumns, evidenceRows) => {
		let columns = this.renderColumnsMultiSelect(evidenceColumns, evidenceRows);
		let tableArea= this.refs.tableArea ? document.getElementById('tableArea').offsetHeight : '400px';
		let appHeight=document.getElementById('app').offsetHeight;
		let voucherFormHeight=document.getElementById('voucherFormHeight')?document.getElementById('voucherFormHeight').offsetHeight:0;
		let totalLine=document.getElementById('totalLine');
		let totalHeight=0;
		if(totalLine){
			totalHeight=totalLine.offsetHeight;
		}else{
			totalHeight=0;
		}
		this.tableHeight=appHeight-voucherFormHeight-totalHeight-80+'px';
		return (
			<div class='voucherTable'>
			<NCDiv fieldid="voucherTable" areaCode={NCDiv.config.TableCom}>
				<Table					
					onExpand={this.getData}
					onRowClick={this.getRow}
					expandedRowKeys={this.state.expandRow}
					expandedRowRender={this.expandedRowRender}
					expandIconColumnIndex={1}
					columns={columns}
					syncHover={false}
					isDrag={true}
					// currentIndex={this.state.getrow}
					data={evidenceRows}
					// rowClassName={(record,index,indent)=>{
					// 	if(this.state.getrow-1==index){
					// 		return 'selected'
					// 	}else{
					// 		return ''
					// 	}
					// }}
					rowClassName={record => record.errmessage &&record.errmessage.value ? 'errorRow' : ''}
					//lazyload={false}
					scroll={{
						x: true,//evidenceColumns.length > 8 ? 100 + (evidenceColumns.length - 8) * 15 + '%' : '100%',
						y: this.tableHeight
					}}
					bodyStyle={{height:this.tableHeight}}
				/>
				</NCDiv>
			</div>
		);
	};

	//金额格式的修改
	formatDot(value, len) {
		let formatVal, dotSplit, val;

		val = (value.value || 0).toString();

		dotSplit = val.split('.');

		if (dotSplit.length > 2 || !value.value) {
			return value.value;
		}

		if (value.scale && value.scale != '-1') {
			len = value.scale;
		}

		len = len || 2;

		if (val.indexOf('.') > -1) {
			formatVal = val.substring(0, val.indexOf('.') + len + 1);
		} else {
			formatVal = val;
		}

		return formatVal;
	}

	//数字转换成千分位 格式
	commafy(num) {
		let pointIndex, intPart, pointPart;

		if (isNaN(num)) {
			return '';
		}
		num = num + '';
		if (/^.*\..*$/.test(num)) {
			pointIndex = num.lastIndexOf('.');
			intPart = num.substring(0, pointIndex);
			pointPart = num.substring(pointIndex + 1, num.length);
			intPart = intPart + '';
			let re = /(-?\d+)(\d{3})/;
			while (re.test(intPart)) {
				intPart = intPart.replace(re, '$1,$2');
			}
			num = intPart + '.' + pointPart;
		} else {
			num = num + '';
			let re = /(-?\d+)(\d{3})/;
			while (re.test(num)) {
				num = num.replace(re, '$1,$2');
			}
		}
		return num;
	}

	formatAcuracy(value, len) {
		// return this.toThousands(formatVal);
		if (value.value === null || value.value === undefined) {
			return value.value;
		}
		return this.commafy(this.formatDot(value, len));
	}

	// changemoney(evidenceData) {
	// 	let {json }=this.state;
	// 	if (evidenceData.localcreditamountTotle.value&&evidenceData.localdebitamountTotle.value&&evidenceData.localdebitamountTotle.value == evidenceData.localcreditamountTotle.value) {
	// 		if (evidenceData.localdebitamountTotle.value < 0) {
	// 			return json['20021005card-000297'] + convertCurrency(json,Math.abs(evidenceData.localcreditamountTotle.value));
	// 		} else {
	// 			return convertCurrency(json,evidenceData.localcreditamountTotle.value);
	// 		}
	// 	}
	// }
	//标错模态框确定事件
	saveErrorMessage = (MsgModalAll, e) => {
		let self = this;
		let { saveData, getrow } = self.state;
		let saveDataClone = JSON.parse(JSON.stringify(saveData));
		let { rows } = self.state.evidenceData;
		//修改会计期间
		//saveData.period.value="03"
		if (getrow) {
			saveDataClone.period.value = saveDataClone.period.value.split('-')[1];
			for (let i = 0, len = saveDataClone.details.length; i < len; i++) {
				if (saveDataClone.details[i].key == getrow) {
					saveDataClone.details[i]['errmessage']={
						value:e
					};
				}
			}
			if(!saveDataClone.errmessage||!saveDataClone.errmessage.value||saveDataClone.errmessage.value==''){
				saveDataClone.errmessage={value:self.state.json['20021005card-000259']/*"错误"*/}
				saveDataClone.errmessageh={value:self.state.json['20021005card-000259']/*"错误"*/}
			}
		}
		saveDataClone.details.forEach(function(v, i, a) {
			if (v.ass) {
				delete v.ass;
			}
			if (v.cashflow) {
				delete v.cashflow;
			}
		});
		let url = '/nccloud/gl/voucher/saveerror.do';
		ajax({
			url,
			data: saveDataClone,
			success: function(response) {
				//更新缓存数据，使得列表数据是最新的 
				vouchersaveUpdateCache('update','pk_voucher',response.data.voucher.pk_voucher.value,response.data,self.formId,dataSourceTable);
				toast({ content: self.state.json['20021005card-000102'], color: 'success' });/* 国际化处理： 已作标错处理*/
				let {voucher, paraInfo }=response.data
				self.loadVoucher(voucher, paraInfo,true);
			}
		});
		self.setState({
			SaveErrorModalShow: false
		});
	};
	//刷新界面数据
	refreshVoucherData=(voucher,paraInfo,status,voucherType)=>{
		let pk_voucher = voucher.pk_voucher.value;
		let urllist = '/nccloud/gl/voucher/query.do';
		this.queryList({pk_voucher:pk_voucher}, urllist, 'browse');
	}
	//及时核销
	queryRtverify=()=>{
		let self=this;
		let {rtverifyData,saveData,getrow,voucherView}=self.state;
		let mockData={
			pk_voucher: saveData.pk_voucher && voucherView ? saveData.pk_voucher.value :self.datavoucher,
			pk_detail:
				saveData.details &&
				saveData.details.length != 0 &&
				getrow &&
				voucherView &&
				saveData.details[getrow - 1] &&
				saveData.details[getrow - 1].pk_detail
					? saveData.details[getrow - 1].pk_detail.value
					: self.detailvalue}
		let url = '/nccloud/gl/verify/rtverify.do';
		ajax({
			url:url,
			data:mockData,
			success: function(response){
			  let { data, success } = response;
			  if(success){
				self.setState({
					rtverifyData:data,
					VrifyModalShow: true
				})
			  }
			}
		})
	}
	//参照核销数据整理
	loadData = () => {
		let { saveData, getrow, evidenceData, voucherStatus } = this.state;
		if (voucherStatus == 'update') {
			return saveData.details && saveData.details.length != 0 && getrow
				? Object.assign({}, saveData.details[getrow - 1])
				: '';
		} else {
			return evidenceData.rows && evidenceData.rows.length != 0 && getrow
				? Object.assign({}, evidenceData.rows[getrow - 1])
				: '';
		}
	};
	//参照核销
	apportionQuery = (apport,paraInfo) => {
		let self = this;
		let url = '/nccloud/gl/verify/refverify.do';
		ajax({
			url,
			data: apport,
			async:true,
			success: function(res) {
                let { data } = res;
                if(!data){
                    data={};
                }
				data.paraInfo=paraInfo;
				self.setState({
					// evidenceData,
					verifyRefData: data,
					VrifyRefModalShow: true
				});
			}
		});
	};
	//联查序时账
	modalcontent() {
		this.AssiBalance.props.modal.show('exportjournal')
		// return (
		// 	<div>
		// 		<AssiBalance
		// 			show={this.state.showAss}
		// 			clickhandle={this.clickhandle}
		// 			dataout={this.state.sequenceData}
		// 		/>
		// 	</div>
		// );
	}

	//表单编辑前事件
	bodyBeforeEvent = (props, moduleId, key, value, index, record) => {
		let { unitOrg, headFunc } = this.state;
		let meta = props.meta.getMeta();
		meta[moduleId].items.find((item) => item.attrcode == 'pk_vouchertype').queryCondition = () => {
			let pk_org = unitOrg.value;
			return {
				GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
				isDataPowerEnable: 'Y',
				DataPowerOperationCode: 'fi',
				pk_org: headFunc.pk_accountingbook.value
			};
		};
		if(key!=='prepareddate'){
			props.meta.setMeta(meta);
		}
		return true;
	};
	//附件上传
	onHideUploader = () => {
		this.setState({
			showUploaderTable: false
		});
	};

	onHideApprove=()=>{
		this.setState({
			showViewLoaderDetail: false
		});
	}

	//附件上传方法
	beforeUpload(billId, fullPath, file, fileList) {
		return true
	}

	appSureBtnClick = () => {
		//补录信息保存
		let self = this;
		let { saveData, evidenceData,listItem ,getrow} = self.state;
		let url = '/nccloud/gl/voucher/update.do';
		evidenceData.rows[getrow-1].expand=self.infoModal.state.listItem;
		let cloneNewLine = deepClone(evidenceData.rows);
		if (saveData.period.value.indexOf('-') != -1) {
			let newValue = saveData.period.value.split('-')[1];
			saveData.period.value = newValue;
		}
		let newRows = cloneNewLine.filter(function(v, i, a) {
			return v.explanation.value != '';
		});
		let cutRowData=[];
		cutData(cutRowData,newRows)
		saveData.details = cutRowData;
		ajax({
			url,
			data: saveData,
			success: function(response) {
				toast({ content: self.state.json['20021005card-000093'], color: 'success', position: 'bottom' });/* 国际化处理： 保存成功*/
				if (response.data) {
					let { voucher, paraInfo } = response.data;
					saveData.ts=voucher.ts;
					voucher.details.map((item, i) => {
						if (item.expand) {
							evidenceData.rows[i].expand = item.expand;
							evidenceData.rows[i].ts=item.ts;
						}
					});
					self.setState({
						saveData,
						evidenceData
					});
				}
			}
		});
	};

	voucherButton=()=>{
		let self=this;
		let { saveData }=self.state;
		//不同状态控制按钮显示隐藏
		self.props.button.setButtonVisible(
			[
				'save',
				'saveadd',
				'temp',
				'linkoffset',
				'linkoffersrc',
				'linkofferdes',
				'cancel',
				'errcorr',
				'signmistake',
				'addline',
				'delete',
				'abandon',
				'deline',
				'copyline',
				'cutline',
				'pasteline',
				'verifyref',
				'apportion',
				'savenext',
				'showsum',
				'print',
				'output',
				'loadtmp'
			],
			false
		);
		self.props.button.setButtonVisible(
			{'add':true,'location':saveData.pk_accountingbook.value?true:false,'adjustAdd':true,'keyboard':true,'refresh':true,
			 'tolist':saveData.pk_accountingbook.value?true:false,
				'delete':saveData.pk_accountingbook.value?true:false,
				'copy':saveData.pk_accountingbook.value?true:false,
				'verify':saveData.pk_accountingbook.value?true:false,
				'edit':saveData.pk_accountingbook.value?true:false,
				'attachment':saveData.pk_accountingbook.value?true:false,
				'bodyattach':saveData.pk_accountingbook.value?true:false,
				'managetmp':saveData.pk_accountingbook.value?true:false,
				'abandon':saveData.pk_accountingbook.value?true:false,
				'unabandon':saveData.pk_accountingbook.value?true:false,
				'convert':saveData.pk_accountingbook.value?true:false,
				'viewscan':saveData.pk_accountingbook.value?true:false,
				'viewlook':saveData.pk_accountingbook.value?true:false,
				'cashflow':saveData.pk_accountingbook.value?true:false,
			}
		
		);
	}
	cleanVoucher=() => {
		//新增取消清空整张凭证
		let { saveData, evidenceData ,voucherView } = this.state;
		for (let k in saveData) {
			if (isObj(saveData[k])) {
				saveData[k].value = '';
				saveData[k].display = '';
			}
		}
		let rows =evidenceData.rows&&evidenceData.rows.length!=0?[ evidenceData.rows[0] ]:[];
		for (let i = 0, len = rows.length; i < len; i++) {
			//清空分录
			for (let k in rows[i]) {
				if (isObj(rows[i][k])) {
					rows[i][k].value = '';
					rows[i][k].display = '';
					if (rows[i][k].editable) {
						rows[i][k].editable = false;
					}
				}
				if(rows[i].expand&&rows[i].expand!='{}'){
					for(let m in rows[i].expand){
						if(rows[i].expand[m].display){
							rows[i].expand[m].display='';
						}
						if(rows[i].expand[m].value){
							rows[i].expand[m].value='';
						}
					}
				}
			}
		}
		evidenceData.rows = rows;
		evidenceData.index = 0;
		this.setState({
			voucherView:true,
			saveData,
			evidenceData
		},()=>{
			this.totleCount()
			//清空凭证按钮处理
			this.voucherButton()
		});
	}

	render() {
		let self = this;
		let { table, button, search, modal, form } = self.props;
		let {
			evidenceColumns,
			options,
			placeholder,
			disabled,
			evidenceData,
			voucherStatus,
			MsgModalAll,
			VrifyModalShow,
			SaveErrorModalShow,
			LinkModalShow,
			Inspectionshow,
			headContent,
			showUploaderTable,
			showViewLoaderDetail,
			target,
			SubjectModalShow,
			VrifyRefModalShow,
			AssistAccModalShow,
			changeNumberShow,
			update,
			apportionmentShow,
			voucherView,
			footContent,
			findRows,
			errorFlag,
			saveData,
			getrow,
			prepareddate,
			headFunc,
			unitOrg,
			showAss,
			isNumChange,currentRowPrevass,verifyRefData,rtverifyData
		} = self.state;
		let apportionmentValue = {
			amountScale:getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].amount
			&& evidenceData.rows[getrow - 1].amount.scale ? Number( evidenceData.rows[getrow - 1].amount.scale) : evidenceData.rows[getrow - 1]&&evidenceData.rows[getrow - 1].scale&&Number(evidenceData.rows[getrow - 1].scale),
			amount:
				getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].amount
					? evidenceData.rows[getrow - 1].amount.value
					: '',
			pk_accasoa:
				getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].pk_accasoa
					? evidenceData.rows[getrow - 1].pk_accasoa.value
					: '',
			pk_accountingbook: saveData.pk_accountingbook
				? saveData.pk_accountingbook.value
				: headFunc.pk_accountingbook.value,
			prepareddate: prepareddate.value,
			pk_unit:
				getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].pk_unit
					? evidenceData.rows[getrow - 1].pk_unit.value
					: unitOrg.value
		};
		const { createModal } = modal;
		const { createForm } = form;
		let evidenceRows = evidenceData.rows;
		let buttonType = self.props.getUrlParam('pagekey'); //生成凭证控制按钮显影
		const map = {
			'expand': 'f4'
		  };
		  const handlers = {
			'expand': self.expandNode.bind(self)
		  };
		  let tableWidth=this.refs.tableArea ? document.getElementById('tableArea').offsetWidth:'98%';
		return (
			<div id="evidence" className="nc-bill-card nc-bill-list">
				<div id="voucherFormHeight">
					<NCDiv areaCode={NCDiv.config.HEADER}>
					<HeadButtom
						isNumChange={isNumChange}
						abandonShow={self.state.abandonShow}
						pkaccount={self.state.headFunc.pk_accountingbook}
						buttonload={self.props}
						realDetail={self.voucherRows}
						status={self.state}
						selectedIndex={self.selectedIndex}
						loadAttachment={(v) => {
							//弹出模态框,修改附属信息
							let { saveData } = this.state;
							let { attachment } = saveData;
							attachment.display = v;
							attachment.value = v;
							self.props.form.setFormItemsValue(self.formId, { attachment: { value: v } });
							this.setState({
								saveData
							});
						}}
						updateState={(v, status,offset,voucherStatus) => {
							//offset冲销默认标识控制编辑太按钮
							if(offset){
								this.offsetButton=offset;
							//	voucherStatus
							}
							let { voucher, paraInfo } = v;
							self.loadVoucher(voucher, paraInfo, status,voucherStatus?voucherStatus:'update');
							self.props.form.setFormStatus(self.formId, status ? 'browse' : 'edit');
							//调整期特殊处理
							let meta = self.props.meta.getMeta();
								meta[self.formId].items.map((item) => {
									//凭证类别参照添加过滤
									if (item.attrcode == 'pk_vouchertype') {
										item.queryCondition = () => {
											return {
												GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
												pk_org: saveData.pk_accountingbook.value,
												isDataPowerEnable: 'Y',
												DataPowerOperationCode: 'fi'
											};
										};
									}
									if (item.attrcode == 'adjustperiod') {
										item.queryCondition = () => {
											return {
												pk_accountingbook:saveData.pk_accountingbook.value
											};
										};
									}
								});
								self.props.meta.setMeta(meta);
							//self.totleCount();
						}}
						abandUpdate={(v) => {
							//错误凭证回调
							self.setState({
								abandonShow: v
							});
						}}

						cleanButton={(v)=>{
							if (v.voucherView) {
								self.props.form.setFormStatus(self.formId, 'browse');
								//self.props.form.setFormStatus(self.formIdtail, 'browse');
							} else {
								self.props.form.setFormStatus(self.formId, 'edit');
								//self.props.form.setFormStatus(self.formIdtail, 'edit');
							}
							self.setState(
								{
									voucherStatus:'save',
									voucherView: v.voucherView
								},
								() => {
									self.props.button.setButtonVisible(
										[
											'save','saveadd','temp','copy','edit','cancel','errcorr','signmistake',
											'addline','delete','abandon','managetmp','unabandon','cashflow','convert',
											'deline','refresh',"viewscan",'viewlook','linksrc','linkdes',
											'copyline','linkoffersrc','linkofferdes','adjustAdd',
											'cutline','showsum','location',
											'pasteline','linkbill','output','print',
											'verifyref',
											'apportion',
											'loadtmp',
											'savenext',

										],
										false
									);
									self.props.button.setButtonVisible(
										[
											'add',
											'keyboard',
											'adjustAdd'
										],
										true
									);
								}
							);
							
						}}
						cancelUpdate={(data,v,status) => {
							//作废取消作废回调
							let { voucher, paraInfo } = data;
							self.loadVoucher(voucher, paraInfo, status);
							if (v == 'N') {
								//作废
								self.props.button.setButtonVisible({ unabandon: false, abandon: true });
							} else {
								self.props.button.setButtonVisible({ unabandon: true, abandon: false });
							}
						}}
						upNumber={()=>{
							saveData.num.value = self.props.form.getFormItemsValue('head','num').value;
							self.setState({
								isNumChange:true
							});
						}}
						saveAdd={(v, p) => {
							//保存新增
							// id.form.setFormStatus('head', 'add');
							// id.form.EmptyAllFormValue('head');
							// parentState.voucherView({ voucherView: false });
							let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
							let pk_accpont = {
								pk_accountingbook: p.next.pk_accountingbook.value,
								isVoucher:true
							};
							self.props.controlAutoFocus(true);
							self.props.form.setFormItemsValue(this.formId, {
								pk_accountingbook: {
									value: p.next.pk_accountingbook.value,
									display: p.next.pk_accountingbook.display
								},
								adjustperiod:{display:'',value:'' },
								attachment: { value: '' },
								isdifflag:{display:'否',value:false},
								quantityflag:{display:'否',value:false}
							});
							self.props.executeAutoFocus();
							//self.props.form.setFormItemsValue(this.formId, { });//保存新增清空附单数据
							self.ifCheck(pk_accpont, url, true);
						}}
						backOld={(v) => {
							//取消回调
							let { voucher, paraInfo } = v;
							let propsType = self.props.getUrlParam('status');
							self.loadVoucher(voucher, paraInfo, true, 'update');
							//self.props.form.setFormStatus(self.formId, 'browse');
						}}
						saveUpdate={(v, status,voucherType) => {
							let { voucher, paraInfo } = v;
							self.loadVoucher(voucher, paraInfo, status,voucherType?voucherType:'update');
						}}
						updatePage={(v, e) => {
							//刷新回调
							//删除凭证回调函数显示前一张凭证
							this.queryList(v, e, 'browse');
							
						}}
						voucherView={(v) => {
							//跟新凭证状态浏览太和修改太
							let { checkedArray ,checkedAll}=self.state;
							for (var i = 0; i < checkedArray.length; i++) {
								checkedArray[i] = false;
							}
							self.props.controlAutoFocus(true);
							if (v.voucherView) {
								self.props.form.setFormStatus(self.formId, 'browse');
							} else {
								self.props.form.setFormStatus(self.formId, 'edit');
							}
							self.props.executeAutoFocus();
							self.setState(
								{
									checkedAll:false,
									checkedArray:checkedArray,
									getrow:'',
									voucherView: v.voucherView
								},
								() => {
									self.buttonStatus();
									buttonStatusDetails(self.props,false,self.state.saveData)//复选框表体按钮禁用
								}
							);
						}}
						cleanVoucher={self.cleanVoucher.bind(self)}
						changepk_accsoa={(v) => {
							//转换功能回调
							evidenceData.rows = v;
							evidenceData.index = v.length;
							self.setState({
								evidenceData
							});
						}}
						cleanTable={(voucherkind) => {
							//新增按钮清空操作
							//新增如果有核算账簿，则根据核算账簿跟新
							//若无核算账簿取默认核算账簿带出初始值
							let self = this;
							let { saveData, evidenceData,voucherStatus } = self.state;
							if (voucherkind) {
								self.voucherkind = '1';
								saveData.voucherkind.value = '1';
								//调整期凭证过滤未添加 关于依赖核算账簿过滤后续抽取公共方法
								let meta = self.props.meta.getMeta();
								meta[self.formId].items.map((item) => {
									//凭证类别参照添加过滤
									if (item.attrcode == 'pk_vouchertype') {
										item.queryCondition = () => {
											return {
												GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
												pk_org: saveData.pk_accountingbook.value,
												isDataPowerEnable: 'Y',
												DataPowerOperationCode: 'fi'
											};
										};
									}
									if (item.attrcode == 'adjustperiod') {
										item.queryCondition = () => {
											return {
												pk_accountingbook:saveData.pk_accountingbook.value
											};
										};
									}
								});
								self.props.meta.setMeta(meta);
							}else{
								self.voucherkind = '';
							}
							voucherStatus='save';//更新凭证页面状态
							let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
							if (saveData.pk_accountingbook && saveData.pk_accountingbook.value) {
								
								let pk_accpont = {
									pk_accountingbook: saveData.pk_accountingbook.value,
									isVoucher:true
								};
								self.props.form.setFormItemsValue(self.formId, {
									pk_accountingbook: {
										value: saveData.pk_accountingbook.value,
										display: saveData.pk_accountingbook.display
									}
								});
								self.ifCheck(pk_accpont, url, true);
							} else {
								if (self.defaultAccount.value) {
									let pk_accpontd = {
										pk_accountingbook: self.defaultAccount.value,
										isVoucher:true
									};
									self.props.form.setFormItemsValue(self.formId, {
										pk_accountingbook: {
											value: self.defaultAccount.value,
											display: self.defaultAccount.display
										}
									});
									self.ifCheck(pk_accpontd, url, true);
								} else {
									//空凭证
									for (let k in saveData) {
										//清空保存数据
										if (isObj(saveData[k])) {
											saveData[k].value = '';
											saveData[k].display = '';
										}
									}
									if (evidenceData.localcreditamountTotle.value) {
										//清空合计值
										evidenceData.localcreditamountTotle.value = '';
									}
									if (evidenceData.localdebitamountTotle.value) {
										evidenceData.localdebitamountTotle.value = '';
									}
									let rows = [ evidenceData.rows[0] ];
									for (let i = 0, len = rows.length; i < len; i++) {
										//清空分录
										for (let k in rows[i]) {
											if(k=='expand'||k=='memoryValue'){//清空展开字段
												rows[i][k]={}
											}
											if (isObj(rows[i][k])) {
												rows[i][k].value = '';
												rows[i][k].display = '';
												if (rows[i][k].editable) {
													rows[i][k].editable = false;
												}
											}
										}
									}
									evidenceData.rows = rows;
									self.setState({
										saveData,
										voucherStatus:'save',
										evidenceData
									});
								}
							}

							// evidenceData.rows.map((item, i) => {
							// 	for (let index in item) {
							// 		item[index] = {};
							// 	}
							// });
							// self.setState({
							// 	evidenceData
							// });
						}}
						copyState={() => {
							if (saveData.pk_voucher && saveData.pk_voucher.value) {
								let { saveData, voucherView,evidenceData } = self.state;
								let url = '/nccloud/gl/voucher/copy.do';
								// let data = {
								// 	pk_voucher: saveData.pk_voucher.value
								// };
								let details=saveData.details;
								if(!details){
									details=evidenceData.rows;
								}
								let cutRowData=[];
								cutData(cutRowData,details)
								saveData.details = cutRowData;
								ajax({
									url,
									data: saveData,
									success: function(response) {
										let { voucher, paraInfo } = response.data;
										if (voucher && paraInfo) {
										//	self.props.form.setFormStatus(self.formId, 'edit');
										//	voucherView = false; //复制页面状态和新增一样
											self.loadVoucher(voucher, paraInfo, false, 'save');
										}
									}
								});
							}
						}}
						verifyModal={(v,datavoucher,detailvalue)=>{
							//是否即时核销回调
								// let self=this;
								let {getrow}=self.state;
								getrow=v;
								this.datavoucher=datavoucher;//即时核销弹出框特殊处理
								this.detailvalue=detailvalue;
								
								self.setState({
									getrow,
									// VrifyModalShow:true
								},()=>{
									this.queryRtverify();//保存弹及时核销，请求及时核销数据
								})
						}}
					/>
					</NCDiv>
					<div className="nc-bill-top-area nc-bill-form-area">
						{createForm(this.formId, {
							//编辑后事件
							setVisibleByForm:true,
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent:  this.bodyBeforeEvent.bind(this),//添加过滤
							onLastFormEnter:onLastFormEnter.bind(this),
							fieldid:"voucher"
						})}
					</div>
				</div>
				<div className="table-area" ref="tableArea" id="tableArea">
					<div className="header table-header">
						<div className="title">{this.state.json['20021005card-000281']}</div>
						<div className="btn-group table-button-type">
							{this.props.button.createButtonApp({
								area: buttonType ? buttonType + '_detail' : 'detail',
								onButtonClick: handleButtonClick.bind(this, this.state),
								popContainer: document.querySelector('.table-button-type'),
								tabindex:-1
							})}
						</div>
					</div>
					<div class='autoheight'>
						<HotKeys
							keyMap={map}
							handlers={handlers}
						>
							{this.getTable(evidenceColumns, evidenceRows)}
						</HotKeys>
						{totalLine(evidenceData, saveData, this.state.json,tableWidth)}
					</div>
				</div>

				<AssidModal
					hotKeyboard={true}
					voucherType={'voucher'}
					showOrHide={AssistAccModalShow}
					assData={
						getrow && evidenceData.rows[getrow - 1] &&evidenceData.rows[getrow - 1].ass&& evidenceData.rows[getrow - 1].ass.length!=0 ? (
							evidenceData.rows[getrow - 1].ass
						) : (
							currentRowPrevass
						)
					}
					assid={
						getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].assid ? (
							evidenceData.rows[getrow - 1].assid.value
						) : (
							''
						)
					}
					pk_accasoa={apportionmentValue.pk_accasoa}
					prepareddate={apportionmentValue.prepareddate}
					pk_accountingbook={apportionmentValue.pk_accountingbook}
					pk_org={apportionmentValue.pk_unit}
					onConfirm={(assDatas) => {
						this.handleAssistAccModal(AssistAccModalShow, assDatas);
					}}
					handleClose={() => {
						let AssistAccModalShow = false;
						this.setState({ AssistAccModalShow });
					}}
					ViewModel={this.ViewModel}
				/>

				<ChangeNumberModal
					show={changeNumberShow}
					title={this.state.json['20021005card-000272']}/* 国际化处理： 维护信息*/
					loadForm={findRows}
					onConfirm={(e) => {
						this.changeUnit(e);
					}}
					onCancel={() => {
						let changeNumberShow = false;
						this.setState({ changeNumberShow });
					}}
				/>
				<ApportionmentModal
					show={apportionmentShow}
					title={this.state.json['20021005card-000273']}/* 国际化处理： 快速分摊*/
					loadForm={apportionmentValue}
					onConfirm={(e) => {
						this.ApportAction(apportionmentShow, e);
					}}
					onCancel={() => {
						let apportionmentShow = false;
						this.setState({ apportionmentShow });
					}}
				/>
				{/* <Loading fullScreen showBackDrop={true} show={this.state.isLoading} /> */}
				<SaveErrorModal
					show={SaveErrorModalShow}
					title={this.state.json['20021005card-000274']}/* 国际化处理： 分录标错*/
					defaultValue={{
						errorMessage: evidenceData.rows[getrow - 1]&&evidenceData.rows[getrow - 1].errmessage && evidenceData.rows[getrow - 1].errmessage.value
					}}
					onConfirm={(e) => {
						this.saveErrorMessage(SaveErrorModalShow, e);
					}}
					onCancel={() => {
						let SaveErrorModalShow = false;
						this.setState({ SaveErrorModalShow });
					}}
				/>

				<VrifyModal
					show={VrifyModalShow}
					title={this.state.json['20021005card-000275']}/* 国际化处理： 及时核销*/
					rtverifyData={rtverifyData}
					sendData={{
                        paraInfo:saveData.paraInfo,
						pk_voucher: saveData.pk_voucher && voucherView ? saveData.pk_voucher.value : 
						self.datavoucher,
						pk_detail:
							saveData.details &&
							saveData.details.length != 0 &&
							getrow &&
							voucherView &&
							saveData.details[getrow - 1] &&
							saveData.details[getrow - 1].pk_detail
								? saveData.details[getrow - 1].pk_detail.value
								: self.detailvalue
					}}
					//cashFlow={status.evidenceRows}
					// getCashFlow={(e) => {
					// 	this.setState({
					// 		saveData: e,
					// 		VrifyModalModalShow: false
					// 	})
					// }}
					refreshVoucherData={()=>{//及时核销处理成功之后刷新数据
						this.refreshVoucherData(saveData,saveData.paraInfo,true,'update')
					}}
					onCancel={() => {
						let VrifyModalShow = false;
						this.setState({ VrifyModalShow });
					}}
				/>
				<VrifyRefModal
					show={VrifyRefModalShow}
					title={this.state.json['20021005card-000276']}/* 国际化处理： 参照核销*/
					// querydata={{ data: this.loadData() }}
					querydata={verifyRefData}
					updataAmount={(v) => {
						//待补充 同步计算全局，集团
						let { getrow, voucherStatus, evidenceData } = this.state;
						if (voucherStatus == 'update') {
							//区别新增太和修改太
							saveData.details[getrow - 1].amount.value = v.money_Y;
							saveData.details[getrow - 1].m_voDebitDetails = v.m_voDebitDetails;
							saveData.details[getrow - 1].m_voCreditDetails = v.m_voCreditDetails;
							if( v.direct =='credit'&&saveData.details[getrow - 1].expand.verifyno){
								if(!saveData.details[getrow - 1].verifyno){
									saveData.details[getrow - 1].verifyno={}
								}
								saveData.details[getrow - 1].verifyno.value=v.m_voCreditDetails[0].m_VerifyNo;
								saveData.details[getrow - 1].expand.verifyno.value=v.m_voCreditDetails[0].m_VerifyNo;
							}else if(saveData.details[getrow - 1].expand&&saveData.details[getrow - 1].expand.verifyno){
								if(!saveData.details[getrow - 1].verifyno){
									saveData.details[getrow - 1].verifyno={}
								}
								saveData.details[getrow - 1].verifyno.value=v.m_voDebitDetails[0].m_VerifyNo;
								saveData.details[getrow - 1].expand.verifyno.value=v.m_voDebitDetails[0].m_VerifyNo;
							}
							let { data, currInfo } = self.exportData(saveData.details[getrow - 1]);
							let countValue = amountconvert(data, currInfo, 'amount');
							countRow(saveData.details[getrow - 1], countValue, v.direct =='credit'?'localdebitamount':'localcreditamount');
							this.totleCount();
							this.setState({
								saveData,
								VrifyRefModalShow:false
							});
						} else {
							evidenceData.rows[getrow - 1].amount.value = v.money_Y;
							evidenceData.rows[getrow - 1].m_voDebitDetails = v.m_voDebitDetails;
							evidenceData.rows[getrow - 1].m_voCreditDetails = v.m_voCreditDetails;
							if( v.direct =='credit'&&evidenceData.rows[getrow - 1].expand&&evidenceData.rows[getrow - 1].expand.verifyno){
								if(!evidenceData.rows[getrow - 1].verifyno){
									evidenceData.rows[getrow - 1].verifyno={};
								}
								evidenceData.rows[getrow - 1].verifyno.value=v.m_voCreditDetails[0].m_VerifyNo;
								evidenceData.rows[getrow - 1].expand.verifyno.value=v.m_voCreditDetails[0].m_VerifyNo;
							}else if(evidenceData.rows[getrow - 1].expand&&evidenceData.rows[getrow - 1].expand.verifyno){
								if(!evidenceData.rows[getrow - 1].verifyno){
									evidenceData.rows[getrow - 1].verifyno={};
								}
								evidenceData.rows[getrow - 1].verifyno.value=v.m_voDebitDetails[0].m_VerifyNo;
								evidenceData.rows[getrow - 1].expand.verifyno.value=v.m_voDebitDetails[0].m_VerifyNo;
							}
							let { data, currInfo } = self.exportData(evidenceData.rows[getrow - 1]);
							let countValue = amountconvert(data, currInfo, 'amount');
							countRow(evidenceData.rows[getrow - 1], countValue,v.direct =='credit'?'localdebitamount':'localcreditamount');
							this.totleCount();
							this.setState({
								evidenceData,
								VrifyRefModalShow:false
							});
						}
					}}
					onCancel={() => {
						let VrifyRefModalShow = false;
						this.setState({ VrifyRefModalShow });
					}}
				/>

				{LinkModalShow&&<LinkModal
					show={LinkModalShow}
					title={this.state.json['20021005card-000277']}/* 国际化处理： 联查余额*/
					querydata={//{ data: this.loadData() }
						{voucher : {...saveData,
								details:evidenceData.rows
							},
						selectIndex: this.state.getrow.toString()}}
					sendData={{
						pk_accasoa:
							evidenceData.rows &&
							evidenceData.rows.length != 0 &&
							getrow &&
							evidenceData.rows[getrow - 1] &&
							evidenceData.rows[getrow - 1].pk_accasoa
								?evidenceData.rows[getrow - 1].pk_accasoa.display
								: '',
						period:
							evidenceData.rows &&
							evidenceData.rows.length != 0 &&
							getrow &&
							evidenceData.rows[getrow - 1] &&
							evidenceData.rows[getrow - 1].period
								? evidenceData.rows[getrow - 1].period.value
								: '',
						assid:
							evidenceData.rows &&
							evidenceData.rows.length != 0 &&
							getrow &&
							evidenceData.rows[getrow - 1] &&
							evidenceData.rows[getrow - 1].assid
								? evidenceData.rows[getrow - 1].assid.display
								: '',
						pk_currtype:
							evidenceData.rows &&
							evidenceData.rows.length != 0 &&
							getrow &&
							evidenceData.rows[getrow - 1] &&
							evidenceData.rows[getrow - 1].pk_currtype
								? evidenceData.rows[getrow - 1].pk_currtype.display
								: ''
					}}
					//cashFlow={status.evidenceRows}
					// getCashFlow={(e) => {
					// 	this.setState({
					// 		saveData: e,
					// 		VrifyModalModalShow: false
					// 	})
					// }}
					onCancel={() => {
						let LinkModalShow = false;
						this.setState({ LinkModalShow });
					}}
				/>}

				<Inspection
					show={Inspectionshow}
					sourceData={this.state.sourceData}
					cancel={() => {
						let Inspectionshow = false;
						this.setState({ Inspectionshow });
					}}
				/>

				{/* {this.modalcontent()} */}
				{this.state.json['20021005card-000278']?createModal('infoModal', {
					title: this.state.json['20021005card-000278'],/* 国际化处理： 补录信息*/
					content: (
						<div className="nc-theme-form-label-c">
							<InfoModal
								pk_org={
									getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].pk_unit
								? evidenceData.rows[getrow - 1].pk_unit.value
								: unitOrg.value
									}
								defaultValue={getrow && evidenceData.rows[getrow - 1] }
								OnChange={(infoModal) => {
									self.infoModal=infoModal;
								}}
							/>
						</div>
					),
					className: 'senior',
					beSureBtnClick: this.appSureBtnClick.bind(this),
				}):null}

				{/* {showAss ? this.modalcontent() : null} */}

				{/* <NCPopconfirm trigger="click" placement="right" content={'你们'} defaultOverlayShown={true}>
				</NCPopconfirm> */}

				<div className="nc-faith-demo-div2">
					{/* 这里是附件上传组件的使用，需要传入三个参数 */}
					{showUploaderTable && (
						<NCUploader
							billId={
								saveData.details &&
								saveData.details.length != 0 &&
								getrow &&
								voucherView &&
								saveData.details[getrow - 1].pk_detail ? (
									saveData.details[getrow - 1].pk_detail.value
								) : (
									''
								)
							}
							target={target}
							uploadTitle ={this.state.json['20021005card-000094']}/* 国际化处理： 序号*/
							placement={'bottom'}
							onHide={this.onHideUploader}
							beforeUpload={this.beforeUpload.bind(this)}
							billNo={getrow}
						/>
					)}
				</div>
				
					{/*附件上传只能下载*/}
					<ApproveDown 
						show={showViewLoaderDetail}
					   	onHide={this.onHideApprove}
					  	billId={showViewLoaderDetail&&
							  saveData.details &&
							saveData.details.length != 0 &&
							getrow &&
							voucherView &&
							saveData.details[getrow - 1]&&
							saveData.details[getrow - 1].pk_detail&&saveData.details[getrow - 1].pk_detail.value ? (
								saveData.details[getrow - 1].pk_detail.value
							) : (
								''
							)}
					  />

				<AssiBalance
					title = {this.state.json['20021005card-000314']}  /* 国际化处理： 联查序时账*/
					dataout={this.state.sequenceData}
					ExportJournalRef = {(init)=>{this.AssiBalance = init}}
				/>
				

			</div>
		);
	}
}
})
function modifierMeta(props, meta, pk_acc,self) {
	let formId = 'head';
	//let formIdtail = 'tail';
	let status = props.getUrlParam('status');
	let appcode = props.getUrlParam('c') || props.getSearchParam('c');
	let adjustAddPage=props.getUrlParam('adjust');
	let genVoucher=props.getUrlParam('pagekey');
	let genVoucherTwo=props.getUrlParam('pagecode');
	meta[formId].status = status;
	//	meta.detailexpand.status = status;
	//meta[formIdtail].status = status;
	meta['print'] = printItem(appcode); //打印
	props.form.setFormItemsValue('print',{'printsubjlevel':{display: self.state.json['20021005card-000280'], value: '0'}});/* 国际化处理： 不汇总*/
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_accountingbook') {
			if(genVoucher&&genVoucher=='generate'||genVoucherTwo&&genVoucherTwo.indexOf('_gen')!=-1){
				item.disabled=true;
			}
			item.queryCondition = () => {
				return {
					TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
					appcode: appcode
				};
			};
			
		}
		if(item.attrcode=='adjustperiod'&&adjustAddPage){
			item.visible=true;
		}

		if (item.attrcode == 'adjustperiod') {
			item.queryCondition = () => {
				return {
					pk_accountingbook: pk_acc
				};
			};
		}
		if (item.attrcode == 'pk_vouchertype') {
			item.queryCondition = () => {
				return {
					GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
					isDataPowerEnable: 'Y',
					DataPowerOperationCode: 'fi',
					pk_org: pk_acc
				};
			};
		}
	});

	// meta[formId].items.map((item) => {
	// 	if (item.attrcode == 'pk_org_v') {
	// 		item.queryCondition = () => {
	// 			let data = props.form.getFormItemsValue(formId, 'vbillcode').value;
	// 			return { vbillcode: data };
	// 		};
	// 	}
	// });
	// meta[formIdtail].items.map((item) => {
	// 	if (item.attrcode == 'pk_org_v') {
	// 		item.queryCondition = () => {
	// 			let data = props.form.getFormItemsValue(formId, 'vbillcode').value;
	// 			return { vbillcode: data };
	// 		};
	// 	}
	// });
	let multiLang = props.MutiInit.getIntl('2052');
	let porCol = {
		attrcode: 'opr',
		label: multiLang && multiLang.get('20521030-0005'),
		visible: true,
		width: 200,
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			return status === 'browse' ? (
				<span
					onClick={() => {
						props.cardTable.toggleRowView(tableId, record);
					}}
				>
					{self.state.json['20021005card-000079']}
				</span>
			) : (
				<div className="currency-opr-col">
					<i
						className="iconfont icon-canzhao"
						onClick={(e) => {
							props.cardTable.openModel(tableId, 'edit', record, index);
							e.stopPropagation();
						}}
					/>
					&nbsp;&nbsp;
					<span
						className="currency-opr-del"
						onClick={(e) => {
							props.cardTable.deleteRowsByIndex(tableId, index);
							e.stopPropagation();
						}}
					>
						<i className="icon iconfont icon-shanchu" />
					</span>
				</div>
			);
		}
	};
	//meta[tableId].items.push(porCol);

	return meta;
}

function itemUpdate(item, response) {
	let {
		isShowUnit,
		currinfo,
		orgCurrinfo,
		groupCurrinfo,
		globalCurrinfo,
		excrate2,
		excrate3,
		excrate4,
		excrate2scale,
		excrate3scale,
		excrate4scale,
		unit,
		NC001,
		NC002,
		unit_v,
		groupscale, // 集团精度
		scale, // 默认币种精度
		globalscale, // 全局精度
		orgscale, // 组织精度
		pk_vouchertype,
		num,
		isEditVoucherNO,
		isAttachmentMust,
		bizDate,
		orgmode, //汇率计算方式 是否为除 true=除
		groupmode, //集团本币汇率计算方式 是否为除 true=除
		globalmode, //全局本币汇率计算方式 是否为除 true=除
		roundtype, //原币进舍规则
		orgroundtype, //本币进舍规则
		grouproundtype, //集团本币进舍规则
		globalroundtype, //全局本币进舍规则
		pricescale, //单价精度
		priceroundtype //单价进舍规则
	} = response;
	if(bizDate&&item.busidate){
		item.busidate={
			value:bizDate,
			display:bizDate
		}
	}
	if(bizDate&&item.verifydate){
		item.verifydate={
			value:bizDate,
			display:bizDate
		}
	}
	if (scale) {
		item.scale = scale;
	}
	if (orgscale) {
		item.orgscale = orgscale;
	}
	if (groupscale) {
		item.groupscale = groupscale;
	}
	if (globalscale) {
		item.globalscale = globalscale;
	}
	if (NC001) {
		item.groupType = NC001;
	}
	if (NC002) {
		item.globalType = NC002;
	}
	if (excrate2||excrate2=='0') {
		//根据核算账簿赋值默认汇率，默认币种，默认业务单元
		item.excrate2 = {
			value: excrate2
		};
	}
	if (excrate3||excrate3=='0') {
		//根据核算账簿赋值默认汇率，默认币种，默认业务单元
		item.excrate3 = {
			value: excrate3
		};
	}
	if (excrate4||excrate4=='0') {
		item.excrate4 = {
			value: excrate4
		};
	}
	if (excrate2scale) {
		item.excrate2scale = {
			value: excrate2scale
		};
	}
	if (excrate3scale) {
		item.excrate3scale = {
			value: excrate3scale
		};
	}
	if (excrate4scale) {
		item.excrate4scale = {
			value: excrate4scale
		};
	}
	item.orgmode = {
		value: orgmode
	};
	item.groupmode = {
		value: groupmode
	};
	item.globalmode = {
		value: groupmode
	};

	if (roundtype) {
		//原币进舍规则
		item.roundtype = {
			value: roundtype
		};
	}
	if (orgroundtype) {
		//本币进舍规则
		item.orgroundtype = {
			value: orgroundtype
		};
	}
	if (grouproundtype) {
		//集团本币进舍规则
		item.grouproundtype = {
			value: grouproundtype
		};
	}
	if (globalroundtype) {
		//全局本币进舍规则
		item.globalroundtype = {
			value: globalroundtype
		};
	}
	if (pricescale) {
		//单价精度
		item.pricescale = {
			value: pricescale
		};
	}
	if (priceroundtype) {
		//单价进舍规则
		item.priceroundtype = {
			value: priceroundtype
		};
	}
	if (currinfo) {
		//设置原币和组织本币币种，默认原币等于组织本币币种
		item.pk_currtype = {
			display: currinfo.display,
			value: currinfo.value
		};
	}
	if(orgCurrinfo){
		item.orgcurrtype = {
			display: orgCurrinfo.display,
			value: orgCurrinfo.value
		};
	}
	if (groupCurrinfo) {
		//集团币种
		item.groupcurrtype = {
			display: groupCurrinfo.display,
			value: groupCurrinfo.value
		};
	}
	if (globalCurrinfo) {
		//全局币种
		item.globalcurrtype = {
			display: globalCurrinfo.display,
			value: globalCurrinfo.value
		};
	}
	if (unit) {
		item.pk_unit = {
			display: unit.display,
			value: unit.value
		};
	}
	if (unit_v) {
		item.pk_unit_v = {
			display: unit_v.display,
			value: unit_v.value
		};
	}
}

Welcome = createPage({
	//	initTemplate: initTemplate,
	mutiLangCode: '2002',
 	orderOfHotKey: ['head'],
	// hotKeyChange: true
})(Welcome);

//const enhanceWelcome =Enhance(Welcome) 
export default Welcome;
