const settkleArray = [ 'bankaccount', 'billtype', 'checkstyle', 'checkno', 'checkdate' ];
const verifyArray = [ 'verifyno', 'verifydate' ];
export default function voucher(response) {
    let {voucher,paraInfo}=response;
    let { NC001, NC002, isShowUnit, excrate2, excrate3, unit, isShowNum } = paraInfo;
    let checkedArray=[];
    let freeValue = [];
    if (voucher.details && voucher.details.length != 0) {
        // for (let i = 0, len =voucher.details; i < len; i++) {
        // }
        voucher.details.map((item, i) => {
            //渲染是否有动态新增列，有待优化
            checkedArray.push(false);
            item.key = ++i;
            if (NC001) {
                item.groupType = NC001;
            }
            if (NC002) {
                item.globalType = NC002;
            }
            if (item.isShowNum && item.isShowNum.value) {
                //根据返回数据判断是否有数量单价
                item.flag = true;
                //evidenceData.newline[flag]=true;
            }
            if (item.direction.value && item.direction.value == 'D') {
                item.amount = {
                    value: item.debitamount.value,
                    editable: item.debitamount.editable
                };
            } else {
                item.amount = {
                    value: item.creditamount.value,
                    editable: item.creditamount.editable
                };
            }

            if (item.settle && !item.settle.value) {
                freeValue = freeValue.filter((v, i, a) => {
                    return !settkleArray.includes(v.attrcode);
                });
            }
            if (item.verify && !item.verify.value) {
                freeValue = freeValue.filter((v, i, a) => {
                    return !verifyArray.includes(v.attrcode);
                });
            }
            item.childform = freeValue;
        });
        voucher.checkedArray=checkedArray
    }
	return voucher;
}
