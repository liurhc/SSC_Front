import React, { Component } from 'react';
import {base, getLangCode} from 'nc-lightapp-front';
const {NCDiv} = base;
import setScale from '../../../../public/common/amountFormat.js';
import {addThousandMark} from '../../../../public/common/amountFormat.js';
import { convertCurrency } from '../../../../public/components/oftenApi';

/**
 * 合计行
 */
export default function totalLine(evidenceData, saveData, json,tableWidth) {
    let scale = saveData.paraInfo && saveData.paraInfo.orgscale ? saveData.paraInfo.orgscale : '2';
    let roundtype = saveData.paraInfo && saveData.paraInfo.orgroundtype ? saveData.paraInfo.orgroundtype : '4';
    let totalAmount = getTotalAmount(evidenceData);
    let langCode = getLangCode();
    //总计
    let totaldebit = setScale(totalAmount.totaldebit, scale, roundtype);
    let totalcredit = setScale(totalAmount.totalcredit, scale, roundtype);
    let totalamount = setScale(Math.abs(totaldebit - totalcredit), scale, roundtype);
    let totalcaps = upperCaseMoney(totalamount == 0 ? totaldebit : 0, json);

    //财务科目
    let totalfinacrebit = setScale(totalAmount.totalfinacredit, scale, roundtype);
    let totalfinacdebit = setScale(totalAmount.totalfinacdebit, scale, roundtype);
    let totalfinacamount = setScale(Math.abs(totalfinacdebit - totalfinacrebit), scale, roundtype);
    let totalfinacaps = upperCaseMoney(totalfinacamount == 0 ? totalfinacdebit : 0, json);

    //预算科目
    let totalbugetcredit = setScale(totalAmount.totalbugetcredit, scale, roundtype);
    let totalbugetdebit = setScale(totalAmount.totalbugetdebit, scale, roundtype);
    let totalbugetamount = setScale(Math.abs(totalbugetdebit - totalbugetcredit), scale, roundtype);
    let totalbugetcaps = upperCaseMoney(totalbugetamount == 0 ? totalbugetdebit : 0, json);

    return (
        <NCDiv areaCode={NCDiv.config.BOTTOM}>
        <div className="total-line nc-bill-search-area nc-theme-area-split-bc" id="totalLine" style={{width:tableWidth}}>
            {total({
                "totalamount":{
                    linetitle: '',//json['20021005card-000309']/*'总计'*/,
                    total: totalamount,
                    totalcaps: totalcaps,
                    totaldebit: totaldebit,
                    totalcredit: totalcredit
                },
            "totalfinacamount":{
                linetitle: json['20021005card-000310']/*'财务科目'*/,
                total: totalfinacamount,
                totalcaps: totalfinacaps,
                totaldebit: totalfinacdebit,
                totalcredit: totalfinacrebit
            },
            "totalbugetamount":{
                linetitle: json['20021005card-000311']/*'预算科目'*/,
                total: totalbugetamount,
                totalcaps: totalbugetcaps,
                totaldebit: totalbugetdebit,
                totalcredit: totalbugetcredit
            }
            //     linetitle: '总计',
            //     total: totalamount,
            //     totalcaps: totalcaps,
            //     totaldebit: totaldebit,
            //     totalcredit: totalcredit
            // }, json)}
            // {saveData.paraInfo&&saveData.paraInfo.isParallel ? total({
            //     linetitle: '财务科目',
            //     total: totalfinacamount,
            //     totalcaps: totalfinacaps,
            //     totaldebit: totalfinacdebit,
            //     totalcredit: totalfinacrebit
            // }, json) : ''}
            // {saveData.paraInfo&&saveData.paraInfo.isParallel ? total({
            //     linetitle: '预算科目',
            //     total: totalbugetamount,
            //     totalcaps: totalbugetcaps,
            //     totaldebit: totalbugetdebit,
            //     totalcredit: totalbugetcredit
            // }, json) : ''}
           }, json,saveData,"simpchn"==langCode)}
        </div>
        </NCDiv>
    )
}

function total(param, json,saveData,isShowCaps) {
    return (
        <div>
            <ul>
            {!(saveData.paraInfo&&saveData.paraInfo.isParallel) ?
                <li>
                    <span className="nc-theme-common-font-c">{param.totalamount.linetitle}</span>
                </li>
                :
                <div>
                    <li>
                        <span className="nc-theme-common-font-c">{param.totalfinacamount.linetitle}</span>
                    </li>
                    <li>
                        <span className="nc-theme-common-font-c">{param.totalbugetamount.linetitle}</span>
                    </li>
                </div>
                }
            </ul>
            <ul>
            {!(saveData.paraInfo&&saveData.paraInfo.isParallel) ?
                <li>
                    <span className="total-item total-sum nc-theme-common-font-c">
                        {json['20021005card-000282']/**合计差额*/}：
                        <span className="total-number">{addThousandMark(param.totalamount.total)}</span>
                    </span>
                </li>
            :
                <div>
                    <li>
                        <span className="total-item total-sum nc-theme-common-font-c">
                            {json['20021005card-000282']/**合计差额*/}：
                            <span className="total-number">{addThousandMark(param.totalfinacamount.total)}</span>
                        </span>
                    </li>
                    <li>
                        <span className="total-item total-sum nc-theme-common-font-c">
                            {json['20021005card-000282']/**合计差额*/}：
                            <span className="total-number">{addThousandMark(param.totalbugetamount.total)}</span>
                        </span>
                    </li>
                </div>
            }
            </ul>
            <ul>
                {isShowCaps ? (
                    !(saveData.paraInfo && saveData.paraInfo.isParallel) ?
                        <li>
                            <span className="total-caps nc-theme-common-font-c">
                                {json['20021005card-000283']/**大写合计*/}：
                        <span className="total-caps-count">{param.totalamount.totalcaps}</span>
                            </span>
                        </li>
                        :
                        <div>
                            <li>
                                <span className="total-caps nc-theme-common-font-c">
                                    {json['20021005card-000283']/**大写合计*/}：
                            <span className="total-caps-count">{param.totalfinacamount.totalcaps}</span>
                                </span>
                            </li>
                            <li>
                                <span className="total-caps nc-theme-common-font-c">
                                    {json['20021005card-000283']/**大写合计*/}：
                            <span className="total-caps-count">{param.totalbugetamount.totalcaps}</span>
                                </span>
                            </li>
                        </div>) : null
                }
            </ul>
            <ul>
            {!(saveData.paraInfo&&saveData.paraInfo.isParallel) ?
                <li>
                    <span className="total-item nc-theme-common-font-c">
                        {json['20021005card-000284']/**组织借方合计*/}：
                        <span className="total-number">{addThousandMark(param.totalamount.totaldebit)}</span>
                    </span>
                </li>
            :
                <div>
                    <li>
                        <span className="total-item nc-theme-common-font-c">
                            {json['20021005card-000284']/**组织借方合计*/}：
                            <span className="total-number">{addThousandMark(param.totalfinacamount.totaldebit)}</span>
                        </span>
                    </li>
                    <li>
                        <span className="total-item nc-theme-common-font-c">
                            {json['20021005card-000284']/**组织借方合计*/}：
                            <span className="total-number">{addThousandMark(param.totalbugetamount.totaldebit)}</span>
                        </span>
                    </li>
                </div>
                }
            </ul>
            <ul>
            {!(saveData.paraInfo&&saveData.paraInfo.isParallel) ?
                <li>
                    <span className="total-item nc-theme-common-font-c">
                        {json['20021005card-000285']/**组织贷方合计*/}：
                        <span className="total-number">{addThousandMark(param.totalamount.totalcredit)}</span>
                    </span>
                </li>
            :
                <div>
                    <li>
                        <span className="total-item nc-theme-common-font-c">
                            {json['20021005card-000285']/**组织贷方合计*/}：
                            <span className="total-number">{addThousandMark(param.totalfinacamount.totalcredit)}</span>
                        </span>
                    </li>
                    <li>
                        <span className="total-item nc-theme-common-font-c">
                            {json['20021005card-000285']/**组织贷方合计*/}：
                            <span className="total-number">{addThousandMark(param.totalbugetamount.totalcredit)}</span>
                        </span>
                    </li>
                </div>
                }
            </ul>
        </div>
    );
}

function upperCaseMoney(amount, json) {
    if (amount < 0) {
        return json['20021005card-000297'] + convertCurrency(json, Math.abs(amount));
    } else if (amount == 0) {
        return '';
    } else {
        return convertCurrency(json, amount);
    }
}

function getTotalAmount(evidenceData) {
    let details = evidenceData.rows;
    if (!details || details.length == 0)
        return 0;

    let totalfinacredit = 0;
    let totalfinacdebit = 0;
    let totalbugetcredit = 0;
    let totalbugetdebit = 0;
    let totalcredit = 0;
    let totaldebit = 0;
    for (let i = 0; i < details.length; i++) {
        let detail = details[i];
        let localcreditamount = Number(detail.localcreditamount && detail.localcreditamount.value ? detail.localcreditamount.value : 0);
        let localdebitamount = Number(detail.localdebitamount && detail.localdebitamount.value ? detail.localdebitamount.value : 0)
        totalcredit = totalcredit + localcreditamount;
        totaldebit = totaldebit + localdebitamount;
        if (detail.pk_accasoa && detail.pk_accasoa.value && detail.pk_accasoa.value.length > 0) {
            let accproperty = detail.pk_accasoa.accproperty ? detail.pk_accasoa.accproperty : (detail.accsubjaccproperty ? detail.accsubjaccproperty.value : '0');
            if (accproperty == '1') {
                //预算科目
                totalbugetcredit = totalbugetcredit + localcreditamount;
                totalbugetdebit = totalbugetdebit + localdebitamount;
            } else {
                //财务科目
                totalfinacredit = totalfinacredit + Number(detail.localcreditamount && detail.localcreditamount.value ? detail.localcreditamount.value : 0);
                totalfinacdebit = totalfinacdebit + Number(detail.localdebitamount && detail.localdebitamount.value ? detail.localdebitamount.value : 0);
            }
        }
    }

    return { totalfinacredit, totalfinacdebit, totalbugetcredit, totalbugetdebit, totalcredit, totaldebit }
}
