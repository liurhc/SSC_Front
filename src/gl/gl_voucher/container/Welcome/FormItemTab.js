import React, { Component } from 'react';
import { high, base, ajax ,DongbaToLocalTime} from 'nc-lightapp-front';
import moment from 'moment';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCRow: Row,
	NCCol: Col,
	NCForm: Form,
	NCTooltip,
	NCTZDatePickClientTime
} = base;
import createScript from '../../../public/components/uapRefer.js';
import { formItemTabOnEnter} from './events';
import './formtab.less';

const { NCFormItem: FormItem } = Form;
export default class FormItemTab extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tabs: [],
			listItem: {}, //模板数据对应值
			openindex: '',
			settle: false,
			verify: false,
			pk_org: '', //参照过滤
			querydata: {}
		};
	}
	componentWillReceiveProps(nextProp) {
		let self = this;
		let freeValue = nextProp.tabs;
		let { listItem, tabs, pk_org } = self.state;
		if (freeValue && freeValue.length != 0) {
			freeValue.forEach((item, i) => {
				let key = {
					value: '',
					display: ''
				};
				listItem[item.attrcode] = key;
			});
			if (nextProp.defaultValue) {
				for (let e in nextProp.defaultValue) {
					if (e == 'cashflowname') {
						listItem.cashflow = nextProp.defaultValue[e];
					}
					if (listItem.hasOwnProperty(e)) {
						listItem[e] = JSON.parse(JSON.stringify(nextProp.defaultValue[e]));
						if ( (e == 'checkdate' || e == 'verifydate') && (!nextProp.defaultValue[e].value)) {//&&nextProp.defaultValue[e].value!=null
							listItem[e].value = nextProp.prepareddate;
						}
					}
					
				}
			}
			if (nextProp.pk_org) {
				pk_org = nextProp.pk_org;
			}
			self.setState(
				{
					listItem,
					pk_org,
					tabs: freeValue,
					openindex: nextProp.freeKey,
					querydata: nextProp.querydata
				},
				() => {
				}
			);
		}
	}

	queryList = (data) => {
		let self = this;
		let { voucherView } = self.props;
		let { listItem, pk_org } = self.state;
		let moduleId='formItemTab';
		return data.length != 0 ? (
			data.map((item, i) => {
				switch (item.itemtype) {
					case 'refer':
						let referUrl = item.refcode + '.js';
						if (!self.state[item.attrcode]) {
							{
								createScript.call(self, referUrl, item.attrcode);
							}
						} else {
							return (
								<Col xs={4} md={4} className="tabItem" fieldid={`${item.attrcode}_div`}>
									<Col xs={4} md={4} className="tabLabel">
										{item.label}
									</Col>
									<Col xs={8} md={8} className="tabInput">
									<div id={`hot-key-${moduleId}-${item.attrcode}-${i}`}>
										{self.state[item.attrcode] && !voucherView ? (
											self.state[item.attrcode]({
												fieldid:item.attrcode,
												value: {
													refname: listItem[item.attrcode].display,
													refpk: listItem[item.attrcode].value
												},
												disabled:listItem[item.attrcode]&&listItem[item.attrcode].editable ? listItem[item.attrcode].editable : false,
												queryCondition: () => {
													return {
														pk_org: self.state.pk_org
													};
												},
												onChange: (v) => {
													listItem[item.attrcode].display = v.refname;
													listItem[item.attrcode].value = v.refpk;
													// listItem[item.attrcode].display = v.refname
													this.setState(
														{
															listItem
														},
														() => {
															this.props.OnChange(item.attrcode, v, self.props.freeKey);
														}
													);
												},
												onEnter:(event)=>{
													formItemTabOnEnter(event,this,item.attrcode,`hot-key-${moduleId}-${item.attrcode}-${i}`,i,listItem)
												}
											})
										) : (
											<NCTooltip trigger="hover" placement={'top'} inverse overlay={listItem[item.attrcode].display}>
											<span fieldid={item.attrcode}>{listItem[item.attrcode].display}</span>
											</NCTooltip>
										)}
										</div>
									</Col>
								</Col>
							);
						}
						break;
					case 'datepicker':
					case 'datetimepicker':
						return (
							<Col xs={4} md={4} className="tabItem" fieldid={`${item.attrcode}_div`}>
								<Col xs={4} md={4} className="tabLabel">
									{item.label}
								</Col>
								<Col xs={8} md={8} className="tabInput">
								<div id={`hot-key-${moduleId}-${item.attrcode}-${i}`}>
									{!voucherView ? (
										<NCTZDatePickClientTime
											format={"YYYY-MM-DD"}
											fieldid={item.attrcode}
											name={item.attrcode}
											showClear={true}
											disabled={listItem[item.attrcode].editable}
											placeholder={data.label}
											isRequire={true}
											showTime={false}
											value={listItem[item.attrcode]&&listItem[item.attrcode].value?listItem[item.attrcode].value:''}
											onChange={(v) => {
												listItem[item.attrcode].value = v;
												this.setState(
													{
														listItem
													},
													() => {
														this.props.OnChange(item.attrcode, v, self.props.freeKey);
													}
												);
											}}
											onEnter={(event)=>{
												formItemTabOnEnter(event,this,item.attrcode,`hot-key-${moduleId}-${item.attrcode}-${i}`,i,listItem)
											}}
										/>
									) : (
										<span fieldid={item.attrcode}>{
											listItem[item.attrcode].value?DongbaToLocalTime(moment(listItem[item.attrcode].value)).format('YYYY-MM-DD'):''
										}</span>
									)}
									</div>
								</Col>
							</Col>
						);
						break;
					case 'input':
						return (
							<Col xs={4} md={4} className="tabItem" fieldid={`${item.attrcode}_div`}>
								<Col xs={4} md={4} className="tabLabel">
									{item.label}
								</Col>
								<Col xs={8} md={8} className="tabInput">
								<div id={`hot-key-${moduleId}-${item.attrcode}-${i}`}>
									{!voucherView ? (
										<FormControl
											fieldid={item.attrcode}
											value={listItem[item.attrcode].value}
											disabled={item.attrcode == 'cashflow' ? true : false}
											onChange={(v) => {
												listItem[item.attrcode].value = v;
												this.setState(
													{
														listItem
													},
													() => {
														this.props.OnChange(item.attrcode, v, self.props.freeKey);
													}
												);
											}}
											onEnter={(event)=>{
												formItemTabOnEnter(event,this,item.attrcode,`hot-key-${moduleId}-${item.attrcode}-${i}`,i,listItem)
											}}
										/>
									) : (
										<NCTooltip trigger="hover" placement={'top'} inverse overlay={listItem[item.attrcode].value}>
										<span fieldid={item.attrcode}>{listItem[item.attrcode].value}</span>
										</NCTooltip>
									)}
									</div>
								</Col>
							</Col>
						);
						break;
					case 'number':
						return (
							<Col xs={4} md={4} className="tabItem" fieldid={`${item.attrcode}_div`}>
								<Col xs={4} md={4} className="tabLabel">
									{item.label}
								</Col>
								<Col xs={8} md={8} className="tabInput">
								<div id={`hot-key-${moduleId}-${item.attrcode}-${i}`}>
									{!voucherView ? (
										<FormControl
											fieldid={item.attrcode}
											value={listItem[item.attrcode].value}
											disabled={listItem[item.attrcode].editable}
											onChange={(v) => {
												listItem[item.attrcode].value = v;
												this.setState(
													{
														listItem
													},
													() => {
														this.props.OnChange(item.attrcode, v, self.props.freeKey);
													}
												);
											}}
											onEnter={(event)=>{
												formItemTabOnEnter(event,this,item.attrcode,`hot-key-${moduleId}-${item.attrcode}-${i}`,i,listItem)
											}}
										/>
									) : (
										<span fieldid={item.attrcode}>{listItem[item.attrcode].value}</span>
									)}
									</div>
								</Col>
							</Col>
						);
						break;
					default:
						<Col xs={4} md={4}>
							<div />
						</Col>;
						break;
				}
			})
		) : (
			<Col />
		);
	};

	render() {
		let self = this;
		let { tabs } = self.state;
		let { freeKey } = self.props;
		return <Row className='formTab'>{this.queryList(tabs)}</Row>;
	}
}
