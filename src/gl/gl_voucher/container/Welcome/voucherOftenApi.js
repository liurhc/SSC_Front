export function emptyEnter(code, originData) {
	switch (code) {
		case 'localcreditamount':
			if (originData[code].value) {
				if (originData.groupdebitamount) {
					originData.groupdebitamount = {
						//集团空格
						...originData.groupdebitamount,
						value: originData.groupcreditamount && originData.groupcreditamount.value
					};
				}
				if (originData.groupcreditamount) {
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: ''
					};
				}

				if (originData.globaldebitamount) {
					originData.globaldebitamount = {
						//全局
						...originData.globaldebitamount,
						value: originData.globalcreditamount && originData.globalcreditamount.value
					};
				}

				if (originData.globalcreditamount) {
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: ''
					};
				}

				originData.localdebitamount = {
					...originData.localdebitamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
			} else {
				if (originData.groupcreditamount) {
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: originData.groupdebitamount && originData.groupdebitamount.value
					};
				}
				if (originData.groupdebitamount) {
					originData.groupdebitamount = {
						//集团空格
						...originData.groupdebitamount,
						value: ''
					};
				}

				if (originData.globalcreditamount) {
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: originData.globaldebitamount && originData.globaldebitamount.value
					};
				}

				if (originData.globaldebitamount) {
					originData.globaldebitamount = {
						//全局
						...originData.globaldebitamount,
						value: ''
					};
				}

				originData[code] = {
					...originData[code],
					value: originData.localdebitamount.value
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: ''
				};
			}
			break;
		case 'localdebitamount':
			if (originData[code].value) {
				if (originData.groupcreditamount) {
					//组织集团缺乏判断后续添加
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: originData.groupdebitamount && originData.groupdebitamount.value
					};
				}

				if (originData.groupdebitamount) {
					originData.groupdebitamount = {
						...originData.groupdebitamount,
						value: ''
					};
				}

				if (originData.globalcreditamount) {
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: originData.globaldebitamount && originData.globaldebitamount.value
					};
				}

				if (originData.globaldebitamount) {
					originData.globaldebitamount = {
						...originData.globaldebitamount,
						value: ''
					};
				}
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
			} else {
				if (originData.groupdebitamount) {
					originData.groupdebitamount = {
						//集团空格
						...originData.groupdebitamount,
						value: originData.groupcreditamount && originData.groupcreditamount.value
					};
				}
				if (originData.groupcreditamount) {
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: ''
					};
				}

				if (originData.globaldebitamount) {
					originData.globaldebitamount = {
						//全局
						...originData.globaldebitamount,
						value: originData.globalcreditamount && originData.globalcreditamount.value
					};
				}
				if (originData.globalcreditamount) {
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: ''
					};
				}

				originData[code] = {
					...originData[code],
					value: originData.localcreditamount.value
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: ''
				};
			}
			break;

		case 'groupcreditamount':
			if (originData[code].value) {
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: originData.localcreditamount && originData.localcreditamount.value
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: ''
				};
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: originData.globalcreditamount && originData.globalcreditamount.value
				};
				originData.globalcreditamount = {
					...originData.globalcreditamount,
					value: ''
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
			} else {
				originData[code] = {
					...originData[code],
					value: originData.groupdebitamount && originData.groupdebitamount.value
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: ''
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: originData.localdebitamount && originData.localdebitamount.value
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: ''
				};
				originData.globalcreditamount = {
					...originData.globalcreditamount,
					value: originData.globaldebitamount && originData.globaldebitamount.value
				};
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: ''
				};
			}
			break;
		case 'groupdebitamount':
			if (originData[code].value) {
				originData.groupcreditamount = {
					...originData.groupcreditamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: originData.localdebitamount && originData.localdebitamount.value
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: ''
				};
				originData.globalcreditamount = {
					//全局
					...originData.globalcreditamount,
					value: originData.globaldebitamount && originData.globaldebitamount.value
				};
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: ''
				};
			} else {
				originData[code] = {
					...originData[code],
					value: originData.groupcreditamount && originData.groupcreditamount.value
				};
				originData.groupcreditamount = {
					...originData.groupcreditamount,
					value: ''
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: originData.localcreditamount && originData.localcreditamount.value
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: ''
				};
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: originData.globalcreditamount && originData.globalcreditamount.value
				};

				originData.globalcreditamount = {
					//全局
					...originData.globalcreditamount,
					value: ''
				};
			}

			break;
		case 'globalcreditamount':
			if (originData[code].value) {
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: originData.localcreditamount && originData.localcreditamount.value
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: ''
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: originData.groupcreditamount && originData.groupcreditamount.value
				};
				originData.groupcreditamount = {
					...originData.groupcreditamount,
					value: ''
				};
			} else {
				originData[code] = {
					...originData[code],
					value: originData.globaldebitamount && originData.globaldebitamount.value
				};
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: ''
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: originData.localdebitamount && originData.localdebitamount.value
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: ''
				};
				originData.groupcreditamount = {
					...originData.groupcreditamount,
					value: originData.groupdebitamount && originData.groupdebitamount.value
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: ''
				};
			}
			break;

		case 'globaldebitamount':
			if (originData[code].value) {
				originData.globalcreditamount = {
					...originData.globalcreditamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: originData.localdebitamount && originData.localdebitamount.value
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: ''
				};
				originData.groupcreditamount = {
					//全局
					...originData.groupcreditamount,
					value: originData.groupdebitamount && originData.groupdebitamount.value
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: ''
				};
			} else {
				originData[code] = {
					...originData[code],
					value: originData.globalcreditamount && originData.globalcreditamount.value
				};
				originData.globalcreditamount = {
					...originData.globalcreditamount,
					value: ''
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: originData.localcreditamount && originData.localcreditamount.value
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: ''
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: originData.groupcreditamount && originData.groupcreditamount.value
				};
				originData.groupcreditamount = {
					//全局
					...originData.groupcreditamount,
					value: ''
				};
			}
			break;

		default:
			break;
	}
}


export function updateDirection(originData) {
	if(originData.direction&&originData.direction.value&&originData.direction.value=="D"){
		originData.direction={
			value:'C'
		}
		if(originData.cashflow&&Array.isArray(originData.cashflow)){
			originData.cashflow.forEach((item,index) => {
				item.direct={
					...item.direct,
					display:'贷',
					value:'贷'
				}
			});
		}
	}else{
		if(originData.direction&&originData.direction.value&&originData.direction.value=="C"){
			originData.direction={
				value:'D'
			}
			if(originData.cashflow&&Array.isArray(originData.cashflow)){
				originData.cashflow.forEach((item,index) => {
					item.direct={
						...item.direct,
						display:'借',
						value:'借'
					}
				});
			}
		}
	}
	
}

export function buttonStatusDetails(props, params, saveData) {
	if (params) {
		if (saveData && saveData.detailmodflag && saveData.detailmodflag.value === false) {
			//若为false 增删按钮不可用
			props.button.setButtonDisabled({
				deline: true,
				copyline: true,
				cutline: true,
				pasteline: true,
				linkgroup: false,
				linkbal: false,
				linksquence: false,
				linkbudget: false,
				bodyattach: false,
				verify: false
			});
			//props.button.setButtonDisabled(['','','','',''], false);
		} else {
			if (saveData.pk_accountingbook.value) {
				props.button.setButtonDisabled(
					[
						'deline',
						'bodyattach',
						'signmistakeb',
						'addinfo',
						'copyline',
						'cutline',
						'pasteline',
						'linkgroup',
						'linkbal',
						'linksquence',
						'linkbudget',
						'verify'
					],
					false
				);
			}
		}
	} else {
		if (saveData.pk_accountingbook.value) {
			props.button.setButtonDisabled(
				[
					'deline',
					'bodyattach',
					'signmistakeb',
					'addinfo',
					'copyline',
					'cutline',
					'pasteline',
					'linkgroup',
					'linkbal',
					'linksquence',
					'linkbudget',
					'verify'
				],
				true
			);
		} else {
			props.button.setButtonDisabled(
				[
					'addline',
					'deline',
					'bodyattach',
					'signmistakeb',
					'addinfo',
					'copyline',
					'cutline',
					'pasteline',
					'linkgroup',
					'linkbal',
					'linksquence',
					'linkbudget',
					'verify'
				],
				true
			);
		}
	}
}

export function countRow(originData, countValue, key) {
	//跟新计算后的值
	let keyString;
	if (key != 'amount') {
		if (key == 'globalcreditamount' || key == 'globaldebitamount') {
			keyString = key.substring(6);
		} else {
			keyString = key.substring(5);
		}
	} else {
		keyString = 'amount';
	}
	for (let item in countValue) {
		if (
			originData.hasOwnProperty(item) ||
			item.indexOf('localamount') != -1 ||
			item.indexOf('groupamount') != -1 ||
			item.indexOf('globalamount') != -1 ||
			item.indexOf('quantity') != -1 ||
			item.indexOf('price') != -1
		) {
			if (item == 'quantity' && originData.debitquantity) {
				//数量特殊处理
				originData.debitquantity = {
					...originData.debitquantity,
					value: countValue.quantity
				};
			} else {
				if(originData[item]){
					originData[item] = {
						...originData[item],
						value: countValue[item]
					};
				}
			}
			if (item == 'localamount') {
				if (keyString != 'creditamount' && keyString != 'debitamount') {
					if (originData.localcreditamount.value) {
						originData.localcreditamount = {
							...originData.localcreditamount,
							value: countValue[item]
						};
					} else {
						originData.localdebitamount = {
							...originData.localdebitamount,
							value: countValue[item]
						};
					}
				} else {
					if (keyString == 'creditamount') {
						originData.localcreditamount = {
							...originData.localcreditamount,
							value: countValue[item]
						};
						originData.localdebitamount = {
							...originData.localdebitamount,
							value: ''
						};
					} else {
						originData.localdebitamount = {
							...originData.localdebitamount,
							value: countValue[item]
						};
						originData.localcreditamount = {
							...originData.localcreditamount,
							value: ''
						};
					}
				}
			}
			if (item == 'groupamount'&& originData.groupcreditamount) {
				if (keyString != 'creditamount' && keyString != 'debitamount') {
					//originData.direction && originData.direction.value == 'C'
					if (originData.groupcreditamount&&originData.groupcreditamount.value) {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: countValue[item]
						};
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: ''
						};
					} else {
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: countValue[item]
						};
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: ''
						};
					}
				} else {
					if (keyString == 'creditamount') {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: countValue[item]
						};
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: ''
						};
					} else {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: ''
						};
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: countValue[item]
						};
					}
				}
			}
			if (item == 'globalamount'&& originData.globalcreditamount) {
				if (keyString != 'creditamount' && keyString != 'debitamount') {
					//originData.direction && originData.direction.value == 'C'
					if (originData.globalcreditamount&&originData.globalcreditamount.value) {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: countValue[item]
						};
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: ''
						};
					} else {
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: countValue[item]
						};
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: ''
						};
					}
				} else {
					if (keyString == 'creditamount') {
						//全局特殊处理
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: countValue[item]
						};
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: ''
						};
					} else {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: ''
						};
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: countValue[item]
						};
					}
				}
			}
		}
	}
}

export function memoryHistry(record) {
	record['memoryValue']={
		amount:record.amount.value,
		localcreditamount:record.localcreditamount.value,
		localdebitamount:record.localdebitamount.value,
		groupcreditamount:record.groupcreditamount&&record.groupcreditamount.value,
		groupdebitamount:record.groupdebitamount&&record.groupdebitamount.value,
		globalcreditamount:record.globalcreditamount&&record.globalcreditamount.value,
		globaldebitamount:record.globaldebitamount&&record.globaldebitamount.value,
		debitquantity:record.debitquantity&&record.debitquantity.value,
		price:record.price&&record.price.value
	}
}

export function throttle(fn,delay) {
	let last=0;//上一次觸發會調的時間
	let timer=null;//定時器
	return function() {
		let self=this;
		let arg=arguments;
		let now=+new Date();
		if(now-last<delay){
			clearTimeout(timer)
			timer=setTimeout(() => {
				last=now
				fn.apply(self,arg)
			}, delay);
		}else{
			last=now;
			fn.apply(self,arg)
		}
	}

}


// 比较两个值(基本类型值或对象）是否一样
export function compare(a, b) {
	if (typeof a === 'object' && typeof b === 'object') {
		if (a && b) {
			// 去掉undefined
			a = JSON.parse(JSON.stringify(a));
			b = JSON.parse(JSON.stringify(b));
			var akeys = Object.keys(a),
				bkeys = Object.keys(b);
			if (akeys.length === bkeys.length) {
				for (var i = 0, l = akeys.length; i < l; i++) {
					var flag = compare(a[akeys[i]], b[akeys[i]]);
					if (!flag) {
						return false;
					}
				}
				return true;
			} else {
				return false;
			}
		} else {
			return Object.is(a, b);
		}
	} else {
		return a == b;
	}
}


export function isObj(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'Object';
	}
	

export function fmoney(s, n) {
		/*
		 * 参数说明：
		 * s：要格式化的数字
		 * n：保留几位小数
		 * */
		if(s){
			n = n > 0 && n <= 20 ? n : 2;
			s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
			var l = s.split(".")[0].split("").reverse(),
				r = s.split(".")[1],
				t = "";
			for (var  i = 0; i < l.length; i++) {
				t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
			}
			return t.split("").reverse().join("") + "." + r;
		}
		
	}

export function cutData(cutQueryData,querydata) {
	let newclone=querydata;
		for(let i=0,len=newclone.length;i<len;i++){
			let everyLine=newclone[i];
			let copyobj={};
			for(let y in everyLine){
				if(everyLine.hasOwnProperty(y)){
					if(y!=='childform'&&y!=='expand'){
						copyobj[y]=everyLine[y];
					}
					if(isObj(everyLine[y])&&("value" in everyLine[y])){
						if((everyLine[y].value==null||everyLine[y].value=='')&&(everyLine[y].display==null||everyLine[y].display=='')){
							delete copyobj[y];
							continue;
						}
						let loadValue=everyLine[y].value;
						let loadDisplay=everyLine[y].display;
						copyobj[y]={};
						copyobj[y].value=loadValue
						if(y=='explanation'){
							copyobj[y]={
								value:everyLine[y].display?everyLine[y].display:everyLine[y].value,
								display:everyLine[y].display?everyLine[y].display:everyLine[y].value
							}
						}
						if(y=='assid'){
							copyobj[y].display=loadDisplay;
						}
					}else{
						if(y=='expand'){//展开子表放置details
							let expandItem=everyLine[y];
							for(let k in expandItem){
								if(expandItem[k].value){
									copyobj[k]=expandItem[k]
								}
							}
						}
		
					}
				}
			}
			cutQueryData.push(copyobj)
		}
}