import React, { PureComponent } from 'react';
import { pageInfoClick } from './events';
import UseOftenModal from '../UseOftenModal';
import ManageOftenModal from '../ManageOftenModal';
import SaveErrorModal from '../SaveErrorModal';
import LocationModal from '../locationModal';
import CashFlow from '../Cashflow';
import { onButtonClick } from './events';
import { high, base, ajax, deepClone, toast, promptBox, cardCache,getMultiLang,createPageIcon,useJS,viewModel,cacheTools} from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
import {save} from './events/buttonAction';
import { createPrint, confirmPrint } from '../../voucher_list/list/print';
import './headButton.less';
import { formId, dataSourceTable } from '../../../public/components/constJSON';
import vouchersaveUpdateCache from './events/vouchersaveUpdateCache.js';
const {
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRow: Row,
	NCCol: Col,
	NCMessage: Message,
	NCTable: Table,
	NCCheckbox: Checkbox,
	NCNumber,
	NCHotKeys:HotKeys,
	NCDiv
} = base;

const { NCUploader, ApproveDown } = high;

//const deepClone = require('../../../public/components/deepClone');
import './index.less';
const styleType = {
	marginRight: 5
};
//let {Transfer} = high;
//判断数组对象是否有相同值
const ifSomeKey = function(orgin, param) {
	const obj = {};
	return orgin.reduce((pre, next) => {
		let des = next[param];
		obj[des] = obj[des] ? ++obj[des] : 1;
		if (obj[des] > 1) {
			return (pre = true);
		}
		return pre;
	}, false);
};
useJS.setConfig({
	'sscrp/rppub/components/image/index': "../../../../sscrp/rppub/components/image/index.js",
	'uap/common/components/printOnClient/index': "../../../../uap/common/components/printOnClient/index.js"
})
//  @withShell
let ButtonHead=useJS(["sscrp/rppub/components/image/index","uap/common/components/printOnClient/index",],function(image,print){
return class ButtonHead extends PureComponent {
	static defaultProps = {
		multiSelect: {
			type: 'checkbox',
			param: 'key'
		}
	};

	constructor(props) {
		//每定义一个state 都要添加备注
		super(props);
		this.state = {
		//	title: this.state.json['20021005card-000064'],/* 国际化处理： 记账凭证*/
			abandonShow: 'N', //Y是作废凭证，N取消作废
			UseOftenModalShow: false, //调用常用模板模态框
			ManageOftenModalShow: false, //调用管理模板模态框
			SaveErrorModalShow: false, //标错模态框
			locationModalShow: false, //定位模态框
			CashFlowModalShow: false, //现金流量模态框
			isSingleItem: 'N', //分录标错判断
			showViewLoader: false, //浏览附件
			button: '',
			printQuery: [],
			
			pkVoucher: '', //保存新增添加字段
			convertData: [], //及时折算数据
			checkedAll: false,
			checkedArray: [],
			attachvalue: '', //附单数据赋值
			contentMessage: {
				value: '',
				index: ''
			},
			pk_accperiodscheme:'',//定位会计期间过滤
			message: '',
			showUploader: false, //附件上传
			target: null,
			signData: '',
			json:{},
			inlt:null
		};
		this.instantProductVoucher = false; //折算标识
		this.isInstantPrint = false; //打印
		this.isVerify = false; //核销
		this.verifyIndex = '-1';
		this.saveType = 'save';
		this.loadQuery=[];
		this.image=window['sscrp/rppub/components/image/index'];
		this.printOnClient=print;
	}

	componentWillMount() {
		let callback= (json,status,inlt) =>{
			this.setState({json:json,inlt},()=>{
				//initTemplate.call(this, this.props);
				// this.loadDept();
				this.loadQuery= [
					{
						itemName: this.state.json['20021005card-000099'],/* 国际化处理： 凭证类别*/
						itemType: 'refer',
						itemKey: 'pk_vouchertype',
						config: { refCode: 'uapbd/refer/fiacc/VoucherTypeDefaultGridRef' }
					},
					{
						itemName: this.state.json['20021005card-000100'],/* 国际化处理： 会计期间*/
						itemType: 'refer',
						itemKey: 'period',
						//config:{refCode:"../../../uapbd/refer/org/AccountBookTreeRef"},
						config: { refCode: 'uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef' },
						queryGridUrl: '',
						refType: 'grid'
					},
					{ itemName: this.state.json['20021005card-000101'], itemType: 'textInput', itemKey: 'num' }/* 国际化处理： 凭证号*/
				]
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}

	componentWillReceiveProps(nextProp) {
		// if(nextProp.pkaccount.value){
		// 	let { loadQuery }=this.state;
		// 	let pkacc={itemName:'核算账簿',itemType:'num',itemKey:'pk_accountbook'};
		// 	loadQuery.unshift(pkacc);
		// 	this.setState({
		// 		loadQuery
		// 	})
		// }
	}

	// shouldComponentUpdate(nextProps, nextState) {

	// 	// if (this.props.color !== nextProps.color) {
	// 	//   return true;
	// 	// }
	// 	if (this.state.UseOftenModalShow !== nextState.UseOftenModalShow) {
	// 	  return true;
	// 	}
	// 	if (this.state.ManageOftenModalShow !== nextState.ManageOftenModalShow) {
	// 		return true;
	// 	}
	// 	if (this.state.SaveErrorModalShow !== nextState.SaveErrorModalShow) {
	// 		return true;
	// 	}
	// 	if (this.state.CashFlowModalShow !== nextState.CashFlowModalShow) {
	// 		return true;
	// 	}
	// 	 return false;
	//   }

	onSelect = ({ key }) => {};
	onVisibleChange = (visible) => {};

	//调用常用模板 num 1:新增（替换为后端新模板）模板  2： 追加（追加分录）
	reloadModal = (UseOftenModalShow, data, num) => {
		const settkleArray = [ 'bankaccount', 'billtype', 'checkstyle', 'checkno', 'checkdate' ];
		const verifyArray = [ 'verifyno', 'verifydate' ];
		let freeValue = [];
		if (num == 1) {
			this.setState(
				{
					UseOftenModalShow: false
				},
				() => {
					toast({ content: this.state.json['20021005card-000242'], color: 'success' });/* 国际化处理： 调用常用完成*/
					this.props.updateState(data, this.props.status.voucherView, false, this.props.status.voucherStatus);
				}
			);
		}
		if (num == 2) {
			let { evidenceData,saveData } = this.props.status;
			let evidenceDataClone = deepClone(evidenceData);
			if (data.voucher.details && data.voucher.details.length != '0') {
				saveData.details = evidenceDataClone.rows.concat(data.voucher.details);
				data.voucher=saveData
			}
			this.setState(
				{
					UseOftenModalShow: false
				},
				() => {
					toast({ content: this.state.json['20021005card-000242'], color: 'success' });/* 国际化处理： 调用常用完成*/
					this.props.updateState(data, this.props.status.voucherView, false, this.props.status.voucherStatus);
				}
			);
		}
	};

	// 回调管理常用模板模态框
	handleManageOftenDatas = () => {
		this.setState({
			ManageOftenModalShow: false
		});
	};

	//定位查询数据
	locationQuery = (listitem, locationModalShow) => {
		let self = this;
		if (JSON.stringify(listitem.period.values) == '{}') {
			//会计期间默认值处理
			let disPeriod = listitem.period.display.split('-');
			let year = disPeriod[0];
			let periodDay = disPeriod[1];
			listitem.period.values = {
				periodyear: {
					value: year
				},
				accperiodmth: {
					value: periodDay
				}
			};
		}
		let data = {
			pk_accountingbook: [ this.props.pkaccount.value ],
			year_start: listitem.period.values.periodyear.value,
			year_end: listitem.period.values.periodyear.value,
			period_start: listitem.period.values.accperiodmth.value,
			period_end: listitem.period.values.accperiodmth.value,
			pk_vouchertype: listitem.pk_vouchertype[0],
			pageType: 'card',
			num_start: listitem.num.value,
			num_end: listitem.num.value
		};
		let url = '/nccloud/gl/voucher/voucherQuery.do';
		ajax({
			url,
			data: data,
			success: function(response) {
				if (response.data) {
					//	let addData=voucherQuery(response.data)
					//	addData.voucherView=self.props.status.voucherView;
					self.props.updateState(response.data, self.props.status.voucherView);
				}
			}
		});
		this.setState({
			locationModalShow: false
		});
	};

	//凭证标错模态框 确定
	saveErrorMessage = (MsgModalAll, e) => {
		let self = this;
		let { saveData } = self.props.status;
		let saveDataClone = deepClone(saveData);
		let { rows } = self.props.status.evidenceData;
		//修改会计期间
		//saveData.period.value="03"
		if (self.state.isSingleItem == 'Y') {
			for (let i = 0, len = saveData.details.length; i < len; i++) {
				if (saveData.details[i].detailindex.value == self.state.detailIndex) {
					saveData.details[i].errmessage.value = e;
					saveData.details[i].errmessage.display = e;
				}
			}
			self.state.isSingleItem == 'N';
		} else {
			saveDataClone.period.value = saveDataClone.period.value.split('-')[1];
			saveDataClone.errmessage.value = e;
			//saveDataClone.errmessageeh.value=e;
		}
		if (saveDataClone.details && saveDataClone.details.length != 0) {
			saveDataClone.details.forEach(function(v, i, a) {
				if (v.ass) {
					delete v.ass;
				}
				if (v.cashflow) {
					delete v.cashflow;
				}
			});
		}
		let url = '/nccloud/gl/voucher/saveerror.do';
		ajax({
			url,
			data: saveDataClone,
			success: function(response) {
				//更新缓存数据，使得列表数据是最新的
				vouchersaveUpdateCache(
					'update',
					'pk_voucher',
					response.data.voucher.pk_voucher.value,
					response.data,
					formId,
					dataSourceTable
				);
				toast({ content: self.state.json['20021005card-000102'], color: 'success' });/* 国际化处理： 已作标错处理*/
				let { data } = response;
				data.voucher.abandonShow = 'Y';
				self.props.updateState(data, true);
			}
		});
		self.setState({
			SaveErrorModalShow: false
		});
	};

	handleRefresh = () => {
		let localUrl = window.location.href;
		if (localUrl.indexOf('?') != -1) {
			let idName = localUrl.split('?')[1].split('=')[1];
			this.queryList(idName);
			setGlobalStorage('localStorage','localSearchId', idName);
		}
	};

	//首张->末张
	handlelistFirst = () => {
		let state = this.props.status.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20021005card-000103'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let searchData = JSON.parse(getGlobalStorage('localStorage','SearchData'));
		let searchId = searchData[0].pk_voucher.value;
		if (searchId) {
			this.queryList(searchId);
			setGlobalStorage('localStorage','localSearchId', searchId);
		}
	};
	handlelistPrevious = () => {
		let state = this.props.status.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20021005card-000103'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let isCopy = this.props.status.copyStatus;
		if (isCopy == 'copy') {
			let localUrl = window.location.href;
			if (localUrl.indexOf('?') != -1) {
				let idName = getGlobalStorage('localStorage','localSearchId');
				this.queryList(idName);
			}
			return;
		}
		let searchData = JSON.parse(getGlobalStorage('localStorage','SearchData'));
		let localUrl = window.location.href;
		if (localUrl.indexOf('?') != -1) {
			let idName = getGlobalStorage('localStorage','localSearchId');
			if (!idName) {
				idName = localUrl.split('?')[1].split('=')[1];
			}
			for (let i = 0, len = searchData.length; i < len; i++) {
				if (searchData[i].pk_voucher.value == idName) {
					if (i - 1 > 0 || i - 1 == 0) {
						let searchId = searchData[i - 1].pk_voucher.value;
						this.queryList(searchId);
						setGlobalStorage('localStorage','localSearchId', searchId);
					} else {
						Message.create({ content: this.state.json['20021005card-000104'], color: 'warning' });/* 国际化处理： 已经是第一条*/
						// toast({ content: '已经是第一条', color: 'warning' });
					}
				}
			}
		}
	};
	handlelistNext = () => {
		let state = this.props.status.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20021005card-000103'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let searchData = JSON.parse(getGlobalStorage('localStorage','SearchData'));
		let localUrl = window.location.href;
		if (localUrl.indexOf('?') != -1) {
			let idName = getGlobalStorage('localStorage','localSearchId');
			if (!idName) {
				idName = localUrl.split('?')[1].split('=')[1];
			}
			for (let i = 0, len = searchData.length; i < len; i++) {
				if (searchData[i].pk_voucher.value == idName) {
					if (i + 1 < len) {
						let searchId = searchData[i + 1].pk_voucher.value;
						this.queryList(searchId);
						setGlobalStorage('localStorage',"localSearchId", searchId);
					} else {
						Message.create({ content: this.state.json['20021005card-000105'], color: 'warning' });/* 国际化处理： 已经是最后一条*/
					}
				}
			}
		}
	};
	handlelistEnd = () => {
		let state = this.props.status.voucherStatus;
		if (state == 'update') {
			toast({ content: this.state.json['20021005card-000103'], color: 'warning' });/* 国际化处理： 已更改表单，请保存后重试*/
			return;
		}
		let searchData = JSON.parse(getGlobalStorage('localStorage','SearchData'));
		let searchId = searchData[searchData.length - 1].pk_voucher.value;
		if (searchId) {
			this.queryList(searchId);
			setGlobalStorage('localStorage','localSearchId', searchId);
		}
	};

	handlelist = () => {
		window.location.href = '../../voucher_list/list/index.html?isChecked=' + 'true';
	};

	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		let selIds = [];
		let { convertData } = self.state;
		let ifHaveSome = ifSomeKey(convertData, 'desorgbook');
		if (ifHaveSome) {
			toast({ content: self.state.json['20021005card-000243'], color: 'warning' });/* 国际化处理： 存在相同目的账簿,不允许多选*/
			return;
		}
		// var d = [{des: 'qwe',b: 'e'}, {des: 'qwe',b: 'e'},{des: 'q34e',b: 'ff'},{des: 'fe',b: 'cce'}];
		// var dd = d.reduce((pre, next) => {
		// 	pre[next['des']] = pre[next['des']] ? ++pre[next['des']] : 1
		// 	return pre;
		// },{});
		// var flag = Object.values(dd).some(item => item > 1);
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}
		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		});
	};
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let { contentMessage, convertData } = self.state;
		contentMessage.value = record.refname;
		let checkedArray = self.state.checkedArray.concat();
		for (var i = 0; i < convertData.length; i++) {
			if (convertData[i].desorgbook == record.desorgbook) {
				checkedArray[i] = false;
			}
		}
		checkedArray[index] = !self.state.checkedArray[index];
		self.setState({
			checkedArray: checkedArray,
			contentMessage
		});
	};

	renderColumnsMultiSelect(columns) {
		const { data, checkedArray } = this.state;
		const { multiSelect } = this.props;

		let select_column = {};
		let indeterminate_bool = false;
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					title: (
						<Checkbox
							className="table-checkbox"
							checked={this.state.checkedAll}
							indeterminate={indeterminate_bool && !this.state.checkedAll}
							onChange={this.onAllCheckChange}
						/>
					),
					key: 'checkbox',
					dataIndex: 'checkbox',
					width: '5%',
					render: (text, record, index) => {
						return (
							<Checkbox
								className="table-checkbox"
								checked={this.state.checkedArray[index]}
								onChange={this.onCheckboxChange.bind(this, text, record, index)}
							/>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}

	appSureBtnClick = () => {
		let self = this;
		let { checkedArray, convertData, pkVoucher } = self.state;
		let { status } = self.props;
		let ifConvert = {
			pk_voucher: status.saveData.pk_voucher ? status.saveData.pk_voucher.value : pkVoucher,
			rules: []
		};
		let url = '/nccloud/gl/voucher/convertRealTime.do';
		for (let i = 0; i < convertData.length; i++) {
			if (checkedArray[i]) {
				ifConvert.rules.push(convertData[i]);
			}
		}
		if (ifConvert.rules.length != '0') {
			ajax({
				url,
				data: ifConvert,
				success: (res) => {
					let { message, desVoucher } = res.data;
					self.setState({
						message: message,
						desVoucher: desVoucher
					});
					self.props.buttonload.modal.show('openModal');
				}
			});
		} else {
			if (checkedArray.length != 0) {
				toast({ content: self.state.json['20021005card-000244'], color: 'warning' });/* 国际化处理： 请勾选折算规则*/
			}else if(self.isVerify){
				this.props.verifyModal(self.verifyIndex,self.isVerify,self.verifydetailPk);
			}
		}
	};

	appSureBtnClickOpen = () => {
		let { desVoucher } = this.state;
		//后续考虑打开一个窗口，多个页签
		if (this.isInstantPrint) {
			this.props.buttonload.modal.show('print');
		}
		if(this.isVerify&&this.verifyIndex){
			if (this.verifyIndex != '-1') {
				this.props.verifyModal(this.verifyIndex,this.isVerify,this.verifydetailPk);
			}
		}
		if (desVoucher && desVoucher.length != 0) {
			desVoucher.forEach((e, i) => {
				this.props.buttonload.openTo('/gl/gl_voucher/pages/main/index.html#/Welcome', {
					pagecode: '20021005card',
					status: 'browse',
					id: e
				});
			});
		}
	};

	//附件上传
	onHideUploader = () => {
		this.setState({
			showUploader: false
		});
	};

	onHideApprove = () => {
		this.setState({
			showViewLoader: false
		});
	};

	beforeUpload(billId, fullPath, file, fileList) {
		// 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表

		let self = this;
		let { status } = self.props;
		// if(status.saveData.pk_checked && status.saveData.pk_checked.value && status.saveData.pk_checked.value != '~'){
		// 	return false
		// }

		// const isJPG = file.type === 'image/jpeg';
		// if (!isJPG) {
		// 	alert('只支持jpg格式图片');
		// }
		// const isLt2M = file.size / 1024 / 1024 < 2;
		// if (!isLt2M) {
		// 	alert(self.state.json['20021005card-000245']);/* 国际化处理： 上传大小小于2M*/
		// }
		// return isLt2M;
		return true
		// 备注： return false 不执行上传  return true 执行上传
	}

	handleClick = () => {
		let backUrl = this.props.buttonload.getUrlParam('backUrl');
		let ifstep = this.props.buttonload.getUrlParam('step');
		let ifshow = this.props.buttonload.getUrlParam('ifshowQuery');
		let type = this.props.buttonload.getUrlParam('type');
		let backAppCode = this.props.buttonload.getUrlParam('backAppCode');
		let backPageCode = this.props.buttonload.getUrlParam('backPageCode');

		let yieldCode = this.props.buttonload.getUrlParam('pagecode');
		let list = yieldCode ? yieldCode.replace(/card/g, 'list') : '20021005list';
		let appcode = this.props.buttonload.getUrlParam('appcode');
		let listback=this.props.buttonload.getUrlParam('listback');
		let isMuti=this.props.buttonload.getUrlParam('isMuti');
		//window.history.go(-1);
		let param={
			pagekey: this.props.buttonload.getUrlParam('pagekey') || '',
			step: ifstep ? ifstep : '',
			ifshowQuery: ifshow ? ifshow : '',
			type: type ? type : '',
			n: this.props.buttonload.getUrlParam('n'),
			appcode: backAppCode ? backAppCode : appcode,
			pagecode: backPageCode ? backPageCode : list,
			listback:listback,
			cardBack: true, //卡片反列表标识，避免单个跳转卡片
			fromapp:appcode, //在返回会计平台时使用的参数
			isMuti:isMuti?isMuti:''//会计平台用
		}
		let scene=this.props.buttonload.getUrlParam('scene');
		if(scene){
			param.scene=scene;
		}
		this.props.buttonload.pushTo(backUrl || '/', param);
	};

	signClick = () => {
		let self = this;
		let { signData } = self.state;
		if(!signData){
			toast({content: self.state.json['20021005card-000312']/*"请选择签字日期！"*/, color: 'warning'});
			return;
		}
		let { status } = self.props;
		let signUrl = '/nccloud/gl/voucher/sign.do';
		let data = {
			pk_voucher: status.saveData.pk_voucher.value,
			date: signData
		};
		ajax({
			url: signUrl,
			data: data,
			async: true,
			success: (res) => {
				let { success, data } = res;
				if (data.error && JSON.stringify(data.error) != '{}') {
					let errmessage = '';
					for (let key in data.error) {
						errmessage = data.error[key];
						break;
					}
					toast({ content: errmessage, color: 'warning', position: 'bottom' });
				} else if (success) {
                    vouchersaveUpdateCache(
                        'update',
                        'pk_voucher',
                        data.voucher.pk_voucher.value,
                        data,
                        formId,
                        dataSourceTable
                    );
					self.props.saveUpdate(data, true);
					toast({ content: self.state.json['20021005card-000088'], color: 'success', position: 'bottom' });/* 国际化处理： 签字成功*/
				}
			},
			error: (res) => {
				toast({ content: res.message, color: 'warning' });
			}
		});
	};

	render() {
		const { status, selectedIndex } = this.props;
		const { cardPagination, modal } = this.props.buttonload;
		const { createModal } = modal;
		const { createCardPagination } = cardPagination;

		let that = {
			//适配打印数据
			props: this.props.buttonload
		};
		const {
			attachvalue,
			message,
			target,
			showUploader,
			UseOftenModalShow,
			ManageOftenModalShow,
			SaveErrorModalShow,
			CashFlowModalShow,
			locationModalShow,
			convertData,
			showViewLoader
		} = this.state;

		let conversionColumns = [
			{
				title: (<span fieldid='index'>{this.state.json['20021005card-000094']}</span>),/* 国际化处理： 序号*/
				dataIndex: 'index',
				key: 'index',
				width: 100,
				render: (text, record, index) => {
					return <span fieldid='index'>{index + 1}</span>;
				}
			},
			{ title: (<span fieldid='soblinkname'>{this.state.json['20021005card-000246']}</span>), dataIndex: 'soblinkname', key: 'soblinkname', width: 100 },/* 国际化处理： 目的账簿*/
			{ title: (<span fieldid='rulecode'>{this.state.json['20021005card-000247']}</span>), dataIndex: 'rulecode', key: 'rulecode', width: 100 },/* 国际化处理： 规则编码*/
			{ title: (<span fieldid='rulename'>{this.state.json['20021005card-000248']}</span>), dataIndex: 'rulename', key: 'rulename', width: 100 },/* 国际化处理： 规则名称*/
			{
				title: this.state.json['20021005card-000249'],/* 国际化处理： 即时汇率*/
				dataIndex: 'realtimeRate',
				key: 'soblinkname',
				width: 100,
				render: (text, record, index) => {
					return record.realtimeRateflag ? (
						<div>
							<NCNumber
								fieldid='soblinkname'
								value={record.realtimeRate ? record.realtimeRate : ''}
								scale={Number(record.scale ? record.scale : 8)}
								//	disabled={true}
								onFocus={this.focus}
								onChange={(v) => {
									record.realtimeRate = v;
									this.setState({
										convertData
									});
								}}
							/>
						</div>
					) : (
						<div />
					);
				}
			}
		];

		let columns = this.renderColumnsMultiSelect(conversionColumns);

		let buttonType = this.props.buttonload.getUrlParam('pagekey') || 'gl_voucher'; //生成凭证控制按钮显影

		//	let crossData = getDefData('checkedData', dataSource);
		let back = this.props.buttonload.getUrlParam('backflag');
		let scene= this.props.buttonload.getUrlParam('scene');
		let dataPks=scene&&scene.indexOf('_Preview2019')==-1?cacheTools.get(scene):null;
		let backflag=(back == 'noback'||(dataPks&&dataPks.length==1&&scene&&scene.indexOf("_MadeBill")!=-1))?false:true;
		const { createBillHeadInfo } = this.props.buttonload.BillHeadInfo
		return (
			<HotKeys
				keyMap={{
					'savevoucher': 'ctrl+s',
					//'saveadd':'alt+s',
					'backPage':'alt+b'
				}}
				handlers={{'savevoucher': (e)=>{
					e.preventDefault();
					onButtonClick.call(this, status, this.props, this.state,this.props.buttonload,'save');
				},
					//'saveadd':onButtonClick.bind(this, status, this.props, this.state,this.props.buttonload,'saveadd'),
					'backPage':this.handleClick.bind(this)
				}}
			focused
			attach={document.body}
		>
			<div id="headButton" tabindex="-1">
				<div className="header">
					{/* {(status.voucherView || this.props.buttonload.getUrlParam('ifshowQuery')) && back != 'noback' ? (
						<i className="back-btn iconfont icon-fanhuishangyiji" onClick={this.handleClick} />
					) : null}
					{createPageIcon()}
					<div className="title" fieldid={`${this.props.buttonload.getUrlParam('n') || this.props.buttonload.getSearchParam('n') || this.state.json['20021005card-000250']}_title`}>
						{this.props.buttonload.getUrlParam('n') || this.props.buttonload.getSearchParam('n') || this.state.json['20021005card-000250']}
					</div> */}
						<div className="header-title-search-area">
							{createBillHeadInfo(
								{
									title: this.props.buttonload.getUrlParam('n') || this.props.buttonload.getSearchParam('n') || this.state.json['20021005card-000250'],//标题
									initShowBackBtn: ((status.voucherView || this.props.buttonload.getUrlParam('ifshowQuery')) && backflag)? true : false,
									backBtnClick: this.handleClick
								}
							)}
						</div>
					{/* TODO 这里要改一下，写样式看效果暂时写的 */}
					{status.saveData.tempsaveflag && status.saveData.tempsaveflag.value ? (
						<span className="tag tag-gray">{this.state.json['20021005card-000112']}</span>/* 国际化处理： 暂存*/
					) : status.saveData.discardflag && status.saveData.discardflag.value ? (
						<span className="tag tag-gray">{this.state.json['20021005card-000258']}</span>/* 国际化处理： 作废*/
					) : (
						''
					)}
					{/* {status.saveData.discardflag &&status.saveData.discardflag.value ? <span className="tag tag-gray">作废</span> : ''} */}
					{status.saveData.errmessage &&
					status.saveData.errmessage.value &&
					status.saveData.errmessage.value != '' ? (
						<span className="tag tag-red">{this.state.json['20021005card-000259']}</span>/* 国际化处理： 错误*/
					) : (
						''
					)}
					{status.saveData.pk_manager &&
					status.saveData.pk_manager.value &&
					status.saveData.pk_manager.value != '' ? (
						<span className="tag tag-blue">{this.state.json['20021005card-000260']}</span>/* 国际化处理： 记账*/
					) : status.saveData.pk_checked &&
					status.saveData.pk_checked.value &&
					status.saveData.pk_checked.value != '~' ? (
						<span className="tag tag-blue">{this.state.json['20021005card-000261']}</span>/* 国际化处理： 审核*/
					) : status.saveData.pk_casher &&
					status.saveData.pk_casher.value &&
					status.saveData.pk_casher.value != '~' ? (
						<span className="tag tag-blue">{this.state.json['20021005card-000262']}</span>/* 国际化处理： 签字*/
					) : (
						''
					)}
					{/* {status.saveData.pk_casher &&status.saveData.pk_casher.value ? <span className="tag tag-blue">签字</span> : ''} */}
					{status.saveData.offervoucher &&
					status.saveData.offervoucher.value &&
					status.saveData.offervoucher.value != '' ? (
						<span className="tag tag-pink">{this.state.json['20021005card-000263']}</span>/* 国际化处理： 冲销*/
					) : status.saveData.isOffer && status.saveData.isOffer.value ? (
						<span className="tag tag-pink">{this.state.json['20021005card-000264']}</span>/* 国际化处理： 被冲销*/
					) : (
						''
					)}
					{/* {status.saveData.isOffer &&status.saveData.isOffer.value ? <span className="tag tag-pink">冲销</span> : ''} */}
					{status.saveData.convertflag && status.saveData.convertflag.value ? (
						<span className="tag tag-yellow">{this.state.json['20021005card-000265']}</span>/* 国际化处理： 被折算*/
					) : (
						''
					)}
					{/* {status.saveData.convertflag &&status.saveData.convertflag.value? <span className="tag tag-yellow">折算</span> : ''} */}
					{(status.saveData.voucherkind && status.saveData.voucherkind.value == '1') ||
					(status.saveData.adjustperiod &&
						status.saveData.adjustperiod.value &&
						status.saveData.adjustperiod.value.length == 3) ? (
						<span className="tag tag-purple">{this.state.json['20021005card-000266']}</span>/* 国际化处理： 调整期凭证*/
					) : (
						''
					)}

					<div className="btn-group">
						{this.props.buttonload.button.createButtonApp({
							area: buttonType,
							buttonLimit: 3,
							onButtonClick: onButtonClick.bind(this, status, this.props, this.state),
							// popContainer: document.querySelector('.header-button-area')
						})}
					</div>
					{/* 这里是附件上传组件，需要传入三个参数 */}
					{showUploader && (
						<NCUploader
							billId={status.saveData.pk_voucher.value}
							target={target}
							placement={'bottom'}
							uploadTitle={this.state.json['20021005card-000094']}/* 国际化处理： 序号*/
							onHide={this.onHideUploader}
							beforeUpload={this.beforeUpload.bind(this)}
							// customInterface={
                            //     {
                            //         queryLeftTree:"/nccloud/gl/voucher/filelefttree.do",
                            //         queryAttachments: "/nccloud/gl/voucher/filequery.do"
                            //     }
                            // }


						/>
					)}
					{/*附件上传只能下载*/}
					 
					<ApproveDown
						show={showViewLoader}
						onHide={this.onHideApprove}
						billId={showViewLoader&&status.saveData&&status.saveData.pk_voucher?status.saveData.pk_voucher.value:''}
					/>
					

					{status.voucherView ? ( //联查凭证不需要翻页
						<div className="header-cardPagination-area">
							{createCardPagination({
								handlePageInfoChange: pageInfoClick.bind(this, this.props),
								dataSource: dataSourceTable,
							})}
						</div>
					) : (
						''
					)}
					{/* <div className="header-button-area">
						<div className="title">凭证维护</div>
						{self.state.abandonShow == 'Y' ? <span className="red">错误</span> : ''}
						{self.state.cancelShow == 'Y' ? <span className="red">作废凭证</span> : ''}
						{this.props.buttonload.button.createButtonApp({
							area: buttonType,
							buttonLimit: 3,
							onButtonClick: onButtonClick.bind(this, status, this.props, this.state),
							popContainer: document.querySelector('.header-button-area')
						})}
						{status.voucherView ? (
							<div className="header-cardPagination-area">
								{createCardPagination({
									handlePageInfoChange: pageInfoClick.bind(this, this.props)
								})}
							</div>
						) : (
							''
						)}
					</div> */}
				</div>
				{/* <div className="nc-faith-demo-div2"> */}
				{/* 这里是附件上传组件的使用，需要传入三个参数 */}
				{/* {showUploader && (
						<NCUploader
							billId={status.saveData.pk_voucher.value}
							billNo={status.saveData.num.value}
							target={target}
							placement={'bottom'}
							onHide={this.onHideUploader}
							beforeUpload={this.beforeUpload}
						/>
					)}
				</div> */}

				<UseOftenModal
					show={UseOftenModalShow}
					ref="UseOftenModal"
					title={this.state.json['20021005card-000107']}/* 国际化处理： 调用常用模板*/
					onConfirm={(useModal, num) => {
						this.reloadModal(UseOftenModalShow, useModal, num);
					}}
					onCancel={() => {
						let UseOftenModalShow = false;
						this.setState({ UseOftenModalShow });
					}}
				/>

				<SaveErrorModal
					show={SaveErrorModalShow}
					defaultValue={{
						errorMessage: status.saveData.errmessage && status.saveData.errmessage.value
					}}
					title={this.state.json['20021005card-000109']}/* 国际化处理： 标错*/
					onConfirm={(e) => {
						this.saveErrorMessage(SaveErrorModalShow, e);
					}}
					onCancel={() => {
						let SaveErrorModalShow = false;
						this.setState({ SaveErrorModalShow });
					}}
				/>

				<LocationModal
					pk_account={this.state.pk_accperiodscheme}
					pk_accountingbook={status.saveData.pk_accountingbook ? status.saveData.pk_accountingbook.value : ''}
					show={locationModalShow}
					defaultValue={{
						num: status.saveData.num,
						pk_vouchertype: status.saveData.pk_vouchertype,
						period: status.saveData.period
					}}
					loadData={this.loadQuery}
					onConfirm={(listItem) => {
						this.locationQuery(listItem, locationModalShow);
					}}
					onCancel={() => {
						let locationModalShow = false;
						this.setState({ locationModalShow });
					}}
				/>

				{/* <CashFlowModal
					show={CashFlowModalShow}
					title={'现金流量'}
					sendData={status.saveData}
					cashFlow={status.evidenceData.rows}
					getCashFlow={(e) => {
						let CashFlowModalShow = false;
						this.setState({
							CashFlowModalShow
						},()=>{
							this.props.updateState(e)
						})
					}}
					onCancel={() => {
						let CashFlowModalShow = false;
						this.setState({ CashFlowModalShow });
					}}
				/> */}

				{/* <ManageOftenModal
						show={ManageOftenModalShow}
						pk_acc={status.saveData.pk_accountingbook?status.saveData.pk_accountingbook.value:''}
						title={'维护常用模板'}
						onConfirm={(manageOftenDatas) => {
							this.handleManageOftenDatas(ManageOftenModalShow, manageOftenDatas);
						}}
						onCancel={() => {
							let ManageOftenModalShow = false;
							this.setState({ ManageOftenModalShow });
						}}
					/> */}

				{createModal('manageModal', {
					title: this.state.json['20021005card-000253'],/* 国际化处理： 维护模板*/
					draggable:false,//禁止拖拽
      				resizable:false,//禁止拖拽
					content: (
						<ManageOftenModal
							pk_acc={status.saveData.pk_accountingbook ? status.saveData.pk_accountingbook.value : ''}
							sendData={status.saveData}
							title={this.state.json['20021005card-000252']}/* 国际化处理： 维护常用模板*/
							onConfirm={(manageOftenDatas) => {
								this.handleManageOftenDatas(ManageOftenModalShow, manageOftenDatas);
							}}
							onCancel={() => {
								let ManageOftenModalShow = false;
								this.setState({ ManageOftenModalShow });
							}}
						/>
					),
					className: 'combine',
					noFooter: true
					//	beSureBtnClick: this.cashBtnClick.bind(this)
				})}

				{this.state.json['20021005card-000110']?createModal('CashFlowModal', {
					title: this.state.json['20021005card-000110'],/* 国际化处理： 现金流量*/
					content: (
						<CashFlow
							onRef={(ref)=>{this.cashFlow=ref}}
							cashFlow={status.evidenceData.rows}
							sendData={status.saveData}
							selectedIndex={selectedIndex}
							cashflowType={status.voucherView}
							getCashFlow={(e) => {
								this.props.buttonload.modal.close('CashFlowModal');
								this.props.updateState(e, status.voucherView,false,status.voucherStatus);
							}}
						/>
					),
					size: 'xlg',
					beSureBtnClick: ()=>{this.cashFlow&&this.cashFlow.onConfirm()}
				}):null}

				{this.state.json['20021005card-000254']?createModal('signModal', {
					title: this.state.json['20021005card-000254'],/* 国际化处理： 签字日期*/
					content: (
						<div>
							<Row>
								<Col sm={4} md={4} xs={4}>
									<span className="item-title">{this.state.json['20021005card-000254']}</span>
								</Col>
								<Col sm={8} md={8} xs={8}>
									<DatePicker
										value={this.state.signData}
										onChange={(v) => {
											let { signData } = this.state;
											signData = v;
											this.setState({
												signData
											});
										}}
									/>
								</Col>
							</Row>
						</div>
					),
					size: 'sm',
					beSureBtnClick: this.signClick.bind(this)
				}):null}

				{this.state.json['20021005card-000255']?createModal('attach', {
					title: this.state.json['20021005card-000255'],/* 国际化处理： 附单数据*/
					content: (
						<div>
							<NCNumber
								value={attachvalue ? attachvalue : ''}
								onChange={(v) => {
									let { attachvalue } = this.state;
									attachvalue = v;
									this.setState(
										{
											attachvalue
										},
										() => {
											this.props.loadAttachment(this.state.attachvalue);
										}
									);
								}}
							/>
						</div>
					),
					size: 'lg',
					//noFooter: false,
					beSureBtnClick: () => {
						let self = this;
						let { attachvalue } = self.state;
						let { status ,buttonload} = self.props;
						let realDetails=self.props.realDetail&&self.props.realDetail.length!=0?self.props.realDetail:status.evidenceData.rows;
						let cashHave = false;
						let cashtype = true;
						for (let i = 0, len =realDetails.length; i < len; i++) {
							let item = realDetails[i];
							if (item.cashflow && item.cashflow.length > 0) {
								cashHave = true;
							}
							if (
								item.pk_accasoa.cashtype == '1' ||
								item.pk_accasoa.cashtype == '2' ||
								item.pk_accasoa.cashtype == '3'
							) {
								cashtype = false;
							}
						}
						if (cashHave && cashtype) {
							promptBox({
								color: 'warning',
								content: self.state.json['20021005card-000227'],/* 国际化处理： 非现金凭证录入了现金流量,要继续吗？*/
								beSureBtnClick: save.bind(self, status, self.saveType, buttonload, self.state)
							});
						} else {
							save.call(self, status, self.saveType, buttonload, self.props);
						}
						
					},
					cancelBtnClick:()=>{
						let self = this;
						let { attachvalue } = self.state;
						let { status ,buttonload} = self.props;
						save.call(self, status, self.saveType, buttonload, self.props);
					}
				}):null}

				{this.state.json['20021005card-000256']?createModal('print', {
					title: this.state.json['20021005card-000256'], // 弹框表头信息/* 国际化处理： 打印*/
					content: createPrint.call(that,this), //弹框内容，可以是字符串或dom
					showCustomBtns:true,
					customBtns:this.printButton(),
					className: 'senior'
				}):null}

				{this.state.json['20021005card-000257']?createModal('exportFileModal', {
					title: this.state.json['20021005card-000257'],/* 国际化处理： 即时折算*/
					content: 
						(<NCDiv fieldid='convert' areaCode={NCDiv.config.TableCom}>
							<Table bordered columns={columns} data={convertData} />
						</NCDiv>)
						,
					size: 'lg',
					beSureBtnClick: this.appSureBtnClick.bind(this),
					cancelBtnClick:()=>{
						if(this.isVerify&&this.verifyIndex){
							if (this.verifyIndex != '-1') {
								this.props.verifyModal(this.verifyIndex,this.isVerify,this.verifydetailPk);
							}
						}
					}
				}):null}
				{this.state.json['20021005card-000257']?createModal('openModal', {
					title: this.state.json['20021005card-000257'],/* 国际化处理： 即时折算*/
					content: <span className="nc-theme-common-font-c">{message}</span>,
					size: 'lg',
					beSureBtnClick: this.appSureBtnClickOpen.bind(this),
				}):null}
			</div>
			</HotKeys>		
			);
	}

	printButton=()=>{
		let json=this.state.json;
		return (
			<div>
				<Button shape="border" colors="primary" onClick={this.previewPrint.bind(this, false)}>{json['20021005card-000300']/**预览 */}</Button>
				<Button shape="border" colors="primary" onClick={this.previewPrint.bind(this, true)}>{json['20021005card-000301']/**打印 */}</Button>
				<Button shape="border" colors="primary" onClick={this.cancelBtnClick.bind(this)}>{json['20021005card-000302']/**取消 */}</Button>
			</div>
		)
	}

	previewPrint = (isPrint) => {
		let self = this;
		let { status, buttonload } = self.props;
		//点击确定按钮事件
		let selectIndex = '-1';
		if (status.getrow) {
			selectIndex = status.getrow;
		}
		if (status.checkedAll) {
			selectIndex = '1'
		}
		confirmPrint.call(self, buttonload, selectIndex, status.saveData.pk_voucher.value,isPrint);
		if (self.isVerify) {
			self.props.verifyModal(self.verifyIndex,self.isVerify,self.verifydetailPk);
		}

	}

	cancelBtnClick = () => {
		let self = this;
		self.props.buttonload.modal.close('print');
		if (self.isVerify) {
			self.props.verifyModal(self.verifyIndex,self.isVerify,self.verifydetailPk);
		}
	}
}
})
//   Rate.propTyps={
//     rate:PropTyps.node
//   }
export default ButtonHead;
