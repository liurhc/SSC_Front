import { ajax, base, toast, deepClone } from 'nc-lightapp-front';
let tableid = 'gl_voucher';
import { isObj } from '../../../../public/components/oftenApi';
import {buttonStatusDetails ,cutData} from "../voucherOftenApi";
import { queryRelatedAppcode } from '../../../../public/common/queryRelatedAppcode.js';
export default function handleButtonClick(voucherState, props, id) {
	let record = this.state.getrow;
	let { evidenceData,getrow, saveData, voucherStatus, checkedArray } = this.state;
	let checkAfter = [];
	let { rows } = evidenceData;
	let originData = this.findByKey(record, rows);

	if (id != 'addline' && id != 'deline' && id != 'copyline' && id != 'pasteline') {
		if (!getrow) {
			toast({ content: this.state.json['20021005card-000234'], color: 'warning' });/* 国际化处理： 请选择分录行*/
			return false;
		}
	}

	switch (id) {
		case 'addline':
		let addlineLocation
			for (let i = 0, len = checkedArray.length; i < len; i++) {
				if (checkedArray[i]) {
					addlineLocation=i+1;
				}
			}
			this.addNewLine(addlineLocation ? addlineLocation : evidenceData.rows.length);
			break;
		case 'deline':
			let cancleDelete = rows.filter(function(v, i, a) {
					return checkedArray[i]&&v.ismatched&&v.ismatched.value;
				});
			if(cancleDelete.length=='0'){
				rows = rows.filter(function(v, i, a) {
					return !checkedArray[i];
				});
				rows.map((item, index) => {
					item.key = index + 1;
					checkAfter.push(false);
				});
				evidenceData.rows = rows;
				evidenceData.index = rows.length;
				saveData.details = evidenceData.rows;
				this.totleCount();
				this.setState({
					checkedAll:false,
					evidenceData,
					saveData,
					checkedArray: checkAfter
				},()=>{
					buttonStatusDetails(this.props, false, saveData)//表体按钮控制有待优化
				});
			}else{
				let message=this.state.inlt&&this.state.inlt.get('20021005card-000299', {detailnumber:cancleDelete[0].key });
				toast({ content: message, color: 'warning' });
			}
			
			break;
		case 'copyline':
			this.copyData = []; //复制数据置空操作
			for (let i = 0, len = checkedArray.length; i < len; i++) {
				if (checkedArray[i]) {	
					this.copyData.push(JSON.parse(JSON.stringify(evidenceData.rows[i])));
					
				}
				evidenceData.rows[i].ischecked=false;
			}
			for(let y=0,length=this.copyData.length;y<length;y++){
				if(this.copyData[y].ismatched){
					this.copyData[y].ismatched=null;	
				}
				this.copyData[y].ischecked=true;
			}
			this.setState({
				evidenceData
			});
			break;
		case 'addinfo': //补录信息
			let urlInfo = '/nccloud/gl/voucher/detailExpend.do';
			let dataInfo = {
				pk_accountingbook: originData.pk_accountingbook.value,
				pk_accasoa: originData.pk_accasoa.value,
				prepareddate: originData.prepareddate.value
			};
			ajax({
				url: urlInfo,
				data: dataInfo,
				success: (res) => {
					if (res.data.settle) {
						props.modal.show('infoModal');
					}
				}
			});
			break;
		case 'cutline':
			let cancleCut = rows.filter(function(v, i, a) {
				return checkedArray[i]&&v.ismatched&&v.ismatched.value;
			});
			if(cancleCut.length=='0'){
				this.copyData = []; //粘帖后数据置空操作
				for (let i = 0, len = checkedArray.length; i < len; i++) {
					if (checkedArray[i]) {
						this.copyData.push(evidenceData.rows[i]);
					}
				}
				rows = rows.filter(function(v, i, a) {
					return !checkedArray[i];
				});
				rows.map((item, index) => {
					item.key = index + 1;
					if(item.ischecked){
						checkAfter.push(true);
					}else{
						checkAfter.push(false);
					}
				});
				evidenceData.rows = rows;
				evidenceData.index = rows.length;
				this.totleCount();
				this.setState({
					evidenceData,
					checkedArray: checkAfter
				});
			}else{
				let message=this.state.inlt&&this.state.inlt.get('20021005card-000299', {detailnumber:cancleCut[0].key });
				toast({ content: message, color: 'warning' });
			}
			
			break;
		case 'pasteline':
			let pastData = deepClone(this.copyData);
			if(this.tableHeight=='350'){//动态计算表格高度,产生滚动条
				let tableArea_H = this.refs.tableArea ? getComputedStyle(this.refs.tableArea, null).height : '400px';
				let height = tableArea_H.replace('px', '') - 95;
				this.tableHeight=height;
			}

			//粘帖数据处理
			for (let i = 0, len = pastData.length; i < len; i++) {
				for (let key in pastData[i]) {
					if (key == 'pk_detail') {
						pastData[i][key].value = '';
					}
					if (key == 'Ismatched') {
						pastData[i][key].value = false;
					}
					if (key == 'modifyflag') {
						pastData[i][key].value = 'YYYYYYYYYYYYYYYYYYYY';
					}
					if (key == 'UserData') {
						pastData[i][key].value = '';
					}
					if (isObj(pastData[i][key]) && pastData[i][key].editable) {
						pastData[i][key].editable = false;
					}
				}
			}

			let pushKey =record?Number(record):pastData[pastData.length - 1].key;
			pastData.unshift(pushKey, 0);
			Array.prototype.splice.apply(evidenceData.rows, pastData);
			evidenceData.rows.forEach((item, index) => {
				//key 不建议用index 后续优化
				if(item.ischecked){
					checkAfter.push(true);
				}else{
					checkAfter.push(false);
				}
				
				item.key = ++index;
				item.detailindex={value:item.key};
				if(item.cashflow&&item.cashflow.length>0){//更新cashflow行号
					let cashflow=item.cashflow
					for(let cf=0;cf<cashflow.length;cf++){
						cashflow[cf].detailindex.value=item.key;
						cashflow[cf].detailindex.display=item.key;
					}
				}
			});
			this.setState({
				checkedAll: !this.state.checkedAll,
				checkedArray: checkAfter,
				evidenceData
			},()=>{
				this.totleCount(); //同步计算合计值
			});
			
			break;
		case 'query':
			this.setState({
				showModal: true
			});
			break;
		case 'apportion': //快速分摊
			if(getrow){
				let selectRow=rows[getrow-1];
				if(selectRow.ismatched&&selectRow.ismatched.value){
					toast({ content: this.state.json['20021005card-000316'], color: 'warning' });/**分录不允许修改，不能分摊。*/
				}else{
					this.setState({
						apportionmentShow: true
					});
				}
			}else{
				toast({ content: this.state.json['20021005card-000234'], color: 'warning' });/**请选择分录行*/
			}
			break;
		case 'signmistakeb': //分录标错
			if (getrow) {
				this.setState({
					SaveErrorModalShow: true
				});
			} else {
				toast({ content: this.state.json['20021005card-000091'], color: 'warning' });/* 国际化处理： 请选择标错分录行*/
			}

			break;
		case 'verify': //及时核销
			this.queryRtverify();
			// this.setState({
			// 	VrifyModalShow: true
			// });
			break;
		case 'verifyref': //参照核销
			evidenceData.rows.forEach((v, i, a) => {
				v.pk_accountingbook = {
					display: saveData.pk_accountingbook.display,
					value: saveData.pk_accountingbook.value
				};
				v.prepareddate = {
					display: saveData.prepareddate.value,
					value: saveData.prepareddate.value
				};
				v.pk_vouchertype = {
					display: saveData.pk_vouchertype.display,
					value: saveData.pk_vouchertype.value
				};
				v.creditamount = { display: '', value: '' };
				v.debitamount = { display: '', value: '' };
				if (v.localdebitamount.value) {
					v.debitamount = { display: v.localdebitamount.value, value: v.localdebitamount.value };
				} else {
					v.creditamount = { display: v.localdebitamount.value, value: v.localdebitamount.value };
				}
			});
			this.setState(
				{
					evidenceData
				},
				() => {
					let apport = this.loadData();
					if (apport.ismatched && apport.ismatched.value) {
						toast({ content: this.state.json['20021005card-000235'], color: 'warning' });/* 国际化处理： 当前分录已有后续业务，不能进行参照核销*/
					} else {
						this.apportionQuery(apport, saveData.paraInfo);
					}
				}
			);

			// this.setState({
			// 	evidenceData,
			// 	VrifyRefModalShow: true
			// });
			break;
		case 'linkbal':
			//联查余额
			evidenceData.rows.forEach((v, i, a) => {
				v.pk_accountingbook = {
					display: saveData.pk_accountingbook.display,
					value: saveData.pk_accountingbook.value
				};
				v.prepareddate = {
					display: saveData.prepareddate.value,
					value: saveData.prepareddate.value
				};
				v.pk_vouchertype = {
					display: saveData.pk_vouchertype.display,
					value: saveData.pk_vouchertype.value
				};
				v.creditamount = { display: '', value: '' };
				v.debitamount = { display: '', value: '' };
				if (v.localdebitamount.value) {
					v.debitamount = { display: v.localdebitamount.value, value: v.localdebitamount.value };
				} else {
					v.creditamount = { display: v.localdebitamount.value, value: v.localdebitamount.value };
				}
			});
			this.setState({
				evidenceData,
				LinkModalShow: true
			});
			break;
		case 'linkbudget': //联查预算
			let url = '/nccloud/gl/voucher/linkbudget.do';
			let dataLink = deepClone(saveData);
			if (voucherStatus == 'save') {
				dataLink.details = [ evidenceData.rows[getrow - 1] ];
			} else {
				dataLink.details = [ dataLink.details[getrow - 1] ];
			}
			ajax({
				url,
				data: dataLink,
				success: (res) => {
					if (res.data.hint) {
						toast({ content: res.data.hint, color: 'warning' });
					} else {
						this.setState({
							sourceData: res.data,
							Inspectionshow: true
						});
					}
				}
			});
			break;

		case 'linksquence':
			//联查序时账
			let sequenceparam={};
			sequenceparam.pk_accountingbook=saveData.pk_accountingbook;
			sequenceparam.year=saveData.year;
			sequenceparam.period=saveData.period;
			sequenceparam.prepareddate=saveData.prepareddate;
			sequenceparam.adjustperiod=saveData.adjustperiod;
			let selectRow={};
			if (voucherStatus == 'save') {
				selectRow=evidenceData.rows[getrow - 1];
			} else {
				selectRow=saveData.details[getrow - 1];
			}
			if(!selectRow.pk_accasoa||!selectRow.pk_accasoa.value||selectRow.pk_accasoa.value==''){
				toast({ content: this.state.json['20021005card-000319']/**所选分录没有科目，不能联查。 */, color: 'warning' })
				return;
			}
			let detail={};
			detail.pk_unit=selectRow.pk_unit;
			detail.pk_currtype=selectRow.pk_currtype;
			detail.assid=selectRow.assid;
			detail.pk_accasoa=selectRow.pk_accasoa;
			sequenceparam.details=[detail];
			sequenceparam.scene='voucherlink';
			let param=JSON.stringify(sequenceparam);
			let seqappcode=queryRelatedAppcode('20023040')/**序时账 */
			this.props.openTo('/gl/manageReport/journal/content/index.html',{
				status:param,
				appcode:seqappcode,
				pagecode:'20023040page'
			});
			// let urlS = '/nccloud/gl/accountrep/voucherlinksequence.do';
			
			// let cutQueryData=[];
			// cutData(cutQueryData,sequenceLink.details)
			// sequenceLink.details=cutQueryData
			// ajax({
			// 	url: urlS,
			// 	data: sequenceLink,
			// 	success: (res) => {
			// 		this.setState({
			// 			// showAss: true,
			// 			sequenceData: res.data
			// 		},()=>{
			// 			this.modalcontent()
			// 		});
			// 	}
			// });

			break;
		case 'bodyattach':
			if (
				(saveData.pk_checked && saveData.pk_checked.value && saveData.pk_checked.value != '~') ||
				(saveData.pk_casher && saveData.pk_casher.value && saveData.pk_casher.value != '~') ||
				(saveData.pk_manager && saveData.pk_manager.value && saveData.pk_manager.value != 'N/A') ||
				props.getUrlParam('type') == 'check' ||
				props.getUrlParam('type') == 'tally' ||
				props.getUrlParam('type') == 'query' ||
				props.getUrlParam('type') == 'sign'||((props.getUrlParam('pagecode')&&props.getUrlParam('pagecode').indexOf('link')!=-1)||
				props.getUrlParam('pagekey')=='link'||props.getUrlParam('pagekey')=='pre')
			) {
				this.setState({
					showViewLoaderDetail: true
				});
			} else {
				this.setState({
					showUploaderTable: true
				});
			}

			break;

		case 'print':
			//打印
			break;
	}
}

export function dealOperate(props, opurl, id, callBack, type) {
	// const selectedData = props.table.getCheckedRows(tableid);
	// if (selectedData.length == 0) return
	// let indexArr = [];
	// let dataArr = [];
	// let indexObj = {};

	// //只传ts和Pk_voucher主键
	// selectedData.forEach((val) => {
	//     dataArr.push(val.data.values.pk_voucher.value);
	//     indexArr.push(val.index);
	//     indexObj[val.data.values.pk_voucher.value] = val.index;
	// });
	let data = {};
	if (id == 'buleoffset' || id == 'redoffset' || id == 'offset') {
		data = {
			pk_voucher: props.saveData.pk_voucher.value,
			offsettype: type
		};
	} else {
		data = {
			pk_voucher: props.saveData.pk_voucher.value
		};
	}

	ajax({
		url: opurl,
		data: data,
		async: true,
		success: (res) => {
			let { success, data } = res;
			if (data.error && data.error.length > 0) {
				toast({ content: data.error, color: 'warning' });
			} else if (success) {
				if (id == 'delete') {
					props.table.deleteTableRowsByIndex(tableid, indexArr);
					toast({ content: this.state.json['20021005card-000081'], color: 'success' });/* 国际化处理： 删除成功*/
				} else {
					dealSuccess(props, data, id, callBack);
				}
			}
		},
		error: (res) => {
			toast({ content: res.message, color: 'warning' });
		}
	});
}

export function dealSuccess(props, data, id, callBack) {
	let succdatas = data.success;
	if (succdatas) {
		succdatas.details.map((item, i) => {
			item.key = ++i;
			if (item.price.value && item.price.value != '0') {
				item.flag = true;
				if (item.creditquantity.value) {
					//借贷数量同步显示
					item.debitquantity.value = item.creditquantity.value;
					item.creditquantity.value = '';
				}
			}
			if (item.creditamount.value && item.creditamount.value != '0') {
				item.amount = {
					value: item.creditamount.value
				};
			}
			if (item.debitamount.value && item.debitamount.value != '0') {
				item.amount = {
					value: item.debitamount.value
				};
			}
			//保存成功后添加子表字段结算字段
			//  item.childform=evidenceData.rows[item.key-1].childform;
		});
	} else {
		//针对冲销功能字段解析
		data.details.map((item, i) => {
			item.key = ++i;
			if (item.price.value && item.price.value != '0') {
				item.flag = true;
				if (item.creditquantity.value) {
					//借贷数量同步显示
					item.debitquantity.value = item.creditquantity.value;
					item.creditquantity.value = '';
				}
			}
			if (item.creditamount.value && item.creditamount.value != '0') {
				item.amount = {
					value: item.creditamount.value
				};
			}
			if (item.debitamount.value && item.debitamount.value != '0') {
				item.amount = {
					value: item.debitamount.value
				};
			}
			//保存成功后添加子表字段结算字段
			//  item.childform=evidenceData.rows[item.key-1].childform;
		});
	}
	switch (id) {
		case 'delete':
			succdatas.forEach((val) => {
				props.table.deleteTableRowsByIndex(tableId, indexObj[val.pk_voucher]);
			});
			toast({ content: this.state.json['20021005card-000081'], color: 'success' });/* 国际化处理： 删除成功*/
			break;
		case 'abandon':
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "discardflag", val.discardflag, indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000082'], color: 'success' });/* 国际化处理： 作废成功*/
			break;
		case 'unabandon':
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "discardflag", val.discardflag, indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000083'], color: 'success' });/* 国际化处理： 取消作废成功*/
			break;
		case 'check':
			//审核
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_checked", val.pk_checked, indexObj[val.pk_voucher]);
			// });
			succdatas.voucherView = true;
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000084'], color: 'success' });/* 国际化处理： 审核成功*/
			break;
		case 'uncheck':
			//取消审核
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_checked", "", indexObj[val.pk_voucher]);
			// });
			succdatas.voucherView = true;
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000085'], color: 'success' });/* 国际化处理： 取消审核成功*/
			break;
		case 'tally':
			//记账
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_manager", val.pk_manager, indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000086'], color: 'success' });/* 国际化处理： 记账成功*/
			break;
		case 'untally':
			//取消记账
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_manager", "", indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000087'], color: 'success' });/* 国际化处理： 取消记账成功*/
			break;
		case 'sign':
			//签字
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "signflag", val.signflag, indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000088'], color: 'success' });/* 国际化处理： 签字成功*/
			break;
		case 'unsign':
			//取消签字
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "signflag", val.signflag, indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000089'], color: 'success' });/* 国际化处理： 取消签字成功*/
			break;
		case 'buleoffset':
			//冲销
			data.voucherView = false;

			callBack.saveUpdate(data);
			toast({ content: this.state.json['20021005card-000090'], color: 'success' });/* 国际化处理： 冲销成功*/
			break;
		case 'redoffset':
			//冲销
			data.voucherView = false;

			callBack.saveUpdate(data);
			toast({ content: this.state.json['20021005card-000090'], color: 'success' });/* 国际化处理： 冲销成功*/
			break;
		case 'offset':
			//冲销
			data.voucherView = false;
			callBack.saveUpdate(data);
			toast({ content: this.state.json['20021005card-000090'], color: 'success' });/* 国际化处理： 冲销成功*/
	}
}
