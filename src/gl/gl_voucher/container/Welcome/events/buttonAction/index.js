import parallel from "./parallel";
import save from "./save";
import tempSave from "./tempSave";
export {parallel,save,tempSave}