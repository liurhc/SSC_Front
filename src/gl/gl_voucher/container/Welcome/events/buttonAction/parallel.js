import { ajax, toast, promptBox} from 'nc-lightapp-front';

/**
 * 平行记账
 */
export default function parallel(_this,voucherState,callBack){
	if(!voucherState.saveData.paraInfo.GLGOVPermission){//平行记账license未校验通过
		toast({ content: _this.state.json['20021005card-000303']/*"没有平行记账权限!"*/, color: 'warning', position: 'bottom' });
		return;
	}
    if(!voucherState.saveData.paraInfo.isParallel){
		//该财务组织不允许平行记账业务
		toast({ content: _this.state.json['20021005card-000304']/*"该财务核算账簿不允许平行记账业务!"*/, color: 'warning', position: 'bottom' });
		return;
	}
	
	let evidenceData=voucherState.evidenceData;
	let details =evidenceData.rows;
	let finaccAcc=false;
    let bugetAcc=false;
	let parallelAcc=false;//是否有平行记账科目
	let nparalleAcc=false;//是否有例外科目
	let newDetails=[];
	for(let i=0;i<details.length;i++){
		let detail=details[i];
		if((detail.localcreditamount&&detail.localcreditamount.value && detail.localcreditamount.value != '0') ||
		(detail.localdebitamount&&detail.localdebitamount.value && detail.localdebitamount.value != '0') ||
		(detail.groupcreditamount&&detail.groupcreditamount.value && detail.groupcreditamount.value != '0') ||
		(detail.groupdebitamount&&detail.groupdebitamount.value && detail.groupdebitamount.value != '0') ||
		(detail.globalcreditamount&&detail.globalcreditamount.value && detail.globalcreditamount.value != '0') ||
		(detail.globaldebitamount&&detail.globaldebitamount.value && detail.globaldebitamount.value != '0') ||
		(detail.debitquantity && detail.debitquantity.value && detail.debitquantity.value != '0')){
			if(detail.pk_accasoa.accproperty==0||(detail.accsubjaccproperty&&detail.accsubjaccproperty.value=='0')){
				finaccAcc=true;
				newDetails.push(detail);//将空行和预算行过滤
			}else if(detail.pk_accasoa.accproperty==1||(detail.accsubjaccproperty&&detail.accsubjaccproperty.value=='1')){
				bugetAcc=true;
            }
            
            let parallelaccounts=detail.pk_accasoa.parallelaccounts?(detail.pk_accasoa.parallelaccounts):(detail.accsubjparallel?detail.accsubjparallel.value:'N');
			let nparallelaccounts=detail.pk_accasoa.nparallelaccounts?(detail.pk_accasoa.nparallelaccounts):(detail.accsubjnparallel?detail.accsubjnparallel.value:'N');
            if(parallelaccounts=='Y'&&nparallelaccounts!='Y'){
                parallelAcc=true;
            }else if(nparallelaccounts=='Y'){
				nparalleAcc=true;
			}
		}
	}
	if(!finaccAcc){
		//没有财务分录，不能进行平行记账
		toast({ content: _this.state.json['20021005card-000305']/*"没有财务分录，不能进行平行记账!"*/, color: 'warning', position: 'bottom' });
		return;
    }
    
    if(!parallelAcc||nparalleAcc){
        //没有平行记账科目是否继续？
        promptBox({
			color: 'warning',
			title: _this.state.json['20021005card-000207']/*"提示"*/,
			content:_this.state.json['20021005card-000306']/*"该凭证不需要平行记账，是否继续平行记账处理？"*/,
			beSureBtnClick: doParallelwithCheckBugetAcc.bind(_this, voucherState, newDetails,callBack,bugetAcc)
		});
    }else{
        doParallelwithCheckBugetAcc.call(_this,voucherState, newDetails,callBack,bugetAcc)
    }
}

function doParallelwithCheckBugetAcc(voucherState, newDetails,callBack,bugetAcc){
	let _this=this;
    if(bugetAcc){
		//存在预算科目是否继续？
		promptBox({
			color: 'warning',
			title: _this.state.json['20021005card-000207']/*"提示"*/,
			content: _this.state.json['20021005card-000307']/*"已存在预算分录，平行记账会覆盖现有数据，是否继续？"*/,
			beSureBtnClick: doParallel.bind(_this, voucherState, newDetails,callBack)
		});
	}else{
		doParallel(voucherState,newDetails,callBack)
	}
}

function doParallel(voucherState,newDetails,callBack){
    let newVoucher= JSON.parse(JSON.stringify(voucherState.saveData));
	newVoucher.details=newDetails;
	newVoucher.autoParallel = { value: 'N' };
	ajax({
		url:'/nccloud/gl/voucher/parallel.do',
		data:newVoucher,
		success: (res) => {
			if(res.data&&res.data.error){
				toast({ content: res.data.error, color: 'warning', position: 'bottom' });
			}
			let status=res.data.pk_voucher&&res.data.pk_voucher.value&&res.data.pk_voucher.value!=''?'update':'save'
			callBack.updateState(res.data, false, false, status);
		}
	})
}