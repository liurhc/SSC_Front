import { ajax } from 'nc-lightapp-front';
import { toast,cardCache} from 'nc-lightapp-front';
import {formId,dataSourceTable,oldSource,} from '../../../../../public/components/constJSON';
import vouchersaveUpdateCache from '../vouchersaveUpdateCache.js';

import {cutData} from '../../voucherOftenApi';
let { setDefData} = cardCache;
/**
 * 凭证缓存
 */
export default function tempSave(props) {
	let self = this;
	let {
		nov,
		attachment,
		evidenceData,
		saveData,
		voucherStatus,
		isNumChange
	} = props;
	let url = '/nccloud/gl/voucher/tempSave.do';
	let newclone  = evidenceData.rows;
	if(newclone.length>'1'){
		 newclone = newclone.filter(function(v, i, a) {
			//分录行过滤数据
			return (
				(v.localcreditamount.value && v.localcreditamount.value != '0') ||
				(v.localdebitamount.value && v.localdebitamount.value != '0') ||
				(v.groupcreditamount.value && v.groupcreditamount.value != '0') ||
				(v.groupdebitamount.value && v.groupdebitamount.value != '0') ||
				(v.globalcreditamount.value && v.globalcreditamount.value != '0') ||
				(v.globaldebitamount.value && v.globaldebitamount.value != '0') ||
				(v.pk_accasoa.value&&v.pk_accasoa.value!='')||
				(v.debitquantity && v.debitquantity.value && v.debitquantity.value != '0')
			);
		});
	}
	let newData=[]
	if (voucherStatus != 'update') {
		cutData(newData,newclone)
		nov.value = Number(nov.value);
		attachment.value = Number(attachment.value);
		saveData.details = newData;
		saveData.oldnum = saveData.num.value;
		if (!isNumChange) {
			saveData.num.value = '0';
		}
	} else {
		if (saveData.period.value.indexOf('-') != -1) {
			let newValue = saveData.period.value.split('-')[1];
			saveData.period.value = newValue;
		}
		cutData(newData,newclone)
		saveData.details = newData;
	}
	saveData.autoParallel = { value: 'N' }
	ajax({
		url,
		data: saveData,
		success: function(response) {
			if (response.data) {
				var voucherStatus = true;
				let {warn} = response.data;
				if (warn) {
					toast({ content: warn, color: 'success', position: 'bottom' });
				} else {
					toast({ content: self.state.json['20021005card-000241'], color: 'success', position: 'bottom' });/* 国际化处理： 暂存成功*/
					
				}
				let oldValueData = JSON.stringify(response.data);
				setDefData('oldValue', oldSource, oldValueData);//保存成功后存储数据，取消处理
				////更新缓存数据，使得列表数据是最新的 
				if(saveData.pk_voucher&&saveData.pk_voucher.value){//有主键pk_voucher 修改暂存
					vouchersaveUpdateCache('update','pk_voucher',saveData.pk_voucher.value,response.data,formId,dataSourceTable);
				}else{//新增暂存
					vouchersaveUpdateCache('add','pk_voucher',response.data.voucher.pk_voucher.value,response.data,formId,dataSourceTable);
				}
				self.props.saveUpdate(response.data, voucherStatus);
			} else {
				toast({ content: voucher.error.message, color: 'warning', duration: 30000 });
			}
		}
	});
}
