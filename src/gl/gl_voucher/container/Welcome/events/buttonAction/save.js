import { toast, promptBox, ajax, cardCache } from 'nc-lightapp-front';
import {
	formId,
	dataSourceTable,
	dataSource,
	dataSourceNumber,
	oldSource,
	dataSourceSaveNext
} from '../../../../../public/components/constJSON';
import vouchersaveUpdateCache from '../vouchersaveUpdateCache.js';
let { setDefData, getDefData} = cardCache;
/**
 * 凭证保存
 */
export default function save(voucherState, status, extend, parentState) {
	let realDetails=parentState.realDetail&&parentState.realDetail.length!=0?parentState.realDetail:voucherState.evidenceData.rows;
	
	let cashHave = false;//分录中是否录了现金流量
	let cashtype = true; //是否有现金类科目
	let cashmainitem= false;//录了现金流量是否有录主表项

    let hasParallel = false; //是否有平行记账科目
	let bugetAccount = false;//是否有预算科目
	let hasNParallel=false;//是否有例外科目
	let paraInfo = voucherState.saveData.paraInfo;
	let saveData=voucherState.saveData;
	if(saveData.voucherkind&&saveData.voucherkind.value=='1'){
		//调整期凭证校验调整期是否录入
		if(!voucherState.saveData.adjustperiod||!voucherState.saveData.adjustperiod.value||voucherState.saveData.adjustperiod.value==''){
			toast({ content: this.state.json['20021005card-000317'], color: 'warning', position: 'bottom' });/* 国际化处理： 制单日期必录！请先录入制单日期*/
        	return;
		}
	}
	//let details=filterDetails(realDetails);
	
    for (let i = 0, len = realDetails.length; i < len; i++) {
		let item = realDetails[i];
        if (item.cashflow && item.cashflow.length > 0) {
			if (paraInfo.isParallel&&paraInfo.GLGOVPermission) {
				let cashflow= item.cashflow;
				for(let j=0;j<cashflow.length;j++){
					let pk_cashflow_main=cashflow[j].pk_cashflow_main;
					if(pk_cashflow_main&&pk_cashflow_main.value&&pk_cashflow_main.value.length>0){
						cashmainitem=true;//如果平行记账凭证，有主表项才提示
					}
				}
			}else{
				cashmainitem=true;
			}
            cashHave = true;//分录中录了现金流量
        }
        if (item.pk_accasoa.cashtype == '1' || item.pk_accasoa.cashtype == '2' || item.pk_accasoa.cashtype == '3') {
            cashtype = false;//有现金类科目
        } 
        let parallelaccounts = item.pk_accasoa.parallelaccounts ? (item.pk_accasoa.parallelaccounts) : (item.accsubjparallel ? item.accsubjparallel.value : 'N');
        let nparallelaccounts = item.pk_accasoa.nparallelaccounts ? (item.pk_accasoa.nparallelaccounts) : (item.accsubjnparallel ? item.accsubjnparallel.value : 'N');
        let accproperty = item.pk_accasoa.accproperty ? item.pk_accasoa.accproperty : (item.accsubjaccproperty ? item.accsubjaccproperty.value : '0');
        if (parallelaccounts == 'Y' && nparallelaccounts != 'Y' && accproperty == '0') {
            hasParallel = true;
		}
		if(nparallelaccounts=='Y'){
			hasNParallel=true;
		}
        if (accproperty == '1') {
            bugetAccount = true;
        }
	}

    if (!voucherState.saveData.prepareddate.value) {
        toast({ content: this.state.json['20021005card-000226'], color: 'warning', position: 'bottom' });/* 国际化处理： 制单日期必录！请先录入制单日期*/
        return false;
    }

    // if ((!voucherState.saveData.attachment.value || voucherState.saveData.attachment.value == '0') 
    //     &&voucherState.attachModal) {
    //     //附单数据录入
    //     this.saveType = status;
    //     if (props.attachvalue) {
    //         props.attachvalue = '';
    //     }
    //     extend.modal.show('attach');
    //     return false;
    // }
    if (cashHave && cashtype && cashmainitem) {
        promptBox({
            color: 'warning',
            content: this.state.json['20021005card-000227'],/* 国际化处理： 非现金凭证录入了现金流量,要继续吗？*/
            beSureBtnClick: confirmAutoParalle.bind(this, voucherState, status, extend, hasParallel&&!bugetAccount&&!hasNParallel, parentState)
        });
    } else {
        confirmAutoParalle.call(this, voucherState, status, extend, hasParallel&&!bugetAccount&&!hasNParallel, parentState);
    }
}

//确认是否自动平行记账
function confirmAutoParalle(voucherState, status, extend, isAutoParallel, callback) {
    //判断ZK001参数是否要自动平行记账
    let _this = this;
	let paraInfo = voucherState.saveData.paraInfo;
	voucherState.saveData.autoParallel = { value: 'N' };
	if (isAutoParallel&&paraInfo.GLGOVPermission&&
		(!voucherState.saveData.offervoucher||!voucherState.saveData.offervoucher.value||voucherState.saveData.offervoucher.value=='')/*冲销凭证不自动平行记账*/) {
        if (paraInfo.autoParallel == '1') {
            //自动生成
            autoParallel.call(_this,voucherState, status, extend,callback)
            // voucherState.saveData.autoParallel = { value: 'Y' };
            // saveButton.call(_this, voucherState, status, extend)
        } else if (paraInfo.autoParallel == '2') {
            //提示是否自动平行记账
            promptBox({
                color: 'warning',
                content:  _this.state.json['20021005card-000308']/*'有需要平行记账的科目,是否需要进行平行记账操作?'*/,
                beSureBtnClick: () => {
                    voucherState.saveData.autoParallel = { value: 'N' };
                    autoParallel.call(_this,voucherState, status, extend,callback)
                    // voucherState.saveData.autoParallel = { value: 'Y' };
                    // saveButton.call(_this, voucherState, status, extend)
                },
                cancelBtnClick: () => {
                    voucherState.saveData.autoParallel = { value: 'N' };
                    saveButton.call(_this, voucherState, status, extend)
                }
            });
        } else {
            //不生成
            voucherState.saveData.autoParallel = { value: 'N' };
            saveButton.call(_this, voucherState, status, extend)
        }
    } else {
		voucherState.saveData.autoParallel = { value: 'N' };
        saveButton.call(_this, voucherState, status, extend)
    }
}

//自动平行记账
function autoParallel(voucherState, status, extend, callback) {
    let _this=this;
	let newVoucher = JSON.parse(JSON.stringify(voucherState.saveData));
	let realDetails = _this.props.realDetail && _this.props.realDetail.length != 0 ? _this.props.realDetail : voucherState.evidenceData.rows;
	newVoucher.details=realDetails;
	newVoucher.params={isAutoParallel: true}
	ajax({
        url: '/nccloud/gl/voucher/parallel.do',
        data: newVoucher,
        success: (res) => {
            let {data} =res;
            callback.updateState(res.data, false, false, status);
            voucherState.saveData=data.voucher;
            saveButton.call(_this,voucherState,status,extend)
        }
    })
}

//保存
 function saveButton(props, status, extend) {
	let self = this;
	let {
		evidenceData,
		saveData,
		voucherStatus,
		saveNumber,
		isNumChange
	} = props;
	if (!props.saveData.pk_accountingbook.value) {
		toast({ content: self.state.json['20021005card-000236'], color: 'warning' });/* 国际化处理： 请选择核算账簿！*/
		return false;
	}
	if (status == 'saveAdd') {
		saveNumber = '2';
		saveData.isAdd = true;
	}
	//如果有主键则是修改，没有主键是新增
	if (saveData.pk_voucher && saveData.pk_voucher.value && saveData.pk_voucher.value != '') {
		voucherStatus = 'update'
	} else {
		voucherStatus = 'save'
	}
	let url = voucherStatus == 'update' ? '/nccloud/gl/voucher/update.do' : '/nccloud/gl/voucher/insert.do';
	let realDetails=self.props.realDetail&&self.props.realDetail.length!=0?self.props.realDetail:evidenceData.rows;
	let newData= filterDetails(realDetails);

	if (voucherStatus != 'update') {
		//	nov.value = Number(nov.value);
		//	attachment.value = Number(attachment.value);
		saveData.details = newData;
		saveData.oldnum = extend.form.getFormItemsValue('head', 'num').value;
		if (!isNumChange) {
			saveData.num.value = '0';
			saveData.free10 = 'VOUCHERNEWADD';
		} else {
			saveData.num.value = extend.form.getFormItemsValue('head', 'num').value;
		}
	} else {
		saveData.oldnum = extend.form.getFormItemsValue('head', 'num').value;
		if (saveData.period && saveData.period.value && saveData.period.value.indexOf('-') != -1) {
			let newValue = saveData.period.value.split('-')[1];
			saveData.period.value = newValue;
		}
		saveData.details = newData;
	}
	// return false
	ajax({
		url,
		data: saveData,
		success: function (response) {
			//会计期间赋值 保存ts pk
			if (response.data) {
				if (status !== 'saveNext') { //保存成功状态标识为联查 调用凭证保存后返回列表同步删除列表数据
					setDefData('saveSuccess', successStatus, 'success');
				}

				if (response.data.voucher && response.data.voucher.offervoucher && response.data.voucher.offervoucher.value) {
					let backData = JSON.parse(getDefData('oldValue', oldSource));
					backData.voucher.isOffer.value = true;
					deleDetal(backData)
					vouchersaveUpdateCache('update', 'pk_voucher', backData.voucher.pk_voucher.value, backData, formId, dataSourceTable);
				}
		
				var voucherStatus = true;
				var successStatus = true;//连续弹出框,只能执行一次控制标识
				let { voucher, paraInfo, warn } = response.data;
				let {
					instantProductVoucher,//折算
					isInstantPrint,//打印
					isVerify,//核销
					verifyIndex,//核销分录标记
				} = paraInfo;
				if (warn) {
					promptBox({
						color: 'warning',
						content: warn,
						hasCloseBtn: true,
						noFooter: true
					});
				} else {
					if (saveData.convertflag && JSON.stringify(saveData.convertflag) != '{}' && saveData.convertflag.value) {
						toast({ content: self.state.json['20021005card-000237'], color: 'success', duration: 'infinity' });/* 国际化处理： 修改的凭证已经被折算过,请重新进行折算*/
					} else {
						toast({ content: self.state.json['20021005card-000093'], color: 'success', position: 'bottom' });/* 国际化处理： 保存成功*/
					}
				}
				if (verifyIndex && verifyIndex != '-1') {
					self.verifyIndex = verifyIndex;
					self.verifydetailPk=voucher.details[self.verifyIndex - 1].pk_detail.value;
				}else{
					self.verifyIndex ='-1';
				}

				if ((saveData.oldnum || saveData.num.value) != voucher.num.value) {
					let message = self.state.inlt && self.state.inlt.get('20021005card-000067', { oldnum: saveData.oldnum || saveData.num.value, newnum: voucher.num.value });
					toast({ content: message, color: 'warning' });/* 国际化处理： 请注意，凭证号已更新*/
				}
				//调用凭证保存成功删除缓存数据
				let linkNumber = getDefData('number', dataSourceNumber);
				let linkData11 = getDefData('gl_voucher', dataSource)
				if (linkData11 && (linkNumber || linkNumber == '0')) {
					let newRows = linkData11['gl_voucher'].rows.filter(function (v, i, a) {//第一条保存成功删除对应行 避免少录入一行
						return i != linkNumber;
					});
					linkData11['gl_voucher'].rows = newRows;
					setDefData('gl_voucher', dataSource, linkData11)
				}
				if (status == 'saveNext') {
					setDefData('saveNext', dataSourceSaveNext, true); //保存下一张标识，返回列表一条直接跳卡片控制
					let linkData11 = getDefData('gl_voucher', dataSource)
					if (linkData11 && typeof linkData11 != 'undefined') {
						let record = linkData11['gl_voucher'].rows&&linkData11['gl_voucher'].rows[linkNumber] && linkData11['gl_voucher'].rows[linkNumber].values;
						if (record) {
							//保存&下一张
							nextPage(record).then((res) => {
								let linkNumber = getDefData('number', dataSourceNumber);
								let linkData11 = getDefData('gl_voucher', dataSource);
								//	loadVoucher(voucher,paraInfo,evidenceData,saveData)
								self.props.saveUpdate(res.data, linkNumber == linkData11['gl_voucher'].rows.length ? true : false, 'save');
								//setDefData('number', dataSourceNumber, linkNumber + 1);//跟新保存下一张序号
							});
						} else {
							toast({ content: self.state.json['20021005card-000238'], color: 'warning' });/* 国际化处理： 最后一张了*/
							self.props.saveUpdate(response.data, true);
						}
					} else {//保存下一张特殊处理
						self.props.saveUpdate(response.data, true);
					}
				} else {
					if (saveNumber != '2') {
						self.props.saveUpdate(response.data, voucherStatus);
					}
				}
				let oldValueData = JSON.stringify(response.data);
				setDefData('oldValue', oldSource, oldValueData);//保存成功后存储数据，取消处理
				//let cachData=JSON.parse(oldValueData)
				let cachData = {};
				cachData.voucher = {};
				cachData.paraInfo = response.data.paraInfo;
				for (let key in response.data.voucher) {
					if (key != 'details') {
						cachData.voucher[key] = response.data.voucher[key];
					}
				}
				if (saveData.pk_voucher && saveData.pk_voucher.value) {//有主键pk_voucher 修改保存
					//deleDetal(cachData)
					vouchersaveUpdateCache('update', 'pk_voucher', saveData.pk_voucher.value, cachData, formId, dataSourceTable);
				} else {//新增保存
					//deleDetal(cachData)
					vouchersaveUpdateCache('add', 'pk_voucher', response.data.voucher.pk_voucher.value, cachData, formId, dataSourceTable);
					let idObj = { id: response.data.voucher.pk_voucher.value, status: 1 }//1 修改 2新增  3删除
					self.props.buttonload.cardPagination.setCardPaginationId(idObj);

				}
				if (instantProductVoucher && voucher.voucherkind.value != '1') {//折算弹框标识  调整期不需要弹
					self.instantProductVoucher = true;
				}
				if (isInstantPrint) {
					self.isInstantPrint = true;
				}
				if (isVerify) {
					self.isVerify = true;
				}

				if (self.instantProductVoucher) {
					promptBox({
						color: 'warning',
						content: self.state.json['20021005card-000239'],/* 国际化处理： 是否立即生成折算凭证？*/
						beSureBtnClick: () => {
							ajax({
								url: '/nccloud/gl/voucher/fctimerulequery.do',
								data: {
									pk_accountingbook: voucher.pk_accountingbook.value,
									pk_voucher: voucher.pk_voucher.value
								},
								success: (res) => {
									let checkedArray = [];
									res.data.map((e, i) => {
										checkedArray.push(e.defaultrule);
									});
									self.setState({
										pkVoucher: voucher.pk_voucher.value,
										convertData: res.data,
										checkedArray: checkedArray
									});
								}
							});
							extend.modal.show('exportFileModal');
						},
						cancelBtnClick: () => {
							// 取消按钮点击调用函数,非必输
							//	resolve(true);
							if (self.isInstantPrint) {
								extend.modal.show('print');
							}
							if(self.isVerify&&self.verifyIndex){
								if (self.verifyIndex != '-1') {
									self.props.verifyModal(self.verifyIndex,self.isVerify,voucher.details[self.verifyIndex - 1].pk_detail.value);
								}
							}
						}
					});
					successStatus = false
				}

				if (self.isInstantPrint && successStatus) {
					promptBox({
						color: "warning",
						content: self.state.json['20021005card-000240'],/* 国际化处理： 是否立即打印？*/
						beSureBtnClick: () => {
							extend.modal.show('print');
						},
						cancelBtnClick: () => {
							if (self.isVerify && verifyIndex) {
								self.props.verifyModal(verifyIndex, voucher.pk_voucher && voucher.pk_voucher.value, voucher.details[verifyIndex - 1].pk_detail.value);
							}
						}

					})
					successStatus = false
				}

				if (self.isVerify && verifyIndex && successStatus) {
					if (verifyIndex != '-1') {
						self.verifyIndex = verifyIndex;
						self.props.verifyModal(verifyIndex, voucher.pk_voucher && voucher.pk_voucher.value, voucher.details[verifyIndex - 1].pk_detail.value);
					}
					//保存新增特殊处理及时核销参数
				}

				if (saveNumber == '2') {
					//保存新增带出默认值
					//	extend.ViewModel.setData('oldValue', voucher);
					self.props.saveAdd(voucher, paraInfo);
					//return false;
				}

			}
		},
		error: function (response) {
			toast({ content: response.message, color: 'danger', position: 'bottom', duration: 'infinity' });
			saveData.num.value = saveData.oldnum;
		}
	});
}



function nextPage(record) {
	let urlList = '/nccloud/gl/voucher/listtovoucher.do';
	return new Promise((resolve, reject) => {
		ajax({
			url: urlList,
			data: record,
			success: function (response) {
				resolve(response);

			}
		});
	});

}

function deleDetal(oldValueData) {
	for (let x in oldValueData.voucher) {//删除存入缓存的字段
		if (oldValueData.voucher.hasOwnProperty(x)) {
			if (x == 'details') {
				delete oldValueData.voucher[x]
			}
		}
	}
}

function isObj(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'Object';
}

function filterDetails(realDetails){
	let newclone = realDetails.filter(function (v, i, a) {
		//分录空行过滤数据
		return (
			(v.localcreditamount && v.localcreditamount.value && v.localcreditamount.value != '0') ||
			(v.localdebitamount && v.localdebitamount.value && v.localdebitamount.value != '0') ||
			(v.groupcreditamount && v.groupcreditamount.value && v.groupcreditamount.value != '0') ||
			(v.groupdebitamount && v.groupdebitamount.value && v.groupdebitamount.value != '0') ||
			(v.globalcreditamount && v.globalcreditamount.value && v.globalcreditamount.value != '0') ||
			(v.globaldebitamount && v.globaldebitamount.value && v.globaldebitamount.value != '0') ||
			(v.debitquantity && v.debitquantity.value && v.debitquantity.value != '0')
		);
	});
	let newData = [];
	for (let i = 0, len = newclone.length; i < len; i++) {
		let everyLine = newclone[i];
		let copyobj = {};
		for (let y in everyLine) {
			if (everyLine.hasOwnProperty(y)) {
				if (y !== 'childform' && y !== 'expand') {
					copyobj[y] = everyLine[y];
				}
				if (isObj(everyLine[y]) && ("value" in everyLine[y])) {
					if ((everyLine[y].value == null || everyLine[y].value == '') && (everyLine[y].display == null || everyLine[y].display == '')) {
						delete copyobj[y];
						continue;
					}
					let loadValue = everyLine[y].value;
					let loadDisplay = everyLine[y].display;
					copyobj[y] = {};
					copyobj[y].value = loadValue
					if (y == 'explanation') {
						copyobj[y] = {
							value: loadDisplay,
							display: loadDisplay
						}
					}
					if (y == 'assid') {
						copyobj[y].display = loadDisplay;
					}
				} else {
					if (y == 'expand') {//展开子表放置details
						let expandItem = everyLine[y];
						for (let k in expandItem) {
							if (expandItem[k].value) {
								copyobj[k] = expandItem[k]
							}
						}
					}

				}
			}
		}
		newData.push(copyobj)
	}
	return newData;
}