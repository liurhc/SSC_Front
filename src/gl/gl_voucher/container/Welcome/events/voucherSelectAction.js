import {ajax,deepClone,toast,cardCache} from 'nc-lightapp-front';
import { isObj } from '../../../../public/components/oftenApi';
let { setDefData, getDefData,addCache,getCurrentLastId,updateCache ,deleteCacheById,getNextId} = cardCache;
import {
	formId,
	dataSourceTable,
	tableId,
	dataSource,
	dataSourceCoord,
	dataSourceDetail,
	successStatus,
	dataSourceNumber,
	oldSource,
	dataSourceSaveNext
} from '../../../../public/components/constJSON';
import vouchersaveUpdateCache from './vouchersaveUpdateCache.js';
//const deepClone = require('../../../../public/components/deepClone');

export default function voucherSelectAction(props,state,key,parentFunc){
	//凭证下拉按钮
		if (update) { return false;}
		let self = this;
		let { update, copyStatus, evidenceData, saveData, errorFlag,abandonShow,cancelShow,voucherView } =props;
		let { num, pk_voucher, period, pk_casher, pk_checked, pk_manager, pk_prepared, pk_system,
			prepareddate, tempsaveflag, creator, title } = saveData;
			
		//let date= new Date();
		//let currentDate=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
		if (key == "errcorr") {//纠错 vouchervo的errmessage清空 errmessageeh保留  分录不需要管
			if(saveData.errmessage){
				saveData.errmessage.value = "";
			}
			abandonShow = "N";
			self.setState({
				abandonShow:abandonShow,
				saveData
			},()=>{
				parentFunc.abandUpdate(abandonShow)
			});
		} else if (key == "signmistake") {//标错 vouchervo 的 errmessage  errmessageeh都设置    分录标错  voucherdetail的 errmessage就行了 errmessageeh好像没啥用
			let { SaveErrorModalShow, isSingleItem } =state;
			SaveErrorModalShow = true;
			isSingleItem = 'N'
			self.setState({
				 saveData,
				SaveErrorModalShow,
				isSingleItem
			});
		} else if (`${key}` == "10") {
			window.location.href = '../account.html';
		} else if (key == "abandon") {//作废
			let url = '/nccloud/gl/voucher/abandon.do';
			let pk_voucherValue = pk_voucher.value;
			let param = { "pk_voucher": pk_voucherValue };
			ajax({
				url: url,
				data: param,
				async: true,
				success: (response) => {
					let { error, success } = response.data;
					if (success) {
						//更新缓存数据，使得列表数据是最新的 
						vouchersaveUpdateCache('update','pk_voucher',pk_voucherValue,response.data,formId,dataSourceTable);
						toast({ content: self.state.json['20021005card-000096'], color: 'success' });/* 国际化处理： 当前凭证作废处理操作成功*/
						parentFunc.cancelUpdate(response.data,"Y",true)
					} else {
						if(isObj(error)){
							for(let item in error){
								toast({ content: error[item], color: 'warning' });
							}
						}
					}
				},
				error: (res) => {
					toast({ content: res.message, color: 'danger' });
				}
			});
		} else if (key== "unabandon") {//取消作废
			let url = '/nccloud/gl/voucher/unabandon.do';
			let pk_voucherValue = pk_voucher.value;
			let param = { "pk_voucher": pk_voucherValue };
			ajax({
				url: url,
				data: param,
				async: true,
				success: (response) => {
					let { error, success } = response.data;
					if (success) {
						//更新缓存数据，使得列表数据是最新的 
						vouchersaveUpdateCache('update','pk_voucher',pk_voucherValue,response.data,formId,dataSourceTable);
						toast({ content: self.state.json['20021005card-000098'], color: 'success' });/* 国际化处理： 当前凭证取消作废处理操作成功*/
						parentFunc.cancelUpdate(response.data,"N",true)
					} else {
						if(isObj(error)){
							for(let item in error){
								toast({ content: error[item], color: 'warning' });
							}
						}
					}
				},
				error: (res) => {
					toast({ content: res.message, color: 'danger' });
				}
			});
		} else if (key == "cashflow") {//现金流量
			let { CashFlowModalShow }=state;
			CashFlowModalShow=true
			self.setState({
				CashFlowModalShow
			})
		}else if(key=="delete"){//删除凭证，显示前一张凭证,如果没有前一张就显示空凭证
			
			if(pk_voucher){
				let deleteVoucher=pk_voucher.value;
				let url = '/nccloud/gl/voucher/delete.do';
				let param={"pk_voucher":pk_voucher.value};
				ajax({
					url,
					data: param,
					async: true,
					success: (response) => {
						let nextId = getNextId(deleteVoucher, dataSourceTable);
						setDefData('oldValue', oldSource, null);//删除成功清空缓存数据
							if(response.data&&response.data.warn){
								toast({ content: warn, color: 'success' });
							}else{
								//更新缓存数据，使得列表数据是最新的 
								vouchersaveUpdateCache('delete','pk_voucher',deleteVoucher,'',formId,dataSourceTable);
								let idObj={id:nextId,status:1}//1 修改 2新增  3删除
								self.props.buttonload.cardPagination.setCardPaginationId(idObj);
								toast({ content: self.state.json['20021005card-000081'], color: 'success' });/* 国际化处理： 删除成功*/
							}
							if(nextId){
								let searchId= {
									pk_voucher:nextId
								};
								let urllist = '/nccloud/gl/voucher/query.do';
								parentFunc.updatePage(searchId,urllist);
							}else{
								parentFunc.buttonload.form.EmptyAllFormValue('head');
								parentFunc.buttonload.form.setFormStatus('head', 'browse');
								parentFunc.cleanVoucher();
							//	parentFunc.voucherView({ voucherView: true });
							}
					}
				})
			}
		}else if (key == "copy") {//复制
			 // let copyVoucherData=self.state.saveData;
			parentFunc.copyState();
		}else if (key=="verify") {
			let { VrifyModalShow }=state;
			VrifyModalShow=true
			self.setState({
				VrifyModalShow
			})
		}


}
