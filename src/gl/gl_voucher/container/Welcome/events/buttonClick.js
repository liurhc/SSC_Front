import { ajax, toast, cacheTools, promptBox, output, cardCache } from 'nc-lightapp-front';
import {
	saveButton,
	onOftenSelect,
	voucherSelectAction,
	locationModal,
	pageInfoClick
} from './index.js';
let { getDefData ,setDefData} = cardCache;
import {
	formId,
	dataSourceTable,
	tableId,
	oldSource
} from '../../../../public/components/constJSON';
import vouchersaveUpdateCache from './vouchersaveUpdateCache.js';
import {parallel,save,tempSave} from './buttonAction';
import { queryRelatedAppcode } from '../../../../public/common/queryRelatedAppcode.js';
let tableid = 'gl_voucher';
export function isObj(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'Object';
}

export default function buttonClick(voucherState, parentState, props, id, headFunc) {
	let offsetUrl = '/nccloud/gl/voucher/offset.do';

	let realDetails=parentState.realDetail&&parentState.realDetail.length!=0?parentState.realDetail:voucherState.evidenceData.rows;

	switch (headFunc) {
		case 'add':
			id.form.setFormStatus('head', 'add');
			id.form.EmptyAllFormValue('head');
			id.form.setItemsVisible('head', { adjustperiod: false });
			id.controlAutoFocus(true);
			id.form.setFormItemsDisabled('head', { prepareddate: true ,pk_accountingbook: false});
			parentState.voucherView({ voucherView: false });
			parentState.cleanTable();
			id.executeAutoFocus();
			break;
		case 'adjustAdd': //调整期凭证
			id.form.setFormStatus('head', 'add');
			id.form.EmptyAllFormValue('head');
			parentState.voucherView({ voucherView: false });
			id.form.setItemsVisible('head', { adjustperiod: true });
			id.form.setFormItemsDisabled('head', { prepareddate: true });
			parentState.cleanTable('1');
			break;
		case 'save':
			saveVoucher.call(this,voucherState,'save',id,parentState,props);
			break;
		case 'temp':
			tempSave.call(this, voucherState);
			break;
		case 'savenext':
			// let linkData11 = getDefData('gl_voucher', dataSource);
			// let voucherDetail = getDefData('voucher_detail', dataSourceDetail); //生成
			// if (!voucherState.saveData.prepareddate.value) {
			// 	toast({ content: this.state.json['20021005card-000226'], color: 'warning', position: 'bottom' });/* 国际化处理： 制单日期必录！请先录入制单日期*/
			// 	return false;
			// }
			//saveButton.call(this, voucherState, 'saveNext', id);
			saveVoucher.call(this,voucherState,'saveNext',id,parentState,props);
			break;
		case 'refresh': //刷新按钮操作
			let propsData = voucherState.saveData.pk_voucher.value;
			pageInfoClick.call(this, parentState, '', propsData);
			// templeteButton.call(this, voucherState)
			break;
		case 'transfer': //转换
			//accsubjname
			//accsubjcode
			voucherState.saveData.details.forEach((item, i) => {
				if (item.pk_accasoa.display == item.accsubjcode.value) {
					item.pk_accasoa.display = item.accsubjname.value;
				} else {
					item.pk_accasoa.display = item.accsubjcode.value;
				}
			});
			parentState.changepk_accsoa(voucherState.saveData.details);
			break;

		case 'saveadd':
			// if (
			// 	(!voucherState.saveData.attachment.value || voucherState.saveData.attachment.value == '0') &&
			// 	voucherState.attachModal
			// ) {
			// 	//附单数据录入
			// 	this.saveType = 'saveAdd';
			// 	if (props.attachvalue) {
			// 		props.attachvalue = '';
			// 		//props.form.setFormItemsValue(self.formId, { attachment: { value: '' } });
			// 	}
			// 	id.modal.show('attach');
			// 	return false;
			// }
			//saveButton.call(this, voucherState, 'saveAdd', id);
			id.controlAutoFocus(true);
			saveVoucher.call(this,voucherState,'saveAdd',id,parentState,props);
			id.executeAutoFocus();
			break;
		case 'loadtmp': //调用模板
			if (realDetails && realDetails[0]&&realDetails[0].pk_accasoa&&realDetails[0].pk_accasoa.value) {
				promptBox({
					color: 'warning',
					content: this.state.json['20021005card-000228'],/* 国际化处理： 是否保存?如果选择是,请重新调用常用!*/
					beSureBtnClick: saveVoucher.bind(this, voucherState, 'save', id,parentState,props),
					cancelBtnClick: onOftenSelect.bind(this, voucherState, props, 'loadtmp', parentState)
				});
			} else {
				onOftenSelect.call(this, voucherState, props, 'loadtmp', parentState);
			}
			break;
		case 'managetmp': //维护常用
			onOftenSelect.call(this, voucherState, props, 'managetmp', parentState);
			break;
		case 'delete': //删除
			promptBox({
				color: 'warning',
				title: this.state.json['20021005card-000041'],/* 国际化处理： 删除*/
				content: this.state.json['20021005card-000229'],/* 国际化处理： 确定要删除吗?*/
				beSureBtnClick: voucherSelectAction.bind(this, voucherState, props, 'delete', parentState)
			});
			//voucherSelectAction.call(this, voucherState,props,'delete',parentState)
			break;
		case 'copy': //复制
			parentState.copyState();
			//voucherSelectAction.call(this, voucherState, props, 'copy', parentState);
			break;
		case 'abandon': //作废
			voucherSelectAction.call(this, voucherState, props, 'abandon', parentState);
			break;
		case 'unabandon': //取消作废
			voucherSelectAction.call(this, voucherState, props, 'unabandon', parentState);
			break;
		case 'errcorr': //纠错
			voucherSelectAction.call(this, voucherState, props, 'errcorr', parentState);
			break;
		case 'signmistake': //标错
			voucherSelectAction.call(this, voucherState, props, 'signmistake', parentState);
			break;
		case 'verify': //及时核销
			//voucherSelectAction.call(this, voucherState,props,'verify',parentState)
			break;
		case 'cashflow': //现金流量
		if (!voucherState.saveData.pk_accountingbook.value) {
			toast({ content: this.state.json['20021005card-000092'], color: 'warning' });/* 国际化处理： 请先选定核算账簿*/
			return;
		}
			id.modal.show('CashFlowModal');
			break;
		case 'adjustvoucher': //调整凭证
			break;
		case 'location': //定位
			if(voucherState.headFunc.pk_accountingbook.value){
				locationModal.call(this, voucherState, props);
			}else{
				toast({ content: this.state.json['20021005card-000230'], color: 'warning', position: 'bottom' });/* 国际化处理： 请先选择核算账簿!*/
			}
			break;
		case 'keyboard':
			let dataKey = this.state.json['20021005card-000231'];/* 国际化处理： 1.等于（＝）：借贷自动找平2.空格键；金额在借贷方自动切换*/
			toast({ content: dataKey, color: 'warning', position: 'bottom' });
			break;
		case 'tolist':
			id.pushTo('/', {
				pagecode: '20021005list'
				// status: 'jump',
				// pk_operatinglog: '1001Z31000000000FYV1',
				// groupid: null,
				// src: 'fip'
			});
			//  window.location.href = '../../voucher_list/list/index.html?isChecked='+"true";
			break;
		// case 'delete':
		//     let delUrl = '/nccloud/gl/voucher/delete.do'
		//     dealOperate(props, delUrl, id);
		//     break;
		case 'query':
			this.setState({
				showModal: true
			});
			break;
		case 'refesh':
			//刷新
			break;
		// case 'abandon':
		//     //作废
		//     let abnUrl = '/nccloud/gl/voucher/abandon.do'
		//     dealOperate(props, abnUrl, id);
		//     break;
		// case 'unabandon':
		//     //取消作废
		//     let unabnUrl = '/nccloud/gl/voucher/unabandon.do'
		//     dealOperate(props, unabnUrl, id);
		//     break;
		case 'check':
			//审核
			let checkUrl = '/nccloud/gl/voucher/check.do';
			dealOperate(this,voucherState, checkUrl, headFunc, parentState);
			break;
		case 'uncheck':
			//取消审核
			let uncheckUrl = '/nccloud/gl/voucher/uncheck.do';
			dealOperate(this,voucherState, uncheckUrl, headFunc, parentState);
			break;
		case 'tally':
			//记账
			let tallyUrl = '/nccloud/gl/voucher/tally.do';
			dealOperate(this,voucherState, tallyUrl, headFunc, parentState);
			break;
		case 'untally':
			//取消记账
			let untallyUrl = '/nccloud/gl/voucher/untally.do';
			dealOperate(this,voucherState, untallyUrl, headFunc, parentState);
			break;
		case 'sign':
			//签字
			if (voucherState.saveData.paraInfo && voucherState.saveData.paraInfo.signdateType == '3') {
				id.modal.show('signModal');
			} else {
				let signUrl = '/nccloud/gl/voucher/sign.do';
				dealOperate(this,voucherState, signUrl, headFunc, parentState);
			}
			break;
		case 'unsign':
			//取消签字
			let unsignUrl = '/nccloud/gl/voucher/unsign.do';
			dealOperate(this,voucherState, unsignUrl, headFunc, parentState);
			break;
		case 'blueoffset':
			//蓝冲
			dealOperate(this,voucherState, offsetUrl, headFunc, parentState, '2');
			break;
		case 'redoffset':
			//红冲
			dealOperate(this,voucherState, offsetUrl, headFunc, parentState, '1');
			break;
		case 'offset':
			//默认冲销
			dealOperate(this,voucherState, offsetUrl, headFunc, parentState, '0');
			break;
		case 'showsum': //汇总
			let showsumUrl = '/nccloud/gl/voucher/showSumDetails.do';
			dealOperate(this,voucherState, showsumUrl, headFunc, parentState);
			break;
		case 'edit': //修改
			parentState.voucherView({ voucherView: false });
			let olddata=JSON.parse(JSON.stringify(voucherState.saveData));
			olddata.details=realDetails;
			let oldvalue={
				voucher:olddata,
				paraInfo:voucherState.saveData.paraInfo
			}
			setDefData('oldValue', oldSource, JSON.stringify(oldvalue));
			break;
		case 'linkbill': //联查单据
			let appid = id.getUrlParam('appcode') || id.getSearchParam('c');
			let fipappcode=queryRelatedAppcode('10170410');/**会计平台单据生成节点 */
			cacheTools.set(appid + '_LinkSrc', { relationID: voucherState.saveData.pk_voucher.value });
			id.openTo('/fip/fip/generate/list/index.html#/', {
				status: 'browse',
				scene: appid + '_LinkSrc',
				appcode: fipappcode,
				pagecode: '10170410_1017041001'
			});
			break;
		case 'import':
			//导入
			break;
		case 'convert': //及时折算
			let queryConvert = '/nccloud/gl/voucher/fctimerulequery.do';
			let convertData = {
				pk_accountingbook: voucherState.saveData.pk_accountingbook.value,
				pk_voucher: voucherState.saveData.pk_voucher.value
			};
			if(voucherState.saveData.tempsaveflag&&voucherState.saveData.tempsaveflag.value){
				toast({content:this.state.json['20021005card-000313']/*"暂存凭证不能折算。"*/,color:'warning'});
				return;
			}
			if (
				voucherState.saveData.convertflag &&
				JSON.stringify(voucherState.saveData.convertflag) != '{}' &&
				voucherState.saveData.convertflag.value
			) {
				promptBox({
					color: 'warning',
					content: this.state.json['20021005card-000232'],/* 国际化处理： 该凭证已经进行了折算，是否需要重新折算？*/
					beSureBtnClick: () => {
						ajax({
							url: queryConvert,
							data: convertData,
							//async:true,
							success: (res) => {
								let checkedArray = [];
								res.data.map((e, i) => {
									checkedArray.push(e.defaultrule);
								});
								this.setState({
									convertData: res.data,
									checkedArray: checkedArray
								});
							}
						});
						id.modal.show('exportFileModal');
					}
				});
			} else {
				ajax({
					url: queryConvert,
					data: convertData,
					//async:true,
					success: (res) => {
						let checkedArray = [];
						res.data.map((e, i) => {
							checkedArray.push(e.defaultrule);
						});
						this.setState({
							convertData: res.data,
							checkedArray: checkedArray
						});
					}
				});
				id.modal.show('exportFileModal');
			}

			break;
		case 'parallel'://平行记账
			parallel(this,voucherState,parentState);
			break;
		case 'linkofferdes': //联查被冲销
			if (voucherState.saveData.offervoucher && voucherState.saveData.offervoucher.value) {
				id.openTo('/gl/gl_voucher/pages/main/index.html#/Welcome', {
					pagecode: '20021005card',
					status: 'browse',
					id: voucherState.saveData.offervoucher.value
				});
			}
			break;
		case 'linksrc': //联查来源
			if (voucherState.saveData.pk_sourcepk && voucherState.saveData.pk_sourcepk.value) {
				id.openTo('/gl/gl_voucher/pages/main/index.html#/Welcome', {
					pagecode: '20021005card',
					status: 'browse',
					id: voucherState.saveData.pk_sourcepk.value
				});
			}
			break;
		case 'linkdes': //联查目的
			let linkdes = '/nccloud/gl/voucher/linkdes.do';
			ajax({
				url: linkdes,
				data: voucherState.saveData,
				//async:true,
				success: (res) => {
					if (res.data) {
						res.data.forEach((e, i) => {
							this.props.buttonload.openTo('/gl/gl_voucher/pages/main/index.html#/Welcome', {
								pagecode: '20021005card',
								status: 'browse',
								id: e
							});
						});
					}
				}
			});
			break;
		case 'linkoffersrc': //联查冲销
			let linkoffersrc = '/nccloud/gl/voucher/linkoffersrc.do';
			let linkData = {
				pk_voucher: voucherState.saveData.pk_voucher.value
			};
			ajax({
				url: linkoffersrc,
				data: linkData,
				//async:true,
				success: (res) => {
					if (res.data) {
						res.data.forEach((e, i) => {
							this.props.buttonload.openTo('/gl/gl_voucher/pages/main/index.html#/Welcome', {
								pagecode: '20021005card',
								status: 'browse',
								id: e
							});
						});
					}
				}
			});
			break;
		case 'export':
			//导出
			break;
		case 'cancel':
			//取消
			promptBox({
				color: 'warning',
				title: this.state.json['20021005card-000004'],/* 国际化处理： 取消*/
				content: this.state.json['20021005card-000233'],/* 国际化处理： 确定要取消吗?*/
				beSureBtnClick: () => {
					if (voucherState.voucherStatus == 'update') {
						if (getDefData('oldValue', oldSource)) {
							let backData = JSON.parse(getDefData('oldValue', oldSource));
							parentState.backOld(backData);
						}
					} else {
						//取消清空savedata值
						if (getDefData('oldValue', oldSource)) {
							let backData = JSON.parse(getDefData('oldValue', oldSource));
							parentState.backOld(backData);
						}else{
							parentState.buttonload.form.EmptyAllFormValue('head');
							parentState.buttonload.form.setFormStatus('head', 'browse');
							parentState.cleanVoucher();
						}
						//parentState.cleanButton({ voucherView: true });
					}
				}
			});

			break;
		case 'print':
		//点打印禁止弹出即时核销
			this.isVerify=false;
			id.modal.show('print');
			//打印
			break;
		case 'output':
			//输出
			let outputdata = {
				printsubjlevel: '0', //printsubjlevel：科目汇总到第几级，不汇总是0
				printasslevel: '0', //printasslevel：按辅助项汇总 勾选-1 不勾选0
				printmode: '0', //printmode：代账单格式 0不勾选 1勾选
				selectIndex: '-1' //selectIndex: 选中第几行分录 从0开始
			};
			let outputArr = [];
			outputArr.push(voucherState.saveData.pk_voucher.value);
			output({
				url: '/nccloud/gl/voucher/outputvoucher.do',
				data: {
					appcode: '20020PREPA', //小应用编码
					oids: outputArr,
					userjson: JSON.stringify(outputdata),
					outputType: 'output'
				}
			});
			break;
		case 'attachment':
			if (
				(voucherState.saveData.pk_checked &&
					voucherState.saveData.pk_checked.value &&
					voucherState.saveData.pk_checked.value != '~') ||
				(voucherState.saveData.pk_casher &&
					voucherState.saveData.pk_casher.value &&
					voucherState.saveData.pk_casher.value != '~') ||
				(voucherState.saveData.pk_manager &&
					voucherState.saveData.pk_manager.value &&
					voucherState.saveData.pk_manager.value != 'N/A') ||
				id.getUrlParam('type') == 'check' ||
				id.getUrlParam('type') == 'tally' ||
				id.getUrlParam('type') == 'sign' ||((id.getUrlParam('pagecode')&&id.getUrlParam('pagecode').indexOf('link')!=-1)||
				id.getUrlParam('pagekey')=='link'||id.getUrlParam('pagekey')=='pre')
			) {
				this.setState({
					showViewLoader: true
				});
			} else {
				this.setState({
					showUploader: true
				});
			}
			break;
		case 'viewscan': //影响扫描
			// let image = require('sscrp/rppub/components/image');
			const { imageScan } = this.image;
			// data.body.bodys = dealImagebodys(data.body.bodys);
			// imageScan(data, billno, 'C0', 'iweb');
			let scanInfo = getImageInfo(voucherState.saveData);
			imageScan(scanInfo, 'iweb');
			break;
		case 'viewlook': //影像查看
			if (voucherState.saveData.pk_voucher&&voucherState.saveData.pk_voucher.value && voucherState.saveData.pk_voucher.value != '') {
				// let image_imageView = require('sscrp/rppub/components/image');
				const { imageView } = this.image;
				// data.body.bodys = dealImagebodys(data.body.bodys);
				// imageView(data, billno, 'C0', '');
				let viewInfo = getImageInfo(voucherState.saveData);
				imageView(viewInfo, 'iweb');
			}
			break;
	}
}
function getImageInfo(saveData) {
	let billInfo = {};
	billInfo.pk_billid = saveData.pk_voucher.value;
	billInfo.pk_billtype = 'C0';
	billInfo.pk_tradetype = 'C0';
	billInfo.pk_org = saveData.pk_org.value;
	billInfo.BillType = 'C0';
	billInfo.BillDate = saveData.prepareddate.value;
	billInfo.Busi_Serial_No = saveData.pk_voucher.value;
	billInfo.OrgNo = saveData.pk_org.value;
	billInfo.BillCode = saveData.num.value;
	billInfo.OrgName = saveData.pk_org.value;
	billInfo.Cash = saveData.totaldebit.value;
	billInfo.src = 'fip';
	return billInfo;
}

export function dealOperate(_this,props, opurl, id, callBack, type) {
	if (!props.saveData.pk_voucher.value) {
		return false;
	}
	let data = {};
	if (id == 'blueoffset' || id == 'redoffset' || id == 'offset') {
		data = {
			pk_voucher: props.saveData.pk_voucher.value,
			offsettype: type
		};
	} else {
		data = {
			pk_voucher: props.saveData.pk_voucher.value
		};
	}

	ajax({
		url: opurl,
		data: data,
		async: true,
		success: (res) => {
			let { success, data } = res;
			if (data.error && JSON.stringify(data.error) != '{}') {
				let errmessage = '';
				for (let key in data.error) {
					errmessage = data.error[key];
					break;
				}
				toast({ content: errmessage, color: 'warning', position: 'bottom' });
			} else if (success) {
				if(data.warn){
					let errmessage = '';
				for (let key in data.warn) {
					errmessage = data.warn[key];
					break;
				}
					toast({ content: errmessage, color: 'warning', position: 'bottom' });
				}
				if (id == 'delete') {
					props.table.deleteTableRowsByIndex(tableid, indexArr);
					toast({ content: _this.state.json['20021005card-000081'], color: 'success', position: 'bottom' });/* 国际化处理： 删除成功*/
				} else {
					dealSuccess(_this,props, data, id, callBack);
				}
			}
		},
		error: (res) => {
			toast({ content: res.message, color: 'warning' });
		}
	});
}

export function dealSuccess(_this,props, data, id, callBack) {
	//	let oldValueData = JSON.stringify(data);//数据存储，取消返回冲销前数据
	switch (id) {
		case 'delete':
			succdatas.forEach((val) => {
				props.table.deleteTableRowsByIndex(tableId, indexObj[val.pk_voucher]);
			});
			toast({ content: _this.state.json['20021005card-000081'], color: 'success', position: 'bottom' });/* 国际化处理： 删除成功*/
			break;
		case 'abandon':
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "discardflag", val.discardflag, indexObj[val.pk_voucher]);
			// });
			//更新缓存数据，使得列表数据是最新的
			vouchersaveUpdateCache(
				'update',
				'pk_voucher',
				data.voucher.pk_voucher.value,
				data,
				formId,
				dataSourceTable
			);
			callBack.saveUpdate(data);
			toast({ content: _this.state.json['20021005card-000082'], color: 'success', position: 'bottom' });/* 国际化处理： 作废成功*/
			break;
		case 'unabandon':
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "discardflag", val.discardflag, indexObj[val.pk_voucher]);
			// });
			//更新缓存数据，使得列表数据是最新的
			vouchersaveUpdateCache(
				'update',
				'pk_voucher',
				data.voucher.pk_voucher.value,
				data,
				formId,
				dataSourceTable
			);
			callBack.saveUpdate(data);
			toast({ content: _this.state.json['20021005card-000083'], color: 'success', position: 'bottom' });/* 国际化处理： 取消作废成功*/
			break;
		case 'check':
			//审核
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_checked", val.pk_checked, indexObj[val.pk_voucher]);
			// });
			//	succdatas.voucherView = true;
			//更新缓存数据，使得列表数据是最新的
			vouchersaveUpdateCache(
				'update',
				'pk_voucher',
				data.voucher.pk_voucher.value,
				data,
				formId,
				dataSourceTable
			);
			callBack.saveUpdate(data, true);
			toast({ content: _this.state.json['20021005card-000084'], color: 'success', position: 'bottom' });/* 国际化处理： 审核成功*/
			break;
		case 'uncheck':
			//取消审核
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_checked", "", indexObj[val.pk_voucher]);
			// });
			//succdatas.voucherView = true;
			//更新缓存数据，使得列表数据是最新的
			vouchersaveUpdateCache(
				'update',
				'pk_voucher',
				data.voucher.pk_voucher.value,
				data,
				formId,
				dataSourceTable
			);
			callBack.saveUpdate(data, true);
			toast({ content: _this.state.json['20021005card-000085'], color: 'success', position: 'bottom' });/* 国际化处理： 取消审核成功*/
			break;
		case 'tally':
			//记账
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_manager", val.pk_manager, indexObj[val.pk_voucher]);
			// });
			//更新缓存数据，使得列表数据是最新的
			vouchersaveUpdateCache(
				'update',
				'pk_voucher',
				data.voucher.pk_voucher.value,
				data,
				formId,
				dataSourceTable
			);
			callBack.saveUpdate(data, true);
			toast({ content: _this.state.json['20021005card-000086'], color: 'success', position: 'bottom' });/* 国际化处理： 记账成功*/
			break;
		case 'untally':
			//取消记账
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_manager", "", indexObj[val.pk_voucher]);
			// });
			//更新缓存数据，使得列表数据是最新的
			vouchersaveUpdateCache(
				'update',
				'pk_voucher',
				data.voucher.pk_voucher.value,
				data,
				formId,
				dataSourceTable
			);
			callBack.saveUpdate(data, true);
			toast({ content: _this.state.json['20021005card-000087'], color: 'success', position: 'bottom' });/* 国际化处理： 取消记账成功*/
			break;
		case 'sign':
			//签字
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "signflag", val.signflag, indexObj[val.pk_voucher]);
			// });
			//更新缓存数据，使得列表数据是最新的
			vouchersaveUpdateCache(
				'update',
				'pk_voucher',
				data.voucher.pk_voucher.value,
				data,
				formId,
				dataSourceTable
			);
			callBack.saveUpdate(data, true);
			toast({ content: _this.state.json['20021005card-000088'], color: 'success', position: 'bottom' });/* 国际化处理： 签字成功*/
			break;
		case 'unsign':
			//取消签字
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "signflag", val.signflag, indexObj[val.pk_voucher]);
			// });
			//更新缓存数据，使得列表数据是最新的
			vouchersaveUpdateCache(
				'update',
				'pk_voucher',
				data.voucher.pk_voucher.value,
				data,
				formId,
				dataSourceTable
			);
			callBack.saveUpdate(data, true);
			toast({ content: _this.state.json['20021005card-000089'], color: 'success', position: 'bottom' });/* 国际化处理： 取消签字成功*/
			break;
		case 'blueoffset':
			//冲销
			//	succdatas.voucherView = false;
			//setDefData('oldValue', oldSource, oldValueData);
			callBack.updateState(data, false, true);
			//toast({ content: "冲销成功", color: 'success', position: 'bottom' });
			break;
		case 'redoffset':
			//冲销
			//	succdatas.voucherView = false;
			//setDefData('oldValue', oldSource, oldValueData);
			callBack.updateState(data, false, true);
			// toast({ content: "冲销成功", color: 'success', position: 'bottom' });
			break;
		case 'offset':
			//冲销
			//	succdatas.voucherView = false;
			//setDefData('oldValue', oldSource, oldValueData);
			callBack.updateState(data, false, true);
			//toast({ content: "冲销成功", color: 'success', position: 'bottom' });
			break;
		case 'showsum':
			//	succdatas.voucherView = true;
			callBack.updateState(data, true);
		//  toast({ content: "汇总成功", color: 'success', position: 'bottom' });
	}
}

export function saveVoucher(voucherState, status, id, parentState,props) {
	if ((!voucherState.saveData.attachment.value || voucherState.saveData.attachment.value == '0')
		&& voucherState.attachModal) {
		//附单数据录入
		this.saveType = status;
		if (props.attachvalue) {
			props.attachvalue = '';
		}
		id.modal.show('attach');
		return false;
	}
	save.call(this, voucherState, status, id, parentState);
}