import {ajax,deepClone} from 'nc-lightapp-front';
import { toast } from 'nc-lightapp-front';
//const deepClone = require('../../../../public/components/deepClone');

export default function imageSelect(props,{key}){

	//影像扫描1，影像查看2
		let bodysData=[];
		let headData={};
		for(let k in props.saveData){
			if(k!='details'){
				headData[k]=props.saveData[k].value?props.saveData[k].value:'';
			}
		}
		props.saveData.details.map((item,i)=>{
			for(let k in item){
				item[k]=item[k].value?item[k].value:'';
			}
			bodysData[i]=item;
		})
		let data={
			"body":{bodys:bodysData},
			"head":headData
		}
		let billno=props.saveData.pk_voucher.value;//'VR0120180326A0000034';
		let tradetype='D0';
		if(`${key}`=="1"){
			imageuploadBrowserSend(data,billno,tradetype, 'iweb');
		}else if(`${key}`=="2"){
			imageshowBrowserSend(data,billno,tradetype, '');
		}


}
