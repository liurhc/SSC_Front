import {ajax,deepClone} from 'nc-lightapp-front';
import { toast } from 'nc-lightapp-front';
//const deepClone = require('../../../../public/components/deepClone');
export default function locationModal(props,status,extend) {
        let self = this;
		let { pk_accountingbook, nov, startdate, attachment, evidenceData, saveData, update,saveNumber,lastMessage } = props;
		let {locationModalShow,...others}=status
			//self.refs.ManageOftenModal.queryClass(props.saveData.pk_accountingbook.value, saveDataClone);
			// let SubjectModalShow = true;
			if(saveData.pk_accountingbook&&saveData.pk_accountingbook.value){
				ajax({
					url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
					data: {
						pk_accountingbook: saveData.pk_accountingbook.value
					},
					success: (res) => {
						let { success, data } = res;
						self.setState({
							pk_accperiodscheme: data.pk_accperiodscheme,
							locationModalShow: true,
						});
					}
				})
			}
}
