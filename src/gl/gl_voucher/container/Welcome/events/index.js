import onOftenSelect from './onOftenSelect';
import voucherSelectAction from './voucherSelectAction';
import imageSelect from './imageSelect';
import onButtonClick from "./buttonClick";
import pageInfoClick from "./pageInfoClick";
import handleButtonClick from "./handleButtonClick";
import locationModal from "./locationModal";
import afterEvent from "./afterEvent";
import BeforeEvent from "./beforeEvent";
import initTemplate from "./initTemplate";
import {onEnterAction,onLastFormEnter} from "./hotkey/onEnterAction";
import {formItemTabOnEnter} from "./hotkey/formItemTabOnEnter";
import onEqual from './hotkey/onEqual';
export {initTemplate,afterEvent,BeforeEvent,onOftenSelect,voucherSelectAction,imageSelect,onButtonClick,locationModal,handleButtonClick,pageInfoClick,
    onEnterAction,onLastFormEnter,onEqual,formItemTabOnEnter}
