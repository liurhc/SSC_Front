import { ajax, promptBox, toast ,cardCache} from 'nc-lightapp-front';
let { getDefData } = cardCache;
import {oldSource} from '../../../../public/components/constJSON';
export default function afterEvent(props, moduleId, key, v, oldValue, i, s, g) {
	let self = this;
	let { saveData, headFunc, evidenceData,voucherStatus} = self.state;
	//let nameValue = saveData[key];
	let nameValue={
		display:v.display,
		value:v.value
	};
	saveData[key]=nameValue;
	this.setState({
		saveData
	});
	if (key === 'pk_accountingbook') {

		let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
		let { prepareddate } = self.state;
		let { pk_accountingbook, checkNot } = headFunc;
		let pk_accpont = {
			pk_accountingbook: v.value,
			isVoucher:true
		};
		if (oldValue.value) {
			promptBox({
				title: self.state.json['20021005card-000224'],/* 国际化处理： 确认修改*/
				content: self.state.json['20021005card-000225'],/* 国际化处理： 确定修改核算账簿,这样会清空您录入的信息?*/
				beSureBtnClick: () => {
					this.props.form.setFormItemsValue(this.formId, {//切换账簿清空调整期，附属单据
						adjustperiod:{display:'',value:'' },
						attachment: { value: '' }, 
						pk_accountingbook:{display:v.display,value:v.value}
					});
					let copyData=this.copyData
					if(copyData&&copyData.length!='0'){
						this.copyData=[];
					}
					this.ifCheck(pk_accpont, url, true);
					
				},
				cancelBtnClick:()=>{
					this.props.form.setFormItemsValue(this.formId, {
						pk_accountingbook: { value: oldValue.value, display: oldValue.display }
					});
					let { saveData, headFunc, evidenceData } = this.state;
					let nameValue = saveData.pk_accountingbook;
					headFunc.pk_accountingbook.value=oldValue.value
					nameValue.display = oldValue.display;
					nameValue.value = oldValue.value;
					this.setState({
						saveData,headFunc
					});

				}
				
			});
		} else {
			pk_accountingbook.value = v.value;
			if (pk_accountingbook.value) {
				self.ifCheck(pk_accpont, url);
			}
		}
		self.setState({
			headFunc
		});
		//编辑后时间凭证类别添加过滤
		let meta = props.meta.getMeta();
		meta[self.formId].items.map((item) => {
			//凭证类别参照添加过滤
			if (item.attrcode == 'pk_vouchertype') {
				item.queryCondition = () => {
					return {
						GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
						pk_org: v.value,
						isDataPowerEnable: 'Y',
						DataPowerOperationCode: 'fi'
					};
				};
			}
			if (item.attrcode == 'adjustperiod') {
				item.queryCondition = () => {
					return {
						pk_accountingbook: v.value
					};
				};
			}
		});
		props.meta.setMeta(meta);
	}

	if (key === 'pk_vouchertype') {
		// 三个字段变化动态获取凭证号
		if (
			v.value &&
			v.value.trim() &&
			self.state.saveData.pk_accountingbook.value &&
			self.state.saveData.pk_accountingbook.value.trim() &&
			self.state.saveData.prepareddate.value &&
			self.state.saveData.prepareddate.value.trim()
		) {
			//self.getNov(saveData.pk_accountingbook, saveData.prepareddate, {display: v.display, value: v.value})
			self.getCurry(saveData.pk_accountingbook.value, saveData.prepareddate.value, v, 'pk_vouchertype');
		}
		//根据凭证类别获取币种
	}

	// 	3. 调整期选完后 发送请求 voucherValueChange.do
	// {
	// 	key:'adjustperiod',
	// 	value:'2018-06A',
	// 	pk_vouchertype:'...'//凭证类别
	// 	year:'2018',//b.PERIODYEAR
	// 	period:'06',//endmonth
	// }
	// 返回
	// {
	// 	prepareddate:'2018-06-30',
	// 	num:15
	// }
	// 设置到制单日期、凭证号字段上
	// 4. 选择制单日期后发送请求 voucherValueChange.do
	// {
	// 	key:'prepareddate',
	// 	value:'2018-06-30',
	// 	pk_vouchertype:'...'//凭证类别
	// }
	// 返回{num:16} //设置凭证号

	if (key === 'adjustperiod') {
		let { prepareddate,evidenceData} = self.state;
		if (v.values) {
			let url = '/nccloud/gl/voucher/voucherValueChange.do';
			let data = {
				key: 'adjustperiod',
				pk_accountingbook: saveData.pk_accountingbook.value || '',
				value: v.refcode,
				pk_vouchertype: saveData.pk_vouchertype.value || '', //凭证类别
				year: v.values && v.values['b.PERIODYEAR'].value, //b.PERIODYEAR
				period: v.values && v.values.endmonth.value //endmonth
			};
			ajax({
				url,
				data,
				success: (res) => {
					if (res.success) {
						let { num } = res.data;
						// if (res.data.prepareddate&&res.data.prepareddate.indexOf(' ') != -1) {
						// 	res.data.prepareddate = res.data.prepareddate.split(" ")[0]
						// }
						//self.props.form.setFormItemsValue(self.formId, { period: { value:'',display:'' } });
						self.props.form.setFormItemsValue(self.formId, { prepareddate: { value: res.data.prepareddate, display: res.data.prepareddate }, period: { value: '', display: '' }, num: { value: num } });
						//self.props.form.setFormItemsValue(self.formId, { num: { value: num } });
						saveData.prepareddate.value = res.data.prepareddate;
						saveData.num.value = num;
						saveData.period.value = '';
						saveData.adjustperiod.value=v.values && v.values['ACCPERIODMTH'].value;
						prepareddate.value = res.data.prepareddate;
						evidenceData.rows.forEach(function(item, i) {//切换日期跟新分录业务日期 核销日期
							if(item.busidate){
								item.busidate.value=res.data.prepareddate;
								item.busidate.display=res.data.prepareddate;
							}
							if(item.verifydate){
								item.verifydate.value=res.data.prepareddate;
								item.verifydate.display=res.data.prepareddate;
							}
						});
						
						self.setState({
							evidenceData
						})
						
					} else {
						toast({ content: res.error, color: 'warning', position: 'bottom' });
					}
				}
			});
		}else{
			self.props.form.setFormItemsValue(self.formId, { prepareddate: { value:'', display: ''}, period: { value: '', display: '' }, num: { value: ''} });
			saveData.prepareddate.value ='';
			saveData.num.value = '';
			saveData.period.value = '';
			prepareddate.value = ''
		}
		self.setState({
			prepareddate,
			saveData
		});
		
	}

	if (key === 'prepareddate') {
		let oldPreValue = oldValue && oldValue.value?oldValue.value:null;
		if (v.value) {
			let { prepareddate } = self.state;
			if (v.value && v.value.indexOf(' ') != -1) {
				v.value = v.value.split(" ")[0]
			}
			//清空期间
			self.props.form.setFormItemsValue(self.formId, { period: { value: '', display: '' }, adjustperiod: { value: '', display: '' } });
			saveData.period.value = '';
			let url = '/nccloud/gl/voucher/voucherValueChange.do';
			let data = {
				key: 'prepareddate',
				pk_accountingbook: saveData.pk_accountingbook.value || '',
				value: v.value,
				pk_vouchertype: saveData.pk_vouchertype.value || '',//凭证类别
				oldvalue: oldValue.value
			};
			prepareddate.value = v.value;
			if(!oldPreValue||oldPreValue==''){
				if(getDefData('oldValue', oldSource)){
					let oldDataValue= JSON.parse(getDefData('oldValue', oldSource));
					if(oldDataValue&&oldDataValue.voucher&&oldDataValue.voucher.prepareddate&&oldDataValue.voucher.prepareddate.value&&oldDataValue.voucher.prepareddate.value!=''){
						data.oldvalue=oldDataValue.voucher.prepareddate.value;
					}
				}
			}
			
			ajax({
				url,
				data,
				success: (res) => {
					if (res.success) {
						let { num, isVersionChange } = res.data;
						if(num){
							saveData.num.value = num;
							self.props.form.setFormItemsValue(self.formId, { num: { value: num } });
						}
						if (isVersionChange) {
							evidenceData.rows.forEach(function (item, i) {//切换日期清空辅助核算
								if (item.assid && item.assid.value) {
									item.assid.value = '';
									item.assid.display = '';
									item.ass = []
								}
							});
						}
						evidenceData.rows.forEach(function (item, i) {//切换日期跟新分录业务日期
							let busidate = item.busidate && item.busidate.value && item.busidate.value != '' ?
								item.busidate.value.split(" ")[0] : '';
							let oldPre = oldPreValue && oldPreValue != '' ?
								oldPreValue.split(" ")[0] : '';
							if (busidate == oldPre) {
								item.busidate={value: v.value, display:v.vlaue}
							}
						});
						self.setState({
							saveData,
							prepareddate,
							evidenceData
						});
					} else {
						toast({ content: res.error, color: 'warning', position: 'bottom' });
					}
				}
			});
		}else{
			evidenceData.rows.forEach(function (item, i) {//切换日期跟新分录业务日期
				let busidate=item.busidate && item.busidate.value&&item.busidate.value!=''?
						item.busidate.value.split(" ")[0]:'';
				let oldPre=oldPreValue&&oldPreValue!=''?
						oldPreValue.split(" ")[0]:'';
				if (busidate == oldPre) {
					item.busidate.value = v.value;
					item.busidate.display = v.value;
				}
			});
			self.setState({
				saveData,
				evidenceData
			});
		}
		
	}

	if (key === 'pk_org_v') {
		// let val = { display: value.refname, value: value.refpk };
		// props.form.setFormItemsValue(moduleId, { pk_org: val });
		let data = {
			nbcrcode: 'HO',
			pk_group: '0001A1100000000005T5',
			pk_org: value.value
		};
		// ajax({
		// 	url: '/nccloud/reva/revebill/contractfront.do',
		// 	data: data,
		// 	success: (res) => {
		// 		if(res.success) {
		// 			let pk_org_v_value = res.data[1];
		// 			props.form.setFormItemsValue(moduleId, { vbillcode: pk_org_v_value });
		// 		}
		// 	}
		// });
	}
	if (key === 'pk_org') {
		// let val = { display: value.refname, value: value.refpk };
		// props.form.setFormItemsValue(moduleId, { pk_org: val });

		//主组织处理
		//props.resMetaAfterPkorgEdit();
	}
	if (key === 'dbilldate') {
		//表头编辑后事件
		// let data = props.createHeadAfterEventData('20521030', this.formId, this.tableId, moduleId, key, value);
		// ajax({
		// 	url: '/nccloud/reva/revebill/afteredit.do',
		// 	data: data,
		// 	success: (res) => {
		// 		if (res.data && res.data.head && res.data.head.head) {
		// 			let dealmny = res.data.head.head.rows[0].values.ndealtotalmny;
		// 			props.form.setFormItemsValue(moduleId, { ndealtotalmny: dealmny });
		// 		}
		// 	}
		// });
	}

	if (key === 'cmaterialvid') {
		let materialsNew = value;
		if (materialsNew && materialsNew.length > 1) {
			props.cardTable.setValByKeyAndIndex(moduleId, i, key, {
				value: materialsNew[0].refpk,
				display: materialsNew[0].refname
			});
			for (let i = 1; i < materialsNew.length; i++) {
				props.cardTable.addRow(moduleId);
				let ll = props.cardTable.getNumberOfRows(moduleId);
				props.cardTable.setValByKeyAndIndex(moduleId, ll - 1, key, {
					value: materialsNew[i].refpk,
					display: materialsNew[i].refname
				});
			}
		}
	}

	//表体编辑后事件
	if (key == 'fconfirmpoint') {
		let data = props.createBodyAfterEventData('20521030', this.formId, this.tableId, moduleId, key, oldValue);
		// ajax({
		// 	url: '/nccloud/reva/revebill/cardafteredit.do',
		// 	data: data,
		// 	success: (res) => {
		// 		if (res.data && res.data.body && res.data.body[this.tableId]) {
		// 			let npobnum = res.data.body[this.tableId].rows[0].values.npobnum;
		// 			props.cardTable.setValByKeyAndRowNumber(moduleId, i+1,'npobnum',npobnum.value);
		// 		}
		// 	}
		// });
	}

	if(key=='num'){
		if(oldValue.value!=v.value)
			self.setState({
				isNumChange:true
			});
	}
}
