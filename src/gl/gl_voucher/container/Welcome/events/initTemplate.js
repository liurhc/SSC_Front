import { cardCache} from 'nc-lightapp-front';
let { getDefData} = cardCache;
import {query_accountingbook} from '../../../../public/components/constJSON';
import toVoucherCard from './toVoucherCard';
import { printItem } from '../../../voucher_list/list/print';

// let pageId = '20021005card';
export default function (props) {
	let appcode = props.getUrlParam('appcode')||props.getUrlParam('c')||props.getSearchParam('c');
	let pageId = props.getUrlParam('pagecode')||props.getUrlParam('p')||props.getSearchParam('p')||'20021005card';
	let self = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appid: appcode//注册按钮的id
		},
		function (data) {
			if (data) {
				self.echoData(data);//渲染表格
				if (data.template) {
					let meta = data.template;
					modifierMeta(self.props, meta, data.context.defaultAccbookPk, self);
					self.props.meta.setMeta(meta);

					let pk_defaultAcc=setDefaultValue(self,data.context);
					toVoucherCard.call(self,pk_defaultAcc);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function modifierMeta(props, meta, pk_acc, self) {
	let formId = 'head';
	let status = props.getUrlParam('status');
	let appcode = props.getUrlParam('c') || props.getSearchParam('c');
	let adjustAddPage = props.getUrlParam('adjust');
	let genVoucher = props.getUrlParam('pagekey');
	let genVoucherTwo = props.getUrlParam('pagecode');
	meta[formId].status = status;

	meta['print'] = printItem(appcode); //打印
	props.form.setFormItemsValue('print', { 'printsubjlevel': { display: self.state.json['20021005card-000280'], value: '0' } });/* 国际化处理： 不汇总*/
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_accountingbook') {
			if (genVoucher && genVoucher == 'generate' || genVoucherTwo && genVoucherTwo.indexOf('_gen') != -1) {
				item.disabled = true;
			}
			item.queryCondition = () => {
				return {
					TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
					appcode: appcode
				};
			};

		}
		if (item.attrcode == 'adjustperiod' && adjustAddPage) {
			item.visible = true;
		}

		if (item.attrcode == 'adjustperiod') {
			item.queryCondition = () => {
				return {
					pk_accountingbook: pk_acc
				};
			};
		}
		if (item.attrcode == 'pk_vouchertype') {
			item.queryCondition = () => {
				return {
					GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
					isDataPowerEnable: 'Y',
					DataPowerOperationCode: 'fi',
					pk_org: pk_acc
				};
			};
		}
	});
	return meta;
}

function setDefaultValue(self,context) {
	//设置默认核算账簿
	let pk_defaultAcc='';
	let pk_acc = getDefData('normalQuery', query_accountingbook);
	if (pk_acc) {
		//若列表查询根据列表核算账簿查询
		self.props.form.setFormItemsValue(self.formId, {
			pk_accountingbook: {
				value: pk_acc.value,
				display: pk_acc.display
			}
		});
		self.defaultAccount.display = pk_acc.display;
		self.defaultAccount.value = pk_acc.value;
		pk_defaultAcc=pk_acc.value;
	} else {
		self.props.form.setFormItemsValue(self.formId, {
			pk_accountingbook: {
				value: context.defaultAccbookPk,
				display: context.defaultAccbookName
			}
		});
		self.defaultAccount.display = context.defaultAccbookName;
		self.defaultAccount.value = context.defaultAccbookPk;
		pk_defaultAcc=context.defaultAccbookPk;
	}
	return pk_defaultAcc;
}

