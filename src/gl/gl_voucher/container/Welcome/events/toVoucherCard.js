/**
 * 外系统生成、联查、预览凭证到卡片
 */
import { ajax,cardCache,cacheTools} from 'nc-lightapp-front';
let { setDefData, getDefData} = cardCache;
import { buttonStatusDetails} from "../voucherOftenApi";
import {dataSourceCoord,dataSourceDetail,oldSource,cardCacheKey} from '../../../../public/components/constJSON';

export default function toVoucherCard(pk_defaultAcc) {
    let self = this;
    //小友机器人
    let boturl = '/nccloud/gl/voucher/botCreateVoucher.do';
    let param = self.props.getUrlParam('param') || self.props.getSearchParam('param');
    if (param && param != '{}') {
        param = JSON.parse(param);
        let allAttributes = {
            "dateTime": param.dateTime,
            "businessName": param.businessName,
            "money": param.money,
            "currency": param.currency,
            "companyName": param.companyName,// 页面输入的公司名称
            "companychoose": param.companychoose,// 选择的公司信息
        }
        ajax({
            url: boturl,
            data: allAttributes,
            success: function (res) {
                let { success, data } = res;
                if (success) {
                    self.loadVoucher(data.voucher, data.paraInfo,'','','robot')
                }
            }
        })
    }

    let { headFunc, saveData } = self.state;
    //单页路由跳转赋值操作 联查凭证和生成凭证
    let crossData = getDefData('voucher_coorder', dataSourceCoord); //联查
    let voucherDetail = getDefData(cardCacheKey, dataSourceDetail); //生成
    let propsData = self.props.getUrlParam('id');
    let propsType = self.props.getUrlParam('status');
    let adjustAddPage = self.props.getUrlParam('adjust');
    if (adjustAddPage) {//列表调卡片调整期标识
        self.voucherkind = '1';
        self.props.form.setFormItemsDisabled('head', { prepareddate: true });
    }
    buttonStatusDetails(self.props, false, saveData)//表体按钮控制有待优化
    if (
        (crossData && JSON.stringify(crossData) != '{}') ||
        (voucherDetail && JSON.stringify(voucherDetail) != '{}') && !propsData
    ) {
        if (crossData) {
            let { voucher, paraInfo } = crossData;
            self.loadVoucher(voucher, paraInfo, propsType == 'browse' ? true : false);
        } else if (voucherDetail && voucherDetail.card) {
            let { voucher, paraInfo } = voucherDetail.card;
            self.loadVoucher(voucher, paraInfo, propsType == 'browse' ? true : false);
        } else {
            let { voucher, paraInfo } = voucherDetail;
            self.loadVoucher(voucher, paraInfo, propsType == 'browse' ? true : false);
        }
    } else {
        if (propsData && (propsType == 'browse' || propsType == 'edit')) {
            let url = '/nccloud/gl/voucher/query.do';
            let data = {
                pk_voucher: propsData
            };

            self.queryList(data, url);
        } else {
            if (pk_defaultAcc) {
                let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
                let pk_accpont = {
                    pk_accountingbook: pk_defaultAcc,
                    isVoucher: true
                };
                self.ifCheck(pk_accpont, url);
            } else {
                if (adjustAddPage) {//列表跳卡片，调整期添加
                    saveData.voucherkind.value = '1';
                }
                self.buttonStatus();
                if(!param){//小友生成凭证到卡片form区核算账簿不聚焦
                    self.props.controlAutoFocus(true);
                }
                self.voucherPageButton();//页面初始化没账簿后凭证页面表头状态控制
                if(!param){
                    self.props.executeAutoFocus();
                }
            }
        }
    }
    //新增凭证赋默认值操作
    //self.loadValue();
    let src = this.props.getUrlParam('scene');
    if (src&&src.indexOf('_Preview2019')==-1) {
        let checkedData = cacheTools.get(src);
        if (checkedData) {
            ajax({
                url: '/nccloud/fip/generate/generateBillForGL.do',
                data: checkedData,
                //  async: false,
                success: (response) => {
                    if (response.data) {
                        let { voucher, paraInfo } = response.data;
                        if (voucher && paraInfo) {
                            //存储数据，取消拿到回调值
                            let oldValueData = JSON.stringify(response.data);
                            setDefData('oldValue', oldSource, oldValueData);
                            //self.ViewModel.setData('oldValue', voucher);
                            self.loadVoucher(voucher, paraInfo, false, 'update');
                        } else {
                            //window.location.refresh();
                        }
                    }
                }
            });
        }
    }
}