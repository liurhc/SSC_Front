import React, { Component } from 'react';
import { ajax  } from 'nc-lightapp-front';
import {formItemTabOnEnter,ishasEexpandedRrow,findFirstFouceInput} from "./formItemTabOnEnter";
/**
 * @desc 监听enter键
 * @param event 事件 13 Enter 40 ArrowDown 38 ArrowUp
 * @param loadColums table中所有显示的列
 * @param attrcode 当前字段
 * @param id div的id属性 id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`}
 * @param index 行号
 */
export  function onEnterAction(event,$this,attrcode,id,index,record){
    let loadColumns=dealColumn($this.state.evidenceColumns);
    if((event.e&&event.e.code=='Enter')||(event.e&&event.e.key=='Enter')||
            event.code=='Enter'||event.key=='Enter'||event.eventKey=='Enter'||attrcode=="pk_accasoa"){
        let currentColumnArr=getColumnArr(loadColumns,attrcode,id);
        if(currentColumnArr.ishasEexpandedRrow){//判断是否有展开项,有则聚焦到第一个可编辑的元素上
            findFirstFouceInput($this,id);
        }else{
            if(!document.getElementById(currentColumnArr.id)){
                let tableScroll=document.getElementsByClassName('u-table-scroll')[0];
                let tr=tableScroll.querySelector('tbody.u-table-tbody tr:first-child');
                let tableHeight=0;
                if(tr.scrollHeight){
                    tableHeight=tr.scrollHeight;
                }
                let tableFixedRight=document.getElementsByClassName('u-table-fixed-right')[0];
                let tableBodyScroll=tableFixedRight.querySelector('.u-table-body-inner');
                tableBodyScroll.scrollTop=tableHeight?tableHeight+tableScroll.offsetHeight:tableScroll.offsetHeight;
            }
            let nextColumn=document.getElementById(currentColumnArr.id);
            if(currentColumnArr.nextAttrcode=="assid"){
                // nextColumn.focus();
                let input=nextColumn.querySelector('input:not([tabindex="-1"])');
                input.focus();
                let assidDataFlag=checkAssidData($this,record);
                if(assidDataFlag&&!record[currentColumnArr.nextAttrcode].editable&&!input.defaultValue){//有辅助核算&可编辑&没有选过值
                    $this.handleAssistAcc.call($this, record.key);
                }
                
            }else{
                // let input=nextColumn.querySelector(`div[fieldid=${currentColumnArr.nextAttrcode}] input`);
                let input=nextColumn.querySelector('input:not([tabindex="-1"])');
                if($this.ViewModel){
                    $this.ViewModel.shouldAutoFocus = true;//bbqin让加的
                }
                input.focus();
                input.select();
                if ($this.ViewModel) {
                    $this.ViewModel.shouldAutoFocus = false;
                }
            }
        }

    }else if((event.e&&event.e.code=='ArrowUp')||(event.e&&event.e.key=='ArrowUp')||
        event.code=='ArrowUp'||event.key=='ArrowUp'||attrcode=="pk_accasoa"){
            onArrowUpDown($this,'ArrowUp',id);

    }else if((event.e&&event.e.code=='ArrowDown')||(event.e&&event.e.key=='ArrowDown')||
        event.code=='ArrowDown'||event.key=='ArrowDown'||attrcode=="pk_accasoa"){
            onArrowUpDown($this,'ArrowDown',id);
    }
    
}
/**
 * @desc 判断是否有辅助核算
 * @param pk_accasoa 科目
 * @param prepareddate 业务日期
 */
function checkAssidData($this,record){
    let queryData = {
        pk_accasoa: record.pk_accasoa.value,
        prepareddate: $this.state.prepareddate.value
    };
    let assidDataFlag =false;
    //请求辅助核算数据
    let url = '/nccloud/gl/voucher/queryAssItem.do';

    ajax({
        url: url,
        data: queryData,
        async: false,
        success: function(response) {
            const { success } = response;
            //渲染已有账表数据遮罩
            if (success) {
                if (response.data && response.data.length > 0) {
                    assidDataFlag =true;
                }
            }
        }
    })
    return assidDataFlag;
}
/**
 * @desc 上下键换行操作
 * @param 
 */
export function onArrowUpDown($this,directionFlag,id){
    let idArr=id.split('-');
    let nextId='';
    if(directionFlag=="ArrowUp"){
        if(idArr[4]>0){
            idArr[4]=idArr[4]-1;
        }

    }else if(directionFlag=="ArrowDown"){
        idArr[4]=idArr[4]-0+1;
    }
    nextId=idArr.join('-');
    let nextColumn=document.getElementById(nextId);
    if($this.ViewModel){
        $this.ViewModel.shouldAutoFocus = true;//bbqin让加的
    }
    if(nextColumn){
        let input=nextColumn.querySelector('input:not([tabindex="-1"])');
        input.focus();
        input.select();
    }
    if ($this.ViewModel) {
        $this.ViewModel.shouldAutoFocus = false;
    }
}
/**
 * @desc createform最后enter跳出的事件回调
 * @param 
 */
export function onLastFormEnter(){
    let tableFixedRight=document.getElementsByClassName('u-table-fixed-right')[0];
    let tableBodyScroll=tableFixedRight.querySelector('.u-table-body-inner');
    tableBodyScroll.scrollTop=0;
    let currentFirst='hot-key-voucherTable-explanation-0';
    let nextColumn=document.getElementById(currentFirst);
    let input=nextColumn.querySelector('input:not([tabindex="-1"])');
    input.focus();
}
/**
 * @desc table列对象转换为列数组
 * @param Columns table 列
 * @param attrcode 当前字段
 * @param id 当前id
 */
function getColumnArr(Columns,attrcode,id){
    let isEexpandedRrow=false;
    let colunmArr=[];
    let currentIndex,nextIndex=0
    let nextAttrcode,nextId='';
    let idArr=id.split('-');
    Columns.map((item,index)=>{
        colunmArr.push(Columns[index].attrcode||Columns[index].key);
    })
    if(attrcode=="quantity"){//数量单价列特殊处理
        idArr[3]='price';
        nextId=idArr.join('-');
        nextAttrcode='price'
    }else{
        if(attrcode=='price'){
            attrcode='quantity';
        }
        for(let index=0;index<Columns.length;index++){
            if(Columns[index].key==attrcode||Columns[index].attrcode==attrcode){//返回当前字段的下标
                currentIndex=index;
                nextIndex=index+1;
                if(nextIndex==Columns.length){//最后一个字段
                    //判断是否有展开项
                    if(ishasEexpandedRrow(id)){
                        nextId=idArr.join('-');
                        nextAttrcode='';
                        isEexpandedRrow=true;
                    }else{
                        nextAttrcode=colunmArr[1];
                        idArr[4]=idArr[4]-0+1;
                    }
                }else{
                nextAttrcode=Columns[nextIndex].attrcode||Columns[nextIndex].key; 
                }
                
            
                idArr[3]=nextAttrcode;
                nextId=idArr.join('-');
                let nextColumn=document.getElementById(nextId);
                if(!nextColumn){//第一行有数量单价，下一行没有数据单价的情况
                    attrcode=nextAttrcode;
                    continue;
                }else{
                    let input=nextColumn.querySelector('input:not([tabindex="-1"])');
                    if((input.disabled&&nextAttrcode!='assid')||(!nextColumn.querySelector('.icon-canzhaozuixin')&&nextAttrcode=='assid')){//下一个输入框是禁用的就再赵下一个
                        attrcode=nextAttrcode;
                        continue;
                    }else{
                        break;
                    }
                }
            }
        }
    }
    return {"colunmArr":colunmArr,"id":nextId,"nextAttrcode":nextAttrcode,"ishasEexpandedRrow":isEexpandedRrow}
}
/**
 * @desc 把empty列移动到最后
 * @param Columns table 列
 */
function dealColumn(columns){
    let empty=null;
    for(let i=0;i<columns.length-1;i++){
        if(columns[i].attrcode=="empty"){
            empty=columns.splice(i,1);
            break;
        }
    }
    if(empty&&empty.length>0){
        columns.splice(columns.length,0,empty[0]);
    }
    return columns;
}