import React, { Component } from 'react';
import { ajax  } from 'nc-lightapp-front';
/**
 * @desc 监听enter键
 * @param event 事件 13 Enter 40 ArrowDown 38 ArrowUp
 * @param loadColums table中所有显示的列
 * @param attrcode 当前字段
 * @param id div的id属性 id={`hot-key-${this.moduleId}-${item.attrcode}-${index}`}
 * @param index 行号
 */
export  function formItemTabOnEnter(event,$this,attrcode,id,index,listItem){
    let currentDiv=document.getElementById(id);
    let nextDiv=currentDiv.parentElement.parentElement.nextSibling;
    if(nextDiv){
        let input=nextDiv.querySelector('input:not([tabindex="-1"])');
        if(input.getAttribute('disabled')){//继续查找下一个可编辑元素
            findNextSiblings($this,nextDiv)
        }else{
            getAutoFocus($this, input ); 
        }
    }else{//最后一个，应该跳转到下一行的第一个元素上
        let parentDiv=currentDiv.parentElement.parentElement;
        let tr=parentDiv.parentElement.parentElement.parentElement.nextSibling;
        let index=tr.getAttribute('data-row-key')-1;
        //hot-key-voucherTable-explanation-1
        let nextid='hot-key-voucherTable-explanation-'+index;
        let explanationDiv=document.getElementById(nextid);
        let input=explanationDiv.querySelector('input:not([tabindex="-1"])');
        if(input.getAttribute('disabled')){//继续查找下一个可编辑元素
            findNextSiblings($this,tr)
        }else{
            getAutoFocus($this, input ); 
        }
    }
}
/**
 * @desc 递归查找下一个可聚焦元素
 * @param td {dom} 上一个td
 * @param ViewModel {object} 全局变量
 * @param moduleId {string} 当前区域的Id
 */
function findNextSiblings($this, currentdiv ) {
    var nextDiv = currentdiv.nextElementSibling;
    var input = nextDiv && nextDiv.querySelector('input:not([tabindex="-1"])');
    if (input === null) {
        findNextSiblings($this,nextDiv);
    }else{
        getAutoFocus($this, input );
    }  
}
//可编辑元素得到焦点
function getAutoFocus($this, input ) {
    if (input !== null) {
        if($this.ViewModel){
            $this.ViewModel.shouldAutoFocus = true;//bbqin让加的
        }
        input.focus();
        input.select();
        if ($this.ViewModel) {
            $this.ViewModel.shouldAutoFocus = false;
        }
    }
}
//判断是否有展开项
export function ishasEexpandedRrow(id){
    let flag=false;
    let currentTr=document.getElementById(id).parentElement.parentElement;
    if(currentTr){
        let nextTr=currentTr.nextSibling;
        if(nextTr.className.indexOf('u-table-expanded-row')!=-1){//判断是否有展开项
            flag=true;
        }
    }
    return flag;
}
//展开项第一个可编辑的元素得到焦点
export function findFirstFouceInput($this,id){
    let currentTr=document.getElementById(id).parentElement.parentElement;
    if(currentTr){
        let nextTr=currentTr.nextSibling;
        if(nextTr.querySelector('input:not([tabindex="-1"])')&&nextTr.querySelector('input:not([disabled])')){
            let input=nextTr.querySelector('input[tabindex="0"]')||nextTr.querySelector('input:not([disabled])');
            input.focus();
        }else{
            let index=currentTr.getAttribute('data-row-key');
            //hot-key-voucherTable-explanation-1
            let nextid='hot-key-voucherTable-explanation-'+index;
            let explanationDiv=document.getElementById(nextid);
            let input=explanationDiv.querySelector('input:not([tabindex="-1"])');
            if(input.getAttribute('disabled')){//继续查找下一个可编辑元素
                findNextSiblings($this,tr)
            }else{
                getAutoFocus($this, input ); 
            }
        }
    }
}
//展开项中判断是否是最后一个字段
function formItemTabLast(){

}
