/**
 * 等号快捷键
 */
import { countRow, memoryHistry } from "../../voucherOftenApi";
import amountconvert from '../../../../amountConvert';
import { Subtr } from '../../../../../public/common/method.js';

export default function onEqual(key, record) {
    let {
        localcreditamountTotle, localdebitamountTotle,
        globalcreditamountTotle, globaldebitamountTotle,
        groupcreditamountTotle, groupdebitamountTotle
    } = this.state.evidenceData;
    let oldtotal = 0;
    let oldamount = 0;
    switch (key) {
        case 'localcreditamount':
            //等号计算，先把原币清空，再计算，否则会报最大折算误差
            record.amount = { ...record.amount, value: '' }
            record.creditamount = { ...record.creditamount, value: '' }
            record.debitamount = { ...record.debitamount, value: '' }
            oldtotal = Subtr(localdebitamountTotle.value, localcreditamountTotle.value);
            oldamount = Subtr(record.localdebitamount.value, record.localcreditamount.value);
            record.localdebitamount = { ...record.localdebitamount, value: '' }
            record.localcreditamount = {
                ...record.localcreditamount,
                value: Subtr(oldtotal, oldamount)
            }
            break;
        case 'localdebitamount':
            record.amount = { ...record.amount, value: '' }
            record.creditamount = { ...record.creditamount, value: '' }
            record.debitamount = { ...record.debitamount, value: '' }
            oldtotal = Subtr(localcreditamountTotle.value, localdebitamountTotle.value);
            oldamount = Subtr(record.localcreditamount.value, record.localdebitamount.value);
            record.localcreditamount = { ...record.localcreditamount, value: '' }
            record.localdebitamount = {
                ...record.localdebitamount,
                value: Subtr(oldtotal, oldamount)
            }
            break;
        case 'groupcreditamount':
            oldtotal = Subtr(groupdebitamountTotle.value, groupcreditamountTotle.value);
            oldamount = Subtr(record.groupdebitamount.value, record.groupcreditamount.value);
            record.groupdebitamount = { ...record.groupdebitamount, value: '' }
            record.groupcreditamount = {
                ...record.groupcreditamount,
                value: Subtr(oldtotal, oldamount)
            }
            break;
        case 'groupdebitamount':
            oldtotal = Subtr(groupcreditamountTotle.value, groupdebitamountTotle.value);
            oldamount = Subtr(record.groupcreditamount.value, record.groupdebitamount.value);
            record.groupcreditamount = { ...record.groupcreditamount, value: '' }
            record.groupdebitamount = {
                ...record.groupdebitamount,
                value: Subtr(oldtotal, oldamount)
            }
            break;
        case 'globalcreditamount':
            oldtotal = Subtr(globaldebitamountTotle.value, globalcreditamountTotle.value);
            oldamount = Subtr(record.globaldebitamount.value, record.globalcreditamount.value);
            record.globaldebitamount = { ...record.globaldebitamount, value: '' }
            record.globalcreditamount = {
                ...record.globalcreditamount,
                value: Subtr(oldtotal, oldamount)
            }
            break;
        case 'globaldebitamount':
            oldtotal = Subtr(globalcreditamountTotle.value, globaldebitamountTotle.value);
            oldamount = Subtr(record.globalcreditamount.value, record.globaldebitamount.value);
            record.globalcreditamount = { ...record.globalcreditamount, value: '' }
            record.globaldebitamount = {
                ...record.globaldebitamount,
                value: Subtr(oldtotal, oldamount)
            }
            break;
    }

    let { data, currInfo } = this.exportData(record);//键盘输入等号，直接保存
    let countValue = amountconvert(data, currInfo, key);
    countRow(record, countValue, key);
    memoryHistry(record)//记录之前值防止反算问题
}
