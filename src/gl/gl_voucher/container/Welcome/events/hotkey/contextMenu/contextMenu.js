import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ajax, base, toast, deepClone ,viewModel} from 'nc-lightapp-front';
let {removeGlobalStorage}=viewModel;
const detailExpend = [
	'bankaccount',
	'billtype',
	'checkstyle',
	'checkno',
	'checkdate',
	'verifyno',
	'verifydate',
	'cashflow'
];
export default class contextMenu extends Component{
	constructor(props){
		super(props);
	}
	getColIncludeTitle=()=>{
		let meta=this.props.meta.getMeta();
		let appcode=this.props.getUrlParam('appcode') || this.props.getSearchParam('c');
		let pagecode=this.props.getUrlParam('pagecode') || this.props.getSearchParam('p');
		let code=meta.code;
		let pageid=meta.pageid;
		let areaCode='gl_voucher_detail';//'voucherTable';
		let areaId=meta[areaCode].oid;
		
		ajax({
			url: '/nccloud/platform/template/getUserTemplateMap.do',
			data: {
				appcode,
				pagecode,
				templatecode: code,
				templateId: pageid
			},
			success: result => {
				this.keySessionS= 'appTempletStorage_' + appcode + '_' + pagecode;
				removeGlobalStorage('localStorage', this.keySessionS);
				let tableThead=document.getElementsByClassName('u-table-scroll')[0];
				let th=tableThead.querySelectorAll('thead.u-table-thead th');
				let widthObj={};
				for(let i=0;i<th.length;i++){
					let dataindex=th[i].getAttribute('dataindex');
					if(dataindex){
						widthObj[dataindex]=th[i].width;
					}
				}
				let areaList = result.data[0].areaList;
				let ajaxColums = areaList.filter(item => item.code == areaCode)[0].formPropertyList || [];
				ajaxColums = ajaxColums.filter(
					(v) => {
						return v.code!='pk_unit_v'&&v.code.indexOf('freevalue') == -1 && !detailExpend.includes(v.code) && v.visible == true;
					});
				ajaxColums.map((item,index)=>{
					item.propertyId = item.pk_form_property?item.pk_form_property:'';
					item.width=widthObj[item.code]?widthObj[item.code]:item.width;
				})
				ajax({
					url: '/nccloud/platform/template/propertySetting.do',
					data: {
						appcode,
						pagecode,
						templatecode: code,
						templateId: pageid,
						[areaCode]: {
							areaId,
							items: ajaxColums
						}
					},
					success:(res)=>{
						toast({ content: '列宽保存成功', color: 'success' });
					},
					error:(error)=>{
						toast({ content: '列宽保存失败', color: 'warning' });
					}
				})
			}
		})
	}
	render(){
		return(
			<div id="nc-table-setColumnMenu">
				<ul className="firstColumn">
					<li
						className="MouseRightListItem columnOrder glvoucher"
						// onClick={() => { this.getColIncludeTitle()}}
					>
						<div
								onClick={() => { this.getColIncludeTitle()}}
							>
								保存列宽
						</div>

					</li>
				</ul>
			</div>
		)
	}
}