
import React from 'react';
import ReactDOM from 'react-dom';
import Contextmenu from './contextmenu.js';
// import { isFunction } from '../../public';

// require('./index.less');

let testType = '';
//testType = "autoTest";//自动化测试

/**
 * 在表格表体鼠标点击右键绑定事件
 * @param {*} area 表格表体的DOM(主表或者嵌套子表)
 */
function TableColSetting(area, noColSetClass, colSetCallback, removeCustomRightMenu) {
	if (area) {
		let eventDoms = [];
		if (testType) {
			let headers = area.querySelectorAll('.u-table-header');
			eventDoms = [].concat(Array.from(headers));
		} else {
			let tableBodys = area.querySelectorAll('.u-table-body');
			let tableBodyOuters = area.querySelectorAll('.u-table-body-outer');
			eventDoms = [].concat(Array.from(tableBodys)).concat(Array.from(tableBodyOuters));
		}
		eventDoms.length &&
			eventDoms.forEach((element) => {
				element.addEventListener(
					'contextmenu',
					createMouseRightMenu.bind(this, noColSetClass, colSetCallback, removeCustomRightMenu)
				);
			});
		document.addEventListener('click', removeCustomRightMenu);
	}
}

// table willunmonut remove listener add by wanglongx
function removeTableListener(removeCustomRightMenu) {
	this.eventDoms &&
		this.eventDoms.forEach((ele) => {
			ele.removeEventListener('contextmenu', createMouseRightMenu);
		});
	document.removeEventListener('click', removeCustomRightMenu);
	// document.removeEventListener('contextmenu', removeCustomRightMenu);
	delete this.eventDoms;
}

//鼠标右键监听事件
function createMouseRightMenu(noColSetClass, colSetCallback, removeCustomRightMenu, ev) {
	ev.preventDefault();
	// ev.stopPropagation();
	let ischeckbox =
		ev.path &&
		ev.path.length &&
		ev.path.some(
			(val) => {
				return val.nodeName === 'INPUT' &&
					val.labels &&
					val.labels[0] &&
					val.labels[0].classList &&
					val.labels[0].classList.contains('table-checkbox');
			}

		);
	let isNoSet = ev.target && ev.target.classList && ev.target.classList.contains(noColSetClass);
	removeCustomRightMenu(ev, true);
	if (!ischeckbox && !isNoSet) {
		let section = document.createElement('section');
		section.classList.add('nc-table-setColumnMenu-wrap');
		document.body.appendChild(section);
		ReactDOM.render(
			<Contextmenu {...this.props}/>,
			section
		);
		// isFunction(colSetCallback) && colSetCallback();
		getMouseRightMenuPosition(ev, section);
	}
}

/**
 * 计算鼠标右键已定义菜单的位置
 */
function getMouseRightMenuPosition(ev, section) {
	// clientX/Y 获取到的是触发点相对于浏览器可视区域左上角距离
	const clickX = ev.clientX;
	const clickY = ev.clientY;
	// window.innerWidth/innerHeight 获取的是当前浏览器窗口的视口宽度/高度
	const screenW = window.innerWidth;
	const screenH = window.innerHeight;
	// 获取自定义菜单的宽度/高度
	const rootW = section.offsetWidth;
	const rootH = section.offsetHeight;

	// right为true，说明鼠标点击的位置到浏览器的右边界的宽度可以放下菜单。否则，菜单放到左边。
	// bottom为true，说明鼠标点击位置到浏览器的下边界的高度可以放下菜单。否则，菜单放到上边。
	const right = screenW - clickX > rootW;
	const left = !right;
	const bottom = screenH - clickY > rootH;
	const top = !bottom;
	if (right) {
		section.style.left = `${clickX}px`;
	}
	if (left) section.style.left = `${clickX - rootW}px`;
	if (bottom) section.style.top = `${clickY}px`;
	if (top) section.style.top = `${clickY - rootH}px`;
	if (screenW - clickX < 110 + 32) {
		section.classList.add('nearRight');
	}
}
export { TableColSetting, removeTableListener };
