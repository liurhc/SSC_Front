import { ajax, deepClone, toast, promptBox, cardCache } from 'nc-lightapp-front';
import {
	dataSourceTable,
	tableId,
	dataSource,
	dataSourceCoord,
	dataSourceDetail,
	successStatus,
	dataSourceNumber,
	oldSource,
	dataSourceSaveNext
} from '../../../../public/components/constJSON';
let { setDefData, getDefData,addCache,getCurrentLastId,updateCache ,deleteCacheById} = cardCache;
export default function vouchersaveUpdateCache(_voucherStatus,idname,id,resultData,formId,dataSourceTable){
    if(_voucherStatus == 'delete'){
        deleteCacheById(idname,id,dataSourceTable);
    }else{
        resultData.voucher.totaldebit.scale=resultData.paraInfo.scale;
		resultData.voucher.totalcredit.scale=resultData.paraInfo.scale;
        let updataData={
            body:{bodys:{rows:[]}},
            head:{}
        }
        updataData.head[formId]={rows:[]};
        let headrowsData={values:resultData.voucher};
        let bodyrowsData=[];
        if(resultData.voucher.details){
            resultData.voucher.details.map((item,index)=>{
                bodyrowsData.push({'values':item});
            })
        }
        updataData.head[formId].rows.push(headrowsData);
        updataData.body.bodys.rows=bodyrowsData;
        if(_voucherStatus == 'add'){//新增保存,复制保存，新增暂存
            /*
                * id：数据主键的值
                * formId: 卡片表头的区域编码
                * dataSourceTable: 缓存数据命名空间
                */
            addCache(id,updataData,formId,dataSourceTable);
        }else if(_voucherStatus == 'update'){//修改保存,修改暂存
            /*
                * idname: 数据主键的命名
                * id：数据主键的值
                * formId: 卡片表头的区域编码
                * dataSourceTable: 缓存数据命名空间
            */
            updateCache(idname,id,updataData,formId,dataSourceTable);
        }
    }
}
