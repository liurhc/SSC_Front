import React, { Component } from 'react';
import {base} from 'nc-lightapp-front';
const {NCNumber}=base;

import ReferLoader from '../../../../public/ReferVoucherLoader';

const cashFlowItemRef ='uapbd/refer/fiacc/CashflowTreeRef/index.js';
const innercustRef ='uapbd/refer/org/FinanceOrgAllGroupAllDataTreeRef/index.js';
const currtypeRef='uapbd/refer/pubinfo/CurrtypeGridRef/index.js';

/**
 * 归集
 */
export default class Imputation extends Component {

    constructor(props){
        super(props);
        this.state={
            imputation:{
                cashflow: {
                    display: '',
                    value: ''
                },
                innercust: {
                    display: '',
                    value: ''
                },
                rate: {value:'100'},
                currtype: props.currtype
            }
        }
        this.multilang=props.multilang;//多语
        this.pk_org='';//核算账簿默认组织
    }

    componentDidMount() {
        this.props.callback(this.state.imputation)
    }

    render() {
        return (
            <div>
                <div className="modal-form-item">
                    <span className="item-title nc-theme-common-font-c">{this.multilang['20021005card-000017']/*现金流量项目*/}</span>
                    <div className="item-content">
                        <ReferLoader
                            tag='cashflowItem'
                            refcode={cashFlowItemRef}
                            value={{
                                refname:this.state.imputation.cashflow.display,
                                refpk:this.state.imputation.cashflow.value
                            }}
                            onlyLeafCanSelect={true}
                            queryCondition={() => {
                                return {
                                    TreeRefActionExt: 'nccloud.web.gl.ref.CashFlowTypeRefSqlBuilder',
                                    pk_org: this.pk_org,
                                    ismain: 'Y'
                                };
                            }}
                            onChange={(v) => {
                                let { imputation } = this.state;
                                if(v.refcode&&v.refname){
                                    imputation.cashflow.display = v.refcode + '/' + v.refname;
                                    imputation.cashflow.value = v.refpk;
                                }else{
                                    imputation.cashflow.display ='';
                                    imputation.cashflow.value = '';
                                }
                                this.setState({
                                    imputation
                                });
                            }}
                        />
                    </div>
                </div>
                <div className="modal-form-item">
                    <span className="item-title nc-theme-common-font-c">{this.multilang['20021005card-000018']/*内部单位*/}</span>
                    <div className="item-content">
                        <ReferLoader
                            tag='innercust'
                            refcode={innercustRef}
                            value={{
                                refname: this.state.imputation.innercust.display,
                                refpk: this.state.imputation.innercust.value

                            }}
                            onChange={(v) => {
                                let { imputation } = this.state;
                                imputation.innercust.display = v.refname;
                                imputation.innercust.value = v.refpk;
                                this.setState({
                                    imputation
                                });
                            }}
                        />
                    </div>
                </div>
                <div className="modal-form-item">
                    <span className="item-title nc-theme-common-font-c">{this.multilang['20021005card-000019']/*比例*/}(%)</span>
                    <div className="item-content">
                        <NCNumber
                            max={100}
                            min={0}
                            type="customer"
                            value={this.state.imputation.rate.value}
                            onChange={(v) => {
                                let { imputation } = this.state;
                                imputation.rate.value= v
                                this.setState({
                                    imputation
                                });
                            }}
                        />
                    </div>
                </div>
                <div className="modal-form-item">
                    <span className="item-title nc-theme-common-font-c">{this.multilang['20021005card-000020']/*币种*/}</span>
                    <div className="item-content">
                        <ReferLoader
                            tag='currtype'
                            refcode={currtypeRef}
                            value={{
                                refname: this.state.imputation.currtype.display,
                                refpk: this.state.imputation.currtype.value
                            }}
                            onChange={(v) => {
                                let { imputation } = this.state;
                                imputation.currtype.display = v.refname;
                                imputation.currtype.value = v.refpk;
                                this.setState({
                                    imputation
                                });
                            }}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

