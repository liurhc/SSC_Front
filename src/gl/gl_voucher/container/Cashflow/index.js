import React, { Component } from 'react';
import { createPage, base ,ajax,getMultiLang,cardCache} from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, beforeEvent,amountUtils } from './events';
import Imputation from './Imputation';
let { getDefData } = cardCache;
import './index.less';
const {NCButton: Button} = base;

let formId = 'cashflow';
let tableId = 'cfinner';
class Cashflow extends Component {
	constructor(props) {
		super(props);
		this.state = {
			voucher: {},
			cashdetails: '', //凭证分录信息
			imput: {
				rate: {value:'100'},
				cashflow: {
					display: '',
					value: ''
				},
				innercust: {
					display: '',
					value: ''
				},
				currtype: {
					display: '',
					value: ''
				},
				json:{},
				indexTable: []
			},
			json:{}
		};
		
	}


	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				initTemplate.call(this, this.props, this.state);
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}

	componentDidMount() {
		this.props.onRef(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.cashFlow && nextProps.cashFlow.length != 0) {
			this.setState({
				cashdetails: nextProps.cashFlow
			});
		}
	}

	imputClick = () => {
		let { cashflow, innercust, currtype, indexTable, rate } = this.state.imput;
		let props=this.props;
		let { sendData } = this.props;
		ajax({
			url:'/nccloud/gl/glpub/cfitemquery.do',
			data:{pk_cashflow_main:cashflow.value},
			success:function(response) {
				let  { data,success } = response;
				indexTable.map((item, i) => {
					let detailno=props.editTable.getValByKeyAndIndex(tableId, item, 'detailno').value;
					let detail=sendData.details[detailno-1];
					props.editTable.setValByKeyAndIndex(tableId, item, 'pk_cashflow_main',cashflow);
					props.editTable.setValByKeyAndIndex(tableId, item, 'pk_currtype', currtype);
					if(!innercust||!innercust.value||innercust.value==''){
						let innserCorpData=getDefData(detail.assid.value,'innserCorpDataSource');
						if(innserCorpData)
							props.editTable.setValByKeyAndIndex(tableId, item, 'pk_innercorp', innserCorpData);
						else
							props.editTable.setValByKeyAndIndex(tableId, item, 'pk_innercorp', {display:'',value:''});
					}else{
						props.editTable.setValByKeyAndIndex(tableId, item, 'pk_innercorp', innercust);
					}
					let occamount=props.editTable.getValByKeyAndIndex(tableId, item, 'occamount').value * rate.value / 100;
					let localamount=props.editTable.getValByKeyAndIndex(tableId, item, 'localamount').value * rate.value / 100;
					let groupamount=props.editTable.getValByKeyAndIndex(tableId, item, 'groupamount').value * rate.value / 100;
					let globalamount=props.editTable.getValByKeyAndIndex(tableId, item, 'globalamount').value * rate.value / 100;
					let record={
						direct:props.editTable.getValByKeyAndIndex(tableId, item, 'direct'),
						occamount: {value:occamount},
						localamount: {value:localamount},
						groupamount: {value:groupamount},
						globalamount: {value:globalamount},
					}
					let cashtype=detail.pk_accasoa.cashtype;
					let nativeAmount;
					if (detail.localdebitamount&&detail.localdebitamount.value&&detail.localdebitamount.value-0!=0) {
						nativeAmount = detail.localdebitamount.value;
					} else {
						nativeAmount = detail.localcreditamount.value;
					}
					if (data)
						amountUtils(props,tableId,item,record,data.itemtype,nativeAmount,cashtype);
				});
			}
		});
	};

	render() {
		let { editTable, form, modal, sendData } = this.props;
		const { createModal } = modal;
		let { createEditTable } = editTable;
		let { createForm } = form;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'cashflow',
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="form-wrapper">
					{createForm(formId)}
				</div>
				<div className="table-wrapper cashFlowTable">
					{createEditTable(tableId, {
						showIndex: false,
						showCheck: true,
						isDrag:true,
						onAfterEvent: afterEvent.bind(this),
						onBeforeEvent: beforeEvent.bind(this),
						// height:300
					})}
					{this.state.json['20021005card-000015']?(createModal('imputModal', {
						title: this.state.json['20021005card-000015'],/* 国际化处理： 归集*/
						content: (
							<Imputation
								pk_org={sendData.paraInfo.unit.value}
								multilang={this.state.json}
								currtype={this.state.imput.currtype}
								callback={this.imputCallBack}
							/>
						),
						className: 'senior',
						beSureBtnClick: this.imputClick.bind(this)
					})):null}
				</div>
				{/* <div className="u-modal-footer cashflowButton">
					<Button className="button-primary" onClick={this.onConfirm.bind(this)} fieldid="cashflow">
						{this.state.json['20021005card-000003']}
					</Button>
				</div> */}
			</div>
		);
	}

	imputCallBack=(imputation)=>{
		let {imput} =this.state;
		imput.cashflow=imputation.cashflow;
		imput.innercust=imputation.innercust;
		imput.rate=imputation.rate;
		imput.currtype=imputation.currtype;
		this.setState({imput})
	}

	onConfirm() {
		if(!!this.props.cashflowType)//浏览态不走
			return;
		//确定后
		let { sendData } = this.props;
		let { cashdetails } = this.state;

		let rows = this.props.editTable.getAllData(tableId).rows;
		//先将现金流量清空
		for (let index = 0; index < cashdetails.length; index++) {
			cashdetails[index].cashflow = [];
			if (!cashdetails[index].hasOwnProperty('expand')) {
				cashdetails[index].expand = {};
			}
			cashdetails[index].expand.cashflowname = {
				display: '',
				value: ''
			};
		}
		//重新设置现金流量
		if (rows && rows.length > 0) {
			for (let i = 0; i < cashdetails.length; i++) {
				for (let j = 0; j < rows.length; j++) {
					if (
						((rows[j].values.pk_cashflow_main.value && rows[j].values.pk_cashflow_main.value != '') ||
							(rows[j].values.pk_cashflow_ass.value && rows[j].values.pk_cashflow_ass.value != '')) &&
						cashdetails[i].detailindex.value == rows[j].values.detailno.value
					) {
						if(!sendData.paraInfo.NC001){
							rows[j].values.groupamount={value:'0'}
						}
						if(!sendData.paraInfo.NC002){
							rows[j].values.globalamount={value:'0'}
						}
						rows[j].values.detailindex={
							value:rows[j].values.detailno.value
						}
						if(!rows[j].values.pk_detail){
							rows[j].values.pk_detail={
								value:cashdetails[i].pk_detail?cashdetails[i].pk_detail.value:''
							}
						}
						cashdetails[i].cashflow.push(rows[j].values);
						let cashflowname = '';
						if (rows[j].values.pk_cashflow_main&&rows[j].values.pk_cashflow_main.value) {
							cashflowname =
								'【' +
								rows[j].values.pk_cashflow_main.display +
								'：' +
								rows[j].values.localamount.value +
								'】';
						}
						if (rows[j].values.pk_cashflow_ass&&rows[j].values.pk_cashflow_ass.value) {
							cashflowname +=
								'【' +
								rows[j].values.pk_cashflow_ass.display +
								'：' +
								rows[j].values.localamount.value +
								'】';
						}
						cashdetails[i].expand.cashflowname = {
							display: cashflowname,
							value: cashflowname
						};
					}
				}
			}
		}
		sendData.details = cashdetails;
		let voucher={
			voucher:sendData,
			paraInfo:sendData.paraInfo
		}
		this.props.getCashFlow(voucher);
	}
}

Cashflow = createPage({
	//	initTemplate: initTemplate,
	mutiLangCode: '2002'
})(Cashflow);

export default Cashflow;
