export default function setCashFlowAmount(props,tableid,index,record,cashflowItemType,nativeAmount,cashtype){
    let occamount={
        display:record.occamount.value,
        value:record.occamount.value
    };
    let localamount={
        display:record.localamount.value,
        value:record.localamount.value
    };
    let groupamount={
        display:record.groupamount.value,
        value:record.groupamount.value
    };
    let globalamount={
        display:record.globalamount.value,
        value:record.globalamount.value
    };
    if ((cashflowItemType== '2' && record.direct.value == '贷'/*-=notranslate=-*/)
        || (cashflowItemType == '1' && record.direct.value == '借'/*-=notranslate=-*/)) {
        //借方流入、贷方流出
        if ((nativeAmount > 0 && record.localamount.value > 0)
            || (nativeAmount < 0 && record.localamount.value < 0)) {
            //setAmountOpp(props, tableid, index, record);
            occamount.value=occamount.value*-1;
            occamount.display=occamount.display*-1;
            localamount.value=localamount.value*-1;
            localamount.display=localamount.display*-1;
            groupamount.value=groupamount.value*-1;
            groupamount.display=groupamount.display*-1;
            globalamount.value=globalamount.value*-1;
            globalamount.display=globalamount.display*-1;
        }
    }
    if ((cashflowItemType == '1' && record.direct.value == '贷'/*-=notranslate=-*/)
        || (cashflowItemType == '2' && record.direct.value == '借'/*-=notranslate=-*/)) {
        if ((nativeAmount > 0 && record.localamount.value < 0)
            || (nativeAmount < 0 && record.localamount.value > 0)) {
                occamount.value=occamount.value*-1;
                occamount.display=occamount.display*-1;
                localamount.value=localamount.value*-1;
                localamount.display=localamount.display*-1;
                groupamount.value=groupamount.value*-1;
                groupamount.display=groupamount.display*-1;
                globalamount.value=globalamount.value*-1;
                globalamount.display=globalamount.display*-1;
        }
    }
    if (cashtype > 0) {//现金类、银行类、现金等价物类取反
        occamount.value=occamount.value*-1;
        occamount.display=occamount.display*-1;
        localamount.value=localamount.value*-1;
        localamount.display=localamount.display*-1;
        groupamount.value=groupamount.value*-1;
        groupamount.display=groupamount.display*-1;
        globalamount.value=globalamount.value*-1;
        globalamount.display=globalamount.display*-1;
    }
    props.editTable.setValByKeyAndIndex(tableid,index,'occamount',occamount);
    props.editTable.setValByKeyAndIndex(tableid,index,'localamount',localamount);
    props.editTable.setValByKeyAndIndex(tableid,index,'groupamount',groupamount);
    props.editTable.setValByKeyAndIndex(tableid,index,'globalamount',globalamount);
}
