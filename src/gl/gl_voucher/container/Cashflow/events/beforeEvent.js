import { ajax } from 'nc-lightapp-front';
let tableId = 'cfinner';
export default function beforeEvent(props,tableid,item,index,val,record) {
    if(item.key!='detailno'&&(!record.values.detailno||!record.values.detailno.value||record.values.detailno.value==''))
        return false;

    let meta = props.meta.getMeta();
    let voucher=props.sendData;
    let pk_org=voucher.pk_org?voucher.pk_org.value:null;
    if(!pk_org){
        let url = "/nccloud/gl/glpub/queryFinanceOrg.do";
        let param = {};
        param.pk_accountingbook = voucher.pk_accountingbook.value;
        ajax({
            url: url,
            data: param,
            async: false,
            success: function(response) {
                let { data, success } = response;
                if (success) {
                    pk_org=data.pk_org;
                    voucher.pk_org={value:data.pk_org};
                }
            }
        });
    }
    switch(item.key){
        case 'pk_cashflow_main'://主表项
            meta[tableId].items = meta[tableId].items.map((item, key) => {
                if (item.attrcode == 'pk_cashflow_main') {
                    item.queryCondition = () => {
                        return {
                            'TreeRefActionExt': 'nccloud.web.gl.ref.CashFlowTypeRefSqlBuilder',
                            'pk_org': pk_org, //设置组织
                            'ismain': 'Y', //Y 主表项 N附表项
                            isDataPowerEnable: 'Y',
						    DataPowerOperationCode: 'fi',
                        }
                    }
                    item.onlyLeafCanSelect=true;
                }
                return item;
            });
            props.meta.setMeta(meta);
            return true;
        case 'pk_cashflow_ass':            
            meta[tableId].items = meta[tableId].items.map((item, key) => {
                if (item.attrcode == 'pk_cashflow_ass') {
                    item.queryCondition = () => {
                        return {
                            'TreeRefActionExt': 'nccloud.web.gl.ref.CashFlowTypeRefSqlBuilder',
                            'pk_org': pk_org, //设置组织
                            'ismain': 'N', //Y 主表项 N附表项
                            isDataPowerEnable: 'Y',
						    DataPowerOperationCode: 'fi',
                        }
                    }
                    item.onlyLeafCanSelect=true;
                }
                return item;
            });
            props.meta.setMeta(meta);
            return true;
        case 'pk_unit':
        case 'assid':
        case 'direct':
            return false;            
        default:
            return true;
    }
}
