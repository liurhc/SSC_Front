
import { ajax,toast,cardCache } from 'nc-lightapp-front';
import amountconvert from '../../../amountConvert';
import {queryCurrinfo} from '../../../amountConvert/currInfo.js';
import setScale from '../../../../public/common/amountFormat.js';
import {currInfoDataSource} from '../../../../public/components/constJSON';
let { setDefData, getDefData } = cardCache;
import amountUtils from './amountUtils'; 
//import { resolve } from 'url';
let tableId = 'cfinner';
export default function afterEvent(props,tableid,key,val,changedrows,index,record,type) {
    let voucher=props.sendData;
    switch (key) {
        case 'detailno':   
            if(!val||val-0<1){
                props.editTable.setValByKeyAndIndex(tableId,index,'pk_unit',{//业务单元
                    value: '', display: '',
                })
                props.editTable.setValByKeyAndIndex(tableId,index,'pk_currtype',{//币种
                    value: '', display: '',
                })
                props.editTable.setValByKeyAndIndex(tableId,index,'pk_accasoa',{//会计科目
                    value: '', display: '',
                })
                props.editTable.setValByKeyAndIndex(tableId,index,'assid',{//辅助核算
                    value: '', display: '',
                })
                props.editTable.setValByKeyAndIndex(tableId,index,'pk_cashflow_main',{
                    value: '', display: '',
                })
                props.editTable.setValByKeyAndIndex(tableId,index,'pk_cashflow_ass',{
                    value: '', display: '',
                })
                props.editTable.setValByKeyAndIndex(tableId,index,'occamount',{//原币
                    value: '', display: '',
                })
                props.editTable.setValByKeyAndIndex(tableId,index,'localamount',{
                    value: '', display: '',
                })
                props.editTable.setValByKeyAndIndex(tableId,index,'groupamount',{
                    value: '', display: '',
                })
                props.editTable.setValByKeyAndIndex(tableId,index,'globalamount',{
                    value: '', display: '',
                })
                return;
            }
            //将对应行的 业务单元、币种、方向设置到行上
            if(val-0>props.sendData.details.length){
                toast({ content: this.state.json['20021005card-000008'], color: 'warning'});/* 国际化处理： 分录号过大！*/
                return;
            }
            let details= props.sendData.details[val-1];
            let ass=details&&details.ass;
            if(ass&&ass.length>0){
                let assvos=[];
                ass.map((item, i) => {
                    let assvo={};
                    assvo.pk_checktype=item['m_pk_checktype'];
                    assvo.pk_checkvalue=item['m_pk_checkvalue'];
                    assvos.push(assvo);
                });
                let innserCorpData=getDefData(details.assid.value,'innserCorpDataSource');
                if(innserCorpData){
                    props.editTable.setValByKeyAndIndex(tableId,index,'pk_innercorp',innserCorpData);
                }else{
                    ajax({
                        url:'/nccloud/gl/cashflow/queryInnerCorp.do',
                        data:{assvo:assvos},
                        success:function(response) {
                            if(response.data){
                                props.editTable.setValByKeyAndIndex(tableId,index,'pk_innercorp',response.data);
                                setDefData(details.assid.value,'innserCorpDataSource',response.data);
                            }
                        }
                    })
                }
                
            }
            if(details){
                props.editTable.setValByKeyAndIndex(tableId,index,'pk_unit',{//业务单元
                    value: details.pk_unit.value, display: details.pk_unit_v.display,
                })
    
                props.editTable.setValByKeyAndIndex(tableId,index,'pk_currtype',{//币种
                    value: details.pk_currtype.value, display: details.pk_currtype.display,
                })
               
                props.editTable.setValByKeyAndIndex(tableId,index,'pk_accasoa',{//会计科目
                    value: details.pk_accasoa.value, display: details.pk_accasoa.display,
                })
                props.editTable.setValByKeyAndIndex(tableId,index,'assid',{//辅助核算
                    value: details.assid.value, display: details.assid.display,
                })
            }
           

            let direction={};
            if(details&&details.direction&&(details.direction=='C'||details.direction=='D')){
                direction.value=details.direction.value=='D'?'借'/*-=notranslate=-*/:'贷'/*-=notranslate=-*/
                direction.display=details.direction.value=='D'?this.state.json['20021005card-000009']:this.state.json['20021005card-000010']/* 国际化处理： 借,贷*/
            }else if(details){
                if(details&&details.localcreditamount&&details.localcreditamount.value){
                    direction.value='贷'/*-=notranslate=-*/;
                    direction.display=this.state.json['20021005card-000010'];/* 国际化处理： 贷*/
                }else{
                    direction.value='借'/*-=notranslate=-*/;
                    direction.display=this.state.json['20021005card-000009'];/* 国际化处理： 借*/
                }
            }           
            props.editTable.setValByKeyAndIndex(tableId,index,'direct',direction)//方向
            
            let currInfo=queryCurrinfo(voucher.pk_accountingbook.value,record.values.pk_currtype.value,voucher.prepareddate.value)
            countAmount(props,tableid,index,val,details,currInfo);
            
            break;
        case 'pk_cashflow_main':
            //主表项 
            //现金流入在借方 金额为负 在贷方为正
            //现金流出在借方 金额为正 在贷方为负
            //cashtype>0 方向与以上方向相反
            let detailno=record.values.detailno.value;
            let detail= props.sendData.details[detailno-1];
            let cashtype=detail.pk_accasoa.cashtype?detail.pk_accasoa.cashtype:(detail.accsubjcashtype&&detail.accsubjcashtype.value);
            let nativeAmount;
            if (detail.localdebitamount&&detail.localdebitamount.value&&detail.localdebitamount.value!=0) {
                nativeAmount = detail.localdebitamount.value;
            } else {
                nativeAmount = detail.localcreditamount.value;
            }
            ajax({
                url:'/nccloud/gl/glpub/cfitemquery.do',
                data:{pk_cashflow_main:val.refpk},
                success:function(response) {
                    let  { data,success } = response; 
                    if(data){//itemtype==1:流入 itemtype==2:流出
                        amountUtils(props,tableid,index,record.values,data.itemtype,nativeAmount,cashtype);
                    } 
                }
            })
            if(val.refpk&&val.refpk){
                let pk_cashflow_main={
                    value:val.refpk,
                    display:val.refcode+'/'+val.refname,
                    isEdit:false
                }
                props.editTable.setValByKeyAndIndex(tableid,index,key,pk_cashflow_main);
            }
        break;
        case 'pk_cashflow_ass':
            if (val.refpk && val.refpk != '') {
                let pk_cashflow_ass = {
                    value: val.refpk,
                    display: val.refcode + '/' + val.refname,
                    isEdit: false
                }
                props.editTable.setValByKeyAndIndex(tableid, index, key, pk_cashflow_ass);
            }
            break;
        case 'pk_currtype':
            //币种
            if(!val.refpk||val.refpk=='')
                return;
            let id=voucher.pk_accountingbook.value+val.refpk+voucher.prepareddate;
            let currdata=getDefData(id,currInfoDataSource);
            if(currdata){
                let {scale,roundtype}=currdata;
                props.editTable.setValByKeyAndIndex(tableid, index, 'occamount', {
                    value: record.values.occamount.value,
                    display:setScale(record.values.occamount.value,scale,roundtype),
                    scale: scale
                });
            }else{
                ajax({
                    url: '/nccloud/gl/glpub/ratequery.do',
                    data: {
                        "pk_accountingbook": voucher.pk_accountingbook.value,
                        "pk_currtype": record.values.pk_currtype.value,
                        "prepareddate": voucher.prepareddate
                    },
                    async: false,
                    success: function (response) {
                        setDefData(id, currInfoDataSource, response.data);
                        let {scale,roundtype}=response.data;
                        props.editTable.setValByKeyAndIndex(tableid, index, 'occamount', {
                            value: record.values.occamount.value,
                            display:setScale(record.values.occamount.value,scale,roundtype),
                            scale: scale
                        });
                    }
                })
            }
            break;

        case 'occamount':
            
            let occamount=amountconvert({
                amount:val,
                pk_currtype:record.values.pk_currtype.value,
                pk_accountingbook:voucher.pk_accountingbook.value,
                prepareddate:voucher.prepareddate.value
            },{},'amount');
            let ocurrInfo=queryCurrinfo(voucher.pk_accountingbook.value,record.values.pk_currtype.value,voucher.prepareddate.value);
            setAmount(props,index,occamount.amount, occamount.localamount, occamount.groupamount, occamount.globalamount,ocurrInfo);
            break;
        case 'localamount':
            let localamount=amountconvert({
                localamount:val,
                pk_currtype:record.values.pk_currtype.value,
                pk_accountingbook:voucher.pk_accountingbook.value,
                prepareddate:voucher.prepareddate.value
            },{},'localamount');
            let lcurrInfo=queryCurrinfo(voucher.pk_accountingbook.value,record.values.pk_currtype.value,voucher.prepareddate.value);
            setAmount(props,index,localamount.amount, localamount.localamount, localamount.groupamount, localamount.globalamount,lcurrInfo);
            break;
        case 'groupamount':
            // let groupamount=amountconvert({
            //     amount:record.values.occamount.value,
            //     localamount:record.values.occamount.value,
            //     groupamount:val,
            //     pk_currtype:record.values.pk_currtype.value,
            //     pk_accountingbook:voucher.pk_accountingbook.value,
            //     prepareddate:voucher.prepareddate.value
            // },{},'groupamount');
            // let grcurrInfo=queryCurrinfo(voucher.pk_accountingbook.value,record.values.pk_currtype.value,voucher.prepareddate.value);
            // setAmount(props,index,groupamount.amount, groupamount.localamount, groupamount.groupamount, groupamount.globalamount,grcurrInfo);
            break;            
        case 'globalamount':
            // let globalamount=amountconvert({
            //     globalamount:val,
            //     pk_currtype:record.values.pk_currtype.value, 
            //     pk_accountingbook:voucher.pk_accountingbook.value,
            //     prepareddate:voucher.prepareddate.value
            // },{},'globalamount');
            // let glcurrInfo=queryCurrinfo(voucher.pk_accountingbook.value,record.values.pk_currtype.value,voucher.prepareddate.value);
            // setAmount(props,index,globalamount.amount, globalamount.localamount, globalamount.groupamount, globalamount.globalamount,glcurrInfo);
            break; 
        default:
            break;
    }
} 

function countAmount(props,tableid,index,val,details,currInfo){
    let totleData= props.editTable.getAllData(tableid).rows;
    totleData.pop();

    let amount=details.amount.value==''?0:details.amount.value*1;
    let localamount=(!!!details.localcreditamount.value?0:details.localcreditamount.value*1)
            +(!!!details.localdebitamount.value?0:details.localdebitamount.value*1);
    let groupamount=(!!!details.groupcreditamount.value?0:details.groupcreditamount.value*1)
            +(!!!details.groupdebitamount.value?0:details.groupdebitamount.value*1);
    let globalamount=(!!!details.globalcreditamount.value?0:details.globalcreditamount.value*1)
            +(!!!details.globaldebitamount.value?0:details.globaldebitamount.value*1);
    totleData.map((item, i) => {//只循环一次
        if (item.values.detailno.value == val) {
            amount = amount - (!!!item.values.occamount.value?0:item.values.occamount.value);
            localamount = localamount - (!!!item.values.localamount.value?0:item.values.localamount.value);
            groupamount = groupamount - (!!!item.values.groupamount.value?0:item.values.groupamount.value);
            globalamount = globalamount - (!!!item.values.globalamount.value?0:item.values.globalamount.value);
        }
    });

    setAmount(props,index,amount,localamount,groupamount,globalamount,currInfo);
    
}

function setAmount(props,index,amount,localamount,groupamount,globalamount,currInfo){
    props.editTable.setValByKeyAndIndex(tableId,index,'occamount',{//原币
        value:setScale(amount,currInfo.scale,currInfo.roundtype), 
        display:setScale(amount,currInfo.scale,currInfo.roundtype),
        scale:currInfo.scale
    });
    props.editTable.setValByKeyAndIndex(tableId,index,'localamount',{//本币
        value:setScale(localamount,currInfo.orgscale,currInfo.orgroundtype), 
        display:setScale(localamount,currInfo.orgscale,currInfo.orgroundtype),
        scale:currInfo.orgscale
    });
    props.editTable.setValByKeyAndIndex(tableId,index,'groupamount',{//集团
        value: setScale(groupamount,currInfo.groupscale,currInfo.grouproundtype), 
        display:setScale(groupamount,currInfo.groupscale,currInfo.grouproundtype),
        scale:currInfo.groupscale
    });
    props.editTable.setValByKeyAndIndex(tableId,index,'globalamount',{//全局
        value:setScale(globalamount,currInfo.globalscale,currInfo.globalroundtype), 
        display:setScale(globalamount,currInfo.globalscale,currInfo.globalroundtype),
        scale:currInfo.globalscale
    });
}
