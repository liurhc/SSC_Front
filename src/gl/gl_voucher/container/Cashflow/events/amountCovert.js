
let local_convert='local_convert';//根据本币计算
let raw_convert='raw_convert';//根据原币计算
//currInfo{NC001,NC002, excrate2,excrate3,excrate4,ismod}

//根据原币计算
function getAmountByOcc(amount,currInfo){  
    let data={
        amount:amount,
        localamount:0,
        groupamount:'',
        globalamount:''
    };
    data.localamount=getLocalAmount(amount,currInfo);
    data.groupamount=getGroupAmount(data.amount,data.localamount,currInfo);
    data.globalamount=getGlobalAmount(data.amount,data.localamount,currInfo);
    return data;
}

//根据组织本币计算
function getAmountByLocal(localamount,currInfo){
    let data={
        amount:0,
        localamount:localamount,
        groupamount:'',
        globalamount:''
    };

    data.amount=getOccAmount(localamount,currInfo);
    data.groupamount=getGroupAmount(data.amount,data.localamount,currInfo);
    data.globalamount=getGlobalAmount(data.amount,data.localamount,currInfo);
    return data;
}

//根据集团本币计算
function getAmountByGroup(groupamount,currInfo){
    let data={
        amount:0,
        localamount:0,
        groupamount:groupamount,
        globalamount:''
    };

    if (currInfo.excrate3 && currInfo.excrate3 != '0') {
        if (raw_convert == currInfo.NC001) {
            if (currInfo.groupmode) {
                data.amount=groupamount*currInfo.excrate3;            
            } else {
                data.amount=groupamount/currInfo.excrate3;
            }
            data.localamount=getLocalAmount(data.amount,currInfo);
            data.globalamount=getGlobalAmount(data.amount,data.localamount,currInfo);
        } else if (local_convert == currInfo.NC001) {
            if (currInfo.groupmode) {
                data.localamount=groupamount*currInfo.excrate3;            
            } else {
                data.localamount=groupamount/currInfo.excrate3;
            }
            data.amount=getOccAmount(data.localamount,currInfo);
            data.globalamount=getGlobalAmount(data.amount,data.localamount,currInfo);
        }
    }
    return data;
}

//根据全局本币计算
function getAmountByGlobal(globalamount,currInfo){
    let data={
        amount:0,
        localamount:0,
        groupamount:'',
        globalamount:globalamount,
    };

    if (currInfo.excrate4 && currInfo.excrate4 != '0') {
        if (raw_convert == currInfo.NC002) {
            if (currInfo.globalmode) {
                data.amount=globalamount*currInfo.excrate3;            
            } else {
                data.amount=globalamount/currInfo.excrate3;
            }
            data.localamount=getLocalAmount(data.amount,currInfo);
            data.groupamount=getGroupAmount(data.amount,data.localamount,currInfo);
        } else if (local_convert == currInfo.NC001) {
            if (currInfo.globalmode) {
                data.localamount=globalamount*currInfo.excrate3;            
            } else {
                data.localamount=globalamount/currInfo.excrate3;
            }
            data.amount=getOccAmount(data.localamount,currInfo);
            data.groupamount=getGroupAmount(data.amount,data.localamount,currInfo);
        }
    }
    return data;
}

//计算原币
function getOccAmount(localamount,currInfo){
    let amount=0;
    if(currInfo.excrate2&&currInfo.excrate2!='0'){
        if(currInfo.orgmode){
            amount=localamount*currInfo.excrate2;
        }else{
            amount=localamount/currInfo.excrate2;
        }
    }
    return amount;
}

//计算组织本币
function getLocalAmount(amount,currInfo){
    let localamount=0;
    if(currInfo.excrate2&&currInfo.excrate2!='0'){
        if(currInfo.orgmode){
            localamount=amount/currInfo.excrate2;
        }else{
            localamount=amount*currInfo.excrate2;
        }
    }
    return localamount;
}

//计算集团本币
function getGroupAmount(amount,localamount,currInfo){
    let groupamount='';
    if(currInfo.excrate3&&currInfo.excrate3!='0'){
        if(raw_convert==currInfo.NC001){
            if(currInfo.groupmode){
                groupamount=amount/currInfo.excrate3;
            }else{
                groupamount=amount*currInfo.excrate3;
            }
        }else if(local_convert==currInfo.NC001){
            if(currInfo.groupmode){
                groupamount=localamount/currInfo.excrate3;
            }else{
                groupamount=localamount*currInfo.excrate3;
            }
        }      
    }
    return groupamount;
}

//计算全局本币
function getGlobalAmount(amount,localamount,currInfo){
    let globalamount='';
    if(currInfo.excrate4&&currInfo.excrate4!='0'){
        if(raw_convert==currInfo.NC002){
            if(currInfo.globalmode){
                globalamount=amount/currInfo.excrate4;
            }else{
                globalamount=amount*currInfo.excrate4;
            }
        }else if(local_convert==currInfo.NC002){
            if(currInfo.globalmode){
                globalamount=localamount/currInfo.excrate4;
            }else{
                globalamount=localamount*currInfo.excrate4;
            }
        }      
    }
    return globalamount;   
}

export {getAmountByOcc,getAmountByLocal,getAmountByGroup,getAmountByGlobal}



