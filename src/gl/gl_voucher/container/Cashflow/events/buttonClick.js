import { createPage, ajax, base, toast, cacheTools, deepClone ,cardCache} from 'nc-lightapp-front';
import amountconvert from '../../../amountConvert';
//import {queryCurrinfo} from '../../../amountConvert/currInfo.js';
let formId = 'cashflow';
let tableId = 'cfinner';
let pagecode = '2002cashflow';
let { setDefData } = cardCache;
export default function buttonClick(props, key, record, index) {
	let { selectedIndex, sendData } = this.props;
	let { cashflow, currtype, innercust, rate, indexTable } = this.state.imput;	
	let cloneNewLine = deepClone(this.state.cashdetails);
	let newclone = cloneNewLine.filter(function(v, i, a) {
		//分录行过滤数据
		return (
			(v.localcreditamount.value && v.localcreditamount.value != '0') ||
			(v.localdebitamount.value && v.localdebitamount.value != '0') ||
			(v.quantity && v.quantity.value && v.quantity.value != '0')||
			(v.pk_accasoa && v.pk_accasoa.value)
		);
	});
	for (let i = 1; i <= newclone.length; i++) {
		newclone[i - 1].detailindex = { value: i.toString() };
	}
	sendData.details = newclone;
	switch (key) {
		case 'load':
			//载入
			let cloneVoucher = deepClone(sendData);
			let rows = props.editTable.getAllRows(tableId);
			let allrows = {};
			for (let i = 0; i < rows.length; i++) {
				let row = rows[i];
				row.values.detailindex=row.values.detailno;
				allrows[row.values.detailno.value] = []
				allrows[row.values.detailno.value].push(row.values);
			}
			let details=cloneVoucher.details;
			for (let i = 0; i < details.length; i++) {
				details[i].cashflow=allrows[(i+1).toString()];
			}
			ajax({
				url: '/nccloud/gl/cashflow/loadcf.do',
				data: cloneVoucher,
				success: function(response) {
					let { data, message, success } = response;
					if (data && data.length > 0) {
						let datarows=[];
						for(let j=0;j<data.length;j++){
							data[j].detailno=data[j].detailindex;
							datarows.push({values:data[j]});
							if (data[j].pk_innercorp.value && data[j].pk_innercorp.value != '') {
								setDefData(newclone[data[j].detailno.value - 1].assid.value, 'innserCorpDataSource', data[j].pk_innercorp);
							}
						// 	if(!allrows.hasOwnProperty(data[j].detailindex.value)){
						// 		data[j].detailno=data[j].detailindex;
						// 		props.editTable.addRow(tableId, undefined, false,data[j])
						// 		if(data[j].pk_innercorp.value&&data[j].pk_innercorp.value!=''){
						// 			setDefData(newclone[data[j].detailno.value-1].assid.value,'innserCorpDataSource',data[j].pk_innercorp);
						// 		}
						// 	}
						}
						props.editTable.setTableData(tableId,{rows:datarows})
					}
				}
			});
			break;

		case 'analysis':
			//分析
			if(sendData&&sendData.details&&sendData.details.length>0){
				let newData=JSON.parse(JSON.stringify(sendData))
				//将预算科目去掉
				let details=sendData.details;
				let newDetails=[]
				for(let i=0;i<details.length;i++){
					if(details[i].pk_accasoa.accproperty==1||(details[i].accsubjaccproperty&&details[i].accsubjaccproperty.value=='1')){
						//预算科目去掉
					}else{
						newDetails.push(details[i]);
					}
				}
				if(newDetails.length==0){
					return;
				}
				newData.details=newDetails;
				let analysistype = props.form.getFormItemsValue(formId, 'analysistype').value;
				if(analysistype){
					analysistype=analysistype.replace(';','');
				}
				ajax({
					url: '/nccloud/gl/cashflow/analysiscf.do',
					data: {
						analysistype: analysistype == ' ' ? '' : analysistype,
						voucher: newData,
						selectedIndex: selectedIndex ? selectedIndex.toString() : '0'
					},
					success: function(response) {
						let { data, message, success } = response;
						if (data && data.length > 0) {
							data = data.map((item, i) => {
								let localamount=amountconvert({
									localamount:item.localamount.value,
									pk_currtype:item.pk_currtype.value,
									pk_accountingbook:sendData.pk_accountingbook.value,
									prepareddate:sendData.prepareddate.value
								},{},'localamount');
								if (!item.occamount||!item.occamount.value) {
									item.occamount={
										display:localamount.amount,
										value:localamount.amount,
										scale:item.occamount.scale
									}
								}
								if(!item.groupamount||!item.groupamount.value){
									item.groupamount={
										display:localamount.groupamount,
										value:localamount.groupamount,
										scale:item.groupamount.scale
									}
								}
								if(!item.globalamount||!item.globalamount.value){
									item.globalamount={
										display:localamount.globalamount,
										value:localamount.globalamount,
										scale:item.globalamount.scale
									}
								}
								
								item.detailno = item.detailindex;
								if(item.pk_innercorp.value&&item.pk_innercorp.value!=''){
									setDefData(newclone[item.detailno.value-1].assid.value,'innserCorpDataSource',item.pk_innercorp);
								}
								return Object.assign({}, { values: item });
							});
							props.editTable.setTableData(tableId, { rows: data }, false);
						} else {
							props.editTable.setTableData(tableId, { rows: [] }, false);
						}
					}
				});
			}else{
				props.editTable.setTableData(tableId, { rows: [] }, false);
			}
			break;
		case 'imputation':
			//归集
			indexTable=[];
			if (index||index==0) {
				//操作列归集
				indexTable.push(index);
				currtype = record.values.pk_currtype;
			} else {
				let getRows = props.editTable.getCheckedRows(tableId);
				if (getRows.length != 0) {
					getRows.map((item, i) => {
						indexTable.push(item.index);
					});
					currtype = getRows[0].data.values.pk_currtype;
				} else {
					toast({ content: this.state.json['20021005card-000011'], color: 'warning' });/* 国际化处理： 请选择归集项！*/
					return;
				}
			}
			this.setState(
				{
					imput: {
						...this.state.imput,
						currtype,
						indexTable
					},
				},
				() => {
					props.modal.show('imputModal');
				}
			);
			break;
		case 'add':
			//增行
			props.editTable.addRow(tableId, undefined, true, undefined);
			break;

		case 'delete':
			//删除
			props.editTable.deleteTableRowsByIndex(tableId, index, true);
			break;
	}
}

