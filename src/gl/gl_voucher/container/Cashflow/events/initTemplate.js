import { deepClone,ajax} from 'nc-lightapp-front';
import buttonClick from './buttonClick';
import {accAdd} from '../../../../public/common/method.js';
import setScale from '../../../../public/common/amountFormat.js';

let formId='cashflow';
let tableId = 'cfinner';
let pageId = '2002cashflow';
export default function (props) { 
	let self=this;
    props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: '20020PREPA'//小应用id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(self,props, meta);
					
					let { sendData,cashflowType }=props;
					props.meta.setMeta(meta);
					setDefaultValue(props,self.state,self);

					if(!cashflowType){//通卡片状态一样
						props.editTable.setStatus(tableId,'edit');
						props.button.setButtonDisabled(['analysis','load','imputation','add'],false)
					}else{
						props.editTable.setStatus(tableId,'browse');
						props.button.setButtonDisabled(['analysis','load','imputation','add'],true)
					}
					
					//props.editTable.hideColByKey(tableId,"pk_unit")
					if(sendData.paraInfo&&!sendData.paraInfo.isShowUnit){
						props.editTable.hideColByKey(tableId,"pk_unit")
					}
					if(sendData.paraInfo&&!sendData.paraInfo.NC001){
						props.editTable.hideColByKey(tableId,"groupamount")
					}
					if(sendData.paraInfo&&!sendData.paraInfo.NC002){
						props.editTable.hideColByKey(tableId,"globalamount")
					}
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					//props.button.setButtonDisabled(['analysis','load','imputation','add'],true)
				}
			}
		}
    )
}

function modifierMeta(self,props, meta) {
    meta[formId].status='edit';
    meta[tableId].status='browse';
    
	meta[tableId].items.push({
		label: self.state.json['20021005card-000012'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: '150px',
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let status = props.editTable.getStatus(tableId);
			let buttonAry = status === 'browse' ?
				[]
				:
				['imputation','delete'];//归集、删除
			return props.button.createOprationButton(buttonAry, {
				area: tableId,
				buttonLimit: 3,
				onButtonClick: (props, key) => buttonClick.call(self,props, key, record, index)
			});
		}
	});
    return meta;
}

function setDefaultValue(props,state,self){
	let paraInfo=props.sendData.paraInfo;
	let orgscale=paraInfo.orgscale-0;
	let details=state.cashdetails;
	
	//let details=voucher.cashdetails;
	if(details&&details.length>0){
		let amount=0;	
		let cashflowArr={};	
		let pk_accasoas=[]
		for(let i=0;i<details.length;i++){
			if(details[i].pk_accasoa&&(!details[i].pk_accasoa.cashtype||details[i].pk_accasoa.cashtype=='')){
				pk_accasoas.push(details[i].pk_accasoa.value);
			}
		}
		if(pk_accasoas.length>0){
			ajax({
				url:'/nccloud/gl/voucher/cashtype.do',
				data:{
					pk_accasoas:pk_accasoas,
					date:props.sendData.prepareddate.value,
				},
				async: false,
				success:function(response) {
					if(response.data){
						for(let i=0;i<details.length;i++){
							if(details[i].pk_accasoa&&(!details[i].pk_accasoa.cashtype||details[i].pk_accasoa.cashtype=='')){
								details[i].pk_accasoa.cashtype=response.data[details[i].pk_accasoa.value];
							}
						}
					}
				}
			});
		}
		for(let i=0;i<details.length;i++){
			//如果是现金类科目要计算合计
			if(details[i].pk_accasoa&&(details[i].pk_accasoa.cashtype=='1')||details[i].pk_accasoa.cashtype=='2'||details[i].pk_accasoa.cashtype=='3'){
				if(details[i].localdebitamount.value||details[i].localcreditamount.value){
					if(details[i].localdebitamount.value&&details[i].localdebitamount.value!=''&&(details[i].localdebitamount.value-0)!='0'){
						//amount+=details[i].localdebitamount.value*1
						amount=accAdd(amount,details[i].localdebitamount.value*1);
					}else{
						//amount+=details[i].localcreditamount.value*-1
						amount=accAdd(amount,details[i].localcreditamount.value*-1);
					}
				}
			}
			let cashflow=details[i].cashflow;
			if(cashflow&&cashflow.length>0){
				cashflow.map((cash) => {
					let key=cash.detailindex.value+cash.occamount.value+cash.localamount.value+cash.pk_currtype.value;
					let cashKey=Number(cash.detailindex.value-1)
					if(!cashflowArr.hasOwnProperty(key)){
						cashflowArr[key]=[];
					}
					if(details[cashKey]&&details[cashKey].localcreditamount&&details[cashKey].localcreditamount.value){
						cash.direct.value='贷'/*-=notranslate=-*/;
						cash.direct.display=self.state.json['20021005card-000010'];/* 国际化处理： 贷*/
					}else{
						cash.direct.value='借'/*-=notranslate=-*/;
						cash.direct.display=self.state.json['20021005card-000009'];/* 国际化处理： 借*/
					}
					cashflowArr[key].push(cash);
				});
			}
		}
		let str='';
		if(amount>=0){
			str=self.state.json['20021005card-000013']+setScale(amount,orgscale);/* 国际化处理： 借方 */
		}else{
			str=self.state.json['20021005card-000014']+setScale(amount*-1,orgscale);/* 国际化处理： 贷方 */
		}
		props.form.setFormItemsValue(formId,{amount:{value:str,display:str}});

		//现金流量主表项、附表项合并
		//此处有个bug，如果分录+原币+组织本币+币种相同，有主表项附表项多行的情况下只会显示一行，由于重量端也是如此，bug暂不处理
		let cashflowData=[];
		for (let key in cashflowArr) {
			let cashflow=deepClone(cashflowArr[key][0]);
			let pk_cashflow_main={};
			let pk_cashflow_ass={};
			cashflowArr[key].map((cash, i) => {
				if (cash.pk_cashflow_main.value != '') {
					pk_cashflow_main = cash.pk_cashflow_main
				}
				if (cash.pk_cashflow_ass.value != '') {
					pk_cashflow_ass = cash.pk_cashflow_ass
				}
			});
			cashflow.pk_cashflow_main=pk_cashflow_main;
			cashflow.pk_cashflow_ass=pk_cashflow_ass;
			cashflow.detailno=cashflow.detailindex;
			cashflowData.push({values:cashflow});
		}
		props.editTable.setTableData(tableId, {rows:cashflowData})

	}
}
