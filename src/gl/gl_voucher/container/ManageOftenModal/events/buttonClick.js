import { createPage, ajax, base, toast, cacheTools, deepClone } from 'nc-lightapp-front';

let tableId = 'commontable';
let pageId = '2002template';
export default function buttonClick(props, key, record, index) {

	switch (key) {
		case 'move':
		//gl.voucher.moveToSort
			let { moveList, classData, moverow } = this.state;
			let pk_commnvclass = record.values.pk_commnvclass.value;
			moverow.commonsummary = record.values.commonsummary.value;
			moverow.commoncode = record.values.commoncode.value;
			moverow.pk_commonvoucher = record.values.pk_commonvoucher.value;
			moveList = classData.filter((v, i, a) => {
				return v.pk_commnvclass != pk_commnvclass;
			});
			this.setState(
				{
					moveList,
					moverow
				},
				() => {
					props.modal.show('moveModal');
				}
			);

			break;
		case 'cover':
			//覆盖
			cover.call(this,props, record, index);
			break;

		case 'delete':
			//删除
			//gl.voucher.commvoudelete
			deleteFunc.call(this,props,record,index)
			break;
		case 'edit':
			let { rowKey, editrow } = this.state;
			editrow.pk_commnvclass = record.values.pk_commnvclass.value;
			editrow.commonsummary = record.values.commonsummary.value;
			editrow.commoncode = record.values.commoncode.value;
			editrow.pk_commonvoucher=record.values.pk_commonvoucher.value;
			this.setState(
				{
					rowKey: record.rowid,
					editrow
				},
				() => {
					props.modal.show('editTable');
				}
			);
			break;
	}
}

function deleteFunc(props,record,index) {
	let self = this;
	let url = '/nccloud/gl/voucher/commvoudelete.do';
	let data={
		pk_commonvoucher:record.values.pk_commonvoucher.value
	}
	ajax({
		url,data,
		success:(res)=>{
			const { data, success } = res;
			if (success) {
				toast({ content: self.state.json['20021005card-000081'], color: 'success' });/* 国际化处理： 删除成功*/
				props.editTable.deleteTableRowsByIndex(tableId, index);
			}
		}
	})
}



function cover(props, record, index) {
	let self = this;
	let url = '/nccloud/gl/voucher/commvouconvert.do';
	let data = deepClone(props.sendData);
	data.commoncode = {
		display: '',
		value: record.values.commoncode.value
	};
	data.commonsummary = {
		display: '',
		value: record.values.commonsummary.value
	};
	data.pk_commnvclass = {
		display: '',
		value: record.values.pk_commnvclass.value
	};
	delete data.pk_commonvoucher;
	let json = {
		newvo: data,
		oldpk: record.values.pk_commonvoucher.value
	};
	ajax({
		url,
		data: json,
		success: function(response) {
			const { data, success } = response;
			if (success) {
				//覆盖后更新pk
				props.editTable.setValByKeyAndIndex(tableId,index,'pk_commonvoucher',data.pk_commonvoucher)
				toast({ content: self.state.json['20021005card-000127'], color: 'success' });/* 国际化处理： 覆盖完成*/
			}
		}
	});
}
