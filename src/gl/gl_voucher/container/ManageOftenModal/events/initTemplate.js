import { createPage, ajax, base, toast, cacheTools, deepClone } from 'nc-lightapp-front';
import buttonClick from './buttonClick';
let { NCPopconfirm, NCIcon } = base;

let tableId = 'commontable';
let pageId = '2002template';
export default function(props) {
	let self = this;
	props.createUIDom(
		{
			pagecode: pageId, //页面id
			appcode: '20020PREPA' //小应用id
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(self, props, meta);

					let { sendData } = props;
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('delete', self.state.json['20021005card-000128']);/* 国际化处理： 确认要删除吗？*/
					props.button.setPopContent('cover', self.state.json['20021005card-000129']);/* 国际化处理： 确认要覆盖吗？*/
				}
			}
		}
	);
}

function modifierMeta(self, props, meta) {
	// meta[tableId].status='browse';

	// meta[tableId].items = meta[tableId].items.map((item, key) => {
	//     //item.width = 150;
	//     //设置主表项和附表项的参照过滤

	// 	return item;
	// });

	meta[tableId].items.push({
		label: self.state.json['20021005card-000012'],/* 国际化处理： 操作*/
		itemtype: 'customer',
		attrcode: 'opr',
		width: '150px',
		visible: true,
		fixed: 'right',
		render: (text, record, index) => {
			let status = props.editTable.getStatus(tableId);
			let buttonAry = status === 'browse' ? [ 'move', 'cover', 'delete', 'edit' ] : []; //归集、删除//move 移动 cover覆盖
			return props.button.createOprationButton(buttonAry, {
				area: tableId,
				//buttonLimit: 4,
				onButtonClick: (props, key) => buttonClick.call(self, props, key, record, index)
			});
		}
	});
	return meta;
}
