import { createPage, ajax, base, toast, cacheTools } from 'nc-lightapp-front';
let tableId = 'cfinner';
export default function afterEvent(props, tableid, key, val, changedrows, index, record, type) {
	switch (key) {
		case 'detailno':
			//将对应行的 业务单元、币种、方向设置到行上
			let details = props.sendData.details[val - 1];
			// let  localArr=[];
			//localArr.push(Object.assign({},{values:details}))
			let ocValue = countAmountOcc(props, tableid, index, val, key, record, details); //原币计算

			let groupValue = countAmountGroup(props, tableid, index, val, key, record, details); //集团

			let globalValue = countAmountGlobal(props, tableid, index, val, key, record, details);

			props.editTable.setValByKeyAndIndex(tableId, val, 'pk_currtype', {
				//币种
				value: details.pk_currtype.value,
				display: details.pk_currtype.display
			});
			props.editTable.setValByKeyAndIndex(tableId, val, 'localamount', {
				//本币
				value: details.amount.value,
				display: details.amount.value
			});
			props.editTable.setValByKeyAndIndex(tableId, val, 'pk_unit', {
				//业务单元
				value: details.pk_unit.value,
				display: details.pk_unit.display
			});
			props.editTable.setValByKeyAndIndex(tableId, val, 'pk_accasoa', {
				//会计科目
				value: details.pk_accasoa.value,
				display: details.pk_accasoa.display
			});
			props.editTable.setValByKeyAndIndex(tableId, val, 'assid', {
				//辅助核算
				value: details.assid.value,
				display: details.assid.display
			});
			props.editTable.setValByKeyAndIndex(tableId, val, 'pk_innercorp', {
				//内部单位
				value: details.innercust ? details.innercust.value : '',
				display: details.innercust ? details.innercust.display : ''
			});
			props.editTable.setValByKeyAndIndex(tableId, val, 'direct', {
				//方向
				value: details.direction.value == 'D' ? this.state.json['20021005card-000009'] : this.state.json['20021005card-000010'],/* 国际化处理： 借,贷*/
				display: details.direction.value == 'D' ? this.state.json['20021005card-000009'] : this.state.json['20021005card-000010']/* 国际化处理： 借,贷*/
			});
			props.editTable.setValByKeyAndIndex(tableId, val, 'occamount', {
				//原币
				value: ocValue,
				display: ocValue
			});
			props.editTable.setValByKeyAndIndex(tableId, val, 'localamount', {
				//原币
				value: ocValue,
				display: ocValue
			});
			props.editTable.setValByKeyAndIndex(tableId, val, 'groupamount', {
				//集团
				value: groupValue,
				display: groupValue
			});
			props.editTable.setValByKeyAndIndex(tableId, val, 'globalamount', {
				//全局
				value: globalValue,
				display: globalValue
			});
			// pk_cashflow_main=主表项
			// pk_cashflow_ass=附表项
			// occamount=原币
			// localamount=本币
			// groupamount=集团本币
			// globalamount=全局本币

			//带选项

			//  props.editTable.setTableData(tableId, {rows:localArr})
			break;
		case 'pk_cashflow_main':
			//主表项
			//现金流入在借方 金额为负 在贷方为正
			//现金流出在借方 金额为正 在贷方为负
			ajax({
				url: '/nccloud/gl/glpub/cfitemquery.do',
				data: { pk_cashflow_main: val.refpk },
				success: function(response) {
					let { data, success } = response;
					if (data) {
						if (data.itemtype == '1') {
							//现金流入
							if (record.values.direct.value == '借' /*-=notranslate=-*/) {
								if (record.values.occamount.value != '' && record.values.occamount.value - 0 > 0) {
									setAmountOpp(props, tableid, index, key, record);
								}
							} else {
								if (record.values.occamount.value != '' && record.values.occamount.value - 0 < 0) {
									setAmountOpp(props, tableid, index, key, record);
								}
							}
						} else if (data.itemtype == '2') {
							//现金流出
							if (record.values.direct.value == '借' /*-=notranslate=-*/) {
								if (record.values.occamount.value != '' && record.values.occamount.value - 0 < 0) {
									setAmountOpp(props, tableid, index, key, record);
								}
							} else {
								if (record.values.occamount.value != '' && record.values.occamount.value - 0 > 0) {
									setAmountOpp(props, tableid, index, key, record);
								}
							}
						}
					}
				}
			});
			let pk_cashflow_main = {
				value: val.refpk,
				display: val.refcode + '/' + val.refname,
				isEdit: false
			};
			props.editTable.setValByKeyAndIndex(tableid, index, key, pk_cashflow_main);
			break;
		case 'pk_cashflow_ass':
			let pk_cashflow_ass = {
				value: val.refpk,
				display: val.refcode + '/' + val.refname,
				isEdit: false
			};
			props.editTable.setValByKeyAndIndex(tableid, index, key, pk_cashflow_ass);
			break;
		case 'pk_currtype':
			//币种
			break;

		case 'occamount':
			countAmount(props, tableid, index, val, key, record);
			break;
		//金额换算
		default:
			break;
	}
}

function countAmount(props, tableid, index, val, key, record) {
	let arr = props.sendData.details;
	let pk_accountingbook = ''; //财务核算账簿
	let data = ''; //日期
	for (let i = 0; i < arr.length; i++) {
		if (record.values.detailindex.value == arr[i].detailindex.value) {
			pk_accountingbook = arr[i].pk_accountingbook.value;
			data = arr[i].prepareddate.value;
		}
	}
	let body = { pk_accountingbook: pk_accountingbook, pk_currtype: record.values.pk_currtype.value, date: data };
	let url = '/nccloud/gl/glpub/ratequery.do';
	ajax({
		url,
		data: body,
		success: function(response) {
			let { NC001, NC002, excrate2, excrate3, excrate4 } = response.data;
			props.editTable.setValByKeyAndIndex(tableId, index, 'localamount', {
				value: props.editTable.getValByKeyAndIndex(tableId, index, 'localamount').value * excrate2,
				display: props.editTable.getValByKeyAndIndex(tableId, index, 'localamount').value * excrate2
			});
			props.editTable.setValByKeyAndIndex(tableId, index, 'globalamount', {
				value: props.editTable.getValByKeyAndIndex(tableId, index, 'globalamount').value * excrate4,
				display: props.editTable.getValByKeyAndIndex(tableId, index, 'globalamount').value * excrate4
			});
			props.editTable.setValByKeyAndIndex(tableId, index, 'groupamount', {
				value: props.editTable.getValByKeyAndIndex(tableId, index, 'groupamount').value * excrate3,
				display: props.editTable.getValByKeyAndIndex(tableId, index, 'groupamount').value * excrate3
			});
		}
	});
}

function countAmountOcc(props, tableid, index, val, key, record, details) {
	let totleData = props.editTable.getAllData(tableid).rows;
	totleData.pop();
	let finalValue = '';
	totleData.map((item, i) => {
		if (item.values.detailindex.value == val) {
			finalValue = details.amount.value - item.values.occamount.value;
		} else {
			finalValue = details.amount.value;
		}
	});
	return finalValue;
}

function countAmountGlobal(props, tableid, index, val, key, record, details) {
	let totleData = props.editTable.getAllData(tableid).rows;
	totleData.pop();
	let finalValue = '';
	totleData.map((item, i) => {
		if (details.direction.value == 'D') {
			if (item.values.detailindex.value == val) {
				finalValue = details.globaldebitamount.value - item.values.globalamount.value;
			} else {
				finalValue = details.globaldebitamount.value;
			}
		} else {
			if (item.values.detailindex.value == val) {
				finalValue = details.globalcreditamount.value - item.values.globalamount.value;
			} else {
				finalValue = details.globalcreditamount.value;
			}
		}
	});
	return finalValue;
}

function countAmountGroup(props, tableid, index, val, key, record, details) {
	let totleData = props.editTable.getAllData(tableid).rows;
	totleData.pop();
	let finalValue = '';
	totleData.map((item, i) => {
		if (details.direction.value == 'D') {
			if (item.values.detailindex.value == val) {
				finalValue = details.groupdebitamount.value - item.values.groupamount.value;
			} else {
				finalValue = details.groupdebitamount.value;
			}
		} else {
			if (item.values.detailindex.value == val) {
				finalValue = details.groupcreditamount.value - item.values.groupamount.value;
			} else {
				finalValue = details.groupcreditamount.value;
			}
		}
	});
	return finalValue;
}

function setAmountOpp(props, tableid, index, key, record) {
	//设置金额为相反数
	if (record.values.occamount.value != '') {
		let occamount = {
			value: record.values.occamount.value * -1,
			display: record.values.occamount.value * -1,
			isEdit: false
		};
		props.editTable.setValByKeyAndIndex(tableid, index, 'occamount', occamount);
	}
	if (record.values.localamount.value != '') {
		let localamount = {
			value: record.values.localamount.value * -1,
			display: record.values.localamount.value * -1,
			isEdit: false
		};
		props.editTable.setValByKeyAndIndex(tableid, index, 'localamount', localamount);
	}
	if (record.values.groupamount.value != '') {
		let groupamount = {
			value: record.values.groupamount.value * -1,
			display: record.values.groupamount.value * -1,
			isEdit: false
		};
		props.editTable.setValByKeyAndIndex(tableid, index, 'groupamount', groupamount);
	}
	if (record.values.globalamount.value != '') {
		let globalamount = {
			value: record.values.globalamount.value * -1,
			display: record.values.globalamount.value * -1,
			isEdit: false
		};
		props.editTable.setValByKeyAndIndex(tableid, index, 'globalamount', globalamount);
	}
}
