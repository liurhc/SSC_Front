import React, { Component } from 'react';
import { high, base, ajax, deepClone, toast, createPage, promptBox,getMultiLang} from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCModal: Modal,
	NCFormGroup: FormGroup,
	NCForm: Form
} = base;
const { Refer } = high;
import { buttonClick, initTemplate, afterEvent } from './events';
//import {tableId} from '../../../public/components/constJSON'
import './index.less';
//import { toASCII } from 'punycode';
const { NCFormItem: FormItem } = Form;

const tableId = 'commontable';

class ManagModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			classData: [], //请求返回的分类列表数据
			moveList: [], //移动列表
			editClass: {}, //编辑分类标识
			addClass: {
				name: '',
				prop: 'COMMON'
			},
			rowKey: '', //修改行标识
			movePk: '', //移动目标pk
			saveDataClone: {}, //保存接口的凭证数据
			pk_accountingbook: '', //核算账簿主键
			addrow: {
				//新增模板数据
				commonsummary: '',
				commoncode: '',
				pk_commnvclass: ''
			},
			editrow: {
				//新增模板数据
				commonsummary: '',
				commoncode: '',
				pk_commnvclass: '',
				pk_commonvoucher: ''
			},
			moverow: {
				//移动模板数据
				commonsummary: '',
				commoncode: '',
				pk_commnvclass: '',
				pk_commonvoucher: ''
			},
			json:{},
			selectedCurrValue: 'COMMON', //对公对私 单选
			selected_Class_Pk: '' // 选中的模板分类 pk
		};
		
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				initTemplate.call(this, this.props);
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}

	//查询分类
	queryClass(book, saveDataClone) {
		if (saveDataClone) {
			this.setState({
				saveDataClone: saveDataClone
			});
		}
	}

	componentWillReceiveProps(nextProps) {
		let self = this;
		if (nextProps.pk_acc && nextProps.pk_acc !== self.state.pk_accountingbook) {
			self.setState({
				saveDataClone: nextProps.sendData,
				pk_accountingbook: nextProps.pk_acc
			});
			// if (nextProps.sendData) {
			// 	//传入凭证保存数据
			// 	self.setState({
			// 		saveDataClone: nextProps.sendData
			// 	});
			// }
		}
	}

	// shouldComponentUpdate (nextProps,nextState){
	// 	if(this.state.classData.length!=nextState.classData.length){
	// 		return true;
	// 	}
	// 	if(nextProps.show!=this.props.show){
	// 		return true
	// 	}
	// 	if(nextState.innerModalshow!=this.state.innerModalshow){
	// 		return true
	// 	}
	// 	if(this.state.addClass.name){
	// 		return true
	// 	}
	// 	return false;
	// }

	componentDidMount() {
		let self = this;
		let url = '/nccloud/gl/voucher/commclassquery.do';
		let dataload = {
			pk_accountingbook: this.props.pk_acc
		};
		ajax({
			url,
			data: dataload,
			success: function(response) {
				let { data } = response;
				if (data&&data.length!='0') {
					self.setState({
						classData: data
					},()=>{
						self.classItemClick.call(self,data[0].pk_commnvclass)
					});
				}
			}
		});
	}
	//新增分类
	onAddClass = () => {
		this.props.modal.show('classModal');
	};

	//新增分类确认按钮
	addBtnClick = () => {
		let self = this;
		let { pk_accountingbook, addClass, selectedCurrValue } = self.state;
		if (addClass.name) {
			let url = '/nccloud/gl/voucher/commclasssave.do';
			let data = {
				pk_accountingbook: pk_accountingbook,
				name: addClass.name,
				property: selectedCurrValue
			};
			ajax({
				url,
				data,
				success: function(res) {
					if (res.success) {
						let { classData } = self.state;
						if (res.data.property == 'COMMON') {
							res.data.name = res.data.name + self.state.json['20021005card-000130'];/* 国际化处理： (公用)*/
						} else {
							res.data.name = res.data.name + self.state.json['20021005card-000131'];/* 国际化处理： (私用)*/
						}
						classData.push(res.data);
						self.setState({
							classData
						},()=>{
							self.classItemClick.call(self,res.data.pk_commnvclass)
						});
						self.props.modal.close('classModal');
					}
				}
			});
		} else {
			toast({ content: self.state.json['20021005card-000132'], color: "danger" });/* 国际化处理： 请输入分类名称*/
		}
	};

	//编辑图标确认
	editBtnClick = () => {
		let self = this;
		let { editClass, pk_accountingbook, classData, selectedCurrValue } = self.state;
		let url = '/nccloud/gl/voucher/commclasssave.do';
		editClass.pk_accountingbook = pk_accountingbook;
		editClass.name = editClass.name.split('(')[0];
		editClass.property = selectedCurrValue;
		ajax({
			url,
			data: editClass,
			success: (res) => {
				if (res.success) {
					classData.map((item, i) => {
						if (item.pk_commnvclass == editClass.pk_commnvclass) {
							if (res.data.property == 'COMMON') {
								res.data.name = res.data.name + this.state.json['20021005card-000130'];/* 国际化处理： (公用)*/
							} else {
								res.data.name = res.data.name + this.state.json['20021005card-000131'];/* 国际化处理： (私用)*/
							}
							item.name = res.data.name;
						}
					});
					self.setState({
						classData
					});
				}
			}
		});
	};

	//点击分类编辑图标

	onClassEdit = (item, org) => {
		let { editClass } = this.state;
		let neworg = Object.assign({}, org);
		editClass = neworg;
		this.setState(
			{
				editClass
			},
			() => {
				this.props.modal.show('editModal');
			}
		);
	};

	// 点击分类删除图标
	onClassDel(item, org) {
		let self = this;
		let url = '/nccloud/gl/voucher/commclassdelete.do';
		ajax({
			url,
			data: org,
			success: function(response) {
				let { classData } = self.state;
				if (response.success) {
					toast({ content: self.state.json['20021005card-000081'], color: 'success' });/* 国际化处理： 删除成功*/
					let { classData } = self.state;
					classData = classData.filter((v, i, a) => {
						return v.pk_commnvclass != org.pk_commnvclass;
					});
					self.setState({
						classData
					},()=>{
						self.classItemClick.call(self,classData&&classData.length!='0'&&classData[0].pk_commnvclass)
					});
				}
			}
		});
	}

	//新增模板确定
	sureClick = () => {
		let self = this;
		let { addrow } = self.state;
		let url = '/nccloud/gl/voucher/commvousave.do';
		let data = deepClone(self.props.sendData);
		data.commoncode = {
			display: '',
			value: addrow.commoncode
		};
		data.commonsummary = {
			display: '',
			value: addrow.commonsummary
		};
		data.pk_commnvclass = {
			display: '',
			value: addrow.pk_commnvclass
		};
		delete data.pk_commonvoucher;
		ajax({
			url,
			data,
			success: function(response) {
				const { data, success } = response;
				if (success) {
					toast({ content: self.state.json['20021005card-000133'], color: 'success' });/* 国际化处理： 新增模板保存成功*/
					self.props.editTable.addRow(tableId, undefined, false, {
						commoncode: { display: addrow.commoncode, value: addrow.commoncode },
						commonsummary: { display: addrow.commonsummary, value: addrow.commonsummary },
						pk_commnvclass: { display: addrow.pk_commnvclass, value: addrow.pk_commnvclass },
						pk_commonvoucher: { display: data.pk_commonvoucher.display, value: data.pk_commonvoucher.value }
					});
					self.props.editTable.setStatus(tableId, 'browse');
					self.props.modal.close('addTable');
				}
			}
		});
	};

	moveClick = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/commvouupdate.do';
		let { pk_accountingbook, movePk, moverow } = self.state;
		if(movePk){
			let data = deepClone(self.props.sendData);
			data.commoncode = {
				display: '',
				value: moverow.commoncode
			};
			data.commonsummary = {
				display: '',
				value: moverow.commonsummary
			};
			data.pk_commnvclass = {
				display: '',
				value: movePk
			};
			data.pk_commonvoucher = {
				display: '',
				value: moverow.pk_commonvoucher
			};
			//	delete data.pk_commonvoucher;
			ajax({
				url,
				data,
				success: (res) => {
					if (res.success) {
						toast({ content: self.state.json['20021005card-000134'], color: 'success' });/* 国际化处理： 移动成功*/
						//	self.props.editTable.deleteTableRowsByIndex(tableId, index);
					}
				}
			});
		}
	};
	handleRadioCurrChange = (v) => {
		let { selectedCurrValue } = this.state;
		selectedCurrValue = v;
		this.setState({
			selectedCurrValue
		});
	};

	//分类行点击事件
	classItemClick = (pk_commnvclass) => {
		this.setState({
			selected_Class_Pk: pk_commnvclass
		});
		let self = this;
		let { addrow } = self.state;
		let url = '/nccloud/gl/voucher/commvouquery.do';
		let data = {
			pk_commnvclass: pk_commnvclass
		};
		addrow.pk_commnvclass = pk_commnvclass;
		ajax({
			url,
			data,
			success: (res) => {
				let setTable = [];
				res.data.map((item, i) => {
					let obj = {};
					obj.commoncode = item.commoncode;
					obj.commonsummary = item.commonsummary;
					obj.pk_commnvclass = item.pk_commnvclass;
					obj.pk_commonvoucher = item.pk_commonvoucher;
					setTable.push(obj);
				});
				setTable = setTable.map((item, i) => {
					return Object.assign({}, { values: item });
				});
				self.props.editTable.setTableData(tableId, { rows: setTable });
				//self.props.editTable.setStatus(tableId,'edit');
			}
		});
		self.setState({
			addrow
		});
	};
	//移动选中行事件
	checkMove = (placc, e) => {
		let { movePk } = this.state;
		e.target.parentNode.childNodes.forEach((item) => {
			item.style.color = '#666666';
		});
		e.target.style.color = '#ff0000';
		movePk = placc;
		this.setState({
			movePk
		});
	};

	//修改行确定事件
	sureEditClick = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/commvouupdate.do';
		let { pk_accountingbook, movePk, editrow, rowKey } = self.state;
		let data = deepClone(self.props.sendData);
		data.commoncode = {
			display: '',
			value: editrow.commoncode
		};
		data.commonsummary = {
			display: '',
			value: editrow.commonsummary
		};
		data.pk_commnvclass = {
			display: '',
			value: editrow.pk_commnvclass
		};
		data.pk_commonvoucher = {
			display: '',
			value: editrow.pk_commonvoucher
		};
		//	delete data.pk_commonvoucher;
		ajax({
			url,
			data,
			success: (res) => {
				if (res.success) {
					toast({ content: self.state.json['20021005card-000135'], color: 'success' });/* 国际化处理： 修改成功*/
					self.props.editTable.setValByKeyAndRowId(tableId, rowKey, 'commoncode', {
						display: editrow.commoncode,
						value: editrow.commoncode
					});
					self.props.editTable.setValByKeyAndRowId(tableId, rowKey, 'commonsummary', {
						display: editrow.commonsummary,
						value: editrow.commonsummary
					});
				}
			}
		});
	};

	loadRows = () => {
		let self = this;
		let { classData } = self.state;
		let { editTable, button, form, modal } = self.props;
		let { createEditTable } = editTable;
		return (
			<div className="often-modal">
				<div className="often-modal-left nc-theme-area-split-bc">
					<div className="left-header">
						<div onClick={this.onAddClass.bind(this)} className="btn-add-class nc-theme-btn-secondary">
							{this.state.json['20021005card-000137']}
						</div>
					</div>
					<div className="left-body nc-theme-area-split-bc">
						{/* <div className="classOuter">
							<span className="classTip">模板分类</span> 
							<span onClick={(e) => this.onClassAdd('a', e)}><Icon type="uf-add-s-o" /></span>
							<span onClick={(e) => this.onClassDel('a', e)}><Icon type="uf-reduce-s-o" /></span>
							<span onClick={(e) => this.onClassEdit('a', e)}><Icon type="uf-pencil-s" /></span>
							<span onClick={(e) => this.onClassFresh('a', e)}><Icon type="uf-list-s-o" /></span> */}
						{/* </div> */}
						{/* <div className="class-title">模板分类</div> */}
						{classData.length != 0 &&
							classData.map((item, index) => {
								return (
									<div
										key={item.pk_commnvclass}
										class={
											this.state.selected_Class_Pk === item.pk_commnvclass ? (
												'class-item selected-item nc-theme-tree-item-active-bgc nc-theme-bbr-bc'
											) : (
												'class-item nc-theme-bbr-bc'
											)
										}
										onClick={this.classItemClick.bind(this, item.pk_commnvclass, item.moveChild)}
									>
										<div class={this.state.selected_Class_Pk==item.pk_commnvclass?
												('class-name nc-theme-tree-item-active-c'):('class-name')}>
											{item.dispName || item.name}
										</div>
										<i
											className="iconfont icon-bianji nc-theme-icon-font-c"
											onClick={this.onClassEdit.bind(this, index, item)}
										/>
										<i
											className="iconfont icon-shanchu1 nc-theme-icon-font-c"
											onClick={() => {
												promptBox({
													color: 'warning',
													content: this.state.json['20021005card-000136'],/* 国际化处理： 是否删除分类，分类下的常用凭证模板也将被删除*/
													beSureBtnClick: this.onClassDel.bind(this, index, item)
												});
											}}
										/>
									</div>
								);
							})}
					</div>
				</div>
				<div className="often-modal-right">
					<div className="right-header">
						<span className="nc-theme-title-font-c">{this.state.json['20021005card-000143']}</span>
						<Button
							className="button-primary btn-add-template"
							disabled={classData.length!='0'?false:true}
							onClick={() => {
								this.props.modal.show('addTable');
							}}
						>
							{this.state.json['20021005card-000139']}
						</Button>
					</div>
					<div className="right-body">
						{createEditTable(tableId, {
							showIndex: false
							//	onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</div>
			</div>
		);
	};

	render() {
		let {
			show,
			modal,
			editTable,
			title,
			content,
			confirmText,
			cancelText,
			closeButton,
			icon,
			isButtonWhite,
			isButtonShow
		} = this.props;
		const { createModal } = modal;
		//let { editTable, button,form ,modal} = self.props;
		let { createEditTable } = editTable;
		let { innerModalshow, editModalshow, addClass, editClass, addrow, moveList, editrow } = this.state;
		return (
			<div id="manageModal">
				{this.loadRows()}

				{createModal('classModal', {
					title: this.state.json['20021005card-000137'],/* 国际化处理： 新增分类*/
					content: [
						<div className="modal-form-item">
							<span className="item-title"><span class='u-must'>*</span>{this.state.json['20021005card-000144']}</span>
							<FormControl
								value={addClass.name}
								onChange={(v) => {
									addClass.name = v;
									this.setState({
										addClass
									});
								}}
							/>
						</div>,
						<div className="modal-form-item">
							<span className="item-title">{this.state.json['20021005card-000145']}</span>
							<Radio.NCRadioGroup
								selectedValue={this.state.selectedCurrValue}
								onChange={this.handleRadioCurrChange.bind(this)}
							>
								<Radio value="COMMON">{this.state.json['20021005card-000146']}</Radio>
								<Radio value="PERSON">{this.state.json['20021005card-000147']}</Radio>
							</Radio.NCRadioGroup>
						</div>
					],
					className: 'smsize',
					userControl: true,
					beSureBtnClick: this.addBtnClick.bind(this),
					cancelBtnClick:()=>{
						this.props.modal.close('classModal');
					}
				})}

				{createModal('editModal', {
					title: this.state.json['20021005card-000138'],/* 国际化处理： 编辑分类*/
					content: [
						<div className="modal-form-item">
							<span className="item-title">{this.state.json['20021005card-000144']}</span>
							<FormControl
								value={editClass.name}
								onChange={(v) => {
									editClass.name = v;
									this.setState({
										editClass
									});
								}}
							/>
						</div>,
						<div className="modal-form-item">
							<span className="item-title">{this.state.json['20021005card-000145']}</span>
							<Radio.NCRadioGroup
								selectedValue={this.state.selectedCurrValue}
								onChange={this.handleRadioCurrChange.bind(this)}
							>
								<Radio value="COMMON">{this.state.json['20021005card-000146']}</Radio>
								<Radio value="PERSON">{this.state.json['20021005card-000147']}</Radio>
							</Radio.NCRadioGroup>
						</div>
					],
					className: 'smsize',
					beSureBtnClick: this.editBtnClick.bind(this)
				})}

				{createModal('addTable', {
					title: this.state.json['20021005card-000139'],/* 国际化处理： 新增模板*/
					content: [
						<div className="modal-form-item">
							<span className="item-title"><span class='u-must'>*</span>{this.state.json['20021005card-000148']}</span>
							<FormControl
								value={addrow.commoncode}
								onChange={(v) => {
									addrow.commoncode = v;
									this.setState({
										addrow
									});
								}}
							/>
						</div>,
						<div className="modal-form-item">
							<span className="item-title"><span class='u-must'>*</span>{this.state.json['20021005card-000149']}</span>
							<FormControl
								value={addrow.commonsummary}
								onChange={(v) => {
									addrow.commonsummary = v;
									this.setState({
										addrow
									});
								}}
							/>
						</div>
					],
					className: 'smsize',
					userControl: true,
					beSureBtnClick: this.sureClick.bind(this),
					cancelBtnClick:()=>{
						this.props.modal.close('addTable');
					}
				})}

				{createModal('editTable', {
					title: this.state.json['20021005card-000140'],/* 国际化处理： 修改模板*/
					content: [
						<div className="modal-form-item">
							<span className="item-title">{this.state.json['20021005card-000148']}</span>
							<FormControl
								value={editrow.commoncode}
								onChange={(v) => {
									editrow.commoncode = v;
									this.setState({
										editrow
									});
								}}
							/>
						</div>,
						<div className="modal-form-item">
							<span className="item-title">{this.state.json['20021005card-000149']}</span>
							<FormControl
								value={editrow.commonsummary}
								onChange={(v) => {
									editrow.commonsummary = v;
									this.setState({
										editrow
									});
								}}
							/>
						</div>
					],
					className: 'smsize',
					beSureBtnClick: this.sureEditClick.bind(this)
				})}

				{createModal('moveModal', {
					title: this.state.json['20021005card-000141'],/* 国际化处理： 移动至*/
					content: (
						<div>
							{moveList &&
								moveList.length != 0 &&
								moveList.map((item, index) => (
									<div
										className="modal-form-item"
										onClick={this.checkMove.bind(this, item.pk_commnvclass)}
									>
										{item.name}
									</div>
								))}
						</div>
					),
					className: 'smsize',
					beSureBtnClick: this.moveClick.bind(this)
				})}
			</div>
		);
	}
}

ManagModal = createPage({
	//initTemplate: initTemplate,
	mutiLangCode: '2002'
})(ManagModal);

export default ManagModal;
