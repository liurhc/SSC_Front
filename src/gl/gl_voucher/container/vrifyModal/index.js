import React, { Component } from 'react';
import { high, base, ajax,getMultiLang } from 'nc-lightapp-front';
import {
	CheckboxItem,
	RadioItem,
	TextAreaItem,
	ReferItem,
	SelectItem,
	InputItem,
	DateTimePickerItem
} from '../../../public/components/FormItems';
import createScript from '../../../public/components/uapRefer.js';
import Vrify from '../../../verify/verify/vrify/exportVrify.js';
const format = 'YYYY-MM-DD';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	AutoComplete,
	NCDropdown: Dropdown,
	NCPanel: Panel,
	NCModal: Modal,
	NCForm,
	NCRangePicker: RangePicker
} = base;
import './index.less';
const { NCFormItem: FormItem } = NCForm;
const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};

/* 检测类型是否为数组 */
export function isArray(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'Array';
}
export default class SearchModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loadData: [], //查询模板加载数据
			listItem: {}, //模板数据对应值
			dataRows: [], //辅助核算列表数据
			closeButton: false,
			checkedAll: false,
			checkedArray: [ false, false, false ],
			columns10: [],
			json:{}
			// pk_accountingbook:''
		};
	}


	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}
	componentWillReceiveProps(nextProp) {}

	componentDidMount() {
	}
	onRef = (ref) => {
		this.child = ref;
	};
	close=()=> {
        this.props.onCancel(false);
    }
	render() {
		let { show, sendData,rtverifyData } = this.props;
		return (

			<Modal 
				show={show} 
				onHide={ this.close }
				animation={true} 
				id="vrifyModaltwo" 
				// className={'msg-modal'}
				size='xlg'
				fieldid="vrify"
			>
				<Modal.Header closeButton  fieldid="header-area">
					<Modal.Title fieldid={`${this.state.json['20021005card-000126']}_title`}>{this.state.json['20021005card-000126']/* 国际化处理： 凭证查询*/}</Modal.Title>
					{/* <span onClick={() => {
							this.props.onCancel(false);
						}} style={{position:"absolute",right:"20px",fontSize:"16px"}}
					>x</span> */}
				</Modal.Header>
				<Modal.Body id="locationModal">
					<div className='voucherReferVerify'>
						<Vrify
							data={sendData}
							rtverifyData={rtverifyData}
							voucherVerifyflag='0'
							onRef={this.onRef}
							refreshVoucherData={this.props.refreshVoucherData}
							closeModal={(e) => {
								this.props.onCancel(false);
							}}
						/>
					</div>
				</Modal.Body>
				<Modal.Footer>
				</Modal.Footer>
			</Modal>
		);
	}
}
SearchModal.defaultProps = defaultProps12;
