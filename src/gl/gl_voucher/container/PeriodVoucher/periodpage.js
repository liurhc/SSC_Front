import React, { Component } from 'react';
import { createPage, ajax ,cardCache,getMultiLang,createPageIcon} from 'nc-lightapp-front';
import {
	dataSourceDetail,
	mutilangJson_list,
	periodDataSource,
	oldSource
} from '../../../public/components/constJSON';
let { setDefData,getDefData } = cardCache;
import './index.less';
import HeaderArea from '../../../public/components/HeaderArea';
const formId = 'cycleform';
const tableId = 'cycletable';
const pageId = '20021005cycle';
class PeriodPage extends Component {
	constructor(props) {
		super(props);
		this.state={
			json:{}
		}
		this.ViewModel = this.props.ViewModel;
		
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				this.initTemplate.call(this, this.props);
				setDefData('mutilangJson_list', mutilangJson_list, json);
			})
		}
		getMultiLang({moduleId:['20021005card','20021005list'],domainName:'gl',currentLocale:'zh-CN',callback});

	}




	render() {
		let { table, form } = this.props;
		let { createSimpleTable } = table;
		let { createForm } = form;
		return (
			<div className="nc-bill-list_voucher" id="nc-bill-list_voucher">
				<HeaderArea
					title={this.state.json['20021005card-000150']/* 国际化处理： 新增周期凭证*/}
					searchContent={
						<div className="header-special-form">
							{createForm(formId, {
								onAfterEvent: this.afterEvent.bind(this)
							})}
						</div>
					}
					btnContent={
						this.props.button.createButtonApp({
							area: 'head',
							buttonLimit: 3,
							onButtonClick: this.buttonClick.bind(this)
						})
					}
				/>
				{/* <div className="nc-bill-header-area">
					{createPageIcon()}
					<div className="header-title-search-area">
						<h2 className="title-search-detail">{this.state.json['20021005card-000150']}</h2>
					</div>
					<div className="header-special-form">
						{createForm(formId, {
							onAfterEvent: this.afterEvent.bind(this)
						})}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'head',
							buttonLimit: 3,
							onButtonClick: this.buttonClick.bind(this)
						})}
					</div>
				</div> */}
				{/* <div className="form-wrapper">
					{createForm(formId, {
						onAfterEvent: this.afterEvent.bind(this)
					})}
				</div> */}
				<div className="table-wrapper">
					{createSimpleTable(tableId, {
						showIndex: true,
						showCheck: false
					})}
				</div>
			</div>
		);
	}

	initTemplate = (props) => {
		let appcode = props.getUrlParam('appcode') || props.getSearchParam('c');
		let _this = this;
		props.createUIDom(
			{
				pagecode: pageId, //页面id
				appcode: appcode //小应用id
			},
			function(data) {
				if (data) {
					if (data.template) {
						let meta = data.template;
						meta = _this.modifierMeta(props, meta);
						props.meta.setMeta(meta);
						let linkData = getDefData('voucher_period', periodDataSource);
						if(linkData){
							props.form.setFormItemsValue(formId,{pk_accountingbook:linkData});
							_this.getUnExcused(props,linkData);
						}else{
							let pk_accountingbook=props.form.getFormItemsValue(formId,'pk_accountingbook').value;
							if((!pk_accountingbook||pk_accountingbook=='')&&data.context.defaultAccbookPk&&data.context.defaultAccbookPk!=''){
								props.form.setFormItemsValue(formId,{pk_accountingbook:{
									display: data.context.defaultAccbookName,
									value: data.context.defaultAccbookPk
								}});
								_this.getUnExcused(props,{
									display: data.context.defaultAccbookName,
									value: data.context.defaultAccbookPk
								});
							}
						}
					}
					if (data.button) {
						let button = data.button;
						props.button.setButtons(button);
					}
				}
			}
		);
	};

	modifierMeta = (props, meta) => {
		let appcode = props.getUrlParam('appcode') || props.getSearchParam('c');
		meta[formId].status = 'edit';
		meta[formId].items = meta[formId].items.map((item, key) => {
			if (item.attrcode == 'pk_accountingbook') {
				item.queryCondition = () => {
					return {
						TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
						appcode: appcode
					};
				};
			}
			return item;
		});

		let material_event = {
			label: this.state.json['20021005card-000012'],/* 国际化处理： 操作*/
			itemtype: 'customer',
			attrcode: 'opr',
			width: '150px',
			visible: true,
			fixed: 'right',
			render: (text, record, index) => {
				let buttonAry = [ 'excused' ];
				return props.button.createOprationButton(buttonAry, {
					area: tableId,
					buttonLimit: 3,
					onButtonClick: (props, key) => this.excused(props, key, record, index)
				});
			}
		};
		meta[tableId].items.push(material_event);
		return meta;
	};

	excused = (props, key, record, index) => {
		let self = this;
		let queryPeriod = '/nccloud/gl/amortize/transfervoucher.do';
		let predata = {};
		for (let key in record) {
			if(record[key])
				predata[key] = record[key].value;
		}
		predata.includeUntall=props.form.getFormItemsValue(formId,'isuntally').value;
		ajax({
			url: queryPeriod,
			data: predata,
			success: (res) => {
			//	let voucherDetail = getDefData('voucher_detail', dataSourceDetail); //生成
				setDefData('voucher_detail',dataSourceDetail,res.data);
				let oldValueData = JSON.stringify(res.data);//取消数据处理
				//setDefData('oldValue', oldSource, oldValueData);
				//self.ViewModel.setData('gl_voucher', res.data);
				self.props.pushTo('/Welcome', {
					//c: self.props.getUrlParam('c') || self.props.getSearchParam('c'),
					appcode: self.props.getUrlParam('appcode') || self.props.getSearchParam('c'),
					pagecode: '20021005card',
					status: 'edit'
				});
			}
		});
	};

	afterEvent = (props, moduleid, key, value, oldvalue) => {
		if ('pk_accountingbook' == key) {
			if (value.refpk && value.refpk != '') {
				let pk_accountingbook={
					display:value.refname,
					value:value.refpk
				}
				this.getUnExcused(props, pk_accountingbook);
			} else {
				props.table.setAllTableData(tableId, { rows: [] });
			}
		}
	};

	buttonClick = (props, key) => {
		if (key == 'refresh') {
			let pk_accountingbook = props.form.getFormItemsValue(formId, 'pk_accountingbook');
			if (pk_accountingbook && pk_accountingbook.value != '') {
				this.getUnExcused(props, pk_accountingbook);
			} else {
				props.table.setAllTableData(tableId, { rows: [] });
			}
		}
	};

	getUnExcused = (props, pk_accountingbook) => {
		let queryPeriod = '/nccloud/gl/amortize/getunexcused.do';
		let perData = {
			pk_accountingbook: pk_accountingbook.value
		};
		let _this = this;
		setDefData('voucher_period', periodDataSource,pk_accountingbook);
		ajax({
			url: queryPeriod,
			data: perData,
			success: (res) => {
				let { data } = res;
				if (data) {
					props.table.setAllTableData(tableId, data[tableId]);
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
				}
			}
		});
	};
}

PeriodPage = createPage(
	{
		//initTemplate: initTemplate,
	}
)(PeriodPage);
export default PeriodPage;
