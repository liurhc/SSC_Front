
import {asyncComponent} from 'nc-lightapp-front';
import Gltransfer from './periodpage.js';

// import VoucherList from '../../gl_voucher/voucher_list/list';
// import Voucher from '../../gl_voucher/container/Welcome';
// import PeriodVoucher from '../../gl_voucher/container/PeriodVoucher/periodpage';
import Voucher from 'gl/voucher_card';
//const Voucher = asyncComponent(() => import(/* webpackChunkName: "gl/gl_voucher/container/Welcome" */'../../../gl_voucher/container/Welcome'));

// const edit11 = asyncComponent(() => import(/* webpackChunkName: "reva_demo/module/apply/card/soCard" */'../../apply/card'));

// const appHome = asyncComponent(() => import(/* webpackChunkName: "demo/module/so/js/AppHome" */'pages/app/home'));
// const appMain = asyncComponent(() => import(/* webpackChunkName: "demo/module/so/js/AppMain" */'pages/app/main'));

const routes = [
  {
    path: '/',
    component: Gltransfer,
    exact: true,
  },
  {
    path: '/Welcome',
    component: Voucher,
  }
];

export default routes;
