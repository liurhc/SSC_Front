import { ajax, base, toast } from 'nc-lightapp-front';
import {saveButton,templeteButton,onOftenSelect,voucherSelectAction,imageSelect,locationModal} from './index.js';
let tableid = 'gl_voucher';
export default function pageInfoClick(props,parentProp, pk) {
    let searchId= {
        pk_voucher:pk
    };
    let urllist = '/nccloud/gl/voucher/query.do';
    props.updatePage(searchId,urllist);
}
