import { ajax, base, toast } from 'nc-lightapp-front';
import {saveButton,templeteButton,onOftenSelect,voucherSelectAction,imageSelect,locationModal} from './index.js';
let tableid = 'gl_voucher';
export default function handleButtonClick(voucherState,props,id) {
    let offsetUrl = '/nccloud/gl/voucher/offset.do'
    let record = this.state.getrow;
    let {evidenceData,copyRecord,copyData,getrow}=this.state;
    let { rows,index } =evidenceData;
    let originData = this.findByKey(record, rows);
    switch (id) {
        case 'addline':
            this.addNewLine(record?record:evidenceData.index);
            this.setState({
                evidenceData
            })
            break;
        case 'deline':
            if (originData) {
                rows = rows.filter(function(v, i, a) {
                    return v.key != originData.key;
                });
                if (record <= rows.length) {
                    rows.map((item, index) => {
                        if (item.key >= record) {
                            item.key--;
                        }
                    });
                }
                this.state.evidenceData.rows = rows;                                         
                this.state.evidenceData.index=index-1;
                this.totleCount();
            }
            
            this.setState({
                evidenceData: this.state.evidenceData
            },()=>{
            });
            break;
        case 'copyline':
            copyData=evidenceData.rows[copyRecord];
           // this.state.copyData.pk_detail.display=null;
           // this.state.copyData.pk_detail.value=null;
           this.setState({
                copyData
           })
            break;
        case 'cutline':
            copyData = evidenceData.rows[copyRecord];
            this.state.copyOrCut = "cut";
            let record = ++copyRecord;
            if (originData) {
                rows = rows.filter(function (v, i, a) {
                     return v.key != originData.key;
                });
                this.state.evidenceData.rows = rows;
            }
            if (record <= rows.length) {
                 rows.map((item, index) => {
                    if (item.key >= record) {
                        item.key--;
                    }
                });
            }
            this.totleCount();//同步计算合计值
            this.setState({
                 evidenceData: this.state.evidenceData,
                 copyData
            });
            break;
        case 'pasteline':
            let newkey = ++copyRecord;
            let newLine = Object.assign({}, copyData, {
                key: newkey
            });
            if (newkey <= evidenceData.rows.length) {
                evidenceData.rows.map((item, index) => {
                    if (item.key >= newkey) {
                        item.key++;
                    }
                });
            }
            evidenceData.rows.splice(newkey - 1, 0, newLine);
            this.totleCount();//同步计算合计值
            this.setState({
                evidenceData
            })
            break;
        case 'query':
            this.setState({
                showModal:true
            })
            break;
        case 'apportion'://快速分摊
            this.setState({
                apportionmentShow:true
            })
            break;

        case'signmistakeb'://分录标错
            if(getrow){
                this.setState({
                    SaveErrorModalShow:true
                })
            }else{
                toast({ content:this.state.json['20021005card-000091'], color: 'error' });/* 国际化处理： 请选择标错分录行*/
            }
           
            break;
        case 'verify':
            this.setState({
                VrifyModalShow:true
            })
         break;
        case 'print':
            //打印
            break;
    }
}

export function dealOperate(props, opurl, id,callBack,type) {
    // const selectedData = props.table.getCheckedRows(tableid);
    // if (selectedData.length == 0) return
    // let indexArr = [];
    // let dataArr = [];
    // let indexObj = {};

    // //只传ts和Pk_voucher主键
    // selectedData.forEach((val) => {
    //     dataArr.push(val.data.values.pk_voucher.value);
    //     indexArr.push(val.index);
    //     indexObj[val.data.values.pk_voucher.value] = val.index;
    // });
    let data={};
    if( id=='buleoffset'||id=='redoffset'||id=='offset'){
        data={
            pk_voucher: props.saveData.pk_voucher.value,
            offsettype:type
        }
    }else{
        data= {
            pk_voucher: props.saveData.pk_voucher.value
        }
    }


    ajax({
        url: opurl,
        data:data,
        async:true,
        success: (res) => {
            let { success, data } = res;
            if (data.error&&data.error.length > 0) {
                toast({ content: data.error, color: 'warning', position: 'bottom' })
            }
            else if (success) {
                if (id == 'delete') {
                    props.table.deleteTableRowsByIndex(tableid, indexArr);
                    toast({ content: this.state.json['20021005card-000081'], color: 'success', position: 'bottom' })/* 国际化处理： 删除成功*/
                } else {                   
                    dealSuccess(props, data, id,callBack);                    
                }

            }
        },
        error: (res) => {
            toast({ content: res.message, color: 'warning'})
        }
    })
}

export function dealSuccess(props, data, id,callBack) {
    let succdatas = data.success;
    if(succdatas){
        succdatas.details.map((item,i)=>{
            item.key=++i
            if(item.price.value&&item.price.value!='0'){
                item.flag=true;
                if(item.creditquantity.value){//借贷数量同步显示
                    item.debitquantity.value=item.creditquantity.value;
                    item.creditquantity.value='';
                }
            }
            if(item.creditamount.value&&item.creditamount.value!='0'){
                item.amount={
                    value:item.creditamount.value
                }
            }
            if(item.debitamount.value&&item.debitamount.value!='0'){
                item.amount={
                    value:item.debitamount.value
                }
            }
            //保存成功后添加子表字段结算字段
          //  item.childform=evidenceData.rows[item.key-1].childform;
        })
    }else{//针对冲销功能字段解析
        data.details.map((item,i)=>{
            item.key=++i
            if(item.price.value&&item.price.value!='0'){
                item.flag=true;
                if(item.creditquantity.value){//借贷数量同步显示
                    item.debitquantity.value=item.creditquantity.value;
                    item.creditquantity.value='';
                }
            }
            if(item.creditamount.value&&item.creditamount.value!='0'){
                item.amount={
                    value:item.creditamount.value
                }
            }
            if(item.debitamount.value&&item.debitamount.value!='0'){
                item.amount={
                    value:item.debitamount.value
                }
            }
            //保存成功后添加子表字段结算字段
          //  item.childform=evidenceData.rows[item.key-1].childform;
        })
    }
    switch (id) {
        case 'delete':
            succdatas.forEach((val) => {
                props.table.deleteTableRowsByIndex(tableId, indexObj[val.pk_voucher]);
            });
            toast({ content: this.state.json['20021005card-000081'], color: 'success', position: 'bottom' })/* 国际化处理： 删除成功*/
            break;
        case 'abandon':
            // succdatas.forEach((val) => {
            //     props.table.setTableValueBykey(tableid, "discardflag", val.discardflag, indexObj[val.pk_voucher]);
            // });
            callBack.saveUpdate(succdatas);
            toast({ content: this.state.json['20021005card-000082'], color: 'success', position: 'bottom' })/* 国际化处理： 作废成功*/
            break;
        case 'unabandon':
            // succdatas.forEach((val) => {
            //     props.table.setTableValueBykey(tableid, "discardflag", val.discardflag, indexObj[val.pk_voucher]);
            // });
            callBack.saveUpdate(succdatas);
            toast({ content: this.state.json['20021005card-000083'], color: 'success', position: 'bottom' })/* 国际化处理： 取消作废成功*/
            break;
        case 'check':
            //审核
            // succdatas.forEach((val) => {
            //     props.table.setTableValueBykey(tableid, "pk_checked", val.pk_checked, indexObj[val.pk_voucher]);
            // });
            succdatas.voucherView=true;
            callBack.saveUpdate(succdatas);
            toast({ content: this.state.json['20021005card-000084'], color: 'success', position: 'bottom' });/* 国际化处理： 审核成功*/
            break;
        case 'uncheck':
            //取消审核
            // succdatas.forEach((val) => {
            //     props.table.setTableValueBykey(tableid, "pk_checked", "", indexObj[val.pk_voucher]);
            // });
            succdatas.voucherView=true;
            callBack.saveUpdate(succdatas);
            toast({ content: this.state.json['20021005card-000085'], color: 'success', position: 'bottom' });/* 国际化处理： 取消审核成功*/
            break;
        case 'tally':
            //记账
            // succdatas.forEach((val) => {
            //     props.table.setTableValueBykey(tableid, "pk_manager", val.pk_manager, indexObj[val.pk_voucher]);
            // });
            callBack.saveUpdate(succdatas);
            toast({ content: this.state.json['20021005card-000086'], color: 'success', position: 'bottom' });/* 国际化处理： 记账成功*/
            break;
        case 'untally':
            //取消记账
            // succdatas.forEach((val) => {
            //     props.table.setTableValueBykey(tableid, "pk_manager", "", indexObj[val.pk_voucher]);
            // });
            callBack.saveUpdate(succdatas);
            toast({ content: this.state.json['20021005card-000087'], color: 'success', position: 'bottom' });/* 国际化处理： 取消记账成功*/
            break;
        case 'sign':
            //签字
            // succdatas.forEach((val) => {
            //     props.table.setTableValueBykey(tableid, "signflag", val.signflag, indexObj[val.pk_voucher]);
            // });
            callBack.saveUpdate(succdatas);
            toast({ content: this.state.json['20021005card-000088'], color: 'success', position: 'bottom' });/* 国际化处理： 签字成功*/
            break;
        case 'unsign':
            //取消签字
            // succdatas.forEach((val) => {
            //     props.table.setTableValueBykey(tableid, "signflag", val.signflag, indexObj[val.pk_voucher]);
            // });
            callBack.saveUpdate(succdatas);
            toast({ content: this.state.json['20021005card-000089'], color: 'success', position: 'bottom' });/* 国际化处理： 取消签字成功*/
            break;
        case 'buleoffset':
            //冲销
            data.voucherView=false;

            callBack.saveUpdate( data);
            toast({ content: this.state.json['20021005card-000090'], color: 'success', position: 'bottom' });/* 国际化处理： 冲销成功*/
            break; 
        case 'redoffset':
            //冲销
            data.voucherView=false;
           
            callBack.saveUpdate( data);
            toast({ content: this.state.json['20021005card-000090'], color: 'success', position: 'bottom' });/* 国际化处理： 冲销成功*/
            break;  
        case 'offset':
            //冲销
            data.voucherView=false;
            callBack.saveUpdate( data);
            toast({ content: this.state.json['20021005card-000090'], color: 'success', position: 'bottom' });/* 国际化处理： 冲销成功*/
    }
}

