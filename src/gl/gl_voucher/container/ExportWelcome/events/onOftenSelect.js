import { ajax, deepClone } from 'nc-lightapp-front';
import { toast } from 'nc-lightapp-front';
//const deepClone = require('../../../../public/components/deepClone');

export default function oftenButton(props, state, key) {
	if (!props.saveData.pk_accountingbook.value) {
		toast({ content: this.state.json['20021005card-000092'], color: 'warning' });/* 国际化处理： 请先选定核算账簿*/
		return;
	}
	// 复用保存数据，传给新增模板
	let self = this;
	let { pk_accountingbook, nov, startdate, attachment, evidenceData, saveData, update, saveNumber } = props;
	let url = update ? '/gl_web/gl/voucher/update.do' : '/gl_web/gl/voucher/insert.do';
	let cloneNewLine = deepClone(evidenceData.rows);
	if (!update) {
		let newclone = cloneNewLine.filter(function(v, i, a) {
			return v.explanation.value != '';
		});
		newclone.forEach(function(v, i, a) {
			let ass = [];
			if (v.flag == true && v.localcreditamount.value) {
				v.creditquantity = v.debitquantity;
				v.debitquantity = {
					value: ''
				};
			}
			v.explanation.value = v.explanation.display;
			v.ass = v.childform;
			delete v.childform;
			delete v.checkedNumber;
			delete v.key;
			delete v.isEdit;
		});
		nov.value = Number(nov.value);
		attachment.value = Number(attachment.value);
		saveData.details = newclone;
	} else {
		if (saveData.period.value.indexOf('-') != -1) {
			let newValue = saveData.period.value.split('-')[1];
			saveData.period.value = newValue;
		}
		let newRows = cloneNewLine.filter(function(v, i, a) {
			return v.explanation.value != '';
		});
		newRows.forEach(function(v, i, a) {
			v.explanation.value = v.explanation.display;
			if (!v.flag) {
				v.price.value = '';
				v.debitquantity.value = '';
				if (v.creditquantity) {
					v.creditquantity.value = '';
				}
			}
		});
		saveData.details = newRows;
	}
	//自动获取的凭证号置0操作
	let saveDataClone = deepClone(saveData);
	if (props.novAuto == true) {
		saveDataClone.num.value = '0';
		saveDataClone.num.display = '0';
	}

	if (key == 'loadtmp') {
		// 调用模板
		let { UseOftenModalShow, ...others } = state;
		self.refs.UseOftenModal.queryClass(props.saveData.pk_accountingbook.value);
		self.setState(
			{
				...others,
				UseOftenModalShow: true
			},
			() => {
				// self.props.updateState(self.state)
			}
		);
	}

	if (key == 'managetmp') {
		// 管理模板
		let { ManageOftenModalShow, ...others } = state;
		self.refs.ManageOftenModal.queryClass(props.saveData.pk_accountingbook.value, saveDataClone);
		// let SubjectModalShow = true;
		self.setState({
			ManageOftenModalShow: true
		});
	}
}
