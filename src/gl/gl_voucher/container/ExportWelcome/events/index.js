import saveButton from './saveButton';
import templeteButton from './templeteButton';
import onOftenSelect from './onOftenSelect';
import voucherSelectAction from './voucherSelectAction';
import imageSelect from './imageSelect';
import onButtonClick from "./buttonClick";
import pageInfoClick from "./pageInfoClick";
import handleButtonClick from "./handleButtonClick";
import locationModal from "./locationModal";
export {saveButton,templeteButton,onOftenSelect,voucherSelectAction,imageSelect,onButtonClick,locationModal,handleButtonClick,pageInfoClick}
