import {ajax,deepClone} from 'nc-lightapp-front';
import { toast } from 'nc-lightapp-front';
//const deepClone = require('../../../../public/components/deepClone');

export default function saveData(props) {
        let self = this;
		let { pk_accountingbook, nov, startdate, attachment, evidenceData, saveData, update,saveNumber } = props;
		let url = '/nccloud/gl/voucher/tempSave.do';
		let cloneNewLine = deepClone(evidenceData.rows);
		//表体最后一条合计数据删除
		//let newRows = self.truncate(evidenceData.rows);
		// newRows.forEach(function(v, i, a) {
		// 	if (v.localcreditamount || v.localdebitamount) {
		// 		if (v.localcreditamount) {
		// 			v.localcreditamount.value = Number(v.localcreditamount.value);
		// 		} else {
		// 			v.localdebitamount.value = Number(v.localdebitamount.value);
		// 		}
		// 	}
		// 	delete v.key;
		// 	delete v.isEdit;
		// });
		//let newRows=evidenceData.rows;
		if (!update) {
			let newclone=cloneNewLine.filter(function(v, i, a) {
					return v.explanation.value != '';
			});
			newclone.forEach(function(v, i, a) {
				let ass = [];
				if(v.flag==true&&v.localcreditamount.value){
					v.creditquantity=v.debitquantity;
					v.debitquantity={
						value:''
					}
				}
			//	v.explanation.value=v.explanation.display;
				v.ass = v.childform;
				delete v.childform;
				delete v.checkedNumber;
				delete v.key;
				delete v.isEdit;
			});
			nov.value = Number(nov.value);
			attachment.value = Number(attachment.value);
			saveData.details = newclone;
		}else{
			if(saveData.period.value.indexOf('-')!=-1){
				let newValue=saveData.period.value.split('-')[1];
				saveData.period.value=newValue;
			}
			let newRows=cloneNewLine.filter(function(v, i, a) {
				return v.explanation.value != '';
			});
			newRows.forEach(function(v, i, a) {
				//修改摘要为可录入
				//v.explanation.value=v.explanation.display;
				if(!v.flag){
					v.price.value='';
					v.debitquantity.value='';
					if(v.creditquantity){
						v.creditquantity.value='';
					}
				}
			});
			saveData.details=newRows;
		}
		//自动获取的凭证号置0操作
		let saveDataClone = deepClone(saveData);
		if (props.novAuto == true) {
			saveDataClone.num.value = '0';
			saveDataClone.num.display = '0';
		}

		ajax({
			url,
			data:saveData,
			success: function(response) {
				//会计期间赋值 保存ts pk
				if (response.data) {
					toast({ content: this.state.json['20021005card-000093'], color: 'success' });/* 国际化处理： 保存成功*/
					if(saveNumber=='2'){
						 window.location.href = './welcome.html';
					}
					response.data.details.map((item,i)=>{
						item.key=++i
						if(item.price.value&&item.price.value!='0'){
							item.flag=true;
							if(item.creditquantity.value){//借贷数量同步显示
								item.debitquantity.value=item.creditquantity.value;
								item.creditquantity.value='';
							}
						}
					})
					evidenceData.rows=response.data.details;
					evidenceData.index=response.data.details.length;
					if (saveData.num.value != response.data.num.value ) {
						toast({ content: this.state.json['20021005card-000067'], color: 'warning' });/* 国际化处理： 请注意，凭证号已更新*/
					}
					saveData = response.data;
					evidenceData.localcreditamountTotle.value=response.data.totalcredit.display;
					evidenceData.localdebitamountTotle.value=response.data.totaldebit.display;
					saveData.period.value = response.data.year.value + '-' + response.data.period.value;
					update = true;
					self.setState({
						evidenceData,
						update,
						saveData
					},()=>{
						//self.props.saveUpdate(self.state)
					});
				} else {
					toast({ content: response.data.error.message, color: 'warning',duration: 30000});
				}
			}
        });

}
