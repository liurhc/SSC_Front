import { ajax, base, toast, cacheTools } from 'nc-lightapp-front';
import { saveButton, templeteButton, onOftenSelect, voucherSelectAction, imageSelect, locationModal } from './index.js';
let tableid = 'gl_voucher';
export default function buttonClick(voucherState, parentState, props, id, headFunc) {
	let offsetUrl = '/nccloud/gl/voucher/offset.do';
	switch (headFunc) {
		case 'save':
			saveButton.call(this, voucherState);
			// window.location.href = '../../../gl_voucher/pages/voucher/index.html'
			// props.linkTo('../../../gl_voucher/pages/voucher/index.html', {
			//     status: 'jump',
			//     pk_operatinglog: '1001Z31000000000FYV1',
			//     groupid: null,
			//     src: 'fip'
			//     })
			break;
		case 'temp':
			templeteButton.call(this, voucherState);
			break;
		case 'saveadd':
			saveButton.call(this, voucherState, 'saveAdd', props);
			break;
		case 'cyclevoucher': //周期凭证
			break;
		case 'loadtmp': //调用模板
			onOftenSelect.call(this, voucherState, props, 'loadtmp');
			break;
		case 'managetmp': //管理模板
			onOftenSelect.call(this, voucherState, props, 'managetmp');
			break;
		case 'delete': //删除
			voucherSelectAction.call(this, voucherState, props, 'delete', parentState);
			break;
		case 'copy': //复制
			voucherSelectAction.call(this, voucherState, props, 'copy', parentState);
			break;
		case 'abandon': //作废
			voucherSelectAction.call(this, voucherState, props, 'abandon', parentState);
			break;
		case 'unabandon': //取消作废
			voucherSelectAction.call(this, voucherState, props, 'unabandon', parentState);
			break;
		case 'errcorr': //纠错
			voucherSelectAction.call(this, voucherState, props, 'errcorr', parentState);
			break;
		case 'signmistake': //标错
			voucherSelectAction.call(this, voucherState, props, 'signmistake', parentState);
			break;
		case 'convert': //及时折算
			break;
		case 'verify': //及时核销
			//voucherSelectAction.call(this, voucherState,props,'verify',parentState)
			break;
		case 'cashflow': //现金流量
			voucherSelectAction.call(this, voucherState, props, 'cashflow', parentState);
			break;
		case 'adjustvoucher': //调整凭证
			break;
		case 'location': //定位
			locationModal.call(this, voucherState, props);
			break;
		case 'tolist':
			id.linkTo(
				'../../../gl_voucher/voucher_list/list/index.html' + window.location.search,
				{
					// status: 'jump',
					// pk_operatinglog: '1001Z31000000000FYV1',
					// groupid: null,
					// src: 'fip'
				}
			);
			//  window.location.href = '../../voucher_list/list/index.html?isChecked='+"true";
			break;
		// case 'delete':
		//     let delUrl = '/nccloud/gl/voucher/delete.do'
		//     dealOperate(props, delUrl, id);
		//     break;
		case 'query':
			this.setState({
				showModal: true
			});
			break;
		case 'refesh':
			//刷新
			break;
		// case 'abandon':
		//     //作废
		//     let abnUrl = '/nccloud/gl/voucher/abandon.do'
		//     dealOperate(props, abnUrl, id);
		//     break;
		// case 'unabandon':
		//     //取消作废
		//     let unabnUrl = '/nccloud/gl/voucher/unabandon.do'
		//     dealOperate(props, unabnUrl, id);
		//     break;
		case 'check':
			//审核
			let checkUrl = '/nccloud/gl/voucher/check.do';
			dealOperate(voucherState, checkUrl, headFunc, parentState);
			break;
		case 'uncheck':
			//取消审核
			let uncheckUrl = '/nccloud/gl/voucher/uncheck.do';
			dealOperate(voucherState, uncheckUrl, headFunc, parentState);
			break;
		case 'tally':
			//记账
			let tallyUrl = '/nccloud/gl/voucher/tally.do';
			dealOperate(voucherState, tallyUrl, headFunc, parentState);
			break;
		case 'untally':
			//取消记账
			let untallyUrl = '/nccloud/gl/voucher/untally.do';
			dealOperate(voucherState, untallyUrl, headFunc, parentState);
			break;
		case 'sign':
			//签字
			let signUrl = '/nccloud/gl/voucher/sign.do';
			dealOperate(voucherState, signUrl, headFunc, parentState);
			break;
		case 'unsign':
			//取消签字
			let unsignUrl = '/nccloud/gl/voucher/unsign.do';
			dealOperate(voucherState, unsignUrl, headFunc, parentState);
			break;
		case 'buleoffset':
			//蓝冲
			dealOperate(voucherState, offsetUrl, headFunc, parentState, '2');
			break;
		case 'redoffset':
			//红冲
			dealOperate(voucherState, offsetUrl, headFunc, parentState, '1');
			break;
		case 'offset':
			//默认冲销
			dealOperate(voucherState, offsetUrl, headFunc, parentState, '0');
			break;
		case 'showsum': //汇总
			let showsumUrl = '/nccloud/gl/voucher/showSumDetails.do';
			dealOperate(voucherState, showsumUrl, headFunc, parentState);
			break;
		case 'edit': //修改
			parentState.voucherView({ voucherView: false });
			break;
		case 'linksrc': //联查单据
			let getParm = (parm) => {
				let appUrl = decodeURIComponent(window.location.href).split('?');
				if (appUrl && appUrl[1]) {
					let appPrams = appUrl[1].split('&');
					if (appPrams && appPrams instanceof Array) {
						let parmObj = {};
						appPrams.forEach((item) => {
							let key = item.split('=')[0];
							let value = item.split('=')[1];
							parmObj[key] = value;
						});
						return parmObj[parm];
					}
				}
			};
			let appid = getParm('ar') || '';
			cacheTools.set(appid + '_LinkSrc', { relationID: voucherState.saveData.pk_voucher.value });
			window.parent.openNew(
				{ code: '10170410', name: this.state.json['20021005card-000080'], pk_appregister: '0001Z31000000002QMYF' },/* 国际化处理： 单据生成*/
				null,
				'&status=browse&src=' + appid + '_LinkSrc'
			);
			break;
		case 'import':
			//导入
			break;
		case 'export':
			//导出
			break;
		case 'print':
			//打印
			break;
	}
}

export function dealOperate(props, opurl, id, callBack, type) {
	// const selectedData = props.table.getCheckedRows(tableid);
	// if (selectedData.length == 0) return
	// let indexArr = [];
	// let dataArr = [];
	// let indexObj = {};

	// //只传ts和Pk_voucher主键
	// selectedData.forEach((val) => {
	//     dataArr.push(val.data.values.pk_voucher.value);
	//     indexArr.push(val.index);
	//     indexObj[val.data.values.pk_voucher.value] = val.index;
	// });
	let data = {};
	if (id == 'buleoffset' || id == 'redoffset' || id == 'offset') {
		data = {
			pk_voucher: props.saveData.pk_voucher.value,
			offsettype: type
		};
	} else {
		data = {
			pk_voucher: props.saveData.pk_voucher.value
		};
	}

	ajax({
		url: opurl,
		data: data,
		async: true,
		success: (res) => {
			let { success, data } = res;
			if (data.error && data.error.length > 0) {
				toast({ content: data.error, color: 'warning', position: 'bottom' });
			} else if (success) {
				if (id == 'delete') {
					props.table.deleteTableRowsByIndex(tableid, indexArr);
					toast({ content: this.state.json['20021005card-000081'], color: 'success', position: 'bottom' });/* 国际化处理： 删除成功*/
				} else {
					dealSuccess(props, data, id, callBack);
				}
			}
		},
		error: (res) => {
			toast({ content: res.message, color: 'warning' });
		}
	});
}

export function dealSuccess(props, data, id, callBack) {
	let succdatas = data.voucher;
	if (succdatas) {
		succdatas.details.map((item, i) => {
			item.key = ++i;
			if (item.price.value && item.price.value != '0') {
				item.flag = true;
				if (item.creditquantity.value) {
					//借贷数量同步显示
					item.debitquantity.value = item.creditquantity.value;
					item.creditquantity.value = '';
				}
			}
			if (item.creditamount.value && item.creditamount.value != '0') {
				item.amount = {
					value: item.creditamount.value
				};
			}
			if (item.debitamount.value && item.debitamount.value != '0') {
				item.amount = {
					value: item.debitamount.value
				};
			}
			//保存成功后添加子表字段结算字段
			//  item.childform=evidenceData.rows[item.key-1].childform;
		});
	} else {
		//针对冲销功能字段解析
		data.details.map((item, i) => {
			item.key = ++i;
			if (item.price.value && item.price.value != '0') {
				item.flag = true;
				if (item.creditquantity.value) {
					//借贷数量同步显示
					item.debitquantity.value = item.creditquantity.value;
					item.creditquantity.value = '';
				}
			}
			if (item.creditamount.value && item.creditamount.value != '0') {
				item.amount = {
					value: item.creditamount.value
				};
			}
			if (item.debitamount.value && item.debitamount.value != '0') {
				item.amount = {
					value: item.debitamount.value
				};
			}
			//保存成功后添加子表字段结算字段
			//  item.childform=evidenceData.rows[item.key-1].childform;
		});
	}
	switch (id) {
		case 'delete':
			succdatas.forEach((val) => {
				props.table.deleteTableRowsByIndex(tableId, indexObj[val.pk_voucher]);
			});
			toast({ content: this.state.json['20021005card-000081'], color: 'success', position: 'bottom' });/* 国际化处理： 删除成功*/
			break;
		case 'abandon':
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "discardflag", val.discardflag, indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000082'], color: 'success', position: 'bottom' });/* 国际化处理： 作废成功*/
			break;
		case 'unabandon':
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "discardflag", val.discardflag, indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000083'], color: 'success', position: 'bottom' });/* 国际化处理： 取消作废成功*/
			break;
		case 'check':
			//审核
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_checked", val.pk_checked, indexObj[val.pk_voucher]);
			// });
			succdatas.voucherView = true;
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000084'], color: 'success', position: 'bottom' });/* 国际化处理： 审核成功*/
			break;
		case 'uncheck':
			//取消审核
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_checked", "", indexObj[val.pk_voucher]);
			// });
			succdatas.voucherView = true;
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000085'], color: 'success', position: 'bottom' });/* 国际化处理： 取消审核成功*/
			break;
		case 'tally':
			//记账
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_manager", val.pk_manager, indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000086'], color: 'success', position: 'bottom' });/* 国际化处理： 记账成功*/
			break;
		case 'untally':
			//取消记账
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "pk_manager", "", indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000087'], color: 'success', position: 'bottom' });/* 国际化处理： 取消记账成功*/
			break;
		case 'sign':
			//签字
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "signflag", val.signflag, indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000088'], color: 'success', position: 'bottom' });/* 国际化处理： 签字成功*/
			break;
		case 'unsign':
			//取消签字
			// succdatas.forEach((val) => {
			//     props.table.setTableValueBykey(tableid, "signflag", val.signflag, indexObj[val.pk_voucher]);
			// });
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000089'], color: 'success', position: 'bottom' });/* 国际化处理： 取消签字成功*/
			break;
		case 'buleoffset':
			//冲销
			succdatas.voucherView = false;

			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000090'], color: 'success', position: 'bottom' });/* 国际化处理： 冲销成功*/
			break;
		case 'redoffset':
			//冲销
			succdatas.voucherView = false;

			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000090'], color: 'success', position: 'bottom' });/* 国际化处理： 冲销成功*/
			break;
		case 'offset':
			//冲销
			succdatas.voucherView = false;
			callBack.saveUpdate(succdatas);
			toast({ content: this.state.json['20021005card-000090'], color: 'success', position: 'bottom' });/* 国际化处理： 冲销成功*/
			break;
		case 'showsum':
			succdatas.voucherView = false;
			callBack.saveUpdate(succdatas);
		//  toast({ content: "汇总成功", color: 'success', position: 'bottom' });
	}
}
