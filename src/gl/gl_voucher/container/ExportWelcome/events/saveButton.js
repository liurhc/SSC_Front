import {ajax,deepClone} from 'nc-lightapp-front';
import { toast,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
export default function saveData(props,status,extend) {
		let self = this;
		const settkleArray=['bankaccount','billtype','checkstyle','checkno','checkdate'];
		const verifyArray=['verifyno','verifydate'];
		let { pk_accountingbook, nov, startdate, attachment, evidenceData, saveData, voucherStatus,saveNumber,lastMessage } = props;
		if(status=='saveAdd'){
			saveNumber='2'
		}
		let url = voucherStatus=='update' ? '/nccloud/gl/voucher/update.do' : '/nccloud/gl/voucher/insert.do';
		let cloneNewLine = deepClone(evidenceData.rows);
		//表体最后一条合计数据删除
		//let newRows = self.truncate(evidenceData.rows);
		// newRows.forEach(function(v, i, a) {
		// 	if (v.localcreditamount || v.localdebitamount) {
		// 		if (v.localcreditamount) {
		// 			v.localcreditamount.value = Number(v.localcreditamount.value);
		// 		} else {
		// 			v.localdebitamount.value = Number(v.localdebitamount.value);
		// 		}
		// 	}
		// 	delete v.key;
		// 	delete v.isEdit;
		// });
		//let newRows=evidenceData.rows;
		if (voucherStatus!='update') {
			let newclone=cloneNewLine.filter(function(v, i, a) {
					return v.explanation.value != '';
			});
			newclone.forEach(function(v, i, a) {
				let ass = [];
				if(v.flag==true&&v.localcreditamount.value){
					v.creditquantity=v.debitquantity;
					v.debitquantity={
						value:''
					}
				}
			//	v.explanation.value=v.explanation.display;
				//v.ass = v.childform;
				delete v.childform;
				delete v.checkedNumber;
				delete v.key;
				delete v.isEdit;
			});
			nov.value = Number(nov.value);
			attachment.value = Number(attachment.value);
			saveData.details = newclone;
		}else{
			if(saveData.period.value.indexOf('-')!=-1){
				let newValue=saveData.period.value.split('-')[1];
				saveData.period.value=newValue;
			}
			let newRows=cloneNewLine.filter(function(v, i, a) {
				return v.explanation.value != '';
			});
			newRows.forEach(function(v, i, a) {
				//修改摘要为可录入
				//v.explanation.value=v.explanation.display;
				if(!v.flag){
					v.price.value='';
					v.debitquantity.value='';
					if(v.creditquantity){
						v.creditquantity.value='';
					}
				}
				delete v.cashflow
				delete v.ass
			});
			saveData.details=newRows;
		}
		//自动获取的凭证号置0操作
		let saveDataClone = deepClone(saveData);
		if (props.novAuto == true) {
			saveDataClone.num.value = '0';
			saveDataClone.num.display = '0';
		}

		ajax({
			url,
			data:saveData,
			success: function(response) {
				//会计期间赋值 保存ts pk
				if (response.data) {
					let { voucher,paraInfo }=response.data;
					let {NC001,NC002,isShowUnit,excrate2,excrate3,unit }=paraInfo;
					let freeValue=[];
					toast({ content: this.state.json['20021005card-000093'], color: 'success' });/* 国际化处理： 保存成功*/
					if(saveNumber=='2'){//保存新增带出默认值
						let valueArr=[];
						let pk_accountingbook={
							display:voucher.pk_accountingbook.display,
							value:voucher.pk_accountingbook.value
						}
						let pk_vouchertype={
							display:voucher.pk_vouchertype.display,
							value:voucher.pk_vouchertype.value
						}
						let num={
							value:voucher.num.value
						}
						let newValue= Object.assign({"pk_accountingbook":pk_accountingbook},
							{"pk_vouchertype":pk_vouchertype},{"num":num})
						lastMessage.push(newValue)
						let  oldValue=JSON.parse( getGlobalStorage('sessionStorage', 'loadValue'));
						if(oldValue&&oldValue.length!=0){
							// sessionStorage.clear();
							removeGlobalStorage('sessionStorage');
							setGlobalStorage('sessionStorage',"loadValue",JSON.stringify(lastMessage));
						}else{
							setGlobalStorage('sessionStorage',"loadValue",JSON.stringify(lastMessage));
						}	
						extend.linkTo('../../../gl_voucher/pages/voucher/index.html'+window.location.search)
					}
					voucher.details.map((item,i)=>{
						item.key=++i
						if(NC001){
							item.groupType=NC001
						}
						if(NC002){
						   item.globalType=NC002
						}
						if(item.price.value&&item.price.value!='0'){
							item.flag=true;
							if(item.creditquantity.value){//借贷数量同步显示
								item.debitquantity.value=item.creditquantity.value;
								item.creditquantity.value='';
							}
						}
						if(item.creditamount.value&&item.creditamount.value!='0'){
							item.amount={
								value:item.creditamount.value
							}
						}
						if(item.debitamount.value&&item.debitamount.value!='0'){
							item.amount={
								value:item.debitamount.value
							}
						}
						//保存成功后添加子表字段结算字段
						if(item.settle&&!item.settle.value){
							freeValue=freeValue.filter((v,i,a)=>{
								return!settkleArray.includes(v.attrcode)
							})
						}
						if(item.verify&&!item.verify.value){
							freeValue=freeValue.filter((v,i,a)=>{
								return!verifyArray.includes(v.attrcode)
							})
						}
						item.childform=freeValue
					})
					evidenceData.rows=voucher.details;
					evidenceData.index=voucher.details.length;
					if (saveData.num.value != voucher.num.value ) {
						toast({ content: this.state.json['20021005card-000067'], color: 'warning' });/* 国际化处理： 请注意，凭证号已更新*/
					}
					saveData = voucher;
					evidenceData.localcreditamountTotle.value=voucher.totalcredit.display;
					evidenceData.localdebitamountTotle.value=voucher.totaldebit.display;
					saveData.period.value = voucher.year.value + '-' + voucher.period.value;
					//update = true;
					self.setState({
						evidenceData,
						saveData
					},()=>{
						self.props.saveUpdate(self.state.saveData)
					});
				} else {
					toast({ content: voucher.error.message, color: 'warning',duration: 30000});
				}
			}
        });


		// axios
		// 	.post(url, saveData)
		// 	.then(function(response) {
		// 		//会计期间赋值 保存ts pk
		// 		self.setState({
		// 			isLoading: false
		// 		});
		// 		if (response.data.success) {
		// 			toast({ content: '保存成功', color: 'success' });
		// 			if(saveNumber=='2'){
		// 				 window.location.href = './welcome.html';
		// 			}
		// 			response.data.data.details.map((item,i)=>{
		// 				item.key=++i
		// 				if(item.price.value&&item.price.value!='0'){
		// 					item.flag=true;
		// 					if(item.creditquantity.value){//借贷数量同步显示
		// 						item.debitquantity.value=item.creditquantity.value;
		// 						item.creditquantity.value='';
		// 					}
		// 				}
		// 			})
		// 			evidenceData.rows=response.data.data.details;
		// 			evidenceData.index=response.data.data.details.length;
		// 			if (saveData.num.value != response.data.data.num.value ) {
		// 				toast({ content: '请注意，凭证号已更新', color: 'warning' });
		// 			}
		// 			saveData = response.data.data;
		// 			evidenceData.localcreditamountTotle.value=response.data.data.totalcredit.display;
		// 			evidenceData.localdebitamountTotle.value=response.data.data.totaldebit.display;
		// 			saveData.period.value = response.data.data.year.value + '-' + response.data.data.period.value;
		// 			update = true;
		// 			self.setState({
		// 				evidenceData,
		// 				update,
		// 				saveData
		// 			});
		// 		} else {
		// 			toast({ content: response.data.error.message, color: 'warning',duration: 30000});
		// 		}
		// 	})
		// 	.catch(function(error) {
		// 		self.setState({
		// 			isLoading: false
		// 		});
		// 		toast({ content: error.message, color: 'danger' });
		// 	});

}
