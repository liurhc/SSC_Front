import {ajax,deepClone} from 'nc-lightapp-front';
import { toast,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
//const deepClone = require('../../../../public/components/deepClone');

export default function voucherSelectAction(props,state,key,parentFunc){
	//凭证下拉按钮
		if (update) { return false;}
		let self = this;
		let { update, copyStatus, evidenceData, saveData, errorFlag,abandonShow,voucherView } =props;
		let { num, pk_voucher, period, pk_casher, pk_checked, pk_manager, pk_prepared, pk_system,
			prepareddate, tempsaveflag, creator, title } = saveData;
		//let date= new Date();
		//let currentDate=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
		if (key == "errcorr") {//纠错 vouchervo的errmessage清空 errmessageeh保留  分录不需要管
			if(saveData.errmessage){
				saveData.errmessage.value = "";
			}
			// 
			// saveData.errmessageeh.value = "";
			// errorFlag = '';
			abandonShow = "N";
			self.setState({
				abandonShow:abandonShow,
				saveData
			},()=>{
				parentFunc.abandUpdate(abandonShow)
			});
		} else if (key == "signmistake") {//标错 vouchervo 的 errmessage  errmessageeh都设置    分录标错  voucherdetail的 errmessage就行了 errmessageeh好像没啥用
			let { SaveErrorModalShow, isSingleItem } =state;
			SaveErrorModalShow = true;
			isSingleItem = 'N'
			self.setState({
				 saveData,
				SaveErrorModalShow,
				isSingleItem
			});
		} else if (`${key}` == "10") {
			window.location.href = '../account.html';
		} else if (key == "abandon") {//作废
			let url = '/nccloud/gl/voucher/abandon.do';
			let pk_voucherValue = pk_voucher.value;
			let param = { "pk_voucher": pk_voucherValue };
			ajax({
				url: url,
				data: param,
				async: true,
				success: (response) => {
					let { error, success } = response.data;
					if (success) {
						toast({ content: this.state.json['20021005card-000096'], color: 'success' });/* 国际化处理： 当前凭证作废处理操作成功*/
						saveData =success;
						self.state.abandonShow = "Y";
						self.setState({
							abandonShow: props.abandonShow,
							 saveData
						})
					} else {
						toast({ content: error, color: 'warning' });
						self.setState({
							isLoading: false
						});
					}
				},
				error: (res) => {
					toast({ content: this.state.json['20021005card-000097'], color: 'danger' });/* 国际化处理： 后台报错,请联系管理员*/
				}
			});
		} else if (key== "unabandon") {//取消作废
			let url = '/nccloud/gl/voucher/unabandon.do';
			let pk_voucherValue = pk_voucher.value;
			let param = { "pk_voucher": pk_voucherValue };
			ajax({
				url: url,
				data: param,
				async: true,
				success: (response) => {
					let { error, success } = response.data;
					if (success) {
						toast({ content: this.state.json['20021005card-000098'], color: 'success' });/* 国际化处理： 当前凭证取消作废处理操作成功*/
						saveData = success;						
						self.state.abandonShow = "N";
						self.setState({
							abandonShow: props.abandonShow,
							 saveData
						})
					} else {
						toast({ content: error, color: 'warning' });
						self.setState({
							isLoading: false
						});
					}
				},
				error: (res) => {
					toast({ content: this.state.json['20021005card-000097'], color: 'danger' });/* 国际化处理： 后台报错,请联系管理员*/
				}
			});
		} else if (key == "cashflow") {//现金流量
			let { CashFlowModalShow }=state;
			CashFlowModalShow=true
			self.setState({
				CashFlowModalShow
			})
		}else if(key=="delete"){//删除凭证，显示前一张凭证,如果没有前一张就显示空凭证
			let url = '/nccloud/gl/voucher/delete.do';
			//let pk_voucherValue=[pk_voucher.value];
			let param={"pk_voucher":pk_voucher.value};

			ajax({
				url,
				data: param,
				async: true,
				success: (response) => {
					toast({ content: this.state.json['20021005card-000081'], color: 'success' });/* 国际化处理： 删除成功*/
						let searchData = JSON.parse(getGlobalStorage('sessionStorage', 'allpks'));
						let localUrl=window.location.href;
						if(localUrl.indexOf('?')!=-1){
							let idName=localUrl.split('#')[1].split('&')[1].split('=')[1];
							for(let i=0,len=searchData.length;i<len;i++){
								if(searchData[i]==idName){
									if((i-1)>0||(i-1)==0){
										let searchId= {
											pk_voucher:searchData[i-1]
										};
										let urllist = '/nccloud/gl/voucher/query.do';
										parentFunc.updatePage(searchId,urllist);
									}else{
									//	Message.create({content: '已经是第一条', color: 'warning'});
										// toast({ content: '已经是第一条', color: 'warning' });
									}
								}
							}
						}

				}
			})
		}else if (key == "copy") {//复制
			 // let copyVoucherData=self.state.saveData;
			parentFunc.copyState();
		}else if (key=="verify") {
			let { VrifyModalShow }=state;
			VrifyModalShow=true
			self.setState({
				VrifyModalShow
			})
		}


}
