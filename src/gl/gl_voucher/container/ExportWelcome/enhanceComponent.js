import React, { Component } from 'react';
import { base, ajax,high } from 'nc-lightapp-front';
//import moment from 'moment';
//import { Panel,Menu,ButtonGroup} from 'tinper-bee';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,NCAutoComplete:AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCMenu:Menu,NCButtonGroup:ButtonGroup
} = base;
const { Refer } = high;

export const Enhance= (ComposedComponent)=> class extends Component{
    

    render(){
        return<ComposedComponent {...this.props} />
    }
}
