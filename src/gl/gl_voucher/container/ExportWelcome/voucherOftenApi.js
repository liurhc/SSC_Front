export function emptyEnter(code, originData) {
	switch (code) {
		case 'localcreditamount':
			if (originData[code].value) {
				if (originData.groupdebitamount) {
					originData.groupdebitamount = {
						//集团空格
						...originData.groupdebitamount,
						value: originData.groupcreditamount && originData.groupcreditamount.value
					};
				}
				if (originData.groupcreditamount) {
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: ''
					};
				}

				if (originData.globaldebitamount) {
					originData.globaldebitamount = {
						//全局
						...originData.globaldebitamount,
						value: originData.globalcreditamount && originData.globalcreditamount.value
					};
				}

				if (originData.globalcreditamount) {
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: ''
					};
				}

				originData.localdebitamount = {
					...originData.localdebitamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
			} else {
				if (originData.groupcreditamount) {
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: originData.groupdebitamount && originData.groupdebitamount.value
					};
				}
				if (originData.groupdebitamount) {
					originData.groupdebitamount = {
						//集团空格
						...originData.groupdebitamount,
						value: ''
					};
				}

				if (originData.globalcreditamount) {
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: originData.globaldebitamount && originData.globaldebitamount.value
					};
				}

				if (originData.globaldebitamount) {
					originData.globaldebitamount = {
						//全局
						...originData.globaldebitamount,
						value: ''
					};
				}

				originData[code] = {
					...originData[code],
					value: originData.localdebitamount.value
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: ''
				};
			}
			break;
		case 'localdebitamount':
			if (originData[code].value) {
				if (originData.groupcreditamount) {
					//组织集团缺乏判断后续添加
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: originData.groupdebitamount && originData.groupdebitamount.value
					};
				}

				if (originData.groupdebitamount) {
					originData.groupdebitamount = {
						...originData.groupdebitamount,
						value: ''
					};
				}

				if (originData.globalcreditamount) {
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: originData.globaldebitamount && originData.globaldebitamount.value
					};
				}

				if (originData.globaldebitamount) {
					originData.globaldebitamount = {
						...originData.globaldebitamount,
						value: ''
					};
				}
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
			} else {
				if (originData.groupdebitamount) {
					originData.groupdebitamount = {
						//集团空格
						...originData.groupdebitamount,
						value: originData.groupcreditamount && originData.groupcreditamount.value
					};
				}
				if (originData.groupcreditamount) {
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: ''
					};
				}

				if (originData.globaldebitamount) {
					originData.globaldebitamount = {
						//全局
						...originData.globaldebitamount,
						value: originData.globalcreditamount && originData.globalcreditamount.value
					};
				}
				if (originData.globalcreditamount) {
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: ''
					};
				}

				originData[code] = {
					...originData[code],
					value: originData.localcreditamount.value
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: ''
				};
			}
			break;

		case 'groupcreditamount':
			if (originData[code].value) {
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: originData.localcreditamount && originData.localcreditamount.value
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: ''
				};
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: originData.globalcreditamount && originData.globalcreditamount.value
				};
				originData.globalcreditamount = {
					...originData.globalcreditamount,
					value: ''
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
			} else {
				originData[code] = {
					...originData[code],
					value: originData.groupdebitamount && originData.groupdebitamount.value
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: ''
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: originData.localdebitamount && originData.localdebitamount.value
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: ''
				};
				originData.globalcreditamount = {
					...originData.globalcreditamount,
					value: originData.globaldebitamount && originData.globaldebitamount.value
				};
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: ''
				};
			}
			break;
		case 'groupdebitamount':
			if (originData[code].value) {
				originData.groupcreditamount = {
					...originData.groupcreditamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: originData.localdebitamount && originData.localdebitamount.value
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: ''
				};
				originData.globalcreditamount = {
					//全局
					...originData.globalcreditamount,
					value: originData.globaldebitamount && originData.globaldebitamount.value
				};
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: ''
				};
			} else {
				originData[code] = {
					...originData[code],
					value: originData.groupcreditamount && originData.groupcreditamount.value
				};
				originData.groupcreditamount = {
					...originData.groupcreditamount,
					value: ''
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: originData.localcreditamount && originData.localcreditamount.value
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: ''
				};
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: originData.globalcreditamount && originData.globalcreditamount.value
				};

				originData.globalcreditamount = {
					//全局
					...originData.globalcreditamount,
					value: ''
				};
			}

			break;
		case 'globalcreditamount':
			if (originData[code].value) {
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: originData.localcreditamount && originData.localcreditamount.value
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: ''
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: originData.groupcreditamount && originData.groupcreditamount.value
				};
				originData.groupcreditamount = {
					...originData.groupcreditamount,
					value: ''
				};
			} else {
				originData[code] = {
					...originData[code],
					value: originData.globaldebitamount && originData.globaldebitamount.value
				};
				originData.globaldebitamount = {
					...originData.globaldebitamount,
					value: ''
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: originData.localdebitamount && originData.localdebitamount.value
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: ''
				};
				originData.groupcreditamount = {
					...originData.groupcreditamount,
					value: originData.groupdebitamount && originData.groupdebitamount.value
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: ''
				};
			}
			break;

		case 'globaldebitamount':
			if (originData[code].value) {
				originData.globalcreditamount = {
					...originData.globalcreditamount,
					value: originData[code].value
				};
				originData[code] = {
					...originData[code],
					value: ''
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: originData.localdebitamount && originData.localdebitamount.value
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: ''
				};
				originData.groupcreditamount = {
					//全局
					...originData.groupcreditamount,
					value: originData.groupdebitamount && originData.groupdebitamount.value
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: ''
				};
			} else {
				originData[code] = {
					...originData[code],
					value: originData.globalcreditamount && originData.globalcreditamount.value
				};
				originData.globalcreditamount = {
					...originData.globalcreditamount,
					value: ''
				};
				originData.localdebitamount = {
					...originData.localdebitamount,
					value: originData.localcreditamount && originData.localcreditamount.value
				};
				originData.localcreditamount = {
					...originData.localcreditamount,
					value: ''
				};
				originData.groupdebitamount = {
					...originData.groupdebitamount,
					value: originData.groupcreditamount && originData.groupcreditamount.value
				};
				originData.groupcreditamount = {
					//全局
					...originData.groupcreditamount,
					value: ''
				};
			}
			break;

		default:
			break;
	}
}

export function buttonStatusDetails(props, params, saveData) {
	if (params) {
		if (saveData && saveData.detailmodflag && saveData.detailmodflag.value === false) {
			//若为false 增删按钮不可用
			props.button.setButtonDisabled({
				deline: true,
				copyline: true,
				cutline: true,
				pasteline: true,
				linkgroup: false,
				linkbal: false,
				linksquence: false,
				linkbudget: false,
				bodyattach: false,
				verify: false
			});
			//props.button.setButtonDisabled(['','','','',''], false);
		} else {
			if (saveData.pk_accountingbook.value) {
				props.button.setButtonDisabled(
					[
						'deline',
						'bodyattach',
						'signmistakeb',
						'addinfo',
						'copyline',
						'cutline',
						'pasteline',
						'linkgroup',
						'linkbal',
						'linksquence',
						'linkbudget',
						'verify'
					],
					false
				);
			}
		}
	} else {
		if (saveData.pk_accountingbook.value) {
			props.button.setButtonDisabled(
				[
					'deline',
					'bodyattach',
					'signmistakeb',
					'addinfo',
					'copyline',
					'cutline',
					'pasteline',
					'linkgroup',
					'linkbal',
					'linksquence',
					'linkbudget',
					'verify'
				],
				true
			);
		} else {
			props.button.setButtonDisabled(
				[
					'addline',
					'deline',
					'bodyattach',
					'signmistakeb',
					'addinfo',
					'copyline',
					'cutline',
					'pasteline',
					'linkgroup',
					'linkbal',
					'linksquence',
					'linkbudget',
					'verify'
				],
				true
			);
		}
	}
}

export function countRow(originData, countValue, key) {
	//跟新计算后的值
	let keyString;
	if (key != 'amount') {
		if (key == 'globalcreditamount' || key == 'globaldebitamount') {
			keyString = key.substring(6);
		} else {
			keyString = key.substring(5);
		}
	} else {
		keyString = 'amount';
	}
	for (let item in countValue) {
		if (
			originData.hasOwnProperty(item) ||
			item.indexOf('localamount') != -1 ||
			item.indexOf('groupamount') != -1 ||
			item.indexOf('globalamount') != -1 ||
			item.indexOf('quantity') != -1 ||
			item.indexOf('price') != -1
		) {
			if (item == 'quantity' && originData.debitquantity) {
				//数量特殊处理
				originData.debitquantity = {
					...originData.debitquantity,
					value: countValue.quantity
				};
			} else {
				originData[item] = {
					...originData[item],
					value: countValue[item]
				};
			}
			if (item == 'localamount') {
				if (keyString != 'creditamount' && keyString != 'debitamount') {
					if (originData.localcreditamount.value) {
						originData.localcreditamount = {
							...originData.localcreditamount,
							value: countValue[item]
						};
					} else {
						originData.localdebitamount = {
							...originData.localdebitamount,
							value: countValue[item]
						};
					}
				} else {
					if (keyString == 'creditamount') {
						originData.localcreditamount = {
							...originData.localcreditamount,
							value: countValue[item]
						};
						originData.localdebitamount = {
							...originData.localdebitamount,
							value: ''
						};
					} else {
						originData.localdebitamount = {
							...originData.localdebitamount,
							value: countValue[item]
						};
						originData.localcreditamount = {
							...originData.localcreditamount,
							value: ''
						};
					}
				}
			}
			if (item == 'groupamount') {
				if (keyString != 'creditamount' && keyString != 'debitamount') {
					if (originData.direction && originData.direction.value == 'C') {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: countValue[item]
						};
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: ''
						};
					} else {
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: countValue[item]
						};
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: ''
						};
					}
				} else {
					if (keyString == 'creditamount') {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: countValue[item]
						};
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: ''
						};
					} else {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: ''
						};
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: countValue[item]
						};
					}
				}
			}
			if (item == 'globalamount') {
				if (keyString != 'creditamount' && keyString != 'debitamount') {
					if (originData.direction && originData.direction.value == 'C') {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: countValue[item]
						};
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: ''
						};
					} else {
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: countValue[item]
						};
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: ''
						};
					}
				} else {
					if (keyString == 'creditamount') {
						//全局特殊处理
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: countValue[item]
						};
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: ''
						};
					} else {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: ''
						};
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: countValue[item]
						};
					}
				}
			}
		}
	}
}

// 比较两个值(基本类型值或对象）是否一样
export function compare(a, b) {
	if (typeof a === 'object' && typeof b === 'object') {
		if (a && b) {
			// 去掉undefined
			a = JSON.parse(JSON.stringify(a));
			b = JSON.parse(JSON.stringify(b));
			var akeys = Object.keys(a),
				bkeys = Object.keys(b);
			if (akeys.length === bkeys.length) {
				for (var i = 0, l = akeys.length; i < l; i++) {
					var flag = compare(a[akeys[i]], b[akeys[i]]);
					if (!flag) {
						return false;
					}
				}
				return true;
			} else {
				return false;
			}
		} else {
			return Object.is(a, b);
		}
	} else {
		return a == b;
	}
}
