import React, { Component } from 'react';
import { createPage, high, base, ajax, deepClone, toast, cardCache ,sum,getMultiLang} from 'nc-lightapp-front';
import {
	CheckboxItem,
	RadioItem,
	TextAreaItem,
	ReferItem,
	SelectItem,
	InputItem,
	DateTimePickerItem
} from '../../../public/components/FormItems'; 
import ChangeNumberModal from '../ChangeNumberModal'; //修改汇率
import CheckOrNot from '../CheckOrNot'; //确认模态框
import FormItemTab from '../Welcome/FormItemTab.js';
import Rate from '../Welcome/rate.js';
import { isObj } from '../../../public/components/oftenApi';
import AssidModal from '../../../public/components/assidDbModal';
import ReferLoader from '../../../public/ReferLoader';
import { emptyEnter,buttonStatusDetails,countRow } from "../Welcome/voucherOftenApi";
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCTooltip,
	NCAutoComplete: AutoComplete,
	NCDropdown: Dropdown,
	NCPanel: Panel,
	NCForm: Form,
	NCButtonGroup: ButtonGroup
} = base;
import amountconvert from '../../amountConvert';
import {queryCurrinfo} from '../../amountConvert/currInfo.js';
import './index.less';
//引入高级组件
import { Enhance } from './enhanceComponent.js';
const Option = Select.Option;
const format = 'YYYY-MM-DD';
const styleType = {
	marginRight: 5
};

const detailExpend = [
	'bankaccount',
	'billtype',
	'checkstyle',
	'checkno',
	'checkdate',
	'verifyno',
	'verifydate',
	'cashflow'
];

const settkleArray = [ 'bankaccount', 'billtype', 'checkstyle', 'checkno', 'checkdate' ];
const verifyArray = [ 'verifyno', 'verifydate' ];
class Welcome extends Component {
	static defaultProps = {
		multiSelect: {
			type: 'checkbox',
			param: 'key'
		}
	};
	constructor(props) {
		//每定义一个state 都要添加备注
		super(props);
		this.state = {
            json:props.json,
			currentRowPrevass:[],//当前行的前一行的辅助核算
			coordinationStatus: 'uncoordination', //协同修改状态coordination,非协同状态uncoordination
			datavoucher: { voucher: {}, paraInfo: {} }, //外界传入的凭证数据
			currentRow: {
				currentBatchNo: '', //当前选中行的batchNo
				currentRowId: '',
				rowCount: '' //总行数
			},
			// title: this.state.json['20021005card-000064'],/* 国际化处理： 记账凭证*/
			showUploader: false,
			target: null,
			referDom: '',
			referDom2: '',
			headFunc: {
				checkNot: false, //是否保存莫泰框
				pk_accountingbook: {
					//核算账簿
					display: '',
					value: ''
				}
			}, //存储表头参照
			checkedAll: false, //分录复选框
			checkedArray: [ false, false, false, false ],
			lastMessage: [],
			unitOrg: {
				//业务单元过滤
				value: ''
			},
			errorFlag: '',
			saveNumber: '', //保存新增标识
			update: false, //修改太页面标识
			getrow: '', //获取指定行的Key
			expandRow: [], //展开子表数据
			open: false, //footer默认不展示
			index: '', //摘要信息下标
			findRows: [], //修改汇率选定行数据
			accIndex: '', //辅助核算下标
			assList: [], //根据会计科目和制单日期获取的辅助核算数据
			freeValue: [], //存储分录自定义项
			MsgModalAll: false, //控制常用模态框显隐
			SubjectModalShow: false, //科目模态框
			AssistAccModalShow: false, //辅助核算模态框
			ExplanationModalShow: false, //摘要模态框
			changeNumberShow: false, //修改汇率模态框
			apportionmentShow: false, //快速分摊模态框
			VrifyModalShow: false, //及时核销模态框
			SaveErrorModalShow: false, //标错模态框
			novAuto: true, //凭证号是否自动获取的，true是自动，false为手动填的
			id: '',
			isadd: false, //根据核算账簿动态增列判断是否已经新增
			isNC01: false, //根据核算账簿动态增列判断是否已经新增集团借贷
			isNC02: false, //根据核算账簿动态增列判断是否已经新增全局借贷
			checkForm: false, //保存数据控制
			//参照依赖核算账簿值获取

			prepareddate: {
				value: ''
			},
			nov: {
				//凭证号
				value: ''
			},
			startdate: {
				//制单日期
			},
			attachment: {
				//附单据数,
				value: '0'
			},
			period: {
				//会计期间
			},
			billmaker: {
				//制单人
			},
			approver: {
				//审核人
			},
			saveData: {
				//保存数据整理
				details: []
			},
			headContent: [], //表头数据
			footContent: [], //表尾数据
			evidenceColumns: [], //表体显示列
			evidenceData: {
				//表体数据
				rows: [
					{
						key: 1,
						checkedNumber: false,
						isEdit: true,
						userdata: { display: null, value: null, scale: null },
						excrate2: {
							value: ''
						},
						excrate3: {
							value: ''
						},
						excrate4: {
							value: ''
                        },
                        excrate2scale:{
                            value:''
                        },
                        excrate3scale:{
                            value:''
                        },
                        excrate4scale:{
                            value:''
                        },
                        pricescale:{
                            value:''
                        },
						childform: []
					},
				],
				index: 1,
				localdebitamountTotle: {
					//借方合计
					value: ''
				},
				localcreditamountTotle: {
					//贷方合计
					value: ''
				},

				groupdebitamountTotle: {
					//组织借方合计
					value: ''
				},
				groupcreditamountTotle: {
					//组织贷方合计
					value: ''
				},

				globaldebitamountTotle: {
					//集团借方合计
					value: ''
				},
				globalcreditamountTotle: {
					//集团贷方合计
					value: ''
				},
				newLine: {
					isEdit: true,
					excrate2: {
						value: ''
					},
					creditamount: { value: '' },
					debitamount: { value: '' },
					excrate3: {
						value: ''
					},
					excrate4: {
						value: ''
                    },
                    excrate2scale:{
                        value:''
                    },
                    excrate3scale:{
                        value:''
                    },
                    excrate4scale:{
                        value:''
                    },
                    pricescale:{
                        value:''
                    },
					childform: [
						{
							p_location: {
								//所在地
								value: '',
								display: '',
								scale: -1
							},
							maxpledge: {
								//可质押价值
								value: '',
								display: '',
								scale: -1
							}
						}
					]
				}
			},
			options: [ ],
			placeholder: '',
			disabled: false, //可编辑性控制
			copyData: {}, //复制的数据
			copyRecord: '', //复制的key
			voucherStatus: 'save', //凭证单状态 update：修改态，save：保存态
			voucherView: false, //凭证编辑太和修改太唯一标识
			copyStatus: '', //复制的凭证copy
			abandonShow: 'N', //Y是作废凭证，N取消作废
			detailIndex: '', //标错时判断行号
			accountData: [] //会计科目数据
		};
	}

	componentWillMount() {
		// let callback= (json) =>{
		// 	this.setState({json:json},()=>{
		// 	//	initTemplate.call(this, this.props, this.state);
		// 	})
		// }
		// getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}

	expandNode = () => {
		let { getrow, expandRow } = this.state;
		if (getrow) {
			expandRow.push(getrow);
			this.setState({
				expandRow
			});
		}
	};
	componentWillReceiveProps(nextProps) {
		let self = this;
		let {headFunc}=self.state;
		// let {evidenceData}=self.state;
		if (nextProps.datavoucher.voucher && nextProps.datavoucher.voucher != self.state.datavoucher.voucher) {
			let { voucher, paraInfo } = nextProps.datavoucher;
			headFunc.pk_accountingbook.value =voucher.pk_accountingbook.value;
			voucher.details.map((item, index) => {
				// if (item.userdata.value) {
					item.rowEdited = true;
				// } else {
				// 	item.rowEdited = false;
				// }
				if(item.busidate){
					if(!item.busidate.value){
						if(item.expand&&item.expand.verifydate&&item.expand.verifydate.value){
							item.busidate.value=item.expand.verifydate.value;
							item.busidate.display=item.expand.verifydate.value;
						}else{
							item.busidate.value=item.prepareddate.value.split(' ')[0];
							item.busidate.display=item.prepareddate.value.split(' ')[0];
						}
					}
					
				}else{
					item.busidate={
						value:item.prepareddate.value.split(' ')[0],
						display:item.prepareddate.value.split(' ')[0]}
				}
			});
			self.setState(
				{
					coordinationStatus: nextProps.coordinationStatus,
					currentRow: nextProps.currentRow,
					datavoucher: nextProps.datavoucher,
					voucherView: nextProps.voucherView,
					headFunc
				});
		}
	}
	componentWillMount() {
		let pk_appregister = '20020PREPA'; //this.props.getUrlParam('appcode')||this.props.getSearchParam('c');
		let { id } = this.state;
		let type;
		this.searchById(pk_appregister).then((datapk) => {
			let self = this;
			let { headFunc } = self.state;
			let propsType = 'edit'; // self.props.getUrlParam('status');
			if (propsType == 'edit') {
				//表格分录过滤添加
				headFunc.pk_accountingbook.value =self.props.datavoucher.voucher.pk_accountingbook.value //datapk;
				self.setState(
					{
						voucherView: propsType == 'browse' ? true : false,
						headFunc
					},
					() => {
						self.loadVoucher(self.props.datavoucher.voucher, self.props.datavoucher.paraInfo);
					}
				);
			}
		});
	}
	componentDidMount() {
		let self = this;
		let {saveData} = self.state;
		self.props.onRef(self);
		// self.loadVoucher(self.props.datavoucher.voucher, self.props.datavoucher.paraInfo);
		// this.buttonStatus()//按钮状态控制
		// document.body.addEventListener(
		// 	'keydown',
		// 	function(e) {
		// 		if (e.keyCode == 113) {
		// 			e.preventDefault();
		// 			self.expandNode();
		// 		}

		// 		if (e.keyCode == 83 && e.ctrlKey) {
		// 			e.preventDefault();
		// 			//self.handleSave()
		// 		}

		// 		if (e.keyCode == 83 && e.altKey) {
		// 			e.preventDefault();
		// 			//self.handleAllways()
		// 		}

		// 		if (e.keyCode == 73 && e.ctrlKey) {
		// 			e.preventDefault();
		// 			//增行')
		// 			self.handleAdd();
		// 		}

		// 		if (e.keyCode == 68 && e.ctrlKey) {
		// 			e.preventDefault();
		// 			//'删行')
		// 			self.handleCancel();
		// 		}

		// 		if (e.keyCode == 67 && e.altKey) {
		// 			e.preventDefault();
		// 			//复制')
		// 			self.handleCopy();
		// 		}

		// 		if (e.keyCode == 86 && e.altKey) {
		// 			e.preventDefault();
		// 			//粘贴')
		// 			self.handlePaste();
		// 		}
		// 	},
		// 	false
		// );

		// let tit=document.getElementById('showTime');
		// let rect=tit.getBoundingClientRect()//获取页面导航条相对浏览器视窗的位置
		// let inDiv=document.createElement('div');
		// tit.parentNode.replaceChild(inDiv,tit);
		// inDiv.appendChild(tit);
		// inDiv.style.height=rect.height+'px';
		// let titTop=tit.offsetTop;//获取区域距离页面顶部的距离
		//滚动事件监听
		// document.onscroll = function() {
		// 	let btop = document.body.scrollTop || document.documentElement.scrollTop;
		// 	if (btop > titTop) {
		// 		tit.className = 'clearfix fix';
		// 	} else {
		// 		tit.className = 'clearfix';
		// 	}
		// };
	}

	componentWillUnmount() {
		//组件卸载移除DOM 原生事件,防止内存泄漏
		//document.body.removeEventListener("keydown")
		//document.body.removeEventListener("keydown")
	}

	//根据模板渲染界面请求
	searchById = (pk_appregister) => {
		let self = this;
		let { voucherView } = self.state;
		let url = '/nccloud/platform/pub/mergerequest.do';
		let rqJson = { pagecode: '20021005card', appcode: pk_appregister };
		let data = [
			{
				rqCode: 'template',
				rqJson: JSON.stringify(rqJson),
				rqUrl: '/platform/templet/querypage.do'
			},
			{
				rqCode: 'button',
				rqJson: JSON.stringify(rqJson),
				rqUrl: '/platform/appregister/queryallbtns.do'
			},
			{
				rqCode: 'context',
				rqJson: JSON.stringify(rqJson),
				rqUrl: '/platform/appregister/queryappcontext.do'
			}
		];
		let p1 = new Promise((resolve, reject) => {
			ajax({
				url,
				data,
				success: function(response) {
					//核算账簿赋默认值
					let { data } = response;
					if (data.button) {
						let button = data.button;
						self.props.button.setButtons(button);
					}
					self.echoData(response.data);
					if (data.template) {
						let meta = data.template;
						modifierMeta(self.props, meta, data.context.defaultAccbookPk);
						self.props.meta.setMeta(meta);
					}
					resolve(data.context.defaultAccbookPk);
				}
			});
		});
		return p1;
	};
	updateRowScale = (response, key) => {
		//会计科目选中更新精度设置
		let { evidenceData } = this.state;
		if (Array.isArray(response.data)) {
			if (response.data.length == 1) {
				let {
					currinfo,
					excrate2,
					excrate3,
					excrate4,
					excrate2scale,
					excrate3scale,
					excrate4scale,
					NC001,
					NC002,
					scale,
					globalCurrinfo,
					globalmode,
					globalroundtype,
					globalscale,
					groupCurrinfo,
					groupmode,
					orgmode,
					grouproundtype,
					groupscale,
					orgroundtype,
					orgscale,
					quantityscale,
				} = response.data[0];
				if (scale) {
					evidenceData.rows[key].scale = scale;
				}
				if (NC001) {
					evidenceData.rows[key].groupType = NC001;
				}
				if (NC002) {
					evidenceData.rows[key].globalType = NC002;
				}
				if (excrate2||excrate2=='0') {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].excrate2 = {
						value: excrate2
					};
				}
				if (excrate3||excrate3=='0') {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].excrate3 = {
						value: excrate3
					};
				}
				if (excrate4||excrate4=='0') {
					evidenceData.rows[key].excrate4 = {
						value: excrate4
					};
				}
				if (excrate2scale) {
					evidenceData.rows[key].excrate2scale = {
						value: excrate2scale
					};
				}
				if (excrate3scale) {
					evidenceData.rows[key].excrate3scale = {
						value: excrate3scale
					};
				}
				if (excrate4scale) {
					evidenceData.rows[key].excrate4scale = {
						value: excrate4scale
					};
				}
				if (currinfo) {
					evidenceData.rows[key].pk_currtype = {
						display: currinfo.display,
						value: currinfo.value
					};
				}
				if (globalCurrinfo) {
					evidenceData.rows[key].globalCurrinfo = {
						display: globalCurrinfo.display,
						value: globalCurrinfo.value
					};
				}
				if (groupCurrinfo) {
					evidenceData.rows[key].groupCurrinfo = {
						display: groupCurrinfo.display,
						value: groupCurrinfo.value
					};
				}
				if (globalmode) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].globalmode = {
						value: globalmode
					};
				}
				if (globalroundtype) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].globalroundtype = {
						value: globalroundtype
					};
				}
				if (globalscale) {
					evidenceData.rows[key].globalscale = globalscale;
				}
				if (groupmode) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].groupmode = {
						value: groupmode
					};
				}
				if (grouproundtype) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].grouproundtype = {
						value: grouproundtype
					};
				}
				if (groupscale) {
					evidenceData.rows[key].groupscale = groupscale;
				}
				if (orgroundtype) {
					//根据核算账簿赋值默认汇率，默认币种，默认业务单元
					evidenceData.rows[key].orgroundtype = {
						value: orgroundtype
					};
				}
				if (orgscale) {
					evidenceData.rows[key].orgscale = orgscale;
				}
				if(quantityscale){
					evidenceData.rows[key].quantityscale = {
						value: quantityscale
					};
				}
				evidenceData.rows[key].orgmode = {
					value: orgmode
				};
				evidenceData.rows[key].groupmode = {
					value: groupmode
				};
				evidenceData.rows[key].globalmode = {
					value: globalmode
				};
			} else {
				response.data.map((item, index) => {
					if (JSON.stringify(item) != '{}') {
						evidenceData.rows.forEach((content, rIndex) => {
							if (key == rIndex) {
								content.scale = item.scale;
								content.groupType = item.NC001;
								content.globalType = item.NC002;
								content.excrate2 = {
									value: item.excrate2
								};
								content.excrate3 = {
									value: item.excrate3
								};
								content.excrate4 = {
									value: item.excrate4
								};
								content.excrate3scale = {
									value: item.excrate3scale
								};
								content.excrate4scale = {
									value: item.excrate4scale
								};
								content.excrate4scale = {
									value: item.excrate4scale
								};
								content.currinfo = {
									display:  item.currinfo&&item.currinfo.display,
									value: item.currinfo&&item.currinfo.value
								};
								content.groupCurrinfo = {
									display: item.groupCurrinfo ? item.groupCurrinfo.display : '',
									value: item.groupCurrinfo ? item.groupCurrinfo.value : ''
								};
								content.globalCurrinfo = {
									display: item.globalCurrinfo ? item.globalCurrinfo.display : '',
									value: item.globalCurrinfo ? item.globalCurrinfo.value : ''
								};
								content.globalmode = {
									value: item.globalmode
								};
								content.globalroundtype = {
									value: item.globalroundtype
								};
								content.globalscale = item.globalscale;
								content.groupmode = {
									value: item.groupmode
								};
								content.grouproundtype = {
									value: item.grouproundtype
								};
								content.groupscale = item.groupscale;

								content.quantityscale = {
									value: item.quantityscale
								};
								content.orgroundtype = {
									value: item.orgroundtype
								};
								content.orgmode={
									value: item.orgmode
								};
								content.groupmode={
									value: item.groupmode
								};
								content.globalmode={
									value: item.globalmode
								};
								content.orgscale = item.orgscale;
							}
						});
					}
					key++;
				});
			}
		}
		this.setState({
			evidenceData
		});
    };
    loadCurryData = (response) => {
		let self = this;
		let { saveData, evidenceData } = self.state;
		let { pk_currtype, excrate2, excrate3, excrate4, NC001, NC002, scale, num } = response.data;
		saveData.num = {
			display: num.toString(),
			value: num.toString()
		};
		//凭证号赋默认值
		self.props.form.setFormItemsValue(self.formId, { num: { value: response.data.num } });
		this.updateRows(evidenceData, response.data);
		self.setState({
			evidenceData,
			saveData
		});
	};
	getCurry(book, date, values, type, key) {
		var self = this;
		let url = '/nccloud/gl/voucher/voucherValueChange.do';
		let data = {
			key: type,
			value: values.value,
			pk_accountingbook: book,
			prepareddate: date
		};
		ajax({
			url,
			data,
			success: function(response) {
				if (response.data) {
					if (type == 'pk_vouchertype') {
						self.loadCurryData(response);
					} else {
						self.updateRowScale(response, key);
					}
				}
			}
		});
	}

	//凭证分录等号操作
	equal = (code, originData) => {
		let {
			rows,
			localcreditamountTotle,
			localdebitamountTotle,
			globalcreditamountTotle,
			globaldebitamountTotle,
			groupcreditamountTotle,
			groupdebitamountTotle
		} = this.state.evidenceData;
		switch (code) {
			case 'localcreditamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[code] = {
						...originData[code],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: localdebitamountTotle.value - localcreditamountTotle.value
					};
				} else {
					originData.localdebitamount = {
						...originData.localdebitamount,
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: localdebitamountTotle.value - localcreditamountTotle.value
					};
				}
				break;
			case 'localdebitamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[code] = {
						...originData[code],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: localcreditamountTotle.value - localdebitamountTotle.value
					};
				} else {
					originData.localcreditamount = {
						...originData.localcreditamount,
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: localcreditamountTotle.value - localdebitamountTotle.value
					};
				}
				break;

			case 'groupcreditamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[code] = {
						...originData[code],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: groupdebitamountTotle.value - groupcreditamountTotle.value
					};
				} else {
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: groupdebitamountTotle.value - groupcreditamountTotle.value
					};
					originData.groupdebitamount = {
						...originData.groupdebitamount,
						value: ''
					};
				}
				break;
			case 'groupdebitamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[code] = {
						...originData[code],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: groupcreditamountTotle.value - groupdebitamountTotle.value
					};
				} else {
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: groupcreditamountTotle.value - groupdebitamountTotle.value
					};
					originData.groupcreditamount = {
						...originData.groupcreditamount,
						value: ''
					};
				}
				break;
			case 'globalcreditamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[code] = {
						...originData[code],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: globaldebitamountTotle.value - globalcreditamountTotle.value
					};
				} else {
					this.totleCount(code, 'localdebitamountTotle');
					originData.globaldebitamount = {
						...originData.globaldebitamount,
						value: ''
					};
					originData[code] = {
						...originData[code],
						value: globaldebitamountTotle.value - globalcreditamountTotle.value
					};
				}
				break;
			case 'globaldebitamount':
				if (originData[code].value && originData[code].value != 0) {
					originData[item.attrcode] = {
						...originData[item.attrcode],
						value: ''
					};
					this.totleCount(code, 'localdebitamountTotle');
					originData[code] = {
						...originData[code],
						value: globalcreditamountTotle.value - globaldebitamountTotle.value
					};
				} else {
					this.totleCount(code, 'localdebitamountTotle');
					originData.globalcreditamount = {
						...originData.globalcreditamount,
						value: ''
					};
					originData[code] = {
						...originData[code],
						value: globalcreditamountTotle.value - globaldebitamountTotle.value
					};
				}
				break;
			default:
				break;
		}
	};

	//参数抽取

	exportData = (originData) => {
		let data = {
			amount: originData.amount ? originData.amount.value : '', //原币
			localamount:
			originData.localcreditamount&&originData.localcreditamount.value ||  originData.localdebitamount&&originData.localdebitamount.value
					? originData.localcreditamount.value || originData.localdebitamount.value
					: '', //组织本币
			groupamount:
			originData.groupcreditamount&&originData.groupcreditamount.value || originData.groupdebitamount&&originData.groupdebitamount.value
					? originData.groupcreditamount.value || originData.groupdebitamount.value
					: '', //集团本币
			globalamount:
			originData.globalcreditamount&&originData.globalcreditamount.value || originData.globaldebitamount&&originData.globaldebitamount.value
					? originData.globalcreditamount.value || originData.globaldebitamount.value
					: '', //全局本币
			pk_currtype: originData.pk_currtype.value, //原币币种
			orgcurrtype: originData.orgcurrtype ? originData.orgcurrtype.value : '', //本币币种
			groupcurrtype: originData.groupcurrtype ? originData.groupcurrtype.value : '', //集团本币币种
			globalcurrtype: originData.globalcurrtype ? originData.globalcurrtype.value : '', //全局本币币种
			excrate2: originData.excrate2 ? originData.excrate2.value : '', //本币汇率
			excrate3: originData.excrate3 ? originData.excrate3.value : '', //集团本币汇率
			excrate4: originData.excrate4 ? originData.excrate4.value : '', //全局本币汇率
			price: originData.price ? originData.price.value : '', //单价
			pk_accountingbook: originData.pk_accountingbook ? originData.pk_accountingbook.value : '', //账簿
			prepareddate: originData.prepareddate ? originData.prepareddate.value : '', //制单日期
			quantity: originData.debitquantity ? originData.debitquantity.value : '' //数量分借贷，后续处理
		};
		let currInfo = {
			roundtype: originData.roundtype ? originData.roundtype.value : '', //原币进舍规则
			orgroundtype: originData.orgroundtype ? originData.orgroundtype.value : '', //本币进舍规则
			grouproundtype: originData.grouproundtype ? originData.grouproundtype.value : '', //集团本币进舍规则
			globalroundtype: originData.globalroundtype ? originData.globalroundtype.value : '', //全局本币进舍规则
			pricescale: originData.pricescale ? originData.pricescale.value : '', //单价精度
			quantityscale: originData.debitquantity&&originData.debitquantity.scale ?originData.debitquantity.scale : originData.quantityscale&&originData.quantityscale.value, //数量精度
			priceroundtype: originData.priceroundtype ? originData.priceroundtype.value : '', //单价进舍规则
			scale: originData.scale ? originData.scale : '', //原币精度
			excrate2scale: originData.excrate2scale ? originData.excrate2scale.value : '', //本币汇率
			excrate3scale: originData.excrate3scale ? originData.excrate3scale.value : '', //本币汇率
			excrate4scale: originData.excrate4scale ? originData.excrate4scale.value : '', //本币汇率
			orgscale: originData.orgscale ? originData.orgscale : '', //组织本币精度
			groupscale: originData.groupscale ? originData.groupscale : '', //集团本币精度
			globalscale: originData.globalscale ? originData.globalscale : '', //全局本币精度
			maxconverr: originData.maxconverr ? originData.maxconverr.value : '',
			NC001: originData.groupType ? originData.groupType : '', //集团本币计算方式
			NC002: originData.globalType ? originData.globalType : '', //全局本币计算方式
			orgmode: originData.orgmode ? originData.orgmode.value : '', //汇率计算方式 是否为除 true=除
			groupmode: originData.groupmode
				? originData.groupmode.value === undefined ? '' : originData.groupmode.value
				: '', //集团本币汇率计算方式 是否为除 true=除
			globalmode: originData.globalmode
				? originData.globalmode.value === undefined ? '' : originData.globalmode.value
				: '' //全局本币汇率计算方式 是否为除 true=除
		};

		return { data, currInfo };
	};

	//根据模板数据渲染表格列
	editeColums(data) {
		let openRowCol = {
			title: '',
			key: 'open',
			dataIndex: 'open',
			width: '50px',
			fixed: "right",
			render: (text, record, index) => {
				let errorTip = <div>{record.errmessage ? record.errmessage.value : ''}</div>;
				let checkTip = <div>{record.checkmessage ? record.checkmessage.value : ''}</div>;
				return [
					record.checkmessage && record.checkmessage.value && (
						<NCTooltip trigger="hover" placement={'top'} inverse overlay={checkTip}>
							<i className="iconfont icon-zhuyi1" />
						</NCTooltip>
					),
					record.errmessage &&record.errmessage.value&& (
						<NCTooltip trigger="hover" placement={'top'} inverse overlay={errorTip}>
							<i className="iconfont icon-shibai" />
						</NCTooltip>
					)
				];
			}
		};
		let dataFirst = {
			title: this.state.json['20021005card-000065'],/* 国际化处理： 行号*/
			key: 'key',
			dataIndex: 'key',
            width: '80px',
            fixed: "left",
			render: (text, record, index) => <span  class='indexCode'>{index + 1}</span>
		};
		data.map((item, index) => {
			item.title = item.label;
			item.dataIndex = item.attrcode;
			item.width = '200px';
			item.render = (text, record, index) => {
				//根据字段类型，渲染不同的组件
				let self = this;
				let { rows } = self.state.evidenceData;
				let originData = self.findByKey(record.key, rows);
				let errorMsg, errorBorder;
				let defaultValue = {};
				let { pk_accountingbook, prepareddate, saveData, voucherView, evidenceColumns } = self.state;
				let { data, currInfo } = self.exportData(originData);
				if (text && text.value !== null) {
					defaultValue = { refname: text.display, refpk: text.value };
				}
				switch (item.itemtype) {
					case 'input':
						if (item.attrcode == 'explanation') {
							let { value } = originData.explanation;
							let { options, placeholder, voucherStatus } = self.state;
							return !voucherView ? (
								<div className="refer-input-wraper">
									<AutoComplete
										value={value}
										disabled={record.explanationEdited}
										//options={options}
										placeholder={placeholder}
										maxlength="200"
										onValueChange={(value) => {
											let { rows } = this.state.evidenceData;
											let originData = this.findByKey(record.key, rows);
											if (originData) {
												originData[item.attrcode] = {
													value: value
												};
											}
										}}
									/>
									{!record[item.attrcode].editable ? (
										<i
											disabled={
												record[item.attrcode].editable ? record[item.attrcode].editable : false
											}
											className="iconfont icon-canzhao"
											onClick={() => {
												this.explanationClick(record.key);
											}}
										/>
									) : null}
								</div>
							) : (
								<p class='accasoaClass'><span>{value}</span></p>
							);
						} else if (item.attrcode == 'assid') {
							return !voucherView&&!record.rowEdited ? (
								<div className="refer-input-wraper">
									<FormControl
										value={record.assid ? record.assid.display : ''}
										disabled={true}
										onFocus={this.focus}
										onChange={(v) => {
											let cloneNewLine = deepClone(evidenceColumns);
											cloneNewLine.splice(4, 0, number1);
											this.setState({
												evidenceColumns: cloneNewLine
											});
										}}
									/>
									{!record[item.attrcode].editable ? (
										<i
											className="iconfont icon-canzhaozuixin"
											onClick={this.handleAssistAcc.bind(this, record.key)}
										/>
									) : null}
								</div>
							) : (
								<p class='accasoaClass'><span>{record.assid ? record.assid.display : ''}</span></p>
							);
						}else{
							return <div>{ !voucherView&&!record.rowEdited ? (<FormControl
								value={text&&text.value && text.value != '0' ? text.value : ''}
								onChange={(v) => {
									let { evidenceData } = this.state;
									if (record) {
										record[item.attrcode]={
											value:v
										}
									}
									this.setState({
										evidenceData
									});
								}}
							/>):(
								<span>{text&&text.value}</span>
							)}</div>
						}
						break;
					case 'refer':
						let referUrl = item.refcode + '.js';
						let editable = record[item.attrcode].editable ? record[item.attrcode].editable : false;
						if (item.attrcode != 'pk_accasoa') {
							if (item.attrcode == 'pk_currtype') {
								//币种
								return !voucherView&&(!record['userdata']||!record['userdata'].value ? true : false)&&!record.rowEdited  ? (
									<ReferLoader
										tag={item.attrcode}
										refcode={referUrl}
										disabled={record['userdata']&&record['userdata'].value ? true : false}
										value={{ refname: text.display, refpk: text.value }}
										queryCondition={() => { 
											return {
												pk_accountingbook: self.state.headFunc.pk_accountingbook.value,
												pk_org: self.state.unitOrg.value
											};
										}}
										onChange={(v) => {
											let { prepareddate, evidenceData, headFunc } = this.state;
											let originData = this.findByKey(record.key, evidenceData.rows);
											if (originData) {
												originData[item.attrcode] = {
													display: v.refname,
													value: v.refpk
												};
											}
											//根据币种请求汇率
											if (item.attrcode == 'pk_currtype') {
												let url = '/nccloud/gl/glpub/ratequery.do';
												let pk_currtype = {
													pk_accountingbook: headFunc.pk_accountingbook.value,
													pk_currtype: v.refpk,
													prepareddate: prepareddate.value
												};
												if (headFunc.pk_accountingbook.value && v.refpk && prepareddate.value) {
													this.queryRate(pk_currtype, url, originData);
												}
											}
											//摘要联动
											let saveDataObj = JSON.parse(JSON.stringify(this.state.saveData));
											if (saveDataObj.explanation) {
												saveDataObj.explanation.value = this.state.evidenceData.rows[0].explanation.display;
											}
											this.setState({
												evidenceData,
												saveData: saveDataObj
											});
										}}
									/>
								) : (
									<p class='accasoaClass'><span>{text.display}</span></p>
								);
							} else {
								return !voucherView&&!record.rowEdited ? (
									<ReferLoader
										tag={item.attrcode}
										refcode={referUrl}
										disabled={record.rowEdited}
										value={{ refname: text.display, refpk: text.value }}
										queryCondition={() => {
											return {
												pk_accountingbook: self.state.headFunc.pk_accountingbook.value,
												pk_org: self.state.unitOrg.value
											};
										}}
										onChange={(v) => {
											let { prepareddate, evidenceData, headFunc } = this.state;
											let originData = this.findByKey(record.key, evidenceData.rows);
											if (originData) {
												originData[item.attrcode] = {
													display: v.refname,
													value: v.refpk
												};
											}
											//根据币种请求汇率
											if (item.attrcode == 'pk_currtype') {
												let url = '/nccloud/gl/glpub/ratequery.do';
												let pk_currtype = {
													pk_accountingbook: headFunc.pk_accountingbook.value,
													pk_currtype: v.refpk,
													prepareddate: prepareddate.value
												};
												if (headFunc.pk_accountingbook.value && v.refpk && prepareddate.value) {
													this.queryRate(pk_currtype, url, originData);
												}
											}
											//摘要联动
											let saveDataObj = JSON.parse(JSON.stringify(this.state.saveData));
											if (saveDataObj.explanation) {
												saveDataObj.explanation.display = evidenceData.rows[0].explanation.display;
												saveDataObj.explanation.value = evidenceData.rows[0].explanation.value;
											}
											this.setState({
												evidenceData,
												saveData: saveDataObj
											});
										}}
									/>
								) : (
									<p class='accasoaClass'><span>{text.display}</span></p>
								);
								// }
							}
						} else {
							//会计科目参照特殊处理
							return !voucherView&&!record.rowEdited ? (
								<ReferLoader
									tag={item.attrcode}
									refcode={referUrl}
									disabled={record.rowEdited}
									isMultiSelectedEnabled={false} //单选
									isShowHighFilter={false}
									onlyLeafCanSelect={true}
									isShowDisabledData={false}
									value={{ ...text.text,refname: text.display, refpk: text.value }}
									queryCondition={() => {
										if (record.userdata&&record.userdata.value) {
											//协同分录的加过滤类
											return {
												isDataPowerEnable:"Y",
												DataPowerOperationCode:"fi",
												accountcode: record.accsubjcode.value,
												pk_accountingbook: self.state.headFunc.pk_accountingbook.value,
												dateStr: prepareddate.value.split(' ')[0],
												TreeRefActionExt: 'nccloud.web.gl.ref.AccountChildRefBuilder'
											};
										} else {
											return {
												isDataPowerEnable:"Y",
												DataPowerOperationCode:"fi",
												pk_accountingbook: self.state.headFunc.pk_accountingbook.value,
												dateStr: prepareddate.value.split(' ')[0]
											};
										}
									}}
									onChange={(v) => {
										let _this = this;
										let { prepareddate, evidenceData, headFunc, freeValue } = _this.state;
										//切换会计科目修改币种汇率
										if (v && v.refpk) {
											let queryArry = [];
											let accasoaArr = [];
											accasoaArr.push(v);
											let newData = accasoaArr.map((item,i) => {
												let pk_accasoa = {
													cashtype: item.cashtype&&item.cashtype || item.nodeData&&item.nodeData.cashtype,
													display: item.dispname&&item.dispname || item.nodeData&&item.nodeData.dispname,
                                                    value: item.refpk,
                                                    text:item
                                                };
												//多选数量列控制
												if (
													item.unit != null ||
													(item.nodeData && JSON.stringify(item.nodeData) != '{}')
												) {
													_this.ifNumber(item.unit || item.nodeData.unit, index);
												}
												queryArry.push(item.refpk);
												return {
													...evidenceData.rows[index],
                                                    pk_accasoa
												};
											});
											newData.unshift(index, 1);
											Array.prototype.splice.apply(evidenceData.rows, newData);
											evidenceData.rows.forEach(function(item, i) {
												item.detailindex = {//多选重置 现金流量多选
													value: (i + 1).toString()
												};
												item.key = ++i;
											});
											_this.getCurry(
												record.pk_accountingbook.value,
												record.prepareddate.value.split(' ')[0],
												{ value: queryArry },
												'pk_accasoa',
												index
											);
										} else {
											evidenceData.rows[index][item.attrcode] = {
                                                ...evidenceData.rows[index][item.attrcode],
												display: '',
												value: ''
											};
										}
										if (evidenceData.rows[index].ass && evidenceData.rows[index].ass.length != 0) {
											//切换会计科目清空辅助核算
											if(!evidenceData.rows[index].userdata||!evidenceData.rows[index].userdata.value){//协同分录不清空辅助核算
												evidenceData.rows[index].ass = [];
												evidenceData.rows[index].assid = {
													...evidenceData.rows[index],
													display: '',
													value: ''
												};
											}
										}

										//如果是非现金类科目，将现金流量清空
										if(evidenceData.rows[index].cashflow&&evidenceData.rows[index].cashflow.lenght!=0){
											if(v&&v.length>0&&v[0].cashtype=='1'){
												evidenceData.rows[index].cashflow=[];
												evidenceData.rows[index].cashflowname={value:'',display:''};
												evidenceData.rows[index].expand.cashflowname={value:'',display:''};
											}
										}
										_this.setState({
											expandRow:[],
											evidenceData
										});
									}}
								/>
							) : (
								<p class='accasoaClass'><span>{text.display}</span></p>
							);
							// }
						}
						break;
					case 'ass':
						return (
							<div className="refer-input-wraper">
								<FormControl
									// value={record.assid ? record.assid.display : ''}
									disabled={record.rowEdited}
									onFocus={this.focus}
									onChange={(v) => {
										let cloneNewLine = deepClone(evidenceColumns);
										cloneNewLine.splice(4, 0, number1);
										this.setState({
											evidenceColumns: cloneNewLine
										});
									}}
								/>
								<i
									className="iconfont icon-canzhaozuixin"
									onClick={this.handleAssistAcc.bind(this, record.key)}
								/>
							</div>
						);
					case 'number':
						if (item.attrcode == 'amount') {
							let { checkedNumber } = record ;
							return !voucherView&&!record.rowEdited ? (
								<div className="refer-input-wraper">
									<NCNumber
										// scale={Number(record.scale ? Number(record.scale) : 2)}
										scale={record.amount&&record.amount.scale ? Number(record.amount.scale) : Number(record.scale)}
										disabled={record.rowEdited}
										value={text?text.value:''}
										onFocus={this.focus}
										placeholder={checkedNumber ? this.state.json['20021005card-000066'] : ''}/* 国际化处理： 请输入数字*/
										
										onFocus={() => {
											let _this = this;
											let { prepareddate, evidenceData, headFunc, freeValue } = _this.state;
											//摘要最后一行新增空行
											// if (record.key == evidenceData.rows.length) {
											// 	//在最后一行点击，自动新增一行
											// 	if (index >= 1) {
											// 		evidenceData.rows[index].explanation.display =
											// 			evidenceData.rows[index - 1].explanation.display;
											// 		evidenceData.rows[index].explanation.value =
											// 			evidenceData.rows[index - 1].explanation.value;
											// 	}
											// 	if(!this.offsetButton){
											// 		this.addNewLine(rows.length);
											// 	}
											// }
										}}
										onBlur={(v) => {
											if (v) {
												data.pk_currtype = record.pk_currtype.value;
												data.pk_accountingbook = record.pk_accountingbook.value;
												data.prepareddate = record.prepareddate.value;
												let countValue = amountconvert(data, currInfo, 'amount');
												if (countValue.message) {
													toast({ content: countValue.message, color: 'warning' });
												}
												countRow(record, countValue, 'amount');
											}
											// this.totleCount(item.attrcode, 'localdebitamountTotle');
										}}
										onChange={(v) => {
											if (record) {
												record[item.attrcode].value=v
											}
											this.setState({
												evidenceData: this.state.evidenceData
											});
										}}
									/>
									{!record[item.attrcode].editable ? (
										<i
											className="iconfont icon-canzhaozuixin"
											onClick={this.changeNumberClick.bind(this, record.key)}
										/>
									) : null}
								</div>
							) : (
								// <div class='curryClass'><span>{text?text.value : ''}</span></div>
                                <p class='curryClass'><span>{text?text.value&&self.commafy(text.value) : ''}</span></p>
							);
						} else if (item.attrcode == 'localdebitamount') {
							return !voucherView&&!record.rowEdited ? (
								<div className="textCenter">
									<NCNumber
										// scale={Number(originData.orgscale ? Number(originData.orgscale) : 2)}
										scale={record.localdebitamount&&record.localdebitamount.scale ? Number(record.localdebitamount.scale) :  Number(record.orgscale)}
										//onFocus={this.focus}
										disabled={record.rowEdited}
										value={text.value && text.value != '0' ? text.value : ''}
										onFocus={(v) => {
											record[item.attrcode].visible = true;
										}}
										onBlur={(v) => {
											record[item.attrcode].visible = false;
											if (v &&v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
												data.localamount = record.localdebitamount.value;
												data.pk_currtype = record.pk_currtype.value;
												data.pk_accountingbook = record.pk_accountingbook.value;
												data.prepareddate = record.prepareddate.value;
												let countValue = amountconvert(data, currInfo, 'localdebitamount');
												if (countValue.message) {
													toast({
														content: countValue.message,
														color: 'warning',
														duration: 5
													});
												}
												countRow(record, countValue, 'localdebitamount');
											}
										}}
										onKeyUp={(event) => {
											let {
												localcreditamountTotle,
												localdebitamountTotle
											} = this.state.evidenceData;
											let originData = this.findByKey(record.key, rows);
											if (
												event.keyCode == '187' ||
												(event.keyCode == 229 && originData.excrate2.value)
											) {
												this.equal(item.attrcode, originData);
												data.localamount = record.localdebitamount.value;
												data.pk_currtype = record.pk_currtype.value;
												data.pk_accountingbook = record.pk_accountingbook.value;
												data.prepareddate = record.prepareddate.value;
												let countValue = amountconvert(data, currInfo, item.attrcode);
												countRow(originData, countValue,  item.attrcode);
												this.totleCount(item.attrcode, 'localdebitamountTotle');
											} else if (event.keyCode == '32' && originData.excrate2.value) {
												emptyEnter(item.attrcode, originData);
												this.totleCount(item.attrcode, 'localdebitamountTotle');
											}
										}}
										onChange={(v) => {
											let self = this;
											let { evidenceData } = self.state;
										//	let originData = self.findByKey(record.key, evidenceData.rows);
											if (v && v !== '=' && v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
												if (record) {
													record[item.attrcode].value = v
												}
												this.setState({
													evidenceData
												});
											}
										}}
									/>
									{/* <Rate  rate={record.excrate2.value|| ''} ifShow={record[item.attrcode]} /> */}
								</div>
							) : (
								<div class='curryClass'><span>
									{text?text.value&&self.commafy(text.value) : ''}
									<Rate rate={record.excrate2.value || ''} ifShow={record[item.attrcode]} />
								</span></div>
							);
						} else if (item.attrcode == 'localcreditamount') {
							return !voucherView&&!record.rowEdited ? (
								<div className="textCenter">
									<NCNumber
										// scale={Number(originData.orgscale ? Number(originData.orgscale) : 2)}
										scale={record.localcreditamount&&record.localcreditamount.scale ? Number(record.localcreditamount.scale) :  Number(record.orgscale)}
										//onFocus={this.focus}
										disabled={record.rowEdited}
										value={text.value && text.value != '0' ? text.value : ''}
										onFocus={(v) => {
											record[item.attrcode].visible = true;
										}}
										onBlur={(v) => {
											record[item.attrcode].visible = false;
											if (v && v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
												data.localamount = record.localcreditamount.value;
												data.pk_currtype = record.pk_currtype.value;
												data.pk_accountingbook = record.pk_accountingbook.value;
												data.prepareddate = record.prepareddate.value;
												let countValue = amountconvert(data, currInfo, 'localcreditamount');
												countRow(record, countValue, 'localcreditamount');
											}
											// this.totleCount(item.attrcode, item.attrcode + 'Totle');
										}}
										onKeyUp={(event) => {
											let {
												localcreditamountTotle,
												localdebitamountTotle
											} = this.state.evidenceData;
											//	let originData = this.findByKey(record.key, rows);
											if (
												event.keyCode == '187' ||
												(event.keyCode == 229 && originData.excrate2.value)
											) {
												this.equal(item.attrcode, record);
												data.localamount = record.localcreditamount.value;
												data.pk_currtype = record.pk_currtype.value;
												data.pk_accountingbook = record.pk_accountingbook.value;
												data.prepareddate = record.prepareddate.value;
												let countValue = amountconvert(data, currInfo, item.attrcode);
												countRow(record, countValue,  item.attrcode);
												// this.totleCount(item.attrcode, 'localdebitamountTotle');
											} else if (event.keyCode == '32' && record.excrate2.value) {
												emptyEnter(item.attrcode, record);
												// this.totleCount(item.attrcode, 'localdebitamountTotle');
											} 
										}}
										onChange={(v) => {
											let { evidenceData } = this.state;
											if (v && v !== '=' && v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
												if (record) {
													record[item.attrcode].value=v
												}
												this.setState({
													evidenceData
												});
											}
										}}
									/>
									{/* <Rate  rate={record.excrate2.value|| ''} ifShow={record[item.attrcode]} /> */}
								</div>
							) : (
								<div class='curryClass'><span>
									{text?text.value&&self.commafy(text.value) : ''}
									<Rate rate={record.excrate2.value || ''} ifShow={record[item.attrcode]} />
								</span></div>
							);
						}
						break;
					case 'num':
						return (
							<div className="textCenter">
								<FormControl
									onFocus={this.focus}
									disabled={record.rowEdited}
									value={text[item.key].value || ''}
									onChange={(v) => {
										let { rows } = this.state.evidenceData;
										let originData = this.findByKey(record.key, rows);
										if (originData) {
											originData.localcreditamount = {
												value: Number(v)
											};
											originData.localdebitamount = {
												value: ''
											};
											originData.amount = {
												value: Number(v)
											};
										}
									}}
								/>
								{errorMsg}
							</div>
						);
						break;
					case 'textField':
						return (
							<div className="textCenter">
								<FormControl
									onFocus={this.focus}
									disabled={record.rowEdited}
									value={text[item.key] ? text[item.key].value : ''}
									onChange={(v) => {
										let { rows } = this.state.evidenceData;
										let originData = this.findByKey(record.key, rows);
										if (originData) {
											originData.localcreditamount = {
												value: v
											};
											originData.localdebitamount = {
												value: ''
											};
											originData.amount = {
												value: v
											};
										}
									}}
								/>
								{errorMsg}
							</div>
						);
						break;
					case 'datepicker':
						return (
							<div className="textCenter">
									{!voucherView&&!record.rowEdited ? (
										<DatePicker
											value={text&&text.value && text.value != '0' ? text.value : ''}
											onChange={(v) => {
												let { evidenceData } = this.state;
												if (record) {
													record[item.attrcode]={
														value:v,
														display:v
													}
												}
												this.setState({
													evidenceData
												});
											}}
										/>
									) : (
										<span>{text&&text.value}</span>
									)}
							</div>
						);
						break;
					default:
						break;
				}
			};
		});
		// data.unshift(dataFirst);
		// let dataCloums=[openRowCol,dataFirst].concat(data);
		let dataCloums=[dataFirst].concat(data);
		return dataCloums;
		// return data;
	}

	//修改页面数据加载渲染
	echoData(data) {
		let self = this;
		let lastTotle = [];
		let {
			freeValue,
			attachment,
			evidenceData,
			evidenceColumns,
			headContent,
			footContent,
			saveData,
			prepareddate
		} = self.state;
		let loadColums = data.template.gl_voucher_detail.items;
		evidenceData.rows.map((item, index) => {
			//根据列key 渲染加载的rows
			loadColums.forEach(function(v, i, a) {
				let objData = {
					display: '',
					value: '',
					editable: false
				};
				if (v.attrcode != 'cashflow') {
					//暂时先不保存现金流量
					item[v.attrcode] = objData;
				}
			});
			item.detailindex = {
				value: (index + 1).toString()
			};
			item.pk_unit = {
				//添加业务单元字段
				display: '',
				value: ''
			};
			item.groupdebitamount = {
				//添加集团本币借方发生额
				display: '',
				value: ''
			};
			item.groupcreditamount = {
				//添加集团本币贷方发生额
				display: '',
				value: ''
			};
			item.globaldebitamount = {
				//添加全局本币借方发生额
				display: '',
				value: ''
			};
			item.globalcreditamount = {
				//添加全局本币贷方发生额
				display: '',
				value: ''
			};
			item.debitquantity = {
				//数量
				display: '',
				value: ''
			};
			item.price = {
				//单价
				display: '',
				value: ''
			};
		});
		//新增行添加列信息
		loadColums.forEach(function(v, i, a) {
			// if(v.key=='explanation'){
			// 	v.itemType='explanation'
			// }
			if (detailExpend.includes(v.attrcode)) {
				//结算 往来核销 现金流量
				freeValue.push(v);
			}
			if (v.attrcode.indexOf('freevalue') != -1 && v.visible) {
				//分录自定义项
				freeValue.push(v);
			}
			let objData = {
				display: '',
				value: '',
				editable: false
			};
			if (v.attrcode != 'cashflow') {
				//暂时先不保存现金流量
				evidenceData.newLine[v.attrcode] = objData;
			}
			evidenceData.newLine.pk_unit = objData;
			evidenceData.newLine.groupdebitamount = objData;
			evidenceData.newLine.groupcreditamount = objData;
			evidenceData.newLine.globaldebitamount = objData;
			evidenceData.newLine.globalcreditamount = objData;
			evidenceData.newLine.debitquantity = objData;
			evidenceData.newLine.price = objData;
			evidenceData.newLine.detailindex = {
				value: ''
			};
		});
		loadColums = loadColums.filter(function(v, i, a) {
			return v.attrcode!='pk_unit_v'&&v.attrcode.indexOf('freevalue') == -1 && !detailExpend.includes(v.attrcode) && v.visible == true&&v.attrcode!='add';
		});
		evidenceColumns = self.editeColums(loadColums);

		headContent = data.template.head.items;
		headContent.forEach(function(v, i, a) {
			//修改模板数据为保存数据格式
			let objData = {
				display: '',
				value: ''
			};
			if (v.initialvalue) {
				objData = {
					display: v.initialvalue.display,
					value: v.initialvalue.value
				};
			}
			saveData[v.attrcode] = objData;
		});
		footContent = data.template.tail.items;
		footContent.forEach(function(v, i, a) {
			let objData = {
				display: '',
				value: ''
			};
			saveData[v.attrcode] = objData;
		});
		self.setState({
			freeValue,
			headContent,
			footContent,
			evidenceColumns,
			attachment,
			saveData,
			evidenceData
		});
	}
	//得到保存的数据
	handleExportState = () => {
		return this.state;
	};
	//得到所有行数据
	handleGetAllRows = () => {
		let { rows } = this.state.evidenceData;
		return rows;
	};
	//得到选中行数据
	handleGetRow = () => {
		let { evidenceData } = this.state;
		let record = this.state.getrow;
		let copyData = evidenceData.rows[record - 1];
		return copyData;
	};
	//点击保存
	handleSave = () => {
		this.setState({
			checkForm: true
		});
	};
	//是否是string类型
	isString(str) {
		return typeof str == 'string' && str.constructor == String;
	}
	//是否是空对象
	isEmptyObject(obj) {
		for (var key in obj) {
			return false; //返回false，不为空对象
		}
		return true; //返回true，为空对象
	}
	//整理表体数据
	checkNumber = (values, dates, refs) => {
		let self = this;
		values.forEach(function(v, i, a) {
			let ele = self.state[v.name];

			let { value } = v;
			if (typeof value == 'string' && value.indexOf(',') != -1) {
				value = self.removeThousands(value);
			}
			if (ele) {
				if (self.isEmptyObject(value)) {
					ele = {
						value: null,
						display: '',
						scale: -1
					};
				} else {
					if (refs) {
						if (refs.indexOf(v.name) > -1) {
							ele.value = value.refpk;
							ele.display = value.refname;
						} else {
							ele.value = value.value || value;

							if (dates.indexOf(v.name) > -1) {
								if (self.isString(ele.value)) {
									return ele.value;
								} else {
									ele.value = ele.value.format(format);
								}
							}
						}
					}
				}
			}
		});
	};
    getTableHeight = () => {
        // let accountContentHeight = this.refs.accountContent && getComputedStyle(this.refs.accountContent, null).height || "400px";
        // let tableHeight = accountContentHeight.replace('px', '') - 200;
        let tableHeight=document.getElementById('app').offsetHeight-260;
        return tableHeight;
    }
	//新增行操作
	addNewLine(record,type) {
        if(this.tableHeight=='350'){//动态计算表格高度,产生滚动条
			let tableArea_H = this.refs.tableArea ? getComputedStyle(this.refs.tableArea, null).height : '400px';
			let height = tableArea_H.replace('px', '') - 95;
			this.tableHeight=height;
		}
		let { evidenceData, currentRow,saveData,checkedArray } = this.state;
		let originData = this.findByKey(record, evidenceData.rows);
		let newkey;
		if(type=='add'){
		    newkey = ++record;
		}else if(type=='insert'){
			newkey = record;
		}
		// if(currentRow.rowCount){//如果有值就去总行数
		// 	newkey=++currentRow.rowCount;
		// }
		//新增分录行带出上一行信息 摘要 币种 汇率 组织集团依赖
		evidenceData.newLine.deleteStatus = '1'; // 0表示不可以删除的协同分录，其余的可以删除
		evidenceData.newLine.orgcurrtype = originData.orgcurrtype;
		evidenceData.newLine.groupcurrtype = originData.groupcurrtype;
		evidenceData.newLine.globalcurrtype = originData.globalcurrtype;
		evidenceData.newLine.pk_accountingbook = originData.pk_accountingbook;
		evidenceData.newLine.prepareddate = originData.prepareddate;
		evidenceData.newLine.busidate = originData.busidate;
		evidenceData.newLine.explanation = originData.explanation;//摘要
		evidenceData.newLine.pk_unit_v = originData.pk_unit_v;//业务单元
        evidenceData.newLine.pk_unit = originData.pk_unit;
        //新增行时币种汇率优先取凭证类别的，若没有就取账簿默认的 liuhuit 20181106
        if(saveData.paraInfo&&saveData.paraInfo.vouchertypeParaInfo){
            evidenceData.newLine.pk_currtype = saveData.paraInfo.vouchertypeParaInfo.pk_currtype;
            evidenceData.newLine.excrate2.value = saveData.paraInfo.vouchertypeParaInfo.excrate2;
            evidenceData.newLine.excrate3.value = saveData.paraInfo.vouchertypeParaInfo.excrate3;
            evidenceData.newLine.excrate4.value = saveData.paraInfo.vouchertypeParaInfo.excrate4;
            evidenceData.newLine.excrate2scale.value = saveData.paraInfo.vouchertypeParaInfo.excrate2scale;
            evidenceData.newLine.excrate3scale.value = saveData.paraInfo.vouchertypeParaInfo.excrate3scale;
            evidenceData.newLine.excrate4scale.value = saveData.paraInfo.vouchertypeParaInfo.excrate4scale;
            evidenceData.newLine.pricescale.value=saveData.paraInfo.vouchertypeParaInfo.pricescale;
            evidenceData.newLine.scale = saveData.paraInfo.vouchertypeParaInfo.scale;//原币币种精度
        }else{
            evidenceData.newLine.pk_currtype = saveData.paraInfo.orgCurrinfo;
            evidenceData.newLine.excrate2.value = saveData.paraInfo.excrate2;
            evidenceData.newLine.excrate3.value = saveData.paraInfo.excrate3;
            evidenceData.newLine.excrate4.value = saveData.paraInfo.excrate4;
            evidenceData.newLine.excrate2scale.value = saveData.paraInfo.excrate2scale;
            evidenceData.newLine.excrate3scale.value = saveData.paraInfo.excrate3scale;
            evidenceData.newLine.excrate4scale.value = saveData.paraInfo.excrate4scale;
            evidenceData.newLine.pricescale.value=saveData.paraInfo.pricescale;
            evidenceData.newLine.scale = saveData.paraInfo.scale;//原币币种精度
        }
		// evidenceData.newLine.pk_currtype = originData.pk_currtype;
		// evidenceData.newLine.excrate2 = originData.excrate2;
		// evidenceData.newLine.excrate3 = originData.excrate3;
		// evidenceData.newLine.excrate4 = originData.excrate4;
		evidenceData.newLine.groupType = originData.groupType;
		evidenceData.newLine.globalType = originData.globalType;
		// evidenceData.newLine.scale = originData.scale;//原币币种精度
		evidenceData.newLine.orgscale = originData.orgscale;//组织本币币种精度
		evidenceData.newLine.groupscale = originData.groupscale;//集团本币币种精度
		evidenceData.newLine.globalscale = originData.globalscale;//组织本币币种精度
		evidenceData.newLine.detailindex.value = record.toString();
		evidenceData.newLine.direction = { display: null, value: 'D', scale: null }; //默认借方
		evidenceData.newLine.userdata = { display: null, value: null, scale: null };
		if (currentRow.currentBatchNo != '0000') {
			evidenceData.newLine.batchNo = currentRow.currentBatchNo;
		}

		let cloneNewLine = deepClone(evidenceData.newLine);
		let newLine = Object.assign({}, cloneNewLine, {
			key: newkey
		});
		if (newkey <= evidenceData.rows.length) {
			evidenceData.rows.map((item, index) => {
				if (item.key >= newkey) {
					item.key++;
				}
			});
		}
		evidenceData.rows.splice(newkey - 1, 0, newLine);
		evidenceData.rows.forEach((item, i) => {
			checkedArray.push(false);
		});
		saveData.details=evidenceData.rows;//跟新saveData details 同步跟新现金流量
		this.setState({
			checkedArray,
            evidenceData,
            expandRow:[]
		});
	}

	//表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

	//格式化后台返回数据
	dataFormat = (data) => {
		let result = [];
		if (data) {
			if (data[0]) {
				data.map((item, index) => {
					item.key = index + 1;
					result.push(item);
				});
			}
		}
		return result;
	};

	//获取计算合计之后的数据
	getTableSumData = (data, amount, Totle) => {
		if (data.length != 0) {
			//需要合计的字段
			let { rows } = this.state.evidenceData;
			let newObj = this.state.evidenceData[Totle];
			let sumObj = {
				[amount]: []
			};
			for (let key in sumObj) {
				let arr = data.map((item) => {
					return item[key].value ? parseFloat(item[key].value) : 0;
				});
				//let totleValue= sum(arr.slice(0))
				newObj.value =sum(...arr).replace(/,/g, '')  
				// arr.reduce((prev, curr) => {
				// 	return  prev + curr;
				// });
			}
			return newObj;
		}
	};
	//表体点击行事件
	getRow = (expanded, record) => {
		this.Changecheck(record);
		this.state.copyRecord = record;
		//表体数据增删获取key
		let { getrow, evidenceData } = this.state;
		evidenceData.rows.map((row, index) => {
			if (record == index) {
				row.rowEdited = false;
			} else {
				row.rowEdited = true;
			}
		});
		getrow = expanded.key;
		this.setState({
			getrow: getrow,
			evidenceData
		});
	};
	//表格复选框修改
	Changecheck = (index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		if (!checkedArray[index]) {
			checkedArray[index] = !self.state.checkedArray[index];
		}
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			if (i != index) {
				checkedArray[i] = false;
			}
		}
		self.setState({
			checkedArray: checkedArray
		});
	};
	//复制
	handleCopy = () => {
		//根据key去evidenceData取出对应行的数据
		let { evidenceData, copyRecord } = this.state;
		this.state.copyData = evidenceData.rows[copyRecord];
		this.state.copyData.pk_detail.display = null;
		this.state.copyData.pk_detail.value = null;
	};
	//剪切
	handleCut = () => {
		//根据key去evidenceData取出对应行的数据
		let { evidenceData, copyRecord } = this.state;
		this.state.copyData = evidenceData.rows[copyRecord];
		this.state.copyOrCut = 'cut';
		let record = ++copyRecord;
		let { rows } = this.state.evidenceData;
		let originData = this.findByKey(record, rows);
		if (originData) {
			rows = rows.filter(function(v, i, a) {
				return v.key != originData.key;
			});
			this.state.evidenceData.rows = rows;
		}
		if (record <= rows.length) {
			rows.map((item, index) => {
				if (item.key >= record) {
					item.key--;
				}
			});
		}
		this.totleCount(); //同步计算合计值
		this.setState({
			evidenceData: this.state.evidenceData
		});
	};
	//粘贴
	handlePaste = () => {
		let { evidenceData, copyData, copyRecord } = this.state;
		let newkey = ++copyRecord;
		let newLine = Object.assign({}, copyData, {
			key: newkey
		});
		if (newkey <= evidenceData.rows.length) {
			evidenceData.rows.map((item, index) => {
				if (item.key >= newkey) {
					item.key++;
				}
			});
		}
		evidenceData.rows.splice(newkey - 1, 0, newLine);
		this.totleCount(); //同步计算合计值
		this.setState({
			evidenceData
		});
	};
	//主子表信息 点击展开触发
	getData = (expanded, record) => {
		let self = this;
		let { expandRow, getrow, voucherView, headFunc, evidenceData, prepareddate } = self.state;
		if (expanded) {
			let originData = self.findByKey(record.key, evidenceData.rows);
			if (record.pk_accasoa.value && headFunc.pk_accountingbook.value && prepareddate.value) {
				let url = '/nccloud/gl/voucher/detailExpend.do';
				let data = {
					pk_accountingbook: headFunc.pk_accountingbook.value,
					pk_accasoa: record.pk_accasoa.value,
					prepareddate: prepareddate.value
				};
				ajax({
					url,
					data: data,
					async: false,
					success: function(response) {
						let { freeValue } = self.state;
						let settle = response.data.settle; //是否显示结算信息
						let verify = response.data.verify; //往来核销信息是否显示
						if (!settle) {
							freeValue = freeValue.filter((v, i, a) => {
								return !settkleArray.includes(v.attrcode);
							});
						}
						if (!verify) {
							freeValue = freeValue.filter((v, i, a) => {
								return !verifyArray.includes(v.attrcode);
							});
						}
						originData.childform = freeValue;
						if (originData.expand && JSON.stringify(originData.expand) != '{}') {
							originData.childform.map((e, i) => {
								if (e.attrcode == 'cashflow') {
									originData.expand['cashflowname'] = {
										value: originData.expand.cashflowname.value,
										display: originData.expand.cashflowname.display
									};
								} else {
									originData.expand[e.attrcode] = {
										value:  originData.expand[e.attrcode]&&originData.expand[e.attrcode].value,
										display:  originData.expand[e.attrcode]&&originData.expand[e.attrcode].display
									};
								}
							});
						} else {
							originData.expand = {}; //新增凭证添加expand字段赋默认值
							originData.childform.map((e, i) => {
								if (e.attrcode == 'cashflow') {
									originData.expand['cashflowname'] = {
										value: '',
										display: ''
									};
								} else {
									originData.expand[e.attrcode] = {
										value: '',
										display: ''
									};
								}
							});
						}
						self.setState(
							{
								evidenceData
							}
						);
					}
				});
			}
		}
		if (expandRow.includes(record.key)) {
			expandRow = expandRow.filter(function(v, i, a) {
				return v != record.key;
			});
		} else {
			expandRow.push(record.key);
		}
		this.setState({
			expandRow
		});
	};
	//子表展开数据展示
	expandedRowRender = (record, index, indent) => {
		let self = this;
		let { voucherView, headFunc, prepareddate, evidenceData ,saveData} = self.state;
		return (
			<FormItemTab
				voucherView={voucherView}
				defaultValue={record.expand}
				pk_org={record.pk_unit.value}
				prepareddate={saveData.prepareddate.value}
				tabs={record.childform}
				propsForm={self.props}
				freeKey={record.key}
				OnChange={(i, v, e) => {
					let { rows } = this.state.evidenceData;
					let originData = this.findByKey(e, rows);
					if (originData&&originData.expand) {
						if (i == 'cashflow') {
							//现金流量赋值
							originData.expand.cashflowname.value = v;
							originData.cashflowname.value = v;
						} else {
								if (isObj(v)) {
									originData.expand[i].value = v.refpk;
									originData.expand[i].display = v.refname;
									if(originData[i]){
										originData[i].value = v.refpk; //同时在expand　外赋值便于后端取 区别新增和修改
										originData[i].display = v.refname;
									}else{
										originData[i]={
											value:v.refpk,
											display:v.refname
										}
									}
								} else {
									originData.expand[i].value = v;
									if(originData[i]){
										originData[i].value = v;
									}else{
										originData[i]={
											value:v
										}
									}
									
								}
						}
					}
					this.setState({
						evidenceData
					});
				}}
			/>
		);
	};
	//合计计算
	totleCount = (userdata, Totle) => {
		let _this = this;
		let { rows } = _this.state.evidenceData;
		let { evidenceData,isNC01,isNC02 } = _this.state;
		// let expenseData = rows.map((e) => {
		// 	return e;
		// });

		let expenseData = _this.dataFormat(rows);
		let sumData = _this.getTableSumData(expenseData, 'localcreditamount', 'localcreditamountTotle'); //计算组织合计数据
		_this.getTableSumData(expenseData, 'localdebitamount', 'localdebitamountTotle'); //计算合计数据
		//判断是否有集团全局列
		if(isNC01){
			_this.getTableSumData(expenseData, 'groupdebitamount', 'groupdebitamountTotle'); //计算集团合计数据
			_this.getTableSumData(expenseData, 'groupcreditamount', 'groupcreditamountTotle'); //计算合计数据
		}
		if(isNC02){
			_this.getTableSumData(expenseData, 'globaldebitamount', 'globaldebitamountTotle'); //计算全局合计数据
			_this.getTableSumData(expenseData, 'globalcreditamount', 'globalcreditamountTotle'); //计算合计数据
		}
		
		if (sumData) {
			_this.setState({
				evidenceData
			});
			//this.forceUpdate()
		}
	};

	//日期控制
	handleInputChange = (obj, type, val) => {
		let { saveData } = this.state;
		let nameValue = saveData[obj];
		// if (type === 'begindate' || type === 'enddate') {
		// 	val = moment(val);
		// }
		//nameValue.value = val;
	};
	//跟新计算后的值
	countRow = (originData, countValue, key) => {
		let keyString;
		if (key != 'amount') {
			keyString = key.substring(5);
		} else {
			keyString = 'amount';
		}
		for (let item in countValue) {
			originData[item] = {
				...originData[item],
				value: countValue[item]
			};
			if (item == 'localamount') {
				if (keyString != 'creditamount' && keyString != 'debitamount') {
					if (originData.localcreditamount.value) {
						originData.localcreditamount = {
							...originData.localcreditamount,
							value: countValue[item]
						};
					} else {
						originData.localdebitamount = {
							...originData.localdebitamount,
							value: countValue[item]
						};
					}
				} else {
					if (keyString == 'creditamount') {
						originData.localcreditamount = {
							...originData.localcreditamount,
							value: countValue[item]
						};
						originData.localdebitamount = {
							...originData.localdebitamount,
							value: ''
						};
					} else {
						originData.localdebitamount = {
							...originData.localdebitamount,
							value: countValue[item]
						};
						originData.localcreditamount = {
							...originData.localcreditamount,
							value: ''
						};
					}
				}
			}
			if (item == 'groupamount') {
				if (keyString != 'creditamount' && keyString != 'debitamount') {
					if (originData.groupcreditamount.value) {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: countValue[item]
						};
					} else {
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: countValue[item]
						};
					}
				} else {
					if (keyString == 'creditamount') {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: countValue[item]
						};
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: ''
						};
					} else {
						originData.groupcreditamount = {
							...originData.groupcreditamount,
							value: ''
						};
						originData.groupdebitamount = {
							...originData.groupdebitamount,
							value: countValue[item]
						};
					}
				}
			}
			if (item == 'globalamount') {
				if (keyString != 'creditamount' && keyString != 'debitamount') {
					if (originData.globalcreditamount.value) {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: countValue[item]
						};
					} else {
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: countValue[item]
						};
					}
				} else {
					if (keyString == 'creditamount') {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: countValue[item]
						};
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: ''
						};
					} else {
						originData.globalcreditamount = {
							...originData.globalcreditamount,
							value: ''
						};
						originData.globaldebitamount = {
							...originData.globaldebitamount,
							value: countValue[item]
						};
					}
				}
			}
		}
	};
	//删除
	handleCancel = () => {
		let record = this.state.getrow;
		let { rows, index } = this.state.evidenceData;
		let originData = this.findByKey(record, rows);
		if (originData.deleteStatus && originData.deleteStatus == '0') {
			toast({ content: this.state.json['20021005card-000120'], color: 'warning' });/* 国际化处理： 协同分录不可以删除，请知悉!*/
			return false;
		}
		if (originData) {
			rows = rows.filter(function(v, i, a) {
				return v.key != originData.key;
			});
			if (record <= rows.length) {
				rows.map((item, index) => {
					if (item.key >= record) {
						item.key--;
					}
				});
			}
			this.state.evidenceData.rows = rows;
			this.state.evidenceData.index = index - 1;
			this.totleCount();
		}

		this.setState({
			evidenceData: this.state.evidenceData
		});
	};

	//及时核销
	veriFi = () => {};

	//下拉时间

	changeSelect = (value) => {
		//alert(value)
	};
	handleAddLast = () => {
		let { evidenceData, currentRow } = this.state;
		this.addNewLine(evidenceData.rows.length,'add');
	};
	handleAdd = () => {
		let record = this.state.getrow;
		let { evidenceData } = this.state;
		this.addNewLine(record ? record : evidenceData.index,'insert');
	};
	//复制当前行拆分成两行
	coordinationResolute = () => {
		let record = this.state.getrow;
		let { evidenceData, currentRow } = this.state;
		let originData = this.findByKey(record, evidenceData.rows);
		let getCheckedRowData = this.handleGetRow();
		let copyRowData = deepClone(getCheckedRowData);
		//清空金额
		for (let k in copyRowData) {
			if (
				k == 'amount' ||
				k == 'localdebitamount' ||
				k == 'localcreditamount' ||
				k == 'globalcreditamount' ||
				k == 'globaldebitamount' ||
				k == 'groupcreditamount' ||
				k == 'groupdebitamount'
			) {
				copyRowData[k].display = '';
				copyRowData[k].value = '';
			}
			copyRowData['deleteStatus'] = '1'; //0表示原有协同分录不可删除，1 表示可以删除
		}
		let newkey = ++record;
		let newLine = Object.assign({}, copyRowData, {
			key: newkey
		});
		if (newkey <= evidenceData.rows.length) {
			evidenceData.rows.map((item, index) => {
				if (item.key >= newkey) {
					item.key++;
				}
			});
		}
		evidenceData.rows.splice(newkey - 1, 0, newLine);
		this.setState({
			evidenceData
		});
	};
	//常用按钮点击弹出模态框
	handleAllways = () => {
		this.setState({
			saveNumber: '2',
			checkForm: true
		});
	};
	//摘要模态框
	explanationClick = (e) => {
		let self = this;
		let ExplanationModalShow = true;
		this.setState({
			ExplanationModalShow,
			index: e
		});
	};
	//点击原币弹出修改汇率 精度模态框
	changeNumberClick = (e) => {
		let self = this;
		let { evidenceData, findRows ,saveData} = self.state;
		let originData = deepClone(this.findByKey(e, evidenceData.rows)); //避免修改原对象数组，对其他产生影响
		if(!originData.amount.value){
			let newcurrinfo=queryCurrinfo(saveData.pk_accountingbook.value||originData.pk_accountingbook.value,originData.pk_currtype.value,saveData.prepareddate.value)
			if(originData.excrate2){
				originData.excrate2.value=newcurrinfo.excrate2
			}
			if(originData.excrate3){
				originData.excrate3.value=newcurrinfo.excrate3
			}
			if(originData.excrate4){
				originData.excrate4.value=newcurrinfo.excrate4
			}

		}

		if (!originData.flag) {
			//传入子组件字段控制
			delete originData.debitquantity;
			delete originData.price;
		}
		if (originData.excrate3 && !originData.excrate3.value) {
			//传入子组件字段控制
			delete originData.excrate3;
		}
		if (originData.excrate4 && !originData.excrate4.value) {
			//传入子组件字段控制
			delete originData.excrate4;
		}

		if (!originData.groupType) {
			delete originData.groupdebitamount;
			delete originData.groupcreditamount;
		}
		if (!originData.globalType) {
			delete originData.globaldebitamount;
			delete originData.globalcreditamount;
		}

		if (findRows.length == 0) {
			findRows.push(originData);
		} else {
			findRows = [];
			findRows.push(originData);
		}
		let changeNumberShow = true;
		this.setState({
			changeNumberShow,
			findRows
		});
	};

	//常用凭证选择
	onVisibleChange = (visible) => {
	};

	//模态框组件
	handleOperationTypeAll = (MsgModalAll, e) => {
		let { rows } = this.state.evidenceData;
		let { index, options } = this.state;
		MsgModalAll = false;
		let originData = this.findByKey(index, rows);
		originData.explanation = {
			value: e.value
		};
		options.push(e.value);
		this.setState({
			options,
			ExplanationModalShow: MsgModalAll,
			evidenceData: this.state.evidenceData
		});
	};

    //修改汇率和原币值
    //修改汇率和原币值
	changeUnit = (e) => {
		let { evidenceData } = this.state;
		let { rows } = evidenceData;
		rows = rows.map((t) => {
			return t.key === e[0].key ? e[0] : t;
		});
		evidenceData.rows = rows;
		this.setState(
			{
				evidenceData,
				changeNumberShow: false
			}
		);
	};
	refresh = (response) => {
		let { evidenceData, freeValue, saveData, update } = this.state;
		response.data.voucher.details.map((item, i) => {
			item.key = ++i;
			if (item.price.value && item.price.value != '0') {
				item.flag = true;
				if (item.creditquantity.value) {
					//借贷数量同步显示
					item.debitquantity.value = item.creditquantity.value;
					item.creditquantity.value = '';
				}
			}
			if (item.creditamount.value && item.creditamount.value != '0') {
				item.amount = {
					value: item.creditamount.value
				};
			}
			if (item.debitamount.value && item.debitamount.value != '0') {
				item.amount = {
					value: item.debitamount.value
				};
			}

			if (item.errmessage && item.errmessage.value) {
				//分录标错标识
				item.showTip = true;
			}
			//保存成功后添加子表字段结算字段
			//item.childform=evidenceData.rows[item.key-1].childform;
			if (!item.settle.value) {
				freeValue = freeValue.filter((v, i, a) => {
					return !settkleArray.includes(v.attrcode);
				});
			}
			if (!item.verify.value) {
				freeValue = freeValue.filter((v, i, a) => {
					return !verifyArray.includes(v.attrcode);
				});
			}
			item.childform = freeValue;
		});
		evidenceData.rows = response.data.voucher.details;
		evidenceData.index = response.data.voucher.details.length;
		if (saveData.num.value != response.data.voucher.num.value) {
			toast({ content: this.state.json['20021005card-000067'], color: 'warning' });/* 国际化处理： 请注意，凭证号已更新*/
		}
		saveData = response.data.voucher;
		evidenceData.localcreditamountTotle.value = response.data.voucher.totalcredit.value;
		evidenceData.localdebitamountTotle.value = response.data.voucher.totaldebit.value;
		// saveData.period.value = response.data.voucher.year.value + '-' + response.data.voucher.period.value;
		update = true;
		this.setState({
			evidenceData,
			update,
			saveData
		});
	};

	//辅助核算点击弹出模态框
	handleAssistAcc = (index) => {
		//ass
		let { isFreevalueDefault, saveData, unitOrg, evidenceData,currentRowPrevass } = this.state;
		let prepareddate = saveData.prepareddate.value;
		let pk_accountingbook = saveData.pk_accountingbook.value;
		let pk_unit = unitOrg.value;
		let pk_accasoa = '';
		let rows = evidenceData.rows;

		if (isFreevalueDefault && index != '1' && rows[index - 2].ass && rows[index - 2].ass.length != 0) {
			// rows[index - 1]['ass'] = rows[index - 2].ass;
			currentRowPrevass=rows[index - 2].ass;
		}else{
			currentRowPrevass=[];
		}
		for (let i = 0, len = rows.length; i < len; i++) {
			if (rows[i].key == index) {
				pk_accasoa = this.state.evidenceData.rows[i].pk_accasoa.value;
			}
		}
		if (!pk_accountingbook) {
			toast({ content: this.state.json['20021005card-000068'], color: 'warning' });/* 国际化处理： 请选择核算账簿*/
			return false;
		}
		if (!pk_accasoa) {
			toast({ content: this.state.json['20021005card-000069'], color: 'warning' });/* 国际化处理： 请选择会计科目*/
			return false;
		}
		if (!prepareddate) {
			toast({ content: this.state.json['20021005card-000070'], color: 'warning' });/* 国际化处理： 请选择制单日期*/
			return false;
		}
		this.setState({
			evidenceData,
			getrow: index,
			AssistAccModalShow: true,
			currentRowPrevass
		});
	};
	// 辅助核算点确定后续操作
	handleAssistAccModal = (MsgModalAll, assDatas) => {
		let self = this;
		let { getrow, evidenceData } = self.state;
		let { rows } = self.state.evidenceData;
		if (Array.isArray(assDatas)) {
			let newData = assDatas.map((item) => {
				let assid = {
					display: item.assname,
					value: item.assid
                };
                if(item.quantityscale){//辅助核算返回有数量精度就更新分录行精度
                    if(rows[getrow - 1].quantityscale&&rows[getrow - 1].quantityscale.value){
                        rows[getrow - 1].quantityscale.value=item.quantityscale;
                    }else{
                        rows[getrow - 1].quantityscale={value:item.quantityscale};
                    }
                }
				
				return {
					...rows[getrow - 1],
					ass: JSON.stringify(item.assData) != '{}' ? item.assData : null, //辅助核算为{}  传 null
					assid,
					// quantityscale
				};
			});
			newData.unshift(getrow - 1, 1);
			Array.prototype.splice.apply(rows, newData);
			rows.forEach(function(item, index) {
				item.key = ++index;
			});
			self.setState({
				AssistAccModalShow: false,
				evidenceData: self.state.evidenceData
			});
		} else {
			rows.forEach(function(item, index) {
				if (item.key == accIndex) {
					item.assid.display = '';
					item.assid.value = '';
				}
			});
			self.setState({
				AssistAccModalShow: false,
				evidenceData: self.state.evidenceData
			});
		}
	};
	//刷新清除缓存
	accountRefresh = () => {
		globalStore.clearCache('accountData');
	};
	//科目模态框显示
	handleClick = () => {
		let url = '/nccloud/gl/glpub/queryaccount.do'; //gl.glpub.queryaccount
		let self = this;
		let SubjectModalShow = true;
		let { pk_accountingbook, prepareddate, accountData } = self.state;
		let data = {
			pk_accountingbook: pk_accountingbook.value,
			versiondate: prepareddate.value
		};
		if (globalStore.isCache('accountData') && globalStore.getCache('accountData').length > 0) {
			//处理数据
			let accountData = globalStore.getCache('accountData');
		} else {
			ajax({
				url,
				data,
				success: function(response) {
					let { accountData } = response.data;
					globalStore.setCache('accountData', accountData);
					self.setState({
						SubjectModalShow,
						accountData
					});
				}
			});
		}
	};

	handleGTypeChange = () => {};

	//详情页添加数量列
	addNumber = (evidenceColumns) => {
		let self = this;
		let quantity = {
			title: this.state.json['20021005card-000071'],/* 国际化处理： 数量/单价*/
			key: 'quantity',
			dataIndex: 'quantity',
			width: '200px',
			render: (text, record, index) => {
				let { pk_accountingbook, prepareddate, voucherView } = self.state;
				let { rows } = self.state.evidenceData;
				let originData = self.findByKey(record.key, rows);
				let { data, currInfo } = self.exportData(originData);
				if (record.flag) {
					if (!voucherView&&!record.rowEdited) {
						return (
							<div className="number">
								<NCNumber
									className="count"
									disabled={record.rowEdited}
									scale={record.debitquantity&&record.debitquantity.scale ? Number(record.debitquantity.scale) : 2}
									value={record.debitquantity.value || ''}
									placeholder={this.state.json['20021005card-000043']}/* 国际化处理： 请输入数量*/
									//disabled={record.debitquantity.value!='0'?false:true}
									onKeyUp={(v) => {}}
									onBlur={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (v) {
											let countValue = amountconvert(data, currInfo, 'debitquantity');
											countRow(originData, countValue, 'debitquantity');
										}
										self.setState({
											evidenceData
										});
									}}
									onChange={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (originData) {
											originData.debitquantity = {
												...originData.debitquantity,
												value: v
											};
										}
										self.setState(
											{
												evidenceData
											}
										);
									}}
								/>
								{/* <span>{data.name.text}</span> */}

								<NCNumber
									className="price"
									scale={record.price&&record.price.scale? Number(record.price.scale) : Number(record.pricescale.value)}
									disabled={record.rowEdited}
									value={record.price.value || ''}
									placeholder={this.state.json['20021005card-000045']}/* 国际化处理： 请输入单价*/
									onKeyUp={(v) => {}}
									onBlur={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (v) {
											let countValue = amountconvert(data, currInfo, 'price');
											countRow(originData, countValue, 'price');
										}
										self.setState({
											evidenceData
										});
									}}
									onChange={(v) => {
										let { evidenceData } = self.state;
										let originData = self.findByKey(record.key, evidenceData.rows);
										if (originData) {
											originData.price = {
												...originData.price,
												value: v
											};
										}
										self.setState({
											//evidenceColumns:cloneNewLine,
											evidenceData
										});
									}}
								/>
							</div>
						);
					} else {
						return (
							<span>
								{record.debitquantity.value || ''}/{record.price.value || ''}
							</span>
						);
					}
				} else {
					return <span />;
				}
			}
		};
		//判断是否有数量这一列
		if (!(JSON.stringify(evidenceColumns).indexOf(JSON.stringify(quantity)) != -1)) {
			let cloneNewLine = deepClone(evidenceColumns);
			cloneNewLine.splice(5, 0, quantity);
			evidenceColumns = cloneNewLine;
		}
		return evidenceColumns;
	};

	//详情页添加业务单元列

	addUnit = (evidenceColumns) => {
		let self = this;
		let number = {
			title: this.state.json['20021005card-000072'],/* 国际化处理： 业务单元*/
			key: 'pk_unit_v',
			dataIndex: 'pk_unit_v',
			width: '200px',
			render: (text, record, index) => {
				//"uapbd/refer/org/BusinessUnitTreeRef"
				let { evidenceColumns, voucherView } = self.state;
				let referUrl = 'uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef/index.js';
				let attrcode = 'pk_unit_v';
				let editable =
					record['pk_unit_v'] && record['pk_unit_v'].editable ? record['pk_unit_v'].editable : false;
				return !voucherView&&!record.rowEdited ? (
					<ReferLoader
						tag={attrcode}
						refcode={referUrl}
						disabled={editable}
						value={{ refname: (text && text.display) || '', refpk: (text && text.value) || '' }}
						queryCondition={() => {
							return {
								pk_accountingbook: self.state.headFunc.pk_accountingbook.value,
								VersionStartDate: self.state.prepareddate.value,
								TreeRefActionExt: 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
							};
						}}
						onChange={(v) => {
							let { rows } = this.state.evidenceData;
							let originData = this.findByKey(record.key, rows);
							if (originData && originData.pk_unit_v.value) {
								//切换业务单元清空辅助核算
								if (originData.ass && originData.ass.length != '0') {
									originData.ass = [];
									originData.assid = {
										...originData.assid,
										display: '',
										value: ''
									};
								}
							}
							if (originData) {
								originData.pk_unit_v = {
									display: v.refname ? v.refname : '',
									value: v.values ? v.values.pk_vid.value : ''
								};
								originData.pk_unit = {
									display: '',
									value: v.refpk ? v.refpk : ''
								};
							}
							this.setState({
								evidenceData: this.state.evidenceData
							});
						}}
					/>
				) : (
					<span>{text ? text.display : ''}</span>
				);
			}
		};
		let originData = self.findByKey(number.key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			let cloneNewLine = deepClone(evidenceColumns);
			cloneNewLine.splice(2, 0, number);
			evidenceColumns = cloneNewLine;
		}
		return evidenceColumns;
	};

	//详情页添加集团列
	addGroup = (evidenceColumns) => {
		let self = this;
		let addGroup = [
			{
				title: this.state.json['20021005card-000073'],/* 国际化处理： 集团本币借方发生额*/
				key: 'groupdebitamount',
				attrcode: 'groupdebitamount',
				itemType: 'amount-rate'
			},
			{
				title: this.state.json['20021005card-000074'],/* 国际化处理： 集团本币贷方发生额*/
				itemType: 'amount-rate',
				key: 'groupcreditamount',
				attrcode: 'groupcreditamount'
			}
		];
		let addCurryLine = self.addCurry(addGroup)
		let originData = self.findByKey(addCurryLine[0].key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			let cloneNewLine = evidenceColumns.concat(addCurryLine);
			evidenceColumns = cloneNewLine;
		}
		return evidenceColumns;
	};

	//详情页添加全局列
	addGlobalLine = (evidenceColumns) => {
		let self = this;
		let addGlobal = [
			{
				title: this.state.json['20021005card-000075'],/* 国际化处理： 全局本币借方发生额*/
				key: 'globaldebitamount',
				attrcode: 'globaldebitamount',
				itemType: 'amount-rate'
			},
			{
				title: this.state.json['20021005card-000076'],/* 国际化处理： 全局本币贷方发生额*/
				itemType: 'amount-rate',
				key: 'globalcreditamount',
				attrcode: 'globalcreditamount'
			}
		];
		let addAllLine = self.addGlobal(addGlobal);
		let originData = self.findByKey(addAllLine[0].key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			let cloneNewLine = evidenceColumns.concat(addAllLine);
			evidenceColumns = cloneNewLine;
		}
		return evidenceColumns;
	};
	loadVoucher(voucher, paraInfo) {
		let self = this;
		let {
			checkedArray,
			datavoucher,
			nov,
			startdate,
			attachment,
			evidenceData,
			saveData,
			update,
			abandonShow,
			prepareddate,
			isadd,
			isNC01,
			isNC02,
			headFunc,
			unitOrg,
			freeValue,
			evidenceColumns,expandRow
		} = self.state;
		let { pk_accountingbook } = headFunc;
		if (voucher.details && voucher.details.length != 0) {
			let { NC001, NC002, isShowUnit, excrate2, excrate3, unit,isShowNum } = paraInfo;
			let finalColums = evidenceColumns;
			voucher.details.map((item, i) => {
				//渲染是否有动态新增列，有待优化
				checkedArray.push(false);
				item.key = ++i;
				itemUpdate(item, paraInfo);//更新分录上的精度
				// if (NC001) {
				// 	item.groupType = NC001;
				// }
				// if (NC002) {
				// 	item.globalType = NC002;
				// }
				if (item.isShowNum && item.isShowNum.value) {
					//根据返回数据判断是否有数量单价
					item.flag = true;
				}
				// if (item.price.value && item.price.value != '0') {
				// 	//根据返回数据判断是否有数量列
				// 	item.flag = true;
				// 	//evidenceData.newline[flag]=true;
				// 	self.addNumber();
				// }
				item.orgcurrtype = {
					display: paraInfo.currinfo.display,
					value: paraInfo.currinfo.value
				};
				//集团币种
				item.groupcurrtype = {
					display: paraInfo.groupCurrinfo.display,
					value: paraInfo.groupCurrinfo.value
				};
				//全局币种
				item.globalcurrtype = {
					display: paraInfo.globalCurrinfo.display,
					value: paraInfo.globalCurrinfo.value
				};
				if (item.direction.value == 'C') {
					if(!(item.amount&&item.amount.value)){
						item.amount = {
							value: item.creditamount.value,
							editable: item.creditamount.editable
						};
					}
				} else if (item.direction.value == 'D') {
					if(!(item.amount&&item.amount.value)){
						item.amount = {
							value: item.debitamount.value,
							editable: item.debitamount.editable
						};
					}
				}
				if (item.settle && !item.settle.value) {
					freeValue = freeValue.filter((v, i, a) => {
						return !settkleArray.includes(v.attrcode);
					});
				}
				if (item.verify && !item.verify.value) {
					freeValue = freeValue.filter((v, i, a) => {
						return !verifyArray.includes(v.attrcode);
					});
				}
				item.childform = freeValue;
			});
			// }
			if (unit) {
				unitOrg.value = unit.value;
			}
			if (isShowUnit) {
				isadd = true;
				//根据返回数据判断是否业务单元
				finalColums = self.addUnit(evidenceColumns);
			}
			if (isShowNum) {
				//有待完善
				//根据返回数据判断是否数量列
				finalColums = self.addNumber(finalColums);
			}
	
			if (NC001) {
				//根据返回数据判断是否集团
				isNC01 = true;
				finalColums = self.addGroup(finalColums);
				//	self.addGroup(NC001);
			}
	
			if (NC002) {
				//根据返回数据判断是否集团
				isNC02 = true;
				finalColums = self.addGlobalLine(finalColums);
				//	self.addGroup(NC001);
			}
			//修改太页面参照设置依赖
			pk_accountingbook.value = voucher.pk_accountingbook.value;
			prepareddate.value = voucher.prepareddate.value;
			if (voucher.details && voucher.details.length != 0) {
				evidenceData.rows = voucher.details;
				evidenceData.index = voucher.details.length;
			} else {
				evidenceData.rows = [];
			}
			saveData = voucher;
			evidenceData.localcreditamountTotle.value = voucher.totalcredit.value; //合计值获取
			evidenceData.localdebitamountTotle.value = voucher.totaldebit.value;
			evidenceData.groupcreditamountTotle.value = voucher.totalcreditgroup.value;
			evidenceData.groupdebitamountTotle.value = voucher.totaldebitgroup.value;
			evidenceData.globalcreditamountTotle.value = voucher.totalcreditglobal.value;
			evidenceData.globaldebitamountTotle.value = voucher.totaldebitglobal.value;
			saveData['paraInfo'] = paraInfo;
            if(expandRow&&expandRow.length!=0){//分录展开控制，保存前展开，保存成功统一折叠处理
                expandRow=[];
            }
			self.setState({
				checkedArray,expandRow,
				isadd,
				isNC01,
				isNC02,
				voucherStatus: 'update',
				headFunc,
				evidenceData,
				unitOrg,
				evidenceColumns: finalColums,
				update,
				saveData,
				abandonShow
				//datavoucher
			});
		} else {
			evidenceData.rows = [];
			self.setState({
				evidenceData
			});
		}
	}
	referValue = (value, index) => {
	};

	focus = () => {
	};

	//正则校验只能输入数字
	numCheck = (num, scale) => {
		let reg = new RegExp(`^\\d+\\.?\\d{0,${scale === -1 || !scale ? this.scale : scale}}$`, 'g');
		let flag = reg.test(num);
		return flag || num === '';
	};
	//币种请求成功更新分录行精度汇率
	modifyRows = (originData, response) => {
		let {
			maxconverr,
			scale,
			NC001,
			NC002,
			excrate2,
			globalroundtype,
			grouproundtype,
			globalscale,
			groupscale,
			orgroundtype,
			orgscale,
			excrate3,
			excrate4,
			excrate2scale,
			excrate3scale,
			excrate4scale,
			roundtype,
			pricescale,
			priceroundtype,
			orgmode,
			groupmode,
			globalmode
		} = response;
		if (NC001) {
			originData.groupType = NC001;
		}
		if (NC002) {
			originData.globalType = NC002;
		}
		if (excrate2) {
			//根据核算账簿赋值默认汇率，默认币种，默认业务单元
			originData.excrate2 = {
				value: excrate2
			};
		}
		if (excrate3) {
			//根据核算账簿赋值默认汇率，默认币种，默认业务单元
			originData.excrate3 = {
				value: excrate3
			};
		}
		if (excrate4) {
			originData.excrate4 = {
				value: excrate4
			};
		}
		if (excrate2scale) {
			originData.excrate2scale = {
				value: excrate2scale
			};
		}
		if (excrate3scale) {
			originData.excrate3scale = {
				value: excrate3scale
			};
		}
		if (excrate4scale) {
			originData.excrate4scale = {
				value: excrate4scale
			};
		}
		if (maxconverr) {
			originData.maxconverr = {
				value: maxconverr
			};
		}
		if (orgroundtype) {
			originData.orgroundtype = {
				value: orgroundtype
			};
		}
		if (grouproundtype) {
			originData.grouproundtype = {
				value: grouproundtype
			};
		}
		if (globalroundtype) {
			originData.globalroundtype = {
				value: globalroundtype
			};
		}
		if (globalscale) {
			originData.globalscale = globalscale;
		}
		if (groupscale) {
			originData.groupscale = groupscale;
		}
		if (orgscale) {
			originData.orgscale = orgscale;
		}
		if (scale) {
			originData.scale = scale;
		}
		if (roundtype) {
			originData.roundtype = {
				value: roundtype
			};
		}
		if (pricescale) {
			originData.pricescale = {
				value: pricescale
			};
		}
		if (priceroundtype) {
			originData.priceroundtype = {
				value: priceroundtype
			};
		}
		if (orgmode) {
			originData.orgmode = {
				value: orgmode
			};
		}
		if (groupmode) {
			originData.groupmode = {
				value: groupmode
			};
		}
		if (globalmode) {
			originData.globalmode = {
				value: globalmode
			};
		}
	};
	//根据币种获取汇率
	queryRate = (data, url, originData) => {
		let self = this;
		let { evidenceColumns } = self.state;
		ajax({
			url,
			data: data,
			success: function(response) {
				// scale roundtype excrate2 excrate3 excrate4 pricescale priceroundtype orgmode groupmode globalmode
				let {
					maxconverr,
					scale,
					NC001,
					NC002,
					excrate2,
					excrate3,
					excrate4,
					roundtype,
					pricescale,
					priceroundtype,
					orgmode,
					groupmode,
					globalmode
				} = response.data;
				self.modifyRows(originData, response.data);
				if (originData.amount.value) {
					//切换汇率请求同步跟进原币计算
					let { data, currInfo } = self.exportData(originData);
					data.pk_currtype = originData.pk_currtype.value;
					data.pk_accountingbook = originData.pk_accountingbook.value;
					data.prepareddate = originData.prepareddate.value;
					let countValue = amountconvert(data, currInfo, 'amount');
					countRow(originData, countValue, 'amount');
					self.totleCount();
				}
			}
		});
	};

	//动态新增集团和全局列
	addCurry = (data) => {
		data.map((item, index) => {
			item.dataIndex = item.name;
			item.width = '200px';
			item.render = (text, record, index) => {
				//根据字段类型，渲染不同的组件
				let self = this;
				let { pk_accountingbook, prepareddate, evidenceColumns, voucherView } = self.state;
				let { rows } = self.state.evidenceData;
				let originData = self.findByKey(record.key, rows);
				let errorMsg, errorBorder;
				let defaultValue = {};
				let { data, currInfo } = self.exportData(record);
				if (text.value !== null) {
					//defaultValue = { refname: text[item.key].display, refpk: text[item.key].value };
				}
				switch (item.itemType) {
					case 'amount-rate':
						return !voucherView&&!record.rowEdited ? (
							<div className="textCenter">
								<NCNumber
									scale={record[item.key]&&record[item.key].scale ? Number(record[item.key].scale) : Number(record.groupscale)}
									disabled={record.rowEdited}
									value={
										text[item.key].value && text[item.key].value != '0' ? text[item.key].value : ''
									}
									onFocus={(v) => {
										record[item.key].visible = true;
									}}
									onBlur={(v) => {
										record[item.key].visible = false;
										if (v && v.indexOf('=') == -1 && v.indexOf(' ') == -1) {
											let countValue = amountconvert(data, currInfo, item.key);
											countRow(record, countValue, item.key);
										}
									}}
									onKeyUp={(event) => {
										let {
											rows,
											groupcreditamountTotle,
											groupdebitamountTotle,
											globalcreditamountTotle,
											globaldebitamountTotle
										} = this.state.evidenceData;
										let originData = this.findByKey(record.key, rows);
										if (event.keyCode == '187' || event.keyCode == 229) {
											this.equal(item.attrcode, originData);
											let { data, currInfo } = this.exportData(originData);
											let countValue = amountconvert(data, currInfo, item.attrcode);
											countRow(originData, countValue,  item.attrcode);
											// this.totleCount(item.attrcode, 'localdebitamountTotle');
										} else if (event.keyCode == '32' && originData.excrate3.value) {
											emptyEnter(item.attrcode, originData);
											// this.totleCount(item.attrcode, 'localdebitamountTotle');
										}
									}}
									onChange={(v) => {
										let { localcreditamountTotle, localdebitamountTotle } = this.state.evidenceData;
										if(v && v !== '=' && v.indexOf('=') == -1 && v.indexOf(' ') == -1){
											record[item.attrcode].value=v 
										this.setState({
											evidenceData: this.state.evidenceData
										});
									}
									}}
								/>
							</div>
						) : (
							<div class='curryClass'><span>
								{text[item.key].value&&self.commafy(text[item.key].value)}
								<Rate rate={text.excrate3 ? text.excrate3.value : ''} ifShow={text[item.key]} />
							</span></div>
						);
					default:
						break;
				}
			};
		});
		return data;
	};
	//动态新增集团和全局列
	addGlobal = (data) => {
		data.map((item, index) => {
			item.dataIndex = item.name;
			item.width = '200px';
			item.render = (text, record, index) => {
				//根据字段类型，渲染不同的组件
				let self = this;
				let { pk_accountingbook, prepareddate, evidenceColumns, voucherView } = self.state;
				let { rows } = self.state.evidenceData;
				let originData = self.findByKey(record.key, rows);
				let errorMsg, errorBorder;
				let defaultValue = {};
				let { data, currInfo } = self.exportData(record);
				if (text.value !== null) {
					defaultValue = { refname: text[item.key].display, refpk: text[item.key].value };
				}
				switch (item.itemType) {
					case 'amount-rate':
						return !voucherView&&!record.rowEdited ? (
							<div className="textCenter">
								<NCNumber
									// scale={originData.globalscale ? Number(originData.globalscale) : 2}
									scale={record[item.key]&&record[item.key].scale? Number(record[item.key].scale) :  Number(record.globalscale)}
									disabled={record.rowEdited}
									value={
										text[item.key].value && text[item.key].value != '0' ? text[item.key].value : ''
									}
									onFocus={(v) => {
										record[item.key].visible = true;
									}}
									onBlur={(v) => {
										record[item.key].visible = false;
										if (v) {
											let countValue = amountconvert(data, currInfo, item.key);
											countRow(record, countValue, item.key);
										}
									}}
									onKeyUp={(event) => {
										let {
											rows,
											localcreditamountTotle,
											localdebitamountTotle,
											groupcreditamountTotle,
											groupdebitamountTotle,
											globalcreditamountTotle,
											globaldebitamountTotle
										} = this.state.evidenceData;
										let originData = this.findByKey(record.key, rows);
										if (event.keyCode == '187' || event.keyCode == 229) {
											this.equal(item.attrcode, originData);
											let { data, currInfo } = this.exportData(originData);
											let countValue = amountconvert(data, currInfo, item.attrcode);
											countRow(originData, countValue,  item.attrcode);
											// this.totleCount(item.attrcode, 'localdebitamountTotle');
										} else if (event.keyCode == '32' && originData.excrate3.value) {
											emptyEnter(item.attrcode, originData);
											// this.totleCount(item.attrcode, 'localdebitamountTotle');
										}
									}}
									onChange={(v) => {
										let { localcreditamountTotle, localdebitamountTotle } = this.state.evidenceData;
										if (v && v !== '=' && v.indexOf('=') == -1 && v.indexOf(' ') == -1){
											record[item.attrcode].value =v
											this.setState({
												evidenceData: this.state.evidenceData
											});
										}
									}}
								/>
								{/* {errorMsg} */}
							</div>
						) : (
							<div class='curryClass'><span>
								{text[item.key].value&&self.commafy(text[item.key].value)}
								<Rate rate={text.excrate4 ? text.excrate4.value : ''} ifShow={text[item.key]} />
							</span></div>
						);
					default:
						break;
				}
			};
		});
		return data;
	};
	//切换核算账簿后续操作
	changePK = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';

		let { prepareddate, saveData } = self.state;
		let pk_accpont = {
			pk_accountingbook: saveData.pk_accountingbook.value,
			date: prepareddate.value,
			isVoucher:true
		};
		this.ifCheck(pk_accpont, url);
	};
	querybook = (dataAjax, url) => {
		let self = this;
		//根据核算账簿动态加载全局集团列 精度 汇率加载默认值
		let {
			evidenceColumns,
			isadd,
			evidenceData,
			isNC01,
			isNC02,
			unitOrg,
			groupScale,
			globalScale,
			orgScale,
			saveData,
			headFunc,
			prepareddate,
			attachModal,expandRow
		} = self.state;
		let number = {
			//业务单元列
			title: this.state.json['20021005card-000072'],/* 国际化处理： 业务单元*/
			key: 'pk_unit_v',
			dataIndex: 'pk_unit_v',
			width: '200px',
			render: (text, record, index) => {
				//"uapbd/refer/org/BusinessUnitTreeRef"
				let self = this;
				let { voucherView } = self.state;
				let referUrl = 'uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef/index.js';
				let attrcode = 'pk_unit_v';
				let editable =
					record['pk_unit_v'] && record['pk_unit_v'].editable ? record['pk_unit_v'].editable : false;
				return !voucherView&&!record.rowEdited ? (
					<ReferLoader
						tag={attrcode}
						refcode={referUrl}
						disabled={record.rowEdited}
						value={{ refname: text.display, refpk: text.value }}
						queryCondition={() => {
							return {
								pk_accountingbook: self.state.headFunc.pk_accountingbook.value,
								VersionStartDate: self.state.prepareddate.value,
								TreeRefActionExt: 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
							};
						}}
						onChange={(v) => {
							let { rows } = this.state.evidenceData;
							let originData = this.findByKey(record.key, rows);
							if (originData && originData.pk_unit_v.value) {
								//切换业务单元清空辅助核算
								if (originData.ass && originData.ass.length != '0') {
									originData.ass = [];
									originData.assid = {
										...originData.assid,
										display: '',
										value: ''
									};
								}
							}
							if (originData) {
								originData.pk_unit_v = {
									display: v.refname ? v.refname : '',
									value: v.values ? v.values.pk_vid.value : ''
								};
								originData.pk_unit = {
									display: '',
									value: v.refpk ? v.refpk : ''
								};
							}
							this.setState({
								evidenceData: this.state.evidenceData
							});
						}}
					/>
				) : (
					<span>{text ? text.display : ''}</span>
				);
			}
		};
		let addGroup = [
			{
				title: this.state.json['20021005card-000073'],/* 国际化处理： 集团本币借方发生额*/
				attrcode: 'groupdebitamount',
				key: 'groupdebitamount',
				itemType: 'amount-rate'
			},
			{
				title: this.state.json['20021005card-000074'],/* 国际化处理： 集团本币贷方发生额*/
				itemType: 'amount-rate',
				key: 'groupcreditamount',
				attrcode: 'groupcreditamount'
			}
		];
		let addGlobal = [
			{
				title: this.state.json['20021005card-000075'],/* 国际化处理： 全局本币借方发生额*/
				key: 'globaldebitamount',
				attrcode: 'globaldebitamount',
				itemType: 'amount-rate'
			},
			{
				title: this.state.json['20021005card-000076'],/* 国际化处理： 全局本币贷方发生额*/
				itemType: 'amount-rate',
				key: 'globalcreditamount',
				attrcode: 'globalcreditamount'
			}
		];
		let addAll = [
			{
				title: this.state.json['20021005card-000073'],/* 国际化处理： 集团本币借方发生额*/
				key: 'groupdebitamount',
				attrcode: 'groupdebitamount',
				itemType: 'amount-rate'
			},
			{
				title: this.state.json['20021005card-000074'],/* 国际化处理： 集团本币贷方发生额*/
				itemType: 'amount-rate',
				key: 'groupcreditamount',
				attrcode: 'groupcreditamount'
			},
			{
				title: this.state.json['20021005card-000075'],/* 国际化处理： 全局本币借方发生额*/
				key: 'globaldebitamount',
				itemType: 'amount-rate',
				attrcode: 'globaldebitamount'
			},
			{
				title: this.state.json['20021005card-000076'],/* 国际化处理： 全局本币贷方发生额*/
				itemType: 'amount-rate',
				key: 'globalcreditamount',
				attrcode: 'globalcreditamount'
			}
		];

		ajax({
			url,
			data: dataAjax,
			success: function(response) {
				let cloneNewLine = deepClone(evidenceColumns);
				if (response.data) {
					let {
						isShowUnit,
						currinfo,
						groupCurrinfo,
						globalCurrinfo,
						excrate2,
						excrate3,
						excrate4,
						unit,
						NC001,
						NC002,
						unit_v,
						groupscale, // 集团精度
						scale, // 默认币种精度
						globalscale, // 全局精度
						orgscale, // 组织精度
						pk_vouchertype,
						num,
						isEditVoucherNO,
						isAttachmentMust,
						isFreevalueDefault, //是否带出辅助核算
						bizDate,
						orgmode, //汇率计算方式 是否为除 true=除
						groupmode, //集团本币汇率计算方式 是否为除 true=除
						globalmode, //全局本币汇率计算方式 是否为除 true=除
						roundtype, //原币进舍规则
						orgroundtype, //本币进舍规则
						grouproundtype, //集团本币进舍规则
						globalroundtype, //全局本币进舍规则
						pricescale, //单价精度
						priceroundtype //单价进舍规则
					} = response.data;
					// self.props.form.setFormItemsValue(self.formId, {
					// 	prepareddate: { value: bizDate },
					// 	pk_vouchertype: { value: pk_vouchertype.value, display: pk_vouchertype.display },
					// 	num: { value: num }
					// });
					// if (!isEditVoucherNO) {
					// 	//控制凭证号编辑性
					// 	self.props.form.setFormItemsDisabled(self.formId, { num: true });
					// }
					// if (!isAttachmentMust) {
					// 	//控制赋单数据必录
					// 	self.props.form.setFormItemsRequired(self.formId, { attachment: true });
					// }
					attachModal = isAttachmentMust;
					prepareddate.value = bizDate;
					saveData.prepareddate.value = bizDate;
					saveData.num.value = num;
					saveData.pk_vouchertype.display = pk_vouchertype.display;
					saveData.pk_vouchertype.value = pk_vouchertype.value;
					saveData.pk_accountingbook.value = dataAjax.pk_accountingbook;
					saveData.showUnit = isShowUnit;
					saveData['paraInfo'] = response.data;
					headFunc.pk_accountingbook.value = dataAjax.pk_accountingbook;

					if (unit) {
						unitOrg.value = unit.value;
					}
					if (isShowUnit) {
						if (!isadd) {
							cloneNewLine.splice(3, 0, number);
							isadd = true;
						}
						self.updateRows(evidenceData, response.data);
					}
					if (!isShowUnit) {
						self.updateRows(evidenceData, response.data);
						if (isadd) {
							cloneNewLine = cloneNewLine.filter(function(v, i, a) {
								return v.key != number.key;
							});
							isadd = false;
						}
					}
					if (NC001) {
						//集团
						if (!isNC01) {
							let addCurryLine = self.addCurry(addGroup);
							cloneNewLine = cloneNewLine.concat(addCurryLine);
							isNC01 = true;
						}
					}
					if (!NC001) {
						if (isNC01) {
							cloneNewLine = cloneNewLine.filter(
								(item) => !addGroup.map((item) => item.key).includes(item.key)
							);
							isNC01 = false;
						}
					}
					if (NC002) {
						//全局
						if (!isNC02) {
							let addGlobalLine = self.addGlobal(addGlobal);
							cloneNewLine = cloneNewLine.concat(addGlobalLine);
							isNC02 = true;
						}
					}
					if (!NC002) {
						if (isNC02) {
							cloneNewLine = cloneNewLine.filter(
								(item) => !addGlobal.map((item) => item.key).includes(item.key)
							);
							isNC02 = false;
						}
					}
					//数量列 切换核算账簿清除
					cloneNewLine = cloneNewLine.filter(function(v, i, a) {
						return v.key != 'quantity';
                    });
                    if(expandRow&&expandRow.length!=0){//分录展开控制，保存前展开，保存成功统一折叠处理
						expandRow=[];
					}
					self.setState(
						{
							isadd,
							isNC01,
							isNC02,
							isFreevalueDefault: isFreevalueDefault,
                            headFunc,
                            expandRow,
							prepareddate,
							evidenceData,
							evidenceColumns: cloneNewLine,
							saveData,
							attachModal
						});
				}
			}
		});
	};

	//是否二级核算请求
	ifCheck = (dataAjax, url, clearRows) => {
		let self = this;
		//根据核算账簿动态加载全局集团列 精度 汇率加载默认值
		let {
			evidenceColumns,
			isadd,
			evidenceData,
			isNC01,
			isNC02,
			unitOrg,
			groupScale,
			globalScale,
			orgScale,
			saveData,
			headFunc,
			prepareddate,
			attachModal
		} = self.state;
		if (clearRows) {
			for (let k in saveData) {
				//清空保存数据
				if (isObj(saveData[k])) {
					saveData[k].value = '';
					saveData[k].display = '';
				}
			}
			if (evidenceData.localcreditamountTotle.value) {
				//清空合计值
				evidenceData.localcreditamountTotle.value = '';
			}
			if (evidenceData.localdebitamountTotle.value) {
				evidenceData.localdebitamountTotle.value = '';
			}
			let rows = [ evidenceData.rows[0] ];
			for (let i = 0, len = rows.length; i < len; i++) {
				//清空分录
				for (let k in rows[i]) {
					if (isObj(rows[i][k])) {
						rows[i][k].value = '';
						rows[i][k].display = '';
						if (rows[i][k].editable) {
							rows[i][k].editable = false;
						}
					}
				}
			}
			evidenceData.rows = rows;
			self.setState(
				{
					saveData,
					voucherStatus: 'save',
					evidenceData
				},
				() => {
					self.querybook(dataAjax, url);
				}
			);
		} else {
			self.querybook(dataAjax, url);
		}
	};
	//更新分录行精度
	updateRows = (evidenceData, response) => {
		evidenceData.rows.forEach(function(item, index) {
			if (!item.pk_accasoa.value) {
				//会计科目优先级最高
				itemUpdate(item, response);
			}
		});
		//新增行赋值
		itemUpdate(evidenceData.newLine, response);
	};
	//是否有数量列
	ifNumber = (data, index1) => {
		let self = this;
		let { evidenceColumns, evidenceData } = self.state;
		data && (evidenceData.rows[index1].flag = true);
		!data && (evidenceData.rows[index1].flag = false);
		let quantity = {
			title: this.state.json['20021005card-000071'],/* 国际化处理： 数量/单价*/
			key: 'quantity',
			dataIndex: 'quantity',
			width: '200px',
			render: (text, record, index) => {
				let { pk_accountingbook, prepareddate, voucherView } = self.state;
				//let editable=record[item.attrcode].editable ? record[item.attrcode].editable:false
				let { rows } = self.state.evidenceData;
			//	let originData = self.findByKey(record.key, rows);
				let { data, currInfo } = self.exportData(record);
				if (record.flag) {
					if (!voucherView&&!record.rowEdited) {
						return (
							<div className="number">
								<NCNumber
									className="count"
									//disabled={editable}
									scale={record.debitquantity&&record.debitquantity.scale ? Number(record.debitquantity.scale) : record.quantityscale&&Number(record.quantityscale.value)}
									value={record.debitquantity&&record.debitquantity.value || ''}
									placeholder={this.state.json['20021005card-000043']}/* 国际化处理： 请输入数量*/
                                    //disabled={record.debitquantity.value!='0'?false:true}
                                    disabled={record.rowEdited}
									onKeyUp={(v) => {}}
									onBlur={(v) => {
										let { evidenceData } = self.state;
										//let originData = self.findByKey(record.key, evidenceData.rows);
										if (v) {
											let countValue = amountconvert(data, currInfo, 'debitquantity');
											countRow(record, countValue, 'debitquantity');
										}
										self.setState({
											evidenceData
										});
									}}
									onChange={(v) => {
										let { evidenceData } = self.state;
										//let originData = self.findByKey(record.key, evidenceData.rows);
										if (record) {
											record.debitquantity = {
												value: v
											};
										}
										self.setState(
											{
												evidenceData
											}
										);
									}}
								/>
								{/* <span>{data.name.text}</span> */}

								<span className="space" />
								<NCNumber
									className="price"
									//disabled={editable}
									scale={record.price&&record.price.scale? Number(record.price.scale) : record.pricescale&&Number(record.pricescale.value)}
									value={record.price&&record.price.value || ''}
									placeholder={this.state.json['20021005card-000045']}/* 国际化处理： 请输入单价*/
									onKeyUp={(v) => {}}
									onBlur={(v) => {
										let { evidenceData } = self.state;
										//let originData = self.findByKey(record.key, evidenceData.rows);
										if (v) {
											let countValue = amountconvert(data, currInfo, 'price');
											countRow(record, countValue, 'price');
										}
										self.setState({
											evidenceData
										});
									}}
									onChange={(v) => {
										let { evidenceData } = self.state;
										if (record) {
											record.price = {
												value: v
											};
										}
										self.setState({
											//evidenceColumns:cloneNewLine,
											evidenceData
										});
									}}
								/>
							</div>
						);
					} else {
						return (
							<span>
								{record.debitquantity.value || ''}/{record.price.value || ''}
							</span>
						);
					}
				} else {
					return <span />;
				}
			}
		};
		//判断是否有数量这一列
		let num = false;
		for (let v of evidenceData.rows) {
			if (v.flag == true) {
				num = true;
				break;
			}
		}

		// evidenceColumns.forEach(item=>{
		// 	if(item.key=='quantity'){

		// 	}
		// })
		let originData = self.findByKey(quantity.key, evidenceColumns);
		if (!(originData&&JSON.stringify(originData)!={})) {
			if (num) {
				let cloneNewLine = deepClone(evidenceColumns);
				for(let i=0,len=cloneNewLine.length;i<len;i++){
					if(cloneNewLine[i].attrcode=='pk_currtype'){
						cloneNewLine.splice(i+1, 0, quantity);
						break;
					}
				}
			//	cloneNewLine.splice(5, 0, quantity);
				self.setState({
					evidenceColumns: cloneNewLine
				});
			}
		} else {
			if (!num) {
				evidenceColumns = evidenceColumns.filter(function(v, i, a) {
					return v.key != 'quantity';
				});
				self.setState({
					evidenceColumns
				});
			}
			self.setState({
				evidenceData
			});
		}
	};
	//凭证分录添加复选框
	onAllCheckChange = () => {
		return false; //屏蔽全选功能
		let self = this;
		let checkedArray = [];
		//let listData = self.state.data.concat();
		let selIds = [];
		// let id = self.props.multiSelect.param;
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}

		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		});
	};
	//凭证分录复选框点击
	onCheckboxChange = (text, record, index) => {
		let self = this;
		let allFlag = false;
		let checkedArray = self.state.checkedArray.concat();
		let { getrow, evidenceData } = this.state;
		let selectRowKey;
		evidenceData.rows.map((row, rowid) => {
			if (index == rowid) {
				row.rowEdited = false;
			} else {
				row.rowEdited = true;
			}
		});
		for (var i = 0; i < self.state.checkedArray.length; i++) {
            
			if (!checkedArray[i]) {
				allFlag = false;
				// break;
			} else {
				// allFlag = true;
            }
            checkedArray[i]=false;
        }
		checkedArray[index] = !self.state.checkedArray[index];
		if(checkedArray[index]){
			selectRowKey=record.key;
		}else{
			selectRowKey='';
		}
		self.setState({
            evidenceData,
			getrow: selectRowKey,
			checkedAll: allFlag,
			checkedArray: checkedArray
		});
		event.stopPropagation();
	};
	//表格添加复选框
	renderColumnsMultiSelect(columns) {
		const { data, checkedArray } = this.state;
		const { multiSelect } = this.props;

		let select_column = {};
		let indeterminate_bool = false;
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
            }
            let openRowCol = {
				title: '',
				key: 'open',
				dataIndex: 'open',
				width: '50px',
				fixed: "right",
				render: (text, record, index) => {
					let errorTip = <div>{record.errmessage ? record.errmessage.value : ''}</div>;
					let checkTip = <div>{record.checkmessage ? record.checkmessage.value : ''}</div>;
					return [
						record.checkmessage && record.checkmessage.value && (
							<NCTooltip trigger="hover" placement={'top'} inverse overlay={checkTip}>
								<i className="iconfont icon-zhuyi1" />
							</NCTooltip>
						),
						record.errmessage &&record.errmessage.value&& (
							<NCTooltip trigger="hover" placement={'top'} inverse overlay={errorTip}>
								<i className="iconfont icon-shibai" />
							</NCTooltip>
						)
					];
				}
			};
			let defaultColumns = [
				{
					title: (
						<Checkbox
							className="table-checkbox"
							checked={this.state.checkedAll}
							indeterminate={indeterminate_bool && !this.state.checkedAll}
							onChange={this.onAllCheckChange}
						/>
					),
					key: 'checkbox',
					dataIndex: 'checkbox',
					width: '50px',
					fixed: "left",
					render: (text, record, index) => {
						let checkTip = <div>{record.checkmessage ? record.checkmessage.value : ''}</div>;
						return (
							<Checkbox
								checked={this.state.checkedArray[index]}
								onChange={this.onCheckboxChange.bind(this, text, record, index)}
							/>

							// {/* {!record.showCheck ? (
							// 	<NCTooltip trigger="hover" placement={'top'} inverse overlay={checkTip}>
							// 		<i className="iconfont icon-zhuyi1" />
							// 	</NCTooltip>
							// ) : (
							// 	''
							// )} */}
						);
					}
				}
			];
            columns = defaultColumns.concat(columns);
            columns.push(openRowCol)
		}
		return columns;
	}
	//动态渲染表格
	getTable = (evidenceColumns, evidenceRows) => {
		let tableArea_H = this.refs.tableArea ? getComputedStyle(this.refs.tableArea, null).height : '400px';
		let height = tableArea_H.replace('px', '') - 200;
		let columns = this.renderColumnsMultiSelect(evidenceColumns);
		return (
            <div class='voucherTable'>
                <Table
                    bordered
                    onExpand={this.getData}
                    onRowClick={this.getRow}
                    expandedRowKeys={this.state.expandRow}
                    expandedRowRender={this.expandedRowRender}
                    expandIconColumnIndex={1}
                    columns={columns}
					data={evidenceRows}
					bodyStyle={{height:'340px'}}
                    scroll={{
                        x:true,
					   y:340
                    }}
                />
            </div>
		);
	};
	//金额格式的修改
	formatDot(value, len) {
		let formatVal, dotSplit, val;

		val = (value.value || 0).toString();

		dotSplit = val.split('.');

		if (dotSplit.length > 2 || !value.value) {
			return value.value;
		}

		if (value.scale && value.scale != '-1') {
			len = value.scale;
		}

		len = len || 2;

		if (val.indexOf('.') > -1) {
			formatVal = val.substring(0, val.indexOf('.') + len + 1);
		} else {
			formatVal = val;
		}

		return formatVal;
	}

	//数字转换成千分位 格式
	commafy(num) {
		let pointIndex, intPart, pointPart;

		if (isNaN(num)) {
			return '';
		}
		num = num + '';
		if (/^.*\..*$/.test(num)) {
			pointIndex = num.lastIndexOf('.');
			intPart = num.substring(0, pointIndex);
			pointPart = num.substring(pointIndex + 1, num.length);
			intPart = intPart + '';
			let re = /(-?\d+)(\d{3})/;
			while (re.test(intPart)) {
				intPart = intPart.replace(re, '$1,$2');
			}
			num = intPart + '.' + pointPart;
		} else {
			num = num + '';
			let re = /(-?\d+)(\d{3})/;
			while (re.test(num)) {
				num = num.replace(re, '$1,$2');
			}
		}
		return num;
	}

	formatAcuracy(value, len) {
		// return this.toThousands(formatVal);
		if (value.value === null || value.value === undefined) {
			return value.value;
		}
		return this.commafy(this.formatDot(value, len));
	}

	changemoney(evidenceData) {
		if (evidenceData.localdebitamountTotle.value == evidenceData.localcreditamountTotle.value) {
			if (evidenceData.localdebitamountTotle.value < 0) {
				return this.state.json['20021005card-000121'] + convertCurrency(sum(evidenceData.localcreditamountTotle.value));/* 国际化处理： 负*/
			} else {
				return convertCurrency(evidenceData.localcreditamountTotle.value);
			}
		}
	}
	render() {
		let self = this;
		let { table, button, search } = self.props;
		let {
			evidenceColumns,
			options,
			placeholder,
			disabled,
			evidenceData,
			MsgModalAll,
			VrifyModalShow,
			SaveErrorModalShow,
			headContent,
			showUploader,
			target,
			SubjectModalShow,
			AssistAccModalShow,
			ExplanationModalShow,
			changeNumberShow,
			update,
			checkNot,
			apportionmentShow,
			voucherView,
			footContent,
			findRows,
			errorFlag,
			saveData,
			getrow,
			prepareddate,
			headFunc,
			unitOrg,currentRowPrevass
		} = self.state;
		let apportionmentValue = {
			amount: 
				getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].amount
					? evidenceData.rows[getrow - 1].amount.value
					: '',
			pk_accasoa:
				getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].pk_accasoa
					? evidenceData.rows[getrow - 1].pk_accasoa.value
					: '',
			pk_accountingbook: saveData.pk_accountingbook
				? saveData.pk_accountingbook.value
				: headFunc.pk_accountingbook.value,
			prepareddate: prepareddate.value,
			pk_unit:
				getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].pk_unit
					? evidenceData.rows[getrow - 1].pk_unit.value
					: unitOrg.value
		};
		let evidenceRows = evidenceData.rows;
		return (
			<div id="evidence">
                <div className="table-area" ref="tableArea">
                    <div class='autoheight'>
                        {this.getTable(evidenceColumns, evidenceRows)}
                        </div>
					</div>
				<AssidModal
					voucherType={'gl_coordination'}
					showOrHide={AssistAccModalShow}
					assData={
						getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].ass&& evidenceData.rows[getrow - 1].ass.length!=0 ? (
							evidenceData.rows[getrow - 1].ass
						) : (
							currentRowPrevass
						)
					}
					assid={
						getrow && evidenceData.rows[getrow - 1] && evidenceData.rows[getrow - 1].assid ? (
							evidenceData.rows[getrow - 1].assid.value
						) : (
							''
						)
					}
					pk_accasoa={apportionmentValue.pk_accasoa}
					prepareddate={apportionmentValue.prepareddate}
					pk_accountingbook={apportionmentValue.pk_accountingbook}
					pk_org={apportionmentValue.pk_unit}
					isMultiSelectedEnabled={false}
					onConfirm={(assDatas) => {
						this.handleAssistAccModal(AssistAccModalShow, assDatas);
					}}
					handleClose={() => {
						let AssistAccModalShow = false;
						this.setState({ AssistAccModalShow });
					}}
				/>
				<ChangeNumberModal
					show={changeNumberShow}
					title={this.state.json['20021005card-000078']}/* 国际化处理： 修改汇率*/
					loadForm={findRows}
					onConfirm={(e) => {
						this.changeUnit(e);
					}}
					onCancel={() => {
						let changeNumberShow = false;
						this.setState({ changeNumberShow });
					}}
				/>
				<CheckOrNot
					show={checkNot}
					onConfirm={(e) => {
						let { checkNot } = this.state;
						checkNot = false;
						this.setState(
							{
								checkNot
							},
							() => {
								this.changePK();
							}
						);
					}}
					onCancel={(e) => {
						let { checkNot } = this.state;
						checkNot = false;
						this.setState({
							checkNot
						});
					}}
				/>
			</div>
		);
	}
}
function modifierMeta(props, meta, pk_acc) {
	let formId = 'head';
	let formIdtail = 'tail';
	let status = props.getUrlParam('status');
	let appcode = props.getUrlParam('c');
	meta[formId].status = status;
	//	meta.detailexpand.status = status;
	meta[formIdtail].status = status;

	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_accountingbook') {
			item.queryCondition = () => {
				return {
					TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
					appcode: appcode
				};
			};
		}
		if (item.attrcode == 'pk_vouchertype') {
			item.queryCondition = () => {
				return {
					GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
					isDataPowerEnable: 'Y',
					DataPowerOperationCode: 'fi',
					pk_org: pk_acc
				};
			};
		}
	});
	meta[formIdtail].items.map((item) => {
		if (item.attrcode == 'pk_org_v') {
			item.queryCondition = () => {
				let data = props.form.getFormItemsValue(formId, 'vbillcode').value;
				return { vbillcode: data };
			};
		}
	});
	let multiLang = props.MutiInit.getIntl('2052');
	let porCol = {
		attrcode: 'opr',
		label: multiLang && multiLang.get('20521030-0005'),
		visible: true,
		width: 200,
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			return status === 'browse' ? (
				<span
					onClick={() => {
						props.cardTable.toggleRowView(tableId, record);
					}}
				>
					{this.state.json['20021005card-000079']/* 国际化处理： 切换视图*/}
				</span>
			) : (
				<div className="currency-opr-col">
					<span
						className="currency-opr-del"
						onClick={(e) => {
							props.cardTable.openModel(tableId, 'edit', record, index);
							e.stopPropagation();
						}}
					>
						<i className="icon iconfont icon-gengduo" />
					</span>
					&nbsp;&nbsp;
					<span
						className="currency-opr-del"
						onClick={(e) => {
							props.cardTable.deleteRowsByIndex(tableId, index);
							e.stopPropagation();
						}}
					>
						<i className="icon iconfont icon-shanchu" />
					</span>
				</div>
			);
		}
	};
	//meta[tableId].items.push(porCol);

	return meta;
}
function itemUpdate(item, response) {
	let {
		isShowUnit,
		currinfo,
		groupCurrinfo,
		globalCurrinfo,
		excrate2,
		excrate3,
		excrate4,
		excrate2scale,
		excrate3scale,
		excrate4scale,
		unit,
		NC001,
		NC002,
		unit_v,
		groupscale, // 集团精度
		scale, // 默认币种精度
		globalscale, // 全局精度
		orgscale, // 组织精度
		pk_vouchertype,
		num,
		isEditVoucherNO,
		isAttachmentMust,
		bizDate,
		orgmode, //汇率计算方式 是否为除 true=除
		groupmode, //集团本币汇率计算方式 是否为除 true=除
		globalmode, //全局本币汇率计算方式 是否为除 true=除
		roundtype, //原币进舍规则
		orgroundtype, //本币进舍规则
		grouproundtype, //集团本币进舍规则
		globalroundtype, //全局本币进舍规则
		pricescale, //单价精度
		priceroundtype //单价进舍规则
	} = response;
	if (scale) {
		item.scale = scale;
	}
	if (orgscale) {
		item.orgscale = orgscale;
	}
	if (groupscale) {
		item.groupscale = groupscale;
	}
	if (globalscale) {
		item.globalscale = globalscale;
	}
	if (NC001) {
		item.groupType = NC001;
	}
	if (NC002) {
		item.globalType = NC002;
	}
	if ((!item.excrate2||!item.excrate2.value)&&(excrate2||excrate2=='0')) {
		//根据核算账簿赋值默认汇率，默认币种，默认业务单元
		item.excrate2 = {
			value: excrate2
		};
	}
	if ((!item.excrate3||!item.excrate3.value)&&(excrate3||excrate3=='0')) {
		//根据核算账簿赋值默认汇率，默认币种，默认业务单元
		item.excrate3 = {
			value: excrate3
		};
	}
	if ((!item.excrate4||!item.excrate4.value)&&(excrate4||excrate4=='0')) {
		item.excrate4 = {
			value: excrate4
		};
	}
	if ((!item.excrate2scale||!item.excrate2scale.value)&&excrate2scale) {
		item.excrate2scale = {
			value: excrate2scale
		};
	}
	if ((!item.excrate3scale||!item.excrate3scale.value)&&excrate3scale) {
		item.excrate3scale = {
			value: excrate3scale
		};
	}
	if ((!item.excrate4scale||!item.excrate4scale.value)&&excrate4scale) {
		item.excrate4scale = {
			value: excrate4scale
		};
	}
	item.orgmode = {
		value: orgmode
	};
	item.groupmode = {
		value: groupmode
	};
	item.globalmode = {
		value: groupmode
	};

	if (roundtype) {
		//原币进舍规则
		item.roundtype = {
			value: roundtype
		};
	}
	if (orgroundtype) {
		//本币进舍规则
		item.orgroundtype = {
			value: orgroundtype
		};
	}
	if (grouproundtype) {
		//集团本币进舍规则
		item.grouproundtype = {
			value: grouproundtype
		};
	}
	if (globalroundtype) {
		//全局本币进舍规则
		item.globalroundtype = {
			value: globalroundtype
		};
	}
	if (pricescale) {
		//单价精度
		item.pricescale = {
			value: pricescale
		};
	}
	if (priceroundtype) {
		//单价进舍规则
		item.priceroundtype = {
			value: priceroundtype
		};
	}
	if (currinfo&&!item.pk_currtype.value) {
		//设置原币和组织本币币种，默认原币等于组织本币币种
		item.pk_currtype = {
			display: currinfo.display,
			value: currinfo.value
		};
		item.orgcurrtype = {
			display: currinfo.display,
			value: currinfo.value
		};
	}
	if (groupCurrinfo) {
		//集团币种
		item.groupcurrtype = {
			display: groupCurrinfo.display,
			value: groupCurrinfo.value
		};
	}
	if (globalCurrinfo) {
		//全局币种
		item.globalcurrtype = {
			display: globalCurrinfo.display,
			value: globalCurrinfo.value
		};
	}
	if (unit) {
		item.pk_unit = {
			display: unit.display,
			value: unit.value
		};
	}
	if (unit_v) {
		item.pk_unit_v = {
			display: unit_v.display,
			value: unit_v.value
		};
	}
}

Welcome = createPage({
	//initTemplate: initTemplate,
	mutiLangCode: '2002'
})(Welcome);

const enhanceWelcome = Enhance(Welcome);
export default enhanceWelcome;
