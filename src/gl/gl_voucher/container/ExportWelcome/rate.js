import PropTyps from 'prop-types'

const Rate = (props) => {
    const { rate ,ifShow}=props

	return ifShow.visible? (
        <div className='rateName'>
              <span>{this.state.json['20021005card-000122']/* 国际化处理： 汇率*/}：</span>
              <span>{rate}</span>
        </div>
	    ):''
  }

  Rate.propTyps={
    rate:PropTyps.string.isRequired
  }

  export default Rate;
