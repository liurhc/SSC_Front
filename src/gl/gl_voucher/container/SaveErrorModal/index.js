import React, { Component } from 'react';
//import moment from 'moment';
//import { Panel,PanelGroup,Label, Button, Radio,  Breadcrumb, Con, Row, Col, Tree, Message, Icon, Table, Modal,Checkbox } from 'tinper-bee';
//import Menu, { Item as MenuItem, Divider, SubMenu, MenuItemGroup } from 'bee-menus';
//import zhCN from 'rc-calendar/lib/locale/zh_CN';
// import Form from 'bee-form';
// import 'bee-form/build/Form.css';
import { high, base, ajax,getMultiLang } from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCModal: Modal,
	NCForm: Form,
	NCTextArea: TextArea
} = base;
const { Refer } = high;
import { InputItem } from '../../../public/components/FormItems';
import './index.less';

const { NCFormItem: FormItem } = Form;
const format = 'YYYY-MM-DD';
export default class SaveModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			checkFormNow: false, //控制表单form回调
			errorMessage: '',
			json:{}
		};
	}

	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		isButtonWhite: true,
		isButtonShow: true
	};

	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				//	initTemplate.call(this, this.props, this.state);
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}


	componentWillReceiveProps(nextProp) {
		if (nextProp.defaultValue && nextProp.defaultValue.errorMessage) {
			this.setState({
				errorMessage: nextProp.defaultValue.errorMessage
			});
		}else if (nextProp.show) {
			this.setState({
				errorMessage: ''
			});
		}
	}

	checkForm = () => {
		let { errorMessage } = this.state;
		this.props.onConfirm(errorMessage);
		this.setState({
			checkFormNow: false
		});
	};

	loadRows = () => {
		let { checkFormNow, errorMessage } = this.state;
		return (
			<Form submitCallBack={this.checkForm} showSubmit={false} checkFormNow={checkFormNow}>
				<FormItem
					inline={true}
					showMast={true}
					labelName={this.state.json['20021005card-000151']}/* 国际化处理： 备注:*/
					method="blur"
					inputAlfer="%"
				//	errorMessage="输入格式错误"
				>
					<TextArea
						name="vbillno"
						type="customer"
						//disabled={true}
						value={this.state.errorMessage}
						onChange={(v) => {
							let { errorMessage } = this.state;
							errorMessage = v;
							this.setState({
								errorMessage
							});
						}}
					/>
				</FormItem>
			</Form>
		);
	};

	render() {
		let {
			show,
			title,
			content,
			closeButton,
			icon,
			isButtonWhite,
			isButtonShow
		} = this.props;

		return (
			<Modal
				className="simpleModal junior save-error-modal"
				show={show}
				onHide={() => {
					let { errorMessage } = this.state;
					errorMessage = '';
					this.setState(
						{
							errorMessage
						},
						() => {
							this.props.onCancel(true);
						}
					);
				}}
			>
				<Modal.Header closeButton>
					<Modal.Title>{title}</Modal.Title>
				</Modal.Header>
				<Modal.Body>{this.loadRows()}</Modal.Body>
				{isButtonShow && (
					<Modal.Footer>
						<Button className="button-primary" onClick={() => this.checkForm()}>
							{ this.state.json['20021005card-000003']}
						</Button>
						<Button
							onClick={() => {
								let { errorMessage } = this.state;
								errorMessage = '';
								this.setState(
									{
										errorMessage
									},
									() => {
										this.props.onCancel(false);
									}
								);
							}}
						>
							{ this.state.json['20021005card-000004']}
						</Button>
					</Modal.Footer>
				)}
			</Modal>
		);
	}
}
