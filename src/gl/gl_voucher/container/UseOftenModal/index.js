import React, { Component } from 'react';
import { high, base, ajax ,getMultiLang} from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCModal: Modal,
	NCForm: Form
} = base;
const { Refer } = high;
import { CheckboxItem, RadioItem, InputItem } from '../../../public/components/FormItems';
import './index.less';
const { NCFormItem: FormItem } = Form;
const format = 'YYYY-MM-DD';

//调用的借贷等信息可以先不写，只显示编码和名称

export default class UseModal extends Component {
	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true
	};

	constructor(props) {
		super(props);
		this.state = {
			classData: [], //请求返回的凭证分类数据
			modals: [], //所有模板
			isLoading: false,
			account_pk: '',
			json:{}
		};
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
			//	initTemplate.call(this, this.props, this.state);
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}


	queryClass(book, saveDataClone) {
		let self = this;
		let { account_pk,modals} = self.state;
		if (book) {
			let url = '/nccloud/gl/voucher/commClassVoucherQuery.do';
			let data = {
				pk_accountingbook: book
			};
			ajax({
				url,
				data,
				success: function(response) {
					let { data } = response;
					if(modals&&modals.length!='0'){
						modals=[];
					}
					data&&data.length!=0&&data.forEach(item => {
						if(item.commonvoucher&&item.commonvoucher.length!=0){
							let newClass = {
								pk_commnvclass: item.commnvclass.pk_commnvclass,
								modals: item.commonvoucher,
								dispName: item.commnvclass.name
							};
							modals.push(newClass);
						}
					});
					self.setState({
						modals,
						account_pk: book
					});
				}
			});
		}
	}
	//获取模板
	// getModel(pk_commnvclass, dispName) {
	// 	let self = this;
	// 	let url = '/nccloud/gl/voucher/commvouquery.do';
	// 	//let url='/User/Post';
	// 	let data = {
	// 		pk_commnvclass: pk_commnvclass,
	// 	};

	// 	ajax({
	// 		url, data,
	// 		success:function(response){
	// 			let { data }=response;
	// 			let newClass = {
	// 				pk_commnvclass: pk_commnvclass,
	// 				modals: data,
	// 				dispName: dispName,
	// 			}
	// 			self.state.modals.push(newClass)
	// 			self.setState({
	// 				modals: self.state.modals
	// 			})
	// 		}
	// 	})
	// }
	//新增1，追加2
	load = (pk_commonvoucher, num) => {
		let self = this;
		let url = '/nccloud/gl/voucher/commvouload.do';
		//let url='/User/Post';
		let data = {
			pk_commonvoucher: pk_commonvoucher
		};
		ajax({
			url,
			data,
			success: function(response) {
				let { data } = response;
				self.props.onConfirm(data, num);
			}
		});
	};
	loadRows = () => {
		let self = this;
		let { modals } = self.state;
		let modalsList = modals.map((item, index) => {
			return (
				<div key={item.pk_commnvclass} className="block">
					{/* <div className="modalTip">
						<span>模板分类:</span> <span>{item.dispName}</span>
					</div> */}
					{item.modals && item.modals.length != 0 ? (
						item.modals.map((itemChild, indexChild) => {
							return (
								<div className="block-item nc-theme-area-split-bc">
									{/* <div className="modalInfo">
											<div className="modalCode"> 编码： {itemChild.commoncode}</div>
											<div>名称： {itemChild.commonsummary}</div>
										</div> */}
									<div className="code-name">
										<span className="code nc-theme-title-font-c">{itemChild.commoncode}</span>
										<span className="name nc-theme-title-font-c">{itemChild.commonsummary}</span>
									</div>
									<div className="main-info">
										<div className="main-info-item">
											{itemChild.account.length>0&&itemChild.account[0].direction=='C'?
												<span className="indicator type2">{this.state.json['20021005card-000010']}</span>:/* 国际化处理： 贷*/
												<span className="indicator type1">{this.state.json['20021005card-000009']}</span>}
											<span className="nc-theme-common-font-c">{itemChild.account.length>0?itemChild.account[0].accasoa:''}</span>
										</div>
										<div className="main-info-item">
											{itemChild.account.length>1&&itemChild.account[1].direction=='C'?
												<span className="indicator type2">{this.state.json['20021005card-000010']}</span>:/* 国际化处理： 贷*/
												<span className="indicator type1">{this.state.json['20021005card-000009']}</span>}
											<span className="nc-theme-common-font-c">{itemChild.account.length>1?itemChild.account[1].accasoa:''}</span>
										</div>
									</div>
									<div className="other-info">{itemChild.explanation}</div>
									<div className="btns">
										<Button onClick={this.load.bind(this, itemChild.pk_commonvoucher, 1)}>
											{this.state.json['20021005card-000113']}
										</Button>

										<Button onClick={this.load.bind(this, itemChild.pk_commonvoucher, 2)}>
											{this.state.json['20021005card-000186']}
										</Button>
									</div>
								</div>
							);
						})
					) : (
						<div/>
					)}
				</div>
			);
		});
		return modalsList;
	};
	render() {
		let {
			show,
			title,
			content,
			closeButton,
			icon,
			isButtonWhite,
			isButtonShow
		} = this.props;
		let { modals }=this.state;
		return (
			<Modal className="useoften-modal" show={show} onHide={() =>{//关闭同步清空,下次查询避免重复
				let  { modals}=this.state;
				this.setState({
					modals:[]
				},()=>{
					this.props.onCancel()
				})
				
			}}>
				<Modal.Header closeButton>
					<Modal.Title>{title}</Modal.Title>
				</Modal.Header>
				<Modal.Body>{modals&&modals.length!='0'?this.loadRows():<div class='number'>{this.state.json['20021005card-000296']}</div>}</Modal.Body>
				{isButtonShow && (
					<Modal.Footer>
						<Button
							onClick={() => {
								let  { modals}=this.state;
								this.setState({
									modals:[]
								},()=>{
									this.props.onCancel()
								})
							}}
						>
							{this.state.json['20021005card-000004']}
						</Button>
					</Modal.Footer>
				)}
			</Modal>
		);
	}
}
