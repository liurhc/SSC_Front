import React, { Component } from 'react';
import { high, base, ajax,getMultiLang } from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCModal: Modal,
	NCForm: Form
} = base;
const { Refer } = high;
import { CheckboxItem, RadioItem, TextAreaItem, ReferItem, InputItem } from '../../../public/components/FormItems';
import './index.less';

const { NCFormItem: FormItem } = Form;
const format = 'YYYY-MM-DD';
export default class MsgModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
			checkFormNow: false ,//控制表单form回调
			json:{}
		};
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
			//	initTemplate.call(this, this.props, this.state);
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}

	componentWillReceiveProps(nextProps) {}

	modalMessage = () => {
		this.props.onConfirm(true);
	};

	render() {
		let {
			show,
			title,
			content,
			closeButton,
			icon,
			isButtonWhite,
			isButtonShow
		} = this.props;

		return (
			<Modal
				className={isButtonShow ? 'msg-modal' : 'msg-modal footer-hidden'}
				show={show}
				onHide={this.close}
				animation={false}
				backdrop="static"
			>
				<Modal.Header closeButton={closeButton}>
					<Modal.Title>
						<span className={`title-icon iconfont ${icon}`} />
						<span className="bd-title-1">{title}</span>
						<span>
							<Icon
								className="close-icon iconfont icon-guanbi"
								onClick={() => {
									this.props.onCancel(true);
								}}
							/>
						</span>
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div>{this.state.json['20021005card-000060']/* 国际化处理： 是否保存*/}</div>
				</Modal.Body>
				<Modal.Footer>
					<Button
						className={`btn-2 ${isButtonWhite ? '' : 'btn-cancel'}`}
						onClick={() => this.modalMessage()}
					>
						{ this.state.json['20021005card-000003']}
					</Button>
					<Button
						className="btn-2 btn-cancel"
						onClick={() => {
							this.props.onCancel(false);
						}}
					>
						{this.state.json['20021005card-000004']}
					</Button>
				</Modal.Footer>
			</Modal>
		);
	}
}
