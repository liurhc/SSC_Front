import React, { Component } from 'react';
import { high, base, ajax,getMultiLang } from 'nc-lightapp-front';
import {
	CheckboxItem,
	RadioItem,
	TextAreaItem,
	ReferItem,
	SelectItem,
	InputItem,
	DateTimePickerItem
} from '../../../public/components/FormItems';
const { Refer } = high;
import createScript from '../../../public/components/uapRefer.js';
const format = 'YYYY-MM-DD';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	AutoComplete,
	NCDropdown: Dropdown,
	NCPanel: Panel,
	NCModal: Modal,
	NCForm,
	NCRangePicker: RangePicker
} = base;
import './index.less';
const { NCFormItem: FormItem } = NCForm;


const defaultProps12 = {
	prefixCls: 'bee-table',
	multiSelect: {
		type: 'checkbox',
		param: 'key'
	}
};

/* 检测类型是否为数组 */
export function isArray(param) {
	return Object.prototype.toString.call(param).slice(8, -1) === 'Array';
}

export default class SearchModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loadData: [], //查询模板加载数据
			listItem: {}, //模板数据对应值
			dataRows: [], //辅助核算列表数据
			closeButton: false,
			checkedAll: false,
			checkedArray: [ false, false, false ],
			json:{},
			pk_account:'',//会计期间过滤
			pk_accountingbook:''//凭证类别过滤
		};
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				//initTemplate.call(this, this.props, this.state);
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}

	componentWillReceiveProps(nextProp) {
		let self = this;
		let { listItem, loadData,pk_account } = self.state;
		if (loadData.length == 0) {
			nextProp.loadData.forEach((item, i) => {
				let key = '';
				if (item.itemType == 'refer') {
					// key={
					//     display:'',
					//     value:''
					// }
				} else {
					// key={
					//     value:''
					// }
					if (item.itemType == 'date') {
						key = [];
					}
				}
				let name = item.itemKey;
				listItem[name] = key;
			});
			self.setState(
				{
					//showModal: nextProp.showModal,
					loadData: nextProp.loadData,
					listItem
				},
				() => {
				}
			);
		}

		if(nextProp.pk_account && nextProp.pk_account !== pk_account){
			self.setState({
				pk_accountingbook:nextProp.pk_accountingbook,
				pk_account:nextProp.pk_account
			})
		}
		if (nextProp.defaultValue) {
			for (let e in nextProp.defaultValue) {
				if (listItem.hasOwnProperty(e)) {
					if (e == 'period' && nextProp.defaultValue[e]) {
						listItem[e] = {
							display: nextProp.defaultValue[e].display,
							value: nextProp.defaultValue[e].value,
							values: nextProp.defaultValue[e].values ? nextProp.defaultValue[e].values : {}
						};
					} else {
						listItem[e] = nextProp.defaultValue[e];
					}
				}
			}
			self.setState({
				listItem
			});
		}
	}

	componentDidMount() {}

	//表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

	confirm = () => {
		let { listItem } = this.state;
		this.setState(
			{
				showModal: false
			},
			() => {
				this.props.onConfirm(listItem);
			}
		);
	};
	queryList = (data) => {
		let self = this;
		let { listItem ,pk_account} = self.state;
		return data.length != 0 ? (
			data.map((item, i) => {
				switch (item.itemType) {
					case 'refer':
						let referUrl = item.config.refCode + '/index.js';
						if (!self.state[item.itemKey]) {
							{
								createScript.call(self, referUrl, item.itemKey);
							}
						} else {
							return (
								<FormItem
									inline={true}
									showMast={true}
									labelName={item.itemName}
									isRequire={true}
									method="change"
									//  change={self.handleGTypeChange.bind(this, 'contracttype')}
								>
									{self.state[item.itemKey] ? (
										self.state[item.itemKey]({
											value: {
												refname: listItem[item.itemKey] ? listItem[item.itemKey].display : '',
												refpk: listItem[item.itemKey] ? listItem[item.itemKey].value : ''
											},
											queryCondition:() => {

												if (item.attrcode == 'period') {
													//设置会计期间参照过滤
													item.queryCondition = () => {
														return {
															pk_accperiodscheme: pk_account,
															GridRefActionExt: 'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
														};
													};
												}
												if (item.attrcode == 'pk_vouchertype') {
													//设置凭证类别过滤
													item.queryCondition = () => {
														return {
															pk_org: pk_accountingbook,
															isDataPowerEnable: 'Y',
															DataPowerOperationCode: 'fi',
															GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder'
														};
													};
												}

												// return { 
												// 	"GridRefActionExt": 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
												// 	"pk_accperiodscheme": pk_account
												// }
											},
											onChange: (v) => {
												let self = this;
												let { listItem } = self.state;
												listItem[item.itemKey].value = v.refpk;
												listItem[item.itemKey].display = v.refname;
												listItem[item.itemKey].values = v.values;
												self.setState(
													{
														listItem
													},
													() => {
														//self.props.onConfirm(listItem);
													}
												);
											}
										})
									) : (
										<div />
									)}
								</FormItem>
							);
						}
					case 'date':
						// let  date=listItem[item.itemKey];
						//    let newDate = [];
						//    if (date && Object.prototype.toString.call(date) == '[object Array]') {
						// 	   if (date.length > 1) {
						// 		   date.map((val) => {
						// 			  // newDate.push(moment(val));
						// 		   });
						// 	   }
						//    }
						//    newDate.length == 0 ? (newDate = '') : newDate;
						return (
							<FormItem
								inline={true}
								showMast={true}
								labelName={item.itemName}
								method="blur"
								inputAlfer="%"
								//errorMessage="输入格式错误"
							>
								<RangePicker
									name={item.itemKey}
									//	type="customer"
									//format={format}
									//onSelect={onDateSelect}
									//onChange={onDateChange.bind(this, index)}
									// locale={zhCN}
									//	value={val}
									showClear={true}
									placeholder={data.label}
									isRequire={true}
									format={format}
									//disabled={isChange}
									//value={moment(listItem[item.itemKey].value)}
									value={listItem[item.itemKey]}
									//value={listItem[item.itemKey]}
									// locale={zhCN}
									onChange={(v) => {
										//	listItem[item.itemKey] = v[0].format('YYYY-MM-DD');
										//let newData = {};
										if (isArray(v)) {
											listItem[item.itemKey] = v;
											// newData.firstvalue = v[0].format('YYYY-MM-DD');
											// newData.secondvalue = v[1].format('YYYY-MM-DD');
											//listItem[item.itemKey].push(newData)
										}

										self.setState(
											{
												listItem
											},
											() => {
												// self.props.onConfirm(listItem);
											}
										);
									}}
									//placeholder={dateInputPlaceholder}
								/>
							</FormItem>
						);
					case 'textInput':
						return (
							<FormItem
								inline={true}
								showMast={true}
								labelName={item.itemName}
								// isRequire={true}
								method="change"
								//  change={self.handleGTypeChange.bind(this, 'contracttype')}
							>
								<FormControl
									value={listItem[item.itemKey] ? listItem[item.itemKey].value : ''}
									//disabled={true}
									onChange={(v) => {
										listItem[item.itemKey] = {
											value: v
										};
										this.setState(
											{
												listItem
											},
											() => {
											}
										);
									}}
								/>
							</FormItem>
						);
					case 'radio':
						return (
							<FormItem
								inline={true}
								showMast={false}
								labelName={item.itemName}
								isRequire={true}
								method="change"
								//  change={self.handleGTypeChange.bind(this, 'contracttype')}
							>
								<RadioItem
									name={item.itemKey}
									type="customer"
									//  defaultValue={this.state.assureInfo.contracttype.value}
									items={() => {
										return item.itemChild;
									}}
									onChange={(v) => {
										listItem[item.itemKey] = v;
										this.setState({
											listItem
										});
									}}
								/>
							</FormItem>
						);
					case 'select':
						return (
							<FormItem
								inline={true}
								showMast={false}
								labelName={item.itemName}
								isRequire={true}
								method="change"
								//  change={self.handleGTypeChange.bind(this, 'contracttype')}
							>
								<SelectItem
									name={item.itemKey}
									//  defaultValue={this.state[item.itemKey].value?this.state[item.itemKey].value:'all'}
									items={() => {
										return item.itemChild;
									}}
									onChange={(v) => {
										listItem[item.itemKey] = v;
										this.setState({
											listItem
										});
									}}
								/>
							</FormItem>
						);
					case 'checkbox':
						return (
							<CheckboxItem
								//defaultValue={this.state.periodloan.value}
								boxs={() => item.itemChild}
								onChange={(v) => {
									v.map((item, index) => {
										listItem[item.itemKey] = item.checked;
									});
									this.setState({ listItem });
								}}
							/>
						);
					default:
						break;
				}
			})
		) : (
			<div />
		);
	};

	render() {
		let { data, dataRows, listItem } = this.state;
		let { show, loadData } = this.props;
		return (
			<Modal 
			fieldid="location"
			show={show} onHide={() => {
				this.props.onCancel(false);
			}} className="senior simpleModal modal-use-form-componet">
				<Modal.Header closeButton>
					<Modal.Title>{this.state.json['20021005card-000318']/* 国际化处理： 凭证查询*/}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="nc-theme-form-label-c">
					<NCForm submitAreaClassName="classArea" showSubmit={false}>
						{this.queryList(loadData)}
					</NCForm>
					</div>
				</Modal.Body>
				<Modal.Footer>
					<Button
						className="button-primary"
						onClick={() => {
							this.props.onConfirm(listItem);
						}}
					>
						{this.state.json['20021005card-000003']/* 国际化处理： 确定*/}
					</Button>
					<Button
						onClick={() => {
							this.props.onCancel(false);
						}}
					>
						{this.state.json['20021005card-000004']/* 国际化处理： 取消*/}
					</Button>
				</Modal.Footer>
				{/* <Loading fullScreen showBackDrop={true} show={this.state.isLoading} /> */}
			</Modal>
		);
	}
}
SearchModal.defaultProps = defaultProps12;
